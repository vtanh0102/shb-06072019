 update DYNAMIC_BUTTONS set DELETED = 1, DATE_MODIFIED_UTC = getutcdate(), MODIFIED_USER_ID = null where DELETED = 0 and VIEW_NAME = 'KPIM0106.MassUpdate';
if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0106.MassUpdate' and DELETED = 0) begin -- then
	print 'DYNAMIC_BUTTONS KPIM0106.MassUpdate';
	exec dbo.spDYNAMIC_BUTTONS_InsButton     'KPIM0106.MassUpdate',  0, 'KPIM0106', 'edit', null, null, 'MassSubmitApproval', null, '.LBL_SUBMIT_APPROVAL', '.LBL_SUBMIT_APPROVAL', null, 'if ( !ValidateOne() ) return false;', 0;
	exec dbo.spDYNAMIC_BUTTONS_InsButton     'KPIM0106.MassUpdate',  1, 'KPIM0106', 'edit', null, null, 'MassApprove'       , null, '.LBL_APPROVE'        , '.LBL_APPROVE'        , null, 'if ( !ValidateOne() ) return false;', 0;
	exec dbo.spDYNAMIC_BUTTONS_InsButton     'KPIM0106.MassUpdate',  2, 'KPIM0106', 'edit', null, null, 'MassReject'        , null, '.LBL_REJECT'         , '.LBL_REJECT'         , null, 'if ( !ValidateOne() ) return false;', 0;
end -- if;
GO