ALTER TABLE dbo.M_ORGANIZATION DROP COLUMN BUSSINES_ID;
ALTER TABLE dbo.M_ORGANIZATION_AUDIT DROP COLUMN BUSSINES_ID;

ALTER TABLE dbo.M_ORGANIZATION ADD BUSSINES_ID uniqueidentifier;
ALTER TABLE dbo.M_ORGANIZATION_AUDIT ADD BUSSINES_ID uniqueidentifier;

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_ORGANIZATION' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_ORGANIZATION';
	Create Table dbo.M_ORGANIZATION
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_M_ORGANIZATION primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, ORGANIZATION_NAME                  nvarchar(200) not null
		, ORGANIZATION_CODE                  nvarchar(50) not null
		, LEVEL_NUMBER                       int null
		, DESCRIPTION                        nvarchar(500) null
		, BUSSINES_ID                        uniqueidentifier null
		, BUSINESS_CODE                      nvarchar(50) null
		, STATUS                             nvarchar(5) null
		, ORIGINAL_CODE                      nvarchar(50) null
		, AREA_ID                            uniqueidentifier null
		, REGION_ID                          uniqueidentifier null
		, PARENT_ID                          uniqueidentifier null

		)

	create index IDX_M_ORGANIZATION_ASSIGNED_USER_ID on dbo.M_ORGANIZATION (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_ORGANIZATION_TEAM_ID          on dbo.M_ORGANIZATION (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_ORGANIZATION_TEAM_SET_ID      on dbo.M_ORGANIZATION (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_ORGANIZATION_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_ORGANIZATION_CSTM';
	Create Table dbo.M_ORGANIZATION_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_M_ORGANIZATION_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table M_ORGANIZATION add ASSIGNED_USER_ID uniqueidentifier null';
	alter table M_ORGANIZATION add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_M_ORGANIZATION_ASSIGNED_USER_ID on dbo.M_ORGANIZATION (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table M_ORGANIZATION add TEAM_ID uniqueidentifier null';
	alter table M_ORGANIZATION add TEAM_ID uniqueidentifier null;
	create index IDX_M_ORGANIZATION_TEAM_ID          on dbo.M_ORGANIZATION (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table M_ORGANIZATION add TEAM_SET_ID uniqueidentifier null';
	alter table M_ORGANIZATION add TEAM_SET_ID uniqueidentifier null;
	create index IDX_M_ORGANIZATION_TEAM_SET_ID      on dbo.M_ORGANIZATION (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'ORGANIZATION_NAME') begin -- then
	print 'alter table M_ORGANIZATION add ORGANIZATION_NAME nvarchar(200) null';
	alter table M_ORGANIZATION add ORGANIZATION_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'ORGANIZATION_CODE') begin -- then
	print 'alter table M_ORGANIZATION add ORGANIZATION_CODE nvarchar(50) null';
	alter table M_ORGANIZATION add ORGANIZATION_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'LEVEL_NUMBER') begin -- then
	print 'alter table M_ORGANIZATION add LEVEL_NUMBER int null';
	alter table M_ORGANIZATION add LEVEL_NUMBER int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table M_ORGANIZATION add DESCRIPTION nvarchar(500) null';
	alter table M_ORGANIZATION add DESCRIPTION nvarchar(500) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'BUSSINES_ID') begin -- then
	print 'alter table M_ORGANIZATION add BUSSINES_ID uniqueidentifier null';
	alter table M_ORGANIZATION add BUSSINES_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'BUSINESS_CODE') begin -- then
	print 'alter table M_ORGANIZATION add BUSINESS_CODE nvarchar(50) null';
	alter table M_ORGANIZATION add BUSINESS_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'STATUS') begin -- then
	print 'alter table M_ORGANIZATION add STATUS nvarchar(5) null';
	alter table M_ORGANIZATION add STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'ORIGINAL_CODE') begin -- then
	print 'alter table M_ORGANIZATION add ORIGINAL_CODE nvarchar(50) null';
	alter table M_ORGANIZATION add ORIGINAL_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'AREA_ID') begin -- then
	print 'alter table M_ORGANIZATION add AREA_ID uniqueidentifier null';
	alter table M_ORGANIZATION add AREA_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'REGION_ID') begin -- then
	print 'alter table M_ORGANIZATION add REGION_ID uniqueidentifier null';
	alter table M_ORGANIZATION add REGION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_ORGANIZATION' and COLUMN_NAME = 'PARENT_ID') begin -- then
	print 'alter table M_ORGANIZATION add PARENT_ID uniqueidentifier null';
	alter table M_ORGANIZATION add PARENT_ID uniqueidentifier null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_ORGANIZATION')
	Drop View dbo.vwM_ORGANIZATION;
GO


Create View dbo.vwM_ORGANIZATION
as
select M_ORGANIZATION.ID
     , M_ORGANIZATION.ORGANIZATION_NAME
     , M_ORGANIZATION.ORGANIZATION_CODE
     , M_ORGANIZATION.LEVEL_NUMBER
     , M_ORGANIZATION.DESCRIPTION
     , M_ORGANIZATION.BUSSINES_ID
     , M_ORGANIZATION.BUSINESS_CODE
     , M_ORGANIZATION.STATUS
     , M_ORGANIZATION.ORIGINAL_CODE
     , M_ORGANIZATION.AREA_ID
     , M_ORGANIZATION.REGION_ID
     , M_ORGANIZATION.PARENT_ID
     , M_ORGANIZATION.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , M_ORGANIZATION.DATE_ENTERED
     , M_ORGANIZATION.DATE_MODIFIED
     , M_ORGANIZATION.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , M_ORGANIZATION.CREATED_BY      as CREATED_BY_ID
     , M_ORGANIZATION.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , M_ORGANIZATION_CSTM.*
  from            M_ORGANIZATION
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = M_ORGANIZATION.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = M_ORGANIZATION.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = M_ORGANIZATION.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = M_ORGANIZATION.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = M_ORGANIZATION.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = M_ORGANIZATION.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = M_ORGANIZATION.MODIFIED_USER_ID
  left outer join M_ORGANIZATION_CSTM
               on M_ORGANIZATION_CSTM.ID_C     = M_ORGANIZATION.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = M_ORGANIZATION.ID
 where M_ORGANIZATION.DELETED = 0

GO

Grant Select on dbo.vwM_ORGANIZATION to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_ORGANIZATION_Edit')
	Drop View dbo.vwM_ORGANIZATION_Edit;
GO


Create View dbo.vwM_ORGANIZATION_Edit
as
select *
  from vwM_ORGANIZATION

GO

Grant Select on dbo.vwM_ORGANIZATION_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_ORGANIZATION_List')
	Drop View dbo.vwM_ORGANIZATION_List;
GO


Create View dbo.vwM_ORGANIZATION_List
as
select *
  from vwM_ORGANIZATION

GO

Grant Select on dbo.vwM_ORGANIZATION_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_Delete;
GO


Create Procedure dbo.spM_ORGANIZATION_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update M_ORGANIZATION
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spM_ORGANIZATION_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_Undelete;
GO


Create Procedure dbo.spM_ORGANIZATION_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIM0106';
	
	-- BEGIN Oracle Exception
		update M_ORGANIZATION
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from M_ORGANIZATION_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spM_ORGANIZATION_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_Update;
GO


Create Procedure dbo.spM_ORGANIZATION_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @ORGANIZATION_NAME                  nvarchar(200)
	, @ORGANIZATION_CODE                  nvarchar(50)
	, @LEVEL_NUMBER                       int
	, @DESCRIPTION                        nvarchar(500)
	, @BUSSINES_ID                        uniqueidentifier
	, @BUSINESS_CODE                      nvarchar(50)
	, @STATUS                             nvarchar(5)
	, @ORIGINAL_CODE                      nvarchar(50)
	, @AREA_ID                            uniqueidentifier
	, @REGION_ID                          uniqueidentifier
	, @PARENT_ID                          uniqueidentifier

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from M_ORGANIZATION where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into M_ORGANIZATION
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, ORGANIZATION_NAME                  
			, ORGANIZATION_CODE                  
			, LEVEL_NUMBER                       
			, DESCRIPTION                        
			, BUSSINES_ID                        
			, BUSINESS_CODE                      
			, STATUS                             
			, ORIGINAL_CODE                      
			, AREA_ID                            
			, REGION_ID                          
			, PARENT_ID                          

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @ORGANIZATION_NAME                  
			, @ORGANIZATION_CODE                  
			, @LEVEL_NUMBER                       
			, @DESCRIPTION                        
			, @BUSSINES_ID                        
			, @BUSINESS_CODE                      
			, @STATUS                             
			, @ORIGINAL_CODE                      
			, @AREA_ID                            
			, @REGION_ID                          
			, @PARENT_ID                          

			);
	end else begin
		update M_ORGANIZATION
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , ORGANIZATION_NAME                    = @ORGANIZATION_NAME                  
		     , ORGANIZATION_CODE                    = @ORGANIZATION_CODE                  
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , BUSSINES_ID                          = @BUSSINES_ID                        
		     , BUSINESS_CODE                        = @BUSINESS_CODE                      
		     , STATUS                               = @STATUS                             
		     , ORIGINAL_CODE                        = @ORIGINAL_CODE                      
		     , AREA_ID                              = @AREA_ID                            
		     , REGION_ID                            = @REGION_ID                          
		     , PARENT_ID                            = @PARENT_ID                          

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from M_ORGANIZATION_CSTM where ID_C = @ID) begin -- then
			insert into M_ORGANIZATION_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0106', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spM_ORGANIZATION_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_MassDelete;
GO


Create Procedure dbo.spM_ORGANIZATION_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spM_ORGANIZATION_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spM_ORGANIZATION_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_MassUpdate;
GO


Create Procedure dbo.spM_ORGANIZATION_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from M_ORGANIZATION
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIM0106', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0106', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update M_ORGANIZATION
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spM_ORGANIZATION_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_ORGANIZATION_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_ORGANIZATION_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spM_ORGANIZATION_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spM_ORGANIZATION_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spM_ORGANIZATION_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_ORGANIZATION';
	exec dbo.spSqlBuildAuditTrigger 'M_ORGANIZATION';
	exec dbo.spSqlBuildAuditView    'M_ORGANIZATION';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0106.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0106.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIM0106.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIM0106.DetailView'   , 'KPIM0106', 'vwM_ORGANIZATION_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 1, 'KPIM0106.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 2, 'KPIM0106.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 3, 'KPIM0106.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 4, 'KPIM0106.LBL_BUSSINES_ID', 'BUSSINES_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 5, 'KPIM0106.LBL_BUSINESS_CODE', 'BUSINESS_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 6, 'KPIM0106.LBL_STATUS', 'STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 7, 'KPIM0106.LBL_ORIGINAL_CODE', 'ORIGINAL_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 8, 'KPIM0106.LBL_AREA_ID', 'AREA_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 9, 'KPIM0106.LBL_REGION_ID', 'REGION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 10, 'KPIM0106.LBL_PARENT_ID', 'PARENT_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 11, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 12, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 13, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0106.DetailView', 14, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIM0106.DetailView', 'KPIM0106';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIM0106.EditView'  , 'KPIM0106';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIM0106.PopupView' , 'KPIM0106';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0106.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIM0106.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0106.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIM0106.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0106.EditView', 'KPIM0106'      , 'vwM_ORGANIZATION_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 1, 'KPIM0106.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 2, 'KPIM0106.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 3, 'KPIM0106.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1, 500, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView', 4, 'KPIM0106.LBL_BUSSINES_ID', 'BUSSINES_ID', 0, 1, 'BUSSINES_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 5, 'KPIM0106.LBL_BUSINESS_CODE', 'BUSINESS_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 6, 'KPIM0106.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView', 7, 'KPIM0106.LBL_ORIGINAL_CODE', 'ORIGINAL_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView', 8, 'KPIM0106.LBL_AREA_ID', 'AREA_ID', 0, 1, 'AREA_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView', 9, 'KPIM0106.LBL_REGION_ID', 'REGION_ID', 0, 1, 'REGION_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView', 10, 'KPIM0106.LBL_PARENT_ID', 'PARENT_ID', 0, 1, 'PARENT_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.EditView', 11, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.EditView', 12, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0106.EditView.Inline', 'KPIM0106'      , 'vwM_ORGANIZATION_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 1, 'KPIM0106.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 2, 'KPIM0106.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 3, 'KPIM0106.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1, 500, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView.Inline', 4, 'KPIM0106.LBL_BUSSINES_ID', 'BUSSINES_ID', 0, 1, 'BUSSINES_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 5, 'KPIM0106.LBL_BUSINESS_CODE', 'BUSINESS_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 6, 'KPIM0106.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.EditView.Inline', 7, 'KPIM0106.LBL_ORIGINAL_CODE', 'ORIGINAL_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView.Inline', 8, 'KPIM0106.LBL_AREA_ID', 'AREA_ID', 0, 1, 'AREA_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView.Inline', 9, 'KPIM0106.LBL_REGION_ID', 'REGION_ID', 0, 1, 'REGION_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.EditView.Inline', 10, 'KPIM0106.LBL_PARENT_ID', 'PARENT_ID', 0, 1, 'PARENT_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.EditView.Inline', 11, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.EditView.Inline', 12, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0106.PopupView.Inline', 'KPIM0106'      , 'vwM_ORGANIZATION_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 1, 'KPIM0106.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 2, 'KPIM0106.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 3, 'KPIM0106.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1, 500, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.PopupView.Inline', 4, 'KPIM0106.LBL_BUSSINES_ID', 'BUSSINES_ID', 0, 1, 'BUSSINES_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 5, 'KPIM0106.LBL_BUSINESS_CODE', 'BUSINESS_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 6, 'KPIM0106.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0106.PopupView.Inline', 7, 'KPIM0106.LBL_ORIGINAL_CODE', 'ORIGINAL_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.PopupView.Inline', 8, 'KPIM0106.LBL_AREA_ID', 'AREA_ID', 0, 1, 'AREA_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.PopupView.Inline', 9, 'KPIM0106.LBL_REGION_ID', 'REGION_ID', 0, 1, 'REGION_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIM0106.PopupView.Inline', 10, 'KPIM0106.LBL_PARENT_ID', 'PARENT_ID', 0, 1, 'PARENT_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.PopupView.Inline', 11, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0106.PopupView.Inline', 12, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0106.SearchBasic'    , 'KPIM0106', 'vwM_ORGANIZATION_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchBasic', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIM0106.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0106.SearchAdvanced' , 'KPIM0106', 'vwM_ORGANIZATION_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 1, 'KPIM0106.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 2, 'KPIM0106.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 3, 'KPIM0106.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1, 500, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIM0106.SearchAdvanced', 4, 'KPIM0106.LBL_BUSSINES_ID', 'BUSSINES_ID', 0, 1, 'BUSSINES_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 5, 'KPIM0106.LBL_BUSINESS_CODE', 'BUSINESS_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 6, 'KPIM0106.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchAdvanced', 7, 'KPIM0106.LBL_ORIGINAL_CODE', 'ORIGINAL_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIM0106.SearchAdvanced', 8, 'KPIM0106.LBL_AREA_ID', 'AREA_ID', 0, 1, 'AREA_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIM0106.SearchAdvanced', 9, 'KPIM0106.LBL_REGION_ID', 'REGION_ID', 0, 1, 'REGION_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIM0106.SearchAdvanced', 10, 'KPIM0106.LBL_PARENT_ID', 'PARENT_ID', 0, 1, 'PARENT_NAME', 'return KPIM0106Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0106.SearchAdvanced' , 11, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0106.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0106.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0106.SearchPopup'    , 'KPIM0106', 'vwM_ORGANIZATION_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0106.SearchPopup', 0, 'KPIM0106.LBL_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 1, 1, 200, 35, null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0106.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0106.Export', 'KPIM0106', 'vwM_ORGANIZATION_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0106.Export'         ,  1, 'KPIM0106.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0106.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0106.ListView', 'KPIM0106'      , 'vwM_ORGANIZATION_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0106.ListView', 2, 'KPIM0106.LBL_LIST_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 'ORGANIZATION_NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIM0106/view.aspx?id={0}', null, 'KPIM0106', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0106.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0106.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0106.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0106.PopupView', 'KPIM0106'      , 'vwM_ORGANIZATION_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0106.PopupView', 1, 'KPIM0106.LBL_LIST_ORGANIZATION_NAME', 'ORGANIZATION_NAME', 'ORGANIZATION_NAME', '45%', 'listViewTdLinkS1', 'ID ORGANIZATION_NAME', 'SelectKPIM0106(''{0}'', ''{1}'');', null, 'KPIM0106', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0106.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0106.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0106.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0106.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0106.SearchDuplicates', 'KPIM0106', 'vwM_ORGANIZATION_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0106.SearchDuplicates'          , 1, 'KPIM0106.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIM0106/view.aspx?id={0}', null, 'KPIM0106', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIM0106', '.moduleList.KPIM0106', '~/KPIM0106/', 1, 1, 100, 0, 1, 1, 1, 0, 'M_ORGANIZATION', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIM0106';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIM0106' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0106', 'KPIM0106.LNK_NEW_M_ORGANIZATION' , '~/KPIM0106/edit.aspx'   , 'CreateKPIM0106.gif', 1,  1, 'KPIM0106', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0106', 'KPIM0106.LNK_M_ORGANIZATION_LIST', '~/KPIM0106/default.aspx', 'KPIM0106.gif'      , 1,  2, 'KPIM0106', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0106', '.LBL_IMPORT'              , '~/KPIM0106/import.aspx' , 'Import.gif'        , 1,  3, 'KPIM0106', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0106', '.LNK_ACTIVITY_STREAM'     , '~/KPIM0106/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIM0106', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIM0106', null, null, N'Quản lý phòng ban List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIM0106', null, null, N'Create Quản lý phòng ban';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_M_ORGANIZATION_LIST'                          , N'en-US', N'KPIM0106', null, null, N'Quản lý phòng ban';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_M_ORGANIZATION'                           , N'en-US', N'KPIM0106', null, null, N'Create Quản lý phòng ban';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIM0106', null, null, N'Quản lý phòng ban Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_M_ORGANIZATION_NOT_FOUND'                     , N'en-US', N'KPIM0106', null, null, N'Quản lý phòng ban not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_M_ORGANIZATION_CONFIRMATION'           , N'en-US', N'KPIM0106', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIM0106', null, null, N'Quản lý phòng ban';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIM0106', null, null, N'Quả';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIM0106'                                          , N'en-US', null, N'moduleList', 100, N'Quản lý phòng ban';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_NAME'                                 , 'en-US', 'KPIM0106', null, null, 'organization name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_NAME'                            , 'en-US', 'KPIM0106', null, null, 'organization name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_CODE'                                 , 'en-US', 'KPIM0106', null, null, 'organization code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_CODE'                            , 'en-US', 'KPIM0106', null, null, 'organization code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LEVEL_NUMBER'                                      , 'en-US', 'KPIM0106', null, null, 'level number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_LEVEL_NUMBER'                                 , 'en-US', 'KPIM0106', null, null, 'level number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIM0106', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIM0106', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_BUSSINES_ID'                                       , 'en-US', 'KPIM0106', null, null, 'bussines id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_BUSSINES_ID'                                  , 'en-US', 'KPIM0106', null, null, 'bussines id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_BUSINESS_CODE'                                     , 'en-US', 'KPIM0106', null, null, 'business code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_BUSINESS_CODE'                                , 'en-US', 'KPIM0106', null, null, 'business code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STATUS'                                            , 'en-US', 'KPIM0106', null, null, 'status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STATUS'                                       , 'en-US', 'KPIM0106', null, null, 'status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORIGINAL_CODE'                                     , 'en-US', 'KPIM0106', null, null, 'original code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORIGINAL_CODE'                                , 'en-US', 'KPIM0106', null, null, 'original code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_AREA_ID'                                           , 'en-US', 'KPIM0106', null, null, 'area id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_AREA_ID'                                      , 'en-US', 'KPIM0106', null, null, 'area id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REGION_ID'                                         , 'en-US', 'KPIM0106', null, null, 'region id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REGION_ID'                                    , 'en-US', 'KPIM0106', null, null, 'region id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PARENT_ID'                                         , 'en-US', 'KPIM0106', null, null, 'parent id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PARENT_ID'                                    , 'en-US', 'KPIM0106', null, null, 'parent id';

-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_ORGANIZATION_CSRM';
	exec dbo.spSqlBuildAuditTrigger 'M_ORGANIZATION_CSRM';
	exec dbo.spSqlBuildAuditView    'M_ORGANIZATION_CSRM';
end -- if;
GO




