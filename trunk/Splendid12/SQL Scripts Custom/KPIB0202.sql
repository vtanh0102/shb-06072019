
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATES' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATES';
	Create Table dbo.B_KPI_ALLOCATES
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATES primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, ALLOCATE_NAME                      nvarchar(200) not null
		, ALLOCATE_CODE                      nvarchar(50) not null
		, YEAR                               int not null
		, PERIOD                             nvarchar(5) null
		, VERSION_NUMBER                     nvarchar(50) null
		, APPROVE_STATUS                     nvarchar(5) null
		, APPROVED_BY                        uniqueidentifier null
		, APPROVED_DATE                      datetime null
		, ALLOCATE_TYPE                      nvarchar(5) null
		, ORGANIZATION_ID                    uniqueidentifier null
		, EMPLOYEE_ID                        uniqueidentifier null
		, STATUS                             nvarchar(5) null
		, KPI_STANDARD_ID                    uniqueidentifier not null
		, FILE_ID                            uniqueidentifier null
		, ASSIGN_BY                          uniqueidentifier not null
		, ASSIGN_DATE                        datetime null
		, STAFT_NUMBER                       int null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null

		)

	create index IDX_B_KPI_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATES_TEAM_ID          on dbo.B_KPI_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATES_ACCOUNTS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATES_ACCOUNTS';
	Create Table dbo.B_KPI_ALLOCATES_ACCOUNTS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATES_ACCOUNTS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, B_KPI_ALLOCATE_ID             uniqueidentifier not null
		, ACCOUNT_ID          uniqueidentifier not null
		)

	create index IDX_B_KPI_ALLOCATES_ACCOUNTS_B_KPI_ALLOCATE_ID    on dbo.B_KPI_ALLOCATES_ACCOUNTS (B_KPI_ALLOCATE_ID, ACCOUNT_ID, DELETED)
	create index IDX_B_KPI_ALLOCATES_ACCOUNTS_ACCOUNT_ID on dbo.B_KPI_ALLOCATES_ACCOUNTS (ACCOUNT_ID, B_KPI_ALLOCATE_ID, DELETED)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATES_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATES_CSTM';
	Create Table dbo.B_KPI_ALLOCATES_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ALLOCATES_CSTM primary key
		)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATE_DETAILS';
	Create Table dbo.B_KPI_ALLOCATE_DETAILS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATE_DETAILS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, KPI_NAME                           nvarchar(200) not null
		, KPI_ALLOCATE_ID                    uniqueidentifier not null
		, UNIT                               int null
		, RADIO                              float null
		, MAX_RATIO_COMPLETE                 float null
		, KPI_STANDARD_DETAIL_ID             uniqueidentifier null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, MONTH_1                            money null
		, MONTH_2                            money null
		, MONTH_3                            money null
		, MONTH_4                            money null
		, MONTH_5                            money null
		, MONTH_6                            money null
		, MONTH_7                            money null
		, MONTH_8                            money null
		, MONTH_9                            money null
		, MONTH_10                           money null
		, MONTH_11                           money null
		, MONTH_12                           money null

		)

	create index IDX_B_KPI_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS_ACCOUNTS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATE_DETAILS_ACCOUNTS';
	Create Table dbo.B_KPI_ALLOCATE_DETAILS_ACCOUNTS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATE_DETAILS_ACCOUNTS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, B_KPI_ALLOCATE_DETAIL_ID             uniqueidentifier not null
		, ACCOUNT_ID          uniqueidentifier not null
		)

	create index IDX_B_KPI_ALLOCATE_DETAILS_ACCOUNTS_B_KPI_ALLOCATE_DETAIL_ID    on dbo.B_KPI_ALLOCATE_DETAILS_ACCOUNTS (B_KPI_ALLOCATE_DETAIL_ID, ACCOUNT_ID, DELETED)
	create index IDX_B_KPI_ALLOCATE_DETAILS_ACCOUNTS_ACCOUNT_ID on dbo.B_KPI_ALLOCATE_DETAILS_ACCOUNTS (ACCOUNT_ID, B_KPI_ALLOCATE_DETAIL_ID, DELETED)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATE_DETAILS_CSTM';
	Create Table dbo.B_KPI_ALLOCATE_DETAILS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ALLOCATE_DETAILS_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_TEAM_ID          on dbo.B_KPI_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_NAME') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_NAME nvarchar(200) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_CODE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_CODE nvarchar(50) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'YEAR') begin -- then
	print 'alter table B_KPI_ALLOCATES add YEAR int null';
	alter table B_KPI_ALLOCATES add YEAR int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'PERIOD') begin -- then
	print 'alter table B_KPI_ALLOCATES add PERIOD nvarchar(5) null';
	alter table B_KPI_ALLOCATES add PERIOD nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATES add VERSION_NUMBER nvarchar(50) null';
	alter table B_KPI_ALLOCATES add VERSION_NUMBER nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVE_STATUS') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVE_STATUS nvarchar(5) null';
	alter table B_KPI_ALLOCATES add APPROVE_STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVED_BY') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVED_BY uniqueidentifier null';
	alter table B_KPI_ALLOCATES add APPROVED_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVED_DATE') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVED_DATE datetime null';
	alter table B_KPI_ALLOCATES add APPROVED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_TYPE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_TYPE nvarchar(5) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_TYPE nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ORGANIZATION_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add ORGANIZATION_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ORGANIZATION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'EMPLOYEE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add EMPLOYEE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add EMPLOYEE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'STATUS') begin -- then
	print 'alter table B_KPI_ALLOCATES add STATUS nvarchar(5) null';
	alter table B_KPI_ALLOCATES add STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'KPI_STANDARD_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add KPI_STANDARD_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add KPI_STANDARD_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FILE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add FILE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add FILE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGN_BY') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGN_BY uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ASSIGN_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGN_DATE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGN_DATE datetime null';
	alter table B_KPI_ALLOCATES add ASSIGN_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'STAFT_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATES add STAFT_NUMBER int null';
	alter table B_KPI_ALLOCATES add STAFT_NUMBER int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ALLOCATES add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ALLOCATES add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ALLOCATES add REMARK nvarchar(max) null';
	alter table B_KPI_ALLOCATES add REMARK nvarchar(max) null;
end -- if;


GO


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_NAME') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_NAME nvarchar(200) null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_ALLOCATE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_ALLOCATE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_ALLOCATE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'UNIT') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add UNIT int null';
	alter table B_KPI_ALLOCATE_DETAILS add UNIT int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'RADIO') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add RADIO float null';
	alter table B_KPI_ALLOCATE_DETAILS add RADIO float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MAX_RATIO_COMPLETE') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MAX_RATIO_COMPLETE float null';
	alter table B_KPI_ALLOCATE_DETAILS add MAX_RATIO_COMPLETE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_STANDARD_DETAIL_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_STANDARD_DETAIL_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_STANDARD_DETAIL_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ALLOCATE_DETAILS add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add REMARK nvarchar(max) null';
	alter table B_KPI_ALLOCATE_DETAILS add REMARK nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_1') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_1 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_1 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_2') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_2 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_2 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_3') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_3 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_3 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_4') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_4 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_4 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_5') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_5 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_5 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_6') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_6 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_6 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_7') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_7 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_7 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_8') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_8 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_8 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_9') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_9 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_9 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_10') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_10 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_10 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_11') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_11 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_11 money null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_12') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_12 money null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_12 money null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES')
	Drop View dbo.vwB_KPI_ALLOCATES;
GO


Create View dbo.vwB_KPI_ALLOCATES
as
select B_KPI_ALLOCATES.ID
     , B_KPI_ALLOCATES.ALLOCATE_NAME
     , B_KPI_ALLOCATES.ALLOCATE_CODE
     , B_KPI_ALLOCATES.YEAR
     , B_KPI_ALLOCATES.PERIOD
     , B_KPI_ALLOCATES.VERSION_NUMBER
     , B_KPI_ALLOCATES.APPROVE_STATUS
     , B_KPI_ALLOCATES.APPROVED_BY
     , B_KPI_ALLOCATES.APPROVED_DATE
     , B_KPI_ALLOCATES.ALLOCATE_TYPE
     , B_KPI_ALLOCATES.ORGANIZATION_ID
     , B_KPI_ALLOCATES.EMPLOYEE_ID
     , B_KPI_ALLOCATES.STATUS
     , B_KPI_ALLOCATES.KPI_STANDARD_ID
     , B_KPI_ALLOCATES.FILE_ID
     , B_KPI_ALLOCATES.ASSIGN_BY
     , B_KPI_ALLOCATES.ASSIGN_DATE
     , B_KPI_ALLOCATES.STAFT_NUMBER
     , B_KPI_ALLOCATES.DESCRIPTION
     , B_KPI_ALLOCATES.REMARK
     , B_KPI_ALLOCATES.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ALLOCATES.DATE_ENTERED
     , B_KPI_ALLOCATES.DATE_MODIFIED
     , B_KPI_ALLOCATES.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ALLOCATES.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ALLOCATES.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ALLOCATES_CSTM.*
  from            B_KPI_ALLOCATES
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ALLOCATES.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ALLOCATES.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ALLOCATES.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ALLOCATES.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ALLOCATES.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ALLOCATES.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ALLOCATES.MODIFIED_USER_ID
  left outer join B_KPI_ALLOCATES_CSTM
               on B_KPI_ALLOCATES_CSTM.ID_C     = B_KPI_ALLOCATES.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ALLOCATES.ID
 where B_KPI_ALLOCATES.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATES to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES_ACCOUNTS')
	Drop View dbo.vwB_KPI_ALLOCATES_ACCOUNTS;
GO


Create View dbo.vwB_KPI_ALLOCATES_ACCOUNTS
as
select B_KPI_ALLOCATES.ID               as B_KPI_ALLOCATE_ID
     , B_KPI_ALLOCATES.ALLOCATE_NAME             as B_KPI_ALLOCATE_NAME
     , B_KPI_ALLOCATES.ASSIGNED_USER_ID as B_KPI_ALLOCATE_ASSIGNED_USER_ID
     , vwACCOUNTS.ID                 as ACCOUNT_ID
     , vwACCOUNTS.NAME               as ACCOUNT_NAME
     , vwACCOUNTS.*
  from           B_KPI_ALLOCATES
      inner join B_KPI_ALLOCATES_ACCOUNTS
              on B_KPI_ALLOCATES_ACCOUNTS.B_KPI_ALLOCATE_ID = B_KPI_ALLOCATES.ID
             and B_KPI_ALLOCATES_ACCOUNTS.DELETED    = 0
      inner join vwACCOUNTS
              on vwACCOUNTS.ID                = B_KPI_ALLOCATES_ACCOUNTS.ACCOUNT_ID
 where B_KPI_ALLOCATES.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATES_ACCOUNTS to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS
as
select B_KPI_ALLOCATE_DETAILS.ID
     , B_KPI_ALLOCATE_DETAILS.KPI_NAME
     , B_KPI_ALLOCATE_DETAILS.KPI_ALLOCATE_ID
     , B_KPI_ALLOCATE_DETAILS.UNIT
     , B_KPI_ALLOCATE_DETAILS.RADIO
     , B_KPI_ALLOCATE_DETAILS.MAX_RATIO_COMPLETE
     , B_KPI_ALLOCATE_DETAILS.KPI_STANDARD_DETAIL_ID
     , B_KPI_ALLOCATE_DETAILS.DESCRIPTION
     , B_KPI_ALLOCATE_DETAILS.REMARK
     , B_KPI_ALLOCATE_DETAILS.MONTH_1
     , B_KPI_ALLOCATE_DETAILS.MONTH_2
     , B_KPI_ALLOCATE_DETAILS.MONTH_3
     , B_KPI_ALLOCATE_DETAILS.MONTH_4
     , B_KPI_ALLOCATE_DETAILS.MONTH_5
     , B_KPI_ALLOCATE_DETAILS.MONTH_6
     , B_KPI_ALLOCATE_DETAILS.MONTH_7
     , B_KPI_ALLOCATE_DETAILS.MONTH_8
     , B_KPI_ALLOCATE_DETAILS.MONTH_9
     , B_KPI_ALLOCATE_DETAILS.MONTH_10
     , B_KPI_ALLOCATE_DETAILS.MONTH_11
     , B_KPI_ALLOCATE_DETAILS.MONTH_12
     , B_KPI_ALLOCATE_DETAILS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ALLOCATE_DETAILS.DATE_ENTERED
     , B_KPI_ALLOCATE_DETAILS.DATE_MODIFIED
     , B_KPI_ALLOCATE_DETAILS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ALLOCATE_DETAILS.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ALLOCATE_DETAILS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ALLOCATE_DETAILS_CSTM.*
  from            B_KPI_ALLOCATE_DETAILS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ALLOCATE_DETAILS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ALLOCATE_DETAILS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ALLOCATE_DETAILS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ALLOCATE_DETAILS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ALLOCATE_DETAILS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ALLOCATE_DETAILS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ALLOCATE_DETAILS.MODIFIED_USER_ID
  left outer join B_KPI_ALLOCATE_DETAILS_CSTM
               on B_KPI_ALLOCATE_DETAILS_CSTM.ID_C     = B_KPI_ALLOCATE_DETAILS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ALLOCATE_DETAILS.ID
 where B_KPI_ALLOCATE_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS_ACCOUNTS')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS_ACCOUNTS;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS_ACCOUNTS
as
select B_KPI_ALLOCATE_DETAILS.ID               as B_KPI_ALLOCATE_DETAIL_ID
     , B_KPI_ALLOCATE_DETAILS.KPI_NAME             as B_KPI_ALLOCATE_DETAIL_NAME
     , B_KPI_ALLOCATE_DETAILS.ASSIGNED_USER_ID as B_KPI_ALLOCATE_DETAIL_ASSIGNED_USER_ID
     , vwACCOUNTS.ID                 as ACCOUNT_ID
     , vwACCOUNTS.NAME               as ACCOUNT_NAME
     , vwACCOUNTS.*
  from           B_KPI_ALLOCATE_DETAILS
      inner join B_KPI_ALLOCATE_DETAILS_ACCOUNTS
              on B_KPI_ALLOCATE_DETAILS_ACCOUNTS.B_KPI_ALLOCATE_DETAIL_ID = B_KPI_ALLOCATE_DETAILS.ID
             and B_KPI_ALLOCATE_DETAILS_ACCOUNTS.DELETED    = 0
      inner join vwACCOUNTS
              on vwACCOUNTS.ID                = B_KPI_ALLOCATE_DETAILS_ACCOUNTS.ACCOUNT_ID
 where B_KPI_ALLOCATE_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS_ACCOUNTS to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES_Edit')
	Drop View dbo.vwB_KPI_ALLOCATES_Edit;
GO


Create View dbo.vwB_KPI_ALLOCATES_Edit
as
select *
  from vwB_KPI_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ALLOCATES_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES_List')
	Drop View dbo.vwB_KPI_ALLOCATES_List;
GO


Create View dbo.vwB_KPI_ALLOCATES_List
as
select *
  from vwB_KPI_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ALLOCATES_List to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS_Edit')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS_Edit;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS_Edit
as
select *
  from vwB_KPI_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS_List')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS_List;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS_List
as
select *
  from vwB_KPI_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_ACCOUNTS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_ACCOUNTS_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_ACCOUNTS_Delete
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ALLOCATE_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ALLOCATE_ID    = @B_KPI_ALLOCATE_ID
	   and ACCOUNT_ID = @ACCOUNT_ID
	   and DELETED          = 0;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_ACCOUNTS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_ACCOUNTS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_ACCOUNTS_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_ACCOUNTS_Update
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ALLOCATE_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID uniqueidentifier;
	-- BEGIN Oracle Exception
		select @ID = ID
		  from B_KPI_ALLOCATES_ACCOUNTS
		 where B_KPI_ALLOCATE_ID = @B_KPI_ALLOCATE_ID
		   and ACCOUNT_ID = @ACCOUNT_ID
		   and DELETED           = 0;
	-- END Oracle Exception
	
	if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
		set @ID = newid();
		insert into B_KPI_ALLOCATES_ACCOUNTS
			( ID               
			, CREATED_BY       
			, DATE_ENTERED     
			, MODIFIED_USER_ID 
			, DATE_MODIFIED    
			, DATE_MODIFIED_UTC
			, B_KPI_ALLOCATE_ID
			, ACCOUNT_ID
			)
		values
			( @ID               
			, @MODIFIED_USER_ID 
			,  getdate()        
			, @MODIFIED_USER_ID 
			,  getdate()        
			,  getutcdate()     
			, @B_KPI_ALLOCATE_ID
			, @ACCOUNT_ID
			);
	end -- if;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_ACCOUNTS_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ALLOCATE_ID       = @ID
	   and DELETED          = 0;


	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATES
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Undelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ALLOCATE_ID       = @ID
	   and DELETED          = 0;


	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB0202';
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATES
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ALLOCATES_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @ALLOCATE_NAME                      nvarchar(200)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @YEAR                               int
	, @PERIOD                             nvarchar(5)
	, @VERSION_NUMBER                     nvarchar(50)
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @ALLOCATE_TYPE                      nvarchar(5)
	, @ORGANIZATION_ID                    uniqueidentifier
	, @EMPLOYEE_ID                        uniqueidentifier
	, @STATUS                             nvarchar(5)
	, @KPI_STANDARD_ID                    uniqueidentifier
	, @FILE_ID                            uniqueidentifier
	, @ASSIGN_BY                          uniqueidentifier
	, @ASSIGN_DATE                        datetime
	, @STAFT_NUMBER                       int
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ALLOCATES where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ALLOCATES
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, ALLOCATE_NAME                      
			, ALLOCATE_CODE                      
			, YEAR                               
			, PERIOD                             
			, VERSION_NUMBER                     
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, ALLOCATE_TYPE                      
			, ORGANIZATION_ID                    
			, EMPLOYEE_ID                        
			, STATUS                             
			, KPI_STANDARD_ID                    
			, FILE_ID                            
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, STAFT_NUMBER                       
			, DESCRIPTION                        
			, REMARK                             

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @ALLOCATE_NAME                      
			, @ALLOCATE_CODE                      
			, @YEAR                               
			, @PERIOD                             
			, @VERSION_NUMBER                     
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @ALLOCATE_TYPE                      
			, @ORGANIZATION_ID                    
			, @EMPLOYEE_ID                        
			, @STATUS                             
			, @KPI_STANDARD_ID                    
			, @FILE_ID                            
			, @ASSIGN_BY                          
			, @ASSIGN_DATE                        
			, @STAFT_NUMBER                       
			, @DESCRIPTION                        
			, @REMARK                             

			);
	end else begin
		update B_KPI_ALLOCATES
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , ALLOCATE_NAME                        = @ALLOCATE_NAME                      
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , YEAR                                 = @YEAR                               
		     , PERIOD                               = @PERIOD                             
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , ALLOCATE_TYPE                        = @ALLOCATE_TYPE                      
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID                    
		     , EMPLOYEE_ID                          = @EMPLOYEE_ID                        
		     , STATUS                               = @STATUS                             
		     , KPI_STANDARD_ID                      = @KPI_STANDARD_ID                    
		     , FILE_ID                              = @FILE_ID                            
		     , ASSIGN_BY                            = @ASSIGN_BY                          
		     , ASSIGN_DATE                          = @ASSIGN_DATE                        
		     , STAFT_NUMBER                         = @STAFT_NUMBER                       
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATES_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATES_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0202', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ALLOCATE_DETAIL_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATE_DETAILS_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ALLOCATE_DETAIL_ID    = @B_KPI_ALLOCATE_DETAIL_ID
	   and ACCOUNT_ID = @ACCOUNT_ID
	   and DELETED          = 0;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ALLOCATE_DETAIL_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID uniqueidentifier;
	-- BEGIN Oracle Exception
		select @ID = ID
		  from B_KPI_ALLOCATE_DETAILS_ACCOUNTS
		 where B_KPI_ALLOCATE_DETAIL_ID = @B_KPI_ALLOCATE_DETAIL_ID
		   and ACCOUNT_ID = @ACCOUNT_ID
		   and DELETED           = 0;
	-- END Oracle Exception
	
	if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
		set @ID = newid();
		insert into B_KPI_ALLOCATE_DETAILS_ACCOUNTS
			( ID               
			, CREATED_BY       
			, DATE_ENTERED     
			, MODIFIED_USER_ID 
			, DATE_MODIFIED    
			, DATE_MODIFIED_UTC
			, B_KPI_ALLOCATE_DETAIL_ID
			, ACCOUNT_ID
			)
		values
			( @ID               
			, @MODIFIED_USER_ID 
			,  getdate()        
			, @MODIFIED_USER_ID 
			,  getdate()        
			,  getutcdate()     
			, @B_KPI_ALLOCATE_DETAIL_ID
			, @ACCOUNT_ID
			);
	end -- if;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATE_DETAILS_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ALLOCATE_DETAIL_ID       = @ID
	   and DELETED          = 0;


	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATE_DETAILS
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Undelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	
	update B_KPI_ALLOCATE_DETAILS_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ALLOCATE_DETAIL_ID       = @ID
	   and DELETED          = 0;


	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB0202_DETAIL';
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATE_DETAILS
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ALLOCATE_DETAILS_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_NAME                           nvarchar(200)
	, @KPI_ALLOCATE_ID                    uniqueidentifier
	, @UNIT                               int
	, @RADIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @KPI_STANDARD_DETAIL_ID             uniqueidentifier
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @MONTH_1                            money
	, @MONTH_2                            money
	, @MONTH_3                            money
	, @MONTH_4                            money
	, @MONTH_5                            money
	, @MONTH_6                            money
	, @MONTH_7                            money
	, @MONTH_8                            money
	, @MONTH_9                            money
	, @MONTH_10                           money
	, @MONTH_11                           money
	, @MONTH_12                           money

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ALLOCATE_DETAILS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ALLOCATE_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_NAME                           
			, KPI_ALLOCATE_ID                    
			, UNIT                               
			, RADIO                              
			, MAX_RATIO_COMPLETE                 
			, KPI_STANDARD_DETAIL_ID             
			, DESCRIPTION                        
			, REMARK                             
			, MONTH_1                            
			, MONTH_2                            
			, MONTH_3                            
			, MONTH_4                            
			, MONTH_5                            
			, MONTH_6                            
			, MONTH_7                            
			, MONTH_8                            
			, MONTH_9                            
			, MONTH_10                           
			, MONTH_11                           
			, MONTH_12                           

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_NAME                           
			, @KPI_ALLOCATE_ID                    
			, @UNIT                               
			, @RADIO                              
			, @MAX_RATIO_COMPLETE                 
			, @KPI_STANDARD_DETAIL_ID             
			, @DESCRIPTION                        
			, @REMARK                             
			, @MONTH_1                            
			, @MONTH_2                            
			, @MONTH_3                            
			, @MONTH_4                            
			, @MONTH_5                            
			, @MONTH_6                            
			, @MONTH_7                            
			, @MONTH_8                            
			, @MONTH_9                            
			, @MONTH_10                           
			, @MONTH_11                           
			, @MONTH_12                           

			);
	end else begin
		update B_KPI_ALLOCATE_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , KPI_NAME                             = @KPI_NAME                           
		     , KPI_ALLOCATE_ID                      = @KPI_ALLOCATE_ID                    
		     , UNIT                                 = @UNIT                               
		     , RADIO                                = @RADIO                              
		     , MAX_RATIO_COMPLETE                   = @MAX_RATIO_COMPLETE                 
		     , KPI_STANDARD_DETAIL_ID               = @KPI_STANDARD_DETAIL_ID             
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , MONTH_1                              = @MONTH_1                            
		     , MONTH_2                              = @MONTH_2                            
		     , MONTH_3                              = @MONTH_3                            
		     , MONTH_4                              = @MONTH_4                            
		     , MONTH_5                              = @MONTH_5                            
		     , MONTH_6                              = @MONTH_6                            
		     , MONTH_7                              = @MONTH_7                            
		     , MONTH_8                              = @MONTH_8                            
		     , MONTH_9                              = @MONTH_9                            
		     , MONTH_10                             = @MONTH_10                           
		     , MONTH_11                             = @MONTH_11                           
		     , MONTH_12                             = @MONTH_12                           

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATE_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATE_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0202_DETAIL', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_MassDelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ALLOCATES_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ALLOCATES_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ALLOCATES
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB0202', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0202', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ALLOCATES
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ALLOCATES_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on

	update B_KPI_ALLOCATES_ACCOUNTS
	   set B_KPI_ALLOCATE_ID       = @ID
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ALLOCATE_ID       = @MERGE_ID
	   and DELETED          = 0;



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ALLOCATES_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Merge to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ALLOCATE_DETAILS_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ALLOCATE_DETAILS
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB0202_DETAIL', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0202_DETAIL', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ALLOCATE_DETAILS
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on

	update B_KPI_ALLOCATE_DETAILS_ACCOUNTS
	   set B_KPI_ALLOCATE_DETAIL_ID       = @ID
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ALLOCATE_DETAIL_ID       = @MERGE_ID
	   and DELETED          = 0;



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ALLOCATE_DETAILS_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATES';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATES';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATES';
end -- if;
GO


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATE_DETAILS';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0202.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0202.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB0202.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB0202.DetailView'   , 'KPIB0202', 'vwB_KPI_ALLOCATES_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 1, 'KPIB0202.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 2, 'KPIB0202.LBL_YEAR', 'YEAR', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 3, 'KPIB0202.LBL_PERIOD', 'PERIOD', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 4, 'KPIB0202.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 5, 'KPIB0202.LBL_APPROVE_STATUS', 'APPROVE_STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 6, 'KPIB0202.LBL_APPROVED_BY', 'APPROVED_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 7, 'KPIB0202.LBL_APPROVED_DATE', 'APPROVED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 8, 'KPIB0202.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 9, 'KPIB0202.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 10, 'KPIB0202.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 11, 'KPIB0202.LBL_STATUS', 'STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 12, 'KPIB0202.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 13, 'KPIB0202.LBL_FILE_ID', 'FILE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 14, 'KPIB0202.LBL_ASSIGN_BY', 'ASSIGN_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 15, 'KPIB0202.LBL_ASSIGN_DATE', 'ASSIGN_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 16, 'KPIB0202.LBL_STAFT_NUMBER', 'STAFT_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 17, 'KPIB0202.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 18, 'KPIB0202.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 19, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 20, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 21, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202.DetailView', 22, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO

-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0202_DETAIL.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0202_DETAIL.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB0202_DETAIL.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB0202_DETAIL.DetailView'   , 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 1, 'KPIB0202_DETAIL.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 2, 'KPIB0202_DETAIL.LBL_UNIT', 'UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 3, 'KPIB0202_DETAIL.LBL_RADIO', 'RADIO', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 4, 'KPIB0202_DETAIL.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 5, 'KPIB0202_DETAIL.LBL_KPI_STANDARD_DETAIL_ID', 'KPI_STANDARD_DETAIL_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 6, 'KPIB0202_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 7, 'KPIB0202_DETAIL.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 8, 'KPIB0202_DETAIL.LBL_MONTH_1', 'MONTH_1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 9, 'KPIB0202_DETAIL.LBL_MONTH_2', 'MONTH_2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 10, 'KPIB0202_DETAIL.LBL_MONTH_3', 'MONTH_3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 11, 'KPIB0202_DETAIL.LBL_MONTH_4', 'MONTH_4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 12, 'KPIB0202_DETAIL.LBL_MONTH_5', 'MONTH_5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 13, 'KPIB0202_DETAIL.LBL_MONTH_6', 'MONTH_6', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 14, 'KPIB0202_DETAIL.LBL_MONTH_7', 'MONTH_7', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 15, 'KPIB0202_DETAIL.LBL_MONTH_8', 'MONTH_8', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 16, 'KPIB0202_DETAIL.LBL_MONTH_9', 'MONTH_9', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 17, 'KPIB0202_DETAIL.LBL_MONTH_10', 'MONTH_10', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 18, 'KPIB0202_DETAIL.LBL_MONTH_11', 'MONTH_11', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 19, 'KPIB0202_DETAIL.LBL_MONTH_12', 'MONTH_12', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 20, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 21, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 22, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0202_DETAIL.DetailView', 23, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO

-- delete from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0202.DetailView';

if not exists(select * from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0202.DetailView' and MODULE_NAME = 'Accounts' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_RELATIONSHIPS KPIB0202.DetailView';
	exec dbo.spDETAILVIEWS_RELATIONSHIPS_InsertOnly 'KPIB0202.DetailView', 'Accounts', '~/KPIB0202/Accounts', null, 'Accounts.LBL_MODULE_NAME';
end -- if;
GO

-- delete from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0202_DETAIL.DetailView';

if not exists(select * from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0202_DETAIL.DetailView' and MODULE_NAME = 'Accounts' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_RELATIONSHIPS KPIB0202_DETAIL.DetailView';
	exec dbo.spDETAILVIEWS_RELATIONSHIPS_InsertOnly 'KPIB0202_DETAIL.DetailView', 'Accounts', '~/KPIB0202_DETAIL/Accounts', null, 'Accounts.LBL_MODULE_NAME';
end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB0202.DetailView', 'KPIB0202';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB0202.EditView'  , 'KPIB0202';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB0202.PopupView' , 'KPIB0202';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB0202.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB0202.EditView', -1, null;
end -- if;
GO


if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202.Accounts' and DELETED = 0) begin -- then
	print 'DYNAMIC_BUTTONS KPIB0202.Accounts';
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0202.Accounts', 0, 'KPIB0202', 'edit', 'Accounts', 'edit', 'Accounts.Create'         , null, '.LBL_NEW_BUTTON_LABEL'   , '.LBL_NEW_BUTTON_TITLE'   , '.LBL_NEW_BUTTON_KEY'   , null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsPopup  'KPIB0202.Accounts', 1, 'KPIB0202', 'edit', 'Accounts', 'list', 'AccountPopup();', null, '.LBL_SELECT_BUTTON_LABEL', '.LBL_SELECT_BUTTON_TITLE', '.LBL_SELECT_BUTTON_KEY', null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0202.Accounts', 2, 'KPIB0202', 'view', 'Accounts', 'list', 'Accounts.Search'         , null, '.LBL_SEARCH_BUTTON_LABEL', '.LBL_SEARCH_BUTTON_TITLE', null, null, null;
end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB0202_DETAIL.DetailView', 'KPIB0202_DETAIL';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB0202_DETAIL.EditView'  , 'KPIB0202_DETAIL';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB0202_DETAIL.PopupView' , 'KPIB0202_DETAIL';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202_DETAIL.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB0202_DETAIL.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202_DETAIL.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB0202_DETAIL.EditView', -1, null;
end -- if;
GO


if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0202_DETAIL.Accounts' and DELETED = 0) begin -- then
	print 'DYNAMIC_BUTTONS KPIB0202_DETAIL.Accounts';
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0202_DETAIL.Accounts', 0, 'KPIB0202_DETAIL', 'edit', 'Accounts', 'edit', 'Accounts.Create'         , null, '.LBL_NEW_BUTTON_LABEL'   , '.LBL_NEW_BUTTON_TITLE'   , '.LBL_NEW_BUTTON_KEY'   , null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsPopup  'KPIB0202_DETAIL.Accounts', 1, 'KPIB0202_DETAIL', 'edit', 'Accounts', 'list', 'AccountPopup();', null, '.LBL_SELECT_BUTTON_LABEL', '.LBL_SELECT_BUTTON_TITLE', '.LBL_SELECT_BUTTON_KEY', null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0202_DETAIL.Accounts', 2, 'KPIB0202_DETAIL', 'view', 'Accounts', 'list', 'Accounts.Search'         , null, '.LBL_SEARCH_BUTTON_LABEL', '.LBL_SEARCH_BUTTON_TITLE', null, null, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202.EditView', 'KPIB0202'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 1, 'KPIB0202.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 2, 'KPIB0202.LBL_YEAR', 'YEAR', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 3, 'KPIB0202.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 4, 'KPIB0202.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 5, 'KPIB0202.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 6, 'KPIB0202.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.EditView', 7, 'KPIB0202.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 8, 'KPIB0202.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 9, 'KPIB0202.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 10, 'KPIB0202.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 11, 'KPIB0202.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 12, 'KPIB0202.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 1, 1, 'KPI_STANDARD_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 13, 'KPIB0202.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView', 14, 'KPIB0202.LBL_ASSIGN_BY', 'ASSIGN_BY', 1, 1, 'ASSIGN_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.EditView', 15, 'KPIB0202.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView', 16, 'KPIB0202.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.EditView', 17, 'KPIB0202.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.EditView', 18, 'KPIB0202.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.EditView', 19, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.EditView', 20, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202.EditView.Inline', 'KPIB0202'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 1, 'KPIB0202.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 2, 'KPIB0202.LBL_YEAR', 'YEAR', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 3, 'KPIB0202.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 4, 'KPIB0202.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 5, 'KPIB0202.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 6, 'KPIB0202.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.EditView.Inline', 7, 'KPIB0202.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 8, 'KPIB0202.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 9, 'KPIB0202.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 10, 'KPIB0202.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 11, 'KPIB0202.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 12, 'KPIB0202.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 1, 1, 'KPI_STANDARD_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 13, 'KPIB0202.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.EditView.Inline', 14, 'KPIB0202.LBL_ASSIGN_BY', 'ASSIGN_BY', 1, 1, 'ASSIGN_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.EditView.Inline', 15, 'KPIB0202.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.EditView.Inline', 16, 'KPIB0202.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.EditView.Inline', 17, 'KPIB0202.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.EditView.Inline', 18, 'KPIB0202.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.EditView.Inline', 19, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.EditView.Inline', 20, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202.PopupView.Inline', 'KPIB0202'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 1, 'KPIB0202.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 2, 'KPIB0202.LBL_YEAR', 'YEAR', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 3, 'KPIB0202.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 4, 'KPIB0202.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 5, 'KPIB0202.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 6, 'KPIB0202.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.PopupView.Inline', 7, 'KPIB0202.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 8, 'KPIB0202.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 9, 'KPIB0202.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 10, 'KPIB0202.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 11, 'KPIB0202.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 12, 'KPIB0202.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 1, 1, 'KPI_STANDARD_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 13, 'KPIB0202.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202.PopupView.Inline', 14, 'KPIB0202.LBL_ASSIGN_BY', 'ASSIGN_BY', 1, 1, 'ASSIGN_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0202.PopupView.Inline', 15, 'KPIB0202.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202.PopupView.Inline', 16, 'KPIB0202.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.PopupView.Inline', 17, 'KPIB0202.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202.PopupView.Inline', 18, 'KPIB0202.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.PopupView.Inline', 19, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202.PopupView.Inline', 20, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202.SearchBasic'    , 'KPIB0202', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchBasic', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0202.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202.SearchAdvanced' , 'KPIB0202', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 1, 'KPIB0202.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 2, 'KPIB0202.LBL_YEAR', 'YEAR', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 3, 'KPIB0202.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 4, 'KPIB0202.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 5, 'KPIB0202.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 6, 'KPIB0202.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0202.SearchAdvanced', 7, 'KPIB0202.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 8, 'KPIB0202.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 9, 'KPIB0202.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 10, 'KPIB0202.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 11, 'KPIB0202.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 12, 'KPIB0202.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 1, 1, 'KPI_STANDARD_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 13, 'KPIB0202.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202.SearchAdvanced', 14, 'KPIB0202.LBL_ASSIGN_BY', 'ASSIGN_BY', 1, 1, 'ASSIGN_NAME', 'return KPIB0202Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0202.SearchAdvanced', 15, 'KPIB0202.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchAdvanced', 16, 'KPIB0202.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0202.SearchAdvanced', 17, 'KPIB0202.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0202.SearchAdvanced', 18, 'KPIB0202.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB0202.SearchAdvanced' , 19, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202.SearchPopup'    , 'KPIB0202', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202.SearchPopup', 0, 'KPIB0202.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;

end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202_DETAIL.EditView', 'KPIB0202_DETAIL'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.EditView', 1, 'KPIB0202_DETAIL.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 2, 'KPIB0202_DETAIL.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 3, 'KPIB0202_DETAIL.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 4, 'KPIB0202_DETAIL.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.EditView', 5, 'KPIB0202_DETAIL.LBL_KPI_STANDARD_DETAIL_ID', 'KPI_STANDARD_DETAIL_ID', 0, 1, 'KPI_STANDARD_DETAIL_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.EditView', 6, 'KPIB0202_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.EditView', 7, 'KPIB0202_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 8, 'KPIB0202_DETAIL.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 9, 'KPIB0202_DETAIL.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 10, 'KPIB0202_DETAIL.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 11, 'KPIB0202_DETAIL.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 12, 'KPIB0202_DETAIL.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 13, 'KPIB0202_DETAIL.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 14, 'KPIB0202_DETAIL.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 15, 'KPIB0202_DETAIL.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 16, 'KPIB0202_DETAIL.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 17, 'KPIB0202_DETAIL.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 18, 'KPIB0202_DETAIL.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView', 19, 'KPIB0202_DETAIL.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.EditView', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.EditView', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202_DETAIL.EditView.Inline', 'KPIB0202_DETAIL'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.EditView.Inline', 1, 'KPIB0202_DETAIL.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 2, 'KPIB0202_DETAIL.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 3, 'KPIB0202_DETAIL.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 4, 'KPIB0202_DETAIL.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.EditView.Inline', 5, 'KPIB0202_DETAIL.LBL_KPI_STANDARD_DETAIL_ID', 'KPI_STANDARD_DETAIL_ID', 0, 1, 'KPI_STANDARD_DETAIL_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.EditView.Inline', 6, 'KPIB0202_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.EditView.Inline', 7, 'KPIB0202_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 8, 'KPIB0202_DETAIL.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 9, 'KPIB0202_DETAIL.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 10, 'KPIB0202_DETAIL.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 11, 'KPIB0202_DETAIL.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 12, 'KPIB0202_DETAIL.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 13, 'KPIB0202_DETAIL.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 14, 'KPIB0202_DETAIL.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 15, 'KPIB0202_DETAIL.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 16, 'KPIB0202_DETAIL.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 17, 'KPIB0202_DETAIL.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 18, 'KPIB0202_DETAIL.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.EditView.Inline', 19, 'KPIB0202_DETAIL.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.EditView.Inline', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.EditView.Inline', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0202_DETAIL.PopupView.Inline', 'KPIB0202_DETAIL'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.PopupView.Inline', 1, 'KPIB0202_DETAIL.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 2, 'KPIB0202_DETAIL.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 3, 'KPIB0202_DETAIL.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 4, 'KPIB0202_DETAIL.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0202_DETAIL.PopupView.Inline', 5, 'KPIB0202_DETAIL.LBL_KPI_STANDARD_DETAIL_ID', 'KPI_STANDARD_DETAIL_ID', 0, 1, 'KPI_STANDARD_DETAIL_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.PopupView.Inline', 6, 'KPIB0202_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0202_DETAIL.PopupView.Inline', 7, 'KPIB0202_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 8, 'KPIB0202_DETAIL.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 9, 'KPIB0202_DETAIL.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 10, 'KPIB0202_DETAIL.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 11, 'KPIB0202_DETAIL.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 12, 'KPIB0202_DETAIL.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 13, 'KPIB0202_DETAIL.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 14, 'KPIB0202_DETAIL.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 15, 'KPIB0202_DETAIL.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 16, 'KPIB0202_DETAIL.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 17, 'KPIB0202_DETAIL.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 18, 'KPIB0202_DETAIL.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0202_DETAIL.PopupView.Inline', 19, 'KPIB0202_DETAIL.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.PopupView.Inline', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0202_DETAIL.PopupView.Inline', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202_DETAIL.SearchBasic'    , 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchBasic', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0202_DETAIL.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202_DETAIL.SearchAdvanced' , 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202_DETAIL.SearchAdvanced', 1, 'KPIB0202_DETAIL.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 2, 'KPIB0202_DETAIL.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 3, 'KPIB0202_DETAIL.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 4, 'KPIB0202_DETAIL.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0202_DETAIL.SearchAdvanced', 5, 'KPIB0202_DETAIL.LBL_KPI_STANDARD_DETAIL_ID', 'KPI_STANDARD_DETAIL_ID', 0, 1, 'KPI_STANDARD_DETAIL_NAME', 'return KPIB0202_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0202_DETAIL.SearchAdvanced', 6, 'KPIB0202_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0202_DETAIL.SearchAdvanced', 7, 'KPIB0202_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 8, 'KPIB0202_DETAIL.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 9, 'KPIB0202_DETAIL.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 10, 'KPIB0202_DETAIL.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 11, 'KPIB0202_DETAIL.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 12, 'KPIB0202_DETAIL.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 13, 'KPIB0202_DETAIL.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 14, 'KPIB0202_DETAIL.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 15, 'KPIB0202_DETAIL.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 16, 'KPIB0202_DETAIL.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 17, 'KPIB0202_DETAIL.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 18, 'KPIB0202_DETAIL.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchAdvanced', 19, 'KPIB0202_DETAIL.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB0202_DETAIL.SearchAdvanced' , 20, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0202_DETAIL.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0202_DETAIL.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0202_DETAIL.SearchPopup'    , 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0202_DETAIL.SearchPopup', 0, 'KPIB0202_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.Accounts' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.Accounts' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202.Accounts';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202.Accounts', 'KPIB0202', 'vwB_KPI_ALLOCATES_ACCOUNTS';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202.Accounts', 0, 'Accounts.LBL_LIST_NAME', 'NAME', 'NAME', '50%', 'listViewTdLinkS1', 'ID', '~/Accounts/view.aspx?id={0}', null, 'Accounts', 'ASSIGNED_USER_ID';
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202.Export', 'KPIB0202', 'vwB_KPI_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202.Export'         ,  1, 'KPIB0202.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202.ListView', 'KPIB0202'      , 'vwB_KPI_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202.ListView', 2, 'KPIB0202.LBL_LIST_ALLOCATE_NAME', 'ALLOCATE_NAME', 'ALLOCATE_NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIB0202/view.aspx?id={0}', null, 'KPIB0202', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202.PopupView', 'KPIB0202'      , 'vwB_KPI_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202.PopupView', 1, 'KPIB0202.LBL_LIST_ALLOCATE_NAME', 'ALLOCATE_NAME', 'ALLOCATE_NAME', '45%', 'listViewTdLinkS1', 'ID ALLOCATE_NAME', 'SelectKPIB0202(''{0}'', ''{1}'');', null, 'KPIB0202', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202.SearchDuplicates', 'KPIB0202', 'vwB_KPI_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202.SearchDuplicates'          , 1, 'KPIB0202.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB0202/view.aspx?id={0}', null, 'KPIB0202', 'ASSIGNED_USER_ID';
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.Accounts' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.Accounts' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202_DETAIL.Accounts';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202_DETAIL.Accounts', 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_ACCOUNTS';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202_DETAIL.Accounts', 0, 'Accounts.LBL_LIST_NAME', 'NAME', 'NAME', '50%', 'listViewTdLinkS1', 'ID', '~/Accounts/view.aspx?id={0}', null, 'Accounts', 'ASSIGNED_USER_ID';
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202_DETAIL.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202_DETAIL.Export', 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202_DETAIL.Export'         ,  1, 'KPIB0202_DETAIL.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202_DETAIL.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202_DETAIL.ListView', 'KPIB0202_DETAIL'      , 'vwB_KPI_ALLOCATE_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202_DETAIL.ListView', 2, 'KPIB0202_DETAIL.LBL_LIST_KPI_NAME', 'KPI_NAME', 'KPI_NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIB0202_DETAIL/view.aspx?id={0}', null, 'KPIB0202_DETAIL', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202_DETAIL.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202_DETAIL.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202_DETAIL.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202_DETAIL.PopupView', 'KPIB0202_DETAIL'      , 'vwB_KPI_ALLOCATE_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202_DETAIL.PopupView', 1, 'KPIB0202_DETAIL.LBL_LIST_KPI_NAME', 'KPI_NAME', 'KPI_NAME', '45%', 'listViewTdLinkS1', 'ID KPI_NAME', 'SelectKPIB0202_DETAIL(''{0}'', ''{1}'');', null, 'KPIB0202_DETAIL', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202_DETAIL.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0202_DETAIL.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0202_DETAIL.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0202_DETAIL.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0202_DETAIL.SearchDuplicates', 'KPIB0202_DETAIL', 'vwB_KPI_ALLOCATE_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0202_DETAIL.SearchDuplicates'          , 1, 'KPIB0202_DETAIL.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB0202_DETAIL/view.aspx?id={0}', null, 'KPIB0202_DETAIL', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB0202', '.moduleList.KPIB0202', '~/KPIB0202/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ALLOCATES', 1, 0, 0, 0, 0, 1;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB0202_DETAIL', '.moduleList.KPIB0202_DETAIL', '~/KPIB0202_DETAIL/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ALLOCATE_DETAILS', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB0202';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB0202' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202', 'KPIB0202.LNK_NEW_B_KPI_ALLOCATE' , '~/KPIB0202/edit.aspx'   , 'CreateKPIB0202.gif', 1,  1, 'KPIB0202', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202', 'KPIB0202.LNK_B_KPI_ALLOCATE_LIST', '~/KPIB0202/default.aspx', 'KPIB0202.gif'      , 1,  2, 'KPIB0202', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202', '.LBL_IMPORT'              , '~/KPIB0202/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB0202', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202', '.LNK_ACTIVITY_STREAM'     , '~/KPIB0202/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB0202', 'list';
end -- if;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB0202_DETAIL';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB0202_DETAIL' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202_DETAIL', 'KPIB0202_DETAIL.LNK_NEW_B_KPI_ALLOCATE_DETAIL' , '~/KPIB0202_DETAIL/edit.aspx'   , 'CreateKPIB0202_DETAIL.gif', 1,  1, 'KPIB0202_DETAIL', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202_DETAIL', 'KPIB0202_DETAIL.LNK_B_KPI_ALLOCATE_DETAIL_LIST', '~/KPIB0202_DETAIL/default.aspx', 'KPIB0202_DETAIL.gif'      , 1,  2, 'KPIB0202_DETAIL', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202_DETAIL', '.LBL_IMPORT'              , '~/KPIB0202_DETAIL/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB0202_DETAIL', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0202_DETAIL', '.LNK_ACTIVITY_STREAM'     , '~/KPIB0202_DETAIL/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB0202_DETAIL', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB0202', null, null, N'KPIB0202 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB0202', null, null, N'Create KPIB0202';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ALLOCATE_LIST'                          , N'en-US', N'KPIB0202', null, null, N'KPIB0202';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ALLOCATE'                           , N'en-US', N'KPIB0202', null, null, N'Create KPIB0202';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB0202', null, null, N'KPIB0202 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ALLOCATE_NOT_FOUND'                     , N'en-US', N'KPIB0202', null, null, N'KPIB0202 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ALLOCATE_CONFIRMATION'           , N'en-US', N'KPIB0202', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB0202', null, null, N'KPIB0202';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB0202', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB0202'                                          , N'en-US', null, N'moduleList', 100, N'KPIB0202';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_NAME'                                     , 'en-US', 'KPIB0202', null, null, 'allocate name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_NAME'                                , 'en-US', 'KPIB0202', null, null, 'allocate name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_CODE'                                     , 'en-US', 'KPIB0202', null, null, 'allocate code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_CODE'                                , 'en-US', 'KPIB0202', null, null, 'allocate code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_YEAR'                                              , 'en-US', 'KPIB0202', null, null, 'year:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_YEAR'                                         , 'en-US', 'KPIB0202', null, null, 'year';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERIOD'                                            , 'en-US', 'KPIB0202', null, null, 'period:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERIOD'                                       , 'en-US', 'KPIB0202', null, null, 'period';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'KPIB0202', null, null, 'version number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'KPIB0202', null, null, 'version number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVE_STATUS'                                    , 'en-US', 'KPIB0202', null, null, 'approve status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVE_STATUS'                               , 'en-US', 'KPIB0202', null, null, 'approve status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_BY'                                       , 'en-US', 'KPIB0202', null, null, 'approved by:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_BY'                                  , 'en-US', 'KPIB0202', null, null, 'approved by';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_DATE'                                     , 'en-US', 'KPIB0202', null, null, 'approved date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_DATE'                                , 'en-US', 'KPIB0202', null, null, 'approved date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_TYPE'                                     , 'en-US', 'KPIB0202', null, null, 'allocate type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_TYPE'                                , 'en-US', 'KPIB0202', null, null, 'allocate type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_ID'                                   , 'en-US', 'KPIB0202', null, null, 'organization id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_ID'                              , 'en-US', 'KPIB0202', null, null, 'organization id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_EMPLOYEE_ID'                                       , 'en-US', 'KPIB0202', null, null, 'employee id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_EMPLOYEE_ID'                                  , 'en-US', 'KPIB0202', null, null, 'employee id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STATUS'                                            , 'en-US', 'KPIB0202', null, null, 'status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STATUS'                                       , 'en-US', 'KPIB0202', null, null, 'status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_STANDARD_ID'                                   , 'en-US', 'KPIB0202', null, null, 'kpi standard_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_STANDARD_ID'                              , 'en-US', 'KPIB0202', null, null, 'kpi standard_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FILE_ID'                                           , 'en-US', 'KPIB0202', null, null, 'file id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FILE_ID'                                      , 'en-US', 'KPIB0202', null, null, 'file id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_BY'                                         , 'en-US', 'KPIB0202', null, null, 'assign by:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_BY'                                    , 'en-US', 'KPIB0202', null, null, 'assign by';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_DATE'                                       , 'en-US', 'KPIB0202', null, null, 'assign date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_DATE'                                  , 'en-US', 'KPIB0202', null, null, 'assign date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STAFT_NUMBER'                                      , 'en-US', 'KPIB0202', null, null, 'staft number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STAFT_NUMBER'                                 , 'en-US', 'KPIB0202', null, null, 'staft number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIB0202', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIB0202', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIB0202', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIB0202', null, null, 'remark';




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPIB0202_DETAIL List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB0202_DETAIL', null, null, N'Create KPIB0202_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ALLOCATE_DETAIL_LIST'                          , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPIB0202_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ALLOCATE_DETAIL'                           , N'en-US', N'KPIB0202_DETAIL', null, null, N'Create KPIB0202_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPIB0202_DETAIL Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ALLOCATE_DETAIL_NOT_FOUND'                     , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPIB0202_DETAIL not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ALLOCATE_DETAIL_CONFIRMATION'           , N'en-US', N'KPIB0202_DETAIL', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPIB0202_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB0202_DETAIL', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB0202_DETAIL'                                          , N'en-US', null, N'moduleList', 100, N'KPIB0202_DETAIL';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_NAME'                                          , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_NAME'                                     , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_ALLOCATE_ID'                                   , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi allocate_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_ALLOCATE_ID'                              , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi allocate_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_UNIT'                                              , 'en-US', 'KPIB0202_DETAIL', null, null, 'unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_UNIT'                                         , 'en-US', 'KPIB0202_DETAIL', null, null, 'unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RADIO'                                             , 'en-US', 'KPIB0202_DETAIL', null, null, 'radio:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RADIO'                                        , 'en-US', 'KPIB0202_DETAIL', null, null, 'radio';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MAX_RATIO_COMPLETE'                                , 'en-US', 'KPIB0202_DETAIL', null, null, 'max ratio_complete:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MAX_RATIO_COMPLETE'                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'max ratio_complete';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_STANDARD_DETAIL_ID'                            , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi standard_detail_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_STANDARD_DETAIL_ID'                       , 'en-US', 'KPIB0202_DETAIL', null, null, 'kpi standard_detail_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIB0202_DETAIL', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIB0202_DETAIL', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIB0202_DETAIL', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIB0202_DETAIL', null, null, 'remark';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_1'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_1'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_2'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_2'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_3'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_3'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_4'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_4'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_5'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_5'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 5';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_6'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 6:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_6'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 6';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_7'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 7:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_7'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 7';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_8'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 8:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_8'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 8';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_9'                                           , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 9:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_9'                                      , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 9';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_10'                                          , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 10:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_10'                                     , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 10';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_11'                                          , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 11:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_11'                                     , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 11';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_12'                                          , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 12:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_12'                                     , 'en-US', 'KPIB0202_DETAIL', null, null, 'month 12';


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATE_DETAILS_CSRM';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATE_DETAILS_CSRM';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATE_DETAILS_CSRM';
end -- if;
GO

-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATES_CSRM';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATES_CSRM';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATES_CSRM';
end -- if;
GO







