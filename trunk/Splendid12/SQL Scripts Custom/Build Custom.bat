del BaseTables.sql
del Tables.sql
del Views.sql
del Procedures.sql
del Triggers.sql
del Data.sql
del Terminology.sql

copy BaseTables\*.0.sql         + BaseTables\*.1.sql         + BaseTables\*.2.sql       BaseTables.sql
copy Tables\*.0.sql             + Tables\*.1.sql             + Tables\*.2.sql           Tables.sql
copy Views\*.0.sql              + Views\*.1.sql              + Views\*.2.sql            Views.sql
copy Procedures\*.0.sql         + Procedures\*.1.sql         + Procedures\*.2.sql       Procedures.sql
copy Triggers\*.0.sql           + Triggers\*.1.sql           + Triggers\*.2.sql         Triggers.sql
copy Data\*.0.sql               + Data\*.1.sql               + Data\*.2.sql             Data.sql
copy Terminology\*.0.sql        + Terminology\*.1.sql        + Terminology\*.2.sql      Terminology.sql

Copy BaseTables.sql + Tables.sql + Views.sql + Procedures.sql + Triggers.sql + Data.sql + Terminology.sql "Build Custom.sql"

