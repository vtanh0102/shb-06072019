
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS';
	Create Table dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ACTUAL_ALLOCATE_DETAILS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, ALLOCATE_ID                        uniqueidentifier not null
		, ALLOCATE_DETAIL_ID                 uniqueidentifier not null
		, SYNS_ACTUAL_MONTH_1                float null
		, FINAL_ACTUAL_MONTH_1               float null
		, SYNS_ACTUAL_MONTH_2                float null
		, FINAL_ACTUAL_MONTH_2               float null
		, SYNS_ACTUAL_MONTH_3                float null
		, FINAL_ACTUAL_MONTH_3               float null
		, SYNS_ACTUAL_MONTH_4                float null
		, FINAL_ACTUAL_MONTH_4               float null
		, SYNS_ACTUAL_MONTH_5                float null
		, FINAL_ACTUAL_MONTH_5               float null
		, SYNS_ACTUAL_MONTH_6                float null
		, FINAL_ACTUAL_MONTH_6               float null
		, SYNS_ACTUAL_MONTH_7                float null
		, FINAL_ACTUAL_MONTH_7               float null
		, SYNS_ACTUAL_MONTH_8                float null
		, FINAL_ACTUAL_MONTH_8               float null
		, SYNS_ACTUAL_MONTH_9                float null
		, FINAL_ACTUAL_MONTH_9               float null
		, SYNS_ACTUAL_MONTH_10               float null
		, FINAL_ACTUAL_MONTH_10              float null
		, SYNS_ACTUAL_MONTH_11               float null
		, FINAL_ACTUAL_MONTH_11              float null
		, SYNS_ACTUAL_MONTH_12               float null
		, FINAL_ACTUAL_MONTH_12              float null

		)

	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM';
	Create Table dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM primary key
		)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES';
	Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ACT_PERCENT_ALLOCATES primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, ALLOCATED_ID                       uniqueidentifier not null
		, PERCENT_ACTUAL_TOTAL               float null
		, FINAL_PERCENT_ACTUAL_TOTAL         float null
		, PERCENT_ACTUAL_MONTH_1             float null
		, FINAL_PERCENT_ACTUAL_MONTH_1       float null
		, PERCENT_ACTUAL_MONTH_2             float null
		, FINAL_PERCENT_ACTUAL_MONTH_2       float null
		, PERCENT_ACTUAL_MONTH_3             float null
		, FINAL_PERCENT_ACTUAL_MONTH_3       float null
		, PERCENT_ACTUAL_MONTH_4             float null
		, FINAL_PERCENT_ACTUAL_MONTH_4       float null
		, PERCENT_ACTUAL_MONTH_5             float null
		, FINAL_PERCENT_ACTUAL_MONTH_5       float null
		, PERCENT_ACTUAL_MONTH_6             float null
		, FINAL_PERCENT_ACTUAL_MONTH_6       float null
		, PERCENT_ACTUAL_MONTH_7             float null
		, FINAL_PERCENT_ACTUAL_MONTH_7       float null
		, PERCENT_ACTUAL_MONTH_8             float null
		, FINAL_PERCENT_ACTUAL_MONTH_8       float null
		, PERCENT_ACTUAL_MONTH_9             float null
		, FINAL_PERCENT_ACTUAL_MONTH_9       float null
		, PERCENT_ACTUAL_MONTH_10            float null
		, FINAL_PERCENT_ACTUAL_MONTH_10      float null
		, PERCENT_ACTUAL_MONTH_11            float null
		, FINAL_PERCENT_ACTUAL_MONTH_11      float null
		, PERCENT_ACTUAL_MONTH_12            float null
		, FINAL_PERCENT_ACTUAL_MONTH_12      float null

		)

	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ACT_PERCENT_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_TEAM_ID          on dbo.B_KPI_ACT_PERCENT_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ACT_PERCENT_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS';
	Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, B_KPI_ACT_PERCENT_ALLOCATE_ID             uniqueidentifier not null
		, ACCOUNT_ID          uniqueidentifier not null
		)

	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_B_KPI_ACT_PERCENT_ALLOCATE_ID    on dbo.B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS (B_KPI_ACT_PERCENT_ALLOCATE_ID, ACCOUNT_ID, DELETED)
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_ACCOUNT_ID on dbo.B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS (ACCOUNT_ID, B_KPI_ACT_PERCENT_ALLOCATE_ID, DELETED)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES_CSTM';
	Create Table dbo.B_KPI_ACT_PERCENT_ALLOCATES_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ACT_PERCENT_ALLOCATES_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ACTUAL_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ACTUAL_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'ALLOCATE_ID') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ALLOCATE_ID uniqueidentifier null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ALLOCATE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'ALLOCATE_DETAIL_ID') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ALLOCATE_DETAIL_ID uniqueidentifier null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add ALLOCATE_DETAIL_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_1') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_1 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_1') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_1 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_2') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_2 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_2') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_2 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_3') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_3 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_3 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_3') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_3 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_3 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_4') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_4 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_4 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_4') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_4 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_4 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_5') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_5 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_5 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_5') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_5 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_5 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_6') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_6 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_6 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_6') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_6 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_6 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_7') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_7 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_7 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_7') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_7 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_7 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_8') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_8 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_8 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_8') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_8 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_8 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_9') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_9 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_9 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_9') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_9 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_9 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_10') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_10 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_10 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_10') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_10 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_10 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_11') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_11 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_11 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_11') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_11 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_11 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'SYNS_ACTUAL_MONTH_12') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_12 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add SYNS_ACTUAL_MONTH_12 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACTUAL_ALLOCATE_DETAILS' and COLUMN_NAME = 'FINAL_ACTUAL_MONTH_12') begin -- then
	print 'alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_12 float null';
	alter table B_KPI_ACTUAL_ALLOCATE_DETAILS add FINAL_ACTUAL_MONTH_12 float null;
end -- if;


GO


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ACT_PERCENT_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_TEAM_ID          on dbo.B_KPI_ACT_PERCENT_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ACT_PERCENT_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ACT_PERCENT_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'ALLOCATED_ID') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add ALLOCATED_ID uniqueidentifier null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add ALLOCATED_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_TOTAL') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_TOTAL float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_TOTAL float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_TOTAL') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_TOTAL float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_TOTAL float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_1') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_1 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_1') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_1 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_2') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_2 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_2') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_2 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_3') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_3 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_3 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_3') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_3 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_3 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_4') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_4 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_4 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_4') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_4 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_4 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_5') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_5 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_5 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_5') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_5 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_5 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_6') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_6 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_6 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_6') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_6 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_6 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_7') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_7 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_7 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_7') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_7 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_7 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_8') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_8 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_8 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_8') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_8 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_8 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_9') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_9 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_9 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_9') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_9 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_9 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_10') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_10 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_10 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_10') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_10 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_10 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_11') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_11 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_11 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_11') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_11 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_11 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'PERCENT_ACTUAL_MONTH_12') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_12 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add PERCENT_ACTUAL_MONTH_12 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ACT_PERCENT_ALLOCATES' and COLUMN_NAME = 'FINAL_PERCENT_ACTUAL_MONTH_12') begin -- then
	print 'alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_12 float null';
	alter table B_KPI_ACT_PERCENT_ALLOCATES add FINAL_PERCENT_ACTUAL_MONTH_12 float null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACTUAL_ALLOCATE_DETAILS')
	Drop View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS;
GO


Create View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS
as
select B_KPI_ACTUAL_ALLOCATE_DETAILS.ID
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.ALLOCATE_ID
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.ALLOCATE_DETAIL_ID
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_1
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_1
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_2
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_2
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_3
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_3
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_4
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_4
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_5
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_5
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_6
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_6
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_7
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_7
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_8
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_8
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_9
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_9
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_10
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_10
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_11
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_11
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.SYNS_ACTUAL_MONTH_12
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.FINAL_ACTUAL_MONTH_12
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ACTUAL_ALLOCATE_DETAILS.DATE_ENTERED
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.DATE_MODIFIED
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ACTUAL_ALLOCATE_DETAILS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM.*
  from            B_KPI_ACTUAL_ALLOCATE_DETAILS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ACTUAL_ALLOCATE_DETAILS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ACTUAL_ALLOCATE_DETAILS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ACTUAL_ALLOCATE_DETAILS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ACTUAL_ALLOCATE_DETAILS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ACTUAL_ALLOCATE_DETAILS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ACTUAL_ALLOCATE_DETAILS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ACTUAL_ALLOCATE_DETAILS.MODIFIED_USER_ID
  left outer join B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM
               on B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM.ID_C     = B_KPI_ACTUAL_ALLOCATE_DETAILS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ACTUAL_ALLOCATE_DETAILS.ID
 where B_KPI_ACTUAL_ALLOCATE_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACT_PERCENT_ALLOCATES')
	Drop View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES;
GO


Create View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES
as
select B_KPI_ACT_PERCENT_ALLOCATES.ID
     , B_KPI_ALLOCATES.ID AS ALLOCATED_ID
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_TOTAL
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_TOTAL
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_1
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_1
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_2
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_2
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_3
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_3
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_4
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_4
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_5
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_5
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_6
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_6
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_7
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_7
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_8
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_8
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_9
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_9
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_10
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_10
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_11
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_11
     , B_KPI_ACT_PERCENT_ALLOCATES.PERCENT_ACTUAL_MONTH_12
     , B_KPI_ACT_PERCENT_ALLOCATES.FINAL_PERCENT_ACTUAL_MONTH_12
     , B_KPI_ACT_PERCENT_ALLOCATES.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ACT_PERCENT_ALLOCATES.DATE_ENTERED
     , B_KPI_ACT_PERCENT_ALLOCATES.DATE_MODIFIED
     , B_KPI_ACT_PERCENT_ALLOCATES.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ACT_PERCENT_ALLOCATES.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ACT_PERCENT_ALLOCATES.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ACT_PERCENT_ALLOCATES_CSTM.*
	 , B_KPI_ALLOCATES.ALLOCATE_NAME
       , B_KPI_ALLOCATES.ALLOCATE_CODE
       , B_KPI_ALLOCATES.YEAR
       , B_KPI_ALLOCATES.PERIOD
       , B_KPI_ALLOCATES.VERSION_NUMBER
       , B_KPI_ALLOCATES.ALLOCATE_TYPE
       , B_KPI_ALLOCATES.ORGANIZATION_ID
       , B_KPI_ALLOCATES.EMPLOYEE_ID
       , B_KPI_ALLOCATES.KPI_STANDARD_ID
       , B_KPI_ALLOCATES.FILE_ID
       , B_KPI_ALLOCATES.ASSIGN_BY
       , B_KPI_ALLOCATES.ASSIGN_DATE
       , B_KPI_ALLOCATES.STAFT_NUMBER
       , B_KPI_ALLOCATES.DESCRIPTION

  from            B_KPI_ALLOCATES
  left outer join 
            	B_KPI_ACT_PERCENT_ALLOCATES ON dbo.B_KPI_ALLOCATES.ID = dbo.B_KPI_ACT_PERCENT_ALLOCATES.ALLOCATED_ID
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ACT_PERCENT_ALLOCATES.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ACT_PERCENT_ALLOCATES.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ACT_PERCENT_ALLOCATES.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ACT_PERCENT_ALLOCATES.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ACT_PERCENT_ALLOCATES.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ACT_PERCENT_ALLOCATES.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ACT_PERCENT_ALLOCATES.MODIFIED_USER_ID
  left outer join B_KPI_ACT_PERCENT_ALLOCATES_CSTM
               on B_KPI_ACT_PERCENT_ALLOCATES_CSTM.ID_C     = B_KPI_ACT_PERCENT_ALLOCATES.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ACT_PERCENT_ALLOCATES.ID
 where B_KPI_ACT_PERCENT_ALLOCATES.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ACT_PERCENT_ALLOCATES to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS')
	Drop View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS;
GO


Create View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
as
select B_KPI_ACT_PERCENT_ALLOCATES.ID               as B_KPI_ACT_PERCENT_ALLOCATE_ID
     , B_KPI_ACT_PERCENT_ALLOCATES.ALLOCATED_ID             as B_KPI_ACT_PERCENT_ALLOCATED_ID
     , B_KPI_ACT_PERCENT_ALLOCATES.ASSIGNED_USER_ID as B_KPI_ACT_PERCENT_ALLOCATE_ASSIGNED_USER_ID
     , vwACCOUNTS.ID                 as ACCOUNT_ID
     , vwACCOUNTS.NAME               as ACCOUNT_NAME
     , vwACCOUNTS.*
  from           B_KPI_ACT_PERCENT_ALLOCATES
      inner join B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
              on B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS.B_KPI_ACT_PERCENT_ALLOCATE_ID = B_KPI_ACT_PERCENT_ALLOCATES.ID
             and B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS.DELETED    = 0
      inner join vwACCOUNTS
              on vwACCOUNTS.ID                = B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS.ACCOUNT_ID
 where B_KPI_ACT_PERCENT_ALLOCATES.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACTUAL_ALLOCATE_DETAILS_Edit')
	Drop View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_Edit;
GO


Create View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_Edit
as
select *
  from vwB_KPI_ACTUAL_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACTUAL_ALLOCATE_DETAILS_List')
	Drop View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_List;
GO


Create View dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_List
as
select *
  from vwB_KPI_ACTUAL_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ACTUAL_ALLOCATE_DETAILS_List to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACT_PERCENT_ALLOCATES_Edit')
	Drop View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_Edit;
GO


Create View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_Edit
as
select *
  from vwB_KPI_ACT_PERCENT_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ACT_PERCENT_ALLOCATES_List')
	Drop View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_List;
GO


Create View dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_List
as
select *
  from vwB_KPI_ACT_PERCENT_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ACT_PERCENT_ALLOCATES_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete;
GO


Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ACTUAL_ALLOCATE_DETAILS
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Undelete;
GO


Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB0301';
	
	-- BEGIN Oracle Exception
		update B_KPI_ACTUAL_ALLOCATE_DETAILS
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ACTUAL_ALLOCATE_DETAILS_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Update;
GO


Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @ALLOCATE_ID                        uniqueidentifier
	, @ALLOCATE_DETAIL_ID                 uniqueidentifier
	, @SYNS_ACTUAL_MONTH_1                float
	, @FINAL_ACTUAL_MONTH_1               float
	, @SYNS_ACTUAL_MONTH_2                float
	, @FINAL_ACTUAL_MONTH_2               float
	, @SYNS_ACTUAL_MONTH_3                float
	, @FINAL_ACTUAL_MONTH_3               float
	, @SYNS_ACTUAL_MONTH_4                float
	, @FINAL_ACTUAL_MONTH_4               float
	, @SYNS_ACTUAL_MONTH_5                float
	, @FINAL_ACTUAL_MONTH_5               float
	, @SYNS_ACTUAL_MONTH_6                float
	, @FINAL_ACTUAL_MONTH_6               float
	, @SYNS_ACTUAL_MONTH_7                float
	, @FINAL_ACTUAL_MONTH_7               float
	, @SYNS_ACTUAL_MONTH_8                float
	, @FINAL_ACTUAL_MONTH_8               float
	, @SYNS_ACTUAL_MONTH_9                float
	, @FINAL_ACTUAL_MONTH_9               float
	, @SYNS_ACTUAL_MONTH_10               float
	, @FINAL_ACTUAL_MONTH_10              float
	, @SYNS_ACTUAL_MONTH_11               float
	, @FINAL_ACTUAL_MONTH_11              float
	, @SYNS_ACTUAL_MONTH_12               float
	, @FINAL_ACTUAL_MONTH_12              float

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ACTUAL_ALLOCATE_DETAILS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ACTUAL_ALLOCATE_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, ALLOCATE_ID                        
			, ALLOCATE_DETAIL_ID                 
			, SYNS_ACTUAL_MONTH_1                
			, FINAL_ACTUAL_MONTH_1               
			, SYNS_ACTUAL_MONTH_2                
			, FINAL_ACTUAL_MONTH_2               
			, SYNS_ACTUAL_MONTH_3                
			, FINAL_ACTUAL_MONTH_3               
			, SYNS_ACTUAL_MONTH_4                
			, FINAL_ACTUAL_MONTH_4               
			, SYNS_ACTUAL_MONTH_5                
			, FINAL_ACTUAL_MONTH_5               
			, SYNS_ACTUAL_MONTH_6                
			, FINAL_ACTUAL_MONTH_6               
			, SYNS_ACTUAL_MONTH_7                
			, FINAL_ACTUAL_MONTH_7               
			, SYNS_ACTUAL_MONTH_8                
			, FINAL_ACTUAL_MONTH_8               
			, SYNS_ACTUAL_MONTH_9                
			, FINAL_ACTUAL_MONTH_9               
			, SYNS_ACTUAL_MONTH_10               
			, FINAL_ACTUAL_MONTH_10              
			, SYNS_ACTUAL_MONTH_11               
			, FINAL_ACTUAL_MONTH_11              
			, SYNS_ACTUAL_MONTH_12               
			, FINAL_ACTUAL_MONTH_12              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @ALLOCATE_ID                        
			, @ALLOCATE_DETAIL_ID                 
			, @SYNS_ACTUAL_MONTH_1                
			, @FINAL_ACTUAL_MONTH_1               
			, @SYNS_ACTUAL_MONTH_2                
			, @FINAL_ACTUAL_MONTH_2               
			, @SYNS_ACTUAL_MONTH_3                
			, @FINAL_ACTUAL_MONTH_3               
			, @SYNS_ACTUAL_MONTH_4                
			, @FINAL_ACTUAL_MONTH_4               
			, @SYNS_ACTUAL_MONTH_5                
			, @FINAL_ACTUAL_MONTH_5               
			, @SYNS_ACTUAL_MONTH_6                
			, @FINAL_ACTUAL_MONTH_6               
			, @SYNS_ACTUAL_MONTH_7                
			, @FINAL_ACTUAL_MONTH_7               
			, @SYNS_ACTUAL_MONTH_8                
			, @FINAL_ACTUAL_MONTH_8               
			, @SYNS_ACTUAL_MONTH_9                
			, @FINAL_ACTUAL_MONTH_9               
			, @SYNS_ACTUAL_MONTH_10               
			, @FINAL_ACTUAL_MONTH_10              
			, @SYNS_ACTUAL_MONTH_11               
			, @FINAL_ACTUAL_MONTH_11              
			, @SYNS_ACTUAL_MONTH_12               
			, @FINAL_ACTUAL_MONTH_12              

			);
	end else begin
		update B_KPI_ACTUAL_ALLOCATE_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , ALLOCATE_ID                          = @ALLOCATE_ID                        
		     , ALLOCATE_DETAIL_ID                   = @ALLOCATE_DETAIL_ID                 
		     , SYNS_ACTUAL_MONTH_1                  = @SYNS_ACTUAL_MONTH_1                
		     , FINAL_ACTUAL_MONTH_1                 = @FINAL_ACTUAL_MONTH_1               
		     , SYNS_ACTUAL_MONTH_2                  = @SYNS_ACTUAL_MONTH_2                
		     , FINAL_ACTUAL_MONTH_2                 = @FINAL_ACTUAL_MONTH_2               
		     , SYNS_ACTUAL_MONTH_3                  = @SYNS_ACTUAL_MONTH_3                
		     , FINAL_ACTUAL_MONTH_3                 = @FINAL_ACTUAL_MONTH_3               
		     , SYNS_ACTUAL_MONTH_4                  = @SYNS_ACTUAL_MONTH_4                
		     , FINAL_ACTUAL_MONTH_4                 = @FINAL_ACTUAL_MONTH_4               
		     , SYNS_ACTUAL_MONTH_5                  = @SYNS_ACTUAL_MONTH_5                
		     , FINAL_ACTUAL_MONTH_5                 = @FINAL_ACTUAL_MONTH_5               
		     , SYNS_ACTUAL_MONTH_6                  = @SYNS_ACTUAL_MONTH_6                
		     , FINAL_ACTUAL_MONTH_6                 = @FINAL_ACTUAL_MONTH_6               
		     , SYNS_ACTUAL_MONTH_7                  = @SYNS_ACTUAL_MONTH_7                
		     , FINAL_ACTUAL_MONTH_7                 = @FINAL_ACTUAL_MONTH_7               
		     , SYNS_ACTUAL_MONTH_8                  = @SYNS_ACTUAL_MONTH_8                
		     , FINAL_ACTUAL_MONTH_8                 = @FINAL_ACTUAL_MONTH_8               
		     , SYNS_ACTUAL_MONTH_9                  = @SYNS_ACTUAL_MONTH_9                
		     , FINAL_ACTUAL_MONTH_9                 = @FINAL_ACTUAL_MONTH_9               
		     , SYNS_ACTUAL_MONTH_10                 = @SYNS_ACTUAL_MONTH_10               
		     , FINAL_ACTUAL_MONTH_10                = @FINAL_ACTUAL_MONTH_10              
		     , SYNS_ACTUAL_MONTH_11                 = @SYNS_ACTUAL_MONTH_11               
		     , FINAL_ACTUAL_MONTH_11                = @FINAL_ACTUAL_MONTH_11              
		     , SYNS_ACTUAL_MONTH_12                 = @SYNS_ACTUAL_MONTH_12               
		     , FINAL_ACTUAL_MONTH_12                = @FINAL_ACTUAL_MONTH_12              

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ACTUAL_ALLOCATE_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Delete;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Delete
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ACT_PERCENT_ALLOCATE_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ACT_PERCENT_ALLOCATE_ID    = @B_KPI_ACT_PERCENT_ALLOCATE_ID
	   and ACCOUNT_ID = @ACCOUNT_ID
	   and DELETED          = 0;
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Update;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Update
	( @MODIFIED_USER_ID          uniqueidentifier
	, @B_KPI_ACT_PERCENT_ALLOCATE_ID    uniqueidentifier
	, @ACCOUNT_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID uniqueidentifier;
	-- BEGIN Oracle Exception
		select @ID = ID
		  from B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
		 where B_KPI_ACT_PERCENT_ALLOCATE_ID = @B_KPI_ACT_PERCENT_ALLOCATE_ID
		   and ACCOUNT_ID = @ACCOUNT_ID
		   and DELETED           = 0;
	-- END Oracle Exception
	
	if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
		set @ID = newid();
		insert into B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
			( ID               
			, CREATED_BY       
			, DATE_ENTERED     
			, MODIFIED_USER_ID 
			, DATE_MODIFIED    
			, DATE_MODIFIED_UTC
			, B_KPI_ACT_PERCENT_ALLOCATE_ID
			, ACCOUNT_ID
			)
		values
			( @ID               
			, @MODIFIED_USER_ID 
			,  getdate()        
			, @MODIFIED_USER_ID 
			,  getdate()        
			,  getutcdate()     
			, @B_KPI_ACT_PERCENT_ALLOCATE_ID
			, @ACCOUNT_ID
			);
	end -- if;
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Delete;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	update B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ACT_PERCENT_ALLOCATE_ID       = @ID
	   and DELETED          = 0;


	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ACT_PERCENT_ALLOCATES
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Undelete;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	
	update B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
	   set DELETED          = 1
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	  where B_KPI_ACT_PERCENT_ALLOCATE_ID       = @ID
	   and DELETED          = 0;


	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB0301';
	
	-- BEGIN Oracle Exception
		update B_KPI_ACT_PERCENT_ALLOCATES
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ACT_PERCENT_ALLOCATES_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Update;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @ALLOCATED_ID                       uniqueidentifier
	, @PERCENT_ACTUAL_TOTAL               float
	, @FINAL_PERCENT_ACTUAL_TOTAL         float
	, @PERCENT_ACTUAL_MONTH_1             float
	, @FINAL_PERCENT_ACTUAL_MONTH_1       float
	, @PERCENT_ACTUAL_MONTH_2             float
	, @FINAL_PERCENT_ACTUAL_MONTH_2       float
	, @PERCENT_ACTUAL_MONTH_3             float
	, @FINAL_PERCENT_ACTUAL_MONTH_3       float
	, @PERCENT_ACTUAL_MONTH_4             float
	, @FINAL_PERCENT_ACTUAL_MONTH_4       float
	, @PERCENT_ACTUAL_MONTH_5             float
	, @FINAL_PERCENT_ACTUAL_MONTH_5       float
	, @PERCENT_ACTUAL_MONTH_6             float
	, @FINAL_PERCENT_ACTUAL_MONTH_6       float
	, @PERCENT_ACTUAL_MONTH_7             float
	, @FINAL_PERCENT_ACTUAL_MONTH_7       float
	, @PERCENT_ACTUAL_MONTH_8             float
	, @FINAL_PERCENT_ACTUAL_MONTH_8       float
	, @PERCENT_ACTUAL_MONTH_9             float
	, @FINAL_PERCENT_ACTUAL_MONTH_9       float
	, @PERCENT_ACTUAL_MONTH_10            float
	, @FINAL_PERCENT_ACTUAL_MONTH_10      float
	, @PERCENT_ACTUAL_MONTH_11            float
	, @FINAL_PERCENT_ACTUAL_MONTH_11      float
	, @PERCENT_ACTUAL_MONTH_12            float
	, @FINAL_PERCENT_ACTUAL_MONTH_12      float

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ACT_PERCENT_ALLOCATES where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ACT_PERCENT_ALLOCATES
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, ALLOCATED_ID                       
			, PERCENT_ACTUAL_TOTAL               
			, FINAL_PERCENT_ACTUAL_TOTAL         
			, PERCENT_ACTUAL_MONTH_1             
			, FINAL_PERCENT_ACTUAL_MONTH_1       
			, PERCENT_ACTUAL_MONTH_2             
			, FINAL_PERCENT_ACTUAL_MONTH_2       
			, PERCENT_ACTUAL_MONTH_3             
			, FINAL_PERCENT_ACTUAL_MONTH_3       
			, PERCENT_ACTUAL_MONTH_4             
			, FINAL_PERCENT_ACTUAL_MONTH_4       
			, PERCENT_ACTUAL_MONTH_5             
			, FINAL_PERCENT_ACTUAL_MONTH_5       
			, PERCENT_ACTUAL_MONTH_6             
			, FINAL_PERCENT_ACTUAL_MONTH_6       
			, PERCENT_ACTUAL_MONTH_7             
			, FINAL_PERCENT_ACTUAL_MONTH_7       
			, PERCENT_ACTUAL_MONTH_8             
			, FINAL_PERCENT_ACTUAL_MONTH_8       
			, PERCENT_ACTUAL_MONTH_9             
			, FINAL_PERCENT_ACTUAL_MONTH_9       
			, PERCENT_ACTUAL_MONTH_10            
			, FINAL_PERCENT_ACTUAL_MONTH_10      
			, PERCENT_ACTUAL_MONTH_11            
			, FINAL_PERCENT_ACTUAL_MONTH_11      
			, PERCENT_ACTUAL_MONTH_12            
			, FINAL_PERCENT_ACTUAL_MONTH_12      

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @ALLOCATED_ID                       
			, @PERCENT_ACTUAL_TOTAL               
			, @FINAL_PERCENT_ACTUAL_TOTAL         
			, @PERCENT_ACTUAL_MONTH_1             
			, @FINAL_PERCENT_ACTUAL_MONTH_1       
			, @PERCENT_ACTUAL_MONTH_2             
			, @FINAL_PERCENT_ACTUAL_MONTH_2       
			, @PERCENT_ACTUAL_MONTH_3             
			, @FINAL_PERCENT_ACTUAL_MONTH_3       
			, @PERCENT_ACTUAL_MONTH_4             
			, @FINAL_PERCENT_ACTUAL_MONTH_4       
			, @PERCENT_ACTUAL_MONTH_5             
			, @FINAL_PERCENT_ACTUAL_MONTH_5       
			, @PERCENT_ACTUAL_MONTH_6             
			, @FINAL_PERCENT_ACTUAL_MONTH_6       
			, @PERCENT_ACTUAL_MONTH_7             
			, @FINAL_PERCENT_ACTUAL_MONTH_7       
			, @PERCENT_ACTUAL_MONTH_8             
			, @FINAL_PERCENT_ACTUAL_MONTH_8       
			, @PERCENT_ACTUAL_MONTH_9             
			, @FINAL_PERCENT_ACTUAL_MONTH_9       
			, @PERCENT_ACTUAL_MONTH_10            
			, @FINAL_PERCENT_ACTUAL_MONTH_10      
			, @PERCENT_ACTUAL_MONTH_11            
			, @FINAL_PERCENT_ACTUAL_MONTH_11      
			, @PERCENT_ACTUAL_MONTH_12            
			, @FINAL_PERCENT_ACTUAL_MONTH_12      

			);
	end else begin
		update B_KPI_ACT_PERCENT_ALLOCATES
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , ALLOCATED_ID                         = @ALLOCATED_ID                       
		     , PERCENT_ACTUAL_TOTAL                 = @PERCENT_ACTUAL_TOTAL               
		     , FINAL_PERCENT_ACTUAL_TOTAL           = @FINAL_PERCENT_ACTUAL_TOTAL         
		     , PERCENT_ACTUAL_MONTH_1               = @PERCENT_ACTUAL_MONTH_1             
		     , FINAL_PERCENT_ACTUAL_MONTH_1         = @FINAL_PERCENT_ACTUAL_MONTH_1       
		     , PERCENT_ACTUAL_MONTH_2               = @PERCENT_ACTUAL_MONTH_2             
		     , FINAL_PERCENT_ACTUAL_MONTH_2         = @FINAL_PERCENT_ACTUAL_MONTH_2       
		     , PERCENT_ACTUAL_MONTH_3               = @PERCENT_ACTUAL_MONTH_3             
		     , FINAL_PERCENT_ACTUAL_MONTH_3         = @FINAL_PERCENT_ACTUAL_MONTH_3       
		     , PERCENT_ACTUAL_MONTH_4               = @PERCENT_ACTUAL_MONTH_4             
		     , FINAL_PERCENT_ACTUAL_MONTH_4         = @FINAL_PERCENT_ACTUAL_MONTH_4       
		     , PERCENT_ACTUAL_MONTH_5               = @PERCENT_ACTUAL_MONTH_5             
		     , FINAL_PERCENT_ACTUAL_MONTH_5         = @FINAL_PERCENT_ACTUAL_MONTH_5       
		     , PERCENT_ACTUAL_MONTH_6               = @PERCENT_ACTUAL_MONTH_6             
		     , FINAL_PERCENT_ACTUAL_MONTH_6         = @FINAL_PERCENT_ACTUAL_MONTH_6       
		     , PERCENT_ACTUAL_MONTH_7               = @PERCENT_ACTUAL_MONTH_7             
		     , FINAL_PERCENT_ACTUAL_MONTH_7         = @FINAL_PERCENT_ACTUAL_MONTH_7       
		     , PERCENT_ACTUAL_MONTH_8               = @PERCENT_ACTUAL_MONTH_8             
		     , FINAL_PERCENT_ACTUAL_MONTH_8         = @FINAL_PERCENT_ACTUAL_MONTH_8       
		     , PERCENT_ACTUAL_MONTH_9               = @PERCENT_ACTUAL_MONTH_9             
		     , FINAL_PERCENT_ACTUAL_MONTH_9         = @FINAL_PERCENT_ACTUAL_MONTH_9       
		     , PERCENT_ACTUAL_MONTH_10              = @PERCENT_ACTUAL_MONTH_10            
		     , FINAL_PERCENT_ACTUAL_MONTH_10        = @FINAL_PERCENT_ACTUAL_MONTH_10      
		     , PERCENT_ACTUAL_MONTH_11              = @PERCENT_ACTUAL_MONTH_11            
		     , FINAL_PERCENT_ACTUAL_MONTH_11        = @FINAL_PERCENT_ACTUAL_MONTH_11      
		     , PERCENT_ACTUAL_MONTH_12              = @PERCENT_ACTUAL_MONTH_12            
		     , FINAL_PERCENT_ACTUAL_MONTH_12        = @FINAL_PERCENT_ACTUAL_MONTH_12      

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ACT_PERCENT_ALLOCATES_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ACT_PERCENT_ALLOCATES_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassDelete;
GO


Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ACTUAL_ALLOCATE_DETAILS
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ACTUAL_ALLOCATE_DETAILS
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACTUAL_ALLOCATE_DETAILS_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ACTUAL_ALLOCATE_DETAILS_Merge to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassDelete;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ACT_PERCENT_ALLOCATES
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0301', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ACT_PERCENT_ALLOCATES
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ACT_PERCENT_ALLOCATES_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on

	update B_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS
	   set B_KPI_ACT_PERCENT_ALLOCATE_ID       = @ID
	     , DATE_MODIFIED    = getdate()
	     , DATE_MODIFIED_UTC= getutcdate()
	     , MODIFIED_USER_ID = @MODIFIED_USER_ID
	 where B_KPI_ACT_PERCENT_ALLOCATE_ID       = @MERGE_ID
	   and DELETED          = 0;



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ACT_PERCENT_ALLOCATES_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ACTUAL_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ACTUAL_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditView    'B_KPI_ACTUAL_ALLOCATE_DETAILS';
end -- if;
GO


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ACT_PERCENT_ALLOCATES';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ACT_PERCENT_ALLOCATES';
	exec dbo.spSqlBuildAuditView    'B_KPI_ACT_PERCENT_ALLOCATES';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0301.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0301.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB0301.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB0301.DetailView'   , 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 1, 'KPIB0301.LBL_PERCENT_ACTUAL_TOTAL', 'PERCENT_ACTUAL_TOTAL', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 2, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_TOTAL', 'FINAL_PERCENT_ACTUAL_TOTAL', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 3, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_1', 'PERCENT_ACTUAL_MONTH_1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 4, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_1', 'FINAL_PERCENT_ACTUAL_MONTH_1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 5, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_2', 'PERCENT_ACTUAL_MONTH_2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 6, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_2', 'FINAL_PERCENT_ACTUAL_MONTH_2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 7, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_3', 'PERCENT_ACTUAL_MONTH_3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 8, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_3', 'FINAL_PERCENT_ACTUAL_MONTH_3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 9, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_4', 'PERCENT_ACTUAL_MONTH_4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 10, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_4', 'FINAL_PERCENT_ACTUAL_MONTH_4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 11, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_5', 'PERCENT_ACTUAL_MONTH_5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 12, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_5', 'FINAL_PERCENT_ACTUAL_MONTH_5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 13, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_6', 'PERCENT_ACTUAL_MONTH_6', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 14, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_6', 'FINAL_PERCENT_ACTUAL_MONTH_6', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 15, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_7', 'PERCENT_ACTUAL_MONTH_7', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 16, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_7', 'FINAL_PERCENT_ACTUAL_MONTH_7', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 17, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_8', 'PERCENT_ACTUAL_MONTH_8', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 18, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_8', 'FINAL_PERCENT_ACTUAL_MONTH_8', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 19, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_9', 'PERCENT_ACTUAL_MONTH_9', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 20, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_9', 'FINAL_PERCENT_ACTUAL_MONTH_9', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 21, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_10', 'PERCENT_ACTUAL_MONTH_10', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 22, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_10', 'FINAL_PERCENT_ACTUAL_MONTH_10', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 23, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_11', 'PERCENT_ACTUAL_MONTH_11', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 24, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_11', 'FINAL_PERCENT_ACTUAL_MONTH_11', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 25, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_12', 'PERCENT_ACTUAL_MONTH_12', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 26, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_12', 'FINAL_PERCENT_ACTUAL_MONTH_12', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 27, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 28, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 29, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0301.DetailView', 30, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO

-- delete from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0301.DetailView';

if not exists(select * from DETAILVIEWS_RELATIONSHIPS where DETAIL_NAME = 'KPIB0301.DetailView' and MODULE_NAME = 'Accounts' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_RELATIONSHIPS KPIB0301.DetailView';
	exec dbo.spDETAILVIEWS_RELATIONSHIPS_InsertOnly 'KPIB0301.DetailView', 'Accounts', '~/KPIB0301/Accounts', null, 'Accounts.LBL_MODULE_NAME';
end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB0301.DetailView', 'KPIB0301';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB0301.EditView'  , 'KPIB0301';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB0301.PopupView' , 'KPIB0301';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0301.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB0301.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0301.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB0301.EditView', -1, null;
end -- if;
GO


if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0301.Accounts' and DELETED = 0) begin -- then
	print 'DYNAMIC_BUTTONS KPIB0301.Accounts';
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0301.Accounts', 0, 'KPIB0301', 'edit', 'Accounts', 'edit', 'Accounts.Create'         , null, '.LBL_NEW_BUTTON_LABEL'   , '.LBL_NEW_BUTTON_TITLE'   , '.LBL_NEW_BUTTON_KEY'   , null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsPopup  'KPIB0301.Accounts', 1, 'KPIB0301', 'edit', 'Accounts', 'list', 'AccountPopup();', null, '.LBL_SELECT_BUTTON_LABEL', '.LBL_SELECT_BUTTON_TITLE', '.LBL_SELECT_BUTTON_KEY', null, null;
	exec dbo.spDYNAMIC_BUTTONS_InsButton 'KPIB0301.Accounts', 2, 'KPIB0301', 'view', 'Accounts', 'list', 'Accounts.Search'         , null, '.LBL_SEARCH_BUTTON_LABEL', '.LBL_SEARCH_BUTTON_TITLE', null, null, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0301.EditView', 'KPIB0301'      , 'vwB_KPI_ACT_PERCENT_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0301.EditView', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 1, 'KPIB0301.LBL_PERCENT_ACTUAL_TOTAL', 'PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 2, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_TOTAL', 'FINAL_PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 3, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_1', 'PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 4, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_1', 'FINAL_PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 5, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_2', 'PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 6, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_2', 'FINAL_PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 7, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_3', 'PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 8, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_3', 'FINAL_PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 9, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_4', 'PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 10, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_4', 'FINAL_PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 11, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_5', 'PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 12, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_5', 'FINAL_PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 13, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_6', 'PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 14, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_6', 'FINAL_PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 15, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_7', 'PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 16, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_7', 'FINAL_PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 17, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_8', 'PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 18, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_8', 'FINAL_PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 19, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_9', 'PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 20, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_9', 'FINAL_PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 21, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_10', 'PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 22, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_10', 'FINAL_PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 23, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_11', 'PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 24, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_11', 'FINAL_PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 25, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_12', 'PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView', 26, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_12', 'FINAL_PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.EditView', 27, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.EditView', 28, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0301.EditView.Inline', 'KPIB0301'      , 'vwB_KPI_ACT_PERCENT_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0301.EditView.Inline', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 1, 'KPIB0301.LBL_PERCENT_ACTUAL_TOTAL', 'PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 2, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_TOTAL', 'FINAL_PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 3, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_1', 'PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 4, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_1', 'FINAL_PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 5, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_2', 'PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 6, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_2', 'FINAL_PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 7, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_3', 'PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 8, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_3', 'FINAL_PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 9, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_4', 'PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 10, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_4', 'FINAL_PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 11, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_5', 'PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 12, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_5', 'FINAL_PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 13, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_6', 'PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 14, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_6', 'FINAL_PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 15, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_7', 'PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 16, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_7', 'FINAL_PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 17, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_8', 'PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 18, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_8', 'FINAL_PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 19, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_9', 'PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 20, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_9', 'FINAL_PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 21, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_10', 'PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 22, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_10', 'FINAL_PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 23, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_11', 'PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 24, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_11', 'FINAL_PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 25, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_12', 'PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.EditView.Inline', 26, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_12', 'FINAL_PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.EditView.Inline', 27, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.EditView.Inline', 28, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0301.PopupView.Inline', 'KPIB0301'      , 'vwB_KPI_ACT_PERCENT_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0301.PopupView.Inline', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 1, 'KPIB0301.LBL_PERCENT_ACTUAL_TOTAL', 'PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 2, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_TOTAL', 'FINAL_PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 3, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_1', 'PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 4, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_1', 'FINAL_PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 5, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_2', 'PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 6, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_2', 'FINAL_PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 7, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_3', 'PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 8, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_3', 'FINAL_PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 9, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_4', 'PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 10, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_4', 'FINAL_PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 11, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_5', 'PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 12, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_5', 'FINAL_PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 13, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_6', 'PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 14, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_6', 'FINAL_PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 15, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_7', 'PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 16, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_7', 'FINAL_PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 17, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_8', 'PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 18, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_8', 'FINAL_PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 19, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_9', 'PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 20, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_9', 'FINAL_PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 21, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_10', 'PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 22, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_10', 'FINAL_PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 23, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_11', 'PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 24, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_11', 'FINAL_PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 25, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_12', 'PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0301.PopupView.Inline', 26, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_12', 'FINAL_PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.PopupView.Inline', 27, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0301.PopupView.Inline', 28, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0301.SearchBasic'    , 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0301.SearchBasic', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0301.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0301.SearchAdvanced' , 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0301.SearchAdvanced', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 1, 'KPIB0301.LBL_PERCENT_ACTUAL_TOTAL', 'PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 2, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_TOTAL', 'FINAL_PERCENT_ACTUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 3, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_1', 'PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 4, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_1', 'FINAL_PERCENT_ACTUAL_MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 5, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_2', 'PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 6, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_2', 'FINAL_PERCENT_ACTUAL_MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 7, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_3', 'PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 8, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_3', 'FINAL_PERCENT_ACTUAL_MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 9, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_4', 'PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 10, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_4', 'FINAL_PERCENT_ACTUAL_MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 11, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_5', 'PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 12, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_5', 'FINAL_PERCENT_ACTUAL_MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 13, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_6', 'PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 14, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_6', 'FINAL_PERCENT_ACTUAL_MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 15, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_7', 'PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 16, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_7', 'FINAL_PERCENT_ACTUAL_MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 17, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_8', 'PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 18, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_8', 'FINAL_PERCENT_ACTUAL_MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 19, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_9', 'PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 20, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_9', 'FINAL_PERCENT_ACTUAL_MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 21, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_10', 'PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 22, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_10', 'FINAL_PERCENT_ACTUAL_MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 23, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_11', 'PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 24, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_11', 'FINAL_PERCENT_ACTUAL_MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 25, 'KPIB0301.LBL_PERCENT_ACTUAL_MONTH_12', 'PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0301.SearchAdvanced', 26, 'KPIB0301.LBL_FINAL_PERCENT_ACTUAL_MONTH_12', 'FINAL_PERCENT_ACTUAL_MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB0301.SearchAdvanced' , 27, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0301.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0301.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0301.SearchPopup'    , 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0301.SearchPopup', 0, 'KPIB0301.LBL_ALLOCATED_ID', 'ALLOCATED_ID', 1, 1, 'ALLOCATED_NAME', 'return KPIB0301Popup();', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.Accounts' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.Accounts' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0301.Accounts';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0301.Accounts', 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_ACCOUNTS';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0301.Accounts', 0, 'Accounts.LBL_LIST_NAME', 'NAME', 'NAME', '50%', 'listViewTdLinkS1', 'ID', '~/Accounts/view.aspx?id={0}', null, 'Accounts', 'ASSIGNED_USER_ID';
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0301.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0301.Export', 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.Export'         ,  1, 'KPIB0301.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0301.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0301.ListView', 'KPIB0301'      , 'vwB_KPI_ACT_PERCENT_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.ListView', 2, 'KPIB0301.LBL_LIST_ALLOCATED_ID', 'ALLOCATED_NAME', 'ALLOCATED_NAME', '20%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0301.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0301.PopupView', 'KPIB0301'      , 'vwB_KPI_ACT_PERCENT_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.PopupView', 1, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0301.PopupView', 2, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0301.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0301.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0301.SearchDuplicates', 'KPIB0301', 'vwB_KPI_ACT_PERCENT_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0301.SearchDuplicates'          , 1, 'KPIB0301.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB0301/view.aspx?id={0}', null, 'KPIB0301', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB0301', '.moduleList.KPIB0301', '~/KPIB0301/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ACT_PERCENT_ALLOCATES', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB0301';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB0301' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0301', 'KPIB0301.LNK_NEW_B_KPI_ACT_PERCENT_ALLOCATE' , '~/KPIB0301/edit.aspx'   , 'CreateKPIB0301.gif', 1,  1, 'KPIB0301', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0301', 'KPIB0301.LNK_B_KPI_ACT_PERCENT_ALLOCATE_LIST', '~/KPIB0301/default.aspx', 'KPIB0301.gif'      , 1,  2, 'KPIB0301', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0301', '.LBL_IMPORT'              , '~/KPIB0301/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB0301', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0301', '.LNK_ACTIVITY_STREAM'     , '~/KPIB0301/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB0301', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB0301', null, null, N'KPIB0301 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB0301', null, null, N'Create KPIB0301';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ACT_PERCENT_ALLOCATE_LIST'                          , N'en-US', N'KPIB0301', null, null, N'KPIB0301';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ACT_PERCENT_ALLOCATE'                           , N'en-US', N'KPIB0301', null, null, N'Create KPIB0301';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB0301', null, null, N'KPIB0301 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ACT_PERCENT_ALLOCATE_NOT_FOUND'                     , N'en-US', N'KPIB0301', null, null, N'KPIB0301 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ACT_PERCENT_ALLOCATE_CONFIRMATION'           , N'en-US', N'KPIB0301', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB0301', null, null, N'KPIB0301';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB0301', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB0301'                                          , N'en-US', null, N'moduleList', 100, N'KPIB0301';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATED_ID'                                      , 'en-US', 'KPIB0301', null, null, 'allocated id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATED_ID'                                 , 'en-US', 'KPIB0301', null, null, 'allocated id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_TOTAL'                              , 'en-US', 'KPIB0301', null, null, 'percent actual_total:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_TOTAL'                         , 'en-US', 'KPIB0301', null, null, 'percent actual_total';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_TOTAL'                        , 'en-US', 'KPIB0301', null, null, 'final percent_actual_total:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_TOTAL'                   , 'en-US', 'KPIB0301', null, null, 'final percent_actual_total';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_1'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_1'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_1'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_1'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_2'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_2'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_2'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_2'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_3'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_3'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_3'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_3'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_4'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_4'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_4'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_4'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_5'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_5'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_5';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_5'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_5'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_5';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_6'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_6:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_6'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_6';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_6'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_6:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_6'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_6';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_7'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_7:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_7'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_7';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_7'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_7:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_7'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_7';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_8'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_8:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_8'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_8';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_8'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_8:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_8'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_8';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_9'                            , 'en-US', 'KPIB0301', null, null, 'percent actual_month_9:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_9'                       , 'en-US', 'KPIB0301', null, null, 'percent actual_month_9';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_9'                      , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_9:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_9'                 , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_9';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_10'                           , 'en-US', 'KPIB0301', null, null, 'percent actual_month_10:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_10'                      , 'en-US', 'KPIB0301', null, null, 'percent actual_month_10';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_10'                     , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_10:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_10'                , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_10';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_11'                           , 'en-US', 'KPIB0301', null, null, 'percent actual_month_11:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_11'                      , 'en-US', 'KPIB0301', null, null, 'percent actual_month_11';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_11'                     , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_11:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_11'                , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_11';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_ACTUAL_MONTH_12'                           , 'en-US', 'KPIB0301', null, null, 'percent actual_month_12:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_ACTUAL_MONTH_12'                      , 'en-US', 'KPIB0301', null, null, 'percent actual_month_12';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_PERCENT_ACTUAL_MONTH_12'                     , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_12:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_PERCENT_ACTUAL_MONTH_12'                , 'en-US', 'KPIB0301', null, null, 'final percent_actual_month_12';





-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ACT_PERCENT_ALLOCATES_CSRM';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ACT_PERCENT_ALLOCATES_CSRM';
	exec dbo.spSqlBuildAuditView    'B_KPI_ACT_PERCENT_ALLOCATES_CSRM';
end -- if;
GO

-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ACTUAL_ALLOCATE_DETAILS_CSRM';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ACTUAL_ALLOCATE_DETAILS_CSRM';
	exec dbo.spSqlBuildAuditView    'B_KPI_ACTUAL_ALLOCATE_DETAILS_CSRM';
end -- if;
GO

