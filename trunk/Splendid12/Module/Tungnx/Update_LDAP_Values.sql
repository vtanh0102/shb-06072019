/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[DELETED]
      ,[CREATED_BY]
      ,[DATE_ENTERED]
      ,[MODIFIED_USER_ID]
      ,[DATE_MODIFIED]
      ,[DATE_MODIFIED_UTC]
      ,[CATEGORY]
      ,[NAME]
      ,[VALUE]
  FROM [SplendidCRM_Dev].[dbo].[CONFIG]
  WHERE NAME LIKE '%LDA%' 

  UPDATE [CONFIG] SET [VALUE] = 0
  WHERE NAME = 'LDAP.SingleSignOn.Enabled';

  GO

  UPDATE [CONFIG] SET [VALUE] = 'LDAP://shbho.shb.vn'
  WHERE NAME = 'LDAP.ActiveDirectory.Path';
  GO

  UPDATE [CONFIG] SET [VALUE] = 'shbho'
  WHERE NAME = 'LDAP.Domain';
  GO
