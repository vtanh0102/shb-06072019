DROP TABLE [dbo].[B_KPI_ORG_ACTUAL_RESULT_DETAIL]
GO
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL';
	Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ORG_ACTUAL_RESULT_DETAIL primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, NAME                               nvarchar(150) null
		, ACTUAL_RESULT_CODE                 nvarchar(50) null
		, YEAR                               int null
		, MONTH_PERIOD                       nvarchar(50) null
		, VERSION_NUMBER                     nvarchar(2) null
		, KPI_ID                             uniqueidentifier null
		, KPI_CODE                           nvarchar(50) null
		, KPI_NAME                           nvarchar(200) null
		, KPI_UNIT                           int null
		, LEVEL_NUMBER                       int null
		, RATIO                              float null
		, PLAN_VALUE                         float null
		, SYNC_VALUE                         float null
		, FINAL_VALUE                        float null
		, PERCENT_SYNC_VALUE                 float null
		, PERCENT_FINAL_VALUE                float null
		, PERCENT_MANUAL_VALUE               float null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, LATEST_SYNC_DATE                   datetime null
		, FLEX1                              nvarchar(500) null
		, FLEX2                              nvarchar(500) null
		, FLEX3                              nvarchar(500) null
		, FLEX4                              nvarchar(500) null
		, FLEX5                              nvarchar(500) null

		)

	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_ASSIGNED_USER_ID on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_TEAM_ID          on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_TEAM_SET_ID      on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_NAME  on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM';
	Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_ASSIGNED_USER_ID on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_TEAM_ID          on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_DETAIL_TEAM_SET_ID      on dbo.B_KPI_ORG_ACTUAL_RESULT_DETAIL (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add NAME nvarchar(150) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'ACTUAL_RESULT_CODE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add ACTUAL_RESULT_CODE nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add ACTUAL_RESULT_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'YEAR') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add YEAR int null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add YEAR int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'MONTH_PERIOD') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add MONTH_PERIOD nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add MONTH_PERIOD nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add VERSION_NUMBER nvarchar(2) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add VERSION_NUMBER nvarchar(2) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'KPI_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'KPI_CODE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_CODE nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'KPI_NAME') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_NAME nvarchar(200) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'KPI_UNIT') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_UNIT int null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add KPI_UNIT int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'LEVEL_NUMBER') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add LEVEL_NUMBER int null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add LEVEL_NUMBER int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'RATIO') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add RATIO float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add RATIO float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'PLAN_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PLAN_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PLAN_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'SYNC_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add SYNC_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add SYNC_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FINAL_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FINAL_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FINAL_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'PERCENT_SYNC_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_SYNC_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_SYNC_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'PERCENT_FINAL_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_FINAL_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_FINAL_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'PERCENT_MANUAL_VALUE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_MANUAL_VALUE float null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add PERCENT_MANUAL_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add REMARK nvarchar(max) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add REMARK nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'LATEST_SYNC_DATE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add LATEST_SYNC_DATE datetime null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add LATEST_SYNC_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FLEX1') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX1 nvarchar(500) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX1 nvarchar(500) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FLEX2') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX2 nvarchar(500) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX2 nvarchar(500) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FLEX3') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX3 nvarchar(500) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX3 nvarchar(500) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FLEX4') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX4 nvarchar(500) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX4 nvarchar(500) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_DETAIL' and COLUMN_NAME = 'FLEX5') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX5 nvarchar(500) null';
	alter table B_KPI_ORG_ACTUAL_RESULT_DETAIL add FLEX5 nvarchar(500) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL
as
select B_KPI_ORG_ACTUAL_RESULT_DETAIL.ID
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.NAME
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.ACTUAL_RESULT_CODE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.YEAR
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.MONTH_PERIOD
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.VERSION_NUMBER
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.KPI_ID
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.KPI_CODE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.KPI_NAME
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.KPI_UNIT
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.LEVEL_NUMBER
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.RATIO
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.PLAN_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.SYNC_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FINAL_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.PERCENT_SYNC_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.PERCENT_FINAL_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.PERCENT_MANUAL_VALUE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.DESCRIPTION
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.REMARK
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.LATEST_SYNC_DATE
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FLEX1
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FLEX2
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FLEX3
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FLEX4
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.FLEX5
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.DATE_ENTERED
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.DATE_MODIFIED
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM.*
  from            B_KPI_ORG_ACTUAL_RESULT_DETAIL
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ORG_ACTUAL_RESULT_DETAIL.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ORG_ACTUAL_RESULT_DETAIL.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ORG_ACTUAL_RESULT_DETAIL.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ORG_ACTUAL_RESULT_DETAIL.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ORG_ACTUAL_RESULT_DETAIL.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ORG_ACTUAL_RESULT_DETAIL.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ORG_ACTUAL_RESULT_DETAIL.MODIFIED_USER_ID
  left outer join B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM
               on B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM.ID_C     = B_KPI_ORG_ACTUAL_RESULT_DETAIL.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ORG_ACTUAL_RESULT_DETAIL.ID
 where B_KPI_ORG_ACTUAL_RESULT_DETAIL.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit
as
select *
  from vwB_KPI_ORG_ACTUAL_RESULT_DETAIL

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List
as
select *
  from vwB_KPI_ORG_ACTUAL_RESULT_DETAIL

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Undelete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB030201_DETAIL';
	
	-- BEGIN Oracle Exception
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ORG_ACTUAL_RESULT_DETAIL_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ACTUAL_RESULT_CODE                 nvarchar(50)
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @KPI_ID                             uniqueidentifier
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @KPI_UNIT                           int
	, @LEVEL_NUMBER                       int
	, @RATIO                              float
	, @PLAN_VALUE                         float
	, @SYNC_VALUE                         float
	, @FINAL_VALUE                        float
	, @PERCENT_SYNC_VALUE                 float
	, @PERCENT_FINAL_VALUE                float
	, @PERCENT_MANUAL_VALUE               float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @LATEST_SYNC_DATE                   datetime
	, @FLEX1                              nvarchar(500)
	, @FLEX2                              nvarchar(500)
	, @FLEX3                              nvarchar(500)
	, @FLEX4                              nvarchar(500)
	, @FLEX5                              nvarchar(500)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ORG_ACTUAL_RESULT_DETAIL
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ACTUAL_RESULT_CODE                 
			, YEAR                               
			, MONTH_PERIOD                       
			, VERSION_NUMBER                     
			, KPI_ID                             
			, KPI_CODE                           
			, KPI_NAME                           
			, KPI_UNIT                           
			, LEVEL_NUMBER                       
			, RATIO                              
			, PLAN_VALUE                         
			, SYNC_VALUE                         
			, FINAL_VALUE                        
			, PERCENT_SYNC_VALUE                 
			, PERCENT_FINAL_VALUE                
			, PERCENT_MANUAL_VALUE               
			, DESCRIPTION                        
			, REMARK                             
			, LATEST_SYNC_DATE                   
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @ACTUAL_RESULT_CODE                 
			, @YEAR                               
			, @MONTH_PERIOD                       
			, @VERSION_NUMBER                     
			, @KPI_ID                             
			, @KPI_CODE                           
			, @KPI_NAME                           
			, @KPI_UNIT                           
			, @LEVEL_NUMBER                       
			, @RATIO                              
			, @PLAN_VALUE                         
			, @SYNC_VALUE                         
			, @FINAL_VALUE                        
			, @PERCENT_SYNC_VALUE                 
			, @PERCENT_FINAL_VALUE                
			, @PERCENT_MANUAL_VALUE               
			, @DESCRIPTION                        
			, @REMARK                             
			, @LATEST_SYNC_DATE                   
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , ACTUAL_RESULT_CODE                   = @ACTUAL_RESULT_CODE                 
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD                       
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , KPI_ID                               = @KPI_ID                             
		     , KPI_CODE                             = @KPI_CODE                           
		     , KPI_NAME                             = @KPI_NAME                           
		     , KPI_UNIT                             = @KPI_UNIT                           
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , RATIO                                = @RATIO                              
		     , PLAN_VALUE                           = @PLAN_VALUE                         
		     , SYNC_VALUE                           = @SYNC_VALUE                         
		     , FINAL_VALUE                          = @FINAL_VALUE                        
		     , PERCENT_SYNC_VALUE                   = @PERCENT_SYNC_VALUE                 
		     , PERCENT_FINAL_VALUE                  = @PERCENT_FINAL_VALUE                
		     , PERCENT_MANUAL_VALUE                 = @PERCENT_MANUAL_VALUE               
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , LATEST_SYNC_DATE                     = @LATEST_SYNC_DATE                   
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 where ID                                   = @ID                                 ;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB030201_DETAIL', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassDelete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ORG_ACTUAL_RESULT_DETAIL
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB030201_DETAIL', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB030201_DETAIL', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ORG_ACTUAL_RESULT_DETAIL
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ORG_ACTUAL_RESULT_DETAIL';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ORG_ACTUAL_RESULT_DETAIL';
	exec dbo.spSqlBuildAuditView    'B_KPI_ORG_ACTUAL_RESULT_DETAIL';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB030201_DETAIL.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB030201_DETAIL.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB030201_DETAIL.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB030201_DETAIL.DetailView'   , 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 1, 'KPIB030201_DETAIL.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 2, 'KPIB030201_DETAIL.LBL_YEAR', 'YEAR', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 3, 'KPIB030201_DETAIL.LBL_MONTH_PERIOD', 'MONTH_PERIOD', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 4, 'KPIB030201_DETAIL.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 5, 'KPIB030201_DETAIL.LBL_KPI_ID', 'KPI_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 6, 'KPIB030201_DETAIL.LBL_KPI_CODE', 'KPI_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 7, 'KPIB030201_DETAIL.LBL_KPI_NAME', 'KPI_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 8, 'KPIB030201_DETAIL.LBL_KPI_UNIT', 'KPI_UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 9, 'KPIB030201_DETAIL.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 10, 'KPIB030201_DETAIL.LBL_RATIO', 'RATIO', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 11, 'KPIB030201_DETAIL.LBL_PLAN_VALUE', 'PLAN_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 12, 'KPIB030201_DETAIL.LBL_SYNC_VALUE', 'SYNC_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 13, 'KPIB030201_DETAIL.LBL_FINAL_VALUE', 'FINAL_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 14, 'KPIB030201_DETAIL.LBL_PERCENT_SYNC_VALUE', 'PERCENT_SYNC_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 15, 'KPIB030201_DETAIL.LBL_PERCENT_FINAL_VALUE', 'PERCENT_FINAL_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 16, 'KPIB030201_DETAIL.LBL_PERCENT_MANUAL_VALUE', 'PERCENT_MANUAL_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 17, 'KPIB030201_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 18, 'KPIB030201_DETAIL.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 19, 'KPIB030201_DETAIL.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 20, 'KPIB030201_DETAIL.LBL_FLEX1', 'FLEX1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 21, 'KPIB030201_DETAIL.LBL_FLEX2', 'FLEX2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 22, 'KPIB030201_DETAIL.LBL_FLEX3', 'FLEX3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 23, 'KPIB030201_DETAIL.LBL_FLEX4', 'FLEX4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 24, 'KPIB030201_DETAIL.LBL_FLEX5', 'FLEX5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 25, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 26, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 27, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201_DETAIL.DetailView', 28, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB030201_DETAIL.DetailView', 'KPIB030201_DETAIL';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB030201_DETAIL.EditView'  , 'KPIB030201_DETAIL';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB030201_DETAIL.PopupView' , 'KPIB030201_DETAIL';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB030201_DETAIL.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB030201_DETAIL.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB030201_DETAIL.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB030201_DETAIL.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201_DETAIL.EditView', 'KPIB030201_DETAIL'      , 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 1, 'KPIB030201_DETAIL.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 2, 'KPIB030201_DETAIL.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 3, 'KPIB030201_DETAIL.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 4, 'KPIB030201_DETAIL.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 5, 'KPIB030201_DETAIL.LBL_KPI_ID', 'KPI_ID', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 6, 'KPIB030201_DETAIL.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 7, 'KPIB030201_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 8, 'KPIB030201_DETAIL.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 9, 'KPIB030201_DETAIL.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 10, 'KPIB030201_DETAIL.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 11, 'KPIB030201_DETAIL.LBL_PLAN_VALUE', 'PLAN_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 12, 'KPIB030201_DETAIL.LBL_SYNC_VALUE', 'SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 13, 'KPIB030201_DETAIL.LBL_FINAL_VALUE', 'FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 14, 'KPIB030201_DETAIL.LBL_PERCENT_SYNC_VALUE', 'PERCENT_SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 15, 'KPIB030201_DETAIL.LBL_PERCENT_FINAL_VALUE', 'PERCENT_FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 16, 'KPIB030201_DETAIL.LBL_PERCENT_MANUAL_VALUE', 'PERCENT_MANUAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.EditView', 17, 'KPIB030201_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.EditView', 18, 'KPIB030201_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201_DETAIL.EditView', 19, 'KPIB030201_DETAIL.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 20, 'KPIB030201_DETAIL.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 21, 'KPIB030201_DETAIL.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 22, 'KPIB030201_DETAIL.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 23, 'KPIB030201_DETAIL.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView', 24, 'KPIB030201_DETAIL.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.EditView', 25, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.EditView', 26, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201_DETAIL.EditView.Inline', 'KPIB030201_DETAIL'      , 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 1, 'KPIB030201_DETAIL.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 2, 'KPIB030201_DETAIL.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 3, 'KPIB030201_DETAIL.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 4, 'KPIB030201_DETAIL.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 5, 'KPIB030201_DETAIL.LBL_KPI_ID', 'KPI_ID', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 6, 'KPIB030201_DETAIL.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 7, 'KPIB030201_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 8, 'KPIB030201_DETAIL.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 9, 'KPIB030201_DETAIL.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 10, 'KPIB030201_DETAIL.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 11, 'KPIB030201_DETAIL.LBL_PLAN_VALUE', 'PLAN_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 12, 'KPIB030201_DETAIL.LBL_SYNC_VALUE', 'SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 13, 'KPIB030201_DETAIL.LBL_FINAL_VALUE', 'FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 14, 'KPIB030201_DETAIL.LBL_PERCENT_SYNC_VALUE', 'PERCENT_SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 15, 'KPIB030201_DETAIL.LBL_PERCENT_FINAL_VALUE', 'PERCENT_FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 16, 'KPIB030201_DETAIL.LBL_PERCENT_MANUAL_VALUE', 'PERCENT_MANUAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.EditView.Inline', 17, 'KPIB030201_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.EditView.Inline', 18, 'KPIB030201_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201_DETAIL.EditView.Inline', 19, 'KPIB030201_DETAIL.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 20, 'KPIB030201_DETAIL.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 21, 'KPIB030201_DETAIL.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 22, 'KPIB030201_DETAIL.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 23, 'KPIB030201_DETAIL.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.EditView.Inline', 24, 'KPIB030201_DETAIL.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.EditView.Inline', 25, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.EditView.Inline', 26, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201_DETAIL.PopupView.Inline', 'KPIB030201_DETAIL'      , 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 1, 'KPIB030201_DETAIL.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 2, 'KPIB030201_DETAIL.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 3, 'KPIB030201_DETAIL.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 4, 'KPIB030201_DETAIL.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 5, 'KPIB030201_DETAIL.LBL_KPI_ID', 'KPI_ID', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 6, 'KPIB030201_DETAIL.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 7, 'KPIB030201_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 8, 'KPIB030201_DETAIL.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 9, 'KPIB030201_DETAIL.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 10, 'KPIB030201_DETAIL.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 11, 'KPIB030201_DETAIL.LBL_PLAN_VALUE', 'PLAN_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 12, 'KPIB030201_DETAIL.LBL_SYNC_VALUE', 'SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 13, 'KPIB030201_DETAIL.LBL_FINAL_VALUE', 'FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 14, 'KPIB030201_DETAIL.LBL_PERCENT_SYNC_VALUE', 'PERCENT_SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 15, 'KPIB030201_DETAIL.LBL_PERCENT_FINAL_VALUE', 'PERCENT_FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 16, 'KPIB030201_DETAIL.LBL_PERCENT_MANUAL_VALUE', 'PERCENT_MANUAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.PopupView.Inline', 17, 'KPIB030201_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201_DETAIL.PopupView.Inline', 18, 'KPIB030201_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201_DETAIL.PopupView.Inline', 19, 'KPIB030201_DETAIL.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 20, 'KPIB030201_DETAIL.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 21, 'KPIB030201_DETAIL.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 22, 'KPIB030201_DETAIL.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 23, 'KPIB030201_DETAIL.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201_DETAIL.PopupView.Inline', 24, 'KPIB030201_DETAIL.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.PopupView.Inline', 25, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201_DETAIL.PopupView.Inline', 26, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201_DETAIL.SearchBasic'    , 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201_DETAIL.SearchBasic', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB030201_DETAIL', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201_DETAIL.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201_DETAIL.SearchAdvanced' , 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201_DETAIL.SearchAdvanced', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB030201_DETAIL', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 1, 'KPIB030201_DETAIL.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 2, 'KPIB030201_DETAIL.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 3, 'KPIB030201_DETAIL.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 4, 'KPIB030201_DETAIL.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 5, 'KPIB030201_DETAIL.LBL_KPI_ID', 'KPI_ID', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 6, 'KPIB030201_DETAIL.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 7, 'KPIB030201_DETAIL.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 8, 'KPIB030201_DETAIL.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 9, 'KPIB030201_DETAIL.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 10, 'KPIB030201_DETAIL.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 11, 'KPIB030201_DETAIL.LBL_PLAN_VALUE', 'PLAN_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 12, 'KPIB030201_DETAIL.LBL_SYNC_VALUE', 'SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 13, 'KPIB030201_DETAIL.LBL_FINAL_VALUE', 'FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 14, 'KPIB030201_DETAIL.LBL_PERCENT_SYNC_VALUE', 'PERCENT_SYNC_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 15, 'KPIB030201_DETAIL.LBL_PERCENT_FINAL_VALUE', 'PERCENT_FINAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 16, 'KPIB030201_DETAIL.LBL_PERCENT_MANUAL_VALUE', 'PERCENT_MANUAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB030201_DETAIL.SearchAdvanced', 17, 'KPIB030201_DETAIL.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB030201_DETAIL.SearchAdvanced', 18, 'KPIB030201_DETAIL.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201_DETAIL.SearchAdvanced', 19, 'KPIB030201_DETAIL.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 20, 'KPIB030201_DETAIL.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 21, 'KPIB030201_DETAIL.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 22, 'KPIB030201_DETAIL.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 23, 'KPIB030201_DETAIL.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201_DETAIL.SearchAdvanced', 24, 'KPIB030201_DETAIL.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB030201_DETAIL.SearchAdvanced' , 25, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201_DETAIL.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201_DETAIL.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201_DETAIL.SearchPopup'    , 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201_DETAIL.SearchPopup', 0, 'KPIB030201_DETAIL.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB030201_DETAIL', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201_DETAIL.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201_DETAIL.Export', 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201_DETAIL.Export'         ,  1, 'KPIB030201_DETAIL.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201_DETAIL.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201_DETAIL.ListView', 'KPIB030201_DETAIL'      , 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201_DETAIL.ListView', 2, 'KPIB030201_DETAIL.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIB030201_DETAIL/view.aspx?id={0}', null, 'KPIB030201_DETAIL', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201_DETAIL.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201_DETAIL.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201_DETAIL.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201_DETAIL.PopupView', 'KPIB030201_DETAIL'      , 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201_DETAIL.PopupView', 1, 'KPIB030201_DETAIL.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectKPIB030201_DETAIL(''{0}'', ''{1}'');', null, 'KPIB030201_DETAIL', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201_DETAIL.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201_DETAIL.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201_DETAIL.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201_DETAIL.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201_DETAIL.SearchDuplicates', 'KPIB030201_DETAIL', 'vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201_DETAIL.SearchDuplicates'          , 1, 'KPIB030201_DETAIL.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB030201_DETAIL/view.aspx?id={0}', null, 'KPIB030201_DETAIL', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB030201_DETAIL', '.moduleList.KPIB030201_DETAIL', '~/KPIB030201_DETAIL/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ORG_ACTUAL_RESULT_DETAIL', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB030201_DETAIL';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB030201_DETAIL' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201_DETAIL', 'KPIB030201_DETAIL.LNK_NEW_B_KPI_ORG_ACTUAL_RESULT_DETAIL' , '~/KPIB030201_DETAIL/edit.aspx'   , 'CreateKPIB030201_DETAIL.gif', 1,  1, 'KPIB030201_DETAIL', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201_DETAIL', 'KPIB030201_DETAIL.LNK_B_KPI_ORG_ACTUAL_RESULT_DETAIL_LIST', '~/KPIB030201_DETAIL/default.aspx', 'KPIB030201_DETAIL.gif'      , 1,  2, 'KPIB030201_DETAIL', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201_DETAIL', '.LBL_IMPORT'              , '~/KPIB030201_DETAIL/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB030201_DETAIL', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201_DETAIL', '.LNK_ACTIVITY_STREAM'     , '~/KPIB030201_DETAIL/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB030201_DETAIL', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPIB030201_DETAIL List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB030201_DETAIL', null, null, N'Create KPIB030201_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ORG_ACTUAL_RESULT_DETAIL_LIST'                          , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPIB030201_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ORG_ACTUAL_RESULT_DETAIL'                           , N'en-US', N'KPIB030201_DETAIL', null, null, N'Create KPIB030201_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPIB030201_DETAIL Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ORG_ACTUAL_RESULT_DETAIL_NOT_FOUND'                     , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPIB030201_DETAIL not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ORG_ACTUAL_RESULT_DETAIL_CONFIRMATION'           , N'en-US', N'KPIB030201_DETAIL', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPIB030201_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB030201_DETAIL', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB030201_DETAIL'                                          , N'en-US', null, N'moduleList', 100, N'KPIB030201_DETAIL';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'KPIB030201_DETAIL', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'KPIB030201_DETAIL', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ACTUAL_RESULT_CODE'                                , 'en-US', 'KPIB030201_DETAIL', null, null, 'actual result_code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ACTUAL_RESULT_CODE'                           , 'en-US', 'KPIB030201_DETAIL', null, null, 'actual result_code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_YEAR'                                              , 'en-US', 'KPIB030201_DETAIL', null, null, 'year:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_YEAR'                                         , 'en-US', 'KPIB030201_DETAIL', null, null, 'year';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_PERIOD'                                      , 'en-US', 'KPIB030201_DETAIL', null, null, 'month period:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_PERIOD'                                 , 'en-US', 'KPIB030201_DETAIL', null, null, 'month period';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'KPIB030201_DETAIL', null, null, 'version number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'KPIB030201_DETAIL', null, null, 'version number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_ID'                                            , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_ID'                                       , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_CODE'                                          , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_CODE'                                     , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_NAME'                                          , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_NAME'                                     , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_UNIT'                                          , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_UNIT'                                     , 'en-US', 'KPIB030201_DETAIL', null, null, 'kpi unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LEVEL_NUMBER'                                      , 'en-US', 'KPIB030201_DETAIL', null, null, 'level number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_LEVEL_NUMBER'                                 , 'en-US', 'KPIB030201_DETAIL', null, null, 'level number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RATIO'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'ratio:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RATIO'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'ratio';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PLAN_VALUE'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'plan value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PLAN_VALUE'                                   , 'en-US', 'KPIB030201_DETAIL', null, null, 'plan value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_SYNC_VALUE'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'sync value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_SYNC_VALUE'                                   , 'en-US', 'KPIB030201_DETAIL', null, null, 'sync value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FINAL_VALUE'                                       , 'en-US', 'KPIB030201_DETAIL', null, null, 'final value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FINAL_VALUE'                                  , 'en-US', 'KPIB030201_DETAIL', null, null, 'final value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_SYNC_VALUE'                                , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent sync_value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_SYNC_VALUE'                           , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent sync_value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_FINAL_VALUE'                               , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent final_value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_FINAL_VALUE'                          , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent final_value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_MANUAL_VALUE'                              , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent manual_value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_MANUAL_VALUE'                         , 'en-US', 'KPIB030201_DETAIL', null, null, 'percent manual_value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIB030201_DETAIL', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIB030201_DETAIL', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIB030201_DETAIL', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIB030201_DETAIL', null, null, 'remark';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LATEST_SYNC_DATE'                                  , 'en-US', 'KPIB030201_DETAIL', null, null, 'latest sync_date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_LATEST_SYNC_DATE'                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'latest sync_date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX1'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX1'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX2'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX2'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX3'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX3'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX4'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX4'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX5'                                             , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX5'                                        , 'en-US', 'KPIB030201_DETAIL', null, null, 'flex5';






