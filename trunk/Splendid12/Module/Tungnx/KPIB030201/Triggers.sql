
-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ORG_ACTUAL_RESULT';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ORG_ACTUAL_RESULT';
	exec dbo.spSqlBuildAuditView    'B_KPI_ORG_ACTUAL_RESULT';
end -- if;
GO




