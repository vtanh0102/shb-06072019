
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ORG_ACTUAL_RESULT';
	Create Table dbo.B_KPI_ORG_ACTUAL_RESULT
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ORG_ACTUAL_RESULT primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, NAME                               nvarchar(150) null
		, ACTUAL_RESULT_CODE                 nvarchar(50) null
		, YEAR                               int null
		, MONTH_PERIOD                       nvarchar(50) null
		, ORGANIZATION_ID                    uniqueidentifier null
		, ORGANIZATION_CODE                  nvarchar(50) null
		, VERSION_NUMBER                     nvarchar(2) null
		, ALLOCATE_CODE                      nvarchar(50) null
		, ASSIGN_BY                          uniqueidentifier null
		, ASSIGN_DATE                        datetime null
		, PERCENT_SYNC_TOTAL                 float null
		, PERCENT_FINAL_TOTAL                float null
		, PERCENT_MANUAL_TOTAL               float null
		, TOTAL_AMOUNT_01                    float null
		, TOTAL_AMOUNT_02                    float null
		, TOTAL_AMOUNT_03                    float null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, FILE_ID                            uniqueidentifier null
		, LATEST_SYNC_DATE                   datetime null
		, APPROVE_ID                         uniqueidentifier null
		, APPROVE_STATUS                     nvarchar(5) null
		, APPROVED_BY                        uniqueidentifier null
		, APPROVED_DATE                      datetime null
		, FLEX1                              nvarchar(0) null
		, FLEX2                              nvarchar(0) null
		, FLEX3                              nvarchar(0) null
		, FLEX4                              nvarchar(0) null
		, FLEX5                              nvarchar(0) null

		)

	create index IDX_B_KPI_ORG_ACTUAL_RESULT_ASSIGNED_USER_ID on dbo.B_KPI_ORG_ACTUAL_RESULT (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_TEAM_ID          on dbo.B_KPI_ORG_ACTUAL_RESULT (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_TEAM_SET_ID      on dbo.B_KPI_ORG_ACTUAL_RESULT (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_NAME  on dbo.B_KPI_ORG_ACTUAL_RESULT (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_CSTM';
	Create Table dbo.B_KPI_ORG_ACTUAL_RESULT_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ORG_ACTUAL_RESULT_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_ASSIGNED_USER_ID on dbo.B_KPI_ORG_ACTUAL_RESULT (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_TEAM_ID          on dbo.B_KPI_ORG_ACTUAL_RESULT (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ORG_ACTUAL_RESULT_TEAM_SET_ID      on dbo.B_KPI_ORG_ACTUAL_RESULT (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add NAME nvarchar(150) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ACTUAL_RESULT_CODE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ACTUAL_RESULT_CODE nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ACTUAL_RESULT_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'YEAR') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add YEAR int null';
	alter table B_KPI_ORG_ACTUAL_RESULT add YEAR int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'MONTH_PERIOD') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add MONTH_PERIOD nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add MONTH_PERIOD nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ORGANIZATION_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ORGANIZATION_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ORGANIZATION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ORGANIZATION_CODE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ORGANIZATION_CODE nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ORGANIZATION_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add VERSION_NUMBER nvarchar(2) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add VERSION_NUMBER nvarchar(2) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ALLOCATE_CODE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ALLOCATE_CODE nvarchar(50) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ALLOCATE_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ASSIGN_BY') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGN_BY uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGN_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'ASSIGN_DATE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGN_DATE datetime null';
	alter table B_KPI_ORG_ACTUAL_RESULT add ASSIGN_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'PERCENT_SYNC_TOTAL') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_SYNC_TOTAL float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_SYNC_TOTAL float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'PERCENT_FINAL_TOTAL') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_FINAL_TOTAL float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_FINAL_TOTAL float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'PERCENT_MANUAL_TOTAL') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_MANUAL_TOTAL float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add PERCENT_MANUAL_TOTAL float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'TOTAL_AMOUNT_01') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_01 float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_01 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'TOTAL_AMOUNT_02') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_02 float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_02 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'TOTAL_AMOUNT_03') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_03 float null';
	alter table B_KPI_ORG_ACTUAL_RESULT add TOTAL_AMOUNT_03 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add REMARK nvarchar(max) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add REMARK nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FILE_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FILE_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FILE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'LATEST_SYNC_DATE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add LATEST_SYNC_DATE datetime null';
	alter table B_KPI_ORG_ACTUAL_RESULT add LATEST_SYNC_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'APPROVE_ID') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add APPROVE_ID uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add APPROVE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'APPROVE_STATUS') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add APPROVE_STATUS nvarchar(5) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add APPROVE_STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'APPROVED_BY') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add APPROVED_BY uniqueidentifier null';
	alter table B_KPI_ORG_ACTUAL_RESULT add APPROVED_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'APPROVED_DATE') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add APPROVED_DATE datetime null';
	alter table B_KPI_ORG_ACTUAL_RESULT add APPROVED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FLEX1') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FLEX1 nvarchar(0) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FLEX1 nvarchar(0) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FLEX2') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FLEX2 nvarchar(0) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FLEX2 nvarchar(0) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FLEX3') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FLEX3 nvarchar(0) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FLEX3 nvarchar(0) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FLEX4') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FLEX4 nvarchar(0) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FLEX4 nvarchar(0) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ORG_ACTUAL_RESULT' and COLUMN_NAME = 'FLEX5') begin -- then
	print 'alter table B_KPI_ORG_ACTUAL_RESULT add FLEX5 nvarchar(0) null';
	alter table B_KPI_ORG_ACTUAL_RESULT add FLEX5 nvarchar(0) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT
as
select B_KPI_ORG_ACTUAL_RESULT.ID
     , B_KPI_ORG_ACTUAL_RESULT.NAME
     , B_KPI_ORG_ACTUAL_RESULT.ACTUAL_RESULT_CODE
     , B_KPI_ORG_ACTUAL_RESULT.YEAR
     , B_KPI_ORG_ACTUAL_RESULT.MONTH_PERIOD
     , B_KPI_ORG_ACTUAL_RESULT.ORGANIZATION_ID
     , B_KPI_ORG_ACTUAL_RESULT.ORGANIZATION_CODE
     , B_KPI_ORG_ACTUAL_RESULT.VERSION_NUMBER
     , B_KPI_ORG_ACTUAL_RESULT.ALLOCATE_CODE
     , B_KPI_ORG_ACTUAL_RESULT.ASSIGN_BY
     , B_KPI_ORG_ACTUAL_RESULT.ASSIGN_DATE
     , B_KPI_ORG_ACTUAL_RESULT.PERCENT_SYNC_TOTAL
     , B_KPI_ORG_ACTUAL_RESULT.PERCENT_FINAL_TOTAL
     , B_KPI_ORG_ACTUAL_RESULT.PERCENT_MANUAL_TOTAL
     , B_KPI_ORG_ACTUAL_RESULT.TOTAL_AMOUNT_01
     , B_KPI_ORG_ACTUAL_RESULT.TOTAL_AMOUNT_02
     , B_KPI_ORG_ACTUAL_RESULT.TOTAL_AMOUNT_03
     , B_KPI_ORG_ACTUAL_RESULT.DESCRIPTION
     , B_KPI_ORG_ACTUAL_RESULT.REMARK
     , B_KPI_ORG_ACTUAL_RESULT.FILE_ID
     , B_KPI_ORG_ACTUAL_RESULT.LATEST_SYNC_DATE
     , B_KPI_ORG_ACTUAL_RESULT.APPROVE_ID
     , B_KPI_ORG_ACTUAL_RESULT.APPROVE_STATUS
     , B_KPI_ORG_ACTUAL_RESULT.APPROVED_BY
     , B_KPI_ORG_ACTUAL_RESULT.APPROVED_DATE
     , B_KPI_ORG_ACTUAL_RESULT.FLEX1
     , B_KPI_ORG_ACTUAL_RESULT.FLEX2
     , B_KPI_ORG_ACTUAL_RESULT.FLEX3
     , B_KPI_ORG_ACTUAL_RESULT.FLEX4
     , B_KPI_ORG_ACTUAL_RESULT.FLEX5
     , B_KPI_ORG_ACTUAL_RESULT.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ORG_ACTUAL_RESULT.DATE_ENTERED
     , B_KPI_ORG_ACTUAL_RESULT.DATE_MODIFIED
     , B_KPI_ORG_ACTUAL_RESULT.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ORG_ACTUAL_RESULT.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ORG_ACTUAL_RESULT.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ORG_ACTUAL_RESULT_CSTM.*
  from            B_KPI_ORG_ACTUAL_RESULT
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ORG_ACTUAL_RESULT.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ORG_ACTUAL_RESULT.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ORG_ACTUAL_RESULT.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ORG_ACTUAL_RESULT.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ORG_ACTUAL_RESULT.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ORG_ACTUAL_RESULT.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ORG_ACTUAL_RESULT.MODIFIED_USER_ID
  left outer join B_KPI_ORG_ACTUAL_RESULT_CSTM
               on B_KPI_ORG_ACTUAL_RESULT_CSTM.ID_C     = B_KPI_ORG_ACTUAL_RESULT.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ORG_ACTUAL_RESULT.ID
 where B_KPI_ORG_ACTUAL_RESULT.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT_Edit')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT_Edit;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT_Edit
as
select *
  from vwB_KPI_ORG_ACTUAL_RESULT

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ORG_ACTUAL_RESULT_List')
	Drop View dbo.vwB_KPI_ORG_ACTUAL_RESULT_List;
GO


Create View dbo.vwB_KPI_ORG_ACTUAL_RESULT_List
as
select *
  from vwB_KPI_ORG_ACTUAL_RESULT

GO

Grant Select on dbo.vwB_KPI_ORG_ACTUAL_RESULT_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Delete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ORG_ACTUAL_RESULT
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Undelete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB030201';
	
	-- BEGIN Oracle Exception
		update B_KPI_ORG_ACTUAL_RESULT
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ORG_ACTUAL_RESULT_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Update;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ACTUAL_RESULT_CODE                 nvarchar(50)
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @ORGANIZATION_ID                    uniqueidentifier
	, @ORGANIZATION_CODE                  nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @ASSIGN_BY                          uniqueidentifier
	, @ASSIGN_DATE                        datetime
	, @PERCENT_SYNC_TOTAL                 float
	, @PERCENT_FINAL_TOTAL                float
	, @PERCENT_MANUAL_TOTAL               float
	, @TOTAL_AMOUNT_01                    float
	, @TOTAL_AMOUNT_02                    float
	, @TOTAL_AMOUNT_03                    float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @FILE_ID                            uniqueidentifier
	, @LATEST_SYNC_DATE                   datetime
	, @APPROVE_ID                         uniqueidentifier
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @FLEX1                              nvarchar(0)
	, @FLEX2                              nvarchar(0)
	, @FLEX3                              nvarchar(0)
	, @FLEX4                              nvarchar(0)
	, @FLEX5                              nvarchar(0)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ORG_ACTUAL_RESULT where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ORG_ACTUAL_RESULT
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ACTUAL_RESULT_CODE                 
			, YEAR                               
			, MONTH_PERIOD                       
			, ORGANIZATION_ID                    
			, ORGANIZATION_CODE                  
			, VERSION_NUMBER                     
			, ALLOCATE_CODE                      
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, PERCENT_SYNC_TOTAL                 
			, PERCENT_FINAL_TOTAL                
			, PERCENT_MANUAL_TOTAL               
			, TOTAL_AMOUNT_01                    
			, TOTAL_AMOUNT_02                    
			, TOTAL_AMOUNT_03                    
			, DESCRIPTION                        
			, REMARK                             
			, FILE_ID                            
			, LATEST_SYNC_DATE                   
			, APPROVE_ID                         
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @ACTUAL_RESULT_CODE                 
			, @YEAR                               
			, @MONTH_PERIOD                       
			, @ORGANIZATION_ID                    
			, @ORGANIZATION_CODE                  
			, @VERSION_NUMBER                     
			, @ALLOCATE_CODE                      
			, @ASSIGN_BY                          
			, @ASSIGN_DATE                        
			, @PERCENT_SYNC_TOTAL                 
			, @PERCENT_FINAL_TOTAL                
			, @PERCENT_MANUAL_TOTAL               
			, @TOTAL_AMOUNT_01                    
			, @TOTAL_AMOUNT_02                    
			, @TOTAL_AMOUNT_03                    
			, @DESCRIPTION                        
			, @REMARK                             
			, @FILE_ID                            
			, @LATEST_SYNC_DATE                   
			, @APPROVE_ID                         
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ORG_ACTUAL_RESULT
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , ACTUAL_RESULT_CODE                   = @ACTUAL_RESULT_CODE                 
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD                       
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID                    
		     , ORGANIZATION_CODE                    = @ORGANIZATION_CODE                  
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , ASSIGN_BY                            = @ASSIGN_BY                          
		     , ASSIGN_DATE                          = @ASSIGN_DATE                        
		     , PERCENT_SYNC_TOTAL                   = @PERCENT_SYNC_TOTAL                 
		     , PERCENT_FINAL_TOTAL                  = @PERCENT_FINAL_TOTAL                
		     , PERCENT_MANUAL_TOTAL                 = @PERCENT_MANUAL_TOTAL               
		     , TOTAL_AMOUNT_01                      = @TOTAL_AMOUNT_01                    
		     , TOTAL_AMOUNT_02                      = @TOTAL_AMOUNT_02                    
		     , TOTAL_AMOUNT_03                      = @TOTAL_AMOUNT_03                    
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , FILE_ID                              = @FILE_ID                            
		     , LATEST_SYNC_DATE                     = @LATEST_SYNC_DATE                   
		     , APPROVE_ID                           = @APPROVE_ID                         
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 where ID                                   = @ID                                 ;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ACTUAL_RESULT_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB030201', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_MassDelete;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ORG_ACTUAL_RESULT_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ORG_ACTUAL_RESULT
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB030201', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB030201', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ORG_ACTUAL_RESULT
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ORG_ACTUAL_RESULT_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ORG_ACTUAL_RESULT_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ORG_ACTUAL_RESULT_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ORG_ACTUAL_RESULT_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ORG_ACTUAL_RESULT';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ORG_ACTUAL_RESULT';
	exec dbo.spSqlBuildAuditView    'B_KPI_ORG_ACTUAL_RESULT';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB030201.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB030201.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB030201.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB030201.DetailView'   , 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 0, 'KPIB030201.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 1, 'KPIB030201.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 2, 'KPIB030201.LBL_YEAR', 'YEAR', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 3, 'KPIB030201.LBL_MONTH_PERIOD', 'MONTH_PERIOD', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 4, 'KPIB030201.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 5, 'KPIB030201.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 6, 'KPIB030201.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 7, 'KPIB030201.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 8, 'KPIB030201.LBL_ASSIGN_BY', 'ASSIGN_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 9, 'KPIB030201.LBL_ASSIGN_DATE', 'ASSIGN_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 10, 'KPIB030201.LBL_PERCENT_SYNC_TOTAL', 'PERCENT_SYNC_TOTAL', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 11, 'KPIB030201.LBL_PERCENT_FINAL_TOTAL', 'PERCENT_FINAL_TOTAL', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 12, 'KPIB030201.LBL_PERCENT_MANUAL_TOTAL', 'PERCENT_MANUAL_TOTAL', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 13, 'KPIB030201.LBL_TOTAL_AMOUNT_01', 'TOTAL_AMOUNT_01', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 14, 'KPIB030201.LBL_TOTAL_AMOUNT_02', 'TOTAL_AMOUNT_02', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 15, 'KPIB030201.LBL_TOTAL_AMOUNT_03', 'TOTAL_AMOUNT_03', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 16, 'KPIB030201.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 17, 'KPIB030201.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 18, 'KPIB030201.LBL_FILE_ID', 'FILE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 19, 'KPIB030201.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 20, 'KPIB030201.LBL_APPROVE_ID', 'APPROVE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 21, 'KPIB030201.LBL_APPROVE_STATUS', 'APPROVE_STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 22, 'KPIB030201.LBL_APPROVED_BY', 'APPROVED_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 23, 'KPIB030201.LBL_APPROVED_DATE', 'APPROVED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 24, 'KPIB030201.LBL_FLEX1', 'FLEX1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 25, 'KPIB030201.LBL_FLEX2', 'FLEX2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 26, 'KPIB030201.LBL_FLEX3', 'FLEX3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 27, 'KPIB030201.LBL_FLEX4', 'FLEX4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 28, 'KPIB030201.LBL_FLEX5', 'FLEX5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 29, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 30, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 31, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB030201.DetailView', 32, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB030201.DetailView', 'KPIB030201';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB030201.EditView'  , 'KPIB030201';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB030201.PopupView' , 'KPIB030201';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB030201.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB030201.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB030201.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB030201.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201.EditView', 'KPIB030201'      , 'vwB_KPI_ORG_ACTUAL_RESULT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 1, 'KPIB030201.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 2, 'KPIB030201.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 3, 'KPIB030201.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView', 4, 'KPIB030201.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 5, 'KPIB030201.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 6, 'KPIB030201.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 7, 'KPIB030201.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView', 8, 'KPIB030201.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView', 9, 'KPIB030201.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 10, 'KPIB030201.LBL_PERCENT_SYNC_TOTAL', 'PERCENT_SYNC_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 11, 'KPIB030201.LBL_PERCENT_FINAL_TOTAL', 'PERCENT_FINAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 12, 'KPIB030201.LBL_PERCENT_MANUAL_TOTAL', 'PERCENT_MANUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 13, 'KPIB030201.LBL_TOTAL_AMOUNT_01', 'TOTAL_AMOUNT_01', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 14, 'KPIB030201.LBL_TOTAL_AMOUNT_02', 'TOTAL_AMOUNT_02', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 15, 'KPIB030201.LBL_TOTAL_AMOUNT_03', 'TOTAL_AMOUNT_03', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.EditView', 16, 'KPIB030201.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.EditView', 17, 'KPIB030201.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView', 18, 'KPIB030201.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView', 19, 'KPIB030201.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView', 20, 'KPIB030201.LBL_APPROVE_ID', 'APPROVE_ID', 0, 1, 'APPROVE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 21, 'KPIB030201.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView', 22, 'KPIB030201.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView', 23, 'KPIB030201.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 24, 'KPIB030201.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 25, 'KPIB030201.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 26, 'KPIB030201.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 27, 'KPIB030201.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView', 28, 'KPIB030201.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.EditView', 29, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.EditView', 30, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201.EditView.Inline', 'KPIB030201'      , 'vwB_KPI_ORG_ACTUAL_RESULT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 1, 'KPIB030201.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 2, 'KPIB030201.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 3, 'KPIB030201.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView.Inline', 4, 'KPIB030201.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 5, 'KPIB030201.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 6, 'KPIB030201.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 7, 'KPIB030201.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView.Inline', 8, 'KPIB030201.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView.Inline', 9, 'KPIB030201.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 10, 'KPIB030201.LBL_PERCENT_SYNC_TOTAL', 'PERCENT_SYNC_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 11, 'KPIB030201.LBL_PERCENT_FINAL_TOTAL', 'PERCENT_FINAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 12, 'KPIB030201.LBL_PERCENT_MANUAL_TOTAL', 'PERCENT_MANUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 13, 'KPIB030201.LBL_TOTAL_AMOUNT_01', 'TOTAL_AMOUNT_01', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 14, 'KPIB030201.LBL_TOTAL_AMOUNT_02', 'TOTAL_AMOUNT_02', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 15, 'KPIB030201.LBL_TOTAL_AMOUNT_03', 'TOTAL_AMOUNT_03', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.EditView.Inline', 16, 'KPIB030201.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.EditView.Inline', 17, 'KPIB030201.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView.Inline', 18, 'KPIB030201.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView.Inline', 19, 'KPIB030201.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView.Inline', 20, 'KPIB030201.LBL_APPROVE_ID', 'APPROVE_ID', 0, 1, 'APPROVE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 21, 'KPIB030201.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.EditView.Inline', 22, 'KPIB030201.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.EditView.Inline', 23, 'KPIB030201.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 24, 'KPIB030201.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 25, 'KPIB030201.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 26, 'KPIB030201.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 27, 'KPIB030201.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.EditView.Inline', 28, 'KPIB030201.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.EditView.Inline', 29, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.EditView.Inline', 30, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB030201.PopupView.Inline', 'KPIB030201'      , 'vwB_KPI_ORG_ACTUAL_RESULT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 1, 'KPIB030201.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 2, 'KPIB030201.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 3, 'KPIB030201.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.PopupView.Inline', 4, 'KPIB030201.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 5, 'KPIB030201.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 6, 'KPIB030201.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 7, 'KPIB030201.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.PopupView.Inline', 8, 'KPIB030201.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.PopupView.Inline', 9, 'KPIB030201.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 10, 'KPIB030201.LBL_PERCENT_SYNC_TOTAL', 'PERCENT_SYNC_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 11, 'KPIB030201.LBL_PERCENT_FINAL_TOTAL', 'PERCENT_FINAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 12, 'KPIB030201.LBL_PERCENT_MANUAL_TOTAL', 'PERCENT_MANUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 13, 'KPIB030201.LBL_TOTAL_AMOUNT_01', 'TOTAL_AMOUNT_01', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 14, 'KPIB030201.LBL_TOTAL_AMOUNT_02', 'TOTAL_AMOUNT_02', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 15, 'KPIB030201.LBL_TOTAL_AMOUNT_03', 'TOTAL_AMOUNT_03', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.PopupView.Inline', 16, 'KPIB030201.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB030201.PopupView.Inline', 17, 'KPIB030201.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.PopupView.Inline', 18, 'KPIB030201.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.PopupView.Inline', 19, 'KPIB030201.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.PopupView.Inline', 20, 'KPIB030201.LBL_APPROVE_ID', 'APPROVE_ID', 0, 1, 'APPROVE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 21, 'KPIB030201.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB030201.PopupView.Inline', 22, 'KPIB030201.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB030201.PopupView.Inline', 23, 'KPIB030201.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 24, 'KPIB030201.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 25, 'KPIB030201.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 26, 'KPIB030201.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 27, 'KPIB030201.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB030201.PopupView.Inline', 28, 'KPIB030201.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.PopupView.Inline', 29, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB030201.PopupView.Inline', 30, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201.SearchBasic'    , 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201.SearchBasic', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, 'KPIB030201', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201.SearchAdvanced' , 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201.SearchAdvanced', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, 'KPIB030201', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 1, 'KPIB030201.LBL_ACTUAL_RESULT_CODE', 'ACTUAL_RESULT_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 2, 'KPIB030201.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 3, 'KPIB030201.LBL_MONTH_PERIOD', 'MONTH_PERIOD', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB030201.SearchAdvanced', 4, 'KPIB030201.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 5, 'KPIB030201.LBL_ORGANIZATION_CODE', 'ORGANIZATION_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 6, 'KPIB030201.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 7, 'KPIB030201.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB030201.SearchAdvanced', 8, 'KPIB030201.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201.SearchAdvanced', 9, 'KPIB030201.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 10, 'KPIB030201.LBL_PERCENT_SYNC_TOTAL', 'PERCENT_SYNC_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 11, 'KPIB030201.LBL_PERCENT_FINAL_TOTAL', 'PERCENT_FINAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 12, 'KPIB030201.LBL_PERCENT_MANUAL_TOTAL', 'PERCENT_MANUAL_TOTAL', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 13, 'KPIB030201.LBL_TOTAL_AMOUNT_01', 'TOTAL_AMOUNT_01', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 14, 'KPIB030201.LBL_TOTAL_AMOUNT_02', 'TOTAL_AMOUNT_02', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 15, 'KPIB030201.LBL_TOTAL_AMOUNT_03', 'TOTAL_AMOUNT_03', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB030201.SearchAdvanced', 16, 'KPIB030201.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB030201.SearchAdvanced', 17, 'KPIB030201.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB030201.SearchAdvanced', 18, 'KPIB030201.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201.SearchAdvanced', 19, 'KPIB030201.LBL_LATEST_SYNC_DATE', 'LATEST_SYNC_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB030201.SearchAdvanced', 20, 'KPIB030201.LBL_APPROVE_ID', 'APPROVE_ID', 0, 1, 'APPROVE_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 21, 'KPIB030201.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB030201.SearchAdvanced', 22, 'KPIB030201.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB030201Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB030201.SearchAdvanced', 23, 'KPIB030201.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 24, 'KPIB030201.LBL_FLEX1', 'FLEX1', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 25, 'KPIB030201.LBL_FLEX2', 'FLEX2', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 26, 'KPIB030201.LBL_FLEX3', 'FLEX3', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 27, 'KPIB030201.LBL_FLEX4', 'FLEX4', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB030201.SearchAdvanced', 28, 'KPIB030201.LBL_FLEX5', 'FLEX5', 0, 1, 0, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB030201.SearchAdvanced' , 29, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB030201.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB030201.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB030201.SearchPopup'    , 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB030201.SearchPopup', 0, 'KPIB030201.LBL_NAME', 'NAME', 0, 1, 150, 35, 'KPIB030201', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201.Export', 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201.Export'         ,  1, 'KPIB030201.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201.ListView', 'KPIB030201'      , 'vwB_KPI_ORG_ACTUAL_RESULT_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201.ListView', 2, 'KPIB030201.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIB030201/view.aspx?id={0}', null, 'KPIB030201', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201.PopupView', 'KPIB030201'      , 'vwB_KPI_ORG_ACTUAL_RESULT_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201.PopupView', 1, 'KPIB030201.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectKPIB030201(''{0}'', ''{1}'');', null, 'KPIB030201', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB030201.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB030201.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB030201.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB030201.SearchDuplicates', 'KPIB030201', 'vwB_KPI_ORG_ACTUAL_RESULT_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB030201.SearchDuplicates'          , 1, 'KPIB030201.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB030201/view.aspx?id={0}', null, 'KPIB030201', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB030201', '.moduleList.KPIB030201', '~/KPIB030201/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ORG_ACTUAL_RESULT', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB030201';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB030201' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201', 'KPIB030201.LNK_NEW_B_KPI_ORG_ACTUAL_RESULT' , '~/KPIB030201/edit.aspx'   , 'CreateKPIB030201.gif', 1,  1, 'KPIB030201', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201', 'KPIB030201.LNK_B_KPI_ORG_ACTUAL_RESULT_LIST', '~/KPIB030201/default.aspx', 'KPIB030201.gif'      , 1,  2, 'KPIB030201', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201', '.LBL_IMPORT'              , '~/KPIB030201/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB030201', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB030201', '.LNK_ACTIVITY_STREAM'     , '~/KPIB030201/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB030201', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB030201', null, null, N'KPIB030201 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB030201', null, null, N'Create KPIB030201';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ORG_ACTUAL_RESULT_LIST'                          , N'en-US', N'KPIB030201', null, null, N'KPIB030201';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ORG_ACTUAL_RESULT'                           , N'en-US', N'KPIB030201', null, null, N'Create KPIB030201';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB030201', null, null, N'KPIB030201 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ORG_ACTUAL_RESULT_NOT_FOUND'                     , N'en-US', N'KPIB030201', null, null, N'KPIB030201 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ORG_ACTUAL_RESULT_CONFIRMATION'           , N'en-US', N'KPIB030201', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB030201', null, null, N'KPIB030201';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB030201', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB030201'                                          , N'en-US', null, N'moduleList', 100, N'KPIB030201';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'KPIB030201', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'KPIB030201', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ACTUAL_RESULT_CODE'                                , 'en-US', 'KPIB030201', null, null, 'MA PHAN GIAO';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ACTUAL_RESULT_CODE'                           , 'en-US', 'KPIB030201', null, null, 'MA PHAN GIAO';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_YEAR'                                              , 'en-US', 'KPIB030201', null, null, 'YEAR';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_YEAR'                                         , 'en-US', 'KPIB030201', null, null, 'YEAR';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_PERIOD'                                      , 'en-US', 'KPIB030201', null, null, 'Ky';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_PERIOD'                                 , 'en-US', 'KPIB030201', null, null, 'Ky';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_ID'                                   , 'en-US', 'KPIB030201', null, null, 'organization id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_ID'                              , 'en-US', 'KPIB030201', null, null, 'organization id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_CODE'                                 , 'en-US', 'KPIB030201', null, null, 'organization code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_CODE'                            , 'en-US', 'KPIB030201', null, null, 'organization code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'KPIB030201', null, null, 'version number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'KPIB030201', null, null, 'version number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_CODE'                                     , 'en-US', 'KPIB030201', null, null, 'allocate code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_CODE'                                , 'en-US', 'KPIB030201', null, null, 'allocate code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_BY'                                         , 'en-US', 'KPIB030201', null, null, 'assign by:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_BY'                                    , 'en-US', 'KPIB030201', null, null, 'assign by';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_DATE'                                       , 'en-US', 'KPIB030201', null, null, 'assign date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_DATE'                                  , 'en-US', 'KPIB030201', null, null, 'assign date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_SYNC_TOTAL'                                , 'en-US', 'KPIB030201', null, null, 'percent sync_total:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_SYNC_TOTAL'                           , 'en-US', 'KPIB030201', null, null, 'percent sync_total';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_FINAL_TOTAL'                               , 'en-US', 'KPIB030201', null, null, 'percent final_total:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_FINAL_TOTAL'                          , 'en-US', 'KPIB030201', null, null, 'percent final_total';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERCENT_MANUAL_TOTAL'                              , 'en-US', 'KPIB030201', null, null, 'percent manual_total:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERCENT_MANUAL_TOTAL'                         , 'en-US', 'KPIB030201', null, null, 'percent manual_total';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TOTAL_AMOUNT_01'                                   , 'en-US', 'KPIB030201', null, null, 'total amount_01:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TOTAL_AMOUNT_01'                              , 'en-US', 'KPIB030201', null, null, 'total amount_01';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TOTAL_AMOUNT_02'                                   , 'en-US', 'KPIB030201', null, null, 'total amount_02:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TOTAL_AMOUNT_02'                              , 'en-US', 'KPIB030201', null, null, 'total amount_02';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TOTAL_AMOUNT_03'                                   , 'en-US', 'KPIB030201', null, null, 'total amount_03:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TOTAL_AMOUNT_03'                              , 'en-US', 'KPIB030201', null, null, 'total amount_03';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIB030201', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIB030201', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIB030201', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIB030201', null, null, 'remark';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FILE_ID'                                           , 'en-US', 'KPIB030201', null, null, 'file id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FILE_ID'                                      , 'en-US', 'KPIB030201', null, null, 'file id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LATEST_SYNC_DATE'                                  , 'en-US', 'KPIB030201', null, null, 'latest sync_date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_LATEST_SYNC_DATE'                             , 'en-US', 'KPIB030201', null, null, 'latest sync_date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVE_ID'                                        , 'en-US', 'KPIB030201', null, null, 'approve id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVE_ID'                                   , 'en-US', 'KPIB030201', null, null, 'approve id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVE_STATUS'                                    , 'en-US', 'KPIB030201', null, null, 'approve status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVE_STATUS'                               , 'en-US', 'KPIB030201', null, null, 'approve status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_BY'                                       , 'en-US', 'KPIB030201', null, null, 'approved by:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_BY'                                  , 'en-US', 'KPIB030201', null, null, 'approved by';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_DATE'                                     , 'en-US', 'KPIB030201', null, null, 'approved date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_DATE'                                , 'en-US', 'KPIB030201', null, null, 'approved date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX1'                                             , 'en-US', 'KPIB030201', null, null, 'flex1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX1'                                        , 'en-US', 'KPIB030201', null, null, 'flex1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX2'                                             , 'en-US', 'KPIB030201', null, null, 'flex2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX2'                                        , 'en-US', 'KPIB030201', null, null, 'flex2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX3'                                             , 'en-US', 'KPIB030201', null, null, 'flex3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX3'                                        , 'en-US', 'KPIB030201', null, null, 'flex3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX4'                                             , 'en-US', 'KPIB030201', null, null, 'flex4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX4'                                        , 'en-US', 'KPIB030201', null, null, 'flex4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX5'                                             , 'en-US', 'KPIB030201', null, null, 'flex5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX5'                                        , 'en-US', 'KPIB030201', null, null, 'flex5';






