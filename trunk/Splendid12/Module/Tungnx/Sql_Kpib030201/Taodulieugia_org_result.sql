-----B_KPI_ORG_ACTUAL_RESULT

SELECT 'RCD-' + convert(nvarchar(36), NEWID()), convert(nvarchar(2), MONTH(getdate())) + '.' + convert(nvarchar(4), YEAR(getdate())) FROM B_KPI_ORG_ALLOCATES

SELECT	NEWID()
        ,ALC.[DELETED]
        ,ALC.[CREATED_BY]
        ,ALC.[DATE_ENTERED]
        ,ALC.[MODIFIED_USER_ID]
        ,ALC.[DATE_MODIFIED]
        ,ALC.[DATE_MODIFIED_UTC]
        ,ALC.[ASSIGNED_USER_ID]
        ,ALC.[TEAM_ID]
        ,ALC.[TEAM_SET_ID]
        ,ALC.[ALLOCATE_NAME]
        ,'RCD-' + convert(nvarchar(36), NEWID())
        ,YEAR(getdate())
        , convert(nvarchar(2), MONTH(getdate()))  + '.' + convert(nvarchar(4), YEAR(getdate()))
        ,MORG.[ID]
        ,ALC.[ORGANIZATION_CODE]
        ,ALC.[VERSION_NUMBER]
        ,ALC.[ALLOCATE_CODE]
        ,ALC.[ASSIGN_BY]
        ,ALC.[ASSIGN_DATE]
        ,[PERCENT_SYNC_TOTAL]
        ,[PERCENT_FINAL_TOTAL]
        ,[PERCENT_MANUAL_TOTAL]
        ,[TOTAL_AMOUNT_01]
        ,[TOTAL_AMOUNT_02]
        ,[TOTAL_AMOUNT_03]
        ,[DESCRIPTION]
        ,[REMARK]
        ,ALC.[FILE_ID]
        , getdate()
        ,[APPROVE_ID]
        ,[APPROVE_STATUS]
        ,[APPROVED_BY]
        ,[APPROVED_DATE]
        ,ALC.[FLEX1]
        ,ALC.[FLEX2]
        ,ALC.[FLEX3]
        ,ALC.[FLEX4]
        ,ALC.[FLEX5]
FROM [dbo].[B_KPI_ORG_ALLOCATES] ALC
INNER JOIN M_ORGANIZATION MORG ON MORG.[ORGANIZATION_CODE] = ALC.[ORGANIZATION_CODE]
WHERE 1=1 
	AND ALC.[ORGANIZATION_CODE] = ''
	AND ALC.[YEAR] = YEAR(getdate())
	AND ALC.[APPROVE_STATUS] = '65'
	AND ALC.[VERSION_NUMBER] = '0'

---------[B_KPI_ORG_ACTUAL_RESULT_DETAIL]
SELECT NEWID()
        ,[DELETED]
        ,[CREATED_BY]
        ,[DATE_ENTERED]
        ,[MODIFIED_USER_ID]
        ,[DATE_MODIFIED]
        ,[DATE_MODIFIED_UTC]
        ,[ASSIGNED_USER_ID]
        ,[TEAM_ID]
        ,[TEAM_SET_ID]
        ,[NAME]
        ,[ACTUAL_RESULT_CODE]
        ,YEAR(getdate())
        , convert(nvarchar(2), MONTH(getdate()))  + '.' + convert(nvarchar(4), YEAR(getdate()))
        ,[VERSION_NUMBER]
        ,[KPI_ID]
        ,[KPI_CODE]
        ,[KPI_NAME]
        ,[KPI_UNIT]
        ,[LEVEL_NUMBER]
        ,[RATIO]
        ,[MONTH_1]
        ,[SYNC_VALUE]
        ,[FINAL_VALUE]
        ,[PERCENT_SYNC_VALUE]
        ,[PERCENT_FINAL_VALUE]
        ,[PERCENT_MANUAL_VALUE]
        ,[DESCRIPTION]
        ,[REMARK]
        ,getdate()
        ,[FLEX1]
        ,[FLEX2]
        ,[FLEX3]
        ,[FLEX4]
        ,[FLEX5]
  FROM [dbo].[B_KPI_ORG_ALLOCATE_DETAILS]
  WHERE 1=1  
		AND [KPI_ALLOCATE_ID] = NEWID()
		AND [ALLOCATE_CODE] = ''



INSERT INTO [dbo].[B_KPI_ORG_ACTUAL_RESULT_DETAIL]
           ([ID]
           ,[DELETED]
           ,[CREATED_BY]
           ,[DATE_ENTERED]
           ,[MODIFIED_USER_ID]
           ,[DATE_MODIFIED]
           ,[DATE_MODIFIED_UTC]
           ,[ASSIGNED_USER_ID]
           ,[TEAM_ID]
           ,[TEAM_SET_ID]
           ,[NAME]
           ,[ACTUAL_RESULT_CODE]
           ,[YEAR]
           ,[MONTH_PERIOD]
           ,[VERSION_NUMBER]
           ,[KPI_CODE]
           ,[KPI_NAME]
           ,[KPI_UNIT]
           ,[LEVEL_NUMBER]
           ,[RATIO]
           ,[PLAN_VALUE]
           ,[SYNC_VALUE]
           ,[FINAL_VALUE]
           ,[PERCENT_SYNC_VALUE]
           ,[PERCENT_FINAL_VALUE]
           ,[PERCENT_MANUAL_VALUE]
           ,[DESCRIPTION]
           ,[REMARK]
           ,[LATEST_SYNC_DATE]
           ,[FLEX1]
           ,[FLEX2]
           ,[FLEX3]
           ,[FLEX4]
           ,[FLEX5])

     SELECT NEWID()
           ,a.[DELETED]
           ,a.[CREATED_BY]
           ,a.[DATE_ENTERED]
           ,a.[MODIFIED_USER_ID]
           ,a.[DATE_MODIFIED]
           ,a.[DATE_MODIFIED_UTC]
           ,a.[ASSIGNED_USER_ID]
           ,a.[TEAM_ID]
           ,a.[TEAM_SET_ID]
           ,''
           ,'qqqq124'
           ,2018
           ,'06'
           ,'0'
           ,b.[KPI_CODE]
           ,A.[KPI_NAME]
           ,A.[UNIT]
           ,b.[LEVEL_NUMBER]
           ,A.[RATIO]
           ,20000 --[PLAN_VALUE]
           ,1000 --[SYNC_VALUE]
           ,1000 --[FINAL_VALUE]
           ,0--[PERCENT_SYNC_VALUE]
           ,0--[PERCENT_FINAL_VALUE]
           ,0--[PERCENT_MANUAL_VALUE]
           ,''--[DESCRIPTION]
           ,''--[REMARK]
           ,getdate() --[LATEST_SYNC_DATE]
           ,''--[FLEX1]
           ,''--[FLEX2]
           ,''--[FLEX3]
           ,''--[FLEX4]
           ,''--[FLEX5]
  FROM [dbo].[M_GROUP_KPI_DETAILS] A
  LEFT JOIN M_KPIS B ON A.[KPI_ID] = B.[ID]
  where A.[GROUP_KPI_ID] = '070fd4fd-2633-4a9a-8e7b-777f0af509f1'
GO


