USE [SplendidCRM]
GO

/****** Object:  Table [dbo].[M_GROUP_KPIS]    Script Date: 6/12/2018 9:21:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[M_ORGANIZATION](
	[ID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[DELETED] [bit] NOT NULL DEFAULT ((0)),
	[CREATED_BY] [uniqueidentifier] NULL,
	[DATE_ENTERED] [datetime] NOT NULL DEFAULT (getdate()),
	[MODIFIED_USER_ID] [uniqueidentifier] NULL,
	[DATE_MODIFIED] [datetime] NOT NULL DEFAULT (getdate()),
	[DATE_MODIFIED_UTC] [datetime] NULL DEFAULT (getutcdate()),
	[ASSIGNED_USER_ID] [uniqueidentifier] NULL,
	[TEAM_ID] [uniqueidentifier] NULL,
	[TEAM_SET_ID] [uniqueidentifier] NULL,
	[ORGANIZATION_CODE] [nvarchar](50) NOT NULL,
	[ORGANIZATION_NAME] [nvarchar](200) NOT NULL,
	[PARENT_ID] [int] NULL,
	[LEVEL_NUMBER] integer NULL,	
	[BUSSINES_ID] [nvarchar](50) NULL,	
	[DESCRIPTION] [nvarchar](max) NULL,
	[REMARK] [nvarchar](max) NULL,
	[STATUS] [nvarchar](5) NULL,
 CONSTRAINT [PK_M_ORGANIZATION] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


