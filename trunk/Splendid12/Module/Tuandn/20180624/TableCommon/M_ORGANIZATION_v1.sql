USE [SplendidCRM1]
GO

/****** Object:  Table [dbo].[M_ORGANIZATION]    Script Date: 6/23/2018 10:03:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[M_ORGANIZATION](
	[ID] uniqueidentifier NOT NULL,
	[ORGANIZATION_CODE] [nvarchar](50) NULL,
	[ORGANIZATION_NAME] [nvarchar](200) NULL,
	[PARENT_ID] [int] NULL,
	[LEVEL_NUMBER] [tinyint] NULL,
	[DESCRIPTION] [nvarchar](500) NULL,
	[BUSSINES_ID] [int] NULL,
	[STATUS] [nvarchar](5) NULL,
 CONSTRAINT [PK_M_ORGANIZATION] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[M_ORGANIZATION] ADD  CONSTRAINT [DF_M_ORGANIZATION_STATUS]  DEFAULT ((1)) FOR [STATUS]
GO


