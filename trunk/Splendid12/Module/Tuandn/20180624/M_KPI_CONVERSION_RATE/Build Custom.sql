
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_KPI_CONVERSION_RATE';
	Create Table dbo.M_KPI_CONVERSION_RATE
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_M_KPI_CONVERSION_RATE primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, TYPE                               nvarchar(10) null
		, FROM_CODE                          nvarchar(50) null
		, FROM_NAME                          nvarchar(200) null
		, TO_CODE                            nvarchar(50) null
		, TO_NAME                            nvarchar(200) null
		, CONVERSION_RATE                    float null
		, CONVERSION_RATE1                   float null
		, CONVERSION_RATE2                   float null
		, STATUS                             nvarchar(5) null
		, DESCRIPTION                        nvarchar(max) null

		)

	create index IDX_M_KPI_CONVERSION_RATE_ASSIGNED_USER_ID on dbo.M_KPI_CONVERSION_RATE (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_KPI_CONVERSION_RATE_TEAM_ID          on dbo.M_KPI_CONVERSION_RATE (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_KPI_CONVERSION_RATE_TEAM_SET_ID      on dbo.M_KPI_CONVERSION_RATE (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_KPI_CONVERSION_RATE_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_KPI_CONVERSION_RATE_CSTM';
	Create Table dbo.M_KPI_CONVERSION_RATE_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_M_KPI_CONVERSION_RATE_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add ASSIGNED_USER_ID uniqueidentifier null';
	alter table M_KPI_CONVERSION_RATE add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_M_KPI_CONVERSION_RATE_ASSIGNED_USER_ID on dbo.M_KPI_CONVERSION_RATE (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add TEAM_ID uniqueidentifier null';
	alter table M_KPI_CONVERSION_RATE add TEAM_ID uniqueidentifier null;
	create index IDX_M_KPI_CONVERSION_RATE_TEAM_ID          on dbo.M_KPI_CONVERSION_RATE (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add TEAM_SET_ID uniqueidentifier null';
	alter table M_KPI_CONVERSION_RATE add TEAM_SET_ID uniqueidentifier null;
	create index IDX_M_KPI_CONVERSION_RATE_TEAM_SET_ID      on dbo.M_KPI_CONVERSION_RATE (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'TYPE') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add TYPE nvarchar(10) null';
	alter table M_KPI_CONVERSION_RATE add TYPE nvarchar(10) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'FROM_CODE') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add FROM_CODE nvarchar(50) null';
	alter table M_KPI_CONVERSION_RATE add FROM_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'FROM_NAME') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add FROM_NAME nvarchar(200) null';
	alter table M_KPI_CONVERSION_RATE add FROM_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'TO_CODE') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add TO_CODE nvarchar(50) null';
	alter table M_KPI_CONVERSION_RATE add TO_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'TO_NAME') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add TO_NAME nvarchar(200) null';
	alter table M_KPI_CONVERSION_RATE add TO_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'CONVERSION_RATE') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE float null';
	alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'CONVERSION_RATE1') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE1 float null';
	alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'CONVERSION_RATE2') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE2 float null';
	alter table M_KPI_CONVERSION_RATE add CONVERSION_RATE2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'STATUS') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add STATUS nvarchar(5) null';
	alter table M_KPI_CONVERSION_RATE add STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_KPI_CONVERSION_RATE' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table M_KPI_CONVERSION_RATE add DESCRIPTION nvarchar(max) null';
	alter table M_KPI_CONVERSION_RATE add DESCRIPTION nvarchar(max) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_KPI_CONVERSION_RATE')
	Drop View dbo.vwM_KPI_CONVERSION_RATE;
GO


Create View dbo.vwM_KPI_CONVERSION_RATE
as
select M_KPI_CONVERSION_RATE.ID
     , M_KPI_CONVERSION_RATE.TYPE
     , M_KPI_CONVERSION_RATE.FROM_CODE
     , M_KPI_CONVERSION_RATE.FROM_NAME
     , M_KPI_CONVERSION_RATE.TO_CODE
     , M_KPI_CONVERSION_RATE.TO_NAME
     , M_KPI_CONVERSION_RATE.CONVERSION_RATE
     , M_KPI_CONVERSION_RATE.CONVERSION_RATE1
     , M_KPI_CONVERSION_RATE.CONVERSION_RATE2
     , M_KPI_CONVERSION_RATE.STATUS
     , M_KPI_CONVERSION_RATE.DESCRIPTION
     , M_KPI_CONVERSION_RATE.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , M_KPI_CONVERSION_RATE.DATE_ENTERED
     , M_KPI_CONVERSION_RATE.DATE_MODIFIED
     , M_KPI_CONVERSION_RATE.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , M_KPI_CONVERSION_RATE.CREATED_BY      as CREATED_BY_ID
     , M_KPI_CONVERSION_RATE.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , M_KPI_CONVERSION_RATE_CSTM.*
  from            M_KPI_CONVERSION_RATE
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = M_KPI_CONVERSION_RATE.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = M_KPI_CONVERSION_RATE.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = M_KPI_CONVERSION_RATE.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = M_KPI_CONVERSION_RATE.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = M_KPI_CONVERSION_RATE.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = M_KPI_CONVERSION_RATE.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = M_KPI_CONVERSION_RATE.MODIFIED_USER_ID
  left outer join M_KPI_CONVERSION_RATE_CSTM
               on M_KPI_CONVERSION_RATE_CSTM.ID_C     = M_KPI_CONVERSION_RATE.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = M_KPI_CONVERSION_RATE.ID
 where M_KPI_CONVERSION_RATE.DELETED = 0

GO

Grant Select on dbo.vwM_KPI_CONVERSION_RATE to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_KPI_CONVERSION_RATE_Edit')
	Drop View dbo.vwM_KPI_CONVERSION_RATE_Edit;
GO


Create View dbo.vwM_KPI_CONVERSION_RATE_Edit
as
select *
  from vwM_KPI_CONVERSION_RATE

GO

Grant Select on dbo.vwM_KPI_CONVERSION_RATE_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_KPI_CONVERSION_RATE_List')
	Drop View dbo.vwM_KPI_CONVERSION_RATE_List;
GO


Create View dbo.vwM_KPI_CONVERSION_RATE_List
as
select *
  from vwM_KPI_CONVERSION_RATE

GO

Grant Select on dbo.vwM_KPI_CONVERSION_RATE_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_Delete;
GO


Create Procedure dbo.spM_KPI_CONVERSION_RATE_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update M_KPI_CONVERSION_RATE
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spM_KPI_CONVERSION_RATE_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_Undelete;
GO


Create Procedure dbo.spM_KPI_CONVERSION_RATE_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIM0102';
	
	-- BEGIN Oracle Exception
		update M_KPI_CONVERSION_RATE
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from M_KPI_CONVERSION_RATE_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spM_KPI_CONVERSION_RATE_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_Update;
GO


Create Procedure dbo.spM_KPI_CONVERSION_RATE_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @TYPE                               nvarchar(10)
	, @FROM_CODE                          nvarchar(50)
	, @FROM_NAME                          nvarchar(200)
	, @TO_CODE                            nvarchar(50)
	, @TO_NAME                            nvarchar(200)
	, @CONVERSION_RATE                    float
	, @CONVERSION_RATE1                   float
	, @CONVERSION_RATE2                   float
	, @STATUS                             nvarchar(5)
	, @DESCRIPTION                        nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from M_KPI_CONVERSION_RATE where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into M_KPI_CONVERSION_RATE
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, TYPE                               
			, FROM_CODE                          
			, FROM_NAME                          
			, TO_CODE                            
			, TO_NAME                            
			, CONVERSION_RATE                    
			, CONVERSION_RATE1                   
			, CONVERSION_RATE2                   
			, STATUS                             
			, DESCRIPTION                        

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @TYPE                               
			, @FROM_CODE                          
			, @FROM_NAME                          
			, @TO_CODE                            
			, @TO_NAME                            
			, @CONVERSION_RATE                    
			, @CONVERSION_RATE1                   
			, @CONVERSION_RATE2                   
			, @STATUS                             
			, @DESCRIPTION                        

			);
	end else begin
		update M_KPI_CONVERSION_RATE
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , TYPE                                 = @TYPE                               
		     , FROM_CODE                            = @FROM_CODE                          
		     , FROM_NAME                            = @FROM_NAME                          
		     , TO_CODE                              = @TO_CODE                            
		     , TO_NAME                              = @TO_NAME                            
		     , CONVERSION_RATE                      = @CONVERSION_RATE                    
		     , CONVERSION_RATE1                     = @CONVERSION_RATE1                   
		     , CONVERSION_RATE2                     = @CONVERSION_RATE2                   
		     , STATUS                               = @STATUS                             
		     , DESCRIPTION                          = @DESCRIPTION                        

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from M_KPI_CONVERSION_RATE_CSTM where ID_C = @ID) begin -- then
			insert into M_KPI_CONVERSION_RATE_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0102', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spM_KPI_CONVERSION_RATE_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_MassDelete;
GO


Create Procedure dbo.spM_KPI_CONVERSION_RATE_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spM_KPI_CONVERSION_RATE_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spM_KPI_CONVERSION_RATE_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_MassUpdate;
GO


Create Procedure dbo.spM_KPI_CONVERSION_RATE_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from M_KPI_CONVERSION_RATE
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIM0102', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0102', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update M_KPI_CONVERSION_RATE
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spM_KPI_CONVERSION_RATE_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_KPI_CONVERSION_RATE_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_KPI_CONVERSION_RATE_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spM_KPI_CONVERSION_RATE_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spM_KPI_CONVERSION_RATE_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spM_KPI_CONVERSION_RATE_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_KPI_CONVERSION_RATE';
	exec dbo.spSqlBuildAuditTrigger 'M_KPI_CONVERSION_RATE';
	exec dbo.spSqlBuildAuditView    'M_KPI_CONVERSION_RATE';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0102.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0102.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIM0102.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIM0102.DetailView'   , 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 0, 'KPIM0102.LBL_TYPE', 'TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 1, 'KPIM0102.LBL_FROM_CODE', 'FROM_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 2, 'KPIM0102.LBL_FROM_NAME', 'FROM_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 3, 'KPIM0102.LBL_TO_CODE', 'TO_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 4, 'KPIM0102.LBL_TO_NAME', 'TO_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 5, 'KPIM0102.LBL_CONVERSION_RATE', 'CONVERSION_RATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 6, 'KPIM0102.LBL_CONVERSION_RATE1', 'CONVERSION_RATE1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 7, 'KPIM0102.LBL_CONVERSION_RATE2', 'CONVERSION_RATE2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 8, 'KPIM0102.LBL_STATUS', 'STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 9, 'KPIM0102.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 10, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 11, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 12, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0102.DetailView', 13, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIM0102.DetailView', 'KPIM0102';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIM0102.EditView'  , 'KPIM0102';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIM0102.PopupView' , 'KPIM0102';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0102.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIM0102.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0102.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIM0102.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0102.EditView', 'KPIM0102'      , 'vwM_KPI_CONVERSION_RATE_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 1, 'KPIM0102.LBL_FROM_CODE', 'FROM_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 2, 'KPIM0102.LBL_FROM_NAME', 'FROM_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 3, 'KPIM0102.LBL_TO_CODE', 'TO_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 4, 'KPIM0102.LBL_TO_NAME', 'TO_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 5, 'KPIM0102.LBL_CONVERSION_RATE', 'CONVERSION_RATE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 6, 'KPIM0102.LBL_CONVERSION_RATE1', 'CONVERSION_RATE1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 7, 'KPIM0102.LBL_CONVERSION_RATE2', 'CONVERSION_RATE2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView', 8, 'KPIM0102.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0102.EditView', 9, 'KPIM0102.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.EditView', 10, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.EditView', 11, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0102.EditView.Inline', 'KPIM0102'      , 'vwM_KPI_CONVERSION_RATE_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 1, 'KPIM0102.LBL_FROM_CODE', 'FROM_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 2, 'KPIM0102.LBL_FROM_NAME', 'FROM_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 3, 'KPIM0102.LBL_TO_CODE', 'TO_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 4, 'KPIM0102.LBL_TO_NAME', 'TO_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 5, 'KPIM0102.LBL_CONVERSION_RATE', 'CONVERSION_RATE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 6, 'KPIM0102.LBL_CONVERSION_RATE1', 'CONVERSION_RATE1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 7, 'KPIM0102.LBL_CONVERSION_RATE2', 'CONVERSION_RATE2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.EditView.Inline', 8, 'KPIM0102.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0102.EditView.Inline', 9, 'KPIM0102.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.EditView.Inline', 10, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.EditView.Inline', 11, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0102.PopupView.Inline', 'KPIM0102'      , 'vwM_KPI_CONVERSION_RATE_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 1, 'KPIM0102.LBL_FROM_CODE', 'FROM_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 2, 'KPIM0102.LBL_FROM_NAME', 'FROM_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 3, 'KPIM0102.LBL_TO_CODE', 'TO_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 4, 'KPIM0102.LBL_TO_NAME', 'TO_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 5, 'KPIM0102.LBL_CONVERSION_RATE', 'CONVERSION_RATE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 6, 'KPIM0102.LBL_CONVERSION_RATE1', 'CONVERSION_RATE1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 7, 'KPIM0102.LBL_CONVERSION_RATE2', 'CONVERSION_RATE2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0102.PopupView.Inline', 8, 'KPIM0102.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0102.PopupView.Inline', 9, 'KPIM0102.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.PopupView.Inline', 10, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0102.PopupView.Inline', 11, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0102.SearchBasic'    , 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchBasic', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIM0102.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0102.SearchAdvanced' , 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 1, 'KPIM0102.LBL_FROM_CODE', 'FROM_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 2, 'KPIM0102.LBL_FROM_NAME', 'FROM_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 3, 'KPIM0102.LBL_TO_CODE', 'TO_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 4, 'KPIM0102.LBL_TO_NAME', 'TO_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 5, 'KPIM0102.LBL_CONVERSION_RATE', 'CONVERSION_RATE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 6, 'KPIM0102.LBL_CONVERSION_RATE1', 'CONVERSION_RATE1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 7, 'KPIM0102.LBL_CONVERSION_RATE2', 'CONVERSION_RATE2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchAdvanced', 8, 'KPIM0102.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIM0102.SearchAdvanced', 9, 'KPIM0102.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0102.SearchAdvanced' , 10, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0102.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0102.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0102.SearchPopup'    , 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0102.SearchPopup', 0, 'KPIM0102.LBL_TYPE', 'TYPE', 0, 1, 10, 35, null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0102.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0102.Export', 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0102.Export'         ,  1, 'KPIM0102.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0102.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0102.ListView', 'KPIM0102'      , 'vwM_KPI_CONVERSION_RATE_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0102.ListView', 2, 'KPIM0102.LBL_LIST_TYPE', 'TYPE', 'TYPE', '35%', 'listViewTdLinkS1', 'ID', '~/KPIM0102/view.aspx?id={0}', null, 'KPIM0102', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0102.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0102.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0102.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0102.PopupView', 'KPIM0102'      , 'vwM_KPI_CONVERSION_RATE_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0102.PopupView', 1, 'KPIM0102.LBL_LIST_TYPE', 'TYPE', 'TYPE', '45%', 'listViewTdLinkS1', 'ID TYPE', 'SelectKPIM0102(''{0}'', ''{1}'');', null, 'KPIM0102', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0102.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0102.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0102.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0102.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0102.SearchDuplicates', 'KPIM0102', 'vwM_KPI_CONVERSION_RATE_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0102.SearchDuplicates'          , 1, 'KPIM0102.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIM0102/view.aspx?id={0}', null, 'KPIM0102', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIM0102', '.moduleList.KPIM0102', '~/KPIM0102/', 1, 1, 100, 0, 1, 1, 1, 0, 'M_KPI_CONVERSION_RATE', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIM0102';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIM0102' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0102', 'KPIM0102.LNK_NEW_M_KPI_CONVERSION_RATE' , '~/KPIM0102/edit.aspx'   , 'CreateKPIM0102.gif', 1,  1, 'KPIM0102', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0102', 'KPIM0102.LNK_M_KPI_CONVERSION_RATE_LIST', '~/KPIM0102/default.aspx', 'KPIM0102.gif'      , 1,  2, 'KPIM0102', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0102', '.LBL_IMPORT'              , '~/KPIM0102/import.aspx' , 'Import.gif'        , 1,  3, 'KPIM0102', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0102', '.LNK_ACTIVITY_STREAM'     , '~/KPIM0102/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIM0102', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIM0102', null, null, N'KPIM0102 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIM0102', null, null, N'Create KPIM0102';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_M_KPI_CONVERSION_RATE_LIST'                          , N'en-US', N'KPIM0102', null, null, N'KPIM0102';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_M_KPI_CONVERSION_RATE'                           , N'en-US', N'KPIM0102', null, null, N'Create KPIM0102';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIM0102', null, null, N'KPIM0102 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_M_KPI_CONVERSION_RATE_NOT_FOUND'                     , N'en-US', N'KPIM0102', null, null, N'KPIM0102 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_M_KPI_CONVERSION_RATE_CONFIRMATION'           , N'en-US', N'KPIM0102', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIM0102', null, null, N'KPIM0102';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIM0102', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIM0102'                                          , N'en-US', null, N'moduleList', 100, N'KPIM0102';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TYPE'                                              , 'en-US', 'KPIM0102', null, null, 'type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TYPE'                                         , 'en-US', 'KPIM0102', null, null, 'type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FROM_CODE'                                         , 'en-US', 'KPIM0102', null, null, 'from code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FROM_CODE'                                    , 'en-US', 'KPIM0102', null, null, 'from code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FROM_NAME'                                         , 'en-US', 'KPIM0102', null, null, 'from name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FROM_NAME'                                    , 'en-US', 'KPIM0102', null, null, 'from name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TO_CODE'                                           , 'en-US', 'KPIM0102', null, null, 'to code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TO_CODE'                                      , 'en-US', 'KPIM0102', null, null, 'to code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TO_NAME'                                           , 'en-US', 'KPIM0102', null, null, 'to name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TO_NAME'                                      , 'en-US', 'KPIM0102', null, null, 'to name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_CONVERSION_RATE'                                   , 'en-US', 'KPIM0102', null, null, 'conversion rate:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_CONVERSION_RATE'                              , 'en-US', 'KPIM0102', null, null, 'conversion rate';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_CONVERSION_RATE1'                                  , 'en-US', 'KPIM0102', null, null, 'conversion rate1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_CONVERSION_RATE1'                             , 'en-US', 'KPIM0102', null, null, 'conversion rate1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_CONVERSION_RATE2'                                  , 'en-US', 'KPIM0102', null, null, 'conversion rate2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_CONVERSION_RATE2'                             , 'en-US', 'KPIM0102', null, null, 'conversion rate2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STATUS'                                            , 'en-US', 'KPIM0102', null, null, 'status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STATUS'                                       , 'en-US', 'KPIM0102', null, null, 'status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIM0102', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIM0102', null, null, 'description';






