
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_USER_EXTEND' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_USER_EXTEND';
	Create Table dbo.B_USER_EXTEND
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_USER_EXTEND primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, NAME                               nvarchar(150) not null
		, USER_ID                            uniqueidentifier null
		, ID_SOURCE                          int null
		, USER_CODE                          nvarchar(50) null
		, BIRTH_DATE                         datetime null
		, SEX                                nvarchar(20) null
		, ORGANIZATION_ID                    uniqueidentifier null
		, TITLE_ID                           uniqueidentifier null
		, TITLE_ADD_ID                       uniqueidentifier null
		, RETIRED_DATE                       datetime null
		, CONTRACT_TYPE                      nvarchar(50) null
		, POSITION_ID                        uniqueidentifier null
		, MANAGER_NAME                       nvarchar(50) null
		, DATE_START_WORK                    datetime null
		, DATE_START_INTERN                  datetime null
		, DATE_START_PROBATION               datetime null
		, DATE_ACTING_PROMOTED               datetime null
		, OFFICAL_PROMOTED_DATE              datetime null
		, CURRENT_POSITION_WORK_TIME         datetime null
		, SENIORITY                          nvarchar(20) null
		, SALARY_STATUS                      nvarchar(10) null
		, POS_NAME_CENTER                    nvarchar(50) null
		, AREA                               nvarchar(50) null
		, PREVIOUS_EMPLOY_HISTORY            nvarchar(50) null
		, EMPLOYMENT_HISTORY                 nvarchar(50) null
		, BUSINESS_TYPE                      nvarchar(30) null
		, USER_LDAP                          nvarchar(40) null
		, FLEX1                              nvarchar(150) null
		, FLEX2                              nvarchar(150) null
		, FLEX3                              nvarchar(150) null
		, FLEX4                              nvarchar(150) null
		, FLEX5                              nvarchar(150) null
		, FLEX6                              nvarchar(150) null
		, FLEX7                              nvarchar(150) null
		, FLEX8                              nvarchar(150) null
		, FLEX9                              nvarchar(150) null
		, FLEX10                             nvarchar(150) null

		)

	create index IDX_B_USER_EXTEND_ASSIGNED_USER_ID on dbo.B_USER_EXTEND (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_USER_EXTEND_TEAM_ID          on dbo.B_USER_EXTEND (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_USER_EXTEND_TEAM_SET_ID      on dbo.B_USER_EXTEND (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_USER_EXTEND_NAME  on dbo.B_USER_EXTEND (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_USER_EXTEND_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_USER_EXTEND_CSTM';
	Create Table dbo.B_USER_EXTEND_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_USER_EXTEND_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_USER_EXTEND add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_USER_EXTEND add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_USER_EXTEND_ASSIGNED_USER_ID on dbo.B_USER_EXTEND (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_USER_EXTEND add TEAM_ID uniqueidentifier null';
	alter table B_USER_EXTEND add TEAM_ID uniqueidentifier null;
	create index IDX_B_USER_EXTEND_TEAM_ID          on dbo.B_USER_EXTEND (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_USER_EXTEND add TEAM_SET_ID uniqueidentifier null';
	alter table B_USER_EXTEND add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_USER_EXTEND_TEAM_SET_ID      on dbo.B_USER_EXTEND (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table B_USER_EXTEND add NAME nvarchar(150) null';
	alter table B_USER_EXTEND add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'USER_ID') begin -- then
	print 'alter table B_USER_EXTEND add USER_ID uniqueidentifier null';
	alter table B_USER_EXTEND add USER_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'ID_SOURCE') begin -- then
	print 'alter table B_USER_EXTEND add ID_SOURCE int null';
	alter table B_USER_EXTEND add ID_SOURCE int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'USER_CODE') begin -- then
	print 'alter table B_USER_EXTEND add USER_CODE nvarchar(50) null';
	alter table B_USER_EXTEND add USER_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'BIRTH_DATE') begin -- then
	print 'alter table B_USER_EXTEND add BIRTH_DATE datetime null';
	alter table B_USER_EXTEND add BIRTH_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'SEX') begin -- then
	print 'alter table B_USER_EXTEND add SEX nvarchar(20) null';
	alter table B_USER_EXTEND add SEX nvarchar(20) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'ORGANIZATION_ID') begin -- then
	print 'alter table B_USER_EXTEND add ORGANIZATION_ID uniqueidentifier null';
	alter table B_USER_EXTEND add ORGANIZATION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'TITLE_ID') begin -- then
	print 'alter table B_USER_EXTEND add TITLE_ID uniqueidentifier null';
	alter table B_USER_EXTEND add TITLE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'TITLE_ADD_ID') begin -- then
	print 'alter table B_USER_EXTEND add TITLE_ADD_ID uniqueidentifier null';
	alter table B_USER_EXTEND add TITLE_ADD_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'RETIRED_DATE') begin -- then
	print 'alter table B_USER_EXTEND add RETIRED_DATE datetime null';
	alter table B_USER_EXTEND add RETIRED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'CONTRACT_TYPE') begin -- then
	print 'alter table B_USER_EXTEND add CONTRACT_TYPE nvarchar(50) null';
	alter table B_USER_EXTEND add CONTRACT_TYPE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'POSITION_ID') begin -- then
	print 'alter table B_USER_EXTEND add POSITION_ID uniqueidentifier null';
	alter table B_USER_EXTEND add POSITION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'MANAGER_NAME') begin -- then
	print 'alter table B_USER_EXTEND add MANAGER_NAME nvarchar(50) null';
	alter table B_USER_EXTEND add MANAGER_NAME nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'DATE_START_WORK') begin -- then
	print 'alter table B_USER_EXTEND add DATE_START_WORK datetime null';
	alter table B_USER_EXTEND add DATE_START_WORK datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'DATE_START_INTERN') begin -- then
	print 'alter table B_USER_EXTEND add DATE_START_INTERN datetime null';
	alter table B_USER_EXTEND add DATE_START_INTERN datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'DATE_START_PROBATION ') begin -- then
	print 'alter table B_USER_EXTEND add DATE_START_PROBATION  datetime null';
	alter table B_USER_EXTEND add DATE_START_PROBATION  datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'DATE_ACTING_PROMOTED') begin -- then
	print 'alter table B_USER_EXTEND add DATE_ACTING_PROMOTED datetime null';
	alter table B_USER_EXTEND add DATE_ACTING_PROMOTED datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'OFFICAL_PROMOTED_DATE') begin -- then
	print 'alter table B_USER_EXTEND add OFFICAL_PROMOTED_DATE datetime null';
	alter table B_USER_EXTEND add OFFICAL_PROMOTED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'CURRENT_POSITION_WORK_TIME') begin -- then
	print 'alter table B_USER_EXTEND add CURRENT_POSITION_WORK_TIME datetime null';
	alter table B_USER_EXTEND add CURRENT_POSITION_WORK_TIME datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'SENIORITY') begin -- then
	print 'alter table B_USER_EXTEND add SENIORITY nvarchar(20) null';
	alter table B_USER_EXTEND add SENIORITY nvarchar(20) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'SALARY_STATUS') begin -- then
	print 'alter table B_USER_EXTEND add SALARY_STATUS nvarchar(10) null';
	alter table B_USER_EXTEND add SALARY_STATUS nvarchar(10) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'POS_NAME_CENTER') begin -- then
	print 'alter table B_USER_EXTEND add POS_NAME_CENTER nvarchar(50) null';
	alter table B_USER_EXTEND add POS_NAME_CENTER nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'AREA') begin -- then
	print 'alter table B_USER_EXTEND add AREA nvarchar(50) null';
	alter table B_USER_EXTEND add AREA nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'PREVIOUS_EMPLOY_HISTORY') begin -- then
	print 'alter table B_USER_EXTEND add PREVIOUS_EMPLOY_HISTORY nvarchar(50) null';
	alter table B_USER_EXTEND add PREVIOUS_EMPLOY_HISTORY nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'EMPLOYMENT_HISTORY') begin -- then
	print 'alter table B_USER_EXTEND add EMPLOYMENT_HISTORY nvarchar(50) null';
	alter table B_USER_EXTEND add EMPLOYMENT_HISTORY nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'BUSINESS_TYPE') begin -- then
	print 'alter table B_USER_EXTEND add BUSINESS_TYPE nvarchar(30) null';
	alter table B_USER_EXTEND add BUSINESS_TYPE nvarchar(30) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'USER_LDAP') begin -- then
	print 'alter table B_USER_EXTEND add USER_LDAP nvarchar(40) null';
	alter table B_USER_EXTEND add USER_LDAP nvarchar(40) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX1') begin -- then
	print 'alter table B_USER_EXTEND add FLEX1 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX1 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX2') begin -- then
	print 'alter table B_USER_EXTEND add FLEX2 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX2 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX3') begin -- then
	print 'alter table B_USER_EXTEND add FLEX3 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX3 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX4') begin -- then
	print 'alter table B_USER_EXTEND add FLEX4 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX4 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX5') begin -- then
	print 'alter table B_USER_EXTEND add FLEX5 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX5 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX6') begin -- then
	print 'alter table B_USER_EXTEND add FLEX6 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX6 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX7') begin -- then
	print 'alter table B_USER_EXTEND add FLEX7 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX7 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX8') begin -- then
	print 'alter table B_USER_EXTEND add FLEX8 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX8 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX9') begin -- then
	print 'alter table B_USER_EXTEND add FLEX9 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX9 nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_USER_EXTEND' and COLUMN_NAME = 'FLEX10') begin -- then
	print 'alter table B_USER_EXTEND add FLEX10 nvarchar(150) null';
	alter table B_USER_EXTEND add FLEX10 nvarchar(150) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_USER_EXTEND')
	Drop View dbo.vwB_USER_EXTEND;
GO


Create View dbo.vwB_USER_EXTEND
as
select B_USER_EXTEND.ID
     , B_USER_EXTEND.USER_ID
     , B_USER_EXTEND.ID_SOURCE
     , B_USER_EXTEND.USER_CODE
     , B_USER_EXTEND.BIRTH_DATE
     , B_USER_EXTEND.SEX
     , B_USER_EXTEND.ORGANIZATION_ID
     , B_USER_EXTEND.TITLE_ID
     , B_USER_EXTEND.TITLE_ADD_ID
     , B_USER_EXTEND.RETIRED_DATE
     , B_USER_EXTEND.CONTRACT_TYPE
     , B_USER_EXTEND.POSITION_ID
     , B_USER_EXTEND.MANAGER_NAME
     , B_USER_EXTEND.DATE_START_WORK
     , B_USER_EXTEND.DATE_START_INTERN
     , B_USER_EXTEND.DATE_START_PROBATION 
     , B_USER_EXTEND.DATE_ACTING_PROMOTED
     , B_USER_EXTEND.OFFICAL_PROMOTED_DATE
     , B_USER_EXTEND.CURRENT_POSITION_WORK_TIME
     , B_USER_EXTEND.SENIORITY
     , B_USER_EXTEND.SALARY_STATUS
     , B_USER_EXTEND.POS_NAME_CENTER
     , B_USER_EXTEND.AREA
     , B_USER_EXTEND.PREVIOUS_EMPLOY_HISTORY
     , B_USER_EXTEND.EMPLOYMENT_HISTORY
     , B_USER_EXTEND.BUSINESS_TYPE
     , B_USER_EXTEND.USER_LDAP
     , B_USER_EXTEND.FLEX1
     , B_USER_EXTEND.FLEX2
     , B_USER_EXTEND.FLEX3
     , B_USER_EXTEND.FLEX4
     , B_USER_EXTEND.FLEX5
     , B_USER_EXTEND.FLEX6
     , B_USER_EXTEND.FLEX7
     , B_USER_EXTEND.FLEX8
     , B_USER_EXTEND.FLEX9
     , B_USER_EXTEND.FLEX10
     , B_USER_EXTEND.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_USER_EXTEND.DATE_ENTERED
     , B_USER_EXTEND.DATE_MODIFIED
     , B_USER_EXTEND.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_USER_EXTEND.CREATED_BY      as CREATED_BY_ID
     , B_USER_EXTEND.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_USER_EXTEND_CSTM.*
  from            B_USER_EXTEND
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_USER_EXTEND.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_USER_EXTEND.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_USER_EXTEND.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_USER_EXTEND.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_USER_EXTEND.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_USER_EXTEND.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_USER_EXTEND.MODIFIED_USER_ID
  left outer join B_USER_EXTEND_CSTM
               on B_USER_EXTEND_CSTM.ID_C     = B_USER_EXTEND.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_USER_EXTEND.ID
 where B_USER_EXTEND.DELETED = 0

GO

Grant Select on dbo.vwB_USER_EXTEND to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_USER_EXTEND_Edit')
	Drop View dbo.vwB_USER_EXTEND_Edit;
GO


Create View dbo.vwB_USER_EXTEND_Edit
as
select *
  from vwB_USER_EXTEND

GO

Grant Select on dbo.vwB_USER_EXTEND_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_USER_EXTEND_List')
	Drop View dbo.vwB_USER_EXTEND_List;
GO


Create View dbo.vwB_USER_EXTEND_List
as
select *
  from vwB_USER_EXTEND

GO

Grant Select on dbo.vwB_USER_EXTEND_List to public;
GO


/*

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_Delete;
GO


Create Procedure dbo.spB_USER_EXTEND_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_USER_EXTEND
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_USER_EXTEND_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_Undelete;
GO


Create Procedure dbo.spB_USER_EXTEND_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'B_USER_EXTEND';
	
	-- BEGIN Oracle Exception
		update B_USER_EXTEND
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_USER_EXTEND_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_USER_EXTEND_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_Update;
GO


Create Procedure dbo.spB_USER_EXTEND_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @USER_ID                            uniqueidentifier
	, @ID_SOURCE                          int
	, @USER_CODE                          nvarchar(50)
	, @BIRTH_DATE                         datetime
	, @SEX                                nvarchar(20)
	, @ORGANIZATION_ID                    uniqueidentifier
	, @TITLE_ID                           uniqueidentifier
	, @TITLE_ADD_ID                       uniqueidentifier
	, @RETIRED_DATE                       datetime
	, @CONTRACT_TYPE                      nvarchar(50)
	, @POSITION_ID                        uniqueidentifier
	, @MANAGER_NAME                       nvarchar(50)
	, @DATE_START_WORK                    datetime
	, @DATE_START_INTERN                  datetime
	, @DATE_START_PROBATION               datetime
	, @DATE_ACTING_PROMOTED               datetime
	, @OFFICAL_PROMOTED_DATE              datetime
	, @CURRENT_POSITION_WORK_TIME         datetime
	, @SENIORITY                          nvarchar(20)
	, @SALARY_STATUS                      nvarchar(10)
	, @POS_NAME_CENTER                    nvarchar(50)
	, @AREA                               nvarchar(50)
	, @PREVIOUS_EMPLOY_HISTORY            nvarchar(50)
	, @EMPLOYMENT_HISTORY                 nvarchar(50)
	, @BUSINESS_TYPE                      nvarchar(30)
	, @USER_LDAP                          nvarchar(40)
	, @FLEX1                              nvarchar(150)
	, @FLEX2                              nvarchar(150)
	, @FLEX3                              nvarchar(150)
	, @FLEX4                              nvarchar(150)
	, @FLEX5                              nvarchar(150)
	, @FLEX6                              nvarchar(150)
	, @FLEX7                              nvarchar(150)
	, @FLEX8                              nvarchar(150)
	, @FLEX9                              nvarchar(150)
	, @FLEX10                             nvarchar(150)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_USER_EXTEND where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_USER_EXTEND
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, USER_ID                            
			, ID_SOURCE                          
			, USER_CODE                          
			, BIRTH_DATE                         
			, SEX                                
			, ORGANIZATION_ID                    
			, TITLE_ID                           
			, TITLE_ADD_ID                       
			, RETIRED_DATE                       
			, CONTRACT_TYPE                      
			, POSITION_ID                        
			, MANAGER_NAME                       
			, DATE_START_WORK                    
			, DATE_START_INTERN                  
			, DATE_START_PROBATION               
			, DATE_ACTING_PROMOTED               
			, OFFICAL_PROMOTED_DATE              
			, CURRENT_POSITION_WORK_TIME         
			, SENIORITY                          
			, SALARY_STATUS                      
			, POS_NAME_CENTER                    
			, AREA                               
			, PREVIOUS_EMPLOY_HISTORY            
			, EMPLOYMENT_HISTORY                 
			, BUSINESS_TYPE                      
			, USER_LDAP                          
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              
			, FLEX6                              
			, FLEX7                              
			, FLEX8                              
			, FLEX9                              
			, FLEX10                             

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @USER_ID                            
			, @ID_SOURCE                          
			, @USER_CODE                          
			, @BIRTH_DATE                         
			, @SEX                                
			, @ORGANIZATION_ID                    
			, @TITLE_ID                           
			, @TITLE_ADD_ID                       
			, @RETIRED_DATE                       
			, @CONTRACT_TYPE                      
			, @POSITION_ID                        
			, @MANAGER_NAME                       
			, @DATE_START_WORK                    
			, @DATE_START_INTERN                  
			, @DATE_START_PROBATION               
			, @DATE_ACTING_PROMOTED               
			, @OFFICAL_PROMOTED_DATE              
			, @CURRENT_POSITION_WORK_TIME         
			, @SENIORITY                          
			, @SALARY_STATUS                      
			, @POS_NAME_CENTER                    
			, @AREA                               
			, @PREVIOUS_EMPLOY_HISTORY            
			, @EMPLOYMENT_HISTORY                 
			, @BUSINESS_TYPE                      
			, @USER_LDAP                          
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              
			, @FLEX6                              
			, @FLEX7                              
			, @FLEX8                              
			, @FLEX9                              
			, @FLEX10                             

			);
	end else begin
		update B_USER_EXTEND
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , USER_ID                              = @USER_ID                            
		     , ID_SOURCE                            = @ID_SOURCE                          
		     , USER_CODE                            = @USER_CODE                          
		     , BIRTH_DATE                           = @BIRTH_DATE                         
		     , SEX                                  = @SEX                                
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID                    
		     , TITLE_ID                             = @TITLE_ID                           
		     , TITLE_ADD_ID                         = @TITLE_ADD_ID                       
		     , RETIRED_DATE                         = @RETIRED_DATE                       
		     , CONTRACT_TYPE                        = @CONTRACT_TYPE                      
		     , POSITION_ID                          = @POSITION_ID                        
		     , MANAGER_NAME                         = @MANAGER_NAME                       
		     , DATE_START_WORK                      = @DATE_START_WORK                    
		     , DATE_START_INTERN                    = @DATE_START_INTERN                  
		     , DATE_START_PROBATION                 = @DATE_START_PROBATION               
		     , DATE_ACTING_PROMOTED                 = @DATE_ACTING_PROMOTED               
		     , OFFICAL_PROMOTED_DATE                = @OFFICAL_PROMOTED_DATE              
		     , CURRENT_POSITION_WORK_TIME           = @CURRENT_POSITION_WORK_TIME         
		     , SENIORITY                            = @SENIORITY                          
		     , SALARY_STATUS                        = @SALARY_STATUS                      
		     , POS_NAME_CENTER                      = @POS_NAME_CENTER                    
		     , AREA                                 = @AREA                               
		     , PREVIOUS_EMPLOY_HISTORY              = @PREVIOUS_EMPLOY_HISTORY            
		     , EMPLOYMENT_HISTORY                   = @EMPLOYMENT_HISTORY                 
		     , BUSINESS_TYPE                        = @BUSINESS_TYPE                      
		     , USER_LDAP                            = @USER_LDAP                          
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              
		     , FLEX6                                = @FLEX6                              
		     , FLEX7                                = @FLEX7                              
		     , FLEX8                                = @FLEX8                              
		     , FLEX9                                = @FLEX9                              
		     , FLEX10                               = @FLEX10                             

		 where ID                                   = @ID                                 ;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_USER_EXTEND_CSTM where ID_C = @ID) begin -- then
			insert into B_USER_EXTEND_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_USER_EXTEND', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_USER_EXTEND_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_MassDelete;
GO


Create Procedure dbo.spB_USER_EXTEND_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_USER_EXTEND_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_USER_EXTEND_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_MassUpdate;
GO


Create Procedure dbo.spB_USER_EXTEND_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_USER_EXTEND
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'B_USER_EXTEND', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_USER_EXTEND', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_USER_EXTEND
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_USER_EXTEND_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_USER_EXTEND_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_USER_EXTEND_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_USER_EXTEND_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_USER_EXTEND_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_USER_EXTEND_Merge to public;
GO
*/


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_USER_EXTEND';
	exec dbo.spSqlBuildAuditTrigger 'B_USER_EXTEND';
	exec dbo.spSqlBuildAuditView    'B_USER_EXTEND';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'B_USER_EXTEND.DetailView';
/*
if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'B_USER_EXTEND.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS B_USER_EXTEND.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'B_USER_EXTEND.DetailView'   , 'B_USER_EXTEND', 'vwB_USER_EXTEND_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 1, 'B_USER_EXTEND.LBL_USER_ID', 'USER_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 2, 'B_USER_EXTEND.LBL_ID_SOURCE', 'ID_SOURCE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 3, 'B_USER_EXTEND.LBL_USER_CODE', 'USER_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 4, 'B_USER_EXTEND.LBL_BIRTH_DATE', 'BIRTH_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 5, 'B_USER_EXTEND.LBL_SEX', 'SEX', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 6, 'B_USER_EXTEND.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 7, 'B_USER_EXTEND.LBL_TITLE_ID', 'TITLE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 8, 'B_USER_EXTEND.LBL_TITLE_ADD_ID', 'TITLE_ADD_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 9, 'B_USER_EXTEND.LBL_RETIRED_DATE', 'RETIRED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 10, 'B_USER_EXTEND.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 11, 'B_USER_EXTEND.LBL_POSITION_ID', 'POSITION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 12, 'B_USER_EXTEND.LBL_MANAGER_NAME', 'MANAGER_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 13, 'B_USER_EXTEND.LBL_DATE_START_WORK', 'DATE_START_WORK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 14, 'B_USER_EXTEND.LBL_DATE_START_INTERN', 'DATE_START_INTERN', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 15, 'B_USER_EXTEND.LBL_DATE_START_PROBATION ', 'DATE_START_PROBATION ', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 16, 'B_USER_EXTEND.LBL_DATE_ACTING_PROMOTED', 'DATE_ACTING_PROMOTED', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 17, 'B_USER_EXTEND.LBL_OFFICAL_PROMOTED_DATE', 'OFFICAL_PROMOTED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 18, 'B_USER_EXTEND.LBL_CURRENT_POSITION_WORK_TIME', 'CURRENT_POSITION_WORK_TIME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 19, 'B_USER_EXTEND.LBL_SENIORITY', 'SENIORITY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 20, 'B_USER_EXTEND.LBL_SALARY_STATUS', 'SALARY_STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 21, 'B_USER_EXTEND.LBL_POS_NAME_CENTER', 'POS_NAME_CENTER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 22, 'B_USER_EXTEND.LBL_AREA', 'AREA', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 23, 'B_USER_EXTEND.LBL_PREVIOUS_EMPLOY_HISTORY', 'PREVIOUS_EMPLOY_HISTORY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 24, 'B_USER_EXTEND.LBL_EMPLOYMENT_HISTORY', 'EMPLOYMENT_HISTORY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 25, 'B_USER_EXTEND.LBL_BUSINESS_TYPE', 'BUSINESS_TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 26, 'B_USER_EXTEND.LBL_USER_LDAP', 'USER_LDAP', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 27, 'B_USER_EXTEND.LBL_FLEX1', 'FLEX1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 28, 'B_USER_EXTEND.LBL_FLEX2', 'FLEX2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 29, 'B_USER_EXTEND.LBL_FLEX3', 'FLEX3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 30, 'B_USER_EXTEND.LBL_FLEX4', 'FLEX4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 31, 'B_USER_EXTEND.LBL_FLEX5', 'FLEX5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 32, 'B_USER_EXTEND.LBL_FLEX6', 'FLEX6', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 33, 'B_USER_EXTEND.LBL_FLEX7', 'FLEX7', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 34, 'B_USER_EXTEND.LBL_FLEX8', 'FLEX8', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 35, 'B_USER_EXTEND.LBL_FLEX9', 'FLEX9', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 36, 'B_USER_EXTEND.LBL_FLEX10', 'FLEX10', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 37, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 38, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 39, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_USER_EXTEND.DetailView', 40, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'B_USER_EXTEND.DetailView', 'B_USER_EXTEND';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'B_USER_EXTEND.EditView'  , 'B_USER_EXTEND';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'B_USER_EXTEND.PopupView' , 'B_USER_EXTEND';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'B_USER_EXTEND.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'B_USER_EXTEND.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'B_USER_EXTEND.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'B_USER_EXTEND.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'B_USER_EXTEND.EditView', 'B_USER_EXTEND'      , 'vwB_USER_EXTEND_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView', 1, 'B_USER_EXTEND.LBL_USER_ID', 'USER_ID', 0, 1, 'USER_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 2, 'B_USER_EXTEND.LBL_ID_SOURCE', 'ID_SOURCE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 3, 'B_USER_EXTEND.LBL_USER_CODE', 'USER_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 4, 'B_USER_EXTEND.LBL_BIRTH_DATE', 'BIRTH_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 5, 'B_USER_EXTEND.LBL_SEX', 'SEX', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView', 6, 'B_USER_EXTEND.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView', 7, 'B_USER_EXTEND.LBL_TITLE_ID', 'TITLE_ID', 0, 1, 'TITLE_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView', 8, 'B_USER_EXTEND.LBL_TITLE_ADD_ID', 'TITLE_ADD_ID', 0, 1, 'TITLE_ADD_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 9, 'B_USER_EXTEND.LBL_RETIRED_DATE', 'RETIRED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 10, 'B_USER_EXTEND.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView', 11, 'B_USER_EXTEND.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 'POSITION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 12, 'B_USER_EXTEND.LBL_MANAGER_NAME', 'MANAGER_NAME', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 13, 'B_USER_EXTEND.LBL_DATE_START_WORK', 'DATE_START_WORK', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 14, 'B_USER_EXTEND.LBL_DATE_START_INTERN', 'DATE_START_INTERN', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 15, 'B_USER_EXTEND.LBL_DATE_START_PROBATION ', 'DATE_START_PROBATION ', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 16, 'B_USER_EXTEND.LBL_DATE_ACTING_PROMOTED', 'DATE_ACTING_PROMOTED', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 17, 'B_USER_EXTEND.LBL_OFFICAL_PROMOTED_DATE', 'OFFICAL_PROMOTED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView', 18, 'B_USER_EXTEND.LBL_CURRENT_POSITION_WORK_TIME', 'CURRENT_POSITION_WORK_TIME', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 19, 'B_USER_EXTEND.LBL_SENIORITY', 'SENIORITY', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 20, 'B_USER_EXTEND.LBL_SALARY_STATUS', 'SALARY_STATUS', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 21, 'B_USER_EXTEND.LBL_POS_NAME_CENTER', 'POS_NAME_CENTER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 22, 'B_USER_EXTEND.LBL_AREA', 'AREA', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 23, 'B_USER_EXTEND.LBL_PREVIOUS_EMPLOY_HISTORY', 'PREVIOUS_EMPLOY_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 24, 'B_USER_EXTEND.LBL_EMPLOYMENT_HISTORY', 'EMPLOYMENT_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 25, 'B_USER_EXTEND.LBL_BUSINESS_TYPE', 'BUSINESS_TYPE', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 26, 'B_USER_EXTEND.LBL_USER_LDAP', 'USER_LDAP', 0, 1, 40, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 27, 'B_USER_EXTEND.LBL_FLEX1', 'FLEX1', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 28, 'B_USER_EXTEND.LBL_FLEX2', 'FLEX2', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 29, 'B_USER_EXTEND.LBL_FLEX3', 'FLEX3', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 30, 'B_USER_EXTEND.LBL_FLEX4', 'FLEX4', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 31, 'B_USER_EXTEND.LBL_FLEX5', 'FLEX5', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 32, 'B_USER_EXTEND.LBL_FLEX6', 'FLEX6', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 33, 'B_USER_EXTEND.LBL_FLEX7', 'FLEX7', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 34, 'B_USER_EXTEND.LBL_FLEX8', 'FLEX8', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 35, 'B_USER_EXTEND.LBL_FLEX9', 'FLEX9', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView', 36, 'B_USER_EXTEND.LBL_FLEX10', 'FLEX10', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.EditView', 37, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.EditView', 38, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'B_USER_EXTEND.EditView.Inline', 'B_USER_EXTEND'      , 'vwB_USER_EXTEND_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView.Inline', 1, 'B_USER_EXTEND.LBL_USER_ID', 'USER_ID', 0, 1, 'USER_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 2, 'B_USER_EXTEND.LBL_ID_SOURCE', 'ID_SOURCE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 3, 'B_USER_EXTEND.LBL_USER_CODE', 'USER_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 4, 'B_USER_EXTEND.LBL_BIRTH_DATE', 'BIRTH_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 5, 'B_USER_EXTEND.LBL_SEX', 'SEX', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView.Inline', 6, 'B_USER_EXTEND.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView.Inline', 7, 'B_USER_EXTEND.LBL_TITLE_ID', 'TITLE_ID', 0, 1, 'TITLE_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView.Inline', 8, 'B_USER_EXTEND.LBL_TITLE_ADD_ID', 'TITLE_ADD_ID', 0, 1, 'TITLE_ADD_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 9, 'B_USER_EXTEND.LBL_RETIRED_DATE', 'RETIRED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 10, 'B_USER_EXTEND.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.EditView.Inline', 11, 'B_USER_EXTEND.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 'POSITION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 12, 'B_USER_EXTEND.LBL_MANAGER_NAME', 'MANAGER_NAME', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 13, 'B_USER_EXTEND.LBL_DATE_START_WORK', 'DATE_START_WORK', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 14, 'B_USER_EXTEND.LBL_DATE_START_INTERN', 'DATE_START_INTERN', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 15, 'B_USER_EXTEND.LBL_DATE_START_PROBATION ', 'DATE_START_PROBATION ', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 16, 'B_USER_EXTEND.LBL_DATE_ACTING_PROMOTED', 'DATE_ACTING_PROMOTED', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 17, 'B_USER_EXTEND.LBL_OFFICAL_PROMOTED_DATE', 'OFFICAL_PROMOTED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.EditView.Inline', 18, 'B_USER_EXTEND.LBL_CURRENT_POSITION_WORK_TIME', 'CURRENT_POSITION_WORK_TIME', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 19, 'B_USER_EXTEND.LBL_SENIORITY', 'SENIORITY', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 20, 'B_USER_EXTEND.LBL_SALARY_STATUS', 'SALARY_STATUS', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 21, 'B_USER_EXTEND.LBL_POS_NAME_CENTER', 'POS_NAME_CENTER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 22, 'B_USER_EXTEND.LBL_AREA', 'AREA', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 23, 'B_USER_EXTEND.LBL_PREVIOUS_EMPLOY_HISTORY', 'PREVIOUS_EMPLOY_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 24, 'B_USER_EXTEND.LBL_EMPLOYMENT_HISTORY', 'EMPLOYMENT_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 25, 'B_USER_EXTEND.LBL_BUSINESS_TYPE', 'BUSINESS_TYPE', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 26, 'B_USER_EXTEND.LBL_USER_LDAP', 'USER_LDAP', 0, 1, 40, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 27, 'B_USER_EXTEND.LBL_FLEX1', 'FLEX1', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 28, 'B_USER_EXTEND.LBL_FLEX2', 'FLEX2', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 29, 'B_USER_EXTEND.LBL_FLEX3', 'FLEX3', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 30, 'B_USER_EXTEND.LBL_FLEX4', 'FLEX4', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 31, 'B_USER_EXTEND.LBL_FLEX5', 'FLEX5', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 32, 'B_USER_EXTEND.LBL_FLEX6', 'FLEX6', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 33, 'B_USER_EXTEND.LBL_FLEX7', 'FLEX7', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 34, 'B_USER_EXTEND.LBL_FLEX8', 'FLEX8', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 35, 'B_USER_EXTEND.LBL_FLEX9', 'FLEX9', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.EditView.Inline', 36, 'B_USER_EXTEND.LBL_FLEX10', 'FLEX10', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.EditView.Inline', 37, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.EditView.Inline', 38, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'B_USER_EXTEND.PopupView.Inline', 'B_USER_EXTEND'      , 'vwB_USER_EXTEND_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.PopupView.Inline', 1, 'B_USER_EXTEND.LBL_USER_ID', 'USER_ID', 0, 1, 'USER_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 2, 'B_USER_EXTEND.LBL_ID_SOURCE', 'ID_SOURCE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 3, 'B_USER_EXTEND.LBL_USER_CODE', 'USER_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 4, 'B_USER_EXTEND.LBL_BIRTH_DATE', 'BIRTH_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 5, 'B_USER_EXTEND.LBL_SEX', 'SEX', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.PopupView.Inline', 6, 'B_USER_EXTEND.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.PopupView.Inline', 7, 'B_USER_EXTEND.LBL_TITLE_ID', 'TITLE_ID', 0, 1, 'TITLE_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.PopupView.Inline', 8, 'B_USER_EXTEND.LBL_TITLE_ADD_ID', 'TITLE_ADD_ID', 0, 1, 'TITLE_ADD_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 9, 'B_USER_EXTEND.LBL_RETIRED_DATE', 'RETIRED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 10, 'B_USER_EXTEND.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_USER_EXTEND.PopupView.Inline', 11, 'B_USER_EXTEND.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 'POSITION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 12, 'B_USER_EXTEND.LBL_MANAGER_NAME', 'MANAGER_NAME', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 13, 'B_USER_EXTEND.LBL_DATE_START_WORK', 'DATE_START_WORK', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 14, 'B_USER_EXTEND.LBL_DATE_START_INTERN', 'DATE_START_INTERN', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 15, 'B_USER_EXTEND.LBL_DATE_START_PROBATION ', 'DATE_START_PROBATION ', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 16, 'B_USER_EXTEND.LBL_DATE_ACTING_PROMOTED', 'DATE_ACTING_PROMOTED', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 17, 'B_USER_EXTEND.LBL_OFFICAL_PROMOTED_DATE', 'OFFICAL_PROMOTED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'B_USER_EXTEND.PopupView.Inline', 18, 'B_USER_EXTEND.LBL_CURRENT_POSITION_WORK_TIME', 'CURRENT_POSITION_WORK_TIME', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 19, 'B_USER_EXTEND.LBL_SENIORITY', 'SENIORITY', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 20, 'B_USER_EXTEND.LBL_SALARY_STATUS', 'SALARY_STATUS', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 21, 'B_USER_EXTEND.LBL_POS_NAME_CENTER', 'POS_NAME_CENTER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 22, 'B_USER_EXTEND.LBL_AREA', 'AREA', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 23, 'B_USER_EXTEND.LBL_PREVIOUS_EMPLOY_HISTORY', 'PREVIOUS_EMPLOY_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 24, 'B_USER_EXTEND.LBL_EMPLOYMENT_HISTORY', 'EMPLOYMENT_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 25, 'B_USER_EXTEND.LBL_BUSINESS_TYPE', 'BUSINESS_TYPE', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 26, 'B_USER_EXTEND.LBL_USER_LDAP', 'USER_LDAP', 0, 1, 40, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 27, 'B_USER_EXTEND.LBL_FLEX1', 'FLEX1', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 28, 'B_USER_EXTEND.LBL_FLEX2', 'FLEX2', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 29, 'B_USER_EXTEND.LBL_FLEX3', 'FLEX3', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 30, 'B_USER_EXTEND.LBL_FLEX4', 'FLEX4', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 31, 'B_USER_EXTEND.LBL_FLEX5', 'FLEX5', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 32, 'B_USER_EXTEND.LBL_FLEX6', 'FLEX6', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 33, 'B_USER_EXTEND.LBL_FLEX7', 'FLEX7', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 34, 'B_USER_EXTEND.LBL_FLEX8', 'FLEX8', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 35, 'B_USER_EXTEND.LBL_FLEX9', 'FLEX9', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_USER_EXTEND.PopupView.Inline', 36, 'B_USER_EXTEND.LBL_FLEX10', 'FLEX10', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.PopupView.Inline', 37, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_USER_EXTEND.PopupView.Inline', 38, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'B_USER_EXTEND.SearchBasic'    , 'B_USER_EXTEND', 'vwB_USER_EXTEND_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'B_USER_EXTEND.SearchBasic', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, 'B_USER_EXTEND', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'B_USER_EXTEND.SearchAdvanced' , 'B_USER_EXTEND', 'vwB_USER_EXTEND_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'B_USER_EXTEND.SearchAdvanced', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, 'B_USER_EXTEND', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_USER_EXTEND.SearchAdvanced', 1, 'B_USER_EXTEND.LBL_USER_ID', 'USER_ID', 0, 1, 'USER_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 2, 'B_USER_EXTEND.LBL_ID_SOURCE', 'ID_SOURCE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 3, 'B_USER_EXTEND.LBL_USER_CODE', 'USER_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 4, 'B_USER_EXTEND.LBL_BIRTH_DATE', 'BIRTH_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 5, 'B_USER_EXTEND.LBL_SEX', 'SEX', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_USER_EXTEND.SearchAdvanced', 6, 'B_USER_EXTEND.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_USER_EXTEND.SearchAdvanced', 7, 'B_USER_EXTEND.LBL_TITLE_ID', 'TITLE_ID', 0, 1, 'TITLE_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_USER_EXTEND.SearchAdvanced', 8, 'B_USER_EXTEND.LBL_TITLE_ADD_ID', 'TITLE_ADD_ID', 0, 1, 'TITLE_ADD_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 9, 'B_USER_EXTEND.LBL_RETIRED_DATE', 'RETIRED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 10, 'B_USER_EXTEND.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_USER_EXTEND.SearchAdvanced', 11, 'B_USER_EXTEND.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 'POSITION_NAME', 'return B_USER_EXTENDPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 12, 'B_USER_EXTEND.LBL_MANAGER_NAME', 'MANAGER_NAME', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 13, 'B_USER_EXTEND.LBL_DATE_START_WORK', 'DATE_START_WORK', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 14, 'B_USER_EXTEND.LBL_DATE_START_INTERN', 'DATE_START_INTERN', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 15, 'B_USER_EXTEND.LBL_DATE_START_PROBATION ', 'DATE_START_PROBATION ', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 16, 'B_USER_EXTEND.LBL_DATE_ACTING_PROMOTED', 'DATE_ACTING_PROMOTED', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 17, 'B_USER_EXTEND.LBL_OFFICAL_PROMOTED_DATE', 'OFFICAL_PROMOTED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_USER_EXTEND.SearchAdvanced', 18, 'B_USER_EXTEND.LBL_CURRENT_POSITION_WORK_TIME', 'CURRENT_POSITION_WORK_TIME', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 19, 'B_USER_EXTEND.LBL_SENIORITY', 'SENIORITY', 0, 1, 20, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 20, 'B_USER_EXTEND.LBL_SALARY_STATUS', 'SALARY_STATUS', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 21, 'B_USER_EXTEND.LBL_POS_NAME_CENTER', 'POS_NAME_CENTER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 22, 'B_USER_EXTEND.LBL_AREA', 'AREA', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 23, 'B_USER_EXTEND.LBL_PREVIOUS_EMPLOY_HISTORY', 'PREVIOUS_EMPLOY_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 24, 'B_USER_EXTEND.LBL_EMPLOYMENT_HISTORY', 'EMPLOYMENT_HISTORY', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 25, 'B_USER_EXTEND.LBL_BUSINESS_TYPE', 'BUSINESS_TYPE', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 26, 'B_USER_EXTEND.LBL_USER_LDAP', 'USER_LDAP', 0, 1, 40, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 27, 'B_USER_EXTEND.LBL_FLEX1', 'FLEX1', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 28, 'B_USER_EXTEND.LBL_FLEX2', 'FLEX2', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 29, 'B_USER_EXTEND.LBL_FLEX3', 'FLEX3', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 30, 'B_USER_EXTEND.LBL_FLEX4', 'FLEX4', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 31, 'B_USER_EXTEND.LBL_FLEX5', 'FLEX5', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 32, 'B_USER_EXTEND.LBL_FLEX6', 'FLEX6', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 33, 'B_USER_EXTEND.LBL_FLEX7', 'FLEX7', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 34, 'B_USER_EXTEND.LBL_FLEX8', 'FLEX8', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 35, 'B_USER_EXTEND.LBL_FLEX9', 'FLEX9', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_USER_EXTEND.SearchAdvanced', 36, 'B_USER_EXTEND.LBL_FLEX10', 'FLEX10', 0, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'B_USER_EXTEND.SearchAdvanced' , 37, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_USER_EXTEND.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_USER_EXTEND.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'B_USER_EXTEND.SearchPopup'    , 'B_USER_EXTEND', 'vwB_USER_EXTEND_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'B_USER_EXTEND.SearchPopup', 0, 'B_USER_EXTEND.LBL_NAME', 'NAME', 1, 1, 150, 35, 'B_USER_EXTEND', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_USER_EXTEND.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_USER_EXTEND.Export', 'B_USER_EXTEND', 'vwB_USER_EXTEND_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_USER_EXTEND.Export'         ,  1, 'B_USER_EXTEND.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_USER_EXTEND.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_USER_EXTEND.ListView', 'B_USER_EXTEND'      , 'vwB_USER_EXTEND_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'B_USER_EXTEND.ListView', 2, 'B_USER_EXTEND.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/B_USER_EXTEND/view.aspx?id={0}', null, 'B_USER_EXTEND', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_USER_EXTEND.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_USER_EXTEND.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_USER_EXTEND.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_USER_EXTEND.PopupView', 'B_USER_EXTEND'      , 'vwB_USER_EXTEND_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'B_USER_EXTEND.PopupView', 1, 'B_USER_EXTEND.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectB_USER_EXTEND(''{0}'', ''{1}'');', null, 'B_USER_EXTEND', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_USER_EXTEND.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_USER_EXTEND.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_USER_EXTEND.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_USER_EXTEND.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_USER_EXTEND.SearchDuplicates', 'B_USER_EXTEND', 'vwB_USER_EXTEND_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'B_USER_EXTEND.SearchDuplicates'          , 1, 'B_USER_EXTEND.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/B_USER_EXTEND/view.aspx?id={0}', null, 'B_USER_EXTEND', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'B_USER_EXTEND', '.moduleList.B_USER_EXTEND', '~/B_USER_EXTEND/', 1, 0, 100, 0, 0, 0, 0, 0, 'B_USER_EXTEND', 0, 0, 0, 0, 0, 0;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'B_USER_EXTEND';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'B_USER_EXTEND' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_USER_EXTEND', 'B_USER_EXTEND.LNK_NEW_B_USER_EXTEND' , '~/B_USER_EXTEND/edit.aspx'   , 'CreateB_USER_EXTEND.gif', 1,  1, 'B_USER_EXTEND', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_USER_EXTEND', 'B_USER_EXTEND.LNK_B_USER_EXTEND_LIST', '~/B_USER_EXTEND/default.aspx', 'B_USER_EXTEND.gif'      , 1,  2, 'B_USER_EXTEND', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_USER_EXTEND', '.LBL_IMPORT'              , '~/B_USER_EXTEND/import.aspx' , 'Import.gif'        , 1,  3, 'B_USER_EXTEND', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_USER_EXTEND', '.LNK_ACTIVITY_STREAM'     , '~/B_USER_EXTEND/stream.aspx' , 'ActivityStream.gif', 1,  4, 'B_USER_EXTEND', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'B_USER_EXTEND', null, null, N'B_USER_EXTEND List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'B_USER_EXTEND', null, null, N'Create B_USER_EXTEND';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_USER_EXTEND_LIST'                          , N'en-US', N'B_USER_EXTEND', null, null, N'B_USER_EXTEND';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_USER_EXTEND'                           , N'en-US', N'B_USER_EXTEND', null, null, N'Create B_USER_EXTEND';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'B_USER_EXTEND', null, null, N'B_USER_EXTEND Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_USER_EXTEND_NOT_FOUND'                     , N'en-US', N'B_USER_EXTEND', null, null, N'B_USER_EXTEND not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_USER_EXTEND_CONFIRMATION'           , N'en-US', N'B_USER_EXTEND', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'B_USER_EXTEND', null, null, N'B_USER_EXTEND';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'B_USER_EXTEND', null, null, N'B_U';

exec dbo.spTERMINOLOGY_InsertOnly N'B_USER_EXTEND'                                          , N'en-US', null, N'moduleList', 100, N'B_USER_EXTEND';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'B_USER_EXTEND', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_USER_ID'                                           , 'en-US', 'B_USER_EXTEND', null, null, 'user id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_USER_ID'                                      , 'en-US', 'B_USER_EXTEND', null, null, 'user id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ID_SOURCE'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'id source:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ID_SOURCE'                                    , 'en-US', 'B_USER_EXTEND', null, null, 'id source';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_USER_CODE'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'user code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_USER_CODE'                                    , 'en-US', 'B_USER_EXTEND', null, null, 'user code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_BIRTH_DATE'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'birth date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_BIRTH_DATE'                                   , 'en-US', 'B_USER_EXTEND', null, null, 'birth date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_SEX'                                               , 'en-US', 'B_USER_EXTEND', null, null, 'sex:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_SEX'                                          , 'en-US', 'B_USER_EXTEND', null, null, 'sex';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_ID'                                   , 'en-US', 'B_USER_EXTEND', null, null, 'organization id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_ID'                              , 'en-US', 'B_USER_EXTEND', null, null, 'organization id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TITLE_ID'                                          , 'en-US', 'B_USER_EXTEND', null, null, 'title id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TITLE_ID'                                     , 'en-US', 'B_USER_EXTEND', null, null, 'title id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TITLE_ADD_ID'                                      , 'en-US', 'B_USER_EXTEND', null, null, 'title add_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TITLE_ADD_ID'                                 , 'en-US', 'B_USER_EXTEND', null, null, 'title add_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RETIRED_DATE'                                      , 'en-US', 'B_USER_EXTEND', null, null, 'retired date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RETIRED_DATE'                                 , 'en-US', 'B_USER_EXTEND', null, null, 'retired date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_CONTRACT_TYPE'                                     , 'en-US', 'B_USER_EXTEND', null, null, 'contract type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_CONTRACT_TYPE'                                , 'en-US', 'B_USER_EXTEND', null, null, 'contract type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_POSITION_ID'                                       , 'en-US', 'B_USER_EXTEND', null, null, 'position id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_POSITION_ID'                                  , 'en-US', 'B_USER_EXTEND', null, null, 'position id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MANAGER_NAME'                                      , 'en-US', 'B_USER_EXTEND', null, null, 'manager name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MANAGER_NAME'                                 , 'en-US', 'B_USER_EXTEND', null, null, 'manager name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DATE_START_WORK'                                   , 'en-US', 'B_USER_EXTEND', null, null, 'date start_work:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DATE_START_WORK'                              , 'en-US', 'B_USER_EXTEND', null, null, 'date start_work';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DATE_START_INTERN'                                 , 'en-US', 'B_USER_EXTEND', null, null, 'date start_intern:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DATE_START_INTERN'                            , 'en-US', 'B_USER_EXTEND', null, null, 'date start_intern';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DATE_START_PROBATION '                             , 'en-US', 'B_USER_EXTEND', null, null, 'date start_probation:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DATE_START_PROBATION '                        , 'en-US', 'B_USER_EXTEND', null, null, 'date start_probation';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DATE_ACTING_PROMOTED'                              , 'en-US', 'B_USER_EXTEND', null, null, 'date acting_promoted:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DATE_ACTING_PROMOTED'                         , 'en-US', 'B_USER_EXTEND', null, null, 'date acting_promoted';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_OFFICAL_PROMOTED_DATE'                             , 'en-US', 'B_USER_EXTEND', null, null, 'offical promoted_date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_OFFICAL_PROMOTED_DATE'                        , 'en-US', 'B_USER_EXTEND', null, null, 'offical promoted_date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_CURRENT_POSITION_WORK_TIME'                        , 'en-US', 'B_USER_EXTEND', null, null, 'current position_work_time:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_CURRENT_POSITION_WORK_TIME'                   , 'en-US', 'B_USER_EXTEND', null, null, 'current position_work_time';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_SENIORITY'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'seniority:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_SENIORITY'                                    , 'en-US', 'B_USER_EXTEND', null, null, 'seniority';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_SALARY_STATUS'                                     , 'en-US', 'B_USER_EXTEND', null, null, 'salary status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_SALARY_STATUS'                                , 'en-US', 'B_USER_EXTEND', null, null, 'salary status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_POS_NAME_CENTER'                                   , 'en-US', 'B_USER_EXTEND', null, null, 'pos name_center:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_POS_NAME_CENTER'                              , 'en-US', 'B_USER_EXTEND', null, null, 'pos name_center';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_AREA'                                              , 'en-US', 'B_USER_EXTEND', null, null, 'area:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_AREA'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'area';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PREVIOUS_EMPLOY_HISTORY'                           , 'en-US', 'B_USER_EXTEND', null, null, 'previous employ_history:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PREVIOUS_EMPLOY_HISTORY'                      , 'en-US', 'B_USER_EXTEND', null, null, 'previous employ_history';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_EMPLOYMENT_HISTORY'                                , 'en-US', 'B_USER_EXTEND', null, null, 'employment history:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_EMPLOYMENT_HISTORY'                           , 'en-US', 'B_USER_EXTEND', null, null, 'employment history';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_BUSINESS_TYPE'                                     , 'en-US', 'B_USER_EXTEND', null, null, 'business type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_BUSINESS_TYPE'                                , 'en-US', 'B_USER_EXTEND', null, null, 'business type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_USER_LDAP'                                         , 'en-US', 'B_USER_EXTEND', null, null, 'user ldap:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_USER_LDAP'                                    , 'en-US', 'B_USER_EXTEND', null, null, 'user ldap';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX1'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX1'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX2'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX2'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX3'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX3'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX4'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX4'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX5'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX5'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex5';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX6'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex6:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX6'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex6';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX7'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex7:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX7'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex7';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX8'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex8:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX8'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex8';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX9'                                             , 'en-US', 'B_USER_EXTEND', null, null, 'flex9:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX9'                                        , 'en-US', 'B_USER_EXTEND', null, null, 'flex9';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX10'                                            , 'en-US', 'B_USER_EXTEND', null, null, 'flex10:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX10'                                       , 'en-US', 'B_USER_EXTEND', null, null, 'flex10';
*/