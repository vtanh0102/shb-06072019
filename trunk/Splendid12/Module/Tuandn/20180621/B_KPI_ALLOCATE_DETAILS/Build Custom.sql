
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATE_DETAILS';
	Create Table dbo.B_KPI_ALLOCATE_DETAILS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATE_DETAILS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, KPI_ALLOCATE_ID                    uniqueidentifier not null
		, ALLOCATE_CODE                      nvarchar(50) null
		, VERSION_NUMBER                     nvarchar(2) null
		, EMPLOYEE_ID                        uniqueidentifier null
		, MA_NHAN_VIEN                       nvarchar(50) null
		, KPI_CODE                           nvarchar(50) null
		, KPI_NAME                           nvarchar(200) null
		, LEVEL_NUMBER                       int null
		, KPI_UNIT                           int null
		, UNIT                               int null
		, RADIO                              float null
		, MAX_RATIO_COMPLETE                 float null
		, KPI_GROUP_DETAIL_ID                uniqueidentifier null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, TOTAL_VALUE                        float null
		, MONTH_1                            float null
		, MONTH_2                            float null
		, MONTH_3                            float null
		, MONTH_4                            float null
		, MONTH_5                            float null
		, MONTH_6                            float null
		, MONTH_7                            float null
		, MONTH_8                            float null
		, MONTH_9                            float null
		, MONTH_10                           float null
		, MONTH_11                           float null
		, MONTH_12                           float null
		, FLEX1                              nvarchar(1000) null
		, FLEX2                              nvarchar(1000) null
		, FLEX3                              nvarchar(1000) null
		, FLEX4                              nvarchar(1000) null
		, FLEX5                              nvarchar(1000) null

		)

	create index IDX_B_KPI_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATE_DETAILS_CSTM';
	Create Table dbo.B_KPI_ALLOCATE_DETAILS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ALLOCATE_DETAILS_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATE_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_ID          on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATE_DETAILS_TEAM_SET_ID      on dbo.B_KPI_ALLOCATE_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_ALLOCATE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_ALLOCATE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_ALLOCATE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'ALLOCATE_CODE') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add ALLOCATE_CODE nvarchar(50) null';
	alter table B_KPI_ALLOCATE_DETAILS add ALLOCATE_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add VERSION_NUMBER nvarchar(2) null';
	alter table B_KPI_ALLOCATE_DETAILS add VERSION_NUMBER nvarchar(2) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'EMPLOYEE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add EMPLOYEE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add EMPLOYEE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MA_NHAN_VIEN') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MA_NHAN_VIEN nvarchar(50) null';
	alter table B_KPI_ALLOCATE_DETAILS add MA_NHAN_VIEN nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_CODE') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_CODE nvarchar(50) null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_NAME') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_NAME nvarchar(200) null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'LEVEL_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add LEVEL_NUMBER int null';
	alter table B_KPI_ALLOCATE_DETAILS add LEVEL_NUMBER int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_UNIT') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_UNIT int null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_UNIT int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'UNIT') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add UNIT int null';
	alter table B_KPI_ALLOCATE_DETAILS add UNIT int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'RADIO') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add RADIO float null';
	alter table B_KPI_ALLOCATE_DETAILS add RADIO float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MAX_RATIO_COMPLETE') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MAX_RATIO_COMPLETE float null';
	alter table B_KPI_ALLOCATE_DETAILS add MAX_RATIO_COMPLETE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'KPI_GROUP_DETAIL_ID') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add KPI_GROUP_DETAIL_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATE_DETAILS add KPI_GROUP_DETAIL_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ALLOCATE_DETAILS add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add REMARK nvarchar(max) null';
	alter table B_KPI_ALLOCATE_DETAILS add REMARK nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'TOTAL_VALUE') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add TOTAL_VALUE float null';
	alter table B_KPI_ALLOCATE_DETAILS add TOTAL_VALUE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_1') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_1 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_1 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_2') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_2 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_2 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_3') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_3 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_3 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_4') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_4 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_4 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_5') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_5 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_5 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_6') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_6 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_6 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_7') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_7 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_7 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_8') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_8 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_8 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_9') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_9 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_9 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_10') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_10 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_10 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_11') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_11 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_11 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'MONTH_12') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add MONTH_12 float null';
	alter table B_KPI_ALLOCATE_DETAILS add MONTH_12 float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'FLEX1') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add FLEX1 nvarchar(1000) null';
	alter table B_KPI_ALLOCATE_DETAILS add FLEX1 nvarchar(1000) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'FLEX2') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add FLEX2 nvarchar(1000) null';
	alter table B_KPI_ALLOCATE_DETAILS add FLEX2 nvarchar(1000) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'FLEX3') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add FLEX3 nvarchar(1000) null';
	alter table B_KPI_ALLOCATE_DETAILS add FLEX3 nvarchar(1000) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'FLEX4') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add FLEX4 nvarchar(1000) null';
	alter table B_KPI_ALLOCATE_DETAILS add FLEX4 nvarchar(1000) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATE_DETAILS' and COLUMN_NAME = 'FLEX5') begin -- then
	print 'alter table B_KPI_ALLOCATE_DETAILS add FLEX5 nvarchar(1000) null';
	alter table B_KPI_ALLOCATE_DETAILS add FLEX5 nvarchar(1000) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS
as
select B_KPI_ALLOCATE_DETAILS.ID
     , B_KPI_ALLOCATE_DETAILS.KPI_ALLOCATE_ID
     , B_KPI_ALLOCATE_DETAILS.ALLOCATE_CODE
     , B_KPI_ALLOCATE_DETAILS.VERSION_NUMBER
     , B_KPI_ALLOCATE_DETAILS.EMPLOYEE_ID
     , B_KPI_ALLOCATE_DETAILS.MA_NHAN_VIEN
     , B_KPI_ALLOCATE_DETAILS.KPI_CODE
     , B_KPI_ALLOCATE_DETAILS.KPI_NAME
     , B_KPI_ALLOCATE_DETAILS.LEVEL_NUMBER
     , B_KPI_ALLOCATE_DETAILS.KPI_UNIT
     , B_KPI_ALLOCATE_DETAILS.UNIT
     , B_KPI_ALLOCATE_DETAILS.RADIO
     , B_KPI_ALLOCATE_DETAILS.MAX_RATIO_COMPLETE
     , B_KPI_ALLOCATE_DETAILS.KPI_GROUP_DETAIL_ID
     , B_KPI_ALLOCATE_DETAILS.DESCRIPTION
     , B_KPI_ALLOCATE_DETAILS.REMARK
     , B_KPI_ALLOCATE_DETAILS.TOTAL_VALUE
     , B_KPI_ALLOCATE_DETAILS.MONTH_1
     , B_KPI_ALLOCATE_DETAILS.MONTH_2
     , B_KPI_ALLOCATE_DETAILS.MONTH_3
     , B_KPI_ALLOCATE_DETAILS.MONTH_4
     , B_KPI_ALLOCATE_DETAILS.MONTH_5
     , B_KPI_ALLOCATE_DETAILS.MONTH_6
     , B_KPI_ALLOCATE_DETAILS.MONTH_7
     , B_KPI_ALLOCATE_DETAILS.MONTH_8
     , B_KPI_ALLOCATE_DETAILS.MONTH_9
     , B_KPI_ALLOCATE_DETAILS.MONTH_10
     , B_KPI_ALLOCATE_DETAILS.MONTH_11
     , B_KPI_ALLOCATE_DETAILS.MONTH_12
     , B_KPI_ALLOCATE_DETAILS.FLEX1
     , B_KPI_ALLOCATE_DETAILS.FLEX2
     , B_KPI_ALLOCATE_DETAILS.FLEX3
     , B_KPI_ALLOCATE_DETAILS.FLEX4
     , B_KPI_ALLOCATE_DETAILS.FLEX5
     , B_KPI_ALLOCATE_DETAILS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , B_KPI_ALLOCATE_DETAILS.DATE_ENTERED
     , B_KPI_ALLOCATE_DETAILS.DATE_MODIFIED
     , B_KPI_ALLOCATE_DETAILS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ALLOCATE_DETAILS.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ALLOCATE_DETAILS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ALLOCATE_DETAILS_CSTM.*
  from            B_KPI_ALLOCATE_DETAILS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ALLOCATE_DETAILS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ALLOCATE_DETAILS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ALLOCATE_DETAILS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ALLOCATE_DETAILS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ALLOCATE_DETAILS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ALLOCATE_DETAILS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ALLOCATE_DETAILS.MODIFIED_USER_ID
  left outer join B_KPI_ALLOCATE_DETAILS_CSTM
               on B_KPI_ALLOCATE_DETAILS_CSTM.ID_C     = B_KPI_ALLOCATE_DETAILS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ALLOCATE_DETAILS.ID
 where B_KPI_ALLOCATE_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS_Edit')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS_Edit;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS_Edit
as
select *
  from vwB_KPI_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATE_DETAILS_List')
	Drop View dbo.vwB_KPI_ALLOCATE_DETAILS_List;
GO


Create View dbo.vwB_KPI_ALLOCATE_DETAILS_List
as
select *
  from vwB_KPI_ALLOCATE_DETAILS

GO

Grant Select on dbo.vwB_KPI_ALLOCATE_DETAILS_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATE_DETAILS
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Undelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'B_KPI_ALLOCATE_DETAILS';
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATE_DETAILS
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ALLOCATE_DETAILS_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_ALLOCATE_ID                    uniqueidentifier
	, @ALLOCATE_CODE                      nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @EMPLOYEE_ID                        uniqueidentifier
	, @MA_NHAN_VIEN                       nvarchar(50)
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @LEVEL_NUMBER                       int
	, @KPI_UNIT                           int
	, @UNIT                               int
	, @RADIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @KPI_GROUP_DETAIL_ID                uniqueidentifier
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @TOTAL_VALUE                        float
	, @MONTH_1                            float
	, @MONTH_2                            float
	, @MONTH_3                            float
	, @MONTH_4                            float
	, @MONTH_5                            float
	, @MONTH_6                            float
	, @MONTH_7                            float
	, @MONTH_8                            float
	, @MONTH_9                            float
	, @MONTH_10                           float
	, @MONTH_11                           float
	, @MONTH_12                           float
	, @FLEX1                              nvarchar(1000)
	, @FLEX2                              nvarchar(1000)
	, @FLEX3                              nvarchar(1000)
	, @FLEX4                              nvarchar(1000)
	, @FLEX5                              nvarchar(1000)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ALLOCATE_DETAILS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ALLOCATE_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_ALLOCATE_ID                    
			, ALLOCATE_CODE                      
			, VERSION_NUMBER                     
			, EMPLOYEE_ID                        
			, MA_NHAN_VIEN                       
			, KPI_CODE                           
			, KPI_NAME                           
			, LEVEL_NUMBER                       
			, KPI_UNIT                           
			, UNIT                               
			, RADIO                              
			, MAX_RATIO_COMPLETE                 
			, KPI_GROUP_DETAIL_ID                
			, DESCRIPTION                        
			, REMARK                             
			, TOTAL_VALUE                        
			, MONTH_1                            
			, MONTH_2                            
			, MONTH_3                            
			, MONTH_4                            
			, MONTH_5                            
			, MONTH_6                            
			, MONTH_7                            
			, MONTH_8                            
			, MONTH_9                            
			, MONTH_10                           
			, MONTH_11                           
			, MONTH_12                           
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_ALLOCATE_ID                    
			, @ALLOCATE_CODE                      
			, @VERSION_NUMBER                     
			, @EMPLOYEE_ID                        
			, @MA_NHAN_VIEN                       
			, @KPI_CODE                           
			, @KPI_NAME                           
			, @LEVEL_NUMBER                       
			, @KPI_UNIT                           
			, @UNIT                               
			, @RADIO                              
			, @MAX_RATIO_COMPLETE                 
			, @KPI_GROUP_DETAIL_ID                
			, @DESCRIPTION                        
			, @REMARK                             
			, @TOTAL_VALUE                        
			, @MONTH_1                            
			, @MONTH_2                            
			, @MONTH_3                            
			, @MONTH_4                            
			, @MONTH_5                            
			, @MONTH_6                            
			, @MONTH_7                            
			, @MONTH_8                            
			, @MONTH_9                            
			, @MONTH_10                           
			, @MONTH_11                           
			, @MONTH_12                           
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ALLOCATE_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , KPI_ALLOCATE_ID                      = @KPI_ALLOCATE_ID                    
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , EMPLOYEE_ID                          = @EMPLOYEE_ID                        
		     , MA_NHAN_VIEN                         = @MA_NHAN_VIEN                       
		     , KPI_CODE                             = @KPI_CODE                           
		     , KPI_NAME                             = @KPI_NAME                           
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , KPI_UNIT                             = @KPI_UNIT                           
		     , UNIT                                 = @UNIT                               
		     , RADIO                                = @RADIO                              
		     , MAX_RATIO_COMPLETE                   = @MAX_RATIO_COMPLETE                 
		     , KPI_GROUP_DETAIL_ID                  = @KPI_GROUP_DETAIL_ID                
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , TOTAL_VALUE                          = @TOTAL_VALUE                        
		     , MONTH_1                              = @MONTH_1                            
		     , MONTH_2                              = @MONTH_2                            
		     , MONTH_3                              = @MONTH_3                            
		     , MONTH_4                              = @MONTH_4                            
		     , MONTH_5                              = @MONTH_5                            
		     , MONTH_6                              = @MONTH_6                            
		     , MONTH_7                              = @MONTH_7                            
		     , MONTH_8                              = @MONTH_8                            
		     , MONTH_9                              = @MONTH_9                            
		     , MONTH_10                             = @MONTH_10                           
		     , MONTH_11                             = @MONTH_11                           
		     , MONTH_12                             = @MONTH_12                           
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATE_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATE_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_KPI_ALLOCATE_DETAILS', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ALLOCATE_DETAILS_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ALLOCATE_DETAILS
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'B_KPI_ALLOCATE_DETAILS', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_KPI_ALLOCATE_DETAILS', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ALLOCATE_DETAILS
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATE_DETAILS_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ALLOCATE_DETAILS_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ALLOCATE_DETAILS_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATE_DETAILS_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATE_DETAILS';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATE_DETAILS';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'B_KPI_ALLOCATE_DETAILS.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'B_KPI_ALLOCATE_DETAILS.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'B_KPI_ALLOCATE_DETAILS.DetailView'   , 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 1, 'B_KPI_ALLOCATE_DETAILS.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 3, 'B_KPI_ALLOCATE_DETAILS.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 4, 'B_KPI_ALLOCATE_DETAILS.LBL_MA_NHAN_VIEN', 'MA_NHAN_VIEN', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 5, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_CODE', 'KPI_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 6, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_NAME', 'KPI_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 7, 'B_KPI_ALLOCATE_DETAILS.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 8, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_UNIT', 'KPI_UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 9, 'B_KPI_ALLOCATE_DETAILS.LBL_UNIT', 'UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 10, 'B_KPI_ALLOCATE_DETAILS.LBL_RADIO', 'RADIO', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 11, 'B_KPI_ALLOCATE_DETAILS.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 12, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_GROUP_DETAIL_ID', 'KPI_GROUP_DETAIL_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 13, 'B_KPI_ALLOCATE_DETAILS.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 14, 'B_KPI_ALLOCATE_DETAILS.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 15, 'B_KPI_ALLOCATE_DETAILS.LBL_TOTAL_VALUE', 'TOTAL_VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 16, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_1', 'MONTH_1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 17, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_2', 'MONTH_2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 18, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_3', 'MONTH_3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 19, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_4', 'MONTH_4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 20, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_5', 'MONTH_5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 21, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_6', 'MONTH_6', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 22, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_7', 'MONTH_7', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 23, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_8', 'MONTH_8', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 24, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_9', 'MONTH_9', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 25, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_10', 'MONTH_10', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 26, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_11', 'MONTH_11', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 27, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_12', 'MONTH_12', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 28, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX1', 'FLEX1', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 29, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX2', 'FLEX2', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 30, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX3', 'FLEX3', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 31, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX4', 'FLEX4', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 32, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX5', 'FLEX5', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 33, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 34, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 35, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'B_KPI_ALLOCATE_DETAILS.DetailView', 36, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'B_KPI_ALLOCATE_DETAILS.DetailView', 'B_KPI_ALLOCATE_DETAILS';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'B_KPI_ALLOCATE_DETAILS.EditView'  , 'B_KPI_ALLOCATE_DETAILS';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'B_KPI_ALLOCATE_DETAILS.PopupView' , 'B_KPI_ALLOCATE_DETAILS';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'B_KPI_ALLOCATE_DETAILS.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'B_KPI_ALLOCATE_DETAILS.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'B_KPI_ALLOCATE_DETAILS.EditView', 'B_KPI_ALLOCATE_DETAILS'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 1, 'B_KPI_ALLOCATE_DETAILS.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView', 3, 'B_KPI_ALLOCATE_DETAILS.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 4, 'B_KPI_ALLOCATE_DETAILS.LBL_MA_NHAN_VIEN', 'MA_NHAN_VIEN', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 5, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 6, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 7, 'B_KPI_ALLOCATE_DETAILS.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 8, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 9, 'B_KPI_ALLOCATE_DETAILS.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 10, 'B_KPI_ALLOCATE_DETAILS.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 11, 'B_KPI_ALLOCATE_DETAILS.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView', 12, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_GROUP_DETAIL_ID', 'KPI_GROUP_DETAIL_ID', 0, 1, 'KPI_GROUP_DETAIL_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.EditView', 13, 'B_KPI_ALLOCATE_DETAILS.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.EditView', 14, 'B_KPI_ALLOCATE_DETAILS.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 15, 'B_KPI_ALLOCATE_DETAILS.LBL_TOTAL_VALUE', 'TOTAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 16, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 17, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 18, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 19, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 20, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 21, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 22, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 23, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 24, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 25, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 26, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 27, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 28, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX1', 'FLEX1', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 29, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX2', 'FLEX2', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 30, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX3', 'FLEX3', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 31, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX4', 'FLEX4', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView', 32, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX5', 'FLEX5', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.EditView', 33, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.EditView', 34, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 'B_KPI_ALLOCATE_DETAILS'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 1, 'B_KPI_ALLOCATE_DETAILS.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 3, 'B_KPI_ALLOCATE_DETAILS.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 4, 'B_KPI_ALLOCATE_DETAILS.LBL_MA_NHAN_VIEN', 'MA_NHAN_VIEN', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 5, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 6, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 7, 'B_KPI_ALLOCATE_DETAILS.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 8, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 9, 'B_KPI_ALLOCATE_DETAILS.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 10, 'B_KPI_ALLOCATE_DETAILS.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 11, 'B_KPI_ALLOCATE_DETAILS.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 12, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_GROUP_DETAIL_ID', 'KPI_GROUP_DETAIL_ID', 0, 1, 'KPI_GROUP_DETAIL_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 13, 'B_KPI_ALLOCATE_DETAILS.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 14, 'B_KPI_ALLOCATE_DETAILS.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 15, 'B_KPI_ALLOCATE_DETAILS.LBL_TOTAL_VALUE', 'TOTAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 16, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 17, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 18, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 19, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 20, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 21, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 22, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 23, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 24, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 25, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 26, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 27, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 28, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX1', 'FLEX1', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 29, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX2', 'FLEX2', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 30, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX3', 'FLEX3', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 31, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX4', 'FLEX4', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 32, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX5', 'FLEX5', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 33, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.EditView.Inline', 34, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 'B_KPI_ALLOCATE_DETAILS'      , 'vwB_KPI_ALLOCATE_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 1, 'B_KPI_ALLOCATE_DETAILS.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 3, 'B_KPI_ALLOCATE_DETAILS.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 4, 'B_KPI_ALLOCATE_DETAILS.LBL_MA_NHAN_VIEN', 'MA_NHAN_VIEN', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 5, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 6, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 7, 'B_KPI_ALLOCATE_DETAILS.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 8, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 9, 'B_KPI_ALLOCATE_DETAILS.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 10, 'B_KPI_ALLOCATE_DETAILS.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 11, 'B_KPI_ALLOCATE_DETAILS.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 12, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_GROUP_DETAIL_ID', 'KPI_GROUP_DETAIL_ID', 0, 1, 'KPI_GROUP_DETAIL_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 13, 'B_KPI_ALLOCATE_DETAILS.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 14, 'B_KPI_ALLOCATE_DETAILS.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 15, 'B_KPI_ALLOCATE_DETAILS.LBL_TOTAL_VALUE', 'TOTAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 16, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 17, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 18, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 19, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 20, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 21, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 22, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 23, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 24, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 25, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 26, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 27, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 28, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX1', 'FLEX1', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 29, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX2', 'FLEX2', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 30, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX3', 'FLEX3', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 31, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX4', 'FLEX4', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 32, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX5', 'FLEX5', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 33, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'B_KPI_ALLOCATE_DETAILS.PopupView.Inline', 34, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'B_KPI_ALLOCATE_DETAILS.SearchBasic'    , 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_KPI_ALLOCATE_DETAILS.SearchBasic', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'B_KPI_ALLOCATE_DETAILS.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'B_KPI_ALLOCATE_DETAILS.SearchAdvanced' , 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 1, 'B_KPI_ALLOCATE_DETAILS.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 2, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 3, 'B_KPI_ALLOCATE_DETAILS.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 4, 'B_KPI_ALLOCATE_DETAILS.LBL_MA_NHAN_VIEN', 'MA_NHAN_VIEN', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 5, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_CODE', 'KPI_CODE', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 6, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_NAME', 'KPI_NAME', 0, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 7, 'B_KPI_ALLOCATE_DETAILS.LBL_LEVEL_NUMBER', 'LEVEL_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 8, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_UNIT', 'KPI_UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 9, 'B_KPI_ALLOCATE_DETAILS.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 10, 'B_KPI_ALLOCATE_DETAILS.LBL_RADIO', 'RADIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 11, 'B_KPI_ALLOCATE_DETAILS.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 12, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_GROUP_DETAIL_ID', 'KPI_GROUP_DETAIL_ID', 0, 1, 'KPI_GROUP_DETAIL_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 13, 'B_KPI_ALLOCATE_DETAILS.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 14, 'B_KPI_ALLOCATE_DETAILS.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 15, 'B_KPI_ALLOCATE_DETAILS.LBL_TOTAL_VALUE', 'TOTAL_VALUE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 16, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_1', 'MONTH_1', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 17, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_2', 'MONTH_2', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 18, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_3', 'MONTH_3', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 19, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_4', 'MONTH_4', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 20, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_5', 'MONTH_5', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 21, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_6', 'MONTH_6', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 22, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_7', 'MONTH_7', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 23, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_8', 'MONTH_8', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 24, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_9', 'MONTH_9', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 25, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_10', 'MONTH_10', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 26, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_11', 'MONTH_11', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 27, 'B_KPI_ALLOCATE_DETAILS.LBL_MONTH_12', 'MONTH_12', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 28, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX1', 'FLEX1', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 29, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX2', 'FLEX2', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 30, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX3', 'FLEX3', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 31, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX4', 'FLEX4', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'B_KPI_ALLOCATE_DETAILS.SearchAdvanced', 32, 'B_KPI_ALLOCATE_DETAILS.LBL_FLEX5', 'FLEX5', 0, 1, 1000, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'B_KPI_ALLOCATE_DETAILS.SearchAdvanced' , 33, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS B_KPI_ALLOCATE_DETAILS.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'B_KPI_ALLOCATE_DETAILS.SearchPopup'    , 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'B_KPI_ALLOCATE_DETAILS.SearchPopup', 0, 'B_KPI_ALLOCATE_DETAILS.LBL_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_ID', 1, 1, 'KPI_ALLOCATE_NAME', 'return B_KPI_ALLOCATE_DETAILPopup();', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_KPI_ALLOCATE_DETAILS.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_KPI_ALLOCATE_DETAILS.Export', 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.Export'         ,  1, 'B_KPI_ALLOCATE_DETAILS.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_KPI_ALLOCATE_DETAILS.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_KPI_ALLOCATE_DETAILS.ListView', 'B_KPI_ALLOCATE_DETAILS'      , 'vwB_KPI_ALLOCATE_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.ListView', 2, 'B_KPI_ALLOCATE_DETAILS.LBL_LIST_KPI_ALLOCATE_ID', 'KPI_ALLOCATE_NAME', 'KPI_ALLOCATE_NAME', '20%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_KPI_ALLOCATE_DETAILS.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_KPI_ALLOCATE_DETAILS.PopupView', 'B_KPI_ALLOCATE_DETAILS'      , 'vwB_KPI_ALLOCATE_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.PopupView', 1, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'B_KPI_ALLOCATE_DETAILS.PopupView', 2, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'B_KPI_ALLOCATE_DETAILS.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS B_KPI_ALLOCATE_DETAILS.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'B_KPI_ALLOCATE_DETAILS.SearchDuplicates', 'B_KPI_ALLOCATE_DETAILS', 'vwB_KPI_ALLOCATE_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'B_KPI_ALLOCATE_DETAILS.SearchDuplicates'          , 1, 'B_KPI_ALLOCATE_DETAILS.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/B_KPI_ALLOCATE_DETAILS/view.aspx?id={0}', null, 'B_KPI_ALLOCATE_DETAILS', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'B_KPI_ALLOCATE_DETAILS', '.moduleList.B_KPI_ALLOCATE_DETAILS', '~/B_KPI_ALLOCATE_DETAILS/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ALLOCATE_DETAILS', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'B_KPI_ALLOCATE_DETAILS';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'B_KPI_ALLOCATE_DETAILS' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_KPI_ALLOCATE_DETAILS', 'B_KPI_ALLOCATE_DETAILS.LNK_NEW_B_KPI_ALLOCATE_DETAIL' , '~/B_KPI_ALLOCATE_DETAILS/edit.aspx'   , 'CreateB_KPI_ALLOCATE_DETAILS.gif', 1,  1, 'B_KPI_ALLOCATE_DETAILS', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_KPI_ALLOCATE_DETAILS', 'B_KPI_ALLOCATE_DETAILS.LNK_B_KPI_ALLOCATE_DETAIL_LIST', '~/B_KPI_ALLOCATE_DETAILS/default.aspx', 'B_KPI_ALLOCATE_DETAILS.gif'      , 1,  2, 'B_KPI_ALLOCATE_DETAILS', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_KPI_ALLOCATE_DETAILS', '.LBL_IMPORT'              , '~/B_KPI_ALLOCATE_DETAILS/import.aspx' , 'Import.gif'        , 1,  3, 'B_KPI_ALLOCATE_DETAILS', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'B_KPI_ALLOCATE_DETAILS', '.LNK_ACTIVITY_STREAM'     , '~/B_KPI_ALLOCATE_DETAILS/stream.aspx' , 'ActivityStream.gif', 1,  4, 'B_KPI_ALLOCATE_DETAILS', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_KPI_ALLOCATE_DETAIL List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'Create B_KPI_ALLOCATE_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ALLOCATE_DETAIL_LIST'                          , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_KPI_ALLOCATE_DETAILS';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ALLOCATE_DETAIL'                           , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'Create B_KPI_ALLOCATE_DETAIL';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_KPI_ALLOCATE_DETAIL Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ALLOCATE_DETAIL_NOT_FOUND'                     , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_KPI_ALLOCATE_DETAIL not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ALLOCATE_DETAIL_CONFIRMATION'           , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_KPI_ALLOCATE_DETAILS';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'B_KPI_ALLOCATE_DETAILS', null, null, N'B_K';

exec dbo.spTERMINOLOGY_InsertOnly N'B_KPI_ALLOCATE_DETAILS'                                          , N'en-US', null, N'moduleList', 100, N'B_KPI_ALLOCATE_DETAILS';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_ALLOCATE_ID'                                   , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi allocate_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_ALLOCATE_ID'                              , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi allocate_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_CODE'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'allocate code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_CODE'                                , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'allocate code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'version number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'version number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_EMPLOYEE_ID'                                       , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'employee id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_EMPLOYEE_ID'                                  , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'employee id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MA_NHAN_VIEN'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'ma nhan_vien:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MA_NHAN_VIEN'                                 , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'ma nhan_vien';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_CODE'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_CODE'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_NAME'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_NAME'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LEVEL_NUMBER'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'level number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_LEVEL_NUMBER'                                 , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'level number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_UNIT'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_UNIT'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_UNIT'                                              , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_UNIT'                                         , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RADIO'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'radio:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RADIO'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'radio';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MAX_RATIO_COMPLETE'                                , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'max ratio_complete:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MAX_RATIO_COMPLETE'                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'max ratio_complete';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_GROUP_DETAIL_ID'                               , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi group_detail_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_GROUP_DETAIL_ID'                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'kpi group_detail_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'remark';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TOTAL_VALUE'                                       , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'total value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TOTAL_VALUE'                                  , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'total value';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_1'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_1'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_2'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_2'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_3'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_3'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_4'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_4'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_5'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_5'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 5';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_6'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 6:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_6'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 6';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_7'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 7:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_7'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 7';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_8'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 8:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_8'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 8';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_9'                                           , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 9:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_9'                                      , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 9';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_10'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 10:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_10'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 10';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_11'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 11:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_11'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 11';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MONTH_12'                                          , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 12:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MONTH_12'                                     , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'Month 12';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX1'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex1:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX1'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex1';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX2'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex2:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX2'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex2';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX3'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex3:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX3'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex3';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX4'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex4:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX4'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex4';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FLEX5'                                             , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex5:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FLEX5'                                        , 'en-US', 'B_KPI_ALLOCATE_DETAILS', null, null, 'flex5';






