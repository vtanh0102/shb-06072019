
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATES' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATES';
	Create Table dbo.B_KPI_ALLOCATES
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_B_KPI_ALLOCATES primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, NAME                               nvarchar(150) not null
		, ALLOCATE_CODE                      nvarchar(50) not null
		, ALLOCATE_NAME                      nvarchar(200) not null
		, YEAR                               int null
		, PERIOD                             nvarchar(5) null
		, VERSION_NUMBER                     nvarchar(50) null
		, APPROVE_STATUS                     nvarchar(5) null
		, APPROVED_BY                        uniqueidentifier null
		, APPROVED_DATE                      datetime null
		, ALLOCATE_TYPE                      nvarchar(5) null
		, ORGANIZATION_ID                    uniqueidentifier null
		, ORGANIZATION_CODE					 nvarchar(50) null -- ORGANIZATION_CODE
		, EMPLOYEE_ID                        uniqueidentifier null
		, MA_NHAN_VIEN						 nvarchar(50) null ---Ma nhan vien
		, TOTAL_PLAN_PERCENT                 float default 0   ---TOTAL_PLAN_PERCENT
		, TOTAL_PLAN_VALUE				     float default 0   ---TOTAL_PLAN_VALUE
		, KPI_GROUP_ID						 uniqueidentifier default(newid()) ---KPI_GROUP_ID
		, STATUS                             nvarchar(5) null
		, KPI_STANDARD_ID                    uniqueidentifier null
		, FILE_ID                            uniqueidentifier null
		, ASSIGN_BY                          uniqueidentifier null
		, ASSIGN_DATE                        datetime null
		, STAFT_NUMBER                       int null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, POSITION_ID                        uniqueidentifier null
		, AREA_ID	                         uniqueidentifier null
		, FLEX1								 text null
		, FLEX2								 text null
		, FLEX3								 text null
		, FLEX4								 text null
		, FLEX5								 text null

		)

	create index IDX_B_KPI_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATES_TEAM_ID          on dbo.B_KPI_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_B_KPI_ALLOCATES_NAME  on dbo.B_KPI_ALLOCATES (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'B_KPI_ALLOCATES_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.B_KPI_ALLOCATES_CSTM';
	Create Table dbo.B_KPI_ALLOCATES_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_B_KPI_ALLOCATES_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_ASSIGNED_USER_ID on dbo.B_KPI_ALLOCATES (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add TEAM_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add TEAM_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_TEAM_ID          on dbo.B_KPI_ALLOCATES (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add TEAM_SET_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add TEAM_SET_ID uniqueidentifier null;
	create index IDX_B_KPI_ALLOCATES_TEAM_SET_ID      on dbo.B_KPI_ALLOCATES (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table B_KPI_ALLOCATES add NAME nvarchar(150) null';
	alter table B_KPI_ALLOCATES add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_CODE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_CODE nvarchar(50) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_NAME') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_NAME nvarchar(200) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'YEAR') begin -- then
	print 'alter table B_KPI_ALLOCATES add YEAR int null';
	alter table B_KPI_ALLOCATES add YEAR int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'PERIOD') begin -- then
	print 'alter table B_KPI_ALLOCATES add PERIOD nvarchar(5) null';
	alter table B_KPI_ALLOCATES add PERIOD nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATES add VERSION_NUMBER nvarchar(50) null';
	alter table B_KPI_ALLOCATES add VERSION_NUMBER nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVE_STATUS') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVE_STATUS nvarchar(5) null';
	alter table B_KPI_ALLOCATES add APPROVE_STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVED_BY') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVED_BY uniqueidentifier null';
	alter table B_KPI_ALLOCATES add APPROVED_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'APPROVED_DATE') begin -- then
	print 'alter table B_KPI_ALLOCATES add APPROVED_DATE datetime null';
	alter table B_KPI_ALLOCATES add APPROVED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ALLOCATE_TYPE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ALLOCATE_TYPE nvarchar(5) null';
	alter table B_KPI_ALLOCATES add ALLOCATE_TYPE nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ORGANIZATION_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add ORGANIZATION_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ORGANIZATION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'EMPLOYEE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add EMPLOYEE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add EMPLOYEE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'STATUS') begin -- then
	print 'alter table B_KPI_ALLOCATES add STATUS nvarchar(5) null';
	alter table B_KPI_ALLOCATES add STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'KPI_STANDARD_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add KPI_STANDARD_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add KPI_STANDARD_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FILE_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add FILE_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add FILE_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGN_BY') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGN_BY uniqueidentifier null';
	alter table B_KPI_ALLOCATES add ASSIGN_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ASSIGN_DATE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ASSIGN_DATE datetime null';
	alter table B_KPI_ALLOCATES add ASSIGN_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'STAFT_NUMBER') begin -- then
	print 'alter table B_KPI_ALLOCATES add STAFT_NUMBER int null';
	alter table B_KPI_ALLOCATES add STAFT_NUMBER int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table B_KPI_ALLOCATES add DESCRIPTION nvarchar(max) null';
	alter table B_KPI_ALLOCATES add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table B_KPI_ALLOCATES add REMARK nvarchar(max) null';
	alter table B_KPI_ALLOCATES add REMARK nvarchar(max) null;
end -- if;
--------------------ADD NEW COLUMN--------------------------
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'ORGANIZATION_CODE') begin -- then
	print 'alter table B_KPI_ALLOCATES add ORGANIZATION_CODE nvarchar(50) null';
	alter table B_KPI_ALLOCATES add ORGANIZATION_CODE nvarchar(50) null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'MA_NHAN_VIEN') begin -- then
	print 'alter table B_KPI_ALLOCATES add MA_NHAN_VIEN nvarchar(50) null';
	alter table B_KPI_ALLOCATES add MA_NHAN_VIEN nvarchar(50) null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TOTAL_PLAN_PERCENT') begin -- then
	print 'alter table B_KPI_ALLOCATES add TOTAL_PLAN_PERCENT FLOAT default 0';
	alter table B_KPI_ALLOCATES add TOTAL_PLAN_PERCENT float default 0;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'TOTAL_PLAN_VALUE') begin -- then
	print 'alter table B_KPI_ALLOCATES add TOTAL_PLAN_VALUE float default 0';
	alter table B_KPI_ALLOCATES add TOTAL_PLAN_VALUE float default 0;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'KPI_GROUP_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add KPI_GROUP_ID uniqueidentifier default(newid())';
	alter table B_KPI_ALLOCATES add KPI_GROUP_ID uniqueidentifier default(newid());
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FLEX1') begin -- then
	print 'alter table B_KPI_ALLOCATES add FLEX1 text null';
	alter table B_KPI_ALLOCATES add FLEX1 text null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FLEX2') begin -- then
	print 'alter table B_KPI_ALLOCATES add FLEX2 text null';
	alter table B_KPI_ALLOCATES add FLEX2 text null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FLEX3') begin -- then
	print 'alter table B_KPI_ALLOCATES add FLEX3 text null';
	alter table B_KPI_ALLOCATES add FLEX3 text null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FLEX4') begin -- then
	print 'alter table B_KPI_ALLOCATES add FLEX4 text null';
	alter table B_KPI_ALLOCATES add FLEX4 text null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'FLEX5') begin -- then
	print 'alter table B_KPI_ALLOCATES add FLEX5 text null';
	alter table B_KPI_ALLOCATES add FLEX5 text null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'POSITION_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add POSITION_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add POSITION_ID uniqueidentifier null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'B_KPI_ALLOCATES' and COLUMN_NAME = 'AREA_ID') begin -- then
	print 'alter table B_KPI_ALLOCATES add AREA_ID uniqueidentifier null';
	alter table B_KPI_ALLOCATES add AREA_ID uniqueidentifier null;
end -- if;

--------------------------------END ADD------------------------------
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES')
	Drop View dbo.vwB_KPI_ALLOCATES;
GO


Create View dbo.vwB_KPI_ALLOCATES
as
select B_KPI_ALLOCATES.ID
     , B_KPI_ALLOCATES.NAME
     , B_KPI_ALLOCATES.ALLOCATE_CODE
     , B_KPI_ALLOCATES.ALLOCATE_NAME
     , B_KPI_ALLOCATES.YEAR
     , B_KPI_ALLOCATES.PERIOD
     , B_KPI_ALLOCATES.VERSION_NUMBER
     , B_KPI_ALLOCATES.APPROVE_STATUS
     , B_KPI_ALLOCATES.APPROVED_BY
     , B_KPI_ALLOCATES.APPROVED_DATE
     , B_KPI_ALLOCATES.ALLOCATE_TYPE
     , B_KPI_ALLOCATES.ORGANIZATION_ID
     , B_KPI_ALLOCATES.EMPLOYEE_ID
     , B_KPI_ALLOCATES.STATUS
     , B_KPI_ALLOCATES.KPI_STANDARD_ID
     , B_KPI_ALLOCATES.FILE_ID
     , B_KPI_ALLOCATES.ASSIGN_BY
     , B_KPI_ALLOCATES.ASSIGN_DATE
     , B_KPI_ALLOCATES.STAFT_NUMBER
     , B_KPI_ALLOCATES.DESCRIPTION
     , B_KPI_ALLOCATES.REMARK
     , B_KPI_ALLOCATES.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

	 , B_KPI_ALLOCATES.ORGANIZATION_CODE
	 , B_KPI_ALLOCATES.MA_NHAN_VIEN
	 , B_KPI_ALLOCATES.TOTAL_PLAN_PERCENT
	 , B_KPI_ALLOCATES.TOTAL_PLAN_VALUE
	 , B_KPI_ALLOCATES.KPI_GROUP_ID
	 , B_KPI_ALLOCATES.POSITION_ID
	 , B_KPI_ALLOCATES.AREA_ID
	 , B_KPI_ALLOCATES.FLEX1
	 , B_KPI_ALLOCATES.FLEX2
	 , B_KPI_ALLOCATES.FLEX3
	 , B_KPI_ALLOCATES.FLEX4
	 , B_KPI_ALLOCATES.FLEX5

     , B_KPI_ALLOCATES.DATE_ENTERED
     , B_KPI_ALLOCATES.DATE_MODIFIED
     , B_KPI_ALLOCATES.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , B_KPI_ALLOCATES.CREATED_BY      as CREATED_BY_ID
     , B_KPI_ALLOCATES.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , B_KPI_ALLOCATES_CSTM.*
  from            B_KPI_ALLOCATES
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = B_KPI_ALLOCATES.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = B_KPI_ALLOCATES.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = B_KPI_ALLOCATES.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = B_KPI_ALLOCATES.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = B_KPI_ALLOCATES.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = B_KPI_ALLOCATES.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = B_KPI_ALLOCATES.MODIFIED_USER_ID
  left outer join B_KPI_ALLOCATES_CSTM
               on B_KPI_ALLOCATES_CSTM.ID_C     = B_KPI_ALLOCATES.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = B_KPI_ALLOCATES.ID
 where B_KPI_ALLOCATES.DELETED = 0

GO

Grant Select on dbo.vwB_KPI_ALLOCATES to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES_Edit')
	Drop View dbo.vwB_KPI_ALLOCATES_Edit;
GO


Create View dbo.vwB_KPI_ALLOCATES_Edit
as
select *
  from vwB_KPI_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ALLOCATES_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwB_KPI_ALLOCATES_List')
	Drop View dbo.vwB_KPI_ALLOCATES_List;
GO


Create View dbo.vwB_KPI_ALLOCATES_List
as
select *
  from vwB_KPI_ALLOCATES

GO

Grant Select on dbo.vwB_KPI_ALLOCATES_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Delete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATES
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Undelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIB0203';
	
	-- BEGIN Oracle Exception
		update B_KPI_ALLOCATES
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from B_KPI_ALLOCATES_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Update;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @ALLOCATE_NAME                      nvarchar(200)
	, @YEAR                               int
	, @PERIOD                             nvarchar(5)
	, @VERSION_NUMBER                     nvarchar(50)
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @ALLOCATE_TYPE                      nvarchar(5)
	, @ORGANIZATION_ID                    uniqueidentifier
	, @EMPLOYEE_ID                        uniqueidentifier
	, @STATUS                             nvarchar(5)
	, @KPI_STANDARD_ID                    uniqueidentifier
	, @FILE_ID                            uniqueidentifier
	, @ASSIGN_BY                          uniqueidentifier
	, @ASSIGN_DATE                        datetime
	, @STAFT_NUMBER                       int
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)

	, @ORGANIZATION_CODE				  nvarchar(50)
	, @MA_NHAN_VIEN						  nvarchar(50)
	, @TOTAL_PLAN_PERCENT				  float
	, @TOTAL_PLAN_VALUE					  float
	, @KPI_GROUP_ID						  uniqueidentifier
	, @POSITION_ID						  uniqueidentifier
	, @AREA_ID						  uniqueidentifier
	, @FLEX1							 text
	, @FLEX2							 text
	, @FLEX3							 text
	, @FLEX4							 text
	, @FLEX5							 text	
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ALLOCATES where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ALLOCATES
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ALLOCATE_CODE                      
			, ALLOCATE_NAME                      
			, YEAR                               
			, PERIOD                             
			, VERSION_NUMBER                     
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, ALLOCATE_TYPE                      
			, ORGANIZATION_ID                    
			, EMPLOYEE_ID                        
			, STATUS                             
			, KPI_STANDARD_ID                    
			, FILE_ID                            
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, STAFT_NUMBER                       
			, DESCRIPTION                        
			, REMARK                             
			 , ORGANIZATION_CODE
			 , MA_NHAN_VIEN
			 , TOTAL_PLAN_PERCENT
			 , TOTAL_PLAN_VALUE
			 , KPI_GROUP_ID
			 , POSITION_ID
			 , AREA_ID
			 , FLEX1
			 , FLEX2
			 , FLEX3
			 , FLEX4
			 , FLEX5
			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @ALLOCATE_CODE                      
			, @ALLOCATE_NAME                      
			, @YEAR                               
			, @PERIOD                             
			, @VERSION_NUMBER                     
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @ALLOCATE_TYPE                      
			, @ORGANIZATION_ID                    
			, @EMPLOYEE_ID                        
			, @STATUS                             
			, @KPI_STANDARD_ID                    
			, @FILE_ID                            
			, @ASSIGN_BY                          
			, @ASSIGN_DATE                        
			, @STAFT_NUMBER                       
			, @DESCRIPTION                        
			, @REMARK                             
			 , @ORGANIZATION_CODE
			 , @MA_NHAN_VIEN
			 , @TOTAL_PLAN_PERCENT
			 , @TOTAL_PLAN_VALUE
			 , @KPI_GROUP_ID
			 , @POSITION_ID
			 , @AREA_ID
			 , @FLEX1
			 , @FLEX2
			 , @FLEX3
			 , @FLEX4
			 , @FLEX5
			);
	end else begin
		update B_KPI_ALLOCATES
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , ALLOCATE_NAME                        = @ALLOCATE_NAME                      
		     , YEAR                                 = @YEAR                               
		     , PERIOD                               = @PERIOD                             
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , ALLOCATE_TYPE                        = @ALLOCATE_TYPE                      
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID                    
		     , EMPLOYEE_ID                          = @EMPLOYEE_ID                        
		     , STATUS                               = @STATUS                             
		     , KPI_STANDARD_ID                      = @KPI_STANDARD_ID                    
		     , FILE_ID                              = @FILE_ID                            
		     , ASSIGN_BY                            = @ASSIGN_BY                          
		     , ASSIGN_DATE                          = @ASSIGN_DATE                        
		     , STAFT_NUMBER                         = @STAFT_NUMBER                       
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK       
			 , ORGANIZATION_CODE					= @ORGANIZATION_CODE
			 , MA_NHAN_VIEN							= @MA_NHAN_VIEN
			 , TOTAL_PLAN_PERCENT					= @TOTAL_PLAN_PERCENT
			 , TOTAL_PLAN_VALUE						= @TOTAL_PLAN_VALUE
			 , KPI_GROUP_ID							= @KPI_GROUP_ID
			 , POSITION_ID							= @POSITION_ID
			 , AREA_ID								= @AREA_ID
			 , FLEX1								= @FLEX1
			 , FLEX2								= @FLEX2
			 , FLEX3								= @FLEX3
			 , FLEX4								= @FLEX4
			 , FLEX5								= @FLEX5                      

		 where ID                                   = @ID                                 ;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATES_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATES_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0203', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_MassDelete;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spB_KPI_ALLOCATES_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spB_KPI_ALLOCATES_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_MassUpdate;
GO


Create Procedure dbo.spB_KPI_ALLOCATES_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from B_KPI_ALLOCATES
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIB0203', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0203', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update B_KPI_ALLOCATES
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spB_KPI_ALLOCATES_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spB_KPI_ALLOCATES_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spB_KPI_ALLOCATES_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spB_KPI_ALLOCATES_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spB_KPI_ALLOCATES_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'B_KPI_ALLOCATES';
	exec dbo.spSqlBuildAuditTrigger 'B_KPI_ALLOCATES';
	exec dbo.spSqlBuildAuditView    'B_KPI_ALLOCATES';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0203.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIB0203.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIB0203.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIB0203.DetailView'   , 'KPIB0203', 'vwB_KPI_ALLOCATES_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 0, 'KPIB0203.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 1, 'KPIB0203.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 2, 'KPIB0203.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 3, 'KPIB0203.LBL_YEAR', 'YEAR', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 4, 'KPIB0203.LBL_PERIOD', 'PERIOD', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 5, 'KPIB0203.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 6, 'KPIB0203.LBL_APPROVE_STATUS', 'APPROVE_STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 7, 'KPIB0203.LBL_APPROVED_BY', 'APPROVED_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 8, 'KPIB0203.LBL_APPROVED_DATE', 'APPROVED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 9, 'KPIB0203.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 10, 'KPIB0203.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 11, 'KPIB0203.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 12, 'KPIB0203.LBL_STATUS', 'STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 13, 'KPIB0203.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 14, 'KPIB0203.LBL_FILE_ID', 'FILE_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 15, 'KPIB0203.LBL_ASSIGN_BY', 'ASSIGN_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 16, 'KPIB0203.LBL_ASSIGN_DATE', 'ASSIGN_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 17, 'KPIB0203.LBL_STAFT_NUMBER', 'STAFT_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 18, 'KPIB0203.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 19, 'KPIB0203.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 20, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 21, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 22, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 23, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 24, 'KPIB0203.LBL_POSITION_ID', 'POSITION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIB0203.DetailView', 25, 'KPIB0203.LBL_AREA_ID', 'AREA_ID', '{0}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIB0203.DetailView', 'KPIB0203';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIB0203.EditView'  , 'KPIB0203';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIB0203.PopupView' , 'KPIB0203';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0203.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIB0203.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIB0203.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIB0203.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0203.EditView', 'KPIB0203'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 1, 'KPIB0203.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 2, 'KPIB0203.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 3, 'KPIB0203.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 4, 'KPIB0203.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 5, 'KPIB0203.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 6, 'KPIB0203.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 7, 'KPIB0203.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.EditView', 8, 'KPIB0203.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 9, 'KPIB0203.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 10, 'KPIB0203.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 11, 'KPIB0203.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 12, 'KPIB0203.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 13, 'KPIB0203.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 0, 1, 'KPI_STANDARD_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 14, 'KPIB0203.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView', 15, 'KPIB0203.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.EditView', 16, 'KPIB0203.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView', 17, 'KPIB0203.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.EditView', 18, 'KPIB0203.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.EditView', 19, 'KPIB0203.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.EditView', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.EditView', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0203.EditView.Inline', 'KPIB0203'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 1, 'KPIB0203.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 2, 'KPIB0203.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 3, 'KPIB0203.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 4, 'KPIB0203.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 5, 'KPIB0203.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 6, 'KPIB0203.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 7, 'KPIB0203.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.EditView.Inline', 8, 'KPIB0203.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 9, 'KPIB0203.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 10, 'KPIB0203.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 11, 'KPIB0203.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 12, 'KPIB0203.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 13, 'KPIB0203.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 0, 1, 'KPI_STANDARD_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 14, 'KPIB0203.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.EditView.Inline', 15, 'KPIB0203.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.EditView.Inline', 16, 'KPIB0203.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.EditView.Inline', 17, 'KPIB0203.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.EditView.Inline', 18, 'KPIB0203.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.EditView.Inline', 19, 'KPIB0203.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.EditView.Inline', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.EditView.Inline', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIB0203.PopupView.Inline', 'KPIB0203'      , 'vwB_KPI_ALLOCATES_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 1, 'KPIB0203.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 2, 'KPIB0203.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 3, 'KPIB0203.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 4, 'KPIB0203.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 5, 'KPIB0203.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 6, 'KPIB0203.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 7, 'KPIB0203.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.PopupView.Inline', 8, 'KPIB0203.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 9, 'KPIB0203.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 10, 'KPIB0203.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 11, 'KPIB0203.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 12, 'KPIB0203.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 13, 'KPIB0203.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 0, 1, 'KPI_STANDARD_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 14, 'KPIB0203.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange      'KPIB0203.PopupView.Inline', 15, 'KPIB0203.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIB0203.PopupView.Inline', 16, 'KPIB0203.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIB0203.PopupView.Inline', 17, 'KPIB0203.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.PopupView.Inline', 18, 'KPIB0203.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIB0203.PopupView.Inline', 19, 'KPIB0203.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.PopupView.Inline', 20, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIB0203.PopupView.Inline', 21, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0203.SearchBasic'    , 'KPIB0203', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB0203.SearchBasic', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB0203', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0203.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0203.SearchAdvanced' , 'KPIB0203', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB0203.SearchAdvanced', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB0203', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 1, 'KPIB0203.LBL_ALLOCATE_CODE', 'ALLOCATE_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 2, 'KPIB0203.LBL_ALLOCATE_NAME', 'ALLOCATE_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 3, 'KPIB0203.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 4, 'KPIB0203.LBL_PERIOD', 'PERIOD', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 5, 'KPIB0203.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 6, 'KPIB0203.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 7, 'KPIB0203.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 'APPROVED_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0203.SearchAdvanced', 8, 'KPIB0203.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 9, 'KPIB0203.LBL_ALLOCATE_TYPE', 'ALLOCATE_TYPE', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 10, 'KPIB0203.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 'ORGANIZATION_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 11, 'KPIB0203.LBL_EMPLOYEE_ID', 'EMPLOYEE_ID', 0, 1, 'EMPLOYEE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 12, 'KPIB0203.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 13, 'KPIB0203.LBL_KPI_STANDARD_ID', 'KPI_STANDARD_ID', 0, 1, 'KPI_STANDARD_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 14, 'KPIB0203.LBL_FILE_ID', 'FILE_ID', 0, 1, 'FILE_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsChange       'KPIB0203.SearchAdvanced', 15, 'KPIB0203.LBL_ASSIGN_BY', 'ASSIGN_BY', 0, 1, 'ASSIGN_NAME', 'return KPIB0203Popup();', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIB0203.SearchAdvanced', 16, 'KPIB0203.LBL_ASSIGN_DATE', 'ASSIGN_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.SearchAdvanced', 17, 'KPIB0203.LBL_STAFT_NUMBER', 'STAFT_NUMBER', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0203.SearchAdvanced', 18, 'KPIB0203.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0203.SearchAdvanced', 19, 'KPIB0203.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB0203.SearchAdvanced' , 20, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0203.SearchPopup'    , 'KPIB0203', 'vwB_KPI_ALLOCATES_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPIB0203.SearchPopup', 0, 'KPIB0203.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPIB0203', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0203.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0203.Export', 'KPIB0203', 'vwB_KPI_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0203.Export'         ,  1, 'KPIB0203.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0203.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0203.ListView', 'KPIB0203'      , 'vwB_KPI_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0203.ListView', 2, 'KPIB0203.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPIB0203/view.aspx?id={0}', null, 'KPIB0203', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0203.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0203.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0203.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0203.PopupView', 'KPIB0203'      , 'vwB_KPI_ALLOCATES_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0203.PopupView', 1, 'KPIB0203.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectKPIB0203(''{0}'', ''{1}'');', null, 'KPIB0203', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0203.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIB0203.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIB0203.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIB0203.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIB0203.SearchDuplicates', 'KPIB0203', 'vwB_KPI_ALLOCATES_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIB0203.SearchDuplicates'          , 1, 'KPIB0203.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIB0203/view.aspx?id={0}', null, 'KPIB0203', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIB0203', '.moduleList.KPIB0203', '~/KPIB0203/', 1, 1, 100, 0, 1, 1, 1, 0, 'B_KPI_ALLOCATES', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIB0203';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIB0203' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0203', 'KPIB0203.LNK_NEW_B_KPI_ALLOCATE' , '~/KPIB0203/edit.aspx'   , 'CreateKPIB0203.gif', 1,  1, 'KPIB0203', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0203', 'KPIB0203.LNK_B_KPI_ALLOCATE_LIST', '~/KPIB0203/default.aspx', 'KPIB0203.gif'      , 1,  2, 'KPIB0203', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0203', '.LBL_IMPORT'              , '~/KPIB0203/import.aspx' , 'Import.gif'        , 1,  3, 'KPIB0203', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIB0203', '.LNK_ACTIVITY_STREAM'     , '~/KPIB0203/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIB0203', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIB0203', null, null, N'KPIB0203 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIB0203', null, null, N'Create KPIB0203';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_B_KPI_ALLOCATE_LIST'                          , N'en-US', N'KPIB0203', null, null, N'KPIB0203';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_B_KPI_ALLOCATE'                           , N'en-US', N'KPIB0203', null, null, N'Create KPIB0203';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIB0203', null, null, N'KPIB0203 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_B_KPI_ALLOCATE_NOT_FOUND'                     , N'en-US', N'KPIB0203', null, null, N'KPIB0203 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_B_KPI_ALLOCATE_CONFIRMATION'           , N'en-US', N'KPIB0203', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIB0203', null, null, N'KPIB0203';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIB0203', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIB0203'                                          , N'en-US', null, N'moduleList', 100, N'KPIB0203';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'KPIB0203', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'KPIB0203', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_CODE'                                     , 'en-US', 'KPIB0203', null, null, 'Allocate Code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_CODE'                                , 'en-US', 'KPIB0203', null, null, 'Allocate Code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_NAME'                                     , 'en-US', 'KPIB0203', null, null, 'Allocate Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_NAME'                                , 'en-US', 'KPIB0203', null, null, 'Allocate Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_YEAR'                                              , 'en-US', 'KPIB0203', null, null, 'Year:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_YEAR'                                         , 'en-US', 'KPIB0203', null, null, 'Year';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_PERIOD'                                            , 'en-US', 'KPIB0203', null, null, 'Period:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_PERIOD'                                       , 'en-US', 'KPIB0203', null, null, 'Period';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'KPIB0203', null, null, 'Version Number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'KPIB0203', null, null, 'Version Number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVE_STATUS'                                    , 'en-US', 'KPIB0203', null, null, 'Approve Status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVE_STATUS'                               , 'en-US', 'KPIB0203', null, null, 'Approve Status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_BY'                                       , 'en-US', 'KPIB0203', null, null, 'Approved By:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_BY'                                  , 'en-US', 'KPIB0203', null, null, 'Approved By';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_DATE'                                     , 'en-US', 'KPIB0203', null, null, 'Approved Date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_DATE'                                , 'en-US', 'KPIB0203', null, null, 'Approved Date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ALLOCATE_TYPE'                                     , 'en-US', 'KPIB0203', null, null, 'Allocate Type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ALLOCATE_TYPE'                                , 'en-US', 'KPIB0203', null, null, 'Allocate Type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_ID'                                   , 'en-US', 'KPIB0203', null, null, 'Organization:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_ID'                              , 'en-US', 'KPIB0203', null, null, 'Organization';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_EMPLOYEE_ID'                                       , 'en-US', 'KPIB0203', null, null, 'Employee:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_EMPLOYEE_ID'                                  , 'en-US', 'KPIB0203', null, null, 'Employee';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STATUS'                                            , 'en-US', 'KPIB0203', null, null, 'Status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STATUS'                                       , 'en-US', 'KPIB0203', null, null, 'Status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_STANDARD_ID'                                   , 'en-US', 'KPIB0203', null, null, 'KPI Standard:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_STANDARD_ID'                              , 'en-US', 'KPIB0203', null, null, 'KPI Standard';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_FILE_ID'                                           , 'en-US', 'KPIB0203', null, null, 'File:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_FILE_ID'                                      , 'en-US', 'KPIB0203', null, null, 'File';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_BY'                                         , 'en-US', 'KPIB0203', null, null, 'Assign By:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_BY'                                    , 'en-US', 'KPIB0203', null, null, 'Assign By';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ASSIGN_DATE'                                       , 'en-US', 'KPIB0203', null, null, 'Assign Date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ASSIGN_DATE'                                  , 'en-US', 'KPIB0203', null, null, 'Assign Date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STAFT_NUMBER'                                      , 'en-US', 'KPIB0203', null, null, 'Staft Number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STAFT_NUMBER'                                 , 'en-US', 'KPIB0203', null, null, 'Staft Number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIB0203', null, null, 'Description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIB0203', null, null, 'Description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIB0203', null, null, 'Remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIB0203', null, null, 'Remark';






