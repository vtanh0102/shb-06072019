USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spM_GROUP_KPI_MassApprove]    Script Date: 7/1/2018 1:31:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spM_GROUP_KPI_MassApprove]
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier

	, @APPROVED_BY  uniqueidentifier	
	, @APPROVE_STATUS     nvarchar(5)
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;
	

	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		
		update M_GROUP_KPIS
			set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			    , DATE_MODIFIED     =  getdate()
			    , DATE_MODIFIED_UTC =  getutcdate()
			    , APPROVED_BY  = @APPROVED_BY
			    , APPROVE_STATUS = isnull(@APPROVE_STATUS,'')
				, APPROVED_DATE = getdate()

			where ID                = @ID
			and DELETED           = 0;
	

	end -- while;
  end

GO


