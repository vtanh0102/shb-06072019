
-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_GROUP_KPIS_CSTM';
	exec dbo.spSqlBuildAuditTrigger 'M_GROUP_KPIS_CSTM';
	exec dbo.spSqlBuildAuditView    'M_GROUP_KPIS_CSTM';
end -- if;
GO


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_GROUP_KPI_DETAILS_CSTM';
	exec dbo.spSqlBuildAuditTrigger 'M_GROUP_KPI_DETAILS_CSTM';
	exec dbo.spSqlBuildAuditView    'M_GROUP_KPI_DETAILS_CSTM';
end -- if;
GO




