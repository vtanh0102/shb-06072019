
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPIS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_GROUP_KPIS';
	Create Table dbo.M_GROUP_KPIS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_M_GROUP_KPIS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, KPI_GROUP_CODE                     nvarchar(50) not null
		, KPI_GROUP_NAME                     nvarchar(200) not null
		, YEAR                               int null
		, VERSION_NUMBER                     nvarchar(10) null
		, APPROVE_STATUS                     nvarchar(5) null
		, APPROVED_BY                        uniqueidentifier null
		, APPROVED_DATE                      datetime null
		, POSITION_ID                        uniqueidentifier null
		, TYPE                               int null
		, ORGANIZATION_ID                    uniqueidentifier null
		, ORGANIZATION_CODE                  nvarchar(50) null
		, BUSINESS_CODE		                 nvarchar(50) null
		, IS_COMMON                          nvarchar(1) null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null
		, STATUS                             nvarchar(5) null

		)

	create index IDX_M_GROUP_KPIS_ASSIGNED_USER_ID on dbo.M_GROUP_KPIS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPIS_TEAM_ID          on dbo.M_GROUP_KPIS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPIS_TEAM_SET_ID      on dbo.M_GROUP_KPIS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPIS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_GROUP_KPIS_CSTM';
	Create Table dbo.M_GROUP_KPIS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_M_GROUP_KPIS_CSTM primary key
		)
  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and TABLE_TYPE = 'BASE TABLE')
  begin    
	print 'Create Table dbo.M_GROUP_KPI_DETAILS';
	Create Table dbo.M_GROUP_KPI_DETAILS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_M_GROUP_KPI_DETAILS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, KPI_ID                             uniqueidentifier null
		, KPI_CODE                           nvarchar(50) null
		, KPI_NAME                           nvarchar(200) not null
		, GROUP_KPI_ID                       nvarchar(50) not null
		, UNIT                               int null
		, RATIO                              float null
		, MAX_RATIO_COMPLETE                 float null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null

		)

	create index IDX_M_GROUP_KPI_DETAILS_ASSIGNED_USER_ID on dbo.M_GROUP_KPI_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_ID          on dbo.M_GROUP_KPI_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_SET_ID      on dbo.M_GROUP_KPI_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPI_DETAILS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_GROUP_KPI_DETAILS_CSTM';
	Create Table dbo.M_GROUP_KPI_DETAILS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_M_GROUP_KPI_DETAILS_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table M_GROUP_KPIS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table M_GROUP_KPIS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPIS_ASSIGNED_USER_ID on dbo.M_GROUP_KPIS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table M_GROUP_KPIS add TEAM_ID uniqueidentifier null';
	alter table M_GROUP_KPIS add TEAM_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPIS_TEAM_ID          on dbo.M_GROUP_KPIS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table M_GROUP_KPIS add TEAM_SET_ID uniqueidentifier null';
	alter table M_GROUP_KPIS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPIS_TEAM_SET_ID      on dbo.M_GROUP_KPIS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'KPI_GROUP_CODE') begin -- then
	print 'alter table M_GROUP_KPIS add KPI_GROUP_CODE nvarchar(50) null';
	alter table M_GROUP_KPIS add KPI_GROUP_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'KPI_GROUP_NAME') begin -- then
	print 'alter table M_GROUP_KPIS add KPI_GROUP_NAME nvarchar(200) null';
	alter table M_GROUP_KPIS add KPI_GROUP_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'YEAR') begin -- then
	print 'alter table M_GROUP_KPIS add YEAR int null';
	alter table M_GROUP_KPIS add YEAR int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'VERSION_NUMBER') begin -- then
	print 'alter table M_GROUP_KPIS add VERSION_NUMBER nvarchar(10) null';
	alter table M_GROUP_KPIS add VERSION_NUMBER nvarchar(10) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'APPROVE_STATUS') begin -- then
	print 'alter table M_GROUP_KPIS add APPROVE_STATUS nvarchar(5) null';
	alter table M_GROUP_KPIS add APPROVE_STATUS nvarchar(5) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'APPROVED_BY') begin -- then
	print 'alter table M_GROUP_KPIS add APPROVED_BY uniqueidentifier null';
	alter table M_GROUP_KPIS add APPROVED_BY uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'APPROVED_DATE') begin -- then
	print 'alter table M_GROUP_KPIS add APPROVED_DATE datetime null';
	alter table M_GROUP_KPIS add APPROVED_DATE datetime null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'POSITION_ID') begin -- then
	print 'alter table M_GROUP_KPIS add POSITION_ID uniqueidentifier null';
	alter table M_GROUP_KPIS add POSITION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'TYPE') begin -- then
	print 'alter table M_GROUP_KPIS add TYPE int null';
	alter table M_GROUP_KPIS add TYPE int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'ORGANIZATION_ID') begin -- then
	print 'alter table M_GROUP_KPIS add ORGANIZATION_ID uniqueidentifier null';
	alter table M_GROUP_KPIS add ORGANIZATION_ID uniqueidentifier null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'IS_COMMON') begin -- then
	print 'alter table M_GROUP_KPIS add IS_COMMON nvarchar(1) null';
	alter table M_GROUP_KPIS add IS_COMMON nvarchar(1) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table M_GROUP_KPIS add DESCRIPTION nvarchar(max) null';
	alter table M_GROUP_KPIS add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table M_GROUP_KPIS add REMARK nvarchar(max) null';
	alter table M_GROUP_KPIS add REMARK nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'STATUS') begin -- then
	print 'alter table M_GROUP_KPIS add STATUS nvarchar(5) null';
	alter table M_GROUP_KPIS add STATUS nvarchar(5) null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'ORGANIZATION_CODE') begin -- then
	print 'alter table M_GROUP_KPIS add ORGANIZATION_CODE nvarchar(50) null';
	alter table M_GROUP_KPIS add ORGANIZATION_CODE nvarchar(50) null;
end -- if;
if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS' and COLUMN_NAME = 'BUSINESS_CODE') begin -- then
	print 'alter table M_GROUP_KPIS add BUSINESS_CODE nvarchar(50) null';
	alter table M_GROUP_KPIS add BUSINESS_CODE nvarchar(50) null;
end -- if;

GO


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_ASSIGNED_USER_ID on dbo.M_GROUP_KPI_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add TEAM_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add TEAM_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_ID          on dbo.M_GROUP_KPI_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add TEAM_SET_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_SET_ID      on dbo.M_GROUP_KPI_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'KPI_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add KPI_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add KPI_ID uniqueidentifier null;
end -- if;


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'KPI_CODE') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add KPI_CODE nvarchar(50) null';
	alter table M_GROUP_KPI_DETAILS add KPI_CODE nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'KPI_NAME') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add KPI_NAME nvarchar(200) null';
	alter table M_GROUP_KPI_DETAILS add KPI_NAME nvarchar(200) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'GROUP_KPI_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add GROUP_KPI_ID nvarchar(50) null';
	alter table M_GROUP_KPI_DETAILS add GROUP_KPI_ID nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'UNIT') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add UNIT int null';
	alter table M_GROUP_KPI_DETAILS add UNIT int null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'RATIO') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add RATIO float null';
	alter table M_GROUP_KPI_DETAILS add RATIO float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'MAX_RATIO_COMPLETE') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add MAX_RATIO_COMPLETE float null';
	alter table M_GROUP_KPI_DETAILS add MAX_RATIO_COMPLETE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add DESCRIPTION nvarchar(max) null';
	alter table M_GROUP_KPI_DETAILS add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add REMARK nvarchar(max) null';
	alter table M_GROUP_KPI_DETAILS add REMARK nvarchar(max) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPIS')
	Drop View dbo.vwM_GROUP_KPIS;
GO


Create View dbo.vwM_GROUP_KPIS
as
select M_GROUP_KPIS.ID
     , M_GROUP_KPIS.KPI_GROUP_CODE
     , M_GROUP_KPIS.KPI_GROUP_NAME
     , M_GROUP_KPIS.YEAR
     , M_GROUP_KPIS.VERSION_NUMBER
     , M_GROUP_KPIS.APPROVE_STATUS
     , M_GROUP_KPIS.APPROVED_BY
     , M_GROUP_KPIS.APPROVED_DATE
     , M_GROUP_KPIS.POSITION_ID
     , M_GROUP_KPIS.TYPE
     , M_GROUP_KPIS.ORGANIZATION_ID
	 , M_GROUP_KPIS.ORGANIZATION_CODE
	 , M_GROUP_KPIS.BUSINESS_CODE
     , M_GROUP_KPIS.IS_COMMON
     , M_GROUP_KPIS.DESCRIPTION
     , M_GROUP_KPIS.REMARK
     , M_GROUP_KPIS.STATUS
     , M_GROUP_KPIS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , M_GROUP_KPIS.DATE_ENTERED
     , M_GROUP_KPIS.DATE_MODIFIED
     , M_GROUP_KPIS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , M_GROUP_KPIS.CREATED_BY      as CREATED_BY_ID
     , M_GROUP_KPIS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , M_GROUP_KPIS_CSTM.*
  from            M_GROUP_KPIS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = M_GROUP_KPIS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = M_GROUP_KPIS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = M_GROUP_KPIS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = M_GROUP_KPIS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = M_GROUP_KPIS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = M_GROUP_KPIS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = M_GROUP_KPIS.MODIFIED_USER_ID
  left outer join M_GROUP_KPIS_CSTM
               on M_GROUP_KPIS_CSTM.ID_C     = M_GROUP_KPIS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = M_GROUP_KPIS.ID
 where M_GROUP_KPIS.DELETED = 0

GO

Grant Select on dbo.vwM_GROUP_KPIS to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS')
	Drop View dbo.vwM_GROUP_KPI_DETAILS;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS
as
select M_GROUP_KPI_DETAILS.ID
     , M_GROUP_KPI_DETAILS.KPI_ID
     , M_GROUP_KPI_DETAILS.KPI_CODE
	 , M_GROUP_KPI_DETAILS.KPI_NAME
     , M_GROUP_KPI_DETAILS.GROUP_KPI_ID
     , M_GROUP_KPI_DETAILS.UNIT
     , M_GROUP_KPI_DETAILS.RATIO
     , M_GROUP_KPI_DETAILS.MAX_RATIO_COMPLETE
     , M_GROUP_KPI_DETAILS.DESCRIPTION
     , M_GROUP_KPI_DETAILS.REMARK
     , M_GROUP_KPI_DETAILS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , M_GROUP_KPI_DETAILS.DATE_ENTERED
     , M_GROUP_KPI_DETAILS.DATE_MODIFIED
     , M_GROUP_KPI_DETAILS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , M_GROUP_KPI_DETAILS.CREATED_BY      as CREATED_BY_ID
     , M_GROUP_KPI_DETAILS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , M_GROUP_KPI_DETAILS_CSTM.*
  from            M_GROUP_KPI_DETAILS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = M_GROUP_KPI_DETAILS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = M_GROUP_KPI_DETAILS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = M_GROUP_KPI_DETAILS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = M_GROUP_KPI_DETAILS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = M_GROUP_KPI_DETAILS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = M_GROUP_KPI_DETAILS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = M_GROUP_KPI_DETAILS.MODIFIED_USER_ID
  left outer join M_GROUP_KPI_DETAILS_CSTM
               on M_GROUP_KPI_DETAILS_CSTM.ID_C     = M_GROUP_KPI_DETAILS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = M_GROUP_KPI_DETAILS.ID
 where M_GROUP_KPI_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPIS_Edit')
	Drop View dbo.vwM_GROUP_KPIS_Edit;
GO


Create View dbo.vwM_GROUP_KPIS_Edit
as
select *
  from vwM_GROUP_KPIS

GO

Grant Select on dbo.vwM_GROUP_KPIS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPIS_List')
	Drop View dbo.vwM_GROUP_KPIS_List;
GO


Create View dbo.vwM_GROUP_KPIS_List
as
select *
  from vwM_GROUP_KPIS

GO

Grant Select on dbo.vwM_GROUP_KPIS_List to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS_Edit')
	Drop View dbo.vwM_GROUP_KPI_DETAILS_Edit;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS_Edit
as
select *
  from vwM_GROUP_KPI_DETAILS

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS_List')
	Drop View dbo.vwM_GROUP_KPI_DETAILS_List;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS_List
as
select *
  from vwM_GROUP_KPI_DETAILS

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_Delete;
GO


Create Procedure dbo.spM_GROUP_KPIS_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update M_GROUP_KPIS
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spM_GROUP_KPIS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_Undelete;
GO


Create Procedure dbo.spM_GROUP_KPIS_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPIM0101';
	
	-- BEGIN Oracle Exception
		update M_GROUP_KPIS
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from M_GROUP_KPIS_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spM_GROUP_KPIS_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_Update;
GO


Create Procedure dbo.spM_GROUP_KPIS_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_GROUP_CODE                     nvarchar(50)
	, @KPI_GROUP_NAME                     nvarchar(200)
	, @YEAR                               int
	, @VERSION_NUMBER                     nvarchar(10)
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @POSITION_ID                        uniqueidentifier
	, @TYPE                               int
	, @ORGANIZATION_ID                    uniqueidentifier
	, @ORGANIZATION_CODE 				  nvarchar(50)
	, @BUSINESS_CODE					  nvarchar(50)
	, @IS_COMMON                          nvarchar(1)
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @STATUS                             nvarchar(5)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from M_GROUP_KPIS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into M_GROUP_KPIS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_GROUP_CODE                     
			, KPI_GROUP_NAME                     
			, YEAR                               
			, VERSION_NUMBER                     
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, POSITION_ID                        
			, TYPE                               
			, ORGANIZATION_ID  
			, ORGANIZATION_CODE
			, BUSINESS_CODE
			, IS_COMMON                          
			, DESCRIPTION                        
			, REMARK                             
			, STATUS                             

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_GROUP_CODE                     
			, @KPI_GROUP_NAME                     
			, @YEAR                               
			, @VERSION_NUMBER                     
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @POSITION_ID                        
			, @TYPE                               
			, @ORGANIZATION_ID 
			, @ORGANIZATION_CODE
			, @BUSINESS_CODE
			, @IS_COMMON                          
			, @DESCRIPTION                        
			, @REMARK                             
			, @STATUS                             

			);
	end else begin
		update M_GROUP_KPIS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , KPI_GROUP_CODE                       = @KPI_GROUP_CODE                     
		     , KPI_GROUP_NAME                       = @KPI_GROUP_NAME                     
		     , YEAR                                 = @YEAR                               
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , POSITION_ID                          = @POSITION_ID                        
		     , TYPE                                 = @TYPE                               
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID  
			 , ORGANIZATION_CODE                    = @ORGANIZATION_CODE  
			 , BUSINESS_CODE 	                    = @BUSINESS_CODE  			 
		     , IS_COMMON                            = @IS_COMMON                          
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , STATUS                               = @STATUS                             

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from M_GROUP_KPIS_CSTM where ID_C = @ID) begin -- then
			insert into M_GROUP_KPIS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0101', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spM_GROUP_KPIS_Update to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_Delete;
GO


Create Procedure dbo.spM_GROUP_KPI_DETAILS_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update M_GROUP_KPI_DETAILS
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spM_GROUP_KPI_DETAILS_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_Undelete;
GO


Create Procedure dbo.spM_GROUP_KPI_DETAILS_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'KPID0101';
	
	-- BEGIN Oracle Exception
		update M_GROUP_KPI_DETAILS
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from M_GROUP_KPI_DETAILS_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spM_GROUP_KPI_DETAILS_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_Update;
GO


Create Procedure dbo.spM_GROUP_KPI_DETAILS_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_ID                             uniqueidentifier
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @GROUP_KPI_ID                       nvarchar(50)
	, @UNIT                               int
	, @RATIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from M_GROUP_KPI_DETAILS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into M_GROUP_KPI_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_ID                             
			, KPI_CODE 
			, KPI_NAME                           
			, GROUP_KPI_ID                       
			, UNIT                               
			, RATIO                              
			, MAX_RATIO_COMPLETE                 
			, DESCRIPTION                        
			, REMARK                             

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_ID 
			, @KPI_CODE                             
			, @KPI_NAME                           
			, @GROUP_KPI_ID                       
			, @UNIT                               
			, @RATIO                              
			, @MAX_RATIO_COMPLETE                 
			, @DESCRIPTION                        
			, @REMARK                             

			);
	end else begin
		update M_GROUP_KPI_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , KPI_ID                               = @KPI_ID
			 , @KPI_CODE                               = @KPI_CODE                             
		     , KPI_NAME                             = @KPI_NAME                           
		     , GROUP_KPI_ID                         = @GROUP_KPI_ID                       
		     , UNIT                                 = @UNIT                               
		     , RATIO                                = @RATIO                              
		     , MAX_RATIO_COMPLETE                   = @MAX_RATIO_COMPLETE                 
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from M_GROUP_KPI_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into M_GROUP_KPI_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPID0101', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spM_GROUP_KPI_DETAILS_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_MassDelete;
GO


Create Procedure dbo.spM_GROUP_KPIS_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spM_GROUP_KPIS_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spM_GROUP_KPIS_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_MassUpdate;
GO


Create Procedure dbo.spM_GROUP_KPIS_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from M_GROUP_KPIS
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPIM0101', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIM0101', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update M_GROUP_KPIS
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spM_GROUP_KPIS_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPIS_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPIS_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spM_GROUP_KPIS_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spM_GROUP_KPIS_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spM_GROUP_KPIS_Merge to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_MassDelete;
GO


Create Procedure dbo.spM_GROUP_KPI_DETAILS_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spM_GROUP_KPI_DETAILS_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spM_GROUP_KPI_DETAILS_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_MassUpdate;
GO


Create Procedure dbo.spM_GROUP_KPI_DETAILS_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	, @ASSIGNED_USER_ID  uniqueidentifier
	, @TEAM_ID           uniqueidentifier
	, @TEAM_SET_LIST     varchar(8000)
	, @TEAM_SET_ADD      bit

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;

	declare @TEAM_SET_ID  uniqueidentifier;
	declare @OLD_SET_ID   uniqueidentifier;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;


	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;

		if @TEAM_SET_ADD = 1 and @TEAM_SET_ID is not null begin -- then
				select @OLD_SET_ID = TEAM_SET_ID
				     , @TEAM_ID    = isnull(@TEAM_ID, TEAM_ID)
				  from M_GROUP_KPI_DETAILS
				 where ID                = @ID
				   and DELETED           = 0;
			if @OLD_SET_ID is not null begin -- then
				exec dbo.spTEAM_SETS_AddSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @OLD_SET_ID, @TEAM_ID, @TEAM_SET_ID;
			end -- if;
		end -- if;


		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'KPID0101', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPID0101', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update M_GROUP_KPI_DETAILS
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()
			     , ASSIGNED_USER_ID  = isnull(@ASSIGNED_USER_ID, ASSIGNED_USER_ID)
			     , TEAM_ID           = isnull(@TEAM_ID         , TEAM_ID         )
			     , TEAM_SET_ID       = isnull(@TEAM_SET_ID     , TEAM_SET_ID     )

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spM_GROUP_KPI_DETAILS_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spM_GROUP_KPI_DETAILS_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spM_GROUP_KPI_DETAILS_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spM_GROUP_KPI_DETAILS_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spM_GROUP_KPI_DETAILS_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spM_GROUP_KPI_DETAILS_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_GROUP_KPIS';
	exec dbo.spSqlBuildAuditTrigger 'M_GROUP_KPIS';
	exec dbo.spSqlBuildAuditView    'M_GROUP_KPIS';
end -- if;
GO


-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'M_GROUP_KPI_DETAILS';
	exec dbo.spSqlBuildAuditTrigger 'M_GROUP_KPI_DETAILS';
	exec dbo.spSqlBuildAuditView    'M_GROUP_KPI_DETAILS';
end -- if;
GO





-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPID0101.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPID0101.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPID0101.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPID0101.DetailView'   , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 1, 'KPID0101.LBL_KPI_NAME', 'KPI_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 2, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 3, 'KPID0101.LBL_UNIT', 'UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 4, 'KPID0101.LBL_RATIO', 'RATIO', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 5, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 7, 'KPID0101.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 8, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 9, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 10, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 11, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO

-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0101.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0101.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIM0101.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIM0101.DetailView'   , 'KPIM0101', 'vwM_GROUP_KPIS_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 1, 'KPIM0101.LBL_KPI_GROUP_NAME', 'KPI_GROUP_NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 2, 'KPIM0101.LBL_YEAR', 'YEAR', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 3, 'KPIM0101.LBL_VERSION_NUMBER', 'VERSION_NUMBER', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 4, 'KPIM0101.LBL_APPROVE_STATUS', 'APPROVE_STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 5, 'KPIM0101.LBL_APPROVED_BY', 'APPROVED_BY', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 6, 'KPIM0101.LBL_APPROVED_DATE', 'APPROVED_DATE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 7, 'KPIM0101.LBL_POSITION_ID', 'POSITION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 8, 'KPIM0101.LBL_TYPE', 'TYPE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 9, 'KPIM0101.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 10, 'KPIM0101.LBL_IS_COMMON', 'IS_COMMON', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 11, 'KPIM0101.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 12, 'KPIM0101.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 13, 'KPIM0101.LBL_STATUS', 'STATUS', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 14, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 15, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 16, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPIM0101.DetailView', 17, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPID0101.DetailView', 'KPID0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPID0101.EditView'  , 'KPID0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPID0101.PopupView' , 'KPID0101';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPID0101.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPID0101.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPID0101.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPID0101.EditView', -1, null;
end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPIM0101.DetailView', 'KPIM0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPIM0101.EditView'  , 'KPIM0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPIM0101.PopupView' , 'KPIM0101';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0101.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPIM0101.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPIM0101.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPIM0101.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.EditView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 1, 'KPID0101.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 2, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 3, 'KPID0101.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 4, 'KPID0101.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 5, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.EditView.Inline', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 1, 'KPID0101.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 2, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 3, 'KPID0101.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 4, 'KPID0101.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 5, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView.Inline', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView.Inline', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView.Inline', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView.Inline', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.PopupView.Inline', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 1, 'KPID0101.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 2, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 3, 'KPID0101.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 4, 'KPID0101.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 5, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.PopupView.Inline', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.PopupView.Inline', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.PopupView.Inline', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.PopupView.Inline', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchBasic'    , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchBasic', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPID0101.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchAdvanced' , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 1, 'KPID0101.LBL_KPI_NAME', 'KPI_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 2, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 3, 'KPID0101.LBL_UNIT', 'UNIT', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 4, 'KPID0101.LBL_RATIO', 'RATIO', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 5, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPID0101.SearchAdvanced', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPID0101.SearchAdvanced', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPID0101.SearchAdvanced' , 8, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchPopup'    , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchPopup', 0, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;

end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0101.EditView', 'KPIM0101'      , 'vwM_GROUP_KPIS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 1, 'KPIM0101.LBL_KPI_GROUP_NAME', 'KPI_GROUP_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 2, 'KPIM0101.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 3, 'KPIM0101.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 4, 'KPIM0101.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 5, 'KPIM0101.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIM0101.EditView', 6, 'KPIM0101.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 7, 'KPIM0101.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 8, 'KPIM0101.LBL_TYPE', 'TYPE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 9, 'KPIM0101.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 10, 'KPIM0101.LBL_IS_COMMON', 'IS_COMMON', 0, 1, 1, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.EditView', 11, 'KPIM0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.EditView', 12, 'KPIM0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView', 13, 'KPIM0101.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.EditView', 14, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.EditView', 15, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0101.EditView.Inline', 'KPIM0101'      , 'vwM_GROUP_KPIS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 1, 'KPIM0101.LBL_KPI_GROUP_NAME', 'KPI_GROUP_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 2, 'KPIM0101.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 3, 'KPIM0101.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 4, 'KPIM0101.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 5, 'KPIM0101.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIM0101.EditView.Inline', 6, 'KPIM0101.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 7, 'KPIM0101.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 8, 'KPIM0101.LBL_TYPE', 'TYPE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 9, 'KPIM0101.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 10, 'KPIM0101.LBL_IS_COMMON', 'IS_COMMON', 0, 1, 1, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.EditView.Inline', 11, 'KPIM0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.EditView.Inline', 12, 'KPIM0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.EditView.Inline', 13, 'KPIM0101.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.EditView.Inline', 14, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.EditView.Inline', 15, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPIM0101.PopupView.Inline', 'KPIM0101'      , 'vwM_GROUP_KPIS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 1, 'KPIM0101.LBL_KPI_GROUP_NAME', 'KPI_GROUP_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 2, 'KPIM0101.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 3, 'KPIM0101.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 4, 'KPIM0101.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 5, 'KPIM0101.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl     'KPIM0101.PopupView.Inline', 6, 'KPIM0101.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 7, 'KPIM0101.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 8, 'KPIM0101.LBL_TYPE', 'TYPE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 9, 'KPIM0101.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 10, 'KPIM0101.LBL_IS_COMMON', 'IS_COMMON', 0, 1, 1, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.PopupView.Inline', 11, 'KPIM0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPIM0101.PopupView.Inline', 12, 'KPIM0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPIM0101.PopupView.Inline', 13, 'KPIM0101.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.PopupView.Inline', 14, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPIM0101.PopupView.Inline', 15, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0101.SearchBasic'    , 'KPIM0101', 'vwM_GROUP_KPIS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchBasic', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIM0101.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0101.SearchAdvanced' , 'KPIM0101', 'vwM_GROUP_KPIS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 1, 'KPIM0101.LBL_KPI_GROUP_NAME', 'KPI_GROUP_NAME', 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 2, 'KPIM0101.LBL_YEAR', 'YEAR', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 3, 'KPIM0101.LBL_VERSION_NUMBER', 'VERSION_NUMBER', 0, 1, 10, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 4, 'KPIM0101.LBL_APPROVE_STATUS', 'APPROVE_STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 5, 'KPIM0101.LBL_APPROVED_BY', 'APPROVED_BY', 0, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPIM0101.SearchAdvanced', 6, 'KPIM0101.LBL_APPROVED_DATE', 'APPROVED_DATE', 0, 1, 'DatePicker', null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 7, 'KPIM0101.LBL_POSITION_ID', 'POSITION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 8, 'KPIM0101.LBL_TYPE', 'TYPE', 0, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 9, 'KPIM0101.LBL_ORGANIZATION_ID', 'ORGANIZATION_ID', 0, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 10, 'KPIM0101.LBL_IS_COMMON', 'IS_COMMON', 0, 1, 1, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIM0101.SearchAdvanced', 11, 'KPIM0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIM0101.SearchAdvanced', 12, 'KPIM0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchAdvanced', 13, 'KPIM0101.LBL_STATUS', 'STATUS', 0, 1, 5, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.SearchAdvanced' , 14, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0101.SearchPopup'    , 'KPIM0101', 'vwM_GROUP_KPIS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.SearchPopup', 0, 'KPIM0101.LBL_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 1, 1, 50, 35, null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.Export', 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.Export'         ,  1, 'KPID0101.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.ListView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.ListView', 2, 'KPID0101.LBL_LIST_KPI_ID', 'KPI_ID', 'KPI_ID', '35%', 'listViewTdLinkS1', 'ID', '~/KPID0101/view.aspx?id={0}', null, 'KPID0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.PopupView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.PopupView', 1, 'KPID0101.LBL_LIST_KPI_ID', 'KPI_ID', 'KPI_ID', '45%', 'listViewTdLinkS1', 'ID KPI_ID', 'SelectKPID0101(''{0}'', ''{1}'');', null, 'KPID0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.SearchDuplicates', 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.SearchDuplicates'          , 1, 'KPID0101.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPID0101/view.aspx?id={0}', null, 'KPID0101', 'ASSIGNED_USER_ID';
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0101.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0101.Export', 'KPIM0101', 'vwM_GROUP_KPIS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0101.Export'         ,  1, 'KPIM0101.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0101.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0101.ListView', 'KPIM0101'      , 'vwM_GROUP_KPIS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0101.ListView', 2, 'KPIM0101.LBL_LIST_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 'KPI_GROUP_CODE', '35%', 'listViewTdLinkS1', 'ID', '~/KPIM0101/view.aspx?id={0}', null, 'KPIM0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0101.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0101.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0101.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0101.PopupView', 'KPIM0101'      , 'vwM_GROUP_KPIS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0101.PopupView', 1, 'KPIM0101.LBL_LIST_KPI_GROUP_CODE', 'KPI_GROUP_CODE', 'KPI_GROUP_CODE', '45%', 'listViewTdLinkS1', 'ID KPI_GROUP_CODE', 'SelectKPIM0101(''{0}'', ''{1}'');', null, 'KPIM0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0101.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPIM0101.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPIM0101.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPIM0101.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPIM0101.SearchDuplicates', 'KPIM0101', 'vwM_GROUP_KPIS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPIM0101.SearchDuplicates'          , 1, 'KPIM0101.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPIM0101/view.aspx?id={0}', null, 'KPIM0101', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPID0101', '.moduleList.KPID0101', '~/KPID0101/', 1, 0, 100, 0, 1, 1, 1, 0, 'M_GROUP_KPI_DETAILS', 0, 0, 0, 0, 0, 1;
GO


exec dbo.spMODULES_InsertOnly null, 'KPIM0101', '.moduleList.KPIM0101', '~/KPIM0101/', 1, 1, 100, 0, 1, 1, 1, 0, 'M_GROUP_KPIS', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPID0101';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPID0101' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', 'KPID0101.LNK_NEW_M_GROUP_KPI_DETAIL' , '~/KPID0101/edit.aspx'   , 'CreateKPID0101.gif', 1,  1, 'KPID0101', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', 'KPID0101.LNK_M_GROUP_KPI_DETAIL_LIST', '~/KPID0101/default.aspx', 'KPID0101.gif'      , 1,  2, 'KPID0101', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', '.LBL_IMPORT'              , '~/KPID0101/import.aspx' , 'Import.gif'        , 1,  3, 'KPID0101', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', '.LNK_ACTIVITY_STREAM'     , '~/KPID0101/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPID0101', 'list';
end -- if;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPIM0101';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPIM0101' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0101', 'KPIM0101.LNK_NEW_M_GROUP_KPI' , '~/KPIM0101/edit.aspx'   , 'CreateKPIM0101.gif', 1,  1, 'KPIM0101', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0101', 'KPIM0101.LNK_M_GROUP_KPI_LIST', '~/KPIM0101/default.aspx', 'KPIM0101.gif'      , 1,  2, 'KPIM0101', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0101', '.LBL_IMPORT'              , '~/KPIM0101/import.aspx' , 'Import.gif'        , 1,  3, 'KPIM0101', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPIM0101', '.LNK_ACTIVITY_STREAM'     , '~/KPIM0101/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPIM0101', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPID0101', null, null, N'KPID0101 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPID0101', null, null, N'Create KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_M_GROUP_KPI_DETAIL_LIST'                          , N'en-US', N'KPID0101', null, null, N'KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_M_GROUP_KPI_DETAIL'                           , N'en-US', N'KPID0101', null, null, N'Create KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPID0101', null, null, N'KPID0101 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_M_GROUP_KPI_DETAIL_NOT_FOUND'                     , N'en-US', N'KPID0101', null, null, N'KPID0101 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_M_GROUP_KPI_DETAIL_CONFIRMATION'           , N'en-US', N'KPID0101', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPID0101', null, null, N'KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPID0101', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPID0101'                                          , N'en-US', null, N'moduleList', 100, N'KPID0101';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_ID'                                            , 'en-US', 'KPID0101', null, null, 'KPI Id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_ID'                                       , 'en-US', 'KPID0101', null, null, 'KPI Id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_NAME'                                          , 'en-US', 'KPID0101', null, null, 'KPI Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_NAME'                                     , 'en-US', 'KPID0101', null, null, 'KPI Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_GROUP_KPI_ID'                                      , 'en-US', 'KPID0101', null, null, 'Group KPI Id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_GROUP_KPI_ID'                                 , 'en-US', 'KPID0101', null, null, 'Group KPI Id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_UNIT'                                              , 'en-US', 'KPID0101', null, null, 'Unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_UNIT'                                         , 'en-US', 'KPID0101', null, null, 'Unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RATIO'                                             , 'en-US', 'KPID0101', null, null, 'RATIO:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RATIO'                                        , 'en-US', 'KPID0101', null, null, 'RATIO';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MAX_RATIO_COMPLETE'                                , 'en-US', 'KPID0101', null, null, 'Max Ratio Complete:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MAX_RATIO_COMPLETE'                           , 'en-US', 'KPID0101', null, null, 'Max Ratio Complete';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPID0101', null, null, 'Description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPID0101', null, null, 'Description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPID0101', null, null, 'Remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPID0101', null, null, 'Remark';




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPIM0101', null, null, N'KPIM0101 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPIM0101', null, null, N'Create KPIM0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_M_GROUP_KPI_LIST'                          , N'en-US', N'KPIM0101', null, null, N'KPIM0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_M_GROUP_KPI'                           , N'en-US', N'KPIM0101', null, null, N'Create KPIM0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPIM0101', null, null, N'KPIM0101 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_M_GROUP_KPI_NOT_FOUND'                     , N'en-US', N'KPIM0101', null, null, N'KPIM0101 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_M_GROUP_KPI_CONFIRMATION'           , N'en-US', N'KPIM0101', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPIM0101', null, null, N'KPIM0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPIM0101', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPIM0101'                                          , N'en-US', null, N'moduleList', 100, N'KPIM0101';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_GROUP_CODE'                                    , 'en-US', 'KPIM0101', null, null, 'KPI Group Code:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_GROUP_CODE'                               , 'en-US', 'KPIM0101', null, null, 'KPI Group Code';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_GROUP_NAME'                                    , 'en-US', 'KPIM0101', null, null, 'KPI Group Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_GROUP_NAME'                               , 'en-US', 'KPIM0101', null, null, 'KPI Group Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_YEAR'                                              , 'en-US', 'KPIM0101', null, null, 'Year:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_YEAR'                                         , 'en-US', 'KPIM0101', null, null, 'Year';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VERSION_NUMBER'                                    , 'en-US', 'KPIM0101', null, null, 'Version Number:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VERSION_NUMBER'                               , 'en-US', 'KPIM0101', null, null, 'Version Number';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVE_STATUS'                                    , 'en-US', 'KPIM0101', null, null, 'Approve Status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVE_STATUS'                               , 'en-US', 'KPIM0101', null, null, 'Approve Status';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_BY'                                       , 'en-US', 'KPIM0101', null, null, 'Approved By:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_BY'                                  , 'en-US', 'KPIM0101', null, null, 'Approved By';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_APPROVED_DATE'                                     , 'en-US', 'KPIM0101', null, null, 'Approved Date:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_APPROVED_DATE'                                , 'en-US', 'KPIM0101', null, null, 'Approved Date';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_POSITION_ID'                                       , 'en-US', 'KPIM0101', null, null, 'Position:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_POSITION_ID'                                  , 'en-US', 'KPIM0101', null, null, 'Position';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_TYPE'                                              , 'en-US', 'KPIM0101', null, null, 'Type:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_TYPE'                                         , 'en-US', 'KPIM0101', null, null, 'Type';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_ORGANIZATION_ID'                                   , 'en-US', 'KPIM0101', null, null, 'Organization:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_ORGANIZATION_ID'                              , 'en-US', 'KPIM0101', null, null, 'Organization';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_IS_COMMON'                                         , 'en-US', 'KPIM0101', null, null, 'Is Common:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_IS_COMMON'                                    , 'en-US', 'KPIM0101', null, null, 'Is Common';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPIM0101', null, null, 'Description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPIM0101', null, null, 'Description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPIM0101', null, null, 'Remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPIM0101', null, null, 'Remark';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_STATUS'                                            , 'en-US', 'KPIM0101', null, null, 'Status:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_STATUS'                                       , 'en-US', 'KPIM0101', null, null, 'Status';






