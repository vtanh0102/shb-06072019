
-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPID0101.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPID0101.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPID0101.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPID0101.DetailView'   , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 0, 'KPID0101.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 1, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 2, 'KPID0101.LBL_UNIT', 'UNIT', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 3, 'KPID0101.LBL_RATIO', 'RATIO', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 4, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 5, 'KPID0101.LBL_KPI_ID', 'KPI_ID', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 7, 'KPID0101.LBL_REMARK', 'REMARK', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 8, '.LBL_ASSIGNED_TO'                , 'ASSIGNED_TO'                      , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 9, 'Teams.LBL_TEAM'                  , 'TEAM_NAME'                        , '{0}'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 10, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'KPID0101.DetailView', 11, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'KPID0101.DetailView', 'KPID0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'KPID0101.EditView'  , 'KPID0101';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'KPID0101.PopupView' , 'KPID0101';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPID0101.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'KPID0101.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'KPID0101.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'KPID0101.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.EditView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 1, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 2, 'KPID0101.LBL_UNIT', 'UNIT', 1, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 3, 'KPID0101.LBL_RATIO', 'RATIO', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 4, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView', 5, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.EditView.Inline', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 1, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 2, 'KPID0101.LBL_UNIT', 'UNIT', 1, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 3, 'KPID0101.LBL_RATIO', 'RATIO', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 4, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.EditView.Inline', 5, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView.Inline', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.EditView.Inline', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView.Inline', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.EditView.Inline', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'KPID0101.PopupView.Inline', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 1, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 2, 'KPID0101.LBL_UNIT', 'UNIT', 1, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 3, 'KPID0101.LBL_RATIO', 'RATIO', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 4, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'KPID0101.PopupView.Inline', 5, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.PopupView.Inline', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine   'KPID0101.PopupView.Inline', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.PopupView.Inline', 8, '.LBL_ASSIGNED_TO'                       , 'ASSIGNED_USER_ID'           , 0, 1, 'ASSIGNED_TO'        , 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup 'KPID0101.PopupView.Inline', 9, 'Teams.LBL_TEAM'                         , 'TEAM_ID'                    , 0, 1, 'TEAM_NAME'          , 'Teams', null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchBasic'    , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPID0101.SearchBasic', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPID0101', null;
	exec dbo.spEDITVIEWS_FIELDS_InsControl      'KPID0101.SearchBasic'    , 1, '.LBL_CURRENT_USER_FILTER', 'CURRENT_USER_ONLY', 0, null, 'CheckBox', 'return ToggleUnassignedOnly();', null, null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchAdvanced' , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPID0101.SearchAdvanced', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPID0101', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 1, 'KPID0101.LBL_GROUP_KPI_ID', 'GROUP_KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 2, 'KPID0101.LBL_UNIT', 'UNIT', 1, 1, 30, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 3, 'KPID0101.LBL_RATIO', 'RATIO', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 4, 'KPID0101.LBL_MAX_RATIO_COMPLETE', 'MAX_RATIO_COMPLETE', 1, 1, 10, 10, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPID0101.SearchAdvanced', 5, 'KPID0101.LBL_KPI_ID', 'KPI_ID', 1, 1, 50, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPID0101.SearchAdvanced', 6, 'KPID0101.LBL_DESCRIPTION', 'DESCRIPTION', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPID0101.SearchAdvanced', 7, 'KPID0101.LBL_REMARK', 'REMARK', 0, 1,   1, 70, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPID0101.SearchAdvanced' , 8, '.LBL_ASSIGNED_TO'     , 'ASSIGNED_USER_ID', 0, null, 'AssignedUser'    , null, 6;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPID0101.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPID0101.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'KPID0101.SearchPopup'    , 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'KPID0101.SearchPopup', 0, 'KPID0101.LBL_NAME', 'NAME', 1, 1, 150, 35, 'KPID0101', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.Export', 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.Export'         ,  1, 'KPID0101.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.ListView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.ListView', 2, 'KPID0101.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/KPID0101/view.aspx?id={0}', null, 'KPID0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.ListView', 3, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.ListView', 4, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '5%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.PopupView', 'KPID0101'      , 'vwM_GROUP_KPI_DETAILS_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.PopupView', 1, 'KPID0101.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectKPID0101(''{0}'', ''{1}'');', null, 'KPID0101', 'ASSIGNED_USER_ID';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.PopupView', 2, '.LBL_LIST_ASSIGNED_USER'                  , 'ASSIGNED_TO'     , 'ASSIGNED_TO'     , '10%';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'KPID0101.PopupView', 3, 'Teams.LBL_LIST_TEAM'                      , 'TEAM_NAME'       , 'TEAM_NAME'       , '10%';

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'KPID0101.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS KPID0101.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'KPID0101.SearchDuplicates', 'KPID0101', 'vwM_GROUP_KPI_DETAILS_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'KPID0101.SearchDuplicates'          , 1, 'KPID0101.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/KPID0101/view.aspx?id={0}', null, 'KPID0101', 'ASSIGNED_USER_ID';
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'KPID0101', '.moduleList.KPID0101', '~/KPID0101/', 1, 1, 100, 0, 1, 1, 1, 0, 'M_GROUP_KPI_DETAILS', 1, 0, 0, 0, 0, 1;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'KPID0101';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'KPID0101' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', 'KPID0101.LNK_NEW_M_GROUP_KPI_DETAIL' , '~/KPID0101/edit.aspx'   , 'CreateKPID0101.gif', 1,  1, 'KPID0101', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', 'KPID0101.LNK_M_GROUP_KPI_DETAIL_LIST', '~/KPID0101/default.aspx', 'KPID0101.gif'      , 1,  2, 'KPID0101', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', '.LBL_IMPORT'              , '~/KPID0101/import.aspx' , 'Import.gif'        , 1,  3, 'KPID0101', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'KPID0101', '.LNK_ACTIVITY_STREAM'     , '~/KPID0101/stream.aspx' , 'ActivityStream.gif', 1,  4, 'KPID0101', 'list';
end -- if;
GO



