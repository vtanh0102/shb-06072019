
if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS')
	Drop View dbo.vwM_GROUP_KPI_DETAILS;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS
as
select M_GROUP_KPI_DETAILS.ID
     , M_GROUP_KPI_DETAILS.NAME
     , M_GROUP_KPI_DETAILS.GROUP_KPI_ID
     , M_GROUP_KPI_DETAILS.UNIT
     , M_GROUP_KPI_DETAILS.RATIO
     , M_GROUP_KPI_DETAILS.MAX_RATIO_COMPLETE
     , M_GROUP_KPI_DETAILS.KPI_ID
     , M_GROUP_KPI_DETAILS.DESCRIPTION
     , M_GROUP_KPI_DETAILS.REMARK
     , M_GROUP_KPI_DETAILS.ASSIGNED_USER_ID
     , USERS_ASSIGNED.USER_NAME    as ASSIGNED_TO
     , TEAMS.ID                    as TEAM_ID
     , TEAMS.NAME                  as TEAM_NAME
     , TEAM_SETS.ID                as TEAM_SET_ID
     , TEAM_SETS.TEAM_SET_NAME     as TEAM_SET_NAME

     , M_GROUP_KPI_DETAILS.DATE_ENTERED
     , M_GROUP_KPI_DETAILS.DATE_MODIFIED
     , M_GROUP_KPI_DETAILS.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , M_GROUP_KPI_DETAILS.CREATED_BY      as CREATED_BY_ID
     , M_GROUP_KPI_DETAILS.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , M_GROUP_KPI_DETAILS_CSTM.*
  from            M_GROUP_KPI_DETAILS
  left outer join USERS                      USERS_ASSIGNED
               on USERS_ASSIGNED.ID        = M_GROUP_KPI_DETAILS.ASSIGNED_USER_ID
  left outer join TEAMS
               on TEAMS.ID                 = M_GROUP_KPI_DETAILS.TEAM_ID
              and TEAMS.DELETED            = 0
  left outer join TEAM_SETS
               on TEAM_SETS.ID             = M_GROUP_KPI_DETAILS.TEAM_SET_ID
              and TEAM_SETS.DELETED        = 0

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = M_GROUP_KPI_DETAILS.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = M_GROUP_KPI_DETAILS.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = M_GROUP_KPI_DETAILS.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = M_GROUP_KPI_DETAILS.MODIFIED_USER_ID
  left outer join M_GROUP_KPI_DETAILS_CSTM
               on M_GROUP_KPI_DETAILS_CSTM.ID_C     = M_GROUP_KPI_DETAILS.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = M_GROUP_KPI_DETAILS.ID
 where M_GROUP_KPI_DETAILS.DELETED = 0

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS_Edit')
	Drop View dbo.vwM_GROUP_KPI_DETAILS_Edit;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS_Edit
as
select *
  from vwM_GROUP_KPI_DETAILS

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwM_GROUP_KPI_DETAILS_List')
	Drop View dbo.vwM_GROUP_KPI_DETAILS_List;
GO


Create View dbo.vwM_GROUP_KPI_DETAILS_List
as
select *
  from vwM_GROUP_KPI_DETAILS

GO

Grant Select on dbo.vwM_GROUP_KPI_DETAILS_List to public;
GO



