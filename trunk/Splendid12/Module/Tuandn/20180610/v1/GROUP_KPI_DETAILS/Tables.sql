


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'ASSIGNED_USER_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add ASSIGNED_USER_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add ASSIGNED_USER_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_ASSIGNED_USER_ID on dbo.M_GROUP_KPI_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'TEAM_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add TEAM_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add TEAM_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_ID          on dbo.M_GROUP_KPI_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'TEAM_SET_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add TEAM_SET_ID uniqueidentifier null';
	alter table M_GROUP_KPI_DETAILS add TEAM_SET_ID uniqueidentifier null;
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_SET_ID      on dbo.M_GROUP_KPI_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add NAME nvarchar(150) null';
	alter table M_GROUP_KPI_DETAILS add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'GROUP_KPI_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add GROUP_KPI_ID nvarchar(50) null';
	alter table M_GROUP_KPI_DETAILS add GROUP_KPI_ID nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'UNIT') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add UNIT nvarchar(30) null';
	alter table M_GROUP_KPI_DETAILS add UNIT nvarchar(30) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'RATIO') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add RATIO float null';
	alter table M_GROUP_KPI_DETAILS add RATIO float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'MAX_RATIO_COMPLETE') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add MAX_RATIO_COMPLETE float null';
	alter table M_GROUP_KPI_DETAILS add MAX_RATIO_COMPLETE float null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'KPI_ID') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add KPI_ID nvarchar(50) null';
	alter table M_GROUP_KPI_DETAILS add KPI_ID nvarchar(50) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'DESCRIPTION') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add DESCRIPTION nvarchar(max) null';
	alter table M_GROUP_KPI_DETAILS add DESCRIPTION nvarchar(max) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and COLUMN_NAME = 'REMARK') begin -- then
	print 'alter table M_GROUP_KPI_DETAILS add REMARK nvarchar(max) null';
	alter table M_GROUP_KPI_DETAILS add REMARK nvarchar(max) null;
end -- if;


GO


