
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPI_DETAILS' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_GROUP_KPI_DETAILS';
	Create Table dbo.M_GROUP_KPI_DETAILS
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_M_GROUP_KPI_DETAILS primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, ASSIGNED_USER_ID                   uniqueidentifier null
		, TEAM_ID                            uniqueidentifier null
		, TEAM_SET_ID                        uniqueidentifier null
		, NAME                               nvarchar(150) not null
		, GROUP_KPI_ID                       nvarchar(50) not null
		, UNIT                               nvarchar(30) not null
		, RATIO                              float not null
		, MAX_RATIO_COMPLETE                 float not null
		, KPI_ID                             nvarchar(50) not null
		, DESCRIPTION                        nvarchar(max) null
		, REMARK                             nvarchar(max) null

		)

	create index IDX_M_GROUP_KPI_DETAILS_ASSIGNED_USER_ID on dbo.M_GROUP_KPI_DETAILS (ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_ID          on dbo.M_GROUP_KPI_DETAILS (TEAM_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPI_DETAILS_TEAM_SET_ID      on dbo.M_GROUP_KPI_DETAILS (TEAM_SET_ID, ASSIGNED_USER_ID, DELETED, ID)
	create index IDX_M_GROUP_KPI_DETAILS_NAME  on dbo.M_GROUP_KPI_DETAILS (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'M_GROUP_KPI_DETAILS_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.M_GROUP_KPI_DETAILS_CSTM';
	Create Table dbo.M_GROUP_KPI_DETAILS_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_M_GROUP_KPI_DETAILS_CSTM primary key
		)
  end
GO




