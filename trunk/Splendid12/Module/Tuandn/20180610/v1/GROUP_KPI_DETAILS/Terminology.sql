
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'KPID0101', null, null, N'KPID0101 List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'KPID0101', null, null, N'Create KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_M_GROUP_KPI_DETAIL_LIST'                          , N'en-US', N'KPID0101', null, null, N'KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_M_GROUP_KPI_DETAIL'                           , N'en-US', N'KPID0101', null, null, N'Create KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'KPID0101', null, null, N'KPID0101 Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_M_GROUP_KPI_DETAIL_NOT_FOUND'                     , N'en-US', N'KPID0101', null, null, N'KPID0101 not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_M_GROUP_KPI_DETAIL_CONFIRMATION'           , N'en-US', N'KPID0101', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'KPID0101', null, null, N'KPID0101';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'KPID0101', null, null, N'KPI';

exec dbo.spTERMINOLOGY_InsertOnly N'KPID0101'                                          , N'en-US', null, N'moduleList', 100, N'KPID0101';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'KPID0101', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'KPID0101', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_GROUP_KPI_ID'                                      , 'en-US', 'KPID0101', null, null, 'group kpi_id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_GROUP_KPI_ID'                                 , 'en-US', 'KPID0101', null, null, 'group kpi_id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_UNIT'                                              , 'en-US', 'KPID0101', null, null, 'unit:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_UNIT'                                         , 'en-US', 'KPID0101', null, null, 'unit';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_RATIO'                                             , 'en-US', 'KPID0101', null, null, 'ratio:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_RATIO'                                        , 'en-US', 'KPID0101', null, null, 'ratio';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_MAX_RATIO_COMPLETE'                                , 'en-US', 'KPID0101', null, null, 'max ratio_complete:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_MAX_RATIO_COMPLETE'                           , 'en-US', 'KPID0101', null, null, 'max ratio_complete';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_KPI_ID'                                            , 'en-US', 'KPID0101', null, null, 'kpi id:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_KPI_ID'                                       , 'en-US', 'KPID0101', null, null, 'kpi id';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_DESCRIPTION'                                       , 'en-US', 'KPID0101', null, null, 'description:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_DESCRIPTION'                                  , 'en-US', 'KPID0101', null, null, 'description';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_REMARK'                                            , 'en-US', 'KPID0101', null, null, 'remark:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_REMARK'                                       , 'en-US', 'KPID0101', null, null, 'remark';






