
function M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed(fldM_GROUP_KPI_DETAIL_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldM_GROUP_KPI_DETAIL_NAME.id.substring(0, fldM_GROUP_KPI_DETAIL_NAME.id.length - 'M_GROUP_KPI_DETAIL_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_M_GROUP_KPI_DETAIL_NAME = document.getElementById(userContext + 'PREV_M_GROUP_KPI_DETAIL_NAME');
	if ( fldPREV_M_GROUP_KPI_DETAIL_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_M_GROUP_KPI_DETAIL_NAME');
	}
	else if ( fldPREV_M_GROUP_KPI_DETAIL_NAME.value != fldM_GROUP_KPI_DETAIL_NAME.value )
	{
		if ( fldM_GROUP_KPI_DETAIL_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPID0101.AutoComplete.M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Get(fldM_GROUP_KPI_DETAIL_NAME.value, M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed_OnSucceededWithContext, M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_NAME_AjaxErrors');
		var fldM_GROUP_KPI_DETAIL_ID        = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_ID'       );
		var fldM_GROUP_KPI_DETAIL_NAME      = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_NAME'     );
		var fldPREV_M_GROUP_KPI_DETAIL_NAME = document.getElementById(userContext + 'PREV_M_GROUP_KPI_DETAIL_NAME');
		if ( fldM_GROUP_KPI_DETAIL_ID        != null ) fldM_GROUP_KPI_DETAIL_ID.value        = sID  ;
		if ( fldM_GROUP_KPI_DETAIL_NAME      != null ) fldM_GROUP_KPI_DETAIL_NAME.value      = sNAME;
		if ( fldPREV_M_GROUP_KPI_DETAIL_NAME != null ) fldPREV_M_GROUP_KPI_DETAIL_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPID0101.AutoComplete service is null');
	}
}

function M_GROUP_KPI_DETAILS_M_GROUP_KPI_DETAIL_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldM_GROUP_KPI_DETAIL_ID        = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_ID'       );
	var fldM_GROUP_KPI_DETAIL_NAME      = document.getElementById(userContext + 'M_GROUP_KPI_DETAIL_NAME'     );
	var fldPREV_M_GROUP_KPI_DETAIL_NAME = document.getElementById(userContext + 'PREV_M_GROUP_KPI_DETAIL_NAME');
	if ( fldM_GROUP_KPI_DETAIL_ID        != null ) fldM_GROUP_KPI_DETAIL_ID.value        = '';
	if ( fldM_GROUP_KPI_DETAIL_NAME      != null ) fldM_GROUP_KPI_DETAIL_NAME.value      = '';
	if ( fldPREV_M_GROUP_KPI_DETAIL_NAME != null ) fldPREV_M_GROUP_KPI_DETAIL_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

