using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web.Services;
using System.ComponentModel;
using SplendidCRM;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace SplendidCRM.KPIM0101
{
    public class KPIM0101
    {
        public Guid ID;
        public string NAME;

        public KPIM0101()
        {
            ID = Guid.Empty;
            NAME = String.Empty;
        }
    }

    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    [ToolboxItem(false)]
    public class AutoComplete : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public KPIM0101 M_GROUP_KPIS_M_GROUP_KPI_NAME_Get(string sNAME)
        {
            KPIM0101 item = new KPIM0101();
            //try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "select ID        " + ControlChars.CrLf
                         + "     , NAME      " + ControlChars.CrLf
                         + "  from vwM_GROUP_KPIS" + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Security.Filter(cmd, "KPIM0101", "list");
                        Sql.AppendParameter(cmd, sNAME, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "NAME");
                        cmd.CommandText += " order by NAME" + ControlChars.CrLf;
                        using (IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            if (rdr.Read())
                            {
                                item.ID = Sql.ToGuid(rdr["ID"]);
                                item.NAME = Sql.ToString(rdr["NAME"]);
                            }
                        }
                    }
                }
                if (Sql.IsEmptyGuid(item.ID))
                {
                    string sCULTURE = Sql.ToString(Session["USER_SETTINGS/CULTURE"]);
                    L10N L10n = new L10N(sCULTURE);
                    throw (new Exception(L10n.Term("KPIM0101.ERR_M_GROUP_KPI_NOT_FOUND")));
                }
            }
            //catch
            {
                // 02/04/2007 Paul.  Don't catch the exception.  
                // It is a web service, so the exception will be handled properly by the AJAX framework. 
            }
            return item;
        }

        [WebMethod(EnableSession = true)]
        public string[] M_GROUP_KPIS_M_GROUP_KPI_NAME_List(string prefixText, int count)
        {
            string[] arrItems = new string[0];
            try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "select distinct  " + ControlChars.CrLf
                         + "       NAME      " + ControlChars.CrLf
                         + "  from vwM_GROUP_KPIS" + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Security.Filter(cmd, "KPIM0101", "list");
                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "NAME");
                        cmd.CommandText += " order by NAME" + ControlChars.CrLf;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(0, count, dt);
                                arrItems = new string[dt.Rows.Count];
                                for (int i = 0; i < dt.Rows.Count; i++)
                                    arrItems[i] = Sql.ToString(dt.Rows[i]["NAME"]);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return arrItems;
        }

        [WebMethod(EnableSession = true)]
        public string[] M_GROUP_KPIS_NAME_List(string prefixText, int count)
        {
            return M_GROUP_KPIS_M_GROUP_KPI_NAME_List(prefixText, count);
        }

        [WebMethod(EnableSession = true)]
        public string[] GetKPIM02(string prefixText, int count)
        {
            List<string> lstItem = new List<string>();
            try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "SELECT DISTINCT         " + ControlChars.CrLf
                         + "  ID, CODE        " + ControlChars.CrLf
                         + "  FROM vwKPIM02   " + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Security.Filter(cmd, "KPIM02", "list");
                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "CODE");
                        Sql.AppendParameter(cmd, KPIs_Utils.STATUS_ACTIVE, Sql.SqlFilterMode.Contains, "STATUS");
                        cmd.CommandText += " ORDER BY CODE " + ControlChars.CrLf;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(0, count, dt);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    lstItem.Add(string.Format("{0}-{1}", dt.Rows[i]["CODE"], dt.Rows[i]["CODE"]));
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return lstItem.ToArray();
        }


        [WebMethod(EnableSession = true)]
        public string GetKPIM02_Detail(string CODE, int count)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            JavaScriptSerializer json = new JavaScriptSerializer();
            try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "SELECT DISTINCT         " + ControlChars.CrLf
                         + "  ID, CODE, NAME,       " + ControlChars.CrLf
                         + "  UNIT, RADIO,          " + ControlChars.CrLf
                         + "  MAX_RATIO_COMPLETE, REMARK, LEVEL_NUMBER     " + ControlChars.CrLf
                         + "  FROM vwKPIM02   " + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Security.Filter(cmd, "KPIM02", "list");
                        Sql.AppendParameter(cmd, CODE, 50, Sql.SqlFilterMode.Exact, "CODE");
                        cmd.CommandText += " order by NAME" + ControlChars.CrLf;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(0, count, dt);
                                Dictionary<string, object> row;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    row = new Dictionary<string, object>();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row.Add(col.ColumnName, dr[col]);
                                    }
                                    rows.Add(row);
                                }

                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return json.Serialize(rows);
        }

        public string DataTableToJSON(DataTable dt)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (DataRow dtRow in dt.Rows)
            {
                object[] arr = new object[dt.Rows.Count + 1];

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    arr[i] = dt.Rows[i].ItemArray;
                }

                dict.Add(dt.TableName, arr);
            }
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }
    }
}

