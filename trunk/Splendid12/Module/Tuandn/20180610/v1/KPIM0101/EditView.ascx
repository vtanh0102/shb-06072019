<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIM0101.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    #ctl00_cntBody_ctlEditView_ASSIGNED_TO_NAME {
        width: 30%;
    }
    #ctl00_cntBody_ctlEditView_ASSIGNED_TO{
        width: 25%;
    }
    .dataField input[type=text], .dataField input[type=file], .dataField input[type=password], .dataField select {
        background-color: #d7f5ed;
        border: 1px solid #a4e8d5;
        border-radius: 4px;
        line-height: 30px;
        margin: 8px 0px 8px 0px;
        padding: 4px;
    }
    /* 12/24/2017 Paul.  Set width to 75% instead of default width. */
    .dataField input[type=text], .dataField input[type=file], .dataField input[type=password] {
        height: 20px;
        width: 75%;
    }
    /* 12/24/2017 Paul.  We need to make space for select and clear button. */
    .modulePopupText {
        width: 50%;
    }

    .dataField textarea {
        background-color: #d7f5ed;
        border: 1px solid #a4e8d5;
        border-radius: 4px;
        margin: 8px 0px 8px 0px;
        padding: 4px;
    }
</style>

<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIM0101" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="No.">
                        <HeaderStyle CssClass="dataLabel" />                        
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_TYPE" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtKPI_ID" Text='<%# Bind("KPI_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_NAME" ItemStyle-Width="30%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtKPI_NAME" Text='<%# Bind("NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_UNIT" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlUNIT" runat="server" DataTextField="UNIT" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtRATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_MAX_RATIO_COMPL" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMAX_RATIO_COMPL" Text='<%# Bind("MAX_RATIO_COMPLETE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.LBL_LIST_REMARK" ItemStyle-Width="90%">
                        <HeaderStyle CssClass="dataLabel" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtREMARK" Text='<%# Bind("REMARK") %>' runat="server" />
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="btnInsert" CommandName="Insert" Text='<%# L10n.Term(".LBL_ADD_BUTTON_LABEL") %>' Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server" />
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton Visible='<%# Container.ItemIndex != grdMain.EditItemIndex && SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Delete" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' SkinID="delete_inline" runat="server" />
                            <asp:LinkButton Visible='<%# Container.ItemIndex != grdMain.EditItemIndex && SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Delete" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_DELETE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript">
                    $(function () {
                        var editId = getUrlParameter('ID');
                        if (editId == null) {
                            $("[id$=ctl00_cntBody_ctlEditView_ASSIGNED_TO]").closest("tr").hide();
                        }

                        $("[id$=txtKPI_ID]").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: '<%=ResolveUrl("~/KPIM0101/AutoComplete.asmx/GetKPIM02") %>',
                                    data: "{ 'prefixText': '" + request.term + "', 'count': 10}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('-')[0],
                                                val: item.split('-')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                //console.log(i.item);
                                GetKPIM02_Detail(this, i.item.value);
                            },
                            minLength: 1
                        });
                    });

                    function GetKPIM02_Detail(thizz, code) {
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/KPIM0101/AutoComplete.asmx/GetKPIM02_Detail") %>',
                            data: '{CODE: "' + code + '", count: 10 }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var objData = $.parseJSON(data.d);
                                var json = objData[0];
                                //console.log(json);
                                //$(thizz).closest("tr").find("input[id$=txtID]").val(json['ID']);
                                $(thizz).closest("tr").find("input[name$=txtKPI_NAME]").val(json['NAME']);
                                $(thizz).closest("tr").find("[id$=ddlUNIT]").val(json['UNIT']);
                                $(thizz).closest("tr").find("[id$=txtRATIO]").val(json['RADIO']);
                                $(thizz).closest("tr").find("[id$=txtMAX_RATIO_COMPL]").val(json['MAX_RATIO_COMPLETE']);
                                $(thizz).closest("tr").find("[id$=txtREMARK]").val(json['REMARK']);
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }

                    function getUrlParameter(sParam) {
                        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                            sURLVariables = sPageURL.split('&'),
                            sParameterName,
                            i;
                        for (i = 0; i < sURLVariables.length; i++) {
                            sParameterName = sURLVariables[i].split('=');

                            if (sParameterName[0] === sParam) {
                                return sParameterName[1] === undefined ? true : sParameterName[1];
                            }
                        }
                    };

                    // http://www.isocra.com/2008/02/table-drag-and-drop-jquery-plugin/
                    $(document).ready(function () {

                        $("#<%= grdMain.ClientID %>").tableDnD({
                            dragHandle: "dragHandle",
                            onDragClass: "jQueryDragBorder",
                            onDragStart: function (table, row) {
                                var txtINDEX = document.getElementById('<%= txtINDEX.ClientID %>');
                                txtINDEX.value = (row.parentNode.rowIndex - 1);
                            },
                            onDrop: function (table, row) {
                                var txtINDEX = document.getElementById('<%= txtINDEX.ClientID %>');
                                txtINDEX.value += ',' + (row.rowIndex - 1);
                                document.getElementById('<%= btnINDEX_MOVE.ClientID %>').click();
                            }
                        });
                        $("#<%= grdMain.ClientID %> tr").hover(
                            function () {
                                if (!$(this).hasClass("nodrag"))
                                    $(this.cells[0]).addClass('jQueryDragHandle');
                            },
                            function () {
                                if (!$(this).hasClass("nodrag"))
                                    $(this.cells[0]).removeClass('jQueryDragHandle');
                            }
                        );
                    });
                </script>
            </SplendidCRM:InlineScript>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
