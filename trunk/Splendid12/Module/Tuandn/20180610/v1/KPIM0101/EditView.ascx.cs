using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.KPIM0101
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        //detail info
        protected DataView vwMain;
        protected SplendidGrid grdMain;
        protected bool bEnableAdd = true;
        protected DataView CartView;
        protected static ArrayList gIDeleteRowList = new ArrayList();

        private void FirstDataGridViewRow()
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("NO", typeof(string)));
                dt.Columns.Add(new DataColumn("ID", typeof(string)));
                dt.Columns.Add(new DataColumn("AutoCompleteKPI_ID", typeof(string)));
                dt.Columns.Add(new DataColumn("KPI_ID", typeof(string)));
                dt.Columns.Add(new DataColumn("NAME", typeof(string)));
                dt.Columns.Add(new DataColumn("UNIT", typeof(string)));
                dt.Columns.Add(new DataColumn("RATIO", typeof(string)));
                dt.Columns.Add(new DataColumn("MAX_RATIO_COMPLETE", typeof(string)));
                dt.Columns.Add(new DataColumn("REMARK", typeof(string)));
                dr = dt.NewRow();
                dr["NO"] = 1;
                dr["ID"] = string.Empty;
                dr["AutoCompleteKPI_ID"] = string.Empty;
                dr["KPI_ID"] = string.Empty;
                dr["NAME"] = string.Empty;
                dr["UNIT"] = string.Empty;
                dr["RATIO"] = string.Empty;
                dr["MAX_RATIO_COMPLETE"] = string.Empty;
                dr["REMARK"] = string.Empty;
                dt.Rows.Add(dr);

                ViewState["CurrentTable"] = dt;

                grdMain.DataSource = dt;
                grdMain.DataBind();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void grdMain_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.CssClass += " nodrag nodrop";
            }
        }

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUNIT = (e.Item.Cells[4].FindControl("ddlUNIT") as DropDownList);
                Load_DropdownListUNIT(ddlUNIT);
            }
        }

        private void Load_DropdownListUNIT(DropDownList ddlUNIT)
        {
            if (ddlUNIT != null)
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "SELECT NAME, DISPLAY_NAME   " + ControlChars.CrLf
                         + "  FROM vwTERMINOLOGY_List   " + ControlChars.CrLf
                         + " WHERE 1 = 1                " + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Sql.AppendParameter(cmd, "CURRENCY_UNIT_LIST", 50, Sql.SqlFilterMode.Exact, "LIST_NAME");
                        Sql.AppendParameter(cmd, L10n.NAME.ToLower(), 10, Sql.SqlFilterMode.Exact, "LANG");
                        cmd.CommandText += " ORDER BY LIST_ORDER";

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);

                                ddlUNIT.DataTextField = "DISPLAY_NAME";
                                ddlUNIT.DataValueField = "NAME";
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    ddlUNIT.DataSource = dt;
                                    ddlUNIT.DataBind();
                                }
                            }
                        }
                    }

                }
            }
        }


        private void AddNewRow()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");

                        //HiddenField kpiID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("txtID");
                        TextBox txtKPI_ID = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_ID");
                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[4].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        //dtCurrentTable.Rows[i - 1]["NO"] = i + 1; 
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = txtKPI_ID.Text;
                        dtCurrentTable.Rows[i - 1]["NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text;
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text;
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    grdMain.DataSource = dtCurrentTable;
                    grdMain.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }


        private void AddNewRowForEdit()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        //HiddenField kpiID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("txtID");
                        TextBox txtKPI_ID = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_ID");
                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[4].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        //dtCurrentTable.Rows[i - 1]["NO"] = i + 1; 
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = txtKPI_ID.Text;
                        dtCurrentTable.Rows[i - 1]["NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text == string.Empty ? 0 : double.Parse(ratio.Text);
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text == string.Empty ? 0 : double.Parse(maxRatioComplete.Text);
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    grdMain.DataSource = dtCurrentTable;
                    grdMain.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        TextBox txtKPI_ID = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_ID");
                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[4].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtREMARK");

                        lblNO.Text = (i + 1).ToString();
                        txtKPI_ID.Text = dt.Rows[i]["KPI_ID"].ToString();
                        kpiName.Text = dt.Rows[i]["NAME"].ToString();
                        unit.SelectedValue = dt.Rows[i]["UNIT"].ToString();
                        ratio.Text = dt.Rows[i]["RATIO"].ToString();
                        maxRatioComplete.Text = dt.Rows[i]["MAX_RATIO_COMPLETE"].ToString();
                        remark.Text = dt.Rows[i]["REMARK"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        private void SetPreviousDataForEdit()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        TextBox txtKPI_ID = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_ID");
                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[4].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtREMARK");

                        lblNO.Text = (i + 1).ToString();
                        txtKPI_ID.Text = dt.Rows[i]["KPI_ID"].ToString();
                        kpiName.Text = dt.Rows[i]["NAME"].ToString();
                        unit.Items.FindByText(dt.Rows[i]["UNIT"].ToString()).Selected = true;
                        ratio.Text = dt.Rows[i]["RATIO"].ToString();
                        maxRatioComplete.Text = dt.Rows[i]["MAX_RATIO_COMPLETE"].ToString();
                        remark.Text = dt.Rows[i]["REMARK"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        private void SetRowData()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        HiddenField kpiID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("txtID");
                        TextBox txtKPI_ID = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_ID");
                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[4].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[5].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["ID"] = kpiID.Value;
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = txtKPI_ID.Text;
                        dtCurrentTable.Rows[i - 1]["NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text;
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text;
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }

                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
        }

        private void grdMain_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Edit":
                        {
                            grdMain.EditItemIndex = e.Item.ItemIndex;
                            grdMain.ShowFooter = false;
                            grdMain.DataBind();
                            break;
                        }
                    case "Cancel":
                        {
                            grdMain.EditItemIndex = -1;
                            grdMain.ShowFooter = bEnableAdd;
                            grdMain.DataBind();
                            break;
                        }
                    case "Delete":
                        {
                            if (Sql.IsEmptyGuid(gID))
                            {
                                SetRowData();
                                if (ViewState["CurrentTable"] != null)
                                {
                                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                                    DataRow drCurrentRow = null;
                                    int rowIndex = Convert.ToInt32(e.Item.ItemIndex);
                                    if (dt.Rows.Count > 1)
                                    {
                                        dt.Rows.Remove(dt.Rows[rowIndex]);
                                        drCurrentRow = dt.NewRow();
                                        ViewState["CurrentTable"] = dt;
                                        grdMain.DataSource = dt;
                                        grdMain.DataBind();
                                        SetPreviousData();
                                    }
                                }
                            }
                            else
                            {
                                if (ViewState["CurrentTable"] != null)
                                {
                                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                                    DataRow drCurrentRow = null;
                                    int rowIndex = Convert.ToInt32(e.Item.ItemIndex);
                                    if (dt.Rows.Count > 1)
                                    {
                                        //dt.Rows[rowIndex];
                                        HiddenField kpiID_Details = (HiddenField)e.Item.FindControl("txtID");
                                        Guid gIDeleteRow = Sql.ToGuid(kpiID_Details.Value);
                                        if (!Sql.IsEmptyGuid(gIDeleteRow))
                                        {
                                            gIDeleteRowList.Add(gIDeleteRow);
                                        }

                                        dt.Rows.Remove(dt.Rows[rowIndex]);
                                        drCurrentRow = dt.NewRow();
                                        ViewState["CurrentTable"] = dt;
                                        grdMain.DataSource = dt;
                                        grdMain.DataBind();
                                        SetPreviousDataForEdit();
                                    }
                                }
                            }
                            break;
                        }
                    case "Update":
                        {
                            Guid gCase = Sql.ToGuid(Request["ID"]);
                            Guid gID = Sql.ToGuid(vwMain[e.Item.ItemIndex]["ID_C"]);
                            grdMain.EditItemIndex = -1;
                            grdMain.ShowFooter = bEnableAdd;
                            break;
                        }
                    case "Insert":
                        {
                            if (Sql.IsEmptyGuid(gID))
                            {
                                AddNewRow();
                            }
                            else
                            {
                                AddNewRowForEdit();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;
                                    Security.Filter(cmd, m_sMODULE, "edit");
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;
                                    SqlProcs.spM_GROUP_KPIS_Update
                                        (ref gID
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_STATUS").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").Text == string.Empty ? Security.FULL_NAME : new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue == DateTime.MinValue ? DateTime.Now.Date : new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue 
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TYPE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "IS_COMMON").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "POSITION_ID").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                        , trn
                                        );

                                    if (ViewState["CurrentTable"] != null)
                                    {
                                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                        if (dtCurrentTable.Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                            {
                                                HiddenField hdnfID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("txtID");
                                                TextBox kpiID = (TextBox)grdMain.Items[i].Cells[2].FindControl("txtKPI_ID");
                                                TextBox kpiName = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtKPI_NAME");
                                                DropDownList unit = (DropDownList)grdMain.Items[i].Cells[4].FindControl("ddlUNIT");
                                                TextBox ratio = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtRATIO");
                                                TextBox maxRatioComplete = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                                                TextBox remark = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtREMARK");
                                                Guid gIDT;
                                                Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                                {
                                                    gIDT = Guid.NewGuid();
                                                }
                                                else
                                                {
                                                    if (hdnfID.Value == null || hdnfID.Value == string.Empty)
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        gIDT = Guid.Parse(hdnfID.Value);
                                                    }
                                                }


                                                SqlProcs.spM_GROUP_KPI_DETAILS_Update
                                                     (ref gIDT
                                                     , gASSIGNED_USER_ID
                                                     , gTEAM_ID
                                                     , string.Empty
                                                     , kpiName.Text
                                                     , gID.ToString()
                                                     , unit.SelectedItem.Text
                                                     , float.Parse(ratio.Text)
                                                     , float.Parse(maxRatioComplete.Text)
                                                     , kpiID.Text
                                                     , remark.Text
                                                     , string.Empty
                                                     , string.Empty
                                                     , trn
                                                     );
                                            }
                                            ViewState["CurrentTable"] = dtCurrentTable;
                                            if (gIDeleteRowList != null && gIDeleteRowList.Count > 0)
                                            {
                                                foreach (var gIDeleteRow in gIDeleteRowList)
                                                {
                                                    SqlProcs.spM_GROUP_KPI_DETAILS_Delete(Guid.Parse(gIDeleteRow.ToString()), trn);
                                                }
                                            }
                                        }
                                    }

                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    SplendidCRM.SqlProcs.spTRACKER_Update
                                        (Security.USER_ID
                                        , m_sMODULE
                                        , gID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                        , "save"
                                        , trn
                                        );
                                    if (plcSubPanel.Visible)
                                    {
                                        foreach (Control ctl in plcSubPanel.Controls)
                                        {
                                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                            if (ctlSubPanel != null)
                                            {
                                                ctlSubPanel.Save(gID, m_sMODULE, trn);
                                            }
                                        }
                                    }
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {

                // 07/25/2010 Paul.  Lets experiment with jQuery drag and drop. 
                ScriptManager mgrAjax = ScriptManager.GetCurrent(this.Page);
                ScriptReference scrJQueryTableDnD = new ScriptReference("~/Include/javascript/jquery.tablednd_0_5.js");
                if (!mgrAjax.Scripts.Contains(scrJQueryTableDnD))
                    mgrAjax.Scripts.Add(scrJQueryTableDnD);

                //bEnableAdd = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);

                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                Security.Filter(cmd, m_sMODULE, "edit");
                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            if (!Sql.IsEmptyGuid(gDuplicateID))
                                            {
                                                TextBox txtCODE = this.FindControl("CODE") as TextBox;
                                                string strKPIsCode = KPIs_Utils.GenerateKPIsCode();
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.ReadOnly = true;
                                                    txtCODE.Text = strKPIsCode;
                                                }
                                            }
                                            TextBox txtNAME = this.FindControl("NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();
                                            DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                                            if (ddlYEAR != null)
                                            {
                                                KPIs_Utils.FillDropdownListOfYear(ddlYEAR);
                                            }
                                            
                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["NAME"] = Sql.ToString(rdr["NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["NAME"] = ViewState["NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (Sql.IsEmptyGuid(gID))
                        {
                            Load_KPI_Detail(dbf, gDuplicateID);
                        }
                        else
                        {
                            Load_KPI_Detail(dbf, gID);
                        }
                    }
                    else
                    {
                        FirstDataGridViewRow();

                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        //SET DEFAULT 
                        TextBox txtCODE = this.FindControl("CODE") as TextBox;
                        string strKPIsCode = KPIs_Utils.GenerateKPIsCode();
                        if (txtCODE != null)
                        {
                            txtCODE.ReadOnly = true;
                            txtCODE.Text = strKPIsCode;
                        }
                        TextBox txtNAME = this.FindControl("NAME") as TextBox;
                        if (txtNAME != null)
                        {
                            txtNAME.Focus();
                        }
                        DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                        if (ddlYEAR != null)
                        {
                            KPIs_Utils.FillDropdownListOfYear(ddlYEAR);
                        }
                        Label lblVERSION_NUMBER = this.FindControl("VERSION_NUMBER") as Label;
                        if (lblVERSION_NUMBER != null)
                        {
                            lblVERSION_NUMBER.Text = strKPIsCode.Split('-')[1].ToString();
                        }
                        DropDownList ddlAPPROVE_STATUS = this.FindControl("APPROVE_STATUS") as DropDownList;
                        if (ddlAPPROVE_STATUS != null)
                        {
                            ddlAPPROVE_STATUS.Enabled = false;
                        }                        
                        //END SET DEFAULT

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["NAME"] = ViewState["NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Load_KPI_Detail(DbProviderFactory dbf, Guid gID)
        {
            //DETAILS LIST                           
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL_DETAILS;
                sSQL_DETAILS = "SELECT '' AS NO, ID, KPI_ID, NAME, UNIT, RATIO, MAX_RATIO_COMPLETE, DESCRIPTION, REMARK     " + ControlChars.CrLf
                     + "  FROM vwM_GROUP_KPI_DETAILS_Edit   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL_DETAILS;
                    Security.Filter(cmd, m_sMODULE, "edit");
                    Sql.AppendParameter(cmd, gID, "GROUP_KPI_ID", false);
                    //Sql.AppendParameter(cmd, 0, "DELETED", false);//NO SELECT DELETED
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                //KPIDetails
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                grdMain.ShowFooter = bEnableAdd;
                                for (int i = 0; i < grdMain.Items.Count; i++)
                                {
                                    //FirstDataGridViewRow();
                                    Label lblNO = (Label)grdMain.Items[i].Cells[1].FindControl("lblNO");
                                    HiddenField kpiID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("txtID");
                                    TextBox txtKPI_ID = (TextBox)grdMain.Items[i].Cells[2].FindControl("txtKPI_ID");
                                    TextBox kpiName = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtKPI_NAME");
                                    DropDownList unit = (DropDownList)grdMain.Items[i].Cells[4].FindControl("ddlUNIT");
                                    TextBox ratio = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtRATIO");
                                    TextBox maxRatioComplete = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtMAX_RATIO_COMPL");
                                    TextBox remark = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtREMARK");
                                    //SET VALUE
                                    lblNO.Text = (i + 1).ToString();
                                    kpiID.Value = dtCurrent.Rows[i]["ID"].ToString();
                                    txtKPI_ID.Text = dtCurrent.Rows[i]["KPI_ID"].ToString();
                                    kpiName.Text = dtCurrent.Rows[i]["NAME"].ToString();
                                    unit.Items.FindByText(dtCurrent.Rows[i]["UNIT"].ToString()).Selected = true;
                                    ratio.Text = dtCurrent.Rows[i]["RATIO"].ToString();
                                    maxRatioComplete.Text = dtCurrent.Rows[i]["MAX_RATIO_COMPLETE"].ToString();
                                    remark.Text = dtCurrent.Rows[i]["DESCRIPTION"].ToString();

                                }
                            }
                        }
                    }
                }

            }
            //End Details list
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);

            grdMain.ItemCommand += new DataGridCommandEventHandler(grdMain_ItemCommand);
            grdMain.ItemCreated += new DataGridItemEventHandler(grdMain_ItemCreated);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIM0101";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
