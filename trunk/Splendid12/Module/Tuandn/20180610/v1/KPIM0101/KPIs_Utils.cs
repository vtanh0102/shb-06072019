﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace SplendidCRM.KPIM0101
{
    public class KPIs_Utils
    {

        private static string M_GROUP_KPIS_CODE = "KPIs{0}-{1}";
        private static Int32 NUM_OF_YEAR = 5;
        public static string STATUS_ACTIVE = "Active";
        public static string STATUS_INACTIVE = "InActive";

        public static int ACTIVE = 1;
        public static int INACTIVE = 0;

        public static string M_GROUP_KPIS_TYPE         = "M_GROUP_KPIS_TYPE";
        public static string M_GROUP_KPIS_ORGANIZATION = "M_GROUP_KPIS_ORGANIZATION";
        public static string YES_NO_DOM                = "yesno_dom";
                

        public static string GenerateKPIsCode()
        {
            string year = DateTime.Now.Year.ToString();
            string number = getNumberCodeFromTable().ToString().PadLeft(5, '0');
            string KPIsCode = string.Format(M_GROUP_KPIS_CODE, year, number);
            return KPIsCode;
        }

        public static void FillDropdownListOfYear(System.Web.UI.WebControls.DropDownList ddlYEAR)
        {
            ddlYEAR.Items.Clear();
            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= NUM_OF_YEAR; i++)
            {
                // Now just add an entry that's the current year
                ddlYEAR.Items.Add((currentYear + i).ToString());
            }
        }

        static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);

        }

        private static string getNumberCodeFromTable()
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            Int32 numberCode = 0;
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT MAX(CODE) AS CODE          " + ControlChars.CrLf
                                     + "  FROM vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                string code = rdr["CODE"].ToString();
                                if (code != null && code != string.Empty)
                                {
                                    numberCode = Int32.Parse(code.Split('-')[1].ToString()) + 1;
                                }
                                else
                                {
                                    numberCode = 1;
                                }
                            }

                        }
                        else
                        {
                            numberCode = 1;
                        }
                    }
                }
            }
            return numberCode.ToString();
        }

        public static void KPI_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, GROUP_KPI_ID                      " + ControlChars.CrLf
                                     + "  FROM vwM_GROUP_KPI_DETAILS_List   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Security.Filter(cmd, "KPIM0101", "list");
                    Sql.AppendParameter(cmd, gID, "GROUP_KPI_ID");
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    SqlProcs.spM_GROUP_KPI_DETAILS_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }

        public static string Get_DisplayName(string name, string listName)
        {
            string displayName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT NAME, DISPLAY_NAME            " + ControlChars.CrLf
                                     + "  FROM vwTERMINOLOGY_List   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Security.Filter(cmd, "KPIM0101", "list");
                    Sql.AppendParameter(cmd, name, "NAME");
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                
                            }

                        }

                    }
                }
            }
            return displayName;
        }


    }
}