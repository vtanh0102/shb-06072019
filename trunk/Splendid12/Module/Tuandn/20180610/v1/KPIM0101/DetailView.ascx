<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIM0101.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divDetailView" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIM0101" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
	<SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIM0101" Visible="<%# !PrintView %>" Runat="Server" />

	<asp:HiddenField ID="LAYOUT_DETAIL_VIEW" Runat="server" />
	<table ID="tblMain" class="tabDetailView" runat="server">
	</table>

	<div id="divDetailSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
	</div>

    <asp:UpdatePanel runat="server">
		<ContentTemplate>
			<asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
				<asp:HiddenField ID="txtINDEX" Runat="server" />
				<asp:Button ID="btnINDEX_MOVE" style="display: none" runat="server" />
				<asp:Label ID="Label1" CssClass="error" EnableViewState="false" Runat="server" />
			</asp:Panel>
			
			<SplendidCRM:SplendidGrid id="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
				<Columns>
					<asp:TemplateColumn ItemStyle-CssClass="dragHandle">
						<ItemTemplate><asp:Image SkinID="blank" Width="14px" runat="server" /></ItemTemplate>
					</asp:TemplateColumn>
                 
					<asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_TYPE"   ItemStyle-Width="10%">
						<ItemTemplate><asp:Label ID="txtKPI_ID" runat="server" Text='<%# Eval("KPI_ID") %>'/></ItemTemplate>											
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_NAME" ItemStyle-Width="20%">
						<ItemTemplate><asp:Label ID="txtKPI_NAME" Text='<%# Eval("NAME") %>' runat="server" /></ItemTemplate>
					</asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_UNIT" ItemStyle-Width="10%">
						<ItemTemplate><asp:Label ID="ddlUNIT" runat="server" Text='<%# Eval("UNIT") %>'/></ItemTemplate>
					</asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO" ItemStyle-Width="10%">
						<ItemTemplate><asp:Label ID="txtRATIO" Text='<%# Eval("RATIO") %>' runat="server" /></ItemTemplate>
					</asp:TemplateColumn>               
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_MAX_RATIO_COMPL" ItemStyle-Width="10%">
						<ItemTemplate><asp:Label ID="txtMAX_RATIO_COMPL" Text='<%# Eval("MAX_RATIO_COMPLETE") %>' runat="server" /></ItemTemplate>
					</asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.LBL_LIST_REMARK" ItemStyle-Width="80%" HeaderStyle-HorizontalAlign="Center">
						<ItemTemplate><asp:Label ID="txtREMARK" Text='<%# Eval("DESCRIPTION") %>' runat="server" /></ItemTemplate>                                       
					</asp:TemplateColumn>             
				</Columns>
	        </SplendidCRM:SplendidGrid>
      </ContentTemplate>
    </asp:UpdatePanel>
    <SplendidCRM:InlineScript runat="server">
          <script type="text/javascript">
              $(function () {
                  var approveStatus = $("[id$=APPROVE_STATUS]").text();
                  if (approveStatus.match("^0")) {
                      $("[id$=APPROVED_BY]").closest("tr").hide();
                  }
              });
           </script>
    </SplendidCRM:InlineScript>
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
