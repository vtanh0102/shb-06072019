<%@ Control Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.SplendidControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Diagnostics" %>
<script runat="server">
	public partial class SqlProcs
	{
		#region spKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static void spKPIM02_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sCODE, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME)
		{
			SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.Transaction = trn;
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = "spKPIM02_Update";
							IDbDataParameter parID                     = Sql.AddParameter(cmd, "@ID"                    , gID                      );
							IDbDataParameter parMODIFIED_USER_ID       = Sql.AddParameter(cmd, "@MODIFIED_USER_ID"      ,  Security.USER_ID        );
							IDbDataParameter parASSIGNED_USER_ID       = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID"      , gASSIGNED_USER_ID        );
							IDbDataParameter parTEAM_ID                = Sql.AddParameter(cmd, "@TEAM_ID"               , gTEAM_ID                 );
							IDbDataParameter parTEAM_SET_LIST          = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST"         , sTEAM_SET_LIST           , 8000);
							IDbDataParameter parNAME                   = Sql.AddParameter(cmd, "@NAME"                  , sNAME                    , 150);
							IDbDataParameter parCODE                   = Sql.AddParameter(cmd, "@CODE"                  , sCODE                    ,  50);
							IDbDataParameter parPARENT_ID              = Sql.AddParameter(cmd, "@PARENT_ID"             , sPARENT_ID               ,  50);
							IDbDataParameter parLEVEL_NUMBER           = Sql.AddParameter(cmd, "@LEVEL_NUMBER"          , nLEVEL_NUMBER            );
							IDbDataParameter parUNIT                   = Sql.AddParameter(cmd, "@UNIT"                  , nUNIT                    );
							IDbDataParameter parRADIO                  = Sql.AddParameter(cmd, "@RADIO"                 , flRADIO                  );
							IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE"    , flMAX_RATIO_COMPLETE     );
							IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE"    , flMIN_RATIO_COMPLETE     );
							IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE );
							IDbDataParameter parSTATUS                 = Sql.AddParameter(cmd, "@STATUS"                , sSTATUS                  ,  50);
							IDbDataParameter parMAX_POINT              = Sql.AddParameter(cmd, "@MAX_POINT"             , nMAX_POINT               );
							IDbDataParameter parMIN_POINT              = Sql.AddParameter(cmd, "@MIN_POINT"             , nMIN_POINT               );
							IDbDataParameter parDEFAULT_POINT          = Sql.AddParameter(cmd, "@DEFAULT_POINT"         , nDEFAULT_POINT           );
							IDbDataParameter parSORTING_ORDER          = Sql.AddParameter(cmd, "@SORTING_ORDER"         , nSORTING_ORDER           );
							IDbDataParameter parDESCRIPTION            = Sql.AddParameter(cmd, "@DESCRIPTION"           , sDESCRIPTION             , 200);
							IDbDataParameter parREMARK                 = Sql.AddParameter(cmd, "@REMARK"                , sREMARK                  , 200);
							IDbDataParameter parTAG_SET_NAME           = Sql.AddParameter(cmd, "@TAG_SET_NAME"          , sTAG_SET_NAME            , 4000);
							parID.Direction = ParameterDirection.InputOutput;
							cmd.ExecuteNonQuery();
							gID = Sql.ToGuid(parID.Value);
						}
						trn.Commit();
					}
					catch
					{
						trn.Rollback();
						throw;
					}
				}
			}
		}
		#endregion

		#region spKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static void spKPIM02_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sCODE, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME, IDbTransaction trn)
		{
			IDbConnection con = trn.Connection;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.Transaction = trn;
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "spKPIM02_Update";
				IDbDataParameter parID                     = Sql.AddParameter(cmd, "@ID"                    , gID                      );
				IDbDataParameter parMODIFIED_USER_ID       = Sql.AddParameter(cmd, "@MODIFIED_USER_ID"      ,  Security.USER_ID        );
				IDbDataParameter parASSIGNED_USER_ID       = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID"      , gASSIGNED_USER_ID        );
				IDbDataParameter parTEAM_ID                = Sql.AddParameter(cmd, "@TEAM_ID"               , gTEAM_ID                 );
				IDbDataParameter parTEAM_SET_LIST          = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST"         , sTEAM_SET_LIST           , 8000);
				IDbDataParameter parNAME                   = Sql.AddParameter(cmd, "@NAME"                  , sNAME                    , 150);
				IDbDataParameter parCODE                   = Sql.AddParameter(cmd, "@CODE"                  , sCODE                    ,  50);
				IDbDataParameter parPARENT_ID              = Sql.AddParameter(cmd, "@PARENT_ID"             , sPARENT_ID               ,  50);
				IDbDataParameter parLEVEL_NUMBER           = Sql.AddParameter(cmd, "@LEVEL_NUMBER"          , nLEVEL_NUMBER            );
				IDbDataParameter parUNIT                   = Sql.AddParameter(cmd, "@UNIT"                  , nUNIT                    );
				IDbDataParameter parRADIO                  = Sql.AddParameter(cmd, "@RADIO"                 , flRADIO                  );
				IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE"    , flMAX_RATIO_COMPLETE     );
				IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE"    , flMIN_RATIO_COMPLETE     );
				IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE );
				IDbDataParameter parSTATUS                 = Sql.AddParameter(cmd, "@STATUS"                , sSTATUS                  ,  50);
				IDbDataParameter parMAX_POINT              = Sql.AddParameter(cmd, "@MAX_POINT"             , nMAX_POINT               );
				IDbDataParameter parMIN_POINT              = Sql.AddParameter(cmd, "@MIN_POINT"             , nMIN_POINT               );
				IDbDataParameter parDEFAULT_POINT          = Sql.AddParameter(cmd, "@DEFAULT_POINT"         , nDEFAULT_POINT           );
				IDbDataParameter parSORTING_ORDER          = Sql.AddParameter(cmd, "@SORTING_ORDER"         , nSORTING_ORDER           );
				IDbDataParameter parDESCRIPTION            = Sql.AddParameter(cmd, "@DESCRIPTION"           , sDESCRIPTION             , 200);
				IDbDataParameter parREMARK                 = Sql.AddParameter(cmd, "@REMARK"                , sREMARK                  , 200);
				IDbDataParameter parTAG_SET_NAME           = Sql.AddParameter(cmd, "@TAG_SET_NAME"          , sTAG_SET_NAME            , 4000);
				parID.Direction = ParameterDirection.InputOutput;
				Sql.Trace(cmd);
				cmd.ExecuteNonQuery();
				gID = Sql.ToGuid(parID.Value);
			}
		}
		#endregion

		#region cmdKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static IDbCommand cmdKPIM02_Update(IDbConnection con)
		{
			IDbCommand cmd = con.CreateCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "spKPIM02_Update";
			IDbDataParameter parID                     = Sql.CreateParameter(cmd, "@ID"                    , "Guid",  16);
			IDbDataParameter parMODIFIED_USER_ID       = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID"      , "Guid",  16);
			IDbDataParameter parASSIGNED_USER_ID       = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID"      , "Guid",  16);
			IDbDataParameter parTEAM_ID                = Sql.CreateParameter(cmd, "@TEAM_ID"               , "Guid",  16);
			IDbDataParameter parTEAM_SET_LIST          = Sql.CreateParameter(cmd, "@TEAM_SET_LIST"         , "ansistring", 8000);
			IDbDataParameter parNAME                   = Sql.CreateParameter(cmd, "@NAME"                  , "string", 150);
			IDbDataParameter parCODE                   = Sql.CreateParameter(cmd, "@CODE"                  , "string",  50);
			IDbDataParameter parPARENT_ID              = Sql.CreateParameter(cmd, "@PARENT_ID"             , "string",  50);
			IDbDataParameter parLEVEL_NUMBER           = Sql.CreateParameter(cmd, "@LEVEL_NUMBER"          , "Int32",   4);
			IDbDataParameter parUNIT                   = Sql.CreateParameter(cmd, "@UNIT"                  , "Int32",   4);
			IDbDataParameter parRADIO                  = Sql.CreateParameter(cmd, "@RADIO"                 , "float",   8);
			IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE"    , "float",   8);
			IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.CreateParameter(cmd, "@MIN_RATIO_COMPLETE"    , "float",   8);
			IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@DEFAULT_RATIO_COMPLETE", "float",   8);
			IDbDataParameter parSTATUS                 = Sql.CreateParameter(cmd, "@STATUS"                , "string",  50);
			IDbDataParameter parMAX_POINT              = Sql.CreateParameter(cmd, "@MAX_POINT"             , "Int32",   4);
			IDbDataParameter parMIN_POINT              = Sql.CreateParameter(cmd, "@MIN_POINT"             , "Int32",   4);
			IDbDataParameter parDEFAULT_POINT          = Sql.CreateParameter(cmd, "@DEFAULT_POINT"         , "Int32",   4);
			IDbDataParameter parSORTING_ORDER          = Sql.CreateParameter(cmd, "@SORTING_ORDER"         , "Int32",   4);
			IDbDataParameter parDESCRIPTION            = Sql.CreateParameter(cmd, "@DESCRIPTION"           , "string", 200);
			IDbDataParameter parREMARK                 = Sql.CreateParameter(cmd, "@REMARK"                , "string", 200);
			IDbDataParameter parTAG_SET_NAME           = Sql.CreateParameter(cmd, "@TAG_SET_NAME"          , "string", 4000);
			parID.Direction = ParameterDirection.InputOutput;
			return cmd;
		}
		#endregion

	}




		protected Guid            gID                          ;
		protected bool            bInlineEditMode              = false;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
						DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
						SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							DataRow   rowCurrent = null;
							DataTable dtCurrent  = new DataTable();
							if ( !Sql.IsEmptyGuid(gID) )
							{
								string sSQL ;
								sSQL = "select *           " + ControlChars.CrLf
								     + "  from vwKPIM02_Edit" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Security.Filter(cmd, m_sMODULE, "edit");
									Sql.AppendParameter(cmd, gID, "ID", false);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											rowCurrent = dtCurrent.Rows[0];
											DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
											if ( Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"])  && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED )
											{
												ctlDynamicButtons.ShowButton("SaveConcurrency", true);
												ctlFooterButtons .ShowButton("SaveConcurrency", true);
												throw(new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
											}
										}
										else
										{
											gID = Guid.Empty;
										}
									}
								}
							}

							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
							bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
							if ( bDUPLICATE_CHECHING_ENABLED )
							{
								if ( Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0 )
								{
									ctlDynamicButtons.ShowButton("SaveDuplicate", true);
									ctlFooterButtons .ShowButton("SaveDuplicate", true);
									throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
								}
							}
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIM02_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "CODE"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "PARENT_ID"                          ).SelectedValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "LEVEL_NUMBER"                       ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "UNIT"                               ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "RADIO"                              ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_RATIO_COMPLETE"             ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS"                             ).SelectedValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_POINT"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "SORTING_ORDER"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION"                        ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK"                             ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME"                       ).Text
										, trn
										);

									SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
									SplendidCRM.SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
										, "save"
										, trn
										);
									if ( plcSubPanel.Visible )
									{
										foreach ( Control ctl in plcSubPanel.Controls )
										{
											InlineEditControl ctlSubPanel = ctl as InlineEditControl;
											if ( ctlSubPanel != null )
											{
												ctlSubPanel.Save(gID, m_sMODULE, trn);
											}
										}
									}
									trn.Commit();
									SplendidCache.ClearFavorites();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
							// 02/09/2015 Paul.  Need to specify the SplendidCRM namespace when not using code-behinds. 
							rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
							this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
						}
						
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else
							Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyGuid(gID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *           " + ControlChars.CrLf
							     + "  from vwKPIM02_Edit" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Security.Filter(cmd, m_sMODULE, "edit");
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AppendParameter(cmd, gID, "ID", false);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
											Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											bool bModuleIsAssigned  = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
											Guid gASSIGNED_USER_ID = Guid.Empty;
											if ( bModuleIsAssigned )
												gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

											this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
											this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											TextBox txtNAME = this.FindControl("NAME") as TextBox;
											if ( txtNAME != null )
												txtNAME.Focus();

											ViewState ["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"   ]);
											ViewState ["NAME"              ] = Sql.ToString  (rdr["NAME"            ]);
											ViewState ["ASSIGNED_USER_ID"  ] = gASSIGNED_USER_ID;
											Page.Items["NAME"              ] = ViewState ["NAME"            ];
											Page.Items["ASSIGNED_USER_ID"  ] = ViewState ["ASSIGNED_USER_ID"];
											
											this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
										}
										else
										{
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlDynamicButtons.DisableAll();
											ctlFooterButtons .DisableAll();
											ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
											plcSubPanel.Visible = false;
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("NAME") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						if ( bInlineEditMode )
							plcSubPanel.Visible = false;
						
						this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIM02";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
</script>
<div id="divEditView" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIM02" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:HiddenField ID="LAYOUT_EDIT_VIEW" Runat="server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblMain" class="tabEditView" runat="server">
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<div id="divEditSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
	</div>

	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
