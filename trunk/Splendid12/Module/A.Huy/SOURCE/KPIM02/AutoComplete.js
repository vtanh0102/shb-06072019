
function KPIM02_KPIM02_NAME_Changed(fldKPIM02_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldKPIM02_NAME.id.substring(0, fldKPIM02_NAME.id.length - 'KPIM02_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'KPIM02_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_KPIM02_NAME = document.getElementById(userContext + 'PREV_KPIM02_NAME');
	if ( fldPREV_KPIM02_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_KPIM02_NAME');
	}
	else if ( fldPREV_KPIM02_NAME.value != fldKPIM02_NAME.value )
	{
		if ( fldKPIM02_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIM02.AutoComplete.KPIM02_KPIM02_NAME_Get(fldKPIM02_NAME.value, KPIM02_KPIM02_NAME_Changed_OnSucceededWithContext, KPIM02_KPIM02_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('KPIM02_KPIM02_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			KPIM02_KPIM02_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function KPIM02_KPIM02_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'KPIM02_NAME_AjaxErrors');
		var fldKPIM02_ID        = document.getElementById(userContext + 'KPIM02_ID'       );
		var fldKPIM02_NAME      = document.getElementById(userContext + 'KPIM02_NAME'     );
		var fldPREV_KPIM02_NAME = document.getElementById(userContext + 'PREV_KPIM02_NAME');
		if ( fldKPIM02_ID        != null ) fldKPIM02_ID.value        = sID  ;
		if ( fldKPIM02_NAME      != null ) fldKPIM02_NAME.value      = sNAME;
		if ( fldPREV_KPIM02_NAME != null ) fldPREV_KPIM02_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIM02.AutoComplete service is null');
	}
}

function KPIM02_KPIM02_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'KPIM02_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldKPIM02_ID        = document.getElementById(userContext + 'KPIM02_ID'       );
	var fldKPIM02_NAME      = document.getElementById(userContext + 'KPIM02_NAME'     );
	var fldPREV_KPIM02_NAME = document.getElementById(userContext + 'PREV_KPIM02_NAME');
	if ( fldKPIM02_ID        != null ) fldKPIM02_ID.value        = '';
	if ( fldKPIM02_NAME      != null ) fldKPIM02_NAME.value      = '';
	if ( fldPREV_KPIM02_NAME != null ) fldPREV_KPIM02_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

