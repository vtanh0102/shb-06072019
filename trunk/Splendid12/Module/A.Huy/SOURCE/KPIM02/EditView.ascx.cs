using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.KPIM02
{

	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                          ;
		protected HtmlTable       tblMain                      ;
		protected PlaceHolder     plcSubPanel                  ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
						DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							DataRow   rowCurrent = null;
							DataTable dtCurrent  = new DataTable();
							if ( !Sql.IsEmptyGuid(gID) )
							{
								string sSQL ;
								sSQL = "select *           " + ControlChars.CrLf
								     + "  from vwKPIM02_Edit" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Security.Filter(cmd, m_sMODULE, "edit");
									Sql.AppendParameter(cmd, gID, "ID", false);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											rowCurrent = dtCurrent.Rows[0];
											DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
											if ( Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"])  && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED )
											{
												ctlDynamicButtons.ShowButton("SaveConcurrency", true);
												ctlFooterButtons .ShowButton("SaveConcurrency", true);
												throw(new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
											}
										}
										else
										{
											gID = Guid.Empty;
										}
									}
								}
							}

							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
							bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
							if ( bDUPLICATE_CHECHING_ENABLED )
							{
								if ( Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0 )
								{
									ctlDynamicButtons.ShowButton("SaveDuplicate", true);
									ctlFooterButtons .ShowButton("SaveDuplicate", true);
									throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
								}
							}
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIM02_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "CODE"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "PARENT_ID"                          ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "LEVEL_NUMBER"                       ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "UNIT"                               ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "RADIO"                              ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_RATIO_COMPLETE"             ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS"                             ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_POINT"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "SORTING_ORDER"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION"                        ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK"                             ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME"                       ).Text
										, trn
										);

									SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
									SplendidCRM.SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
										, "save"
										, trn
										);
									if ( plcSubPanel.Visible )
									{
										foreach ( Control ctl in plcSubPanel.Controls )
										{
											InlineEditControl ctlSubPanel = ctl as InlineEditControl;
											if ( ctlSubPanel != null )
											{
												ctlSubPanel.Save(gID, m_sMODULE, trn);
											}
										}
									}
									trn.Commit();
									SplendidCache.ClearFavorites();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
							rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
							this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
						}
						
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else
							Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyGuid(gID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *           " + ControlChars.CrLf
							     + "  from vwKPIM02_Edit" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Security.Filter(cmd, m_sMODULE, "edit");
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AppendParameter(cmd, gID, "ID", false);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
											Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											bool bModuleIsAssigned  = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
											Guid gASSIGNED_USER_ID = Guid.Empty;
											if ( bModuleIsAssigned )
												gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

											this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
											this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											TextBox txtNAME = this.FindControl("NAME") as TextBox;
											if ( txtNAME != null )
												txtNAME.Focus();

											ViewState ["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"   ]);
											ViewState ["NAME"              ] = Sql.ToString  (rdr["NAME"            ]);
											ViewState ["ASSIGNED_USER_ID"  ] = gASSIGNED_USER_ID;
											Page.Items["NAME"              ] = ViewState ["NAME"            ];
											Page.Items["ASSIGNED_USER_ID"  ] = ViewState ["ASSIGNED_USER_ID"];

                                            //TextBox txt_CODE = this.FindControl("CODE") as TextBox;
                                            //if (txt_CODE != null)
                                            //{
                                            //    txt_CODE.Text = generateCODE();
                                            //    //txt_CODE.ReadOnly = true;
                                            //}

											this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
										}
										else
										{
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlDynamicButtons.DisableAll();
											ctlFooterButtons .DisableAll();
											ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
											plcSubPanel.Visible = false;
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        //huynt
                        Guid urlPar_parentID = Guid.Empty;
                        if (Request["parent_id"] != null)
                        {
                            urlPar_parentID = Sql.ToGuid(Request["parent_id"]);
                            DropDownList ddlst_parentid = this.FindControl("PARENT_ID") as DropDownList;
                            ddlst_parentid.ClearSelection(); //making sure the previous selection has been cleared
                            ddlst_parentid.Items.FindByValue(urlPar_parentID.ToString()).Selected = true;
                        }

                        //end huynt
						TextBox txtNAME = this.FindControl("NAME") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
                        TextBox txt_CODE = this.FindControl("CODE") as TextBox;
                        if (txt_CODE != null)
                        {
                            txt_CODE.Text = generateCODE();
                            txt_CODE.ReadOnly = true;
                        }
						this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}
        private string generateCODE()
        {
            string headerCode = "";
            string enderCode = "";
            string code = "";
            //get chuoi tien to tu bang THAMSO_HT
            L10N L10n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
            DataTable dt = Cache.Get(L10n.NAME + ".vwTHAMSO_HT") as DataTable;
            dt = null;
            if (dt == null)
            {
                try
                {
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        con.Open();
                        string sSQL;
                        sSQL = "select VALUE   " + ControlChars.CrLf
                             + "  from THAMSO_HT     " + ControlChars.CrLf
                             + "  where DELETED = 0 and NAME = 'KPIM02_CODE'     " + ControlChars.CrLf;
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQL;
                            using (DbDataAdapter da = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da).SelectCommand = cmd;
                                dt = new DataTable();
                                da.Fill(dt);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                }
            }
            headerCode = dt.Rows[0][0].ToString();
            //END get chuoi tien to tu bang THAMSO_HT
            DropDownList drop_PARENT_ID = this.FindControl("PARENT_ID") as DropDownList;
            if (drop_PARENT_ID.SelectedValue.ToString()!="")
            {
                //get CODE
                L10N L11n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt1 = Cache.Get(L11n.NAME + ".vwKPIM02") as DataTable;
                dt1 = null;
                if (dt1 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "select CODE   " + ControlChars.CrLf
                                 + "  from KPIM02     " + ControlChars.CrLf
                                 + "  where DELETED = 0 and ID = '"+drop_PARENT_ID.SelectedValue.ToString()+"'     " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt1 = new DataTable();
                                    da.Fill(dt1);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                headerCode = dt1.Rows[0][0].ToString();
                //END get CODE
                //get COUNT(*) kpim02 co parent_id duoc chon
                L10N L12n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt2 = Cache.Get(L12n.NAME + ".vwKPIM02") as DataTable;
                dt2 = null;
                if (dt2 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "select COUNT(*)   " + ControlChars.CrLf
                                 + "  from KPIM02     " + ControlChars.CrLf
                                 + "  where DELETED = 0 and PARENT_ID = '" + drop_PARENT_ID.SelectedValue.ToString().ToUpper() + "'     " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt2 = new DataTable();
                                    da.Fill(dt2);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                //END get COUNT(*) kpim02 co parent_id duoc chon
                //tang them 1
                if ((int.Parse(dt2.Rows[0][0].ToString())+1)<10)
                {
                    code = headerCode + ".0" + (int.Parse(dt2.Rows[0][0].ToString())+1).ToString();
                }else
                {
                    code = headerCode + "." + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                //END tang them 1
            }else
            {
                //get COUNT(*) kpim02 co parent_id null
                L10N L12n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt2 = Cache.Get(L12n.NAME + ".vwKPIM02") as DataTable;
                dt2 = null;
                if (dt2 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "SELECT COUNT(*)   " + ControlChars.CrLf
                                 + "  FROM KPIM02     " + ControlChars.CrLf
                                 + "  WHERE DELETED = 0 AND PARENT_ID IS NULL    " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt2 = new DataTable();
                                    da.Fill(dt2);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                //END get COUNT(*) kpim02 co parent_id null
                //tang them 1
                if ((int.Parse(dt2.Rows[0][0].ToString()) + 1) < 10)
                {
                    code = headerCode + "0" + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                else
                {
                    code = headerCode + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                //END tang them 1
            }
            return code;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIM02";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
		#endregion
	}
}
