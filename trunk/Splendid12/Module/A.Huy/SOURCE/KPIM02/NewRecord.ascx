<%@ Control Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.NewRecordControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Diagnostics" %>
<script runat="server">
	public partial class SqlProcs
	{
		#region spKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static void spKPIM02_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sCODE, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME)
		{
			SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.Transaction = trn;
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = "spKPIM02_Update";
							IDbDataParameter parID                     = Sql.AddParameter(cmd, "@ID"                    , gID                      );
							IDbDataParameter parMODIFIED_USER_ID       = Sql.AddParameter(cmd, "@MODIFIED_USER_ID"      ,  Security.USER_ID        );
							IDbDataParameter parASSIGNED_USER_ID       = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID"      , gASSIGNED_USER_ID        );
							IDbDataParameter parTEAM_ID                = Sql.AddParameter(cmd, "@TEAM_ID"               , gTEAM_ID                 );
							IDbDataParameter parTEAM_SET_LIST          = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST"         , sTEAM_SET_LIST           , 8000);
							IDbDataParameter parNAME                   = Sql.AddParameter(cmd, "@NAME"                  , sNAME                    , 150);
							IDbDataParameter parCODE                   = Sql.AddParameter(cmd, "@CODE"                  , sCODE                    ,  50);
							IDbDataParameter parPARENT_ID              = Sql.AddParameter(cmd, "@PARENT_ID"             , sPARENT_ID               ,  50);
							IDbDataParameter parLEVEL_NUMBER           = Sql.AddParameter(cmd, "@LEVEL_NUMBER"          , nLEVEL_NUMBER            );
							IDbDataParameter parUNIT                   = Sql.AddParameter(cmd, "@UNIT"                  , nUNIT                    );
							IDbDataParameter parRADIO                  = Sql.AddParameter(cmd, "@RADIO"                 , flRADIO                  );
							IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE"    , flMAX_RATIO_COMPLETE     );
							IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE"    , flMIN_RATIO_COMPLETE     );
							IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE );
							IDbDataParameter parSTATUS                 = Sql.AddParameter(cmd, "@STATUS"                , sSTATUS                  ,  50);
							IDbDataParameter parMAX_POINT              = Sql.AddParameter(cmd, "@MAX_POINT"             , nMAX_POINT               );
							IDbDataParameter parMIN_POINT              = Sql.AddParameter(cmd, "@MIN_POINT"             , nMIN_POINT               );
							IDbDataParameter parDEFAULT_POINT          = Sql.AddParameter(cmd, "@DEFAULT_POINT"         , nDEFAULT_POINT           );
							IDbDataParameter parSORTING_ORDER          = Sql.AddParameter(cmd, "@SORTING_ORDER"         , nSORTING_ORDER           );
							IDbDataParameter parDESCRIPTION            = Sql.AddParameter(cmd, "@DESCRIPTION"           , sDESCRIPTION             , 200);
							IDbDataParameter parREMARK                 = Sql.AddParameter(cmd, "@REMARK"                , sREMARK                  , 200);
							IDbDataParameter parTAG_SET_NAME           = Sql.AddParameter(cmd, "@TAG_SET_NAME"          , sTAG_SET_NAME            , 4000);
							parID.Direction = ParameterDirection.InputOutput;
							cmd.ExecuteNonQuery();
							gID = Sql.ToGuid(parID.Value);
						}
						trn.Commit();
					}
					catch
					{
						trn.Rollback();
						throw;
					}
				}
			}
		}
		#endregion

		#region spKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static void spKPIM02_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sCODE, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME, IDbTransaction trn)
		{
			IDbConnection con = trn.Connection;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.Transaction = trn;
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "spKPIM02_Update";
				IDbDataParameter parID                     = Sql.AddParameter(cmd, "@ID"                    , gID                      );
				IDbDataParameter parMODIFIED_USER_ID       = Sql.AddParameter(cmd, "@MODIFIED_USER_ID"      ,  Security.USER_ID        );
				IDbDataParameter parASSIGNED_USER_ID       = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID"      , gASSIGNED_USER_ID        );
				IDbDataParameter parTEAM_ID                = Sql.AddParameter(cmd, "@TEAM_ID"               , gTEAM_ID                 );
				IDbDataParameter parTEAM_SET_LIST          = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST"         , sTEAM_SET_LIST           , 8000);
				IDbDataParameter parNAME                   = Sql.AddParameter(cmd, "@NAME"                  , sNAME                    , 150);
				IDbDataParameter parCODE                   = Sql.AddParameter(cmd, "@CODE"                  , sCODE                    ,  50);
				IDbDataParameter parPARENT_ID              = Sql.AddParameter(cmd, "@PARENT_ID"             , sPARENT_ID               ,  50);
				IDbDataParameter parLEVEL_NUMBER           = Sql.AddParameter(cmd, "@LEVEL_NUMBER"          , nLEVEL_NUMBER            );
				IDbDataParameter parUNIT                   = Sql.AddParameter(cmd, "@UNIT"                  , nUNIT                    );
				IDbDataParameter parRADIO                  = Sql.AddParameter(cmd, "@RADIO"                 , flRADIO                  );
				IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE"    , flMAX_RATIO_COMPLETE     );
				IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE"    , flMIN_RATIO_COMPLETE     );
				IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE );
				IDbDataParameter parSTATUS                 = Sql.AddParameter(cmd, "@STATUS"                , sSTATUS                  ,  50);
				IDbDataParameter parMAX_POINT              = Sql.AddParameter(cmd, "@MAX_POINT"             , nMAX_POINT               );
				IDbDataParameter parMIN_POINT              = Sql.AddParameter(cmd, "@MIN_POINT"             , nMIN_POINT               );
				IDbDataParameter parDEFAULT_POINT          = Sql.AddParameter(cmd, "@DEFAULT_POINT"         , nDEFAULT_POINT           );
				IDbDataParameter parSORTING_ORDER          = Sql.AddParameter(cmd, "@SORTING_ORDER"         , nSORTING_ORDER           );
				IDbDataParameter parDESCRIPTION            = Sql.AddParameter(cmd, "@DESCRIPTION"           , sDESCRIPTION             , 200);
				IDbDataParameter parREMARK                 = Sql.AddParameter(cmd, "@REMARK"                , sREMARK                  , 200);
				IDbDataParameter parTAG_SET_NAME           = Sql.AddParameter(cmd, "@TAG_SET_NAME"          , sTAG_SET_NAME            , 4000);
				parID.Direction = ParameterDirection.InputOutput;
				Sql.Trace(cmd);
				cmd.ExecuteNonQuery();
				gID = Sql.ToGuid(parID.Value);
			}
		}
		#endregion

		#region cmdKPIM02_Update
		/// <summary>
		/// spKPIM02_Update
		/// </summary>
		public static IDbCommand cmdKPIM02_Update(IDbConnection con)
		{
			IDbCommand cmd = con.CreateCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "spKPIM02_Update";
			IDbDataParameter parID                     = Sql.CreateParameter(cmd, "@ID"                    , "Guid",  16);
			IDbDataParameter parMODIFIED_USER_ID       = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID"      , "Guid",  16);
			IDbDataParameter parASSIGNED_USER_ID       = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID"      , "Guid",  16);
			IDbDataParameter parTEAM_ID                = Sql.CreateParameter(cmd, "@TEAM_ID"               , "Guid",  16);
			IDbDataParameter parTEAM_SET_LIST          = Sql.CreateParameter(cmd, "@TEAM_SET_LIST"         , "ansistring", 8000);
			IDbDataParameter parNAME                   = Sql.CreateParameter(cmd, "@NAME"                  , "string", 150);
			IDbDataParameter parCODE                   = Sql.CreateParameter(cmd, "@CODE"                  , "string",  50);
			IDbDataParameter parPARENT_ID              = Sql.CreateParameter(cmd, "@PARENT_ID"             , "string",  50);
			IDbDataParameter parLEVEL_NUMBER           = Sql.CreateParameter(cmd, "@LEVEL_NUMBER"          , "Int32",   4);
			IDbDataParameter parUNIT                   = Sql.CreateParameter(cmd, "@UNIT"                  , "Int32",   4);
			IDbDataParameter parRADIO                  = Sql.CreateParameter(cmd, "@RADIO"                 , "float",   8);
			IDbDataParameter parMAX_RATIO_COMPLETE     = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE"    , "float",   8);
			IDbDataParameter parMIN_RATIO_COMPLETE     = Sql.CreateParameter(cmd, "@MIN_RATIO_COMPLETE"    , "float",   8);
			IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@DEFAULT_RATIO_COMPLETE", "float",   8);
			IDbDataParameter parSTATUS                 = Sql.CreateParameter(cmd, "@STATUS"                , "string",  50);
			IDbDataParameter parMAX_POINT              = Sql.CreateParameter(cmd, "@MAX_POINT"             , "Int32",   4);
			IDbDataParameter parMIN_POINT              = Sql.CreateParameter(cmd, "@MIN_POINT"             , "Int32",   4);
			IDbDataParameter parDEFAULT_POINT          = Sql.CreateParameter(cmd, "@DEFAULT_POINT"         , "Int32",   4);
			IDbDataParameter parSORTING_ORDER          = Sql.CreateParameter(cmd, "@SORTING_ORDER"         , "Int32",   4);
			IDbDataParameter parDESCRIPTION            = Sql.CreateParameter(cmd, "@DESCRIPTION"           , "string", 200);
			IDbDataParameter parREMARK                 = Sql.CreateParameter(cmd, "@REMARK"                , "string", 200);
			IDbDataParameter parTAG_SET_NAME           = Sql.CreateParameter(cmd, "@TAG_SET_NAME"          , "string", 4000);
			parID.Direction = ParameterDirection.InputOutput;
			return cmd;
		}
		#endregion

	}



		protected Guid            gID                             ;

		public override bool IsEmpty()
		{
			string sNAME = new SplendidCRM.DynamicControl(this, "NAME").Text;
			return Sql.IsEmptyString(sNAME);
		}

		public override void ValidateEditViewFields()
		{
			if ( !IsEmpty() )
			{
				this.ValidateEditViewFields(m_sMODULE + "." + sEditView);
				this.ApplyEditViewValidationEventRules(m_sMODULE + "." + sEditView);
			}
		}

		public override void Save(Guid gPARENT_ID, string sPARENT_TYPE, IDbTransaction trn)
		{
			if ( IsEmpty() )
				return;
			
			string    sTABLE_NAME    = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
			DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
			
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, null, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, null, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIM02_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, null, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, null, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, null, "CODE"                               ).Text
										, new SplendidCRM.DynamicControl(this, null, "PARENT_ID"                          ).SelectedValue
										, new SplendidCRM.DynamicControl(this, null, "LEVEL_NUMBER"                       ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "UNIT"                               ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "RADIO"                              ).FloatValue
										, new SplendidCRM.DynamicControl(this, null, "MAX_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, null, "MIN_RATIO_COMPLETE"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, null, "DEFAULT_RATIO_COMPLETE"             ).FloatValue
										, new SplendidCRM.DynamicControl(this, null, "STATUS"                             ).SelectedValue
										, new SplendidCRM.DynamicControl(this, null, "MAX_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "MIN_POINT"                          ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "DEFAULT_POINT"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "SORTING_ORDER"                      ).IntegerValue
										, new SplendidCRM.DynamicControl(this, null, "DESCRIPTION"                        ).Text
										, new SplendidCRM.DynamicControl(this, null, "REMARK"                             ).Text
										, new SplendidCRM.DynamicControl(this, null, "TAG_SET_NAME"                       ).Text
										, trn
										);

			SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "NewRecord" )
				{
					this.ValidateEditViewFields(m_sMODULE + "." + sEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + sEditView);
					if ( Page.IsValid )
					{
						SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + sEditView, null);
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid   gPARENT_ID   = new SplendidCRM.DynamicControl(this, "PARENT_ID").ID;
									String sPARENT_TYPE = String.Empty;
									if ( Sql.IsEmptyGuid(gPARENT_ID) )
									{
										gPARENT_ID   = this.PARENT_ID  ;
										sPARENT_TYPE = this.PARENT_TYPE;
									}
									Save(gPARENT_ID, sPARENT_TYPE, trn);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									if ( bShowFullForm || bShowCancel )
										ctlFooterButtons.ErrorText = ex.Message;
									else
										lblError.Text = ex.Message;
									return;
								}
							}
						}
						DataRow rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
						this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + sEditView, rowCurrent);
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else if ( Command != null )
							Command(sender, new CommandEventArgs(e.CommandName, gID.ToString()));
						else if ( !Sql.IsEmptyGuid(gID) )
							Response.Redirect("~/" + m_sMODULE + "/view.aspx?ID=" + gID.ToString());
					}
				}
				else if ( Command != null )
				{
					Command(sender, e);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				if ( bShowFullForm || bShowCancel )
					ctlFooterButtons.ErrorText = ex.Message;
				else
					lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				bool bIsPostBack = this.IsPostBack && !NotPostBack;
				if ( !bIsPostBack )
				{
					if ( NotPostBack )
						this.DataBind();
					this.AppendEditViewFields(m_sMODULE + "." + sEditView, tblMain, null, ctlFooterButtons.ButtonClientID("NewRecord"));
					if ( EditViewLoad != null )
						EditViewLoad(this, null);
					
					if ( bShowFullForm || bShowCancel || sEditView != "NewRecord" )
					{
						pnlMain.CssClass = "";
						pnlEdit.CssClass = "tabForm";
						
						Guid gPARENT_ID = this.PARENT_ID;
						if ( !Sql.IsEmptyGuid(gPARENT_ID) )
						{
							string sMODULE      = String.Empty;
							string sPARENT_TYPE = String.Empty;
							string sPARENT_NAME = String.Empty;
							SplendidCRM.SqlProcs.spPARENT_Get( ref gPARENT_ID, ref sMODULE, ref sPARENT_TYPE, ref sPARENT_NAME);
							if ( !Sql.IsEmptyGuid(gPARENT_ID) && sPARENT_TYPE == "KPIM02" )
							{
								new SplendidCRM.DynamicControl(this, "PARENT_ID"  ).ID   = gPARENT_ID  ;
								new SplendidCRM.DynamicControl(this, "PARENT_NAME").Text = sPARENT_NAME;
							}
						}
					}
					this.ApplyEditViewNewEventRules(m_sMODULE + "." + sEditView);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				if ( bShowFullForm || bShowCancel )
					ctlFooterButtons.ErrorText = ex.Message;
				else
					lblError.Text = ex.Message;
			}
		}

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);

			ctlDynamicButtons.AppendButtons("NewRecord." + (bShowFullForm ? "FullForm" : (bShowCancel ? "WithCancel" : "SaveOnly")), Guid.Empty, Guid.Empty);
			ctlFooterButtons .AppendButtons("NewRecord." + (bShowFullForm ? "FullForm" : (bShowCancel ? "WithCancel" : "SaveOnly")), Guid.Empty, Guid.Empty);
			m_sMODULE = "KPIM02";
			bool bIsPostBack = this.IsPostBack && !NotPostBack;
			if ( bIsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + sEditView, tblMain, null, ctlFooterButtons.ButtonClientID("NewRecord"));
				if ( EditViewLoad != null )
					EditViewLoad(this, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
</script>
<div id="divNewRecord">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderLeft" Src="~/_controls/HeaderLeft.ascx" %>
	<SplendidCRM:HeaderLeft ID="ctlHeaderLeft" Title="KPIM02.LBL_NEW_FORM_TITLE" Width=<%# uWidth %> Visible="<%# ShowHeader %>" Runat="Server" />

	<asp:Panel ID="pnlMain" Width="100%" CssClass="leftColumnModuleS3" runat="server">
		<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
		<SplendidCRM:DynamicButtons ID="ctlDynamicButtons" Visible="<%# ShowTopButtons && !PrintView %>" Runat="server" />

		<asp:Panel ID="pnlEdit" CssClass="" style="margin-bottom: 4px;" Width=<%# uWidth %> runat="server">
			<asp:Literal Text='<%# "<h4>" + L10n.Term("KPIM02.LBL_NEW_FORM_TITLE") + "</h4>" %>' Visible="<%# ShowInlineHeader %>" runat="server" />
			<table ID="tblMain" class="tabEditView" runat="server">
			</table>
		</asp:Panel>

		<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# ShowBottomButtons && !PrintView %>" Runat="server" />
		<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />
	</asp:Panel>
</div>
