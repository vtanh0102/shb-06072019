
function THAMSO_HT_THAMSO_HT_NAME_Changed(fldTHAMSO_HT_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldTHAMSO_HT_NAME.id.substring(0, fldTHAMSO_HT_NAME.id.length - 'THAMSO_HT_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'THAMSO_HT_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_THAMSO_HT_NAME = document.getElementById(userContext + 'PREV_THAMSO_HT_NAME');
	if ( fldPREV_THAMSO_HT_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_THAMSO_HT_NAME');
	}
	else if ( fldPREV_THAMSO_HT_NAME.value != fldTHAMSO_HT_NAME.value )
	{
		if ( fldTHAMSO_HT_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.THAMSO_HT.AutoComplete.THAMSO_HT_THAMSO_HT_NAME_Get(fldTHAMSO_HT_NAME.value, THAMSO_HT_THAMSO_HT_NAME_Changed_OnSucceededWithContext, THAMSO_HT_THAMSO_HT_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('THAMSO_HT_THAMSO_HT_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			THAMSO_HT_THAMSO_HT_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function THAMSO_HT_THAMSO_HT_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'THAMSO_HT_NAME_AjaxErrors');
		var fldTHAMSO_HT_ID        = document.getElementById(userContext + 'THAMSO_HT_ID'       );
		var fldTHAMSO_HT_NAME      = document.getElementById(userContext + 'THAMSO_HT_NAME'     );
		var fldPREV_THAMSO_HT_NAME = document.getElementById(userContext + 'PREV_THAMSO_HT_NAME');
		if ( fldTHAMSO_HT_ID        != null ) fldTHAMSO_HT_ID.value        = sID  ;
		if ( fldTHAMSO_HT_NAME      != null ) fldTHAMSO_HT_NAME.value      = sNAME;
		if ( fldPREV_THAMSO_HT_NAME != null ) fldPREV_THAMSO_HT_NAME.value = sNAME;
	}
	else
	{
		alert('result from THAMSO_HT.AutoComplete service is null');
	}
}

function THAMSO_HT_THAMSO_HT_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'THAMSO_HT_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldTHAMSO_HT_ID        = document.getElementById(userContext + 'THAMSO_HT_ID'       );
	var fldTHAMSO_HT_NAME      = document.getElementById(userContext + 'THAMSO_HT_NAME'     );
	var fldPREV_THAMSO_HT_NAME = document.getElementById(userContext + 'PREV_THAMSO_HT_NAME');
	if ( fldTHAMSO_HT_ID        != null ) fldTHAMSO_HT_ID.value        = '';
	if ( fldTHAMSO_HT_NAME      != null ) fldTHAMSO_HT_NAME.value      = '';
	if ( fldPREV_THAMSO_HT_NAME != null ) fldPREV_THAMSO_HT_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

