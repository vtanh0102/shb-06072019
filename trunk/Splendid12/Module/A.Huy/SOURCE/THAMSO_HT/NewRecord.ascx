<%@ Control Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.NewRecordControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Diagnostics" %>
<script runat="server">
	public partial class SqlProcs
	{
		#region spTHAMSO_HT_Update
		/// <summary>
		/// spTHAMSO_HT_Update
		/// </summary>
		public static void spTHAMSO_HT_Update(ref Guid gID, string sNAME, string sVALUE, string sTAG_SET_NAME)
		{
			SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.Transaction = trn;
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = "spTHAMSO_HT_Update";
							IDbDataParameter parID               = Sql.AddParameter(cmd, "@ID"              , gID                );
							IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID",  Security.USER_ID  );
							IDbDataParameter parNAME             = Sql.AddParameter(cmd, "@NAME"            , sNAME              , 150);
							IDbDataParameter parVALUE            = Sql.AddParameter(cmd, "@VALUE"           , sVALUE             , 200);
							IDbDataParameter parTAG_SET_NAME     = Sql.AddParameter(cmd, "@TAG_SET_NAME"    , sTAG_SET_NAME      , 4000);
							parID.Direction = ParameterDirection.InputOutput;
							cmd.ExecuteNonQuery();
							gID = Sql.ToGuid(parID.Value);
						}
						trn.Commit();
					}
					catch
					{
						trn.Rollback();
						throw;
					}
				}
			}
		}
		#endregion

		#region spTHAMSO_HT_Update
		/// <summary>
		/// spTHAMSO_HT_Update
		/// </summary>
		public static void spTHAMSO_HT_Update(ref Guid gID, string sNAME, string sVALUE, string sTAG_SET_NAME, IDbTransaction trn)
		{
			IDbConnection con = trn.Connection;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.Transaction = trn;
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "spTHAMSO_HT_Update";
				IDbDataParameter parID               = Sql.AddParameter(cmd, "@ID"              , gID                );
				IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID",  Security.USER_ID  );
				IDbDataParameter parNAME             = Sql.AddParameter(cmd, "@NAME"            , sNAME              , 150);
				IDbDataParameter parVALUE            = Sql.AddParameter(cmd, "@VALUE"           , sVALUE             , 200);
				IDbDataParameter parTAG_SET_NAME     = Sql.AddParameter(cmd, "@TAG_SET_NAME"    , sTAG_SET_NAME      , 4000);
				parID.Direction = ParameterDirection.InputOutput;
				Sql.Trace(cmd);
				cmd.ExecuteNonQuery();
				gID = Sql.ToGuid(parID.Value);
			}
		}
		#endregion

		#region cmdTHAMSO_HT_Update
		/// <summary>
		/// spTHAMSO_HT_Update
		/// </summary>
		public static IDbCommand cmdTHAMSO_HT_Update(IDbConnection con)
		{
			IDbCommand cmd = con.CreateCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "spTHAMSO_HT_Update";
			IDbDataParameter parID               = Sql.CreateParameter(cmd, "@ID"              , "Guid",  16);
			IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid",  16);
			IDbDataParameter parNAME             = Sql.CreateParameter(cmd, "@NAME"            , "string", 150);
			IDbDataParameter parVALUE            = Sql.CreateParameter(cmd, "@VALUE"           , "string", 200);
			IDbDataParameter parTAG_SET_NAME     = Sql.CreateParameter(cmd, "@TAG_SET_NAME"    , "string", 4000);
			parID.Direction = ParameterDirection.InputOutput;
			return cmd;
		}
		#endregion

	}



		protected Guid            gID                             ;

		public override bool IsEmpty()
		{
			string sNAME = new SplendidCRM.DynamicControl(this, "NAME").Text;
			return Sql.IsEmptyString(sNAME);
		}

		public override void ValidateEditViewFields()
		{
			if ( !IsEmpty() )
			{
				this.ValidateEditViewFields(m_sMODULE + "." + sEditView);
				this.ApplyEditViewValidationEventRules(m_sMODULE + "." + sEditView);
			}
		}

		public override void Save(Guid gPARENT_ID, string sPARENT_TYPE, IDbTransaction trn)
		{
			if ( IsEmpty() )
				return;
			
			string    sTABLE_NAME    = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
			DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
			
									SqlProcs.spTHAMSO_HT_Update
										( ref gID
										, new SplendidCRM.DynamicControl(this, null, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, null, "VALUE"                              ).Text
										, new SplendidCRM.DynamicControl(this, null, "TAG_SET_NAME"                       ).Text
										, trn
										);

			SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "NewRecord" )
				{
					this.ValidateEditViewFields(m_sMODULE + "." + sEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + sEditView);
					if ( Page.IsValid )
					{
						SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + sEditView, null);
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid   gPARENT_ID   = new SplendidCRM.DynamicControl(this, "PARENT_ID").ID;
									String sPARENT_TYPE = String.Empty;
									if ( Sql.IsEmptyGuid(gPARENT_ID) )
									{
										gPARENT_ID   = this.PARENT_ID  ;
										sPARENT_TYPE = this.PARENT_TYPE;
									}
									Save(gPARENT_ID, sPARENT_TYPE, trn);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									if ( bShowFullForm || bShowCancel )
										ctlFooterButtons.ErrorText = ex.Message;
									else
										lblError.Text = ex.Message;
									return;
								}
							}
						}
						DataRow rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
						this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + sEditView, rowCurrent);
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else if ( Command != null )
							Command(sender, new CommandEventArgs(e.CommandName, gID.ToString()));
						else if ( !Sql.IsEmptyGuid(gID) )
							Response.Redirect("~/" + m_sMODULE + "/view.aspx?ID=" + gID.ToString());
					}
				}
				else if ( Command != null )
				{
					Command(sender, e);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				if ( bShowFullForm || bShowCancel )
					ctlFooterButtons.ErrorText = ex.Message;
				else
					lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				bool bIsPostBack = this.IsPostBack && !NotPostBack;
				if ( !bIsPostBack )
				{
					if ( NotPostBack )
						this.DataBind();
					this.AppendEditViewFields(m_sMODULE + "." + sEditView, tblMain, null, ctlFooterButtons.ButtonClientID("NewRecord"));
					if ( EditViewLoad != null )
						EditViewLoad(this, null);
					
					if ( bShowFullForm || bShowCancel || sEditView != "NewRecord" )
					{
						pnlMain.CssClass = "";
						pnlEdit.CssClass = "tabForm";
						
						Guid gPARENT_ID = this.PARENT_ID;
						if ( !Sql.IsEmptyGuid(gPARENT_ID) )
						{
							string sMODULE      = String.Empty;
							string sPARENT_TYPE = String.Empty;
							string sPARENT_NAME = String.Empty;
							SplendidCRM.SqlProcs.spPARENT_Get( ref gPARENT_ID, ref sMODULE, ref sPARENT_TYPE, ref sPARENT_NAME);
							if ( !Sql.IsEmptyGuid(gPARENT_ID) && sPARENT_TYPE == "THAMSO_HT" )
							{
								new SplendidCRM.DynamicControl(this, "PARENT_ID"  ).ID   = gPARENT_ID  ;
								new SplendidCRM.DynamicControl(this, "PARENT_NAME").Text = sPARENT_NAME;
							}
						}
					}
					this.ApplyEditViewNewEventRules(m_sMODULE + "." + sEditView);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				if ( bShowFullForm || bShowCancel )
					ctlFooterButtons.ErrorText = ex.Message;
				else
					lblError.Text = ex.Message;
			}
		}

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);

			ctlDynamicButtons.AppendButtons("NewRecord." + (bShowFullForm ? "FullForm" : (bShowCancel ? "WithCancel" : "SaveOnly")), Guid.Empty, Guid.Empty);
			ctlFooterButtons .AppendButtons("NewRecord." + (bShowFullForm ? "FullForm" : (bShowCancel ? "WithCancel" : "SaveOnly")), Guid.Empty, Guid.Empty);
			m_sMODULE = "THAMSO_HT";
			bool bIsPostBack = this.IsPostBack && !NotPostBack;
			if ( bIsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + sEditView, tblMain, null, ctlFooterButtons.ButtonClientID("NewRecord"));
				if ( EditViewLoad != null )
					EditViewLoad(this, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
</script>
<div id="divNewRecord">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderLeft" Src="~/_controls/HeaderLeft.ascx" %>
	<SplendidCRM:HeaderLeft ID="ctlHeaderLeft" Title="THAMSO_HT.LBL_NEW_FORM_TITLE" Width=<%# uWidth %> Visible="<%# ShowHeader %>" Runat="Server" />

	<asp:Panel ID="pnlMain" Width="100%" CssClass="leftColumnModuleS3" runat="server">
		<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
		<SplendidCRM:DynamicButtons ID="ctlDynamicButtons" Visible="<%# ShowTopButtons && !PrintView %>" Runat="server" />

		<asp:Panel ID="pnlEdit" CssClass="" style="margin-bottom: 4px;" Width=<%# uWidth %> runat="server">
			<asp:Literal Text='<%# "<h4>" + L10n.Term("THAMSO_HT.LBL_NEW_FORM_TITLE") + "</h4>" %>' Visible="<%# ShowInlineHeader %>" runat="server" />
			<table ID="tblMain" class="tabEditView" runat="server">
			</table>
		</asp:Panel>

		<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# ShowBottomButtons && !PrintView %>" Runat="server" />
		<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />
	</asp:Panel>
</div>
