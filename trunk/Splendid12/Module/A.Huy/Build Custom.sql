
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'THAMSO_HT' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.THAMSO_HT';
	Create Table dbo.THAMSO_HT
		( ID                                 uniqueidentifier not null default(newid()) constraint PK_THAMSO_HT primary key
		, DELETED                            bit not null default(0)
		, CREATED_BY                         uniqueidentifier null
		, DATE_ENTERED                       datetime not null default(getdate())
		, MODIFIED_USER_ID                   uniqueidentifier null
		, DATE_MODIFIED                      datetime not null default(getdate())
		, DATE_MODIFIED_UTC                  datetime null default(getutcdate())

		, NAME                               nvarchar(150) not null
		, VALUE                              nvarchar(200) not null

		)

	create index IDX_THAMSO_HT_NAME  on dbo.THAMSO_HT (NAME, DELETED, ID)

  end
GO


if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'THAMSO_HT_CSTM' and TABLE_TYPE = 'BASE TABLE')
  begin
	print 'Create Table dbo.THAMSO_HT_CSTM';
	Create Table dbo.THAMSO_HT_CSTM
		( ID_C                               uniqueidentifier not null constraint PK_THAMSO_HT_CSTM primary key
		)
  end
GO







if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'THAMSO_HT' and COLUMN_NAME = 'NAME') begin -- then
	print 'alter table THAMSO_HT add NAME nvarchar(150) null';
	alter table THAMSO_HT add NAME nvarchar(150) null;
end -- if;

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'THAMSO_HT' and COLUMN_NAME = 'VALUE') begin -- then
	print 'alter table THAMSO_HT add VALUE nvarchar(200) null';
	alter table THAMSO_HT add VALUE nvarchar(200) null;
end -- if;


GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwTHAMSO_HT')
	Drop View dbo.vwTHAMSO_HT;
GO


Create View dbo.vwTHAMSO_HT
as
select THAMSO_HT.ID
     , THAMSO_HT.NAME
     , THAMSO_HT.VALUE

     , THAMSO_HT.DATE_ENTERED
     , THAMSO_HT.DATE_MODIFIED
     , THAMSO_HT.DATE_MODIFIED_UTC
     , USERS_CREATED_BY.USER_NAME  as CREATED_BY
     , USERS_MODIFIED_BY.USER_NAME as MODIFIED_BY
     , THAMSO_HT.CREATED_BY      as CREATED_BY_ID
     , THAMSO_HT.MODIFIED_USER_ID
     , LAST_ACTIVITY.LAST_ACTIVITY_DATE
     , TAG_SETS.TAG_SET_NAME
     , vwPROCESSES_Pending.ID      as PENDING_PROCESS_ID
     , THAMSO_HT_CSTM.*
  from            THAMSO_HT

  left outer join LAST_ACTIVITY
               on LAST_ACTIVITY.ACTIVITY_ID = THAMSO_HT.ID
  left outer join TAG_SETS
               on TAG_SETS.BEAN_ID          = THAMSO_HT.ID
              and TAG_SETS.DELETED          = 0
  left outer join USERS                       USERS_CREATED_BY
               on USERS_CREATED_BY.ID       = THAMSO_HT.CREATED_BY
  left outer join USERS                       USERS_MODIFIED_BY
               on USERS_MODIFIED_BY.ID      = THAMSO_HT.MODIFIED_USER_ID
  left outer join THAMSO_HT_CSTM
               on THAMSO_HT_CSTM.ID_C     = THAMSO_HT.ID
  left outer join vwPROCESSES_Pending
               on vwPROCESSES_Pending.PARENT_ID = THAMSO_HT.ID
 where THAMSO_HT.DELETED = 0

GO

Grant Select on dbo.vwTHAMSO_HT to public;
GO



if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwTHAMSO_HT_Edit')
	Drop View dbo.vwTHAMSO_HT_Edit;
GO


Create View dbo.vwTHAMSO_HT_Edit
as
select *
  from vwTHAMSO_HT

GO

Grant Select on dbo.vwTHAMSO_HT_Edit to public;
GO


if exists (select * from INFORMATION_SCHEMA.VIEWS where TABLE_NAME = 'vwTHAMSO_HT_List')
	Drop View dbo.vwTHAMSO_HT_List;
GO


Create View dbo.vwTHAMSO_HT_List
as
select *
  from vwTHAMSO_HT

GO

Grant Select on dbo.vwTHAMSO_HT_List to public;
GO




if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_Delete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_Delete;
GO


Create Procedure dbo.spTHAMSO_HT_Delete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	

	
	-- BEGIN Oracle Exception
		delete from TRACKER
		 where ITEM_ID          = @ID
		   and USER_ID          = @MODIFIED_USER_ID;
	-- END Oracle Exception
	
	exec dbo.spPARENT_Delete @ID, @MODIFIED_USER_ID;
	
	-- BEGIN Oracle Exception
		update THAMSO_HT
		   set DELETED          = 1
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 0;
	-- END Oracle Exception
	
	-- BEGIN Oracle Exception
		update SUGARFAVORITES
		   set DELETED           = 1
		     , DATE_MODIFIED     = getdate()
		     , DATE_MODIFIED_UTC = getutcdate()
		     , MODIFIED_USER_ID  = @MODIFIED_USER_ID
		 where RECORD_ID         = @ID
		   and DELETED           = 0;
	-- END Oracle Exception
  end
GO

Grant Execute on dbo.spTHAMSO_HT_Delete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_Undelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_Undelete;
GO


Create Procedure dbo.spTHAMSO_HT_Undelete
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @AUDIT_TOKEN      varchar(255)
	)
as
  begin
	set nocount on
	

	
	exec dbo.spPARENT_Undelete @ID, @MODIFIED_USER_ID, @AUDIT_TOKEN, N'THAMSO_HT';
	
	-- BEGIN Oracle Exception
		update THAMSO_HT
		   set DELETED          = 0
		     , DATE_MODIFIED    = getdate()
		     , DATE_MODIFIED_UTC= getutcdate()
		     , MODIFIED_USER_ID = @MODIFIED_USER_ID
		 where ID               = @ID
		   and DELETED          = 1
		   and ID in (select ID from THAMSO_HT_AUDIT where AUDIT_TOKEN = @AUDIT_TOKEN and ID = @ID);
	-- END Oracle Exception
	
  end
GO

Grant Execute on dbo.spTHAMSO_HT_Undelete to public;
GO

if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_Update' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_Update;
GO


Create Procedure dbo.spTHAMSO_HT_Update
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @NAME                               nvarchar(150)
	, @VALUE                              nvarchar(200)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	

	if not exists(select * from THAMSO_HT where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into THAMSO_HT
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, NAME                               
			, VALUE                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @NAME                               
			, @VALUE                              

			);
	end else begin
		update THAMSO_HT
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , NAME                                 = @NAME                               
		     , VALUE                                = @VALUE                              

		 where ID                                   = @ID                                 ;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from THAMSO_HT_CSTM where ID_C = @ID) begin -- then
			insert into THAMSO_HT_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'THAMSO_HT', @TAG_SET_NAME;
	end -- if;

  end
GO

Grant Execute on dbo.spTHAMSO_HT_Update to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_MassDelete' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_MassDelete;
GO


Create Procedure dbo.spTHAMSO_HT_MassDelete
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier
	)
as
  begin
	set nocount on
	
	declare @ID           uniqueidentifier;
	declare @CurrentPosR  int;
	declare @NextPosR     int;
	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;
		exec dbo.spTHAMSO_HT_Delete @ID, @MODIFIED_USER_ID;
	end -- while;
  end
GO
 
Grant Execute on dbo.spTHAMSO_HT_MassDelete to public;
GO
 
 
if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_MassUpdate' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_MassUpdate;
GO


Create Procedure dbo.spTHAMSO_HT_MassUpdate
	( @ID_LIST          varchar(8000)
	, @MODIFIED_USER_ID uniqueidentifier

	, @TAG_SET_NAME     nvarchar(4000)
	, @TAG_SET_ADD      bit
	)
as
  begin
	set nocount on
	
	declare @ID              uniqueidentifier;
	declare @CurrentPosR     int;
	declare @NextPosR        int;



	set @CurrentPosR = 1;
	while @CurrentPosR <= len(@ID_LIST) begin -- do
		set @NextPosR = charindex(',', @ID_LIST,  @CurrentPosR);
		if @NextPosR = 0 or @NextPosR is null begin -- then
			set @NextPosR = len(@ID_LIST) + 1;
		end -- if;
		set @ID = cast(rtrim(ltrim(substring(@ID_LIST, @CurrentPosR, @NextPosR - @CurrentPosR))) as uniqueidentifier);
		set @CurrentPosR = @NextPosR+1;



		if @TAG_SET_NAME is not null and len(@TAG_SET_NAME) > 0 begin -- then
			if @TAG_SET_ADD = 1 begin -- then
				exec dbo.spTAG_SETS_AddSet       @MODIFIED_USER_ID, @ID, N'THAMSO_HT', @TAG_SET_NAME;
			end else begin
				exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'THAMSO_HT', @TAG_SET_NAME;
			end -- if;
		end -- if;

		-- BEGIN Oracle Exception
			update THAMSO_HT
			   set MODIFIED_USER_ID  = @MODIFIED_USER_ID
			     , DATE_MODIFIED     =  getdate()
			     , DATE_MODIFIED_UTC =  getutcdate()

			 where ID                = @ID
			   and DELETED           = 0;
		-- END Oracle Exception


	end -- while;
  end
GO

Grant Execute on dbo.spTHAMSO_HT_MassUpdate to public;
GO


if exists (select * from INFORMATION_SCHEMA.ROUTINES where ROUTINE_NAME = 'spTHAMSO_HT_Merge' and ROUTINE_TYPE = 'PROCEDURE')
	Drop Procedure dbo.spTHAMSO_HT_Merge;
GO


-- Copyright (C) 2006 SplendidCRM Software, Inc. All rights reserved.
-- NOTICE: This code has not been licensed under any public license.
Create Procedure dbo.spTHAMSO_HT_Merge
	( @ID               uniqueidentifier
	, @MODIFIED_USER_ID uniqueidentifier
	, @MERGE_ID         uniqueidentifier
	)
as
  begin
	set nocount on



	exec dbo.spPARENT_Merge @ID, @MODIFIED_USER_ID, @MERGE_ID;
	
	exec dbo.spTHAMSO_HT_Delete @MERGE_ID, @MODIFIED_USER_ID;
  end
GO

Grant Execute on dbo.spTHAMSO_HT_Merge to public;
GO



-- 01/19/2010 Paul.  Don't create the audit tables on an Offline Client database. 
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'THAMSO_HT';
	exec dbo.spSqlBuildAuditTrigger 'THAMSO_HT';
	exec dbo.spSqlBuildAuditView    'THAMSO_HT';
end -- if;
GO
--huynt THAMSO_HT_CSTM
if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SYSTEM_SYNC_CONFIG' and TABLE_TYPE = 'BASE TABLE') begin -- then
	exec dbo.spSqlBuildAuditTable   'THAMSO_HT_CSTM';
	exec dbo.spSqlBuildAuditTrigger 'THAMSO_HT_CSTM';
	exec dbo.spSqlBuildAuditView    'THAMSO_HT_CSTM';
end -- if;
GO
--END huynt THAMSO_HT_CSTM




-- delete from DETAILVIEWS_FIELDS where DETAIL_NAME = 'THAMSO_HT.DetailView';

if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'THAMSO_HT.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS THAMSO_HT.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'THAMSO_HT.DetailView'   , 'THAMSO_HT', 'vwTHAMSO_HT_Edit', '15%', '35%';
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'THAMSO_HT.DetailView', 0, 'THAMSO_HT.LBL_NAME', 'NAME', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'THAMSO_HT.DetailView', 1, 'THAMSO_HT.LBL_VALUE', 'VALUE', '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'THAMSO_HT.DetailView', 2, '.LBL_DATE_MODIFIED'              , 'DATE_MODIFIED .LBL_BY MODIFIED_BY', '{0} {1} {2}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound     'THAMSO_HT.DetailView', 3, '.LBL_DATE_ENTERED'               , 'DATE_ENTERED .LBL_BY CREATED_BY'  , '{0} {1} {2}', null;

end -- if;
GO


exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.DetailView', 'THAMSO_HT.DetailView', 'THAMSO_HT';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.EditView'  , 'THAMSO_HT.EditView'  , 'THAMSO_HT';
exec dbo.spDYNAMIC_BUTTONS_CopyDefault '.PopupView' , 'THAMSO_HT.PopupView' , 'THAMSO_HT';
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'THAMSO_HT.EditView' and COMMAND_NAME = 'SaveDuplicate' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveDuplicate 'THAMSO_HT.EditView', -1, null;
end -- if;
GO

if not exists(select * from DYNAMIC_BUTTONS where VIEW_NAME = 'THAMSO_HT.EditView' and COMMAND_NAME = 'SaveConcurrency' and DELETED = 0) begin -- then
	exec dbo.spDYNAMIC_BUTTONS_InsSaveConcurrency 'THAMSO_HT.EditView', -1, null;
end -- if;
GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.EditView';
	exec dbo.spEDITVIEWS_InsertOnly            'THAMSO_HT.EditView', 'THAMSO_HT'      , 'vwTHAMSO_HT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.EditView', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.EditView', 1, 'THAMSO_HT.LBL_VALUE', 'VALUE', 1, 1, 200, 35, null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.EditView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.EditView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.EditView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'THAMSO_HT.EditView.Inline', 'THAMSO_HT'      , 'vwTHAMSO_HT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.EditView.Inline', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.EditView.Inline', 1, 'THAMSO_HT.LBL_VALUE', 'VALUE', 1, 1, 200, 35, null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.PopupView.Inline';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.PopupView.Inline' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.PopupView.Inline';
	exec dbo.spEDITVIEWS_InsertOnly            'THAMSO_HT.PopupView.Inline', 'THAMSO_HT'      , 'vwTHAMSO_HT_Edit'      , '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.PopupView.Inline', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound       'THAMSO_HT.PopupView.Inline', 1, 'THAMSO_HT.LBL_VALUE', 'VALUE', 1, 1, 200, 35, null;

end -- if;
--GO


-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchBasic';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchBasic' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.SearchBasic';
	exec dbo.spEDITVIEWS_InsertOnly             'THAMSO_HT.SearchBasic'    , 'THAMSO_HT', 'vwTHAMSO_HT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'THAMSO_HT.SearchBasic', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, 'THAMSO_HT', null;


end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchAdvanced';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchAdvanced' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.SearchAdvanced';
	exec dbo.spEDITVIEWS_InsertOnly             'THAMSO_HT.SearchAdvanced' , 'THAMSO_HT', 'vwTHAMSO_HT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'THAMSO_HT.SearchAdvanced', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, 'THAMSO_HT', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'THAMSO_HT.SearchAdvanced', 1, 'THAMSO_HT.LBL_VALUE', 'VALUE', 1, 1, 200, 35, null;

end -- if;
GO

-- delete from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchPopup';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'THAMSO_HT.SearchPopup' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS THAMSO_HT.SearchPopup';
	exec dbo.spEDITVIEWS_InsertOnly             'THAMSO_HT.SearchPopup'    , 'THAMSO_HT', 'vwTHAMSO_HT_List', '11%', '22%', 3;
	exec dbo.spEDITVIEWS_FIELDS_InsAutoComplete 'THAMSO_HT.SearchPopup', 0, 'THAMSO_HT.LBL_NAME', 'NAME', 1, 1, 150, 35, 'THAMSO_HT', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.Export';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.Export' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS THAMSO_HT.Export';
	exec dbo.spGRIDVIEWS_InsertOnly           'THAMSO_HT.Export', 'THAMSO_HT', 'vwTHAMSO_HT_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsBound     'THAMSO_HT.Export'         ,  1, 'THAMSO_HT.LBL_LIST_NAME'                       , 'NAME'                       , null, null;
end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.ListView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.ListView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS THAMSO_HT.ListView';
	exec dbo.spGRIDVIEWS_InsertOnly           'THAMSO_HT.ListView', 'THAMSO_HT'      , 'vwTHAMSO_HT_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'THAMSO_HT.ListView', 2, 'THAMSO_HT.LBL_LIST_NAME', 'NAME', 'NAME', '35%', 'listViewTdLinkS1', 'ID', '~/THAMSO_HT/view.aspx?id={0}', null, 'THAMSO_HT', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.PopupView' and DELETED = 0;
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.PopupView' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS THAMSO_HT.PopupView';
	exec dbo.spGRIDVIEWS_InsertOnly           'THAMSO_HT.PopupView', 'THAMSO_HT'      , 'vwTHAMSO_HT_List'      ;
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'THAMSO_HT.PopupView', 1, 'THAMSO_HT.LBL_LIST_NAME', 'NAME', 'NAME', '45%', 'listViewTdLinkS1', 'ID NAME', 'SelectTHAMSO_HT(''{0}'', ''{1}'');', null, 'THAMSO_HT', null;

end -- if;
GO


-- delete from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.SearchDuplicates';
if not exists(select * from GRIDVIEWS_COLUMNS where GRID_NAME = 'THAMSO_HT.SearchDuplicates' and DELETED = 0) begin -- then
	print 'GRIDVIEWS_COLUMNS THAMSO_HT.SearchDuplicates';
	exec dbo.spGRIDVIEWS_InsertOnly           'THAMSO_HT.SearchDuplicates', 'THAMSO_HT', 'vwTHAMSO_HT_List';
	exec dbo.spGRIDVIEWS_COLUMNS_InsHyperLink 'THAMSO_HT.SearchDuplicates'          , 1, 'THAMSO_HT.LBL_LIST_NAME'                   , 'NAME'            , 'NAME'            , '50%', 'listViewTdLinkS1', 'ID'         , '~/THAMSO_HT/view.aspx?id={0}', null, 'THAMSO_HT', null;
end -- if;
GO


exec dbo.spMODULES_InsertOnly null, 'THAMSO_HT', '.moduleList.THAMSO_HT', '~/THAMSO_HT/', 1, 1, 100, 0, 0, 0, 0, 0, 'THAMSO_HT', 0, 0, 0, 0, 0, 0;
GO


-- delete from SHORTCUTS where MODULE_NAME = 'THAMSO_HT';
if not exists (select * from SHORTCUTS where MODULE_NAME = 'THAMSO_HT' and DELETED = 0) begin -- then
	exec dbo.spSHORTCUTS_InsertOnly null, 'THAMSO_HT', 'THAMSO_HT.LNK_NEW_THAMSO_HT' , '~/THAMSO_HT/edit.aspx'   , 'CreateTHAMSO_HT.gif', 1,  1, 'THAMSO_HT', 'edit';
	exec dbo.spSHORTCUTS_InsertOnly null, 'THAMSO_HT', 'THAMSO_HT.LNK_THAMSO_HT_LIST', '~/THAMSO_HT/default.aspx', 'THAMSO_HT.gif'      , 1,  2, 'THAMSO_HT', 'list';
	exec dbo.spSHORTCUTS_InsertOnly null, 'THAMSO_HT', '.LBL_IMPORT'              , '~/THAMSO_HT/import.aspx' , 'Import.gif'        , 1,  3, 'THAMSO_HT', 'import';
	exec dbo.spSHORTCUTS_InsertOnly null, 'THAMSO_HT', '.LNK_ACTIVITY_STREAM'     , '~/THAMSO_HT/stream.aspx' , 'ActivityStream.gif', 1,  4, 'THAMSO_HT', 'list';
end -- if;
GO




exec dbo.spTERMINOLOGY_InsertOnly N'LBL_LIST_FORM_TITLE'                                   , N'en-US', N'THAMSO_HT', null, null, N'THAMSO_HT List';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_NEW_FORM_TITLE'                                    , N'en-US', N'THAMSO_HT', null, null, N'Create THAMSO_HT';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_THAMSO_HT_LIST'                          , N'en-US', N'THAMSO_HT', null, null, N'THAMSO_HT';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_NEW_THAMSO_HT'                           , N'en-US', N'THAMSO_HT', null, null, N'Create THAMSO_HT';
exec dbo.spTERMINOLOGY_InsertOnly N'LNK_REPORTS'                                           , N'en-US', N'THAMSO_HT', null, null, N'THAMSO_HT Reports';
exec dbo.spTERMINOLOGY_InsertOnly N'ERR_THAMSO_HT_NOT_FOUND'                     , N'en-US', N'THAMSO_HT', null, null, N'THAMSO_HT not found.';
exec dbo.spTERMINOLOGY_InsertOnly N'NTC_REMOVE_THAMSO_HT_CONFIRMATION'           , N'en-US', N'THAMSO_HT', null, null, N'Are you sure?';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_NAME'                                       , N'en-US', N'THAMSO_HT', null, null, N'THAMSO_HT';
exec dbo.spTERMINOLOGY_InsertOnly N'LBL_MODULE_ABBREVIATION'                               , N'en-US', N'THAMSO_HT', null, null, N'THA';

exec dbo.spTERMINOLOGY_InsertOnly N'THAMSO_HT'                                          , N'en-US', null, N'moduleList', 100, N'THAMSO_HT';

exec dbo.spTERMINOLOGY_InsertOnly 'LBL_NAME'                                              , 'en-US', 'THAMSO_HT', null, null, 'Name:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_NAME'                                         , 'en-US', 'THAMSO_HT', null, null, 'Name';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_VALUE'                                             , 'en-US', 'THAMSO_HT', null, null, 'value:';
exec dbo.spTERMINOLOGY_InsertOnly 'LBL_LIST_VALUE'                                        , 'en-US', 'THAMSO_HT', null, null, 'value';






