<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIB020202.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divDetailView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIB020202" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIB020202" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>
    <div style="overflow-x: scroll; width: 100%;" class="tabForm">
        <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
            <Columns>
                <asp:TemplateColumn ItemStyle-CssClass="dragHandle" Visible="false">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                    <ItemTemplate>
                        <asp:Image SkinID="blank" Width="14px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_NO">
                    <ItemTemplate>
                        <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO")%>'></asp:Label>
                        <asp:HiddenField ID="txtGROUP_KPI_DETAIL_ID" Value='<%# Bind("GROUP_KPI_DETAIL_ID")%>' runat="server" />
                        <asp:HiddenField ID="txtGROUP_KPI_ID" Value='<%# Bind("GROUP_KPI_ID")%>' runat="server" />
                        <asp:HiddenField ID="txtID" Value='<%# Bind("ID")%>' runat="server" />
                        <asp:HiddenField ID="txtKPI_CODE" Value='<%# Bind("KPI_CODE")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                    <ItemTemplate>
                        <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                    <ItemTemplate>
                        <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("UNIT")%>' runat="server" />
                        <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_1">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth1" Text='<%# Bind("MONTH_1")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_2">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth2" Text='<%# Bind("MONTH_2")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_3">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth3" Text='<%# Bind("MONTH_3")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_4">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth4" Text='<%# Bind("MONTH_4")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_5">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth5" Text='<%# Bind("MONTH_5")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_6">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth6" Text='<%# Bind("MONTH_6")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_7">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth7" Text='<%# Bind("MONTH_7")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_8">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth8" Text='<%# Bind("MONTH_8")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_9">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth9" Text='<%# Bind("MONTH_9")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_10">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth10" Text='<%# Bind("MONTH_10")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_11">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth11" Text='<%# Bind("MONTH_11")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_12">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth12" Text='<%# Bind("MONTH_12")%>' runat="server" CssClass="right number" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB020101.LBL_LIST_DESCRIPTION">
                    <ItemTemplate>
                        <asp:Label ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </SplendidCRM:SplendidGrid>
    </div>
</div>
<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
