﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;


namespace SplendidCRM.KPIB020202
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected SplendidGrid grdMain;

        protected ListControl ddrType;
        protected ListControl ddrOrganization;
        protected Label lbOrganization;
        protected ListControl ddrYear;
        protected DropDownList ddlPERIOD;
        protected DropDownList ddlBusinessCode;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("lblKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                KPIs_Utils.FormatNumberForDataGrid(e.Item, format_number);
            }
        }

        protected void grdMain_ItemCreated(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Header)
            {
                //DataGridItem dgItem = new DataGridItem(0, 0, ListItemType.Header);
                //TableCell dgCell = new TableCell();
                //dgCell.ColumnSpan = 2;
                //dgItem.Cells.Add(dgCell);
                //dgCell.Text = "List of Products";
                //grdMain.Controls[0].Controls.AddAt(0, dgItem);
            }
        }

        private static object _lockObject = new object();
        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwB_KPI_ORG_ALLOCATES_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }
                           
                            if (grdMain.Items.Count <= 0)
                            {
                                throw (new Exception(L10n.Term("KPIB020202.ERR_KPI_ORG_NOT_EXIST")));
                            }
                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;

                                    var ORGANIZATION_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID;
                                    string ORGANIZATION_CODE = KPIs_Utils.getNameFromGuidID(ORGANIZATION_ID, "M_ORGANIZATION", "ORGANIZATION_CODE");
                                    string ORGANIZATION_NAME = KPIs_Utils.getNameFromGuidID(ORGANIZATION_ID, "M_ORGANIZATION", "ORGANIZATION_NAME");
                                    var periodMonth = new SplendidCRM.DynamicControl(this, rowCurrent, "PERIOD").SelectedValue;
                                    var nYEAR = new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").SelectedValue;
                                    if (Sql.IsEmptyGuid(gID))
                                    {
                                        if (!KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                                        {
                                            bool allocKpisOrgExisted = KPIs_Utils.spB_KPI_ORG_ALLOCATES_Existed(Sql.ToInteger(nYEAR), KPIs_Utils.FULL_MONTH, ORGANIZATION_CODE, trn);
                                            if (allocKpisOrgExisted)
                                            {
                                                throw (new Exception(L10n.Term("KPIB020202.ERR_KPI_ORG_ALLOCATES_DUPLICATE")));
                                            }
                                        }
                                        else
                                        {
                                            bool allocKpisOrgExisted = KPIs_Utils.spB_KPI_ORG_ALLOCATES_Existed(Sql.ToInteger(nYEAR), KPIs_Utils.START_MONTH, ORGANIZATION_CODE, trn);
                                            if (allocKpisOrgExisted)
                                            {
                                                throw (new Exception(L10n.Term("KPIB020202.ERR_KPI_ORG_ALLOCATES_DUPLICATE")));
                                            }
                                            allocKpisOrgExisted = KPIs_Utils.spB_KPI_ORG_ALLOCATES_Existed(Sql.ToInteger(nYEAR), KPIs_Utils.END_MONTH, ORGANIZATION_CODE, trn);
                                            if (allocKpisOrgExisted)
                                            {
                                                throw (new Exception(L10n.Term("KPIB020202.ERR_KPI_ORG_ALLOCATES_DUPLICATE")));
                                            }
                                        }
                                        bool kpisOrgExisted = KPIs_Utils.spB_KPI_ORG_ALLOCATES_Existed(Sql.ToInteger(nYEAR), periodMonth, ORGANIZATION_CODE, trn);
                                        if (kpisOrgExisted)
                                        {
                                            throw (new Exception(L10n.Term("KPIB020202.ERR_KPI_ORG_ALLOCATES_DUPLICATE")));
                                        }
                                    }

                                    lock (_lockObject)
                                    {

                                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_org_allocates_frefix"]);
                                        string strAllocatedCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_ORG_ALLOCATES_List", "ALLOCATE_CODE");
                                        //bool bDUPLICATE_CHECKING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && (e.CommandName != "SaveDuplicate") && Sql.IsEmptyGuid(gID);
                                        //if (bDUPLICATE_CHECKING_ENABLED)
                                        //{
                                        //    //validate duplicate code                                            
                                        //    if (Utils.DuplicateCheckCode(Application, con, m_sMODULE, strAllocatedCode, "ALLOCATE_CODE", this, rowCurrent) > 0)
                                        //    {
                                        //        //ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                        //        //ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                        //        throw (new Exception(string.Format(L10n.Term(".ERR_DUPLICATE_CODE_EXCEPTION"), strAllocatedCode)));
                                        //    }
                                        //}


                                        string allocateCode = new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_CODE").Text;
                                        KPIB020202_SqlProc.spB_KPI_ORG_ALLOCATES_Update
                                            (ref gID
                                            , gASSIGNED_USER_ID
                                            , gTEAM_ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_NAME").Text
                                            , string.IsNullOrEmpty(allocateCode) ? strAllocatedCode : allocateCode
                                            , nYEAR//new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").SelectedValue
                                            , periodMonth//new SplendidCRM.DynamicControl(this, rowCurrent, "PERIOD").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                            , ORGANIZATION_CODE
                                            , ORGANIZATION_NAME
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_ID").ID
                                            , KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_TYPE").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "STAFT_NUMBER").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_PLAN_PERCENT").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_PLAN_VALUE").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_GROUP_ID").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FILE_ID").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_BY").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_DATE").DateValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX1").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX2").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX3").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX4").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX5").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                            , trn
                                            );
                                        if (ViewState["CurrentTable"] != null)
                                        {
                                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                            if (dtCurrentTable.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                                {
                                                    Guid gIDT;

                                                    HiddenField hdfID = (HiddenField)grdMain.Items[i].Cells[0].FindControl("txtID");
                                                    HiddenField hdfKPI_CODE = (HiddenField)grdMain.Items[i].Cells[0].FindControl("txtKPI_CODE");
                                                    Label lblKPI_NAME = (Label)grdMain.Items[i].Cells[1].FindControl("txtKPI_NAME");
                                                    //Label lblKPI_UNIT = (Label)grdMain.Items[i].Cells[1].FindControl("lblKPI_UNIT");
                                                    HiddenField hdUnitId = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdUnitId");
                                                    Label lblKPI_RATIO = (Label)grdMain.Items[i].Cells[2].FindControl("lblKPI_RATIO");

                                                    TextBox txtMonth1 = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtMonth1");
                                                    TextBox txtMonth2 = (TextBox)grdMain.Items[i].Cells[4].FindControl("txtMonth2");
                                                    TextBox txtMonth3 = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtMonth3");
                                                    TextBox txtMonth4 = (TextBox)grdMain.Items[i].Cells[6].FindControl("txtMonth4");
                                                    TextBox txtMonth5 = (TextBox)grdMain.Items[i].Cells[7].FindControl("txtMonth5");
                                                    TextBox txtMonth6 = (TextBox)grdMain.Items[i].Cells[8].FindControl("txtMonth6");
                                                    TextBox txtMonth7 = (TextBox)grdMain.Items[i].Cells[9].FindControl("txtMonth7");
                                                    TextBox txtMonth8 = (TextBox)grdMain.Items[i].Cells[10].FindControl("txtMonth8");
                                                    TextBox txtMonth9 = (TextBox)grdMain.Items[i].Cells[11].FindControl("txtMonth9");
                                                    TextBox txtMonth10 = (TextBox)grdMain.Items[i].Cells[12].FindControl("txtMonth10");
                                                    TextBox txtMonth11 = (TextBox)grdMain.Items[i].Cells[13].FindControl("txtMonth11");
                                                    TextBox txtMonth12 = (TextBox)grdMain.Items[i].Cells[14].FindControl("txtMonth12");

                                                    TextBox txtDescription = (TextBox)grdMain.Items[i].Cells[15].FindControl("txtDescription");

                                                    if (hdfID.Value == string.Empty)
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        gIDT = Guid.Parse(hdfID.Value);
                                                    }
                                                    //get all month
                                                    decimal month1 = Sql.ToDecimal(txtMonth1.Text);
                                                    decimal month2 = Sql.ToDecimal(txtMonth2.Text);
                                                    decimal month3 = Sql.ToDecimal(txtMonth3.Text);
                                                    decimal month4 = Sql.ToDecimal(txtMonth4.Text);
                                                    decimal month5 = Sql.ToDecimal(txtMonth5.Text);
                                                    decimal month6 = Sql.ToDecimal(txtMonth6.Text);
                                                    decimal month7 = Sql.ToDecimal(txtMonth7.Text);
                                                    decimal month8 = Sql.ToDecimal(txtMonth8.Text);
                                                    decimal month9 = Sql.ToDecimal(txtMonth9.Text);
                                                    decimal month10 = Sql.ToDecimal(txtMonth10.Text);
                                                    decimal month11 = Sql.ToDecimal(txtMonth11.Text);
                                                    decimal month12 = Sql.ToDecimal(txtMonth12.Text);
                                                    //fisrt month
                                                    if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month1 <= 0 || month2 <= 0 || month3 <= 0 || month4 <= 0 || month5 <= 0 || month6 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    }
                                                    if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month7 <= 0 || month8 <= 0 || month9 <= 0 || month10 <= 0 || month11 <= 0 || month12 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    }
                                                    if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month1 <= 0 || month2 <= 0 || month3 <= 0 || month4 <= 0 || month5 <= 0 || month6 <= 0
                                                            || month7 <= 0 || month8 <= 0 || month9 <= 0 || month10 <= 0 || month11 <= 0 || month12 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    }
                                                    //save details
                                                    KPIB020202_SqlProc.spB_KPI_ORG_ALLOCATE_DETAILS_Update
                                                        (
                                                        ref gIDT
                                                        , gASSIGNED_USER_ID
                                                        , gTEAM_ID
                                                        , string.Empty
                                                        , "Test"
                                                        , gID.ToString()
                                                        , string.IsNullOrEmpty(allocateCode) ? strAllocatedCode : allocateCode
                                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                                        , ORGANIZATION_CODE
                                                        , hdfKPI_CODE.Value.ToString()
                                                        , lblKPI_NAME.Text.ToString()
                                                        , 0
                                                        , hdUnitId.Value == string.Empty ? 0 : Sql.ToInteger(hdUnitId.Value)
                                                        , hdUnitId.Value == string.Empty ? 0 : Sql.ToInteger(hdUnitId.Value)
                                                        , lblKPI_RATIO.Text == string.Empty ? 0 : Sql.ToFloat(lblKPI_RATIO.Text)
                                                        , 0
                                                        , string.Empty
                                                        , txtDescription.Text
                                                        , string.Empty
                                                        , 0
                                                        , txtMonth1.Text == string.Empty ? 0 : month1
                                                         , txtMonth2.Text == string.Empty ? 0 : month2
                                                         , txtMonth3.Text == string.Empty ? 0 : month3
                                                         , txtMonth4.Text == string.Empty ? 0 : month4
                                                         , txtMonth5.Text == string.Empty ? 0 : month5
                                                         , txtMonth6.Text == string.Empty ? 0 : month6
                                                         , txtMonth7.Text == string.Empty ? 0 : month7
                                                         , txtMonth8.Text == string.Empty ? 0 : month8
                                                         , txtMonth9.Text == string.Empty ? 0 : month9
                                                         , txtMonth10.Text == string.Empty ? 0 : month10
                                                         , txtMonth11.Text == string.Empty ? 0 : month11
                                                         , txtMonth12.Text == string.Empty ? 0 : month12
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , trn
                                                    );
                                                }
                                                ViewState["CurrentTable"] = dtCurrentTable;
                                            }
                                        }

                                        SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                        SplendidCRM.SqlProcs.spTRACKER_Update
                                            (Security.USER_ID
                                            , m_sMODULE
                                            , gID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_NAME").Text
                                            , "save"
                                            , trn
                                            );
                                        if (plcSubPanel.Visible)
                                        {
                                            foreach (Control ctl in plcSubPanel.Controls)
                                            {
                                                InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                                if (ctlSubPanel != null)
                                                {
                                                    ctlSubPanel.Save(gID, m_sMODULE, trn);
                                                }
                                            }
                                        }
                                        trn.Commit();
                                        SplendidCache.ClearFavorites();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
            else if (e.CommandName == "btnKPIsList")
            {

                if (ddrOrganization == null)
                {
                    return;
                }
                if (ddrType == null)
                {

                }
                if (ddrType.SelectedValue == string.Empty)
                {
                    return;
                }
                if (ddrOrganization.SelectedValue == string.Empty)
                {
                    return;
                }
                Load_KPIs_List(gID);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_ORG_ALLOCATES_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    //cmd.CommandText += " AND APPROVE_STATUS <> '" + KPIs_Constant.KPI_APPROVE_STATUS_APPROVE + "'" + ControlChars.CrLf;
                                    cmd.CommandText += string.Format(" AND APPROVE_STATUS <> '{0}' AND APPROVE_STATUS <> '{1}'", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["ALLOCATE_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);

                                            if (!Sql.IsEmptyGuid(gDuplicateID))
                                            {
                                                TextBox txtCODE = this.FindControl("ALLOCATE_CODE") as TextBox;
                                                //string code_prefix = Sql.ToString(Application["CONFIG.kpi_org_allocates_frefix"]);
                                                //string code = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_ORG_ALLOCATES_List", "ALLOCATE_CODE");
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.ReadOnly = true;
                                                    txtCODE.Text = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                TextBox txtCODE = this.FindControl("ALLOCATE_CODE") as TextBox;
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.ReadOnly = true;
                                                }
                                            }

                                            TextBox txtNAME = this.FindControl("ALLOCATE_NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();
                                            ListControl ddlALLOCATE_TYPE = this.FindControl("ALLOCATE_TYPE") as ListControl;
                                            if (ddlALLOCATE_TYPE != null)
                                            {
                                                ddlALLOCATE_TYPE.Enabled = false;
                                                ddlALLOCATE_TYPE.CssClass = "dropdown-wrapper";
                                            }
                                            ddrYear = this.FindControl("YEAR") as ListControl;
                                            if (ddrYear != null)
                                            {
                                                ddrYear.Enabled = false;
                                                ddrYear.CssClass = "dropdown-wrapper";
                                            }

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["ALLOCATE_NAME"] = Sql.ToString(rdr["ALLOCATE_NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["ALLOCATE_NAME"] = ViewState["ALLOCATE_NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            //organization
                                            ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                                            if (ddrOrganization != null)
                                            {
                                                ddrOrganization.Items.FindByText(rdr["ORGANIZATION_NAME"].ToString()).Selected = true;
                                            }
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (Sql.IsEmptyGuid(gID))
                        {
                            Load_KPIs_List(gDuplicateID);
                        }
                        else
                        {
                            Load_KPIs_List(gID);
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("ALLOCATE_NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                        //Code
                        TextBox txtCODE = this.FindControl("ALLOCATE_CODE") as TextBox;
                        //string code_prefix = Sql.ToString(Application["CONFIG.kpi_org_allocates_frefix"]);
                        //string code = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_ORG_ALLOCATES_List", "ALLOCATE_CODE");
                        if (txtCODE != null)
                        {
                            txtCODE.ReadOnly = true;
                            txtCODE.Text = string.Empty;
                        }
                        //ASSIGN_BY_NAME
                        TextBox txtASSIGN_BY_NAME = this.FindControl("ASSIGN_BY_NAME") as TextBox;
                        if (txtASSIGN_BY_NAME != null)
                        {
                            txtASSIGN_BY_NAME.ReadOnly = true;
                        }

                    }
                    ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
                    if (ddlPERIOD != null)
                    {
                        ddlPERIOD.AutoPostBack = true;
                        ddlPERIOD.SelectedIndexChanged += new EventHandler(ddlPERIOD_SelectedIndexChanged);
                        string strMonth = ddlPERIOD.SelectedValue.ToString();
                        KPIs_Utils.Load_DataGridByMonth(grdMain, strMonth);
                    }
                    //set default date
                    new SplendidCRM.DynamicControl(this, "ASSIGN_DATE").Text = DateTime.Now.ToString("dd/MM/yyyy");
                    ddlBusinessCode = this.FindControl("BUSINESS_CODE_C") as DropDownList;
                    if (ddlBusinessCode != null)
                    {
                        ddlBusinessCode.AutoPostBack = true;
                        ddlBusinessCode.SelectedIndexChanged += new EventHandler(ddlBusinessCode_SelectIndexChange);
                    }

                    //Load default with allocated type
                    if (Sql.IsEmptyGuid(gID))
                    {
                        load_default_allocated_type();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["ALLOCATE_NAME"] = ViewState["ALLOCATE_NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }
                //year
                ddrYear = this.FindControl("YEAR") as ListControl;
                if (ddrYear != null)
                {
                    ddrYear.AutoPostBack = false;
                    ddrYear.SelectedIndexChanged += new EventHandler(ddrChangeListDetail_ddrYear);
                }
                //organization
                ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                if (ddrOrganization != null)
                {
                    ddrOrganization.AutoPostBack = true;
                    ddrOrganization.SelectedIndexChanged += new EventHandler(ddrOrganization_SelectedIndexChanged);
                }
                //type
                ddrType = this.FindControl("ALLOCATE_TYPE") as ListControl;
                if (ddrType != null)
                {
                    ddrType.AutoPostBack = true;
                    ddrType.SelectedIndexChanged += new EventHandler(ddrType_SelectIndexChange);
                }
                ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
                if (ddlPERIOD != null)
                {
                    ddlPERIOD.AutoPostBack = true;
                    ddlPERIOD.SelectedIndexChanged += new EventHandler(ddlPERIOD_SelectedIndexChanged);
                    string strMonth = ddlPERIOD.SelectedValue.ToString();
                    KPIs_Utils.Load_DataGridByMonth(grdMain, strMonth);
                }
                ddlBusinessCode = this.FindControl("BUSINESS_CODE_C") as DropDownList;
                if (ddlBusinessCode != null)
                {
                    ddlBusinessCode.AutoPostBack = true;
                    ddlBusinessCode.SelectedIndexChanged += new EventHandler(ddlBusinessCode_SelectIndexChange);
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddlBusinessCode_SelectIndexChange(object sender, EventArgs e)
        {
            string strORGTYPE = ddlBusinessCode.SelectedValue;
            ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
            if (ddrOrganization == null)
            {
                return;
            }
            if (KPIs_Constant.ORG_TYPE_PGD.Equals(strORGTYPE))//PHONG GIAO DICH
            {
                var dtSource = SplendidCache.M_ORGANIZATION_PDG();
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                    if (ddrOrganization != null)
                    {
                        ddrOrganization.Items.Clear();
                        ddrOrganization.DataSource = dtSource;
                        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                        ddrOrganization.DataValueField = "ID";
                        ddrOrganization.DataBind();
                    }
                }
            }
            else if (KPIs_Constant.ORG_TYPE_DVKH.Equals(strORGTYPE))// DICH VU KHACH HANG
            {
                var dtSource = SplendidCache.M_ORGANIZATION_DVKH();
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                    if (ddrOrganization != null)
                    {
                        ddrOrganization.Items.Clear();
                        ddrOrganization.DataSource = dtSource;
                        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                        ddrOrganization.DataValueField = "ID";
                        ddrOrganization.DataBind();
                    }
                }
            }
            else if (KPIs_Constant.ORG_TYPE_KHCN.Equals(strORGTYPE))//KHACH HANG CA NHAN
            {
                var dtSource = SplendidCache.M_ORGANIZATION_KHCN();
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                    if (ddrOrganization != null)
                    {
                        ddrOrganization.Items.Clear();
                        ddrOrganization.DataSource = dtSource;
                        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                        ddrOrganization.DataValueField = "ID";
                        ddrOrganization.DataBind();
                    }
                }
            }
            else//CHI NHANH
            {
                var dtSource = SplendidCache.M_ORGANIZATION_CN();
                if (dtSource != null && dtSource.Rows.Count > 0)
                {
                    ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                    if (ddrOrganization != null)
                    {
                        ddrOrganization.Items.Clear();
                        ddrOrganization.DataSource = dtSource;
                        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                        ddrOrganization.DataValueField = "ID";
                        ddrOrganization.DataBind();
                    }
                }
            }
        }

        private void ddrOrganization_SelectedIndexChanged(object sender, EventArgs e)
        {
            //HiddenField hdfORGANIZATION_ID = this.FindControl("ORGANIZATION_ID_C") as HiddenField;
            //hdfORGANIZATION_ID.Value = ddrOrganization.SelectedValue;
        }
        private void ddlPERIOD_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
            if (ddlPERIOD != null)
            {
                if (ddlPERIOD.Items.Count > 0)
                {
                    string strMonth = ddlPERIOD.SelectedValue.ToString();
                    KPIs_Utils.Load_DataGridByMonth(grdMain, strMonth);
                }
            }
        }
        protected void ddrChangeListDetail_ddrYear(object sender, EventArgs e)
        {
            //year = this.FindControl("YEAR") as DropDownList;
            //Load_KPIs_List( int.Parse(year.SelectedValue.ToString()), ddrOrganization.SelectedValue.ToString());
            //Load_KPIs_List(gID);
        }
        protected void ddrType_SelectIndexChange(object sender, EventArgs e)
        {
            load_default_allocated_type();
        }

        private void load_default_allocated_type()
        {
            ddrType = this.FindControl("ALLOCATE_TYPE") as ListControl;
            if (ddrType.SelectedValue != null)
            {
                //clear datagrid
                grdMain.DataSource = null;
                grdMain.DataBind();

                int sl = -1;
                Int32.TryParse(ddrType.SelectedValue, out sl);
                if (string.IsNullOrEmpty(ddrType.SelectedValue))
                {
                    ddrOrganization.Enabled = false;
                    return;
                }

                if (sl == 0)
                {
                    ddrOrganization.Enabled = false;
                    return;
                }
                ddlBusinessCode = this.FindControl("BUSINESS_CODE_C") as DropDownList;
                if (ddlBusinessCode != null)
                {
                    if (sl == 3)
                    {
                        if (ddlBusinessCode.Items.Count > 0)
                        {
                            ddlBusinessCode.SelectedIndex = 0;
                        }
                        ddlBusinessCode.Enabled = false;
                    }
                    else if (sl == 4)
                    {
                        ddlBusinessCode.Enabled = true;
                    }
                }

                ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                if (ddrOrganization == null)
                {
                    return;
                }

                if (sl == 1 || sl == 2 || sl == 3 || sl == 4)
                {
                    ddrOrganization.Enabled = true;
                    var dtSource = SplendidCache.M_ORGANIZATION(sl);
                    if (dtSource != null && dtSource.Rows.Count > 0)
                    {
                        ddrOrganization.DataSource = dtSource;
                        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                        ddrOrganization.DataValueField = "ID";
                        ddrOrganization.DataBind();
                    }
                    return;
                }
            }
        }

        private void Load_KPIs_List(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            string sSQL_DETAILS = "";
            string strWhere = "";

            using (IDbConnection con = dbf.CreateConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    if (Sql.IsEmptyGuid(gID))// them moi, empty gID
                    {
                        int sl = -1;
                        Int32.TryParse(ddrType.SelectedValue, out sl);
                        string orgCode = (ddrOrganization == null ? "null" : ddrOrganization.SelectedValue);
                        string year = (ddrYear == null ? DateTime.Now.Year.ToString("yyyy") : ddrYear.SelectedValue);
                        string businessCode = (ddlBusinessCode == null ? "null" : ddlBusinessCode.SelectedValue);

                        strWhere = string.Format(" (IS_COMMON = 1 OR IS_COMMON = 0) AND (TYPE = '{0}' OR ORGANIZATION_ID = '{1}') AND YEAR = '{2}' ", sl, orgCode, year);
                        strWhere += string.Format("  AND APPROVE_STATUS = '{0}' AND STATUS = '{1}' AND BUSINESS_CODE = '{2}' ) ", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Utils.ACTIVE, businessCode);

                        sSQL_DETAILS = " SELECT row_number() OVER (order by DATE_ENTERED) AS NO, GROUP_KPI_ID, '' AS ID  " + ControlChars.CrLf
                                     + " , ID AS GROUP_KPI_DETAIL_ID, KPI_NAME, UNIT, RATIO, MAX_RATIO_COMPLETE      " + ControlChars.CrLf
                                     + " ,  DESCRIPTION, REMARK, '' AS VALUE_STD_PER_MONTH                           " + ControlChars.CrLf
                                     + " , '' AS MONTH_1, '' AS MONTH_2, '' AS MONTH_3, '' AS MONTH_4, '' AS MONTH_5, '' AS MONTH_6,                    " + ControlChars.CrLf
                                     + " '' AS MONTH_7, '' AS MONTH_8, '' AS MONTH_9, '' AS MONTH_10, '' AS MONTH_11, '' AS MONTH_12, KPI_CODE          " + ControlChars.CrLf
                                     + " FROM vwM_GROUP_KPI_DETAILS_List  WHERE GROUP_KPI_ID IN (SELECT DISTINCT [ID] FROM vwM_GROUP_KPIS_Edit WHERE    {0} " + ControlChars.CrLf;

                        cmd.CommandText = string.Format(sSQL_DETAILS, strWhere);
                        cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                        con.Open();
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                if (dtCurrent.Rows.Count > 0)
                                {
                                    grdMain.DataSource = dtCurrent;
                                    grdMain.DataBind();
                                    ViewState["CurrentTable"] = dtCurrent;
                                }
                                else
                                {
                                    grdMain.DataSource = null;
                                    grdMain.DataBind();
                                }
                            }
                        }
                    }
                    else
                    {
                        this.Load_Detail_ForEdit(dbf, gID);
                    }
                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));


                }
            }
            //End Details list
        }
        protected void Load_Detail_ForEdit(DbProviderFactory dbf, Guid ID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT '' AS GROUP_KPI_ID, row_number() OVER (order by DATE_ENTERED) AS NO, ID, ID AS GROUP_KPI_DETAIL_ID,KPI_NAME, UNIT,RADIO AS RATIO, MAX_RATIO_COMPLETE , DESCRIPTION, REMARK, '' AS VALUE_STD_PER_MONTH    " + ControlChars.CrLf
                       + " , MONTH_1, MONTH_2, MONTH_3, MONTH_4, MONTH_5, MONTH_6, MONTH_7                       " + ControlChars.CrLf
                       + " , MONTH_8, MONTH_9, MONTH_10, MONTH_11 , MONTH_12, KPI_CODE                        " + ControlChars.CrLf
                                                      + "  from vwB_KPI_ORG_ALLOCATE_DETAILS_Edit   " + ControlChars.CrLf;

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                    //Security.Filter(cmd, m_sMODULE, "edit");

                    Sql.AppendParameter(cmd, ID, "KPI_ALLOCATE_ID", false);
                    cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                            }
                        }
                    }
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            grdMain.ItemCreated += new DataGridItemEventHandler(grdMain_ItemCreated);

            m_sMODULE = "KPIB020202";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
