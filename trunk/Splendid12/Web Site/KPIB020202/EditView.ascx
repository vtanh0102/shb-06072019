<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB020202.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    #ctl00_cntBody_ctlEditView_ALLOCATE_CODE {
        background-color: transparent;
        border: 0;
        font-size: 1em;
    }

    #ctl00_cntBody_ctlEditView_ASSIGN_BY_NAME {
        width: 33%;
    }

    #ctl00_cntBody_ctlEditView_ALLOCATE_NAME {
        width: 73%;
    }
    .dropdown-wrapper {
        width: 115% /* This hides the arrow icon */;
        background-color: transparent /* This hides the background */;
        background-image: none;
        -webkit-appearance: none /* Webkit Fix */;
        border: none;
        box-shadow: none;
        padding: 0.3em 0.5em;
    }
</style>
<script runat="server">
 
</script>
<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIB020202" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />
     
    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView%>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>
            <div style="overflow-x: scroll; width: 100%;" class="tabForm">
                <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                    <Columns>
                        <asp:TemplateColumn ItemStyle-CssClass="dragHandle" Visible="false">
                            <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                            <ItemTemplate>
                                <asp:Image SkinID="blank" Width="14px" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText=".LBL_NO">
                            <ItemTemplate>
                                <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO")%>'></asp:Label>
                                <asp:HiddenField ID="txtGROUP_KPI_DETAIL_ID" Value='<%# Bind("GROUP_KPI_DETAIL_ID")%>' runat="server" />
                                <asp:HiddenField ID="txtGROUP_KPI_ID" Value='<%# Bind("GROUP_KPI_ID")%>' runat="server" />
                                <asp:HiddenField ID="txtID" Value='<%# Bind("ID")%>' runat="server" />
                                <asp:HiddenField ID="txtKPI_CODE" Value='<%# Bind("KPI_CODE")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                            <ItemTemplate>
                                <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                            <ItemTemplate>
                                <asp:Label ID="lblKPI_UNIT" Text='<%# Bind("UNIT")%>' runat="server"/>
                                <asp:HiddenField ID="hdUnitId" Value='<%# Bind("UNIT")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO")%>' runat="server" CssClass="right"/>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_1">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth1" Text='<%# Bind("MONTH_1")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_2">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth2" Text='<%# Bind("MONTH_2")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_3">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth3" Text='<%# Bind("MONTH_3")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_4">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth4" Text='<%# Bind("MONTH_4")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_5">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth5" Text='<%# Bind("MONTH_5")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_6">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth6" Text='<%# Bind("MONTH_6")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_7">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth7" Text='<%# Bind("MONTH_7")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_8">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth8" Text='<%# Bind("MONTH_8")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_9">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth9" Text='<%# Bind("MONTH_9")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_10">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth10" Text='<%# Bind("MONTH_10")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_11">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth11" Text='<%# Bind("MONTH_11")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB0203.LBL_MONTH_12">
                            <HeaderStyle HorizontalAlign="Right"/>
                            <ItemStyle HorizontalAlign="Right"/>
                            <ItemTemplate>
                                <asp:TextBox ID="txtMonth12" Text='<%# Bind("MONTH_12")%>' runat="server" Width="40px" CssClass="right number format-qty" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KPIB020101.LBL_LIST_DESCRIPTION">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </SplendidCRM:SplendidGrid>
            </div>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
                <script type="text/javascript">
                    $(function () {
                        numeric(".number");
                    });
                </script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
    <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />
    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
