
function KPIR010104_KPIR010104_NAME_Changed(fldKPIR010104_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldKPIR010104_NAME.id.substring(0, fldKPIR010104_NAME.id.length - 'KPIR010104_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010104_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_KPIR010104_NAME = document.getElementById(userContext + 'PREV_KPIR010104_NAME');
	if ( fldPREV_KPIR010104_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_KPIR010104_NAME');
	}
	else if ( fldPREV_KPIR010104_NAME.value != fldKPIR010104_NAME.value )
	{
		if ( fldKPIR010104_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIR010104.AutoComplete.KPIR010104_KPIR010104_NAME_Get(fldKPIR010104_NAME.value, KPIR010104_KPIR010104_NAME_Changed_OnSucceededWithContext, KPIR010104_KPIR010104_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('KPIR010104_KPIR010104_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			KPIR010104_KPIR010104_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function KPIR010104_KPIR010104_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'KPIR010104_NAME_AjaxErrors');
		var fldKPIR010104_ID        = document.getElementById(userContext + 'KPIR010104_ID'       );
		var fldKPIR010104_NAME      = document.getElementById(userContext + 'KPIR010104_NAME'     );
		var fldPREV_KPIR010104_NAME = document.getElementById(userContext + 'PREV_KPIR010104_NAME');
		if ( fldKPIR010104_ID        != null ) fldKPIR010104_ID.value        = sID  ;
		if ( fldKPIR010104_NAME      != null ) fldKPIR010104_NAME.value      = sNAME;
		if ( fldPREV_KPIR010104_NAME != null ) fldPREV_KPIR010104_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIR010104.AutoComplete service is null');
	}
}

function KPIR010104_KPIR010104_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010104_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldKPIR010104_ID        = document.getElementById(userContext + 'KPIR010104_ID'       );
	var fldKPIR010104_NAME      = document.getElementById(userContext + 'KPIR010104_NAME'     );
	var fldPREV_KPIR010104_NAME = document.getElementById(userContext + 'PREV_KPIR010104_NAME');
	if ( fldKPIR010104_ID        != null ) fldKPIR010104_ID.value        = '';
	if ( fldKPIR010104_NAME      != null ) fldKPIR010104_NAME.value      = '';
	if ( fldPREV_KPIR010104_NAME != null ) fldPREV_KPIR010104_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

