﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;
using System.Web.Configuration;
using RestSharp.Extensions;

namespace SplendidCRM.KPIR010104
{

	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                          ;
		protected HtmlTable       tblMain                      ;
		protected PlaceHolder     plcSubPanel                  ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
						DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							DataRow   rowCurrent = null;
							DataTable dtCurrent  = new DataTable();
							if ( !Sql.IsEmptyGuid(gID) )
							{
								string sSQL ;
								sSQL = "select *           " + ControlChars.CrLf
								     + "  from vwKPIR010104_Edit" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Security.Filter(cmd, m_sMODULE, "edit");
									Sql.AppendParameter(cmd, gID, "ID", false);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											rowCurrent = dtCurrent.Rows[0];
											DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
											if ( Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"])  && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED )
											{
												ctlDynamicButtons.ShowButton("SaveConcurrency", true);
												ctlFooterButtons .ShowButton("SaveConcurrency", true);
												throw(new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
											}
										}
										else
										{
											gID = Guid.Empty;
										}
									}
								}
							}

							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
							bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
							if ( bDUPLICATE_CHECHING_ENABLED )
							{
								if ( Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0 )
								{
									ctlDynamicButtons.ShowButton("SaveDuplicate", true);
									ctlFooterButtons .ShowButton("SaveDuplicate", true);
									throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
								}
							}
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIR010104_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME"                       ).Text
										, trn
										);

									SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
									SplendidCRM.SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
										, "save"
										, trn
										);
									if ( plcSubPanel.Visible )
									{
										foreach ( Control ctl in plcSubPanel.Controls )
										{
											InlineEditControl ctlSubPanel = ctl as InlineEditControl;
											if ( ctlSubPanel != null )
											{
												ctlSubPanel.Save(gID, m_sMODULE, trn);
											}
										}
									}
									trn.Commit();
									SplendidCache.ClearFavorites();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
							rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
							this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
						}
						
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else
							Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
            else if (e.CommandName == "Search")
			{
                BindingReport();
            }
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

            #region MyRegion
            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwKPIR010104_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                Security.Filter(cmd, m_sMODULE, "edit");
                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["NAME"] = Sql.ToString(rdr["NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["NAME"] = ViewState["NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["NAME"] = ViewState["NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            } 
            #endregion

            //
            if (!IsPostBack)
            {
                Bind_Branch_List();
                var ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
                if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    ddlBRANCH.SelectedValue = Security.CURRENT_EMPLOYEE.MAIN_POS_CODE;
                    ddlBRANCH.Enabled = false;

                    //if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                    //{
                       
                    //}
                    //else if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                    //{
                    //    ddlBRANCH.SelectedValue = Security.CURRENT_EMPLOYEE.MAIN_POS_CODE;
                    //    ddlBRANCH.Enabled = false;
                    //}

                }
            }
            
		}

	    void Bind_Branch_List()
	    {
            var ddlBRANCH = this.FindControl("BRANCH") as DropDownList;

            if (ddlBRANCH != null)
            {
                ddlBRANCH.Items.Clear();
                ddlBRANCH.DataSource = SplendidCache.M_ORGANIZATION_MAINPOS();//chi nhanh
                ddlBRANCH.DataTextField = "ORGANIZATION_NAME";
                ddlBRANCH.DataValueField = "ORGANIZATION_CODE";
                ddlBRANCH.DataBind();
                ddlBRANCH.Items.Insert(0, new ListItem("--Tất cả--", string.Empty));
                ddlBRANCH.SelectedIndex = 0;
            }
        }

        private void BindingReport()
        {
            ReportViewer reportviewer = this.FindControl("RP_NSLD") as ReportViewer;
            if(reportviewer == null) return;

            reportviewer.LocalReport.ReportPath = Server.MapPath("~/ReportView/Report_NSLD.rdlc");
            var dt = GetDataFormDB();
            //if(dt.DataSet.Tables[0])
            var dtSource = new ReportDataSource("DataSet_NSLD", dt);
            
            reportviewer.LocalReport.DataSources.Clear();
            reportviewer.LocalReport.DataSources.Add(dtSource);
            reportviewer.LocalReport.Refresh();
        }

	    private DataTable GetDataFormDB()
	    {
            var table = new ReportData.DataSet_NSLD.DSGNDataTable();
            var connectionString = WebConfigurationManager.ConnectionStrings["SQL_TMP"].ConnectionString;

            //var query = @"SELECT * from B_KPI_RESULT_EMP_TMP where POS_CODE = @POS_CODE and YEAR = @YEAR and PERIOD = @PERIOD";
	        var query = @"Select U.LAST_NAME, U.FIRST_NAME, U.DATE_START_WORK, U.DATE_START_PROBATION, 
                          R.USER_CODE, R.POS_CODE, R.YEAR, R.PERIOD, R.VALUE_20, R.VALUE_21, R.VALUE_22, R.VALUE_23, R.VALUE_24, R.VALUE_25, R.VALUE_26,
                          R.VALUE_27, R.VALUE_28, R.VALUE_29, R.VALUE_30, R.VALUE_31, R.VALUE_32, R.VALUE_33, R.VALUE_34, R.VALUE_35, R.VALUE_54, R.VALUE_9, R.VALUE_11,R.VALUE_56 "
                        + @" from B_KPI_RESULT_NET_TMP as R inner join M_USERS_TMP as U on R.USER_CODE = U.USER_CODE"
                        + @" where 1=1 "; //r.POS_CODE = @POS_CODE and R.YEAR = @YEAR and r.PERIOD = @PERIOD;";

            try
            {
                var ddl_Year = this.FindControl("YEAR") as DropDownList;
                var ddl_Month = this.FindControl("MONTH") as DropDownList;
                var ddl_POS = this.FindControl("BRANCH") as DropDownList;

                var pos = ddl_POS.SelectedItem.Value;
                var year = ddl_Year.SelectedItem.Value;
                var month = ddl_Month.SelectedItem.Value;
                var monthAvg = int.Parse(ddl_Month.SelectedItem.Text.Trim());

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        if (!string.IsNullOrEmpty(pos))
                        {
                            command.CommandText += " and r.POS_CODE = @POS_CODE ";
                            command.Parameters.AddWithValue("@POS_CODE", pos);
                        }
                        if (!string.IsNullOrEmpty(year))
                        {
                            command.CommandText += " and r.YEAR = @YEAR ";
                            command.Parameters.AddWithValue("@YEAR", year);
                        }
                        if (!string.IsNullOrEmpty(month))
                        {
                            command.CommandText += " and r.PERIOD = @PERIOD ";
                            command.Parameters.AddWithValue("@PERIOD", month);
                        }
                        
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var row = table.NewDSGNRow();
//                            var date = reader.GetSqlDateTime(reader.GetOrdinal("DATE_START_PROBATION"));
//                            if(date.Value.)
                            

                            row.MA_POS = reader.GetString(reader.GetOrdinal("POS_CODE"));
                            row.MA_NV = reader.GetString(reader.GetOrdinal("USER_CODE"));

                            row.TEN_NV = reader.GetString(reader.GetOrdinal("FIRST_NAME")) + " " +
                                         reader.GetString(reader.GetOrdinal("LAST_NAME"));

                            row.TEN_POS = reader.GetString(reader.GetOrdinal("POS_CODE"));//todo

                            //TIN DUNG
                            row.DSGN = SafeGetDouble(reader, "VALUE_20");
                            row.DSGN_SUM = SafeGetDouble(reader, "VALUE_21");
                            row.DSGN_AVG = row.DSGN_SUM / monthAvg;

                            row.DUNO_AVG = SafeGetDouble(reader, "VALUE_22");

                            row.SL_KHVV_SUM = SafeGetDouble(reader, "VALUE_23");
                            row.SL_KHVV_AVG = row.SL_KHVV_SUM/monthAvg;

                            var noQuaHan = SafeGetDouble(reader, "VALUE_25");
                            var noXau = SafeGetDouble(reader, "VALUE_26");
                            var duNoTD = SafeGetDouble(reader, "VALUE_24");

                            //row.TYLE_NO_QH = duNoTD == 0 ? 0 : noQuaHan / duNoTD;
                            //row.TYLE_NO_XAU = duNoTD == 0 ? 0 : noXau / duNoTD;
                            row.TYLE_NO_QH = SafeGetDouble(reader, "VALUE_9");
                            row.TYLE_NO_XAU = SafeGetDouble(reader, "VALUE_11");

                            //TANG DONG
                            row.TANGDONG_HDV_KHCN_SUM = SafeGetDouble(reader, "VALUE_56");
                            row.TANGDONG_HUYDONG_AVG = row.TANGDONG_HDV_KHCN_SUM / monthAvg;
                            row.HUYDONGVON_AVG = SafeGetDouble(reader, "VALUE_28");

                            //THE
                            //NOI DIA
                            row.SL_THE_ND_SUM = SafeGetDouble(reader, "VALUE_29");
                            row.SL_THE_ND_AVG = row.SL_THE_ND_SUM / monthAvg;
                            //GNQT
                            row.SL_THE_GNQT_SUM = SafeGetDouble(reader, "VALUE_30");
                            row.SL_THE_GNQT_AVG = row.SL_THE_GNQT_SUM / monthAvg;
                            //TDQT
                            row.SL_THE_TDQT_SUM = SafeGetDouble(reader, "VALUE_31");
                            row.SL_THE_TDQT_AVG = row.SL_THE_TDQT_SUM / monthAvg;

                            //NGAN HANG DIEN TU
                            //SHB ONLINE
                            row.SL_KH_ONLINE_SUM = SafeGetDouble(reader, "VALUE_32");
                            row.SL_KH_ONLINE_AVG = row.SL_KH_ONLINE_SUM / monthAvg;
                            //SHB MOBILE
                            row.SL_KH_MOBILE_SUM = SafeGetDouble(reader, "VALUE_35");
                            row.SL_KH_MOBILE_AVG = row.SL_KH_MOBILE_SUM / monthAvg;
                            //SMS
                            row.SL_KH_SMS_SUM = SafeGetDouble(reader, "VALUE_34");
                            row.SL_KH_SMS_AVG = row.SL_KH_SMS_SUM / monthAvg;

                            //THU THUAN
                            row.THUTHUAN_SUM = SafeGetDouble(reader, "VALUE_54");
                            row.THUTHUAN_AVG = row.THUTHUAN_SUM / monthAvg;

                            //ADD ROW TO TABLE
                            table.AddDSGNRow(row);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    con.Close();
                }

                return table;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return table;
            }
        }

        private double SafeGetDouble(SqlDataReader reader, string colName)
        {
            var colIndex = reader.GetOrdinal(colName);
            return !reader.IsDBNull(colIndex) ? reader.GetDouble(colIndex) : 0;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIR010104";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
		#endregion
	}
}
