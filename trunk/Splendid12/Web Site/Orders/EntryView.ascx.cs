/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Globalization;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Orders
{
	/// <summary>
	///		Summary description for EntryView.
	/// </summary>
	public class EntryView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 06/22/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;
		protected _controls.EditLineItemsView ctlEditLineItemsView;

		protected HiddenField     ORDER_ID               ;
		protected HiddenField     INVOICE_ID             ;
		protected HiddenField     PAYMENT_ID             ;
		protected HiddenField     PAYMENTS_TRANSACTION_ID;
		protected HtmlTable       tblMain                ;
		// 09/02/2012 Paul.  EditViews were combined into a single view. 
		//protected HtmlTable       tblCreditCard          ;
		//protected HtmlTable       tblAddress             ;
		protected HtmlTable       tblSummary             ;
		protected HtmlTable       tblDescription         ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			Guid gQUOTE_ID      = Sql.ToGuid(Request["QUOTE_ID"]);
			// 06/08/2006 Paul.  Redirect to parent if that is where the note was originated. 
			Guid   gPARENT_ID   = Sql.ToGuid(Request["PARENT_ID"]);
			string sMODULE      = String.Empty;
			string sPARENT_TYPE = String.Empty;
			string sPARENT_NAME = String.Empty;
			try
			{
				SqlProcs.spPARENT_Get(ref gPARENT_ID, ref sMODULE, ref sPARENT_TYPE, ref sPARENT_NAME);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				// The only possible error is a connection failure, so just ignore all errors. 
				gPARENT_ID = Guid.Empty;
			}
			if ( e.CommandName == "Save" )
			{
				this.ValidateEditViewFields(m_sMODULE + ".EntryView"       );
				this.ValidateEditViewFields(m_sMODULE + ".EntryCredit"     );
				this.ValidateEditViewFields(m_sMODULE + ".EntryAddress"    );
				this.ValidateEditViewFields(m_sMODULE + ".EntryDescription");
				if ( Page.IsValid )
				{
					Guid gACCOUNT_ID              = new DynamicControl(this, "BILLING_ACCOUNT_ID").ID;
					Guid gCONTACT_ID              = new DynamicControl(this, "BILLING_CONTACT_ID").ID;
					Guid gORDER_ID                = Sql.ToGuid(ORDER_ID               .Value);
					Guid gINVOICE_ID              = Sql.ToGuid(INVOICE_ID             .Value);
					Guid gPAYMENT_ID              = Sql.ToGuid(PAYMENT_ID             .Value);
					Guid gPAYMENTS_TRANSACTION_ID = Sql.ToGuid(PAYMENTS_TRANSACTION_ID.Value);

					DataTable dtAccountsCustomFields    = SplendidCache.FieldsMetaData_Validated("ACCOUNTS");
					DataTable dtContactsCustomFields    = SplendidCache.FieldsMetaData_Validated("CONTACTS");
					DataTable dtOrdersCustomFields      = SplendidCache.FieldsMetaData_Validated("ORDERS"  );
					DataTable dtOrdersCustomLineItems   = SplendidCache.FieldsMetaData_UnvalidatedCustomFields("ORDERS_LINE_ITEMS");
					DataTable dtInvoicesCustomFields    = SplendidCache.FieldsMetaData_Validated("INVOICES");
					DataTable dtInvoicesCustomLineItems = SplendidCache.FieldsMetaData_UnvalidatedCustomFields("INVOICES_LINE_ITEMS");
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
						using ( IDbTransaction trn = Sql.BeginTransaction(con) )
						{
							try
							{
								if ( Sql.IsEmptyGuid(gACCOUNT_ID) )
								{
									// 08/06/2009 Paul.  ACCOUNT_NUMBER now uses our number sequence table. 
									// 04/07/2010 Paul.  Add EXCHANGE_FOLDER. 
									SqlProcs.spACCOUNTS_Update
										( ref gACCOUNT_ID
										, new DynamicControl(this, "ASSIGNED_USER_ID"           ).ID
										, new DynamicControl(this, "BUSINESS_NAME"              ).Text
										, String.Empty
										, Guid.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, new DynamicControl(this, "BILLING_ADDRESS_STREET"     ).Text
										, new DynamicControl(this, "BILLING_ADDRESS_CITY"       ).Text
										, new DynamicControl(this, "BILLING_ADDRESS_STATE"      ).Text
										, new DynamicControl(this, "BILLING_ADDRESS_POSTALCODE" ).Text
										, new DynamicControl(this, "BILLING_ADDRESS_COUNTRY"    ).Text
										, String.Empty
										, String.Empty
										, new DynamicControl(this, "PHONE_WORK"                 ).Text
										, String.Empty
										, new DynamicControl(this, "EMAIL1"                     ).Text
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty  // ACCOUNT_NUMBER
										, new DynamicControl(this, "TEAM_ID"         ).ID
										, new DynamicControl(this, "TEAM_SET_LIST"   ).Text
										, false         // EXCHANGE_FOLDER
										// 08/07/2015 Paul.  Add picture. 
										, String.Empty  // PICTURE
										// 05/12/2016 Paul.  Add Tags module. 
										, String.Empty  // TAG_SET_NAME
										// 06/07/2017 Paul.  Add NAICSCodes module. 
										, String.Empty  // NAICS_SET_NAME
										// 10/27/2017 Paul.  Add Accounts as email source. 
										, false         // DO_NOT_CALL
										, false         // EMAIL_OPT_OUT
										, false         // INVALID_EMAIL
										// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
										, new DynamicControl(this, "ASSIGNED_SET_LIST").Text
										, trn
										);
									SplendidDynamic.UpdateCustomFields(this, trn, gACCOUNT_ID, "ACCOUNTS", dtAccountsCustomFields);
									new DynamicControl(this, "BILLING_ACCOUNT_ID").ID = gACCOUNT_ID;
								}
								if ( Sql.IsEmptyGuid(gCONTACT_ID) )
								{
									SqlProcs.spCONTACTS_Update
										( ref gCONTACT_ID
										, new DynamicControl(this, "ASSIGNED_USER_ID"          ).ID
										, new DynamicControl(this, "SALUTATION"                ).SelectedValue
										, new DynamicControl(this, "FIRST_NAME"                ).Text
										, new DynamicControl(this, "LAST_NAME"                 ).Text
										, new DynamicControl(this, "BILLING_ACCOUNT_ID"        ).ID
										, String.Empty
										, String.Empty
										, String.Empty
										, Guid.Empty
										, DateTime.MinValue
										, false
										, String.Empty
										, String.Empty
										, new DynamicControl(this, "PHONE_WORK"                ).Text
										, String.Empty
										, String.Empty
										, new DynamicControl(this, "EMAIL1"                    ).Text
										, String.Empty
										, String.Empty
										, String.Empty
										, false
										, false
										, new DynamicControl(this, "BUSINESS_ADDRESS_STREET"    ).Text
										, new DynamicControl(this, "BUSINESS_ADDRESS_CITY"      ).Text
										, new DynamicControl(this, "BUSINESS_ADDRESS_STATE"     ).Text
										, new DynamicControl(this, "BUSINESS_ADDRESS_POSTALCODE").Text
										, new DynamicControl(this, "BUSINESS_ADDRESS_COUNTRY"   ).Text
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, String.Empty
										, Guid.Empty
										, false
										, new DynamicControl(this, "TEAM_ID"         ).ID
										, new DynamicControl(this, "TEAM_SET_LIST"   ).Text
										// 09/27/2013 Paul.  SMS messages need to be opt-in. 
										, new DynamicControl(this, "SMS_OPT_IN"      ).SelectedValue
										// 10/22/2013 Paul.  Provide a way to map Tweets to a parent. 
										, new DynamicControl(this, "TWITTER_SCREEN_NAME").Text
										// 08/07/2015 Paul.  Add picture. 
										, String.Empty  // PICTURE
										// 08/07/2015 Paul.  Add Leads/Contacts relationship. 
										, Guid.Empty    // LEAD_ID
										// 09/27/2015 Paul.  Separate SYNC_CONTACT and EXCHANGE_FOLDER. 
										, false         // EXCHANGE_FOLDER
										// 05/12/2016 Paul.  Add Tags module. 
										, String.Empty  // TAG_SET_NAME
										// 06/20/2017 Paul.  Add number fields to Contacts, Leads, Prospects, Opportunities and Campaigns. 
										, String.Empty  // CONTACT_NUMBER
										// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
										, new DynamicControl(this, "ASSIGNED_SET_LIST").Text
										, trn
										);
									SplendidDynamic.UpdateCustomFields(this, trn, gCONTACT_ID, "CONTACTS", dtContactsCustomFields);
									new DynamicControl(this, "BILLING_CONTACT_ID").ID = gCONTACT_ID;
								}
								ctlEditLineItemsView.UpdateTotals();
								// 08/06/2009 Paul.  ORDER_NUM now uses our number sequence table. 
								SqlProcs.spORDERS_Update
									( ref gORDER_ID
									, new DynamicControl(this, "ASSIGNED_USER_ID"                ).ID
									, new DynamicControl(this, "NAME"                            ).Text
									, gQUOTE_ID
									, new DynamicControl(this, "OPPORTUNITY_ID"                  ).ID
									, new DynamicControl(this, "PAYMENT_TERMS"                   ).SelectedValue
									, new DynamicControl(this, "ORDER_STAGE"                     ).SelectedValue
									, new DynamicControl(this, "PURCHASE_ORDER_NUM"              ).Text
									, new DynamicControl(this, "ORIGINAL_PO_DATE"                ).DateValue
									, new DynamicControl(this, "DATE_ORDER_DUE"                  ).DateValue
									, new DynamicControl(this, "DATE_ORDER_SHIPPED"              ).DateValue
									, new DynamicControl(ctlEditLineItemsView, "SHOW_LINE_NUMS"  ).Checked
									, new DynamicControl(ctlEditLineItemsView, "CALC_GRAND_TOTAL").Checked
									, new DynamicControl(ctlEditLineItemsView, "EXCHANGE_RATE"   ).FloatValue
									, new DynamicControl(ctlEditLineItemsView, "CURRENCY_ID"     ).ID
									, new DynamicControl(ctlEditLineItemsView, "TAXRATE_ID"      ).ID
									, new DynamicControl(ctlEditLineItemsView, "SHIPPER_ID"      ).ID
									, new DynamicControl(ctlEditLineItemsView, "SUBTOTAL"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "DISCOUNT"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "SHIPPING"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "TAX"             ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "TOTAL"           ).DecimalValue
									, new DynamicControl(this, "BILLING_ACCOUNT_ID"              ).ID
									, new DynamicControl(this, "BILLING_CONTACT_ID"              ).ID
									, new DynamicControl(this, "BILLING_ADDRESS_STREET"          ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_CITY"            ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_STATE"           ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_POSTALCODE"      ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_COUNTRY"         ).Text
									, new DynamicControl(this, "SHIPPING_ACCOUNT_ID"             ).ID
									, new DynamicControl(this, "SHIPPING_CONTACT_ID"             ).ID
									, new DynamicControl(this, "SHIPPING_ADDRESS_STREET"         ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_CITY"           ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_STATE"          ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_POSTALCODE"     ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_COUNTRY"        ).Text
									, new DynamicControl(this, "DESCRIPTION"                     ).Text
									, new DynamicControl(this, "ORDER_NUM"                       ).Text
									, new DynamicControl(this, "TEAM_ID"                         ).ID
									, new DynamicControl(this, "TEAM_SET_LIST"                   ).Text
									// 05/12/2016 Paul.  Add Tags module. 
									, String.Empty  // TAG_SET_NAME
									// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
									, new DynamicControl(this, "ASSIGNED_SET_LIST"               ).Text
									, trn
									);
								SplendidDynamic.UpdateCustomFields(this, trn, gORDER_ID, "ORDERS", dtOrdersCustomFields);
								
								DataTable dtLineItems = ctlEditLineItemsView.LineItems;
								foreach ( DataRow row in dtLineItems.Rows )
								{
									if ( row.RowState == DataRowState.Deleted )
									{
										// 05/26/2007 Paul.  In order to access values from deleted row, use DataRowVersion.Original, 
										// otherwise accessing the data will throw an exception "Deleted row information cannot be accessed through the row."
										Guid gITEM_ID = Sql.ToGuid(row["ID", DataRowVersion.Original]);
										if ( !Sql.IsEmptyGuid(gITEM_ID) )
											SqlProcs.spORDERS_LINE_ITEMS_Delete(gITEM_ID, trn);
									}
								}
								// 12/28/2007 Paul.  Renumber the position. 
								int nPOSITION = 1;
								foreach ( DataRow row in dtLineItems.Rows )
								{
									if ( row.RowState != DataRowState.Deleted )
									{
										Guid    gITEM_ID             = Sql.ToGuid   (row["ID"                 ]);
										Guid    gLINE_GROUP_ID       = Sql.ToGuid   (row["LINE_GROUP_ID"      ]);
										string  sLINE_ITEM_TYPE      = Sql.ToString (row["LINE_ITEM_TYPE"     ]);
										//int     nPOSITION            = Sql.ToInteger(row["POSITION"           ]);
										string  sNAME                = Sql.ToString (row["NAME"               ]);
										string  sMFT_PART_NUM        = Sql.ToString (row["MFT_PART_NUM"       ]);
										string  sVENDOR_PART_NUM     = Sql.ToString (row["VENDOR_PART_NUM"    ]);
										Guid    gPRODUCT_TEMPLATE_ID = Sql.ToGuid   (row["PRODUCT_TEMPLATE_ID"]);
										// 07/11/2010 Paul.  Add PARENT_TEMPLATE_ID. 
										Guid    gPARENT_TEMPLATE_ID  = Sql.ToGuid   (row["PARENT_TEMPLATE_ID" ]);
										// 07/15/2010 Paul.  Add GROUP_ID for options management. 
										// 08/13/2010 Paul.  Use LINE_GROUP_ID instead of GROUP_ID. 
										//Guid    gGROUP_ID            = Sql.ToGuid   (row["GROUP_ID"           ]);
										string  sTAX_CLASS           = Sql.ToString (row["TAX_CLASS"          ]);
										// 02/10/2011 Paul.  Stop converting the Quantity to an integer. 
										float   nQUANTITY            = Sql.ToFloat  (row["QUANTITY"           ]);
										Decimal dCOST_PRICE          = Sql.ToDecimal(row["COST_PRICE"         ]);
										Decimal dLIST_PRICE          = Sql.ToDecimal(row["LIST_PRICE"         ]);
										Decimal dUNIT_PRICE          = Sql.ToDecimal(row["UNIT_PRICE"         ]);
										string  sDESCRIPTION         = Sql.ToString (row["DESCRIPTION"        ]);
										// 08/13/2010 Paul.  New discount fields. 
										Guid    gDISCOUNT_ID         = Sql.ToGuid   (row["DISCOUNT_ID"        ]);
										Decimal dDISCOUNT_PRICE      = Sql.ToDecimal(row["DISCOUNT_PRICE"     ]);
										// 08/17/2010 Paul.  Add PRICING fields so that they can be customized per line item. 
										string  sPRICING_FORMULA     = Sql.ToString (row["PRICING_FORMULA"    ]);
										float   fPRICING_FACTOR      = Sql.ToFloat  (row["PRICING_FACTOR"     ]);
										// 12/13/2013 Paul.  Allow each line item to have a separate tax rate. 
										Guid    gTAXRATE_ID          = Sql.ToGuid   (row["TAXRATE_ID"         ]);

										// 03/27/2007 Paul.  Only add if product is defined.  This will exclude the blank row. 
										// 08/11/2007 Paul.  Allow an item to be manually added.  Require either a product ID or a name. 
										// 08/16/2010 Paul.  Add support for comments. 
										// 11/18/2010 Paul.  Reverse the name of this function. 
										if ( _controls.EditLineItemsView.IsLineItemNotEmpty(row) )
										{
											// 07/11/2010 Paul.  Add PARENT_TEMPLATE_ID. 
											// 07/15/2010 Paul.  Add GROUP_ID for options management. 
											// 08/13/2010 Paul.  Use LINE_GROUP_ID instead of GROUP_ID. 
											// 08/17/2010 Paul.  Add PRICING fields so that they can be customized per line item. 
											// 12/13/2013 Paul.  Allow each line item to have a separate tax rate. 
											SqlProcs.spORDERS_LINE_ITEMS_Update
												( ref gITEM_ID        
												, gORDER_ID           
												, gLINE_GROUP_ID      
												, sLINE_ITEM_TYPE     
												, nPOSITION           
												, sNAME               
												, sMFT_PART_NUM       
												, sVENDOR_PART_NUM    
												, gPRODUCT_TEMPLATE_ID
												, sTAX_CLASS          
												, nQUANTITY           
												, dCOST_PRICE         
												, dLIST_PRICE         
												, dUNIT_PRICE         
												, sDESCRIPTION        
												, gPARENT_TEMPLATE_ID 
												, gDISCOUNT_ID        
												, dDISCOUNT_PRICE     
												, sPRICING_FORMULA    
												, fPRICING_FACTOR     
												, gTAXRATE_ID         
												, trn
												);
											// 05/25/2008 Paul.  Line item custom fields need to be updated. 
											SplendidDynamic.UpdateCustomFields(row, trn, gITEM_ID, "ORDERS_LINE_ITEMS", dtOrdersCustomLineItems);
											nPOSITION++;
										}
									}
								}

								// 08/06/2009 Paul.  INVOICE_NUM now uses our number sequence table. 
								// 02/27/2015 Paul.  Add SHIP_DATE to sync with QuickBooks. 
								SqlProcs.spINVOICES_Update
									( ref gINVOICE_ID
									, new DynamicControl(this, "ASSIGNED_USER_ID"                ).ID
									, new DynamicControl(this, "NAME"                            ).Text
									, gQUOTE_ID
									, gORDER_ID
									, new DynamicControl(this, "OPPORTUNITY_ID"                  ).ID
									, "Due on Receipt"  // PAYMENT_TERMS
									, "Due"             // INVOICE_STAGE
									, new DynamicControl(this, "PURCHASE_ORDER_NUM"              ).Text
									, new DynamicControl(this, "DUE_DATE"                        ).DateValue
									, new DynamicControl(ctlEditLineItemsView, "EXCHANGE_RATE"   ).FloatValue
									, new DynamicControl(ctlEditLineItemsView, "CURRENCY_ID"     ).ID
									, new DynamicControl(ctlEditLineItemsView, "TAXRATE_ID"      ).ID
									, new DynamicControl(ctlEditLineItemsView, "SHIPPER_ID"      ).ID
									, new DynamicControl(ctlEditLineItemsView, "SUBTOTAL"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "DISCOUNT"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "SHIPPING"        ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "TAX"             ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "TOTAL"           ).DecimalValue
									, new DynamicControl(ctlEditLineItemsView, "AMOUNT_DUE"      ).DecimalValue
									, new DynamicControl(this, "BILLING_ACCOUNT_ID"              ).ID
									, new DynamicControl(this, "BILLING_CONTACT_ID"              ).ID
									, new DynamicControl(this, "BILLING_ADDRESS_STREET"          ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_CITY"            ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_STATE"           ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_POSTALCODE"      ).Text
									, new DynamicControl(this, "BILLING_ADDRESS_COUNTRY"         ).Text
									, new DynamicControl(this, "SHIPPING_ACCOUNT_ID"             ).ID
									, new DynamicControl(this, "SHIPPING_CONTACT_ID"             ).ID
									, new DynamicControl(this, "SHIPPING_ADDRESS_STREET"         ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_CITY"           ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_STATE"          ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_POSTALCODE"     ).Text
									, new DynamicControl(this, "SHIPPING_ADDRESS_COUNTRY"        ).Text
									, new DynamicControl(this, "DESCRIPTION"                     ).Text
									, new DynamicControl(this, "INVOICE_NUM"                     ).Text
									, new DynamicControl(this, "TEAM_ID"                         ).ID
									, new DynamicControl(this, "TEAM_SET_LIST"                   ).Text
									, new DynamicControl(this, "DUE_DATE"                        ).DateValue
									// 05/12/2016 Paul.  Add Tags module. 
									, String.Empty  // TAG_SET_NAME
									// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
									, new DynamicControl(this, "ASSIGNED_SET_LIST"               ).Text
									, trn
									);
								SplendidDynamic.UpdateCustomFields(this, trn, gORDER_ID, "INVOICES", dtInvoicesCustomFields);
								foreach ( DataRow row in dtLineItems.Rows )
								{
									if ( row.RowState == DataRowState.Deleted )
									{
										// 05/26/2007 Paul.  In order to access values from deleted row, use DataRowVersion.Original, 
										// otherwise accessing the data will throw an exception "Deleted row information cannot be accessed through the row."
										Guid gITEM_ID = Sql.ToGuid(row["ID", DataRowVersion.Original]);
										if ( !Sql.IsEmptyGuid(gITEM_ID) )
											SqlProcs.spINVOICES_LINE_ITEMS_Delete(gITEM_ID, trn);
									}
								}
								// 12/28/2007 Paul.  Renumber the position. 
								nPOSITION = 1;
								foreach ( DataRow row in dtLineItems.Rows )
								{
									if ( row.RowState != DataRowState.Deleted )
									{
										Guid    gITEM_ID             = Sql.ToGuid   (row["ID"                 ]);
										Guid    gLINE_GROUP_ID       = Sql.ToGuid   (row["LINE_GROUP_ID"      ]);
										string  sLINE_ITEM_TYPE      = Sql.ToString (row["LINE_ITEM_TYPE"     ]);
										//int     nPOSITION            = Sql.ToInteger(row["POSITION"           ]);
										string  sNAME                = Sql.ToString (row["NAME"               ]);
										string  sMFT_PART_NUM        = Sql.ToString (row["MFT_PART_NUM"       ]);
										string  sVENDOR_PART_NUM     = Sql.ToString (row["VENDOR_PART_NUM"    ]);
										Guid    gPRODUCT_TEMPLATE_ID = Sql.ToGuid   (row["PRODUCT_TEMPLATE_ID"]);
										// 07/11/2010 Paul.  Add PARENT_TEMPLATE_ID. 
										Guid    gPARENT_TEMPLATE_ID  = Sql.ToGuid   (row["PARENT_TEMPLATE_ID" ]);
										// 07/15/2010 Paul.  Add GROUP_ID for options management. 
										// 08/13/2010 Paul.  Use LINE_GROUP_ID instead of GROUP_ID. 
										//Guid    gGROUP_ID            = Sql.ToGuid   (row["GROUP_ID"           ]);
										string  sTAX_CLASS           = Sql.ToString (row["TAX_CLASS"          ]);
										// 02/10/2011 Paul.  Stop converting the Quantity to an integer. 
										float   nQUANTITY            = Sql.ToFloat  (row["QUANTITY"           ]);
										Decimal dCOST_PRICE          = Sql.ToDecimal(row["COST_PRICE"         ]);
										Decimal dLIST_PRICE          = Sql.ToDecimal(row["LIST_PRICE"         ]);
										Decimal dUNIT_PRICE          = Sql.ToDecimal(row["UNIT_PRICE"         ]);
										string  sDESCRIPTION         = Sql.ToString (row["DESCRIPTION"        ]);
										// 08/13/2010 Paul.  New discount fields. 
										Guid    gDISCOUNT_ID         = Sql.ToGuid   (row["DISCOUNT_ID"        ]);
										Decimal dDISCOUNT_PRICE      = Sql.ToDecimal(row["DISCOUNT_PRICE"     ]);
										// 08/17/2010 Paul.  Add PRICING fields so that they can be customized per line item. 
										string  sPRICING_FORMULA     = Sql.ToString (row["PRICING_FORMULA"    ]);
										float   fPRICING_FACTOR      = Sql.ToFloat  (row["PRICING_FACTOR"     ]);
										// 12/13/2013 Paul.  Allow each line item to have a separate tax rate. 
										Guid    gTAXRATE_ID          = Sql.ToGuid   (row["TAXRATE_ID"         ]);

										// 03/27/2007 Paul.  Only add if product is defined.  This will exclude the blank row. 
										// 08/11/2007 Paul.  Allow an item to be manually added.  Require either a product ID or a name. 
										// 08/16/2010 Paul.  Add support for comments. 
										// 11/18/2010 Paul.  Reverse the name of this function. 
										if ( _controls.EditLineItemsView.IsLineItemNotEmpty(row) )
										{
											// 07/11/2010 Paul.  Add PARENT_TEMPLATE_ID. 
											// 07/15/2010 Paul.  Add GROUP_ID for options management. 
											// 08/13/2010 Paul.  Use LINE_GROUP_ID instead of GROUP_ID. 
											// 08/17/2010 Paul.  Add PRICING fields so that they can be customized per line item. 
											// 12/13/2013 Paul.  Allow each line item to have a separate tax rate. 
											SqlProcs.spINVOICES_LINE_ITEMS_Update
												( ref gITEM_ID        
												, gINVOICE_ID         
												, gLINE_GROUP_ID      
												, sLINE_ITEM_TYPE     
												, nPOSITION           
												, sNAME               
												, sMFT_PART_NUM       
												, sVENDOR_PART_NUM    
												, gPRODUCT_TEMPLATE_ID
												, sTAX_CLASS          
												, nQUANTITY           
												, dCOST_PRICE         
												, dLIST_PRICE         
												, dUNIT_PRICE         
												, sDESCRIPTION        
												, gPARENT_TEMPLATE_ID 
												, gDISCOUNT_ID        
												, dDISCOUNT_PRICE     
												, sPRICING_FORMULA    
												, fPRICING_FACTOR     
												, gTAXRATE_ID         
												, trn
												);
											SplendidDynamic.UpdateCustomFields(row, trn, gITEM_ID, "INVOICES_LINE_ITEMS", dtInvoicesCustomLineItems);
											nPOSITION++;
										}
									}
								}
								// 04/23/2008 Paul.  Update the Amount Due. 
								SqlProcs.spINVOICES_UpdateAmountDue(gINVOICE_ID, trn);
								Decimal dPAYMENT_GROSS = new DynamicControl(ctlEditLineItemsView, "TOTAL").DecimalValue;
								// 08/06/2009 Paul.  PAYMENT_NUM now uses our number sequence table. 
								// 08/26/2010 Paul.  We need a bank fee field to allow for a difference between allocated and received payment. 
								// 05/07/2013 Paul.  Add Contacts field to support B2C. 
								Guid gB2C_CONTACT_ID = Guid.Empty;
								SqlProcs.spPAYMENTS_Update
									( ref gPAYMENT_ID
									, Guid.Empty      // ASSIGNED_USER_ID
									, gACCOUNT_ID
									, DateTime.Now    // PAYMENT_DATE
									, "PayPal"        // PAYMENT_TYPE
									, String.Empty    // CUSTOMER_REFERENCE
									, 1.0f            // EXCHANGE_RATE
									, new DynamicControl(ctlEditLineItemsView, "CURRENCY_ID").ID  // CURRENCY_ID
									, dPAYMENT_GROSS  // AMOUNT
									, String.Empty    // DESCRIPTION
									, Guid.Empty      // CREDIT_CARD_ID
									, String.Empty    // PAYMENT_NUM
									, Guid.Empty      // TEAM_ID
									, String.Empty    // TEAM_SET_LIST
									, Decimal.Zero    // BANK_FEE
									, gB2C_CONTACT_ID
									// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
									, String.Empty    // ASSIGNED_SET_LIST
									, trn
									);
								PAYMENT_ID.Value = gPAYMENT_ID.ToString();

								Guid gINVOICES_PAYMENT_ID = Guid.Empty;
								SqlProcs.spINVOICES_PAYMENTS_Update(ref gINVOICES_PAYMENT_ID, gINVOICE_ID, gPAYMENT_ID, dPAYMENT_GROSS, trn);
								if ( Sql.IsEmptyGuid(gPAYMENTS_TRANSACTION_ID) )
								{
									string sINVOICE_NUMBER = String.Empty;
									string sSQL;
									sSQL = "select INVOICE_NUM" + ControlChars.CrLf
									     + "  from vwINVOICES " + ControlChars.CrLf
									     + " where ID = @ID   " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.Transaction = trn;
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gINVOICE_ID);
										sINVOICE_NUMBER = Sql.ToString(cmd.ExecuteScalar());
									}
						
									SqlProcs.spPAYMENTS_TRANSACTIONS_InsertOnly
										( ref gPAYMENTS_TRANSACTION_ID
										, gPAYMENT_ID
										, "PayPal"           // PAYMENT_GATEWAY
										, "Sale"             // TRANSACTION_TYPE
										, dPAYMENT_GROSS
										, new DynamicControl(ctlEditLineItemsView, "CURRENCY_ID").ID
										, sINVOICE_NUMBER    // INVOICE_NUMBER
										, String.Empty       // DESCRIPTION
										, Guid.Empty         // CREDIT_CARD_ID
										, gACCOUNT_ID
										, "Pending"          // STATUS
										, trn
										);
									PAYMENTS_TRANSACTION_ID.Value = gPAYMENTS_TRANSACTION_ID.ToString();
									/*
									SqlProcs.spPAYMENTS_TRANSACTIONS_Update
										( gPAYMENTS_TRANSACTION_ID
										, sPAYMENT_STATUS    // STATUS
										, sTXN_ID            // TRANSACTION_NUMBER
										, String.Empty       // REFERENCE_NUMBER
										, String.Empty       // AUTHORIZATION_CODE
										, String.Empty       // AVS_CODE
										, sPENDING_REASON    // ERROR_CODE
										, sREASON_CODE       // ERROR_MESSAGE
										, trn
										);
									*/
								}
								trn.Commit();
							}
							catch(Exception ex)
							{
								trn.Rollback();
								SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
								ctlDynamicButtons.ErrorText = ex.Message;
								return;
							}
						}
					}
					Response.Redirect("view.aspx?ID=" + gORDER_ID.ToString());
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( !Sql.IsEmptyGuid(gPARENT_ID) )
					Response.Redirect("~/" + sMODULE + "/view.aspx?ID=" + gPARENT_ID.ToString());
				else if ( !Sql.IsEmptyGuid(gQUOTE_ID) )
					Response.Redirect("~/Quotes/view.aspx?ID=" + gQUOTE_ID.ToString());
				else
					Response.Redirect("default.aspx");
			}
		}

		private void UpdateAccount(Guid gACCOUNT_ID, bool bUpdateBilling, bool bUpdateShipping)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL ;
				sSQL = "select *         " + ControlChars.CrLf
				     + "  from vwACCOUNTS" + ControlChars.CrLf
				     + " where ID = @ID  " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", gACCOUNT_ID);
					con.Open();
					using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
					{
						if ( rdr.Read() )
						{
							new DynamicControl(this, "BUSINESS_NAME").Text = Sql.ToString(rdr["NAME"]);
							if ( bUpdateBilling )
							{
								new DynamicControl(this, "BILLING_ACCOUNT_ID"         ).ID   = Sql.ToGuid  (rdr["ID"                         ]);
								new DynamicControl(this, "BILLING_ACCOUNT_NAME"       ).Text = Sql.ToString(rdr["NAME"                       ]);
								new DynamicControl(this, "BILLING_ADDRESS_STREET"     ).Text = Sql.ToString(rdr["BILLING_ADDRESS_STREET"     ]);
								new DynamicControl(this, "BILLING_ADDRESS_CITY"       ).Text = Sql.ToString(rdr["BILLING_ADDRESS_CITY"       ]);
								new DynamicControl(this, "BILLING_ADDRESS_STATE"      ).Text = Sql.ToString(rdr["BILLING_ADDRESS_STATE"      ]);
								new DynamicControl(this, "BILLING_ADDRESS_POSTALCODE" ).Text = Sql.ToString(rdr["BILLING_ADDRESS_POSTALCODE" ]);
								new DynamicControl(this, "BILLING_ADDRESS_COUNTRY"    ).Text = Sql.ToString(rdr["BILLING_ADDRESS_COUNTRY"    ]);
							}
							if ( bUpdateShipping )
							{
								new DynamicControl(this, "SHIPPING_ACCOUNT_ID"        ).ID   = Sql.ToGuid  (rdr["ID"                         ]);
								new DynamicControl(this, "SHIPPING_ACCOUNT_NAME"      ).Text = Sql.ToString(rdr["NAME"                       ]);
								new DynamicControl(this, "SHIPPING_ADDRESS_STREET"    ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_STREET"    ]);
								new DynamicControl(this, "SHIPPING_ADDRESS_CITY"      ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_CITY"      ]);
								new DynamicControl(this, "SHIPPING_ADDRESS_STATE"     ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_STATE"     ]);
								new DynamicControl(this, "SHIPPING_ADDRESS_POSTALCODE").Text = Sql.ToString(rdr["SHIPPING_ADDRESS_POSTALCODE"]);
								new DynamicControl(this, "SHIPPING_ADDRESS_COUNTRY"   ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_COUNTRY"   ]);
							}
						}
					}
				}
			}
		}

		private void UpdateContact(Guid gCONTACT_ID, bool bUpdateBilling, bool bUpdateShipping)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL ;
				sSQL = "select *         " + ControlChars.CrLf
				     + "  from vwCONTACTS" + ControlChars.CrLf
				     + " where ID = @ID  " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", gCONTACT_ID);
					con.Open();
					using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
					{
						if ( rdr.Read() )
						{
							// 12/16/2007 Paul.  Fix retrieval of account id. 
							Guid gACCOUNT_ID = Sql.ToGuid(rdr["ACCOUNT_ID"]);
							if ( bUpdateBilling )
							{
								new DynamicControl(this, "BILLING_CONTACT_ID"   ).ID   = Sql.ToGuid  (rdr["ID"  ]);
								new DynamicControl(this, "BILLING_CONTACT_NAME" ).Text = Sql.ToString(rdr["NAME"]);
								// 08/21/2010 Paul.  Update address if the contact changes. 
								DynamicControl ctlBILLING_ACCOUNT_ID  = new DynamicControl(this, "BILLING_ACCOUNT_ID" );
								// 08/22/2010 Paul.  Only update account if empty. 
								if ( !Sql.IsEmptyGuid(gACCOUNT_ID) && Sql.IsEmptyGuid(ctlBILLING_ACCOUNT_ID.ID) )
								{
									//new DynamicControl(this, "BILLING_ACCOUNT_ID"         ).ID   = Sql.ToGuid  (rdr["ACCOUNT_ID"                 ]);
									//new DynamicControl(this, "BILLING_ACCOUNT_NAME"       ).Text = Sql.ToString(rdr["ACCOUNT_NAME"               ]);
									UpdateAccount(gACCOUNT_ID, true, false);
								}
								if ( !Sql.IsEmptyString(rdr["PRIMARY_ADDRESS_STREET"]) )
								{
									new DynamicControl(this, "BILLING_ADDRESS_STREET"     ).Text = Sql.ToString(rdr["PRIMARY_ADDRESS_STREET"     ]);
									new DynamicControl(this, "BILLING_ADDRESS_CITY"       ).Text = Sql.ToString(rdr["PRIMARY_ADDRESS_CITY"       ]);
									new DynamicControl(this, "BILLING_ADDRESS_STATE"      ).Text = Sql.ToString(rdr["PRIMARY_ADDRESS_STATE"      ]);
									new DynamicControl(this, "BILLING_ADDRESS_POSTALCODE" ).Text = Sql.ToString(rdr["PRIMARY_ADDRESS_POSTALCODE" ]);
									new DynamicControl(this, "BILLING_ADDRESS_COUNTRY"    ).Text = Sql.ToString(rdr["PRIMARY_ADDRESS_COUNTRY"    ]);
								}
							}
							if ( bUpdateShipping )
							{
								new DynamicControl(this, "SHIPPING_CONTACT_ID"  ).ID   = Sql.ToGuid  (rdr["ID"  ]);
								new DynamicControl(this, "SHIPPING_CONTACT_NAME").Text = Sql.ToString(rdr["NAME"]);
								// 08/21/2010 Paul.  Update address if the contact changes. 
								DynamicControl ctlSHIPPING_ACCOUNT_ID = new DynamicControl(this, "SHIPPING_ACCOUNT_ID");
								// 08/22/2010 Paul.  Only update account if empty. 
								if ( !Sql.IsEmptyGuid(gACCOUNT_ID) && Sql.IsEmptyGuid(ctlSHIPPING_ACCOUNT_ID.ID) )
								{
									//new DynamicControl(this, "SHIPPING_ACCOUNT_ID"        ).ID   = Sql.ToGuid  (rdr["ACCOUNT_ID"            ]);
									//new DynamicControl(this, "SHIPPING_ACCOUNT_NAME"      ).Text = Sql.ToString(rdr["ACCOUNT_NAME"          ]);
									UpdateAccount(gACCOUNT_ID, false, true);
								}
								if ( !Sql.IsEmptyString(rdr["ALT_ADDRESS_STREET"]) )
								{
									new DynamicControl(this, "SHIPPING_ADDRESS_STREET"    ).Text = Sql.ToString(rdr["ALT_ADDRESS_STREET"    ]);
									new DynamicControl(this, "SHIPPING_ADDRESS_CITY"      ).Text = Sql.ToString(rdr["ALT_ADDRESS_CITY"      ]);
									new DynamicControl(this, "SHIPPING_ADDRESS_STATE"     ).Text = Sql.ToString(rdr["ALT_ADDRESS_STATE"     ]);
									new DynamicControl(this, "SHIPPING_ADDRESS_POSTALCODE").Text = Sql.ToString(rdr["ALT_ADDRESS_POSTALCODE"]);
									new DynamicControl(this, "SHIPPING_ADDRESS_COUNTRY"   ).Text = Sql.ToString(rdr["ALT_ADDRESS_COUNTRY"   ]);
								}
							}
						}
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				if ( !IsPostBack )
				{
					string sLOAD_MODULE     = "Orders"  ;
					string sLOAD_MODULE_KEY = "ORDER_ID";
					this.AppendEditViewFields(m_sMODULE + ".EntryView"       , tblMain       , null);
					// 09/02/2012 Paul.  EditViews were combined into a single view. 
					//this.AppendEditViewFields(m_sMODULE + ".EntryCredit"     , tblCreditCard , null);
					//this.AppendEditViewFields(m_sMODULE + ".EntryAddress"    , tblAddress    , null);
					this.AppendEditViewFields(m_sMODULE + ".EntryDescription", tblDescription, null);
					// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
					ctlDynamicButtons.AppendButtons(m_sMODULE + ".EntryView", Guid.Empty, null);
					ctlFooterButtons .AppendButtons(m_sMODULE + ".EntryView", Guid.Empty, null);

					// 08/10/2007 Paul.  Default to today. 
					new DynamicControl(this, "ORDER_DATE").DateValue = T10n.FromServerTime(DateTime.Now);

					// 06/08/2006 Paul.  Prepopulate the Account. 
					Guid gPARENT_ID = Sql.ToGuid(Request["PARENT_ID"]);
					if ( !Sql.IsEmptyGuid(gPARENT_ID) )
					{
						// 04/14/2016 Paul.  New spPARENT_GetWithTeam procedure so that we can inherit Assigned To and Team values. 
						string sMODULE           = String.Empty;
						string sPARENT_TYPE      = String.Empty;
						string sPARENT_NAME      = String.Empty;
						Guid   gASSIGNED_USER_ID = Guid.Empty;
						string sASSIGNED_TO      = String.Empty;
						string sASSIGNED_TO_NAME = String.Empty;
						Guid   gTEAM_ID          = Guid.Empty;
						string sTEAM_NAME        = String.Empty;
						Guid   gTEAM_SET_ID      = Guid.Empty;
						// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
							Guid   gASSIGNED_SET_ID  = Guid.Empty;
						SqlProcs.spPARENT_GetWithTeam(ref gPARENT_ID, ref sMODULE, ref sPARENT_TYPE, ref sPARENT_NAME, ref gASSIGNED_USER_ID, ref sASSIGNED_TO, ref sASSIGNED_TO_NAME, ref gTEAM_ID, ref sTEAM_NAME, ref gTEAM_SET_ID, ref gASSIGNED_SET_ID);
						if ( !Sql.IsEmptyGuid(gPARENT_ID) && sMODULE == "Accounts" )
						{
							UpdateAccount(gPARENT_ID, true, true);
							// 04/14/2016 Paul.  New spPARENT_GetWithTeam procedure so that we can inherit Assigned To and Team values. 
							if ( Sql.ToBoolean(Application["CONFIG.inherit_assigned_user"]) )
							{
								new DynamicControl(this, "ASSIGNED_USER_ID").ID   = gASSIGNED_USER_ID;
								new DynamicControl(this, "ASSIGNED_TO"     ).Text = sASSIGNED_TO     ;
								new DynamicControl(this, "ASSIGNED_TO_NAME").Text = sASSIGNED_TO_NAME;
								// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
								if ( Crm.Config.enable_dynamic_assignment() )
								{
									SplendidCRM._controls.UserSelect ctlUserSelect = FindControl("ASSIGNED_SET_NAME") as SplendidCRM._controls.UserSelect;
									if ( ctlUserSelect != null )
										ctlUserSelect.LoadLineItems(gASSIGNED_SET_ID, true, true);
								}
							}
							if ( Sql.ToBoolean(Application["CONFIG.inherit_team"]) )
							{
								new DynamicControl(this, "TEAM_ID"  ).ID   = gTEAM_ID  ;
								new DynamicControl(this, "TEAM_NAME").Text = sTEAM_NAME;
								SplendidCRM._controls.TeamSelect ctlTeamSelect = FindControl("TEAM_SET_NAME") as SplendidCRM._controls.TeamSelect;
								if ( ctlTeamSelect != null )
									ctlTeamSelect.LoadLineItems(gTEAM_SET_ID, true, true);
							}
						}
						if ( !Sql.IsEmptyGuid(gPARENT_ID) && sMODULE == "Contacts" )
						{
							UpdateContact(gPARENT_ID, true, true);
							// 04/14/2016 Paul.  New spPARENT_GetWithTeam procedure so that we can inherit Assigned To and Team values. 
							if ( Sql.ToBoolean(Application["CONFIG.inherit_assigned_user"]) )
							{
								new DynamicControl(this, "ASSIGNED_USER_ID").ID   = gASSIGNED_USER_ID;
								new DynamicControl(this, "ASSIGNED_TO"     ).Text = sASSIGNED_TO     ;
								new DynamicControl(this, "ASSIGNED_TO_NAME").Text = sASSIGNED_TO_NAME;
								// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
								if ( Crm.Config.enable_dynamic_assignment() )
								{
									SplendidCRM._controls.UserSelect ctlUserSelect = FindControl("ASSIGNED_SET_NAME") as SplendidCRM._controls.UserSelect;
									if ( ctlUserSelect != null )
										ctlUserSelect.LoadLineItems(gASSIGNED_SET_ID, true, true);
								}
							}
							if ( Sql.ToBoolean(Application["CONFIG.inherit_team"]) )
							{
								new DynamicControl(this, "TEAM_ID"  ).ID   = gTEAM_ID  ;
								new DynamicControl(this, "TEAM_NAME").Text = sTEAM_NAME;
								SplendidCRM._controls.TeamSelect ctlTeamSelect = FindControl("TEAM_SET_NAME") as SplendidCRM._controls.TeamSelect;
								if ( ctlTeamSelect != null )
									ctlTeamSelect.LoadLineItems(gTEAM_SET_ID, true, true);
							}
						}
						else if ( !Sql.IsEmptyGuid(gPARENT_ID) && sMODULE == "Opportunities" )
						{
							new DynamicControl(this, "OPPORTUNITY_ID"  ).ID   = gPARENT_ID;
							new DynamicControl(this, "OPPORTUNITY_NAME").Text = sPARENT_NAME;
							// 04/14/2016 Paul.  New spPARENT_GetWithTeam procedure so that we can inherit Assigned To and Team values. 
							if ( Sql.ToBoolean(Application["CONFIG.inherit_assigned_user"]) )
							{
								new DynamicControl(this, "ASSIGNED_USER_ID").ID   = gASSIGNED_USER_ID;
								new DynamicControl(this, "ASSIGNED_TO"     ).Text = sASSIGNED_TO     ;
								new DynamicControl(this, "ASSIGNED_TO_NAME").Text = sASSIGNED_TO_NAME;
								// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
								if ( Crm.Config.enable_dynamic_assignment() )
								{
									SplendidCRM._controls.UserSelect ctlUserSelect = FindControl("ASSIGNED_SET_NAME") as SplendidCRM._controls.UserSelect;
									if ( ctlUserSelect != null )
										ctlUserSelect.LoadLineItems(gASSIGNED_SET_ID, true, true);
								}
							}
							if ( Sql.ToBoolean(Application["CONFIG.inherit_team"]) )
							{
								new DynamicControl(this, "TEAM_ID"  ).ID   = gTEAM_ID  ;
								new DynamicControl(this, "TEAM_NAME").Text = sTEAM_NAME;
								SplendidCRM._controls.TeamSelect ctlTeamSelect = FindControl("TEAM_SET_NAME") as SplendidCRM._controls.TeamSelect;
								if ( ctlTeamSelect != null )
									ctlTeamSelect.LoadLineItems(gTEAM_SET_ID, true, true);
							}
						}
					}
					ctlEditLineItemsView.LoadLineItems(Guid.Empty, Guid.Empty, null, null, sLOAD_MODULE, sLOAD_MODULE_KEY);
				}
				else
				{
					// 12/02/2005 Paul.  When validation fails, the header title does not retain its value.  Update manually. 
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					Utils.SetPageTitle(Page, L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);

					DynamicControl ctlBILLING_ACCOUNT_ID  = new DynamicControl(this, "BILLING_ACCOUNT_ID" );
					DynamicControl ctlSHIPPING_ACCOUNT_ID = new DynamicControl(this, "SHIPPING_ACCOUNT_ID");
					DynamicControl ctlBILLING_CONTACT_ID  = new DynamicControl(this, "BILLING_CONTACT_ID");
					if ( Sql.ToGuid(ViewState["BILLING_ACCOUNT_ID" ]) != ctlBILLING_ACCOUNT_ID.ID )
					{
						UpdateAccount(ctlBILLING_ACCOUNT_ID.ID, true, true);
						ViewState["BILLING_ACCOUNT_ID" ] = ctlBILLING_ACCOUNT_ID.ID;
						ViewState["SHIPPING_ACCOUNT_ID"] = ctlBILLING_ACCOUNT_ID.ID;
					}
					if ( Sql.ToGuid(ViewState["SHIPPING_ACCOUNT_ID"]) != ctlSHIPPING_ACCOUNT_ID.ID )
					{
						UpdateAccount(ctlSHIPPING_ACCOUNT_ID.ID, false, true);
						ViewState["SHIPPING_ACCOUNT_ID"] = ctlSHIPPING_ACCOUNT_ID.ID;
					}
					if ( Sql.ToGuid(ViewState["BILLING_CONTACT_ID"]) != ctlBILLING_CONTACT_ID.ID )
					{
						UpdateContact(ctlBILLING_CONTACT_ID.ID, true, true);
						ViewState["BILLING_CONTACT_ID"] = ctlBILLING_CONTACT_ID.ID;
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Orders";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				// 12/02/2005 Paul.  Need to add the edit fields in order for events to fire. 
				this.AppendEditViewFields(m_sMODULE + ".EntryView"       , tblMain       , null);
				// 09/02/2012 Paul.  EditViews were combined into a single view. 
				//this.AppendEditViewFields(m_sMODULE + ".EntryCredit"     , tblCreditCard , null);
				//this.AppendEditViewFields(m_sMODULE + ".EntryAddress"    , tblAddress    , null);
				this.AppendEditViewFields(m_sMODULE + ".EntryDescription", tblDescription, null);
				// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
				ctlDynamicButtons.AppendButtons(m_sMODULE + ".EntryView", Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + ".EntryView", Guid.Empty, null);
			}
		}
		#endregion
	}
}
