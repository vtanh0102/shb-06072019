/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Orders.QuickBooks
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sQID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
						if ( bQuickBooksEnabled )
						{
							QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									string sSQL;
									if ( Sql.IsEmptyString(sQID) )
									{
										sSQL = "insert into SalesOrders  " + ControlChars.CrLf
										     + "     ( ReferenceNumber   " + ControlChars.CrLf
										     + "     , CustomerName      " + ControlChars.CrLf
										     + "     , CustomerId        " + ControlChars.CrLf
										     + "     , Date              " + ControlChars.CrLf
										     + "     , DueDate           " + ControlChars.CrLf
										     + "     , ShipDate          " + ControlChars.CrLf
										     + "     , ShipMethod        " + ControlChars.CrLf
										     + "     , ShipMethodId      " + ControlChars.CrLf
										     + "     , Memo              " + ControlChars.CrLf
										     + "     , POnumber          " + ControlChars.CrLf
										     + "     , Terms             " + ControlChars.CrLf
										     + "     , ExchangeRate      " + ControlChars.CrLf
										     + "     , Subtotal          " + ControlChars.CrLf
										     + "     , Tax               " + ControlChars.CrLf
										     + "     , TotalAmount       " + ControlChars.CrLf
										     + "     , BillingLine1      " + ControlChars.CrLf
										     + "     , BillingLine2      " + ControlChars.CrLf
										     + "     , BillingLine3      " + ControlChars.CrLf
										     + "     , BillingLine4      " + ControlChars.CrLf
										     + "     , BillingLine5      " + ControlChars.CrLf
										     + "     , BillingCity       " + ControlChars.CrLf
										     + "     , BillingState      " + ControlChars.CrLf
										     + "     , BillingPostalCode " + ControlChars.CrLf
										     + "     , BillingCountry    " + ControlChars.CrLf
										     + "     , ShippingLine1     " + ControlChars.CrLf
										     + "     , ShippingLine2     " + ControlChars.CrLf
										     + "     , ShippingLine3     " + ControlChars.CrLf
										     + "     , ShippingLine4     " + ControlChars.CrLf
										     + "     , ShippingLine5     " + ControlChars.CrLf
										     + "     , ShippingCity      " + ControlChars.CrLf
										     + "     , ShippingState     " + ControlChars.CrLf
										     + "     , ShippingPostalCode" + ControlChars.CrLf
										     + "     , ShippingCountry   " + ControlChars.CrLf
										     + "     , ItemAggregate     " + ControlChars.CrLf
										     + "     )" + ControlChars.CrLf
										     + "values" + ControlChars.CrLf
										     + "     ( @ReferenceNumber   " + ControlChars.CrLf
										     + "     , @CustomerName      " + ControlChars.CrLf
										     + "     , @CustomerId        " + ControlChars.CrLf
										     + "     , @Date              " + ControlChars.CrLf
										     + "     , @DueDate           " + ControlChars.CrLf
										     + "     , @ShipDate          " + ControlChars.CrLf
										     + "     , @ShipMethod        " + ControlChars.CrLf
										     + "     , @ShipMethodId      " + ControlChars.CrLf
										     + "     , @Memo              " + ControlChars.CrLf
										     + "     , @POnumber          " + ControlChars.CrLf
										     + "     , @Terms             " + ControlChars.CrLf
										     + "     , @ExchangeRate      " + ControlChars.CrLf
										     + "     , @Subtotal          " + ControlChars.CrLf
										     + "     , @Tax               " + ControlChars.CrLf
										     + "     , @TotalAmount       " + ControlChars.CrLf
										     + "     , @BillingLine1      " + ControlChars.CrLf
										     + "     , @BillingLine2      " + ControlChars.CrLf
										     + "     , @BillingLine3      " + ControlChars.CrLf
										     + "     , @BillingLine4      " + ControlChars.CrLf
										     + "     , @BillingLine5      " + ControlChars.CrLf
										     + "     , @BillingCity       " + ControlChars.CrLf
										     + "     , @BillingState      " + ControlChars.CrLf
										     + "     , @BillingPostalCode " + ControlChars.CrLf
										     + "     , @BillingCountry    " + ControlChars.CrLf
										     + "     , @ShippingLine1     " + ControlChars.CrLf
										     + "     , @ShippingLine2     " + ControlChars.CrLf
										     + "     , @ShippingLine3     " + ControlChars.CrLf
										     + "     , @ShippingLine4     " + ControlChars.CrLf
										     + "     , @ShippingLine5     " + ControlChars.CrLf
										     + "     , @ShippingCity      " + ControlChars.CrLf
										     + "     , @ShippingState     " + ControlChars.CrLf
										     + "     , @ShippingPostalCode" + ControlChars.CrLf
										     + "     , @ShippingCountry   " + ControlChars.CrLf
										     + "     , @ItemAggregate     " + ControlChars.CrLf
										     + "     ) " + ControlChars.CrLf;
									}
									else
									{
										sSQL = "update SalesOrders                             " + ControlChars.CrLf
										     + "   set ReferenceNumber    = @ReferenceNumber   " + ControlChars.CrLf
										     + "     , CustomerName       = @CustomerName      " + ControlChars.CrLf
										     + "     , CustomerId         = @CustomerId        " + ControlChars.CrLf
										     + "     , Date               = @Date              " + ControlChars.CrLf
										     + "     , DueDate            = @DueDate           " + ControlChars.CrLf
										     + "     , ShipDate           = @ShipDate          " + ControlChars.CrLf
										     + "     , ShipMethod         = @ShipMethod        " + ControlChars.CrLf
										     + "     , ShipMethodId       = @ShipMethodId      " + ControlChars.CrLf
										     + "     , Memo               = @Memo              " + ControlChars.CrLf
										     + "     , POnumber           = @POnumber          " + ControlChars.CrLf
										     + "     , Terms              = @Terms             " + ControlChars.CrLf
										     + "     , ExchangeRate       = @ExchangeRate      " + ControlChars.CrLf
										     + "     , Subtotal           = @Subtotal          " + ControlChars.CrLf
										     + "     , Tax                = @Tax               " + ControlChars.CrLf
										     + "     , TotalAmount        = @TotalAmount       " + ControlChars.CrLf
										     + "     , BillingLine1       = @BillingLine1      " + ControlChars.CrLf
										     + "     , BillingLine2       = @BillingLine2      " + ControlChars.CrLf
										     + "     , BillingLine3       = @BillingLine3      " + ControlChars.CrLf
										     + "     , BillingLine4       = @BillingLine4      " + ControlChars.CrLf
										     + "     , BillingLine5       = @BillingLine5      " + ControlChars.CrLf
										     + "     , BillingCity        = @BillingCity       " + ControlChars.CrLf
										     + "     , BillingState       = @BillingState      " + ControlChars.CrLf
										     + "     , BillingPostalCode  = @BillingPostalCode " + ControlChars.CrLf
										     + "     , BillingCountry     = @BillingCountry    " + ControlChars.CrLf
										     + "     , ShippingLine1      = @ShippingLine1     " + ControlChars.CrLf
										     + "     , ShippingLine2      = @ShippingLine2     " + ControlChars.CrLf
										     + "     , ShippingLine3      = @ShippingLine3     " + ControlChars.CrLf
										     + "     , ShippingLine4      = @ShippingLine4     " + ControlChars.CrLf
										     + "     , ShippingLine5      = @ShippingLine5     " + ControlChars.CrLf
										     + "     , ShippingCity       = @ShippingCity      " + ControlChars.CrLf
										     + "     , ShippingState      = @ShippingState     " + ControlChars.CrLf
										     + "     , ShippingPostalCode = @ShippingPostalCode" + ControlChars.CrLf
										     + "     , ShippingCountry    = @ShippingCountry   " + ControlChars.CrLf
										     + " where ID                 = @ID                " + ControlChars.CrLf;
									}
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@ReferenceNumber"   , new DynamicControl(this, "ReferenceNumber"   ).Text,   11);
									Sql.AddParameter(cmd, "@CustomerName"      , new DynamicControl(this, "CustomerName"      ).Text,   41);
									Sql.AddParameter(cmd, "@CustomerId"        , new DynamicControl(this, "CustomerId"        ).Text,   41);
									Sql.AddParameter(cmd, "@Date"              , new DynamicControl(this, "Date"              ).DateValue);
									Sql.AddParameter(cmd, "@DueDate"           , new DynamicControl(this, "DueDate"           ).DateValue);
									Sql.AddParameter(cmd, "@ShipDate"          , new DynamicControl(this, "ShipDate"          ).DateValue);
									Sql.AddParameter(cmd, "@ShipMethod"        , new DynamicControl(this, "ShipMethod"        ).Text,   15);
									Sql.AddParameter(cmd, "@ShipMethodId"      , new DynamicControl(this, "ShipMethodId"      ).Text,   15);
									Sql.AddParameter(cmd, "@Memo"              , new DynamicControl(this, "Memo"              ).Text, 4095);
									Sql.AddParameter(cmd, "@POnumber"          , new DynamicControl(this, "POnumber"          ).Text,   25);
									Sql.AddParameter(cmd, "@Terms"             , new DynamicControl(this, "Terms"             ).Text,   21);
									Sql.AddParameter(cmd, "@ExchangeRate"      , new DynamicControl(this, "ExchangeRate"      ).FloatValue);
									Sql.AddParameter(cmd, "@Subtotal"          , new DynamicControl(this, "Subtotal"          ).FloatValue);
									Sql.AddParameter(cmd, "@Tax"               , new DynamicControl(this, "Tax"               ).FloatValue);
									Sql.AddParameter(cmd, "@TotalAmount"       , new DynamicControl(this, "TotalAmount"       ).FloatValue);
									Sql.AddParameter(cmd, "@BillingLine1"      , new DynamicControl(this, "BillingLine1"      ).Text, 41);
									Sql.AddParameter(cmd, "@BillingLine2"      , new DynamicControl(this, "BillingLine2"      ).Text, 41);
									Sql.AddParameter(cmd, "@BillingLine3"      , new DynamicControl(this, "BillingLine3"      ).Text, 41);
									Sql.AddParameter(cmd, "@BillingLine4"      , new DynamicControl(this, "BillingLine4"      ).Text, 41);
									Sql.AddParameter(cmd, "@BillingLine5"      , new DynamicControl(this, "BillingLine5"      ).Text, 41);
									Sql.AddParameter(cmd, "@BillingCity"       , new DynamicControl(this, "BillingCity"       ).Text, 31);
									Sql.AddParameter(cmd, "@BillingState"      , new DynamicControl(this, "BillingState"      ).Text, 21);
									Sql.AddParameter(cmd, "@BillingPostalCode" , new DynamicControl(this, "BillingPostalCode" ).Text, 13);
									Sql.AddParameter(cmd, "@BillingCountry"    , new DynamicControl(this, "BillingCountry"    ).Text, 31);
									Sql.AddParameter(cmd, "@ShippingLine1"     , new DynamicControl(this, "ShippingLine1"     ).Text, 41);
									Sql.AddParameter(cmd, "@ShippingLine2"     , new DynamicControl(this, "ShippingLine2"     ).Text, 41);
									Sql.AddParameter(cmd, "@ShippingLine3"     , new DynamicControl(this, "ShippingLine3"     ).Text, 41);
									Sql.AddParameter(cmd, "@ShippingLine4"     , new DynamicControl(this, "ShippingLine4"     ).Text, 41);
									Sql.AddParameter(cmd, "@ShippingLine5"     , new DynamicControl(this, "ShippingLine5"     ).Text, 41);
									Sql.AddParameter(cmd, "@ShippingCity"      , new DynamicControl(this, "ShippingCity"      ).Text, 31);
									Sql.AddParameter(cmd, "@ShippingState"     , new DynamicControl(this, "ShippingState"     ).Text, 21);
									Sql.AddParameter(cmd, "@ShippingPostalCode", new DynamicControl(this, "ShippingPostalCode").Text, 13);
									Sql.AddParameter(cmd, "@ShippingCountry"   , new DynamicControl(this, "ShippingCountry"   ).Text, 31);
								
									if ( !Sql.IsEmptyString(sQID) )
									{
										//Sql.AddParameter(cmd, "@ItemAggregate"     , new DynamicControl(this, "ItemAggregate"     GetItemAggregate(dbf, con, gLOCAL_ID, vwItems));
										Sql.AddParameter(cmd, "@ID", sQID);
									}
									cmd.ExecuteNonQuery();
									if ( !Sql.IsEmptyString(sQID) )
									{
										Hashtable result = dbf.GetLastResult(con);
										sQID = Sql.ToString(result["id"]);
									}
								}
							}
							Response.Redirect("view.aspx?QID=" + sQID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sQID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?QID=" + sQID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.USER_ID == Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]));
			if ( !this.Visible )
				return;

			try
			{
				sQID = Sql.ToString(Request["QID"]);
				if ( !IsPostBack )
				{
					string sDuplicateQID = Sql.ToString(Request["DuplicateQID"]);
					if ( !Sql.IsEmptyString(sQID) || !Sql.IsEmptyString(sDuplicateQID) )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
						// 10/25/2015 Paul.  Orders are not supported on Quickbooks Online. 
						if ( bQuickBooksEnabled && !QuickBooksSync.IsOnlineAppMode(Context.Application) )
						{
							QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								string sSQL ;
								sSQL = "select *          " + ControlChars.CrLf
								     + "  from SalesOrders" + ControlChars.CrLf
								     + " where ID = @ID   " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									if ( !Sql.IsEmptyString(sDuplicateQID) )
									{
										Sql.AddParameter(cmd, "@ID", sDuplicateQID);
										sQID = String.Empty;
									}
									else
									{
										Sql.AddParameter(cmd, "@ID", sQID);
									}
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										using ( DataTable dtCurrent = new DataTable() )
										{
											da.Fill(dtCurrent);
											if ( dtCurrent.Rows.Count > 0 )
											{
												DataRow rdr = dtCurrent.Rows[0];
												//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											
												// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
												ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
												SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
												ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
											
												this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
												ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
												ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
												TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
												if ( txtNAME != null )
													txtNAME.Focus();
											
												//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											}
											else
											{
												ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
												ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
												ctlDynamicButtons.DisableAll();
												ctlFooterButtons .DisableAll();
												ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
												plcSubPanel.Visible = false;
											}
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Orders";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.IsEmptyString(Request["QID"]);
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			// 02/13/2015 Paul.  New QuickBooks Online code uses a different schema. 
			this.LayoutEditView = "EditView.QuickBooks" + (QuickBooksSync.IsOnlineAppMode(Application) ? "Online" : String.Empty);
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

