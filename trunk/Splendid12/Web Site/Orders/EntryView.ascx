<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EntryView.ascx.cs" Inherits="SplendidCRM.Orders.EntryView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2009-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */

</script>
<SplendidCRM:InlineScript runat="server">
<script type="text/javascript">
// 09/03/2012 Paul.  Must place within InlineScript to prevent error. The Controls collection cannot be modified because the control contains code blocks (i.e. ).
function BillingChangeAccount(sPARENT_ID, sPARENT_NAME)
{
	document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BILLING_ACCOUNT_ID"  ).ClientID %>').value = sPARENT_ID  ;
	document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BILLING_ACCOUNT_NAME").ClientID %>').value = sPARENT_NAME;
	document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BUSINESS_NAME"       ).ClientID %>').value = sPARENT_NAME;
	document.forms[0].submit();
}
function BillingAccountPopup()
{
	// 08/29/2009 Paul.  Keep the original change as it also updates BUSINESS_NAME. 
	ChangeAccount = BillingChangeAccount;
	window.open('../Accounts/Popup.aspx', 'AccountPopup', '<%= SplendidCRM.Crm.Config.PopupWindowOptions() %>');
	//return ModulePopup('Accounts', '<%= new SplendidCRM.DynamicControl(this, "BILLING_ACCOUNT_ID").ClientID %>', '<%= new SplendidCRM.DynamicControl(this, "BILLING_ACCOUNT_NAME").ClientID %>', null, true, null);
	return false;
}
function ShippingAccountPopup()
{
	return ModulePopup('Accounts', '<%= new SplendidCRM.DynamicControl(this, "SHIPPING_ACCOUNT_ID").ClientID %>', '<%= new SplendidCRM.DynamicControl(this, "SHIPPING_ACCOUNT_NAME").ClientID %>', null, true, null);
}
function BillingChangeContact(sPARENT_ID, sPARENT_NAME)
{
	document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BILLING_CONTACT_ID"  ).ClientID %>').value = sPARENT_ID  ;
	document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BILLING_CONTACT_NAME").ClientID %>').value = sPARENT_NAME;
	document.forms[0].submit();
}
function BillingContactPopup()
{
	// 09/01/2009 Paul.  Keep the original change as it also updates BUSINESS_NAME. 
	ChangeContact = BillingChangeContact;
	var sACCOUNT_NAME = document.getElementById('<%= new SplendidCRM.DynamicControl(this, "BILLING_ACCOUNT_NAME").ClientID %>').value;
	//return ModulePopup('Contacts', '<%= new SplendidCRM.DynamicControl(this, "BILLING_CONTACT_ID").ClientID %>', '<%= new SplendidCRM.DynamicControl(this, "BILLING_CONTACT_NAME").ClientID %>', 'ACCOUNT_NAME=' + escape(sACCOUNT_NAME), false, null);
	return window.open('../Contacts/Popup.aspx?ACCOUNT_NAME=' + escape(sACCOUNT_NAME), 'ContactPopup', '<%= SplendidCRM.Crm.Config.PopupWindowOptions() %>');
}
function ShippingContactPopup()
{
	var sACCOUNT_NAME = document.getElementById('<%= new SplendidCRM.DynamicControl(this, "SHIPPING_ACCOUNT_NAME").ClientID %>').value;
	return ModulePopup('Contacts', '<%= new SplendidCRM.DynamicControl(this, "SHIPPING_CONTACT_ID").ClientID %>', '<%= new SplendidCRM.DynamicControl(this, "SHIPPING_CONTACT_NAME").ClientID %>', 'ACCOUNT_NAME=' + escape(sACCOUNT_NAME), false, null);
}
</script>
</SplendidCRM:InlineScript>
<div id="divEditView" runat="server">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Orders" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:HiddenField ID="ORDER_ID"                runat="server" />
	<asp:HiddenField ID="INVOICE_ID"              runat="server" />
	<asp:HiddenField ID="PAYMENT_ID"              runat="server" />
	<asp:HiddenField ID="PAYMENTS_TRANSACTION_ID" runat="server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblMain" class="tabEditView" runat="server">
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<%@ Register TagPrefix="SplendidCRM" Tagname="EditLineItemsView" Src="~/_controls/EditLineItemsView.ascx" %>
	<SplendidCRM:EditLineItemsView ID="ctlEditLineItemsView" MODULE="Orders" MODULE_KEY="ORDER_ID" Runat="Server" />

	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblDescription" class="tabEditView" runat="server">
					<tr>
						<th colspan="2"><h4><asp:Label Text='<%# L10n.Term("Orders.LBL_DESCRIPTION_TITLE") %>' runat="server" /></h4></th>
					</tr>
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>
