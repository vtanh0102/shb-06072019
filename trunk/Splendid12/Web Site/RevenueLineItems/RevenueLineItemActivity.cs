/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// RevenueLineItemActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class RevenueLineItemActivity: SplendidActivity
	{
		public RevenueLineItemActivity()
		{
			this.Name = "RevenueLineItemActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.OPPORTUNITY_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.OPPORTUNITY_IDProperty, value); }
		}

		public static DependencyProperty LINE_GROUP_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LINE_GROUP_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LINE_GROUP_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.LINE_GROUP_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.LINE_GROUP_IDProperty, value); }
		}

		public static DependencyProperty LINE_ITEM_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LINE_ITEM_TYPE", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LINE_ITEM_TYPE
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.LINE_ITEM_TYPEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.LINE_ITEM_TYPEProperty, value); }
		}

		public static DependencyProperty POSITIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("POSITION", typeof(Int32), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 POSITION
		{
			get { return ((Int32)(base.GetValue(RevenueLineItemActivity.POSITIONProperty))); }
			set { base.SetValue(RevenueLineItemActivity.POSITIONProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.NAMEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.NAMEProperty, value); }
		}

		public static DependencyProperty MFT_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MFT_PART_NUM", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MFT_PART_NUM
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.MFT_PART_NUMProperty))); }
			set { base.SetValue(RevenueLineItemActivity.MFT_PART_NUMProperty, value); }
		}

		public static DependencyProperty VENDOR_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("VENDOR_PART_NUM", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string VENDOR_PART_NUM
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.VENDOR_PART_NUMProperty))); }
			set { base.SetValue(RevenueLineItemActivity.VENDOR_PART_NUMProperty, value); }
		}

		public static DependencyProperty PRODUCT_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRODUCT_TEMPLATE_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PRODUCT_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.PRODUCT_TEMPLATE_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.PRODUCT_TEMPLATE_IDProperty, value); }
		}

		public static DependencyProperty TAX_CLASSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX_CLASS", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAX_CLASS
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.TAX_CLASSProperty))); }
			set { base.SetValue(RevenueLineItemActivity.TAX_CLASSProperty, value); }
		}

		public static DependencyProperty QUANTITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUANTITY", typeof(float), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float QUANTITY
		{
			get { return ((float)(base.GetValue(RevenueLineItemActivity.QUANTITYProperty))); }
			set { base.SetValue(RevenueLineItemActivity.QUANTITYProperty, value); }
		}

		public static DependencyProperty COST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COST_PRICE", typeof(decimal), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal COST_PRICE
		{
			get { return ((decimal)(base.GetValue(RevenueLineItemActivity.COST_PRICEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.COST_PRICEProperty, value); }
		}

		public static DependencyProperty LIST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_PRICE", typeof(decimal), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal LIST_PRICE
		{
			get { return ((decimal)(base.GetValue(RevenueLineItemActivity.LIST_PRICEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.LIST_PRICEProperty, value); }
		}

		public static DependencyProperty UNIT_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("UNIT_PRICE", typeof(decimal), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal UNIT_PRICE
		{
			get { return ((decimal)(base.GetValue(RevenueLineItemActivity.UNIT_PRICEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.UNIT_PRICEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(RevenueLineItemActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TEMPLATE_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.PARENT_TEMPLATE_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.PARENT_TEMPLATE_IDProperty, value); }
		}

		public static DependencyProperty DISCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid DISCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.DISCOUNT_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.DISCOUNT_IDProperty, value); }
		}

		public static DependencyProperty DISCOUNT_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_PRICE", typeof(decimal), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_PRICE
		{
			get { return ((decimal)(base.GetValue(RevenueLineItemActivity.DISCOUNT_PRICEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.DISCOUNT_PRICEProperty, value); }
		}

		public static DependencyProperty PRICING_FORMULAProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FORMULA", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRICING_FORMULA
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.PRICING_FORMULAProperty))); }
			set { base.SetValue(RevenueLineItemActivity.PRICING_FORMULAProperty, value); }
		}

		public static DependencyProperty PRICING_FACTORProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FACTOR", typeof(float), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float PRICING_FACTOR
		{
			get { return ((float)(base.GetValue(RevenueLineItemActivity.PRICING_FACTORProperty))); }
			set { base.SetValue(RevenueLineItemActivity.PRICING_FACTORProperty, value); }
		}

		public static DependencyProperty TAXRATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_ID", typeof(Guid), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TAXRATE_ID
		{
			get { return ((Guid)(base.GetValue(RevenueLineItemActivity.TAXRATE_IDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.TAXRATE_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_TYPE", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OPPORTUNITY_TYPE
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.OPPORTUNITY_TYPEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.OPPORTUNITY_TYPEProperty, value); }
		}

		public static DependencyProperty LEAD_SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_SOURCE", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_SOURCE
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.LEAD_SOURCEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.LEAD_SOURCEProperty, value); }
		}

		public static DependencyProperty DATE_CLOSEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_CLOSED", typeof(DateTime), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_CLOSED
		{
			get { return ((DateTime)(base.GetValue(RevenueLineItemActivity.DATE_CLOSEDProperty))); }
			set { base.SetValue(RevenueLineItemActivity.DATE_CLOSEDProperty, value); }
		}

		public static DependencyProperty NEXT_STEPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NEXT_STEP", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NEXT_STEP
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.NEXT_STEPProperty))); }
			set { base.SetValue(RevenueLineItemActivity.NEXT_STEPProperty, value); }
		}

		public static DependencyProperty SALES_STAGEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SALES_STAGE", typeof(string), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SALES_STAGE
		{
			get { return ((string)(base.GetValue(RevenueLineItemActivity.SALES_STAGEProperty))); }
			set { base.SetValue(RevenueLineItemActivity.SALES_STAGEProperty, value); }
		}

		public static DependencyProperty PROBABILITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROBABILITY", typeof(float), typeof(RevenueLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float PROBABILITY
		{
			get { return ((float)(base.GetValue(RevenueLineItemActivity.PROBABILITYProperty))); }
			set { base.SetValue(RevenueLineItemActivity.PROBABILITYProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("RevenueLineItemActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("RevenueLineItemActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select REVENUE_LINE_ITEMS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwREVENUE_LINE_ITEMS_AUDIT        REVENUE_LINE_ITEMS          " + ControlChars.CrLf
							                + " inner join vwREVENUE_LINE_ITEMS_AUDIT        REVENUE_LINE_ITEMS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on REVENUE_LINE_ITEMS_AUDIT_OLD.ID = REVENUE_LINE_ITEMS.ID       " + ControlChars.CrLf
							                + "        and REVENUE_LINE_ITEMS_AUDIT_OLD.AUDIT_VERSION = (select max(vwREVENUE_LINE_ITEMS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                            from vwREVENUE_LINE_ITEMS_AUDIT                   " + ControlChars.CrLf
							                + "                                                           where vwREVENUE_LINE_ITEMS_AUDIT.ID            =  REVENUE_LINE_ITEMS.ID           " + ControlChars.CrLf
							                + "                                                             and vwREVENUE_LINE_ITEMS_AUDIT.AUDIT_VERSION <  REVENUE_LINE_ITEMS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                             and vwREVENUE_LINE_ITEMS_AUDIT.AUDIT_TOKEN   <> REVENUE_LINE_ITEMS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                         )" + ControlChars.CrLf
							                + " where REVENUE_LINE_ITEMS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                        " + ControlChars.CrLf
							                + "  from vwREVENUE_LINE_ITEMS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwREVENUE_LINE_ITEMS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                        " + ControlChars.CrLf
							                + "  from vwREVENUE_LINE_ITEMS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								if ( bPast )
									ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								if ( bPast )
									OPPORTUNITY_ID                 = Sql.ToGuid    (rdr["OPPORTUNITY_ID"                ]);
								if ( bPast )
									LINE_GROUP_ID                  = Sql.ToGuid    (rdr["LINE_GROUP_ID"                 ]);
								if ( bPast )
									LINE_ITEM_TYPE                 = Sql.ToString  (rdr["LINE_ITEM_TYPE"                ]);
								if ( bPast )
									POSITION                       = Sql.ToInteger (rdr["POSITION"                      ]);
								if ( bPast )
									NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								if ( bPast )
									MFT_PART_NUM                   = Sql.ToString  (rdr["MFT_PART_NUM"                  ]);
								if ( bPast )
									VENDOR_PART_NUM                = Sql.ToString  (rdr["VENDOR_PART_NUM"               ]);
								if ( bPast )
									PRODUCT_TEMPLATE_ID            = Sql.ToGuid    (rdr["PRODUCT_TEMPLATE_ID"           ]);
								if ( bPast )
									TAX_CLASS                      = Sql.ToString  (rdr["TAX_CLASS"                     ]);
								if ( bPast )
									QUANTITY                       = Sql.ToFloat   (rdr["QUANTITY"                      ]);
								if ( bPast )
									COST_PRICE                     = Sql.ToDecimal (rdr["COST_PRICE"                    ]);
								if ( bPast )
									LIST_PRICE                     = Sql.ToDecimal (rdr["LIST_PRICE"                    ]);
								if ( bPast )
									UNIT_PRICE                     = Sql.ToDecimal (rdr["UNIT_PRICE"                    ]);
								if ( bPast )
									DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								if ( bPast )
									PARENT_TEMPLATE_ID             = Sql.ToGuid    (rdr["PARENT_TEMPLATE_ID"            ]);
								if ( bPast )
									DISCOUNT_ID                    = Sql.ToGuid    (rdr["DISCOUNT_ID"                   ]);
								if ( bPast )
									DISCOUNT_PRICE                 = Sql.ToDecimal (rdr["DISCOUNT_PRICE"                ]);
								if ( bPast )
									PRICING_FORMULA                = Sql.ToString  (rdr["PRICING_FORMULA"               ]);
								if ( bPast )
									PRICING_FACTOR                 = Sql.ToFloat   (rdr["PRICING_FACTOR"                ]);
								if ( bPast )
									TAXRATE_ID                     = Sql.ToGuid    (rdr["TAXRATE_ID"                    ]);
								if ( bPast )
									OPPORTUNITY_TYPE               = Sql.ToString  (rdr["OPPORTUNITY_TYPE"              ]);
								if ( bPast )
									LEAD_SOURCE                    = Sql.ToString  (rdr["LEAD_SOURCE"                   ]);
								if ( bPast )
									DATE_CLOSED                    = Sql.ToDateTime(rdr["DATE_CLOSED"                   ]);
								if ( bPast )
									NEXT_STEP                      = Sql.ToString  (rdr["NEXT_STEP"                     ]);
								if ( bPast )
									SALES_STAGE                    = Sql.ToString  (rdr["SALES_STAGE"                   ]);
								if ( bPast )
									PROBABILITY                    = Sql.ToFloat   (rdr["PROBABILITY"                   ]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("RevenueLineItemActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("REVENUE_LINE_ITEMS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spREVENUE_LINE_ITEMS_Update
								( ref gID
								, OPPORTUNITY_ID
								, LINE_GROUP_ID
								, LINE_ITEM_TYPE
								, POSITION
								, NAME
								, MFT_PART_NUM
								, VENDOR_PART_NUM
								, PRODUCT_TEMPLATE_ID
								, TAX_CLASS
								, QUANTITY
								, COST_PRICE
								, LIST_PRICE
								, UNIT_PRICE
								, DESCRIPTION
								, PARENT_TEMPLATE_ID
								, DISCOUNT_ID
								, DISCOUNT_PRICE
								, PRICING_FORMULA
								, PRICING_FACTOR
								, TAXRATE_ID
								, OPPORTUNITY_TYPE
								, LEAD_SOURCE
								, DATE_CLOSED
								, NEXT_STEP
								, SALES_STAGE
								, PROBABILITY
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("RevenueLineItemActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

