using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web.Services;
using System.ComponentModel;
using SplendidCRM;

namespace SplendidCRM.KPIB0202
{
	public class KPIB0202
	{
		public Guid    ID  ;
		public string  ALLOCATE_NAME;

		public KPIB0202()
		{
			ID   = Guid.Empty  ;
			ALLOCATE_NAME = String.Empty;
		}
	}

	/// <summary>
	/// Summary description for AutoComplete
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.Web.Script.Services.ScriptService]
	[ToolboxItem(false)]
	public class AutoComplete : System.Web.Services.WebService
	{
		[WebMethod(EnableSession=true)]
		public KPIB0202 B_KPI_ALLOCATES_B_KPI_ALLOCATE_ALLOCATE_NAME_Get(string sALLOCATE_NAME)
		{
			KPIB0202 item = new KPIB0202();
			//try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select ID        " + ControlChars.CrLf
					     + "     , ALLOCATE_NAME      " + ControlChars.CrLf
					     + "  from vwB_KPI_ALLOCATES" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, "KPIB0202", "list");

                        Sql.AppendParameter(cmd, sALLOCATE_NAME, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "ALLOCATE_NAME");
						cmd.CommandText += " order by ALLOCATE_NAME" + ControlChars.CrLf;
						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								item.ID   = Sql.ToGuid   (rdr["ID"  ]);
								item.ALLOCATE_NAME = Sql.ToString (rdr["ALLOCATE_NAME"]);
							}
						}
					}
				}
				if ( Sql.IsEmptyGuid(item.ID) )
				{
					string sCULTURE = Sql.ToString (Session["USER_SETTINGS/CULTURE"]);
					L10N L10n = new L10N(sCULTURE);
					throw(new Exception(L10n.Term("KPIB0202.ERR_B_KPI_ALLOCATE_NOT_FOUND")));
				}
			}
			//catch
			{
				// 02/04/2007 Paul.  Don't catch the exception.  
				// It is a web service, so the exception will be handled properly by the AJAX framework. 
			}
			return item;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_ALLOCATES_B_KPI_ALLOCATE_ALLOCATE_NAME_List(string prefixText, int count)
		{
			string[] arrItems = new string[0];
			try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sSQL;
					sSQL = "select distinct  " + ControlChars.CrLf
					     + "       ALLOCATE_NAME      " + ControlChars.CrLf
					     + "  from vwB_KPI_ALLOCATES" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, "KPIB0202", "list");

                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "ALLOCATE_NAME");
						cmd.CommandText += " order by ALLOCATE_NAME" + ControlChars.CrLf;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(0, count, dt);
								arrItems = new string[dt.Rows.Count];
								for ( int i=0; i < dt.Rows.Count; i++ )
									arrItems[i] = Sql.ToString(dt.Rows[i]["ALLOCATE_NAME"]);
							}
						}
					}
				}
			}
			catch
			{
			}
			return arrItems;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_ALLOCATES_ALLOCATE_NAME_List(string prefixText, int count)
		{
			return B_KPI_ALLOCATES_B_KPI_ALLOCATE_ALLOCATE_NAME_List(prefixText, count);
		}
	}
}

