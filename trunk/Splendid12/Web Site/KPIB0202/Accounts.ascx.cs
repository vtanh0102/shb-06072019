using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.KPIB0202
{

	/// <summary>
	///		Summary description for Accounts.
	/// </summary>
	public class Accounts : SubPanelControl
	{
		protected _controls.SubPanelButtons ctlDynamicButtons;
		protected _controls.SearchView     ctlSearchView    ;
		protected UniqueStringCollection arrSelectFields;
		protected Guid            gID            ;
		protected DataView        vwMain         ;
		protected SplendidGrid    grdMain        ;
		protected HtmlInputHidden txtACCOUNT_ID      ;
		protected Button          btnCreateInline   ;
		protected Panel           pnlNewRecordInline;
		protected SplendidCRM.Accounts.NewRecord ctlNewRecord   ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch ( e.CommandName )
				{
					case "Accounts.Edit":
					{
						Guid gACCOUNT_ID = Sql.ToGuid(e.CommandArgument);
						Response.Redirect("~/Accounts/edit.aspx?ID=" + gACCOUNT_ID.ToString());
						break;
					}
					case "Accounts.Remove":
					{
						Guid gACCOUNT_ID = Sql.ToGuid(e.CommandArgument);
						if ( bEditView )
						{
							this.DeleteEditViewRelationship(gACCOUNT_ID);
						}
						else
						{
                            SqlProcs.spB_KPI_ALLOCATES_ACCOUNTS_Delete(gID, gACCOUNT_ID);
						}
						BindGrid();
						break;
					}
					case "Accounts.Create":
						if ( this.IsMobile || Sql.ToBoolean(Application["CONFIG.disable_editview_inline"]) )
							Response.Redirect("~/" + m_sMODULE + "/edit.aspx?PARENT_ID=" + gID.ToString());
						else
						{
							pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "inline");
							ctlDynamicButtons.HideAll();
						}
						break;
					case "NewRecord.Cancel":
						pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "none");
						ctlDynamicButtons.ShowAll();
						break;
					case "NewRecord.FullForm":
						Response.Redirect("~/" + m_sMODULE + "/edit.aspx?PARENT_ID=" + gID.ToString());
						break;
					case "NewRecord":
					{
						Guid gACCOUNTS_ID = Sql.ToGuid(e.CommandArgument);
						if ( !Sql.IsEmptyGuid(gACCOUNTS_ID) )
						{
                            SqlProcs.spB_KPI_ALLOCATES_ACCOUNTS_Update(gID, gACCOUNTS_ID);
							Response.Redirect(Request.RawUrl);
						}
						else
						{
							pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "none");
							ctlDynamicButtons.ShowAll();
						}
						break;
					}
					case "Accounts.Search":
						ctlSearchView.Visible = !ctlSearchView.Visible;
						break;
					case "Search":
						break;
					case "Clear":
						BindGrid();
						break;
					case "SortGrid":
						break;
					case "Preview":
						if ( Page.Master is SplendidMaster )
						{
							CommandEventArgs ePreview = new CommandEventArgs(e.CommandName, new PreviewData(m_sMODULE, Sql.ToGuid(e.CommandArgument)));
							(Page.Master as SplendidMaster).Page_Command(sender, ePreview);
						}
						break;
					default:
						throw(new Exception("Unknown command: " + e.CommandName));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void BindGrid()
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				string sSQL;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					UniqueGuidCollection arrUPDATED = this.GetUpdatedEditViewRelationships();
					if ( bEditView && IsPostBack && arrUPDATED.Count > 0 )
					{
						arrSelectFields.Remove("ACCOUNT_ID"                  );
						arrSelectFields.Remove("ACCOUNT_NAME"                );
						arrSelectFields.Remove("B_KPI_ALLOCATE_ID"              );
						arrSelectFields.Remove("B_KPI_ALLOCATE_NAME"            );
						arrSelectFields.Remove("B_KPI_ALLOCATE_ASSIGNED_USER_ID");
						sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
						     + "     , ID                        as ACCOUNT_ID                  " + ControlChars.CrLf
						     + "     , NAME                      as ACCOUNT_NAME                " + ControlChars.CrLf
						     + "     , @B_KPI_ALLOCATE_ID               as B_KPI_ALLOCATE_ID              " + ControlChars.CrLf
						     + "     , @B_KPI_ALLOCATE_NAME             as B_KPI_ALLOCATE_NAME            " + ControlChars.CrLf
						     + "     , @B_KPI_ALLOCATE_ASSIGNED_USER_ID as B_KPI_ALLOCATE_ASSIGNED_USER_ID" + ControlChars.CrLf
						     + "  from vwACCOUNTS" + ControlChars.CrLf;
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ID"              , gID);
						Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_NAME"            , Sql.ToString(Page.Items["NAME"            ]));
						Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ASSIGNED_USER_ID", Sql.ToGuid  (Page.Items["ASSIGNED_USER_ID"]));

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, m_sMODULE, "list");

						Sql.AppendParameter(cmd, arrUPDATED.ToArray(), "ID");
					}
					else
					{
						sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
						     + "  from vwB_KPI_ALLOCATES_ACCOUNTS" + ControlChars.CrLf;
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, m_sMODULE, "list");

						Sql.AppendParameter(cmd, gID, "B_KPI_ALLOCATE_ID");
					}
					ctlSearchView.SqlSearchClause(cmd);
					cmd.CommandText += grdMain.OrderByClause("DATE_ENTERED", "desc");

					if ( bDebug )
						RegisterClientScriptBlock("vwB_KPI_ALLOCATES_ACCOUNTS", Sql.ClientScriptBlock(cmd));

					try
					{
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								this.ApplyGridViewRules("KPIB0202." + m_sMODULE, dt);
								vwMain = dt.DefaultView;
								grdMain.DataSource = vwMain ;
								grdMain.DataBind();
								if ( bEditView && !IsPostBack )
								{
									this.CreateEditViewRelationships(dt, "ACCOUNT_ID");
								}
							}
						}
					}
					catch(Exception ex)
					{
						SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						ctlDynamicButtons.ErrorText = ex.Message;
					}
				}
			}
		}

		// 01/27/2010 Paul.  This method is only calld when in EditMode. 
		public override void Save(Guid gPARENT_ID, string sPARENT_TYPE, IDbTransaction trn)
		{
			UniqueGuidCollection arrDELETED = this.GetDeletedEditViewRelationships();
			foreach ( Guid gDELETE_ID in arrDELETED )
			{
				if ( !Sql.IsEmptyGuid(gDELETE_ID) )
                    SqlProcs.spB_KPI_ALLOCATES_ACCOUNTS_Delete(gPARENT_ID, gDELETE_ID, trn);
			}

			UniqueGuidCollection arrUPDATED = this.GetUpdatedEditViewRelationships();
			foreach ( Guid gUPDATE_ID in arrUPDATED )
			{
				if ( !Sql.IsEmptyGuid(gUPDATE_ID) )
                    SqlProcs.spB_KPI_ALLOCATES_ACCOUNTS_Update(gPARENT_ID, gUPDATE_ID, trn);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			gID = Sql.ToGuid(Request["ID"]);
			Guid gACCOUNT_ID = Sql.ToGuid(txtACCOUNT_ID.Value);
			if ( !Sql.IsEmptyGuid(gACCOUNT_ID) )
			{
				try
				{
					if ( bEditView )
					{
						this.UpdateEditViewRelationship(gACCOUNT_ID);
					}
					else
					{
                        SqlProcs.spB_KPI_ALLOCATES_ACCOUNTS_Update(gID, gACCOUNT_ID);
					}
					txtACCOUNT_ID.Value = String.Empty;
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			try
			{
				BindGrid();
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}

			if ( !IsPostBack )
			{
				Guid gASSIGNED_USER_ID = Sql.ToGuid(Page.Items["ASSIGNED_USER_ID"]);
				ctlDynamicButtons.AppendButtons("KPIB0202." + m_sMODULE, gASSIGNED_USER_ID, gID);
				ctlNewRecord.PARENT_ID    = gID;
				ctlNewRecord.PARENT_TYPE = "KPIB0202";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlNewRecord.Command      += new CommandEventHandler(Page_Command);
			ctlSearchView.Command     += new CommandEventHandler(Page_Command);
			m_sMODULE = "Accounts";
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("DATE_ENTERED"            );
			arrSelectFields.Add("PENDING_PROCESS_ID"      );
			arrSelectFields.Add("ACCOUNT_ID"                  );
			bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
			if ( bModuleIsAssigned )
				arrSelectFields.Add("ASSIGNED_USER_ID"        );
			arrSelectFields.Add("B_KPI_ALLOCATE_ASSIGNED_USER_ID");
			this.AppendGridColumns(grdMain, "KPIB0202." + m_sMODULE, arrSelectFields, Page_Command);
			if ( IsPostBack )
				ctlDynamicButtons.AppendButtons("KPIB0202." + m_sMODULE, Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}
