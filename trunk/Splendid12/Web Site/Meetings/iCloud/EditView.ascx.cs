/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._controls;

using iCloud;
using iCloud.Contacts;
using iCloud.Calendar;

namespace SplendidCRM.Meetings.iCloud
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 01/13/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sUID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected HiddenField     txtINVITEE_ID                   ;
		protected SplendidGrid    grdInvitees                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					// 11/10/2010 Paul.  Apply Business Rules. 
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					// 04/19/2010 Paul.  We now need to validate the sub panels as they can contain an inline NewRecord control. 
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						DataTable dtCurrent = new DataTable();
						// 12/19/2011 Paul.  Get an empty recordset as a quick way to populate an empty row. 
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL ;
							sSQL = "select *              " + ControlChars.CrLf
							     + "  from vwMEETINGS_Edit" + ControlChars.CrLf
							     + " where ID is null     " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									da.Fill(dtCurrent);
								}
							}
						}
						
						Guid gUSER_ID = Security.USER_ID;
						Guid gTEAM_ID = Security.TEAM_ID;
						string sICLOUD_USERNAME      = String.Empty;
						string sICLOUD_PASSWORD      = String.Empty;
						string sICLOUD_CTAG_CONTACTS = String.Empty;
						string sICLOUD_CTAG_CALENDAR = String.Empty;
						iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
						
						IDbCommand spAPPOINTMENTS_Update = null;
						DataTable dt = iCloudSync.CreateAppointmentsTable(Application, ref spAPPOINTMENTS_Update);
						
						CalendarService service = new CalendarService(Application);
						// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
						service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
						service.QueryClientLoginToken(false);
						
						ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
						
						StringBuilder sbChanges = new StringBuilder();
						AppointmentEntry appointment = new AppointmentEntry();
						appointment.UID = sUID;
						
						DataRow rowCurrent = dtCurrent.NewRow();
						rowCurrent["ASSIGNED_USER_ID"          ] = new DynamicControl(this, "ASSIGNED_USER_ID"          ).ID;
						rowCurrent["NAME"                      ] = new DynamicControl(this, "NAME"                      ).Text;
						rowCurrent["LOCATION"                  ] = new DynamicControl(this, "LOCATION"                  ).Text;
						rowCurrent["DURATION_HOURS"            ] = new DynamicControl(this, "DURATION_HOURS"            ).IntegerValue;
						rowCurrent["DURATION_MINUTES"          ] = new DynamicControl(this, "DURATION_MINUTES"          ).IntegerValue;
						rowCurrent["DATE_TIME"                 ] = new DynamicControl(this, "DATE_TIME"                 ).DateValue;
						rowCurrent["STATUS"                    ] = new DynamicControl(this, "STATUS"                    ).SelectedValue;
						rowCurrent["DESCRIPTION"               ] = new DynamicControl(this, "DESCRIPTION"               ).Text;
						rowCurrent["TEAM_ID"                   ] = new DynamicControl(this, "TEAM_ID"                   ).ID;
						rowCurrent["TEAM_SET_LIST"             ] = new DynamicControl(this, "TEAM_SET_LIST"             ).Text;
						rowCurrent["DATE_ENTERED"              ] = DateTime.Now;
						rowCurrent["DATE_MODIFIED"             ] = DateTime.Now;
						
						DataTable dtAppointmentEmails = iCloudSync.CreateParticipantsTable();
						List<Who> Participants = ViewState["Participants"] as List<Who>;
						if ( Participants != null )
						{
							foreach ( Who who in Participants )
							{
								DataRow row = dtAppointmentEmails.NewRow();
								if ( !String.IsNullOrEmpty(who.ID) )
									row["ICLOUD_ID"] = who.ID;
								if ( !String.IsNullOrEmpty(who.Name) )
									row["NAME"] = who.Name;
								if ( !String.IsNullOrEmpty(who.Email) )
									row["EMAIL1"] = who.Email;
								if ( who.Rel == Who.RelType.EVENT_ORGANIZER )
									row["REL"] = "ORGANIZER";
								else
									row["REL"] = "ATTENDEE";
								if ( who.Attendee_Status != null && !String.IsNullOrEmpty(who.Attendee_Status.Value) )
								{
									switch ( who.Attendee_Status.Value )
									{
										case Who.AttendeeStatus.EVENT_ACCEPTED :  row["ACCEPT_STATUS"] = "accept"   ;  break;
										case Who.AttendeeStatus.EVENT_DECLINED :  row["ACCEPT_STATUS"] = "decline"  ;  break;
										case Who.AttendeeStatus.EVENT_TENTATIVE:  row["ACCEPT_STATUS"] = "tentative";  break;
										case Who.AttendeeStatus.EVENT_INVITED  :  row["ACCEPT_STATUS"] = "none"     ;  break;
									}
								}
								dtAppointmentEmails.Rows.Add(row);
							}
						}
						// 03/23/2013 Paul.  Add the ability to disable participants. 
						bool bDISABLE_PARTICIPANTS = Sql.ToBoolean(Context.Application["CONFIG.iCloud.DisableParticipants"]);
						iCloudSync.SetiCloudAppointment(appointment, rowCurrent, dtAppointmentEmails, sICLOUD_USERNAME, sbChanges, bDISABLE_PARTICIPANTS);
						if ( Sql.IsEmptyString(sUID) )
						{
							service.Insert(Context, appointment);
							sUID = appointment.UID;
						}
						else
						{
							service.Update(Context, appointment);
						}
						Response.Redirect("view.aspx?UID=" + sUID);
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sUID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?UID=" + sUID);
			}
			else if ( e.CommandName == "Invitees.Add" )
			{
				if ( txtINVITEE_ID.Value.Length > 0 )
					txtINVITEE_ID.Value += ",";
				txtINVITEE_ID.Value += e.CommandArgument;
			}
			else if ( e.CommandName == "Invitees.Delete" )
			{
				string sDELETE_ID = e.CommandArgument.ToString().ToLower();
				string[] arrINVITEES = txtINVITEE_ID.Value.Split(',');
				StringBuilder sb = new StringBuilder();
				foreach(string sINVITEE_ID in arrINVITEES)
				{
					if ( sINVITEE_ID != sDELETE_ID )
					{
						if ( sb.Length > 0 )
							sb.Append(",");
						sb.Append(sINVITEE_ID);
					}
				}
				txtINVITEE_ID.Value = sb.ToString();
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sUID = Sql.ToString(Request["UID"]);
				if ( !IsPostBack )
				{
					string sDuplicateUID = Sql.ToString(Request["DuplicateUID"]);
					if ( !Sql.IsEmptyString(sUID) || !Sql.IsEmptyString(sDuplicateUID) )
					{
						Guid gUSER_ID = Security.USER_ID;
						Guid gTEAM_ID = Security.TEAM_ID;
						string sICLOUD_USERNAME      = String.Empty;
						string sICLOUD_PASSWORD      = String.Empty;
						string sICLOUD_CTAG_CONTACTS = String.Empty;
						string sICLOUD_CTAG_CALENDAR = String.Empty;
						iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
						
						IDbCommand spAPPOINTMENTS_Update = null;
						DataTable dt = iCloudSync.CreateAppointmentsTable(Application, ref spAPPOINTMENTS_Update);
						
						CalendarService service = new CalendarService(Application);
						// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
						service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
						service.QueryClientLoginToken(false);
						
						ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
						AppointmentEntry appointment = service.Get(Context, sUID);
						
						DataRow row = dt.NewRow();
						if ( appointment != null )
						{
							iCloudSync.BuildAPPOINTMENTS_Update(Application, Session, spAPPOINTMENTS_Update, null, appointment, gUSER_ID, gTEAM_ID, gUSER_ID);
							row["UID"             ] = appointment.UID;
							row["NAME"            ] = appointment.Title;
							row["DATE_ENTERED"    ] = appointment.Created;
							row["DATE_MODIFIED"   ] = appointment.Updated;
						}
						row["ASSIGNED_TO_NAME"] = Security.USER_NAME;
						row["CREATED_BY_NAME" ] = Security.USER_NAME;
						row["MODIFIED_BY_NAME"] = Security.USER_NAME;
						row["TEAM_NAME"       ] = Security.TEAM_NAME;
						row["TEAM_SET_NAME"   ] = Security.TEAM_NAME;
						foreach(IDbDataParameter par in spAPPOINTMENTS_Update.Parameters)
						{
							string sParameterName = Sql.ExtractDbName(spAPPOINTMENTS_Update, par.ParameterName).ToUpper();
							row[sParameterName] = par.Value;
						}
						dt.Rows.Add(row);
						if ( dt.Rows.Count > 0 )
						{
							DataRow rdr = dt.Rows[0];
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							
							// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
							ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
							ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
							
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Sql.ToGuid(rdr["ASSIGNED_USER_ID"]), rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Sql.ToGuid(rdr["ASSIGNED_USER_ID"]), rdr);
							TextBox txtNAME = this.FindControl("NAME") as TextBox;
							if ( txtNAME != null )
								txtNAME.Focus();
							ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
							
							ViewState ["NAME"            ] = Sql.ToString(rdr["NAME"]);
							ViewState ["ASSIGNED_USER_ID"] = Sql.ToGuid  (rdr["ASSIGNED_USER_ID"]);
							Page.Items["NAME"            ] = ViewState ["NAME"            ];
							Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];

							if ( appointment != null )
							{
								ViewState["Participants"] = appointment.Participants;
								grdInvitees.DataSource = appointment.Participants;
								grdInvitees.DataBind();
							}
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("NAME") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
					}
					// Default to current user. 
					if ( txtINVITEE_ID.Value.Length == 0 )
						txtINVITEE_ID.Value = Security.USER_ID.ToString();
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];

					grdInvitees.DataSource = ViewState["Participants"];
					;
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Meetings";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.IsEmptyString(Request["UID"]);
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

