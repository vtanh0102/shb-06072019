/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using iCloud;
using iCloud.Contacts;
using iCloud.Calendar;

namespace SplendidCRM.Meetings.iCloud
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected _controls.ExportHeader ctlExportHeader;
		protected _controls.SearchView   ctlSearchView  ;
		protected _controls.CheckAll     ctlCheckAll    ;

		protected UniqueStringCollection arrSelectFields;
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected Label         lblError       ;
		protected MassUpdate    ctlMassUpdate  ;
		// 06/06/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected Panel         pnlMassUpdateSeven;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Search" )
				{
					// 10/13/2005 Paul.  Make sure to clear the page index prior to applying search. 
					grdMain.CurrentPageIndex = 0;
					// 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
					grdMain.DataBind();
				}
				// 12/14/2007 Paul.  We need to capture the sort event from the SearchView. 
				else if ( e.CommandName == "SortGrid" )
				{
					grdMain.SetSortFields(e.CommandArgument as string[]);
					// 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
					// 03/17/2011 Paul.  We need to treat a comma-separated list of fields as an array. 
					arrSelectFields.AddFields(grdMain.SortColumn);
				}
				// 11/17/2010 Paul.  Populate the hidden Selected field with all IDs. 
				else if ( e.CommandName == "SelectAll" )
				{
					// 05/22/2011 Paul.  When using custom paging, vwMain may not be defined. 
					if ( vwMain == null )
						grdMain.DataBind();
					ctlCheckAll.SelectAll(vwMain, "ID");
					grdMain.DataBind();
				}
				// 06/06/2015 Paul.  Change standard MassUpdate command to a command to toggle visibility. 
				else if ( e.CommandName == "ToggleMassUpdate" )
				{
					pnlMassUpdateSeven.Visible = !pnlMassUpdateSeven.Visible;
				}
				else if ( e.CommandName == "MassDelete" )
				{
					// 11/27/2010 Paul.  Use new selected items. 
					string[] arrID = ctlCheckAll.SelectedItemsArray;
					if ( arrID != null )
					{
						//Response.Redirect("default.aspx");
					}
				}
				else if ( e.CommandName == "Export" )
				{
					// 11/03/2006 Paul.  Apply ACL rules to Export. 
					int nACLACCESS = SplendidCRM.Security.GetUserAccess(m_sMODULE, "export");
					if ( nACLACCESS  >= 0 )
					{
						// 10/05/2009 Paul.  When exporting, we may need to manually bind.  Custom paging should be disabled when exporting all. 
						if ( vwMain == null )
							grdMain.DataBind();
						if ( nACLACCESS == ACL_ACCESS.OWNER )
							vwMain.RowFilter = "ASSIGNED_USER_ID = '" + Security.USER_ID.ToString() + "'";
						// 11/27/2010 Paul.  Use new selected items. 
						string[] arrID = ctlCheckAll.SelectedItemsArray;
						SplendidExport.Export(vwMain, m_sMODULE, ctlExportHeader.ExportFormat, ctlExportHeader.ExportRange, grdMain.CurrentPageIndex, grdMain.PageSize, arrID, grdMain.AllowCustomPaging);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				if ( !IsPostBack )
				{
					grdMain.OrderByClause("NAME", "asc");
					
					Guid gUSER_ID = Security.USER_ID;
					Guid gTEAM_ID = Security.TEAM_ID;
					string sICLOUD_USERNAME      = String.Empty;
					string sICLOUD_PASSWORD      = String.Empty;
					string sICLOUD_CTAG_CONTACTS = String.Empty;
					string sICLOUD_CTAG_CALENDAR = String.Empty;
					iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
					// 02/28/2012 Paul.  If this user does not define iCloud authentication, then exit early. 
					if ( Sql.IsEmptyString(sICLOUD_USERNAME) )
					{
						return;
					}
					
					IDbCommand spAPPOINTMENTS_Update = null;
					DataTable dt = iCloudSync.CreateAppointmentsTable(Application, ref spAPPOINTMENTS_Update);
					
					CalendarService service = new CalendarService(Application);
					// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
					service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
					service.QueryClientLoginToken(false);
					
					ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
					List<AppointmentEntry> fContactResults = service.Query(Context);
					foreach ( AppointmentEntry appointment in fContactResults )
					{
						iCloudSync.BuildAPPOINTMENTS_Update(Application, Session, spAPPOINTMENTS_Update, null, appointment, gUSER_ID, gTEAM_ID, gUSER_ID);
						DataRow row = dt.NewRow();
						row["UID"             ] = appointment.UID;
						row["NAME"            ] = appointment.Title;
						row["DATE_ENTERED"    ] = appointment.Created;
						row["DATE_MODIFIED"   ] = appointment.Updated;
						row["ASSIGNED_TO_NAME"] = Security.USER_NAME;
						row["CREATED_BY_NAME" ] = Security.USER_NAME;
						row["MODIFIED_BY_NAME"] = Security.USER_NAME;
						row["TEAM_NAME"       ] = Security.TEAM_NAME;
						row["TEAM_SET_NAME"   ] = Security.TEAM_NAME;
						foreach(IDbDataParameter par in spAPPOINTMENTS_Update.Parameters)
						{
							string sParameterName = Sql.ExtractDbName(spAPPOINTMENTS_Update, par.ParameterName).ToUpper();
							row[sParameterName] = par.Value;
						}
						dt.Rows.Add(row);
					}
					
					//this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);
					vwMain = dt.DefaultView;
					grdMain.DataSource = vwMain ;
					ViewState["Meetings"] = dt;
					
					ctlExportHeader.Visible = true;
					ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
					// 06/06/2015 Paul.  Change standard MassUpdate command to a command to toggle visibility. 
					ctlCheckAll  .Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
				}
				else
				{
					DataTable dt = ViewState["Meetings"] as DataTable;
					if ( dt != null )
					{
						vwMain = dt.DefaultView;
						grdMain.DataSource = vwMain ;
					}
				}
				if ( !IsPostBack )
				{
					grdMain.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Contact is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlSearchView  .Command += new CommandEventHandler(Page_Command);
			ctlExportHeader.Command += new CommandEventHandler(Page_Command);
			ctlMassUpdate  .Command += new CommandEventHandler(Page_Command);
			ctlCheckAll    .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Meetings";
			SetMenu(m_sMODULE);
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("NAME");
			this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields);
			if ( Security.GetUserAccess(m_sMODULE, "delete") < 0 && Security.GetUserAccess(m_sMODULE, "edit") < 0 )
				ctlMassUpdate.Visible = false;
			
			// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
			if ( SplendidDynamic.StackedLayout(Page.Theme) )
			{
				// 06/05/2015 Paul.  Move MassUpdate buttons to the SplendidGrid. 
				grdMain.IsMobile       = this.IsMobile;
				grdMain.MassUpdateView = m_sMODULE + ".MassUpdate";
				grdMain.Command       += new CommandEventHandler(Page_Command);
				if ( !IsPostBack )
					pnlMassUpdateSeven.Visible = false;
			}
		}
		#endregion
	}
}

