/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using iCloud;
using iCloud.Contacts;
using iCloud.Calendar;

namespace SplendidCRM.Meetings.iCloud
{
	/// <summary>
	/// Summary description for DetailView.
	/// </summary>
	public class DetailView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons ctlDynamicButtons;

		protected string       sUID             ;
		protected HtmlTable    tblMain          ;
		protected PlaceHolder  plcSubPanel      ;
		protected SplendidGrid grdInvitees      ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Edit" )
				{
					Response.Redirect("edit.aspx?UID=" + sUID);
				}
				else if ( e.CommandName == "Duplicate" )
				{
					Response.Redirect("edit.aspx?DuplicateUID=" +sUID);
				}
				else if ( e.CommandName == "Delete" )
				{
					Guid gUSER_ID = Security.USER_ID;
					Guid gTEAM_ID = Security.TEAM_ID;
					string sICLOUD_USERNAME      = String.Empty;
					string sICLOUD_PASSWORD      = String.Empty;
					string sICLOUD_CTAG_CONTACTS = String.Empty;
					string sICLOUD_CTAG_CALENDAR = String.Empty;
					iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
					
					CalendarService service = new CalendarService(Application);
					// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
					service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
					service.QueryClientLoginToken(false);
					service.Delete(Context, sUID);
					
					Response.Redirect("default.aspx");
				}
				else if ( e.CommandName == "Cancel" )
				{
					Response.Redirect("default.aspx");
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sUID = Sql.ToString(Request["UID"]);
				if ( !IsPostBack )
				{
					if ( !Sql.IsEmptyString(sUID) )
					{
						Guid gUSER_ID = Security.USER_ID;
						Guid gTEAM_ID = Security.TEAM_ID;
						string sICLOUD_USERNAME      = String.Empty;
						string sICLOUD_PASSWORD      = String.Empty;
						string sICLOUD_CTAG_CONTACTS = String.Empty;
						string sICLOUD_CTAG_CALENDAR = String.Empty;
						iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
						
						IDbCommand spAPPOINTMENTS_Update = null;
						DataTable dt = iCloudSync.CreateAppointmentsTable(Application, ref spAPPOINTMENTS_Update);
						
						CalendarService service = new CalendarService(Application);
						// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
						service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
						service.QueryClientLoginToken(false);
						
						ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
						AppointmentEntry appointment = service.Get(Context, sUID);
						if ( appointment != null )
						{
							iCloudSync.BuildAPPOINTMENTS_Update(Application, Session, spAPPOINTMENTS_Update, null, appointment, gUSER_ID, gTEAM_ID, gUSER_ID);
							DataRow row = dt.NewRow();
							row["UID"             ] = appointment.UID;
							row["NAME"            ] = appointment.Title;
							row["DATE_ENTERED"    ] = appointment.Created;
							row["DATE_MODIFIED"   ] = appointment.Updated;
							row["ASSIGNED_TO_NAME"] = Security.USER_NAME;
							row["CREATED_BY_NAME" ] = Security.USER_NAME;
							row["MODIFIED_BY_NAME"] = Security.USER_NAME;
							row["TEAM_NAME"       ] = Security.TEAM_NAME;
							row["TEAM_SET_NAME"   ] = Security.TEAM_NAME;
							foreach(IDbDataParameter par in spAPPOINTMENTS_Update.Parameters)
							{
								string sParameterName = Sql.ExtractDbName(spAPPOINTMENTS_Update, par.ParameterName).ToUpper();
								row[sParameterName] = par.Value;
							}
							dt.Rows.Add(row);
							
							//this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, row);
							
							// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
							ctlDynamicButtons.Title = appointment.Title;
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
							
							this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, row, new CommandEventHandler(Page_Command));
							Page.Items["ASSIGNED_USER_ID"] = Sql.ToGuid(row["ASSIGNED_USER_ID"]);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Sql.ToGuid(row["ASSIGNED_USER_ID"]), row);
							//this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, row);
							
							grdInvitees.DataSource = appointment.Participants;
							grdInvitees.DataBind();
						}
						else
						{
							plcSubPanel.Visible = false;
							
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
						}
					}
					else
					{
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
						ctlDynamicButtons.DisableAll();
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Meetings";
			SetMenu(m_sMODULE);
			this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
			if ( IsPostBack )
			{
				this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null, new CommandEventHandler(Page_Command));
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

