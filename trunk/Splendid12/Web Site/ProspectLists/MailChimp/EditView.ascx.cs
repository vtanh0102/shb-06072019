/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.ProspectLists.MailChimp
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected String          sHID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						// 02/06/2014 Paul.  New MailChimp factory to allow Remote and Online. 
						bool bMailChimpEnabled = Spring.Social.MailChimp.MailChimpSync.MailChimpEnabled(Context.Application);
#if DEBUG
						//bMailChimpEnabled = true;
#endif
						if ( bMailChimpEnabled )
						{
							Spring.Social.MailChimp.Api.IMailChimp mailChimp = Spring.Social.MailChimp.MailChimpSync.CreateApi(Context.Application);
							Spring.Social.MailChimp.Api.List obj = null;
							if ( !Sql.IsEmptyString(sHID) )
							{
								obj = mailChimp.ListOperations.GetById(sHID);
							}
							else
							{
								obj = new Spring.Social.MailChimp.Api.List();
							}
							obj.name                         = new DynamicControl(this, "name"                  ).Text;
							obj.visibility                   = new DynamicControl(this, "visibility"            ).Text;
							obj.notify_on_subscribe          = new DynamicControl(this, "notify_on_subscribe"   ).Text;
							obj.notify_on_unsubscribe        = new DynamicControl(this, "notify_on_unsubscribe" ).Text;
							obj.email_type_option            = new DynamicControl(this, "email_type_option"     ).Checked;
							obj.use_archive_bar              = new DynamicControl(this, "use_archive_bar"       ).Checked;
							obj.permission_reminder          = new DynamicControl(this, "permission_reminder"   ).Text;
							obj.contact = new Spring.Social.MailChimp.Api.List.Contact();
							obj.contact.company              = new DynamicControl(this, "company"               ).Text;
							obj.contact.phone                = new DynamicControl(this, "phone"                 ).Text;
							obj.contact.address1             = new DynamicControl(this, "address1"              ).Text;
							obj.contact.address2             = new DynamicControl(this, "address2"              ).Text;
							obj.contact.city                 = new DynamicControl(this, "city"                  ).Text;
							obj.contact.state                = new DynamicControl(this, "state"                 ).Text;
							obj.contact.zip                  = new DynamicControl(this, "zip"                   ).Text;
							obj.contact.country              = new DynamicControl(this, "country"               ).Text;
							obj.campaign_defaults = new Spring.Social.MailChimp.Api.List.CampaignDefaults();
							obj.campaign_defaults.from_name  = new DynamicControl(this, "from_name"             ).Text;
							obj.campaign_defaults.subject    = new DynamicControl(this, "subject"               ).Text;
							obj.campaign_defaults.from_email = new DynamicControl(this, "from_email"            ).Text;
							obj.campaign_defaults.language   = new DynamicControl(this, "language"              ).Text;
							if ( !Sql.IsEmptyString(sHID))
							{
								mailChimp.ListOperations.Update(obj);
							}
							else
							{
								obj = mailChimp.ListOperations.Insert(obj);
								sHID = obj.id;
							}
							Response.Redirect("view.aspx?HID=" + sHID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sHID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?HID=" + sHID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 07/23/2017 Paul.  Consistent application of security for cloud service. 
			this.Visible = Spring.Social.MailChimp.MailChimpSync.MailChimpEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("MailChimp", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sHID = Sql.ToString(Request["HID"]);
				if ( !IsPostBack )
				{
					string sDuplicateHID = Sql.ToString(Request["DuplicateHID"]);
					if ( !Sql.IsEmptyString(sHID) || !Sql.IsEmptyString(sDuplicateHID) )
					{
						// 02/06/2014 Paul.  New MailChimp factory to allow Remote and Online. 
						Spring.Social.MailChimp.Api.IMailChimp mailChimp = Spring.Social.MailChimp.MailChimpSync.CreateApi(Context.Application);
							
						DataRow rdr = Spring.Social.MailChimp.Api.List.ConvertToRow(mailChimp.ListOperations.GetById(sHID));
						//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							
						ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
						SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
						ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
							
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							
						//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "ProspectLists";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.ToInteger(Request["HID"]) == 0;
			this.LayoutEditView = "EditView.MailChimp";
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

