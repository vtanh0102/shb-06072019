/**
 * Copyright (C) 2018 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.ProspectLists.Watson
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected String          sHID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						bool bWatsonEnabled = Spring.Social.Watson.WatsonSync.WatsonEnabled(Context.Application);
#if DEBUG
						//bWatsonEnabled = true;
#endif
						if ( bWatsonEnabled )
						{
							string database_id = Sql.ToString(Application["CONFIG.Watson.DefaultDatabaseID"]);
							Spring.Social.Watson.Api.IWatson watson = Spring.Social.Watson.WatsonSync.CreateApi(Context.Application);
							Spring.Social.Watson.Api.ProspectList obj = null;
							if ( !Sql.IsEmptyString(sHID) )
							{
								obj = watson.ProspectListOperations.GetById(sHID);
							}
							else
							{
								obj = new Spring.Social.Watson.Api.ProspectList();
							}
							obj.NAME       = new DynamicControl(this, "NAME"      ).Text;
							obj.VISIBILITY = new DynamicControl(this, "VISIBILITY").Checked ? 0 : 1;
							if ( !Sql.IsEmptyString(sHID))
							{
								watson.ProspectListOperations.Update(database_id, obj);
							}
							else
							{
								obj.PARENT_FOLDER_ID = new DynamicControl(this, "DATABASE_ID").SelectedValue;
								Spring.Social.Watson.Api.ProspectListInsert objNew = watson.ProspectListOperations.Insert(obj.PARENT_FOLDER_ID, obj);
								sHID = objNew.CONTACT_LIST_ID;
							}
							Response.Redirect("view.aspx?HID=" + sHID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sHID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?HID=" + sHID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 07/23/2017 Paul.  Consistent application of security for cloud service. 
			this.Visible = Spring.Social.Watson.WatsonSync.WatsonEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("Watson", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sHID = Sql.ToString(Request["HID"]);
				if ( !IsPostBack )
				{
					string sDuplicateHID = Sql.ToString(Request["DuplicateHID"]);
					if ( !Sql.IsEmptyString(sHID) || !Sql.IsEmptyString(sDuplicateHID) )
					{
						StringBuilder sbErrors = new StringBuilder();
						Spring.Social.Watson.WatsonSync.RefreshAccessToken(Application, sbErrors);
						if ( sbErrors.Length == 0 )
						{
							Spring.Social.Watson.Api.IWatson watson = Spring.Social.Watson.WatsonSync.CreateApi(Context.Application);
							
							DataRow rdr = Spring.Social.Watson.Api.ProspectList.ConvertToRow(watson.ProspectListOperations.GetById(sHID));
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							
							ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
							ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
							
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.ErrorText = sbErrors.ToString();
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "ProspectLists";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.ToInteger(Request["HID"]) == 0;
			this.LayoutEditView = "EditView.Watson";
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

