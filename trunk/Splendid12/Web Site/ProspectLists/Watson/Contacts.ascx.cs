/**
 * Copyright (C) 2018 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.ProspectLists.Watson
{
	/// <summary>
	///		Summary description for Contacts.
	/// </summary>
	public class Contacts : SubPanelControl
	{
		// 06/03/2015 Paul.  Combine ListHeader and DynamicButtons. 
		protected _controls.SubPanelButtons ctlDynamicButtons;
		protected String          sHID           ;
		protected DataView        vwMain         ;
		protected SplendidGrid    grdMain        ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch ( e.CommandName )
				{
					case "Contacts.Edit":
					{
						string sMemberID= Sql.ToString(e.CommandArgument);
						Response.Redirect("Contacts/edit.aspx?HID=" + sMemberID);
						break;
					}
					case "Contacts.Remove":
					{
						string sMemberID = Sql.ToString(e.CommandArgument);
						BindGrid();
						break;
					}
					case "Search":
						break;
					case "Clear":
						BindGrid();
						break;
					case "SortGrid":
						break;
					default:
						throw(new Exception("Unknown command: " + e.CommandName));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void BindGrid()
		{
			if ( !Sql.IsEmptyString(sHID) )
			{
				//StringBuilder sbErrors = new StringBuilder();
				//Spring.Social.Watson.Api.IWatson Watson = Spring.Social.Watson.WatsonSync.CreateApi(Context.Application);
				//DataTable dt = Spring.Social.Watson.Api.Contact.ConvertToTable(Watson.ProspectListOperations.GetContacts(sHID));
				//vwMain = dt.DefaultView;
				//grdMain.DataSource = vwMain ;
				//grdMain.DataBind();
				//ViewState["Main"] = dt;
			}
			
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			sHID = Sql.ToString(Page.Items["HID"]);
			try
			{
				BindGrid();
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}

			if ( !IsPostBack )
			{
				ctlDynamicButtons.AppendButtons("ProspectLists." + m_sMODULE + ".Watson", Guid.Empty, Guid.Empty);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Contacts";
			this.AppendGridColumns(grdMain, "ProspectLists." + m_sMODULE + ".Watson", null, Page_Command);
			if ( IsPostBack )
				ctlDynamicButtons.AppendButtons("ProspectLists." + m_sMODULE + ".Watson", Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}

