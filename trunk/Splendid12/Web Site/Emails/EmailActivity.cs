/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// EmailActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class EmailActivity: SplendidActivity
	{
		public EmailActivity()
		{
			this.Name = "EmailActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.IDProperty))); }
			set { base.SetValue(EmailActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(EmailActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.NAMEProperty))); }
			set { base.SetValue(EmailActivity.NAMEProperty, value); }
		}

		public static DependencyProperty DATE_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.DATE_TIMEProperty))); }
			set { base.SetValue(EmailActivity.DATE_TIMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(EmailActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(EmailActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.PARENT_IDProperty))); }
			set { base.SetValue(EmailActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(EmailActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(EmailActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty DESCRIPTION_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION_HTML", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION_HTML
		{
			get { return ((string)(base.GetValue(EmailActivity.DESCRIPTION_HTMLProperty))); }
			set { base.SetValue(EmailActivity.DESCRIPTION_HTMLProperty, value); }
		}

		public static DependencyProperty FROM_ADDRProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_ADDR", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_ADDR
		{
			get { return ((string)(base.GetValue(EmailActivity.FROM_ADDRProperty))); }
			set { base.SetValue(EmailActivity.FROM_ADDRProperty, value); }
		}

		public static DependencyProperty FROM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.FROM_NAMEProperty))); }
			set { base.SetValue(EmailActivity.FROM_NAMEProperty, value); }
		}

		public static DependencyProperty TO_ADDRSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_ADDRS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_ADDRS
		{
			get { return ((string)(base.GetValue(EmailActivity.TO_ADDRSProperty))); }
			set { base.SetValue(EmailActivity.TO_ADDRSProperty, value); }
		}

		public static DependencyProperty CC_ADDRSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CC_ADDRS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CC_ADDRS
		{
			get { return ((string)(base.GetValue(EmailActivity.CC_ADDRSProperty))); }
			set { base.SetValue(EmailActivity.CC_ADDRSProperty, value); }
		}

		public static DependencyProperty BCC_ADDRSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BCC_ADDRS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BCC_ADDRS
		{
			get { return ((string)(base.GetValue(EmailActivity.BCC_ADDRSProperty))); }
			set { base.SetValue(EmailActivity.BCC_ADDRSProperty, value); }
		}

		public static DependencyProperty TO_ADDRS_IDSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_ADDRS_IDS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_ADDRS_IDS
		{
			get { return ((string)(base.GetValue(EmailActivity.TO_ADDRS_IDSProperty))); }
			set { base.SetValue(EmailActivity.TO_ADDRS_IDSProperty, value); }
		}

		public static DependencyProperty TO_ADDRS_NAMESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_ADDRS_NAMES", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_ADDRS_NAMES
		{
			get { return ((string)(base.GetValue(EmailActivity.TO_ADDRS_NAMESProperty))); }
			set { base.SetValue(EmailActivity.TO_ADDRS_NAMESProperty, value); }
		}

		public static DependencyProperty TO_ADDRS_EMAILSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_ADDRS_EMAILS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_ADDRS_EMAILS
		{
			get { return ((string)(base.GetValue(EmailActivity.TO_ADDRS_EMAILSProperty))); }
			set { base.SetValue(EmailActivity.TO_ADDRS_EMAILSProperty, value); }
		}

		public static DependencyProperty CC_ADDRS_IDSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CC_ADDRS_IDS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CC_ADDRS_IDS
		{
			get { return ((string)(base.GetValue(EmailActivity.CC_ADDRS_IDSProperty))); }
			set { base.SetValue(EmailActivity.CC_ADDRS_IDSProperty, value); }
		}

		public static DependencyProperty CC_ADDRS_NAMESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CC_ADDRS_NAMES", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CC_ADDRS_NAMES
		{
			get { return ((string)(base.GetValue(EmailActivity.CC_ADDRS_NAMESProperty))); }
			set { base.SetValue(EmailActivity.CC_ADDRS_NAMESProperty, value); }
		}

		public static DependencyProperty CC_ADDRS_EMAILSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CC_ADDRS_EMAILS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CC_ADDRS_EMAILS
		{
			get { return ((string)(base.GetValue(EmailActivity.CC_ADDRS_EMAILSProperty))); }
			set { base.SetValue(EmailActivity.CC_ADDRS_EMAILSProperty, value); }
		}

		public static DependencyProperty BCC_ADDRS_IDSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BCC_ADDRS_IDS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BCC_ADDRS_IDS
		{
			get { return ((string)(base.GetValue(EmailActivity.BCC_ADDRS_IDSProperty))); }
			set { base.SetValue(EmailActivity.BCC_ADDRS_IDSProperty, value); }
		}

		public static DependencyProperty BCC_ADDRS_NAMESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BCC_ADDRS_NAMES", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BCC_ADDRS_NAMES
		{
			get { return ((string)(base.GetValue(EmailActivity.BCC_ADDRS_NAMESProperty))); }
			set { base.SetValue(EmailActivity.BCC_ADDRS_NAMESProperty, value); }
		}

		public static DependencyProperty BCC_ADDRS_EMAILSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BCC_ADDRS_EMAILS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BCC_ADDRS_EMAILS
		{
			get { return ((string)(base.GetValue(EmailActivity.BCC_ADDRS_EMAILSProperty))); }
			set { base.SetValue(EmailActivity.BCC_ADDRS_EMAILSProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(EmailActivity.TYPEProperty))); }
			set { base.SetValue(EmailActivity.TYPEProperty, value); }
		}

		public static DependencyProperty MESSAGE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MESSAGE_ID", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MESSAGE_ID
		{
			get { return ((string)(base.GetValue(EmailActivity.MESSAGE_IDProperty))); }
			set { base.SetValue(EmailActivity.MESSAGE_IDProperty, value); }
		}

		public static DependencyProperty REPLY_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPLY_TO_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPLY_TO_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.REPLY_TO_NAMEProperty))); }
			set { base.SetValue(EmailActivity.REPLY_TO_NAMEProperty, value); }
		}

		public static DependencyProperty REPLY_TO_ADDRProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPLY_TO_ADDR", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPLY_TO_ADDR
		{
			get { return ((string)(base.GetValue(EmailActivity.REPLY_TO_ADDRProperty))); }
			set { base.SetValue(EmailActivity.REPLY_TO_ADDRProperty, value); }
		}

		public static DependencyProperty INTENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INTENT", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string INTENT
		{
			get { return ((string)(base.GetValue(EmailActivity.INTENTProperty))); }
			set { base.SetValue(EmailActivity.INTENTProperty, value); }
		}

		public static DependencyProperty MAILBOX_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAILBOX_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MAILBOX_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.MAILBOX_IDProperty))); }
			set { base.SetValue(EmailActivity.MAILBOX_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.TEAM_IDProperty))); }
			set { base.SetValue(EmailActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(EmailActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(EmailActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(EmailActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(EmailActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(EmailActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(EmailActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(EmailActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(EmailActivity.CREATED_BYProperty))); }
			set { base.SetValue(EmailActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(EmailActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(EmailActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(EmailActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(EmailActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.DATE_STARTProperty))); }
			set { base.SetValue(EmailActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(EmailActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(EmailActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(EmailActivity.STATUSProperty))); }
			set { base.SetValue(EmailActivity.STATUSProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(EmailActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(EmailActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(EmailActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_START", typeof(DateTime), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_START
		{
			get { return ((DateTime)(base.GetValue(EmailActivity.TIME_STARTProperty))); }
			set { base.SetValue(EmailActivity.TIME_STARTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(EmailActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(EmailActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(EmailActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(EmailActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(EmailActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(EmailActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(EmailActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(EmailActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(EmailActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(EmailActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("EmailActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("EmailActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select EMAILS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwEMAILS_AUDIT        EMAILS          " + ControlChars.CrLf
							                + " inner join vwEMAILS_AUDIT        EMAILS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on EMAILS_AUDIT_OLD.ID = EMAILS.ID       " + ControlChars.CrLf
							                + "        and EMAILS_AUDIT_OLD.AUDIT_VERSION = (select max(vwEMAILS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                from vwEMAILS_AUDIT                   " + ControlChars.CrLf
							                + "                                               where vwEMAILS_AUDIT.ID            =  EMAILS.ID           " + ControlChars.CrLf
							                + "                                                 and vwEMAILS_AUDIT.AUDIT_VERSION <  EMAILS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                 and vwEMAILS_AUDIT.AUDIT_TOKEN   <> EMAILS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                             )" + ControlChars.CrLf
							                + " where EMAILS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *            " + ControlChars.CrLf
							                + "  from vwEMAILS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwEMAILS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *            " + ControlChars.CrLf
							                + "  from vwEMAILS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								if ( !bPast )
									DATE_TIME                      = Sql.ToDateTime(rdr["DATE_TIME"                     ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								DESCRIPTION_HTML               = Sql.ToString  (rdr["DESCRIPTION_HTML"              ]);
								FROM_ADDR                      = Sql.ToString  (rdr["FROM_ADDR"                     ]);
								FROM_NAME                      = Sql.ToString  (rdr["FROM_NAME"                     ]);
								TO_ADDRS                       = Sql.ToString  (rdr["TO_ADDRS"                      ]);
								CC_ADDRS                       = Sql.ToString  (rdr["CC_ADDRS"                      ]);
								BCC_ADDRS                      = Sql.ToString  (rdr["BCC_ADDRS"                     ]);
								TO_ADDRS_IDS                   = Sql.ToString  (rdr["TO_ADDRS_IDS"                  ]);
								TO_ADDRS_NAMES                 = Sql.ToString  (rdr["TO_ADDRS_NAMES"                ]);
								TO_ADDRS_EMAILS                = Sql.ToString  (rdr["TO_ADDRS_EMAILS"               ]);
								CC_ADDRS_IDS                   = Sql.ToString  (rdr["CC_ADDRS_IDS"                  ]);
								CC_ADDRS_NAMES                 = Sql.ToString  (rdr["CC_ADDRS_NAMES"                ]);
								CC_ADDRS_EMAILS                = Sql.ToString  (rdr["CC_ADDRS_EMAILS"               ]);
								BCC_ADDRS_IDS                  = Sql.ToString  (rdr["BCC_ADDRS_IDS"                 ]);
								BCC_ADDRS_NAMES                = Sql.ToString  (rdr["BCC_ADDRS_NAMES"               ]);
								BCC_ADDRS_EMAILS               = Sql.ToString  (rdr["BCC_ADDRS_EMAILS"              ]);
								TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								MESSAGE_ID                     = Sql.ToString  (rdr["MESSAGE_ID"                    ]);
								REPLY_TO_NAME                  = Sql.ToString  (rdr["REPLY_TO_NAME"                 ]);
								REPLY_TO_ADDR                  = Sql.ToString  (rdr["REPLY_TO_ADDR"                 ]);
								INTENT                         = Sql.ToString  (rdr["INTENT"                        ]);
								MAILBOX_ID                     = Sql.ToGuid    (rdr["MAILBOX_ID"                    ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TIME_START                     = Sql.ToDateTime(rdr["TIME_START"                    ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("EmailActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("EMAILS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spEMAILS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, DATE_TIME
								, PARENT_TYPE
								, PARENT_ID
								, DESCRIPTION
								, DESCRIPTION_HTML
								, FROM_ADDR
								, FROM_NAME
								, TO_ADDRS
								, CC_ADDRS
								, BCC_ADDRS
								, TO_ADDRS_IDS
								, TO_ADDRS_NAMES
								, TO_ADDRS_EMAILS
								, CC_ADDRS_IDS
								, CC_ADDRS_NAMES
								, CC_ADDRS_EMAILS
								, BCC_ADDRS_IDS
								, BCC_ADDRS_NAMES
								, BCC_ADDRS_EMAILS
								, TYPE
								, MESSAGE_ID
								, REPLY_TO_NAME
								, REPLY_TO_ADDR
								, INTENT
								, MAILBOX_ID
								, TEAM_ID
								, TEAM_SET_LIST
								, TAG_SET_NAME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("EmailActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

