
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.Workflow.Activities.Rules;
using SplendidCRM._modules;
using System.Globalization;

namespace SplendidCRM.KPIB030301
{
    /// <summary>
    ///		Summary description for ImportView.
    /// </summary>
    public class ImportNetCardView : SplendidControl
    {
        #region Properties
        // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected PlaceHolder phDefaultsView;
        protected SplendidControl ctlDefaultsView;

        protected Guid gID;

        protected SplendidGrid grdMain;

        protected XmlDocument xml;
        protected XmlDocument xmlMapping;
        protected string sImportModule;
        protected HtmlInputFile fileIMPORT;
        protected RequiredFieldValidator reqFILENAME;
        protected CheckBox chkHasHeader;
        protected HtmlTable tblImportMappings;
        protected StringBuilder sbImport;

        protected Label lblStatus;
        protected Label lblSuccessCount;
        protected Label lblDuplicateCount;
        protected Label lblFailedCount;
        protected CheckBox chkUseTransaction;

        protected HiddenField txtACTIVE_TAB;
        protected bool bDuplicateFields = false;
        protected int nMAX_ERRORS = 200;

        protected DataTable dtRuleColumns;

        public string Module
        {
            get { return sImportModule; }
            set { sImportModule = value; }
        }
        #endregion


        protected void GenerateImport(string sTempFileName)
        {
            XmlReader xmlFile;
            xmlFile = XmlReader.Create(Path.Combine(Path.GetTempPath(), sTempFileName), new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            int nImported = 0;
            int nFailed = 0;

            DataTable dtCurrent = ds.Tables[0];

            if (dtCurrent.Rows.Count > 0)
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();

                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    // 11/01/2006 Paul.  The transaction is optional, just make sure to always dispose it. 
                    using (IDbTransaction trn = Sql.BeginTransaction(con))
                    {
                        try
                        {
                            for (int i = 7; i < dtCurrent.Rows.Count; i++)
                            {
                                DataRow rdr = dtCurrent.Rows[i];                                                       

                                int iYEAR = Sql.ToInteger(rdr["ImportField000"]);
                                string MONTH_PERIOD = Sql.ToString(rdr["ImportField001"]).Trim();

                                //Neu nguoi dung khong nhap nam va thang thi chon nam thang hien tai
                                if (iYEAR == 0) iYEAR = int.Parse(DateTime.Now.ToString("yyyy"));
                                if (String.IsNullOrEmpty(MONTH_PERIOD)) MONTH_PERIOD = DateTime.Now.ToString("MM");

                                if (MONTH_PERIOD.Length == 1) MONTH_PERIOD = "0" + MONTH_PERIOD;

                                decimal U_VS_DB_PROFIT_VND = Sql.ToDecimal(rdr["ImportField002"]);
                                decimal U_VS_DB_PROFIT_USD = Sql.ToDecimal(rdr["ImportField003"]);
                                decimal U_VS_DB_FIXED_COST = Sql.ToDecimal(rdr["ImportField004"]);
                                decimal U_VS_DB_EXP_COST = Sql.ToDecimal(rdr["ImportField005"]);
                                decimal U_VS_DB_COST_VND = Sql.ToDecimal(rdr["ImportField006"]);
                                decimal U_VS_DB_COST_USD = Sql.ToDecimal(rdr["ImportField007"]);
                                decimal U_MC_CREDIT_COST = Sql.ToDecimal(rdr["ImportField008"]);
                                decimal U_VS_CREDIT_PROFIT_VND = Sql.ToDecimal(rdr["ImportField009"]);
                                decimal U_VS_CREDIT_PROFIT_USD = Sql.ToDecimal(rdr["ImportField010"]);
                                decimal U_VS_CREDIT_FIXED_COST = Sql.ToDecimal(rdr["ImportField011"]);
                                decimal U_VS_CREDIT_EXP_COST = Sql.ToDecimal(rdr["ImportField012"]);
                                decimal U_VS_CREDIT_COST_VND = Sql.ToDecimal(rdr["ImportField013"]);
                                decimal U_VS_CREDIT_COST_USD = Sql.ToDecimal(rdr["ImportField014"]);
                                decimal U_MC_CREDIT_FEE = Sql.ToDecimal(rdr["ImportField015"]);
                                decimal U_CREDIT_FTP_COST = Sql.ToDecimal(rdr["ImportField016"]);


                                try
                                {
                                    Guid gID = Guid.Empty;
                                    KPIB030301_SQLProc.spB_KPI_TOI_NETCARD_RESULT_TMP_Update
                                            (ref gID
                                                , trn
                                                , iYEAR
                                                , MONTH_PERIOD
                                                , U_VS_DB_PROFIT_VND
                                                , U_VS_DB_PROFIT_USD
                                                , U_VS_DB_FIXED_COST
                                                , U_VS_DB_EXP_COST
                                                , U_VS_DB_COST_VND
                                                , U_VS_DB_COST_USD
                                                , U_MC_CREDIT_COST
                                                , U_VS_CREDIT_PROFIT_VND
                                                , U_VS_CREDIT_PROFIT_USD
                                                , U_VS_CREDIT_FIXED_COST
                                                , U_VS_CREDIT_EXP_COST
                                                , U_VS_CREDIT_COST_VND
                                                , U_VS_CREDIT_COST_USD
                                                , U_MC_CREDIT_FEE
                                                , U_CREDIT_FTP_COST
                                            );

                                    //Bo deu thu thuan the cho tung nhan su ban hang
                                    KPIB030301_SQLProc.spCAL_B_KPI_TOI_NETCARD_RESULT
                                                (trn
                                                    , iYEAR
                                                    , MONTH_PERIOD
                                                );

                                    //Success
                                    nImported++;
                                }
                                catch (Exception ex)
                                {
                                    //Fail
                                    nFailed++;

                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                }
                                
                            }//end for

                            trn.Commit();
                            SplendidCache.ClearFavorites();
                        }
                        catch (Exception ex)
                        {
                            trn.Rollback();
                            SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                            ctlDynamicButtons.ErrorText += ex.Message;
                            return;
                        }
                        finally
                        {
                            if (trn != null)
                                trn.Dispose();
                        }
                    }

                }
                lblStatus.Text = String.Empty;
                // 03/20/2011 Paul.  Include a preview indicator. 
                if (nFailed == 0)
                    lblStatus.Text += L10n.Term("Import.LBL_SUCCESS");
                else
                    lblStatus.Text += L10n.Term("Import.LBL_FAIL");
                lblSuccessCount.Text = nImported.ToString() + " " + L10n.Term("Import.LBL_SUCCESSFULLY");
                lblFailedCount.Text = nFailed.ToString() + " " + L10n.Term("Import.LBL_FAILED_IMPORT");
            }

        }

        protected string SourceType()
        {
            string sSourceType = "excel";
            return sSourceType;
        }


        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Import.Run")
                {
                    if (Page.IsValid)
                    {
                        // 10/10/2006 Paul.  The temp file name is stored in the session so that it is impossible for a hacker to access. 
                        string sTempFileID = Sql.ToString(ViewState["TempFileID"]);
                        string sTempFileName = Sql.ToString(Session["TempFile." + sTempFileID]);
                        if (Sql.IsEmptyString(sTempFileID) || Sql.IsEmptyString(sTempFileName))
                        {
                            throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                        }

                        SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Begin Import");
                        GenerateImport(sTempFileName);
                        SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "End Import");

                    }
                }
                else if (e.CommandName == "Import.Upload")
                {
                    reqFILENAME.Enabled = true;
                    reqFILENAME.Validate();
                    if (Page.IsValid)
                    {
                        HttpPostedFile pstIMPORT = fileIMPORT.PostedFile;
                        if (pstIMPORT != null)
                        {
                            if (pstIMPORT.FileName.Length > 0)
                            {
                                string sFILENAME = Path.GetFileName(pstIMPORT.FileName);
                                string sFILE_EXT = Path.GetExtension(sFILENAME);
                                string sFILE_MIME_TYPE = pstIMPORT.ContentType;

                                // 09/04/2010 Paul.  ACT Imports are taking a long time.  Time the stream conversion to see where the problem lies. 
                                SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Begin Upload: " + sFILENAME);
                                // 05/06/2011 Paul.  We need to be able to distinguish between Excel 2003 and Excel 2007. 
                                xml = SplendidImport.ConvertStreamToXml(sImportModule, SourceType(), string.Empty, pstIMPORT.InputStream, sFILE_EXT);

                                if (xml.DocumentElement == null)
                                    throw (new Exception(L10n.Term("Import.LBL_NOTHING")));

                                // 08/21/2006 Paul.  Don't move to next step if there is no data. 
                                XmlNodeList nlRows = xml.DocumentElement.SelectNodes(sImportModule.ToLower());
                                if (nlRows.Count == 0)
                                    throw (new Exception(L10n.Term("Import.LBL_NOTHING")));

                                // 10/10/2006 Paul.  Don't store the file name in the ViewState because a hacker could find a way to access and alter it.
                                // Storing the file name in the session and an ID in the view state should be sufficiently safe. 
                                string sTempFileID = Guid.NewGuid().ToString();
                                string sTempFileName = Security.USER_ID.ToString() + " " + Guid.NewGuid().ToString() + " " + sFILENAME + ".xml";
                                xml.Save(Path.Combine(Path.GetTempPath(), sTempFileName));
                                // 01/30/2010 Paul.  Were were not storing the full path in the Session for cleanup. 
                                Session["TempFile." + sTempFileID] = Path.Combine(Path.GetTempPath(), sTempFileName);
                                ViewState["TempFileID"] = sTempFileID;
                                SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "End Upload: " + sFILENAME);

                                lblSuccessCount.Text = sFILENAME.ToString() + " " + L10n.Term(".LBL_UPLOAD_SUCCESSFUL");
                            }
                        }
                    }
                    if (xml != null)
                    {
                        if (xml.DocumentElement == null)
                            throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                    }
                    else
                    {
                        throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                    }
                }
                else if (e.CommandName == "Cancel")
                {
                    string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
                    if (Sql.IsEmptyString(sRelativePath))
                        sRelativePath = "~/" + sImportModule + "/";
                    Response.Redirect(sRelativePath);
                }
                else if (e.CommandName == "Import.Download")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "utf-8";
                    Response.ClearContent();
                    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=Template_import_net_card_2018_v1.0.xlsx");
                    Response.TransmitFile(Server.MapPath("~/Import/Template/Template_import_net_card_2018_v1.0.xlsx"));
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                //SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText += ex.Message;
                return;
            }
        }


        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(sImportModule + ".LBL_MODULE_NAME"));

            this.Visible = Security.IS_ADMIN || (SplendidCRM.Security.GetUserAccess(sImportModule, "import") >= 0);
            if (!this.Visible)
            {
                // 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
                Parent.DataBind();
                return;
            }

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    // 09/06/2012 Paul.  Allow direct import into prospect list. 
                    // 10/22/2013 Paul.  Title was not getting set properly. 
                    ViewState["ctlDynamicButtons.Title"] = L10n.Term(sImportModule + ".LBL_MODULE_NAME");
                    string sMODULE_TABLE = Sql.ToString(Application["Modules." + sImportModule + ".TableName"]);
                    dtRuleColumns = SplendidCache.SqlColumns("vw" + sMODULE_TABLE + "_List");
                    ViewState["RULE_COLUMNS"] = dtRuleColumns;
                }
                else
                {
                    // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(ctlDynamicButtons.Title);

                }
            }
            catch (Exception ex)
            {
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // 09/17/2013 Paul.  Add Business Rules to import. 

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            this.m_sMODULE = "Import";
            // 07/21/2010 Paul.  Make sure to highlight the correct menu item. 
            SetMenu(sImportModule);

            string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
            if (Sql.IsEmptyString(sRelativePath))
            {
                // 10/14/2014 Paul.  Correct module name. 
                if (sImportModule == "Project" || sImportModule == "ProjectTask")
                    sImportModule += "s";
                sRelativePath = "~/" + sImportModule + "/";
            }

            // 04/29/2008 Paul.  Make use of dynamic buttons. 
            ctlDynamicButtons.AppendButtons(m_sMODULE + ".ImportView", Guid.Empty, Guid.Empty);
            // 04/27/2018 Paul.  We need to be able to generate an error message. 
            if (IsPostBack)
            {
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}

