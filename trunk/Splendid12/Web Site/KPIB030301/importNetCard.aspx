<%@ Page language="c#" MasterPageFile="~/DefaultView.Master" Codebehind="import.aspx.cs" AutoEventWireup="false" Inherits="SplendidCRM.SplendidPage" %>
<asp:Content ID="cntSidebar" ContentPlaceHolderID="cntSidebar" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="Shortcuts" Src="~/_controls/Shortcuts.ascx" %>
	<SplendidCRM:Shortcuts ID="ctlShortcuts" SubMenu="KPIB030301" Runat="Server" />
</asp:Content>

<asp:Content ID="cntBody" ContentPlaceHolderID="cntBody" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="ImportNetCardView" Src="~/KPIB030301/ImportNetCardView.ascx" %>
	<SplendidCRM:ImportNetCardView ID="ctlImportNetCardView" Module="KPIB030301" Visible='<%# SplendidCRM.Security.GetUserAccess("KPIB030301", "import") >= 0 %>' Runat="Server" />
	<asp:Label ID="lblAccessError" ForeColor="Red" EnableViewState="false" Text='<%# L10n.Term("ACL.LBL_NO_ACCESS") %>' Visible="<%# !ctlImportNetCardView.Visible %>" Runat="server" />
</asp:Content>

