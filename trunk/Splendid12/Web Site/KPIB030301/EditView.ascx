<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB030301.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIB030301" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_NO">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                            <asp:HiddenField ID="txtKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                            <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="B_KPI_ACT_RESULT_DETAIL.LBL_LIST_SYNC_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" HorizontalAlign="Right" />
                        <ItemStyle CssClass="dataField" HorizontalAlign="Right" />
                        <ItemTemplate>
                            <asp:Label ID="lblSYNC_VALUE" Text='<%# Bind("SYNC_VALUE") %>' runat="server" CssClass="right"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="B_KPI_ACT_RESULT_DETAIL.LBL_LIST_FINAL_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" HorizontalAlign="Right" />
                        <ItemStyle CssClass="dataField" HorizontalAlign="Right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtFINAL_VALUE" Text='<%# Bind("FINAL_VALUE") %>' runat="server" CssClass="right"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>

    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
