﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB030301
{

    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;


        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblSYNC_VALUE = (Label)e.Item.FindControl("lblSYNC_VALUE");
                if (lblSYNC_VALUE != null)
                {
                    lblSYNC_VALUE.Text = KPIs_Utils.FormatFloat(lblSYNC_VALUE.Text, format_number);
                }
                Label lblFINAL_VALUE = (Label)e.Item.FindControl("lblFINAL_VALUE");
                if (lblFINAL_VALUE != null)
                {
                    lblFINAL_VALUE.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE.Text, format_number);
                }
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("edit.aspx?ID=" + gID.ToString());
                }
                else if (e.CommandName == "Duplicate")
                {
                    Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    SqlProcs.spB_KPI_TOI_ACTUAL_RESULT_Delete(gID);
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "Cancel")
                {
                    Response.Redirect("default.aspx");
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_TOI_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID, "ID", false);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            Page.Items["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            string approveStatus = rdr["APPROVE_STATUS"].ToString();
                                            if (!KPIs_Constant.KPI_APPROVE_STATUS_APPROVE.Equals(approveStatus) && !KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT.Equals(approveStatus))
                                            {
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            }
                                            ctlDynamicButtons.AppendProcessButtons(rdr);

                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            //oang
                                            KPIs_Utils.LoadEmployee_Info_Detail(this, dbf, Guid.Parse(rdr["EMPLOYEE_ID"].ToString()));
                                        }
                                        else
                                        {
                                            plcSubPanel.Visible = false;
                                            grdMain.Visible = false;
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.AppendProcessButtons(null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            //18/10/2018 Tungnx: sua phan duplicate do chuyen phong ban
                            //var sSQL_DETAILS = "SELECT  Row_Number() OVER(order by KPI_CODE) AS NO, ID, KPI_ID, KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, DESCRIPTION    " + ControlChars.CrLf
                            //            + "  FROM vwB_KPI_TOI_ACT_RESULT_DETAIL_Edit  WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_TOI_ACTUAL_RESULT_Edit WHERE ID = '" + gID.ToString() + "')   ORDER BY KPI_CODE ASC " + ControlChars.CrLf;

                            var sSQL_DETAILS = " SELECT   " + ControlChars.CrLf
                                                + "  Row_Number() OVER(order by KPI_CODE) AS NO, KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, DESCRIPTION   " + ControlChars.CrLf
                                                + "  FROM(" + ControlChars.CrLf
                                                + "  SELECT   " + ControlChars.CrLf
                                                + "  KPI_CODE, KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, DESCRIPTION, sum(SYNC_VALUE) as SYNC_VALUE, sum(FINAL_VALUE) as FINAL_VALUE   " + ControlChars.CrLf
                                                + "  FROM vwB_KPI_TOI_ACT_RESULT_DETAIL_Edit   " + ControlChars.CrLf
                                                + "  WHERE ACTUAL_RESULT_CODE IN(SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_TOI_ACTUAL_RESULT_Edit WHERE ID = '" + gID.ToString() + "')   " + ControlChars.CrLf
                                                + "  GROUP BY  KPI_CODE, KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, DESCRIPTION   " + ControlChars.CrLf
                                                + "  ) TMP ORDER BY KPI_CODE ASC   " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                //Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            grdMain.DataSource = dtCurrent;
                                            decimal totalSync = 0;
                                            decimal totalFinal = 0;
                                            for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                            {
                                                totalSync += Sql.ToDecimal(dtCurrent.Rows[i]["SYNC_VALUE"].ToString());
                                                totalFinal += Sql.ToDecimal(dtCurrent.Rows[i]["FINAL_VALUE"].ToString());
                                            }

                                            string format_number = Sql.ToString(Application["CONFIG.format_number"]);

                                            grdMain.Columns[3].FooterText = L10n.Term("KPIB030301.LBL_MODULE_ABBREVIATION");//"Tổng";
                                            grdMain.Columns[4].FooterText = KPIs_Utils.FormatFloat(totalSync.ToString(), format_number);
                                            grdMain.Columns[4].FooterStyle.HorizontalAlign = HorizontalAlign.Right;
                                            grdMain.Columns[5].FooterText = KPIs_Utils.FormatFloat(totalFinal.ToString(), format_number);
                                            grdMain.Columns[5].FooterStyle.HorizontalAlign = HorizontalAlign.Right;

                                            grdMain.DataBind();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.AppendProcessButtons(null);
                        ctlDynamicButtons.DisableAll();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }

                //ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "edit") > 0 ? true : false;
                
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            m_sMODULE = "KPIB030301";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                ctlDynamicButtons.AppendProcessButtons(null);
            }
        }
        #endregion
    }
}
