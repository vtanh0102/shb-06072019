using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB030301
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected SplendidGrid grdMain;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                //Label lblSYNC_VALUE = (Label)e.Item.FindControl("lblSYNC_VALUE");
                //if (lblSYNC_VALUE != null)
                //{
                //    lblSYNC_VALUE.Text = KPIs_Utils.FormatFloat(lblSYNC_VALUE.Text, format_number);
                //}
                //TextBox lblFINAL_VALUE = (TextBox)e.Item.FindControl("txtFINAL_VALUE");
                //if (lblFINAL_VALUE != null)
                //{
                //    lblFINAL_VALUE.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE.Text, format_number);
                //}
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwB_KPI_TOI_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;
                                    var year = new SplendidCRM.DynamicControl(this, null, "YEAR").SelectedValue;
                                    SqlProcs.spB_KPI_TOI_ACTUAL_RESULT_Update
                                        (ref gID
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ACTUAL_RESULT_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "MONTH_PERIOD").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "EMPLOYEE_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "USER_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_BY").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_SYNC_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_FINAL_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_MANUAL_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_01").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_02").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_03").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FILE_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "LATEST_SYNC_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_ID").ID
                                        , KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                        , trn
                                        );

                                    if (ViewState["CurrentTable"] != null)
                                    {
                                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                        if (dtCurrentTable.Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                            {
                                                HiddenField hdnfID = (HiddenField)grdMain.Items[i].FindControl("txtID");
                                                HiddenField hdnKPIID = (HiddenField)grdMain.Items[i].FindControl("txtKPI_ID");
                                                Label kpiName = (Label)grdMain.Items[i].FindControl("txtKPI_NAME");
                                                Label unit = (Label)grdMain.Items[i].FindControl("txtKPI_UNIT");
                                                HiddenField unitID = (HiddenField)grdMain.Items[i].FindControl("hdUnitId");
                                                //Label ratio = (Label)grdMain.Items[i].FindControl("txtKPI_RATIO");

                                                //Label planValue = (Label)grdMain.Items[i].FindControl("txtPLAN_VALUE");
                                                Label synValue = (Label)grdMain.Items[i].FindControl("txtSYNC_VALUE");
                                                TextBox finalValue = (TextBox)grdMain.Items[i].FindControl("txtFINAL_VALUE");
                                                TextBox description = (TextBox)grdMain.Items[i].FindControl("txtDESCRIPTION");
                                                Guid gIDT;
                                                Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                                {
                                                    gIDT = Guid.NewGuid();
                                                }
                                                else
                                                {
                                                    if (hdnfID.Value == null || hdnfID.Value == string.Empty)
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        gIDT = Guid.Parse(hdnfID.Value);
                                                    }
                                                }
                                                KPIB030301_SQLProc.spB_KPI_TOI_ACT_RESULT_DETAIL_Update_Final_Value
                                        (ref gIDT
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , string.Empty
                                        , Sql.ToDecimal(finalValue.Text)
                                        , description.Text
                                        , string.Empty
                                        , trn
                                        );
                                            }
                                            ViewState["CurrentTable"] = dtCurrentTable;
                                        }


                                    }

                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    SplendidCRM.SqlProcs.spTRACKER_Update
                                        (Security.USER_ID
                                        , m_sMODULE
                                        , gID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ACTUAL_RESULT_CODE").Text
                                        , "save"
                                        , trn
                                        );
                                    if (plcSubPanel.Visible)
                                    {
                                        foreach (Control ctl in plcSubPanel.Controls)
                                        {
                                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                            if (ctlSubPanel != null)
                                            {
                                                ctlSubPanel.Save(gID, m_sMODULE, trn);
                                            }
                                        }
                                    }
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_TOI_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    cmd.CommandText += string.Format(" AND ISNULL(APPROVE_STATUS,'') <> '{0}' AND ISNULL(APPROVE_STATUS,'') <> '{1}'", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);

                                            //TextBox txtNAME = this.FindControl("ACTUAL_RESULT_CODE") as TextBox;
                                            //if ( txtNAME != null )
                                            //    txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["ACTUAL_RESULT_CODE"] = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["ACTUAL_RESULT_CODE"] = ViewState["ACTUAL_RESULT_CODE"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            KPIs_Utils.LoadEmployee_Info(this, dbf, Guid.Parse(rdr["EMPLOYEE_ID"].ToString()));
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                            grdMain.Visible = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (Sql.IsEmptyGuid(gID))
                        {
                            Load_KPI_Act_Result_Detail(gDuplicateID);
                        }
                        else
                        {
                            Load_KPI_Act_Result_Detail(gID);
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("ACTUAL_RESULT_CODE") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["ACTUAL_RESULT_CODE"] = ViewState["ACTUAL_RESULT_CODE"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }


        private void Load_KPI_Act_Result_Detail(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            string sSQL_DETAILS = "";

            //DETAILS LIST                           
            using (IDbConnection con = dbf.CreateConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    sSQL_DETAILS = "SELECT '' AS NO, ID, KPI_ID, KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, DESCRIPTION    " + ControlChars.CrLf
                                        + "  FROM vwB_KPI_TOI_ACT_RESULT_DETAIL_Edit  WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_TOI_ACTUAL_RESULT_Edit WHERE ID = '" + gID.ToString() + "') ORDER BY KPI_CODE ASC  " + ControlChars.CrLf;
                    cmd.CommandText = sSQL_DETAILS;
                    //Security.Filter(cmd, m_sMODULE, "edit");
                    //Sql.AppendParameter(cmd, gID.ToString(), "KPI_ACTUAL_RESULT_ID");
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            grdMain.DataSource = dtCurrent;
                            grdMain.DataBind();

                            if (dtCurrent.Rows.Count > 0)
                            {
                                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                                ViewState["CurrentTable"] = dtCurrent;
                                for (int i = 0; i < grdMain.Items.Count; i++)
                                {
                                    Label lblNO = (Label)grdMain.Items[i].FindControl("lblNO");
                                    HiddenField id = (HiddenField)grdMain.Items[i].FindControl("txtID");
                                    HiddenField kpiID = (HiddenField)grdMain.Items[i].FindControl("txtKPI_ID");
                                    Label kpiName = (Label)grdMain.Items[i].FindControl("txtKPI_NAME");
                                    Label unit = (Label)grdMain.Items[i].FindControl("txtKPI_UNIT");
                                    HiddenField unitID = (HiddenField)grdMain.Items[i].FindControl("hdUnitId");
                                    //Label ratio = (Label)grdMain.Items[i].FindControl("txtKPI_RATIO");
                                    //Label planValue = (Label)grdMain.Items[i].FindControl("txtPLAN_VALUE");
                                    Label synValue = (Label)grdMain.Items[i].FindControl("lblSYNC_VALUE");
                                    TextBox finalValue = (TextBox)grdMain.Items[i].FindControl("txtFINAL_VALUE");
                                    TextBox description = (TextBox)grdMain.Items[i].FindControl("txtDESCRIPTION");
                                    //SET VALUE
                                    lblNO.Text = (i + 1).ToString();
                                    if (!Sql.IsEmptyGuid(gID))
                                        id.Value = dtCurrent.Rows[i]["ID"].ToString();
                                    else
                                        id.Value = string.Empty;
                                    kpiID.Value = dtCurrent.Rows[i]["KPI_ID"].ToString();
                                    kpiName.Text = dtCurrent.Rows[i]["KPI_NAME"].ToString();
                                    unit.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", dtCurrent.Rows[i]["KPI_UNIT"].ToString());
                                    unitID.Value = dtCurrent.Rows[i]["KPI_UNIT"].ToString();
                                    //ratio.Text = dtCurrent.Rows[i]["RATIO"] != null ? dtCurrent.Rows[i]["RATIO"].ToString() : string.Empty;
                                    //planValue.Text = dtCurrent.Rows[i]["PLAN_VALUE"] != null ? dtCurrent.Rows[i]["PLAN_VALUE"].ToString() : string.Empty;
                                    string strSynValue = dtCurrent.Rows[i]["SYNC_VALUE"] != null ? dtCurrent.Rows[i]["SYNC_VALUE"].ToString() : string.Empty;
                                    string strFinalValue = dtCurrent.Rows[i]["FINAL_VALUE"] != null ? dtCurrent.Rows[i]["FINAL_VALUE"].ToString() : string.Empty;
                                    synValue.Text = KPIs_Utils.FormatFloat(strSynValue, format_number);
                                    finalValue.Text = KPIs_Utils.FormatFloat(strFinalValue, format_number);
                                    description.Text = dtCurrent.Rows[i]["DESCRIPTION"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIB030301";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
