using System;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.KPIB030301
{
	/// <summary>
	///		Summary description for MassUpdate.
	/// </summary>
	public class MassUpdate : SplendidCRM.MassUpdate
	{
        protected _controls.MassUpdateButtons ctlDynamicButtons;
        protected bool bShowAssigned = true;
        public CommandEventHandler Command;
        protected _controls.TeamAssignedMassUpdate ctlTeamAssignedMassUpdate;
        protected _controls.TagMassApprove ctlTagMassApprove;
        protected _controls.TagMassUpdate ctlTagMassUpdate;

        public string detailGID = "";

        public bool ShowAssigned
        {
            get { return bShowAssigned; }
            set { bShowAssigned = value; }
        }

        public Guid ASSIGNED_USER_ID
        {
            get
            {
                return ctlTagMassApprove.ASSIGNED_USER;
            }
        }

        public Guid APPROVED_BY
        {
            get
            {
                return ctlTagMassApprove.ASSIGNED_USER;
            }
        }

        public Guid PRIMARY_TEAM_ID
        {
            get
            {
                return ctlTeamAssignedMassUpdate.PRIMARY_TEAM_ID;
            }
        }

        public string TEAM_SET_LIST
        {
            get
            {
                return ctlTeamAssignedMassUpdate.TEAM_SET_LIST;
            }
        }

        public bool ADD_TEAM_SET
        {
            get
            {
                return ctlTeamAssignedMassUpdate.ADD_TEAM_SET;
            }
        }

        public string TAG_SET_NAME
        {
            get
            {
                return ctlTagMassUpdate.TAG_SET_NAME;
            }
        }

        public bool ADD_TAG_SET
        {
            get
            {
                return ctlTagMassUpdate.ADD_TAG_SET;
            }
        }

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			// Command is handled by the parent. 
			if ( Command != null )
				Command(this, e) ;
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if ( !IsPostBack )
				{
                    //int nACLACCESS_Delete = Security.GetUserAccess(m_sMODULE, "delete");
                    //int nACLACCESS_Edit   = Security.GetUserAccess(m_sMODULE, "edit"  );
                    //ctlDynamicButtons.ShowButton("MassUpdate", nACLACCESS_Edit   >= 0);
                    //ctlDynamicButtons.ShowButton("MassDelete", nACLACCESS_Delete >= 0);
                    //20180920 TuanDN: Add
                    int submitApproval = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_SUBMIT_APPROVAL, m_sMODULE, detailGID);
                    int approval = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_APPROVAL, m_sMODULE, detailGID);
                    int reject = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_REJECT, m_sMODULE, detailGID);
                    ctlDynamicButtons.ShowButton("MassSubmitApproval", submitApproval >= 0);
                    ctlDynamicButtons.ShowButton("MassApprove", approval >= 0);
                    ctlDynamicButtons.ShowButton("MassReject", reject >= 0);

                    if ((submitApproval > 0) || (approval > 0) || (reject > 0))
                    {
                        this.Visible = (Security.GetUserAccess(m_sMODULE, "list") > 0) ? true : false;
                    }
                    else
                    {
                        this.Visible = false;
                    }
                }
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIB030301";
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".MassUpdate", Guid.Empty, null);
		}
		#endregion
	}
}
