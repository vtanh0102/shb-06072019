using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web.Services;
using System.ComponentModel;
using SplendidCRM;

namespace SplendidCRM.KPIB030301
{
	public class KPIB030301
	{
		public Guid    ID  ;
		public string  ACTUAL_RESULT_CODE;

		public KPIB030301()
		{
			ID   = Guid.Empty  ;
			ACTUAL_RESULT_CODE = String.Empty;
		}
	}

	/// <summary>
	/// Summary description for AutoComplete
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.Web.Script.Services.ScriptService]
	[ToolboxItem(false)]
	public class AutoComplete : System.Web.Services.WebService
	{
		[WebMethod(EnableSession=true)]
		public KPIB030301 B_KPI_TOI_ACTUAL_RESULT_B_KPI_TOI_ACTUAL_RESULT_ACTUAL_RESULT_CODE_Get(string sACTUAL_RESULT_CODE)
		{
			KPIB030301 item = new KPIB030301();
			//try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select ID        " + ControlChars.CrLf
					     + "     , ACTUAL_RESULT_CODE      " + ControlChars.CrLf
					     + "  from vwB_KPI_TOI_ACTUAL_RESULT" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                        //Security.Filter(cmd, "KPIB030301", "list");

                        Sql.AppendParameter(cmd, sACTUAL_RESULT_CODE, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "ACTUAL_RESULT_CODE");
						cmd.CommandText += " order by ACTUAL_RESULT_CODE" + ControlChars.CrLf;
						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								item.ID   = Sql.ToGuid   (rdr["ID"  ]);
								item.ACTUAL_RESULT_CODE = Sql.ToString (rdr["ACTUAL_RESULT_CODE"]);
							}
						}
					}
				}
				if ( Sql.IsEmptyGuid(item.ID) )
				{
					string sCULTURE = Sql.ToString (Session["USER_SETTINGS/CULTURE"]);
					L10N L10n = new L10N(sCULTURE);
					throw(new Exception(L10n.Term("KPIB030301.ERR_B_KPI_TOI_ACTUAL_RESULT_NOT_FOUND")));
				}
			}
			//catch
			{
				// 02/04/2007 Paul.  Don't catch the exception.  
				// It is a web service, so the exception will be handled properly by the AJAX framework. 
			}
			return item;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_TOI_ACTUAL_RESULT_B_KPI_TOI_ACTUAL_RESULT_ACTUAL_RESULT_CODE_List(string prefixText, int count)
		{
			string[] arrItems = new string[0];
			try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sSQL;
					sSQL = "select distinct  " + ControlChars.CrLf
					     + "       ACTUAL_RESULT_CODE      " + ControlChars.CrLf
					     + "  from vwB_KPI_TOI_ACTUAL_RESULT" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                        //Security.Filter(cmd, "KPIB030301", "list");

                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "ACTUAL_RESULT_CODE");
						cmd.CommandText += " order by ACTUAL_RESULT_CODE" + ControlChars.CrLf;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(0, count, dt);
								arrItems = new string[dt.Rows.Count];
								for ( int i=0; i < dt.Rows.Count; i++ )
									arrItems[i] = Sql.ToString(dt.Rows[i]["ACTUAL_RESULT_CODE"]);
							}
						}
					}
				}
			}
			catch
			{
			}
			return arrItems;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_TOI_ACTUAL_RESULT_ACTUAL_RESULT_CODE_List(string prefixText, int count)
		{
			return B_KPI_TOI_ACTUAL_RESULT_B_KPI_TOI_ACTUAL_RESULT_ACTUAL_RESULT_CODE_List(prefixText, count);
		}
	}
}

