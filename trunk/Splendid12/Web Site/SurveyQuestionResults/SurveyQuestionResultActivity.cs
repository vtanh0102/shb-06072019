/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// SurveyQuestionResultActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class SurveyQuestionResultActivity: SplendidActivity
	{
		public SurveyQuestionResultActivity()
		{
			this.Name = "SurveyQuestionResultActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty SURVEY_RESULT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_RESULT_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_RESULT_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.SURVEY_RESULT_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.SURVEY_RESULT_IDProperty, value); }
		}

		public static DependencyProperty SURVEY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.SURVEY_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.SURVEY_IDProperty, value); }
		}

		public static DependencyProperty SURVEY_PAGE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_PAGE_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_PAGE_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.SURVEY_PAGE_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.SURVEY_PAGE_IDProperty, value); }
		}

		public static DependencyProperty SURVEY_QUESTION_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_QUESTION_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_QUESTION_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.SURVEY_QUESTION_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.SURVEY_QUESTION_IDProperty, value); }
		}

		public static DependencyProperty ANSWER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ANSWER_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ANSWER_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.ANSWER_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.ANSWER_IDProperty, value); }
		}

		public static DependencyProperty ANSWER_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ANSWER_TEXT", typeof(string), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ANSWER_TEXT
		{
			get { return ((string)(base.GetValue(SurveyQuestionResultActivity.ANSWER_TEXTProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.ANSWER_TEXTProperty, value); }
		}

		public static DependencyProperty COLUMN_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COLUMN_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid COLUMN_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.COLUMN_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.COLUMN_IDProperty, value); }
		}

		public static DependencyProperty COLUMN_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COLUMN_TEXT", typeof(string), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string COLUMN_TEXT
		{
			get { return ((string)(base.GetValue(SurveyQuestionResultActivity.COLUMN_TEXTProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.COLUMN_TEXTProperty, value); }
		}

		public static DependencyProperty MENU_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MENU_ID", typeof(Guid), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MENU_ID
		{
			get { return ((Guid)(base.GetValue(SurveyQuestionResultActivity.MENU_IDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.MENU_IDProperty, value); }
		}

		public static DependencyProperty MENU_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MENU_TEXT", typeof(string), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MENU_TEXT
		{
			get { return ((string)(base.GetValue(SurveyQuestionResultActivity.MENU_TEXTProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.MENU_TEXTProperty, value); }
		}

		public static DependencyProperty WEIGHTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WEIGHT", typeof(Int32), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 WEIGHT
		{
			get { return ((Int32)(base.GetValue(SurveyQuestionResultActivity.WEIGHTProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.WEIGHTProperty, value); }
		}

		public static DependencyProperty OTHER_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OTHER_TEXT", typeof(string), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OTHER_TEXT
		{
			get { return ((string)(base.GetValue(SurveyQuestionResultActivity.OTHER_TEXTProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.OTHER_TEXTProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(SurveyQuestionResultActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty QUESTION_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUESTION_TYPE", typeof(string), typeof(SurveyQuestionResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUESTION_TYPE
		{
			get { return ((string)(base.GetValue(SurveyQuestionResultActivity.QUESTION_TYPEProperty))); }
			set { base.SetValue(SurveyQuestionResultActivity.QUESTION_TYPEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("SurveyQuestionResultActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("SurveyQuestionResultActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select SURVEY_QUESTIONS_RESULTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwSURVEY_QUESTIONS_RESULTS_AUDIT        SURVEY_QUESTIONS_RESULTS          " + ControlChars.CrLf
							                + " inner join vwSURVEY_QUESTIONS_RESULTS_AUDIT        SURVEY_QUESTIONS_RESULTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on SURVEY_QUESTIONS_RESULTS_AUDIT_OLD.ID = SURVEY_QUESTIONS_RESULTS.ID       " + ControlChars.CrLf
							                + "        and SURVEY_QUESTIONS_RESULTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwSURVEY_QUESTIONS_RESULTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                                  from vwSURVEY_QUESTIONS_RESULTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                                 where vwSURVEY_QUESTIONS_RESULTS_AUDIT.ID            =  SURVEY_QUESTIONS_RESULTS.ID           " + ControlChars.CrLf
							                + "                                                                   and vwSURVEY_QUESTIONS_RESULTS_AUDIT.AUDIT_VERSION <  SURVEY_QUESTIONS_RESULTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                                   and vwSURVEY_QUESTIONS_RESULTS_AUDIT.AUDIT_TOKEN   <> SURVEY_QUESTIONS_RESULTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                               )" + ControlChars.CrLf
							                + " where SURVEY_QUESTIONS_RESULTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                              " + ControlChars.CrLf
							                + "  from vwSURVEY_QUESTIONS_RESULTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwSURVEY_QUESTIONS_RESULTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                              " + ControlChars.CrLf
							                + "  from vwSURVEY_QUESTIONS_RESULTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								SURVEY_RESULT_ID               = Sql.ToGuid    (rdr["SURVEY_RESULT_ID"              ]);
								SURVEY_ID                      = Sql.ToGuid    (rdr["SURVEY_ID"                     ]);
								SURVEY_PAGE_ID                 = Sql.ToGuid    (rdr["SURVEY_PAGE_ID"                ]);
								SURVEY_QUESTION_ID             = Sql.ToGuid    (rdr["SURVEY_QUESTION_ID"            ]);
								ANSWER_ID                      = Sql.ToGuid    (rdr["ANSWER_ID"                     ]);
								ANSWER_TEXT                    = Sql.ToString  (rdr["ANSWER_TEXT"                   ]);
								COLUMN_ID                      = Sql.ToGuid    (rdr["COLUMN_ID"                     ]);
								COLUMN_TEXT                    = Sql.ToString  (rdr["COLUMN_TEXT"                   ]);
								MENU_ID                        = Sql.ToGuid    (rdr["MENU_ID"                       ]);
								MENU_TEXT                      = Sql.ToString  (rdr["MENU_TEXT"                     ]);
								WEIGHT                         = Sql.ToInteger (rdr["WEIGHT"                        ]);
								OTHER_TEXT                     = Sql.ToString  (rdr["OTHER_TEXT"                    ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								if ( !bPast )
								{
									QUESTION_TYPE                  = Sql.ToString  (rdr["QUESTION_TYPE"                 ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SurveyQuestionResultActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("SURVEY_QUESTIONS_RESULTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spSURVEY_QUESTIONS_RESULTS_Update
								( ref gID
								, SURVEY_RESULT_ID
								, SURVEY_ID
								, SURVEY_PAGE_ID
								, SURVEY_QUESTION_ID
								, ANSWER_ID
								, ANSWER_TEXT
								, COLUMN_ID
								, COLUMN_TEXT
								, MENU_ID
								, MENU_TEXT
								, WEIGHT
								, OTHER_TEXT
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SurveyQuestionResultActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

