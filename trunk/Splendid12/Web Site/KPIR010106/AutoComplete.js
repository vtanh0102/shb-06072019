
function KPIR010106_KPIR010106_NAME_Changed(fldKPIR010106_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldKPIR010106_NAME.id.substring(0, fldKPIR010106_NAME.id.length - 'KPIR010106_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010106_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_KPIR010106_NAME = document.getElementById(userContext + 'PREV_KPIR010106_NAME');
	if ( fldPREV_KPIR010106_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_KPIR010106_NAME');
	}
	else if ( fldPREV_KPIR010106_NAME.value != fldKPIR010106_NAME.value )
	{
		if ( fldKPIR010106_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIR010106.AutoComplete.KPIR010106_KPIR010106_NAME_Get(fldKPIR010106_NAME.value, KPIR010106_KPIR010106_NAME_Changed_OnSucceededWithContext, KPIR010106_KPIR010106_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('KPIR010106_KPIR010106_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			KPIR010106_KPIR010106_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function KPIR010106_KPIR010106_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'KPIR010106_NAME_AjaxErrors');
		var fldKPIR010106_ID        = document.getElementById(userContext + 'KPIR010106_ID'       );
		var fldKPIR010106_NAME      = document.getElementById(userContext + 'KPIR010106_NAME'     );
		var fldPREV_KPIR010106_NAME = document.getElementById(userContext + 'PREV_KPIR010106_NAME');
		if ( fldKPIR010106_ID        != null ) fldKPIR010106_ID.value        = sID  ;
		if ( fldKPIR010106_NAME      != null ) fldKPIR010106_NAME.value      = sNAME;
		if ( fldPREV_KPIR010106_NAME != null ) fldPREV_KPIR010106_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIR010106.AutoComplete service is null');
	}
}

function KPIR010106_KPIR010106_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010106_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldKPIR010106_ID        = document.getElementById(userContext + 'KPIR010106_ID'       );
	var fldKPIR010106_NAME      = document.getElementById(userContext + 'KPIR010106_NAME'     );
	var fldPREV_KPIR010106_NAME = document.getElementById(userContext + 'PREV_KPIR010106_NAME');
	if ( fldKPIR010106_ID        != null ) fldKPIR010106_ID.value        = '';
	if ( fldKPIR010106_NAME      != null ) fldKPIR010106_NAME.value      = '';
	if ( fldPREV_KPIR010106_NAME != null ) fldPREV_KPIR010106_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

