<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIM0101.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    .chzn-container {
        margin-top: 11px;
    }

    input[readonly] {
        background-color: transparent;
        border: 0;
        font-size: 1em;
    }

    input[select] {
        width: 200px !important;
    }

    .gridHeaderLabel-Gray {
        padding: 5px 30px 5px 5px;
    }

    #ctl00_cntBody_ctlEditView_TYPE_chzn {
        width: 276px !important;
    }

    .txaLabel {
        background-color: transparent;
        border: 0;
        font-size: 1em;
        resize: none;
    }

    .dropdown-wrapper {
        width: 115% /* This hides the arrow icon */;
        background-color: transparent /* This hides the background */;
        background-image: none;
        -webkit-appearance: none /* Webkit Fix */;
        border: none;
        box-shadow: none;
        padding: 0.3em 0.5em;
    }
</style>

<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIM0101" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="5%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_CODE" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtKPI_CODE" Text='<%# Bind("KPI_CODE") %>' runat="server" />
                            <asp:HiddenField ID="hdfID" Value='<%# Bind("ID") %>' runat="server" />
                            <asp:HiddenField ID="hdfKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_NAME">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtKPI_NAME" CssClass="txaLabel" Text='<%# Bind("KPI_NAME") %>' ToolTip='<%# Bind("KPI_NAME") %>' runat="server" TextMode="MultiLine" Wrap="true" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LIST_PARENT_NAME" ItemStyle-Width="20%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" Wrap="true" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtPARENT_NAME" CssClass="txaLabel" Text='<%# Bind("PARENT_NAME") %>' ToolTip='<%# Bind("PARENT_NAME") %>' TextMode="MultiLine" runat="server" Wrap="true" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_UNIT" ItemStyle-Width="20%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlUNIT" runat="server" DataTextField="UNIT" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_RATIO" ItemStyle-Width="10%" HeaderStyle-CssClass="left">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtRATIO" Text='<%# Bind("RATIO") %>' runat="server" CssClass="format-qty number right" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_MAX_RATIO_COMPL" ItemStyle-Width="20%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMAX_RATIO_COMPL" Text='<%# Bind("MAX_RATIO_COMPLETE") %>' runat="server" CssClass="format-qty number right" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_REMARK" ItemStyle-Width="50%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtREMARK" Text='<%# Bind("REMARK") %>' runat="server" TextMode="MultiLine" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemTemplate>
                            <asp:ImageButton Visible='<%# Container.ItemIndex != grdMain.EditItemIndex && SplendidCRM.Security.GetUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Delete" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' SkinID="delete_inline" runat="server" />
                            <asp:LinkButton Visible='<%# Container.ItemIndex != grdMain.EditItemIndex && SplendidCRM.Security.GetUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Delete" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_DELETE") %>' runat="server" />
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="btnInsert" CommandName="Insert" Text='<%# L10n.Term(".LBL_ADD_BUTTON_LABEL") %>' Visible='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server" CssClass="addNewRowIntoGrid" />
                        </FooterTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>

            </SplendidCRM:InlineScript>
            <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
            <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript">
                    $(function () {
                        //$("[id$=ctl00_cntBody_ctlEditView_TYPE]").chosen();
                        $("[id$=ctl00_cntBody_ctlEditView_ORGANIZATION_ID]").chosen();
                        $("[id$=ctl00_cntBody_ctlEditView_POSITION_ID]").chosen();
                        $("[id$=ctl00_cntBody_ctlEditView_IS_COMMON]").closest("tr").hide();
                        $("[id$=ctl00_cntBody_ctlEditView_ORGANIZATION_ID_chzn]").closest("tr").hide();
                        var editId = getUrlParameter('ID');
                        if (editId == null) {
                            $("[id$=ctl00_cntBody_ctlEditView_ASSIGNED_TO]").closest("tr").hide();
                            $('[id$=ctl00_cntBody_ctlEditView_ctlDynamicButtons_ctl00_lblTitle]')
                        }

                        $("[id$=txtRATIO]").keydown(function (e) {
                            // Allow: backspace, delete, tab, escape, enter and .
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                                // Allow: Ctrl+A, Command+A
                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                                // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                                // let it happen, don't do anything
                                return;
                            }
                            // Ensure that it is a number and stop the keypress
                            //if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                            //    e.preventDefault();
                            //}
                        });

                        $("[id$=txtMAX_RATIO_COMPL]").keydown(function (e) {
                            // Allow: backspace, delete, tab, escape, enter and .
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                                // Allow: Ctrl+A, Command+A
                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                                // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                                // let it happen, don't do anything
                                return;
                            }
                            // Ensure that it is a number and stop the keypress
                            //if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                            //    e.preventDefault();
                            //}
                        });

                        $("[id$=txtKPI_CODE]").autocomplete({
                            source: function (request, response) {
                                $.ajax({
                                    url: '<%=ResolveUrl("~/KPIM0101/AutoComplete.asmx/GetKPIM02") %>',
                                    data: "{ 'prefixText': '" + request.term + "', 'count': 10}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('-')[0],
                                                val: item.split('-')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        alert(response.responseText);
                                    },
                                    failure: function (response) {
                                        alert(response.responseText);
                                    }
                                });
                            },
                            select: function (e, i) {
                                //console.log(i.item);
                                GetKPIM02_Detail(this, i.item.value);
                            },
                            change: function (event, ui) {
                                if (!ui.item) {
                                    this.value = '';
                                }
                            },
                            minLength: 1,
                            mustMatch: true,
                            selectFirst: true
                        });
                    });

                    function GetKPIM02_Detail(thizz, code) {
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/KPIM0101/AutoComplete.asmx/GetKPIM02_Detail") %>',
                            data: '{CODE: "' + code + '", count: 10 }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var objData = $.parseJSON(data.d);
                                var json = objData[0];
                                console.log(json);
                                $(thizz).closest("tr").find("input[id$=hdfKPI_ID]").val(json['ID']);
                                $(thizz).closest("tr").find("[id$=txtKPI_NAME]").val(json['KPI_NAME']);
                                var parentID = json['PARENT_ID'];
                                if (parentID !== null) {
                                    //$(thizz).closest("tr").find("[id$=txtPARENT_NAME]").val(json['KPI_NAME']);
                                    $.ajax({
                                        type: "POST",
                                        url: '<%=ResolveUrl("~/KPIM0101/AutoComplete.asmx/GetKPIM02_PARENT") %>',
                                        data: '{PARENT_ID: "' + parentID + '", count: 10 }',
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data2) {
                                            var objData2 = $.parseJSON(data2.d);
                                            var json2 = objData2[0];
                                            $(thizz).closest("tr").find("[id$=txtPARENT_NAME]").val(json2['KPI_NAME']);
                                        }, failure: function (response) {
                                            alert(response.d);
                                        }
                                    });
                                }
                                $(thizz).closest("tr").find("[id$=ddlUNIT]").val(json['UNIT']);
                                $(thizz).closest("tr").find("[id$=txtRATIO]").val(json['RATIO']);
                                $(thizz).closest("tr").find("[id$=txtMAX_RATIO_COMPL]").val(json['MAX_RATIO_COMPLETE']);
                                $(thizz).closest("tr").find("[id$=txtREMARK]").val(json['REMARK']);
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    }

                    function getUrlParameter(sParam) {
                        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                            sURLVariables = sPageURL.split('&'),
                            sParameterName,
                            i;
                        for (i = 0; i < sURLVariables.length; i++) {
                            sParameterName = sURLVariables[i].split('=');

                            if (sParameterName[0] === sParam) {
                                return sParameterName[1] === undefined ? true : sParameterName[1];
                            }
                        }
                    };

                    // http://www.isocra.com/2008/02/table-drag-and-drop-jquery-plugin/
                    $(document).ready(function () {

                        $("#<%= grdMain.ClientID %>").tableDnD({
                            dragHandle: "dragHandle",
                            onDragClass: "jQueryDragBorder",
                            onDragStart: function (table, row) {
                                var txtINDEX = document.getElementById('<%= txtINDEX.ClientID %>');
                                txtINDEX.value = (row.parentNode.rowIndex - 1);
                            },
                            onDrop: function (table, row) {
                                var txtINDEX = document.getElementById('<%= txtINDEX.ClientID %>');
                                txtINDEX.value += ',' + (row.rowIndex - 1);
                                document.getElementById('<%= btnINDEX_MOVE.ClientID %>').click();
                            }
                        });
                        $("#<%= grdMain.ClientID %> tr").hover(
                            function () {
                                if (!$(this).hasClass("nodrag"))
                                    $(this.cells[0]).addClass('jQueryDragHandle');
                            },
                            function () {
                                if (!$(this).hasClass("nodrag"))
                                    $(this.cells[0]).removeClass('jQueryDragHandle');
                            }
                        );
                    });
                </script>
            </SplendidCRM:InlineScript>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
