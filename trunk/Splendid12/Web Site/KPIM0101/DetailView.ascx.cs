using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIM0101
{

    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;
        protected MassUpdate ctlMassUpdate;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("lblUNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }

                HiddenField hdfPARENT_ID = (HiddenField)e.Item.FindControl("hdfPARENT_ID");
                if (hdfPARENT_ID != null)
                {
                    var parentId = hdfPARENT_ID.Value;
                    if (parentId != null && parentId != string.Empty)
                    {
                        Label lblPARENT_NAME = (Label)e.Item.FindControl("lblPARENT_NAME");
                        if (lblPARENT_NAME != null)
                        {
                            string parentName = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(parentId), "vwM_KPIS_List", "KPI_NAME");
                            lblPARENT_NAME.Text = parentName;
                        }
                    }
                }
                //get format from config
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);

                Label lblRATIO = (Label)e.Item.FindControl("lblRATIO");
                if (lblRATIO != null)
                {
                    lblRATIO.Text = KPIs_Utils.FormatFloat(lblRATIO.Text, format_number);
                }
                Label lblMAX_RATIO_COMPL = (Label)e.Item.FindControl("lblMAX_RATIO_COMPL");
                if (lblMAX_RATIO_COMPL != null)
                {
                    if (Sql.ToFloat(lblMAX_RATIO_COMPL.Text) == 0)
                    {
                        lblMAX_RATIO_COMPL.Text = string.Empty;
                    }
                    else
                    {
                        lblMAX_RATIO_COMPL.Text = KPIs_Utils.FormatFloat(lblMAX_RATIO_COMPL.Text, format_number);
                    }
                }
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("edit.aspx?ID=" + gID.ToString());
                }
                else if (e.CommandName == "Duplicate")
                {
                    Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    KPIs_Utils.KPI_DETAILS_Delete(gID);
                    KPIM0101_SqlProc.spM_GROUP_KPIS_Delete(gID);
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "Cancel")
                {
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "MassSubmitApproval")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIM0101_SqlProc.spM_GROUP_KPI_MassApprove(sIDs, ctlMassUpdate.APPROVED_BY, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassApprove
                else if (e.CommandName == "MassApprove")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIM0101_SqlProc.spM_GROUP_KPI_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }

                //MassReject
                else if (e.CommandName == "MassReject")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIM0101_SqlProc.spM_GROUP_KPI_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_REJECT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID, "ID", false);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["KPI_GROUP_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            Page.Items["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            //ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            string approveStatus = rdr["APPROVE_STATUS"].ToString();
                                            if (!KPIs_Constant.KPI_APPROVE_STATUS_APPROVE.Equals(approveStatus) && !KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT.Equals(approveStatus))
                                            {
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            }
                                            ctlDynamicButtons.AppendProcessButtons(rdr);

                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            var userId = new DynamicControl(this, "APPROVED_BY").Text;

                                            new DynamicControl(this, "APPROVED_BY").Text = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(userId), "vwUSERS", "USER_NAME");

                                        }
                                        else
                                        {
                                            plcSubPanel.Visible = false;

                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.AppendProcessButtons(null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }
                        //DETAILS LIST                           
                        using (IDbConnection con = dbf.CreateConnection())
                        {

                            string sSQL_DETAILS = " SELECT Row_Number() OVER(order by MG.DATE_ENTERED) AS NO, MG.ID, MG.KPI_ID, MG.KPI_CODE, MG.KPI_NAME, MG.UNIT, MG.RATIO, MG.MAX_RATIO_COMPLETE, MG.DESCRIPTION, MG.REMARK     " + ControlChars.CrLf
                                                + " , MK.PARENT_ID, '' AS PARENT_NAME   " + ControlChars.CrLf
                                                + " FROM vwM_GROUP_KPI_DETAILS_List MG  inner join vwM_KPIS_Edit MK ON (MG.KPI_ID = MK.ID)" + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AppendParameter(cmd, gID, "GROUP_KPI_ID", false);
                                cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            grdMain.DataSource = dtCurrent;
                                            grdMain.DataBind();
                                        }
                                    }
                                }
                            }

                        }
                        //End Details list
                    }
                    else
                    {
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.AppendProcessButtons(null);
                        ctlDynamicButtons.DisableAll();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }
                ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "list") > 0 ? true : false;

                //01/10/2018 Tungnx: check approval button
                ctlMassUpdate.detailGID = gID.ToString();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIM0101";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                ctlDynamicButtons.AppendProcessButtons(null);
            }
        }
        #endregion
    }
}
