<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIM0101.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    .listViewThS1 td {
        height: 20px;
        padding: 22px 54px 5px 0px;
        text-align: -webkit-center;
    }

    .right {
        padding-left: 50px;
    }

    .listView_padding {
        padding-left: 15px;
    }

    .grdMain_title {
        padding-right: 145px !important;
    }
</style>
<div id="divDetailView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIM0101" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIM0101" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_NO">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_CODE" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblKPI_CODE" runat="server" Text='<%# Eval("KPI_CODE") %>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_NAME" ItemStyle-Width="30%">
                        <HeaderStyle CssClass="grdMain_title" />
                        <ItemTemplate>
                            <asp:Label ID="lblKPI_NAME" Text='<%# Eval("KPI_NAME") %>' runat="server" CssClass="listView_padding" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LIST_PARENT_NAME" ItemStyle-Width="30%">
                        <HeaderStyle CssClass="grdMain_title" />
                        <ItemTemplate>
                            <asp:Label ID="lblPARENT_NAME" Text='<%# Eval("PARENT_NAME") %>' runat="server" CssClass="listView_padding" />
                            <asp:HiddenField ID="hdfPARENT_ID" Value='<%# Eval("PARENT_ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_KPI_UNIT" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblUNIT" runat="server" Text='<%# Eval("UNIT") %>' CssClass="listView_padding" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_RATIO" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:Label ID="lblRATIO" Text='<%# Eval("RATIO") %>' runat="server" CssClass="right" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_MAX_RATIO_COMPL" ItemStyle-Width="10%">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                        <ItemStyle CssClass="dataField" />
                        <ItemTemplate>
                            <asp:Label ID="lblMAX_RATIO_COMPL" Text='<%# Eval("MAX_RATIO_COMPLETE") %>' runat="server" CssClass="right" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_REMARK" ItemStyle-Width="80%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblDESC" Text='<%# Eval("REMARK") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>
        </ContentTemplate>
    </asp:UpdatePanel>

</div>
<SplendidCRM:InlineScript runat="server">
    <script type="text/javascript">
        $(function () {
            var approveStatus = $("[id$=APPROVE_STATUS]").text();
            if (approveStatus.match("^0")) {
                $("[id$=APPROVED_BY]").closest("tr").hide();
            }
        });
    </script>
</SplendidCRM:InlineScript>
<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
