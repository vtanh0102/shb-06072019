using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIM0101
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        //detail info
        protected DataView vwMain;
        protected SplendidGrid grdMain;
        protected bool bEnableAdd = true;
        protected DataView CartView;
        protected static ArrayList gIDeleteRowList = new ArrayList();
        protected ListControl ddlType;
        protected DropDownList ddlOrganization;
        protected DropDownList ddlPosition;
        protected DropDownList ddlIS_COMMON;
        protected DropDownList ddlBusinessCode;

        Label CONTRACT_TYPE_C_LABEL;
        Label CONTRACT_TYPE_C_REQUIRED_SYMBOL;        
        protected DropDownList ddlContractTypeC;
        
        DropDownList ddlAPPROVE_STATUS;

        private void FirstDataGridViewRow()
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("NO", typeof(string)));
                dt.Columns.Add(new DataColumn("ID", typeof(string)));
                dt.Columns.Add(new DataColumn("KPI_ID", typeof(string)));
                dt.Columns.Add(new DataColumn("KPI_CODE", typeof(string)));
                dt.Columns.Add(new DataColumn("KPI_NAME", typeof(string)));
                dt.Columns.Add(new DataColumn("PARENT_NAME", typeof(string)));
                dt.Columns.Add(new DataColumn("UNIT", typeof(string)));
                dt.Columns.Add(new DataColumn("RATIO", typeof(string)));
                dt.Columns.Add(new DataColumn("MAX_RATIO_COMPLETE", typeof(string)));
                dt.Columns.Add(new DataColumn("REMARK", typeof(string)));
                dr = dt.NewRow();
                dr["NO"] = 1;
                dr["ID"] = string.Empty;
                dr["KPI_ID"] = string.Empty;
                dr["KPI_CODE"] = string.Empty;
                dr["KPI_NAME"] = string.Empty;
                dr["PARENT_NAME"] = string.Empty;
                dr["UNIT"] = string.Empty;
                dr["RATIO"] = string.Empty;
                dr["MAX_RATIO_COMPLETE"] = string.Empty;
                dr["REMARK"] = string.Empty;
                dt.Rows.Add(dr);

                ViewState["CurrentTable"] = dt;

                grdMain.DataSource = dt;
                grdMain.DataBind();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void grdMain_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.CssClass += " nodrag nodrop";
            }

        }

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlUNIT = (e.Item.Cells[4].FindControl("ddlUNIT") as DropDownList);
                Load_DropdownListUNIT(ddlUNIT);
                TextBox txtKPI_NAME = (TextBox)e.Item.FindControl("txtKPI_NAME");
                if (txtKPI_NAME != null)
                {
                    txtKPI_NAME.Attributes.Add("readonly", "readonly");
                    txtKPI_NAME.Wrap = true;
                }
                //PARENT_NAME
                TextBox txtPARENT_NAME = (TextBox)e.Item.FindControl("txtPARENT_NAME");
                if (txtPARENT_NAME != null)
                {
                    txtPARENT_NAME.Attributes.Add("readonly", "readonly");
                    txtPARENT_NAME.Wrap = true;
                }
            }
        }

        private void Load_DropdownListUNIT(DropDownList ddlUNIT)
        {
            if (ddlUNIT != null)
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "SELECT NAME, DISPLAY_NAME   " + ControlChars.CrLf
                         + "  FROM vwTERMINOLOGY_List   " + ControlChars.CrLf
                         + " WHERE 1 = 1                " + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        Sql.AppendParameter(cmd, "CURRENCY_UNIT_LIST", 50, Sql.SqlFilterMode.Exact, "LIST_NAME");
                        Sql.AppendParameter(cmd, L10n.NAME.ToLower(), 10, Sql.SqlFilterMode.Exact, "LANG");
                        cmd.CommandText += " ORDER BY LIST_ORDER";

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);

                                ddlUNIT.DataTextField = "DISPLAY_NAME";
                                ddlUNIT.DataValueField = "NAME";
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    ddlUNIT.DataSource = dt;
                                    ddlUNIT.DataBind();
                                }
                            }
                        }
                    }

                }
            }
        }

        private void AddNewRow()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");

                        //HiddenField kpiID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfID");
                        HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfKPI_ID");
                        TextBox txtKPI_CODE = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_CODE");

                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        TextBox parentName = (TextBox)grdMain.Items[rowIndex].Cells[4].FindControl("txtPARENT_NAME");

                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[5].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[6].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[8].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        //dtCurrentTable.Rows[i - 1]["NO"] = i + 1; 
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = hdfKPI_ID.Value;
                        dtCurrentTable.Rows[i - 1]["KPI_CODE"] = txtKPI_CODE.Text;
                        dtCurrentTable.Rows[i - 1]["KPI_NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["PARENT_NAME"] = parentName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text;
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text;
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;
                    //bind data grid
                    grdMain.DataSource = dtCurrentTable;
                    grdMain.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        private void AddNewRowForEdit()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        //HiddenField kpiID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdfID");
                        HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfKPI_ID");
                        TextBox txtKPI_CODE = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_CODE");

                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        TextBox parentName = (TextBox)grdMain.Items[rowIndex].Cells[4].FindControl("txtPARENT_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[5].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[6].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[8].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        //dtCurrentTable.Rows[i - 1]["NO"] = i + 1; 
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = hdfKPI_ID.Value;
                        dtCurrentTable.Rows[i - 1]["KPI_CODE"] = txtKPI_CODE.Text;
                        dtCurrentTable.Rows[i - 1]["KPI_NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["PARENT_NAME"] = parentName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text == string.Empty ? 0 : double.Parse(ratio.Text);
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text == string.Empty ? 0 : double.Parse(maxRatioComplete.Text);
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    grdMain.DataSource = dtCurrentTable;
                    grdMain.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfKPI_ID");
                        TextBox txtKPI_CODE = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_CODE");

                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        TextBox parenName = (TextBox)grdMain.Items[rowIndex].Cells[4].FindControl("txtPARENT_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[5].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[6].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[8].FindControl("txtREMARK");

                        lblNO.Text = (i + 1).ToString();
                        hdfKPI_ID.Value = dt.Rows[i]["KPI_ID"].ToString();
                        txtKPI_CODE.Text = dt.Rows[i]["KPI_CODE"].ToString();
                        kpiName.Text = dt.Rows[i]["KPI_NAME"].ToString();
                        parenName.Text = dt.Rows[i]["PARENT_NAME"].ToString();
                        unit.SelectedValue = dt.Rows[i]["UNIT"].ToString();
                        ratio.Text = dt.Rows[i]["RATIO"].ToString();
                        maxRatioComplete.Text = dt.Rows[i]["MAX_RATIO_COMPLETE"].ToString();
                        remark.Text = dt.Rows[i]["REMARK"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        private void SetPreviousDataForEdit()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfKPI_ID");
                        TextBox txtKPI_CODE = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_CODE");

                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        TextBox parentName = (TextBox)grdMain.Items[rowIndex].Cells[4].FindControl("txtPARENT_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[5].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[6].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[8].FindControl("txtREMARK");

                        lblNO.Text = (i + 1).ToString();
                        hdfKPI_ID.Value = dt.Rows[i]["KPI_ID"].ToString();
                        txtKPI_CODE.Text = dt.Rows[i]["KPI_CODE"].ToString();
                        kpiName.Text = dt.Rows[i]["KPI_NAME"].ToString();
                        parentName.Text = dt.Rows[i]["PARENT_NAME"].ToString();
                        unit.SelectedValue = dt.Rows[i]["UNIT"].ToString();//unit.Items.FindByText(dt.Rows[i]["UNIT"].ToString()).Selected = true;
                        ratio.Text = dt.Rows[i]["RATIO"].ToString();
                        maxRatioComplete.Text = dt.Rows[i]["MAX_RATIO_COMPLETE"].ToString();
                        remark.Text = dt.Rows[i]["REMARK"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        private void SetRowData()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        Label lblNO = (Label)grdMain.Items[rowIndex].Cells[1].FindControl("lblNO");
                        HiddenField kpiID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfID");
                        HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[rowIndex].Cells[2].FindControl("hdfKPI_ID");
                        TextBox txtKPI_CODE = (TextBox)grdMain.Items[rowIndex].Cells[2].FindControl("txtKPI_CODE");

                        TextBox kpiName = (TextBox)grdMain.Items[rowIndex].Cells[3].FindControl("txtKPI_NAME");
                        TextBox parentName = (TextBox)grdMain.Items[rowIndex].Cells[4].FindControl("txtPARENT_NAME");
                        DropDownList unit = (DropDownList)grdMain.Items[rowIndex].Cells[5].FindControl("ddlUNIT");
                        TextBox ratio = (TextBox)grdMain.Items[rowIndex].Cells[6].FindControl("txtRATIO");
                        TextBox maxRatioComplete = (TextBox)grdMain.Items[rowIndex].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                        TextBox remark = (TextBox)grdMain.Items[rowIndex].Cells[8].FindControl("txtREMARK");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["NO"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["ID"] = kpiID.Value;
                        dtCurrentTable.Rows[i - 1]["KPI_ID"] = hdfKPI_ID.Value;
                        dtCurrentTable.Rows[i - 1]["KPI_CODE"] = txtKPI_CODE.Text;
                        dtCurrentTable.Rows[i - 1]["KPI_NAME"] = kpiName.Text;
                        dtCurrentTable.Rows[i - 1]["PARENT_NAME"] = parentName.Text;
                        dtCurrentTable.Rows[i - 1]["UNIT"] = unit.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["RATIO"] = ratio.Text;
                        dtCurrentTable.Rows[i - 1]["MAX_RATIO_COMPLETE"] = maxRatioComplete.Text;
                        dtCurrentTable.Rows[i - 1]["REMARK"] = remark.Text;
                        rowIndex++;
                    }

                    ViewState["CurrentTable"] = dtCurrentTable;
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
        }

        private void grdMain_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "Edit":
                        {
                            grdMain.EditItemIndex = e.Item.ItemIndex;
                            grdMain.ShowFooter = false;
                            grdMain.DataBind();
                            break;
                        }
                    case "Cancel":
                        {
                            grdMain.EditItemIndex = -1;
                            grdMain.ShowFooter = bEnableAdd;
                            grdMain.DataBind();
                            break;
                        }
                    case "Delete":
                        {
                            if (Sql.IsEmptyGuid(gID))
                            {
                                SetRowData();
                                if (ViewState["CurrentTable"] != null)
                                {
                                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                                    DataRow drCurrentRow = null;
                                    int rowIndex = Convert.ToInt32(e.Item.ItemIndex);
                                    if (dt.Rows.Count > 1)
                                    {
                                        dt.Rows.Remove(dt.Rows[rowIndex]);
                                        drCurrentRow = dt.NewRow();
                                        ViewState["CurrentTable"] = dt;
                                        grdMain.DataSource = dt;
                                        grdMain.DataBind();
                                        SetPreviousData();
                                    }
                                }
                            }
                            else
                            {
                                if (ViewState["CurrentTable"] != null)
                                {
                                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                                    DataRow drCurrentRow = null;
                                    int rowIndex = Convert.ToInt32(e.Item.ItemIndex);
                                    if (dt.Rows.Count > 1)
                                    {
                                        //dt.Rows[rowIndex];
                                        HiddenField kpiID_Details = (HiddenField)e.Item.FindControl("hdfID");
                                        Guid gIDeleteRow = Sql.ToGuid(kpiID_Details.Value);
                                        if (!Sql.IsEmptyGuid(gIDeleteRow))
                                        {
                                            gIDeleteRowList.Add(gIDeleteRow);
                                        }

                                        dt.Rows.Remove(dt.Rows[rowIndex]);
                                        drCurrentRow = dt.NewRow();
                                        ViewState["CurrentTable"] = dt;
                                        grdMain.DataSource = dt;
                                        grdMain.DataBind();
                                        SetPreviousDataForEdit();
                                    }
                                }
                            }
                            break;
                        }
                    case "Update":
                        {
                            Guid gCase = Sql.ToGuid(Request["ID"]);
                            Guid gID = Sql.ToGuid(vwMain[e.Item.ItemIndex]["ID_C"]);
                            grdMain.EditItemIndex = -1;
                            grdMain.ShowFooter = bEnableAdd;
                            break;
                        }
                    case "Insert":
                        {
                            grdMain.ShowFooter = bEnableAdd;

                            if (Sql.IsEmptyGuid(gID))
                            {
                                AddNewRow();
                            }
                            else
                            {
                                AddNewRowForEdit();
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {

                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }
                            bool bDUPLICATE_CHECKING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && (e.CommandName != "SaveDuplicate") && Sql.IsEmptyGuid(gID);
                            if (bDUPLICATE_CHECKING_ENABLED)
                            {
                                //validate duplicate code
                                var code = new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_GROUP_CODE").Text;
                                if (Utils.DuplicateCheckCode(Application, con, m_sMODULE, code, "KPI_GROUP_CODE", this, rowCurrent) > 0)
                                {
                                    //ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    //ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    TextBox txtCODE = this.FindControl("KPI_GROUP_CODE") as TextBox;
                                    string code_prefix = Sql.ToString(Application["CONFIG.kpi_group_code_prefix"]);
                                    string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwM_GROUP_KPIS_List", "KPI_GROUP_CODE");
                                    if (txtCODE != null)
                                    {
                                        txtCODE.ReadOnly = true;
                                        txtCODE.Text = strKPIsCode;
                                    }
                                    Label lblVERSION_NUMBER = this.FindControl("VERSION_NUMBER") as Label;
                                    if (lblVERSION_NUMBER != null)
                                    {
                                        lblVERSION_NUMBER.Text = strKPIsCode.Split('-')[1].ToString();
                                    }
                                    throw (new Exception(string.Format(L10n.Term(".ERR_DUPLICATE_CODE_EXCEPTION"), code)));
                                }
                            }

                            //check validate Ratio
                            if (ViewState["CurrentTable"] != null)
                            {
                                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                                Hashtable duplicatedFields = new Hashtable();

                                if (dtCurrentTable.Rows.Count > 0)
                                {
                                    Decimal totalRatio = 0;
                                    for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                    {
                                        TextBox kpiCODE = (TextBox)grdMain.Items[i].Cells[2].FindControl("txtKPI_CODE");
                                        if (kpiCODE.Text == string.Empty)
                                        {
                                            throw (new Exception(string.Format("{0} {1}", L10n.Term(".LBL_KPI_CODE"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                        }
                                        if (duplicatedFields.ContainsKey(kpiCODE.Text))
                                        {
                                            throw (new Exception(string.Format(L10n.Term("KPIM0101.ERR_DUPLICATE_EXCEPTION_KPI_CODE"), kpiCODE.Text)));
                                        }
                                        else
                                        {
                                            duplicatedFields.Add(kpiCODE.Text, kpiCODE.Text);
                                        }

                                        TextBox ratio = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtRATIO");
                                        if (ratio.Text != string.Empty)
                                        {
                                            totalRatio += Decimal.Parse(ratio.Text);
                                        }
                                        if (ratio.Text == string.Empty)
                                        {
                                            throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM0101.KPIS_TAGET_RATIO"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                        }
                                    }
                                    if (totalRatio != KPIs_Utils.MAX_RATIO)
                                    {
                                        throw (new Exception(string.Format(L10n.Term(".ERR_NOT_EQUAL_TOTAL"), L10n.Term("KPIM0101.KPIS_TAGET_RATIO").ToLower())));
                                    }
                                }
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;
                                    var organizationId = new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID;
                                    string organizationCode = KPIs_Utils.getNameFromGuidID(organizationId, "M_ORGANIZATION", "ORGANIZATION_CODE");
                                    ddlType = this.FindControl("TYPE") as ListControl;
                                    if (ddlType != null)
                                    {
                                        if (ddlType.SelectedValue == string.Empty)
                                        {
                                            throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM0101.LBL_LIST_TYPE"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                        }
                                        ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
                                        if (ddlIS_COMMON != null)
                                        {
                                            Int32 active = Int32.Parse(ddlIS_COMMON.SelectedValue);
                                            if (active == KPIs_Utils.INACTIVE)
                                            {
                                                Int32 intType = int.Parse(ddlType.SelectedItem.Value);
                                                if (intType == KPIs_Utils.POSITION_ID)
                                                {
                                                    ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
                                                    if (ddlPosition != null)
                                                    {
                                                        if (ddlPosition.SelectedValue == string.Empty)
                                                        {
                                                            throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM0101.LBL_LIST_POSITION_ID"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
                                                    if (ddlOrganization != null)
                                                    {
                                                        if (ddlOrganization.SelectedValue == string.Empty)
                                                        {
                                                            throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM0101.LBL_LIST_ORGANIZATION_ID"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //tuandn check duplicate kpi group
                                        var nYEAR = new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue;
                                        var type = ddlType.SelectedValue;
                                        string typeName = ddlType.Items.Count > 0 ? ddlType.SelectedItem.Text : string.Empty;

                                        var businessCode = new SplendidCRM.DynamicControl(this, rowCurrent, "BUSINESS_CODE").Text;
                                        ListControl ddlBUSINESS_CODE = this.FindControl("BUSINESS_CODE") as ListControl;

                                        var contractTypeC = new SplendidCRM.DynamicControl(this, rowCurrent, "CONTRACT_TYPE_C").Text;
                                        
                                        string businessName = string.Empty;
                                        if (ddlBUSINESS_CODE != null)
                                        {
                                            businessName = ddlBUSINESS_CODE.Items.Count > 0 ? ddlBUSINESS_CODE.SelectedItem.Text : string.Empty;
                                        }
                                        //30/10/2018 Tungnx: Check duplicate by group keys
                                        bool kpisExisted = KPIs_Utils.spM_GROUP_KPIS_Existed(gID, Sql.ToInteger(nYEAR), Sql.ToInteger(type), businessCode, contractTypeC, trn);
                                        if (kpisExisted)
                                        {
                                            throw (new Exception(string.Format(L10n.Term("KPIM0101.ERR_KPI_GROUP_DUPLICATE"), nYEAR, typeName, businessName)));
                                        }

                                    }

                                    KPIM0101_SqlProc.spM_GROUP_KPIS_Update
                                        (ref gID
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_GROUP_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_GROUP_NAME").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_STATUS").Text//KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID == Guid.Empty ? Guid.Empty : new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue == DateTime.MinValue ? DateTime.Now.Date : new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "POSITION_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TYPE").IntegerValue
                                        , organizationId
                                        , organizationCode
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "BUSINESS_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "IS_COMMON").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                        , trn
                                        );

                                    if (ViewState["CurrentTable"] != null)
                                    {
                                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                        if (dtCurrentTable.Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                            {
                                                HiddenField hdnfID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdfID");

                                                HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdfKPI_ID");
                                                TextBox kpiCODE = (TextBox)grdMain.Items[i].Cells[2].FindControl("txtKPI_CODE");

                                                TextBox kpiName = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtKPI_NAME");
                                                DropDownList unit = (DropDownList)grdMain.Items[i].Cells[4].FindControl("ddlUNIT");
                                                TextBox ratio = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtRATIO");
                                                TextBox maxRatioComplete = (TextBox)grdMain.Items[i].Cells[6].FindControl("txtMAX_RATIO_COMPL");
                                                TextBox remark = (TextBox)grdMain.Items[i].Cells[7].FindControl("txtREMARK");
                                                Guid gIDT;
                                                Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                                {
                                                    gIDT = Guid.NewGuid();
                                                }
                                                else
                                                {
                                                    if (hdnfID.Value == null || hdnfID.Value == string.Empty)
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        gIDT = Guid.Parse(hdnfID.Value);
                                                    }
                                                }

                                                KPIM0101_SqlProc.spM_GROUP_KPI_DETAILS_Update
                                                     (ref gIDT
                                                     , gASSIGNED_USER_ID
                                                     , gTEAM_ID
                                                     , string.Empty
                                                     , Sql.ToGuid(hdfKPI_ID.Value)
                                                     , kpiCODE.Text
                                                     , kpiName.Text
                                                     , gID.ToString()
                                                     , Sql.ToInteger(unit.SelectedItem.Value)
                                                     , Sql.ToFloat(ratio.Text)
                                                     , Sql.ToFloat(maxRatioComplete.Text)
                                                     , remark.Text
                                                     , remark.Text
                                                     , string.Empty
                                                     , trn
                                                     );
                                            }
                                            ViewState["CurrentTable"] = dtCurrentTable;
                                            if (gIDeleteRowList != null && gIDeleteRowList.Count > 0)
                                            {
                                                foreach (var gIDeleteRow in gIDeleteRowList)
                                                {
                                                    KPIM0101_SqlProc.spM_GROUP_KPI_DETAILS_Delete(Guid.Parse(gIDeleteRow.ToString()), trn);
                                                }
                                            }
                                        }
                                    }

                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    SplendidCRM.SqlProcs.spTRACKER_Update
                                        (Security.USER_ID
                                        , m_sMODULE
                                        , gID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_GROUP_NAME").Text
                                        , "save"
                                        , trn
                                        );
                                    if (plcSubPanel.Visible)
                                    {
                                        foreach (Control ctl in plcSubPanel.Controls)
                                        {
                                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                            if (ctlSubPanel != null)
                                            {
                                                ctlSubPanel.Save(gID, m_sMODULE, trn);
                                            }
                                        }
                                    }
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {

                // 07/25/2010 Paul.  Lets experiment with jQuery drag and drop. 
                ScriptManager mgrAjax = ScriptManager.GetCurrent(this.Page);
                ScriptReference scrJQueryTableDnD = new ScriptReference("~/Include/javascript/jquery.tablednd_0_5.js");
                if (!mgrAjax.Scripts.Contains(scrJQueryTableDnD))
                    mgrAjax.Scripts.Add(scrJQueryTableDnD);
                //bEnableAdd = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);

                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwM_GROUP_KPIS_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    cmd.CommandText += string.Format(" AND ISNULL(APPROVE_STATUS,'') <> '{0}'", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                                    cmd.CommandText += string.Format(" AND ISNULL(APPROVE_STATUS,'') <> '{0}'", KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["KPI_GROUP_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            if (!Sql.IsEmptyGuid(gDuplicateID))
                                            {
                                                TextBox txtCODE = this.FindControl("KPI_GROUP_CODE") as TextBox;
                                                string code_prefix = Sql.ToString(Application["CONFIG.kpi_group_code_prefix"]);
                                                string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwM_GROUP_KPIS_List", "KPI_GROUP_CODE");
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.ReadOnly = true;
                                                    txtCODE.Text = strKPIsCode;
                                                }
                                                ddlAPPROVE_STATUS = this.FindControl("APPROVE_STATUS") as DropDownList;
                                                if (ddlAPPROVE_STATUS != null)
                                                {
                                                    ddlAPPROVE_STATUS.SelectedIndex = 0;
                                                }
                                            }
                                            else
                                            {
                                                TextBox txtCODE = this.FindControl("KPI_GROUP_CODE") as TextBox;
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.ReadOnly = true;
                                                }
                                            }
                                            TextBox txtNAME = this.FindControl("KPI_GROUP_NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();
                                            DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                                            if (ddlYEAR != null)
                                            {
                                                KPIs_Utils.FillDropdownListOfYear(ddlYEAR);
                                            }
                                            ddlType = this.FindControl("TYPE") as ListControl;
                                            if (ddlType != null)
                                            {
                                                Load_DropdownWithType(ddlType);
                                            }
                                            ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
                                            if (ddlOrganization != null)
                                            {
                                                string organizationValue = rdr["ORGANIZATION_ID"].ToString();
                                                if (!Sql.IsEmptyString(organizationValue))
                                                {
                                                    ddlOrganization.ClearSelection();
                                                    ddlOrganization.Items.FindByValue(organizationValue).Selected = true;
                                                }
                                            }
                                            ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
                                            if (ddlPosition != null)
                                            {
                                                string positonValue = rdr["POSITION_ID"].ToString();
                                                if (!Sql.IsEmptyString(positonValue))
                                                {
                                                    ddlPosition.ClearSelection();
                                                    ddlPosition.Items.FindByValue(positonValue).Selected = true;
                                                }
                                            }
                                            ddlType = this.FindControl("TYPE") as ListControl;
                                            if (ddlType != null)
                                            {
                                                ddlType.AutoPostBack = true;
                                                ddlType.SelectedIndexChanged += new EventHandler(ddlType_SelectedIndexChanged);
                                            }
                                            ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
                                            if (ddlIS_COMMON != null)
                                            {
                                                ddlIS_COMMON.AutoPostBack = true;
                                                ddlIS_COMMON.SelectedIndexChanged += new EventHandler(ddlIS_COMMON_SelectedIndexChanged);
                                            }

                                            ddlAPPROVE_STATUS = this.FindControl("APPROVE_STATUS") as DropDownList;
                                            if (ddlAPPROVE_STATUS != null)
                                            {
                                                ddlAPPROVE_STATUS.Enabled = false;
                                                ddlAPPROVE_STATUS.CssClass = "dropdown-wrapper";
                                            }
                                            HtmlInputHidden lblAPPROVE_STATUS_H = this.FindControl("APPROVE_STATUS_H") as HtmlInputHidden;
                                            if (lblAPPROVE_STATUS_H != null)
                                            {
                                                lblAPPROVE_STATUS_H.Value = rdr["APPROVE_STATUS"].ToString();
                                            }

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["KPI_GROUP_NAME"] = Sql.ToString(rdr["KPI_GROUP_NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["KPI_GROUP_NAME"] = ViewState["KPI_GROUP_NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }

                        }
                        if (Sql.IsEmptyGuid(gID))
                        {
                            Load_KPI_Detail(dbf, gDuplicateID);
                        }
                        else
                        {
                            if (plcSubPanel.Visible)
                            {
                                Load_KPI_Detail(dbf, gID);
                            }
                        }
                    }
                    else
                    {
                        FirstDataGridViewRow();

                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        //SET DEFAULT 
                        TextBox txtCODE = this.FindControl("KPI_GROUP_CODE") as TextBox;
                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_group_code_prefix"]);
                        string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwM_GROUP_KPIS_List", "KPI_GROUP_CODE");
                        if (txtCODE != null)
                        {
                            txtCODE.ReadOnly = true;
                            txtCODE.Text = strKPIsCode;
                        }
                        TextBox txtNAME = this.FindControl("KPI_GROUP_NAME") as TextBox;
                        if (txtNAME != null)
                        {
                            txtNAME.Focus();
                        }
                        DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                        if (ddlYEAR != null)
                        {
                            KPIs_Utils.FillDropdownListOfYear(ddlYEAR);
                        }
                        Label lblVERSION_NUMBER = this.FindControl("VERSION_NUMBER") as Label;
                        if (lblVERSION_NUMBER != null)
                        {
                            lblVERSION_NUMBER.Text = strKPIsCode.Split('-')[1].ToString();
                        }

                        ddlAPPROVE_STATUS = this.FindControl("APPROVE_STATUS") as DropDownList;
                        if (ddlAPPROVE_STATUS != null)
                        {
                            ddlAPPROVE_STATUS.Enabled = false;
                            ddlAPPROVE_STATUS.CssClass = "dropdown-wrapper";
                        }
                        ddlType = this.FindControl("TYPE") as ListControl;
                        if (ddlType != null)
                        {
                            ddlType.AutoPostBack = true;
                            Load_DropdownWithTypeInit(ddlType);
                            ddlType.SelectedIndexChanged += new EventHandler(ddlType_SelectedIndexChanged);
                        }
                        ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
                        if (ddlIS_COMMON != null)
                        {
                            ddlIS_COMMON.AutoPostBack = true;
                            ddlIS_COMMON.SelectedIndexChanged += new EventHandler(ddlIS_COMMON_SelectedIndexChanged);
                        }
                        ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
                        if (ddlOrganization != null)
                        {
                            ddlOrganization.AutoPostBack = true;
                        }
                        ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
                        if (ddlPosition != null)
                        {
                            ddlPosition.AutoPostBack = true;
                        }
                        //END SET DEFAULT
                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["KPI_GROUP_NAME"] = ViewState["KPI_GROUP_NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                    ddlType = this.FindControl("TYPE") as ListControl;
                    if (ddlType != null)
                    {
                        ddlType.AutoPostBack = true;
                        ddlType.SelectedIndexChanged += new EventHandler(ddlType_SelectedIndexChanged);
                    }
                    ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
                    if (ddlIS_COMMON != null)
                    {
                        ddlIS_COMMON.AutoPostBack = true;
                        ddlIS_COMMON.SelectedIndexChanged += new EventHandler(ddlIS_COMMON_SelectedIndexChanged);
                    }
                }

            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Load_DropdownWithTypeInit(ListControl ddlType)
        {
            ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
            ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
            /* Tam thoi an IS_COMMON*/
            ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
            ddlBusinessCode = this.FindControl("BUSINESS_CODE") as DropDownList;
            
            Int32 active = Int32.Parse(ddlIS_COMMON.SelectedValue);
            Int32 intType = int.Parse(ddlType.SelectedItem.Value);

            //Chi hien thi contract type khi loai la CHUC DANH
            CONTRACT_TYPE_C_LABEL = this.FindControl("CONTRACT_TYPE_C_LABEL") as Label;
            CONTRACT_TYPE_C_REQUIRED_SYMBOL = this.FindControl("CONTRACT_TYPE_C_REQUIRED_SYMBOL") as Label;
            ddlContractTypeC = this.FindControl("CONTRACT_TYPE_C") as DropDownList;
            if (ddlContractTypeC != null)
            {
                CONTRACT_TYPE_C_LABEL.Enabled = false;
                CONTRACT_TYPE_C_LABEL.Visible = false;
                CONTRACT_TYPE_C_REQUIRED_SYMBOL.Enabled = false;
                CONTRACT_TYPE_C_REQUIRED_SYMBOL.Visible = false;
                ddlContractTypeC.Enabled = false;
                ddlContractTypeC.Visible = false;
                if (intType == KPIs_Utils.POSITION_ID)
                {
                    CONTRACT_TYPE_C_LABEL.Enabled = true;
                    CONTRACT_TYPE_C_LABEL.Visible = true;
                    CONTRACT_TYPE_C_REQUIRED_SYMBOL.Enabled = true;
                    CONTRACT_TYPE_C_REQUIRED_SYMBOL.Visible = true;
                    ddlContractTypeC.Enabled = true;
                    ddlContractTypeC.Visible = true;
                }
            }
            

            if (intType == KPIs_Utils.POSITION_ID)
            {
                if (ddlBusinessCode != null)
                {
                    ddlBusinessCode.Enabled = true;
                    ddlBusinessCode.Items.Clear();
                    ddlBusinessCode.DataSource = SplendidCache.M_BUSINESS();
                    ddlBusinessCode.DataTextField = "BUSINESS_NAME";
                    ddlBusinessCode.DataValueField = "BUSINESS_CODE";
                    ddlBusinessCode.DataBind();
                    ListItem removeItem = ddlBusinessCode.Items.FindByValue(KPIs_Constant.ORG_TYPE_TAT);
                    if (removeItem != null)
                    {
                        ddlBusinessCode.Items.Remove(removeItem);
                    }
                    ListItem removeItem2 = ddlBusinessCode.Items.FindByValue(KPIs_Constant.ORG_TYPE_PGD);
                    if (removeItem2 != null)
                    {
                        ddlBusinessCode.Items.Remove(removeItem2);
                    }
                }
            }
            if (active == KPIs_Utils.ACTIVE)
            {
                ddlOrganization.Enabled = false;
                ddlOrganization.SelectedIndex = 0;
                ddlPosition.SelectedIndex = 0;
                ddlPosition.Enabled = false;
                return;
            }
            else
            {
                ddlPosition.Enabled = true;
                ddlPosition.SelectedIndex = 0;

                ddlOrganization.SelectedIndex = 0;
                ddlOrganization.Enabled = true;
            }
            if (ddlPosition != null)
            {
                ddlPosition.Items.Clear();
                ddlPosition.Enabled = true;
                ddlPosition.DataSource = SplendidCache.M_POSITION();
                ddlPosition.DataTextField = "POSITION_NAME";
                ddlPosition.DataValueField = "ID";
                ddlPosition.DataBind();
                return;
            }
            if (ddlOrganization != null)
            {
                ddlOrganization.Items.Clear();
                ddlOrganization.Enabled = true;
                ddlOrganization.DataSource = SplendidCache.M_ORGANIZATION();
                ddlOrganization.DataTextField = "ORGANIZATION_NAME";
                ddlOrganization.DataValueField = "ID";
                ddlOrganization.DataBind();
                return;
            }
        }

        private void Bind_Organization(int intLevel)
        {
            ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
            if (ddlOrganization != null)
            {
                ddlOrganization.Items.Clear();
                ddlOrganization.Enabled = true;
                ddlOrganization.DataSource = SplendidCache.M_ORGANIZATION(intLevel);
                ddlOrganization.DataTextField = "ORGANIZATION_NAME";
                ddlOrganization.DataValueField = "ID";
                ddlOrganization.DataBind();
                return;
            }
        }

        protected void ddlIS_COMMON_SelectedIndexChanged(object sender, EventArgs e)
        {
            // User selected a shutdown, show its details
            ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
            ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
            Int32 active = Int32.Parse(ddlIS_COMMON.SelectedValue);
            if (active == KPIs_Utils.ACTIVE)
            {
                ddlOrganization.Items.Clear();
                ddlOrganization.Enabled = false;
                ddlPosition.SelectedIndex = 0;
                ddlPosition.Enabled = false;
            }
            else
            {
                ddlPosition.Enabled = true;
                ddlOrganization.Enabled = true;
            }
            ddlType = this.FindControl("TYPE") as ListControl;
            if (ddlType != null)
            {
                Load_DropdownWithType(ddlType);
            }
        }

        private void Load_DropdownWithType(ListControl ddlType)
        {
            ddlOrganization = this.FindControl("ORGANIZATION_ID") as DropDownList;
            ddlPosition = this.FindControl("POSITION_ID") as DropDownList;
            ddlIS_COMMON = this.FindControl("IS_COMMON") as DropDownList;
            ddlBusinessCode = this.FindControl("BUSINESS_CODE") as DropDownList;

            Int32 active = Int32.Parse(ddlIS_COMMON.SelectedValue);
            //check bussiness
            Int32 intType = int.Parse(ddlType.SelectedItem.Value);

            //Chi hien thi contract type khi loai la CHUC DANH
            CONTRACT_TYPE_C_LABEL = this.FindControl("CONTRACT_TYPE_C_LABEL") as Label;
            CONTRACT_TYPE_C_REQUIRED_SYMBOL = this.FindControl("CONTRACT_TYPE_C_REQUIRED_SYMBOL") as Label;
            ddlContractTypeC = this.FindControl("CONTRACT_TYPE_C") as DropDownList;
            if (ddlContractTypeC != null)
            {
                CONTRACT_TYPE_C_LABEL.Enabled = false;
                CONTRACT_TYPE_C_LABEL.Visible = false;
                CONTRACT_TYPE_C_REQUIRED_SYMBOL.Enabled = false;
                CONTRACT_TYPE_C_REQUIRED_SYMBOL.Visible = false;
                ddlContractTypeC.Enabled = false;
                ddlContractTypeC.Visible = false;
                if (intType == KPIs_Utils.POSITION_ID)
                {
                    CONTRACT_TYPE_C_LABEL.Enabled = true;
                    CONTRACT_TYPE_C_LABEL.Visible = true;
                    CONTRACT_TYPE_C_REQUIRED_SYMBOL.Enabled = true;
                    CONTRACT_TYPE_C_REQUIRED_SYMBOL.Visible = true;
                    ddlContractTypeC.Enabled = true;
                    ddlContractTypeC.Visible = true;
                }
            }

            if (intType == KPIs_Utils.BRANCH_ID)
            {
                if (ddlBusinessCode != null)
                {
                    ddlBusinessCode.Enabled = false;
                    ddlBusinessCode.Items.Clear();
                    ddlBusinessCode.DataSource = SplendidCache.M_BUSINESS();
                    ddlBusinessCode.DataTextField = "BUSINESS_NAME";
                    ddlBusinessCode.DataValueField = "BUSINESS_CODE";
                    ddlBusinessCode.DataBind();
                    if (ddlBusinessCode.Items.Count > 0)
                    {
                        ddlBusinessCode.SelectedIndex = 0;
                    }
                }
            }
            else if (intType == KPIs_Utils.DEPARMENT_ID)
            {
                if (ddlBusinessCode != null)
                {
                    ddlBusinessCode.Enabled = true;
                    ddlBusinessCode.Items.Clear();
                    ddlBusinessCode.DataSource = SplendidCache.M_BUSINESS();
                    ddlBusinessCode.DataTextField = "BUSINESS_NAME";
                    ddlBusinessCode.DataValueField = "BUSINESS_CODE";
                    ddlBusinessCode.DataBind();
                    ListItem removeItem = ddlBusinessCode.Items.FindByValue(KPIs_Constant.ORG_TYPE_TAT);
                    if (removeItem != null)
                    {
                        ddlBusinessCode.Items.Remove(removeItem);
                    }
                }
            }
            else if (intType == KPIs_Utils.POSITION_ID)
            {
                if (ddlBusinessCode != null)
                {
                    ddlBusinessCode.Enabled = true;
                    ddlBusinessCode.Items.Clear();
                    ddlBusinessCode.DataSource = SplendidCache.M_BUSINESS();
                    ddlBusinessCode.DataTextField = "BUSINESS_NAME";
                    ddlBusinessCode.DataValueField = "BUSINESS_CODE";
                    ddlBusinessCode.DataBind();
                    ListItem removeItem = ddlBusinessCode.Items.FindByValue(KPIs_Constant.ORG_TYPE_TAT);
                    if (removeItem != null)
                    {
                        ddlBusinessCode.Items.Remove(removeItem);
                    }
                    ListItem removeItem2 = ddlBusinessCode.Items.FindByValue(KPIs_Constant.ORG_TYPE_PGD);
                    if (removeItem2 != null)
                    {
                        ddlBusinessCode.Items.Remove(removeItem2);
                    }
                }
            }
            /* Tam thoi an IS_COMMON*/
            if (active == KPIs_Utils.ACTIVE)
            {
                ddlOrganization.Enabled = false;
                if (ddlOrganization.Items.Count > 0)
                {
                    ddlOrganization.SelectedIndex = 0;
                }
                ddlPosition.SelectedIndex = 0;
                ddlPosition.Enabled = false;
                return;
            }
            else
            {
                ddlPosition.Enabled = true;
                ddlPosition.SelectedIndex = 0;
                if (ddlOrganization.Items.Count > 0)
                {
                    ddlOrganization.SelectedIndex = 0;
                }
                ddlOrganization.Enabled = true;
            }
            if (ddlType.SelectedItem.Value == string.Empty)
            {
                ddlOrganization.Enabled = false;
                if (ddlOrganization.Items.Count > 0)
                {
                    ddlOrganization.SelectedIndex = 0;
                }
                ddlPosition.SelectedIndex = 0;
                ddlPosition.Enabled = false;
                return;
            }

            if (intType == KPIs_Utils.POSITION_ID)
            {
                if (ddlOrganization != null)
                {
                    ddlOrganization.Enabled = false;
                    if (ddlOrganization.Items.Count > 0)
                    {
                        ddlOrganization.SelectedIndex = 0;
                    }
                }
            }
            else
            {
                if (ddlPosition != null)
                {
                    ddlPosition.Enabled = false;
                    if (ddlPosition.Items.Count > 0)
                    {
                        ddlPosition.SelectedIndex = 0;
                    }
                }
                Bind_Organization(intType);
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // User selected a shutdown, show its details
            Load_DropdownWithType(ddlType);
        }

        private void Load_KPI_Detail(DbProviderFactory dbf, Guid gID)
        {
            //DETAILS LIST                           
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL_DETAILS;
                sSQL_DETAILS = " SELECT Row_Number() OVER(order by MG.DATE_ENTERED) AS NO, MG.ID, MG.KPI_ID, MG.KPI_CODE, MG.KPI_NAME, MG.UNIT, MG.RATIO, MG.MAX_RATIO_COMPLETE, MG.DESCRIPTION, MG.REMARK   " + ControlChars.CrLf
                             + " , MK.PARENT_ID, '' AS PARENT_NAME FROM vwM_GROUP_KPI_DETAILS_Edit MG  inner join vwM_KPIS_Edit MK ON (MG.KPI_ID = MK.ID)  " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL_DETAILS;
                    //Security.Filter(cmd, m_sMODULE, "edit");
                    Sql.AppendParameter(cmd, gID, "GROUP_KPI_ID", false);
                    //Sql.AppendParameter(cmd, 0, "DELETED", false);//NO SELECT DELETED
                    cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                //KPIDetails
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                grdMain.ShowFooter = bEnableAdd;
                                for (int i = 0; i < grdMain.Items.Count; i++)
                                {
                                    //FirstDataGridViewRow();
                                    Label lblNO = (Label)grdMain.Items[i].Cells[1].FindControl("lblNO");

                                    HiddenField kpiID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdfID");
                                    HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[i].Cells[2].FindControl("hdfKPI_ID");
                                    TextBox txtKPI_CODE = (TextBox)grdMain.Items[i].Cells[2].FindControl("txtKPI_CODE");

                                    TextBox kpiName = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtKPI_NAME");
                                    TextBox parentName = (TextBox)grdMain.Items[i].Cells[4].FindControl("txtPARENT_NAME");
                                    DropDownList unit = (DropDownList)grdMain.Items[i].Cells[5].FindControl("ddlUNIT");
                                    TextBox ratio = (TextBox)grdMain.Items[i].Cells[6].FindControl("txtRATIO");
                                    TextBox maxRatioComplete = (TextBox)grdMain.Items[i].Cells[7].FindControl("txtMAX_RATIO_COMPL");
                                    TextBox remark = (TextBox)grdMain.Items[i].Cells[8].FindControl("txtREMARK");
                                    //SET VALUE
                                    lblNO.Text = (i + 1).ToString();
                                    kpiID.Value = dtCurrent.Rows[i]["ID"].ToString();

                                    hdfKPI_ID.Value = dtCurrent.Rows[i]["KPI_ID"].ToString();
                                    txtKPI_CODE.Text = dtCurrent.Rows[i]["KPI_CODE"].ToString();
                                    kpiName.Text = dtCurrent.Rows[i]["KPI_NAME"].ToString();
                                    var parentID = dtCurrent.Rows[i]["PARENT_ID"].ToString();
                                    if (parentID != null && parentID != string.Empty)
                                    {
                                        string strParentName = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(parentID), "vwM_KPIS_List", "KPI_NAME");
                                        parentName.Text = strParentName;
                                        //parentName.Text = dtCurrent.Rows[i]["KPI_NAME"].ToString();
                                    }
                                    if (unit.Items.Count > 0)
                                    {
                                        unit.Items.FindByValue(dtCurrent.Rows[i]["UNIT"].ToString()).Selected = true;
                                    }
                                    //get format from config
                                    string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                                    ratio.Text = KPIs_Utils.FormatFloat(dtCurrent.Rows[i]["RATIO"].ToString(), format_number);
                                    maxRatioComplete.Text = KPIs_Utils.FormatFloat(dtCurrent.Rows[i]["MAX_RATIO_COMPLETE"].ToString(), format_number);
                                    remark.Text = dtCurrent.Rows[i]["REMARK"].ToString();

                                }
                            }
                        }
                    }
                }

            }
            //End Details list
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);

            grdMain.ItemCommand += new DataGridCommandEventHandler(grdMain_ItemCommand);
            grdMain.ItemCreated += new DataGridItemEventHandler(grdMain_ItemCreated);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIM0101";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
