/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ProjectActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class ProjectActivity: SplendidActivity
	{
		public ProjectActivity()
		{
			this.Name = "ProjectActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.IDProperty))); }
			set { base.SetValue(ProjectActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ProjectActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.NAMEProperty))); }
			set { base.SetValue(ProjectActivity.NAMEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(ProjectActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(ProjectActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(ProjectActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(ProjectActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.PARENT_IDProperty))); }
			set { base.SetValue(ProjectActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.TEAM_IDProperty))); }
			set { base.SetValue(ProjectActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ProjectActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ProjectActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty ESTIMATED_START_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ESTIMATED_START_DATE", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ESTIMATED_START_DATE
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.ESTIMATED_START_DATEProperty))); }
			set { base.SetValue(ProjectActivity.ESTIMATED_START_DATEProperty, value); }
		}

		public static DependencyProperty ESTIMATED_END_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ESTIMATED_END_DATE", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ESTIMATED_END_DATE
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.ESTIMATED_END_DATEProperty))); }
			set { base.SetValue(ProjectActivity.ESTIMATED_END_DATEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(ProjectActivity.STATUSProperty))); }
			set { base.SetValue(ProjectActivity.STATUSProperty, value); }
		}

		public static DependencyProperty PRIORITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIORITY", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIORITY
		{
			get { return ((string)(base.GetValue(ProjectActivity.PRIORITYProperty))); }
			set { base.SetValue(ProjectActivity.PRIORITYProperty, value); }
		}

		public static DependencyProperty IS_TEMPLATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_TEMPLATE", typeof(bool), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_TEMPLATE
		{
			get { return ((bool)(base.GetValue(ProjectActivity.IS_TEMPLATEProperty))); }
			set { base.SetValue(ProjectActivity.IS_TEMPLATEProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(ProjectActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(ProjectActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(ProjectActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(ProjectActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ProjectActivity.CREATED_BYProperty))); }
			set { base.SetValue(ProjectActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(ProjectActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ProjectActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ProjectActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(ProjectActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ProjectActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ProjectActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ProjectActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(ProjectActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(ProjectActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProjectActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ProjectActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(ProjectActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(ProjectActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty TOTAL_ACTUAL_EFFORTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_ACTUAL_EFFORT", typeof(float), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float TOTAL_ACTUAL_EFFORT
		{
			get { return ((float)(base.GetValue(ProjectActivity.TOTAL_ACTUAL_EFFORTProperty))); }
			set { base.SetValue(ProjectActivity.TOTAL_ACTUAL_EFFORTProperty, value); }
		}

		public static DependencyProperty TOTAL_ESTIMATED_EFFORTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_ESTIMATED_EFFORT", typeof(float), typeof(ProjectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float TOTAL_ESTIMATED_EFFORT
		{
			get { return ((float)(base.GetValue(ProjectActivity.TOTAL_ESTIMATED_EFFORTProperty))); }
			set { base.SetValue(ProjectActivity.TOTAL_ESTIMATED_EFFORTProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ProjectActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ProjectActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select PROJECT_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwPROJECT_AUDIT        PROJECT          " + ControlChars.CrLf
							                + " inner join vwPROJECT_AUDIT        PROJECT_AUDIT_OLD" + ControlChars.CrLf
							                + "         on PROJECT_AUDIT_OLD.ID = PROJECT.ID       " + ControlChars.CrLf
							                + "        and PROJECT_AUDIT_OLD.AUDIT_VERSION = (select max(vwPROJECT_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                 from vwPROJECT_AUDIT                   " + ControlChars.CrLf
							                + "                                                where vwPROJECT_AUDIT.ID            =  PROJECT.ID           " + ControlChars.CrLf
							                + "                                                  and vwPROJECT_AUDIT.AUDIT_VERSION <  PROJECT.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                  and vwPROJECT_AUDIT.AUDIT_TOKEN   <> PROJECT.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                              )" + ControlChars.CrLf
							                + " where PROJECT.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *             " + ControlChars.CrLf
							                + "  from vwPROJECT_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwPROJECT_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *             " + ControlChars.CrLf
							                + "  from vwPROJECT_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								ESTIMATED_START_DATE           = Sql.ToDateTime(rdr["ESTIMATED_START_DATE"          ]);
								ESTIMATED_END_DATE             = Sql.ToDateTime(rdr["ESTIMATED_END_DATE"            ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								PRIORITY                       = Sql.ToString  (rdr["PRIORITY"                      ]);
								IS_TEMPLATE                    = Sql.ToBoolean (rdr["IS_TEMPLATE"                   ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									TOTAL_ACTUAL_EFFORT            = Sql.ToFloat   (rdr["TOTAL_ACTUAL_EFFORT"           ]);
									TOTAL_ESTIMATED_EFFORT         = Sql.ToFloat   (rdr["TOTAL_ESTIMATED_EFFORT"        ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProjectActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("PROJECT", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spPROJECTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, DESCRIPTION
								, PARENT_TYPE
								, PARENT_ID
								, TEAM_ID
								, TEAM_SET_LIST
								, ESTIMATED_START_DATE
								, ESTIMATED_END_DATE
								, STATUS
								, PRIORITY
								, IS_TEMPLATE
								, EXCHANGE_FOLDER
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProjectActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

