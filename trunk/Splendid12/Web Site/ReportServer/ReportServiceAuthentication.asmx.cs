/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for ReportServiceAuthentication
	/// </summary>
	[WebService(Namespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Name="ReportServiceAuthentication", Description="SplendidCRM Report Service Authentication.")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ReportServiceAuthentication : WebService
	{
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum AuthenticationMode
		{
			None,
			Windows,
			Passport,
			Forms,
		}

		[WebMethod(EnableSession=true)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetAuthenticationMode", RequestNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
		public AuthenticationMode GetAuthenticationMode()
		{
			return Security.IsWindowsAuthentication() ? AuthenticationMode.Windows : AuthenticationMode.Forms;
		}

		[WebMethod(EnableSession=true)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/LogonUser", RequestNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
		public bool LogonUser(string userName, string password, string authority, out string cookieName)
		{
			bool bSuccess = false;
			cookieName = "ASP.NET_SessionId";
			try
			{
				// 12/10/2009 Paul.  ReportBuilder will always try a blank userName & password first. 
				// SplendidCRM will never allow a blank login. 
				if ( !Sql.IsEmptyString(userName) )
				{
					string sUSER_HASH = Security.HashPassword(password);
					// 12/10/2009 Paul.  We could use SplendidInit.LoginUser(), but this is better because it does not load preferences on login failure. 
					soap.LoginUser(ref userName, sUSER_HASH, true);
					bSuccess = true;
				}
			}
			catch
			{
				// 12/10/2009 Paul.  LoginUser() will throw an exception if invalid. 
				// There is no need to return the exception, just return false. 
			}
			return bSuccess;
		}

		[WebMethod(EnableSession=true)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/Logoff", RequestNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
		public void Logoff()
		{
			Session.Abandon();
			// 11/15/2014 Paul.  Prevent resuse of SessionID. 
			// http://support.microsoft.com/kb/899918
			this.Context.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
		}
	}
}
