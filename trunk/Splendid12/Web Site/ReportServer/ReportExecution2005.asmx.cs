/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Caching;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for ReportExecution2005
	/// </summary>
	[WebService(Namespace = "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Name="ReportExecution2005", Description="SplendidCRM Report Execution 2005.")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ReportExecution2005 : WebService
	{
		#region Enums
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum PageCountMode
		{
			Actual,
			Estimate,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ParameterTypeEnum
		{
			Boolean,
			DateTime,
			Integer,
			Float,
			String,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ParameterStateEnum
		{
			HasValidValue,
			MissingValidValue,
			HasOutstandingDependencies,
			DynamicValuesUnavailable,
		}
		#endregion

		#region Structures
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public class ServerInfoHeader : SoapHeader
		{
			public string ReportServerVersionNumber;
			public string ReportServerEdition;
			public string ReportServerVersion;
			public string ReportServerDateTime;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public class ExecutionHeader : SoapHeader
		{
			public string ExecutionID;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public partial class TrustedUserHeader : SoapHeader
		{
			public string UserName;
			[XmlElementAttribute(DataType="base64Binary")]
			public byte[] UserToken;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ValidValue
		{
			public string Label;
			public string Value;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSourcePrompt
		{
			public string Name;
			public string DataSourceID;
			public string Prompt;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ReportParameter
		{
			public string             Name;
			public ParameterTypeEnum  Type;
			public bool               Nullable;
			public bool               AllowBlank;
			public bool               MultiValue;
			public bool               QueryParameter;
			public string             Prompt;
			public bool               PromptUser;
			[XmlArrayItemAttribute("Dependency")]
			public string[]           Dependencies;
			public bool               ValidValuesQueryBased;
			public ValidValue[]       ValidValues;
			public bool               DefaultValuesQueryBased;
			[XmlArrayItemAttribute("Value")]
			public string[]           DefaultValues;
			public ParameterStateEnum State;
			public string             ErrorMessage;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ReportMargins
		{
			public double Top;
			public double Bottom;
			public double Left;
			public double Right;
		}

		/// <remarks/>
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ReportPaperSize
		{
			public double Height;
			public double Width;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class PageSettings
		{
			public ReportPaperSize PaperSize;
			public ReportMargins Margins;
		}

		[XmlIncludeAttribute(typeof(ExecutionInfo2))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ExecutionInfo
		{
			public bool               HasSnapshot;
			public bool               NeedsProcessing;
			public bool               AllowQueryExecution;
			public bool               CredentialsRequired;
			public bool               ParametersRequired;
			public DateTime           ExpirationDateTime;
			public DateTime           ExecutionDateTime;
			public int                NumPages;
			public ReportParameter[]  Parameters;
			public DataSourcePrompt[] DataSourcePrompts;
			public bool               HasDocumentMap;
			public string             ExecutionID;
			public string             ReportPath;
			public string             HistoryID;
			public PageSettings       ReportPageSettings;
			public int                AutoRefreshInterval;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ExecutionInfo2 : ExecutionInfo
		{
			public PageCountMode PageCountMode;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class Warning
		{
			public string Code;
			public string Severity;
			public string ObjectName;
			public string ObjectType;
			public string Message;
		}

		#endregion

		#region Headers
		public ServerInfoHeader    ServerInfoHeaderValue ;
		public TrustedUserHeader   TrustedUserHeaderValue;
		public ExecutionHeader     ExecutionHeaderValue  ;

		private void InitServerInfoHeader()
		{
			ServerInfoHeaderValue = new ServerInfoHeader();
			// 12/07/2009 Paul.  In order for ReportBuilder 2.0 to connect, we need to return SQL Server 2008 information. 
			ServerInfoHeaderValue.ReportServerVersionNumber = "2007.100.1600.22";  //"2005.090.1399.00";
			ServerInfoHeaderValue.ReportServerEdition       = "Standard";
			ServerInfoHeaderValue.ReportServerVersion       = "Microsoft SQL Server Reporting Services Version 10.0.1600.22";  //"Microsoft SQL Server Reporting Services Version 9.00.1399.00";
			ServerInfoHeaderValue.ReportServerDateTime      = DateTime.Now.ToString("s"); // "2009-12-06T03:49:35";

			// 12/06/2009 Paul.  Since all web methods call this init method, it is a common place to validate the user's access. 
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));
		}
		#endregion

		#region Simple Methods
		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListSecureMethods"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("TrustedUserHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListSecureMethods", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public string[] ListSecureMethods()
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any secure methods. 
			string[] arr = new string[0];
			return arr;
		}
		#endregion

		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("ExecutionHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("TrustedUserHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/LoadReportDefinition2", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("executionInfo")]
		public ExecutionInfo2 LoadReportDefinition2([XmlElementAttribute(DataType="base64Binary")] byte[] Definition, out Warning[] warnings)
		{
			InitServerInfoHeader();
			ExecutionHeaderValue = new ExecutionHeader();
			ExecutionHeaderValue.ExecutionID = Guid.NewGuid().ToString();

			warnings = new Warning[0];
			ExecutionInfo2 info = new ExecutionInfo2();
			info.HasSnapshot                  = false;
			info.NeedsProcessing              = true;
			info.AllowQueryExecution          = true;
			//info.CredentialsRequired          = false;
			//info.ParametersRequired           = false;
			info.ExpirationDateTime           = DateTime.Now;
			info.ExpirationDateTime           = DateTime.MinValue;
			info.NumPages                     = 0;
			info.HasDocumentMap               = false;
			info.ExecutionID                  = ExecutionHeaderValue.ExecutionID;
			info.ReportPageSettings           = new PageSettings();
			info.ReportPageSettings.Margins   = new ReportMargins();
			info.ReportPageSettings.PaperSize = new ReportPaperSize();
			info.AutoRefreshInterval          = 0;
			info.PageCountMode                = PageCountMode.Actual;
			return info;
		}

		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("ExecutionHeaderValue")]
		[SoapHeaderAttribute("TrustedUserHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ResetExecution2", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("executionInfo")]
		public ExecutionInfo2 ResetExecution2()
		{
			InitServerInfoHeader();

			ExecutionInfo2 info = new ExecutionInfo2();
			info.HasSnapshot                  = false;
			info.NeedsProcessing              = true;
			info.AllowQueryExecution          = true;
			//info.CredentialsRequired          = false;
			//info.ParametersRequired           = false;
			info.ExpirationDateTime           = DateTime.Now;
			info.ExpirationDateTime           = DateTime.MinValue;
			info.NumPages                     = 0;
			info.HasDocumentMap               = false;
			info.ExecutionID                  = (ExecutionHeaderValue != null) ? ExecutionHeaderValue.ExecutionID : String.Empty;
			info.ReportPageSettings           = new PageSettings();
			info.ReportPageSettings.Margins   = new ReportMargins();
			info.ReportPageSettings.PaperSize = new ReportPaperSize();
			info.AutoRefreshInterval          = 0;
			info.PageCountMode                = PageCountMode.Actual;
			return info;
		}

		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("ExecutionHeaderValue")]
		[SoapHeaderAttribute("TrustedUserHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetExecutionInfo2", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("executionInfo")]
		public ExecutionInfo2 GetExecutionInfo2()
		{
			InitServerInfoHeader();

			ExecutionInfo2 info = new ExecutionInfo2();
			info.HasSnapshot                  = false;
			info.NeedsProcessing              = true;
			info.AllowQueryExecution          = true;
			//info.CredentialsRequired          = false;
			//info.ParametersRequired           = false;
			info.ExpirationDateTime           = DateTime.Now;
			info.ExpirationDateTime           = DateTime.MinValue;
			info.NumPages                     = 0;
			info.HasDocumentMap               = false;
			info.ExecutionID                  = (ExecutionHeaderValue != null) ? ExecutionHeaderValue.ExecutionID : String.Empty;
			info.ReportPageSettings           = new PageSettings();
			info.ReportPageSettings.Margins   = new ReportMargins();
			info.ReportPageSettings.PaperSize = new ReportPaperSize();
			info.AutoRefreshInterval          = 0;
			info.PageCountMode                = PageCountMode.Actual;
			return info;
		}

		// /ReportServer?&rs:SessionID=vb0mp03k5l5gt555vrl5pk55&rs:command=Render&rs:Format=RPL&rc:StartPage=1&rc:EndPage=1&rc:MeasureItems=True&rc:SecondaryStreams=Embedded&rc:StreamNames=False&rc:Toolbar=false&rs:ErrorResponseAsXml=true&rs:AllowNewSessions=false&rs%3aPageCountMode=Estimate
		// http://msdn.microsoft.com/en-us/library/ee301522.aspx
		// Report = RPLStamp Version reportStart ReportProperties *PageContent OffsetsArrayElement ReportElementEnd Version
	}
}
