/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Caching;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for ReportService2006
	/// </summary>
	[WebService(Namespace = "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Name="ReportService2006", Description="SplendidCRM Report Service 2006.")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ReportService2006 : System.Web.Services.WebService
	{
		#region Enums
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum ItemTypeEnum
		{
			Unknown,
			Folder,
			Report,
			Resource,
			DataSource,
			Model,
			Site,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum CredentialRetrievalEnum
		{
			Prompt,
			Store,
			Integrated,
			None,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum SensitivityEnum
		{
			True,
			False,
			Auto,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum ParameterTypeEnum {
			Boolean,
			DateTime,
			Integer,
			Float,
			String,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public enum ParameterStateEnum
		{
			HasValidValue,
			MissingValidValue,
			HasOutstandingDependencies,
			DynamicValuesUnavailable,
		}

		#endregion

		#region Structures
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ModelPerspective
		{
			public string ID;
			public string Name;
			public string Description;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ModelCatalogItem
		{
			public string Model;
			public string Description;
			public ModelPerspective[] Perspectives;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class CatalogItem
		{
			public string ID;
			public string Name;
			public string Path;
			public string VirtualPath;
			public ItemTypeEnum Type;
			public int Size;
			public string Description;
			public bool Hidden;
			public DateTime CreationDate;
			public DateTime ModifiedDate;
			public string CreatedBy;
			public string ModifiedBy;
			public string MimeType;
			public DateTime ExecutionDate;
		}

		[XmlIncludeAttribute(typeof(InvalidDataSourceReference))]
		[XmlIncludeAttribute(typeof(DataSourceReference))]
		[XmlIncludeAttribute(typeof(DataSourceDefinition))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSourceDefinitionOrReference
		{
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class InvalidDataSourceReference : DataSourceDefinitionOrReference
		{
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSourceReference : DataSourceDefinitionOrReference
		{
			public string Reference;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSourceDefinition : DataSourceDefinitionOrReference
		{
			public string Extension;
			public string ConnectString;
			public bool UseOriginalConnectString;
			public bool OriginalConnectStringExpressionBased;
			public CredentialRetrievalEnum CredentialRetrieval;
			public bool WindowsCredentials;
			public bool ImpersonateUser;
			public string Prompt;
			public string UserName;
			public string Password;
			public bool Enabled;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSource
		{
			public string Name;
			[XmlElementAttribute("DataSourceDefinition", typeof(DataSourceDefinition))]
			[XmlElementAttribute("DataSourceReference", typeof(DataSourceReference))]
			[XmlElementAttribute("InvalidDataSourceReference", typeof(InvalidDataSourceReference))]
			public DataSourceDefinitionOrReference Item;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class Warning
		{
			public string Code;
			public string Severity;
			public string ObjectName;
			public string ObjectType;
			public string Message;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class Property
		{
			public string Name;
			public string Value;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", IsNullable=false)]
		public class ServerInfoHeader : SoapHeader
		{
			public string ReportServerVersionNumber;
			public string ReportServerEdition;
			public string ReportServerVersion;
			public string ReportServerDateTime;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class QueryDefinition
		{
			public string CommandType;
			public string CommandText;
			public int    Timeout;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class Field
		{
			public string Alias;
			public string Name;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSetDefinition
		{
			public Field[]         Fields;
			public QueryDefinition Query;
			public SensitivityEnum CaseSensitivity;
			public string          Collation;
			public SensitivityEnum AccentSensitivity;
			public SensitivityEnum KanatypeSensitivity;
			public SensitivityEnum WidthSensitivity;
			public string          Name;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ReportParameter
		{
			public string             Name;
			public ParameterTypeEnum  Type;
			public bool               Nullable;
			public bool               AllowBlank;
			public bool               MultiValue;
			public bool               QueryParameter;
			public string             Prompt;
			public bool               PromptUser;
			[XmlArrayItemAttribute("Dependency")]
			public string[]           Dependencies;
			public bool               ValidValuesQueryBased;
			public ValidValue[]       ValidValues;
			public bool               DefaultValuesQueryBased;
			[XmlArrayItemAttribute("Value")]
			public string[]           DefaultValues;
			public ParameterStateEnum State;
			public string             ErrorMessage;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ValidValue
		{
			public string Label;
			public string Value;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ParameterFieldReference : ParameterValueOrFieldReference
		{
			public string ParameterName;
			public string FieldAlias;
		}

		[XmlIncludeAttribute(typeof(ParameterFieldReference))]
		[XmlIncludeAttribute(typeof(ParameterValue))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ParameterValueOrFieldReference
		{
		}

		/// <remarks/>
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class ParameterValue : ParameterValueOrFieldReference
		{
			public string Name;
			public string Value;
			public string Label;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices")]
		public class DataSourceCredentials
		{
			public string DataSourceName;
			public string UserName;
			public string Password;
		}

		#endregion

		#region Headers
		public ServerInfoHeader    ServerInfoHeaderValue;

		private void InitServerInfoHeader()
		{
			ServerInfoHeaderValue = new ServerInfoHeader();
			// 12/07/2009 Paul.  In order for ReportBuilder 2.0 to connect, we need to return SQL Server 2008 information. 
			ServerInfoHeaderValue.ReportServerVersionNumber = "2007.100.1600.22";  //"2005.090.1399.00";
			ServerInfoHeaderValue.ReportServerEdition       = "Standard";
			ServerInfoHeaderValue.ReportServerVersion       = "Microsoft SQL Server Reporting Services Version 10.0.1600.22";  //"Microsoft SQL Server Reporting Services Version 9.00.1399.00";
			ServerInfoHeaderValue.ReportServerDateTime      = DateTime.Now.ToString("s"); // "2009-12-06T03:49:35";

			// 12/06/2009 Paul.  Since all web methods call this init method, it is a common place to validate the user's access. 
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));
		}
		#endregion

		#region Simple Methods
		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListSecureMethods"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListSecureMethods", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public string[] ListSecureMethods()
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any secure methods. 
			string[] arr = new string[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetProperties(string Item, Property[] Properties)
		{
			InitServerInfoHeader();
			List<Property> lst = new List<Property>();
			if ( Item == "/SplendidCRM Model" )
			{
				foreach ( Property p in Properties )
				{
					switch ( p.Name )
					{
						case "Name":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "SplendidCRM";
							lst.Add(pReturn);
							break;
						}
						case "Description":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "SplendidCRM Reporting Services";
							lst.Add(pReturn);
							break;
						}
						case "ModifiedDate":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = DateTime.Now.ToString("s");
							lst.Add(pReturn);
							break;
						}
						case "MustUsePerspective":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "false";
							lst.Add(pReturn);
							break;
						}
					}
				}
			}
			else if ( Properties == null )
			{
				string Report = Item;
				int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
				// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
				if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
					Report = Report.Substring(nVtiBin);
				if ( Report.EndsWith(".rdl") )
					Report = Report.Substring(0, Report.Length - 4);
				
				// 12/10/2009 Paul.  If Properties is null, then assume we are retrieving the properties of a report. 
				Property pReturn = new Property();
				pReturn.Name  = "Name";
				pReturn.Value = Report;
				lst.Add(pReturn);
			}
			return lst.ToArray();
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetReportParameters"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetReportParameters", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Parameters")]
		public ReportParameter[] GetReportParameters(string Report, string HistoryID, ParameterValue[] Values, DataSourceCredentials[] Credentials)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/10/2009 Paul.  We need to pull the parameters out of the report RDL. 
			ReportParameter[] arr = new ReportParameter[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/SetReportParameters"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/SetReportParameters", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public void SetReportParameters(string Report, ReportParameter[] Parameters)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/10/2009 Paul.  We do not store the report parameters separately, so we will do nothing. 
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListModelPerspectives"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListModelPerspectives", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("ModelCatalogItems")]
		public ModelCatalogItem[] ListModelPerspectives(string Path)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any perspectives, so we can hard-code the result. 
			ModelCatalogItem[] arr = new ModelCatalogItem[1];
			arr[0] = new ModelCatalogItem();
			arr[0].Model = "/SplendidCRM Model";
			arr[0].Perspectives = new ModelPerspective[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetItemType"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetItemType", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Type")]
		public ItemTypeEnum GetItemType(string Item)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  I'm not sure if the Item will always start with a slash. 
			// Instead of assuming that the item is a report, we may want to query the database for the item name. 
			switch ( Item )
			{
				case "/"                       :  return ItemTypeEnum.Folder    ;
				case "/SplendidCRM Model"      :  return ItemTypeEnum.Model     ;
				case "/SplendidCRM Data Source":  return ItemTypeEnum.DataSource;
			}
			return ItemTypeEnum.Report;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("DataSources")]
		public DataSource[] GetItemDataSources(string Item)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore the Item and hard-code the return value. 
			DataSourceReference dsr = new DataSourceReference();
			dsr.Reference = "/SplendidCRM Data Source";
			DataSource[] arr = new DataSource[1];
			arr[0] = new DataSource();
			arr[0].Name = "SplendidCRM Data Source";
			arr[0].Item = dsr;
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/SetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/SetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public void SetItemDataSources(string Item, DataSource[] DataSources)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore this call. 
		}

		// "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetSystemProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetSystemProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetSystemProperties(Property[] Properties)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/07/2009 Paul.  A default installation of SQL Server 2008 returns nothing. 
			// Typical input properties include:
			// DefaultDataSourceFolder = DataSources
			// DefaultModelFolder = Models
			Property[] arr = new Property[0];
			return arr;
		}

		// SOAPAction: "http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetDataSourceContents"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetDataSourceContents", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition")]
		public DataSourceDefinition GetDataSourceContents(string DataSource)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/07/2009 Paul.  SplendidCRM is the only data source, so return static information.
			DataSourceDefinition ds = new DataSourceDefinition();
			// 01/21/2010 Paul.  Specify an Extension to fix the following ReportBuilder 2.0 error: 
			// The selected data extension  cannot be loaded. Verify that the selected data extension is correctly registered in RSReportDesigner.config, and is installed on the client for local reports and on the report server for published reports.
			ds.Extension           = "SQL";
			ds.ConnectString       = String.Empty;  // "data source=(local)\\SplendidCRM;initial catalog=SplendidCRM;";
			ds.CredentialRetrieval = CredentialRetrievalEnum.None;
			ds.WindowsCredentials  = false;
			ds.ImpersonateUser     = false;
			ds.Prompt              = "Enter a user name and password to access the data source:";
			ds.UserName            = String.Empty;
			ds.Password            = String.Empty;
			ds.Enabled             = true;
			ds.UseOriginalConnectString = false;
			ds.OriginalConnectStringExpressionBased = false;
			
			// 01/21/2010 Paul.  Allow Integrated security, but default to Prompt. 
			if ( Sql.ToString(Application["CONFIG.ReportServer.CredentialRetrieval"]).ToLower() == "integrated" )
				ds.CredentialRetrieval = CredentialRetrievalEnum.Integrated;
			
			// 01/21/2010 Paul.  SplendidCRM Data Source must be enabled manually, for security reasons. 
			if ( Sql.ToBoolean(Application["CONFIG.ReportServer.DataSource"]) )
			{
				// 01/21/2010 Paul.  Lets try and return the correct datasource and catalog. 
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sDataSource     = String.Empty;
					string sInitialCatalog = String.Empty;
					string sUserName       = String.Empty;
					string sPassword       = String.Empty;
					string[] arrProperties = con.ConnectionString.Split(';');
					foreach ( string sProperty in arrProperties )
					{
						string[] arrNameValue = sProperty.Trim().Split('=');
						if ( arrNameValue.Length == 2 )
						{
							switch ( arrNameValue[0].ToLower() )
							{
								case "data source"    :  sDataSource     = arrNameValue[1];  break;
								case "initial catalog":  sInitialCatalog = arrNameValue[1];  break;
								case "user id"        :  sUserName       = arrNameValue[1];  break;
								case "password"       :  sPassword       = arrNameValue[1];  break;
							}
						}
					}
					ds.ConnectString = "data source=" + sDataSource + ";initial catalog=" + sInitialCatalog + ";";
					if ( ds.CredentialRetrieval == CredentialRetrievalEnum.Integrated )
					{
						ds.UserName = sUserName;
						ds.Password = sPassword;
					}
				}
			}
			return ds;
		}

		#endregion

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListChildren"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/ListChildren", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("CatalogItems")]
		public CatalogItem[] ListChildren(string Item)
		{
			InitServerInfoHeader();

			CatalogItem[] arr = null;
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select *             " + ControlChars.CrLf
					     + "  from vwREPORTS_Edit" + ControlChars.CrLf
					     + " order by NAME       " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								arr = new CatalogItem[dt.Rows.Count + 2];
								CatalogItem itmDataSource = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM data source. 
								itmDataSource.ID           = "385e5bf1-fe2f-41b0-83e9-6ea2157e2499";
								itmDataSource.Name         = "SplendidCRM Data Source";
								itmDataSource.Path         = "/SplendidCRM Data Source";
								itmDataSource.Type         = ItemTypeEnum.DataSource;
								itmDataSource.CreationDate = DateTime.Now;
								itmDataSource.ModifiedDate = DateTime.Now;
								itmDataSource.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmDataSource.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								CatalogItem itmModel      = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM model. 
								itmModel.ID           = "ffe3817a-2578-44ce-9279-9d92c18fc913";
								itmModel.Name         = "SplendidCRM Model";
								itmModel.Path         = "/SplendidCRM Model";
								itmModel.Type         = ItemTypeEnum.Model;
								itmModel.Size         = 1;  // 12/06/2009 Paul.  We are going to hard-code the size until we are ready to retrieve the cached version. 
								itmModel.CreationDate = DateTime.Now;
								itmModel.ModifiedDate = DateTime.Now;
								itmModel.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmModel.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								arr[0] = itmDataSource;
								arr[1] = itmModel;

								for ( int i=0; i < dt.Rows.Count; i++ )
								{
									CatalogItem itm = new CatalogItem();
									DataRow row = dt.Rows[i];
									itm.ID           = Sql.ToString(row["ID"]);
									itm.Name         = Sql.ToString(row["NAME"]);
									itm.Path         = "/" + Sql.ToString(row["NAME"]);
									itm.Type         = ItemTypeEnum.Report;
									itm.Size         = Sql.ToString  (row["RDL"          ]).Length;
									itm.CreationDate = Sql.ToDateTime(row["DATE_ENTERED" ]);
									itm.ModifiedDate = Sql.ToDateTime(row["DATE_MODIFIED"]);
									itm.CreatedBy    = Sql.ToString  (row["CREATED_BY"   ]);
									itm.ModifiedBy   = Sql.ToString  (row["MODIFIED_BY"  ]);
									arr[i + 2] = itm;
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetReportDefinition"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetReportDefinition", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetReportDefinition(string Report)
		{
			InitServerInfoHeader();

			byte[] abyRDL = null;
			try
			{
				int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
				// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
				if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
					Report = Report.Substring(nVtiBin);
				if ( Report.EndsWith(".rdl") )
					Report = Report.Substring(0, Report.Length - 4);
				// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
				Guid gReportID = Guid.Empty;
				try
				{
					if ( Report.StartsWith("/") )
						Report = Report.Substring(1);
					if ( Report.Length == 36 )
						gReportID = Sql.ToGuid(Report);
				}
				catch
				{
				}
				
				// 12/10/2009 Paul.  The report name will include the full URL. 
				// http://developer6/SplendidCRM6_Sugar/Bugs.rdl
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select RDL           " + ControlChars.CrLf
					     + "  from vwREPORTS_Edit" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
							if ( Sql.IsEmptyGuid(gReportID) )
							{
								cmd.CommandText += " where NAME = @NAME  " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@NAME", Report);
							}
							else
							{
								cmd.CommandText += " where ID = @ID    " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@ID", gReportID);
							}
							cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								string sRDL = Sql.ToString(rdr["RDL"]);
								// 02/12/2010 Paul.  Loading the RDL will remove the Name property from the Report tag, 
								// which causes an exception in Report Builder 2.0. 
								RdlDocument rdl = new RdlDocument();
								rdl.LoadRdl(sRDL);
								abyRDL = UTF8Encoding.UTF8.GetBytes(rdl.OuterXml);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return abyRDL;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/CreateReport"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/CreateReport", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("ItemInfo")]
		public CatalogItem CreateReport(string Report, string Parent, bool Overwrite, [XmlElementAttribute(DataType="base64Binary")] byte[] Definition, Property[] Properties, out Warning[] Warnings)
		{
			InitServerInfoHeader();
			CatalogItem itm = new CatalogItem();
			try
			{
				int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
				// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
				if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
					Report = Report.Substring(nVtiBin);
				if ( Report.EndsWith(".rdl") )
					Report = Report.Substring(0, Report.Length - 4);
				// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
				Guid gReportID = Guid.Empty;
				try
				{
					if ( Report.StartsWith("/") )
						Report = Report.Substring(1);
					if ( Report.Length == 36 )
						gReportID = Sql.ToGuid(Report);
				}
				catch
				{
				}
				
				Warnings = new Warning[0];
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					Guid   gID                 = Guid.Empty;
					Guid   gASSIGNED_USER_ID   = Security.USER_ID;
					// 06/17/2010 Paul.  Add support for Team Management. 
					Guid   gTEAM_ID            = Security.TEAM_ID;
					string sTEAM_SET_LIST      = String.Empty;
					// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
					string sASSIGNED_SET_LIST  = String.Empty;
					// 12/06/2009 Paul.  We may want to extract the view from the RDL and determine the module name. 
					string sMODULE_NAME        = String.Empty;
					Guid   gPRE_LOAD_EVENT_ID  = Guid.Empty;
					Guid   gPOST_LOAD_EVENT_ID = Guid.Empty;
					if ( Overwrite )
					{
						sSQL = "select *                    " + ControlChars.CrLf
						     + "  from vwREPORTS            " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							// 06/17/2010 Paul.  Use new Security.Filter() function to apply Team and ACL security rules.
							// 04/24/2018 Paul.  Provide a way to exclude the SavedSearch for areas that are global in nature. 
							Security.Filter(cmd, "Reports", "edit", "ASSIGNED_USER_ID", true);
							if ( Sql.IsEmptyGuid(gReportID) )
							{
								cmd.CommandText += "   and NAME = @NAME  " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@NAME", Report);
							}
							else
							{
								cmd.CommandText += "   and ID = @ID    " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@ID", gReportID);
							}
							cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;

							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									gID                 = Sql.ToGuid  (rdr["ID"                ]);
									// 12/06/2009 Paul.  Preserve the old module name until we can extract from the RDL. 
									sMODULE_NAME        = Sql.ToString(rdr["MODULE_NAME"       ]);
									gTEAM_ID            = Sql.ToGuid  (rdr["TEAM_ID"           ]);
									sTEAM_SET_LIST      = Sql.ToString(rdr["TEAM_SET_LIST"     ]);
									// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
									sASSIGNED_SET_LIST  = Sql.ToString(rdr["ASSIGNED_SET_LIST" ]);
									// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
									gPRE_LOAD_EVENT_ID  = Sql.ToGuid  (rdr["PRE_LOAD_EVENT_ID" ]);
									gPOST_LOAD_EVENT_ID = Sql.ToGuid  (rdr["POST_LOAD_EVENT_ID"]);
								}
							}
						}
					}
					string sRDL = String.Empty;
					// 12/08/2009 Paul.  ReportBuilder 2.0 is saving the data with the Unicode Byte Order Mark. 
					// The XmlDocument.LoadXml() method does not understand this mark and throws an exception. 
					// http://www.filepie.us/?title=Byte_Order_Mark
					if ( Definition[0] == '\xEF' && Definition[1] == '\xBB' && Definition[2] == '\xBF' )  // EF BB BF
						sRDL = UTF8Encoding.UTF8.GetString(Definition, 3, Definition.Length - 3);
					else if ( Definition[0] == '\xFE' && Definition[1] == '\xFF' )
						sRDL = Encoding.BigEndianUnicode.GetString(Definition, 2, Definition.Length - 2);
					else if ( Definition[0] == '\xFF' && Definition[1] == '\xFE' )
						sRDL = Encoding.Unicode.GetString(Definition, 2, Definition.Length - 2);
					else
						sRDL = UTF8Encoding.UTF8.GetString(Definition);
					// 06/17/2010 Paul.  Add support for Team Management. 
					// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
					SqlProcs.spREPORTS_Update
						( ref gID
						, gASSIGNED_USER_ID
						, Report
						, sMODULE_NAME
						, "Freeform"
						, sRDL
						, gTEAM_ID
						, sTEAM_SET_LIST
						, gPRE_LOAD_EVENT_ID
						, gPOST_LOAD_EVENT_ID
						// 05/17/2017 Paul.  Add Tags module. 
						, String.Empty      // TAG_SET_NAME
						// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
						, sASSIGNED_SET_LIST
						);
					// 04/06/2011 Paul.  Cache reports. 
					SplendidCache.ClearReport(gID);
					
					sSQL = "select *                    " + ControlChars.CrLf
					     + "  from vwREPORTS_Edit       " + ControlChars.CrLf
					     + " where ID = @ID             " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@ID", gID);

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								itm.ID           = Sql.ToString(rdr["ID"]);
								itm.Name         = Sql.ToString(rdr["NAME"]);
								itm.Path         = "/" + Sql.ToString(rdr["NAME"]);
								itm.Type         = ItemTypeEnum.Report;
								itm.Size         = Sql.ToString  (rdr["RDL"          ]).Length;
								itm.CreationDate = Sql.ToDateTime(rdr["DATE_ENTERED" ]);
								itm.ModifiedDate = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
								itm.CreatedBy    = Sql.ToString  (rdr["CREATED_BY"   ]);
								itm.ModifiedBy   = Sql.ToString  (rdr["MODIFIED_BY"  ]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return itm;
		}

		//"http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetUserModel"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices/GetUserModel", RequestNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2006/03/15/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetUserModel(string Model, string Perspective)
		{
			InitServerInfoHeader();
			string sSemanticModel = ReportService2005.GetUserModel(this.Context, Model, Perspective);
			return UTF8Encoding.UTF8.GetBytes(sSemanticModel);
		}

	}
}
