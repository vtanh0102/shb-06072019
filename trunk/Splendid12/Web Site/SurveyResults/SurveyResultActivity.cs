/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// SurveyResultActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class SurveyResultActivity: SplendidActivity
	{
		public SurveyResultActivity()
		{
			this.Name = "SurveyResultActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(SurveyResultActivity.IDProperty))); }
			set { base.SetValue(SurveyResultActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(SurveyResultActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(SurveyResultActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty SURVEY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_ID", typeof(Guid), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_ID
		{
			get { return ((Guid)(base.GetValue(SurveyResultActivity.SURVEY_IDProperty))); }
			set { base.SetValue(SurveyResultActivity.SURVEY_IDProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(SurveyResultActivity.PARENT_IDProperty))); }
			set { base.SetValue(SurveyResultActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty START_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("START_DATE", typeof(DateTime), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime START_DATE
		{
			get { return ((DateTime)(base.GetValue(SurveyResultActivity.START_DATEProperty))); }
			set { base.SetValue(SurveyResultActivity.START_DATEProperty, value); }
		}

		public static DependencyProperty SUBMIT_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBMIT_DATE", typeof(DateTime), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime SUBMIT_DATE
		{
			get { return ((DateTime)(base.GetValue(SurveyResultActivity.SUBMIT_DATEProperty))); }
			set { base.SetValue(SurveyResultActivity.SUBMIT_DATEProperty, value); }
		}

		public static DependencyProperty IS_COMPLETEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_COMPLETE", typeof(bool), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_COMPLETE
		{
			get { return ((bool)(base.GetValue(SurveyResultActivity.IS_COMPLETEProperty))); }
			set { base.SetValue(SurveyResultActivity.IS_COMPLETEProperty, value); }
		}

		public static DependencyProperty IP_ADDRESSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IP_ADDRESS", typeof(string), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string IP_ADDRESS
		{
			get { return ((string)(base.GetValue(SurveyResultActivity.IP_ADDRESSProperty))); }
			set { base.SetValue(SurveyResultActivity.IP_ADDRESSProperty, value); }
		}

		public static DependencyProperty USER_AGENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("USER_AGENT", typeof(string), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string USER_AGENT
		{
			get { return ((string)(base.GetValue(SurveyResultActivity.USER_AGENTProperty))); }
			set { base.SetValue(SurveyResultActivity.USER_AGENTProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(SurveyResultActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(SurveyResultActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(SurveyResultActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(SurveyResultActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(SurveyResultActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(SurveyResultActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty SURVEY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_NAME", typeof(string), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SURVEY_NAME
		{
			get { return ((string)(base.GetValue(SurveyResultActivity.SURVEY_NAMEProperty))); }
			set { base.SetValue(SurveyResultActivity.SURVEY_NAMEProperty, value); }
		}

		public static DependencyProperty SURVEY_RESULT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SURVEY_RESULT_ID", typeof(Guid), typeof(SurveyResultActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SURVEY_RESULT_ID
		{
			get { return ((Guid)(base.GetValue(SurveyResultActivity.SURVEY_RESULT_IDProperty))); }
			set { base.SetValue(SurveyResultActivity.SURVEY_RESULT_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("SurveyResultActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("SurveyResultActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select SURVEY_RESULTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwSURVEY_RESULTS_AUDIT        SURVEY_RESULTS          " + ControlChars.CrLf
							                + " inner join vwSURVEY_RESULTS_AUDIT        SURVEY_RESULTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on SURVEY_RESULTS_AUDIT_OLD.ID = SURVEY_RESULTS.ID       " + ControlChars.CrLf
							                + "        and SURVEY_RESULTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwSURVEY_RESULTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                        from vwSURVEY_RESULTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                       where vwSURVEY_RESULTS_AUDIT.ID            =  SURVEY_RESULTS.ID           " + ControlChars.CrLf
							                + "                                                         and vwSURVEY_RESULTS_AUDIT.AUDIT_VERSION <  SURVEY_RESULTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                         and vwSURVEY_RESULTS_AUDIT.AUDIT_TOKEN   <> SURVEY_RESULTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                     )" + ControlChars.CrLf
							                + " where SURVEY_RESULTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                    " + ControlChars.CrLf
							                + "  from vwSURVEY_RESULTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwSURVEY_RESULTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                    " + ControlChars.CrLf
							                + "  from vwSURVEY_RESULTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								SURVEY_ID                      = Sql.ToGuid    (rdr["SURVEY_ID"                     ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								START_DATE                     = Sql.ToDateTime(rdr["START_DATE"                    ]);
								SUBMIT_DATE                    = Sql.ToDateTime(rdr["SUBMIT_DATE"                   ]);
								IS_COMPLETE                    = Sql.ToBoolean (rdr["IS_COMPLETE"                   ]);
								IP_ADDRESS                     = Sql.ToString  (rdr["IP_ADDRESS"                    ]);
								USER_AGENT                     = Sql.ToString  (rdr["USER_AGENT"                    ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								if ( !bPast )
								{
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
									SURVEY_NAME                    = Sql.ToString  (rdr["SURVEY_NAME"                   ]);
									SURVEY_RESULT_ID               = Sql.ToGuid    (rdr["SURVEY_RESULT_ID"              ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SurveyResultActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("SURVEY_RESULTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spSURVEY_RESULTS_Update
								( ref gID
								, SURVEY_ID
								, PARENT_ID
								, START_DATE
								, SUBMIT_DATE
								, IS_COMPLETE
								, IP_ADDRESS
								, USER_AGENT
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SurveyResultActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

