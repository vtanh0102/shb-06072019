﻿/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.Employees
{
    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdKPI;
        protected SplendidGrid grdEvalution;

        protected HyperLink lnkBasicInfo;
        protected HyperLink lnkKpiInfo;
        protected HyperLink lnkEvaluationResultInfo;

        protected Label lblResultPercent;
        protected Label lblTotalResult;
        protected Label lblAllocateKpiCode;

        protected Label lblEvaluatePeriod;
        protected Label lblFromToMonth;

        protected int nType;

        protected float fPerResult = 0;
        protected float fTotalResult = 0;
        protected string sAllocateKPIACT = string.Empty;
        protected string fromMonth = string.Empty;
        protected string toMonth = string.Empty;
        protected double totalAmount = 0;
        protected double totalFirstMonth = 0;//6 thang dau nam
        protected double totalLastMonth = 0;//6 thang cuoi nam
        protected double totalFullMonth = 0;//12 thang
        protected static double totalValue = 0;
        protected static string period = string.Empty;

        protected void grdKPI_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            string format_number = Sql.ToString(Application["CONFIG.format_number"]);
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                Label lblPERCENT_FINAL_TOTAL = (Label)e.Item.FindControl("lblPERCENT_FINAL_TOTAL");
                if (lblPERCENT_FINAL_TOTAL != null)
                {
                    //lblPERCENT_FINAL_TOTAL.Text = KPIs_Utils.FormatFloat(lblPERCENT_FINAL_TOTAL.Text, format_number);
                }
                Label lblTOTAL_AMOUNT_01 = (Label)e.Item.FindControl("lblTOTAL_AMOUNT_01");
                if (lblTOTAL_AMOUNT_01 != null)
                {
                    lblTOTAL_AMOUNT_01.Text = KPIs_Utils.FormatFloat(lblTOTAL_AMOUNT_01.Text, format_number);
                }
            }
            //if (e.Item.ItemType == ListItemType.Footer)
            //{
            //    Label lblTotalPERCENT = (Label)e.Item.FindControl("lblTotalPERCENT");
            //    if (lblTotalPERCENT != null)
            //    {
            //        lblTotalPERCENT.Text = totalValue.ToString();
            //    }
            //}

        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Response.Redirect("edit.aspx?ID=" + gID.ToString());
            }
            else if (e.CommandName == "Duplicate")
            {
                Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
            }
            else if (e.CommandName == "ResetDefaults")
            {
                ctlDynamicButtons.ErrorText = "Reset is not supported at this time.";
            }
            else if (e.CommandName == "Cancel")
            {
                Response.Redirect("default.aspx");
            }
            else if (e.CommandName == "Delete")
            {
                SqlProcs.spUSERS_Delete(gID);
                Response.Redirect("default.aspx");
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            totalValue = 0;
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            // 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                // 12/06/2005 Paul.  A user can edit himself. 
                ctlDynamicButtons.ShowButton("Edit", !PrintView && ((SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0) || gID == Security.USER_ID));
                ctlDynamicButtons.ShowButton("Duplicate", !PrintView && (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0));
                ctlDynamicButtons.ShowButton("Reset", !PrintView && (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0));
                // 11/28/2005 Paul.  We must always populate the table, otherwise it will disappear during event processing. 
                // 03/19/2008 Paul.  Place AppendDetailViewFields inside OnInit to avoid having to re-populate the data. 
                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {

                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *               " + ControlChars.CrLf
                                 + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf
                                 + " where ID = @ID        " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                Sql.AddParameter(cmd, "@ID", gID);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                // 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            // 11/11/2010 Paul.  Apply Business Rules. 
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                                            ctlDynamicButtons.Title = Sql.ToString(rdr["FULL_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            // 02/13/2013 Paul.  Move relationship append so that it can be controlled by business rules. 
                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire.                                             
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, rdr);

                                            if (Sql.ToBoolean(rdr["IS_SYNC_C"]))
                                                ctlDynamicButtons.ShowButton("Delete", false);
                                            // 11/10/2010 Paul.  Apply Business Rules. 
                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            //if (rdr["CURRENT_POSITION_WORK_TIME_C"] != null && rdr["CURRENT_POSITION_WORK_TIME_C"].ToString() != "")
                                            //    new DynamicControl(this, "CURRENT_POSITION_WORK_TIME_C").Text = DateTime.Parse(rdr["CURRENT_POSITION_WORK_TIME_C"].ToString()).ToString("dd/MM/yyyy");
                                        }
                                        else
                                        {
                                            // 11/25/2006 Paul.  If item is not visible, then don't show its sub panel either. 
                                            plcSubPanel.Visible = false;

                                            // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }
                        //Thong tin KPIs
                        //DataTable dtKPIsEmp = KPIs_Utils.getTotalMonthOfEmployeeWithPeriod(gID, dbf);
                        //if (dtKPIsEmp.Rows.Count > 0)
                        //{
                        //    DataRow rdr = dtKPIsEmp.Rows[0];
                        //    period = rdr["PERIOD"].ToString();
                        //    if (KPIs_Utils.START_MONTH.Equals(period))
                        //    {
                        //        DataTable dtStartMonth = KPIs_Utils.getPercentFinalTotal(gID, "01", "06", dbf);
                        //        if (dtStartMonth.Rows.Count > 0)
                        //        {
                        //            DataRow rdr1 = dtStartMonth.Rows[0];
                        //            totalValue = Sql.ToDouble(rdr1["percent_final_total"]) / 6;
                        //        }
                        //    }
                        //    else if (KPIs_Utils.END_MONTH.Equals(period))
                        //    {
                        //        DataTable dtEndMonth = KPIs_Utils.getPercentFinalTotal(gID, "07", "12", dbf);
                        //        if (dtEndMonth.Rows.Count > 0)
                        //        {
                        //            DataRow rdr2 = dtEndMonth.Rows[0];
                        //            totalValue = Sql.ToDouble(rdr2["percent_final_total"]) / 6;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        DataTable dtFullMonth = KPIs_Utils.getPercentFinalTotal(gID, "01", "12", dbf);
                        //        if (dtFullMonth.Rows.Count > 0)
                        //        {
                        //            DataRow rdr3 = dtFullMonth.Rows[0];
                        //            totalValue = Sql.ToDouble(rdr3["percent_final_total"]) / 12;
                        //        }
                        //    }
                        //}


                        int fromMonth = 1;
                        int toMonth = 12;
                        var monthCurrent = DateTime.Now.Month;
                        if (monthCurrent <= 6)
                        {
                            fromMonth = 1;
                            toMonth = 6;
                            totalValue = KPIs_Utils.getPercentFinalTotalPeriod(gID, DateTime.Now.Year, "01", "06", dbf);
                        }
                        else if (monthCurrent > 6)
                        {
                            fromMonth = 7;
                            toMonth = 12;
                            totalValue = KPIs_Utils.getPercentFinalTotalPeriod(gID, DateTime.Now.Year, "07", "12", dbf);
                        }

                        lblEvaluatePeriod.Text = L10n.Term("Employees.LBL_KPI_PERIOD_EVAL");
                        lblFromToMonth.Text = string.Format(L10n.Term("Employees.LBL_KPI_FROM_TO_MONTH"), string.Format("<b style='color:red;'>{0}</b>", fromMonth), string.Format("<b style='color:red;'>{0}</b>", toMonth));

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                DataTable dtCurrent = new DataTable();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandText = "Get_Data_All_Result_KPI";
                                IDbDataParameter parID = Sql.AddParameter(cmd, "@EmployeeID", gID);
                                IDbDataParameter parFromMonth = Sql.AddParameter(cmd, "@fromMonth", fromMonth);
                                IDbDataParameter parToMonth = Sql.AddParameter(cmd, "@toMonth", toMonth);
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtCurrent);
                                    grdKPI.DataSource = dtCurrent;
                                    // Tam lay =sum % hoan thanh /6
                                    //if (totalValue == 0) -- tam cho Tp/ GDCN
                                    //{
                                    double sum = 0;
                                    for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                    {
                                        sum += Sql.ToDouble(dtCurrent.Rows[i]["ResultKPI"]);
                                    }
                                    totalValue = sum / 6;
                                    //}                                    
                                    grdKPI.Columns[0].FooterText = L10n.Term("Employees.LBL_PERCENT_COMPLETE_PERIOD");
                                    grdKPI.Columns[1].FooterText = totalValue.ToString("N1");


                                    grdKPI.DataBind();
                                }
                            }
                        }

                        // lay du lieu cho KPI                        
                        /*
                        var monthPeriod = DateTime.Now.Month;
                        var year = DateTime.Now.Year;
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = " select * from vwB_KPI_ACTUAL_RESULT_Edit   " + ControlChars.CrLf
                                            +  " WHERE EMPLOYEE_ID = @EMPLOYEE_ID           " + ControlChars.CrLf
                                            +  " AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD    " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AddParameter(cmd, "@EMPLOYEE_ID", gID);
                                Sql.AddParameter(cmd, "@YEAR", year);
                                Sql.AddParameter(cmd, "@MONTH_PERIOD", monthPeriod);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            sAllocateKPIACT = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            fPerResult = Sql.ToFloat(rdr["PERCENT_FINAL_TOTAL"]);
                                            //fTotalResult = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            sAllocateCode = Sql.ToString(rdr["ALLOCATE_CODE"]);
                                        }
                                    }
                                }
                            }                           
                        }

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS_TOI = " SELECT * FROM vwB_KPI_TOI_ACTUAL_RESULT_Edit WHERE EMPLOYEE_ID = @EMPLOYEE_ID  " + ControlChars.CrLf
                                                +  " AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD    " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS_TOI;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                //Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                                Sql.AddParameter(cmd, "@EMPLOYEE_ID", gID);
                                Sql.AddParameter(cmd, "@YEAR", year);
                                Sql.AddParameter(cmd, "@MONTH_PERIOD", monthPeriod);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];

                                            fTotalResult = Sql.ToFloat(rdr["TOTAL_AMOUNT_01"]);
                                        }
                                    }
                                }
                            }
                        }

                        lblAllocateKpiCode.Text = sAllocateCode;
                        lblResultPercent.Text = String.Format("{0:P2}.", fPerResult / 100.00);
                        lblTotalResult.Text = String.Format("{0:#,###}", fTotalResult);

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = " SELECT  Row_Number() OVER(order by KPI_CODE) AS NO, ID, KPI_ID                   " + ControlChars.CrLf
                                             + " , KPI_NAME, KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, DESCRIPTION    " + ControlChars.CrLf
                                             + "  FROM vwB_KPI_ACT_RESULT_DETAIL_Edit  WHERE ACTUAL_RESULT_CODE = @ACTUAL_RESULT_CODE  " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AddParameter(cmd, "@ACTUAL_RESULT_CODE", sAllocateKPIACT);
                                //Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            grdKPI.DataSource = dtCurrent;
                                            grdKPI.DataBind();
                                        }
                                    }
                                }
                            }
                        }
                        */

                        //lay du liẹu danh gia
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = " SELECT *    " + ControlChars.CrLf
                                            + "  FROM vwB_EMP_EVALUATION_Edit  WHERE EMPLOYEE_ID = @EMPLOYEE_ID  " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                //Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                                Sql.AddParameter(cmd, "@EMPLOYEE_ID", gID);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        grdEvalution.DataSource = dtCurrent;
                                        grdEvalution.DataBind();
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.DisableAll();
                        //ctlDynamicButtons.ErrorText = L10n.Term(".ERR_MISSING_REQUIRED_FIELDS") + "ID";
                    }
                }
                else
                {
                    // 06/07/2015 Paul.  Seven theme DetailView.master uses an UpdatePanel, so we need to recall the title. 
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }
                // 06/09/2006 Paul.  Remove data binding in the user controls.  Binding is required, but only do so in the ASPX pages. 
                //Page.DataBind();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            grdKPI.ItemDataBound += new DataGridItemEventHandler(grdKPI_ItemDataBound);
            m_sMODULE = "Employees";

            nType = Sql.ToInteger(Request["Type"]);
            var id = Request["Id"];

            SetMenu(m_sMODULE);

            lnkBasicInfo.CssClass = (nType == 0) ? "current" : "";
            lnkKpiInfo.CssClass = (nType == 1) ? "current" : "";
            lnkEvaluationResultInfo.CssClass = (nType == 2) ? "current" : "";

            lnkBasicInfo.NavigateUrl = Page.AppRelativeVirtualPath + "?id=" + id + "&Type=0" + (ArchiveViewEnabled() ? "&ArchiveView=1" : String.Empty);
            lnkKpiInfo.NavigateUrl = Page.AppRelativeVirtualPath + "?id=" + id + "&Type=1" + (ArchiveViewEnabled() ? "&ArchiveView=1" : String.Empty);
            lnkEvaluationResultInfo.NavigateUrl = Page.AppRelativeVirtualPath + "?id=" + id + "&Type=2" + (ArchiveViewEnabled() ? "&ArchiveView=1" : String.Empty);

            if (IsPostBack)
            {
                // 02/13/2013 Paul.  Move relationship append so that it can be controlled by business rules. 
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
            }
        }
        #endregion
    }
}


