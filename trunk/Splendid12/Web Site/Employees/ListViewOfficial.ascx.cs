/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.Employees
{
    /// <summary>
    ///		Summary description for ListView.
    /// </summary>
    public class ListViewOfficial : SplendidControl
    {
        // 06/05/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlModuleHeader;
        protected _controls.ExportHeader ctlExportHeader;
        protected _controls.SearchView ctlSearchView;
        protected _controls.CheckAll ctlCheckAll;

        protected UniqueStringCollection arrSelectFields;
        protected DataView vwMain;
        protected SplendidGrid grdMain;
        protected Label lblError;
        protected MassUpdate ctlMassUpdate;

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Search")
                {
                    // 10/13/2005 Paul.  Make sure to clear the page index prior to applying search. 
                    grdMain.CurrentPageIndex = 0;
                    // 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
                    grdMain.DataBind();
                }
                // 12/14/2007 Paul.  We need to capture the sort event from the SearchView. 
                else if (e.CommandName == "SortGrid")
                {
                    grdMain.SetSortFields(e.CommandArgument as string[]);
                    // 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
                    // 03/17/2011 Paul.  We need to treat a comma-separated list of fields as an array. 
                    arrSelectFields.AddFields(grdMain.SortColumn);
                }
                else if (e.CommandName == "Export")
                {
                    // 11/03/2006 Paul.  Apply ACL rules to Export. 
                    int nACLACCESS = SplendidCRM.Security.GetUserAccess(m_sMODULE, "export");
                    if (nACLACCESS >= 0)
                    {
                        // 10/05/2009 Paul.  When exporting, we may need to manually bind.  Custom paging should be disabled when exporting all. 
                        if (vwMain == null)
                            grdMain.DataBind();
                        if (vwMain != null && grdMain.Items.Count > 0)
                        {
                            grdMain.AllowCustomPaging = false;
                            grdMain.AllowPaging = false;
                            var oldSize = grdMain.PageSize;
                            grdMain.PageSize = vwMain.Count;
                            grdMain.DataSource = vwMain;
                            grdMain.DataBind();
                            KPIs_Export.ExportDataGridViewDToExcel(grdMain);
                            grdMain.PageSize = oldSize;
                            grdMain.AllowPaging = false;
                            grdMain.AllowCustomPaging = true;
                            grdMain.DataBind();
                        }
                        else
                        {
                            //lblError.Text += ControlChars.CrLf + "vwMain is null.";
                            lblError.Text += ControlChars.CrLf + L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND");
                        }
                        // 05/10/2007 Paul.  ACL_ACCESS.OWNER is not defined for the EMPLOYEES module. 
                        //if ( nACLACCESS == ACL_ACCESS.OWNER )
                        //	vwMain.RowFilter = "ASSIGNED_USER_ID = '" + Security.USER_ID.ToString() + "'";
                        //string[] arrID = null;  // 11/27/2010 Paul.  Checkbox selection is not supported for this module. 
                        //SplendidExport.Export(vwMain, m_sMODULE, ctlExportHeader.ExportFormat, ctlExportHeader.ExportRange, grdMain.CurrentPageIndex, grdMain.PageSize, arrID, grdMain.AllowCustomPaging);
                    }
                }
                else if (e.CommandName == "SelectAll")
                {
                    if (vwMain == null)
                        grdMain.DataBind();
                    ctlCheckAll.SelectAll(vwMain, "ID");
                    grdMain.DataBind();
                }
                else if (e.CommandName == "MassUpdate")
                {
                    // 11/27/2010 Paul.  Use new selected items. 
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        // 10/26/2007 Paul.  Use a stack to run the update in blocks of under 200 IDs. 
                        //string sIDs = Utils.ValidateIDs(arrID);
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                // 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            // 08/29/2009 Paul.  Add support for dynamic teams. 
                                            // 05/13/2016 Paul.  Add Tags module. 
                                            SqlProcs.spUSERS_MassUpdate(sIDs, ctlMassUpdate.ASSIGNED_USER_ID, ctlMassUpdate.PRIMARY_TEAM_ID, ctlMassUpdate.TEAM_SET_LIST, ctlMassUpdate.ADD_TEAM_SET, string.Empty, true, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("official.aspx");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        // 09/08/2009 Paul.  Add support for custom paging. 
        protected void grdMain_OnSelectMethod(int nCurrentPageIndex, int nPageSize)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    string sTABLE_NAME = "EMPLOYEES";
                    cmd.CommandText = "  from vw" + sTABLE_NAME + "_List" + ControlChars.CrLf
                                    + " where 1 = 1" + ControlChars.CrLf
                                    + " AND   ISNULL(USER_CODE_C, '') <> ''         " + ControlChars.CrLf
                                    + " AND IS_SYNC_C = 1";
                    //Security.Filter(cmd, m_sMODULE, "list");
                    ctlSearchView.SqlSearchClauseCustom(cmd, m_sMODULE);
                    // 09/23/2015 Paul.  Paginated results still need to specify export fields. 
                    cmd.CommandText = "select " + (Request.Form[ctlExportHeader.ExportUniqueID] != null ? SplendidDynamic.ExportGridColumns(m_sMODULE + ".Export", arrSelectFields) : Sql.FormatSelectFields(arrSelectFields))
                                    + cmd.CommandText;
                    if (nPageSize > 0)
                    {
                        Sql.PageResults(cmd, sTABLE_NAME, grdMain.OrderByClause(), nCurrentPageIndex, nPageSize);
                    }
                    else
                    {
                        cmd.CommandText += grdMain.OrderByClause();
                    }

                    if (bDebug)
                        RegisterClientScriptBlock("SQLPaged", Sql.ClientScriptBlock(cmd));

                    // 01/13/2010 Paul.  Allow default search to be disabled. 
                    if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                    {
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);
                                // 11/22/2010 Paul.  Apply Business Rules. 
                                this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);

                                vwMain = dt.DefaultView;
                                grdMain.DataSource = vwMain;
                            }
                        }
                        ctlExportHeader.Visible = true;
                    }
                    else
                    {
                        ctlExportHeader.Visible = false;
                    }
                    ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                    ctlCheckAll.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                }
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
            // 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
            if (!this.Visible)
                return;

            try
            {
                // 09/08/2009 Paul.  Add support for custom paging in a DataGrid. Custom paging can be enabled / disabled per module. 
                if (Crm.Config.allow_custom_paging() && Crm.Modules.CustomPaging(m_sMODULE))
                {
                    // 10/05/2009 Paul.  We need to make sure to disable paging when exporting all. 
                    // 01/24/2018 Paul.  Disable custom paging if Selected Records is used as selection can cross pages. 
                    grdMain.AllowCustomPaging = (Request.Form[ctlExportHeader.ExportUniqueID] == null || ctlExportHeader.ExportRange == "Page");
                    grdMain.SelectMethod += new SelectMethodHandler(grdMain_OnSelectMethod);
                    ctlCheckAll.ShowSelectAll = false;
                }

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        grdMain.OrderByClause("FULL_NAME", "asc");

                        // 02/19/2010 Paul.  We cannot lookup the table.  We must hard-code the table to be EMPLOYEES 
                        // The reason is that the lookup will return USERS, and that is the wrong view to use here. 
                        string sTABLE_NAME = "EMPLOYEES";
                        cmd.CommandText = "  from vw" + sTABLE_NAME + "_List" + ControlChars.CrLf
                                        + " where 1 = 1           " + ControlChars.CrLf
                                        + " AND   ISNULL(USER_CODE_C, '') <> ''         " + ControlChars.CrLf
                                        + " AND   IS_SYNC_C = 1 AND ORGANIZATION_PARENT_ID_C IS NOT NULL ";
                        // 05/10/2007 Paul.  ACL_ACCESS.OWNER is not defined for the EMPLOYEES module. 
                        //Security.Filter(cmd, m_sMODULE, "list");

                        ctlSearchView.SqlSearchClauseCustom(cmd, m_sMODULE);
                        // 09/08/2009 Paul.  Custom paging will always require two queries, the first is to get the total number of rows. 
                        if (grdMain.AllowCustomPaging)
                        {
                            cmd.CommandText = "select count(*)" + ControlChars.CrLf
                                            + cmd.CommandText;

                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                            // 01/13/2010 Paul.  Allow default search to be disabled. 
                            if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                            {
                                grdMain.VirtualItemCount = Sql.ToInteger(cmd.ExecuteScalar());
                            }
                        }
                        else
                        {
                            // 04/27/2008 Paul.  The fields in the search clause need to be prepended after any Saved Search sort has been determined.
                            // 09/08/2009 Paul.  The order by clause is separate as it can change due to SearchViews. 
                            // 04/20/2011 Paul.  If there are no fields in the GridView.Export, then return all fields (*). 
                            // 09/23/2015 Paul.  Need to include the data grid fields as it will be bound using the same data set. 
                            cmd.CommandText = "select " + (Request.Form[ctlExportHeader.ExportUniqueID] != null ? SplendidDynamic.ExportGridColumns(m_sMODULE + ".Export", arrSelectFields) : Sql.FormatSelectFields(arrSelectFields))
                                            + cmd.CommandText
                                            + grdMain.OrderByClause();

                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                            // 01/13/2010 Paul.  Allow default search to be disabled. 
                            if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                            {
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        // 11/22/2010 Paul.  Apply Business Rules. 
                                        this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);

                                        vwMain = dt.DefaultView;
                                        grdMain.DataSource = vwMain;
                                    }
                                }
                                ctlExportHeader.Visible = true;
                            }
                            else
                            {
                                ctlExportHeader.Visible = false;
                            }
                            ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                            ctlCheckAll.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                        }
                    }
                }
                if (!IsPostBack)
                {
                    // 06/09/2006 Paul.  Remove data binding in the user controls.  Binding is required, but only do so in the ASPX pages. 
                    //Page.DataBind();
                    // 09/08/2009 Paul.  Let the grid handle the differences between normal and custom paging. 
                    // 09/08/2009 Paul.  Bind outside of the existing connection so that a second connect would not get created. 
                    grdMain.DataBind();
                }


                ListControl lstORGPARENT = ctlSearchView.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;
                if (lstORGPARENT != null)
                {
                    lstORGPARENT.SelectedIndexChanged += new EventHandler(ddrORGPARENT_SelectIndexChange);
                    lstORGPARENT.AutoPostBack = true;
                }

                ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "edit") > 0 ? true : false;
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        private void ddrORGPARENT_SelectIndexChange(object sender, EventArgs e)
        {
            ListControl lstORG = ctlSearchView.FindControl("ORGANIZATION_ID_C") as ListControl;
            ListControl lstORGPARENT = ctlSearchView.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;

            if (lstORG != null && lstORGPARENT != null)
            {
                var dtSource = SplendidCache.M_ORGANIZATION_POS(lstORGPARENT.SelectedValue);
                if (dtSource == null || dtSource.Rows.Count == 0)
                {
                    dtSource = SplendidCache.M_ORGANIZATION_POS();
                }

                lstORG.DataSource = dtSource;
                lstORG.DataTextField = "ORGANIZATION_NAME";
                lstORG.DataValueField = "ID";
                lstORG.DataBind();
                lstORG.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlSearchView.Command += new CommandEventHandler(Page_Command);
            ctlExportHeader.Command += new CommandEventHandler(Page_Command);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);
            ctlCheckAll.Command += new CommandEventHandler(Page_Command);
            // 11/26/2005 Paul.  Add fields early so that sort events will get called. 
            m_sMODULE = "Employees";
            SetMenu(m_sMODULE);
            // 02/08/2008 Paul.  We need to build a list of the fields used by the search clause. 
            arrSelectFields = new UniqueStringCollection();
            arrSelectFields.Add("FULL_NAME");
            this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields);
            if (Security.GetUserAccess(m_sMODULE, "delete") < 0 && Security.GetUserAccess(m_sMODULE, "edit") < 0)
                ctlMassUpdate.Visible = false;

            // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
            if (SplendidDynamic.StackedLayout(Page.Theme))
            {
                ctlModuleHeader.Command += new CommandEventHandler(Page_Command);
                ctlModuleHeader.AppendButtons(m_sMODULE + "." + LayoutListView, Guid.Empty, null);
                grdMain.IsMobile = this.IsMobile;
                grdMain.MassUpdateView = m_sMODULE + ".MassUpdate";
                grdMain.Command += new CommandEventHandler(Page_Command);
            }
        }
        #endregion
    }
}

