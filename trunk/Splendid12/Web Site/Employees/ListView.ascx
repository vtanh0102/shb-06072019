<%@ Control CodeBehind="ListView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Employees.ListView" %>
<script runat="server">
/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divListView">
    <%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlModuleHeader" Module="Employees" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="SearchView" Src="~/_controls/SearchView.ascx" %>
    <SplendidCRM:SearchView ID="ctlSearchView" Module="Employees" Visible="<%# !PrintView %>" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="ExportHeader" Src="~/_controls/ExportHeader.ascx" %>
    <SplendidCRM:ExportHeader ID="ctlExportHeader" Module="Employees" Title="Employees.LBL_LIST_FORM_TITLE" runat="Server" />

    <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
        <asp:Label ID="lblError" CssClass="error" EnableViewState="false" runat="server" />
    </asp:Panel>

    <asp:HiddenField ID="LAYOUT_LIST_VIEW" runat="server" />
    <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText="" ItemStyle-Width="1%">
                <ItemTemplate><%# grdMain.InputCheckbox(!PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE), ctlCheckAll.FieldName, Sql.ToGuid(Eval("ID")), ctlCheckAll.SelectedItems) %></ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

             <SplendidCRM:InlineScript runat="server">      
        <script type="text/javascript">
            $(function () {
                //$('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_divExport]').hide();
                $('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_lstEXPORT_RANGE]').hide();
                $('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_lstEXPORT_FORMAT]').hide();
            });
        </script>
    </SplendidCRM:InlineScript>

    <%@ Register TagPrefix="SplendidCRM" TagName="CheckAll" Src="~/_controls/CheckAll.ascx" %>
    <SplendidCRM:CheckAll ID="ctlCheckAll" Visible="<%# !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
    <asp:Panel ID="pnlMassUpdateSeven" runat="server">
        <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
        <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
    </asp:Panel>

    <%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
    <SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
</div>


