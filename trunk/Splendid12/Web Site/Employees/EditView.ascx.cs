﻿/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Diagnostics;
using SplendidCRM._modules;
using SplendidCRM._controls;

namespace SplendidCRM.Employees
{
    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlDynamicButtons;
        // 01/13/2010 Paul.  Add footer buttons. 
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected Label lbTitle;
        protected Label lbWarning;

        // 09/02/2012 Paul.  EditViews were combined into a single view. 
        //protected HtmlTable       tblAddress                      ;
        protected HtmlTable tblStatus;

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            // 03/15/2014 Paul.  Enable override of concurrency error. 
            if (e.CommandName == "Save" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    if (Sql.IsEmptyGuid(gID))
                    {
                        // 01/16/2006 Paul.  Enable validator before validating page. 
                        this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                        this.ValidateEditViewFields(m_sMODULE + ".EditAddress");
                    }
                    else
                    {
                        string sSQL;
                        sSQL = "select *               " + ControlChars.CrLf
                             + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf;
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            // 11/18/2007 Paul.  Use the current values for any that are not defined in the edit view. 
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                Security.Filter(cmd, m_sMODULE, "edit");
                                Sql.AppendParameter(cmd, gID, "ID", false);
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtCurrent);
                                    if (dtCurrent.Rows.Count > 0)
                                    {
                                        rowCurrent = dtCurrent.Rows[0];
                                        if (!Sql.ToBoolean(rowCurrent["IS_SYNC_C"]))
                                        {
                                            this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                                            this.ValidateEditViewFields(m_sMODULE + ".EditAddress");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // 11/10/2010 Paul.  Apply Business Rules. 
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + ".EditAddress");
                    if (Page.IsValid)
                    {
                        var sqlMore = "";
                        string sTABLE_NAME = "USERS";
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            // 11/18/2007 Paul.  Use the current values for any that are not defined in the edit view. 
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                sqlMore = " AND ID <> '" + gID.ToString() + "'";
                                string sSQL;
                                sSQL = "select *               " + ControlChars.CrLf
                                     + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;
                                    Security.Filter(cmd, m_sMODULE, "edit");
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            // 12/09/2008 Paul.  Throw an exception if the record has been edited since the last load. 
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            // 03/15/2014 Paul.  Enable override of concurrency error. 
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }

                                            if (Sql.ToBoolean(rowCurrent["IS_SYNC_C"]))
                                            {
                                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                                {
                                                    try
                                                    {
                                                        KPIs_UserExtend.spB_USER_Update_DATEWork(ref gID, new DynamicControl(this, "CURRENT_POSITION_WORK_TIME_C").DateValue, trn);
                                                        trn.Commit();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        trn.Rollback();
                                                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                                        ctlDynamicButtons.ErrorText = ex.Message;
                                                        return;
                                                    }

                                                    if (!Sql.IsEmptyString(RulesRedirectURL))
                                                        Response.Redirect(RulesRedirectURL);
                                                    else
                                                        Response.Redirect("view.aspx?ID=" + gID.ToString());

                                                    return;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // 11/19/2007 Paul.  If the record is not found, clear the ID so that the record cannot be updated.
                                            // It is possible that the record exists, but that ACL rules prevent it from being selected. 
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            var usercode = new SplendidCRM.DynamicControl(this, rowCurrent, "USER_CODE_C").Text;
                            string sQLCheckUserCode;
                            sQLCheckUserCode = "select *           " + ControlChars.CrLf
                                 + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf
                                 + "  where USER_CODE_C = '" + usercode.ToString() + "'" + ControlChars.CrLf
                                 + sqlMore;

                            using (IDbCommand cmdCheckApprove = con.CreateCommand())
                            {
                                cmdCheckApprove.CommandText = sQLCheckUserCode;
                                var dtCheck = new DataTable();
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmdCheckApprove;
                                    da.Fill(dtCheck);
                                    if (dtCheck.Rows.Count > 0)
                                    {
                                        throw (new Exception(String.Format(L10n.Term(".ERR_USER_CODE_EXIST"))));
                                    }
                                }
                            }

                            // 11/10/2010 Paul.  Apply Business Rules. 
                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);

                            //change value of bussiness code via Title_ID_C
                            var lstTitle = this.FindControl("TITLE_ID_C") as DropDownList;
                            if (lstTitle != null)
                            {
                                if (lstTitle.SelectedItem.Text.Trim() == "Giao dịch viên")
                                {
                                    new SplendidCRM.DynamicControl(this, rowCurrent, "M_BUSINESS_CODE_C").SelectedValue = "DVKH";
                                }
                                else if (lstTitle.SelectedItem.Text.Trim() == "Chuyên viên Quan hệ KHCN")
                                {
                                    new SplendidCRM.DynamicControl(this, rowCurrent, "M_BUSINESS_CODE_C").SelectedValue = "KHCN";
                                }
                            }

                            var lstOrg = this.FindControl("ORGANIZATION_ID_C") as DropDownList;
                            if (lstOrg != null)
                            {
                                DbProviderFactory dbf1 = DbProviderFactories.GetFactory();
                                using (IDbConnection con1 = dbf.CreateConnection())
                                {
                                    string sSQL;
                                    sSQL = "select TOP 1 AREA_ID           " + ControlChars.CrLf
                                    + "  from vwM_ORGANIZATION_Edit" + ControlChars.CrLf
                                    + "  where ID = '" + lstOrg.SelectedValue + "'" + ControlChars.CrLf;
                                    using (IDbCommand cmd1 = con1.CreateCommand())
                                    {
                                        cmd1.CommandText = sSQL;
                                        using (DbDataAdapter da1 = dbf1.CreateDataAdapter())
                                        {
                                            ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                            using (DataTable dtCurrent1 = new DataTable())
                                            {
                                                da1.Fill(dtCurrent1);
                                                if (dtCurrent1.Rows.Count > 0)
                                                {
                                                    new SplendidCRM.DynamicControl(this, rowCurrent, "AREA_C").SelectedValue = dtCurrent1.Rows[0]["AREA_ID"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    // 11/18/2007 Paul.  Use the current values for any that are not defined in the edit view. 
                                    SqlProcs.spEMPLOYEES_Update
                                        (ref gID
                                        , new DynamicControl(this, rowCurrent, "FIRST_NAME").Text
                                        , new DynamicControl(this, rowCurrent, "LAST_NAME").Text
                                        , new DynamicControl(this, rowCurrent, "REPORTS_TO_ID").ID
                                        , new DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new DynamicControl(this, rowCurrent, "TITLE").Text
                                        , new DynamicControl(this, rowCurrent, "DEPARTMENT").Text
                                        , new DynamicControl(this, rowCurrent, "PHONE_HOME").Text
                                        , new DynamicControl(this, rowCurrent, "PHONE_MOBILE").Text
                                        , new DynamicControl(this, rowCurrent, "PHONE_WORK").Text
                                        , new DynamicControl(this, rowCurrent, "PHONE_OTHER").Text
                                        , new DynamicControl(this, rowCurrent, "PHONE_FAX").Text
                                        , new DynamicControl(this, rowCurrent, "EMAIL1").Text
                                        , new DynamicControl(this, rowCurrent, "EMAIL2").Text
                                        , new DynamicControl(this, rowCurrent, "ADDRESS_STREET").Text
                                        , new DynamicControl(this, rowCurrent, "ADDRESS_CITY").Text
                                        , new DynamicControl(this, rowCurrent, "ADDRESS_STATE").Text
                                        , new DynamicControl(this, rowCurrent, "ADDRESS_POSTALCODE").Text
                                        , new DynamicControl(this, rowCurrent, "ADDRESS_COUNTRY").Text
                                        , new DynamicControl(this, rowCurrent, "EMPLOYEE_STATUS").SelectedValue
                                        , new DynamicControl(this, rowCurrent, "MESSENGER_ID").Text
                                        , new DynamicControl(this, rowCurrent, "MESSENGER_TYPE").SelectedValue
                                        , trn
                                        );
                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    KPIs_UserExtend.spB_USER_Update_Seniority(ref gID, new DynamicControl(this, "DATE_START_WORK_C").DateValue, trn);
                                    trn.Commit();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            // 11/10/2010 Paul.  Apply Business Rules. 
                            // 12/10/2012 Paul.  Provide access to the item data. 
                            rowCurrent = Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                {
                    string sSQLCheckEmployee;
                    sSQLCheckEmployee = "select *               " + ControlChars.CrLf
                         + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf;
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        con.Open();
                        // 11/18/2007 Paul.  Use the current values for any that are not defined in the edit view. 
                        DataRow rowCurrent = null;
                        DataTable dtCurrent = new DataTable();
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQLCheckEmployee;
                            Security.Filter(cmd, m_sMODULE, "edit");
                            Sql.AppendParameter(cmd, gID, "ID", false);
                            using (DbDataAdapter da = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da).SelectCommand = cmd;
                                da.Fill(dtCurrent);
                                if (dtCurrent.Rows.Count > 0)
                                {
                                    rowCurrent = dtCurrent.Rows[0];
                                    if (Sql.ToBoolean(rowCurrent["IS_SYNC_C"]))
                                    {
                                        Response.Redirect("official.aspx?ID=" + gID.ToString());
                                    }
                                    else
                                    {
                                        Response.Redirect("view.aspx?ID=" + gID.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            // 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                // 06/09/2006 Paul.  Remove data binding in the user controls.  Binding is required, but only do so in the ASPX pages. 
                //Page.DataBind();
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        SetPageTitle(L10n.Term("Employees.EditTitleTTS"));
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *               " + ControlChars.CrLf
                                 + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf
                                 + " where ID = @ID        " + ControlChars.CrLf;
                            // + " AND IS_SYNC_C = 'False'        " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AddParameter(cmd, "@ID", gDuplicateID);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AddParameter(cmd, "@ID", gID);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                // 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            // 11/11/2010 Paul.  Apply Business Rules. 
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                                            ctlDynamicButtons.Title = Sql.ToString(rdr["FULL_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);

                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            // 09/02/2012 Paul.  EditViews were combined into a single view. 
                                            //this.AppendEditViewFields(m_sMODULE + ".EditAddress", tblAddress    , rdr);
                                            //this.AppendEditViewFields(m_sMODULE + ".EditStatus" , tblStatus     , rdr);
                                            // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);

                                            if (Sql.ToBoolean(rdr["IS_SYNC_C"]))
                                            {
                                                lbTitle.Text = L10n.Term("Employees.LBL_EMPLOYEE_INFORMATION");
                                                lbWarning.Text = L10n.Term("Employees.LBL_WARNING_UPDATE_EMPLOYEE");

                                                var tbFirstName = this.FindControl("FIRST_NAME") as TextBox;
                                                if (tbFirstName != null)
                                                    tbFirstName.ReadOnly = true;

                                                var tbLastName = this.FindControl("LAST_NAME") as TextBox;
                                                if (tbLastName != null)
                                                    tbLastName.ReadOnly = true;

                                                var lstTypeIdentification = this.FindControl("TYPE_IDENTIFICATION_ID_C") as DropDownList;
                                                if (lstTypeIdentification != null)
                                                    lstTypeIdentification.Attributes.Add("disabled", "disabled");

                                                var tbIdentification = this.FindControl("IDENTIFICATION_C") as TextBox;
                                                if (tbIdentification != null)
                                                    tbIdentification.Attributes.Add("disabled", "disabled");

                                                var lstTitle = this.FindControl("TITLE_ID_C") as DropDownList;
                                                if (lstTitle != null)
                                                {
                                                    var valueSelect = lstTitle.SelectedValue;
                                                    lstTitle.DataSource = SplendidCache.M_TITLE();
                                                    lstTitle.DataValueField = "ID";
                                                    lstTitle.DataTextField = "TITLE_NAME";
                                                    lstTitle.DataBind();
                                                    lstTitle.SelectedValue = valueSelect;
                                                    lstTitle.Attributes.Add("disabled", "disabled");
                                                }

                                                var lstOrganizationParent = this.FindControl("ORGANIZATION_PARENT_ID_C") as DropDownList;
                                                if (lstOrganizationParent != null)
                                                    lstOrganizationParent.Attributes.Add("disabled", "disabled");

                                                var lstOrganization = this.FindControl("ORGANIZATION_ID_C") as DropDownList;
                                                if (lstOrganization != null)
                                                    lstOrganization.Attributes.Add("disabled", "disabled");

                                                var lstSex = this.FindControl("SEX_C") as DropDownList;
                                                if (lstSex != null)
                                                    lstSex.Attributes.Add("disabled", "disabled");

                                                var tbLiteracy = this.FindControl("LITERACY_ID_C") as TextBox;
                                                if (tbLiteracy != null)
                                                    tbLiteracy.Attributes.Add("disabled", "disabled");

                                                var tbUniversity = this.FindControl("UNIVERSITY_C") as TextBox;
                                                if (tbUniversity != null)
                                                    tbUniversity.Attributes.Add("disabled", "disabled");

                                                var tbPhoneWork = this.FindControl("PHONE_WORK") as TextBox;
                                                if (tbPhoneWork != null)
                                                    tbPhoneWork.Attributes.Add("disabled", "disabled");

                                                var tbEmail = this.FindControl("EMAIL1") as TextBox;
                                                if (tbEmail != null)
                                                    tbEmail.Attributes.Add("disabled", "disabled");

                                                var tbAddressStreet = this.FindControl("ADDRESS_STREET") as TextBox;
                                                if (tbAddressStreet != null)
                                                    tbAddressStreet.Attributes.Add("disabled", "disabled");

                                                var lstContractType = this.FindControl("CONTRACT_TYPE_C") as DropDownList;
                                                if (lstContractType != null)
                                                    lstContractType.Attributes.Add("disabled", "disabled");

                                                var lstContractTerm = this.FindControl("CONTRACT_TERM_C") as DropDownList;
                                                if (lstContractTerm != null)
                                                    lstContractTerm.Attributes.Add("disabled", "disabled");

                                                var dpBirthDate = this.FindControl("BIRTH_DATE_C") as DatePicker;
                                                if (dpBirthDate != null)
                                                {
                                                    dpBirthDate.Attributes.Add("disabled", "disabled");
                                                    dpBirthDate.Enabled = false;
                                                }

                                                var dpDateStartWork = this.FindControl("DATE_START_WORK_C") as DatePicker;
                                                if (dpDateStartWork != null)
                                                {
                                                    dpDateStartWork.Attributes.Add("disabled", "disabled");
                                                    dpDateStartWork.Enabled = false;
                                                }

                                                var dpDateEndWork = this.FindControl("DATE_END_WORK_C") as DatePicker;
                                                if (dpDateEndWork != null)
                                                {
                                                    dpDateEndWork.Attributes.Add("disabled", "disabled");
                                                    dpDateEndWork.Enabled = false;
                                                }

                                                var lstBussinessCode = this.FindControl("M_BUSINESS_CODE_C") as DropDownList;
                                                if (lstBussinessCode != null)
                                                    lstBussinessCode.Attributes.Add("disabled", "disabled");
                                            }

                                            if (!Sql.ToBoolean(rdr["IS_SYNC_C"]))
                                            {
                                                var dpCurrentTime = this.FindControl("CURRENT_POSITION_WORK_TIME_C") as DatePicker;
                                                if (dpCurrentTime != null)
                                                    dpCurrentTime.Visible = false;

                                                var lbCurrentTime = this.FindControl("CURRENT_POSITION_WORK_TIME_C_LABEL") as Label;
                                                if (lbCurrentTime != null)
                                                    lbCurrentTime.Visible = false;

                                                var lbCurrentTimeRequ = this.FindControl("CURRENT_POSITION_WORK_TIME_C_REQUIRED_SYMBOL") as Label;
                                                if (lbCurrentTimeRequ != null)
                                                    lbCurrentTimeRequ.Visible = false;

                                            }

                                            // 07/12/2006 Paul.  Status can only be edited by an administrator. 
                                            //DropDownList lstEMPLOYEE_STATUS = FindControl("EMPLOYEE_STATUS") as DropDownList;
                                            //if ( lstEMPLOYEE_STATUS != null )
                                            //    lstEMPLOYEE_STATUS.Enabled = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
                                            // 12/09/2008 Paul.  Throw an exception if the record has been edited since the last load. 
                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            // 11/10/2010 Paul.  Apply Business Rules. 
                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        SetPageTitle(L10n.Term("Employees.NewsTitleTTS"));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        // 09/02/2012 Paul.  EditViews were combined into a single view. 
                        //this.AppendEditViewFields(m_sMODULE + ".EditAddress", tblAddress    , null);
                        this.AppendEditViewFields(m_sMODULE + ".EditStatus", tblStatus, null);
                        // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        // 11/10/2010 Paul.  Apply Business Rules. 
                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);

                        ListControl listContractType = this.FindControl("CONTRACT_TYPE_C") as ListControl;
                        if (listContractType != null)
                        {
                            listContractType.SelectedValue = "9";
                        }

                        var dpCurrentTime = this.FindControl("CURRENT_POSITION_WORK_TIME_C") as DatePicker;
                        if (dpCurrentTime != null)
                            dpCurrentTime.Visible = false;

                        var lbCurrentTime = this.FindControl("CURRENT_POSITION_WORK_TIME_C_LABEL") as Label;
                        if (lbCurrentTime != null)
                            lbCurrentTime.Visible = false;

                        var lbCurrentTimeRequ = this.FindControl("CURRENT_POSITION_WORK_TIME_C_REQUIRED_SYMBOL") as Label;
                        if (lbCurrentTimeRequ != null)
                            lbCurrentTimeRequ.Visible = false;


                        //gen user code                           
                        Label lblUserCode = this.FindControl("USER_CODE_C") as Label;
                        if (lblUserCode != null && string.IsNullOrEmpty(lblUserCode.Text))
                        {
                            var nUserCode = "";

                            while (true)
                            {
                                nUserCode = "TTS" + new Random().Next(0, 99999).ToString("00000");
                                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                                using (IDbConnection con = dbf.CreateConnection())
                                {
                                    string sSQL;
                                    sSQL = "select *           " + ControlChars.CrLf
                                    + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf
                                    + "  where USER_CODE_C = '" + nUserCode + "'" + ControlChars.CrLf;
                                    using (IDbCommand cmd = con.CreateCommand())
                                    {
                                        cmd.CommandText = sSQL;
                                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                                        {
                                            ((IDbDataAdapter)da).SelectCommand = cmd;
                                            using (DataTable dtCurrent = new DataTable())
                                            {
                                                da.Fill(dtCurrent);
                                                if (dtCurrent.Rows.Count <= 0)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            lblUserCode.Text = nUserCode;
                        }
                    }

                }
                else
                {
                    // 12/02/2005 Paul.  When validation fails, the header title does not retain its value.  Update manually. 
                    // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }

                ListControl lstORGPARENT = this.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;
                if (lstORGPARENT != null)
                {
                    lstORGPARENT.SelectedIndexChanged += new EventHandler(ddrORGPARENT_SelectIndexChange);
                    lstORGPARENT.AutoPostBack = true;
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddrORGPARENT_SelectIndexChange(object sender, EventArgs e)
        {
            ListControl lstORG = this.FindControl("ORGANIZATION_ID_C") as ListControl;
            ListControl lstORGPARENT = this.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;

            if (lstORG != null && lstORGPARENT != null)
            {
                lstORG.DataSource = SplendidCache.M_ORGANIZATION_POS(lstORGPARENT.SelectedValue);
                lstORG.DataTextField = "ORGANIZATION_NAME";
                lstORG.DataValueField = "ID";
                lstORG.DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            m_sMODULE = "Employees";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                // 12/02/2005 Paul.  Need to add the edit fields in order for events to fire. 
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                // 09/02/2012 Paul.  EditViews were combined into a single view. 
                //this.AppendEditViewFields(m_sMODULE + ".EditAddress", tblAddress    , null);
                this.AppendEditViewFields(m_sMODULE + ".EditStatus", tblStatus, null);
                // 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                // 11/10/2010 Paul.  Make sure to add the RulesValidator early in the pipeline. 
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}

