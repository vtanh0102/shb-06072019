<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.Employees.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
    <%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Employees" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <%--	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblStatus" class="tabEditView" runat="server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>--%>    

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                    <tr>
                        <th colspan="4">
                            <h4>
                                <asp:Label ID="lbTitle" Text='<%# L10n.Term("Employees.LBL_EMPLOYEE_SETTINGS") %>' runat="server" /></h4>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="4">
                            <p class="text-danger">
                                <asp:Label ID="lbWarning" Text="" runat="server" /></p>
                        </th>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:InlineScript runat="server">
        <script type="text/javascript">
            $(function () {
                $("[id$=ctl00_cntBody_ctlEditView_DATE_START_WORK_C_lblDateFormat]").hide();
                $("[id$=ctl00_cntBody_ctlEditView_DATE_END_WORK_C_lblDateFormat]").hide();
                $("[id$=ctl00_cntBody_ctlEditView_CURRENT_POSITION_WORK_TIME_C_lblDateFormat]").hide();
                $("[id$=ctl00_cntBody_ctlEditView_BIRTH_DATE_C_lblDateFormat]").hide();                
            });
        </script>
    </SplendidCRM:InlineScript>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

