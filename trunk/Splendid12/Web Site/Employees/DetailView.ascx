﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.Employees.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2005-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<style type="text/css">
    .label-align {
        padding-right: 115px;
    }

    .tabDetailView {
        padding-left: 20px !important;
    }

    .tabDetailViewDF_KPIs {
        padding-left: 328px !important;
    }
</style>
<div id="divDetailView" runat="server">
    <%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="Employees" EnablePrint="true" HelpName="DetailView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />

    <asp:Panel ID="pnlSearchTabs" Visible="true" runat="server">
        <ul class="tablist">
            <li>
                <asp:HyperLink ID="lnkBasicInfo" Text='<%# L10n.Term("Users.LNK_USER_DETAIL_INFO"    ) %>' CssClass="current" runat="server" /></li>
            <li>
                <asp:HyperLink ID="lnkKpiInfo" Text='<%# L10n.Term("Users.LNK_USER_KPI_INFO" ) %>' CssClass="" runat="server" /></li>
            <li>
                <asp:HyperLink ID="lnkEvaluationResultInfo" Text='<%# L10n.Term("Users.LNK_USER_EVALUATION_INFO" ) %>' CssClass="" runat="server" /></li>
        </ul>
    </asp:Panel>
    <table id="tblMain" class="tabDetailView" runat="server" visible="<%# nType==0 %>">
        <tr>
            <th colspan="4" class="dataLabel">
                <h4>
                    <asp:Label Text='<%# L10n.Term("Employees.LBL_EMPLOYEE_INFORMATION") %>' runat="server" />
                </h4>
            </th>
        </tr>
    </table>

    <div id="tblKpi" runat="server" visible="<%# nType==1 %>" style="width: 100%;">
        <table class="tabDetailView">
            <%--     <tr>
                <th colspan="4" class="dataLabel" style="align-content:center;">
                    <h3>
                        <asp:Label Text='<%# L10n.Term("Employees.LBL_KPI_INFORMATION") %>' runat="server" />
                    </h3>
                </th>
            </tr>--%>
            <tr>
                <td class="tabDetailViewDF_KPIs" colspan="2" style="align-content: center;">
                    <h3>
                        <asp:Label ID="lblEvaluatePeriod" runat="server" /></h3>
                </td>
            </tr>
            <tr>
                <td class="tabDetailViewDF_KPIs" colspan="2" style="align-content: center; padding-left: 28px;">
                    <h3>
                        <asp:Label ID="lblFromToMonth" runat="server" /></h3>
                </td>
            </tr>
        </table>

        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                    <asp:HiddenField ID="txtINDEX" runat="server" />
                    <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                    <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
                </asp:Panel>

                <SplendidCRM:SplendidGrid ID="grdKPI" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter="true" runat="server">
                    <Columns>
                        <asp:TemplateColumn HeaderText=".LBL_LIST_MONTH">
                            <FooterStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblMONTH" Text='<%# Bind("Month_period") %>' runat="server" />
                            </ItemTemplate>
                            <%--<FooterTemplate>
                                <asp:Label ID="lblTotal" runat="server" Text='<%# L10n.Term("Employees.LBL_PERCENT_COMPLETE_PERIOD") %>'>'></asp:Label>
                            </FooterTemplate>--%>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Employees.LBL_RATE_KPI_COMPLETE">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblPERCENT_FINAL_TOTAL" Text='<%# Bind("ResultKPI") %>' runat="server" />
                            </ItemTemplate>
                            <%--<FooterTemplate>
                                <asp:Label ID="lblTotalPERCENT" runat="server" CssClass="right label-align"></asp:Label>
                            </FooterTemplate>--%>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Employees.LBL_KPI_TOI_TOTAL">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblTOTAL_AMOUNT_01" Text='<%# Bind("ResultKPITOi") %>' runat="server" />
                            </ItemTemplate>
                            <%--<FooterTemplate>
                                <asp:Label ID="lblTotalAMOUNT" runat="server"></asp:Label>
                            </FooterTemplate>--%>
                        </asp:TemplateColumn>
                    </Columns>
                </SplendidCRM:SplendidGrid>

                <SplendidCRM:InlineScript runat="server">
                    <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
                </SplendidCRM:InlineScript>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="tblEvaludation" visible="<%# nType==2 %>" runat="server" class="tabDetailView">
        <br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:Button ID="Button1" Style="display: none" runat="server" />
                    <asp:Label ID="Label2" CssClass="error" EnableViewState="false" runat="server" />
                </asp:Panel>

                <SplendidCRM:SplendidGrid ID="grdEvalution" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0 %>' runat="server">
                    <Columns>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_YEAR">
                            <ItemTemplate>
                                <asp:Label ID="lblYear" Text='<%# Bind("YEAR") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_MARK_1ST">
                            <ItemTemplate>
                                <asp:Label ID="lblMark1st" Text='<%# Bind("MARK_1ST") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_GRADE_1ST">
                            <ItemTemplate>
                                <asp:Label ID="lblGrade1st" Text='<%# Bind("GRADE_1ST") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_DESCRIPTION_1ST">
                            <ItemTemplate>
                                <asp:Label ID="lblDes1st" Text='<%# Bind("DESCRIPTION_1ST") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_MARK_2ST">
                            <ItemTemplate>
                                <asp:Label ID="lblMark2st" Text='<%# Bind("MARK_2ST") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_GRADE_2ND">
                            <ItemTemplate>
                                <asp:Label ID="lblGrade2nd" Text='<%# Bind("GRADE_2ND") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_DESCRIPTION_2ND">
                            <ItemTemplate>
                                <asp:Label ID="lblDes2nd" Text='<%# Bind("DESCRIPTION_2ND") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_MARK_YEAR">
                            <ItemTemplate>
                                <asp:Label ID="lblMarkYear" Text='<%# Bind("MARK_YEAR") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_GRADE_YEAR">
                            <ItemTemplate>
                                <asp:Label ID="lblGradeYear" Text='<%# Bind("GRADE_YEAR") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn HeaderText="EVAB0101.LBL_LIST_DESCRIPTION_YEAR">
                            <ItemTemplate>
                                <asp:Label ID="lblDesYear" Text='<%# Bind("DESCRIPTION_YEAR") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                    </Columns>
                </SplendidCRM:SplendidGrid>

                <SplendidCRM:InlineScript runat="server">
                    <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
                </SplendidCRM:InlineScript>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <div id="divDetailSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />

