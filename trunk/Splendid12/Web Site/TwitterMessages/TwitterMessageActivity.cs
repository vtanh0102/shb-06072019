/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// TwitterMessageActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:32 PM
	/// </summary>
	public class TwitterMessageActivity: SplendidActivity
	{
		public TwitterMessageActivity()
		{
			this.Name = "TwitterMessageActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.TEAM_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(TwitterMessageActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.NAMEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(TwitterMessageActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty DATE_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.DATE_TIMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.DATE_TIMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(TwitterMessageActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.PARENT_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TYPEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TYPEProperty, value); }
		}

		public static DependencyProperty TWITTER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_ID", typeof(Int64), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int64 TWITTER_ID
		{
			get { return ((Int64)(base.GetValue(TwitterMessageActivity.TWITTER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.TWITTER_IDProperty, value); }
		}

		public static DependencyProperty TWITTER_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_USER_ID", typeof(Int64), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int64 TWITTER_USER_ID
		{
			get { return ((Int64)(base.GetValue(TwitterMessageActivity.TWITTER_USER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.TWITTER_USER_IDProperty, value); }
		}

		public static DependencyProperty TWITTER_FULL_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_FULL_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TWITTER_FULL_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TWITTER_FULL_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TWITTER_FULL_NAMEProperty, value); }
		}

		public static DependencyProperty TWITTER_SCREEN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_SCREEN_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TWITTER_SCREEN_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TWITTER_SCREEN_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TWITTER_SCREEN_NAMEProperty, value); }
		}

		public static DependencyProperty ORIGINAL_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORIGINAL_ID", typeof(Int64), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int64 ORIGINAL_ID
		{
			get { return ((Int64)(base.GetValue(TwitterMessageActivity.ORIGINAL_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.ORIGINAL_IDProperty, value); }
		}

		public static DependencyProperty ORIGINAL_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORIGINAL_USER_ID", typeof(Int64), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int64 ORIGINAL_USER_ID
		{
			get { return ((Int64)(base.GetValue(TwitterMessageActivity.ORIGINAL_USER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.ORIGINAL_USER_IDProperty, value); }
		}

		public static DependencyProperty ORIGINAL_FULL_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORIGINAL_FULL_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ORIGINAL_FULL_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ORIGINAL_FULL_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.ORIGINAL_FULL_NAMEProperty, value); }
		}

		public static DependencyProperty ORIGINAL_SCREEN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORIGINAL_SCREEN_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ORIGINAL_SCREEN_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ORIGINAL_SCREEN_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.ORIGINAL_SCREEN_NAMEProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(TwitterMessageActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(TwitterMessageActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.CREATED_BYProperty))); }
			set { base.SetValue(TwitterMessageActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(TwitterMessageActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(TwitterMessageActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(TwitterMessageActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.DATE_STARTProperty))); }
			set { base.SetValue(TwitterMessageActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(TwitterMessageActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.STATUSProperty))); }
			set { base.SetValue(TwitterMessageActivity.STATUSProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_START", typeof(DateTime), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_START
		{
			get { return ((DateTime)(base.GetValue(TwitterMessageActivity.TIME_STARTProperty))); }
			set { base.SetValue(TwitterMessageActivity.TIME_STARTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty IS_RETWEETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_RETWEET", typeof(Int32), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 IS_RETWEET
		{
			get { return ((Int32)(base.GetValue(TwitterMessageActivity.IS_RETWEETProperty))); }
			set { base.SetValue(TwitterMessageActivity.IS_RETWEETProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TwitterMessageActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(TwitterMessageActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(TwitterMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(TwitterMessageActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(TwitterMessageActivity.PARENT_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("TwitterMessageActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("TwitterMessageActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select TWITTER_MESSAGES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwTWITTER_MESSAGES_AUDIT        TWITTER_MESSAGES          " + ControlChars.CrLf
							                + " inner join vwTWITTER_MESSAGES_AUDIT        TWITTER_MESSAGES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on TWITTER_MESSAGES_AUDIT_OLD.ID = TWITTER_MESSAGES.ID       " + ControlChars.CrLf
							                + "        and TWITTER_MESSAGES_AUDIT_OLD.AUDIT_VERSION = (select max(vwTWITTER_MESSAGES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                          from vwTWITTER_MESSAGES_AUDIT                   " + ControlChars.CrLf
							                + "                                                         where vwTWITTER_MESSAGES_AUDIT.ID            =  TWITTER_MESSAGES.ID           " + ControlChars.CrLf
							                + "                                                           and vwTWITTER_MESSAGES_AUDIT.AUDIT_VERSION <  TWITTER_MESSAGES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                           and vwTWITTER_MESSAGES_AUDIT.AUDIT_TOKEN   <> TWITTER_MESSAGES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                       )" + ControlChars.CrLf
							                + " where TWITTER_MESSAGES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                      " + ControlChars.CrLf
							                + "  from vwTWITTER_MESSAGES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwTWITTER_MESSAGES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                      " + ControlChars.CrLf
							                + "  from vwTWITTER_MESSAGES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								if ( !bPast )
									DATE_TIME                      = Sql.ToDateTime(rdr["DATE_TIME"                     ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								TWITTER_ID                     = Sql.ToLong    (rdr["TWITTER_ID"                    ]);
								TWITTER_USER_ID                = Sql.ToLong    (rdr["TWITTER_USER_ID"               ]);
								TWITTER_FULL_NAME              = Sql.ToString  (rdr["TWITTER_FULL_NAME"             ]);
								TWITTER_SCREEN_NAME            = Sql.ToString  (rdr["TWITTER_SCREEN_NAME"           ]);
								ORIGINAL_ID                    = Sql.ToLong    (rdr["ORIGINAL_ID"                   ]);
								ORIGINAL_USER_ID               = Sql.ToLong    (rdr["ORIGINAL_USER_ID"              ]);
								ORIGINAL_FULL_NAME             = Sql.ToString  (rdr["ORIGINAL_FULL_NAME"            ]);
								ORIGINAL_SCREEN_NAME           = Sql.ToString  (rdr["ORIGINAL_SCREEN_NAME"          ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TIME_START                     = Sql.ToDateTime(rdr["TIME_START"                    ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									IS_RETWEET                     = Sql.ToInteger (rdr["IS_RETWEET"                    ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("TwitterMessageActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("TWITTER_MESSAGES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spTWITTER_MESSAGES_Update
								( ref gID
								, ASSIGNED_USER_ID
								, TEAM_ID
								, TEAM_SET_LIST
								, NAME
								, DESCRIPTION
								, DATE_TIME
								, PARENT_TYPE
								, PARENT_ID
								, TYPE
								, TWITTER_ID
								, TWITTER_USER_ID
								, TWITTER_FULL_NAME
								, TWITTER_SCREEN_NAME
								, ORIGINAL_ID
								, ORIGINAL_USER_ID
								, ORIGINAL_FULL_NAME
								, ORIGINAL_SCREEN_NAME
								, TAG_SET_NAME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("TwitterMessageActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

