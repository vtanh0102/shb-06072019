/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// OpportunityActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:29 PM
	/// </summary>
	public class OpportunityActivity: SplendidActivity
	{
		public OpportunityActivity()
		{
			this.Name = "OpportunityActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.IDProperty))); }
			set { base.SetValue(OpportunityActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(OpportunityActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.NAMEProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_TYPE", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OPPORTUNITY_TYPE
		{
			get { return ((string)(base.GetValue(OpportunityActivity.OPPORTUNITY_TYPEProperty))); }
			set { base.SetValue(OpportunityActivity.OPPORTUNITY_TYPEProperty, value); }
		}

		public static DependencyProperty LEAD_SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_SOURCE", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_SOURCE
		{
			get { return ((string)(base.GetValue(OpportunityActivity.LEAD_SOURCEProperty))); }
			set { base.SetValue(OpportunityActivity.LEAD_SOURCEProperty, value); }
		}

		public static DependencyProperty AMOUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AMOUNT", typeof(decimal), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal AMOUNT
		{
			get { return ((decimal)(base.GetValue(OpportunityActivity.AMOUNTProperty))); }
			set { base.SetValue(OpportunityActivity.AMOUNTProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(OpportunityActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty DATE_CLOSEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_CLOSED", typeof(DateTime), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_CLOSED
		{
			get { return ((DateTime)(base.GetValue(OpportunityActivity.DATE_CLOSEDProperty))); }
			set { base.SetValue(OpportunityActivity.DATE_CLOSEDProperty, value); }
		}

		public static DependencyProperty NEXT_STEPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NEXT_STEP", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NEXT_STEP
		{
			get { return ((string)(base.GetValue(OpportunityActivity.NEXT_STEPProperty))); }
			set { base.SetValue(OpportunityActivity.NEXT_STEPProperty, value); }
		}

		public static DependencyProperty SALES_STAGEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SALES_STAGE", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SALES_STAGE
		{
			get { return ((string)(base.GetValue(OpportunityActivity.SALES_STAGEProperty))); }
			set { base.SetValue(OpportunityActivity.SALES_STAGEProperty, value); }
		}

		public static DependencyProperty PROBABILITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROBABILITY", typeof(float), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float PROBABILITY
		{
			get { return ((float)(base.GetValue(OpportunityActivity.PROBABILITYProperty))); }
			set { base.SetValue(OpportunityActivity.PROBABILITYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(OpportunityActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(OpportunityActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(OpportunityActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(OpportunityActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.PARENT_IDProperty))); }
			set { base.SetValue(OpportunityActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.TEAM_IDProperty))); }
			set { base.SetValue(OpportunityActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(OpportunityActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(OpportunityActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.CAMPAIGN_IDProperty))); }
			set { base.SetValue(OpportunityActivity.CAMPAIGN_IDProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(OpportunityActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(OpportunityActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.B2C_CONTACT_IDProperty))); }
			set { base.SetValue(OpportunityActivity.B2C_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty LEAD_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.LEAD_IDProperty))); }
			set { base.SetValue(OpportunityActivity.LEAD_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_NUMBER", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OPPORTUNITY_NUMBER
		{
			get { return ((string)(base.GetValue(OpportunityActivity.OPPORTUNITY_NUMBERProperty))); }
			set { base.SetValue(OpportunityActivity.OPPORTUNITY_NUMBERProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty AMOUNT_BACKUPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AMOUNT_BACKUP", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string AMOUNT_BACKUP
		{
			get { return ((string)(base.GetValue(OpportunityActivity.AMOUNT_BACKUPProperty))); }
			set { base.SetValue(OpportunityActivity.AMOUNT_BACKUPProperty, value); }
		}

		public static DependencyProperty AMOUNT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AMOUNT_USDOLLAR", typeof(decimal), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal AMOUNT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OpportunityActivity.AMOUNT_USDOLLARProperty))); }
			set { base.SetValue(OpportunityActivity.AMOUNT_USDOLLARProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(OpportunityActivity.CREATED_BYProperty))); }
			set { base.SetValue(OpportunityActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(OpportunityActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(OpportunityActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(OpportunityActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(OpportunityActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(OpportunityActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(OpportunityActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(OpportunityActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(OpportunityActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(OpportunityActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_EMAIL1", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_EMAIL1
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ACCOUNT_EMAIL1Property))); }
			set { base.SetValue(OpportunityActivity.ACCOUNT_EMAIL1Property, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_EMAIL1", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_EMAIL1
		{
			get { return ((string)(base.GetValue(OpportunityActivity.B2C_CONTACT_EMAIL1Property))); }
			set { base.SetValue(OpportunityActivity.B2C_CONTACT_EMAIL1Property, value); }
		}

		public static DependencyProperty B2C_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.B2C_CONTACT_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.B2C_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ASSIGNED_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.CAMPAIGN_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.CAMPAIGN_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ASSIGNED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.CAMPAIGN_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.CAMPAIGN_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CAMPAIGN_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.CAMPAIGN_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.CAMPAIGN_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(OpportunityActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(OpportunityActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty LEAD_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ASSIGNED_SET_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.LEAD_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(OpportunityActivity.LEAD_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty LEAD_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ASSIGNED_USER_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.LEAD_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(OpportunityActivity.LEAD_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty LEAD_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_EMAIL1", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_EMAIL1
		{
			get { return ((string)(base.GetValue(OpportunityActivity.LEAD_EMAIL1Property))); }
			set { base.SetValue(OpportunityActivity.LEAD_EMAIL1Property, value); }
		}

		public static DependencyProperty LEAD_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.LEAD_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.LEAD_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(OpportunityActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(OpportunityActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(OpportunityActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(OpportunityActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(OpportunityActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("OpportunityActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("OpportunityActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select OPPORTUNITIES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwOPPORTUNITIES_AUDIT        OPPORTUNITIES          " + ControlChars.CrLf
							                + " inner join vwOPPORTUNITIES_AUDIT        OPPORTUNITIES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on OPPORTUNITIES_AUDIT_OLD.ID = OPPORTUNITIES.ID       " + ControlChars.CrLf
							                + "        and OPPORTUNITIES_AUDIT_OLD.AUDIT_VERSION = (select max(vwOPPORTUNITIES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                       from vwOPPORTUNITIES_AUDIT                   " + ControlChars.CrLf
							                + "                                                      where vwOPPORTUNITIES_AUDIT.ID            =  OPPORTUNITIES.ID           " + ControlChars.CrLf
							                + "                                                        and vwOPPORTUNITIES_AUDIT.AUDIT_VERSION <  OPPORTUNITIES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                        and vwOPPORTUNITIES_AUDIT.AUDIT_TOKEN   <> OPPORTUNITIES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                    )" + ControlChars.CrLf
							                + " where OPPORTUNITIES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                   " + ControlChars.CrLf
							                + "  from vwOPPORTUNITIES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwOPPORTUNITIES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                   " + ControlChars.CrLf
							                + "  from vwOPPORTUNITIES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								if ( !bPast )
									ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								OPPORTUNITY_TYPE               = Sql.ToString  (rdr["OPPORTUNITY_TYPE"              ]);
								LEAD_SOURCE                    = Sql.ToString  (rdr["LEAD_SOURCE"                   ]);
								AMOUNT                         = Sql.ToDecimal (rdr["AMOUNT"                        ]);
								CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								DATE_CLOSED                    = Sql.ToDateTime(rdr["DATE_CLOSED"                   ]);
								NEXT_STEP                      = Sql.ToString  (rdr["NEXT_STEP"                     ]);
								SALES_STAGE                    = Sql.ToString  (rdr["SALES_STAGE"                   ]);
								PROBABILITY                    = Sql.ToFloat   (rdr["PROBABILITY"                   ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								if ( !bPast )
									ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								CAMPAIGN_ID                    = Sql.ToGuid    (rdr["CAMPAIGN_ID"                   ]);
								B2C_CONTACT_ID                 = Sql.ToGuid    (rdr["B2C_CONTACT_ID"                ]);
								if ( !bPast )
									LEAD_ID                        = Sql.ToGuid    (rdr["LEAD_ID"                       ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								OPPORTUNITY_NUMBER             = Sql.ToString  (rdr["OPPORTUNITY_NUMBER"            ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								AMOUNT_BACKUP                  = Sql.ToString  (rdr["AMOUNT_BACKUP"                 ]);
								AMOUNT_USDOLLAR                = Sql.ToDecimal (rdr["AMOUNT_USDOLLAR"               ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"       ]);
									ACCOUNT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"      ]);
									ACCOUNT_EMAIL1                 = Sql.ToString  (rdr["ACCOUNT_EMAIL1"                ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									B2C_CONTACT_ASSIGNED_SET_ID    = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_SET_ID"   ]);
									B2C_CONTACT_ASSIGNED_USER_ID   = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_USER_ID"  ]);
									B2C_CONTACT_EMAIL1             = Sql.ToString  (rdr["B2C_CONTACT_EMAIL1"            ]);
									B2C_CONTACT_NAME               = Sql.ToString  (rdr["B2C_CONTACT_NAME"              ]);
									CAMPAIGN_ASSIGNED_SET_ID       = Sql.ToGuid    (rdr["CAMPAIGN_ASSIGNED_SET_ID"      ]);
									CAMPAIGN_ASSIGNED_USER_ID      = Sql.ToGuid    (rdr["CAMPAIGN_ASSIGNED_USER_ID"     ]);
									CAMPAIGN_NAME                  = Sql.ToString  (rdr["CAMPAIGN_NAME"                 ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									LEAD_ASSIGNED_SET_ID           = Sql.ToGuid    (rdr["LEAD_ASSIGNED_SET_ID"          ]);
									LEAD_ASSIGNED_USER_ID          = Sql.ToGuid    (rdr["LEAD_ASSIGNED_USER_ID"         ]);
									LEAD_EMAIL1                    = Sql.ToString  (rdr["LEAD_EMAIL1"                   ]);
									LEAD_NAME                      = Sql.ToString  (rdr["LEAD_NAME"                     ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("OpportunityActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("OPPORTUNITIES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spOPPORTUNITIES_Update
								( ref gID
								, ASSIGNED_USER_ID
								, ACCOUNT_ID
								, NAME
								, OPPORTUNITY_TYPE
								, LEAD_SOURCE
								, AMOUNT
								, CURRENCY_ID
								, DATE_CLOSED
								, NEXT_STEP
								, SALES_STAGE
								, PROBABILITY
								, DESCRIPTION
								, PARENT_TYPE
								, PARENT_ID
								, ACCOUNT_NAME
								, TEAM_ID
								, TEAM_SET_LIST
								, CAMPAIGN_ID
								, EXCHANGE_FOLDER
								, B2C_CONTACT_ID
								, LEAD_ID
								, TAG_SET_NAME
								, OPPORTUNITY_NUMBER
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("OpportunityActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

