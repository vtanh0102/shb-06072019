/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Opportunities.Pardot
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.ModuleHeader   ctlModuleHeader  ;
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sID          ;
		protected HtmlTable       tblMain      ;
		protected PlaceHolder     plcSubPanel  ;
		protected Label           lblRawContent;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				Spring.Social.Pardot.Api.Opportunity opportunity = new Spring.Social.Pardot.Api.Opportunity();
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

					if ( Page.IsValid )
					{
						if ( Spring.Social.Pardot.PardotSync.PardotEnabled(Application) )
						{
							Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
							string sEmail = new DynamicControl(this, "email").Text;
							// 07/22/2017 Paul.  An exception will be thrown if the email does not exist. 
							try
							{
								Spring.Social.Pardot.Api.Prospect prospect = pardot.ProspectOperations.GetByEmail(sEmail);
							}
							catch
							{
								throw(new Exception(sEmail + " does not exist as a prospect in the system."));
							}
							if ( !Sql.IsEmptyString(sID) )
							{
								opportunity = pardot.OpportunityOperations.GetById(Sql.ToInteger(sID));
							}
							if ( this.FindControl("name"                ) != null ) opportunity.name                = new DynamicControl(this, "name"               ).Text;
							if ( this.FindControl("value"               ) != null ) opportunity.value               = new DynamicControl(this, "value"              ).IntegerValue;
							if ( this.FindControl("probability"         ) != null ) opportunity.probability         = new DynamicControl(this, "probability"        ).IntegerValue;
							if ( this.FindControl("type"                ) != null ) opportunity.type                = new DynamicControl(this, "type"               ).Text;
							if ( this.FindControl("stage"               ) != null ) opportunity.stage               = new DynamicControl(this, "stage"              ).Text;
							if ( this.FindControl("status"              ) != null ) opportunity.status              = new DynamicControl(this, "status"             ).Text;
							if ( this.FindControl("closed_at"           ) != null ) opportunity.closed_at           = new DynamicControl(this, "closed_at"          ).DateValue;
							if ( this.FindControl("campaign_id"         ) != null ) opportunity.campaign_id         = Sql.ToInteger(new DynamicControl(this, "campaign_id").Text);
							if ( this.FindControl("email"               ) != null ) opportunity.email               = new DynamicControl(this, "email"              ).Text;
							if ( this.FindControl("crm_opportunity_fid" ) != null ) opportunity.crm_opportunity_fid = new DynamicControl(this, "crm_opportunity_fid").Text;
							if ( this.FindControl("is_closed"           ) != null ) opportunity.is_closed           = new DynamicControl(this, "is_closed"          ).Checked;
							if ( this.FindControl("is_won"              ) != null ) opportunity.is_won              = new DynamicControl(this, "is_won"             ).Checked;
							if ( !Sql.IsEmptyString(sID) )
							{
								pardot.OpportunityOperations.Update(opportunity);
							}
							else
							{
								opportunity = pardot.OpportunityOperations.Insert(opportunity, sEmail);
								sID = opportunity.id.ToString();
							}
							Response.Redirect("view.aspx?opportunity_id=" + sID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					if ( opportunity != null )
					{
#if DEBUG
						lblRawContent.Text = opportunity.RawContent;
#endif
					}
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?opportunity_id=" + sID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = Spring.Social.Pardot.PardotSync.PardotEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("Pardot", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sID = Sql.ToString(Request["opportunity_id"]);
				if ( !IsPostBack )
				{
					Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
					System.Collections.Generic.IList<Spring.Social.Pardot.Api.Campaign> campaigns = pardot.CampaignOperations.GetAll("name", "asc");
					DataTable dtCampaigns = Spring.Social.Pardot.Api.Campaign.ConvertToCachedTable(campaigns);
					Cache.Insert(L10n.NAME + ".pardot_campaign_dom", dtCampaigns, null, DateTime.Now.AddDays(1), System.Web.Caching.Cache.NoSlidingExpiration);
					
					string sDuplicateID = Sql.ToString(Request["duplicate_id"]);
					if ( !Sql.IsEmptyString(sID) || !Sql.IsEmptyString(sDuplicateID) )
					{
						Spring.Social.Pardot.Api.Opportunity opportunity = null;
						if ( !Sql.IsEmptyString(sDuplicateID) )
							opportunity = pardot.OpportunityOperations.GetById(Sql.ToInteger(sDuplicateID));
						else
							opportunity = pardot.OpportunityOperations.GetById(Sql.ToInteger(sID));
						if ( opportunity != null )
						{
							DataRow rdr = Spring.Social.Pardot.Api.Opportunity.ConvertToRow(opportunity);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
							ctlModuleHeader.Title = Sql.ToString(rdr["name"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
							ViewState["ctlModuleHeader.Title"] = ctlModuleHeader.Title;
								
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							TextBox txtNAME = this.FindControl("name") as TextBox;
							if ( txtNAME != null )
								txtNAME.Focus();
								
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("name") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlModuleHeader.Title = Sql.ToString(ViewState["ctlModuleHeader.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Opportunities";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

