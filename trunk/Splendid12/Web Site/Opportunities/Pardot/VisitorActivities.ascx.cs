/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Opportunities.Pardot
{
	/// <summary>
	///		Summary description for VisitorActivities.
	/// </summary>
	public class VisitorActivities : SubPanelControl
	{
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.SearchView     ctlSearchView    ;
		protected string        sID            ;
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch ( e.CommandName )
				{
					case "Opportunities.Search":
						ctlSearchView.Visible = !ctlSearchView.Visible;
						break;
					case "Search":
						break;
					case "Clear":
						BindGrid();
						break;
					case "SortGrid":
						break;
					default:
						throw(new Exception("Unknown command: " + e.CommandName));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void BindGrid()
		{
			if ( !IsPostBack )
			{
				grdMain.OrderByClause("id", "asc");
				
				if ( Spring.Social.Pardot.PardotSync.PardotEnabled(Application) && !Sql.IsEmptyString(sID) )
				{
					Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
					Spring.Social.Pardot.Api.Opportunity opportunity = pardot.OpportunityOperations.GetById(Sql.ToInteger(sID));
					if ( opportunity != null )
					{
						using ( DataTable data = opportunity.GetVisitorActivities() )
						{
							vwMain = data.DefaultView;
							grdMain.DataSource = vwMain ;
							ViewState["VisitorActivities"] = data;
						}
					}
				}
			}
			else
			{
				DataTable dt = ViewState["VisitorActivities"] as DataTable;
				if ( dt != null )
				{
					vwMain = dt.DefaultView;
					grdMain.DataSource = vwMain ;
				}
			}
			if ( !IsPostBack )
			{
				grdMain.DataBind();
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0) && (SplendidCRM.Security.AdminUserAccess("Pardot", "view") >= 0);
			if ( !this.Visible )
				return;
			try
			{
				sID = Sql.ToString(Request["opportunity_id"]);
				if ( Sql.IsEmptyString(sID) )
					sID = Sql.ToString(ViewState["opportunity_id"]);
				if ( !IsPostBack )
				{
					if ( Sql.IsEmptyString(sID) )
					{
						Guid gID = Sql.ToGuid(Request["ID"]);
						if ( !Sql.IsEmptyGuid(gID) )
						{
							ctlDynamicButtons.Visible = false;
							
							DbProviderFactory dbf = DbProviderFactories.GetFactory();
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								string sSQL ;
								sSQL = "select SYNC_REMOTE_KEY                       " + ControlChars.CrLf
								     + "  from vwOPPORTUNITIES_SYNC                  " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME = @SYNC_SERVICE_NAME" + ControlChars.CrLf
								     + "   and SYNC_LOCAL_ID     = @SYNC_LOCAL_ID    " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_SERVICE_NAME", "Pardot");
									Sql.AddParameter(cmd, "@SYNC_LOCAL_ID"    , gID     );
									sID = Sql.ToString(cmd.ExecuteScalar());
								}
							}
						}
					}
					if ( !Sql.IsEmptyString(sID) )
					{
						ViewState["opportunity_id"] = sID;
						BindGrid();
						ctlDynamicButtons.AppendButtons(m_sMODULE + ".Pardot.VisitorActivities", Guid.Empty, null);
					}
					else
					{
						ctlDynamicButtons.AppendButtons(m_sMODULE + ".Pardot.VisitorActivities", Guid.Empty, null);
						ctlDynamicButtons.DisableAll();
					}
				}
				else
				{
					BindGrid();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlSearchView.Command     += new CommandEventHandler(Page_Command);
			m_sMODULE = "Opportunities";
			this.AppendGridColumns(grdMain, m_sMODULE + ".Pardot.VisitorActivities");
			if ( IsPostBack )
				ctlDynamicButtons.AppendButtons(m_sMODULE + ".Pardot.VisitorActivities", Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}

