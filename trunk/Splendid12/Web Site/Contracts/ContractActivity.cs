/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ContractActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class ContractActivity: SplendidActivity
	{
		public ContractActivity()
		{
			this.Name = "ContractActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.IDProperty))); }
			set { base.SetValue(ContractActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ContractActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.NAMEProperty))); }
			set { base.SetValue(ContractActivity.NAMEProperty, value); }
		}

		public static DependencyProperty REFERENCE_CODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REFERENCE_CODE", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REFERENCE_CODE
		{
			get { return ((string)(base.GetValue(ContractActivity.REFERENCE_CODEProperty))); }
			set { base.SetValue(ContractActivity.REFERENCE_CODEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(ContractActivity.STATUSProperty))); }
			set { base.SetValue(ContractActivity.STATUSProperty, value); }
		}

		public static DependencyProperty TYPE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TYPE_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.TYPE_IDProperty))); }
			set { base.SetValue(ContractActivity.TYPE_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(ContractActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.OPPORTUNITY_IDProperty))); }
			set { base.SetValue(ContractActivity.OPPORTUNITY_IDProperty, value); }
		}

		public static DependencyProperty START_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("START_DATE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime START_DATE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.START_DATEProperty))); }
			set { base.SetValue(ContractActivity.START_DATEProperty, value); }
		}

		public static DependencyProperty END_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("END_DATE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime END_DATE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.END_DATEProperty))); }
			set { base.SetValue(ContractActivity.END_DATEProperty, value); }
		}

		public static DependencyProperty COMPANY_SIGNED_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COMPANY_SIGNED_DATE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime COMPANY_SIGNED_DATE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.COMPANY_SIGNED_DATEProperty))); }
			set { base.SetValue(ContractActivity.COMPANY_SIGNED_DATEProperty, value); }
		}

		public static DependencyProperty CUSTOMER_SIGNED_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CUSTOMER_SIGNED_DATE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime CUSTOMER_SIGNED_DATE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.CUSTOMER_SIGNED_DATEProperty))); }
			set { base.SetValue(ContractActivity.CUSTOMER_SIGNED_DATEProperty, value); }
		}

		public static DependencyProperty EXPIRATION_NOTICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPIRATION_NOTICE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime EXPIRATION_NOTICE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.EXPIRATION_NOTICEProperty))); }
			set { base.SetValue(ContractActivity.EXPIRATION_NOTICEProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(ContractActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty TOTAL_CONTRACT_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_CONTRACT_VALUE", typeof(decimal), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TOTAL_CONTRACT_VALUE
		{
			get { return ((decimal)(base.GetValue(ContractActivity.TOTAL_CONTRACT_VALUEProperty))); }
			set { base.SetValue(ContractActivity.TOTAL_CONTRACT_VALUEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(ContractActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(ContractActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.TEAM_IDProperty))); }
			set { base.SetValue(ContractActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ContractActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ContractActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.B2C_CONTACT_IDProperty))); }
			set { base.SetValue(ContractActivity.B2C_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(ContractActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(ContractActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(ContractActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ContractActivity.CREATED_BYProperty))); }
			set { base.SetValue(ContractActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(ContractActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ContractActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ContractActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(ContractActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ContractActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ContractActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ContractActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ContractActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ContractActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TOTAL_CONTRACT_VALUE_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_CONTRACT_VALUE_USDOLLAR", typeof(decimal), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TOTAL_CONTRACT_VALUE_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(ContractActivity.TOTAL_CONTRACT_VALUE_USDOLLARProperty))); }
			set { base.SetValue(ContractActivity.TOTAL_CONTRACT_VALUE_USDOLLARProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ContractActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ContractActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(ContractActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(ContractActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ContractActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ContractActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.B2C_CONTACT_NAMEProperty))); }
			set { base.SetValue(ContractActivity.B2C_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ContractActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_CONVERSION_RATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_CONVERSION_RATE", typeof(float), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float CURRENCY_CONVERSION_RATE
		{
			get { return ((float)(base.GetValue(ContractActivity.CURRENCY_CONVERSION_RATEProperty))); }
			set { base.SetValue(ContractActivity.CURRENCY_CONVERSION_RATEProperty, value); }
		}

		public static DependencyProperty CURRENCY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.CURRENCY_NAMEProperty))); }
			set { base.SetValue(ContractActivity.CURRENCY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_SYMBOL", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_SYMBOL
		{
			get { return ((string)(base.GetValue(ContractActivity.CURRENCY_SYMBOLProperty))); }
			set { base.SetValue(ContractActivity.CURRENCY_SYMBOLProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(ContractActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(ContractActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ContractActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ASSIGNED_SET_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.OPPORTUNITY_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ContractActivity.OPPORTUNITY_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ASSIGNED_USER_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.OPPORTUNITY_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ContractActivity.OPPORTUNITY_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_NAME", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OPPORTUNITY_NAME
		{
			get { return ((string)(base.GetValue(ContractActivity.OPPORTUNITY_NAMEProperty))); }
			set { base.SetValue(ContractActivity.OPPORTUNITY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(ContractActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(ContractActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(ContractActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(ContractActivity.TYPEProperty))); }
			set { base.SetValue(ContractActivity.TYPEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ContractActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ContractActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CONTRACTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCONTRACTS_AUDIT        CONTRACTS          " + ControlChars.CrLf
							                + " inner join vwCONTRACTS_AUDIT        CONTRACTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CONTRACTS_AUDIT_OLD.ID = CONTRACTS.ID       " + ControlChars.CrLf
							                + "        and CONTRACTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwCONTRACTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                   from vwCONTRACTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                  where vwCONTRACTS_AUDIT.ID            =  CONTRACTS.ID           " + ControlChars.CrLf
							                + "                                                    and vwCONTRACTS_AUDIT.AUDIT_VERSION <  CONTRACTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                    and vwCONTRACTS_AUDIT.AUDIT_TOKEN   <> CONTRACTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                )" + ControlChars.CrLf
							                + " where CONTRACTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwCONTRACTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCONTRACTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwCONTRACTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								REFERENCE_CODE                 = Sql.ToString  (rdr["REFERENCE_CODE"                ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								TYPE_ID                        = Sql.ToGuid    (rdr["TYPE_ID"                       ]);
								ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								if ( !bPast )
									OPPORTUNITY_ID                 = Sql.ToGuid    (rdr["OPPORTUNITY_ID"                ]);
								START_DATE                     = Sql.ToDateTime(rdr["START_DATE"                    ]);
								END_DATE                       = Sql.ToDateTime(rdr["END_DATE"                      ]);
								COMPANY_SIGNED_DATE            = Sql.ToDateTime(rdr["COMPANY_SIGNED_DATE"           ]);
								CUSTOMER_SIGNED_DATE           = Sql.ToDateTime(rdr["CUSTOMER_SIGNED_DATE"          ]);
								EXPIRATION_NOTICE              = Sql.ToDateTime(rdr["EXPIRATION_NOTICE"             ]);
								CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								TOTAL_CONTRACT_VALUE           = Sql.ToDecimal (rdr["TOTAL_CONTRACT_VALUE"          ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								B2C_CONTACT_ID                 = Sql.ToGuid    (rdr["B2C_CONTACT_ID"                ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TOTAL_CONTRACT_VALUE_USDOLLAR  = Sql.ToDecimal (rdr["TOTAL_CONTRACT_VALUE_USDOLLAR" ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"       ]);
									ACCOUNT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"      ]);
									ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									B2C_CONTACT_ASSIGNED_SET_ID    = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_SET_ID"   ]);
									B2C_CONTACT_ASSIGNED_USER_ID   = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_USER_ID"  ]);
									B2C_CONTACT_NAME               = Sql.ToString  (rdr["B2C_CONTACT_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									CURRENCY_CONVERSION_RATE       = Sql.ToFloat   (rdr["CURRENCY_CONVERSION_RATE"      ]);
									CURRENCY_NAME                  = Sql.ToString  (rdr["CURRENCY_NAME"                 ]);
									CURRENCY_SYMBOL                = Sql.ToString  (rdr["CURRENCY_SYMBOL"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									OPPORTUNITY_ASSIGNED_SET_ID    = Sql.ToGuid    (rdr["OPPORTUNITY_ASSIGNED_SET_ID"   ]);
									OPPORTUNITY_ASSIGNED_USER_ID   = Sql.ToGuid    (rdr["OPPORTUNITY_ASSIGNED_USER_ID"  ]);
									OPPORTUNITY_NAME               = Sql.ToString  (rdr["OPPORTUNITY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ContractActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CONTRACTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCONTRACTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, REFERENCE_CODE
								, STATUS
								, TYPE_ID
								, ACCOUNT_ID
								, OPPORTUNITY_ID
								, START_DATE
								, END_DATE
								, COMPANY_SIGNED_DATE
								, CUSTOMER_SIGNED_DATE
								, EXPIRATION_NOTICE
								, CURRENCY_ID
								, TOTAL_CONTRACT_VALUE
								, DESCRIPTION
								, TEAM_ID
								, TEAM_SET_LIST
								, B2C_CONTACT_ID
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ContractActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

