<%@ Control CodeBehind="ListView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.KPIB0302.ListView" %>
<div id="divListView">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlModuleHeader" Module="KPIB0302" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="SearchView" Src="~/_controls/SearchView.ascx" %>
    <SplendidCRM:SearchView ID="ctlSearchView" Module="KPIB0302" ShowDuplicateSearch="false" Visible="<%# !PrintView %>" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="ExportHeader" Src="~/_controls/ExportHeader.ascx" %>
    <SplendidCRM:ExportHeader ID="ctlExportHeader" Module="KPIB0302" Title="KPIB0302.LBL_LIST_FORM_TITLE" runat="Server" />

    <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
        <asp:Label ID="lblError" CssClass="error" EnableViewState="false" runat="server" />
    </asp:Panel>

    <asp:HiddenField ID="LAYOUT_LIST_VIEW" runat="server" />
    <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText="" ItemStyle-Width="1%">
                <ItemTemplate><%# grdMain.InputCheckbox(!PrintView && !IsMobile && Security.CheckApprovalAccess(Sql.ToGuid(Eval("CREATED_BY_ID")), Sql.ToGuid(Eval("APPROVED_BY")), Sql.ToString(Eval("APPROVE_STATUS"))) > 0 && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE), ctlCheckAll.FieldName, Sql.ToGuid(Eval("ID")), ctlCheckAll.SelectedItems) %></ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                <ItemTemplate>
                <%--    Tungnx:Hide
                    <asp:HyperLink onclick=<%# "return SplendidCRM_ChangeFavorites(this, \'" + m_sMODULE + "\', \'" + Sql.ToString(Eval("ID")) + "\')" %> Visible="<%# !this.IsMobile %>" runat="server">
						<asp:Image name='<%# "favAdd_" + Sql.ToString(Eval("ID")) %>' SkinID="favorites_add"    style='<%# "display:" + ( Sql.IsEmptyGuid(Eval("FAVORITE_RECORD_ID")) ? "inline" : "none") %>' ToolTip='<%# L10n.Term(".LBL_ADD_TO_FAVORITES"     ) %>' Runat="server" />
						<asp:Image name='<%# "favRem_" + Sql.ToString(Eval("ID")) %>' SkinID="favorites_remove" style='<%# "display:" + (!Sql.IsEmptyGuid(Eval("FAVORITE_RECORD_ID")) ? "inline" : "none") %>' ToolTip='<%# L10n.Term(".LBL_REMOVE_FROM_FAVORITES") %>' Runat="server" />
                    </asp:HyperLink>
                    <asp:HyperLink onclick=<%# "return SplendidCRM_ChangeFollowing(this, \'" + m_sMODULE + "\', \'" + Sql.ToString(Eval("ID")) + "\')" %> Visible="<%# !this.IsMobile && this.StreamEnabled() %>" runat="server">
						<asp:Image name='<%# "follow_"    + Sql.ToString(Eval("ID")) %>' SkinID="follow"    style='<%# "display:" + ( Sql.IsEmptyGuid(Eval("SUBSCRIPTION_PARENT_ID")) ? "inline" : "none") %>' ToolTip='<%# L10n.Term(".LBL_FOLLOW"   ) %>' Runat="server" />
						<asp:Image name='<%# "following_" + Sql.ToString(Eval("ID")) %>' SkinID="following" style='<%# "display:" + (!Sql.IsEmptyGuid(Eval("SUBSCRIPTION_PARENT_ID")) ? "inline" : "none") %>' ToolTip='<%# L10n.Term(".LBL_FOLLOWING") %>' Runat="server" />
                    </asp:HyperLink>
                    --%>
                    <asp:HyperLink Visible='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit", Sql.ToGuid(Eval("ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) && !Eval("APPROVE_STATUS").Equals(SplendidCRM._modules.KPIs_Constant.KPI_APPROVE_STATUS_APPROVE) && !Eval("APPROVE_STATUS").Equals(SplendidCRM._modules.KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT) %>' NavigateUrl='<%# "~/" + m_sMODULE + "/edit.aspx?id=" + Eval("ID") %>' ToolTip='<%# L10n.Term(".LNK_EDIT") %>' runat="server">
						<asp:Image SkinID="edit_inline" Runat="server" />
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:HyperLinkColumn HeaderText="KPIB0203.LBL_EMPLOYEE_NAME" DataTextField="FULL_NAME" DataNavigateUrlFormatString="~/KPIB0302/view.aspx?id={0}" DataNavigateUrlField="ID" DataTextFormatString="{0:c}"></asp:HyperLinkColumn>
            <asp:BoundColumn HeaderText="KPIB0203.BRANCH" DataField="BRANCH"></asp:BoundColumn>
            <asp:BoundColumn HeaderText=".DEPARTMENT" DataField="DEPARTMENT"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="KPIB0203.LBL_POSITION" DataField="TITLE"></asp:BoundColumn>
            <%--<asp:BoundColumn HeaderText="KPIB0203.LBL_ALLOCATE_CODE" DataField="ALLOCATE_CODE"></asp:BoundColumn>--%>
            <asp:HyperLinkColumn HeaderText="KPIB0203.LBL_ALLOCATE_CODE" DataTextField="ALLOCATE_CODE" DataNavigateUrlFormatString="~/KPIB0302/view.aspx?id={0}" DataNavigateUrlField="ID" DataTextFormatString="{0:c}"></asp:HyperLinkColumn>
            <asp:TemplateColumn HeaderText="KPIB0203.LBL_PERIOD">
                <ItemTemplate>
                    <asp:Label ID="lblPERIOD" Text='<%# Bind("MONTH_PERIOD") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <%-- <asp:TemplateColumn HeaderText="KPIB0302.LBL_LIST_PERCENT_SYNC_TOTAL">
                <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                <ItemStyle CssClass="dataField  right format-qty" />
                <ItemTemplate>
                    <asp:Label ID="lblPERCENT_SYNC_TOTAL" Text='<%# Bind("PERCENT_SYNC_TOTAL" , "{0:N2}") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText="KPIB0302.LBL_LIST_PERCENT_FINAL_TOTAL">
                <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                <ItemStyle CssClass="dataField  right format-qty" />
                <ItemTemplate>
                    <asp:Label ID="lblPERCENT_FINAL_TOTAL" Text='<%#  Bind("PERCENT_FINAL_TOTAL" , "{0:N2}")  %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--<asp:TemplateColumn HeaderText="KPIB0302.LBL_LIST_PERCENT_MANUAL_TOTAL">
                <ItemTemplate>
                    <asp:Label ID="lblPERCENT_MANUAL_TOTAL" Text='<%# Bind("PERCENT_MANUAL_TOTAL") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText="KPIB0203.LBL_APPROVE_STATUS">
                <HeaderStyle CssClass="gridHeaderLabel-Gray right" />
                <ItemStyle CssClass="dataField  right" />
                <ItemTemplate>
                    <asp:Label ID="lblAPPROVE_STATUS" Text='<%# Bind("APPROVE_STATUS") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

    <SplendidCRM:InlineScript runat="server">
        <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
        <script src="../Include/javascript/excelexportjs.js"></script>
        <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('[id$=ctl00_cntBody_ctlListView_ctlSearchView_lnkAdvancedSearch]').closest("li").hide();
                $("[id$=ctl00_cntBody_ctlListView_ctlSearchView_ORGANIZATION_ID_C]").chosen();
                $("[id$=ctl00_cntBody_ctlListView_ctlSearchView_ORGANIZATION_PARENT_ID_C]").chosen();
                //$('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_divExport]').hide();
                $('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_lstEXPORT_RANGE]').hide();
                $('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_lstEXPORT_FORMAT]').hide();

                $("#<%= grdMain.ClientID %>").excelexportjs({                    
                     containerid: "tableData",                      
                    datatype: 'table'                   
            });

            });
        </script>
    </SplendidCRM:InlineScript>

    <%@ Register TagPrefix="SplendidCRM" TagName="CheckAll" Src="~/_controls/CheckAll.ascx" %>
    <SplendidCRM:CheckAll ID="ctlCheckAll" Visible="<%# !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
    <asp:Panel ID="pnlMassUpdateSeven" runat="server">
        <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
        <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
    </asp:Panel>

    <%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
    <SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
</div>
