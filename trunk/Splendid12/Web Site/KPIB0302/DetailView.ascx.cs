﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0302
{

    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;
        protected SplendidGrid grdLeader;
        protected PlaceHolder plLeader;

        protected MassUpdate ctlMassUpdate;
        protected Label lbTitleGroupACTResult;
        protected Label warningKPI;
        protected Label lbTitleMyACTResult;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblPLAN_VALUE = (Label)e.Item.FindControl("lblPLAN_VALUE");
                if (lblPLAN_VALUE != null)
                {
                    lblPLAN_VALUE.Text = KPIs_Utils.FormatFloat(lblPLAN_VALUE.Text, format_number);
                }
                Label lblSYNC_VALUE = (Label)e.Item.FindControl("lblSYNC_VALUE");
                if (lblSYNC_VALUE != null)
                {
                    lblSYNC_VALUE.Text = KPIs_Utils.FormatFloat(lblSYNC_VALUE.Text, format_number);
                }
                Label lblFINAL_VALUE = (Label)e.Item.FindControl("lblFINAL_VALUE");
                if (lblFINAL_VALUE != null)
                {
                    lblFINAL_VALUE.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE.Text, format_number);

                    if (Sql.ToDouble(lblFINAL_VALUE.Text) < 0)
                    {
                        lblFINAL_VALUE.CssClass = "warning-red-bold right";
                    }
                }


                Label lblNEED_VALUE = (Label)e.Item.FindControl("lblNEED_VALUE");
                if (lblNEED_VALUE != null)
                {
                    lblNEED_VALUE.Text = Sql.ToDecimal(lblNEED_VALUE.Text) > 0 ? KPIs_Utils.FormatFloat(lblNEED_VALUE.Text, format_number) : KPIs_Utils.FormatFloat("0", format_number);
                }

                Label lblPERCENT_FINAL_VALUE = (Label)e.Item.FindControl("lblPERCENT_FINAL_VALUE");
                if (lblPERCENT_FINAL_VALUE != null)
                {
                    lblPERCENT_FINAL_VALUE.Text = string.IsNullOrEmpty(lblPERCENT_FINAL_VALUE.Text) ? "0" : KPIs_Utils.FormatFloat(lblPERCENT_FINAL_VALUE.Text, format_number);
                }



            }
        }

        protected void grdLeader_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblPLAN_VALUE = (Label)e.Item.FindControl("lblPLAN_VALUE");
                if (lblPLAN_VALUE != null)
                {
                    lblPLAN_VALUE.Text = KPIs_Utils.FormatFloat(lblPLAN_VALUE.Text, format_number);
                }
                Label lblSYNC_VALUE = (Label)e.Item.FindControl("lblSYNC_VALUE");
                if (lblSYNC_VALUE != null)
                {
                    lblSYNC_VALUE.Text = KPIs_Utils.FormatFloat(lblSYNC_VALUE.Text, format_number);
                }
                Label lblFINAL_VALUE = (Label)e.Item.FindControl("lblFINAL_VALUE");
                if (lblFINAL_VALUE != null)
                {
                    lblFINAL_VALUE.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE.Text, format_number);
                }
                Label lblNEED_VALUE = (Label)e.Item.FindControl("lblNEED_VALUE");
                if (lblNEED_VALUE != null)
                {
                    lblNEED_VALUE.Text = float.Parse(lblNEED_VALUE.Text) > 0 ? KPIs_Utils.FormatFloat(lblNEED_VALUE.Text, format_number) : string.Empty;
                }

                Label lblPERCENT_FINAL_VALUE = (Label)e.Item.FindControl("lblPERCENT_FINAL_VALUE");
                if (lblPERCENT_FINAL_VALUE != null)
                {
                    lblPERCENT_FINAL_VALUE.Text = string.IsNullOrEmpty(lblPERCENT_FINAL_VALUE.Text) ? string.Empty : KPIs_Utils.FormatFloat(lblPERCENT_FINAL_VALUE.Text, format_number);
                }
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("edit.aspx?ID=" + gID.ToString());
                }
                else if (e.CommandName == "Duplicate")
                {
                    Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    KPIs_Utils.B_KPI_ACT_RESULT_DETAIL_Delete(gID);//xoa details
                    SqlProcs.spB_KPI_ACTUAL_RESULT_Delete(gID);
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "Cancel")
                {
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "MassSubmitApproval")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB030202_SQLProc.spB_KPI_ACTUAL_RESULT_MassApprove(sIDs, ctlMassUpdate.APPROVED_BY, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassApprove
                else if (e.CommandName == "MassApprove")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB030202_SQLProc.spB_KPI_ACTUAL_RESULT_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassReject
                else if (e.CommandName == "MassReject")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB030202_SQLProc.spB_KPI_ACTUAL_RESULT_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_REJECT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                else if (e.CommandName == "Export")
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection conReporter = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = "  SELECT  Row_Number() OVER(order by DATE_ENTERED) AS NO, ID, KPI_ID, KPI_NAME,VALUE_STD_PER_MONTH,   " + ControlChars.CrLf
                                             + "  KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, (PLAN_VALUE - FINAL_VALUE) AS NEED_VALUE, " + ControlChars.CrLf
                                             + "  PERCENT_FINAL_VALUE, DESCRIPTION          " + ControlChars.CrLf
                                             + "  FROM vwB_KPI_ACT_RESULT_DETAIL_REPORT     " + ControlChars.CrLf
                                             + " WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_ACTUAL_RESULT_Edit WHERE ID = @ID)  " + ControlChars.CrLf;
                            using (IDbCommand cmdCheck = conReporter.CreateCommand())
                            {
                                cmdCheck.CommandText = sSQL_DETAILS;
                                Sql.AddParameter(cmdCheck, "@ID", gID.ToString());
                                conReporter.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmdCheck));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmdCheck;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            var empID = new DynamicControl(this, "EMPLOYEE_ID").Text;
                                            Security.EMPLOYEE_FIELDS employeesInfo = Security.getUserInfos(Sql.ToString(empID));
                                            var periodMonth = new DynamicControl(this, "MONTH_PERIOD").Text;
                                            string templatePath = Server.MapPath(@"~\Include\Template\R011.xlsx");
                                            KPIs_Export.ExportKPIsResultCaNhan(dtCurrent, employeesInfo, periodMonth, templatePath);
                                        }
                                        else
                                        {
                                            throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw (new Exception(ex.Message, ex.InnerException));
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            lbTitleGroupACTResult.Text = L10n.Term(".TitleGroupACTResult");
            lbTitleMyACTResult.Text = L10n.Term(".TitleMyACTResult");
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;
            warningKPI.Text =  string.Empty;

            float totalKPI = 0;
            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID, "ID", false);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            Page.Items["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            totalKPI = Sql.ToFloat(rdr["PERCENT_FINAL_TOTAL"]);
                                            string approveStatus = rdr["APPROVE_STATUS"].ToString();
                                            if (!KPIs_Constant.KPI_APPROVE_STATUS_APPROVE.Equals(approveStatus) && !KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT.Equals(approveStatus))
                                            {
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            }

                                            ctlDynamicButtons.AppendProcessButtons(rdr);

                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            new DynamicControl(this, "ASSIGN_BY").Text = KPIs_Utils.getFullNameFromUser(new DynamicControl(this, "ASSIGN_BY").Text);

                                            new DynamicControl(this, "APPROVED_BY").Text = KPIs_Utils.getFullNameFromUser(new DynamicControl(this, "APPROVED_BY").Text);

                                            KPIs_Utils.LoadEmployee_Info_Detail(this, dbf, Guid.Parse(rdr["EMPLOYEE_ID"].ToString()), Sql.ToString(rdr["ALLOCATE_CODE"]));
                                        }
                                        else
                                        {
                                            plcSubPanel.Visible = false;
                                            grdMain.Visible = false;

                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.AppendProcessButtons(null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = "  SELECT  Row_Number() OVER(order by DATE_ENTERED) AS NO, ID, KPI_ID, KPI_NAME,                       " + ControlChars.CrLf
                                             + "  KPI_UNIT, RATIO, PLAN_VALUE, SYNC_VALUE, FINAL_VALUE, (PLAN_VALUE - FINAL_VALUE) AS NEED_VALUE, " + ControlChars.CrLf
                                             + "  PERCENT_FINAL_VALUE, DESCRIPTION      " + ControlChars.CrLf
                                             + "  FROM vwB_KPI_ACT_RESULT_DETAIL_Edit   " + ControlChars.CrLf
                                             + " WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_ACTUAL_RESULT_Edit WHERE ID = @ID)  " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AddParameter(cmd, "@ID", gID.ToString());
                                cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            grdMain.DataSource = dtCurrent;
                                            string format_number = Sql.ToString(Application["CONFIG.format_number"]);

                                            grdMain.Columns[7].FooterText = L10n.Term("Employees.LBL_PERCENT_COMPLETE_PERIOD");//"Tổng";
                                            grdMain.Columns[8].FooterText = KPIs_Utils.FormatFloat(Sql.ToString(totalKPI), format_number);//totalKPI.ToString("N1");
                                            grdMain.Columns[8].FooterStyle.HorizontalAlign = HorizontalAlign.Right;

                                            grdMain.DataBind();
                                            ViewState["CurrentTable"] = dtCurrent;
                                        }

                                        for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                        {
                                            if(Sql.ToDouble(dtCurrent.Rows[i]["FINAL_VALUE"]) < 0)
                                            {
                                                warningKPI.Text = string.Format("{0}", L10n.Term("KPIB0302.LBL_WARNING_FINAL_VALUE"));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        using (IDbConnection conReporter = dbf.CreateConnection())
                        {
                            var sSQL_Check = "  SELECT *                     " + ControlChars.CrLf
                                             + "  FROM vwEMPLOYEES_Edit   " + ControlChars.CrLf
                                             + " WHERE REPORTS_TO_ID IN (SELECT EMPLOYEE_ID FROM vwB_KPI_ACTUAL_RESULT WHERE ID= @ID)  " + ControlChars.CrLf;
                            using (IDbCommand cmdCheck = conReporter.CreateCommand())
                            {
                                cmdCheck.CommandText = sSQL_Check;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AddParameter(cmdCheck, "@ID", gID.ToString());
                                conReporter.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmdCheck));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmdCheck;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count == 0)
                                        {
                                            plLeader.Visible = false;
                                            return;
                                        }
                                    }
                                }
                            }
                        }


                        using (IDbConnection conReporterr = dbf.CreateConnection())
                        {
                            var sSQL_DETAILSReporter = "  SELECT  Row_Number() OVER(order by DATE_ENTERED) AS NO, ID, KPI_ID, KPI_NAME,                       " + ControlChars.CrLf
                            + "  KPI_UNIT, RATIO, FLEX2, FLEX3, FLEX4,(case When (Isnumeric(FLEX2)=1 AND Isnumeric(FLEX4)=1) Then Convert(float,FLEX2) - Convert(float,FLEX4) else NULL end) AS NEED_VALUE, PERCENT_MANUAL_VALUE " + ControlChars.CrLf
                            + "  FROM vwB_KPI_ACT_RESULT_DETAIL_Edit   " + ControlChars.CrLf
                            + " WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_ACTUAL_RESULT_Edit WHERE ID = @ID)  " + ControlChars.CrLf;

                            using (IDbCommand cmdReporter = conReporterr.CreateCommand())
                            {
                                cmdReporter.CommandText = sSQL_DETAILSReporter;
                                //Security.Filter(cmd, m_sMODULE, "view");
                                Sql.AddParameter(cmdReporter, "@ID", gID.ToString());
                                cmdReporter.CommandText += "  order by NO   " + ControlChars.CrLf;
                                conReporterr.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmdReporter));

                                using (DbDataAdapter dar = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)dar).SelectCommand = cmdReporter;
                                    using (DataTable dtCurrentr = new DataTable())
                                    {
                                        dar.Fill(dtCurrentr);
                                        if (dtCurrentr.Rows.Count > 0)
                                        {
                                            grdLeader.DataSource = dtCurrentr;
                                            grdLeader.DataBind();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.AppendProcessButtons(null);
                        ctlDynamicButtons.DisableAll();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }
                ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "list") > 0 ? true : false;

                //01/10/2018 Tungnx: check approval button
                ctlMassUpdate.detailGID = gID.ToString();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            grdLeader.ItemDataBound += new DataGridItemEventHandler(grdLeader_ItemDataBound);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);

            m_sMODULE = "KPIB0302";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                ctlDynamicButtons.AppendProcessButtons(null);
            }
        }
        #endregion
    }
}
