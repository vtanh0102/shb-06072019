<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIB0302.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divDetailView" runat="server">
    <style type="text/css">
        .listViewPaginationTdS1 {
            float: left;
        }

        .tabDetailViewDL h4 {
            display: inline-block;
            white-space: nowrap;
            float: left;
        }

        #ctl00_cntBody_ctlDetailView_btnExport {
            float: right;
            margin-top: -33px;
        }

        a.utilsLink {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('[id$=ctl00_cntBody_ctlDetailView_EMPLOYEE_ID]').hide();
        });
    </script>

    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIB0302" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />
    <asp:Button ID="btnExport" CommandName="Export" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LNK_PRINT" ) %>' ToolTip='<%# L10n.Term(".LNK_PRINT" ) %>' runat="server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIB0302" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel" style="display: none">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <h4>
        <asp:Label ID="lbTitleMyACTResult" runat="server" /></h4>
    <div>
        <asp:Label ID="warningKPI" Text="" runat="server" CssClass="required"></asp:Label>
    </div>
    <asp:UpdatePanel runat="server">

        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn HeaderText=".LBL_NO">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                        <ItemStyle CssClass="dataField  format-qty" Width="3%" />
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                            <asp:HiddenField ID="txtKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                        <ItemStyle CssClass="dataField  format-qty" Width="20%" />
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                        <ItemStyle CssClass="dataField  format-qty" Width="10%" />
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                            <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_RATIO">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                        <ItemStyle CssClass="dataField  right format-qty" Width="5%" />
                        <ItemTemplate>
                            <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_PLAN_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                        <ItemStyle CssClass="dataField  right format-qty" Width="10%" />
                        <ItemTemplate>
                            <asp:Label ID="lblPLAN_VALUE" Text='<%# Bind("PLAN_VALUE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_SYNC_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" Width="10%" />
                        <ItemStyle CssClass="dataField  right format-qty" />
                        <ItemTemplate>
                            <asp:Label ID="lblSYNC_VALUE" Text='<%# Bind("SYNC_VALUE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                        <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                        <ItemTemplate>
                            <asp:Label ID="lblFINAL_VALUE" Text='<%# Bind("FINAL_VALUE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_NEED_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                        <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                        <ItemTemplate>
                            <asp:Label ID="lblNEED_VALUE" Text='<%# Bind("NEED_VALUE") %>' runat="server" CssClass="right format-qty" />
                        </ItemTemplate>                        
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_PERCENT_FINAL_VALUE">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                        <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                        <ItemTemplate>
                            <asp:Label ID="lblPERCENT_FINAL_VALUE" Text='<%# Bind("PERCENT_FINAL_VALUE") %>' runat="server" CssClass="right format-qty" />
                        </ItemTemplate>                       
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                        <ItemTemplate>
                            <asp:Label ID="txtDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>



            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:PlaceHolder ID="plLeader" runat="server">
        <h4>
            <asp:Label ID="lbTitleGroupACTResult" runat="server" /></h4>

        <SplendidCRM:SplendidGrid ID="grdLeader" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
            <Columns>
                <asp:TemplateColumn HeaderText=".LBL_NO">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                    <ItemStyle CssClass="dataField  format-qty" Width="3%" />
                    <ItemTemplate>
                        <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                        <asp:HiddenField ID="txtKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                        <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                    <ItemStyle CssClass="dataField  format-qty" Width="20%" />
                    <ItemTemplate>
                        <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray format-qty" />
                    <ItemStyle CssClass="dataField  format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                        <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_RATIO">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField  right format-qty" Width="5%" />
                    <ItemTemplate>
                        <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_PLAN_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField  right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblPLAN_VALUE" Text='<%# Bind("FLEX2") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_SYNC_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" Width="10%" />
                    <ItemStyle CssClass="dataField  right format-qty" />
                    <ItemTemplate>
                        <asp:Label ID="lblSYNC_VALUE" Text='<%# Bind("FLEX3") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblFINAL_VALUE" Text='<%# Bind("FLEX4") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_NEED_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblNEED_VALUE" Text='<%# Bind("NEED_VALUE") %>' runat="server" CssClass="right format-qty" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_PERCENT_FINAL_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblPERCENT_FINAL_VALUE" Text='<%# Bind("PERCENT_MANUAL_VALUE") %>' runat="server" CssClass="right format-qty" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </SplendidCRM:SplendidGrid>
    </asp:PlaceHolder>
</div>

<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
