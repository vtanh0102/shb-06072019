﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIM02
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();

                        //get CODE
                        TextBox txt_KPI_CODE = this.FindControl("KPI_CODE") as TextBox;
                        DataTable dt1 = new DataTable();
                        dt1 = null;
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            if (dt1 == null)
                            {
                                string sSQL;
                                sSQL = "select KPI_CODE             " + ControlChars.CrLf
                                     + "  from vwM_KPIS_Edit        " + ControlChars.CrLf
                                     + "  where KPI_CODE = '" + txt_KPI_CODE.Text.ToString() + "'     " + ControlChars.CrLf
                                       + "  AND ID <> '" + gID.ToString() + "'     " + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        dt1 = new DataTable();
                                        da.Fill(dt1);
                                    }
                                }
                            }
                        }
                        if (dt1.Rows.Count > 0)
                        {
                            ctlDynamicButtons.ErrorText = L10n.Term("KPIM02.ERR_DUPLICATE_KPI_CODE");
                        }
                        else
                        {
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                DataRow rowCurrent = null;
                                DataTable dtCurrent = new DataTable();
                                if (!Sql.IsEmptyGuid(gID))
                                {
                                    string sSQL;
                                    sSQL = "select *           " + ControlChars.CrLf
                                         + "  from vwM_KPIS_Edit" + ControlChars.CrLf;
                                    using (IDbCommand cmd = con.CreateCommand())
                                    {
                                        cmd.CommandText = sSQL;

                                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                        //Security.Filter(cmd, m_sMODULE, "edit");

                                        Sql.AppendParameter(cmd, gID, "ID", false);
                                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                                        {
                                            ((IDbDataAdapter)da).SelectCommand = cmd;
                                            da.Fill(dtCurrent);
                                            if (dtCurrent.Rows.Count > 0)
                                            {
                                                rowCurrent = dtCurrent.Rows[0];
                                                DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                                if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                                {
                                                    ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                    ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                    throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                                }
                                            }
                                            else
                                            {
                                                gID = Guid.Empty;
                                            }
                                        }
                                    }
                                }

                                this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                                bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                                if (bDUPLICATE_CHECHING_ENABLED)
                                {
                                    if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                    {
                                        ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                        ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                        throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                    }
                                }
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                        if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                            gASSIGNED_USER_ID = Security.USER_ID;
                                        Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                        if (Sql.IsEmptyGuid(gTEAM_ID))
                                            gTEAM_ID = Security.TEAM_ID;
                                        var ratio = new SplendidCRM.DynamicControl(this, rowCurrent, "RATIO");
                                        var maxratio = new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_RATIO_COMPLETE");

                                        if (!string.IsNullOrEmpty(ratio.Text))
                                        {
                                            float fRatio = 0;
                                            if (!float.TryParse(ratio.Text, out fRatio))
                                            {
                                                throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM02.LBL_RATIO"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                            }

                                            if (fRatio <= 0)
                                            {
                                                throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM02.LBL_RATIO"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(maxratio.Text))
                                        {

                                            float fMaxRatio = 0;
                                            if (!float.TryParse(maxratio.Text, out fMaxRatio))
                                            {
                                                throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIM02.LBL_MAX_RATIO_COMPLETE"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                            }

                                            if (fMaxRatio < 100)
                                            {
                                                throw (new Exception(string.Format("{0}", L10n.Term(".ERR_MAX_RATIO_COMP_NOT_IN_RANGE"))));
                                            }

                                        }

                                        KPIM02_SqlProc.spM_KPIS_Update
                                            (ref gID
                                            , gASSIGNED_USER_ID
                                            , gTEAM_ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_NAME").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_CODE").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_NAME").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "PARENT_ID").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "LEVEL_NUMBER").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "UNIT").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "RATIO").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_RATIO_COMPLETE").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_RATIO_COMPLETE").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_RATIO_COMPLETE").FloatValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "MAX_POINT").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "MIN_POINT").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DEFAULT_POINT").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "SORTING_ORDER").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                            , trn
                                            );

                                        SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                        SplendidCRM.SqlProcs.spTRACKER_Update
                                            (Security.USER_ID
                                            , m_sMODULE
                                            , gID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                            , "save"
                                            , trn
                                            );
                                        if (plcSubPanel.Visible)
                                        {
                                            foreach (Control ctl in plcSubPanel.Controls)
                                            {
                                                InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                                if (ctlSubPanel != null)
                                                {
                                                    ctlSubPanel.Save(gID, m_sMODULE, trn);
                                                }
                                            }
                                        }
                                        trn.Commit();
                                        SplendidCache.ClearFavorites();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                        ctlDynamicButtons.ErrorText = ex.Message;
                                        return;
                                    }
                                }

                                rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                                this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            }

                            if (!Sql.IsEmptyString(RulesRedirectURL))
                                Response.Redirect(RulesRedirectURL);
                            else
                                Response.Redirect("view.aspx?ID=" + gID.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }

        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //Label LblPageTitle = this.FindControl("LblPageTitle") as Label;
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        //LblPageTitle.Text = L10n.Term("KPIM02.LBL_PAGE_EDIT_TITLE");
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwM_KPIS_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["NAME"] = Sql.ToString(rdr["NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["NAME"] = ViewState["NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }

                        //check TH gia tri =0 thi ko hien thi ta
                        TextBox txt_Ratio = this.FindControl("RATIO") as TextBox;
                        TextBox txt_MRatio = this.FindControl("MAX_RATIO_COMPLETE") as TextBox;
                        if (txt_Ratio.Text == "0" || txt_Ratio.Text == "0.00" || txt_Ratio.Text == "0,00")
                        {
                            txt_Ratio.Text = "";
                        }

                        if (txt_MRatio.Text == "0" || txt_MRatio.Text == "0.00" || txt_MRatio.Text == "0,00")
                        {
                            txt_MRatio.Text = "";
                        }                       
                    }
                    else
                    {
                        //LblPageTitle.Text = L10n.Term("KPIM02.LBL_PAGE_CREATE_TITLE");
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);

                        TextBox txtNAME = this.FindControl("NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();

                        Label txt_LEVEL_NUMBER = this.FindControl("LEVEL_NUMBER") as Label;
                        if (txt_LEVEL_NUMBER != null)
                            txt_LEVEL_NUMBER.Text = "1";

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }

                    ListControl lbParentF = this.FindControl("PARENT_ID") as ListControl;
                    if (lbParentF != null)
                    {
                        lbParentF.AutoPostBack = true;
                        lbParentF.SelectedIndexChanged += new EventHandler(ddrParent_SelectIndexChange);
                    }
                    return;
                    //end huynt
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["NAME"] = ViewState["NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }

                ListControl lbParent = this.FindControl("PARENT_ID") as ListControl;
                if (lbParent != null)
                {
                    lbParent.AutoPostBack = true;
                    lbParent.SelectedIndexChanged += new EventHandler(ddrParent_SelectIndexChange);
                }

                //check TH gia tri =0 thi ko hien thi ta    
                TextBox txt_Ratio1 = this.FindControl("RATIO") as TextBox;
                TextBox txt_MRatio1 = this.FindControl("MAX_RATIO_COMPLETE") as TextBox;
                if (txt_Ratio1.Text == "0" || txt_Ratio1.Text == "0.00" || txt_Ratio1.Text == "0,00")
                {
                    txt_Ratio1.Text = "";
                }

                if (txt_MRatio1.Text == "0" || txt_MRatio1.Text == "0.00" || txt_MRatio1.Text == "0,00")
                {
                    txt_MRatio1.Text = "";
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void ddrParent_SelectIndexChange(object sender, EventArgs e)
        {
            TextBox txt_KPI_CODE = this.FindControl("KPI_CODE") as TextBox;
            ListControl lbParent = this.FindControl("PARENT_ID") as ListControl;
            Label txt_LEVEL_NUMBER = this.FindControl("LEVEL_NUMBER") as Label;
            Guid urlPar_parentID = Guid.Empty;
            if (lbParent != null && lbParent.SelectedValue != "")
            {
                urlPar_parentID = Sql.ToGuid(lbParent.SelectedValue.ToString());
                txt_KPI_CODE = this.FindControl("KPI_CODE") as TextBox;
                if (txt_KPI_CODE != null)
                {
                    txt_KPI_CODE.Text = generateCODE();
                }
                txt_LEVEL_NUMBER.Text = txt_KPI_CODE.Text.Split('.').Length.ToString();
            }
            else
            {
                txt_LEVEL_NUMBER.Text = "1";
            }
        }

        private string generateCODE()
        {
            string headerCode = "";
            string code = "";
            //get chuoi tien to tu bang THAMSO_HT
            L10N L10n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
            DataTable dt = Cache.Get(L10n.NAME + ".vwTHAMSO_HT") as DataTable;
            dt = null;
            if (dt == null)
            {
                try
                {
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        con.Open();
                        string sSQL;
                        sSQL = "select VALUE   " + ControlChars.CrLf
                             + "  from THAMSO_HT     " + ControlChars.CrLf
                             + "  where DELETED = 0 and NAME = 'KPIM02_CODE'     " + ControlChars.CrLf;
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQL;
                            using (DbDataAdapter da = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da).SelectCommand = cmd;
                                dt = new DataTable();
                                da.Fill(dt);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                }
            }
            headerCode = dt.Rows[0][0].ToString();
            //END get chuoi tien to tu bang THAMSO_HT
            DropDownList drop_PARENT_ID = this.FindControl("PARENT_ID") as DropDownList;
            if (drop_PARENT_ID.SelectedValue.ToString() != "")
            {
                //get CODE
                L10N L11n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt1 = Cache.Get(L11n.NAME + ".vwM_KPIS") as DataTable;
                dt1 = null;
                if (dt1 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "select KPI_CODE   " + ControlChars.CrLf
                                 + "  from M_KPIS     " + ControlChars.CrLf
                                 + "  where DELETED = 0 and ID = '" + drop_PARENT_ID.SelectedValue.ToString() + "'     " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt1 = new DataTable();
                                    da.Fill(dt1);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                headerCode = dt1.Rows[0][0].ToString();
                //END get CODE
                //get COUNT(*) kpim02 co parent_id duoc chon
                L10N L12n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt2 = Cache.Get(L12n.NAME + ".vwM_KPIS") as DataTable;
                dt2 = null;
                if (dt2 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "select COUNT(*)   " + ControlChars.CrLf
                                 + "  from M_KPIS     " + ControlChars.CrLf
                                 + "  where DELETED = 0 and PARENT_ID = '" + drop_PARENT_ID.SelectedValue.ToString().ToUpper() + "'     " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt2 = new DataTable();
                                    da.Fill(dt2);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                //END get COUNT(*) kpim02 co parent_id duoc chon
                //tang them 1
                if ((int.Parse(dt2.Rows[0][0].ToString()) + 1) < 10)
                {
                    code = headerCode + ".0" + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                else
                {
                    code = headerCode + "." + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                //END tang them 1
            }
            else
            {
                //get COUNT(*) kpim02 co parent_id null
                L10N L12n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);
                DataTable dt2 = Cache.Get(L12n.NAME + ".vwM_KPIS") as DataTable;
                dt2 = null;
                if (dt2 == null)
                {
                    try
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            string sSQL;
                            sSQL = "SELECT COUNT(*)   " + ControlChars.CrLf
                                 + "  FROM M_KPIS     " + ControlChars.CrLf
                                 + "  WHERE DELETED = 0 AND PARENT_ID IS NULL    " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    dt2 = new DataTable();
                                    da.Fill(dt2);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    }
                }
                //END get COUNT(*) kpim02 co parent_id null
                //tang them 1
                if ((int.Parse(dt2.Rows[0][0].ToString()) + 1) < 10)
                {
                    code = headerCode + "0" + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                else
                {
                    code = headerCode + (int.Parse(dt2.Rows[0][0].ToString()) + 1).ToString();
                }
                //END tang them 1
            }
            return code;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            m_sMODULE = "KPIM02";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
