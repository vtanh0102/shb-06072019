﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIM02.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<style type="text/css">    
    input[readonly] {
        background-color: transparent;
        border: 0;
        font-size: 1em;
    }
</style>
<div id="divEditView" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<div style="margin-top:10px;"></div> 
  <%--  <asp:Label ID="LblPageTitle" Text="Tiêu đề ở đây" runat="server"></asp:Label>--%>
    <SplendidCRM:HeaderButtons  ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIM02" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />
	<asp:HiddenField ID="LAYOUT_EDIT_VIEW" Runat="server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblMain" class="tabEditView" runat="server">
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<div id="divEditSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
	</div>

	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
