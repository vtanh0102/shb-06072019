
function M_KPIS_M_KPI_NAME_Changed(fldM_KPI_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldM_KPI_NAME.id.substring(0, fldM_KPI_NAME.id.length - 'M_KPI_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'M_KPI_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_M_KPI_NAME = document.getElementById(userContext + 'PREV_M_KPI_NAME');
	if ( fldPREV_M_KPI_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_M_KPI_NAME');
	}
	else if ( fldPREV_M_KPI_NAME.value != fldM_KPI_NAME.value )
	{
		if ( fldM_KPI_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIM02.AutoComplete.M_KPIS_M_KPI_NAME_Get(fldM_KPI_NAME.value, M_KPIS_M_KPI_NAME_Changed_OnSucceededWithContext, M_KPIS_M_KPI_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('M_KPIS_M_KPI_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			M_KPIS_M_KPI_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function M_KPIS_M_KPI_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'M_KPI_NAME_AjaxErrors');
		var fldM_KPI_ID        = document.getElementById(userContext + 'M_KPI_ID'       );
		var fldM_KPI_NAME      = document.getElementById(userContext + 'M_KPI_NAME'     );
		var fldPREV_M_KPI_NAME = document.getElementById(userContext + 'PREV_M_KPI_NAME');
		if ( fldM_KPI_ID        != null ) fldM_KPI_ID.value        = sID  ;
		if ( fldM_KPI_NAME      != null ) fldM_KPI_NAME.value      = sNAME;
		if ( fldPREV_M_KPI_NAME != null ) fldPREV_M_KPI_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIM02.AutoComplete service is null');
	}
}

function M_KPIS_M_KPI_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'M_KPI_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldM_KPI_ID        = document.getElementById(userContext + 'M_KPI_ID'       );
	var fldM_KPI_NAME      = document.getElementById(userContext + 'M_KPI_NAME'     );
	var fldPREV_M_KPI_NAME = document.getElementById(userContext + 'PREV_M_KPI_NAME');
	if ( fldM_KPI_ID        != null ) fldM_KPI_ID.value        = '';
	if ( fldM_KPI_NAME      != null ) fldM_KPI_NAME.value      = '';
	if ( fldPREV_M_KPI_NAME != null ) fldPREV_M_KPI_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

