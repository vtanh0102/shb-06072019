/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Contacts.SalesFusion
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.ModuleHeader   ctlModuleHeader  ;
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sID                             ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						bool   bSalesFusionEnabled  = Sql.ToBoolean(Context.Application["CONFIG.SalesFusion.Enabled" ]);
						string sSalesFusionUserName = Sql.ToString (Context.Application["CONFIG.SalesFusion.UserName"]);
						if ( bSalesFusionEnabled && !Sql.IsEmptyString(sSalesFusionUserName) )
						{
							Spring.Social.SalesFusion.Api.ISalesFusion salesFusion = Spring.Social.SalesFusion.SalesFusionSync.CreateApi(Application);
							/*
							DataTable dtUsers    = new DataTable();
							DataTable dtAccounts = new DataTable();
							DataTable dtContacts = new DataTable();
							DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								string sSQL = String.Empty;
								sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
								     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
								     + "  from vwACCOUNTS_SYNC                               " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME     = N'SalesFusion'        " + ControlChars.CrLf
								     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtAccounts);
									}
								}
								sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
								     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
								     + "  from vwCONTACTS_SYNC                               " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME     = N'SalesFusion'        " + ControlChars.CrLf
								     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtContacts);
									}
								}
								sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
								     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
								     + "  from vwUSERS_SYNC                                  " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME     = N'SalesFusion'        " + ControlChars.CrLf
								     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtUsers);
									}
								}
							}
							*/
							Spring.Social.SalesFusion.Contact sfo = null;
							if ( !Sql.IsEmptyString(sID) )
							{
								sfo.SetFromSalesFusion(Sql.ToInteger(sID));
							}
							//sfo.account_id           = new DynamicControl(this, "account_id"        ).Text;
							if ( this.FindControl("salutation"     ) != null ) sfo.salutation          = new DynamicControl(this, "salutation"     ).Text;  // 10
							if ( this.FindControl("first_name"     ) != null ) sfo.first_name          = new DynamicControl(this, "first_name"     ).Text;  // 40
							if ( this.FindControl("last_name"      ) != null ) sfo.last_name           = new DynamicControl(this, "last_name"      ).Text;  // 40
							if ( this.FindControl("phone"          ) != null ) sfo.phone               = new DynamicControl(this, "phone"          ).Text;  // 40
							if ( this.FindControl("mobile"         ) != null ) sfo.mobile              = new DynamicControl(this, "mobile"         ).Text;  // 40
							if ( this.FindControl("fax"            ) != null ) sfo.fax                 = new DynamicControl(this, "fax"            ).Text;  // 40
							if ( this.FindControl("home_phone"     ) != null ) sfo.home_phone          = new DynamicControl(this, "home_phone"     ).Text;  // 40
							if ( this.FindControl("other_phone"    ) != null ) sfo.other_phone         = new DynamicControl(this, "other_phone"    ).Text;  // 40
							if ( this.FindControl("email"          ) != null ) sfo.email               = new DynamicControl(this, "email"          ).Text;  // 250
							if ( this.FindControl("billing_street" ) != null ) sfo.billing_street      = new DynamicControl(this, "billing_street" ).Text;  // 80
							if ( this.FindControl("billing_city"   ) != null ) sfo.billing_city        = new DynamicControl(this, "billing_city"   ).Text;  // 40
							if ( this.FindControl("billing_state"  ) != null ) sfo.billing_state       = new DynamicControl(this, "billing_state"  ).Text;  // 20
							if ( this.FindControl("billing_zip"    ) != null ) sfo.billing_zip         = new DynamicControl(this, "billing_zip"    ).Text;  // 20
							if ( this.FindControl("billing_country") != null ) sfo.billing_country     = new DynamicControl(this, "billing_country").Text;  // 40
							if ( this.FindControl("mailing_street" ) != null ) sfo.mailing_street      = new DynamicControl(this, "mailing_street" ).Text;  // 80
							if ( this.FindControl("mailing_city"   ) != null ) sfo.mailing_city        = new DynamicControl(this, "mailing_city"   ).Text;  // 40
							if ( this.FindControl("mailing_state"  ) != null ) sfo.mailing_state       = new DynamicControl(this, "mailing_state"  ).Text;  // 20
							if ( this.FindControl("mailing_zip"    ) != null ) sfo.mailing_zip         = new DynamicControl(this, "mailing_zip"    ).Text;  // 20
							if ( this.FindControl("mailing_country") != null ) sfo.mailing_country     = new DynamicControl(this, "mailing_country").Text;  // 40
							if ( this.FindControl("title"          ) != null ) sfo.title               = new DynamicControl(this, "title"          ).Text;  // 80
							if ( this.FindControl("department"     ) != null ) sfo.department          = new DynamicControl(this, "department"     ).Text;  // 80
							if ( this.FindControl("assistant_name" ) != null ) sfo.assistant_name      = new DynamicControl(this, "assistant_name" ).Text;  // 40
							if ( this.FindControl("assistant_phone") != null ) sfo.assistant_phone     = new DynamicControl(this, "assistant_phone").Text;  // 40
							if ( this.FindControl("description"    ) != null ) sfo.description         = new DynamicControl(this, "description"    ).Text;  // 450
							if ( this.FindControl("opt_out"        ) != null ) sfo.opt_out             = new DynamicControl(this, "opt_out"        ).Checked;
							if ( this.FindControl("do_not_call"    ) != null ) sfo.do_not_call         = new DynamicControl(this, "do_not_call"    ).Checked;
							if ( this.FindControl("source"         ) != null ) sfo.source              = new DynamicControl(this, "source"         ).Text;  // 250
							if ( this.FindControl("birth_date"     ) != null ) sfo.birth_date          = new DynamicControl(this, "birth_date"     ).DateValue;
							if ( !Sql.IsEmptyString(sID) )
							{
								sfo.Update();
							}
							else
							{
								sID = sfo.Insert();
							}
							Response.Redirect("view.aspx?contact_id=" + sID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?contact_id=" + sID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 07/23/2017 Paul.  Consistent application of security for cloud service. 
			this.Visible = Spring.Social.SalesFusion.SalesFusionSync.SalesFusionEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("SalesFusion", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sID = Sql.ToString(Request["contact_id"]);
				if ( !IsPostBack )
				{
					string sDuplicateID = Sql.ToString(Request["duplicate_id"]);
					if ( !Sql.IsEmptyString(sID) || !Sql.IsEmptyString(sDuplicateID) )
					{
						Spring.Social.SalesFusion.Api.ISalesFusion salesFusion = Spring.Social.SalesFusion.SalesFusionSync.CreateApi(Application);
						Spring.Social.SalesFusion.Api.Contact contact = null;
						if ( !Sql.IsEmptyString(sDuplicateID) )
							contact = salesFusion.ContactOperations.GetById(Sql.ToInteger(sDuplicateID));
						else
							contact = salesFusion.ContactOperations.GetById(Sql.ToInteger(sID));
						if ( contact != null )
						{
							DataRow rdr = Spring.Social.SalesFusion.Api.Contact.ConvertToRow(contact);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
							ctlModuleHeader.Title = Sql.ToString(rdr["FirstName"]) + " " + Sql.ToString(rdr["LastName"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
							ViewState["ctlModuleHeader.Title"] = ctlModuleHeader.Title;
								
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							TextBox txtNAME = this.FindControl("FirstName") as TextBox;
							if ( txtNAME != null )
								txtNAME.Focus();
								
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("FirstName") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlModuleHeader.Title = Sql.ToString(ViewState["ctlModuleHeader.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Contacts";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

