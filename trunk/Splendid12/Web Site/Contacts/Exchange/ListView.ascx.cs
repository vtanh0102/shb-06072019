/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using Microsoft.Exchange.WebServices.Data;

namespace SplendidCRM.Contacts.Exchange
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected _controls.ExportHeader ctlExportHeader;
		protected _controls.SearchView   ctlSearchView  ;
		protected _controls.CheckAll     ctlCheckAll    ;

		protected UniqueStringCollection arrSelectFields;
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected Label         lblError       ;
		protected MassUpdate    ctlMassUpdate  ;
		// 06/06/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected Panel         pnlMassUpdateSeven;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Search" )
				{
					// 10/13/2005 Paul.  Make sure to clear the page index prior to applying search. 
					grdMain.CurrentPageIndex = 0;
					// 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
					grdMain.DataBind();
				}
				// 12/14/2007 Paul.  We need to capture the sort event from the SearchView. 
				else if ( e.CommandName == "SortGrid" )
				{
					grdMain.SetSortFields(e.CommandArgument as string[]);
					// 04/27/2008 Paul.  Sorting has been moved to the database to increase performance. 
					// 03/17/2011 Paul.  We need to treat a comma-separated list of fields as an array. 
					arrSelectFields.AddFields(grdMain.SortColumn);
				}
				// 11/17/2010 Paul.  Populate the hidden Selected field with all IDs. 
				else if ( e.CommandName == "SelectAll" )
				{
					// 05/22/2011 Paul.  When using custom paging, vwMain may not be defined. 
					if ( vwMain == null )
						grdMain.DataBind();
					ctlCheckAll.SelectAll(vwMain, "ID");
					grdMain.DataBind();
				}
				// 06/06/2015 Paul.  Change standard MassUpdate command to a command to toggle visibility. 
				else if ( e.CommandName == "ToggleMassUpdate" )
				{
					pnlMassUpdateSeven.Visible = !pnlMassUpdateSeven.Visible;
				}
				else if ( e.CommandName == "MassDelete" )
				{
					// 11/27/2010 Paul.  Use new selected items. 
					string[] arrID = ctlCheckAll.SelectedItemsArray;
					if ( arrID != null )
					{
						//Response.Redirect("default.aspx");
					}
				}
				else if ( e.CommandName == "Export" )
				{
					// 11/03/2006 Paul.  Apply ACL rules to Export. 
					int nACLACCESS = SplendidCRM.Security.GetUserAccess(m_sMODULE, "export");
					if ( nACLACCESS  >= 0 )
					{
						// 10/05/2009 Paul.  When exporting, we may need to manually bind.  Custom paging should be disabled when exporting all. 
						if ( vwMain == null )
							grdMain.DataBind();
						if ( nACLACCESS == ACL_ACCESS.OWNER )
							vwMain.RowFilter = "ASSIGNED_USER_ID = '" + Security.USER_ID.ToString() + "'";
						// 11/27/2010 Paul.  Use new selected items. 
						string[] arrID = ctlCheckAll.SelectedItemsArray;
						SplendidExport.Export(vwMain, m_sMODULE, ctlExportHeader.ExportFormat, ctlExportHeader.ExportRange, grdMain.CurrentPageIndex, grdMain.PageSize, arrID, grdMain.AllowCustomPaging);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				if ( !IsPostBack )
				{
					grdMain.OrderByClause("NAME", "asc");
					
					Guid gUSER_ID = Security.USER_ID;
					Guid gTEAM_ID = Security.TEAM_ID;
					string sEXCHANGE_ALIAS     = String.Empty;
					string sEXCHANGE_EMAIL     = String.Empty;
					string sEXCHANGE_WATERMARK = String.Empty;
					string sMAIL_SMTPUSER      = String.Empty;
					string sMAIL_SMTPPASS      = String.Empty;
					bool   bOFFICE365_OAUTH_ENABLED = false;
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						// 01/17/2017 Paul.  Get all columns so that we do not get an error when including OFFICE365_OAUTH_ENABLED. 
						sSQL = "select *                 " + ControlChars.CrLf
						     + "  from vwEXCHANGE_USERS  " + ControlChars.CrLf
						     + " where ASSIGNED_USER_ID = @ASSIGNED_USER_ID" + ControlChars.CrLf
						     + " order by EXCHANGE_ALIAS " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gUSER_ID);
							using ( IDataReader rdr = cmd.ExecuteReader() )
							{
								if ( rdr.Read() )
								{
									sEXCHANGE_ALIAS     = Sql.ToString(rdr["EXCHANGE_ALIAS"    ]);
									sEXCHANGE_EMAIL     = Sql.ToString(rdr["EXCHANGE_EMAIL"    ]);
									sEXCHANGE_WATERMARK = Sql.ToString(rdr["EXCHANGE_WATERMARK"]);
									sMAIL_SMTPUSER      = Sql.ToString(rdr["MAIL_SMTPUSER"     ]);
									sMAIL_SMTPPASS      = Sql.ToString(rdr["MAIL_SMTPPASS"     ]);
									// 01/17/2017 Paul.  The gEXCHANGE_ID is to lookup the OAuth credentials. 
									try
									{
										bOFFICE365_OAUTH_ENABLED = Sql.ToBoolean(rdr["OFFICE365_OAUTH_ENABLED"]);
									}
									catch
									{
									}
								}
							}
						}
					}
					// 02/28/2012 Paul.  If this user does not define Exchange authentication, then exit early. 
					if ( Sql.IsEmptyString(Application["CONFIG.Exchange.ServerURL"]) || (Sql.IsEmptyString(sEXCHANGE_ALIAS) && Sql.IsEmptyString(sEXCHANGE_EMAIL)) )
					{
						return;
					}
					
					DataTable dt = new DataTable();
					dt.Columns.Add("NAME"            , typeof(String  ));
					dt.Columns.Add("DATE_MODIFIED"   , typeof(DateTime));
					dt.Columns.Add("ASSIGNED_TO_NAME", typeof(String  ));
					dt.Columns.Add("CREATED_BY_NAME" , typeof(String  ));
					dt.Columns.Add("MODIFIED_BY_NAME", typeof(String  ));
					dt.Columns.Add("TEAM_NAME"       , typeof(String  ));
					dt.Columns.Add("TEAM_SET_NAME"   , typeof(String  ));
					IDbCommand spCONTACTS_Update = null;
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						spCONTACTS_Update = SqlProcs.Factory(con, "spCONTACTS_Update");
						foreach(IDbDataParameter par in spCONTACTS_Update.Parameters)
						{
							// 02/28/2012 Paul.  Parameters need to be initialized to DBNull.Value. 
							par.Value = DBNull.Value;
							string sParameterName = Sql.ExtractDbName(spCONTACTS_Update, par.ParameterName).ToUpper();
							if ( dt.Columns.Contains(sParameterName) )
								continue;
							switch ( par.DbType )
							{
								case DbType.Guid    : dt.Columns.Add(sParameterName, typeof(Guid    ));  break;
								case DbType.Int16   : dt.Columns.Add(sParameterName, typeof(Int16   ));  break;
								case DbType.Int32   : dt.Columns.Add(sParameterName, typeof(Int32   ));  break;
								case DbType.Int64   : dt.Columns.Add(sParameterName, typeof(Int64   ));  break;
								case DbType.Double  : dt.Columns.Add(sParameterName, typeof(Double  ));  break;
								case DbType.Decimal : dt.Columns.Add(sParameterName, typeof(Decimal ));  break;
								case DbType.Byte    : dt.Columns.Add(sParameterName, typeof(Byte    ));  break;
								case DbType.Boolean : dt.Columns.Add(sParameterName, typeof(bool    ));  break;
								case DbType.DateTime: dt.Columns.Add(sParameterName, typeof(DateTime));  break;
								default             : dt.Columns.Add(sParameterName, typeof(String  ));  break;
							}
						}
					}
					
					ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
					ExchangeSync.UserSync User = new ExchangeSync.UserSync(Context, sEXCHANGE_ALIAS, sEXCHANGE_EMAIL, sMAIL_SMTPUSER, sMAIL_SMTPPASS, gUSER_ID, sEXCHANGE_WATERMARK, false, bOFFICE365_OAUTH_ENABLED);
					ExchangeService service = ExchangeUtils.CreateExchangeService(User);
					// 06/23/2015 Paul.  We are getting an exception with Office365 when the category is provided.  Try using Advanced Query Syntax. 
					// 06/23/2015 Paul.  Advanced Query does not work because we cannot search for category and last modified time. 
					// http://blogs.technet.com/b/exchangesearch/archive/2012/03/10/how-to-use-aqs-to-construct-complex-discovery-queries.aspx
					// https://msdn.microsoft.com/en-us/library/aa965711(VS.85).aspx
					// https://msdn.microsoft.com/en-us/library/office/dn579420(v=exchg.150).aspx
					// http://stackoverflow.com/questions/21575264/search-by-category-exchange-server-ews
					SearchFilter filter = null;
					// 06/23/2015 Paul.  Add separate category fields so that we can allow an empty category to mean all. 
					string sCONTACTS_CATEGORY = Sql.ToString(Application["CONFIG.Exchange.Contacts.Category"]);
					if ( !Sql.IsEmptyString(sCONTACTS_CATEGORY) )
					{
						List<SearchFilter> filters = new List<SearchFilter>();
						// 06/23/2015 Paul.  Exchange 2010 does not allow the contains search, so use IsEqualTo instead. 
						filters.Add(new SearchFilter.IsEqualTo(ContactSchema.Categories, sCONTACTS_CATEGORY));
						filter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, filters.ToArray());
					}
					
					int nPageOffset = 0;
					// 07/07/2015 Paul.  Allow the page size to be customized. 
					int nPageSize = Sql.ToInteger(Application["CONFIG.Exchange.Contacts.PageSize"]);
					if ( nPageSize <= 0 )
						nPageSize = 100;
					FindItemsResults<Item> results = null;
					do
					{
						ItemView ivContacts = new ItemView(nPageSize, nPageOffset);
						// 03/26/2010 Paul.  Sort Ascending. 
						ivContacts.OrderBy.Add(ContactSchema.LastModifiedTime, Microsoft.Exchange.WebServices.Data.SortDirection.Ascending);
						// 06/23/2015 Paul.  We are getting an exception with Office365 when the category is provided.  Try using Advanced Query Syntax. 
						if ( filter != null )
							results = service.FindItems(WellKnownFolderName.Contacts, filter, ivContacts);
						else
							results = service.FindItems(WellKnownFolderName.Contacts, ivContacts);
						if ( results.Items.Count > 0 )
							SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), "SyncContacts: " + results.Items.Count.ToString() + " contacts to retrieve from " + sEXCHANGE_ALIAS);
						foreach (Item itemContact in results.Items )
						{
							if ( itemContact is Microsoft.Exchange.WebServices.Data.Contact )
							{
								Microsoft.Exchange.WebServices.Data.Contact contact = itemContact as Microsoft.Exchange.WebServices.Data.Contact;
								// 01/28/2012 Paul.  The transaction is necessary so that an account can be created. 
								ExchangeSync.BuildCONTACTS_Update(Application, Session, spCONTACTS_Update, null, contact, gUSER_ID, gTEAM_ID, gUSER_ID, false, null);
								DataRow row = dt.NewRow();
								row["NAME"            ] = contact.FileAs;
								row["DATE_MODIFIED"   ] = contact.LastModifiedTime;
								row["ASSIGNED_TO_NAME"] = Security.USER_NAME;
								row["CREATED_BY_NAME" ] = Security.USER_NAME;
								row["MODIFIED_BY_NAME"] = Security.USER_NAME;
								row["TEAM_NAME"       ] = Security.TEAM_NAME;
								row["TEAM_SET_NAME"   ] = Security.TEAM_NAME;
								foreach(IDbDataParameter par in spCONTACTS_Update.Parameters)
								{
									string sParameterName = Sql.ExtractDbName(spCONTACTS_Update, par.ParameterName).ToUpper();
									row[sParameterName] = par.Value;
								}
								dt.Rows.Add(row);
							}
						}
						nPageOffset += nPageSize;
					}
					while ( results.MoreAvailable );
					
					//this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);
					vwMain = dt.DefaultView;
					grdMain.DataSource = vwMain ;
					ViewState["Contacts"] = dt;
					
					ctlExportHeader.Visible = true;
					ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
					// 06/06/2015 Paul.  Change standard MassUpdate command to a command to toggle visibility. 
					ctlCheckAll  .Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
				}
				else
				{
					DataTable dt = ViewState["Contacts"] as DataTable;
					if ( dt != null )
					{
						vwMain = dt.DefaultView;
						grdMain.DataSource = vwMain ;
					}
				}
				if ( !IsPostBack )
				{
					grdMain.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Contact is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlSearchView  .Command += new CommandEventHandler(Page_Command);
			ctlExportHeader.Command += new CommandEventHandler(Page_Command);
			ctlMassUpdate  .Command += new CommandEventHandler(Page_Command);
			ctlCheckAll    .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Contacts";
			SetMenu(m_sMODULE);
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("NAME");
			this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields);
			if ( Security.GetUserAccess(m_sMODULE, "delete") < 0 && Security.GetUserAccess(m_sMODULE, "edit") < 0 )
				ctlMassUpdate.Visible = false;
			
			// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
			if ( SplendidDynamic.StackedLayout(Page.Theme) )
			{
				// 06/05/2015 Paul.  Move MassUpdate buttons to the SplendidGrid. 
				grdMain.IsMobile       = this.IsMobile;
				grdMain.MassUpdateView = m_sMODULE + ".MassUpdate";
				grdMain.Command       += new CommandEventHandler(Page_Command);
				if ( !IsPostBack )
					pnlMassUpdateSeven.Visible = false;
			}
		}
		#endregion
	}
}

