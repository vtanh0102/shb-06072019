/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using iCloud;
using iCloud.Contacts;
using iCloud.Calendar;

namespace SplendidCRM.Contacts.iCloud
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 01/13/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sUID                            ;
		protected HtmlTable       tblMain                         ;
		// 09/02/2012 Paul.  EditViews were combined into a single view. 
		//protected HtmlTable       tblAddress                      ;
		//protected HtmlTable       tblDescription                  ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ValidateEditViewFields(m_sMODULE + ".EditAddress"    );
					this.ValidateEditViewFields(m_sMODULE + ".EditDescription");
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + ".EditAddress"    );
					this.ApplyEditViewValidationEventRules(m_sMODULE + ".EditDescription");
					
					if ( Page.IsValid )
					{
						DataTable dtCurrent = new DataTable();
						// 12/19/2011 Paul.  Get an empty recordset as a quick way to populate an empty row. 
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL ;
							sSQL = "select *              " + ControlChars.CrLf
							     + "  from vwCONTACTS_Edit" + ControlChars.CrLf
							     + " where ID is null     " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									da.Fill(dtCurrent);
								}
							}
						}
						
							Guid gUSER_ID = Security.USER_ID;
						Guid gTEAM_ID = Security.TEAM_ID;
						string sICLOUD_USERNAME      = String.Empty;
						string sICLOUD_PASSWORD      = String.Empty;
						string sICLOUD_CTAG_CONTACTS = String.Empty;
						string sICLOUD_CTAG_CALENDAR = String.Empty;
						iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
						
						IDbCommand spCONTACTS_Update = null;
						DataTable dt = iCloudSync.CreateContactsTable(Application, ref spCONTACTS_Update);
						
						ContactsService service = new ContactsService(Application);
						// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
						service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
						service.QueryClientLoginToken(false);
						
						ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
						
						StringBuilder sbChanges = new StringBuilder();
						ContactEntry contact = new ContactEntry();
						DataRow rowCurrent = dtCurrent.NewRow();
						rowCurrent["ASSIGNED_USER_ID"          ] = new DynamicControl(this, "ASSIGNED_USER_ID"          ).ID;
						rowCurrent["SALUTATION"                ] = new DynamicControl(this, "SALUTATION"                ).SelectedValue;
						rowCurrent["FIRST_NAME"                ] = new DynamicControl(this, "FIRST_NAME"                ).Text;
						rowCurrent["LAST_NAME"                 ] = new DynamicControl(this, "LAST_NAME"                 ).Text;
						rowCurrent["ACCOUNT_ID"                ] = new DynamicControl(this, "ACCOUNT_ID"                ).ID;
						rowCurrent["LEAD_SOURCE"               ] = new DynamicControl(this, "LEAD_SOURCE"               ).SelectedValue;
						rowCurrent["TITLE"                     ] = new DynamicControl(this, "TITLE"                     ).Text;
						rowCurrent["DEPARTMENT"                ] = new DynamicControl(this, "DEPARTMENT"                ).Text;
						rowCurrent["REPORTS_TO_ID"             ] = new DynamicControl(this, "REPORTS_TO_ID"             ).ID;
						rowCurrent["BIRTHDATE"                 ] = new DynamicControl(this, "BIRTHDATE"                 ).DateValue;
						rowCurrent["DO_NOT_CALL"               ] = new DynamicControl(this, "DO_NOT_CALL"               ).Checked;
						rowCurrent["PHONE_HOME"                ] = new DynamicControl(this, "PHONE_HOME"                ).Text;
						rowCurrent["PHONE_MOBILE"              ] = new DynamicControl(this, "PHONE_MOBILE"              ).Text;
						rowCurrent["PHONE_WORK"                ] = new DynamicControl(this, "PHONE_WORK"                ).Text;
						rowCurrent["PHONE_OTHER"               ] = new DynamicControl(this, "PHONE_OTHER"               ).Text;
						rowCurrent["PHONE_FAX"                 ] = new DynamicControl(this, "PHONE_FAX"                 ).Text;
						rowCurrent["EMAIL1"                    ] = new DynamicControl(this, "EMAIL1"                    ).Text;
						rowCurrent["EMAIL2"                    ] = new DynamicControl(this, "EMAIL2"                    ).Text;
						rowCurrent["ASSISTANT"                 ] = new DynamicControl(this, "ASSISTANT"                 ).Text;
						rowCurrent["ASSISTANT_PHONE"           ] = new DynamicControl(this, "ASSISTANT_PHONE"           ).Text;
						rowCurrent["EMAIL_OPT_OUT"             ] = new DynamicControl(this, "EMAIL_OPT_OUT"             ).Checked;
						rowCurrent["INVALID_EMAIL"             ] = new DynamicControl(this, "INVALID_EMAIL"             ).Checked;
						rowCurrent["PRIMARY_ADDRESS_STREET"    ] = new DynamicControl(this, "PRIMARY_ADDRESS_STREET"    ).Text;
						rowCurrent["PRIMARY_ADDRESS_CITY"      ] = new DynamicControl(this, "PRIMARY_ADDRESS_CITY"      ).Text;
						rowCurrent["PRIMARY_ADDRESS_STATE"     ] = new DynamicControl(this, "PRIMARY_ADDRESS_STATE"     ).Text;
						rowCurrent["PRIMARY_ADDRESS_POSTALCODE"] = new DynamicControl(this, "PRIMARY_ADDRESS_POSTALCODE").Text;
						rowCurrent["PRIMARY_ADDRESS_COUNTRY"   ] = new DynamicControl(this, "PRIMARY_ADDRESS_COUNTRY"   ).Text;
						rowCurrent["ALT_ADDRESS_STREET"        ] = new DynamicControl(this, "ALT_ADDRESS_STREET"        ).Text;
						rowCurrent["ALT_ADDRESS_CITY"          ] = new DynamicControl(this, "ALT_ADDRESS_CITY"          ).Text;
						rowCurrent["ALT_ADDRESS_STATE"         ] = new DynamicControl(this, "ALT_ADDRESS_STATE"         ).Text;
						rowCurrent["ALT_ADDRESS_POSTALCODE"    ] = new DynamicControl(this, "ALT_ADDRESS_POSTALCODE"    ).Text;
						rowCurrent["ALT_ADDRESS_COUNTRY"       ] = new DynamicControl(this, "ALT_ADDRESS_COUNTRY"       ).Text;
						rowCurrent["DESCRIPTION"               ] = new DynamicControl(this, "DESCRIPTION"               ).Text;
						rowCurrent["TEAM_ID"                   ] = new DynamicControl(this, "TEAM_ID"                   ).ID;
						rowCurrent["TEAM_SET_LIST"             ] = new DynamicControl(this, "TEAM_SET_LIST"             ).Text;
						iCloudSync.SetiCloudContact(contact, rowCurrent, sbChanges);
						if ( Sql.IsEmptyString(sUID) )
						{
							service.Insert(contact);
							sUID = contact.UID;
						}
						else
						{
							service.Update(contact);
						}
						Response.Redirect("view.aspx?UID=" + sUID);
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sUID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?UID=" + sUID);
			}
		}

		// 08/31/2010 Paul.  Update the Address information if the Account changes. 
		private void UpdateAccount(Guid gACCOUNT_ID, bool bUpdateBilling, bool bUpdateShipping)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL ;
				sSQL = "select *         " + ControlChars.CrLf
				     + "  from vwACCOUNTS" + ControlChars.CrLf
				     + " where ID = @ID  " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", gACCOUNT_ID);
					con.Open();
					using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
					{
						if ( rdr.Read() )
						{
							new DynamicControl(this, "ACCOUNT_ID"  ).ID   = Sql.ToGuid  (rdr["ID"  ]);
							new DynamicControl(this, "ACCOUNT_NAME").Text = Sql.ToString(rdr["NAME"]);
							if ( bUpdateBilling )
							{
								new DynamicControl(this, "PRIMARY_ADDRESS_STREET"    ).Text = Sql.ToString(rdr["BILLING_ADDRESS_STREET"     ]);
								new DynamicControl(this, "PRIMARY_ADDRESS_CITY"      ).Text = Sql.ToString(rdr["BILLING_ADDRESS_CITY"       ]);
								new DynamicControl(this, "PRIMARY_ADDRESS_STATE"     ).Text = Sql.ToString(rdr["BILLING_ADDRESS_STATE"      ]);
								new DynamicControl(this, "PRIMARY_ADDRESS_POSTALCODE").Text = Sql.ToString(rdr["BILLING_ADDRESS_POSTALCODE" ]);
								new DynamicControl(this, "PRIMARY_ADDRESS_COUNTRY"   ).Text = Sql.ToString(rdr["BILLING_ADDRESS_COUNTRY"    ]);
							}
							if ( bUpdateShipping )
							{
								new DynamicControl(this, "ALT_ADDRESS_STREET"        ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_STREET"    ]);
								new DynamicControl(this, "ALT_ADDRESS_CITY"          ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_CITY"      ]);
								new DynamicControl(this, "ALT_ADDRESS_STATE"         ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_STATE"     ]);
								new DynamicControl(this, "ALT_ADDRESS_POSTALCODE"    ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_POSTALCODE"]);
								new DynamicControl(this, "ALT_ADDRESS_COUNTRY"       ).Text = Sql.ToString(rdr["SHIPPING_ADDRESS_COUNTRY"   ]);
							}
						}
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sUID = Sql.ToString(Request["UID"]);
				if ( !IsPostBack )
				{
					string sDuplicateUID = Sql.ToString(Request["DuplicateUID"]);
					if ( !Sql.IsEmptyString(sUID) || !Sql.IsEmptyString(sDuplicateUID) )
					{
						Guid gUSER_ID = Security.USER_ID;
						Guid gTEAM_ID = Security.TEAM_ID;
						string sICLOUD_USERNAME      = String.Empty;
						string sICLOUD_PASSWORD      = String.Empty;
						string sICLOUD_CTAG_CONTACTS = String.Empty;
						string sICLOUD_CTAG_CALENDAR = String.Empty;
						iCloudSync.GetUserCredentials(Application, gUSER_ID, ref sICLOUD_USERNAME, ref sICLOUD_PASSWORD, ref sICLOUD_CTAG_CONTACTS, ref sICLOUD_CTAG_CALENDAR);
						
						IDbCommand spCONTACTS_Update = null;
						DataTable dt = iCloudSync.CreateContactsTable(Application, ref spCONTACTS_Update);
						
						ContactsService service = new ContactsService(Application);
						// 01/20/2012 Paul.  Use the SplendidCRM unique key as the iCloud MmeDeviceID. 
						service.setUserCredentials(sICLOUD_USERNAME, sICLOUD_PASSWORD, sICLOUD_CTAG_CONTACTS, sICLOUD_CTAG_CALENDAR, Sql.ToString(Application["CONFIG.unique_key"]));
						service.QueryClientLoginToken(false);
						
						ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gUSER_ID);
						ContactEntry contact = service.Get(sUID);
						
						// 01/28/2012 Paul.  The transaction is necessary so that an account can be created. 
						iCloudSync.BuildCONTACTS_Update(Application, Session, spCONTACTS_Update, null, contact, gUSER_ID, gTEAM_ID, gUSER_ID, false, null);
						DataRow row = dt.NewRow();
						row["UID"             ] = contact.UID;
						row["NAME"            ] = contact.Name.FullName;
						row["DATE_MODIFIED"   ] = contact.Updated;
						row["ASSIGNED_TO_NAME"] = Security.USER_NAME;
						row["CREATED_BY_NAME" ] = Security.USER_NAME;
						row["MODIFIED_BY_NAME"] = Security.USER_NAME;
						row["TEAM_NAME"       ] = Security.TEAM_NAME;
						row["TEAM_SET_NAME"   ] = Security.TEAM_NAME;
						foreach(IDbDataParameter par in spCONTACTS_Update.Parameters)
						{
							string sParameterName = Sql.ExtractDbName(spCONTACTS_Update, par.ParameterName).ToUpper();
							row[sParameterName] = par.Value;
						}
						dt.Rows.Add(row);
						if ( dt.Rows.Count > 0 )
						{
							DataRow rdr = dt.Rows[0];
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + ".EditAddress"    , rdr);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + ".EditDescription", rdr);
							
							string sSALUTATION = Sql.ToString(rdr["SALUTATION"]);
							// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
							ctlDynamicButtons.Title = (!Sql.IsEmptyString(sSALUTATION) ? L10n.Term(".salutation_dom." + sSALUTATION) + " " : String.Empty) + Sql.ToString(rdr["FIRST_NAME"]) + " " + Sql.ToString(rdr["LAST_NAME"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
							ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
							ViewState["ACCOUNT_ID"] = Sql.ToGuid(rdr["ACCOUNT_ID"]);
							
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , rdr);
							// 09/02/2012 Paul.  EditViews were combined into a single view. 
							//this.AppendEditViewFields(m_sMODULE + ".EditAddress"    , tblAddress    , rdr);
							//this.AppendEditViewFields(m_sMODULE + ".EditDescription", tblDescription, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Sql.ToGuid(rdr["ASSIGNED_USER_ID"]), rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Sql.ToGuid(rdr["ASSIGNED_USER_ID"]), rdr);
							TextBox txtFIRST_NAME = this.FindControl("FIRST_NAME") as TextBox;
							if ( txtFIRST_NAME != null )
								txtFIRST_NAME.Focus();
							ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
							
							ViewState ["NAME"            ] = (!Sql.IsEmptyString(sSALUTATION) ? L10n.Term(".salutation_dom." + sSALUTATION) + " " : String.Empty) + Sql.ToString(rdr["FIRST_NAME"]) + " " + Sql.ToString(rdr["LAST_NAME"]);
							ViewState ["ASSIGNED_USER_ID"] = Sql.ToGuid  (rdr["ASSIGNED_USER_ID"]);
							Page.Items["NAME"            ] = ViewState ["NAME"            ];
							Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];

							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + ".EditAddress"    , rdr);
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + ".EditDescription", rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain     , null);
						// 09/02/2012 Paul.  EditViews were combined into a single view. 
						//this.AppendEditViewFields(m_sMODULE + ".EditAddress"    , tblAddress    , null);
						//this.AppendEditViewFields(m_sMODULE + ".EditDescription", tblDescription, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtFIRST_NAME = this.FindControl("FIRST_NAME") as TextBox;
						if ( txtFIRST_NAME != null )
							txtFIRST_NAME.Focus();
					}
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];

					DynamicControl ctlACCOUNT_ID  = new DynamicControl(this, "ACCOUNT_ID" );
					if ( Sql.ToGuid(ViewState["ACCOUNT_ID"]) != ctlACCOUNT_ID.ID )
					{
						UpdateAccount(ctlACCOUNT_ID.ID, true, true);
						ViewState["ACCOUNT_ID" ] = ctlACCOUNT_ID.ID;
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Contacts";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.IsEmptyString(Request["UID"]);
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				// 09/02/2012 Paul.  EditViews were combined into a single view. 
				//this.AppendEditViewFields(m_sMODULE + ".EditAddress"    , tblAddress    , null);
				//this.AppendEditViewFields(m_sMODULE + ".EditDescription", tblDescription, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

