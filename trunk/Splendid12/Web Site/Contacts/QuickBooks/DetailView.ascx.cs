/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Contacts.QuickBooks
{
	/// <summary>
	/// Summary description for DetailView.
	/// </summary>
	public class DetailView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons ctlDynamicButtons;

		protected string      sQID             ;
		protected HtmlTable   tblMain          ;
		protected PlaceHolder plcSubPanel;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Edit" )
				{
					Response.Redirect("edit.aspx?QID=" + sQID);
				}
				else if ( e.CommandName == "Duplicate" )
				{
					Response.Redirect("edit.aspx?DuplicateQID=" +sQID);
				}
				else if ( e.CommandName == "Delete" )
				{
					try
					{
						// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
						if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
						{
							Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
							
							quickbooks.CustomerOperations.Delete(sQID);
						}
						else
						{
							QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								string sSQL;
								sSQL = "delete from Customers" + ControlChars.CrLf
								     + " where ID = @ID      " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@ID", sQID);
									cmd.ExecuteNonQuery();
								}
							}
						}
						Response.Redirect("default.aspx");
					}
					catch(Exception ex)
					{
						ctlDynamicButtons.ErrorText = ex.Message;
					}
				}
				else if ( e.CommandName == "Cancel" )
				{
					Response.Redirect("default.aspx");
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.USER_ID == Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]));
			if ( !this.Visible )
				return;

			try
			{
				sQID = Sql.ToString(Request["QID"]);
				if ( !IsPostBack )
				{
					if ( !Sql.IsEmptyString(sQID) )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
#if DEBUG
						//bQuickBooksEnabled = true;
#endif
						if ( bQuickBooksEnabled )
						{
							// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
							if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
							{
								Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
								
								DataRow rdr = Spring.Social.QuickBooks.Api.Customer.ConvertToRow(quickbooks.CustomerOperations.GetById(sQID));
								//this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
								
								// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
								ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
								SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
								
								this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
								ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, rdr);
								
								//this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
							}
							else
							{
								QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL ;
									sSQL = "select *        " + ControlChars.CrLf
									     + "  from Customers" + ControlChars.CrLf
									     + " where ID = @ID " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", sQID);
										using ( DbDataAdapter da = dbf.CreateDataAdapter() )
										{
											((IDbDataAdapter)da).SelectCommand = cmd;
											using ( DataTable dtCurrent = new DataTable() )
											{
												da.Fill(dtCurrent);
												if ( dtCurrent.Rows.Count > 0 )
												{
													DataRow rdr = dtCurrent.Rows[0];
													//this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
												
													ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
													SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
												
													this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, rdr);
													
													//this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
												}
												else
												{
													plcSubPanel.Visible = false;
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
													ctlDynamicButtons.DisableAll();
													ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
						ctlDynamicButtons.DisableAll();
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Contacts";
			SetMenu(m_sMODULE);
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			// 02/06/2014 Paul.  Go back to a single QuickBooks UI. 
			// 02/13/2015 Paul.  New QuickBooks Online code uses a different schema. 
			this.LayoutDetailView = "DetailView.QuickBooks" + (QuickBooksSync.IsOnlineAppMode(Application) ? "Online" : String.Empty);
			this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
			if ( IsPostBack )
			{
				this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

