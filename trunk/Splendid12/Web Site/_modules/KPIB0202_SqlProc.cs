﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIB0202_SqlProc
    {

        #region spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete(Guid gB_KPI_ALLOCATE_DETAIL_ID, Guid gACCOUNT_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete";
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", gB_KPI_ALLOCATE_DETAIL_ID);
                            IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete(Guid gB_KPI_ALLOCATE_DETAIL_ID, Guid gACCOUNT_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete";
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", gB_KPI_ALLOCATE_DETAIL_ID);
                IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Delete";
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.CreateParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", "Guid", 16);
            IDbDataParameter parACCOUNT_ID = Sql.CreateParameter(cmd, "@ACCOUNT_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update(Guid gB_KPI_ALLOCATE_DETAIL_ID, Guid gACCOUNT_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update";
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", gB_KPI_ALLOCATE_DETAIL_ID);
                            IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update(Guid gB_KPI_ALLOCATE_DETAIL_ID, Guid gACCOUNT_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update";
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", gB_KPI_ALLOCATE_DETAIL_ID);
                IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOU";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_ACCOUNTS_Update";
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parB_KPI_ALLOCATE_DETAIL_ID = Sql.CreateParameter(cmd, "@B_KPI_ALLOCATE_DETAIL_ID", "Guid", 16);
            IDbDataParameter parACCOUNT_ID = Sql.CreateParameter(cmd, "@ACCOUNT_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delet";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delet";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delet";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassD";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassD";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassD";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassU";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassU";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassU";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undel";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undel";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undel";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_NAME, Guid gKPI_ALLOCATE_ID, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, Guid gKPI_STANDARD_DETAIL_ID, string sDESCRIPTION, string sREMARK, decimal dMONTH_1, decimal dMONTH_2, decimal dMONTH_3, decimal dMONTH_4, decimal dMONTH_5, decimal dMONTH_6, decimal dMONTH_7, decimal dMONTH_8, decimal dMONTH_9, decimal dMONTH_10, decimal dMONTH_11, decimal dMONTH_12, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Updat";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                            IDbDataParameter parKPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@KPI_ALLOCATE_ID", gKPI_ALLOCATE_ID);
                            IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                            IDbDataParameter parRADIO = Sql.AddParameter(cmd, "@RADIO", flRADIO);
                            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                            IDbDataParameter parKPI_STANDARD_DETAIL_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_DETAIL_ID", gKPI_STANDARD_DETAIL_ID);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            IDbDataParameter parMONTH_1 = Sql.AddParameter(cmd, "@MONTH_1", dMONTH_1);
                            IDbDataParameter parMONTH_2 = Sql.AddParameter(cmd, "@MONTH_2", dMONTH_2);
                            IDbDataParameter parMONTH_3 = Sql.AddParameter(cmd, "@MONTH_3", dMONTH_3);
                            IDbDataParameter parMONTH_4 = Sql.AddParameter(cmd, "@MONTH_4", dMONTH_4);
                            IDbDataParameter parMONTH_5 = Sql.AddParameter(cmd, "@MONTH_5", dMONTH_5);
                            IDbDataParameter parMONTH_6 = Sql.AddParameter(cmd, "@MONTH_6", dMONTH_6);
                            IDbDataParameter parMONTH_7 = Sql.AddParameter(cmd, "@MONTH_7", dMONTH_7);
                            IDbDataParameter parMONTH_8 = Sql.AddParameter(cmd, "@MONTH_8", dMONTH_8);
                            IDbDataParameter parMONTH_9 = Sql.AddParameter(cmd, "@MONTH_9", dMONTH_9);
                            IDbDataParameter parMONTH_10 = Sql.AddParameter(cmd, "@MONTH_10", dMONTH_10);
                            IDbDataParameter parMONTH_11 = Sql.AddParameter(cmd, "@MONTH_11", dMONTH_11);
                            IDbDataParameter parMONTH_12 = Sql.AddParameter(cmd, "@MONTH_12", dMONTH_12);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATE_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_NAME, Guid gKPI_ALLOCATE_ID, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, Guid gKPI_STANDARD_DETAIL_ID, string sDESCRIPTION, string sREMARK, decimal dMONTH_1, decimal dMONTH_2, decimal dMONTH_3, decimal dMONTH_4, decimal dMONTH_5, decimal dMONTH_6, decimal dMONTH_7, decimal dMONTH_8, decimal dMONTH_9, decimal dMONTH_10, decimal dMONTH_11, decimal dMONTH_12, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Updat";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parKPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@KPI_ALLOCATE_ID", gKPI_ALLOCATE_ID);
                IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                IDbDataParameter parRADIO = Sql.AddParameter(cmd, "@RADIO", flRADIO);
                IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                IDbDataParameter parKPI_STANDARD_DETAIL_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_DETAIL_ID", gKPI_STANDARD_DETAIL_ID);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parMONTH_1 = Sql.AddParameter(cmd, "@MONTH_1", dMONTH_1);
                IDbDataParameter parMONTH_2 = Sql.AddParameter(cmd, "@MONTH_2", dMONTH_2);
                IDbDataParameter parMONTH_3 = Sql.AddParameter(cmd, "@MONTH_3", dMONTH_3);
                IDbDataParameter parMONTH_4 = Sql.AddParameter(cmd, "@MONTH_4", dMONTH_4);
                IDbDataParameter parMONTH_5 = Sql.AddParameter(cmd, "@MONTH_5", dMONTH_5);
                IDbDataParameter parMONTH_6 = Sql.AddParameter(cmd, "@MONTH_6", dMONTH_6);
                IDbDataParameter parMONTH_7 = Sql.AddParameter(cmd, "@MONTH_7", dMONTH_7);
                IDbDataParameter parMONTH_8 = Sql.AddParameter(cmd, "@MONTH_8", dMONTH_8);
                IDbDataParameter parMONTH_9 = Sql.AddParameter(cmd, "@MONTH_9", dMONTH_9);
                IDbDataParameter parMONTH_10 = Sql.AddParameter(cmd, "@MONTH_10", dMONTH_10);
                IDbDataParameter parMONTH_11 = Sql.AddParameter(cmd, "@MONTH_11", dMONTH_11);
                IDbDataParameter parMONTH_12 = Sql.AddParameter(cmd, "@MONTH_12", dMONTH_12);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ALLOCATE_DETAILS_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATE_DETAILS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Updat";
            else
                cmd.CommandText = "spB_KPI_ALLOCATE_DETAILS_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parKPI_NAME = Sql.CreateParameter(cmd, "@KPI_NAME", "string", 200);
            IDbDataParameter parKPI_ALLOCATE_ID = Sql.CreateParameter(cmd, "@KPI_ALLOCATE_ID", "Guid", 16);
            IDbDataParameter parUNIT = Sql.CreateParameter(cmd, "@UNIT", "Int32", 4);
            IDbDataParameter parRADIO = Sql.CreateParameter(cmd, "@RADIO", "float", 8);
            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parKPI_STANDARD_DETAIL_ID = Sql.CreateParameter(cmd, "@KPI_STANDARD_DETAIL_ID", "Guid", 16);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            IDbDataParameter parMONTH_1 = Sql.CreateParameter(cmd, "@MONTH_1", "decimal", 8);
            IDbDataParameter parMONTH_2 = Sql.CreateParameter(cmd, "@MONTH_2", "decimal", 8);
            IDbDataParameter parMONTH_3 = Sql.CreateParameter(cmd, "@MONTH_3", "decimal", 8);
            IDbDataParameter parMONTH_4 = Sql.CreateParameter(cmd, "@MONTH_4", "decimal", 8);
            IDbDataParameter parMONTH_5 = Sql.CreateParameter(cmd, "@MONTH_5", "decimal", 8);
            IDbDataParameter parMONTH_6 = Sql.CreateParameter(cmd, "@MONTH_6", "decimal", 8);
            IDbDataParameter parMONTH_7 = Sql.CreateParameter(cmd, "@MONTH_7", "decimal", 8);
            IDbDataParameter parMONTH_8 = Sql.CreateParameter(cmd, "@MONTH_8", "decimal", 8);
            IDbDataParameter parMONTH_9 = Sql.CreateParameter(cmd, "@MONTH_9", "decimal", 8);
            IDbDataParameter parMONTH_10 = Sql.CreateParameter(cmd, "@MONTH_10", "decimal", 8);
            IDbDataParameter parMONTH_11 = Sql.CreateParameter(cmd, "@MONTH_11", "decimal", 8);
            IDbDataParameter parMONTH_12 = Sql.CreateParameter(cmd, "@MONTH_12", "decimal", 8);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATES_ACCOUNTS_Delete(Guid gB_KPI_ALLOCATE_ID, Guid gACCOUNT_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Del";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Delete";
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ID", gB_KPI_ALLOCATE_ID);
                            IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATES_ACCOUNTS_Delete(Guid gB_KPI_ALLOCATE_ID, Guid gACCOUNT_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Del";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Delete";
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ID", gB_KPI_ALLOCATE_ID);
                IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_ACCOUNTS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Del";
            else
                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Delete";
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.CreateParameter(cmd, "@B_KPI_ALLOCATE_ID", "Guid", 16);
            IDbDataParameter parACCOUNT_ID = Sql.CreateParameter(cmd, "@ACCOUNT_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATES_ACCOUNTS_Update(Guid gB_KPI_ALLOCATE_ID, Guid gACCOUNT_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Upd";
                            else
                                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Update";
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ID", gB_KPI_ALLOCATE_ID);
                            IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Update
        /// </summary>
        public static void spB_KPI_ALLOCATES_ACCOUNTS_Update(Guid gB_KPI_ALLOCATE_ID, Guid gACCOUNT_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Upd";
                else
                    cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Update";
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@B_KPI_ALLOCATE_ID", gB_KPI_ALLOCATE_ID);
                IDbDataParameter parACCOUNT_ID = Sql.AddParameter(cmd, "@ACCOUNT_ID", gACCOUNT_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_ACCOUNTS_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_ACCOUNTS_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_ACCOUNTS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Upd";
            else
                cmd.CommandText = "spB_KPI_ALLOCATES_ACCOUNTS_Update";
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parB_KPI_ALLOCATE_ID = Sql.CreateParameter(cmd, "@B_KPI_ALLOCATE_ID", "Guid", 16);
            IDbDataParameter parACCOUNT_ID = Sql.CreateParameter(cmd, "@ACCOUNT_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATES_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_Delete
        /// </summary>
        public static void spB_KPI_ALLOCATES_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ALLOCATES_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATES_MassDelete
        /// </summary>
        public static void spB_KPI_ALLOCATES_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATES_MassDelete
        /// </summary>
        public static void spB_KPI_ALLOCATES_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ALLOCATES_MassDelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATES_MassUpdate
        /// </summary>
        public static void spB_KPI_ALLOCATES_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATES_MassUpdate
        /// </summary>
        public static void spB_KPI_ALLOCATES_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ALLOCATES_MassUpdate
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ALLOCATES_Merge
        /// </summary>
        public static void spB_KPI_ALLOCATES_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ALLOCATES_Merge
        /// </summary>
        public static void spB_KPI_ALLOCATES_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ALLOCATES_Merge
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATES_Undelete
        /// </summary>
        public static void spB_KPI_ALLOCATES_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATES_Undelete
        /// </summary>
        public static void spB_KPI_ALLOCATES_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ALLOCATES_Undelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_Update
        /// </summary>
        public static void spB_KPI_ALLOCATES_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sALLOCATE_NAME, string sALLOCATE_CODE, Int32 nYEAR, string sPERIOD, string sVERSION_NUMBER, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sALLOCATE_TYPE, Guid gORGANIZATION_ID, Guid gEMPLOYEE_ID, string sSTATUS, Guid gKPI_STANDARD_ID, Guid gFILE_ID, Guid gASSIGN_BY, DateTime dtASSIGN_DATE, Int32 nSTAFT_NUMBER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ALLOCATES_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parALLOCATE_NAME = Sql.AddParameter(cmd, "@ALLOCATE_NAME", sALLOCATE_NAME, 200);
                            IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parPERIOD = Sql.AddParameter(cmd, "@PERIOD", sPERIOD, 5);
                            IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 50);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                            IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                            IDbDataParameter parALLOCATE_TYPE = Sql.AddParameter(cmd, "@ALLOCATE_TYPE", sALLOCATE_TYPE, 5);
                            IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                            IDbDataParameter parEMPLOYEE_ID = Sql.AddParameter(cmd, "@EMPLOYEE_ID", gEMPLOYEE_ID);
                            IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                            IDbDataParameter parKPI_STANDARD_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_ID", gKPI_STANDARD_ID);
                            IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", gFILE_ID);
                            IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", gASSIGN_BY);
                            IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                            IDbDataParameter parSTAFT_NUMBER = Sql.AddParameter(cmd, "@STAFT_NUMBER", nSTAFT_NUMBER);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_Update
        /// </summary>
        public static void spB_KPI_ALLOCATES_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sALLOCATE_NAME, string sALLOCATE_CODE, Int32 nYEAR, string sPERIOD, string sVERSION_NUMBER, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sALLOCATE_TYPE, Guid gORGANIZATION_ID, Guid gEMPLOYEE_ID, string sSTATUS, Guid gKPI_STANDARD_ID, Guid gFILE_ID, Guid gASSIGN_BY, DateTime dtASSIGN_DATE, Int32 nSTAFT_NUMBER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ALLOCATES_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parALLOCATE_NAME = Sql.AddParameter(cmd, "@ALLOCATE_NAME", sALLOCATE_NAME, 200);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parPERIOD = Sql.AddParameter(cmd, "@PERIOD", sPERIOD, 5);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 50);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parALLOCATE_TYPE = Sql.AddParameter(cmd, "@ALLOCATE_TYPE", sALLOCATE_TYPE, 5);
                IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                IDbDataParameter parEMPLOYEE_ID = Sql.AddParameter(cmd, "@EMPLOYEE_ID", gEMPLOYEE_ID);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                IDbDataParameter parKPI_STANDARD_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_ID", gKPI_STANDARD_ID);
                IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", gFILE_ID);
                IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", gASSIGN_BY);
                IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                IDbDataParameter parSTAFT_NUMBER = Sql.AddParameter(cmd, "@STAFT_NUMBER", nSTAFT_NUMBER);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ALLOCATES_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ALLOCATES_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ALLOCATES_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parALLOCATE_NAME = Sql.CreateParameter(cmd, "@ALLOCATE_NAME", "string", 200);
            IDbDataParameter parALLOCATE_CODE = Sql.CreateParameter(cmd, "@ALLOCATE_CODE", "string", 50);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parPERIOD = Sql.CreateParameter(cmd, "@PERIOD", "string", 5);
            IDbDataParameter parVERSION_NUMBER = Sql.CreateParameter(cmd, "@VERSION_NUMBER", "string", 50);
            IDbDataParameter parAPPROVE_STATUS = Sql.CreateParameter(cmd, "@APPROVE_STATUS", "string", 5);
            IDbDataParameter parAPPROVED_BY = Sql.CreateParameter(cmd, "@APPROVED_BY", "Guid", 16);
            IDbDataParameter parAPPROVED_DATE = Sql.CreateParameter(cmd, "@APPROVED_DATE", "DateTime", 8);
            IDbDataParameter parALLOCATE_TYPE = Sql.CreateParameter(cmd, "@ALLOCATE_TYPE", "string", 5);
            IDbDataParameter parORGANIZATION_ID = Sql.CreateParameter(cmd, "@ORGANIZATION_ID", "Guid", 16);
            IDbDataParameter parEMPLOYEE_ID = Sql.CreateParameter(cmd, "@EMPLOYEE_ID", "Guid", 16);
            IDbDataParameter parSTATUS = Sql.CreateParameter(cmd, "@STATUS", "string", 5);
            IDbDataParameter parKPI_STANDARD_ID = Sql.CreateParameter(cmd, "@KPI_STANDARD_ID", "Guid", 16);
            IDbDataParameter parFILE_ID = Sql.CreateParameter(cmd, "@FILE_ID", "Guid", 16);
            IDbDataParameter parASSIGN_BY = Sql.CreateParameter(cmd, "@ASSIGN_BY", "Guid", 16);
            IDbDataParameter parASSIGN_DATE = Sql.CreateParameter(cmd, "@ASSIGN_DATE", "DateTime", 8);
            IDbDataParameter parSTAFT_NUMBER = Sql.CreateParameter(cmd, "@STAFT_NUMBER", "Int32", 4);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion
    }
}