﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;
namespace SplendidCRM._modules
{
    public class KPIB030301_SQLProc
    {
        public static void spB_KPI_TOI_ACT_RESULT_DETAIL_Update_Final_Value(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, decimal flFINAL_VALUE, string sDES, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_TOI_ACT_RESULT_DETAIL_Upda";
                else
                    cmd.CommandText = "spB_KPI_TOI_ACT_RESULT_DETAIL_Update_FINAL_VALUE";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                IDbDataParameter parDES = Sql.AddParameter(cmd, "@DES", sDES);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }

        //Cap nhat ket qua thu thuan the
        public static void spB_KPI_TOI_NETCARD_RESULT_TMP_Update(ref Guid gID, 
            IDbTransaction trn,
            int iYEAR,
            string MONTH_PERIOD,
            decimal U_VS_DB_PROFIT_VND,
            decimal U_VS_DB_PROFIT_USD,
            decimal U_VS_DB_FIXED_COST,
            decimal U_VS_DB_EXP_COST,
            decimal U_VS_DB_COST_VND,
            decimal U_VS_DB_COST_USD,
            decimal U_MC_CREDIT_COST,
            decimal U_VS_CREDIT_PROFIT_VND,
            decimal U_VS_CREDIT_PROFIT_USD,
            decimal U_VS_CREDIT_FIXED_COST,
            decimal U_VS_CREDIT_EXP_COST,
            decimal U_VS_CREDIT_COST_VND,
            decimal U_VS_CREDIT_COST_USD,
            decimal U_MC_CREDIT_FEE,
            decimal U_CREDIT_FTP_COST
        )
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_TOI_NETCARD_RESULT_TMP_Update";

                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);

                IDbDataParameter pariYEAR = Sql.AddParameter(cmd, "@iYEAR", iYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", MONTH_PERIOD);
                IDbDataParameter parU_VS_DB_PROFIT_VND = Sql.AddParameter(cmd, "@U_VS_DB_PROFIT_VND", U_VS_DB_PROFIT_VND);
                IDbDataParameter parU_VS_DB_PROFIT_USD = Sql.AddParameter(cmd, "@U_VS_DB_PROFIT_USD", U_VS_DB_PROFIT_USD);
                IDbDataParameter parU_VS_DB_FIXED_COST = Sql.AddParameter(cmd, "@U_VS_DB_FIXED_COST", U_VS_DB_FIXED_COST);
                IDbDataParameter parU_VS_DB_EXP_COST = Sql.AddParameter(cmd, "@U_VS_DB_EXP_COST", U_VS_DB_EXP_COST);
                IDbDataParameter parU_VS_DB_COST_USD = Sql.AddParameter(cmd, "@U_VS_DB_COST_USD", U_VS_DB_COST_USD);
                IDbDataParameter parU_VS_DB_COST_VND = Sql.AddParameter(cmd, "@U_VS_DB_COST_VND", U_VS_DB_COST_VND);
                IDbDataParameter parU_MC_CREDIT_COST = Sql.AddParameter(cmd, "@U_MC_CREDIT_COST", U_MC_CREDIT_COST);
                IDbDataParameter parU_VS_CREDIT_PROFIT_VND = Sql.AddParameter(cmd, "@U_VS_CREDIT_PROFIT_VND", U_VS_CREDIT_PROFIT_VND);
                IDbDataParameter parU_VS_CREDIT_PROFIT_USD = Sql.AddParameter(cmd, "@U_VS_CREDIT_PROFIT_USD", U_VS_CREDIT_PROFIT_USD);
                IDbDataParameter parU_VS_CREDIT_FIXED_COST = Sql.AddParameter(cmd, "@U_VS_CREDIT_FIXED_COST", U_VS_CREDIT_FIXED_COST);
                IDbDataParameter parU_VS_CREDIT_EXP_COST = Sql.AddParameter(cmd, "@U_VS_CREDIT_EXP_COST", U_VS_CREDIT_EXP_COST);
                IDbDataParameter parU_VS_CREDIT_COST_VND = Sql.AddParameter(cmd, "@U_VS_CREDIT_COST_VND", U_VS_CREDIT_COST_VND);
                IDbDataParameter parU_VS_CREDIT_COST_USD = Sql.AddParameter(cmd, "@U_VS_CREDIT_COST_USD", U_VS_CREDIT_COST_USD);
                IDbDataParameter parU_MC_CREDIT_FEE = Sql.AddParameter(cmd, "@U_MC_CREDIT_FEE", U_MC_CREDIT_FEE);
                IDbDataParameter parU_CREDIT_FTP_COST = Sql.AddParameter(cmd, "@U_CREDIT_FTP_COST", U_CREDIT_FTP_COST);


                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }


        //Cap nhat ket qua thu thuan the
        public static void spCAL_B_KPI_TOI_NETCARD_RESULT(
            IDbTransaction trn,
            int iYEAR,
            string MONTH_PERIOD
        )
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spCAL_B_KPI_TOI_NETCARD_RESULT";

                IDbDataParameter pariYEAR = Sql.AddParameter(cmd, "@iYEAR", iYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", MONTH_PERIOD);

                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }

        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_TOI_ACTUAL_RESULT_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_TOI_ACTUAL_RESULT_Mass";
                            else
                                cmd.CommandText = "spB_KPI_TOI_ACTUAL_RESULT_MassApprove";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_TOI_ACTUAL_RESULT_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_TOI_ACTUAL_RESULT_Mass";
                else
                    cmd.CommandText = "spB_KPI_TOI_ACTUAL_RESULT_MassApprove";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion       

    }
}