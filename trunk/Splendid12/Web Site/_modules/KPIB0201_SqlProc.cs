﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;

namespace SplendidCRM
{
    public class KPIB0201_SqlProc
    {
        #region spB_KPI_STANDARD_Delete
        /// 

        /// spB_KPI_STANDARD_Delete
        /// 

        public static void spB_KPI_STANDARD_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_Delete
        /// 

        /// spB_KPI_STANDARD_Delete
        /// 

        public static void spB_KPI_STANDARD_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_Delete
        /// 

        /// spB_KPI_STANDARD_Delete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Delete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Delete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delet";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Delete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Delete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delet";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_Delete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Delete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delet";
            else
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassD";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassD";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassDelete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassD";
            else
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        public static void spB_KPI_STANDARD_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassU";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        public static void spB_KPI_STANDARD_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassU";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        /// spB_KPI_STANDARD_DETAILS_MassUpdate
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassU";
            else
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Merge
        /// 

        /// spB_KPI_STANDARD_DETAILS_Merge
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Merge
        /// 

        /// spB_KPI_STANDARD_DETAILS_Merge
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_Merge
        /// 

        /// spB_KPI_STANDARD_DETAILS_Merge
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Undelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Undelete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undel";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Undelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Undelete
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undel";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_Undelete
        /// 

        /// spB_KPI_STANDARD_DETAILS_Undelete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undel";
            else
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Update
        /// 

        /// spB_KPI_STANDARD_DETAILS_Update
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_NAME, Guid gKPI_STANDARD_ID, Guid gKPI_ID, string sKPI_CODE, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRATIO, float flMAX_RATIO_COMPLETE, string sGROUP_KPI_DETAIL_ID, string sDESCRIPTION, string sREMARK, double flVALUE_STD_PER_MONTH, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Updat";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                            IDbDataParameter parKPI_STANDARD_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_ID", gKPI_STANDARD_ID);
                            IDbDataParameter parKPI_ID = Sql.AddParameter(cmd, "@KPI_ID", gKPI_ID);
                            IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE);
                            IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nUNIT);
                            IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                            IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                            IDbDataParameter parGROUP_KPI_DETAIL_ID = Sql.AddParameter(cmd, "@GROUP_KPI_DETAIL_ID", sGROUP_KPI_DETAIL_ID, 50);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            IDbDataParameter parVALUE_STD_PER_MONTH = Sql.AddParameter(cmd, "@VALUE_STD_PER_MONTH", flVALUE_STD_PER_MONTH);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_DETAILS_Update
        /// 

        /// spB_KPI_STANDARD_DETAILS_Update
        /// 

        public static void spB_KPI_STANDARD_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_NAME, Guid gKPI_STANDARD_ID, Guid gKPI_ID, string sKPI_CODE, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRATIO, float flMAX_RATIO_COMPLETE, string sGROUP_KPI_DETAIL_ID, string sDESCRIPTION, string sREMARK, double flVALUE_STD_PER_MONTH, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Updat";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parKPI_STANDARD_ID = Sql.AddParameter(cmd, "@KPI_STANDARD_ID", gKPI_STANDARD_ID);
                IDbDataParameter parKPI_ID = Sql.AddParameter(cmd, "@KPI_ID", gKPI_ID);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE);
                IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nUNIT);
                IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                IDbDataParameter parGROUP_KPI_DETAIL_ID = Sql.AddParameter(cmd, "@GROUP_KPI_DETAIL_ID", sGROUP_KPI_DETAIL_ID, 50);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parVALUE_STD_PER_MONTH = Sql.AddParameter(cmd, "@VALUE_STD_PER_MONTH", flVALUE_STD_PER_MONTH);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;

                //cmd.Parameters.Add(new SqlParameter("@VALUE_STD_PER_MONTH", flVALUE_STD_PER_MONTH));

                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_DETAILS_Update
        /// 

        /// spB_KPI_STANDARD_DETAILS_Update
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_DETAILS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Updat";
            else
                cmd.CommandText = "spB_KPI_STANDARD_DETAILS_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parKPI_NAME = Sql.CreateParameter(cmd, "@KPI_NAME", "string", 200);
            IDbDataParameter parKPI_STANDARD_ID = Sql.CreateParameter(cmd, "@KPI_STANDARD_ID", "Guid", 16);
            IDbDataParameter parKPI_ID = Sql.CreateParameter(cmd, "@KPI_ID", "Guid", 16);
            IDbDataParameter parKPI_CODE = Sql.CreateParameter(cmd, "@KPI_CODE", "string", 50);
            IDbDataParameter parLEVEL_NUMBER = Sql.CreateParameter(cmd, "@LEVEL_NUMBER", "Int32", 4);
            IDbDataParameter parUNIT = Sql.CreateParameter(cmd, "@KPI_UNIT", "Int32", 4);
            IDbDataParameter parRATIO = Sql.CreateParameter(cmd, "@RATIO", "float", 8);
            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parGROUP_KPI_DETAIL_ID = Sql.CreateParameter(cmd, "@GROUP_KPI_DETAIL_ID", "string", 50);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            IDbDataParameter parVALUE_STD_PER_MONTH = Sql.CreateParameter(cmd, "@VALUE_STD_PER_MONTH", "float", 8);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_MassDelete
        /// 

        /// spB_KPI_STANDARD_MassDelete
        /// 

        public static void spB_KPI_STANDARD_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_MassDelete
        /// 

        /// spB_KPI_STANDARD_MassDelete
        /// 

        public static void spB_KPI_STANDARD_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_MassDelete
        /// 

        /// spB_KPI_STANDARD_MassDelete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_MassUpdate
        /// 

        /// spB_KPI_STANDARD_MassUpdate
        /// 

        public static void spB_KPI_STANDARD_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_MassUpdate
        /// 

        /// spB_KPI_STANDARD_MassUpdate
        /// 

        public static void spB_KPI_STANDARD_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_MassUpdate
        /// 

        /// spB_KPI_STANDARD_MassUpdate
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_Merge
        /// 

        /// spB_KPI_STANDARD_Merge
        /// 

        public static void spB_KPI_STANDARD_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_Merge
        /// 

        /// spB_KPI_STANDARD_Merge
        /// 

        public static void spB_KPI_STANDARD_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_Merge
        /// 

        /// spB_KPI_STANDARD_Merge
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_Undelete
        /// 

        /// spB_KPI_STANDARD_Undelete
        /// 

        public static void spB_KPI_STANDARD_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_Undelete
        /// 

        /// spB_KPI_STANDARD_Undelete
        /// 

        public static void spB_KPI_STANDARD_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_Undelete
        /// 

        /// spB_KPI_STANDARD_Undelete
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_STANDARD_Update
        /// 

        /// spB_KPI_STANDARD_Update
        /// 

        public static void spB_KPI_STANDARD_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_STANDARD_NAME, string sKPI_STANDARD_CODE, Int32 nYEAR, string sVERSION_NUMBER, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sSENIORITY_ID, Int32 nTYPE, Guid gPOSITION_ID, string sBUSINESS_CODE, Guid gORGANIZATION_ID, string sORGANIZATION_CODE, string sCONTRACT_TYPE, string sIS_BASE, Guid gBASE_ID, string sDESCRIPTION, string sREMARK, string sSTATUS, Guid gGROUP_KPI_ID, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_STANDARD_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parKPI_STANDARD_NAME = Sql.AddParameter(cmd, "@KPI_STANDARD_NAME", sKPI_STANDARD_NAME, 200);
                            IDbDataParameter parKPI_STANDARD_CODE = Sql.AddParameter(cmd, "@KPI_STANDARD_CODE", sKPI_STANDARD_CODE, 50);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 50);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 2);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                            IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                            IDbDataParameter parSENIORITY_ID = Sql.AddParameter(cmd, "@SENIORITY_ID", sSENIORITY_ID, 50);
                            IDbDataParameter parTYPE = Sql.AddParameter(cmd, "@TYPE", nTYPE);
                            IDbDataParameter parPOSITION_ID = Sql.AddParameter(cmd, "@POSITION_ID", gPOSITION_ID);
                            IDbDataParameter parBUSINESS_CODE = Sql.AddParameter(cmd, "@BUSINESS_CODE", sBUSINESS_CODE, 50);
                            IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                            IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                            IDbDataParameter parCONTRACT_TYPE = Sql.AddParameter(cmd, "@CONTRACT_TYPE", sCONTRACT_TYPE, 5);
                            IDbDataParameter parIS_BASE = Sql.AddParameter(cmd, "@IS_BASE", sIS_BASE, 1);
                            IDbDataParameter parBASE_ID = Sql.AddParameter(cmd, "@BASE_ID", gBASE_ID);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                            IDbDataParameter parGROUP_KPI_ID = Sql.AddParameter(cmd, "@GROUP_KPI_ID", gGROUP_KPI_ID);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_STANDARD_Update
        /// 

        /// spB_KPI_STANDARD_Update
        /// 

        public static void spB_KPI_STANDARD_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sKPI_STANDARD_NAME, string sKPI_STANDARD_CODE, Int32 nYEAR, string sVERSION_NUMBER, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sSENIORITY_ID, Int32 nTYPE, Guid gPOSITION_ID, string sBUSINESS_CODE, Guid gORGANIZATION_ID, string sORGANIZATION_CODE, string sCONTRACT_TYPE, string sIS_BASE, Guid gBASE_ID, string sDESCRIPTION, string sREMARK, string sSTATUS, Guid gGROUP_KPI_ID, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_STANDARD_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parKPI_STANDARD_NAME = Sql.AddParameter(cmd, "@KPI_STANDARD_NAME", sKPI_STANDARD_NAME, 200);
                IDbDataParameter parKPI_STANDARD_CODE = Sql.AddParameter(cmd, "@KPI_STANDARD_CODE", sKPI_STANDARD_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 50);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 2);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parSENIORITY_ID = Sql.AddParameter(cmd, "@SENIORITY_ID", sSENIORITY_ID, 50);
                IDbDataParameter parTYPE = Sql.AddParameter(cmd, "@TYPE", nTYPE);
                IDbDataParameter parPOSITION_ID = Sql.AddParameter(cmd, "@POSITION_ID", gPOSITION_ID);
                IDbDataParameter parBUSINESS_CODE = Sql.AddParameter(cmd, "@BUSINESS_CODE", sBUSINESS_CODE, 50);
                IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parCONTRACT_TYPE = Sql.AddParameter(cmd, "@CONTRACT_TYPE", sCONTRACT_TYPE, 5);
                IDbDataParameter parIS_BASE = Sql.AddParameter(cmd, "@IS_BASE", sIS_BASE, 1);
                IDbDataParameter parBASE_ID = Sql.AddParameter(cmd, "@BASE_ID", gBASE_ID);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                IDbDataParameter parGROUP_KPI_ID = Sql.AddParameter(cmd, "@GROUP_KPI_ID", gGROUP_KPI_ID);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_STANDARD_Update
        /// 

        /// spB_KPI_STANDARD_Update
        /// 

        public static IDbCommand cmdB_KPI_STANDARD_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_STANDARD_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parKPI_STANDARD_NAME = Sql.CreateParameter(cmd, "@KPI_STANDARD_NAME", "string", 200);
            IDbDataParameter parKPI_STANDARD_CODE = Sql.CreateParameter(cmd, "@KPI_STANDARD_CODE", "string", 50);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parVERSION_NUMBER = Sql.CreateParameter(cmd, "@VERSION_NUMBER", "string", 50);
            IDbDataParameter parAPPROVE_STATUS = Sql.CreateParameter(cmd, "@APPROVE_STATUS", "string", 2);
            IDbDataParameter parAPPROVED_BY = Sql.CreateParameter(cmd, "@APPROVED_BY", "Guid", 16);
            IDbDataParameter parAPPROVED_DATE = Sql.CreateParameter(cmd, "@APPROVED_DATE", "DateTime", 8);
            IDbDataParameter parSENIORITY_ID = Sql.CreateParameter(cmd, "@SENIORITY_ID", "string", 50);
            IDbDataParameter parTYPE = Sql.CreateParameter(cmd, "@TYPE", "Int32", 4);
            IDbDataParameter parPOSITION_ID = Sql.CreateParameter(cmd, "@POSITION_ID", "Guid", 16);
            IDbDataParameter parBUSINESS_CODE = Sql.CreateParameter(cmd, "@BUSINESS_CODE", "string", 50);
            IDbDataParameter parORGANIZATION_ID = Sql.CreateParameter(cmd, "@ORGANIZATION_ID", "Guid", 16);
            IDbDataParameter parORGANIZATION_CODE = Sql.CreateParameter(cmd, "@ORGANIZATION_CODE", "string", 50);
            IDbDataParameter parCONTRACT_TYPE = Sql.CreateParameter(cmd, "@CONTRACT_TYPE", "string", 5);
            IDbDataParameter parIS_BASE = Sql.CreateParameter(cmd, "@IS_BASE", "string", 1);
            IDbDataParameter parBASE_ID = Sql.CreateParameter(cmd, "@BASE_ID", "Guid", 16);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            IDbDataParameter parSTATUS = Sql.CreateParameter(cmd, "@STATUS", "string", 5);
            IDbDataParameter parGROUP_KPI_ID = Sql.CreateParameter(cmd, "@GROUP_KPI_ID", "Guid", 16);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion


        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_STANDARD_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_STANDARD_Mass";
                            else
                                cmd.CommandText = "spB_KPI_STANDARD_MassApprove";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_STANDARD_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_STANDARD_Mass";
                else
                    cmd.CommandText = "spB_KPI_STANDARD_MassApprove";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Gen_KPI_STANDARD_FULL
        /// <summary>
        /// Gen_KPI_STANDARD_FULL
        /// </summary>
        public static void Gen_KPI_STANDARD_FULL(int @iYear, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "Gen_KPI_STANDARD_FULL";
                else
                    cmd.CommandText = "Gen_KPI_STANDARD_FULL";
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@iYear", @iYear);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
    }
}