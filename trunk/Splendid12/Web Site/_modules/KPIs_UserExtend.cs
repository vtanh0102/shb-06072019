﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIs_UserExtend
    {

        #region spB_USER_EXTEND_Delete
        /// <summary>
        /// spB_USER_EXTEND_Delete
        /// </summary>
        public static void spB_USER_EXTEND_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_USER_EXTEND_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_USER_EXTEND_Delete
        /// <summary>
        /// spB_USER_EXTEND_Delete
        /// </summary>
        public static void spB_USER_EXTEND_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_USER_EXTEND_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_USER_EXTEND_Delete
        /// <summary>
        /// spB_USER_EXTEND_Delete
        /// </summary>
        public static IDbCommand cmdB_USER_EXTEND_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_USER_EXTEND_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_USER_EXTEND_Update
        /// <summary>
        /// spB_USER_EXTEND_Update
        /// </summary>
        public static void spB_USER_EXTEND_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, Guid gUSER_ID, Int32 nID_SOURCE, string sUSER_CODE, DateTime dtBIRTH_DATE, string sSEX, Guid gORGANIZATION_ID, Guid gTITLE_ID, Guid gTITLE_ADD_ID, DateTime dtRETIRED_DATE, string sCONTRACT_TYPE, Guid gPOSITION_ID, string sMANAGER_NAME, DateTime dtDATE_START_WORK, DateTime dtDATE_START_INTERN, DateTime dtDATE_START_PROBATION, DateTime dtDATE_ACTING_PROMOTED, DateTime dtOFFICAL_PROMOTED_DATE, DateTime dtCURRENT_POSITION_WORK_TIME, string sSENIORITY, string sSALARY_STATUS, string sPOS_NAME_CENTER, string sAREA, string sPREVIOUS_EMPLOY_HISTORY, string sEMPLOYMENT_HISTORY, string sBUSINESS_TYPE, string sUSER_LDAP, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sFLEX6, string sFLEX7, string sFLEX8, string sFLEX9, string sFLEX10, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_USER_EXTEND_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                            IDbDataParameter parUSER_ID = Sql.AddParameter(cmd, "@USER_ID", gUSER_ID);
                            IDbDataParameter parID_SOURCE = Sql.AddParameter(cmd, "@ID_SOURCE", nID_SOURCE);
                            IDbDataParameter parUSER_CODE = Sql.AddParameter(cmd, "@USER_CODE", sUSER_CODE, 50);
                            IDbDataParameter parBIRTH_DATE = Sql.AddParameter(cmd, "@BIRTH_DATE", dtBIRTH_DATE);
                            IDbDataParameter parSEX = Sql.AddParameter(cmd, "@SEX", sSEX, 20);
                            IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                            IDbDataParameter parTITLE_ID = Sql.AddParameter(cmd, "@TITLE_ID", gTITLE_ID);
                            IDbDataParameter parTITLE_ADD_ID = Sql.AddParameter(cmd, "@TITLE_ADD_ID", gTITLE_ADD_ID);
                            IDbDataParameter parRETIRED_DATE = Sql.AddParameter(cmd, "@RETIRED_DATE", dtRETIRED_DATE);
                            IDbDataParameter parCONTRACT_TYPE = Sql.AddParameter(cmd, "@CONTRACT_TYPE", sCONTRACT_TYPE, 50);
                            IDbDataParameter parPOSITION_ID = Sql.AddParameter(cmd, "@POSITION_ID", gPOSITION_ID);
                            IDbDataParameter parMANAGER_NAME = Sql.AddParameter(cmd, "@MANAGER_NAME", sMANAGER_NAME, 50);
                            IDbDataParameter parDATE_START_WORK = Sql.AddParameter(cmd, "@DATE_START_WORK", dtDATE_START_WORK);
                            IDbDataParameter parDATE_START_INTERN = Sql.AddParameter(cmd, "@DATE_START_INTERN", dtDATE_START_INTERN);
                            IDbDataParameter parDATE_START_PROBATION = Sql.AddParameter(cmd, "@DATE_START_PROBATION", dtDATE_START_PROBATION);
                            IDbDataParameter parDATE_ACTING_PROMOTED = Sql.AddParameter(cmd, "@DATE_ACTING_PROMOTED", dtDATE_ACTING_PROMOTED);
                            IDbDataParameter parOFFICAL_PROMOTED_DATE = Sql.AddParameter(cmd, "@OFFICAL_PROMOTED_DATE", dtOFFICAL_PROMOTED_DATE);
                            IDbDataParameter parCURRENT_POSITION_WORK_TIME = Sql.AddParameter(cmd, "@CURRENT_POSITION_WORK_TIME", dtCURRENT_POSITION_WORK_TIME);
                            IDbDataParameter parSENIORITY = Sql.AddParameter(cmd, "@SENIORITY", sSENIORITY, 20);
                            IDbDataParameter parSALARY_STATUS = Sql.AddParameter(cmd, "@SALARY_STATUS", sSALARY_STATUS, 10);
                            IDbDataParameter parPOS_NAME_CENTER = Sql.AddParameter(cmd, "@POS_NAME_CENTER", sPOS_NAME_CENTER, 50);
                            IDbDataParameter parAREA = Sql.AddParameter(cmd, "@AREA", sAREA, 50);
                            IDbDataParameter parPREVIOUS_EMPLOY_HISTORY = Sql.AddParameter(cmd, "@PREVIOUS_EMPLOY_HISTORY", sPREVIOUS_EMPLOY_HISTORY, 50);
                            IDbDataParameter parEMPLOYMENT_HISTORY = Sql.AddParameter(cmd, "@EMPLOYMENT_HISTORY", sEMPLOYMENT_HISTORY, 50);
                            IDbDataParameter parBUSINESS_TYPE = Sql.AddParameter(cmd, "@BUSINESS_TYPE", sBUSINESS_TYPE, 30);
                            IDbDataParameter parUSER_LDAP = Sql.AddParameter(cmd, "@USER_LDAP", sUSER_LDAP, 40);
                            IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 150);
                            IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 150);
                            IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 150);
                            IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 150);
                            IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 150);
                            IDbDataParameter parFLEX6 = Sql.AddParameter(cmd, "@FLEX6", sFLEX6, 150);
                            IDbDataParameter parFLEX7 = Sql.AddParameter(cmd, "@FLEX7", sFLEX7, 150);
                            IDbDataParameter parFLEX8 = Sql.AddParameter(cmd, "@FLEX8", sFLEX8, 150);
                            IDbDataParameter parFLEX9 = Sql.AddParameter(cmd, "@FLEX9", sFLEX9, 150);
                            IDbDataParameter parFLEX10 = Sql.AddParameter(cmd, "@FLEX10", sFLEX10, 150);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_USER_EXTEND_Update
        /// <summary>
        /// spB_USER_EXTEND_Update
        /// </summary>
        public static void spB_USER_EXTEND_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, Guid gUSER_ID, Int32 nID_SOURCE, string sUSER_CODE, DateTime dtBIRTH_DATE, string sSEX, Guid gORGANIZATION_ID, Guid gTITLE_ID, Guid gTITLE_ADD_ID, DateTime dtRETIRED_DATE, string sCONTRACT_TYPE, Guid gPOSITION_ID, string sMANAGER_NAME, DateTime dtDATE_START_WORK, DateTime dtDATE_START_INTERN, DateTime dtDATE_START_PROBATION, DateTime dtDATE_ACTING_PROMOTED, DateTime dtOFFICAL_PROMOTED_DATE, DateTime dtCURRENT_POSITION_WORK_TIME, string sSENIORITY, string sSALARY_STATUS, string sPOS_NAME_CENTER, string sAREA, string sPREVIOUS_EMPLOY_HISTORY, string sEMPLOYMENT_HISTORY, string sBUSINESS_TYPE, string sUSER_LDAP, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sFLEX6, string sFLEX7, string sFLEX8, string sFLEX9, string sFLEX10, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_USER_EXTEND_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parUSER_ID = Sql.AddParameter(cmd, "@USER_ID", gUSER_ID);
                IDbDataParameter parID_SOURCE = Sql.AddParameter(cmd, "@ID_SOURCE", nID_SOURCE);
                IDbDataParameter parUSER_CODE = Sql.AddParameter(cmd, "@USER_CODE", sUSER_CODE, 50);
                IDbDataParameter parBIRTH_DATE = Sql.AddParameter(cmd, "@BIRTH_DATE", dtBIRTH_DATE);
                IDbDataParameter parSEX = Sql.AddParameter(cmd, "@SEX", sSEX, 20);
                IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                IDbDataParameter parTITLE_ID = Sql.AddParameter(cmd, "@TITLE_ID", gTITLE_ID);
                IDbDataParameter parTITLE_ADD_ID = Sql.AddParameter(cmd, "@TITLE_ADD_ID", gTITLE_ADD_ID);
                IDbDataParameter parRETIRED_DATE = Sql.AddParameter(cmd, "@RETIRED_DATE", dtRETIRED_DATE);
                IDbDataParameter parCONTRACT_TYPE = Sql.AddParameter(cmd, "@CONTRACT_TYPE", sCONTRACT_TYPE, 50);
                IDbDataParameter parPOSITION_ID = Sql.AddParameter(cmd, "@POSITION_ID", gPOSITION_ID);
                IDbDataParameter parMANAGER_NAME = Sql.AddParameter(cmd, "@MANAGER_NAME", sMANAGER_NAME, 50);
                IDbDataParameter parDATE_START_WORK = Sql.AddParameter(cmd, "@DATE_START_WORK", dtDATE_START_WORK);
                IDbDataParameter parDATE_START_INTERN = Sql.AddParameter(cmd, "@DATE_START_INTERN", dtDATE_START_INTERN);
                IDbDataParameter parDATE_START_PROBATION = Sql.AddParameter(cmd, "@DATE_START_PROBATION", dtDATE_START_PROBATION);
                IDbDataParameter parDATE_ACTING_PROMOTED = Sql.AddParameter(cmd, "@DATE_ACTING_PROMOTED", dtDATE_ACTING_PROMOTED);
                IDbDataParameter parOFFICAL_PROMOTED_DATE = Sql.AddParameter(cmd, "@OFFICAL_PROMOTED_DATE", dtOFFICAL_PROMOTED_DATE);
                IDbDataParameter parCURRENT_POSITION_WORK_TIME = Sql.AddParameter(cmd, "@CURRENT_POSITION_WORK_TIME", dtCURRENT_POSITION_WORK_TIME);
                IDbDataParameter parSENIORITY = Sql.AddParameter(cmd, "@SENIORITY", sSENIORITY, 20);
                IDbDataParameter parSALARY_STATUS = Sql.AddParameter(cmd, "@SALARY_STATUS", sSALARY_STATUS, 10);
                IDbDataParameter parPOS_NAME_CENTER = Sql.AddParameter(cmd, "@POS_NAME_CENTER", sPOS_NAME_CENTER, 50);
                IDbDataParameter parAREA = Sql.AddParameter(cmd, "@AREA", sAREA, 50);
                IDbDataParameter parPREVIOUS_EMPLOY_HISTORY = Sql.AddParameter(cmd, "@PREVIOUS_EMPLOY_HISTORY", sPREVIOUS_EMPLOY_HISTORY, 50);
                IDbDataParameter parEMPLOYMENT_HISTORY = Sql.AddParameter(cmd, "@EMPLOYMENT_HISTORY", sEMPLOYMENT_HISTORY, 50);
                IDbDataParameter parBUSINESS_TYPE = Sql.AddParameter(cmd, "@BUSINESS_TYPE", sBUSINESS_TYPE, 30);
                IDbDataParameter parUSER_LDAP = Sql.AddParameter(cmd, "@USER_LDAP", sUSER_LDAP, 40);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 150);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 150);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 150);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 150);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 150);
                IDbDataParameter parFLEX6 = Sql.AddParameter(cmd, "@FLEX6", sFLEX6, 150);
                IDbDataParameter parFLEX7 = Sql.AddParameter(cmd, "@FLEX7", sFLEX7, 150);
                IDbDataParameter parFLEX8 = Sql.AddParameter(cmd, "@FLEX8", sFLEX8, 150);
                IDbDataParameter parFLEX9 = Sql.AddParameter(cmd, "@FLEX9", sFLEX9, 150);
                IDbDataParameter parFLEX10 = Sql.AddParameter(cmd, "@FLEX10", sFLEX10, 150);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_USER_EXTEND_Update
        /// <summary>
        /// spB_USER_EXTEND_Update
        /// </summary>
        public static IDbCommand cmdB_USER_EXTEND_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_USER_EXTEND_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parNAME = Sql.CreateParameter(cmd, "@NAME", "string", 150);
            IDbDataParameter parUSER_ID = Sql.CreateParameter(cmd, "@USER_ID", "Guid", 16);
            IDbDataParameter parID_SOURCE = Sql.CreateParameter(cmd, "@ID_SOURCE", "Int32", 4);
            IDbDataParameter parUSER_CODE = Sql.CreateParameter(cmd, "@USER_CODE", "string", 50);
            IDbDataParameter parBIRTH_DATE = Sql.CreateParameter(cmd, "@BIRTH_DATE", "DateTime", 8);
            IDbDataParameter parSEX = Sql.CreateParameter(cmd, "@SEX", "string", 20);
            IDbDataParameter parORGANIZATION_ID = Sql.CreateParameter(cmd, "@ORGANIZATION_ID", "Guid", 16);
            IDbDataParameter parTITLE_ID = Sql.CreateParameter(cmd, "@TITLE_ID", "Guid", 16);
            IDbDataParameter parTITLE_ADD_ID = Sql.CreateParameter(cmd, "@TITLE_ADD_ID", "Guid", 16);
            IDbDataParameter parRETIRED_DATE = Sql.CreateParameter(cmd, "@RETIRED_DATE", "DateTime", 8);
            IDbDataParameter parCONTRACT_TYPE = Sql.CreateParameter(cmd, "@CONTRACT_TYPE", "string", 50);
            IDbDataParameter parPOSITION_ID = Sql.CreateParameter(cmd, "@POSITION_ID", "Guid", 16);
            IDbDataParameter parMANAGER_NAME = Sql.CreateParameter(cmd, "@MANAGER_NAME", "string", 50);
            IDbDataParameter parDATE_START_WORK = Sql.CreateParameter(cmd, "@DATE_START_WORK", "DateTime", 8);
            IDbDataParameter parDATE_START_INTERN = Sql.CreateParameter(cmd, "@DATE_START_INTERN", "DateTime", 8);
            IDbDataParameter parDATE_START_PROBATION = Sql.CreateParameter(cmd, "@DATE_START_PROBATION", "DateTime", 8);
            IDbDataParameter parDATE_ACTING_PROMOTED = Sql.CreateParameter(cmd, "@DATE_ACTING_PROMOTED", "DateTime", 8);
            IDbDataParameter parOFFICAL_PROMOTED_DATE = Sql.CreateParameter(cmd, "@OFFICAL_PROMOTED_DATE", "DateTime", 8);
            IDbDataParameter parCURRENT_POSITION_WORK_TIME = Sql.CreateParameter(cmd, "@CURRENT_POSITION_WORK_TIME", "DateTime", 8);
            IDbDataParameter parSENIORITY = Sql.CreateParameter(cmd, "@SENIORITY", "string", 20);
            IDbDataParameter parSALARY_STATUS = Sql.CreateParameter(cmd, "@SALARY_STATUS", "string", 10);
            IDbDataParameter parPOS_NAME_CENTER = Sql.CreateParameter(cmd, "@POS_NAME_CENTER", "string", 50);
            IDbDataParameter parAREA = Sql.CreateParameter(cmd, "@AREA", "string", 50);
            IDbDataParameter parPREVIOUS_EMPLOY_HISTORY = Sql.CreateParameter(cmd, "@PREVIOUS_EMPLOY_HISTORY", "string", 50);
            IDbDataParameter parEMPLOYMENT_HISTORY = Sql.CreateParameter(cmd, "@EMPLOYMENT_HISTORY", "string", 50);
            IDbDataParameter parBUSINESS_TYPE = Sql.CreateParameter(cmd, "@BUSINESS_TYPE", "string", 30);
            IDbDataParameter parUSER_LDAP = Sql.CreateParameter(cmd, "@USER_LDAP", "string", 40);
            IDbDataParameter parFLEX1 = Sql.CreateParameter(cmd, "@FLEX1", "string", 150);
            IDbDataParameter parFLEX2 = Sql.CreateParameter(cmd, "@FLEX2", "string", 150);
            IDbDataParameter parFLEX3 = Sql.CreateParameter(cmd, "@FLEX3", "string", 150);
            IDbDataParameter parFLEX4 = Sql.CreateParameter(cmd, "@FLEX4", "string", 150);
            IDbDataParameter parFLEX5 = Sql.CreateParameter(cmd, "@FLEX5", "string", 150);
            IDbDataParameter parFLEX6 = Sql.CreateParameter(cmd, "@FLEX6", "string", 150);
            IDbDataParameter parFLEX7 = Sql.CreateParameter(cmd, "@FLEX7", "string", 150);
            IDbDataParameter parFLEX8 = Sql.CreateParameter(cmd, "@FLEX8", "string", 150);
            IDbDataParameter parFLEX9 = Sql.CreateParameter(cmd, "@FLEX9", "string", 150);
            IDbDataParameter parFLEX10 = Sql.CreateParameter(cmd, "@FLEX10", "string", 150);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion


        #region spB_USER_EXTEND_Update
        /// <summary>
        /// spB_USER_EXTEND_Update
        /// </summary>
        public static void spB_USER_Update_DATEWork(ref Guid gID, DateTime date, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_USER_Update_DateWork";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parDateWork = Sql.AddParameter(cmd, "@DATEWORK", date);

                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region spB_USER_EXTEND_Update
        /// <summary>
        /// spB_USER_EXTEND_Update
        /// </summary>
        public static void spB_USER_Update_Seniority(ref Guid gID, DateTime dateCurrent, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_USER_Update_Seniority";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parDateWork = Sql.AddParameter(cmd, "@DATESTART", dateCurrent);

                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        public static void spB_Users_C_Import(Guid gID, string USER_CODE_C, string TYPE_IDENTIFICATION_C, string IDENTIFICATION_C, string TITLE_ID_SOURCE, string ORGANIZATION_CODE,
            DateTime BIRTH_DATE_C, string SEX_C, string LITERACY_ID_C, string UNIVERSITY_C, string CONTRACT_TYPE_C, string CONTRACT_TERM_C, DateTime DATE_START_WORK_C, DateTime DATE_END_WORK_C, string M_BUSINESS_CODE_C,IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_Users_C_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@USER_CODE_C", USER_CODE_C);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@TYPE_IDENTIFICATION_C", TYPE_IDENTIFICATION_C);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@IDENTIFICATION_C", IDENTIFICATION_C);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddParameter(cmd, "@TITLE_ID_SOURCE", TITLE_ID_SOURCE);
                //IDbDataParameter parNAME = Sql.AddParameter(cmd, "@ORGANIZATION_PARENT_CODE", ORGANIZATION_PARENT_CODE);
                IDbDataParameter parNAME1 = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", ORGANIZATION_CODE);
                IDbDataParameter parNAME2 = Sql.AddParameter(cmd, "@BIRTH_DATE_C", BIRTH_DATE_C);
                IDbDataParameter parNAME3 = Sql.AddParameter(cmd, "@SEX_C", SEX_C);
                IDbDataParameter parNAME4 = Sql.AddParameter(cmd, "@LITERACY_ID_C", LITERACY_ID_C);
                IDbDataParameter parNAME5 = Sql.AddParameter(cmd, "@UNIVERSITY_C", UNIVERSITY_C);
                IDbDataParameter parNAME6 = Sql.AddParameter(cmd, "@CONTRACT_TYPE_C", CONTRACT_TYPE_C);
                IDbDataParameter parNAME7 = Sql.AddParameter(cmd, "@CONTRACT_TERM_C", CONTRACT_TERM_C);
                IDbDataParameter parNAME8 = Sql.AddParameter(cmd, "@DATE_START_WORK_C", DATE_START_WORK_C);
                IDbDataParameter parNAME9 = Sql.AddParameter(cmd, "@DATE_END_WORK_C", DATE_END_WORK_C);
                IDbDataParameter parNAME10 = Sql.AddParameter(cmd, "@M_BUSINESS_CODE_C", M_BUSINESS_CODE_C);  
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
    }


}