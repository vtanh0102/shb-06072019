﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIB030201_SqlProc
    {

        #region spB_KPI_ORG_ACTUAL_RESULT_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sACTUAL_RESULT_CODE, Int32 nYEAR, string sMONTH_PERIOD, Guid gORGANIZATION_ID, string sORGANIZATION_CODE, string sVERSION_NUMBER, string sALLOCATE_CODE, Guid gASSIGN_BY, DateTime dtASSIGN_DATE, float flPERCENT_SYNC_TOTAL, float flPERCENT_FINAL_TOTAL, float flPERCENT_MANUAL_TOTAL, float flTOTAL_AMOUNT_01, float flTOTAL_AMOUNT_02, float flTOTAL_AMOUNT_03, string sDESCRIPTION, string sREMARK, Guid gFILE_ID, DateTime dtLATEST_SYNC_DATE, Guid gAPPROVE_ID, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Impo";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parACTUAL_RESULT_CODE = Sql.AddParameter(cmd, "@ACTUAL_RESULT_CODE", sACTUAL_RESULT_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parORGANIZATION_ID = Sql.AddParameter(cmd, "@ORGANIZATION_ID", gORGANIZATION_ID);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", gASSIGN_BY);
                IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                IDbDataParameter parPERCENT_SYNC_TOTAL = Sql.AddParameter(cmd, "@PERCENT_SYNC_TOTAL", flPERCENT_SYNC_TOTAL);
                IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                IDbDataParameter parPERCENT_MANUAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_MANUAL_TOTAL", flPERCENT_MANUAL_TOTAL);
                IDbDataParameter parTOTAL_AMOUNT_01 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_01", flTOTAL_AMOUNT_01);
                IDbDataParameter parTOTAL_AMOUNT_02 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_02", flTOTAL_AMOUNT_02);
                IDbDataParameter parTOTAL_AMOUNT_03 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_03", flTOTAL_AMOUNT_03);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", gFILE_ID);
                IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                IDbDataParameter parAPPROVE_ID = Sql.AddParameter(cmd, "@APPROVE_ID", gAPPROVE_ID);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 500);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 500);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 500);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 500);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 500);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sACTUAL_RESULT_CODE, Int32 nYEAR, string sMONTH_PERIOD, string sVERSION_NUMBER, Guid gKPI_ID, string sKPI_CODE, string sKPI_NAME, Int32 nKPI_UNIT, Int32 nLEVEL_NUMBER, float flRATIO, float flPLAN_VALUE, float flSYNC_VALUE, float flFINAL_VALUE, float flPERCENT_SYNC_VALUE, float flPERCENT_FINAL_VALUE, float flPERCENT_MANUAL_VALUE, string sDESCRIPTION, string sREMARK, DateTime dtLATEST_SYNC_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parACTUAL_RESULT_CODE = Sql.AddParameter(cmd, "@ACTUAL_RESULT_CODE", sACTUAL_RESULT_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parKPI_ID = Sql.AddParameter(cmd, "@KPI_ID", gKPI_ID);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parKPI_UNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nKPI_UNIT);
                IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                IDbDataParameter parPLAN_VALUE = Sql.AddParameter(cmd, "@PLAN_VALUE", flPLAN_VALUE);
                IDbDataParameter parSYNC_VALUE = Sql.AddParameter(cmd, "@SYNC_VALUE", flSYNC_VALUE);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                IDbDataParameter parPERCENT_SYNC_VALUE = Sql.AddParameter(cmd, "@PERCENT_SYNC_VALUE", flPERCENT_SYNC_VALUE);
                IDbDataParameter parPERCENT_FINAL_VALUE = Sql.AddParameter(cmd, "@PERCENT_FINAL_VALUE", flPERCENT_FINAL_VALUE);
                IDbDataParameter parPERCENT_MANUAL_VALUE = Sql.AddParameter(cmd, "@PERCENT_MANUAL_VALUE", flPERCENT_MANUAL_VALUE);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 500);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 500);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 500);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 500);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 500);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_Delete
        /// </summary>
        public static void spB_KPI_ORG_ACTUAL_RESULT_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Dele";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_Delete
        /// </summary>
        public static void spB_KPI_ORG_ACTUAL_RESULT_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Dele";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ACTUAL_RESULT_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ACTUAL_RESULT_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Dele";
            else
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// </summary>
        public static void spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// </summary>
        public static void spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
            else
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region Gen_KPI_ORG_ACTUAL_RESULT
        /// 

        /// Gen_KPI_ORG_ACTUAL_RESULT
        /// 
        public static void Gen_KPI_ORG_ACTUAL_RESULT(ref Guid gID, int iYear, string sMONTH_PERIOD, string sORGANIZATION_CODE, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Impo";
                else
                    cmd.CommandText = "Gen_KPI_ORG_ACTUAL_RESULT";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@iYear", iYear);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@sMONTH_PERIOD", sMONTH_PERIOD, 2);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddAnsiParam(cmd, "@sORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sKPI_CODE, decimal flFINAL_VALUE, string sREMARK, DateTime dtLATEST_SYNC_DATE)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                            IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                            IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sKPI_CODE, decimal flFINAL_VALUE, float flPERCENT_FINAL_VALUE, string sREMARK, DateTime dtLATEST_SYNC_DATE, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                IDbDataParameter parPERCENT_FINAL_VALUE = Sql.AddParameter(cmd, "@PERCENT_FINAL_VALUE", flPERCENT_FINAL_VALUE);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import
        /// 
        public static IDbCommand cmdB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETA";
            else
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parMONTH_PERIOD = Sql.CreateParameter(cmd, "@MONTH_PERIOD", "string", 50);
            IDbDataParameter parKPI_CODE = Sql.CreateParameter(cmd, "@KPI_CODE", "string", 50);
            IDbDataParameter parFINAL_VALUE = Sql.CreateParameter(cmd, "@FINAL_VALUE", "float", 8);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            IDbDataParameter parLATEST_SYNC_DATE = Sql.CreateParameter(cmd, "@LATEST_SYNC_DATE", "DateTime", 8);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sORGANIZATION_CODE, float flPERCENT_FINAL_TOTAL, string sDESCRIPTION)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Upda";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Update_Import";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                            IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                            IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 
        public static void spB_KPI_ORG_ACTUAL_RESULT_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sORGANIZATION_CODE, float flPERCENT_FINAL_TOTAL, string sDESCRIPTION, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Upda";
                else
                    cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Update_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ORG_ACTUAL_RESULT_Update_Import
        /// 
        public static IDbCommand cmdB_KPI_ORG_ACTUAL_RESULT_Update_Import(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Upda";
            else
                cmd.CommandText = "spB_KPI_ORG_ACTUAL_RESULT_Update_Import";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parMONTH_PERIOD = Sql.CreateParameter(cmd, "@MONTH_PERIOD", "string", 50);
            IDbDataParameter parORGANIZATION_CODE = Sql.CreateParameter(cmd, "@ORGANIZATION_CODE", "string", 50);
            IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.CreateParameter(cmd, "@PERCENT_FINAL_TOTAL", "float", 8);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

    }
}