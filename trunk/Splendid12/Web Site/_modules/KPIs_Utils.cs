﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SplendidCRM._modules
{
    public class KPIs_Utils
    {

        //private static string M_GROUP_KPIS_CODE = "KPIs{0}-{1}";
        public static Int32 NUM_OF_YEAR = 5;
        public static string STATUS_ACTIVE = "Active";
        public static string STATUS_INACTIVE = "InActive";

        public static int ACTIVE = 1;
        public static int INACTIVE = 0;

        public static string M_GROUP_KPIS_TYPE = "M_GROUP_KPIS_TYPE";
        public static string M_GROUP_KPIS_ORGANIZATION = "M_GROUP_KPIS_ORGANIZATION";
        public static string YES_NO_DOM = "yesno_dom";

        public static int POSITION_ID = 0;
        public static int BRANCH_ID = 3;
        public static int DEPARMENT_ID = 4;

        public static Decimal MAX_RATIO = 100;

        public static string START_MONTH = "00_06";
        public static string END_MONTH = "07_12";
        public static string FULL_MONTH = "00_12";
        public static string BAD_KPIs = "NX";

        public static string GenerateKPIsCode(string prefixText, string fromTable, string columnMax, string year = "")
        {
            if (string.IsNullOrEmpty(year))
                year = DateTime.Now.Year.ToString();
            string number = getNumberCodeFromTable(fromTable, columnMax).ToString().PadLeft(5, '0');

            string KPIsCode = string.Format("{0}{1}-{2}", prefixText, year, number);
            return KPIsCode;
        }

        public static string GenerateAllocatedCode(string prefixText, string subPrefix, string fromTable, string columnMax)
        {
            string year = DateTime.Now.Year.ToString();
            string number = getAllocatedCodeFromTable(fromTable, columnMax).ToString().PadLeft(5, '0');

            string KPIsCode = string.Format("{0}{1}-{2}-{3}", prefixText, subPrefix, year, number);
            return KPIsCode;
        }

        public static void FillDropdownListOfYear(DropDownList ddlYEAR)
        {
            ddlYEAR.Items.Clear();
            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= NUM_OF_YEAR; i++)
            {
                // Now just add an entry that's the current year
                ddlYEAR.Items.Add((currentYear + i).ToString());
            }
        }

        public static void FillDropdownListOfYear(ListControl ddlYEAR)
        {
            ddlYEAR.Items.Clear();
            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= NUM_OF_YEAR; i++)
            {
                // Now just add an entry that's the current year
                ddlYEAR.Items.Add((currentYear + i).ToString());
            }
        }

        static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);

        }

        private static string getNumberCodeFromTable(string fromTable, string columnMax)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            Int32 numberCode = 0;
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT MAX(" + columnMax + ") AS CODE          " + ControlChars.CrLf
                                     + "  FROM " + fromTable + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                string code = rdr["CODE"].ToString();
                                if (code != null && code != string.Empty)
                                {
                                    numberCode = Int32.Parse(code.Split('-')[1].ToString()) + 1;
                                }
                                else
                                {
                                    numberCode = 1;
                                }
                            }

                        }
                        else
                        {
                            numberCode = 1;
                        }
                    }
                }
            }
            return numberCode.ToString();
        }

        public static void LoadEmployee_Info(SplendidControl main, DbProviderFactory dbf, Guid emplID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT E.ID AS USER_ID, E.USER_CODE_C  AS USER_CODE         " + ControlChars.CrLf
                     + "   , E.FULL_NAME, P.POSITION_NAME AS TITLE, O.ORGANIZATION_CODE AS DEPARTMENT_CODE , O.ORGANIZATION_NAME AS  DEPARTMENT       " + ControlChars.CrLf
                     + "   , (select DISPLAY_NAME from vwTERMINOLOGY_List where 1 = 1 and LIST_NAME = 'CONTRACT_TYPE' and LANG = 'en-US' AND NAME = E.CONTRACT_TYPE_C) AS TYPE_OF_LABOR " + ControlChars.CrLf
                    //+ "   , '110300' AS BRANCH_CODE  , 'Branch 1' AS BRANCH , 'Region 1' AS REGION                          " + ControlChars.CrLf
                     + "   , E.POSITION_ID_C  , E.AREA_C  AS AREA_ID_C , O.ID AS ORGANIZATION_ID, B.BUSINESS_NAME            " + ControlChars.CrLf
                     + "   , E.M_BUSINESS_CODE_C  AS BUSINESS_CODE_C                                                         " + ControlChars.CrLf
                     + "   , A.AREA_NAME AS AREA, S.SENIORITY_NAME AS SENIORITY, E.OFFICAL_PROMOTED_DATE_C AS OFFICIAL_TIME  " + ControlChars.CrLf
                     + "  FROM vwEMPLOYEES_Edit E                                " + ControlChars.CrLf
                     + "  LEFT JOIN M_POSITION P ON (P.ID = E.POSITION_ID_C)     " + ControlChars.CrLf //Chuc danh
                     + "  LEFT JOIN M_AREA A ON (A.ID = E.AREA_C)                " + ControlChars.CrLf //Khu vuc
                     + "  LEFT JOIN M_SENIORITY S ON (S.ID = E.SENIORITY_C)      " + ControlChars.CrLf //Tham nien
                     + "  LEFT JOIN M_BUSINESS B ON (B.BUSINESS_CODE = E.M_BUSINESS_CODE_C)      " + ControlChars.CrLf //Chuc nang
                     + "  LEFT JOIN M_ORGANIZATION O ON (O.ID = E.ORGANIZATION_ID_C) WHERE 1 = 1 " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, emplID, "ID_C", false);
                    con.Open();

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                HtmlInputHidden hdfUSER_CODE = main.FindControl("EMPLOYEE_ID") as HtmlInputHidden;
                                if (hdfUSER_CODE != null)
                                {
                                    hdfUSER_CODE.Value = rdr["USER_ID"].ToString();
                                }
                                HtmlInputHidden hdfORGANIZATION_ID = main.FindControl("ORGANIZATION_ID") as HtmlInputHidden;
                                if (hdfORGANIZATION_ID != null)
                                {
                                    hdfORGANIZATION_ID.Value = rdr["ORGANIZATION_ID"].ToString();
                                }

                                Label lblEMPLOYEE_ID = main.FindControl("MA_NHAN_VIEN") as Label;
                                if (lblEMPLOYEE_ID != null)
                                {
                                    lblEMPLOYEE_ID.Text = rdr["USER_CODE"].ToString();
                                }
                                //CHUC NANG
                                Label lblBUSINESS_CODE = main.FindControl("BUSINESS_CODE") as Label;
                                if (lblBUSINESS_CODE != null)
                                {
                                    lblBUSINESS_CODE.Text = rdr["BUSINESS_NAME"].ToString();
                                }
                                Label lblEMPLOYEE_NAME = main.FindControl("EMPLOYEE_NAME") as Label;
                                if (lblEMPLOYEE_NAME != null)
                                {
                                    lblEMPLOYEE_NAME.Text = rdr["FULL_NAME"].ToString();
                                }
                                Label lblPOSITION = main.FindControl("POSITION") as Label;
                                if (lblPOSITION != null)
                                {
                                    lblPOSITION.Text = rdr["TITLE"].ToString();
                                }
                                Label lblTYPE_OF_LABOR = main.FindControl("TYPE_OF_LABOR") as Label;
                                if (lblTYPE_OF_LABOR != null)
                                {
                                    lblTYPE_OF_LABOR.Text = rdr["TYPE_OF_LABOR"].ToString();
                                }
                                Label lblBRANCH = main.FindControl("BRANCH") as Label;
                                if (lblBRANCH != null)
                                {
                                    lblBRANCH.Text = rdr["BRANCH"].ToString();
                                }
                                Label lblBRANCH_CODE = main.FindControl("BRANCH_CODE") as Label;
                                if (lblBRANCH_CODE != null)
                                {
                                    lblBRANCH_CODE.Text = rdr["BRANCH_CODE"].ToString();
                                }
                                Label lblAREA = main.FindControl("AREA") as Label;
                                if (lblAREA != null)
                                {
                                    lblAREA.Text = rdr["AREA"].ToString();
                                }
                                Label lblSENIORITY = main.FindControl("SENIORITY") as Label;
                                if (lblSENIORITY != null)
                                {
                                    lblSENIORITY.Text = rdr["SENIORITY"].ToString();
                                }
                                Label lblDEPARTMENT = main.FindControl("DEPARTMENT") as Label;
                                if (lblDEPARTMENT != null)
                                {
                                    lblDEPARTMENT.Text = rdr["DEPARTMENT"].ToString();
                                }
                                Label lblDEP_CODE = main.FindControl("DEPARTMENT_CODE") as Label;
                                if (lblDEP_CODE != null)
                                {
                                    lblDEP_CODE.Text = rdr["DEPARTMENT_CODE"].ToString();
                                }
                                Label lblREGION = main.FindControl("REGION") as Label;
                                if (lblREGION != null)
                                {
                                    lblREGION.Text = rdr["REGION"].ToString();
                                }
                                Label lblOFFICIAL_TIME = main.FindControl("OFFICIAL_TIME") as Label;
                                if (lblOFFICIAL_TIME != null)
                                {
                                    lblOFFICIAL_TIME.Text = rdr["OFFICIAL_TIME"].ToString();
                                }
                                HtmlInputHidden hdfPOSITION_ID_C = main.FindControl("POSITION_ID") as HtmlInputHidden;
                                if (hdfPOSITION_ID_C != null)
                                {
                                    hdfPOSITION_ID_C.Value = rdr["POSITION_ID_C"].ToString();
                                }
                                HtmlInputHidden hdfAREA_ID_C = main.FindControl("AREA_ID") as HtmlInputHidden;
                                if (hdfAREA_ID_C != null)
                                {
                                    hdfAREA_ID_C.Value = rdr["AREA_ID_C"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void LoadEmployee_Info(SplendidControl main, DbProviderFactory dbf, Guid emplID, string allocateCode)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT E.ID AS USER_ID, E.USER_CODE_C  AS USER_CODE         " + ControlChars.CrLf
                     + "   , E.FULL_NAME, P.POSITION_NAME AS TITLE, O.ORGANIZATION_CODE AS DEPARTMENT_CODE , O.ORGANIZATION_NAME AS  DEPARTMENT       " + ControlChars.CrLf
                     + "   , (select DISPLAY_NAME from vwTERMINOLOGY_List where 1 = 1 and LIST_NAME = 'CONTRACT_TYPE' and LANG = 'en-US' AND NAME = E.CONTRACT_TYPE_C) AS TYPE_OF_LABOR " + ControlChars.CrLf
                    //+ "   , '110300' AS BRANCH_CODE  , 'Branch 1' AS BRANCH , 'Region 1' AS REGION                          " + ControlChars.CrLf
                     + "   , E.POSITION_ID_C  , E.AREA_C  AS AREA_ID_C , O.ID AS ORGANIZATION_ID, B.BUSINESS_NAME            " + ControlChars.CrLf
                     + "   , E.M_BUSINESS_CODE_C  AS BUSINESS_CODE_C                                                         " + ControlChars.CrLf
                     + "   , A.AREA_NAME AS AREA, S.SENIORITY_NAME AS SENIORITY, E.OFFICAL_PROMOTED_DATE_C AS OFFICIAL_TIME  " + ControlChars.CrLf
                     + "    FROM vwB_KPI_ALLOCATES ALC                                 " + ControlChars.CrLf
                     + "   INNER JOIN vwEMPLOYEES_Edit E ON E.ID  =  ALC.EMPLOYEE_ID                              " + ControlChars.CrLf
                     + "  LEFT JOIN M_POSITION P ON (P.ID = ALC.POSITION_ID)     " + ControlChars.CrLf //Chuc danh
                     + "  LEFT JOIN M_AREA A ON (A.ID = ALC.AREA_ID)                " + ControlChars.CrLf //Khu vuc
                     + "  LEFT JOIN M_SENIORITY S ON (S.ID = ALC.SENIORITY_C)      " + ControlChars.CrLf //Tham nien
                     + "  LEFT JOIN M_BUSINESS B ON (B.BUSINESS_CODE = ALC.M_BUSINESS_CODE_C)      " + ControlChars.CrLf //Chuc nang
                     + "  LEFT JOIN M_ORGANIZATION O ON (O.ID = ALC.ORGANIZATION_ID) WHERE 1 = 1 " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, emplID, "EMPLOYEE_ID", false);
                    Sql.AppendParameter(cmd, allocateCode, "ALLOCATE_CODE");
                    con.Open();

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                HtmlInputHidden hdfUSER_CODE = main.FindControl("EMPLOYEE_ID") as HtmlInputHidden;
                                if (hdfUSER_CODE != null)
                                {
                                    hdfUSER_CODE.Value = rdr["USER_ID"].ToString();
                                }
                                HtmlInputHidden hdfORGANIZATION_ID = main.FindControl("ORGANIZATION_ID") as HtmlInputHidden;
                                if (hdfORGANIZATION_ID != null)
                                {
                                    hdfORGANIZATION_ID.Value = rdr["ORGANIZATION_ID"].ToString();
                                }

                                Label lblEMPLOYEE_ID = main.FindControl("MA_NHAN_VIEN") as Label;
                                if (lblEMPLOYEE_ID != null)
                                {
                                    lblEMPLOYEE_ID.Text = rdr["USER_CODE"].ToString();
                                }
                                //CHUC NANG
                                Label lblBUSINESS_CODE = main.FindControl("BUSINESS_CODE") as Label;
                                if (lblBUSINESS_CODE != null)
                                {
                                    lblBUSINESS_CODE.Text = rdr["BUSINESS_NAME"].ToString();
                                }
                                Label lblEMPLOYEE_NAME = main.FindControl("EMPLOYEE_NAME") as Label;
                                if (lblEMPLOYEE_NAME != null)
                                {
                                    lblEMPLOYEE_NAME.Text = rdr["FULL_NAME"].ToString();
                                }
                                Label lblPOSITION = main.FindControl("POSITION") as Label;
                                if (lblPOSITION != null)
                                {
                                    lblPOSITION.Text = rdr["TITLE"].ToString();
                                }
                                Label lblTYPE_OF_LABOR = main.FindControl("TYPE_OF_LABOR") as Label;
                                if (lblTYPE_OF_LABOR != null)
                                {
                                    lblTYPE_OF_LABOR.Text = rdr["TYPE_OF_LABOR"].ToString();
                                }
                                Label lblBRANCH = main.FindControl("BRANCH") as Label;
                                if (lblBRANCH != null)
                                {
                                    lblBRANCH.Text = rdr["BRANCH"].ToString();
                                }
                                Label lblBRANCH_CODE = main.FindControl("BRANCH_CODE") as Label;
                                if (lblBRANCH_CODE != null)
                                {
                                    lblBRANCH_CODE.Text = rdr["BRANCH_CODE"].ToString();
                                }
                                Label lblAREA = main.FindControl("AREA") as Label;
                                if (lblAREA != null)
                                {
                                    lblAREA.Text = rdr["AREA"].ToString();
                                }
                                Label lblSENIORITY = main.FindControl("SENIORITY") as Label;
                                if (lblSENIORITY != null)
                                {
                                    lblSENIORITY.Text = rdr["SENIORITY"].ToString();
                                }
                                Label lblDEPARTMENT = main.FindControl("DEPARTMENT") as Label;
                                if (lblDEPARTMENT != null)
                                {
                                    lblDEPARTMENT.Text = rdr["DEPARTMENT"].ToString();
                                }
                                Label lblDEP_CODE = main.FindControl("DEPARTMENT_CODE") as Label;
                                if (lblDEP_CODE != null)
                                {
                                    lblDEP_CODE.Text = rdr["DEPARTMENT_CODE"].ToString();
                                }
                                Label lblREGION = main.FindControl("REGION") as Label;
                                if (lblREGION != null)
                                {
                                    lblREGION.Text = rdr["REGION"].ToString();
                                }
                                Label lblOFFICIAL_TIME = main.FindControl("OFFICIAL_TIME") as Label;
                                if (lblOFFICIAL_TIME != null)
                                {
                                    lblOFFICIAL_TIME.Text = rdr["OFFICIAL_TIME"].ToString();
                                }
                                HtmlInputHidden hdfPOSITION_ID_C = main.FindControl("POSITION_ID") as HtmlInputHidden;
                                if (hdfPOSITION_ID_C != null)
                                {
                                    hdfPOSITION_ID_C.Value = rdr["POSITION_ID_C"].ToString();
                                }
                                HtmlInputHidden hdfAREA_ID_C = main.FindControl("AREA_ID") as HtmlInputHidden;
                                if (hdfAREA_ID_C != null)
                                {
                                    hdfAREA_ID_C.Value = rdr["AREA_ID_C"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void LoadEmployee_Info_Detail(SplendidControl main, DbProviderFactory dbf, Guid emplID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT E.ID AS USER_ID, E.USER_CODE_C  AS USER_CODE         " + ControlChars.CrLf
                     + "   , E.FULL_NAME, P.POSITION_NAME AS TITLE, O.ORGANIZATION_CODE AS DEPARTMENT_CODE , O.ORGANIZATION_NAME AS  DEPARTMENT       " + ControlChars.CrLf
                     + "   , (select DISPLAY_NAME from vwTERMINOLOGY_List where 1 = 1 and LIST_NAME = 'CONTRACT_TYPE' and LANG = 'en-US' AND NAME = E.CONTRACT_TYPE_C) AS TYPE_OF_LABOR, '110300' AS BRANCH_CODE  , 'Branch 1' AS BRANCH                                     " + ControlChars.CrLf
                     + "   , A.AREA_NAME AS AREA, S.SENIORITY_NAME AS SENIORITY, 'Region 1' AS REGION, E.OFFICAL_PROMOTED_DATE_C AS OFFICIAL_TIME  " + ControlChars.CrLf
                     + "   , E.POSITION_ID_C  , E.AREA_C  AS AREA_ID_C, B.BUSINESS_NAME   " + ControlChars.CrLf
                     + "   , E.M_BUSINESS_CODE_C  AS BUSINESS_CODE_C                      " + ControlChars.CrLf
                     + "  FROM vwEMPLOYEES_Edit E                               " + ControlChars.CrLf
                     + "  LEFT JOIN M_POSITION P ON (P.ID = E.POSITION_ID_C)    " + ControlChars.CrLf//Chuc danh
                     + "  LEFT JOIN M_AREA A ON (A.ID = E.AREA_C)               " + ControlChars.CrLf//Khu vuc
                     + "  LEFT JOIN M_SENIORITY S ON (S.ID = E.SENIORITY_C)     " + ControlChars.CrLf//Tham nien
                     + "  LEFT JOIN M_BUSINESS B ON (B.BUSINESS_CODE = E.M_BUSINESS_CODE_C)    " + ControlChars.CrLf //Chuc nang
                     + "  LEFT JOIN M_ORGANIZATION O ON (O.ID = E.ORGANIZATION_ID_C) WHERE 1=1 " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, emplID, "ID_C", false);
                    con.Open();

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                HtmlInputHidden hdfUSER_CODE = main.FindControl("EMPLOYEE_ID") as HtmlInputHidden;
                                if (hdfUSER_CODE != null)
                                {
                                    hdfUSER_CODE.Value = rdr["USER_ID"].ToString();
                                }
                                HtmlInputHidden hdfPOSITION_ID_C = main.FindControl("POSITION_ID") as HtmlInputHidden;
                                if (hdfPOSITION_ID_C != null)
                                {
                                    hdfPOSITION_ID_C.Value = rdr["POSITION_ID_C"].ToString();
                                }
                                HtmlInputHidden hdfAREA_ID_C = main.FindControl("AREA_ID") as HtmlInputHidden;
                                if (hdfAREA_ID_C != null)
                                {
                                    hdfAREA_ID_C.Value = rdr["AREA_ID_C"].ToString();
                                }
                                //CHUC NANG
                                Label lblBUSINESS_CODE = main.FindControl("BUSINESS_CODE") as Label;
                                if (lblBUSINESS_CODE != null)
                                {
                                    lblBUSINESS_CODE.Text = rdr["BUSINESS_NAME"].ToString();
                                }
                                else
                                {
                                    new DynamicControl(main, "BUSINESS_CODE").Text = rdr["BUSINESS_NAME"].ToString();
                                }

                                Label lblEMPLOYEE_ID = main.FindControl("MA_NHAN_VIEN") as Label;
                                if (lblEMPLOYEE_ID != null)
                                {
                                    lblEMPLOYEE_ID.Text = rdr["USER_CODE"].ToString();
                                }
                                else
                                {
                                    new DynamicControl(main, "MA_NHAN_VIEN").Text = rdr["USER_CODE"].ToString();
                                }

                                new DynamicControl(main, "EMPLOYEE_NAME").Text = rdr["FULL_NAME"].ToString();

                                new DynamicControl(main, "POSITION").Text = rdr["TITLE"].ToString();

                                new DynamicControl(main, "TYPE_OF_LABOR").Text = rdr["TYPE_OF_LABOR"].ToString();

                                new DynamicControl(main, "BRANCH").Text = rdr["BRANCH"].ToString();

                                new DynamicControl(main, "BRANCH_CODE").Text = rdr["BRANCH_CODE"].ToString();

                                new DynamicControl(main, "AREA").Text = rdr["AREA"].ToString();

                                new DynamicControl(main, "SENIORITY").Text = rdr["SENIORITY"].ToString();

                                new DynamicControl(main, "DEPARTMENT").Text = rdr["DEPARTMENT"].ToString();

                                new DynamicControl(main, "DEPARTMENT_CODE").Text = rdr["DEPARTMENT_CODE"].ToString();

                                new DynamicControl(main, "REGION").Text = rdr["REGION"].ToString();

                                new DynamicControl(main, "OFFICIAL_TIME").Text = rdr["OFFICIAL_TIME"].ToString();
                            }
                        }
                    }
                }
            }
        }

        public static void LoadEmployee_Info_Detail(SplendidControl main, DbProviderFactory dbf, Guid emplID, string allocateCode)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT E.ID AS USER_ID, E.USER_CODE_C  AS USER_CODE         " + ControlChars.CrLf
                     + "   , E.FULL_NAME, P.POSITION_NAME AS TITLE, O.ORGANIZATION_CODE AS DEPARTMENT_CODE , O.ORGANIZATION_NAME AS  DEPARTMENT       " + ControlChars.CrLf
                     + "   , (select DISPLAY_NAME from vwTERMINOLOGY_List where 1 = 1 and LIST_NAME = 'CONTRACT_TYPE' and LANG = 'en-US' AND NAME = E.CONTRACT_TYPE_C) AS TYPE_OF_LABOR, '110300' AS BRANCH_CODE  , 'Branch 1' AS BRANCH                                     " + ControlChars.CrLf
                     + "   , A.AREA_NAME AS AREA, S.SENIORITY_NAME AS SENIORITY, 'Region 1' AS REGION, E.OFFICAL_PROMOTED_DATE_C AS OFFICIAL_TIME  " + ControlChars.CrLf
                     + "   , E.POSITION_ID_C  , E.AREA_C  AS AREA_ID_C, B.BUSINESS_NAME   " + ControlChars.CrLf
                     + "   , E.M_BUSINESS_CODE_C  AS BUSINESS_CODE_C                      " + ControlChars.CrLf
                     + "  FROM vwB_KPI_ALLOCATES ALC                            " + ControlChars.CrLf
                     + "  INNER JOIN vwEMPLOYEES_Edit E ON E.ID  =  ALC.EMPLOYEE_ID                             " + ControlChars.CrLf
                     + "  LEFT JOIN M_POSITION P ON (P.ID = ALC.POSITION_ID)    " + ControlChars.CrLf//Chuc danh
                     + "  LEFT JOIN M_AREA A ON (A.ID = ALC.AREA_ID)               " + ControlChars.CrLf//Khu vuc
                     + "  LEFT JOIN M_SENIORITY S ON (S.ID = ALC.SENIORITY_C)     " + ControlChars.CrLf//Tham nien //20-16-2019 Tuan Anh thay doi E.SENIORITY_C  => ALC.SENIORITY_C
                     + "  LEFT JOIN M_BUSINESS B ON (B.BUSINESS_CODE = ALC.M_BUSINESS_CODE_C)    " + ControlChars.CrLf //Chuc nang
                     + "  LEFT JOIN M_ORGANIZATION O ON (O.ID = ALC.ORGANIZATION_ID) WHERE ALC.EMPLOYEE_ID = '" + emplID.ToString() + "' " + ControlChars.CrLf
                     + "  AND  ALC.ALLOCATE_CODE = '" + allocateCode + "' " + ControlChars.CrLf;

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    con.Open();

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                HtmlInputHidden hdfUSER_CODE = main.FindControl("EMPLOYEE_ID") as HtmlInputHidden;
                                if (hdfUSER_CODE != null)
                                {
                                    hdfUSER_CODE.Value = rdr["USER_ID"].ToString();
                                }
                                HtmlInputHidden hdfPOSITION_ID_C = main.FindControl("POSITION_ID") as HtmlInputHidden;
                                if (hdfPOSITION_ID_C != null)
                                {
                                    hdfPOSITION_ID_C.Value = rdr["POSITION_ID_C"].ToString();
                                }
                                HtmlInputHidden hdfAREA_ID_C = main.FindControl("AREA_ID") as HtmlInputHidden;
                                if (hdfAREA_ID_C != null)
                                {
                                    hdfAREA_ID_C.Value = rdr["AREA_ID_C"].ToString();
                                }
                                //CHUC NANG
                                Label lblBUSINESS_CODE = main.FindControl("BUSINESS_CODE") as Label;
                                if (lblBUSINESS_CODE != null)
                                {
                                    lblBUSINESS_CODE.Text = rdr["BUSINESS_NAME"].ToString();
                                }
                                else
                                {
                                    new DynamicControl(main, "BUSINESS_CODE").Text = rdr["BUSINESS_NAME"].ToString();
                                }

                                Label lblEMPLOYEE_ID = main.FindControl("MA_NHAN_VIEN") as Label;
                                if (lblEMPLOYEE_ID != null)
                                {
                                    lblEMPLOYEE_ID.Text = rdr["USER_CODE"].ToString();
                                }
                                else
                                {
                                    new DynamicControl(main, "MA_NHAN_VIEN").Text = rdr["USER_CODE"].ToString();
                                }

                                new DynamicControl(main, "EMPLOYEE_NAME").Text = rdr["FULL_NAME"].ToString();

                                new DynamicControl(main, "POSITION").Text = rdr["TITLE"].ToString();

                                new DynamicControl(main, "TYPE_OF_LABOR").Text = rdr["TYPE_OF_LABOR"].ToString();

                                new DynamicControl(main, "BRANCH").Text = rdr["BRANCH"].ToString();

                                new DynamicControl(main, "BRANCH_CODE").Text = rdr["BRANCH_CODE"].ToString();

                                new DynamicControl(main, "AREA").Text = rdr["AREA"].ToString();

                                new DynamicControl(main, "SENIORITY").Text = rdr["SENIORITY"].ToString();

                                new DynamicControl(main, "DEPARTMENT").Text = rdr["DEPARTMENT"].ToString();

                                new DynamicControl(main, "DEPARTMENT_CODE").Text = rdr["DEPARTMENT_CODE"].ToString();

                                new DynamicControl(main, "REGION").Text = rdr["REGION"].ToString();

                                new DynamicControl(main, "OFFICIAL_TIME").Text = rdr["OFFICIAL_TIME"].ToString();
                            }
                        }
                    }
                }
            }
        }

        private static string getAllocatedCodeFromTable(string fromTable, string columnMax)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            Int32 numberCode = 0;
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT MAX(" + columnMax + ") AS CODE          " + ControlChars.CrLf
                                     + "  FROM " + fromTable + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                DataRow rdr = dtCurrent.Rows[0];
                                string code = rdr["CODE"].ToString();
                                if (code != null && code != string.Empty)
                                {
                                    numberCode = Int32.Parse(code.Split('-')[2].ToString()) + 1;
                                }
                                else
                                {
                                    numberCode = 1;
                                }
                            }

                        }
                        else
                        {
                            numberCode = 1;
                        }
                    }
                }
            }
            return numberCode.ToString();
        }


        public static void KPI_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, GROUP_KPI_ID                      " + ControlChars.CrLf
                                     + "  FROM vwM_GROUP_KPI_DETAILS_List   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                    //Security.Filter(cmd, "KPIM0101", "list");

                    Sql.AppendParameter(cmd, gID, "GROUP_KPI_ID");
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    KPIM0101_SqlProc.spM_GROUP_KPI_DETAILS_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }

        public static void KPI_ORG_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, KPI_ALLOCATE_ID                     " + ControlChars.CrLf
                                     + "  FROM vwB_KPI_ORG_ALLOCATE_DETAILS   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                    //Security.Filter(cmd, "KPIB020202", "list");

                    Sql.AppendParameter(cmd, gID, "KPI_ALLOCATE_ID");
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    KPIB020202_SqlProc.spB_KPI_ORG_ALLOCATE_DETAILS_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }


        public static void KPI_ORG_ACTUAL_RESULT_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, ACTUAL_RESULT_CODE                      " + ControlChars.CrLf
                                     + " FROM vwB_KPI_ORG_ACTUAL_RESULT_DETAIL   " + ControlChars.CrLf
                                     + " WHERE ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM vwB_KPI_ORG_ACTUAL_RESULT_Edit WHERE ID = @ID)  " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    //Security.Filter(cmd, "KPIB030201", "list");
                    Sql.AddParameter(cmd, "@ID", gID.ToString());
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    KPIB030201_SqlProc.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }

        public static void KPI_ALLOCATE_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, KPI_ALLOCATE_ID                      " + ControlChars.CrLf
                                     + "  FROM vwB_KPI_ALLOCATE_DETAILS_Edit   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                    //Security.Filter(cmd, "KPIB0203", "list");

                    Sql.AppendParameter(cmd, gID, "KPI_ALLOCATE_ID");
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    KPIB0203_SqlProcs.spB_KPI_ALLOCATE_DETAILS_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }

        public static void B_KPI_ACT_RESULT_DETAIL_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID                                        " + ControlChars.CrLf
                                     + "  FROM vwB_KPI_ACT_RESULT_DETAIL_Edit   " + ControlChars.CrLf
                                     + "  where ACTUAL_RESULT_CODE IN (select ACTUAL_RESULT_CODE from vwB_KPI_ACTUAL_RESULT_Edit where ID = @ID);   " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    //Security.Filter(cmd, "KPIB0302", "list");
                    Sql.AddParameter(cmd, "@ID", gID);

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtCurrent.Rows.Count; i++)
                                {
                                    var Id = dtCurrent.Rows[i]["ID"];
                                    SqlProcs.spB_KPI_ACT_RESULT_DETAIL_Delete(Sql.ToGuid(Id));
                                }

                            }

                        }

                    }
                }
            }
        }

        public static string Get_DisplayName(string lang, string listName, string name)
        {
            string displayName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT NAME, LIST_NAME, DISPLAY_NAME, LANG           " + ControlChars.CrLf
                                     + "  FROM vwTERMINOLOGY_List   WHERE 1 = 1     " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;

                    Sql.AppendParameter(cmd, listName, 50, Sql.SqlFilterMode.Exact, "LIST_NAME");
                    Sql.AppendParameter(cmd, lang.ToLower(), 10, Sql.SqlFilterMode.Exact, "LANG");
                    Sql.AppendParameter(cmd, name, "NAME");
                    cmd.CommandText += " ORDER BY LIST_ORDER";

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            displayName = dtCurrent.Rows[0]["DISPLAY_NAME"].ToString();

                        }

                    }
                }
            }
            return displayName;
        }


        public static string getFullNameFromUser(string userID)
        {
            string fullName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, FULL_NAME     " + ControlChars.CrLf
                                     + "  from vwUSERS_Edit  WHERE 1 = 1     " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, Sql.ToGuid(userID), "ID", false);
                    cmd.CommandText += " ORDER BY FULL_NAME";

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            fullName = dtCurrent.Rows[0]["FULL_NAME"].ToString();
                        }

                    }
                }
            }
            return fullName;
        }

        public static string checkAreaOfUser(Guid empID)
        {
            string fullName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, AREA_C     " + ControlChars.CrLf
                                     + "  from vwEMPLOYEES_Edit  WHERE 1 = 1 " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, empID, "ID", false);
                    cmd.CommandText += " ORDER BY AREA_C";

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            fullName = dtCurrent.Rows[0]["AREA_C"].ToString();
                        }

                    }
                }
            }
            return fullName;
        }

        public static string getNameFromGuidID(Guid gID, string strTABLE, string columnName)
        {
            string strName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, " + columnName + ControlChars.CrLf
                                     + "  FROM " + strTABLE + "  WHERE 1 = 1  " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, gID, "ID", false);
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            strName = dtCurrent.Rows[0][columnName].ToString();

                        }

                    }
                }
            }
            return strName;
        }

        public static string getValueFromGuidID(Guid gID, string strTABLE, string columnName, string fieldSearch)
        {
            string strName = string.Empty;
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT " + columnName + ControlChars.CrLf
                                     + "  FROM " + strTABLE + "  WHERE 1 = 1  " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, gID, fieldSearch, false);
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            strName = dtCurrent.Rows[0][columnName].ToString();

                        }

                    }
                }
            }
            return strName;
        }

        public static void Load_DataGridByMonth(SplendidGrid grdMain, string month)
        {
            if (grdMain == null)
            {
                return;
            }
            if (START_MONTH == month)
            {
                grdMain.Columns[5].Visible = true;//Month 1
                grdMain.Columns[6].Visible = true;//Month 2
                grdMain.Columns[7].Visible = true;//Month 3
                grdMain.Columns[8].Visible = true;//Month 4
                grdMain.Columns[9].Visible = true;//Month 5
                grdMain.Columns[10].Visible = true;//Month 6
                grdMain.Columns[11].Visible = false;//Month 7
                grdMain.Columns[12].Visible = false;//Month 8
                grdMain.Columns[13].Visible = false;//Month 9
                grdMain.Columns[14].Visible = false;//Month 10
                grdMain.Columns[15].Visible = false;//Month 11
                grdMain.Columns[16].Visible = false;//Month 12
            }
            else if (END_MONTH == month)
            {
                grdMain.Columns[5].Visible = false;//Month 1
                grdMain.Columns[6].Visible = false;//Month 2
                grdMain.Columns[7].Visible = false;//Month 3
                grdMain.Columns[8].Visible = false;//Month 4
                grdMain.Columns[9].Visible = false;//Month 5
                grdMain.Columns[10].Visible = false;//Month 6
                grdMain.Columns[11].Visible = true;//Month 7
                grdMain.Columns[12].Visible = true;//Month 8
                grdMain.Columns[13].Visible = true;//Month 9
                grdMain.Columns[14].Visible = true;//Month 10
                grdMain.Columns[15].Visible = true;//Month 11
                grdMain.Columns[16].Visible = true;//Month 12
            }
            else
            {
                grdMain.Columns[5].Visible = true;//Month 1
                grdMain.Columns[6].Visible = true;//Month 2
                grdMain.Columns[7].Visible = true;//Month 3
                grdMain.Columns[8].Visible = true;//Month 4
                grdMain.Columns[9].Visible = true;//Month 5
                grdMain.Columns[10].Visible = true;//Month 6
                grdMain.Columns[11].Visible = true;//Month 7
                grdMain.Columns[12].Visible = true;//Month 8
                grdMain.Columns[13].Visible = true;//Month 9
                grdMain.Columns[14].Visible = true;//Month 10
                grdMain.Columns[15].Visible = true;//Month 11
                grdMain.Columns[16].Visible = true;//Month 12
            }
        }

        public static string Check_Kpi_Act_Result_Exist(string actualResultCode, string year, string monthPeriod, string maNhanVien, DbProviderFactory dbf)
        {
            string gID = string.Empty;
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID     " + ControlChars.CrLf
                                     + "  from vwB_KPI_ACTUAL_RESULT_Edit  WHERE 1 = 1     " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, actualResultCode, Sql.SqlFilterMode.Exact, "ACTUAL_RESULT_CODE");
                    Sql.AppendParameter(cmd, Sql.ToInteger(year), "YEAR", false);
                    Sql.AppendParameter(cmd, monthPeriod, Sql.SqlFilterMode.Exact, "MONTH_PERIOD");
                    Sql.AppendParameter(cmd, maNhanVien, Sql.SqlFilterMode.Exact, "MA_NHAN_VIEN");
                    cmd.CommandText += " ORDER BY ID";
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            gID = dtCurrent.Rows[0]["ID"].ToString();
                            return gID;
                        }

                    }
                }
            }
            return string.Empty;
        }

        public static string Check_Kpi_Act_Result_Detail_Exist(string actualResultCode, string year, string monthPeriod, string maNhanVien, string kpiCode, DbProviderFactory dbf)
        {
            string gID = string.Empty;
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID     " + ControlChars.CrLf
                                     + "  from vwB_KPI_ACT_RESULT_DETAIL_Edit  WHERE 1 = 1     " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    DataTable dtCurrent = new DataTable();
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, actualResultCode, Sql.SqlFilterMode.Exact, "ACTUAL_RESULT_CODE");
                    Sql.AppendParameter(cmd, Sql.ToInteger(year), "YEAR", false);
                    Sql.AppendParameter(cmd, monthPeriod, Sql.SqlFilterMode.Exact, "MONTH_PERIOD");
                    Sql.AppendParameter(cmd, maNhanVien, Sql.SqlFilterMode.Exact, "MA_NHAN_VIEN");
                    Sql.AppendParameter(cmd, kpiCode, Sql.SqlFilterMode.Exact, "KPI_CODE");
                    cmd.CommandText += " ORDER BY ID";
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            gID = dtCurrent.Rows[0]["ID"].ToString();
                            return gID;
                        }

                    }
                }
            }
            return string.Empty;
        }

        public static bool spM_GROUP_KPIS_Existed(Guid gID, Int32 nYEAR, Int32 nTYPE, string sBUSINESS_CODE, string sCONTRACT_TYPE_C, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            //string sSQL = "select count(*)           " + ControlChars.CrLf
            //                       + "  from vwM_GROUP_KPIS_Edit where 1=1 " + ControlChars.CrLf;

            string sSQL = "SELECT count(*) FROM M_GROUP_KPIS group_kpi     " + ControlChars.CrLf
                            + "  INNER JOIN M_GROUP_KPIS_CSTM group_kpi_cstm ON (group_kpi.ID = group_kpi_cstm.ID_C)   " + ControlChars.CrLf
                            +"   WHERE 1=1 AND STATUS = '1' AND DELETED = '0'  " + ControlChars.CrLf;

            if (!Sql.IsEmptyGuid(gID))
            {
                sSQL = sSQL + "  AND  [ID] <> '" + gID + "'";
            }

            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, nYEAR, "YEAR", false);
                Sql.AppendParameter(cmd, nTYPE, "TYPE", false);
                Sql.AppendParameter(cmd, sBUSINESS_CODE, Sql.SqlFilterMode.Exact, "BUSINESS_CODE");
                Sql.AppendParameter(cmd, sCONTRACT_TYPE_C, Sql.SqlFilterMode.Exact, "CONTRACT_TYPE_C");
                
                //Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Sql.AppendParameter(cmd, Sql.ToString(KPIs_Utils.ACTIVE), Sql.SqlFilterMode.Exact, "STATUS");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        //validate import phan giao ca nhan
        public static bool spM_USERS_Existed(string sUserCode, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwEMPLOYEES_Edit where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, sUserCode, Sql.SqlFilterMode.Exact, "USER_CODE_C");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        public static bool spM_USERS_IDENTIFY_Existed(string sIdentify, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwEMPLOYEES_Edit where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, sIdentify, "IDENTIFICATION_C");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }        

        public static string GenUseCodeTTS(IDbTransaction trn)
        {
            var userCode = "";
            while (true)
            {
                userCode = "TTS" + new Random().Next(0, 99999).ToString("00000");
                IDbConnection con = trn.Connection;
                string sSQL = "select count(*)           " + ControlChars.CrLf
                    + "  from vwEMPLOYEES_Edit" + ControlChars.CrLf
                    + "  where USER_CODE_C = '" + userCode + "'" + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.Transaction = trn;
                    cmd.CommandText = sSQL;
                    Int32 count = (Int32)cmd.ExecuteScalar();
                    if (count <= 0)
                        break;
                }                
            }

            return userCode;
        }

        public static bool spM_KPIs_Existed(string sUserCode, string sKPI_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwB_KPI_STANDARD_DETAILS_Edit  " + ControlChars.CrLf
                                   + "  where KPI_CODE = @KPI_CODE and KPI_STANDARD_ID IN ( " + ControlChars.CrLf
                                   + "  SELECT TOP 1 ID FROM  vwB_KPI_STANDARD_Edit where   " + ControlChars.CrLf
                                   + "  POSITION_ID IN (SELECT TOP 1 POSITION_ID_C FROM vwEMPLOYEES_Edit WHERE USER_CODE_C = @USER_CODE)  AND APPROVE_STATUS = @APPROVE_STATUS) " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE);
                Sql.AddParameter(cmd, "@USER_CODE", sUserCode);
                Sql.AddParameter(cmd, "@APPROVE_STATUS", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        /// <summary>
        /// Check org alloc existed
        /// </summary>
        /// <param name="nYEAR"></param>
        /// <param name="nPeriod"></param>
        /// <param name="sORGANIZATION_CODE"></param>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static bool spB_KPI_ORG_ALLOCATES_Existed(Int32 nYEAR, string nPeriod, string sORGANIZATION_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwB_KPI_ORG_ALLOCATES_Edit where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, nYEAR, "YEAR", false);
                Sql.AppendParameter(cmd, nPeriod, Sql.SqlFilterMode.Exact, "PERIOD");
                Sql.AppendParameter(cmd, sORGANIZATION_CODE, Sql.SqlFilterMode.Exact, "ORGANIZATION_CODE");
                //Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Sql.AppendParameter(cmd, Sql.ToString(KPIs_Utils.ACTIVE), Sql.SqlFilterMode.Exact, "STATUS");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        /// <summary>
        /// Check allocated user
        /// </summary>
        /// <param name="nYEAR"></param>
        /// <param name="nPeriod"></param>
        /// <param name="sUSER_CODE"></param>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static bool spB_KPI_ALLOCATES_Existed(Int32 nYEAR, string nPeriod, string sUSER_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwB_KPI_ALLOCATES_Edit where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, nYEAR, "YEAR", false);
                Sql.AppendParameter(cmd, nPeriod, Sql.SqlFilterMode.Exact, "PERIOD");
                Sql.AppendParameter(cmd, sUSER_CODE, Sql.SqlFilterMode.Exact, "MA_NHAN_VIEN");
                //Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Sql.AppendParameter(cmd, Sql.ToString(KPIs_Utils.ACTIVE), Sql.SqlFilterMode.Exact, "STATUS");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        public static bool spB_KPI_ALLOCATES_Existed(Int32 nYEAR, string firstPeriod, string endPeriod, string sUSER_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwB_KPI_ALLOCATES_Edit where 1=1           " + ControlChars.CrLf
                                   + " and (PERIOD = @F_PERIOD or PERIOD = @E_PERIOD)   ";
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AddParameter(cmd, "@F_PERIOD", firstPeriod);
                Sql.AddParameter(cmd, "@E_PERIOD", endPeriod);
                Sql.AppendParameter(cmd, nYEAR, "YEAR", false);
                //Sql.AppendParameter(cmd, nPeriod, Sql.SqlFilterMode.Exact, "PERIOD");
                Sql.AppendParameter(cmd, sUSER_CODE, Sql.SqlFilterMode.Exact, "MA_NHAN_VIEN");
                //Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Sql.AppendParameter(cmd, Sql.ToString(KPIs_Utils.ACTIVE), Sql.SqlFilterMode.Exact, "STATUS");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nYEAR"></param>
        /// <param name="nPeriod"></param>
        /// <param name="sUSER_CODE"></param>
        /// <param name="trn"></param>
        /// <returns></returns>
        public static bool spB_KPI_ALLOCATES_CN_Existed(Int32 nYEAR, string nPeriod, string sUSER_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)                                    " + ControlChars.CrLf
                                     + "  from vwEMPLOYEES_List E               " + ControlChars.CrLf
                                     + "  LEFT JOIN vwB_KPI_ALLOCATES_List A    " + ControlChars.CrLf
                                     + string.Format("  ON (A.EMPLOYEE_ID = E.ID  and PERIOD in ('{0}')     ", nPeriod) + ControlChars.CrLf
                                     + "  AND A.M_BUSINESS_CODE_C = E.M_BUSINESS_CODE_C         " + ControlChars.CrLf
                                     + "  AND A.POSITION_ID = E.POSITION_ID_C                   " + ControlChars.CrLf
                                     + "  AND convert(nvarchar(50), A.AREA_ID) = E.AREA_C       " + ControlChars.CrLf
                                     + "  AND A.CONTRACT_TYPE_C = E.CONTRACT_TYPE_C             " + ControlChars.CrLf
                                     + "  AND A.SENIORITY_C = E.SENIORITY_C                     " + ControlChars.CrLf
                                     + "  AND A.ORGANIZATION_ID = E.ORGANIZATION_ID_C)   where 1=1                      " + ControlChars.CrLf
                                     + " AND A.YEAR=@YEAR AND A.MA_NHAN_VIEN = @MA_NHAN_VIEN AND A.STATUS = @STATUS     " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AddParameter(cmd, "@YEAR", nYEAR);
                //Sql.AppendParameter(cmd, nPeriod, Sql.SqlFilterMode.Exact, "PERIOD");
                Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sUSER_CODE);
                //Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Sql.AddParameter(cmd, "@STATUS", Sql.ToString(KPIs_Utils.ACTIVE));
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        //validate import DVKD
        public static bool spM_ORGANIZATION_Existed(string sORGANIZATION_CODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from M_ORGANIZATION where 1=1 AND DELETED = 0 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, sORGANIZATION_CODE, Sql.SqlFilterMode.Exact, "ORGANIZATION_CODE");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        public static bool spM_GROUP_KPIS_Existed(string sTYPE, string sYEAR, string sKPI_CODE, string businessCode, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + " from vwM_GROUP_KPI_DETAILS_Edit " + ControlChars.CrLf
                                   + " WHERE GROUP_KPI_ID = (select TOP 1 ID from vwM_GROUP_KPIS_Edit where TYPE = @TYPE AND YEAR = @YEAR AND BUSINESS_CODE = @BUSINESS_CODE AND APPROVE_STATUS = @APPROVE_STATUS AND STATUS = @STATUS) " + ControlChars.CrLf
                                   + " AND KPI_CODE = @KPI_CODE" + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AddParameter(cmd, "@TYPE", sTYPE);
                Sql.AddParameter(cmd, "@YEAR", sYEAR);
                Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE);
                Sql.AddParameter(cmd, "@BUSINESS_CODE", businessCode);
                Sql.AddParameter(cmd, "@APPROVE_STATUS", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                Sql.AddParameter(cmd, "@STATUS", Sql.ToString(KPIs_Utils.ACTIVE));
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        internal static bool spM_KPIS_Existed(string kpiCODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwM_KPIS where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, kpiCODE, Sql.SqlFilterMode.Exact, "KPI_CODE");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        internal static bool spM_KPI_CONVERSION_RATE_Existed(string fromCODE, string toCODE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwM_KPI_CONVERSION_RATE where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, fromCODE, Sql.SqlFilterMode.Exact, "FROM_CODE");
                Sql.AppendParameter(cmd, toCODE, Sql.SqlFilterMode.Exact, "TO_CODE");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        internal static bool spB_KPI_ORG_ACTUAL_RESULT_Existed(int nYEAR, string month, string organizationCode, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)           " + ControlChars.CrLf
                                   + "  from vwB_KPI_ORG_ACTUAL_RESULT_Edit where 1=1 " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AppendParameter(cmd, nYEAR, "YEAR", false);
                Sql.AppendParameter(cmd, month, Sql.SqlFilterMode.Exact, "MONTH_PERIOD");
                Sql.AppendParameter(cmd, organizationCode, Sql.SqlFilterMode.Exact, "ORGANIZATION_CODE");
                Sql.AppendParameter(cmd, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, Sql.SqlFilterMode.Exact, "APPROVE_STATUS");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

        public static DataTable GetValueFromCode(string strCode, string strTABLE, string fieldSearch, DbProviderFactory dbf)
        {

            DataTable dtCurrent = new DataTable();
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "SELECT ID, KPI_CODE, KPI_NAME, UNIT " + ControlChars.CrLf
                                     + "  FROM " + strTABLE + "  WHERE 1 = 1  " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;
                    Sql.AppendParameter(cmd, strCode, fieldSearch);
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            return dtCurrent;

                        }

                    }
                }
            }
            return dtCurrent;
        }

        public static DataTable getTotalMonthOfEmployeeWithPeriod(Guid empID, DbProviderFactory dbf)
        {
            DataTable dtCurrent = new DataTable();
            using (IDbConnection con = dbf.CreateConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    string sSQL = "select  A.EMPLOYEE_ID , A.YEAR , A.PERIOD , A.ALLOCATE_NAME " + ControlChars.CrLf
                                      + " ,SUM(AD.MONTH_1) AS MONTH_1	,SUM(AD.MONTH_2) AS MONTH_2	,SUM(AD.MONTH_3) AS MONTH_3 " + ControlChars.CrLf
                                      + " ,SUM(AD.MONTH_4) AS MONTH_4	,SUM(AD.MONTH_5) AS MONTH_5	,SUM(AD.MONTH_6) AS MONTH_6 " + ControlChars.CrLf
                                      + " ,SUM(AD.MONTH_7) AS MONTH_7	,SUM(AD.MONTH_8) AS MONTH_8	,SUM(AD.MONTH_9) AS MONTH_9 " + ControlChars.CrLf
                                      + " ,SUM(AD.MONTH_10) AS MONTH_10	,SUM(AD.MONTH_11) AS MONTH_11	,SUM(AD.MONTH_12) AS MONTH_12 " + ControlChars.CrLf
                                      + "  from vwB_KPI_ALLOCATES_Edit A inner join vwB_KPI_ALLOCATE_DETAILS_Edit AD  " + ControlChars.CrLf
                                      + "  on (A.ID = AD.KPI_ALLOCATE_ID) where 1=1  and A.EMPLOYEE_ID = @EMPLOYEE_ID and A.APPROVE_STATUS = @APPROVE_STATUS " + ControlChars.CrLf
                                      + " GROUP BY     A.EMPLOYEE_ID, A.YEAR	,A.PERIOD	,A.ALLOCATE_NAME    " + ControlChars.CrLf;

                    cmd.CommandText = sSQL;
                    Sql.AddParameter(cmd, "@EMPLOYEE_ID", empID);
                    Sql.AddParameter(cmd, "@APPROVE_STATUS", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dtCurrent);
                        if (dtCurrent.Rows.Count > 0)
                        {
                            return dtCurrent;

                        }

                    }
                }
            }
            return dtCurrent;
        }

        public static string FormatCurrencyVND(string money)
        {
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
            float moneyParse = 0;
            float.TryParse(money, out moneyParse);
            return moneyParse.ToString("#,###", cul.NumberFormat);
        }

        public static string FormatFloat(string fl, string formatNumber)
        {
            return Sql.ToDecimal(fl).ToString(formatNumber);
        }

        public static bool IsNumber(string textNumber)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(textNumber);
        }

        public static void FormatNumberForDataGrid(DataGridItem item, string format_number)
        {
            if (item == null)
            {
                return;
            }
            TextBox txtMonth1 = (TextBox)item.FindControl("txtMonth1");
            if (txtMonth1 != null && txtMonth1.Text != string.Empty)
            {
                txtMonth1.Text = KPIs_Utils.FormatFloat(txtMonth1.Text, format_number);
            }
            TextBox txtMonth2 = (TextBox)item.FindControl("txtMonth2");
            if (txtMonth2 != null && txtMonth2.Text != string.Empty)
            {
                txtMonth2.Text = KPIs_Utils.FormatFloat(txtMonth2.Text, format_number);
            }
            TextBox txtMonth3 = (TextBox)item.FindControl("txtMonth3");
            if (txtMonth3 != null && txtMonth3.Text != string.Empty)
            {
                txtMonth3.Text = KPIs_Utils.FormatFloat(txtMonth3.Text, format_number);
            }
            TextBox txtMonth4 = (TextBox)item.FindControl("txtMonth4");
            if (txtMonth4 != null && txtMonth4.Text != string.Empty)
            {
                txtMonth4.Text = KPIs_Utils.FormatFloat(txtMonth4.Text, format_number);
            }
            TextBox txtMonth5 = (TextBox)item.FindControl("txtMonth5");
            if (txtMonth5 != null && txtMonth5.Text != string.Empty)
            {
                txtMonth5.Text = KPIs_Utils.FormatFloat(txtMonth5.Text, format_number);
            }
            TextBox txtMonth6 = (TextBox)item.FindControl("txtMonth6");
            if (txtMonth6 != null && txtMonth6.Text != string.Empty)
            {
                txtMonth6.Text = KPIs_Utils.FormatFloat(txtMonth6.Text, format_number);
            }
            TextBox txtMonth7 = (TextBox)item.FindControl("txtMonth7");
            if (txtMonth7 != null && txtMonth7.Text != string.Empty)
            {
                txtMonth7.Text = KPIs_Utils.FormatFloat(txtMonth7.Text, format_number);
            }
            TextBox txtMonth8 = (TextBox)item.FindControl("txtMonth8");
            if (txtMonth8 != null && txtMonth8.Text != string.Empty)
            {
                txtMonth8.Text = KPIs_Utils.FormatFloat(txtMonth8.Text, format_number);
            }
            TextBox txtMonth9 = (TextBox)item.FindControl("txtMonth9");
            if (txtMonth9 != null && txtMonth9.Text != string.Empty)
            {
                txtMonth9.Text = KPIs_Utils.FormatFloat(txtMonth9.Text, format_number);
            }
            TextBox txtMonth10 = (TextBox)item.FindControl("txtMonth10");
            if (txtMonth10 != null && txtMonth10.Text != string.Empty)
            {
                txtMonth10.Text = KPIs_Utils.FormatFloat(txtMonth10.Text, format_number);
            }
            TextBox txtMonth11 = (TextBox)item.FindControl("txtMonth11");
            if (txtMonth11 != null && txtMonth11.Text != string.Empty)
            {
                txtMonth11.Text = KPIs_Utils.FormatFloat(txtMonth11.Text, format_number);
            }
            TextBox txtMonth12 = (TextBox)item.FindControl("txtMonth12");
            if (txtMonth12 != null && txtMonth12.Text != string.Empty)
            {
                txtMonth12.Text = KPIs_Utils.FormatFloat(txtMonth12.Text, format_number);
            }
        }

        public static void FormatNumberForDataGridLabel(DataGridItem item, string format_number)
        {
            if (item == null)
            {
                return;
            }
            Label lblValueMonth = (Label)item.FindControl("lblValueMonth");
            HiddenField hdfKPI_TYPE = (HiddenField)item.FindControl("hdfKPI_TYPE");
            HiddenField hdfIS_ACCUMULATED = (HiddenField)item.FindControl("hdfIS_ACCUMULATED");
            string kpiTYPE = string.Empty;
            bool isLuyKe = false;
            if (hdfKPI_TYPE != null)
            {
                kpiTYPE = hdfKPI_TYPE.Value;
            }
            if (hdfIS_ACCUMULATED != null)
            {
                isLuyKe = Sql.ToBoolean(hdfIS_ACCUMULATED.Value);
            }

            double valueMonth = 0;
            if (lblValueMonth != null)
            {
                valueMonth = Sql.ToDouble(lblValueMonth.Text);
                lblValueMonth.Text = KPIs_Utils.FormatFloat(lblValueMonth.Text, format_number);
            }
            Label txtMonth1 = (Label)item.FindControl("txtMonth1");
            if (txtMonth1 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth1.Text) > valueMonth)
                    {
                        txtMonth1.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth1.Text) < valueMonth)
                        {
                            txtMonth1.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth1.Text) < valueMonth)
                        {
                            txtMonth1.CssClass = "warning right";
                        }
                    }

                }
                txtMonth1.Text = KPIs_Utils.FormatFloat(txtMonth1.Text, format_number);
            }
            Label txtMonth2 = (Label)item.FindControl("txtMonth2");
            if (txtMonth2 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth2.Text) > valueMonth)
                    {
                        txtMonth2.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth2.Text) < (valueMonth * 2))
                        {
                            txtMonth2.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth2.Text) < valueMonth)
                        {
                            txtMonth2.CssClass = "warning right";
                        }
                    }
                }
                txtMonth2.Text = KPIs_Utils.FormatFloat(txtMonth2.Text, format_number);
            }
            Label txtMonth3 = (Label)item.FindControl("txtMonth3");
            if (txtMonth3 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth3.Text) > valueMonth)
                    {
                        txtMonth3.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth3.Text) < (valueMonth * 3))
                        {
                            txtMonth3.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth3.Text) < valueMonth)
                        {
                            txtMonth3.CssClass = "warning right";
                        }
                    }
                }
                txtMonth3.Text = KPIs_Utils.FormatFloat(txtMonth3.Text, format_number);
            }
            Label txtMonth4 = (Label)item.FindControl("txtMonth4");
            if (txtMonth4 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth4.Text) > valueMonth)
                    {
                        txtMonth4.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth4.Text) < (valueMonth * 4))
                        {
                            txtMonth4.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth4.Text) < valueMonth)
                        {
                            txtMonth4.CssClass = "warning right";
                        }
                    }
                }
                txtMonth4.Text = KPIs_Utils.FormatFloat(txtMonth4.Text, format_number);
            }
            Label txtMonth5 = (Label)item.FindControl("txtMonth5");
            if (txtMonth5 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth5.Text) > valueMonth)
                    {
                        txtMonth5.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth5.Text) < (valueMonth * 5))
                        {
                            txtMonth5.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth5.Text) < valueMonth)
                        {
                            txtMonth5.CssClass = "warning right";
                        }
                    }
                }
                txtMonth5.Text = KPIs_Utils.FormatFloat(txtMonth5.Text, format_number);
            }
            Label txtMonth6 = (Label)item.FindControl("txtMonth6");
            if (txtMonth6 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth6.Text) > valueMonth)
                    {
                        txtMonth6.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth6.Text) < (valueMonth * 6))
                        {
                            txtMonth6.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth6.Text) < valueMonth)
                        {
                            txtMonth6.CssClass = "warning right";
                        }
                    }
                }
                txtMonth6.Text = KPIs_Utils.FormatFloat(txtMonth6.Text, format_number);
            }
            Label txtMonth7 = (Label)item.FindControl("txtMonth7");
            if (txtMonth7 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth7.Text) > valueMonth)
                    {
                        txtMonth7.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth7.Text) < (valueMonth * 7))
                        {
                            txtMonth7.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth7.Text) < valueMonth)
                        {
                            txtMonth7.CssClass = "warning right";
                        }
                    }
                }
                txtMonth7.Text = KPIs_Utils.FormatFloat(txtMonth7.Text, format_number);
            }
            Label txtMonth8 = (Label)item.FindControl("txtMonth8");
            if (txtMonth8 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth8.Text) > valueMonth)
                    {
                        txtMonth8.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth8.Text) < (valueMonth * 8))
                        {
                            txtMonth8.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth8.Text) < valueMonth)
                        {
                            txtMonth8.CssClass = "warning right";
                        }
                    }
                }
                txtMonth8.Text = KPIs_Utils.FormatFloat(txtMonth8.Text, format_number);
            }
            Label txtMonth9 = (Label)item.FindControl("txtMonth9");
            if (txtMonth9 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth9.Text) > valueMonth)
                    {
                        txtMonth9.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth9.Text) < (valueMonth * 9))
                        {
                            txtMonth9.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth9.Text) < valueMonth)
                        {
                            txtMonth9.CssClass = "warning right";
                        }
                    }
                }
                txtMonth9.Text = KPIs_Utils.FormatFloat(txtMonth9.Text, format_number);
            }
            Label txtMonth10 = (Label)item.FindControl("txtMonth10");
            if (txtMonth10 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth10.Text) > valueMonth)
                    {
                        txtMonth10.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth10.Text) < (valueMonth * 10))
                        {
                            txtMonth10.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth10.Text) < valueMonth)
                        {
                            txtMonth10.CssClass = "warning right";
                        }
                    }
                }
                txtMonth10.Text = KPIs_Utils.FormatFloat(txtMonth10.Text, format_number);
            }
            Label txtMonth11 = (Label)item.FindControl("txtMonth11");
            if (txtMonth11 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth11.Text) > valueMonth)
                    {
                        txtMonth11.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth11.Text) < (valueMonth * 11))
                        {
                            txtMonth11.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth11.Text) < valueMonth)
                        {
                            txtMonth11.CssClass = "warning right";
                        }
                    }
                }
                txtMonth11.Text = KPIs_Utils.FormatFloat(txtMonth11.Text, format_number);
            }
            Label txtMonth12 = (Label)item.FindControl("txtMonth12");
            if (txtMonth12 != null)
            {
                if (KPIs_Utils.BAD_KPIs.Equals(kpiTYPE))
                {
                    if (Sql.ToDouble(txtMonth12.Text) > valueMonth)
                    {
                        txtMonth12.CssClass = "warning-noxau right";
                    }
                }
                else
                {
                    if (isLuyKe)
                    {
                        if (Sql.ToDouble(txtMonth12.Text) < (valueMonth * 12))
                        {
                            txtMonth12.CssClass = "warning-accumulated right";
                        }
                    }
                    else
                    {
                        if (Sql.ToDouble(txtMonth12.Text) < valueMonth)
                        {
                            txtMonth12.CssClass = "warning right";
                        }
                    }
                }
                txtMonth12.Text = KPIs_Utils.FormatFloat(txtMonth12.Text, format_number);
            }
        }


        public static DataTable getPercentFinalTotal(Guid empID, string fromMonth, string toMonth, DbProviderFactory dbf, bool isManager = false)
        {
            DataTable dtCurrent = new DataTable();
            using (IDbConnection con = dbf.CreateConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    if (!isManager)
                    {
                        string sSQL = " select top 1 year , month_period  " + ControlChars.CrLf
                                    + " , actual_result_code, employee_id " + ControlChars.CrLf
                                    + " , sum(percent_sync_value * ratio) as percent_sync_total                   " + ControlChars.CrLf
                                    + " , (round(sum(percent_final_value * ratio)/100, 2)) as percent_final_total " + ControlChars.CrLf
                                    + " from vwB_KPI_ACT_RESULT_DETAIL_Edit where 1=1 AND [YEAR] = YEAR(GETDATE())          " + ControlChars.CrLf
                                    + " AND EMPLOYEE_ID = @EMPLOYEE_ID AND MONTH_PERIOD BETWEEN @FROM_MONTH AND @TO_MONTH   " + ControlChars.CrLf
                                    + " group by year, month_period, actual_result_code, employee_id" + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        Sql.AddParameter(cmd, "@EMPLOYEE_ID", empID);
                        Sql.AddParameter(cmd, "@FROM_MONTH", fromMonth);
                        Sql.AddParameter(cmd, "@TO_MONTH", toMonth);
                        //Sql.AddParameter(cmd, "@APPROVE_STATUS", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                return dtCurrent;
                            }
                        }
                    }
                }
            }
            return dtCurrent;
        }


        public static double getPercentFinalTotalPeriod(Guid empID, int year, string fromMonth, string toMonth, DbProviderFactory dbf)
        {
            double returnValue = 0;
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spKPI_EMP_CAL_PERCENTAGE_PERIOD";

                    IDbDataParameter parID = Sql.AddParameter(cmd, "@gEmployeeID", empID);
                    IDbDataParameter parYear = Sql.AddParameter(cmd, "@iYear", year);
                    IDbDataParameter parFROMMONTH = Sql.AddParameter(cmd, "@sFrom_MONTH_PERIOD", fromMonth, 2);
                    IDbDataParameter parTOMONTH = Sql.AddParameter(cmd, "@sTo_MONTH_PERIOD", toMonth, 2);
                    IDbDataParameter parReturn = Sql.AddParameter(cmd, "@fSUM_PERCENT_FINAL_VALUE", returnValue);

                    parReturn.Direction = ParameterDirection.InputOutput;
                    cmd.ExecuteNonQuery();

                    returnValue = Sql.ToDouble(parReturn.Value);
                }
            }
            return returnValue;
        }

        public static void NotificationKPIsLessThanStandard(string month, string kpiName)
        {
            SplendidControl control = new SplendidControl();
            Guid gID = Guid.Empty;
            SqlProcs.spB_NOTIFICATION_Update(ref gID
                                , Security.USER_ID
                                , Security.TEAM_ID
                                , string.Empty
                                , string.Format("Warning_{0}", DateTime.Now.Date.ToString("dd_MM_yyyy"))
                                , DateTime.Now
                                , "Warning"
                                , control.GetL10n().Term(".WARNING_KPI_LESS_THAN_STD")
                                , string.Format("KPIs: {0} . Month: {1} - {2}", kpiName, month, control.GetL10n().Term(".WARNING_KPI_LESS_THAN_STD"))
                                , string.Empty
                                , 1
                                , Security.USER_ID//fromID
                                , Security.USER_ID//toID
                                , string.Empty
                                , "12" //Notify_level =12 send mail
                                , KPIs_Utils.ACTIVE
                                , string.Empty);
        }

        public static void SendEmail(HttpApplicationState Application, string sFromAddress, string sFromName, string sToAddress, string sToName, string sCCAddress, string sCCName, string sSubject, string sBody)
        {
            var sMAIL_SMTPSERVER = Sql.ToString(Application["CONFIG.smtpserver"]);
            var nMAIL_SMTPPORT = Sql.ToInteger(Application["CONFIG.smtpport"]);
            var bMAIL_SMTPAUTH_REQ = Sql.ToBoolean(Application["CONFIG.smtpauth_req"]);
            var bMAIL_SMTPSSL = Sql.ToBoolean(Application["CONFIG.smtpssl"]);
            var sMAIL_SMTPUSER = Sql.ToString(Application["CONFIG.smtpuser"]);
            var sMAIL_SMTPPASS = Sql.ToString(Application["CONFIG.smtppass"]);
            string sX509Certificate = Sql.ToString(Application["CONFIG.smtpcertificate"]);
            Guid gINBOUND_EMAIL_KEY = Sql.ToGuid(Application["CONFIG.InboundEmailKey"]);
            Guid gINBOUND_EMAIL_IV = Sql.ToGuid(Application["CONFIG.InboundEmailIV"]);
            sMAIL_SMTPPASS = Security.DecryptPassword(sMAIL_SMTPPASS, gINBOUND_EMAIL_KEY, gINBOUND_EMAIL_IV);
            SplendidMailClient client = new SplendidMailSmtp(Application, sMAIL_SMTPSERVER, nMAIL_SMTPPORT, bMAIL_SMTPAUTH_REQ, bMAIL_SMTPSSL, sMAIL_SMTPUSER, sMAIL_SMTPPASS, sX509Certificate);

            MailMessage mail = new MailMessage();
            MailAddress addr = null;
            MailAddress addrCC = null;
            if (Sql.IsEmptyString(sFromName))
                mail.From = new MailAddress(sFromAddress);
            else
                mail.From = new MailAddress(sFromAddress, sFromName);

            if (!Sql.IsEmptyString(sToAddress))
            {
                if (Sql.IsEmptyString(sToName))
                    addr = new MailAddress(sToAddress);
                else
                    addr = new MailAddress(sToAddress, sToName);

                mail.To.Add(addr);
            }

            if (!Sql.IsEmptyString(sCCAddress))
            {
                if (Sql.IsEmptyString(sCCName))
                    addrCC = new MailAddress(sCCAddress);
                else
                    addrCC = new MailAddress(sCCAddress, sCCName);
                mail.CC.Add(addrCC);
            }

            var emailDefault = Sql.ToString(Application["CONFIG.EmailSystemCC"]) ?? "abc@gmail.com";
            mail.CC.Add(new MailAddress(emailDefault));


            mail.Subject = sSubject;
            mail.Body = sBody;
            client.Send(mail);
        }

        public static double RoundNumber(double value)
        {
            return value;
            //double decimalpoints = Math.Abs(value - Math.Floor(value));
            //if (decimalpoints > 0.5)
            //    return Math.Ceiling(value);
            //else
            //    return Math.Floor(value);
        }

        public static decimal RoundNumber(decimal value)
        {
            return value;
        }

        public static double ConvertBytesToMegabytes(long bytes)
        {
            return Math.Round((bytes / 1024f) / 1024f, 0);
        }

        public static double ConvertKilobytesToMegabytes(long kilobytes)
        {
            return Math.Round(kilobytes / 1024f, 0);
        }

        public static bool spCheckApproval(Guid gID, string sTABLE, IDbTransaction trn)
        {
            bool found = false;
            IDbConnection con = trn.Connection;
            string sSQL = "select count(*)                               " + ControlChars.CrLf
                                   + string.Format("  from {0} where 1=1 ", sTABLE) + ControlChars.CrLf
                                   + "  and (APPROVED_BY = @APPROVED_BY or CREATED_BY_ID = @CREATED_BY_ID) " + ControlChars.CrLf;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandText = sSQL;
                Sql.AddParameter(cmd, "@APPROVED_BY", Security.USER_ID);
                Sql.AddParameter(cmd, "@CREATED_BY_ID", Security.USER_ID);
                Sql.AppendParameter(cmd, gID, "ID");
                Int32 count = (Int32)cmd.ExecuteScalar();
                if (count > 0)
                {
                    found = true;
                }
            }
            return found;
        }

    }
}