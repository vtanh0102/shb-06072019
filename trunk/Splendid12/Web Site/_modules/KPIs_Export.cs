﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SplendidCRM._modules
{
    public class KPIs_Export : SplendidControl
    {
        /// <summary>
        /// Bao cao thu thuan theo CV
        /// </summary>
        /// <param name="dtCurrent"></param>
        public static void ExportExcel(DataTable dtCurrent, string templatePath, string strYEAR)
        {
            HttpResponse Response = HttpContext.Current.Response;
            SplendidControl control = new SplendidControl();
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(6);
            ICell cell_00 = iRowHeader.GetCell(8);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);
            iRowHeader = sheet.GetRow(7);
            cell_00 = iRowHeader.GetCell(8);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYEAR);

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;
            //Qty total
            double totalValue1 = 0;
            double totalValue2 = 0;
            double totalValue3 = 0;
            double totalValue4 = 0;
            double totalValue5 = 0;
            double totalValue6 = 0;
            double totalThuThuan = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var userCode = row["USER_CODE"].ToString();
                var fullName = row["FULL_NAME"].ToString();
                var positionName = row["POSITION_NAME"].ToString();
                var branchName = row["MAIN_POS_NAME"].ToString();
                var departmentName = row["POS_NAME"].ToString();
                var regionName = row["REGION_NAME"].ToString();
                var monthPeriod = row["MONTH_PERIOD"].ToString();

                //Quality
                double finalValue1 = Sql.ToDouble(row["FINAL_VALUE_1"]);//tin dung
                double finalValue2 = Sql.ToDouble(row["FINAL_VALUE_2"]);//huy dong
                double finalValue3 = Sql.ToDouble(row["FINAL_VALUE_3"]);//the

                double finalValue4 = Sql.ToDouble(row["FINAL_VALUE_4"]);//Ebanking
                double finalValue5 = Sql.ToDouble(row["FINAL_VALUE_5"]);//Other
                //double finalValue6 = Sql.ToDouble(row["FINAL_VALUE_6"]);
                //double finalValue7 = Sql.ToDouble(row["FINAL_VALUE_7"]);

                var remark = row["REMARK"].ToString();

                //Create a row and put some cells in it. Rows are 0 based.
                int index = 11 + i;
                IRow iRow = sheet.CreateRow(index);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                sheet.AutoSizeColumn(9);
                sheet.AutoSizeColumn(10);
                sheet.AutoSizeColumn(11);
                sheet.AutoSizeColumn(12);
                sheet.AutoSizeColumn(13);
                sheet.AutoSizeColumn(14);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Ma nhan su
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(userCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_2 = iRow.CreateCell(2);//ten nhan su
                cell_2.SetCellType(CellType.String);
                cell_2.SetCellValue(fullName);
                cell_2.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_3 = iRow.CreateCell(3);//chuc danh
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(positionName);
                cell_3.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_4 = iRow.CreateCell(4);//chi nhanh
                cell_4.SetCellType(CellType.String);
                cell_4.SetCellValue(branchName);
                cell_4.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_5 = iRow.CreateCell(5);//phong
                cell_5.SetCellType(CellType.String);
                cell_5.SetCellValue(departmentName);
                cell_5.CellStyle = style;

                //Create a cell and put a value in it.
                cell_5 = iRow.CreateCell(6);//Vung
                cell_5.SetCellType(CellType.String);
                cell_5.SetCellValue(regionName);
                cell_5.CellStyle = style;

                //Create a cell and put a value in it.
                cell_5 = iRow.CreateCell(7);//Ky
                cell_5.SetCellType(CellType.String);
                cell_5.SetCellValue(monthPeriod);
                cell_5.CellStyle = style;

                //Create a cell and put a value in it. finalValue1
                ICell cell_10 = iRow.CreateCell(8);//Tin dung
                cell_10.SetCellType(CellType.Numeric);
                cell_10.SetCellValue(finalValue1);
                cell_10.CellStyle = style;
                totalValue1 = totalValue1 + Sql.ToDouble(finalValue1);

                //Create a cell and put a value in it. finalValue2
                ICell cell_11 = iRow.CreateCell(9);//Huy dong
                cell_11.SetCellType(CellType.Numeric);
                cell_11.SetCellValue(finalValue2);
                cell_11.CellStyle = style;
                totalValue2 = totalValue2 + Sql.ToDouble(finalValue2);

                //Create a cell and put a value in it. finalValue3
                ICell cell_12 = iRow.CreateCell(10);//The
                cell_12.SetCellType(CellType.Numeric);
                cell_12.SetCellValue(finalValue3);
                cell_12.CellStyle = style;
                totalValue3 = totalValue3 + Sql.ToDouble(finalValue3);

                //Create a cell and put a value in it. finalValue4
                ICell cell_13 = iRow.CreateCell(11);//Ebanking
                cell_13.SetCellType(CellType.Numeric);
                cell_13.SetCellValue(finalValue4);
                cell_13.CellStyle = style;
                totalValue4 = totalValue4 + Sql.ToDouble(finalValue4);

                //Create a cell and put a value in it. finalValue4
                ICell cell_14 = iRow.CreateCell(12);//Khac
                cell_14.SetCellType(CellType.Numeric);
                cell_14.SetCellValue(finalValue5);
                cell_14.CellStyle = style;
                totalValue5 = totalValue5 + Sql.ToDouble(finalValue5); ;

                //Create a cell and put a value in it. Total thu thuan
                totalThuThuan = (Sql.ToDouble(finalValue1) + Sql.ToDouble(finalValue2) + Sql.ToDouble(finalValue3) + +Sql.ToDouble(finalValue4) + Sql.ToDouble(finalValue5));
                ICell cell_15 = iRow.CreateCell(13);
                cell_15.SetCellType(CellType.Numeric);
                cell_15.SetCellValue(totalThuThuan);
                cell_15.CellStyle = style;
                totalValue6 = totalValue6 + totalThuThuan;

                //Create a cell and put a value in it.
                ICell cell_16 = iRow.CreateCell(14);
                cell_16.SetCellType(CellType.String);
                cell_16.SetCellValue(remark);
                cell_16.CellStyle = style;

                i++;
                rowIndex = index;
            }

            //Create row total qty
            ICellStyle cellStyleTotal = workbook.CreateCellStyle();
            cellStyleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.Alignment = HorizontalAlignment.Right;

            IFont fontTotal = workbook.CreateFont();
            fontTotal.FontName = "Times New Roman";
            fontTotal.IsBold = true;
            cellStyleTotal.SetFont(fontTotal);

            IRow iRowTotal = sheet.CreateRow(rowIndex + 1);
            for (int j = 0; j <= 14; j++)
            {
                ICell cell = iRowTotal.CreateCell(j);
                cell.CellStyle = cellStyleTotal;
                if (j == 0)
                {
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(string.Format("{0} = ", control.GetL10n().Term("Quotes.LBL_LIST_SUBTOTAL")));
                }
            }

            var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 0, 7);
            sheet.AddMergedRegion(cellRange);

            //Create a cell total final value 1
            ICell cell_Total_Qty = iRowTotal.CreateCell(8);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue1);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //final value 2
            cell_Total_Qty = iRowTotal.CreateCell(9);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue2);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //final value 3
            cell_Total_Qty = iRowTotal.CreateCell(10);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue3);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //final value 4
            cell_Total_Qty = iRowTotal.CreateCell(11);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue4);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;

            //final value 5
            cell_Total_Qty = iRowTotal.CreateCell(12);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue5);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;

            //final value 6
            cell_Total_Qty = iRowTotal.CreateCell(13);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue6);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;


            //set print area
            workbook.SetPrintArea(0, 0, 14, 0, rowIndex + 1);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao dvkd
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        public static void ExportExcelOrg(DataTable dtCurrent, string templatePath, string strYEAR)
        {
            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(5);
            ICell cell_00 = iRowHeader.CreateCell(6);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);

            iRowHeader = sheet.GetRow(6);
            cell_00 = iRowHeader.CreateCell(6);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYEAR);

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;
            //Qty total
            double totalValue1 = 0;
            double totalValue2 = 0;
            double totalValue3 = 0;
            double totalValue4 = 0;
            double totalValue5 = 0;
            double totalValue6 = 0;
            double totalThuThuan = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var posCode = row["POS_CODE"].ToString();
                var posName = row["POS_NAME"].ToString();
                var regionName = row["REGION_NAME"].ToString();
                //var monthPeriod = row["MONTH_PERIOD"].ToString();

                //Quality                
                double finalValue1 = Sql.ToDouble(row["FINAL_VALUE_1"]);//tin dung
                double finalValue2 = Sql.ToDouble(row["FINAL_VALUE_2"]);//huy dong
                double finalValue3 = Sql.ToDouble(row["FINAL_VALUE_3"]);//the

                double finalValue4 = Sql.ToDouble(row["FINAL_VALUE_4"]);
                double finalValue5 = Sql.ToDouble(row["FINAL_VALUE_5"]);
                //double finalValue6 = Sql.ToDouble(row["FINAL_VALUE_6"]);
                //double finalValue7 = Sql.ToDouble(row["FINAL_VALUE_7"]);

                var remark = row["REMARK"].ToString();

                //Create a row and put some cells in it. Rows are 0 based.
                int index = 11 + i;
                IRow iRow = sheet.CreateRow(index);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                //sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                sheet.AutoSizeColumn(8);
                sheet.AutoSizeColumn(9);
                sheet.AutoSizeColumn(10);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Ma DVKD
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(2);//Ten DVKD
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(3);//Vung
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                //cell_1 = iRow.CreateCell(4);//Ky
                //cell_1.SetCellType(CellType.String);
                //cell_1.SetCellValue(monthPeriod);
                //cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue1
                ICell cell_4 = iRow.CreateCell(4);//Tin dung
                cell_4.SetCellType(CellType.Numeric);
                cell_4.SetCellValue(finalValue1);
                cell_4.CellStyle = style;
                totalValue1 = totalValue1 + Sql.ToDouble(finalValue1);

                //Create a cell and put a value in it. finalValue2
                ICell cell_5 = iRow.CreateCell(5);//Huy dong
                cell_5.SetCellType(CellType.Numeric);
                cell_5.SetCellValue(finalValue2);
                cell_5.CellStyle = style;
                totalValue2 = totalValue2 + Sql.ToDouble(finalValue2);

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(6);//The
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(finalValue3);
                cell_6.CellStyle = style;
                totalValue3 = totalValue3 + Sql.ToDouble(finalValue3);

                //Create a cell and put a value in it. finalValue4
                ICell cell_7 = iRow.CreateCell(7);//Ebanking
                cell_7.SetCellType(CellType.Numeric);
                cell_7.SetCellValue(finalValue4);
                cell_7.CellStyle = style;
                totalValue4 = totalValue4 + Sql.ToDouble(finalValue4);

                //Create a cell and put a value in it. finalValue5
                ICell cell_8 = iRow.CreateCell(8);//Khac
                cell_8.SetCellType(CellType.Numeric);
                cell_8.SetCellValue(finalValue5);
                cell_8.CellStyle = style;
                totalValue5 = totalValue5 + Sql.ToDouble(finalValue5);

                //Create a cell and put a value in it. Total thu thuan
                totalThuThuan = (Sql.ToDouble(finalValue1) + Sql.ToDouble(finalValue2) + Sql.ToDouble(finalValue3) + Sql.ToDouble(finalValue4) + Sql.ToDouble(finalValue5));
                ICell cell_9 = iRow.CreateCell(9);
                cell_9.SetCellType(CellType.Numeric);
                cell_9.SetCellValue(totalThuThuan);
                cell_9.CellStyle = style;
                totalValue6 = totalValue6 + totalThuThuan;

                //Create a cell and put a value in it.
                ICell cell_10 = iRow.CreateCell(10);
                cell_10.SetCellType(CellType.String);
                cell_10.SetCellValue(remark);
                cell_10.CellStyle = style;

                i++;
                rowIndex = index;
            }

            //Create row total qty
            ICellStyle cellStyleTotal = workbook.CreateCellStyle();
            cellStyleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.Alignment = HorizontalAlignment.Right;

            IFont fontTotal = workbook.CreateFont();
            fontTotal.FontName = "Times New Roman";
            fontTotal.IsBold = true;
            cellStyleTotal.SetFont(fontTotal);
            SplendidControl control = new SplendidControl();

            IRow iRowTotal = sheet.CreateRow(rowIndex + 1);
            for (int j = 0; j <= 10; j++)
            {
                ICell cell = iRowTotal.CreateCell(j);
                cell.CellStyle = cellStyleTotal;
                if (j == 0)
                {
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(string.Format("{0} = ", control.GetL10n().Term("Quotes.LBL_LIST_SUBTOTAL")));
                }
            }

            var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 0, 3);
            sheet.AddMergedRegion(cellRange);

            //Create a cell total 
            ICell cell_Total_Qty = iRowTotal.CreateCell(4);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue1);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty2
            cell_Total_Qty = iRowTotal.CreateCell(5);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue2);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty3
            cell_Total_Qty = iRowTotal.CreateCell(6);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue3);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty4
            cell_Total_Qty = iRowTotal.CreateCell(7);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue4);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty5
            cell_Total_Qty = iRowTotal.CreateCell(8);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue5);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty6
            cell_Total_Qty = iRowTotal.CreateCell(9);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue6);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;

            //set print area
            workbook.SetPrintArea(0, 0, 10, 0, rowIndex + 1);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao vung
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        public static void ExportExcelRegion(DataTable dtCurrent, string templatePath, string strYEAR)
        {
            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(6);
            ICell cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);

            iRowHeader = sheet.GetRow(7);
            cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYEAR);

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;
            //Qty total
            double totalValue1 = 0;
            double totalValue2 = 0;
            double totalValue3 = 0;
            double totalValue4 = 0;
            double totalValue5 = 0;
            double totalValue6 = 0;
            double totalThuThuan = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var regionName = row["REGION_NAME"].ToString();
                //var monthPeriod = row["MONTH_PERIOD"].ToString();

                //Quality
                double finalValue1 = Sql.ToDouble(row["FINAL_VALUE_1"]);//tin dung
                double finalValue2 = Sql.ToDouble(row["FINAL_VALUE_2"]);//huy dong
                double finalValue3 = Sql.ToDouble(row["FINAL_VALUE_3"]);//the

                double finalValue4 = Sql.ToDouble(row["FINAL_VALUE_4"]);//Ebanking
                double finalValue5 = Sql.ToDouble(row["FINAL_VALUE_5"]);//Other
                //double finalValue6 = Sql.ToDouble(row["FINAL_VALUE_6"]);
                //double finalValue7 = Sql.ToDouble(row["FINAL_VALUE_7"]);

                var remark = row["REMARK"].ToString();

                //Create a row and put some cells in it. Rows are 0 based.
                int index = 11 + i;
                IRow iRow = sheet.CreateRow(index);
                //sheet.AutoSizeColumn(2);
                //sheet.AutoSizeColumn(3);
                //sheet.AutoSizeColumn(4);
                //sheet.AutoSizeColumn(5);
                //sheet.AutoSizeColumn(6);
                //sheet.AutoSizeColumn(7);
                //sheet.AutoSizeColumn(8);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Ten khu vuc
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //cell_1 = iRow.CreateCell(2);//Ky
                //cell_1.SetCellType(CellType.String);
                //cell_1.SetCellValue(monthPeriod);
                //cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue1
                ICell cell_4 = iRow.CreateCell(2);//Tin dung
                cell_4.SetCellType(CellType.Numeric);
                cell_4.SetCellValue(finalValue1);
                cell_4.CellStyle = style;
                totalValue1 = totalValue1 + Sql.ToDouble(finalValue1);

                //Create a cell and put a value in it. finalValue2
                ICell cell_5 = iRow.CreateCell(3);//Huy dong
                cell_5.SetCellType(CellType.Numeric);
                cell_5.SetCellValue(finalValue2);
                cell_5.CellStyle = style;
                totalValue2 = totalValue2 + Sql.ToDouble(finalValue2);

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(4);//The
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(finalValue3);
                cell_6.CellStyle = style;
                totalValue3 = totalValue3 + Sql.ToDouble(finalValue3);

                //Create a cell and put a value in it. finalValue4
                ICell cell_7 = iRow.CreateCell(5);//Ebanking
                cell_7.SetCellType(CellType.Numeric);
                cell_7.SetCellValue(finalValue4);
                cell_7.CellStyle = style;
                totalValue4 = totalValue4 + Sql.ToDouble(finalValue4);

                //Create a cell and put a value in it. finalValue5
                ICell cell_8 = iRow.CreateCell(6);//Khac
                cell_8.SetCellType(CellType.Numeric);
                cell_8.SetCellValue(finalValue5);
                cell_8.CellStyle = style;
                totalValue5 = totalValue5 + finalValue5;

                //Create a cell and put a value in it. Total thu thuan
                totalThuThuan = (Sql.ToDouble(finalValue1) + Sql.ToDouble(finalValue2) + Sql.ToDouble(finalValue3) + Sql.ToDouble(finalValue4) + Sql.ToDouble(finalValue5));
                ICell cell_9 = iRow.CreateCell(7);
                cell_9.SetCellType(CellType.Numeric);
                cell_9.SetCellValue(totalThuThuan);
                cell_9.CellStyle = style;
                totalValue6 = totalValue6 + totalThuThuan;

                //Create a cell and put a value in it.
                ICell cell_10 = iRow.CreateCell(8);
                cell_10.SetCellType(CellType.String);
                cell_10.SetCellValue(remark);
                cell_10.CellStyle = style;

                i++;
                rowIndex = index;
            }

            //Create row total qty
            ICellStyle cellStyleTotal = workbook.CreateCellStyle();
            cellStyleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            cellStyleTotal.Alignment = HorizontalAlignment.Right;

            IFont fontTotal = workbook.CreateFont();
            fontTotal.FontName = "Times New Roman";
            fontTotal.IsBold = true;
            cellStyleTotal.SetFont(fontTotal);
            SplendidControl control = new SplendidControl();

            IRow iRowTotal = sheet.CreateRow(rowIndex + 1);
            for (int j = 0; j <= 8; j++)
            {
                ICell cell = iRowTotal.CreateCell(j);
                cell.CellStyle = cellStyleTotal;
                if (j == 0)
                {
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(string.Format("{0} = ", control.GetL10n().Term("Quotes.LBL_LIST_SUBTOTAL")));
                }
            }

            var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 0, 1);
            sheet.AddMergedRegion(cellRange);

            //Create a cell total 
            ICell cell_Total_Qty = iRowTotal.CreateCell(2);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue1);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty2
            cell_Total_Qty = iRowTotal.CreateCell(3);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue2);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty3
            cell_Total_Qty = iRowTotal.CreateCell(4);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue3);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty4
            cell_Total_Qty = iRowTotal.CreateCell(5);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue4);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty5
            cell_Total_Qty = iRowTotal.CreateCell(6);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue5);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;
            //qty6
            cell_Total_Qty = iRowTotal.CreateCell(7);
            cell_Total_Qty.SetCellType(CellType.Numeric);
            cell_Total_Qty.SetCellValue(totalValue6);
            cellStyleTotal.DataFormat = df.GetFormat("#,##0.00");
            cell_Total_Qty.CellStyle = cellStyleTotal;

            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            //sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            //set print area
            workbook.SetPrintArea(0, 0, 8, 0, rowIndex + 1);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao Top Bottom Phong
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        /// <param name="orgType"></param>
        /// <param name="strYear"></param>
        internal static void ExportTopBottomDepartment(DataTable dtCurrent, string templatePath, string orgType, string strYear)
        {

            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(5);
            ICell cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);//Nguoi xuat bao cao

            iRowHeader = sheet.GetRow(6);
            cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(orgType);//Loai phong

            iRowHeader = sheet.GetRow(7);
            cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYear);//Nam


            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var posCode = row["POS_CODE"].ToString();//Ma phong giao dich
                var posName = row["POS_NAME"].ToString();//Ten phong giao dich
                var mainPosCode = row["MAIN_POS_CODE"].ToString();//Ma chi nhanh
                var mainPosName = row["MAIN_POS_NAME"].ToString();//Ten chi nhanh

                var regionName = row["REGION_NAME"].ToString();//vung
                var areaName = row["AREA_NAME"].ToString();//Khu vuc
                var monthPeriod = row["MONTH_PERIOD"].ToString();//Ky

                //Quality
                double percentFinalTotal = Sql.ToDouble(row["PERCENT_FINAL_TOTAL"]);//Phan tram hoan thanh

                var remark = row["REMARK"].ToString();

                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                //sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                //Create a row and put some cells in it. Rows are 0 based.
                int index = 11 + i;
                IRow iRow = sheet.CreateRow(index);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Phong
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(2);//Chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(3);//vung
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(4);//Khu vuc
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(areaName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(5);//Ky
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(monthPeriod);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(6);//Phan tram hoan thanh
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(percentFinalTotal);
                cell_6.CellStyle = style;
                rowIndex = index;
                i++;
            }

            //set print area
            workbook.SetPrintArea(0, 0, 6, 0, rowIndex);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao Top Bottom chi nhanh
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        /// <param name="orgType"></param>
        /// <param name="strYear"></param>
        internal static void ExportTopBottomBranch(DataTable dtCurrent, string templatePath, string orgType, string strYear)
        {

            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(5);
            ICell cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);//Nguoi xuat bao cao

            iRowHeader = sheet.GetRow(6);
            cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(orgType);//Loai phong

            iRowHeader = sheet.GetRow(7);
            cell_00 = iRowHeader.CreateCell(3);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYear);//Nam


            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var posCode = row["POS_CODE"].ToString();//Ma phong giao dich
                var posName = row["POS_NAME"].ToString();//Ten phong giao dich
                var mainPosCode = row["MAIN_POS_CODE"].ToString();//Ma chi nhanh
                var mainPosName = row["MAIN_POS_NAME"].ToString();//Ten chi nhanh

                var regionName = row["REGION_NAME"].ToString();//vung
                var areaName = row["AREA_NAME"].ToString();//Khu vuc
                var monthPeriod = row["MONTH_PERIOD"].ToString();//Ky

                //Quality
                double percentFinalTotal = Sql.ToDouble(row["PERCENT_FINAL_TOTAL"]);//Phan tram hoan thanh

                var remark = row["REMARK"].ToString();

                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                //sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                //Create a row and put some cells in it. Rows are 0 based.
                int index = 11 + i;
                IRow iRow = sheet.CreateRow(index);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(2);//vung
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(3);//Khu vuc
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(areaName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(4);//Ky
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(monthPeriod);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(5);//Phan tram hoan thanh
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(percentFinalTotal);
                cell_6.CellStyle = style;
                rowIndex = index;
                i++;
            }

            //set print area
            workbook.SetPrintArea(0, 0, 5, 0, rowIndex);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao Top Ca nhan
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        internal static void ExportExcelTopCaNhan(DataTable dtCurrent, string templatePath, string strYEAR)
        {

            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(4);
            ICell cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);

            iRowHeader = sheet.GetRow(5);
            cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYEAR);

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var regionName = row["REGION_NAME"].ToString();
                var posCode = row["POS_CODE"].ToString();
                var posName = row["POS_NAME"].ToString();
                var mainPosCode = row["MAIN_POS_CODE"].ToString();
                var mainPosName = row["MAIN_POS_NAME"].ToString();

                var userCode = row["USER_CODE"].ToString();
                var fullName = row["FULL_NAME"].ToString();

                //Quality
                double percentFinalTotal = Sql.ToDouble(row["PERCENT_FINAL_TOTAL"]);

                var remark = row["REMARK"].ToString();

                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                //sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                //sheet.AutoSizeColumn(8);
                //Create a row and put some cells in it. Rows are 0 based.
                int index = 8 + i;
                IRow iRow = sheet.CreateRow(index);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Vung
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(2);//Phong giao dich
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posCode);
                cell_1.CellStyle = style;

                cell_1 = iRow.CreateCell(3);//Ten phong giao dich
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(4);//Ma chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(5);//Ten chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosName);
                cell_1.CellStyle = style;

                cell_1 = iRow.CreateCell(6);//Ma nhan su
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(userCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(7);//Ten nhan su
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(fullName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(8);//Tổng hợp % THKH
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(percentFinalTotal);
                cell_6.CellStyle = style;
                rowIndex = index;
                i++;

            }

            //set print area
            workbook.SetPrintArea(0, 0, 8, 0, rowIndex);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao Bottom Ca nhan
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        internal static void ExportExcelBottomCaNhan(DataTable dtCurrent, string templatePath, string strYEAR)
        {

            HttpResponse Response = HttpContext.Current.Response;
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);

            IRow iRowHeader = sheet.GetRow(4);
            ICell cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(Security.FULL_NAME);

            iRowHeader = sheet.GetRow(5);
            cell_00 = iRowHeader.CreateCell(4);
            cell_00.SetCellType(CellType.String);
            cell_00.SetCellValue(strYEAR);

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //variable
                var regionName = row["REGION_NAME"].ToString();
                var posCode = row["POS_CODE"].ToString();
                var posName = row["POS_NAME"].ToString();
                var mainPosCode = row["MAIN_POS_CODE"].ToString();
                var mainPosName = row["MAIN_POS_NAME"].ToString();

                var userCode = row["USER_CODE"].ToString();
                var fullName = row["FULL_NAME"].ToString();

                //Quality
                double percentFinalTotal = Sql.ToDouble(row["PERCENT_FINAL_TOTAL"]);

                var remark = row["REMARK"].ToString();

                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                //sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                //sheet.AutoSizeColumn(8);

                //Create a row and put some cells in it. Rows are 0 based.
                int index = 8 + i;
                IRow iRow = sheet.CreateRow(index);

                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Vung
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(regionName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(2);//Phong giao dich
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posCode);
                cell_1.CellStyle = style;

                cell_1 = iRow.CreateCell(3);//Ten phong giao dich
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(posName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(4);//Ma chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(5);//Ten chi nhanh
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(mainPosName);
                cell_1.CellStyle = style;

                cell_1 = iRow.CreateCell(6);//Ma nhan su
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(userCode);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                cell_1 = iRow.CreateCell(7);//Ten nhan su
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(fullName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it. finalValue3
                ICell cell_6 = iRow.CreateCell(8);//Tổng hợp % THKH
                cell_6.SetCellType(CellType.Numeric);
                cell_6.SetCellValue(percentFinalTotal);
                cell_6.CellStyle = style;
                rowIndex = index;
                i++;
            }

            //set print area
            workbook.SetPrintArea(0, 0, 8, 0, rowIndex);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao KPIs Ca nhan
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        internal static void ExportExcelKPIsCaNhan(DataTable dtCurrent, Security.EMPLOYEE_FIELDS employeeInfo, string periodMonth, string templatePath)
        {
            HttpResponse Response = HttpContext.Current.Response;
            SplendidControl control = new SplendidControl();
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);
            //Thong tin header

            string empCode = employeeInfo.MA_NHAN_VIEN;
            string fullName = employeeInfo.FULL_NAME;//Ten nhan vien
            string positionName = employeeInfo.POSITION_NAME;//Chuc danh
            string orgName = employeeInfo.ORGANIZATION_NAME;//Phong ban

            DateTime dateProbation = Sql.ToDateTime(employeeInfo.DATE_START_PROBATION);//Ngay bat dau thu viec
            DateTime dateStartWork = Sql.ToDateTime(employeeInfo.DATE_START_WORK);//Ngay lam viec chinh thuc
            string rank = employeeInfo.TITLE_NAME; //Cap bac--de empty tam thoi chua co

            IRow iRowHeader = sheet.GetRow(5);
            //Ten nhan vien
            ICell cell_00 = iRowHeader.GetCell(1);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(fullName);
            }
            //Chuc danh
            cell_00 = iRowHeader.GetCell(3);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(positionName);
            }
            //Ma nhan vien
            cell_00 = iRowHeader.GetCell(5);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(empCode);
            }
            //Cap bac
            cell_00 = iRowHeader.GetCell(8);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(rank);
            }
            //Row header 2
            IRow iRowHeader2 = sheet.GetRow(6);
            //Don vi -- Ten phong ban
            cell_00 = iRowHeader2.GetCell(3);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(orgName);
            }
            //Ngay bat dau thu viec
            cell_00 = iRowHeader2.GetCell(5);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateProbation == DateTime.MinValue ? "" : dateProbation.ToShortDateString());
            }
            //Ngay bat dau chinh thuc
            cell_00 = iRowHeader2.GetCell(8);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateStartWork == DateTime.MinValue ? "" : dateStartWork.ToShortDateString());
            }

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;

            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                //get data kpis
                var kpiName = row["KPI_NAME"].ToString();
                var kpiUnit = row["KPI_UNIT"].ToString();
                kpiUnit = KPIs_Utils.Get_DisplayName(control.GetL10n().NAME, "CURRENCY_UNIT_LIST", kpiUnit);
                var ratio = row["RATIO"].ToString();
                //Create a row and put some cells in it. Rows are 0 based.
                int index = 9 + i;
                IRow iRow = sheet.CreateRow(index);
                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;
                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Ten chi tieu
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(kpiName);
                cell_1.CellStyle = style;
                //Create a cell and put a value in it.
                ICell cell_2 = iRow.CreateCell(2);//Don vi
                cell_2.SetCellType(CellType.String);
                cell_2.SetCellValue(kpiUnit);
                cell_2.CellStyle = style;
                //Create a cell and put a value in it.
                ICell cell_3 = iRow.CreateCell(3);//Ti trong
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(string.Format("{0}%", ratio));
                cell_3.CellStyle = style;

                //first month
                if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                {
                    sheet.AutoSizeColumn(4);
                    sheet.AutoSizeColumn(5);
                    sheet.AutoSizeColumn(6);
                    sheet.AutoSizeColumn(7);
                    sheet.AutoSizeColumn(8);
                    sheet.AutoSizeColumn(9);

                    double month_1 = Sql.ToDouble(row["MONTH_1"].ToString());
                    double month_2 = Sql.ToDouble(row["MONTH_2"].ToString());
                    double month_3 = Sql.ToDouble(row["MONTH_3"].ToString());
                    double month_4 = Sql.ToDouble(row["MONTH_4"].ToString());
                    double month_5 = Sql.ToDouble(row["MONTH_5"].ToString());
                    double month_6 = Sql.ToDouble(row["MONTH_6"].ToString());
                    //Create a cell and put a value in it
                    ICell cell_00_06 = iRow.CreateCell(4);//month_1
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_1);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(5);//month_2
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_2);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(6);//month_3
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_3);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(7);//month_4
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_4);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(8);//month_5
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_5);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(9);//month_6
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_6);
                    cell_00_06.CellStyle = style;
                }
                //end month
                if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                {
                    sheet.AutoSizeColumn(4);
                    sheet.AutoSizeColumn(5);
                    sheet.AutoSizeColumn(6);
                    sheet.AutoSizeColumn(7);
                    sheet.AutoSizeColumn(8);
                    sheet.AutoSizeColumn(9);

                    double month_7 = Sql.ToDouble(row["MONTH_7"].ToString());
                    double month_8 = Sql.ToDouble(row["MONTH_8"].ToString());
                    double month_9 = Sql.ToDouble(row["MONTH_9"].ToString());
                    double month_10 = Sql.ToDouble(row["MONTH_10"].ToString());
                    double month_11 = Sql.ToDouble(row["MONTH_11"].ToString());
                    double month_12 = Sql.ToDouble(row["MONTH_12"].ToString());
                    //Create a cell and put a value in it
                    ICell cell_07_12 = iRow.CreateCell(4);//month_7
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_7);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(5);//month_8
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_8);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(6);//month_9
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_9);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(7);//month_10
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_10);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(8);//month_11
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_11);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(9);//month_12
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_12);
                    cell_07_12.CellStyle = style;
                }
                //full month
                if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                {
                    double month_1 = Sql.ToDouble(row["MONTH_1"].ToString());
                    double month_2 = Sql.ToDouble(row["MONTH_2"].ToString());
                    double month_3 = Sql.ToDouble(row["MONTH_3"].ToString());
                    double month_4 = Sql.ToDouble(row["MONTH_4"].ToString());
                    double month_5 = Sql.ToDouble(row["MONTH_5"].ToString());
                    double month_6 = Sql.ToDouble(row["MONTH_6"].ToString());
                    double month_7 = Sql.ToDouble(row["MONTH_7"].ToString());
                    double month_8 = Sql.ToDouble(row["MONTH_8"].ToString());
                    double month_9 = Sql.ToDouble(row["MONTH_9"].ToString());
                    double month_10 = Sql.ToDouble(row["MONTH_10"].ToString());
                    double month_11 = Sql.ToDouble(row["MONTH_11"].ToString());
                    double month_12 = Sql.ToDouble(row["MONTH_12"].ToString());
                    sheet.AutoSizeColumn(4);
                    sheet.AutoSizeColumn(5);
                    sheet.AutoSizeColumn(6);
                    sheet.AutoSizeColumn(7);
                    sheet.AutoSizeColumn(8);
                    sheet.AutoSizeColumn(9);
                    sheet.AutoSizeColumn(10);
                    sheet.AutoSizeColumn(11);
                    sheet.AutoSizeColumn(12);
                    sheet.AutoSizeColumn(13);
                    sheet.AutoSizeColumn(14);
                    sheet.AutoSizeColumn(15);

                    //Create a cell and put a value in it
                    ICell cell_00_06 = iRow.CreateCell(4);//month_1
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_1);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(5);//month_2
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_2);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(6);//month_3
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_3);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(7);//month_4
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_4);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(8);//month_5
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_5);
                    cell_00_06.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_00_06 = iRow.CreateCell(9);//month_6
                    cell_00_06.SetCellType(CellType.Numeric);
                    cell_00_06.SetCellValue(month_6);
                    cell_00_06.CellStyle = style;

                    //Create a cell and put a value in it
                    ICell cell_07_12 = iRow.CreateCell(10);//month_7
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_7);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(11);//month_8
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_8);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(12);//month_9
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_9);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(13);//month_10
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_10);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(14);//month_11
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_11);
                    cell_07_12.CellStyle = style;
                    //Create a cell and put a value in it
                    cell_07_12 = iRow.CreateCell(15);//month_12
                    cell_07_12.SetCellType(CellType.Numeric);
                    cell_07_12.SetCellValue(month_12);
                    cell_07_12.CellStyle = style;
                }
                rowIndex = index;
                i++;
            }

            ICellStyle cellStyleBottom = workbook.CreateCellStyle();
            IFont fontTotal = workbook.CreateFont();
            fontTotal.FontName = "Times New Roman";
            fontTotal.IsBold = true;
            cellStyleBottom.SetFont(fontTotal);
            cellStyleBottom.Alignment = HorizontalAlignment.Center;

            //set print area
            if (KPIs_Utils.START_MONTH.Equals(periodMonth) || KPIs_Utils.END_MONTH.Equals(periodMonth))
            {
                workbook.SetPrintArea(0, 0, 9, 0, rowIndex + 3);
                //Forcing formula recalculation...
                sheet.ForceFormulaRecalculation = true;

                IRow iRowBottom = sheet.CreateRow(rowIndex + 1);
                for (int j = 0; j <= 9; j++)
                {
                    ICell cell = iRowBottom.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;
                    if (j == 5)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(string.Format(control.GetL10n().Term(".LBL_REPORT_SIGNED_DATE"), DateTime.Now.Year));
                    }
                }
                var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 5, 9);
                sheet.AddMergedRegion(cellRange);

                //Create a cell bottom
                IRow iRowBottom2 = sheet.CreateRow(rowIndex + 2);
                //ICell cell_bottom = iRowBottom2.CreateCell(1);
                //cell_bottom.SetCellType(CellType.String);
                //cell_bottom.SetCellValue("NGƯỜI LAO ĐỘNG");
                //cell_bottom.CellStyle = cellStyleBottom;

                for (int j = 0; j <= 9; j++)
                {
                    ICell cell = iRowBottom2.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;
                    if (j == 1)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(control.GetL10n().Term(".LBL_REPORT_EMPLOYEE"));
                    }
                    if (j == 5)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(control.GetL10n().Term(".LBL_REPORT_MANAGER"));
                    }
                }
                cellRange = new CellRangeAddress(rowIndex + 2, rowIndex + 2, 5, 9);
                sheet.AddMergedRegion(cellRange);

                cellStyleBottom = workbook.CreateCellStyle();
                fontTotal = workbook.CreateFont();
                fontTotal.FontName = "Times New Roman";
                cellStyleBottom.SetFont(fontTotal);
                cellStyleBottom.Alignment = HorizontalAlignment.Left;
                cellStyleBottom.WrapText = true;

                iRowBottom2 = sheet.CreateRow(rowIndex + 3);
                for (int j = 0; j <= 9; j++)
                {
                    ICell cell = iRowBottom2.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;

                    if (j == 0)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(string.Format(control.GetL10n().Term(".LBL_REPORT_NOTICE"), DateTime.Now.Year));
                    }
                }
                iRowBottom2.Height = 1280;
                cellRange = new CellRangeAddress(rowIndex + 3, rowIndex + 3, 0, 9);
                sheet.AddMergedRegion(cellRange);
            }
            else
            {
                workbook.SetPrintArea(0, 0, 15, 0, rowIndex + 3);
                //Forcing formula recalculation...
                sheet.ForceFormulaRecalculation = true;
                IRow iRowBottom = sheet.CreateRow(rowIndex + 1);
                for (int j = 0; j <= 15; j++)
                {
                    ICell cell = iRowBottom.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;
                    if (j == 10)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(string.Format(control.GetL10n().Term(".LBL_REPORT_SIGNED_DATE"), DateTime.Now.Year));
                    }
                }
                var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 10, 15);
                sheet.AddMergedRegion(cellRange);

                //Create a cell bottom
                iRowBottom = sheet.CreateRow(rowIndex + 2);
                ICell cell_bottom = iRowBottom.CreateCell(1);
                cell_bottom.SetCellType(CellType.String);
                cell_bottom.SetCellValue(control.GetL10n().Term(".LBL_REPORT_EMPLOYEE"));
                cell_bottom.CellStyle = cellStyleBottom;

                for (int j = 0; j <= 9; j++)
                {
                    ICell cell = iRowBottom.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;
                    if (j == 10)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(control.GetL10n().Term(".LBL_REPORT_MANAGER"));
                    }
                }
                cellRange = new CellRangeAddress(rowIndex + 2, rowIndex + 2, 10, 15);
                sheet.AddMergedRegion(cellRange);
                iRowBottom = sheet.CreateRow(rowIndex + 3);
                for (int j = 0; j <= 9; j++)
                {
                    ICell cell = iRowBottom.CreateCell(j);
                    cell.CellStyle = cellStyleBottom;

                    if (j == 0)
                    {
                        cell.SetCellType(CellType.String);
                        cell.SetCellValue(string.Format(control.GetL10n().Term(".LBL_REPORT_NOTICE"), DateTime.Now.Year));
                        IRichTextString rts = cell.RichStringCellValue;
                        rts.ApplyFont(0, 8, fontTotal);
                    }
                }
                iRowBottom.Height = 1280;
                cellRange = new CellRangeAddress(rowIndex + 3, rowIndex + 3, 0, 15);
                sheet.AddMergedRegion(cellRange);
            }


            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "Excel_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Bao cao ket qua KPIs Ca nhan
        /// </summary>
        /// <param name="dtCurrent"></param>
        /// <param name="templatePath"></param>
        internal static void ExportKPIsResultCaNhan(DataTable dtCurrent, Security.EMPLOYEE_FIELDS employeeInfo, string periodMonth, string templatePath)
        {
            HttpResponse Response = HttpContext.Current.Response;
            SplendidControl control = new SplendidControl();
            FileStream fs = new FileStream(templatePath, FileMode.Open, FileAccess.Read);
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            //Getting the worksheet by its name...
            ISheet sheet = workbook.GetSheetAt(0);
            //Setting the value at row 5 column 3 and column 5
            IDataFormat df = workbook.CreateDataFormat();
            //cell style
            ICellStyle style = workbook.CreateCellStyle();
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;

            IFont font = workbook.CreateFont();
            font.FontName = "Times New Roman";
            style.SetFont(font);
            //Thong tin header

            string empCode = employeeInfo.MA_NHAN_VIEN;
            string fullName = employeeInfo.FULL_NAME;//Ten nhan vien
            string positionName = employeeInfo.POSITION_NAME;//Chuc danh
            string orgName = employeeInfo.ORGANIZATION_NAME;//Phong ban

            DateTime dateProbation = Sql.ToDateTime(employeeInfo.DATE_START_PROBATION);//Ngay bat dau thu viec
            DateTime dateStartWork = Sql.ToDateTime(employeeInfo.DATE_START_WORK);//Ngay lam viec chinh thuc
            string rank = employeeInfo.TITLE_NAME; //Cap bac--de empty tam thoi chua co
            IRow iRowHeader0 = sheet.GetRow(0);
            ICell cell_hearder = iRowHeader0.GetCell(0);
            if (cell_hearder != null)
            {
                IRichTextString strHear = cell_hearder.RichStringCellValue;
                cell_hearder.SetCellType(CellType.String);
                cell_hearder.SetCellValue(string.Format(strHear.String, periodMonth));
            }

            IRow iRowHeader = sheet.GetRow(2);
            //Ten nhan vien
            ICell cell_00 = iRowHeader.GetCell(2);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(fullName);
            }
            else
            {
                cell_00 = iRowHeader.CreateCell(2);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(fullName);
            }
            //Ngay bat dau thu viec
            cell_00 = iRowHeader.GetCell(5);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateProbation == DateTime.MinValue ? "" : dateProbation.ToShortDateString());
            }
            else
            {
                cell_00 = iRowHeader.CreateCell(5);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateProbation == DateTime.MinValue ? "" : dateProbation.ToShortDateString());
            }
            //Cap bac
            cell_00 = iRowHeader.GetCell(8);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(rank);
            }
            else
            {
                cell_00 = iRowHeader.CreateCell(8);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(rank);
            }

            //Row header 2
            IRow iRowHeader2 = sheet.GetRow(3);
            //Ma nhan vien
            cell_00 = iRowHeader2.GetCell(2);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(empCode);
            }
            else
            {
                cell_00 = iRowHeader2.CreateCell(2);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(empCode);

            }
            //Ngay bat dau chinh thuc
            cell_00 = iRowHeader2.GetCell(5);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateStartWork == DateTime.MinValue ? "" : dateStartWork.ToShortDateString());
            }
            else
            {
                cell_00 = iRowHeader2.CreateCell(5);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(dateStartWork == DateTime.MinValue ? "" : dateStartWork.ToShortDateString());
            }
            //Chuc danh
            cell_00 = iRowHeader2.GetCell(8);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(positionName);
            }
            else
            {
                cell_00 = iRowHeader2.CreateCell(8);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(positionName);
            }
            IRow iRowHeader3 = sheet.GetRow(4);
            //Phong ban
            cell_00 = iRowHeader3.GetCell(2);
            if (cell_00 != null)
            {
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(orgName);
            }
            else
            {
                cell_00 = iRowHeader3.CreateCell(2);
                cell_00.SetCellType(CellType.String);
                cell_00.SetCellValue(orgName);
            }

            style.ShrinkToFit = true;
            style.DataFormat = df.GetFormat("#,##0.00");

            int rowIndex = 1;
            int i = 0;
            double total = 0;
            //fill data to excel
            foreach (DataRow row in dtCurrent.Rows)
            {
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                //sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                sheet.AutoSizeColumn(8);

                //get data kpis
                var kpiName = row["KPI_NAME"].ToString();
                var kpiUnit = row["KPI_UNIT"].ToString();
                kpiUnit = KPIs_Utils.Get_DisplayName(control.GetL10n().NAME, "CURRENCY_UNIT_LIST", kpiUnit);
                double ratio = Sql.ToDouble(row["RATIO"].ToString());

                var plan_value = Sql.ToDouble(row["PLAN_VALUE"].ToString());

                var sync_value = Sql.ToDouble(row["SYNC_VALUE"].ToString());
                var final_value = Sql.ToDouble(row["FINAL_VALUE"].ToString());

                var need_value = Sql.ToDouble(row["NEED_VALUE"].ToString());
                var value_std_per_month = Sql.ToDouble(row["VALUE_STD_PER_MONTH"].ToString());
                var percent_final_value = Sql.ToDouble(row["PERCENT_FINAL_VALUE"].ToString());

                //Create a row and put some cells in it. Rows are 0 based.
                int index = 9 + i;
                IRow iRow = sheet.CreateRow(index);
                //Create a cell and put a value in it.
                ICell cell_0 = iRow.CreateCell(0);//STT
                cell_0.SetCellType(CellType.Numeric);
                cell_0.SetCellValue((i + 1).ToString());
                cell_0.CellStyle = style;
                //Create a cell and put a value in it.
                ICell cell_1 = iRow.CreateCell(1);//Ten chi tieu
                cell_1.SetCellType(CellType.String);
                cell_1.SetCellValue(kpiName);
                cell_1.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_2 = iRow.CreateCell(2);//Don vi
                cell_2.SetCellType(CellType.String);
                cell_2.SetCellValue(kpiUnit);
                cell_2.CellStyle = style;

                //Create a cell and put a value in it.
                ICell cell_3 = iRow.CreateCell(3);//Ti trong
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(string.Format("{0}%", ratio));
                cell_3.CellStyle = style;

                cell_3 = iRow.CreateCell(4);//gia tri toi thieu
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(value_std_per_month);
                cell_3.CellStyle = style;

                cell_3 = iRow.CreateCell(5);//gia tri phan giao
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(plan_value);
                cell_3.CellStyle = style;

                cell_3 = iRow.CreateCell(6);//gia tri dong bo
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(sync_value);
                cell_3.CellStyle = style;

                cell_3 = iRow.CreateCell(7);//gia tri ghi nhan
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(final_value);
                cell_3.CellStyle = style;


                cell_3 = iRow.CreateCell(8);//gia tri can thuc hien
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(need_value < 0 ? 0 : need_value);
                cell_3.CellStyle = style;

                cell_3 = iRow.CreateCell(9);//% hoan thanh
                cell_3.SetCellType(CellType.String);
                cell_3.SetCellValue(percent_final_value);
                cell_3.CellStyle = style;

                total = total + (ratio * percent_final_value) / 100;
                rowIndex = index;
                i++;
            }

            workbook.SetPrintArea(0, 0, 9, 0, rowIndex + 1);
            //Forcing formula recalculation...
            sheet.ForceFormulaRecalculation = true;
            ICellStyle cellStyleBottom = workbook.CreateCellStyle();
            IFont fontTotal = workbook.CreateFont();
            fontTotal.FontName = "Times New Roman";
            fontTotal.IsBold = true;
            cellStyleBottom.SetFont(fontTotal);

            IRow iRowBottom = sheet.CreateRow(rowIndex + 1);
            for (int j = 0; j <= 9; j++)
            {
                ICell cell = iRowBottom.CreateCell(j);
                if (j == 0)
                {
                    cellStyleBottom.Alignment = HorizontalAlignment.Center;
                    cell.SetCellType(CellType.String);
                    cell.SetCellValue(string.Format(control.GetL10n().Term(".LBL_FINAL_EVALUTION_RESULT"), DateTime.Now.Year));
                    cell.CellStyle = cellStyleBottom;
                }
                if (j == 9)
                {
                    cellStyleBottom.Alignment = HorizontalAlignment.Right;
                    cell.SetCellType(CellType.Numeric);
                    cell.SetCellValue(Math.Round(total, 2));
                    cell.CellStyle = cellStyleBottom;
                }
            }
            var cellRange = new CellRangeAddress(rowIndex + 1, rowIndex + 1, 0, 8);
            sheet.AddMergedRegion(cellRange);

            MemoryStream ms = new MemoryStream();

            //Writing the workbook content to the FileStream...
            workbook.Write(ms);
            workbook.Close();

            string tmpFilenm = "DGXL_DINHKY_" + empCode + "_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

            Response.Clear();

            Response.Buffer = true;
            //Response.BufferOutput = true;

            Response.AddHeader("content-disposition", "attachment;filename=" + tmpFilenm);
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Length", ms.Length.ToString());

            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(ms.ToArray());

            //ms.WriteTo(Response.OutputStream);
            Response.Cookies.Add(new HttpCookie("downloadStarted", "1") { Expires = DateTime.Now.AddSeconds(20) });
            ms.Close();
            Response.Flush();
            Response.End();
        }

        public static void ExportDataGridViewDToExcel(SplendidGrid grdMain, int startIndex = 1)
        {
            HttpResponse Response = HttpContext.Current.Response;
            //Getting the complete workbook...
            XSSFWorkbook workbook = new XSSFWorkbook();
            //Getting the worksheet by its name...
            ISheet sheet = workbook.CreateSheet("Data");
            //make a header row
            int cellIndex = 0;
            IRow row1 = sheet.CreateRow(0);
            for (int k = 0; k < grdMain.Columns.Count; k++)
            {
                string headerText = grdMain.Columns[k].HeaderText;
                if (headerText != string.Empty)
                {
                    ICell cell = row1.CreateCell(cellIndex);
                    cell.SetCellValue(headerText);
                    cellIndex++;
                }
            }          
            //loops through data
            int rowCount = 0;
            int cellCount = 0;
            foreach (DataGridItem dataGridItem in grdMain.Items)
            {
                IRow row = sheet.CreateRow(rowCount + 1);
                cellCount = 0;
                for (int j = startIndex; j < grdMain.Columns.Count; j++)
                {
                    ICell cell = row.CreateCell(cellCount);
                    try
                    {
                        Control control = dataGridItem.Cells[j].Controls[0] as Control;
                        if (control is Literal)
                        {
                            Literal lbl = control as Literal;
                            cell.SetCellValue(HttpUtility.HtmlDecode(lbl.Text));
                        }
                        else if (control is HyperLink)
                        {
                            HyperLink hp = control as HyperLink;
                            cell.SetCellValue(HttpUtility.HtmlDecode(hp.Text));
                        }
                        else if (control is Label)
                        {
                            Label lbl = control as Label;
                            cell.SetCellValue(HttpUtility.HtmlDecode(lbl.Text));
                        }
                        else
                        {
                            Label ctlLabel = dataGridItem.Cells[j].Controls[1] as Label;
                            cell.SetCellValue(HttpUtility.HtmlDecode(ctlLabel.Text));

                        }
                    }
                    catch
                    {
                        string cellValue = dataGridItem.Cells[j].Text;
                        cell.SetCellValue(HttpUtility.HtmlDecode(cellValue));
                    }
                    cellCount++;
                }
                rowCount++;
            }

            using (var exportData = new MemoryStream())
            {
                string tmpFilenm = "Export_" +
                              DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') +
                              DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0') +
                              ".xlsx";

                Response.Clear();
                workbook.Write(exportData);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", tmpFilenm));
                Response.Charset = "utf-8";
                Response.BinaryWrite(exportData.ToArray());
                Response.End();
            }
        }

        private static void WriteAttachment(string FileName, string content)
        {
            HttpResponse Response = HttpContext.Current.Response;
            Response.ClearHeaders();
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Write(content);
            Response.End();
        }

    }
}