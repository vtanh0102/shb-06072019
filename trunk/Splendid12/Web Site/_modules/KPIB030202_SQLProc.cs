﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;
namespace SplendidCRM._modules
{
    public class KPIB030202_SQLProc
    {
        public static void spB_KPI_ACT_RESULT_DETAIL_Update_Final_Value(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, decimal flFINAL_VALUE, string sDES, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Upda";
                else
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Update_FINAL_VALUE";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                IDbDataParameter parDES = Sql.AddParameter(cmd, "@DES", sDES);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }

        public static void spB_KPI_ACT_RESULT_DETAIL_Calculate_Percentage(int nYear, string sMonthPeriod, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spKPI_EMP_CAL_PERCENTAGE";
                else
                    cmd.CommandText = "spKPI_EMP_CAL_PERCENTAGE";
                IDbDataParameter parTEAM_SET_LIST = Sql.AddParameter(cmd, "@iYear", nYear);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@sMONTH_PERIOD", sMonthPeriod);                
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();                
            }
        }


        public static void spB_KPI_ACT_RESULT_DETAIL_Calculate_Total_Percentage(int nYear, string sMonthPeriod, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spKPI_EMP_CAL_TOTAL_PERCENTAGE";
                else
                    cmd.CommandText = "spKPI_EMP_CAL_TOTAL_PERCENTAGE";
                IDbDataParameter parTEAM_SET_LIST = Sql.AddParameter(cmd, "@iYear", nYear);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@sMONTH_PERIOD", sMonthPeriod);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }


        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_ACTUAL_RESULT_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Mass";
                            else
                                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_MassApprove";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ACTUAL_RESULT_MassApprove
        /// </summary>
        public static void spB_KPI_ACTUAL_RESULT_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Mass";
                else
                    cmd.CommandText = "spB_KPI_ACTUAL_RESULT_MassApprove";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion


        #region spB_KPI_ACTUAL_RESULT_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Import
        /// 
        public static void spB_KPI_ACTUAL_RESULT_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sACTUAL_RESULT_CODE, Int32 nYEAR, string sMONTH_PERIOD, Guid gEMPLOYEE_ID, string sMA_NHAN_VIEN, string sVERSION_NUMBER, string sALLOCATE_CODE, Guid gASSIGN_BY, DateTime dtASSIGN_DATE, float flPERCENT_SYNC_TOTAL, float flPERCENT_FINAL_TOTAL, float flPERCENT_MANUAL_TOTAL, decimal flTOTAL_AMOUNT_01, decimal flTOTAL_AMOUNT_02, decimal flTOTAL_AMOUNT_03, string sDESCRIPTION, string sREMARK, Guid gFILE_ID, DateTime dtLATEST_SYNC_DATE, Guid gAPPROVE_ID, string sAPPROVE_STATUS, Guid gAPPROVED_BY, DateTime dtAPPROVED_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parACTUAL_RESULT_CODE = Sql.AddParameter(cmd, "@ACTUAL_RESULT_CODE", sACTUAL_RESULT_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parEMPLOYEE_ID = Sql.AddParameter(cmd, "@EMPLOYEE_ID", gEMPLOYEE_ID);
                IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 10);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", gASSIGN_BY);
                IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                IDbDataParameter parPERCENT_SYNC_TOTAL = Sql.AddParameter(cmd, "@PERCENT_SYNC_TOTAL", flPERCENT_SYNC_TOTAL);
                IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                IDbDataParameter parPERCENT_MANUAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_MANUAL_TOTAL", flPERCENT_MANUAL_TOTAL);
                IDbDataParameter parTOTAL_AMOUNT_01 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_01", flTOTAL_AMOUNT_01);
                IDbDataParameter parTOTAL_AMOUNT_02 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_02", flTOTAL_AMOUNT_02);
                IDbDataParameter parTOTAL_AMOUNT_03 = Sql.AddParameter(cmd, "@TOTAL_AMOUNT_03", flTOTAL_AMOUNT_03);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", gFILE_ID);
                IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                IDbDataParameter parAPPROVE_ID = Sql.AddParameter(cmd, "@APPROVE_ID", gAPPROVE_ID);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 50);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 50);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 50);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 50);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 50);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion


        #region spB_KPI_ACT_RESULT_DETAIL_Import
        /// 

        /// spB_KPI_ACT_RESULT_DETAIL_Import
        /// 
        public static void spB_KPI_ACT_RESULT_DETAIL_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sACTUAL_RESULT_CODE, Int32 nYEAR, string sMONTH_PERIOD, Guid gEMPLOYEE_ID, string sMA_NHAN_VIEN, string sVERSION_NUMBER, Guid gKPI_ID, string sKPI_CODE, string sKPI_NAME, Int32 nKPI_UNIT, Int32 nLEVEL_NUMBER, float flRATIO, decimal flPLAN_VALUE, decimal flSYNC_VALUE, decimal flFINAL_VALUE, float flPERCENT_SYNC_VALUE, float flPERCENT_FINAL_VALUE, float flPERCENT_MANUAL_VALUE, string sDESCRIPTION, string sREMARK, DateTime dtLATEST_SYNC_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Impo";
                else
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parACTUAL_RESULT_CODE = Sql.AddParameter(cmd, "@ACTUAL_RESULT_CODE", sACTUAL_RESULT_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 2);
                IDbDataParameter parEMPLOYEE_ID = Sql.AddParameter(cmd, "@EMPLOYEE_ID", gEMPLOYEE_ID);
                IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parKPI_ID = Sql.AddParameter(cmd, "@KPI_ID", gKPI_ID);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parKPI_UNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nKPI_UNIT);
                IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                IDbDataParameter parPLAN_VALUE = Sql.AddParameter(cmd, "@PLAN_VALUE", flPLAN_VALUE);
                IDbDataParameter parSYNC_VALUE = Sql.AddParameter(cmd, "@SYNC_VALUE", flSYNC_VALUE);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@FINAL_VALUE", flFINAL_VALUE);
                IDbDataParameter parPERCENT_SYNC_VALUE = Sql.AddParameter(cmd, "@PERCENT_SYNC_VALUE", flPERCENT_SYNC_VALUE);
                IDbDataParameter parPERCENT_FINAL_VALUE = Sql.AddParameter(cmd, "@PERCENT_FINAL_VALUE", flPERCENT_FINAL_VALUE);
                IDbDataParameter parPERCENT_MANUAL_VALUE = Sql.AddParameter(cmd, "@PERCENT_MANUAL_VALUE", flPERCENT_MANUAL_VALUE);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                IDbDataParameter parLATEST_SYNC_DATE = Sql.AddParameter(cmd, "@LATEST_SYNC_DATE", dtLATEST_SYNC_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 200);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 200);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 200);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 200);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 200);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

    }
}