﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIB020202_SqlProc
    {
        #region spB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_D";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_D";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_D";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Merge
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_M";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Undelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sKPI_ALLOCATE_ID, string sALLOCATE_CODE, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sKPI_CODE, string sKPI_NAME, Int32 nLEVEL_NUMBER, Int32 nKPI_UNIT, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, string sKPI_GROUP_DETAIL_ID, string sDESCRIPTION, string sREMARK, decimal flTOTAL_VALUE, decimal flMONTH_1, decimal flMONTH_2, decimal flMONTH_3, decimal flMONTH_4, decimal flMONTH_5, decimal flMONTH_6, decimal flMONTH_7, decimal flMONTH_8, decimal flMONTH_9, decimal flMONTH_10, decimal flMONTH_11, decimal flMONTH_12, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                            IDbDataParameter parKPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@KPI_ALLOCATE_ID", sKPI_ALLOCATE_ID, 150);
                            IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                            IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                            IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                            IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                            IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                            IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                            IDbDataParameter parKPI_UNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nKPI_UNIT);
                            IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                            IDbDataParameter parRADIO = Sql.AddParameter(cmd, "@RADIO", flRADIO);
                            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                            IDbDataParameter parKPI_GROUP_DETAIL_ID = Sql.AddParameter(cmd, "@KPI_GROUP_DETAIL_ID", sKPI_GROUP_DETAIL_ID, 150);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK, 200);
                            IDbDataParameter parTOTAL_VALUE = Sql.AddParameter(cmd, "@TOTAL_VALUE", flTOTAL_VALUE);
                            IDbDataParameter parMONTH_1 = Sql.AddParameter(cmd, "@MONTH_1", flMONTH_1);
                            IDbDataParameter parMONTH_2 = Sql.AddParameter(cmd, "@MONTH_2", flMONTH_2);
                            IDbDataParameter parMONTH_3 = Sql.AddParameter(cmd, "@MONTH_3", flMONTH_3);
                            IDbDataParameter parMONTH_4 = Sql.AddParameter(cmd, "@MONTH_4", flMONTH_4);
                            IDbDataParameter parMONTH_5 = Sql.AddParameter(cmd, "@MONTH_5", flMONTH_5);
                            IDbDataParameter parMONTH_6 = Sql.AddParameter(cmd, "@MONTH_6", flMONTH_6);
                            IDbDataParameter parMONTH_7 = Sql.AddParameter(cmd, "@MONTH_7", flMONTH_7);
                            IDbDataParameter parMONTH_8 = Sql.AddParameter(cmd, "@MONTH_8", flMONTH_8);
                            IDbDataParameter parMONTH_9 = Sql.AddParameter(cmd, "@MONTH_9", flMONTH_9);
                            IDbDataParameter parMONTH_10 = Sql.AddParameter(cmd, "@MONTH_10", flMONTH_10);
                            IDbDataParameter parMONTH_11 = Sql.AddParameter(cmd, "@MONTH_11", flMONTH_11);
                            IDbDataParameter parMONTH_12 = Sql.AddParameter(cmd, "@MONTH_12", flMONTH_12);
                            IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 200);
                            IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 200);
                            IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 200);
                            IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 300);
                            IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 200);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sKPI_ALLOCATE_ID, string sALLOCATE_CODE, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sKPI_CODE, string sKPI_NAME, Int32 nLEVEL_NUMBER, Int32 nKPI_UNIT, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, string sKPI_GROUP_DETAIL_ID, string sDESCRIPTION, string sREMARK, decimal flTOTAL_VALUE, decimal flMONTH_1, decimal flMONTH_2, decimal flMONTH_3, decimal flMONTH_4, decimal flMONTH_5, decimal flMONTH_6, decimal flMONTH_7, decimal flMONTH_8, decimal flMONTH_9, decimal flMONTH_10, decimal flMONTH_11, decimal flMONTH_12, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parKPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@KPI_ALLOCATE_ID", sKPI_ALLOCATE_ID, 150);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                IDbDataParameter parKPI_UNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nKPI_UNIT);
                IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                IDbDataParameter parRADIO = Sql.AddParameter(cmd, "@RADIO", flRADIO);
                IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                IDbDataParameter parKPI_GROUP_DETAIL_ID = Sql.AddParameter(cmd, "@KPI_GROUP_DETAIL_ID", sKPI_GROUP_DETAIL_ID, 150);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK, 200);
                IDbDataParameter parTOTAL_VALUE = Sql.AddParameter(cmd, "@TOTAL_VALUE", flTOTAL_VALUE);
                IDbDataParameter parMONTH_1 = Sql.AddParameter(cmd, "@MONTH_1", flMONTH_1);
                IDbDataParameter parMONTH_2 = Sql.AddParameter(cmd, "@MONTH_2", flMONTH_2);
                IDbDataParameter parMONTH_3 = Sql.AddParameter(cmd, "@MONTH_3", flMONTH_3);
                IDbDataParameter parMONTH_4 = Sql.AddParameter(cmd, "@MONTH_4", flMONTH_4);
                IDbDataParameter parMONTH_5 = Sql.AddParameter(cmd, "@MONTH_5", flMONTH_5);
                IDbDataParameter parMONTH_6 = Sql.AddParameter(cmd, "@MONTH_6", flMONTH_6);
                IDbDataParameter parMONTH_7 = Sql.AddParameter(cmd, "@MONTH_7", flMONTH_7);
                IDbDataParameter parMONTH_8 = Sql.AddParameter(cmd, "@MONTH_8", flMONTH_8);
                IDbDataParameter parMONTH_9 = Sql.AddParameter(cmd, "@MONTH_9", flMONTH_9);
                IDbDataParameter parMONTH_10 = Sql.AddParameter(cmd, "@MONTH_10", flMONTH_10);
                IDbDataParameter parMONTH_11 = Sql.AddParameter(cmd, "@MONTH_11", flMONTH_11);
                IDbDataParameter parMONTH_12 = Sql.AddParameter(cmd, "@MONTH_12", flMONTH_12);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 200);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 200);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 200);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 300);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 200);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATE_DETAILS_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATE_DETAILS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_U";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parNAME = Sql.CreateParameter(cmd, "@NAME", "string", 150);
            IDbDataParameter parKPI_ALLOCATE_ID = Sql.CreateParameter(cmd, "@KPI_ALLOCATE_ID", "string", 150);
            IDbDataParameter parALLOCATE_CODE = Sql.CreateParameter(cmd, "@ALLOCATE_CODE", "string", 50);
            IDbDataParameter parVERSION_NUMBER = Sql.CreateParameter(cmd, "@VERSION_NUMBER", "string", 2);
            IDbDataParameter parORGANIZATION_CODE = Sql.CreateParameter(cmd, "@ORGANIZATION_CODE", "string", 50);
            IDbDataParameter parKPI_CODE = Sql.CreateParameter(cmd, "@KPI_CODE", "string", 50);
            IDbDataParameter parKPI_NAME = Sql.CreateParameter(cmd, "@KPI_NAME", "string", 200);
            IDbDataParameter parLEVEL_NUMBER = Sql.CreateParameter(cmd, "@LEVEL_NUMBER", "Int32", 4);
            IDbDataParameter parKPI_UNIT = Sql.CreateParameter(cmd, "@KPI_UNIT", "Int32", 4);
            IDbDataParameter parUNIT = Sql.CreateParameter(cmd, "@UNIT", "Int32", 4);
            IDbDataParameter parRADIO = Sql.CreateParameter(cmd, "@RADIO", "float", 8);
            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parKPI_GROUP_DETAIL_ID = Sql.CreateParameter(cmd, "@KPI_GROUP_DETAIL_ID", "string", 150);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 200);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 200);
            IDbDataParameter parTOTAL_VALUE = Sql.CreateParameter(cmd, "@TOTAL_VALUE", "float", 8);
            IDbDataParameter parMONTH_1 = Sql.CreateParameter(cmd, "@MONTH_1", "float", 8);
            IDbDataParameter parMONTH_2 = Sql.CreateParameter(cmd, "@MONTH_2", "float", 8);
            IDbDataParameter parMONTH_3 = Sql.CreateParameter(cmd, "@MONTH_3", "float", 8);
            IDbDataParameter parMONTH_4 = Sql.CreateParameter(cmd, "@MONTH_4", "float", 8);
            IDbDataParameter parMONTH_5 = Sql.CreateParameter(cmd, "@MONTH_5", "float", 8);
            IDbDataParameter parMONTH_6 = Sql.CreateParameter(cmd, "@MONTH_6", "float", 8);
            IDbDataParameter parMONTH_7 = Sql.CreateParameter(cmd, "@MONTH_7", "float", 8);
            IDbDataParameter parMONTH_8 = Sql.CreateParameter(cmd, "@MONTH_8", "float", 8);
            IDbDataParameter parMONTH_9 = Sql.CreateParameter(cmd, "@MONTH_9", "float", 8);
            IDbDataParameter parMONTH_10 = Sql.CreateParameter(cmd, "@MONTH_10", "float", 8);
            IDbDataParameter parMONTH_11 = Sql.CreateParameter(cmd, "@MONTH_11", "float", 8);
            IDbDataParameter parMONTH_12 = Sql.CreateParameter(cmd, "@MONTH_12", "float", 8);
            IDbDataParameter parFLEX1 = Sql.CreateParameter(cmd, "@FLEX1", "string", 200);
            IDbDataParameter parFLEX2 = Sql.CreateParameter(cmd, "@FLEX2", "string", 200);
            IDbDataParameter parFLEX3 = Sql.CreateParameter(cmd, "@FLEX3", "string", 200);
            IDbDataParameter parFLEX4 = Sql.CreateParameter(cmd, "@FLEX4", "string", 300);
            IDbDataParameter parFLEX5 = Sql.CreateParameter(cmd, "@FLEX5", "string", 200);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Delete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Delete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_Delete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Delete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassDelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDele";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassDelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDele";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_MassDelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassDelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDele";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassUpdate
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpda";
                            else
                                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassUpdate
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpda";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_MassUpdate
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassUpdate
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpda";
            else
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Merge
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Merge
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_Merge
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Merge
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Undelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Undelete
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_Undelete
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Undelete
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Update
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sALLOCATE_NAME, string sALLOCATE_CODE, string sYEAR, string sPERIOD, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sORGANIZATION_NAME, Guid gAPPROVE_ID, string sAPPROVE_STATUS, string sAPPROVED_BY, DateTime dtAPPROVED_DATE, string sALLOCATE_TYPE, string sDESCRIPTION, Int32 nSTAFT_NUMBER, float flTOTAL_PLAN_PERCENT, float flTOTAL_PLAN_VALUE, string sSTATUS, string sKPI_GROUP_ID, string sFILE_ID, string sASSIGN_BY, DateTime dtASSIGN_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parALLOCATE_NAME = Sql.AddParameter(cmd, "@ALLOCATE_NAME", sALLOCATE_NAME, 200);
                            IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", sYEAR, 50);
                            IDbDataParameter parPERIOD = Sql.AddParameter(cmd, "@PERIOD", sPERIOD, 50);
                            IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                            IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                            IDbDataParameter parORGANIZATION_NAME = Sql.AddParameter(cmd, "@ORGANIZATION_NAME", sORGANIZATION_NAME, 200);
                            IDbDataParameter parAPPROVE_ID = Sql.AddParameter(cmd, "@APPROVE_ID", gAPPROVE_ID);
                            IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                            IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", sAPPROVED_BY, 150);
                            IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                            IDbDataParameter parALLOCATE_TYPE = Sql.AddParameter(cmd, "@ALLOCATE_TYPE", sALLOCATE_TYPE, 50);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                            IDbDataParameter parSTAFT_NUMBER = Sql.AddParameter(cmd, "@STAFT_NUMBER", nSTAFT_NUMBER);
                            IDbDataParameter parTOTAL_PLAN_PERCENT = Sql.AddParameter(cmd, "@TOTAL_PLAN_PERCENT", flTOTAL_PLAN_PERCENT);
                            IDbDataParameter parTOTAL_PLAN_VALUE = Sql.AddParameter(cmd, "@TOTAL_PLAN_VALUE", flTOTAL_PLAN_VALUE);
                            IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 50);
                            IDbDataParameter parKPI_GROUP_ID = Sql.AddParameter(cmd, "@KPI_GROUP_ID", sKPI_GROUP_ID, 150);
                            IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", sFILE_ID, 150);
                            IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", sASSIGN_BY, 150);
                            IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                            IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 150);
                            IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 150);
                            IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 150);
                            IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 150);
                            IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 150);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Update
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sALLOCATE_NAME, string sALLOCATE_CODE, string sYEAR, string sPERIOD, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sORGANIZATION_NAME, Guid gAPPROVE_ID, string sAPPROVE_STATUS, string sAPPROVED_BY, DateTime dtAPPROVED_DATE, string sALLOCATE_TYPE, string sDESCRIPTION, Int32 nSTAFT_NUMBER, float flTOTAL_PLAN_PERCENT, float flTOTAL_PLAN_VALUE, string sSTATUS, string sKPI_GROUP_ID, string sFILE_ID, string sASSIGN_BY, DateTime dtASSIGN_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parALLOCATE_NAME = Sql.AddParameter(cmd, "@ALLOCATE_NAME", sALLOCATE_NAME, 200);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", sYEAR, 50);
                IDbDataParameter parPERIOD = Sql.AddParameter(cmd, "@PERIOD", sPERIOD, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parORGANIZATION_NAME = Sql.AddParameter(cmd, "@ORGANIZATION_NAME", sORGANIZATION_NAME, 200);
                IDbDataParameter parAPPROVE_ID = Sql.AddParameter(cmd, "@APPROVE_ID", gAPPROVE_ID);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", sAPPROVED_BY, 150);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parALLOCATE_TYPE = Sql.AddParameter(cmd, "@ALLOCATE_TYPE", sALLOCATE_TYPE, 50);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                IDbDataParameter parSTAFT_NUMBER = Sql.AddParameter(cmd, "@STAFT_NUMBER", nSTAFT_NUMBER);
                IDbDataParameter parTOTAL_PLAN_PERCENT = Sql.AddParameter(cmd, "@TOTAL_PLAN_PERCENT", flTOTAL_PLAN_PERCENT);
                IDbDataParameter parTOTAL_PLAN_VALUE = Sql.AddParameter(cmd, "@TOTAL_PLAN_VALUE", flTOTAL_PLAN_VALUE);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 50);
                IDbDataParameter parKPI_GROUP_ID = Sql.AddParameter(cmd, "@KPI_GROUP_ID", sKPI_GROUP_ID, 150);
                IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", sFILE_ID, 150);
                IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", sASSIGN_BY, 150);
                IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 150);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 150);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 150);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 150);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 150);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ORG_ALLOCATES_Update
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_Update
        /// </summary>
        public static IDbCommand cmdB_KPI_ORG_ALLOCATES_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parALLOCATE_NAME = Sql.CreateParameter(cmd, "@ALLOCATE_NAME", "string", 200);
            IDbDataParameter parALLOCATE_CODE = Sql.CreateParameter(cmd, "@ALLOCATE_CODE", "string", 50);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "string", 50);
            IDbDataParameter parPERIOD = Sql.CreateParameter(cmd, "@PERIOD", "string", 50);
            IDbDataParameter parVERSION_NUMBER = Sql.CreateParameter(cmd, "@VERSION_NUMBER", "string", 2);
            IDbDataParameter parORGANIZATION_CODE = Sql.CreateParameter(cmd, "@ORGANIZATION_CODE", "string", 50);
            IDbDataParameter parORGANIZATION_NAME = Sql.CreateParameter(cmd, "@ORGANIZATION_NAME", "string", 200);
            IDbDataParameter parAPPROVE_ID = Sql.CreateParameter(cmd, "@APPROVE_ID", "Guid", 16);
            IDbDataParameter parAPPROVE_STATUS = Sql.CreateParameter(cmd, "@APPROVE_STATUS", "string", 5);
            IDbDataParameter parAPPROVED_BY = Sql.CreateParameter(cmd, "@APPROVED_BY", "string", 150);
            IDbDataParameter parAPPROVED_DATE = Sql.CreateParameter(cmd, "@APPROVED_DATE", "DateTime", 8);
            IDbDataParameter parALLOCATE_TYPE = Sql.CreateParameter(cmd, "@ALLOCATE_TYPE", "string", 50);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 200);
            IDbDataParameter parSTAFT_NUMBER = Sql.CreateParameter(cmd, "@STAFT_NUMBER", "Int32", 4);
            IDbDataParameter parTOTAL_PLAN_PERCENT = Sql.CreateParameter(cmd, "@TOTAL_PLAN_PERCENT", "float", 8);
            IDbDataParameter parTOTAL_PLAN_VALUE = Sql.CreateParameter(cmd, "@TOTAL_PLAN_VALUE", "float", 8);
            IDbDataParameter parSTATUS = Sql.CreateParameter(cmd, "@STATUS", "string", 50);
            IDbDataParameter parKPI_GROUP_ID = Sql.CreateParameter(cmd, "@KPI_GROUP_ID", "string", 150);
            IDbDataParameter parFILE_ID = Sql.CreateParameter(cmd, "@FILE_ID", "string", 150);
            IDbDataParameter parASSIGN_BY = Sql.CreateParameter(cmd, "@ASSIGN_BY", "string", 150);
            IDbDataParameter parASSIGN_DATE = Sql.CreateParameter(cmd, "@ASSIGN_DATE", "DateTime", 8);
            IDbDataParameter parFLEX1 = Sql.CreateParameter(cmd, "@FLEX1", "string", 150);
            IDbDataParameter parFLEX2 = Sql.CreateParameter(cmd, "@FLEX2", "string", 150);
            IDbDataParameter parFLEX3 = Sql.CreateParameter(cmd, "@FLEX3", "string", 150);
            IDbDataParameter parFLEX4 = Sql.CreateParameter(cmd, "@FLEX4", "string", 150);
            IDbDataParameter parFLEX5 = Sql.CreateParameter(cmd, "@FLEX5", "string", 150);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion


        #region spB_KPI_ORG_ALLOCATES_MassApprove
        /// <summary>
        /// spB_KPI_ORG_ALLOCATES_MassApprove
        /// </summary>
        public static void spB_KPI_ORG_ALLOCATES_MassApprove(string sID_LIST, Guid gAPPROVED_BY, string sAPPROVE_STATUS, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Mass";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATES_MassApprove";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", gAPPROVED_BY);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion
        public static void spGen_KPI_ORG_ACTUAL_RESULT_InitResult(IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "Gen_KPI_ORG_ACTUAL_RESULT";
                else
                    cmd.CommandText = "Gen_KPI_ORG_ACTUAL_RESULT";
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }


        }


        #region spB_KPI_ORG_ALLOCATES_Import
        /// 

        /// spB_KPI_ORG_ALLOCATES_Import
        /// 
        public static void spB_KPI_ORG_ALLOCATES_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sALLOCATE_NAME, ref string sALLOCATE_CODE, string sYEAR, string sPERIOD, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sORGANIZATION_NAME, Guid gAPPROVE_ID, string sAPPROVE_STATUS, string sAPPROVED_BY, DateTime dtAPPROVED_DATE, string sALLOCATE_TYPE, string sDESCRIPTION, Int32 nSTAFT_NUMBER, float flTOTAL_PLAN_PERCENT, float flTOTAL_PLAN_VALUE, string sSTATUS, string sKPI_GROUP_ID, string sFILE_ID, string sASSIGN_BY, DateTime dtASSIGN_DATE, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ORG_ALLOCATES_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parALLOCATE_NAME = Sql.AddParameter(cmd, "@ALLOCATE_NAME", sALLOCATE_NAME, 200);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", sYEAR, 50);
                IDbDataParameter parPERIOD = Sql.AddParameter(cmd, "@PERIOD", sPERIOD, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parORGANIZATION_NAME = Sql.AddParameter(cmd, "@ORGANIZATION_NAME", sORGANIZATION_NAME, 200);
                IDbDataParameter parAPPROVE_ID = Sql.AddParameter(cmd, "@APPROVE_ID", gAPPROVE_ID);
                IDbDataParameter parAPPROVE_STATUS = Sql.AddParameter(cmd, "@APPROVE_STATUS", sAPPROVE_STATUS, 5);
                IDbDataParameter parAPPROVED_BY = Sql.AddParameter(cmd, "@APPROVED_BY", sAPPROVED_BY, 150);
                IDbDataParameter parAPPROVED_DATE = Sql.AddParameter(cmd, "@APPROVED_DATE", dtAPPROVED_DATE);
                IDbDataParameter parALLOCATE_TYPE = Sql.AddParameter(cmd, "@ALLOCATE_TYPE", sALLOCATE_TYPE, 50);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                IDbDataParameter parSTAFT_NUMBER = Sql.AddParameter(cmd, "@STAFT_NUMBER", nSTAFT_NUMBER);
                IDbDataParameter parTOTAL_PLAN_PERCENT = Sql.AddParameter(cmd, "@TOTAL_PLAN_PERCENT", flTOTAL_PLAN_PERCENT);
                IDbDataParameter parTOTAL_PLAN_VALUE = Sql.AddParameter(cmd, "@TOTAL_PLAN_VALUE", flTOTAL_PLAN_VALUE);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 50);
                IDbDataParameter parKPI_GROUP_ID = Sql.AddParameter(cmd, "@KPI_GROUP_ID", sKPI_GROUP_ID, 150);
                IDbDataParameter parFILE_ID = Sql.AddParameter(cmd, "@FILE_ID", sFILE_ID, 150);
                IDbDataParameter parASSIGN_BY = Sql.AddParameter(cmd, "@ASSIGN_BY", sASSIGN_BY, 150);
                IDbDataParameter parASSIGN_DATE = Sql.AddParameter(cmd, "@ASSIGN_DATE", dtASSIGN_DATE);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 150);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 150);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 150);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 150);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 150);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                parALLOCATE_CODE.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
                sALLOCATE_CODE = Sql.ToString(parALLOCATE_CODE.Value);
            }
        }
        #endregion

        #region spB_KPI_ORG_ALLOCATE_DETAILS_Import
        /// 

        /// spB_KPI_ORG_ALLOCATE_DETAILS_Import
        /// 
        public static void spB_KPI_ORG_ALLOCATE_DETAILS_Import(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sKPI_ALLOCATE_ID, string sALLOCATE_CODE, string sVERSION_NUMBER, string sORGANIZATION_CODE, string sKPI_CODE, string sKPI_NAME, Int32 nYEAR, Int32 nKPI_UNIT, Int32 nUNIT, float flRADIO, float flMAX_RATIO_COMPLETE, string sKPI_GROUP_DETAIL_ID, string sDESCRIPTION, string sREMARK, decimal flTOTAL_VALUE, decimal flMONTH_1, decimal flMONTH_2, decimal flMONTH_3, decimal flMONTH_4, decimal flMONTH_5, decimal flMONTH_6, decimal flMONTH_7, decimal flMONTH_8, decimal flMONTH_9, decimal flMONTH_10, decimal flMONTH_11, decimal flMONTH_12, string sFLEX1, string sFLEX2, string sFLEX3, string sFLEX4, string sFLEX5, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_I";
                else
                    cmd.CommandText = "spB_KPI_ORG_ALLOCATE_DETAILS_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parKPI_ALLOCATE_ID = Sql.AddParameter(cmd, "@KPI_ALLOCATE_ID", sKPI_ALLOCATE_ID, 150);
                IDbDataParameter parALLOCATE_CODE = Sql.AddParameter(cmd, "@ALLOCATE_CODE", sALLOCATE_CODE, 50);
                IDbDataParameter parVERSION_NUMBER = Sql.AddParameter(cmd, "@VERSION_NUMBER", sVERSION_NUMBER, 2);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", sORGANIZATION_CODE, 50);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parKPI_UNIT = Sql.AddParameter(cmd, "@KPI_UNIT", nKPI_UNIT);
                IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                IDbDataParameter parRADIO = Sql.AddParameter(cmd, "@RADIO", flRADIO);
                IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                IDbDataParameter parKPI_GROUP_DETAIL_ID = Sql.AddParameter(cmd, "@KPI_GROUP_DETAIL_ID", sKPI_GROUP_DETAIL_ID, 150);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 200);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK, 200);
                IDbDataParameter parTOTAL_VALUE = Sql.AddParameter(cmd, "@TOTAL_VALUE", flTOTAL_VALUE);
                IDbDataParameter parMONTH_1 = Sql.AddParameter(cmd, "@MONTH_1", flMONTH_1);
                IDbDataParameter parMONTH_2 = Sql.AddParameter(cmd, "@MONTH_2", flMONTH_2);
                IDbDataParameter parMONTH_3 = Sql.AddParameter(cmd, "@MONTH_3", flMONTH_3);
                IDbDataParameter parMONTH_4 = Sql.AddParameter(cmd, "@MONTH_4", flMONTH_4);
                IDbDataParameter parMONTH_5 = Sql.AddParameter(cmd, "@MONTH_5", flMONTH_5);
                IDbDataParameter parMONTH_6 = Sql.AddParameter(cmd, "@MONTH_6", flMONTH_6);
                IDbDataParameter parMONTH_7 = Sql.AddParameter(cmd, "@MONTH_7", flMONTH_7);
                IDbDataParameter parMONTH_8 = Sql.AddParameter(cmd, "@MONTH_8", flMONTH_8);
                IDbDataParameter parMONTH_9 = Sql.AddParameter(cmd, "@MONTH_9", flMONTH_9);
                IDbDataParameter parMONTH_10 = Sql.AddParameter(cmd, "@MONTH_10", flMONTH_10);
                IDbDataParameter parMONTH_11 = Sql.AddParameter(cmd, "@MONTH_11", flMONTH_11);
                IDbDataParameter parMONTH_12 = Sql.AddParameter(cmd, "@MONTH_12", flMONTH_12);
                IDbDataParameter parFLEX1 = Sql.AddParameter(cmd, "@FLEX1", sFLEX1, 200);
                IDbDataParameter parFLEX2 = Sql.AddParameter(cmd, "@FLEX2", sFLEX2, 200);
                IDbDataParameter parFLEX3 = Sql.AddParameter(cmd, "@FLEX3", sFLEX3, 200);
                IDbDataParameter parFLEX4 = Sql.AddParameter(cmd, "@FLEX4", sFLEX4, 300);
                IDbDataParameter parFLEX5 = Sql.AddParameter(cmd, "@FLEX5", sFLEX5, 200);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region spGet_BusinessCode_From_Org_Code
        /// 

        /// spGet_BusinessCode_From_Org_Code
        /// 
        public static void spGet_BusinessCode_From_Org_Code(ref string businessCode, string organizationCode, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spGet_BusinessCode_From_Org_Code";
                else
                    cmd.CommandText = "spGet_BusinessCode_From_Org_Code";
                IDbDataParameter parBUSINESS_CODE = Sql.AddParameter(cmd, "@BusinessCode", businessCode, 10);
                IDbDataParameter parORGANIZATION_CODE = Sql.AddParameter(cmd, "@ORGANIZATION_CODE", organizationCode, 50);
                parBUSINESS_CODE.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                businessCode = Sql.ToString(parBUSINESS_CODE.Value);
            }
        }
        #endregion

    }
}
