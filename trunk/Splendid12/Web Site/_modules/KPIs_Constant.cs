﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SplendidCRM._modules
{
    public class KPIs_Constant
    {
        public static string KPI_APPROVE_STATUS_DONTSEND = "00";
        public static string KPI_APPROVE_STATUS_SUBMIT = "15";
        public static string KPI_APPROVE_STATUS_REJECT = "25";
        public static string KPI_APPROVE_STATUS_APPROVE = "65";

        public static string KPI_IS_COMMON_YES = "1";
        public static string KPI_IS_COMMON_NO = "0";
        public static string KPI_IS_BASE_ACTIVE = "1";

        //- HSCV --> thì hiển thị From và To là combox Position
        //- HSKV --> thì hiển thị From và To là Combobox Area
        //- HSTN --> thì hiển thị From là Seniority
        public static string TYPE_HSCV = "HSCV";
        public static string TYPE_HSKV = "HSKV";
        public static string TYPE_HSTN = "HSTN";
        //report type
        public static string KPI_REPORT_TYPE_R001_1 = "R001-1";//Bao cao top phong
        public static string KPI_REPORT_TYPE_R001_2 = "R001-2";//Bao cao top chi nhanh
        public static string KPI_REPORT_TYPE_R001_3 = "R001-3";//Bao cao bottom phong
        public static string KPI_REPORT_TYPE_R001_4 = "R001-4";//Bao cao bottom chi nhanh

        public static string KPI_REPORT_TYPE_R002_1 = "R002-1";//Bao cao thu thuan theo Ca nhan
        public static string KPI_REPORT_TYPE_R002_2 = "R002-2";//Bao cao thu thuan theo DVKD
        public static string KPI_REPORT_TYPE_R002_3 = "R002-3";//Bao cao thu thuan theo vung
        public static string KPI_REPORT_TYPE_R003 = "R003";
        public static string KPI_REPORT_TYPE_R004_1 = "R004-1";//Bao cao Top Ca Nhan
        public static string KPI_REPORT_TYPE_R004_2 = "R004-2";//Bao cao Bottom Ca nhan
        public static string KPI_REPORT_TYPE_R005 = "R005";
        //org type
        public static string ORG_TYPE_TAT = "TAT";
        public static string ORG_TYPE_PGD = "PGD";
        public static string ORG_TYPE_DVKH = "DVKH";
        public static string ORG_TYPE_KHCN = "KHCN";

    }
}