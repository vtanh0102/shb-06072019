﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIB0302_SqlProc
    {
        #region spB_KPI_ACTUAL_RESULT_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Import
        /// 
        public static void spB_KPI_ACTUAL_RESULT_Import(ref Guid gID, string sMaNhanVien, Int32 niYear, string ssMonth)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Import";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMaNhanVien = Sql.AddAnsiParam(cmd, "@MaNhanVien", sMaNhanVien, 50);
                            IDbDataParameter pariYear = Sql.AddParameter(cmd, "@iYear", niYear);
                            IDbDataParameter parsMonth = Sql.AddAnsiParam(cmd, "@sMonth", ssMonth, 2);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ACTUAL_RESULT_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Import
        /// 
        public static void spB_KPI_ACTUAL_RESULT_Import(ref Guid gID, string sMaNhanVien, Int32 niYear, string ssMonth, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMaNhanVien = Sql.AddAnsiParam(cmd, "@MaNhanVien", sMaNhanVien, 50);
                IDbDataParameter pariYear = Sql.AddParameter(cmd, "@iYear", niYear);
                IDbDataParameter parsMonth = Sql.AddAnsiParam(cmd, "@sMonth", ssMonth, 2);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ACTUAL_RESULT_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Import
        /// 
        public static IDbCommand cmdB_KPI_ACTUAL_RESULT_Import(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Import";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMaNhanVien = Sql.CreateParameter(cmd, "@MaNhanVien", "ansistring", 50);
            IDbDataParameter pariYear = Sql.CreateParameter(cmd, "@iYear", "Int32", 4);
            IDbDataParameter parsMonth = Sql.CreateParameter(cmd, "@sMonth", "ansistring", 2);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Update_Import
        /// 
        public static void spB_KPI_ACTUAL_RESULT_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sMA_NHAN_VIEN, float flPERCENT_FINAL_TOTAL, string sDESCRIPTION)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_I";
                            else
                                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_Import";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                            IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 10);
                            IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Update_Import
        /// 
        public static void spB_KPI_ACTUAL_RESULT_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sMA_NHAN_VIEN, float flPERCENT_FINAL_TOTAL, string sDESCRIPTION, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_I";
                else
                    cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 50);
                IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 10);
                IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.AddParameter(cmd, "@PERCENT_FINAL_TOTAL", flPERCENT_FINAL_TOTAL);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ACTUAL_RESULT_Update_Import
        /// 

        /// spB_KPI_ACTUAL_RESULT_Update_Import
        /// 
        public static IDbCommand cmdB_KPI_ACTUAL_RESULT_Update_Import(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_I";
            else
                cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_Import";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parMONTH_PERIOD = Sql.CreateParameter(cmd, "@MONTH_PERIOD", "string", 50);
            IDbDataParameter parMA_NHAN_VIEN = Sql.CreateParameter(cmd, "@MA_NHAN_VIEN", "string", 10);
            IDbDataParameter parPERCENT_FINAL_TOTAL = Sql.CreateParameter(cmd, "@PERCENT_FINAL_TOTAL", "float", 8);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        #region spB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 
        public static void spB_KPI_ACT_RESULT_DETAIL_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sMA_NHAN_VIEN, string sKPI_CODE, float flSYNC_VALUE, string sREMARK)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Upda";
                            else
                                cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Update_Import";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                            IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 2);
                            IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 50);
                            IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                            IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@SYNC_VALUE", flSYNC_VALUE);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 
        public static void spB_KPI_ACT_RESULT_DETAIL_Update_Import(ref Guid gID, Int32 nYEAR, string sMONTH_PERIOD, string sMA_NHAN_VIEN, string sKPI_CODE, float flSYNC_VALUE, string sREMARK, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Upda";
                else
                    cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Update_Import";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parYEAR = Sql.AddParameter(cmd, "@YEAR", nYEAR);
                IDbDataParameter parMONTH_PERIOD = Sql.AddParameter(cmd, "@MONTH_PERIOD", sMONTH_PERIOD, 2);
                IDbDataParameter parMA_NHAN_VIEN = Sql.AddParameter(cmd, "@MA_NHAN_VIEN", sMA_NHAN_VIEN, 50);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parFINAL_VALUE = Sql.AddParameter(cmd, "@SYNC_VALUE", flSYNC_VALUE);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 

        /// spB_KPI_ACT_RESULT_DETAIL_Update_Import
        /// 
        public static IDbCommand cmdB_KPI_ACT_RESULT_DETAIL_Update_Import(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Upda";
            else
                cmd.CommandText = "spB_KPI_ACT_RESULT_DETAIL_Update_Import";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parYEAR = Sql.CreateParameter(cmd, "@YEAR", "Int32", 4);
            IDbDataParameter parMONTH_PERIOD = Sql.CreateParameter(cmd, "@MONTH_PERIOD", "string", 2);
            IDbDataParameter parMA_NHAN_VIEN = Sql.CreateParameter(cmd, "@MA_NHAN_VIEN", "string", 50);
            IDbDataParameter parKPI_CODE = Sql.CreateParameter(cmd, "@KPI_CODE", "string", 50);
            IDbDataParameter parFINAL_VALUE = Sql.CreateParameter(cmd, "@FINAL_VALUE", "float", 8);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 104857600);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

        /// <summary>
		/// spB_KPI_ACTUAL_RESULT_Update
		/// </summary>
		public static void spB_KPI_ACTUAL_RESULT_Update_Des(ref Guid gID, string sDESCRIPTION, IDbTransaction trn)
		{
			IDbConnection con = trn.Connection;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.Transaction = trn;
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "spB_KPI_ACTUAL_RESULT_Update_Des";
				IDbDataParameter parID                   = Sql.AddParameter(cmd, "@ID"                  , gID                    );				
				IDbDataParameter parDESCRIPTION          = Sql.AddParameter(cmd, "@DESCRIPTION"         , sDESCRIPTION           );				
				parID.Direction = ParameterDirection.InputOutput;
				Sql.Trace(cmd);
				cmd.ExecuteNonQuery();
				gID = Sql.ToGuid(parID.Value);
			}
		}

    }
}