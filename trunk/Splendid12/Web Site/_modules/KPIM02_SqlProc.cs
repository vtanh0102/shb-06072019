﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;

namespace SplendidCRM._modules
{
    public class KPIM02_SqlProc
    {
        #region spM_KPIS_Delete
        /// <summary>
        /// spM_KPIS_Delete
        /// </summary>
        public static void spM_KPIS_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_Delete
        /// <summary>
        /// spM_KPIS_Delete
        /// </summary>
        public static void spM_KPIS_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPIS_Delete
        /// <summary>
        /// spM_KPIS_Delete
        /// </summary>
        public static IDbCommand cmdM_KPIS_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPIS_MassDelete
        /// <summary>
        /// spM_KPIS_MassDelete
        /// </summary>
        public static void spM_KPIS_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_MassDelete
        /// <summary>
        /// spM_KPIS_MassDelete
        /// </summary>
        public static void spM_KPIS_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPIS_MassDelete
        /// <summary>
        /// spM_KPIS_MassDelete
        /// </summary>
        public static IDbCommand cmdM_KPIS_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPIS_MassUpdate
        /// <summary>
        /// spM_KPIS_MassUpdate
        /// </summary>
        public static void spM_KPIS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_MassUpdate
        /// <summary>
        /// spM_KPIS_MassUpdate
        /// </summary>
        public static void spM_KPIS_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPIS_MassUpdate
        /// <summary>
        /// spM_KPIS_MassUpdate
        /// </summary>
        public static IDbCommand cmdM_KPIS_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spM_KPIS_Merge
        /// <summary>
        /// spM_KPIS_Merge
        /// </summary>
        public static void spM_KPIS_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_Merge
        /// <summary>
        /// spM_KPIS_Merge
        /// </summary>
        public static void spM_KPIS_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPIS_Merge
        /// <summary>
        /// spM_KPIS_Merge
        /// </summary>
        public static IDbCommand cmdM_KPIS_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPIS_Undelete
        /// <summary>
        /// spM_KPIS_Undelete
        /// </summary>
        public static void spM_KPIS_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_Undelete
        /// <summary>
        /// spM_KPIS_Undelete
        /// </summary>
        public static void spM_KPIS_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPIS_Undelete
        /// <summary>
        /// spM_KPIS_Undelete
        /// </summary>
        public static IDbCommand cmdM_KPIS_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spM_KPIS_Update
        /// <summary>
        /// spM_KPIS_Update
        /// </summary>
        public static void spM_KPIS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sKPI_CODE, string sKPI_NAME, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRATIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPIS_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                            IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                            IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                            IDbDataParameter parPARENT_ID = Sql.AddParameter(cmd, "@PARENT_ID", sPARENT_ID, 50);
                            IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                            IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                            IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                            IDbDataParameter parMIN_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE", flMIN_RATIO_COMPLETE);
                            IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE);
                            IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 50);
                            IDbDataParameter parMAX_POINT = Sql.AddParameter(cmd, "@MAX_POINT", nMAX_POINT);
                            IDbDataParameter parMIN_POINT = Sql.AddParameter(cmd, "@MIN_POINT", nMIN_POINT);
                            IDbDataParameter parDEFAULT_POINT = Sql.AddParameter(cmd, "@DEFAULT_POINT", nDEFAULT_POINT);
                            IDbDataParameter parSORTING_ORDER = Sql.AddParameter(cmd, "@SORTING_ORDER", nSORTING_ORDER);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 300);
                            IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK, 300);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPIS_Update
        /// <summary>
        /// spM_KPIS_Update
        /// </summary>
        public static void spM_KPIS_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sNAME, string sKPI_CODE, string sKPI_NAME, string sPARENT_ID, Int32 nLEVEL_NUMBER, Int32 nUNIT, float flRATIO, float flMAX_RATIO_COMPLETE, float flMIN_RATIO_COMPLETE, float flDEFAULT_RATIO_COMPLETE, string sSTATUS, Int32 nMAX_POINT, Int32 nMIN_POINT, Int32 nDEFAULT_POINT, Int32 nSORTING_ORDER, string sDESCRIPTION, string sREMARK, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPIS_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parNAME = Sql.AddParameter(cmd, "@NAME", sNAME, 150);
                IDbDataParameter parKPI_CODE = Sql.AddParameter(cmd, "@KPI_CODE", sKPI_CODE, 50);
                IDbDataParameter parKPI_NAME = Sql.AddParameter(cmd, "@KPI_NAME", sKPI_NAME, 200);
                IDbDataParameter parPARENT_ID = Sql.AddParameter(cmd, "@PARENT_ID", sPARENT_ID, 50);
                IDbDataParameter parLEVEL_NUMBER = Sql.AddParameter(cmd, "@LEVEL_NUMBER", nLEVEL_NUMBER);
                IDbDataParameter parUNIT = Sql.AddParameter(cmd, "@UNIT", nUNIT);
                IDbDataParameter parRATIO = Sql.AddParameter(cmd, "@RATIO", flRATIO);
                IDbDataParameter parMAX_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MAX_RATIO_COMPLETE", flMAX_RATIO_COMPLETE);
                IDbDataParameter parMIN_RATIO_COMPLETE = Sql.AddParameter(cmd, "@MIN_RATIO_COMPLETE", flMIN_RATIO_COMPLETE);
                IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.AddParameter(cmd, "@DEFAULT_RATIO_COMPLETE", flDEFAULT_RATIO_COMPLETE);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 50);
                IDbDataParameter parMAX_POINT = Sql.AddParameter(cmd, "@MAX_POINT", nMAX_POINT);
                IDbDataParameter parMIN_POINT = Sql.AddParameter(cmd, "@MIN_POINT", nMIN_POINT);
                IDbDataParameter parDEFAULT_POINT = Sql.AddParameter(cmd, "@DEFAULT_POINT", nDEFAULT_POINT);
                IDbDataParameter parSORTING_ORDER = Sql.AddParameter(cmd, "@SORTING_ORDER", nSORTING_ORDER);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION, 300);
                IDbDataParameter parREMARK = Sql.AddParameter(cmd, "@REMARK", sREMARK, 300);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdM_KPIS_Update
        /// <summary>
        /// spM_KPIS_Update
        /// </summary>
        public static IDbCommand cmdM_KPIS_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPIS_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parNAME = Sql.CreateParameter(cmd, "@NAME", "string", 150);
            IDbDataParameter parKPI_CODE = Sql.CreateParameter(cmd, "@KPI_CODE", "string", 50);
            IDbDataParameter parKPI_NAME = Sql.CreateParameter(cmd, "@KPI_NAME", "string", 200);
            IDbDataParameter parPARENT_ID = Sql.CreateParameter(cmd, "@PARENT_ID", "string", 50);
            IDbDataParameter parLEVEL_NUMBER = Sql.CreateParameter(cmd, "@LEVEL_NUMBER", "Int32", 4);
            IDbDataParameter parUNIT = Sql.CreateParameter(cmd, "@UNIT", "Int32", 4);
            IDbDataParameter parRATIO = Sql.CreateParameter(cmd, "@RATIO", "float", 8);
            IDbDataParameter parMAX_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@MAX_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parMIN_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@MIN_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parDEFAULT_RATIO_COMPLETE = Sql.CreateParameter(cmd, "@DEFAULT_RATIO_COMPLETE", "float", 8);
            IDbDataParameter parSTATUS = Sql.CreateParameter(cmd, "@STATUS", "string", 50);
            IDbDataParameter parMAX_POINT = Sql.CreateParameter(cmd, "@MAX_POINT", "Int32", 4);
            IDbDataParameter parMIN_POINT = Sql.CreateParameter(cmd, "@MIN_POINT", "Int32", 4);
            IDbDataParameter parDEFAULT_POINT = Sql.CreateParameter(cmd, "@DEFAULT_POINT", "Int32", 4);
            IDbDataParameter parSORTING_ORDER = Sql.CreateParameter(cmd, "@SORTING_ORDER", "Int32", 4);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 300);
            IDbDataParameter parREMARK = Sql.CreateParameter(cmd, "@REMARK", "string", 300);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion
    }
}