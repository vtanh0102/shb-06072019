﻿using System;
using System.Configuration;
using log4net;

namespace SplendidCRM._modules
{
    public class LogService
    {
        public static readonly ILog DefaultInstanceLog =
            LogManager.GetLogger(ConfigurationManager.AppSettings["Log.InstanceName"] ?? string.Empty);

        #region ILogService Members        

        public void Debug(Type typeOfOwner, object message, Exception exception = null)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled)
                if (exception == null)
                    logger.Debug(message);
                else
                    logger.Debug(message, exception);
        }

        public void DebugFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled) logger.DebugFormat(provider, format, args);
        }

        public void DebugFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled) logger.DebugFormat(format, arg0, arg1, arg2);
        }

        public void DebugFormat(Type typeOfOwner, string format, object arg0, object arg1)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled) logger.DebugFormat(format, arg0, arg1);
        }

        public void DebugFormat(Type typeOfOwner, string format, object arg0)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled) logger.DebugFormat(format, arg0);
        }

        public void DebugFormat(Type typeOfOwner, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled) logger.DebugFormat(format, args);
        }

        public void Error(Type typeOfOwner, object message, Exception exception = null)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsDebugEnabled)
                if (exception == null)
                    logger.Error(message);
                else
                    logger.Error(message, exception);
        }

        public void ErrorFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsErrorEnabled) logger.ErrorFormat(provider, format, args);
        }

        public void ErrorFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsErrorEnabled) logger.ErrorFormat(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(Type typeOfOwner, string format, object arg0, object arg1)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsErrorEnabled) logger.ErrorFormat(format, arg0, arg1);
        }

        public void ErrorFormat(Type typeOfOwner, string format, object arg0)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsErrorEnabled) logger.ErrorFormat(format, arg0);
        }

        public void ErrorFormat(Type typeOfOwner, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsErrorEnabled) logger.ErrorFormat(format, args);
        }

        public void Fatal(Type typeOfOwner, object message, Exception exception = null)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled)
                if (exception == null)
                    logger.Fatal(message);
                else
                    logger.Fatal(message, exception);
        }

        public void FatalFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled) logger.FatalFormat(provider, format, args);
        }

        public void FatalFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled) logger.FatalFormat(format, arg0, arg1, arg2);
        }

        public void FatalFormat(Type typeOfOwner, string format, object arg0, object arg1)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled) logger.FatalFormat(format, arg0, arg1);
        }

        public void FatalFormat(Type typeOfOwner, string format, object arg0)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled) logger.FatalFormat(format, arg0);
        }

        public void FatalFormat(Type typeOfOwner, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsFatalEnabled) logger.FatalFormat(format, args);
        }

        public void Info(Type typeOfOwner, object message, Exception exception = null)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled)
                if (exception == null)
                    logger.Info(message);
                else
                    logger.Info(message, exception);
        }

        public void InfoFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled) logger.InfoFormat(provider, format, args);
        }

        public void InfoFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled) logger.InfoFormat(format, arg0, arg1, arg2);
        }

        public void InfoFormat(Type typeOfOwner, string format, object arg0, object arg1)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled) logger.InfoFormat(format, arg0, arg1);
        }

        public void InfoFormat(Type typeOfOwner, string format, object arg0)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled) logger.InfoFormat(format, arg0);
        }

        public void InfoFormat(Type typeOfOwner, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsInfoEnabled) logger.InfoFormat(format, args);
        }

        public void Warn(Type typeOfOwner, object message, Exception exception = null)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled)
                if (exception == null)
                    logger.Warn(message);
                else
                    logger.Warn(message, exception);
        }

        public void WarnFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled) logger.WarnFormat(provider, format, args);
        }

        public void WarnFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled) logger.WarnFormat(format, arg0, arg1, arg2);
        }

        public void WarnFormat(Type typeOfOwner, string format, object arg0, object arg1)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled) logger.WarnFormat(format, arg0, arg1);
        }

        public void WarnFormat(Type typeOfOwner, string format, object arg0)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled) logger.WarnFormat(format, arg0);
        }

        public void WarnFormat(Type typeOfOwner, string format, params object[] args)
        {
            ILog logger = GetLogger(typeOfOwner);
            if (logger.IsWarnEnabled) logger.WarnFormat(format, args);
        }

        #endregion

        private static ILog GetLogger(Type typeOfOwner)
        {
            ILog logger = typeOfOwner != null ? LogManager.GetLogger(typeOfOwner) : DefaultInstanceLog;
            return logger;
        }
    }
}