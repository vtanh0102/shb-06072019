﻿using System;
using System.Data;
using System.Data.Common;
using System.Xml;


namespace SplendidCRM._modules
{
    public class KPIM0102_SqlProc
    {

        #region spM_KPI_CONVERSION_RATE_Delete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Delete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Delete(Guid gID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Delete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Delete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Delete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Delete(Guid gID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Delete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_Delete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Delete
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_Delete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Delete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_MassDelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassDelete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_MassDelete(string sID_LIST)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDe";
                            else
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDelete";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_MassDelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassDelete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_MassDelete(string sID_LIST, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDe";
                else
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDelete";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_MassDelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassDelete
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_MassDelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDe";
            else
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassDelete";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_MassUpdate
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassUpdate
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUp";
                            else
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUpdate";
                            IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_MassUpdate
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassUpdate
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_MassUpdate(string sID_LIST, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, bool bTEAM_SET_ADD, string sTAG_SET_NAME, bool bTAG_SET_ADD, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUp";
                else
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUpdate";
                IDbDataParameter parID_LIST = Sql.AddAnsiParam(cmd, "@ID_LIST", sID_LIST, 8000);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTEAM_SET_ADD = Sql.AddParameter(cmd, "@TEAM_SET_ADD", bTEAM_SET_ADD);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                IDbDataParameter parTAG_SET_ADD = Sql.AddParameter(cmd, "@TAG_SET_ADD", bTAG_SET_ADD);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_MassUpdate
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_MassUpdate
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_MassUpdate(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUp";
            else
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_MassUpdate";
            IDbDataParameter parID_LIST = Sql.CreateParameter(cmd, "@ID_LIST", "ansistring", 8000);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTEAM_SET_ADD = Sql.CreateParameter(cmd, "@TEAM_SET_ADD", "bool", 1);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            IDbDataParameter parTAG_SET_ADD = Sql.CreateParameter(cmd, "@TAG_SET_ADD", "bool", 1);
            return cmd;
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Merge
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Merge
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Merge(Guid gID, Guid gMERGE_ID)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Merge";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Merge
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Merge
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Merge(Guid gID, Guid gMERGE_ID, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Merge";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parMERGE_ID = Sql.AddParameter(cmd, "@MERGE_ID", gMERGE_ID);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_Merge
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Merge
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_Merge(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Merge";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parMERGE_ID = Sql.CreateParameter(cmd, "@MERGE_ID", "Guid", 16);
            return cmd;
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Undelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Undelete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Undelete(Guid gID, string sAUDIT_TOKEN)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            if (Sql.IsOracle(cmd))
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undele";
                            else
                                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undelete";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                            cmd.ExecuteNonQuery();
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Undelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Undelete
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Undelete(Guid gID, string sAUDIT_TOKEN, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                if (Sql.IsOracle(cmd))
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undele";
                else
                    cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undelete";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parAUDIT_TOKEN = Sql.AddAnsiParam(cmd, "@AUDIT_TOKEN", sAUDIT_TOKEN, 255);
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_Undelete
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Undelete
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_Undelete(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            if (Sql.IsOracle(cmd))
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undele";
            else
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Undelete";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parAUDIT_TOKEN = Sql.CreateParameter(cmd, "@AUDIT_TOKEN", "ansistring", 255);
            return cmd;
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Update
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Update
        /// </summary>
        public static void spM_KPI_CONVERSION_RATE_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sTYPE, string sFROM_CODE, string sFROM_NAME, string sTO_CODE, string sTO_NAME, float flCONVERSION_RATE, float flCONVERSION_RATE1, float flCONVERSION_RATE2, string sSTATUS, string sDESCRIPTION, string sTAG_SET_NAME)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbTransaction trn = Sql.BeginTransaction(con))
                {
                    try
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Update";
                            IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                            IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                            IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                            IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                            IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                            IDbDataParameter parTYPE = Sql.AddParameter(cmd, "@TYPE", sTYPE, 10);
                            IDbDataParameter parFROM_CODE = Sql.AddParameter(cmd, "@FROM_CODE", sFROM_CODE, 50);
                            IDbDataParameter parFROM_NAME = Sql.AddParameter(cmd, "@FROM_NAME", sFROM_NAME, 200);
                            IDbDataParameter parTO_CODE = Sql.AddParameter(cmd, "@TO_CODE", sTO_CODE, 50);
                            IDbDataParameter parTO_NAME = Sql.AddParameter(cmd, "@TO_NAME", sTO_NAME, 200);
                            IDbDataParameter parCONVERSION_RATE = Sql.AddParameter(cmd, "@CONVERSION_RATE", flCONVERSION_RATE);
                            IDbDataParameter parCONVERSION_RATE1 = Sql.AddParameter(cmd, "@CONVERSION_RATE1", flCONVERSION_RATE1);
                            IDbDataParameter parCONVERSION_RATE2 = Sql.AddParameter(cmd, "@CONVERSION_RATE2", flCONVERSION_RATE2);
                            IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                            IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                            IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                            parID.Direction = ParameterDirection.InputOutput;
                            cmd.ExecuteNonQuery();
                            gID = Sql.ToGuid(parID.Value);
                        }
                        trn.Commit();
                    }
                    catch
                    {
                        trn.Rollback();
                        throw;
                    }
                }
            }
        }
        #endregion

        #region spM_KPI_CONVERSION_RATE_Update
        /// 

        /// spM_KPI_CONVERSION_RATE_Update
        /// 
        public static void spM_KPI_CONVERSION_RATE_Update(ref Guid gID, Guid gASSIGNED_USER_ID, Guid gTEAM_ID, string sTEAM_SET_LIST, string sTYPE, string sFROM_CODE, string sFROM_NAME, string sTO_CODE, string sTO_NAME, float flCONVERSION_RATE, float flCONVERSION_RATE1, float flCONVERSION_RATE2, string sSTATUS, string sDESCRIPTION, string sTAG_SET_NAME, IDbTransaction trn)
        {
            IDbConnection con = trn.Connection;
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.Transaction = trn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "spM_KPI_CONVERSION_RATE_Update";
                IDbDataParameter parID = Sql.AddParameter(cmd, "@ID", gID);
                IDbDataParameter parMODIFIED_USER_ID = Sql.AddParameter(cmd, "@MODIFIED_USER_ID", Security.USER_ID);
                IDbDataParameter parASSIGNED_USER_ID = Sql.AddParameter(cmd, "@ASSIGNED_USER_ID", gASSIGNED_USER_ID);
                IDbDataParameter parTEAM_ID = Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
                IDbDataParameter parTEAM_SET_LIST = Sql.AddAnsiParam(cmd, "@TEAM_SET_LIST", sTEAM_SET_LIST, 8000);
                IDbDataParameter parTYPE = Sql.AddParameter(cmd, "@TYPE", sTYPE, 10);
                IDbDataParameter parFROM_CODE = Sql.AddParameter(cmd, "@FROM_CODE", sFROM_CODE, 50);
                IDbDataParameter parFROM_NAME = Sql.AddParameter(cmd, "@FROM_NAME", sFROM_NAME, 200);
                IDbDataParameter parTO_CODE = Sql.AddParameter(cmd, "@TO_CODE", sTO_CODE, 50);
                IDbDataParameter parTO_NAME = Sql.AddParameter(cmd, "@TO_NAME", sTO_NAME, 200);
                IDbDataParameter parCONVERSION_RATE = Sql.AddParameter(cmd, "@CONVERSION_RATE", flCONVERSION_RATE);
                IDbDataParameter parCONVERSION_RATE1 = Sql.AddParameter(cmd, "@CONVERSION_RATE1", flCONVERSION_RATE1);
                IDbDataParameter parCONVERSION_RATE2 = Sql.AddParameter(cmd, "@CONVERSION_RATE2", flCONVERSION_RATE2);
                IDbDataParameter parSTATUS = Sql.AddParameter(cmd, "@STATUS", sSTATUS, 5);
                IDbDataParameter parDESCRIPTION = Sql.AddParameter(cmd, "@DESCRIPTION", sDESCRIPTION);
                IDbDataParameter parTAG_SET_NAME = Sql.AddParameter(cmd, "@TAG_SET_NAME", sTAG_SET_NAME, 4000);
                parID.Direction = ParameterDirection.InputOutput;
                Sql.Trace(cmd);
                cmd.ExecuteNonQuery();
                gID = Sql.ToGuid(parID.Value);
            }
        }
        #endregion

        #region cmdM_KPI_CONVERSION_RATE_Update
        /// <summary>
        /// spM_KPI_CONVERSION_RATE_Update
        /// </summary>
        public static IDbCommand cmdM_KPI_CONVERSION_RATE_Update(IDbConnection con)
        {
            IDbCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spM_KPI_CONVERSION_RATE_Update";
            IDbDataParameter parID = Sql.CreateParameter(cmd, "@ID", "Guid", 16);
            IDbDataParameter parMODIFIED_USER_ID = Sql.CreateParameter(cmd, "@MODIFIED_USER_ID", "Guid", 16);
            IDbDataParameter parASSIGNED_USER_ID = Sql.CreateParameter(cmd, "@ASSIGNED_USER_ID", "Guid", 16);
            IDbDataParameter parTEAM_ID = Sql.CreateParameter(cmd, "@TEAM_ID", "Guid", 16);
            IDbDataParameter parTEAM_SET_LIST = Sql.CreateParameter(cmd, "@TEAM_SET_LIST", "ansistring", 8000);
            IDbDataParameter parTYPE = Sql.CreateParameter(cmd, "@TYPE", "string", 10);
            IDbDataParameter parFROM_CODE = Sql.CreateParameter(cmd, "@FROM_CODE", "string", 50);
            IDbDataParameter parFROM_NAME = Sql.CreateParameter(cmd, "@FROM_NAME", "string", 200);
            IDbDataParameter parTO_CODE = Sql.CreateParameter(cmd, "@TO_CODE", "string", 50);
            IDbDataParameter parTO_NAME = Sql.CreateParameter(cmd, "@TO_NAME", "string", 200);
            IDbDataParameter parCONVERSION_RATE = Sql.CreateParameter(cmd, "@CONVERSION_RATE", "float", 8);
            IDbDataParameter parCONVERSION_RATE1 = Sql.CreateParameter(cmd, "@CONVERSION_RATE1", "float", 8);
            IDbDataParameter parCONVERSION_RATE2 = Sql.CreateParameter(cmd, "@CONVERSION_RATE2", "float", 8);
            IDbDataParameter parSTATUS = Sql.CreateParameter(cmd, "@STATUS", "string", 5);
            IDbDataParameter parDESCRIPTION = Sql.CreateParameter(cmd, "@DESCRIPTION", "string", 104857600);
            IDbDataParameter parTAG_SET_NAME = Sql.CreateParameter(cmd, "@TAG_SET_NAME", "string", 4000);
            parID.Direction = ParameterDirection.InputOutput;
            return cmd;
        }
        #endregion

    }
}