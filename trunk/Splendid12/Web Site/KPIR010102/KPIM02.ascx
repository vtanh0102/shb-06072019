<%@ Control CodeBehind="KPIM02.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.KPIR010102.KPIM02" %>
<script type="text/javascript">
function KPIM02Popup()
{
	return ModulePopup('KPIM02', '<%= txtM_KPI_ID.ClientID %>', null, 'ClearDisabled=1', true, null);
}
</script>
<input ID="txtM_KPI_ID" type="hidden" Runat="server" />
<%@ Register TagPrefix="SplendidCRM" Tagname="SubPanelButtons" Src="~/_controls/SubPanelButtons.ascx" %>
<SplendidCRM:SubPanelButtons ID="ctlDynamicButtons" Module="KPIM02" SubPanel="divKPIR010102KPIM02" Title="KPIM02.LBL_MODULE_NAME" Runat="Server" />

<div id="divKPIR010102KPIM02" style='<%= "display:" + (CookieValue("divKPIR010102KPIM02") != "1" ? "inline" : "none") %>'>
	<asp:Panel ID="pnlNewRecordInline" Visible='<%# !Sql.ToBoolean(Application["CONFIG.disable_editview_inline"]) %>' Style="display:none" runat="server">
		<%@ Register TagPrefix="SplendidCRM" Tagname="NewRecord" Src="~/KPIM02/NewRecord.ascx" %>
		<SplendidCRM:NewRecord ID="ctlNewRecord" Width="100%" EditView="EditView.Inline" ShowCancel="true" ShowHeader="false" ShowFullForm="true" ShowTopButtons="true" Runat="Server" />
	</asp:Panel>
	
	<%@ Register TagPrefix="SplendidCRM" Tagname="SearchView" Src="~/_controls/SearchView.ascx" %>
	<SplendidCRM:SearchView ID="ctlSearchView" Module="KPIM02" SearchMode="SearchSubpanel" IsSubpanelSearch="true" ShowSearchTabs="false" ShowDuplicateSearch="false" ShowSearchViews="false" Visible="false" Runat="Server" />
	
	<SplendidCRM:SplendidGrid id="grdMain" SkinID="grdSubPanelView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
		<Columns>
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:ImageButton Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("KPIM02", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="KPIM02.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "M_KPI_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_EDIT") %>' SkinID="edit_inline" Runat="server" />
					<asp:LinkButton  Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("KPIM02", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="KPIM02.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "M_KPI_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_EDIT") %>' Runat="server" />
					&nbsp;
					<span onclick="return confirm('<%= L10n.TermJavaScript("KPIR010102.NTC_REMOVE_B_KPI_REPORT_CONFIRMATION") %>')">
						<asp:ImageButton Visible='<%# SplendidCRM.Security.GetUserAccess("KPIR010102", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_REPORT_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="KPIM02.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "M_KPI_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_REMOVE") %>' SkinID="delete_inline" Runat="server" />
						<asp:LinkButton  Visible='<%# SplendidCRM.Security.GetUserAccess("KPIR010102", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_REPORT_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="KPIM02.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "M_KPI_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_REMOVE") %>' Runat="server" />
					</span>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
</div>
