<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIR010102.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    .tabForm input[type=text] {
        width: 75% !important;
    }

    #divGridEditSubPanel {
        overflow-x: scroll;
    }

    #ctl00_cntBody_ctlEditView_PERIOD_REQUIRED_SYMBOL {
        display: none;
    }
</style>
<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIR010102" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<SplendidCRM:InlineScript runat="server">
    <script type="text/javascript">
        $(function () {
            $("#ctl00_cntBody_ctlEditView_MONTH").change(function () {
                var val = this.value;
                if (val == null || val == '') {
                    $('#ctl00_cntBody_ctlEditView_PERIOD').show();
                    $('input[id="ctl00_cntBody_ctlEditView_PERIOD_0"]').prop('checked', true);
                } else {
                    $('#ctl00_cntBody_ctlEditView_PERIOD').hide();
                    $('input[name="ctl00$cntBody$ctlEditView$PERIOD"]').prop('checked', false);
                }
            });

            load_default_period();

            $("[id$=btnSAVE]").click(function (e) {
                $("[id$=ctl00_lblError]").text('');
                setCookie('downloadStarted', 0, 100); //Expiration could be anything... As long as we reset the value
                showModal();
                setTimeout(checkDownloadCookie, 1000); //Initiate the loop to check the cookie.
            });
            //employee name autocomplete
            $("[id$=ctlEditView_EMPLOYEES_NAME]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/KPIR010102/AutoComplete.asmx/GetEmployee") %>',
                        data: "{ 'prefixText': '" + request.term + "', 'count': 15}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                },
                change: function (event, ui) {
                    if (!ui.item) {
                        this.value = '';
                    }
                },
                minLength: 1,
                mustMatch: true,
                selectFirst: true
            });

        });

        function load_default_period() {
            var val = $("#ctl00_cntBody_ctlEditView_MONTH").val();
            if (val == null || val == '') {
                $('#ctl00_cntBody_ctlEditView_PERIOD').show();
                //$('input[id="ctl00_cntBody_ctlEditView_PERIOD_0"]').prop('checked', true);
            } else {
                $('#ctl00_cntBody_ctlEditView_PERIOD').hide();
                $('input[name="ctl00$cntBody$ctlEditView$PERIOD"]').prop('checked', false);
            }
        }

        function showModal() {
            $('body').loadingModal({ text: 'Loading...' });
            var delay = function (ms) { return new Promise(function (r) { setTimeout(r, ms) }) };
            var time = 2000;
            delay(time)
                    .then(function () { $('body').loadingModal('animation', 'rotatingPlane').loadingModal('backgroundColor', 'red'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'wave'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'wanderingCubes').loadingModal('backgroundColor', 'green'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'spinner'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'chasingDots').loadingModal('backgroundColor', 'blue'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'threeBounce'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'cubeGrid'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'fadingCircle').loadingModal('backgroundColor', 'gray'); return delay(time); })
                    .then(function () { $('body').loadingModal('animation', 'foldingCube'); return delay(time); });
            //.then(function () { $('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)').loadingModal('backgroundColor', 'yellow'); return delay(time); })
            //.then(function () { $('body').loadingModal('hide'); return delay(time); })
            //.then(function () { $('body').loadingModal('destroy'); });
        }
        var downloadTimeout;
        var checkDownloadCookie = function () {
            if (getCookie("downloadStarted") == 1) {
                setCookie("downloadStarted", "false", 100); //Expiration could be anything... As long as we reset the value
                // hide the loading modal
                $('body').loadingModal('hide');
                // destroy the plugin
                $('body').loadingModal('destroy');
            } else {
                downloadTimeout = setTimeout(checkDownloadCookie, 1000); //Re-run this function in 1 second.
            }
        };
        var setCookie = function (name, value, expiracy) {
            var exdate = new Date();
            exdate.setTime(exdate.getTime() + expiracy * 1000);
            var c_value = escape(value) + ((expiracy == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = name + "=" + c_value + '; path=/';
        };

        var getCookie = function (name) {
            var i, x, y, ARRcookies = document.cookie.split(";");
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == name) {
                    return y ? decodeURI(unescape(y.replace(/\+/g, ' '))) : y; //;//unescape(decodeURI(y));
                }
            }
        };

    </script>
</SplendidCRM:InlineScript>
<div id="divGridEditSubPanel">
    <%-- Grid Top--%>
    <SplendidCRM:SplendidGrid ID="grdMainTop" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--    <asp:TemplateColumn HeaderText=".LBL_LIST_POS_CODE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_CODE" runat="server" Text='<%# Bind("POS_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_NAME" ItemStyle-Width="25%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_NAME" runat="server" Text='<%# Bind("POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--<asp:TemplateColumn HeaderText=".LBL_LIST_MAIN_POS_CODE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMAIN_POS_CODE" runat="server" Text='<%# Bind("MAIN_POS_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText=".LBL_LIST_MAIN_POS_NAME" ItemStyle-Width="25%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMAIN_POS_NAME" runat="server" Text='<%# Bind("MAIN_POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_REGION" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREGION_NAME" runat="server" Text='<%# Bind("REGION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_AREA" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblAREA_NAME" runat="server" Text='<%# Bind("AREA_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_PERIOD" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMONTH_PERIOD" runat="server" Text='<%# Bind("MONTH_PERIOD") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_PERCENT_FINAL_VALUE" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Label ID="lblPERCENT_FINAL_TOTAL" runat="server" Text='<%# Bind("PERCENT_FINAL_TOTAL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

    <%-- Grid Thu thuan CV--%>
    <SplendidCRM:SplendidGrid ID="grdMainCV" AllowPaging="<%# !PrintView %>" PageSize="20" AllowSorting="true" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="3%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIB030301.LBL_LIST_USER_CODE" ItemStyle-Width="3%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblUSER_CODE" runat="server" Text='<%# Bind("USER_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIB0203.LBL_EMPLOYEE_NAME" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFULL_NAME" runat="server" Text='<%# Bind("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIM0101.LBL_LIST_POSITION_ID" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOSITION_NAME" runat="server" Text='<%# Bind("POSITION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_MAIN_POS_NAME" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMAIN_POS_NAME" runat="server" Text='<%# Bind("MAIN_POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_NAME" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_NAME" runat="server" Text='<%# Bind("POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIM0106.LBL_LIST_REGION_ID" ItemStyle-Width="8%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREGION_NAME" runat="server" Text='<%# Bind("REGION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_PERIOD" ItemStyle-Width="3%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMONTH_PERIOD" runat="server" Text='<%# Bind("MONTH_PERIOD") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_1" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_1" runat="server" Text='<%# Bind("FINAL_VALUE_1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_2" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_2" runat="server" Text='<%# Bind("FINAL_VALUE_2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_3" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_3" runat="server" Text='<%# Bind("FINAL_VALUE_3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_4" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_4" runat="server" Text='<%# Bind("FINAL_VALUE_4") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_OTHER" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblOTHER" runat="server" Text='<%# Bind("FINAL_VALUE_5") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_6" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_6" runat="server" Text='<%# Bind("FINAL_VALUE_6") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_REMARK" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREMARK" runat="server" Text='<%# Bind("REMARK") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

    <%-- Grid Thu thuan DVKD--%>
    <SplendidCRM:SplendidGrid ID="grdMainOrg" AllowPaging="<%# !PrintView %>" PageSize="20" AllowSorting="true" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_CODE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_CODE" runat="server" Text='<%# Bind("POS_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_NAME" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_NAME" runat="server" Text='<%# Bind("POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIM0106.LBL_LIST_REGION_ID" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREGION_NAME" runat="server" Text='<%# Bind("REGION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%-- 
            <asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_PERIOD" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMONTH_PERIOD" runat="server" Text='<%# Bind("MONTH_PERIOD") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_1" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_1" runat="server" Text='<%# Bind("FINAL_VALUE_1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_2" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_2" runat="server" Text='<%# Bind("FINAL_VALUE_2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_3" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_3" runat="server" Text='<%# Bind("FINAL_VALUE_3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_4" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_4" runat="server" Text='<%# Bind("FINAL_VALUE_4") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_OTHER" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblOTHER" runat="server" Text='<%# Bind("FINAL_VALUE_5") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_6" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_6" runat="server" Text='<%# Bind("FINAL_VALUE_6") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_REMARK" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREMARK" runat="server" Text='<%# Bind("REMARK") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

    <%-- Grid Thu thuan Vung--%>
    <SplendidCRM:SplendidGrid ID="grdMainRegion" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIM0106.LBL_LIST_REGION_ID" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREGION_NAME" runat="server" Text='<%# Bind("REGION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <%--<asp:TemplateColumn HeaderText="KPIR010102.LBL_LIST_PERIOD" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMONTH_PERIOD" runat="server" Text='<%# Bind("MONTH_PERIOD") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_1" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_1" runat="server" Text='<%# Bind("FINAL_VALUE_1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_2" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_2" runat="server" Text='<%# Bind("FINAL_VALUE_2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_3" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_3" runat="server" Text='<%# Bind("FINAL_VALUE_3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_4" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_4" runat="server" Text='<%# Bind("FINAL_VALUE_4") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_OTHER" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblOTHER" runat="server" Text='<%# Bind("FINAL_VALUE_5") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_FINAL_VALUE_6" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFINAL_VALUE_6" runat="server" Text='<%# Bind("FINAL_VALUE_6") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_REMARK" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREMARK" runat="server" Text='<%# Bind("REMARK") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>
    <%-- Grid Ca nhan Top--%>
    <SplendidCRM:SplendidGrid ID="grdMainCN" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
        <Columns>
            <asp:TemplateColumn HeaderText=".LBL_LIST_NO" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIM0106.LBL_LIST_REGION_ID" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblREGION_NAME" runat="server" Text='<%# Bind("REGION_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_CODE" ItemStyle-Width="10%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_CODE" runat="server" Text='<%# Bind("POS_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_POS_NAME" ItemStyle-Width="20%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblPOS_NAME" runat="server" Text='<%# Bind("POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_MAIN_POS_CODE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMAIN_POS_CODE" runat="server" Text='<%# Bind("MAIN_POS_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_MAIN_POS_NAME" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblMAIN_POS_NAME" runat="server" Text='<%# Bind("MAIN_POS_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIB030301.LBL_LIST_USER_CODE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblUSER_CODE" runat="server" Text='<%# Bind("USER_CODE") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="KPIB0203.LBL_EMPLOYEE_NAME" ItemStyle-Width="15%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemTemplate>
                    <asp:Label ID="lblFULL_NAME" runat="server" Text='<%# Bind("FULL_NAME") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText=".LBL_LIST_PERCENT_FINAL_VALUE" ItemStyle-Width="5%">
                <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                <ItemStyle HorizontalAlign="Right" />
                <ItemTemplate>
                    <asp:Label ID="lblPERCENT_FINAL_TOTAL" runat="server" Text='<%# Bind("PERCENT_FINAL_TOTAL") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </SplendidCRM:SplendidGrid>

</div>
<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
