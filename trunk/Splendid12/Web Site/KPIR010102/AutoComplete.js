
function B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed(fldB_KPI_REPORT_REPORT_TYPE)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain REPORT_TYPE in the text, so just get the length minus 4. 
	var userContext = fldB_KPI_REPORT_REPORT_TYPE.id.substring(0, fldB_KPI_REPORT_REPORT_TYPE.id.length - 'B_KPI_REPORT_REPORT_TYPE'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_REPORT_REPORT_TYPE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_KPI_REPORT_REPORT_TYPE = document.getElementById(userContext + 'PREV_B_KPI_REPORT_REPORT_TYPE');
	if ( fldPREV_B_KPI_REPORT_REPORT_TYPE == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_KPI_REPORT_REPORT_TYPE');
	}
	else if ( fldPREV_B_KPI_REPORT_REPORT_TYPE.value != fldB_KPI_REPORT_REPORT_TYPE.value )
	{
		if ( fldB_KPI_REPORT_REPORT_TYPE.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIR010102.AutoComplete.B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Get(fldB_KPI_REPORT_REPORT_TYPE.value, B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed_OnSucceededWithContext, B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sREPORT_TYPE = result.REPORT_TYPE;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_KPI_REPORT_REPORT_TYPE_AjaxErrors');
		var fldB_KPI_REPORT_ID        = document.getElementById(userContext + 'B_KPI_REPORT_ID'       );
		var fldB_KPI_REPORT_REPORT_TYPE      = document.getElementById(userContext + 'B_KPI_REPORT_REPORT_TYPE'     );
		var fldPREV_B_KPI_REPORT_REPORT_TYPE = document.getElementById(userContext + 'PREV_B_KPI_REPORT_REPORT_TYPE');
		if ( fldB_KPI_REPORT_ID        != null ) fldB_KPI_REPORT_ID.value        = sID  ;
		if ( fldB_KPI_REPORT_REPORT_TYPE      != null ) fldB_KPI_REPORT_REPORT_TYPE.value      = sREPORT_TYPE;
		if ( fldPREV_B_KPI_REPORT_REPORT_TYPE != null ) fldPREV_B_KPI_REPORT_REPORT_TYPE.value = sREPORT_TYPE;
	}
	else
	{
		alert('result from KPIR010102.AutoComplete service is null');
	}
}

function B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_REPORT_REPORT_TYPE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_KPI_REPORT_ID        = document.getElementById(userContext + 'B_KPI_REPORT_ID'       );
	var fldB_KPI_REPORT_REPORT_TYPE      = document.getElementById(userContext + 'B_KPI_REPORT_REPORT_TYPE'     );
	var fldPREV_B_KPI_REPORT_REPORT_TYPE = document.getElementById(userContext + 'PREV_B_KPI_REPORT_REPORT_TYPE');
	if ( fldB_KPI_REPORT_ID        != null ) fldB_KPI_REPORT_ID.value        = '';
	if ( fldB_KPI_REPORT_REPORT_TYPE      != null ) fldB_KPI_REPORT_REPORT_TYPE.value      = '';
	if ( fldPREV_B_KPI_REPORT_REPORT_TYPE != null ) fldPREV_B_KPI_REPORT_REPORT_TYPE.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

