using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web.Services;
using System.ComponentModel;
using SplendidCRM;
using System.Collections.Generic;

namespace SplendidCRM.KPIR010102
{
    public class KPIR010102
    {
        public Guid ID;
        public string REPORT_TYPE;

        public KPIR010102()
        {
            ID = Guid.Empty;
            REPORT_TYPE = String.Empty;
        }
    }

    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    [ToolboxItem(false)]
    public class AutoComplete : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string getStatusExport()
        {
            if (!Security.IsAuthenticated())
                throw (new Exception("Authentication required"));
            return string.Empty;
        }


        [WebMethod(EnableSession = true)]
        public string[] GetEmployee(string prefixText, int count)
        {
            List<string> lstItem = new List<string>();
            try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "SELECT DISTINCT           " + ControlChars.CrLf
                         + "  ID, FULL_NAME           " + ControlChars.CrLf
                         + "  FROM vwEMPLOYEES_List   " + ControlChars.CrLf
                         + "  WHERE 1=1               " + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, "KPIR010102", "list");
                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "FULL_NAME");
                        cmd.CommandText += " ORDER BY FULL_NAME " + ControlChars.CrLf;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(0, count, dt);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    lstItem.Add(string.Format("{0}-{1}", dt.Rows[i]["FULL_NAME"], dt.Rows[i]["FULL_NAME"]));
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return lstItem.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public KPIR010102 B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_Get(string sREPORT_TYPE)
        {
            KPIR010102 item = new KPIR010102();
            //try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "select ID        " + ControlChars.CrLf
                         + "     , REPORT_TYPE      " + ControlChars.CrLf
                         + "  from vwB_KPI_REPORT" + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                        //Security.Filter(cmd, "KPIR010102", "list");

                        Sql.AppendParameter(cmd, sREPORT_TYPE, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "REPORT_TYPE");
                        cmd.CommandText += " order by REPORT_TYPE" + ControlChars.CrLf;
                        using (IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            if (rdr.Read())
                            {
                                item.ID = Sql.ToGuid(rdr["ID"]);
                                item.REPORT_TYPE = Sql.ToString(rdr["REPORT_TYPE"]);
                            }
                        }
                    }
                }
                if (Sql.IsEmptyGuid(item.ID))
                {
                    string sCULTURE = Sql.ToString(Session["USER_SETTINGS/CULTURE"]);
                    L10N L10n = new L10N(sCULTURE);
                    throw (new Exception(L10n.Term("KPIR010102.ERR_B_KPI_REPORT_NOT_FOUND")));
                }
            }
            //catch
            {
                // 02/04/2007 Paul.  Don't catch the exception.  
                // It is a web service, so the exception will be handled properly by the AJAX framework. 
            }
            return item;
        }

        [WebMethod(EnableSession = true)]
        public string[] B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_List(string prefixText, int count)
        {
            string[] arrItems = new string[0];
            try
            {
                if (!Security.IsAuthenticated())
                    throw (new Exception("Authentication required"));

                SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "select distinct  " + ControlChars.CrLf
                         + "       REPORT_TYPE      " + ControlChars.CrLf
                         + "  from vwB_KPI_REPORT" + ControlChars.CrLf;
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                        //Security.Filter(cmd, "KPIR010102", "list");

                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "REPORT_TYPE");
                        cmd.CommandText += " order by REPORT_TYPE" + ControlChars.CrLf;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(0, count, dt);
                                arrItems = new string[dt.Rows.Count];
                                for (int i = 0; i < dt.Rows.Count; i++)
                                    arrItems[i] = Sql.ToString(dt.Rows[i]["REPORT_TYPE"]);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return arrItems;
        }

        [WebMethod(EnableSession = true)]
        public string[] B_KPI_REPORT_REPORT_TYPE_List(string prefixText, int count)
        {
            return B_KPI_REPORT_B_KPI_REPORT_REPORT_TYPE_List(prefixText, count);
        }
    }
}

