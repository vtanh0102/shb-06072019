using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIR010102
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected DropDownList ddlReportType;
        protected DropDownList ddlBRANCH;
        protected DropDownList ddlDEPARTMENT;
        protected DropDownList ddlMONTH;
        protected DropDownList ddlYEAR;
        protected DropDownList ddlORG_TYPE;
        protected RadioButtonList rblPERIOD;
        DropDownList ddlREGION;

        protected DataView vwMainTop;
        protected DataView vwMainCV;
        protected DataView vwMainOrg;
        protected DataView vwMainRegion;
        protected DataView vwMainCN;

        //grid
        protected SplendidGrid grdMainTop;//top dvkd
        protected SplendidGrid grdMainCV;//chuyen vien
        protected SplendidGrid grdMainOrg;//dvkd
        protected SplendidGrid grdMainRegion;//vung
        protected SplendidGrid grdMainCN;//ca nhan

        protected void grdMainTop_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblPERCENT_FINAL_TOTAL = (Label)e.Item.FindControl("lblPERCENT_FINAL_TOTAL");
                if (lblPERCENT_FINAL_TOTAL != null)
                {
                    lblPERCENT_FINAL_TOTAL.Text = KPIs_Utils.FormatFloat(lblPERCENT_FINAL_TOTAL.Text, format_number);
                }
            }
        }

        protected void grdMainCV_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblFINAL_VALUE_1 = (Label)e.Item.FindControl("lblFINAL_VALUE_1");
                if (lblFINAL_VALUE_1 != null)
                {
                    lblFINAL_VALUE_1.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_1.Text, format_number);
                }
                Label lblFINAL_VALUE_2 = (Label)e.Item.FindControl("lblFINAL_VALUE_2");
                if (lblFINAL_VALUE_2 != null)
                {
                    lblFINAL_VALUE_2.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_2.Text, format_number);
                }
                Label lblFINAL_VALUE_3 = (Label)e.Item.FindControl("lblFINAL_VALUE_3");
                if (lblFINAL_VALUE_3 != null)
                {
                    lblFINAL_VALUE_3.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_3.Text, format_number);
                }
                Label lblFINAL_VALUE_4 = (Label)e.Item.FindControl("lblFINAL_VALUE_4");
                if (lblFINAL_VALUE_4 != null)
                {
                    lblFINAL_VALUE_4.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_4.Text, format_number);
                }
                Label lblOTHER = (Label)e.Item.FindControl("lblOTHER");
                if (lblOTHER != null)
                {
                    lblOTHER.Text = KPIs_Utils.FormatFloat(lblOTHER.Text, format_number);
                }
                Label lblFINAL_VALUE_6 = (Label)e.Item.FindControl("lblFINAL_VALUE_6");
                if (lblFINAL_VALUE_6 != null)
                {
                    lblFINAL_VALUE_6.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_6.Text, format_number);
                }
            }
        }

        protected void grdMainOrg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblFINAL_VALUE_1 = (Label)e.Item.FindControl("lblFINAL_VALUE_1");
                if (lblFINAL_VALUE_1 != null)
                {
                    lblFINAL_VALUE_1.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_1.Text, format_number);
                }
                Label lblFINAL_VALUE_2 = (Label)e.Item.FindControl("lblFINAL_VALUE_2");
                if (lblFINAL_VALUE_2 != null)
                {
                    lblFINAL_VALUE_2.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_2.Text, format_number);
                }
                Label lblFINAL_VALUE_3 = (Label)e.Item.FindControl("lblFINAL_VALUE_3");
                if (lblFINAL_VALUE_3 != null)
                {
                    lblFINAL_VALUE_3.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_3.Text, format_number);
                }
                Label lblFINAL_VALUE_4 = (Label)e.Item.FindControl("lblFINAL_VALUE_4");
                if (lblFINAL_VALUE_4 != null)
                {
                    lblFINAL_VALUE_4.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_4.Text, format_number);
                }
                Label lblOTHER = (Label)e.Item.FindControl("lblOTHER");
                if (lblOTHER != null)
                {
                    lblOTHER.Text = KPIs_Utils.FormatFloat(lblOTHER.Text, format_number);
                }
                Label lblFINAL_VALUE_6 = (Label)e.Item.FindControl("lblFINAL_VALUE_6");
                if (lblFINAL_VALUE_6 != null)
                {
                    lblFINAL_VALUE_6.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_6.Text, format_number);
                }
            }
        }

        protected void grdMainRegion_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblFINAL_VALUE_1 = (Label)e.Item.FindControl("lblFINAL_VALUE_1");
                if (lblFINAL_VALUE_1 != null)
                {
                    lblFINAL_VALUE_1.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_1.Text, format_number);
                }
                Label lblFINAL_VALUE_2 = (Label)e.Item.FindControl("lblFINAL_VALUE_2");
                if (lblFINAL_VALUE_2 != null)
                {
                    lblFINAL_VALUE_2.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_2.Text, format_number);
                }
                Label lblFINAL_VALUE_3 = (Label)e.Item.FindControl("lblFINAL_VALUE_3");
                if (lblFINAL_VALUE_3 != null)
                {
                    lblFINAL_VALUE_3.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_3.Text, format_number);
                }
                Label lblFINAL_VALUE_4 = (Label)e.Item.FindControl("lblFINAL_VALUE_4");
                if (lblFINAL_VALUE_4 != null)
                {
                    lblFINAL_VALUE_4.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_4.Text, format_number);
                }
                Label lblOTHER = (Label)e.Item.FindControl("lblOTHER");
                if (lblOTHER != null)
                {
                    lblOTHER.Text = KPIs_Utils.FormatFloat(lblOTHER.Text, format_number);
                }
                Label lblFINAL_VALUE_6 = (Label)e.Item.FindControl("lblFINAL_VALUE_6");
                if (lblFINAL_VALUE_6 != null)
                {
                    lblFINAL_VALUE_6.Text = KPIs_Utils.FormatFloat(lblFINAL_VALUE_6.Text, format_number);
                }
            }
        }

        protected void grdMainCN_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblPERCENT_FINAL_TOTAL = (Label)e.Item.FindControl("lblPERCENT_FINAL_TOTAL");
                if (lblPERCENT_FINAL_TOTAL != null)
                {
                    lblPERCENT_FINAL_TOTAL.Text = KPIs_Utils.FormatFloat(lblPERCENT_FINAL_TOTAL.Text, format_number);
                }
                //Label lblNO = (Label)e.Item.FindControl("lblNO");
                //if (lblNO != null)
                //{
                //    lblNO.Text = (index += 1).ToString();
                //}
            }
        }
        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        string reportType = ddlReportType.SelectedValue;
                        DataTable dtCurrent = (DataTable)ViewState["CurrentTable"];
                        ddlORG_TYPE = this.FindControl("ORG_TYPE") as DropDownList;
                        ddlYEAR = this.FindControl("YEAR") as DropDownList;
                        string orgType = string.Empty;
                        string strYEAR = string.Empty;
                        if (ddlORG_TYPE != null)
                        {
                            orgType = ddlORG_TYPE.Text;
                        }
                        if (ddlYEAR != null)
                        {
                            strYEAR = ddlYEAR.Text;
                        }
                        //Bao cao Top Phong
                        if (KPIs_Constant.KPI_REPORT_TYPE_R001_1.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R001-1.xlsx");
                                KPIs_Export.ExportTopBottomDepartment(dtCurrent, templatePath, orgType, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao Top Chi Nhanh
                        if (KPIs_Constant.KPI_REPORT_TYPE_R001_2.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R001-2.xlsx");
                                KPIs_Export.ExportTopBottomBranch(dtCurrent, templatePath, orgType, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao Bottom phong
                        if (KPIs_Constant.KPI_REPORT_TYPE_R001_3.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R001-3.xlsx");
                                KPIs_Export.ExportTopBottomDepartment(dtCurrent, templatePath, orgType, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao Bottom chi nhanh
                        if (KPIs_Constant.KPI_REPORT_TYPE_R001_4.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R001-4.xlsx");
                                KPIs_Export.ExportTopBottomBranch(dtCurrent, templatePath, orgType, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao thu thuan theo CV
                        if (KPIs_Constant.KPI_REPORT_TYPE_R002_1.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R002-1.xlsx");
                                KPIs_Export.ExportExcel(dtCurrent, templatePath, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao thu thuan theo DVKD
                        if (KPIs_Constant.KPI_REPORT_TYPE_R002_2.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R002-2.xlsx");
                                KPIs_Export.ExportExcelOrg(dtCurrent, templatePath, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao thu thuan theo vung
                        if (KPIs_Constant.KPI_REPORT_TYPE_R002_3.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R002-3.xlsx");
                                KPIs_Export.ExportExcelRegion(dtCurrent, templatePath, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao top ca nhan
                        if (KPIs_Constant.KPI_REPORT_TYPE_R004_1.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R004-1.xlsx");
                                KPIs_Export.ExportExcelTopCaNhan(dtCurrent, templatePath, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                        //Bao cao bottom ca nhan
                        if (KPIs_Constant.KPI_REPORT_TYPE_R004_2.Equals(reportType))
                        {
                            if (dtCurrent.Rows.Count > 0)
                            {
                                string templatePath = Server.MapPath(@"~\Include\Template\R004-2.xlsx");
                                KPIs_Export.ExportExcelBottomCaNhan(dtCurrent, templatePath, strYEAR);
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
            else if (e.CommandName == "Search")
            {
                string reportType = ddlReportType.SelectedValue;
                //Bao cao Top Phong 
                if (KPIs_Constant.KPI_REPORT_TYPE_R001_1.Equals(reportType))
                {
                    grdMainTop.CurrentPageIndex = 0;
                    grdMainTop.DataBind();
                }
                //Top chi nhanh
                if (KPIs_Constant.KPI_REPORT_TYPE_R001_2.Equals(reportType))
                {
                    grdMainTop.CurrentPageIndex = 0;
                    grdMainTop.DataBind();
                }
                //Bottom phong
                if (KPIs_Constant.KPI_REPORT_TYPE_R001_3.Equals(reportType))
                {
                    grdMainTop.CurrentPageIndex = 0;
                    grdMainTop.DataBind();
                }
                //Bottom chi nhanh
                if (KPIs_Constant.KPI_REPORT_TYPE_R001_4.Equals(reportType))
                {
                    grdMainTop.CurrentPageIndex = 0;
                    grdMainTop.DataBind();
                }
                //Bao cao thu thuan theo CV
                if (KPIs_Constant.KPI_REPORT_TYPE_R002_1.Equals(reportType))
                {
                    grdMainCV.CurrentPageIndex = 0;
                    grdMainCV.DataBind();
                }
                //Bao cao thu thuan theo DVKD
                if (KPIs_Constant.KPI_REPORT_TYPE_R002_2.Equals(reportType))
                {
                    grdMainOrg.CurrentPageIndex = 0;
                    grdMainOrg.DataBind();
                }
                //Bao cao thu thuan theo vung
                if (KPIs_Constant.KPI_REPORT_TYPE_R002_3.Equals(reportType))
                {
                    grdMainRegion.CurrentPageIndex = 0;
                    grdMainRegion.DataBind();
                }
                //Bao cao top ca nhan
                if (KPIs_Constant.KPI_REPORT_TYPE_R004_1.Equals(reportType))
                {
                    grdMainCN.CurrentPageIndex = 0;
                    grdMainCN.DataBind();
                }
                //Bao cao bottom ca nhan
                if (KPIs_Constant.KPI_REPORT_TYPE_R004_2.Equals(reportType))
                {
                    grdMainCN.CurrentPageIndex = 0;
                    grdMainCN.DataBind();
                }
            }
            else
            {
                if (Page.Master is SplendidMaster)
                    (Page.Master as SplendidMaster).Page_Command(sender, e);
            }
        }


        private void LoadDataGrid()
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                DataTable dtCurrent = new DataTable();
                string sSQL;

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //Security.Filter(cmd, m_sMODULE, "edit");
                    ddlYEAR = this.FindControl("YEAR") as DropDownList;
                    ddlREGION = this.FindControl("REGION") as DropDownList;
                    ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
                    DropDownList ddlAREA = this.FindControl("AREA") as DropDownList;
                    ddlDEPARTMENT = this.FindControl("DEPARTMENT") as DropDownList;
                    TextBox txtEMPLOYEES_NAME = this.FindControl("EMPLOYEES_NAME") as TextBox;
                    DropDownList ddlPOSITION = this.FindControl("POSITION") as DropDownList;
                    //TextBox txtKPI_TYPE = this.FindControl("KPI_TYPE") as TextBox;
                    //TextBox txtKPIs = this.FindControl("KPIs") as TextBox;
                    ddlORG_TYPE = this.FindControl("ORG_TYPE") as DropDownList;
                    rblPERIOD = this.FindControl("PERIOD") as RadioButtonList;
                    ddlMONTH = this.FindControl("MONTH") as DropDownList;
                    string reportType = ddlReportType.SelectedValue;

                    //Check role
                    if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                    {
                        //CRM_05
                        if (ddlBRANCH != null)
                        {
                            ddlBRANCH.SelectedValue = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;
                            ddlBRANCH.Enabled = false;
                        }

                        //CRM_06
                        if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (ddlDEPARTMENT != null)
                            {
                                ddlDEPARTMENT.SelectedValue = Security.CURRENT_EMPLOYEE.POS_ID;
                                ddlDEPARTMENT.Enabled = false;
                            }
                        }

                        //CRM_07
                        if ("CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (txtEMPLOYEES_NAME != null)
                            {
                                txtEMPLOYEES_NAME.Text = Security.CURRENT_EMPLOYEE.FULL_NAME;
                                txtEMPLOYEES_NAME.Enabled = false;
                            }

                            if (ddlPOSITION != null)
                            {
                                ddlPOSITION.SelectedValue = Security.CURRENT_EMPLOYEE.POSITION_ID;
                                ddlPOSITION.Enabled = false;
                            }

                            if (ddlAREA != null)
                            {
                                ddlAREA.SelectedValue = Security.CURRENT_EMPLOYEE.AREA_ID;
                                ddlAREA.Enabled = false;
                            }

                            if (ddlREGION != null)
                            {
                                ddlREGION.SelectedValue = Security.CURRENT_EMPLOYEE.REGION_ID;
                                ddlREGION.Enabled = false;
                            }

                            if (ddlORG_TYPE != null)
                            {
                                ddlORG_TYPE.SelectedValue = Security.CURRENT_EMPLOYEE.BUSINESS_CODE;
                                ddlORG_TYPE.Enabled = false;
                            }
                        }
                    }//End Check role


                    //Bao cao Top Phong 
                    if (KPIs_Constant.KPI_REPORT_TYPE_R001_1.Equals(reportType))
                    {
                        string orgType = string.Empty;
                        bool isTotalDetails = true;
                        if (ddlORG_TYPE != null)
                        {
                            orgType = ddlORG_TYPE.SelectedValue;
                        }
                        if (rblPERIOD != null)
                        {
                            isTotalDetails = Sql.ToBoolean(rblPERIOD.SelectedValue);
                        }
                        sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, *     " + ControlChars.CrLf
                                                          + "  from vwBAOCAO_TOP_BOTTOM_DEPARTMENT   where 1=1  " + ControlChars.CrLf
                                                          + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)  " + ControlChars.CrLf;
                        if (isTotalDetails == false)
                        {
                            sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                 + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                 + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                 + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                 + "  from vwBAOCAO_TOP_BOTTOM_DEPARTMENT                                 " + ControlChars.CrLf
                                 + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME , AREA_NAME, REGION_NAME        " + ControlChars.CrLf
                                 + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                        }
                        if (KPIs_Constant.ORG_TYPE_PGD.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, *     " + ControlChars.CrLf
                                                         + "  from vwBAOCAO_TOP_BOTTOM_PGD   where 1=1              " + ControlChars.CrLf
                                                         + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)         " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_PGD                                        " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME         " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }
                        }
                        else if (KPIs_Constant.ORG_TYPE_KHCN.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, * " + ControlChars.CrLf
                                                         + "  from vwBAOCAO_TOP_BOTTOM_KHCN  where 1=1          " + ControlChars.CrLf
                                                         + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)     " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_KHCN                                       " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME         " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }
                        }
                        else if (KPIs_Constant.ORG_TYPE_DVKH.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, *   " + ControlChars.CrLf
                                                            + "  from vwBAOCAO_TOP_BOTTOM_DVKH  where 1=1         " + ControlChars.CrLf
                                                            + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)    " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_DVKH                                       " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME         " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }

                        }
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//Vung  
                        }
                        if (ddlAREA != null && ddlAREA.Items.Count > 0 && ddlAREA.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlAREA.SelectedValue, Sql.SqlFilterMode.Contains, "AREA_ID");//Khu vuc  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainTop.Visible = true;
                                vwMainTop = dtCurrent.DefaultView;
                                grdMainTop.DataSource = vwMainTop;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainTop.ApplySort();
                                    grdMainTop.CurrentPageIndex = 0;
                                    grdMainTop.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }

                    }
                    //Bao cao Bottom Phong 
                    if (KPIs_Constant.KPI_REPORT_TYPE_R001_3.Equals(reportType))
                    {
                        string orgType = string.Empty;
                        bool isTotalDetails = true;
                        if (ddlORG_TYPE != null)
                        {
                            orgType = ddlORG_TYPE.SelectedValue;
                        }
                        if (rblPERIOD != null)
                        {
                            isTotalDetails = Sql.ToBoolean(rblPERIOD.SelectedValue);
                        }
                        sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, *      " + ControlChars.CrLf
                                                          + "  from vwBAOCAO_TOP_BOTTOM_DEPARTMENT   where 1=1  " + ControlChars.CrLf
                                                          + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)    " + ControlChars.CrLf;
                        if (isTotalDetails == false)
                        {
                            sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                 + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                 + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                 + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                 + "  from vwBAOCAO_TOP_BOTTOM_DEPARTMENT                                 " + ControlChars.CrLf
                                 + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME , AREA_NAME, REGION_NAME        " + ControlChars.CrLf
                                 + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                        }
                        if (KPIs_Constant.ORG_TYPE_PGD.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, *      " + ControlChars.CrLf
                                                         + "  from vwBAOCAO_TOP_BOTTOM_PGD   where 1=1              " + ControlChars.CrLf
                                                         + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)         " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_PGD                                        " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME         " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }
                        }
                        else if (KPIs_Constant.ORG_TYPE_KHCN.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, *  " + ControlChars.CrLf
                                                         + "  from vwBAOCAO_TOP_BOTTOM_KHCN  where 1=1          " + ControlChars.CrLf
                                                         + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)     " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_KHCN                                       " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME        " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }
                        }
                        else if (KPIs_Constant.ORG_TYPE_DVKH.Equals(orgType))
                        {
                            sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, *    " + ControlChars.CrLf
                                                            + "  from vwBAOCAO_TOP_BOTTOM_DVKH  where 1=1         " + ControlChars.CrLf
                                                            + "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)    " + ControlChars.CrLf;
                            if (isTotalDetails == false)
                            {
                                sSQL = " WITH ctemp AS ( " + ControlChars.CrLf
                                     + "  select  '' as MONTH_PERIOD, YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME  " + ControlChars.CrLf
                                     + "  , AREA_NAME, REGION_NAME                                            " + ControlChars.CrLf
                                     + "  , ROUND(SUM(PERCENT_FINAL_TOTAL)/12, 0) AS PERCENT_FINAL_TOTAL      " + ControlChars.CrLf
                                     + "  from vwBAOCAO_TOP_BOTTOM_DVKH                                       " + ControlChars.CrLf
                                     + "  GROUP BY YEAR , MAIN_POS_CODE , MAIN_POS_NAME , POS_CODE , POS_NAME, AREA_NAME, REGION_NAME        " + ControlChars.CrLf
                                     + "  ) select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, * FROM ctemp where 1 =1 " + ControlChars.CrLf;
                            }

                        }
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//Vung  
                        }
                        if (ddlAREA != null && ddlAREA.Items.Count > 0 && ddlAREA.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlAREA.SelectedValue, Sql.SqlFilterMode.Contains, "AREA_ID");//Khu vuc  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainTop.Visible = true;
                                vwMainTop = dtCurrent.DefaultView;
                                grdMainTop.DataSource = vwMainTop;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainTop.ApplySort();
                                    grdMainTop.CurrentPageIndex = 0;
                                    grdMainTop.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }

                    }
                    //Bao cao Top Chi nhanh
                    if (KPIs_Constant.KPI_REPORT_TYPE_R001_2.Equals(reportType))
                    {
                        sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL desc) AS NO, *             " + ControlChars.CrLf
                             + "  from vwBAOCAO_TOP_BOTTOM_CHI_NHANH   where 1=1                                        " + ControlChars.CrLf
                             + " AND PERCENT_FINAL_TOTAL > 50 AND (MAIN_POS_NAME <> '' AND MAIN_POS_NAME IS NOT NULL)   " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//Vung  
                        }
                        if (ddlAREA != null && ddlAREA.Items.Count > 0 && ddlAREA.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlAREA.SelectedValue, Sql.SqlFilterMode.Contains, "AREA_ID");//Khu vuc  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainTop.Visible = true;
                                vwMainTop = dtCurrent.DefaultView;
                                grdMainTop.DataSource = vwMainTop;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainTop.ApplySort();
                                    grdMainTop.CurrentPageIndex = 0;
                                    grdMainTop.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao bottom Chi nhanh
                    if (KPIs_Constant.KPI_REPORT_TYPE_R001_4.Equals(reportType))
                    {
                        sSQL = "select TOP 5 row_number() OVER (order by PERCENT_FINAL_TOTAL asc) AS NO, *              " + ControlChars.CrLf
                             + "  from vwBAOCAO_TOP_BOTTOM_CHI_NHANH   where 1=1                                        " + ControlChars.CrLf
                             + " AND PERCENT_FINAL_TOTAL < 50 AND (MAIN_POS_NAME <> '' AND MAIN_POS_NAME IS NOT NULL)   " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//Vung  
                        }
                        if (ddlAREA != null && ddlAREA.Items.Count > 0 && ddlAREA.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlAREA.SelectedValue, Sql.SqlFilterMode.Contains, "AREA_ID");//Khu vuc  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainTop.Visible = true;
                                vwMainTop = dtCurrent.DefaultView;
                                grdMainTop.DataSource = vwMainTop;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainTop.ApplySort();
                                    grdMainTop.CurrentPageIndex = 0;
                                    grdMainTop.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao thu thuan theo CV
                    if (KPIs_Constant.KPI_REPORT_TYPE_R002_1.Equals(reportType))
                    {
                        sSQL = " select row_number() over (order by FULL_NAME, MONTH_PERIOD) AS NO, *    " + ControlChars.CrLf
                              + " , (isnull(final_value_1, 0) + isnull(final_value_2, 0) + isnull(final_value_3, 0) + isnull(final_value_4, 0) + isnull(final_value_5, 0)) as FINAL_VALUE_6 " + ControlChars.CrLf
                              + "  from vwBAOCAO_THUTHUAN   where 1=1               " + ControlChars.CrLf
                              + " AND POSITION_ID in (select ID from M_POSITION)  " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//vung
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (txtEMPLOYEES_NAME != null && txtEMPLOYEES_NAME.Text != string.Empty)
                        {
                            Sql.AppendParameter(cmd, txtEMPLOYEES_NAME.Text, Sql.SqlFilterMode.Contains, "FULL_NAME");//ten nhan vien
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlPOSITION != null && ddlPOSITION.Items.Count > 0 && ddlPOSITION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlPOSITION.SelectedValue, Sql.SqlFilterMode.Contains, "POSITION_ID");//Chuc danh  
                        }
                        if (ddlORG_TYPE != null && ddlORG_TYPE.Items.Count > 0 && ddlORG_TYPE.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlORG_TYPE.SelectedValue, Sql.SqlFilterMode.Contains, "BUSINESS_CODE");//Nghiep vu  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            cmd.CommandText += " order by FULL_NAME, MONTH_PERIOD";
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();

                                //set datasource for DataGridview
                                this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dtCurrent);
                                grdMainCV.VirtualItemCount = dtCurrent.Rows.Count;

                                grdMainCV.Visible = true;
                                vwMainCV = dtCurrent.DefaultView;
                                grdMainCV.DataSource = vwMainCV;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainCV.ApplySort();
                                    grdMainCV.CurrentPageIndex = 0;
                                    grdMainCV.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao thu thuan theo DVKD
                    if (KPIs_Constant.KPI_REPORT_TYPE_R002_2.Equals(reportType))
                    {
                        sSQL = " select  row_number() OVER (order by POS_NAME, REGION_NAME) AS NO, YEAR, POS_CODE , POS_NAME , REGION_ID , REGION_NAME " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_1) AS FINAL_VALUE_1     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_2) AS FINAL_VALUE_2     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_3) AS FINAL_VALUE_3     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_4) AS FINAL_VALUE_4     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_5) AS FINAL_VALUE_5     " + ControlChars.CrLf
                              + " ,SUM(isnull(final_value_1, 0) + isnull(final_value_2, 0) + isnull(final_value_3, 0) + isnull(final_value_4, 0) + isnull(final_value_5, 0)) as FINAL_VALUE_6 " + ControlChars.CrLf
                            //+ " ,SUM(FINAL_VALUE_6) AS FINAL_VALUE_6     " + ControlChars.CrLf
                            //+ " ,SUM(FINAL_VALUE_7) AS FINAL_VALUE_7     " + ControlChars.CrLf
                              + " ,REMARK                                  " + ControlChars.CrLf
                              + "  from vwBAOCAO_THUTHUAN_ORG   where 1=1  " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//vung
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "BRANCH_NAME");//chi nhanh
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            cmd.CommandText += " GROUP BY YEAR, POS_CODE, POS_NAME, REGION_ID, REGION_NAME, REMARK  order by POS_NAME, REGION_NAME";
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainOrg.Visible = true;

                                vwMainOrg = dtCurrent.DefaultView;
                                grdMainOrg.DataSource = vwMainOrg;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainOrg.ApplySort();
                                    grdMainOrg.CurrentPageIndex = 0;
                                    grdMainOrg.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao thu thuan theo vung
                    if (KPIs_Constant.KPI_REPORT_TYPE_R002_3.Equals(reportType))
                    {
                        sSQL = " select row_number() OVER (order by REGION_NAME) AS NO, REGION_NAME ,YEAR " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_1) AS FINAL_VALUE_1     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_2) AS FINAL_VALUE_2     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_3) AS FINAL_VALUE_3     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_4) AS FINAL_VALUE_4     " + ControlChars.CrLf
                              + " ,SUM(FINAL_VALUE_5) AS FINAL_VALUE_5     " + ControlChars.CrLf
                            //+ " ,SUM(FINAL_VALUE_6) AS FINAL_VALUE_6     " + ControlChars.CrLf
                            //+ " ,SUM(FINAL_VALUE_7) AS FINAL_VALUE_7     " + ControlChars.CrLf
                              + " ,SUM(isnull(final_value_1, 0) + isnull(final_value_2, 0) + isnull(final_value_3, 0) + isnull(final_value_4, 0) + isnull(final_value_5, 0)) as FINAL_VALUE_6 " + ControlChars.CrLf
                              + " ,REMARK                                  " + ControlChars.CrLf
                              + "  from vwBAOCAO_THUTHUAN_REGION  where 1=1  " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//vung
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            cmd.CommandText += " group by REGION_NAME, YEAR, REMARK order by REGION_NAME";
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainRegion.Visible = true;
                                vwMainRegion = dtCurrent.DefaultView;
                                grdMainRegion.DataSource = vwMainRegion;
                                ViewState["CurrentTable"] = dtCurrent;
                                if (!IsPostBack)
                                {
                                    grdMainRegion.ApplySort();
                                    grdMainRegion.CurrentPageIndex = 0;
                                    grdMainRegion.DataBind();
                                }
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao top ca nhan
                    if (KPIs_Constant.KPI_REPORT_TYPE_R004_1.Equals(reportType))
                    {
                        sSQL = " select row_number() over (order by year DESC) as NO, * " + ControlChars.CrLf
                              + "  from vwBAOCAO_TOP_CANHAN_BY_REGION   where 1=1       " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedValue, Sql.SqlFilterMode.Contains, "REGION_ID");//vung
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (txtEMPLOYEES_NAME != null && txtEMPLOYEES_NAME.Text != string.Empty)
                        {
                            Sql.AppendParameter(cmd, txtEMPLOYEES_NAME.Text, Sql.SqlFilterMode.Contains, "FULL_NAME");//ten nhan vien
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlPOSITION != null && ddlPOSITION.Items.Count > 0 && ddlPOSITION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlPOSITION.SelectedValue, Sql.SqlFilterMode.Contains, "POSITION_ID");//Chuc danh  
                        }
                        if (ddlORG_TYPE != null && ddlORG_TYPE.Items.Count > 0 && ddlORG_TYPE.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlORG_TYPE.SelectedValue, Sql.SqlFilterMode.Contains, "BUSINESS_CODE");//Nghiep vu  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            //cmd.CommandText += "   order by region_name, percent_final_total desc";
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainCN.Visible = true;
                                vwMainCN = dtCurrent.DefaultView;
                                grdMainCN.DataSource = vwMainCN;
                                grdMainCN.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                //for (int i = 0; i < grdMainCN.Items.Count; i++)
                                //{
                                //    Label lblNO = (Label)grdMainCN.Items[i].FindControl("lblNO");
                                //    lblNO.Text = (i + 1).ToString();
                                //}
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }
                    //Bao cao bottom ca nhan
                    if (KPIs_Constant.KPI_REPORT_TYPE_R004_2.Equals(reportType))
                    {
                        sSQL = " select row_number() over (order by year ASC) as NO, * " + ControlChars.CrLf
                              + "  from vwBAOCAO_BOTTOM_CANHAN_BY_REGION   where 1=1     " + ControlChars.CrLf;
                        cmd.CommandText = sSQL;
                        if (ddlYEAR != null && ddlYEAR.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, Sql.ToInteger(ddlYEAR.SelectedValue), "YEAR", false);
                        }
                        if (ddlREGION != null && ddlREGION.Items.Count > 0 && ddlREGION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlREGION.SelectedItem.Text, Sql.SqlFilterMode.Contains, "REGION_NAME");//vung
                        }
                        if (ddlBRANCH != null && ddlBRANCH.Items.Count > 0 && ddlBRANCH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlBRANCH.SelectedItem.Text, Sql.SqlFilterMode.Contains, "MAIN_POS_NAME");//chi nhanh
                        }
                        if (txtEMPLOYEES_NAME != null && txtEMPLOYEES_NAME.Text != string.Empty)
                        {
                            Sql.AppendParameter(cmd, txtEMPLOYEES_NAME.Text, Sql.SqlFilterMode.Contains, "FULL_NAME");//ten nhan vien
                        }
                        if (ddlDEPARTMENT != null && ddlDEPARTMENT.Items.Count > 0 && ddlDEPARTMENT.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlDEPARTMENT.SelectedItem.Text, Sql.SqlFilterMode.Contains, "POS_NAME");//phong ban   
                        }
                        if (ddlMONTH != null && ddlMONTH.Items.Count > 0 && ddlMONTH.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlMONTH.SelectedValue, Sql.SqlFilterMode.Contains, "MONTH_PERIOD");//Thang  
                        }
                        if (ddlPOSITION != null && ddlPOSITION.Items.Count > 0 && ddlPOSITION.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlPOSITION.SelectedValue, Sql.SqlFilterMode.Contains, "POSITION_ID");//Chuc danh  
                        }
                        if (ddlORG_TYPE != null && ddlORG_TYPE.Items.Count > 0 && ddlORG_TYPE.SelectedValue != string.Empty)
                        {
                            Sql.AppendParameter(cmd, ddlORG_TYPE.SelectedValue, Sql.SqlFilterMode.Contains, "BUSINESS_CODE");//Nghiep vu  
                        }
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            //cmd.CommandText += "   order by region_name, percent_final_total desc";
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                resetDataGrid();
                                //set datasource for DataGridview
                                grdMainCN.Visible = true;
                                vwMainCN = dtCurrent.DefaultView;
                                grdMainCN.DataSource = vwMainCN;
                                grdMainCN.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                //for (int i = 0; i < grdMainCN.Items.Count; i++)
                                //{
                                //    Label lblNO = (Label)grdMainCN.Items[i].FindControl("lblNO");
                                //    lblNO.Text = (i + 1).ToString();
                                //}
                            }
                            else
                            {
                                resetDataGrid();
                                throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                            }
                        }
                    }

                }
            }
        }

        private void resetDataGrid()
        {
            //set datasource for DataGridview            
            grdMainTop.Visible = false;
            //grdMainTop.DataSource = null;
            //grdMainTop.DataBind();

            grdMainCV.Visible = false;
            //grdMainCV.DataSource = null;
            //grdMainCV.DataBind();

            grdMainOrg.Visible = false;
            //grdMainOrg.DataSource = null;
            //grdMainOrg.DataBind();

            grdMainRegion.Visible = false;
            //grdMainRegion.DataSource = null;
            //grdMainRegion.DataBind();

            grdMainCN.Visible = false;
            //grdMainCN.DataSource = null;
            //grdMainCN.DataBind();
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {

                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_REPORT_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                    Bind_Organization();

                    var month = DateTime.Now.ToString("MM");
                    ddlMONTH = this.FindControl("MONTH") as DropDownList;
                    if (ddlMONTH != null)
                        ddlMONTH.SelectedValue = month;
                    var year = DateTime.Now.ToString("yyyy");
                    ddlYEAR = this.FindControl("YEAR") as DropDownList;
                    if (ddlYEAR != null)
                        ddlYEAR.SelectedValue = year;
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }

                ddlReportType = this.FindControl("REPORT_TYPE") as DropDownList;
                if (ddlReportType != null)
                {
                    ddlReportType.AutoPostBack = true;
                    ddlReportType.SelectedIndexChanged += new EventHandler(ddlReportType_SelectedIndexChanged);
                }
                ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
                if (ddlBRANCH != null)
                {
                    ddlBRANCH.SelectedIndexChanged += new EventHandler(ddlBRANCH_SelectIndexChange);
                    ddlBRANCH.AutoPostBack = true;
                }
                ddlREGION = this.FindControl("REGION") as DropDownList;
                if (ddlREGION != null)
                {
                    ddlREGION.SelectedIndexChanged += new EventHandler(ddlREGION_SelectIndexChange);
                    ddlREGION.AutoPostBack = true;
                }
                //ORG_TYPE
                ddlORG_TYPE = this.FindControl("ORG_TYPE") as DropDownList;
                if (ddlORG_TYPE != null)
                {
                    ListItem selectedListItem = ddlORG_TYPE.Items.FindByValue("TAT");
                    if (selectedListItem != null)
                    {
                        ddlORG_TYPE.Items.Remove(ddlORG_TYPE.Items.FindByValue("TAT"));
                    }
                }
                
                Load_Default_Search();
                LoadDataGrid();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddlREGION_SelectIndexChange(object sender, EventArgs e)
        {
            resetDataGrid();
            ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
            if (ddlBRANCH != null)
            {
                ddlBRANCH.Items.Clear();
                
                ddlBRANCH.SelectedIndex = -1;
                ddlBRANCH.SelectedValue = null;
                ddlBRANCH.ClearSelection();

                ddlBRANCH.DataSource = SplendidCache.M_ORGANIZATION_MAINPOS(Sql.ToGuid(ddlREGION.SelectedValue));//chi nhanh
                ddlBRANCH.DataTextField = "ORGANIZATION_NAME";
                ddlBRANCH.DataValueField = "ID";
                ddlBRANCH.DataBind();

                ddlBRANCH.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlBRANCH.SelectedIndex = 0;
            }
        }

        private void ddlBRANCH_SelectIndexChange(object sender, EventArgs e)
        {
            resetDataGrid();
            ddlDEPARTMENT = this.FindControl("DEPARTMENT") as DropDownList;
            ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
            if (ddlDEPARTMENT != null && ddlBRANCH != null)
            {
                ddlDEPARTMENT.Items.Clear();
                ddlDEPARTMENT.DataSource = SplendidCache.M_ORGANIZATION_POS(ddlBRANCH.SelectedValue);//phong ban
                ddlDEPARTMENT.DataTextField = "ORGANIZATION_NAME";
                ddlDEPARTMENT.DataValueField = "ID";
                ddlDEPARTMENT.DataBind();
                ddlDEPARTMENT.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlDEPARTMENT.SelectedIndex = 0;
            }
        }

        private void Bind_Organization()
        {
            ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
            if (ddlBRANCH != null)
            {
                ddlBRANCH.Items.Clear();
                ddlBRANCH.DataSource = SplendidCache.M_ORGANIZATION_MAINPOS();//chi nhanh
                ddlBRANCH.DataTextField = "ORGANIZATION_NAME";
                ddlBRANCH.DataValueField = "ID";
                ddlBRANCH.DataBind();
                ddlBRANCH.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlBRANCH.SelectedIndex = 0;
            }
            ddlDEPARTMENT = this.FindControl("DEPARTMENT") as DropDownList;
            if (ddlDEPARTMENT != null)
            {
                ddlDEPARTMENT.Items.Clear();
                ddlDEPARTMENT.DataSource = SplendidCache.M_ORGANIZATION_POS();//phong ban
                ddlDEPARTMENT.DataTextField = "ORGANIZATION_NAME";
                ddlDEPARTMENT.DataValueField = "ID";
                ddlDEPARTMENT.DataBind();
                ddlDEPARTMENT.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlDEPARTMENT.SelectedIndex = 0;
            }
        }

        private void ddlReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            resetDataGrid();
            Load_Default_Search();
        }

        protected void Load_Default_Search()
        {
            ddlYEAR = this.FindControl("YEAR") as DropDownList;
            DropDownList ddlREGION = this.FindControl("REGION") as DropDownList;
            ddlBRANCH = this.FindControl("BRANCH") as DropDownList;
            DropDownList ddlAREA = this.FindControl("AREA") as DropDownList;
            ddlDEPARTMENT = this.FindControl("DEPARTMENT") as DropDownList;
            Label lblDEPARTMENT = this.FindControl("DEPARTMENT_LABEL") as Label;
            TextBox txtEMPLOYEES_NAME = this.FindControl("EMPLOYEES_NAME") as TextBox;
            DropDownList ddlPOSITION = this.FindControl("POSITION") as DropDownList;
            ddlMONTH = this.FindControl("MONTH") as DropDownList;
            Label lblEmployeeName = this.FindControl("EMPLOYEES_NAME_LABEL") as Label;
            ddlORG_TYPE = this.FindControl("ORG_TYPE") as DropDownList;
            Label lblORG_TYPE = this.FindControl("ORG_TYPE_LABEL") as Label;
            ddlPOSITION = this.FindControl("POSITION") as DropDownList;
            Label lblPOSITION = this.FindControl("POSITION_LABEL") as Label;


            string reportType = ddlReportType.SelectedValue;
            //Bao cao top phong
            if (KPIs_Constant.KPI_REPORT_TYPE_R001_1.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = true;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = true;
                }
                if (ddlDEPARTMENT != null)
                {
                    ddlDEPARTMENT.Visible = true;
                }
                if (lblDEPARTMENT != null)
                {
                    lblDEPARTMENT.Visible = true;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            //Bao cao top chi nhanh
            if (KPIs_Constant.KPI_REPORT_TYPE_R001_2.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = false;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = false;
                }
                if (ddlDEPARTMENT != null)
                {
                    ddlDEPARTMENT.Visible = false;
                }
                if (lblDEPARTMENT != null)
                {
                    lblDEPARTMENT.Visible = false;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            //Bao cao bottom phong
            if (KPIs_Constant.KPI_REPORT_TYPE_R001_3.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = true;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = true;
                }
                if (ddlDEPARTMENT != null)
                {
                    ddlDEPARTMENT.Visible = true;
                }
                if (lblDEPARTMENT != null)
                {
                    lblDEPARTMENT.Visible = true;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            //Bao cao bottom chi nhanh
            if (KPIs_Constant.KPI_REPORT_TYPE_R001_4.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = false;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = false;
                }
                if (ddlDEPARTMENT != null)
                {
                    ddlDEPARTMENT.Visible = false;
                }
                if (lblDEPARTMENT != null)
                {
                    lblDEPARTMENT.Visible = false;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            //Bao cao thu thuan theo Ca nhan
            if (KPIs_Constant.KPI_REPORT_TYPE_R002_1.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = true;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = true;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = true;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = true;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = true;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = true;
                }
            }
            //Bao cao thu thuan theo DVKD
            if (KPIs_Constant.KPI_REPORT_TYPE_R002_2.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = false;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = false;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            //Bao cao thu thuan theo vung
            if (KPIs_Constant.KPI_REPORT_TYPE_R002_3.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = false;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = false;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = false;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = false;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = false;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = false;
                }
            }
            if (KPIs_Constant.KPI_REPORT_TYPE_R003.Equals(reportType))
            {

            }
            //Bao cao Top Ca Nhan
            if (KPIs_Constant.KPI_REPORT_TYPE_R004_1.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = true;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = true;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = true;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = true;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = true;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = true;
                }
            }
            //Bao cao Bottom Ca nhan
            if (KPIs_Constant.KPI_REPORT_TYPE_R004_2.Equals(reportType))
            {
                if (txtEMPLOYEES_NAME != null)
                {
                    txtEMPLOYEES_NAME.Visible = true;
                }
                if (lblEmployeeName != null)
                {
                    lblEmployeeName.Visible = true;
                }
                if (ddlORG_TYPE != null)
                {
                    ddlORG_TYPE.Visible = true;
                }
                if (lblORG_TYPE != null)
                {
                    lblORG_TYPE.Visible = true;
                }
                if (ddlDEPARTMENT != null)
                {
                    ddlDEPARTMENT.Visible = true;
                }
                if (lblDEPARTMENT != null)
                {
                    lblDEPARTMENT.Visible = true;
                }
                if (ddlPOSITION != null)
                {
                    ddlPOSITION.Visible = true;
                }
                if (lblPOSITION != null)
                {
                    lblPOSITION.Visible = true;
                }
            }
            if (KPIs_Constant.KPI_REPORT_TYPE_R005.Equals(reportType))
            {

            }


            //Check role
            if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
            {
                //CRM_05
                if (ddlBRANCH != null)
                {
                    ddlBRANCH.SelectedValue = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;
                    ddlBRANCH.Enabled = false;
                }

                //CRM_06
                if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    if (ddlDEPARTMENT != null)
                    {
                        ddlDEPARTMENT.SelectedValue = Security.CURRENT_EMPLOYEE.POS_ID;
                        ddlDEPARTMENT.Enabled = false;
                    }
                }

                //CRM_07
                if ("CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    if (txtEMPLOYEES_NAME != null)
                    {
                        txtEMPLOYEES_NAME.Text = Security.CURRENT_EMPLOYEE.FULL_NAME;
                        txtEMPLOYEES_NAME.Enabled = false;
                    }

                    if (ddlPOSITION != null)
                    {
                        ddlPOSITION.SelectedValue = Security.CURRENT_EMPLOYEE.POSITION_ID;
                        ddlPOSITION.Enabled = false;
                    }

                    if (ddlAREA != null)
                    {
                        ddlAREA.SelectedValue = Security.CURRENT_EMPLOYEE.AREA_ID;
                        ddlAREA.Enabled = false;
                    }

                    if (ddlREGION != null)
                    {
                        ddlREGION.SelectedValue = Security.CURRENT_EMPLOYEE.REGION_ID;
                        ddlREGION.Enabled = false;
                    }

                    if (ddlORG_TYPE != null)
                    {
                        ddlORG_TYPE.SelectedValue = Security.CURRENT_EMPLOYEE.BUSINESS_CODE;
                        ddlORG_TYPE.Enabled = false;
                    }
                }
            }//End Check role
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);

            grdMainTop.ItemDataBound += new DataGridItemEventHandler(grdMainTop_ItemDataBound);
            grdMainCV.ItemDataBound += new DataGridItemEventHandler(grdMainCV_ItemDataBound);
            grdMainOrg.ItemDataBound += new DataGridItemEventHandler(grdMainOrg_ItemDataBound);
            grdMainRegion.ItemDataBound += new DataGridItemEventHandler(grdMainRegion_ItemDataBound);
            grdMainCN.ItemDataBound += new DataGridItemEventHandler(grdMainCN_ItemDataBound);

            m_sMODULE = "KPIR010102";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));

                grdMainTop.Command += new CommandEventHandler(Page_Command);
                grdMainCV.Command += new CommandEventHandler(Page_Command);
                grdMainOrg.Command += new CommandEventHandler(Page_Command);
                grdMainRegion.Command += new CommandEventHandler(Page_Command);
                grdMainCN.Command += new CommandEventHandler(Page_Command);
            }
        }
        #endregion
    }
}
