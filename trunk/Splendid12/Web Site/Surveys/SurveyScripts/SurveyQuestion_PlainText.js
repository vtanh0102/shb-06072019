﻿/* Copyright (C) 2013 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

function SurveyQuestion_PlainText(row)
{
	this.row = row;
	this.ID  = row.ID.replace(/-/g, '_');
}

SurveyQuestion_PlainText.prototype.Value = function()
{
	return null;
}

SurveyQuestion_PlainText.prototype.Validate = function(divQuestionError)
{
	return true;
}

SurveyQuestion_PlainText.prototype.Render = function(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS, bDisable)
{
	try
	{
		var div = document.createElement('div');
		div.className = 'SurveyAnswerPlainText';
		div.innerHTML = this.row.DESCRIPTION;
		divQuestionBody.appendChild(div);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_PlainText.Render: ' + e.message);
	}
}

SurveyQuestion_PlainText.prototype.Report = function(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS)
{
	try
	{
		this.Render(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS, true);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_PlainText.Report: ' + e.message);
	}
}

SurveyQuestion_PlainText.prototype.Summary = function(divQuestionHeading, divQuestionBody, callback, context)
{
	try
	{
		if ( this.SUMMARY_VISIBLE === undefined )
			this.SUMMARY_VISIBLE = true;
		else
			return;
		if ( callback !== undefined && callback != null )
			callback.call(context||this, 1, null);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_PlainText.Summary: ' + e.message);
	}
}

