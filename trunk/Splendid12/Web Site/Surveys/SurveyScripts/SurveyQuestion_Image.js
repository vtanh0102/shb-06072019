﻿/* Copyright (C) 2013 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

function SurveyQuestion_Image(row)
{
	this.row = row;
	this.ID  = row.ID.replace(/-/g, '_');
}

SurveyQuestion_Image.prototype.Value = function()
{
	return null;
}

SurveyQuestion_Image.prototype.Validate = function(divQuestionError)
{
	return true;
}

SurveyQuestion_Image.prototype.Render = function(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS, bDisable)
{
	try
	{
		SurveyQuestion_Helper_RenderHeader(divQuestionHeading, this.row);
		var img = document.createElement('img');
		img.className = 'SurveyAnswerImage';
		img.src = this.row.IMAGE_URL.replace('~/', sREMOTE_SERVER);
		divQuestionBody.appendChild(img);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_Image.Render: ' + e.message);
	}
}

SurveyQuestion_Image.prototype.Report = function(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS)
{
	try
	{
		this.Render(divQuestionHeading, divQuestionBody, rowQUESTION_RESULTS, true);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_Image.Report: ' + e.message);
	}
}

SurveyQuestion_Image.prototype.Summary = function(divQuestionHeading, divQuestionBody, callback, context)
{
	try
	{
		if ( this.SUMMARY_VISIBLE === undefined )
			this.SUMMARY_VISIBLE = true;
		else
			return;
		if ( callback !== undefined && callback != null )
			callback.call(context||this, 1, null);
	}
	catch(e)
	{
		throw new Error('SurveyQuestion_Image.Summary: ' + e.message);
	}
}

