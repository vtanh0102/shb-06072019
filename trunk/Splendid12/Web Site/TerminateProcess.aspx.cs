/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for TerminateProcess.
	/// </summary>
	public class TerminateProcess : SplendidPage
	{
		protected Literal         litTERMINATE;
		protected Button          btnSubmit   ;
		protected Label           lblError    ;
		protected Label           lblWarning  ;

		override protected bool AuthenticationRequired()
		{
			return false;
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( !IsPostBack )
			{
				SplendidError.SystemMessage("Log", new StackTrace(true).GetFrame(0), "User Terminated Process " + Request["identifier"]);
			}
			try
			{
				litTERMINATE.Text = L10n.Term("BusinessProcesses.LBL_USER_TERMINATE_MESSAGE");
				Guid gINSTANCE_ID = Sql.ToGuid(Request["identifier"]);
				if ( !Sql.IsEmptyGuid(gINSTANCE_ID) )
				{
					Workflow4Utils.Terminate(Context, gINSTANCE_ID, "User Terminated Process.");
				}
				// 11/10/2016 Paul.  Skip during precompile. 
				else if ( !Sql.ToBoolean(Request["PrecompileOnly"]) )
				{
					// 11/10/2016 Paul.  Don't use the standard error label as it will cause the precompile to stop. 
					lblWarning.Text = L10n.Term("BusinessProcesses.LBL_TERMINATE_INVALID_IDENTIFIER");
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
				btnSubmit.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

