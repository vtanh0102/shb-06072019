
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.Workflow.Activities.Rules;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0203
{
    /// <summary>
    ///		Summary description for ImportView.
    /// </summary>
    public class ImportView : SplendidControl
    {
        #region Properties
        // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected PlaceHolder phDefaultsView;
        protected SplendidControl ctlDefaultsView;

        protected Guid gID;

        protected SplendidGrid grdMain;

        protected XmlDocument xml;
        protected XmlDocument xmlMapping;
        protected string sImportModule;
        protected HtmlInputFile fileIMPORT;
        protected RequiredFieldValidator reqFILENAME;
        protected CheckBox chkHasHeader;
        protected HtmlTable tblImportMappings;
        protected StringBuilder sbImport;

        protected Label lblStatus;
        protected Label lblSuccessCount;
        protected Label lblDuplicateCount;
        protected Label lblFailedCount;
        protected CheckBox chkUseTransaction;

        protected HiddenField txtACTIVE_TAB;
        protected bool bDuplicateFields = false;
        protected int nMAX_ERRORS = 200;

        protected DataTable dtRuleColumns;

        public string Module
        {
            get { return sImportModule; }
            set { sImportModule = value; }
        }
        #endregion


        protected void GenerateImport(string sTempFileName)
        {
            XmlReader xmlFile;
            xmlFile = XmlReader.Create(Path.Combine(Path.GetTempPath(), sTempFileName), new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            int nImported = 0;
            int nFailed = 0;

            DataTable dtCurrent = ds.Tables[0];

            if (dtCurrent.Rows.Count > 0)
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();

                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    // 11/01/2006 Paul.  The transaction is optional, just make sure to always dispose it. 
                    using (IDbTransaction trn = Sql.BeginTransaction(con))
                    {
                        try
                        {
                            Hashtable hashSelectedFields = new Hashtable();
                            string allocateCode = string.Empty;
                            for (int i = 1; i < dtCurrent.Rows.Count; i++)
                            {
                                DataRow rdr = dtCurrent.Rows[i];
                                string year = Sql.ToString(rdr["ImportField001"]).Trim();
                                string monthPeriod = Sql.ToString(rdr["ImportField002"]).Trim();
                                string maNhanVien = Sql.ToString(rdr["ImportField003"]).Trim();

                                bool userCodeExisted = KPIs_Utils.spM_USERS_Existed(maNhanVien, trn);
                                if (!userCodeExisted)
                                {
                                    throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_USER_DO_NOT_EXISTED"), i, maNhanVien)));
                                }
                                string importKPICode = Sql.ToString(rdr["ImportField008"]).Trim();
                                bool kpiCodeExisted = KPIs_Utils.spM_KPIs_Existed(maNhanVien, importKPICode, trn);
                                if (!kpiCodeExisted)
                                {
                                    throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_KPI_CODE_DO_NOT_EXISTED"), i, importKPICode)));
                                }

                                bool bKpiAllocatesExisted = KPIs_Utils.spB_KPI_ALLOCATES_Existed(Sql.ToInteger(year), monthPeriod, maNhanVien, trn);
                                if (bKpiAllocatesExisted)
                                {
                                    throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_B_KPI_ALLOCATES_EXISTED"), maNhanVien, monthPeriod, year)));
                                }

                            }

                            for (int i = 1; i < dtCurrent.Rows.Count; i++)
                            {
                                DataRow rdr = dtCurrent.Rows[i];
                                string allocateName = Sql.ToString(rdr["ImportField000"]).Trim();
                                string year = Sql.ToString(rdr["ImportField001"]).Trim();
                                string monthPeriod = Sql.ToString(rdr["ImportField002"]).Trim();
                                string maNhanVien = Sql.ToString(rdr["ImportField003"]).Trim();
                                string code = string.Format("{0}{1}{2}", year, monthPeriod, maNhanVien);

                                if (!hashSelectedFields.ContainsKey(code))
                                {
                                    hashSelectedFields.Add(code, code);
                                    gID = Guid.NewGuid();
                                }
                                KPIB0203_SqlProcs.spB_KPI_ALLOCATES_Import
                                        (ref gID
                                        , Security.USER_ID
                                        , Security.TEAM_ID
                                        , string.Empty//TEAM_SET_LIST
                                        , "IMPORT_KPI_CA_NHAN"//NAME
                                        , ref allocateCode //ALLOCATE_CODE
                                        , allocateName //ALLOCATE_NAME
                                        , Sql.ToInteger(year)//YEAR
                                        , monthPeriod//PERIOD
                                        , "0"//"VERSION_NUMBER
                                        , KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND
                                        , Guid.Empty//APPROVED_BY
                                        , DateTime.Today//APPROVED_DATE
                                        , "0"//ALLOCATE_TYPE
                                        , Guid.Empty//ORGANIZATION_ID
                                        , Guid.Empty//EMPLOYEE_ID
                                        , Sql.ToString(KPIs_Utils.ACTIVE)//STATUS
                                        , Guid.Empty//KPI_STANDARD_ID
                                        , Guid.Empty//FILE_ID
                                        , Sql.ToString(rdr["ImportField005"])//ASSIGN_BY
                                        , Sql.ToDateTime(rdr["ImportField006"])//ASSIGN_DATE
                                        , 0//STAFT_NUMBER
                                        , Sql.ToString(rdr["ImportField007"])//DESCRIPTION
                                        , Sql.ToString(rdr["ImportField007"])//REMARK
                                        , string.Empty//TAG_SET_NAME
                                        , string.Empty //organization code
                                        , maNhanVien//MA_NHAN_VIEN
                                        , 0 // total plan percent
                                        , 0 //total plan value
                                        , Guid.NewGuid() // kpi group id
                                        , string.Empty //flex1
                                        , string.Empty //flex2
                                        , string.Empty //flex3
                                        , string.Empty //flex4
                                        , string.Empty //flex5
                                        , trn
                                        );

                                Guid gID_Detail = Guid.NewGuid();
                                string importKPICode = Sql.ToString(rdr["ImportField008"]).Trim();
                                KPIB0203_SqlProcs.spB_KPI_ALLOCATE_DETAILS_Import
                                                    (
                                                    ref gID_Detail
                                                    , Security.USER_ID
                                                    , Security.TEAM_ID
                                                    , Security.TEAM_NAME
                                                    , gID
                                                    , allocateCode.Trim()//ALLOCATE_CODE
                                                    , "0"
                                                    , Guid.Empty//EMPLOYEE_ID
                                                    , maNhanVien//MA_NHAN_VIEN
                                                    , importKPICode
                                                    , Sql.ToString(rdr["ImportField009"])//KPI_NAME
                                                    , Sql.ToInteger(year)//YEAR
                                                    , 0//KPI_UNIT
                                                    , 0//UNIT
                                                    , 0//KPI_RATIO
                                                    , 0//MAX_RATIO_COMPLETE
                                                    , Guid.Empty//KPI_GROUPS_ID_DETAILS
                                                    , Sql.ToString(rdr["ImportField022"])// description
                                                    , Sql.ToString(rdr["ImportField022"])// Remark
                                                    , 0
                                                    , Sql.ToDecimal(rdr["ImportField010"])//Month1
                                                     , Sql.ToDecimal(rdr["ImportField011"])//Month2
                                                     , Sql.ToDecimal(rdr["ImportField012"])//Month3
                                                     , Sql.ToDecimal(rdr["ImportField013"])//Month4
                                                     , Sql.ToDecimal(rdr["ImportField014"])//Month5
                                                     , Sql.ToDecimal(rdr["ImportField015"])//Month6
                                                     , Sql.ToDecimal(rdr["ImportField016"])//Month7
                                                     , Sql.ToDecimal(rdr["ImportField017"])//Month8
                                                     , Sql.ToDecimal(rdr["ImportField018"])//Month9
                                                     , Sql.ToDecimal(rdr["ImportField019"])//Month10
                                                     , Sql.ToDecimal(rdr["ImportField020"])//Month11
                                                     , Sql.ToDecimal(rdr["ImportField021"])//Month12
                                                     , string.Empty
                                                     , string.Empty
                                                     , string.Empty
                                                     , string.Empty
                                                     , string.Empty
                                                     , string.Empty
                                                     , trn
                                                );

                                if (!Sql.IsEmptyGuid(gID))
                                {
                                    nImported++;
                                }
                                else
                                {
                                    nFailed++;
                                }
                            }

                            trn.Commit();
                            SplendidCache.ClearFavorites();
                        }
                        catch (Exception ex)
                        {
                            trn.Rollback();
                            SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                            ctlDynamicButtons.ErrorText += ex.Message;
                            return;
                        }
                        finally
                        {
                            if (trn != null)
                                trn.Dispose();
                        }
                    }

                }
                lblStatus.Text = String.Empty;
                // 03/20/2011 Paul.  Include a preview indicator. 
                if (nFailed == 0)
                    lblStatus.Text += L10n.Term("Import.LBL_SUCCESS");
                else
                    lblStatus.Text += L10n.Term("Import.LBL_FAIL");
                lblSuccessCount.Text = nImported.ToString() + " " + L10n.Term("Import.LBL_SUCCESSFULLY");
                lblFailedCount.Text = nFailed.ToString() + " " + L10n.Term("Import.LBL_FAILED_IMPORT");
            }

        }

        protected string SourceType()
        {
            string sSourceType = "excel";
            return sSourceType;
        }


        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Import.Run")
                {
                    if (Page.IsValid)
                    {
                        // 10/10/2006 Paul.  The temp file name is stored in the session so that it is impossible for a hacker to access. 
                        string sTempFileID = Sql.ToString(ViewState["TempFileID"]);
                        string sTempFileName = Sql.ToString(Session["TempFile." + sTempFileID]);
                        if (Sql.IsEmptyString(sTempFileID) || Sql.IsEmptyString(sTempFileName))
                        {
                            throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                        }

                        SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Begin Import");
                        GenerateImport(sTempFileName);
                        SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "End Import");

                    }
                }
                else if (e.CommandName == "Import.Upload")
                {
                    reqFILENAME.Enabled = true;
                    reqFILENAME.Validate();
                    if (Page.IsValid)
                    {
                        HttpPostedFile pstIMPORT = fileIMPORT.PostedFile;
                        if (pstIMPORT != null)
                        {
                            if (pstIMPORT.FileName.Length > 0)
                            {
                                string sFILENAME = Path.GetFileName(pstIMPORT.FileName);
                                string sFILE_EXT = Path.GetExtension(sFILENAME);
                                string sFILE_MIME_TYPE = pstIMPORT.ContentType;

                                // 09/04/2010 Paul.  ACT Imports are taking a long time.  Time the stream conversion to see where the problem lies. 
                                SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Begin Upload: " + sFILENAME);
                                // 05/06/2011 Paul.  We need to be able to distinguish between Excel 2003 and Excel 2007. 
                                xml = SplendidImport.ConvertStreamToXml(sImportModule, SourceType(), string.Empty, pstIMPORT.InputStream, sFILE_EXT);

                                if (xml.DocumentElement == null)
                                    throw (new Exception(L10n.Term("Import.LBL_NOTHING")));

                                // 08/21/2006 Paul.  Don't move to next step if there is no data. 
                                XmlNodeList nlRows = xml.DocumentElement.SelectNodes(sImportModule.ToLower());
                                if (nlRows.Count == 0)
                                    throw (new Exception(L10n.Term("Import.LBL_NOTHING")));

                                // 10/10/2006 Paul.  Don't store the file name in the ViewState because a hacker could find a way to access and alter it.
                                // Storing the file name in the session and an ID in the view state should be sufficiently safe. 
                                string sTempFileID = Guid.NewGuid().ToString();
                                string sTempFileName = Security.USER_ID.ToString() + " " + Guid.NewGuid().ToString() + " " + sFILENAME + ".xml";
                                xml.Save(Path.Combine(Path.GetTempPath(), sTempFileName));
                                // 01/30/2010 Paul.  Were were not storing the full path in the Session for cleanup. 
                                Session["TempFile." + sTempFileID] = Path.Combine(Path.GetTempPath(), sTempFileName);
                                ViewState["TempFileID"] = sTempFileID;
                                SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "End Upload: " + sFILENAME);

                                lblSuccessCount.Text = sFILENAME.ToString() + " " + L10n.Term(".LBL_UPLOAD_SUCCESSFUL");
                            }
                        }
                    }
                    if (xml != null)
                    {
                        if (xml.DocumentElement == null)
                            throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                    }
                    else
                    {
                        throw (new Exception(L10n.Term("Import.LBL_NOTHING")));
                    }
                }
                else if (e.CommandName == "Cancel")
                {
                    string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
                    if (Sql.IsEmptyString(sRelativePath))
                        sRelativePath = "~/" + sImportModule + "/";
                    Response.Redirect(sRelativePath);
                }
                else if (e.CommandName == "Import.Download")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "utf-8";
                    Response.ClearContent();
                    Response.ContentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=Template_pg_kpis_canhan_2018_v1.0.xlsx");
                    Response.TransmitFile(Server.MapPath("~/Import/Template/Template_pg_kpis_canhan_2018_v1.0.xlsx"));
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                //SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText += ex.Message;
                return;
            }
        }


        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(sImportModule + ".LBL_MODULE_NAME"));

            this.Visible = Security.IS_ADMIN || (SplendidCRM.Security.GetUserAccess(sImportModule, "import") >= 0);
            if (!this.Visible)
            {
                // 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
                Parent.DataBind();
                return;
            }

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    // 09/06/2012 Paul.  Allow direct import into prospect list. 
                    // 10/22/2013 Paul.  Title was not getting set properly. 
                    ViewState["ctlDynamicButtons.Title"] = L10n.Term(sImportModule + ".LBL_MODULE_NAME");
                    string sMODULE_TABLE = Sql.ToString(Application["Modules." + sImportModule + ".TableName"]);
                    dtRuleColumns = SplendidCache.SqlColumns("vw" + sMODULE_TABLE + "_List");
                    ViewState["RULE_COLUMNS"] = dtRuleColumns;
                }
                else
                {
                    // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(ctlDynamicButtons.Title);

                }
            }
            catch (Exception ex)
            {
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // 09/17/2013 Paul.  Add Business Rules to import. 

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            this.m_sMODULE = "Import";
            // 07/21/2010 Paul.  Make sure to highlight the correct menu item. 
            SetMenu(sImportModule);

            string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
            if (Sql.IsEmptyString(sRelativePath))
            {
                // 10/14/2014 Paul.  Correct module name. 
                if (sImportModule == "Project" || sImportModule == "ProjectTask")
                    sImportModule += "s";
                sRelativePath = "~/" + sImportModule + "/";
            }

            // 04/29/2008 Paul.  Make use of dynamic buttons. 
            ctlDynamicButtons.AppendButtons(m_sMODULE + ".ImportView", Guid.Empty, Guid.Empty);
            // 04/27/2018 Paul.  We need to be able to generate an error message. 
            if (IsPostBack)
            {
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}

