using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;
using System.Collections;
using System.Collections.Generic;

namespace SplendidCRM.KPIB0203
{

    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;
        protected MassUpdate ctlMassUpdate;
        protected static string periodMonth;
        protected static Guid empID;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtUNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblValueMonth = (Label)e.Item.FindControl("lblValueMonth");
                if (lblValueMonth != null)
                {
                    //int valueMonth = (int)Math.Ceiling(Sql.ToFloat(lblValueMonth.Text));
                    double valueMonth = KPIs_Utils.RoundNumber(Sql.ToDouble(lblValueMonth.Text));
                    lblValueMonth.Text = KPIs_Utils.FormatFloat(Sql.ToString(valueMonth), format_number);
                }
                KPIs_Utils.FormatNumberForDataGridLabel(e.Item, format_number);
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("edit.aspx?ID=" + gID.ToString());
                }
                else if (e.CommandName == "Duplicate")
                {
                    Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    KPIs_Utils.KPI_ALLOCATE_DETAILS_Delete(gID);
                    KPIB0203_SqlProcs.spB_KPI_ALLOCATES_Delete(gID);
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "Cancel")
                {
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "MassSubmitApproval")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, ctlMassUpdate.APPROVED_BY, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassApprove
                else if (e.CommandName == "MassApprove")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, trn);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_InitResult(sIDs, trn);
                                            //Warning send by email: Gia tri nho hon toi thieu
                                            WarningValueForMonth();
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassReject
                else if (e.CommandName == "MassReject")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_REJECT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                else if (e.CommandName == "Export")
                {
                    Security.EMPLOYEE_FIELDS employeesInfo = Security.getUserInfos(Sql.ToString(empID));
                    //first month
                    if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                    {
                        string templatePath = Server.MapPath(@"~\Include\Template\R009_00_06.xlsx");
                        DataTable dtCurrent = (DataTable)ViewState["CurrentTable"];
                        if (dtCurrent != null)
                        {
                            KPIs_Export.ExportExcelKPIsCaNhan(dtCurrent, employeesInfo, periodMonth, templatePath);
                        }
                        else
                        {
                            throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                        }
                    }
                    //end month
                    if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                    {
                        string templatePath = Server.MapPath(@"~\Include\Template\R009_07_12.xlsx");
                        DataTable dtCurrent = (DataTable)ViewState["CurrentTable"];
                        if (dtCurrent != null)
                        {
                            KPIs_Export.ExportExcelKPIsCaNhan(dtCurrent, employeesInfo, periodMonth, templatePath);
                        }
                        else
                        {
                            throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                        }
                    }
                    //full month
                    if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                    {
                        string templatePath = Server.MapPath(@"~\Include\Template\R009_00_12.xlsx");
                        DataTable dtCurrent = (DataTable)ViewState["CurrentTable"];
                        if (dtCurrent != null)
                        {
                            KPIs_Export.ExportExcelKPIsCaNhan(dtCurrent, employeesInfo, periodMonth, templatePath);
                        }
                        else
                        {
                            throw (new Exception(L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND")));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_ALLOCATES_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID, "ID", false);
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["ALLOCATE_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            Page.Items["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            //ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            string approveStatus = rdr["APPROVE_STATUS"].ToString();
                                            if (!KPIs_Constant.KPI_APPROVE_STATUS_APPROVE.Equals(approveStatus) && !KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT.Equals(approveStatus))
                                            {
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            }
                                            ctlDynamicButtons.AppendProcessButtons(rdr);

                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            new DynamicControl(this, "PERIOD").Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "KPI_ALLOCATE_PERIOD_LIST", new DynamicControl(this, "PERIOD").Text);
                                            //new DynamicControl(this, "APPROVE_STATUS").Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "M_GROUP_KPIS_APPROVE_STATUS", new DynamicControl(this, "APPROVE_STATUS").Text);
                                            new DynamicControl(this, "ASSIGN_BY").Text = KPIs_Utils.getFullNameFromUser(new DynamicControl(this, "ASSIGN_BY").Text);
                                            new DynamicControl(this, "APPROVED_BY").Text = KPIs_Utils.getFullNameFromUser(new DynamicControl(this, "APPROVED_BY").Text);

                                            empID = Sql.ToGuid(rdr["EMPLOYEE_ID"]);
                                            KPIs_Utils.LoadEmployee_Info_Detail(this, dbf, empID, Sql.ToString(rdr["ALLOCATE_CODE"]));

                                            periodMonth = rdr["PERIOD"].ToString();
                                            KPIs_Utils.Load_DataGridByMonth(grdMain, periodMonth);

                                            LoadDelivery_IndicatorsForView(dbf, gID);
                                        }
                                        else
                                        {
                                            plcSubPanel.Visible = false;

                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.AppendProcessButtons(null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.AppendProcessButtons(null);
                        ctlDynamicButtons.DisableAll();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }
                                
                //01/10/2018 Tungnx: check approval button
                ctlMassUpdate.detailGID = gID.ToString();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        protected void LoadDelivery_IndicatorsForView(DbProviderFactory dbf, Guid ID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT Row_Number() OVER(order by alloc.DATE_ENTERED) AS NO, alloc.ID, '' AS KPI_STANDARD_ID             " + ControlChars.CrLf
                       + " , alloc.KPI_CODE, alloc.KPI_NAME, alloc.KPI_UNIT, alloc.RADIO AS RATIO, alloc.MAX_RATIO_COMPLETE          " + ControlChars.CrLf
                       + " , alloc.MONTH_1, alloc.MONTH_2, alloc.MONTH_3, alloc.MONTH_4, alloc.MONTH_5, alloc.MONTH_6, alloc.MONTH_7 " + ControlChars.CrLf
                       + " , alloc.MONTH_8, alloc.MONTH_9, alloc.MONTH_10, alloc.MONTH_11 , alloc.MONTH_12 , alloc.DESCRIPTION , std.VALUE_STD_PER_MONTH " + ControlChars.CrLf
                       + " , M.IS_ACCUMULATED_C, M.KPI_TYPE_C                                            " + ControlChars.CrLf
                       + "  from vwB_KPI_ALLOCATE_DETAILS_Edit alloc                                               " + ControlChars.CrLf
                       + "  inner join  vwB_KPI_STANDARD_DETAILS_Edit std  ON (alloc.KPI_GROUP_DETAIL_ID = std.ID) " + ControlChars.CrLf
                       + "  inner join vwM_KPIS_Edit M ON (M.KPI_CODE = alloc.KPI_CODE)                  " + ControlChars.CrLf;//join lay truong luy ke

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;
                    //Security.Filter(cmd, m_sMODULE, "edit");
                    Sql.AppendParameter(cmd, ID, "KPI_ALLOCATE_ID", false);
                    cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                            }
                        }
                    }
                }
            }

        }

        protected void WarningValueForMonth()
        {
            if (grdMain.Items.Count > 0)
            {
                string strKPIName = string.Empty;
                for (int i = 0; i < grdMain.Items.Count; i++)
                {
                    Label lblValueMonth = (Label)grdMain.Items[i].Cells[4].FindControl("lblValueMonth");
                    Label lblKPIsName = (Label)grdMain.Items[i].Cells[1].FindControl("txtTargetName");
                    if (lblValueMonth == null)
                    {
                        return;
                    }
                    if (lblKPIsName != null)
                    {
                        strKPIName = lblKPIsName.Text;
                    }
                    //first month
                    if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                    {
                        Label txtMonth1 = (Label)grdMain.Items[i].Cells[3].FindControl("txtMonth1");
                        Label txtMonth2 = (Label)grdMain.Items[i].Cells[4].FindControl("txtMonth2");
                        Label txtMonth3 = (Label)grdMain.Items[i].Cells[5].FindControl("txtMonth3");
                        Label txtMonth4 = (Label)grdMain.Items[i].Cells[6].FindControl("txtMonth4");
                        Label txtMonth5 = (Label)grdMain.Items[i].Cells[7].FindControl("txtMonth5");
                        Label txtMonth6 = (Label)grdMain.Items[i].Cells[8].FindControl("txtMonth6");
                        if (txtMonth1 != null && !Sql.IsEmptyString(txtMonth1.Text))
                        {
                            if (Sql.ToFloat(txtMonth1.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("01", strKPIName);
                            }
                        }
                        if (txtMonth2 != null && !Sql.IsEmptyString(txtMonth2.Text))
                        {
                            if (Sql.ToFloat(txtMonth2.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("02", strKPIName);
                            }
                        }
                        if (txtMonth3 != null && !Sql.IsEmptyString(txtMonth3.Text))
                        {
                            if (Sql.ToFloat(txtMonth3.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("03", strKPIName);
                            }
                        }
                        if (txtMonth4 != null && !Sql.IsEmptyString(txtMonth4.Text))
                        {
                            if (Sql.ToFloat(txtMonth4.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("04", strKPIName);
                            }
                        }
                        if (txtMonth5 != null && !Sql.IsEmptyString(txtMonth5.Text))
                        {
                            if (Sql.ToFloat(txtMonth5.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("05", strKPIName);
                            }
                        }
                        if (txtMonth6 != null && !Sql.IsEmptyString(txtMonth6.Text))
                        {
                            if (Sql.ToFloat(txtMonth6.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("06", strKPIName);
                            }
                        }
                    }
                    //end month
                    if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                    {
                        Label txtMonth7 = (Label)grdMain.Items[i].Cells[9].FindControl("txtMonth7");
                        Label txtMonth8 = (Label)grdMain.Items[i].Cells[10].FindControl("txtMonth8");
                        Label txtMonth9 = (Label)grdMain.Items[i].Cells[11].FindControl("txtMonth9");
                        Label txtMonth10 = (Label)grdMain.Items[i].Cells[12].FindControl("txtMonth10");
                        Label txtMonth11 = (Label)grdMain.Items[i].Cells[13].FindControl("txtMonth11");
                        Label txtMonth12 = (Label)grdMain.Items[i].Cells[14].FindControl("txtMonth12");
                        if (txtMonth7 != null && !Sql.IsEmptyString(txtMonth7.Text))
                        {
                            if (Sql.ToFloat(txtMonth7.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("07", strKPIName);
                            }
                        }
                        if (txtMonth8 != null && !Sql.IsEmptyString(txtMonth8.Text))
                        {
                            if (Sql.ToFloat(txtMonth8.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("08", strKPIName);
                            }
                        }
                        if (txtMonth9 != null && !Sql.IsEmptyString(txtMonth9.Text))
                        {
                            if (Sql.ToFloat(txtMonth9.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("09", strKPIName);
                            }
                        }
                        if (txtMonth10 != null && !Sql.IsEmptyString(txtMonth10.Text))
                        {
                            if (Sql.ToFloat(txtMonth10.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("10", strKPIName);
                            }
                        }
                        if (txtMonth11 != null && !Sql.IsEmptyString(txtMonth11.Text))
                        {
                            if (Sql.ToFloat(txtMonth11.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("11", strKPIName);
                            }
                        }
                        if (txtMonth12 != null && !Sql.IsEmptyString(txtMonth12.Text))
                        {
                            if (Sql.ToFloat(txtMonth12.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("12", strKPIName);
                            }
                        }
                    }
                    //full month
                    if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                    {
                        Label txtMonth1 = (Label)grdMain.Items[i].Cells[3].FindControl("txtMonth1");
                        Label txtMonth2 = (Label)grdMain.Items[i].Cells[4].FindControl("txtMonth2");
                        Label txtMonth3 = (Label)grdMain.Items[i].Cells[5].FindControl("txtMonth3");
                        Label txtMonth4 = (Label)grdMain.Items[i].Cells[6].FindControl("txtMonth4");
                        Label txtMonth5 = (Label)grdMain.Items[i].Cells[7].FindControl("txtMonth5");
                        Label txtMonth6 = (Label)grdMain.Items[i].Cells[8].FindControl("txtMonth6");
                        Label txtMonth7 = (Label)grdMain.Items[i].Cells[9].FindControl("txtMonth7");
                        Label txtMonth8 = (Label)grdMain.Items[i].Cells[10].FindControl("txtMonth8");
                        Label txtMonth9 = (Label)grdMain.Items[i].Cells[11].FindControl("txtMonth9");
                        Label txtMonth10 = (Label)grdMain.Items[i].Cells[12].FindControl("txtMonth10");
                        Label txtMonth11 = (Label)grdMain.Items[i].Cells[13].FindControl("txtMonth11");
                        Label txtMonth12 = (Label)grdMain.Items[i].Cells[14].FindControl("txtMonth12");

                        if (txtMonth1 != null && !Sql.IsEmptyString(txtMonth1.Text))
                        {
                            if (Sql.ToFloat(txtMonth1.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("01", strKPIName);
                            }
                        }
                        if (txtMonth2 != null && !Sql.IsEmptyString(txtMonth2.Text))
                        {
                            if (Sql.ToFloat(txtMonth2.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("02", strKPIName);
                            }
                        }
                        if (txtMonth3 != null && !Sql.IsEmptyString(txtMonth3.Text))
                        {
                            if (Sql.ToFloat(txtMonth3.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("03", strKPIName);
                            }
                        }
                        if (txtMonth4 != null && !Sql.IsEmptyString(txtMonth4.Text))
                        {
                            if (Sql.ToFloat(txtMonth4.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("04", strKPIName);
                            }
                        }
                        if (txtMonth5 != null && !Sql.IsEmptyString(txtMonth5.Text))
                        {
                            if (Sql.ToFloat(txtMonth5.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("05", strKPIName);
                            }
                        }
                        if (txtMonth6 != null && !Sql.IsEmptyString(txtMonth6.Text))
                        {
                            if (Sql.ToFloat(txtMonth6.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("06", strKPIName);
                            }
                        }
                        if (txtMonth7 != null && !Sql.IsEmptyString(txtMonth7.Text))
                        {
                            if (Sql.ToFloat(txtMonth7.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("07", strKPIName);
                            }
                        }
                        if (txtMonth8 != null && !Sql.IsEmptyString(txtMonth8.Text))
                        {
                            if (Sql.ToFloat(txtMonth8.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("08", strKPIName);
                            }
                        }
                        if (txtMonth9 != null && !Sql.IsEmptyString(txtMonth9.Text))
                        {
                            if (Sql.ToFloat(txtMonth9.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("09", strKPIName);
                            }
                        }
                        if (txtMonth10 != null && !Sql.IsEmptyString(txtMonth10.Text))
                        {
                            if (Sql.ToFloat(txtMonth10.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("10", strKPIName);
                            }
                        }
                        if (txtMonth11 != null && !Sql.IsEmptyString(txtMonth11.Text))
                        {
                            if (Sql.ToFloat(txtMonth11.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("11", strKPIName);
                            }
                        }
                        if (txtMonth12 != null && !Sql.IsEmptyString(txtMonth12.Text))
                        {
                            if (Sql.ToFloat(txtMonth12.Text) < Sql.ToFloat(lblValueMonth.Text))
                            {
                                KPIs_Utils.NotificationKPIsLessThanStandard("12", strKPIName);
                            }
                        }
                    }

                }
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);

            m_sMODULE = "KPIB0203";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                ctlDynamicButtons.AppendProcessButtons(null);
            }
        }
        #endregion
    }
}
