<%@ Page language="c#" MasterPageFile="~/ListView.Master" Codebehind="stream.aspx.cs" AutoEventWireup="false" Inherits="SplendidCRM.KPIB0203.StreamDefault" %>
<asp:Content ID="cntSidebar" ContentPlaceHolderID="cntSidebar" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="Shortcuts" Src="~/_controls/Shortcuts.ascx" %>
	<SplendidCRM:Shortcuts ID="ctlShortcuts" SubMenu="KPIB0203" Runat="Server" />
</asp:Content>

<asp:Content ID="cntBody" ContentPlaceHolderID="cntBody" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="StreamView" Src="~/ActivityStream/StreamView.ascx" %>
	<SplendidCRM:StreamView ID="ctlStreamView" Module="KPIB0203" Visible='<%# SplendidCRM.Security.GetUserAccess("KPIB0203", "list") >= 0 %>' Runat="Server" />
	<asp:Label ID="lblAccessError" ForeColor="Red" EnableViewState="false" Text='<%# L10n.Term("ACL.LBL_NO_ACCESS") %>' Visible="<%# !ctlStreamView.Visible %>" Runat="server" />
</asp:Content>

