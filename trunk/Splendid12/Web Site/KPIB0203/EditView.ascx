﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB0203.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    .listViewThS1 td {
        height: 20px;
        padding: 22px 54px 5px 0px;
        text-align: -webkit-center;
    }

    .listViewPaginationTdS1 {
        float: left;
        width: 100px;
    }

        .listViewPaginationTdS1 td span {
            display: none;
        }

    #ctl00_cntBody_ctlEditView_ASSIGN_NAME {
        width: 56%;
    }

    #ctl00_cntBody_ctlEditView_PERIOD {
        width: 79%;
    }

    .divScrollContainer {
        overflow-x: scroll;
        width: 160vh;
    }

    .divScrollContainerHide {
        overflow-x: scroll;
        position: relative;
        width: 190vh;
    }

    .dataField {
        width: 1% !important;
    }

    .dataLabel {
        width: 1% !important;
    }

    #ctl00_cntBody_ctlEditView_divEditView {
        /*width: 59%;*/
    }
</style>
<script type="text/javascript">
    $(function () {
        //numeric(".number");
        $(".listViewPaginationTdS1 td").html('<h4><%# L10n.Term("KPIB0203.LBL_ALLOCATED_TAGET_TITLE") %></h4>');
        $(".listViewPaginationTdS1 td").addClass("dataLabel");
        $(".listViewPaginationTdS1").removeClass("listViewPaginationTdS1");
        $("[id$=ctl00_imgHideHandle]").click(function () {
            $("[id$=divScrollContainer]").removeClass("divScrollContainer");
            $("[id$=divScrollContainer]").addClass("divScrollContainerHide");
        });
        $("[id$=ctl00_imgShowHandle]").click(function () {
            $("[id$=divScrollContainer]").removeClass("divScrollContainerHide");
            $("[id$=divScrollContainer]").addClass("divScrollContainer");
        });

        var isVisible = $('[id$=ctl00_tdShortcuts]').is(':visible');
        if (isVisible) {
            $("[id$=divScrollContainer]").removeClass("divScrollContainerHide");
            $("[id$=divScrollContainer]").addClass("divScrollContainer");
        } else {
            $("[id$=divScrollContainer]").removeClass("divScrollContainer");
            $("[id$=divScrollContainer]").addClass("divScrollContainerHide");
        }
    });
</script>
<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIB0203" EnablePrint="false" HelpName="EditView" EnableHelp="true" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div id="divEditSubPanel" class="tabForm">
        <div id="divScrollContainer">
            <asp:PlaceHolder ID="plcSubPanel" runat="server" />
            <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
                <Columns>
                    <asp:TemplateColumn HeaderText=".LBL_NO">
                        <HeaderStyle Width="20px" HorizontalAlign="Left" />
                        <ItemStyle Width="20px" HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:Label ID="txtNO" Text='<%# Bind("NO") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                        <HeaderStyle Width="80px" HorizontalAlign="Left" />
                        <ItemStyle Width="80px" HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:HiddenField ID="hdfALLOC_DETAILS_ID" Value='<%# Bind("ID") %>' runat="server" />
                            <asp:HiddenField ID="hdfKPI_STANDART_ID" Value='<%# Bind("KPI_STANDART_DETAILS_ID") %>' runat="server" />
                            <asp:HiddenField ID="hdfKPI_CODE" Value='<%# Bind("KPI_CODE") %>' runat="server" />
                            <asp:HiddenField ID="hdfIS_ACCUMULATED" Value='<%# Bind("IS_ACCUMULATED_C") %>' runat="server" />
                            <asp:HiddenField ID="hdfKPI_TYPE" Value='<%# Bind("KPI_TYPE_C") %>' runat="server" />
                            <asp:Label ID="txtTargetName" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                        <HeaderStyle Width="25px" HorizontalAlign="Left" />
                        <ItemStyle Width="25px" HorizontalAlign="Left" />
                        <ItemTemplate>
                            <asp:Label ID="txtUNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                            <asp:HiddenField ID="hdfUNIT" Value='<%# Bind("KPI_UNIT") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                        <HeaderStyle Width="20px" HorizontalAlign="Left" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                            <asp:HiddenField ID="hdfMAXRATIOCOMPL" Value='<%# Bind("MAX_RATIO_COMPLETE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_STANDARD_VALUE_MONTH">
                        <HeaderStyle Width="20px" HorizontalAlign="Left" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:Label ID="lblValueMonth" Text='<%# Bind("VALUE_STD_PER_MONTH") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_1">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth1" Text='<%# Bind("MONTH_1") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_2">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth2" Text='<%# Bind("MONTH_2") %>' runat="server" Width="50px" CssClass="right numbe format-qtyr" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_3">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth3" Text='<%# Bind("MONTH_3") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_4">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth4" Text='<%# Bind("MONTH_4") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_5">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth5" Text='<%# Bind("MONTH_5") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_6">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth6" Text='<%# Bind("MONTH_6") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_7">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth7" Text='<%# Bind("MONTH_7") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_8">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth8" Text='<%# Bind("MONTH_8") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_9">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth9" Text='<%# Bind("MONTH_9") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_10">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth10" Text='<%# Bind("MONTH_10") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_11">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth11" Text='<%# Bind("MONTH_11") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_12">
                        <HeaderStyle Width="20px" HorizontalAlign="right" />
                        <ItemStyle Width="20px" HorizontalAlign="right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonth12" Text='<%# Bind("MONTH_12") %>' runat="server" Width="50px" CssClass="right number format-qty" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_DESCRIPTION">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' TextMode="MultiLine" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>
        </div>
    </div>


    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
