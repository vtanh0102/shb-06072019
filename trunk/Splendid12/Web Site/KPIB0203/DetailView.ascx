﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIB0203.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">
    .listViewPaginationTdS1 {
        float: left;
    }

    .tabDetailViewDL h4 {
        display: inline-block;
        white-space: nowrap;
        float: left;
    }

    #ctl00_cntBody_ctlDetailView_btnExport{
        float: right;
        margin-top: -33px;
    }
    a.utilsLink{
            display:none;
        }
</style>
<script type="text/javascript">
    $(function () {
        $(".listViewPaginationTdS1 td").html('<h4><%# L10n.Term("KPIB0203.LBL_ALLOCATED_TAGET_TITLE") %></h4> <span style="background-color: red !important;font-weight: bold;">&nbsp; &nbsp;</span> <%# L10n.Term(".WARNING_KPI_LESS_THAN_STD") %>');
        $(".listViewPaginationTdS1 td").append(' &nbsp; <span style="background-color: #FFCC66 !important;font-weight: bold;">&nbsp; &nbsp;</span> <%# L10n.Term(".WARNING_KPIS_BAD") %>');
        $(".listViewPaginationTdS1 td").append(' &nbsp; <span style="background-color: #00CC33 !important;font-weight: bold;">&nbsp; &nbsp;</span> <%# L10n.Term(".WARNING_KPIS_ACCUMULATED") %>');
        $("[id$=btnDUPLICATE]").remove();
        $(".listViewPaginationTdS1 td").addClass("dataLabel");
        $(".listViewPaginationTdS1").removeClass("listViewPaginationTdS1");
    });
</script>
<div id="divDetailView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIB0203" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />
    <asp:Button ID="btnExport" CommandName="Export" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LNK_PRINT" ) %>' ToolTip='<%# L10n.Term(".LNK_PRINT" ) %>' runat="server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIB0203" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel" class="tabForm">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
        <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
            <Columns>
                <%--        <asp:TemplateColumn ItemStyle-CssClass="dragHandle" Visible="false">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray" />
                    <ItemTemplate>
                        <asp:Image SkinID="blank" Width="14px" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>--%>
                <asp:TemplateColumn HeaderText=".LBL_NO" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:Label ID="txtNO" Text='<%# Bind("NO") %>' runat="server" Width="20px" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_NAME">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdfALLOC_DETAILS_ID" Value='<%# Bind("ID") %>' runat="server" />
                        <asp:HiddenField ID="hdfKPI_STANDART_ID" Value='<%# Bind("KPI_STANDARD_ID") %>' runat="server" />
                        <asp:HiddenField ID="hdfKPI_CODE" Value='<%# Bind("KPI_CODE") %>' runat="server" />
                        <asp:HiddenField ID="hdfIS_ACCUMULATED" Value='<%# Bind("IS_ACCUMULATED_C") %>' runat="server" />
                        <asp:HiddenField ID="hdfKPI_TYPE" Value='<%# Bind("KPI_TYPE_C") %>' runat="server" />
                        <asp:Label ID="txtTargetName" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                    <ItemTemplate>
                        <asp:Label ID="txtUNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                        <asp:HiddenField ID="hdfUNIT" Value='<%# Bind("KPI_UNIT") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_STANDARD_VALUE_MONTH">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblValueMonth" Text='<%# Bind("VALUE_STD_PER_MONTH") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_1">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth1" Text='<%# Bind("MONTH_1") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_2">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth2" Text='<%# Bind("MONTH_2") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_3">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth3" Text='<%# Bind("MONTH_3") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_4">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth4" Text='<%# Bind("MONTH_4") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_5">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth5" Text='<%# Bind("MONTH_5") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_6">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth6" Text='<%# Bind("MONTH_6") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_7">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth7" Text='<%# Bind("MONTH_7") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_8">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth8" Text='<%# Bind("MONTH_8") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_9">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth9" Text='<%# Bind("MONTH_9") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_10">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth10" Text='<%# Bind("MONTH_10") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_11">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth11" Text='<%# Bind("MONTH_11") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_MONTH_12">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="txtMonth12" Text='<%# Bind("MONTH_12") %>' runat="server" CssClass="right" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB0203.LBL_LIST_DESCRIPTION">
                    <ItemTemplate>
                        <asp:Label ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </SplendidCRM:SplendidGrid>
    </div>
</div>

<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
