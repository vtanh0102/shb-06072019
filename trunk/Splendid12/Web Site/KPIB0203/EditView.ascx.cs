using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0203
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;
        protected _controls.SearchView ctlSearchView;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;
        protected DropDownList ddlPERIOD;
        protected string periodMonth = string.Empty;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtUNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblValueMonth = (Label)e.Item.FindControl("lblValueMonth");
                if (lblValueMonth != null)
                {
                    //lblValueMonth.Text = KPIs_Utils.FormatFloat(lblValueMonth.Text, format_number);
                    //double valueMonth = Math.Ceiling(Sql.ToDouble(lblValueMonth.Text));
                    decimal valueMonth = KPIs_Utils.RoundNumber(Sql.ToDecimal(lblValueMonth.Text));
                    lblValueMonth.Text = KPIs_Utils.FormatFloat(Sql.ToString(valueMonth), format_number);
                }
                KPIs_Utils.FormatNumberForDataGrid(e.Item, format_number);
            }
        }

        private static object _lockObject = new object();
        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwB_KPI_ALLOCATES_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }



                            if (grdMain.Items.Count <= 0)
                            {
                                throw (new Exception(L10n.Term("KPIB0203.ERR_KPI_EMP_NOT_EXIST")));
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;
                                    var organizationId = new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID;
                                    var organizationCode = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(organizationId), "M_ORGANIZATION", "ORGANIZATION_CODE");
                                    periodMonth = new SplendidCRM.DynamicControl(this, rowCurrent, "PERIOD").SelectedValue;
                                    var status = new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").Text;
                                    var year = new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue;
                                    var maNhanVien = new SplendidCRM.DynamicControl(this, rowCurrent, "MA_NHAN_VIEN").Text;

                                    if (Sql.IsEmptyGuid(gID))
                                    {
                                        if (!KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                                        {
                                            bool bKpiAllocExisted = KPIs_Utils.spB_KPI_ALLOCATES_Existed(Sql.ToInteger(year), KPIs_Utils.FULL_MONTH, maNhanVien, trn);
                                            if (bKpiAllocExisted)
                                            {
                                                throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_B_KPI_ALLOCATES_EXISTED"), maNhanVien, KPIs_Utils.FULL_MONTH, year)));
                                            }
                                        }
                                        else
                                        {
                                            bool bKpiAllocExisted = KPIs_Utils.spB_KPI_ALLOCATES_Existed(Sql.ToInteger(year), KPIs_Utils.START_MONTH, KPIs_Utils.END_MONTH, maNhanVien, trn);
                                            if (bKpiAllocExisted)
                                            {
                                                string strPeriod = string.Format("{0} {1} {2}", KPIs_Utils.START_MONTH, L10n.Term(".LBL_OR"), KPIs_Utils.END_MONTH);
                                                throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_B_KPI_ALLOCATES_EXISTED"), maNhanVien, strPeriod, year)));
                                            }
                                        }

                                        bool bKpiAllocatesExisted = KPIs_Utils.spB_KPI_ALLOCATES_CN_Existed(Sql.ToInteger(year), periodMonth, maNhanVien, trn);
                                        if (bKpiAllocatesExisted)
                                        {
                                            throw (new Exception(string.Format(L10n.Term("KPIB0203.ERR_B_KPI_ALLOCATES_EXISTED"), maNhanVien, periodMonth, year)));
                                        }
                                    }

                                    lock (_lockObject)
                                    {
                                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_frefix"]);
                                        string sub_code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_sub_frefix"]);
                                        string strAllocatedCode = KPIs_Utils.GenerateAllocatedCode(code_prefix, sub_code_prefix, "vwB_KPI_ALLOCATES_Edit", "ALLOCATE_CODE");
                                        bool bDUPLICATE_CHECKING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && (e.CommandName != "SaveDuplicate") && Sql.IsEmptyGuid(gID);
                                        if (bDUPLICATE_CHECKING_ENABLED)
                                        {
                                            //validate duplicate code                                   
                                            if (Utils.DuplicateCheckCode(Application, con, m_sMODULE, strAllocatedCode, "ALLOCATE_CODE", this, rowCurrent) > 0)
                                            {
                                                throw (new Exception(string.Format(L10n.Term(".ERR_DUPLICATE_CODE_EXCEPTION"), strAllocatedCode)));
                                            }
                                        }

                                        string allocateCode = new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_CODE").Text;

                                        KPIB0203_SqlProcs.spB_KPI_ALLOCATES_Update
                                            (ref gID
                                            , gASSIGNED_USER_ID
                                            , gTEAM_ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_NAME").Text
                                            , string.IsNullOrEmpty(allocateCode) ? strAllocatedCode : allocateCode
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_NAME").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                            , periodMonth//new SplendidCRM.DynamicControl(this, rowCurrent, "PERIOD").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                            , KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND//new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_STATUS").Text == string.Empty ? "00" : new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_STATUS").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_TYPE").Text
                                            , Sql.ToGuid(organizationId)//new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "EMPLOYEE_ID").ID
                                            , status == string.Empty ? Sql.ToString(KPIs_Utils.ACTIVE) : status
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_STANDARD_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "FILE_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_BY").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_DATE").DateValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "STAFT_NUMBER").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                            , organizationCode //organization code
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "MA_NHAN_VIEN").Text // ma nhan vien
                                            , 0 // total plan percent
                                            , 0 //total plan value
                                            , Guid.NewGuid() // kpi group id
                                            , new SplendidCRM.DynamicControl(this, null, "POSITION_ID").ID
                                            , new SplendidCRM.DynamicControl(this, null, "AREA_ID").ID
                                            , string.Empty //flex1
                                            , string.Empty //flex2
                                            , string.Empty //flex3
                                            , string.Empty //flex4
                                            , string.Empty //flex5
                                            , trn
                                            );

                                        if (ViewState["CurrentTable"] != null)
                                        {
                                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                            if (dtCurrentTable.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                                {
                                                    Guid gIDT;
                                                    HiddenField hdfKPI_STANDART_ID = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfKPI_STANDART_ID");
                                                    HiddenField hdfKPI_CODE = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfKPI_CODE");
                                                    Label targerName = (Label)grdMain.Items[i].Cells[0].FindControl("txtTargetName");
                                                    HiddenField hdfALLOC_DETAILS_ID = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfALLOC_DETAILS_ID");
                                                    HiddenField hdfUNIT = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfUNIT");
                                                    Label lblKPI_RATIO = (Label)grdMain.Items[i].Cells[2].FindControl("lblKPI_RATIO");
                                                    Label lblValueMonth = (Label)grdMain.Items[i].Cells[4].FindControl("lblValueMonth");

                                                    TextBox txtMonth1 = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtMonth1");
                                                    TextBox txtMonth2 = (TextBox)grdMain.Items[i].Cells[4].FindControl("txtMonth2");
                                                    TextBox txtMonth3 = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtMonth3");
                                                    TextBox txtMonth4 = (TextBox)grdMain.Items[i].Cells[6].FindControl("txtMonth4");
                                                    TextBox txtMonth5 = (TextBox)grdMain.Items[i].Cells[7].FindControl("txtMonth5");
                                                    TextBox txtMonth6 = (TextBox)grdMain.Items[i].Cells[8].FindControl("txtMonth6");
                                                    TextBox txtMonth7 = (TextBox)grdMain.Items[i].Cells[9].FindControl("txtMonth7");
                                                    TextBox txtMonth8 = (TextBox)grdMain.Items[i].Cells[10].FindControl("txtMonth8");
                                                    TextBox txtMonth9 = (TextBox)grdMain.Items[i].Cells[11].FindControl("txtMonth9");
                                                    TextBox txtMonth10 = (TextBox)grdMain.Items[i].Cells[12].FindControl("txtMonth10");
                                                    TextBox txtMonth11 = (TextBox)grdMain.Items[i].Cells[13].FindControl("txtMonth11");
                                                    TextBox txtMonth12 = (TextBox)grdMain.Items[i].Cells[14].FindControl("txtMonth12");

                                                    TextBox txtDescription = (TextBox)grdMain.Items[i].Cells[15].FindControl("txtDescription");
                                                    if (hdfALLOC_DETAILS_ID.Value == string.Empty)
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        gIDT = Guid.Parse(hdfALLOC_DETAILS_ID.Value);
                                                    }
                                                    //get all month
                                                    decimal month1 = Sql.ToDecimal(txtMonth1.Text);
                                                    decimal month2 = Sql.ToDecimal(txtMonth2.Text);
                                                    decimal month3 = Sql.ToDecimal(txtMonth3.Text);
                                                    decimal month4 = Sql.ToDecimal(txtMonth4.Text);
                                                    decimal month5 = Sql.ToDecimal(txtMonth5.Text);
                                                    decimal month6 = Sql.ToDecimal(txtMonth6.Text);
                                                    decimal month7 = Sql.ToDecimal(txtMonth7.Text);
                                                    decimal month8 = Sql.ToDecimal(txtMonth8.Text);
                                                    decimal month9 = Sql.ToDecimal(txtMonth9.Text);
                                                    decimal month10 = Sql.ToDecimal(txtMonth10.Text);
                                                    decimal month11 = Sql.ToDecimal(txtMonth11.Text);
                                                    decimal month12 = Sql.ToDecimal(txtMonth12.Text);

                                                    float valueMonth = Sql.ToFloat(lblValueMonth.Text);

                                                    //fisrt month
                                                    /* Comment allow allocated = 0
                                                    if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month1 <= 0 || month2 <= 0 || month3 <= 0 || month4 <= 0 || month5 <= 0 || month6 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    
                                                        if (month1 < valueMonth || month2 < valueMonth || month3 < valueMonth || month4 < valueMonth || month5 < valueMonth || month6 < valueMonth)
                                                        {
                                                            throw (new Exception(L10n.Term("KPIB0203.ERR_KPI_FOR_MONTH_LESS_THAN_STD")));
                                                        }
                                                     
                                                    }
                                                    if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month7 <= 0 || month8 <= 0 || month9 <= 0 || month10 <= 0 || month11 <= 0 || month12 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    
                                                        if (month7 < valueMonth || month8 < valueMonth || month9 < valueMonth || month10 < valueMonth || month11 < valueMonth || month12 < valueMonth)
                                                        {
                                                            throw (new Exception(L10n.Term("KPIB0203.ERR_KPI_FOR_MONTH_LESS_THAN_STD")));
                                                        }
                                                     
                                                    }
                                                    if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                                                    {
                                                        if (month1 <= 0 || month2 <= 0 || month3 <= 0 || month4 <= 0 || month5 <= 0 || month6 <= 0
                                                            || month7 <= 0 || month8 <= 0 || month9 <= 0 || month10 <= 0 || month11 <= 0 || month12 <= 0)
                                                        {
                                                            throw (new Exception(L10n.Term(".ERR_KPI_FOR_MONTH_REQUIRE")));
                                                        }
                                                    
                                                        if (month1 < valueMonth || month2 < valueMonth || month3 < valueMonth || month4 < valueMonth || month5 < valueMonth || month6 < valueMonth
                                                            || month7 < valueMonth || month8 < valueMonth || month9 < valueMonth || month10 < valueMonth || month11 < valueMonth || month12 < valueMonth)
                                                        {
                                                            throw (new Exception(L10n.Term("KPIB0203.ERR_KPI_FOR_MONTH_LESS_THAN_STD")));
                                                        }
                                                     
                                                    }
                                                     */

                                                    //save details
                                                    KPIB0203_SqlProcs.spB_KPI_ALLOCATE_DETAILS_Update
                                                        (ref gIDT
                                                         , gASSIGNED_USER_ID
                                                         , gTEAM_ID
                                                         , string.Empty
                                                         , gID
                                                         , strAllocatedCode
                                                         , "0"
                                                         , new SplendidCRM.DynamicControl(this, rowCurrent, "EMPLOYEE_ID").ID
                                                         , new SplendidCRM.DynamicControl(this, rowCurrent, "MA_NHAN_VIEN").Text
                                                         , hdfKPI_CODE.Value
                                                         , targerName.Text
                                                         , 0
                                                         , hdfUNIT.Value == string.Empty ? 0 : int.Parse(hdfUNIT.Value)
                                                         , hdfUNIT.Value == string.Empty ? 0 : int.Parse(hdfUNIT.Value) //unit
                                                         , lblKPI_RATIO.Text == string.Empty ? 0 : Sql.ToFloat(lblKPI_RATIO.Text)
                                                         , 0 //max ratio complete
                                                         , Sql.ToGuid(hdfKPI_STANDART_ID.Value)
                                                         , txtDescription.Text
                                                         , string.Empty
                                                         , 0
                                                         , txtMonth1.Text == string.Empty ? 0 : month1
                                                         , txtMonth2.Text == string.Empty ? 0 : month2
                                                         , txtMonth3.Text == string.Empty ? 0 : month3
                                                         , txtMonth4.Text == string.Empty ? 0 : month4
                                                         , txtMonth5.Text == string.Empty ? 0 : month5
                                                         , txtMonth6.Text == string.Empty ? 0 : month6
                                                         , txtMonth7.Text == string.Empty ? 0 : month7
                                                         , txtMonth8.Text == string.Empty ? 0 : month8
                                                         , txtMonth9.Text == string.Empty ? 0 : month9
                                                         , txtMonth10.Text == string.Empty ? 0 : month10
                                                         , txtMonth11.Text == string.Empty ? 0 : month11
                                                         , txtMonth12.Text == string.Empty ? 0 : month12
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , string.Empty
                                                         , trn
                                                         );
                                                }
                                                ViewState["CurrentTable"] = dtCurrentTable;
                                            }
                                        }

                                        SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                        SplendidCRM.SqlProcs.spTRACKER_Update
                                            (Security.USER_ID
                                            , m_sMODULE
                                            , gID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                            , "save"
                                            , trn
                                            );
                                        if (plcSubPanel.Visible)
                                        {
                                            foreach (Control ctl in plcSubPanel.Controls)
                                            {
                                                InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                                if (ctlSubPanel != null)
                                                {
                                                    ctlSubPanel.Save(gID, m_sMODULE, trn);
                                                }
                                            }
                                        }
                                        trn.Commit();
                                        SplendidCache.ClearFavorites();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        bool loadAllocated = false;
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_ALLOCATES_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    cmd.CommandText += string.Format(" AND ISNULL(APPROVE_STATUS,'') <> '{0}' AND ISNULL(APPROVE_STATUS,'') <> '{1}'", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;

                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        //LOAD EDIT KPI ALLOCATED
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            loadAllocated = true;
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["ALLOCATE_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("ALLOCATE_NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["ALLOCATE_NAME"] = Sql.ToString(rdr["ALLOCATE_NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["ALLOCATE_NAME"] = ViewState["ALLOCATE_NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                            string allocatedCode = Sql.ToString(rdr["ALLOCATE_CODE"]);
                                            periodMonth = new SplendidCRM.DynamicControl(this, "PERIOD").SelectedValue;

                                            if (allocatedCode != string.Empty)
                                            {
                                                this.LoadDelivery_IndicatorsForEdit(dbf, gID);
                                            }
                                            else
                                            {
                                                string kpiStandardID = Sql.ToString(rdr["KPI_STANDARD_ID"]);
                                                this.LoadDelivery_Indicators(dbf, kpiStandardID);
                                            }
                                            Guid empID = Sql.ToGuid(rdr["EMPLOYEE_ID"]);
                                            KPIs_Utils.LoadEmployee_Info(this, dbf, empID, Sql.ToString(rdr["ALLOCATE_CODE"]));
                                            //Canh bao neu nhan vien chua co tham nie
                                            Label lblSENIORITY = this.FindControl("SENIORITY") as Label;
                                            if (lblSENIORITY != null)
                                            {
                                                if (lblSENIORITY.Text == string.Empty)
                                                {
                                                    throw (new Exception(L10n.Term("KPIB0203.WARNING_EMPLOYEE_MISS_SENIORITY")));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            //ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            //ctlDynamicButtons.DisableAll();
                                            //ctlFooterButtons.DisableAll();
                                            //ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            //plcSubPanel.Visible = false;
                                            loadAllocated = false;                                           
                                        }
                                    }
                                    //LOAD KPI ALLOCATED FROM USER ID
                                    if (!loadAllocated)
                                    {
                                        sSQL = "SELECT *                            " + ControlChars.CrLf
                                             + "   from vwUSERS_Edit where 1 = 1    " + ControlChars.CrLf;

                                        cmd.CommandText = sSQL;
                                        //Security.Filter(cmd, m_sMODULE, "edit");
                                        Sql.AppendParameter(cmd, gID, "ID", false);
                                        if (bDebug)
                                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));
                                        ((IDbDataAdapter)da).SelectCommand = cmd;

                                        using (DataTable dtCurrent = new DataTable())
                                        {
                                            da.Fill(dtCurrent);
                                            if (dtCurrent.Rows.Count > 0)
                                            {
                                                DataRow rdr = dtCurrent.Rows[0];
                                                this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                                ctlDynamicButtons.Title = Sql.ToString(rdr["FULL_NAME"]);
                                                SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                                Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                                ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                                bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                                Guid gASSIGNED_USER_ID = Guid.Empty;
                                                if (bModuleIsAssigned)
                                                    gASSIGNED_USER_ID = Sql.ToGuid(rdr["ID"]);
                                                //var employeeArea = KPIs_Utils.checkAreaOfUser(gID);
                                                //if (employeeArea == null || employeeArea == string.Empty)
                                                //{
                                                //    throw (new Exception(string.Format("{0} {1} {2}", L10n.Term(".LBL_EMPLOYEES"), L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND").ToLower(), L10n.Term(".LBL_AREA_C"))));
                                                //}

                                                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                                //lOAD CONTROL
                                                Label lblCODE = this.FindControl("ALLOCATE_CODE") as Label;
                                                lblCODE.Text = string.Empty;
                                                //string code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_frefix"]);
                                                //string sub_code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_sub_frefix"]);
                                                //string strAllocatedCode = KPIs_Utils.GenerateAllocatedCode(code_prefix, sub_code_prefix, "vwB_KPI_ALLOCATES_Edit", "ALLOCATE_CODE");
                                                //if (lblCODE != null)
                                                //{
                                                //    lblCODE.Text = strAllocatedCode;
                                                //}
                                                TextBox txtNAME = this.FindControl("ALLOCATE_NAME") as TextBox;
                                                if (txtNAME != null)
                                                {
                                                    txtNAME.Focus();
                                                    txtNAME.Text = string.Empty;
                                                }
                                                Label lblYear = this.FindControl("YEAR") as Label;
                                                if (lblYear != null)
                                                {
                                                    lblYear.Text = DateTime.Now.Year.ToString();
                                                }
                                             
                                                ListControl lstMonth = this.FindControl("PERIOD") as ListControl;

                                                if (lstMonth != null)
                                                    lstMonth.SelectedValue = Session["ALLOCATE_PERIOD"].ToString();    

                                                TextBox txtASSIGN_BY = this.FindControl("ASSIGN_NAME") as TextBox;
                                                if (txtASSIGN_BY != null)
                                                {
                                                    txtASSIGN_BY.Text = string.Empty;
                                                }

                                                ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                                ViewState["FULL_NAME"] = Sql.ToString(rdr["FULL_NAME"]);
                                                ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                                Page.Items["ALLOCATE_NAME"] = ViewState["ALLOCATE_NAME"];
                                                Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                                this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                                var positionId = KPIs_Utils.getValueFromGuidID(gID, "vwEMPLOYEES_Edit", "POSITION_ID_C", "ID_C");//chuc danh

                                                //var organizationId = KPIs_Utils.getValueFromGuidID(gID, "USERS_CSTM", "ORGANIZATION_ID_C", "ID_C");
                                                periodMonth = new SplendidCRM.DynamicControl(this, "PERIOD").SelectedValue;

                                                this.LoadDelivery_IndicatorsForUsser(dbf, lblYear.Text, Sql.ToGuid(positionId), Sql.ToGuid(gID));

                                                KPIs_Utils.LoadEmployee_Info(this, dbf, gID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("ALLOCATE_NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();
                        Label lblCODE = this.FindControl("ALLOCATE_CODE") as Label;
                        lblCODE.Text = string.Empty;
                       
                        //string code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_frefix"]);
                        //string sub_code_prefix = Sql.ToString(Application["CONFIG.kpi_allocates_sub_frefix"]);
                        //string strAllocatedCode = KPIs_Utils.GenerateAllocatedCode(code_prefix, sub_code_prefix, "vwB_KPI_ALLOCATES_Edit", "ALLOCATE_CODE");
                        //if (lblCODE != null && lblCODE.Text == string.Empty)
                        //{
                        //    lblCODE.Text = strAllocatedCode;
                        //}
                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                    ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
                    if (ddlPERIOD != null)
                    {
                        ddlPERIOD.AutoPostBack = true;
                        ddlPERIOD.SelectedIndexChanged += new EventHandler(ddlPERIOD_SelectedIndexChanged);
                        string strMonth = ddlPERIOD.SelectedValue.ToString();
                        KPIs_Utils.Load_DataGridByMonth(grdMain, strMonth);
                    }

                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["ALLOCATE_NAME"] = ViewState["ALLOCATE_NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                    ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
                    if (ddlPERIOD != null)
                    {
                        ddlPERIOD.AutoPostBack = true;
                        ddlPERIOD.SelectedIndexChanged += new EventHandler(ddlPERIOD_SelectedIndexChanged);
                    }
                }
                TextBox txtASSIGN_NAME = this.FindControl("ASSIGN_NAME") as TextBox;
                if (txtASSIGN_NAME != null)
                {
                    txtASSIGN_NAME.ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddlPERIOD_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPERIOD = this.FindControl("PERIOD") as DropDownList;
            if (ddlPERIOD != null)
            {
                if (ddlPERIOD.Items.Count > 0)
                {
                    string strMonth = ddlPERIOD.SelectedValue.ToString();
                    KPIs_Utils.Load_DataGridByMonth(grdMain, strMonth);
                    periodMonth = ddlPERIOD.SelectedValue.ToString();
                    LoadDefaultValueForMonth();
                }
            }
        }


        protected void LoadDelivery_IndicatorsForEdit(DbProviderFactory dbf, Guid ID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT Row_Number() OVER(order by A.DATE_ENTERED) AS NO, A.ID, S.ID AS KPI_STANDART_DETAILS_ID,'' AS KPI_STANDARD_ID " + ControlChars.CrLf
                       + " , A.KPI_NAME, A.KPI_UNIT, A.RADIO AS RATIO, A.MAX_RATIO_COMPLETE, A.KPI_CODE                             " + ControlChars.CrLf
                       + " , A.MONTH_1, A.MONTH_2, A.MONTH_3, A.MONTH_4, A.MONTH_5, A.MONTH_6, A.MONTH_7                            " + ControlChars.CrLf
                       + " , A.MONTH_8, A.MONTH_9, A.MONTH_10, A.MONTH_11 , A.MONTH_12 , A.DESCRIPTION , S.VALUE_STD_PER_MONTH      " + ControlChars.CrLf
                       + " , A.KPI_GROUP_DETAIL_ID, A.TEAM_ID       " + ControlChars.CrLf
                       + " , M.IS_ACCUMULATED_C, M.KPI_TYPE_C       " + ControlChars.CrLf
                       + "  from vwB_KPI_ALLOCATE_DETAILS_Edit A    " + ControlChars.CrLf
                       + "   inner join  vwB_KPI_STANDARD_DETAILS_Edit   S          " + ControlChars.CrLf
                       + " ON (A.KPI_GROUP_DETAIL_ID = S.ID)                        " + ControlChars.CrLf
                       + "  inner join vwM_KPIS_Edit M ON (M.KPI_CODE = A.KPI_CODE) " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                    //Security.Filter(cmd, m_sMODULE, "edit");

                    Sql.AppendParameter(cmd, ID, "KPI_ALLOCATE_ID", false);
                    cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                            }
                        }
                    }
                }
            }

        }


        protected void LoadDelivery_Indicators(DbProviderFactory dbf, string kpiStandartID)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "select Row_Number() OVER(order by B.DATE_ENTERED) AS NO, B.ID AS KPI_STANDART_DETAILS_ID, B.KPI_STANDARD_ID, B.KPI_NAME, B.KPI_UNIT, B.RATIO, B.MAX_RATIO_COMPLETE, KPI_CODE " + ControlChars.CrLf
                       + "  , '' AS MONTH_1,'' AS MONTH_2,'' AS MONTH_3,'' AS MONTH_4,'' AS MONTH_5,'' AS MONTH_6,'' AS MONTH_7  " + ControlChars.CrLf
                       + "  , '' AS MONTH_8,  '' AS MONTH_9,'' AS MONTH_10,'' AS MONTH_11 ,'' AS MONTH_12 , '' AS DESCRIPTION    " + ControlChars.CrLf
                                                      + "  ,M.IS_ACCUMULATED_C, M.KPI_TYPE_C                                     " + ControlChars.CrLf
                                                      + " ,B.VALUE_STD_PER_MONTH  from vwB_KPI_STANDARD_DETAILS_Edit  B          " + ControlChars.CrLf
                                                      + "  inner join vwM_KPIS_Edit M ON (M.KPI_CODE = B.KPI_CODE)               " + ControlChars.CrLf;//join lay truong luy ke
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                    //Security.Filter(cmd, m_sMODULE, "edit");

                    Sql.AppendParameter(cmd, kpiStandartID, Sql.SqlFilterMode.Exact, "KPI_STANDARD_ID");
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                //Set default value
                                LoadDefaultValueForMonth();
                            }
                        }
                    }
                }
            }
        }

        protected void LoadDelivery_IndicatorsForUsser(DbProviderFactory dbf, string Year, Guid positionID, Guid employeesId)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = "with CTE as ( select  distinct '' AS ID, B.DATE_ENTERED, B.ID AS KPI_STANDART_DETAILS_ID ,B.KPI_STANDARD_ID, B.KPI_NAME, B.KPI_UNIT, B.RATIO, B.MAX_RATIO_COMPLETE, B.KPI_CODE  " + ControlChars.CrLf
                       + "  , '' AS MONTH_1,'' AS MONTH_2,'' AS MONTH_3,'' AS MONTH_4,'' AS MONTH_5,'' AS MONTH_6,'' AS MONTH_7     " + ControlChars.CrLf
                       + "  , '' AS MONTH_8,  '' AS MONTH_9,'' AS MONTH_10,'' AS MONTH_11 ,'' AS MONTH_12 , '' AS DESCRIPTION       " + ControlChars.CrLf
                                                      + "  ,M.IS_ACCUMULATED_C, M.KPI_TYPE_C                                        " + ControlChars.CrLf
                                                      + "  ,B.VALUE_STD_PER_MONTH  from vwB_KPI_STANDARD_DETAILS_Edit B             " + ControlChars.CrLf
                                                      + "  inner join vwB_KPI_STANDARD_Edit A ON (A.ID = B.KPI_STANDARD_ID)         " + ControlChars.CrLf
                                                      + "  inner join vwM_KPIS_Edit M ON (M.KPI_CODE = B.KPI_CODE)                  " + ControlChars.CrLf//join lay truong luy ke
                                                      + "  WHERE A.YEAR = @YEAR AND A.POSITION_ID = @POSITION_ID AND A.APPROVE_STATUS = @APPROVE_STATUS " + ControlChars.CrLf
                    //+ "   AND A.ORGANIZATION_ID = @ORGANIZATION_ID    " + ControlChars.CrLf
                                                      + "   AND A.AREA_ID_C = (select TOP 1 AREA_C from vwEMPLOYEES_Edit where ID = @EMPLOYEES_ID) " + ControlChars.CrLf//khu vuc
                                                      + "   AND A.BUSINESS_CODE = (select TOP 1 M_BUSINESS_CODE_C from vwEMPLOYEES_Edit where ID = @EMPLOYEES_ID) " + ControlChars.CrLf//BUSINESS_CODE => Chuc nang
                                                      + "   AND A.SENIORITY_ID  = (select TOP 1 SENIORITY_C from vwEMPLOYEES_Edit where ID = @EMPLOYEES_ID) " + ControlChars.CrLf //SENIORITY =>Tham nien
                                                      + "   AND A.CONTRACT_TYPE  = (select TOP 1 CONTRACT_TYPE_C from vwEMPLOYEES_Edit where ID = @EMPLOYEES_ID) " + ControlChars.CrLf //CONTRACT_TYPE => Loai hop dong
                    // Doan nay se sua theo khu vuc 
                       + " )  select CTE.*, ROW_NUMBER() OVER (order by CTE.DATE_ENTERED) AS NO from CTE " + ControlChars.CrLf;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                    //Security.Filter(cmd, m_sMODULE, "edit");

                    //Sql.AppendParameter(cmd, Year, Sql.SqlFilterMode.Exact, "YEAR");
                    Sql.AddParameter(cmd, "@YEAR", Year);
                    Sql.AddParameter(cmd, "@POSITION_ID", positionID);
                    //Sql.AddParameter(cmd, "@ORGANIZATION_ID", organizationId);
                    Sql.AddParameter(cmd, "@EMPLOYEES_ID", employeesId);
                    Sql.AddParameter(cmd, "@APPROVE_STATUS", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE);
                    cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                ViewState["CurrentTable"] = dtCurrent;
                                //Set default value
                                LoadDefaultValueForMonth();
                            }
                        }
                    }
                }
            }
        }

        protected void LoadDefaultValueForMonth()
        {
            if (grdMain.Items.Count > 0)
            {
                for (int i = 0; i < grdMain.Items.Count; i++)
                {
                    HiddenField hdfIS_ACCUMULATED = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfIS_ACCUMULATED");
                    Label lblValueMonth = (Label)grdMain.Items[i].Cells[4].FindControl("lblValueMonth");
                    TextBox txtMonth1 = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtMonth1");
                    TextBox txtMonth2 = (TextBox)grdMain.Items[i].Cells[4].FindControl("txtMonth2");
                    TextBox txtMonth3 = (TextBox)grdMain.Items[i].Cells[5].FindControl("txtMonth3");
                    TextBox txtMonth4 = (TextBox)grdMain.Items[i].Cells[6].FindControl("txtMonth4");
                    TextBox txtMonth5 = (TextBox)grdMain.Items[i].Cells[7].FindControl("txtMonth5");
                    TextBox txtMonth6 = (TextBox)grdMain.Items[i].Cells[8].FindControl("txtMonth6");
                    TextBox txtMonth7 = (TextBox)grdMain.Items[i].Cells[9].FindControl("txtMonth7");
                    TextBox txtMonth8 = (TextBox)grdMain.Items[i].Cells[10].FindControl("txtMonth8");
                    TextBox txtMonth9 = (TextBox)grdMain.Items[i].Cells[11].FindControl("txtMonth9");
                    TextBox txtMonth10 = (TextBox)grdMain.Items[i].Cells[12].FindControl("txtMonth10");
                    TextBox txtMonth11 = (TextBox)grdMain.Items[i].Cells[13].FindControl("txtMonth11");
                    TextBox txtMonth12 = (TextBox)grdMain.Items[i].Cells[14].FindControl("txtMonth12");
                    //0: Binh thuong. 1 Luy ke
                    string formatNumber = Sql.ToString(Application["CONFIG.format_number"]);
                    bool isLuyKe = Sql.ToBoolean(hdfIS_ACCUMULATED.Value);
                    if (KPIs_Utils.START_MONTH.Equals(periodMonth))
                    {
                        txtMonth1.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 1), formatNumber) : lblValueMonth.Text;
                        txtMonth2.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 2), formatNumber) : lblValueMonth.Text;
                        txtMonth3.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 3), formatNumber) : lblValueMonth.Text;
                        txtMonth4.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 4), formatNumber) : lblValueMonth.Text;
                        txtMonth5.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 5), formatNumber) : lblValueMonth.Text;
                        txtMonth6.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 6), formatNumber) : lblValueMonth.Text;
                        txtMonth7.Text = string.Empty;
                        txtMonth8.Text = string.Empty;
                        txtMonth9.Text = string.Empty;
                        txtMonth10.Text = string.Empty;
                        txtMonth11.Text = string.Empty;
                        txtMonth12.Text = string.Empty;
                    }
                    if (KPIs_Utils.END_MONTH.Equals(periodMonth))
                    {
                        txtMonth1.Text = string.Empty;
                        txtMonth2.Text = string.Empty;
                        txtMonth3.Text = string.Empty;
                        txtMonth4.Text = string.Empty;
                        txtMonth5.Text = string.Empty;
                        txtMonth6.Text = string.Empty;
                        txtMonth7.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 7), formatNumber) : lblValueMonth.Text;
                        txtMonth8.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 8), formatNumber) : lblValueMonth.Text;
                        txtMonth9.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 9), formatNumber) : lblValueMonth.Text;
                        txtMonth10.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 10), formatNumber) : lblValueMonth.Text;
                        txtMonth11.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 11), formatNumber) : lblValueMonth.Text;
                        txtMonth12.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 12), formatNumber) : lblValueMonth.Text;
                    }
                    if (KPIs_Utils.FULL_MONTH.Equals(periodMonth))
                    {
                        txtMonth1.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 1), formatNumber) : lblValueMonth.Text;
                        txtMonth2.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 2), formatNumber) : lblValueMonth.Text;
                        txtMonth3.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 3), formatNumber) : lblValueMonth.Text;
                        txtMonth4.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 4), formatNumber) : lblValueMonth.Text;
                        txtMonth5.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 5), formatNumber) : lblValueMonth.Text;
                        txtMonth6.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 6), formatNumber) : lblValueMonth.Text;
                        txtMonth7.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 7), formatNumber) : lblValueMonth.Text;
                        txtMonth8.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 8), formatNumber) : lblValueMonth.Text;
                        txtMonth9.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 9), formatNumber) : lblValueMonth.Text;
                        txtMonth10.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 10), formatNumber) : lblValueMonth.Text;
                        txtMonth11.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 11), formatNumber) : lblValueMonth.Text;
                        txtMonth12.Text = isLuyKe ? KPIs_Utils.FormatFloat(Sql.ToString(Sql.ToDecimal(lblValueMonth.Text) * 12), formatNumber) : lblValueMonth.Text;
                    }
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIB0203";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
