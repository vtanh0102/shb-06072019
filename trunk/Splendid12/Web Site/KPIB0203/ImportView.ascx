<%@ Control CodeBehind="ImportView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.KPIB0203.ImportView" %>

<div id="divImportView">
    <%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Import" Title="Import.LBL_MODULE_NAME" EnableModuleLabel="false" EnablePrint="false" HelpName="ImportView" EnableHelp="true" runat="Server" />

    <ul class="tablist">
        <li id="liImportStep1" class="active"><a id="linkImportStep1" href="javascript:SelectWizardTab(1);" class="current"><%= "1. " + L10n.Term("Import.LBL_IMPORT_STEP_SELECT_SOURCE"   ) %></a></li>
        <%--<li id="liImportStep2" class=""><a id="linkImportStep2" href="javascript:SelectWizardTab(2);" class=""><%= "2. " + L10n.Term("Import.LBL_IMPORT_STEP_SPECIFY_DEFAULTS") %></a></li>
        <li id="liImportStep3" class="">
            <a id="linkImportStep3" href="javascript:SelectWizardTab(3);" class="">
                <span id="spnStep3Upload" style="display: inline"><%= "3. " + L10n.Term("Import.LBL_IMPORT_STEP_UPLOAD_FILE") %></span>
                <span id="spnStep3Connect" style="display: none"><%= "3. " + L10n.Term("Import.LBL_IMPORT_STEP_CONNECT"    ) %></span>
            </a>
        </li>
        <li id="liImportStep4" class=""><a id="linkImportStep4" href="javascript:SelectWizardTab(4);" class=""><%= "4. " + L10n.Term("Import.LBL_IMPORT_STEP_MAP_FIELDS"      ) %></a></li>
        <li id="liImportStep5" class=""><a id="linkImportStep5" href="javascript:SelectWizardTab(5);" class=""><%= "5. " + L10n.Term("Import.LBL_IMPORT_STEP_DUPLICATE_FILTER") %></a></li>
        <li id="liImportStep6" class=""><a id="linkImportStep6" href="javascript:SelectWizardTab(6);" class=""><%= "6. " + L10n.Term("Import.LBL_IMPORT_STEP_BUSINESS_RULES"  ) %></a></li>
        <li id="liImportStep7" class=""><a id="linkImportStep7" href="javascript:SelectWizardTab(7);" class=""><%= "7. " + L10n.Term("Import.LBL_IMPORT_STEP_RESULTS"         ) %></a></li>--%>
    </ul>
    <SplendidCRM:InlineScript runat="server">
        <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
        <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('[id$=ctl00_cntBody_ctlImportView_ctlDynamicButtons_ctl00_btnIMPORT_PREVIEW]').hide();
            });
        </script>
    </SplendidCRM:InlineScript>


    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <br />
                <table id="tblUploadFile" border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td align="left" class="dataLabel" colspan="4">
                            <%= L10n.Term("Import.LBL_SELECT_FILE") %>&nbsp;<asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' runat="server" />
                            <asp:RequiredFieldValidator ID="reqFILENAME" ControlToValidate="fileIMPORT" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" Enabled="false" EnableClientScript="false" EnableViewState="false" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dataLabel">
                            <input id="fileIMPORT" type="file" size="60" maxlength="255" runat="server" />
                            <asp:Button ID="btnUpload" CommandName="Import.Upload" OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term("Import.LBL_UPLOAD_BUTTON_LABEL" ) + "  " %>' ToolTip='<%# L10n.Term("Import.LBL_UPLOAD_BUTTON_TITLE" ) %>' runat="server" />
                            <asp:Button ID="btnDownload" CommandName="Import.Download" OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term(".LBL_DOWNLOAD_TEMPLATE" ) + "  " %>' ToolTip='<%# L10n.Term(".LBL_DOWNLOAD_TEMPLATE" ) %>' runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dataField">
                            <%--<%= L10n.Term("Import.LBL_HAS_HEADER") %>&nbsp;
								<asp:CheckBox ID="chkHasHeader" CssClass="checkbox" runat="server" />--%>
                        </td>
                    </tr>
                </table>
                <table id="tblUploadConnect" border="0" cellspacing="0" cellpadding="0" width="100%" style="display: none">
                    <tr>
                        <td align="left" class="dataLabel">
                            <asp:Button ID="btnSignIn" CssClass="button" Text='<%# "  " + L10n.Term("Import.LBL_SIGNIN_BUTTON_LABEL" ) + "  " %>' ToolTip='<%# L10n.Term("Import.LBL_SIGNIN_BUTTON_TITLE" ) %>' runat="server" />&nbsp;
								<asp:Button ID="btnConnect" CommandName="Import.Connect" OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term("Import.LBL_CONNECT_BUTTON_LABEL") + "  " %>' ToolTip='<%# L10n.Term("Import.LBL_CONNECT_BUTTON_TITLE") %>' runat="server" />&nbsp;
								<asp:Button ID="btnSignOut" CommandName="Import.SignOut" OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term("Import.LBL_SIGNOUT_BUTTON_LABEL") + "  " %>' ToolTip='<%# L10n.Term("Import.LBL_SIGNOUT_BUTTON_TITLE") %>' runat="server" />&nbsp;
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="lblStatus" Font-Bold="true" runat="server" /><br />
                <asp:Label ID="lblSuccessCount" runat="server" /><br />
                <asp:Label ID="lblDuplicateCount" runat="server" /><br />
                <asp:Label ID="lblFailedCount" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</div>

