using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;
using System.Text;

namespace SplendidCRM.KPIB0203
{

    /// <summary>
    ///		Summary description for ListView.
    /// </summary>
    public class ListViewSearch : SplendidControl
    {
        protected _controls.HeaderButtons ctlModuleHeader;
        protected _controls.ExportHeader ctlExportHeader;
        protected _controls.SearchView ctlSearchView;

        protected _controls.CheckAll ctlCheckAll;
        protected _controls.CheckAll ctlCheckAllEmployee;

        protected UniqueStringCollection arrSelectFields;
        protected DataView vwMain;
        protected SplendidGrid grdMain;
        protected Label lblError;
        protected MassUpdate ctlMassUpdate;
        protected Panel pnlMassUpdateSeven;

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Search")
                {
                    grdMain.CurrentPageIndex = 0;
                    grdMain.DataBind();
                }
                else if (e.CommandName == "SortGrid")
                {
                    grdMain.SetSortFields(e.CommandArgument as string[]);
                    arrSelectFields.AddFields(grdMain.SortColumn);
                }
                else if (e.CommandName == "SelectAll")
                {
                    if (vwMain == null)
                        grdMain.DataBind();
                    ctlCheckAll.SelectAll(vwMain, "ID");
                    grdMain.DataBind();
                }
                else if (e.CommandName == "ToggleMassUpdate")
                {
                    pnlMassUpdateSeven.Visible = !pnlMassUpdateSeven.Visible;
                }
                //MassSubmitApproval
                else if (e.CommandName == "MassSubmitApproval")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, ctlMassUpdate.APPROVED_BY, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("search.aspx");
                        }
                    }
                }

                //MassApprove
                else if (e.CommandName == "MassApprove")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, trn);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_InitResult(sIDs, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("search.aspx");
                        }
                    }
                }

                //MassReject
                else if (e.CommandName == "MassReject")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_REJECT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("search.aspx");
                        }
                    }
                }
                else if (e.CommandName == "MassUpdate")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassUpdate(sIDs, ctlMassUpdate.ASSIGNED_USER_ID, ctlMassUpdate.PRIMARY_TEAM_ID, ctlMassUpdate.TEAM_SET_LIST, ctlMassUpdate.ADD_TEAM_SET, ctlMassUpdate.TAG_SET_NAME, ctlMassUpdate.ADD_TAG_SET, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("search.aspx");
                        }
                    }
                }
                else if (e.CommandName == "MassDelete")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "delete", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0203_SqlProcs.spB_KPI_ALLOCATES_MassDelete(sIDs, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect("search.aspx");
                        }
                    }
                }
                else if (e.CommandName == "MassMerge")
                {
                    Server.Transfer("merge.aspx", true);
                }
                else if (e.CommandName == "Export")
                {
                    int nACLACCESS = SplendidCRM.Security.GetUserAccess(m_sMODULE, "export");
                    if (nACLACCESS >= 0)
                    {
                        if (vwMain == null)
                            grdMain.DataBind();
                        if (vwMain != null && grdMain.Items.Count > 0)
                        {
                            grdMain.AllowCustomPaging = false;
                            grdMain.AllowPaging = false;
                            var oldSize = grdMain.PageSize;
                            grdMain.PageSize = vwMain.Count;
                            grdMain.DataSource = vwMain;
                            grdMain.DataBind();
                            KPIs_Export.ExportDataGridViewDToExcel(grdMain, 2);
                            grdMain.PageSize = oldSize;
                            grdMain.AllowPaging = true;
                            grdMain.AllowCustomPaging = true;
                            grdMain.DataBind();
                            //if (nACLACCESS == ACL_ACCESS.OWNER)
                            //    vwMain.RowFilter = "ASSIGNED_USER_ID = '" + Security.USER_ID.ToString() + "'";
                            //string[] arrID = ctlCheckAll.SelectedItemsArray;
                            //SplendidExport.Export(vwMain, m_sMODULE, ctlExportHeader.ExportFormat, ctlExportHeader.ExportRange, grdMain.CurrentPageIndex, grdMain.PageSize, arrID, grdMain.AllowCustomPaging);
                        }
                        else
                        {
                            //lblError.Text += ControlChars.CrLf + "vwMain is null.";
                            lblError.Text += ControlChars.CrLf + L10n.Term(".ERR_M_KPI_REPORT_NOT_FOUND");
                        }
                    }
                }
                else if (e.CommandName == "Report" || e.CommandName == "ReportAll")
                {
                    string[] arrID = ctlCheckAll.SelectedItemsArray;
                    if (e.CommandName == "ReportAll")
                    {
                        if (vwMain == null)
                            grdMain.DataBind();
                        if (vwMain != null)
                        {
                            arrID = new string[vwMain.Count];
                            for (int i = 0; i < vwMain.Count; i++)
                            {
                                arrID[i] = Sql.ToString(vwMain[i]["ID"]);
                            }
                        }
                    }
                    if (arrID != null && arrID.Length > 0)
                    {
                        if (Sql.IsEmptyString(e.CommandArgument))
                        {
                            throw (new Exception("Button CommandArgument is empty."));
                        }
                        string sRenderURL = "~/Reports/render.aspx?" + Sql.ToString(e.CommandArgument).Trim();
                        if (!sRenderURL.EndsWith("&"))
                            sRenderURL += "&";
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        string sMODULE_FIELD_NAME = SplendidCRM.Crm.Modules.SingularTableName(sTABLE_NAME) + "_ID";
                        sRenderURL += sMODULE_FIELD_NAME + "=" + String.Join("&" + sMODULE_FIELD_NAME + "=", arrID);
                        Server.Transfer(sRenderURL);
                    }
                    else
                    {
                        throw (new Exception(L10n.Term(".LBL_NOTHING_SELECTED")));
                    }
                }
                else
                {
                    grdMain.DataBind();
                    if (Page.Master is SplendidMaster)
                        (Page.Master as SplendidMaster).Page_Command(sender, e);
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        protected void grdMain_OnSelectMethod(int nCurrentPageIndex, int nPageSize)
        {
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                    cmd.CommandText = "  from vw" + sTABLE_NAME + "_List" + ControlChars.CrLf
                                    + "  left outer join vwSUGARFAVORITES                                       " + ControlChars.CrLf
                                    + "               on vwSUGARFAVORITES.FAVORITE_RECORD_ID = ID               " + ControlChars.CrLf
                                    + "              and vwSUGARFAVORITES.FAVORITE_USER_ID   = @FAVORITE_USER_ID" + ControlChars.CrLf;
                    Sql.AddParameter(cmd, "@FAVORITE_USER_ID", Security.USER_ID);
                    if (this.StreamEnabled())
                    {
                        cmd.CommandText += "  left outer join vwSUBSCRIPTIONS                                               " + ControlChars.CrLf;
                        cmd.CommandText += "               on vwSUBSCRIPTIONS.SUBSCRIPTION_PARENT_ID = ID                   " + ControlChars.CrLf;
                        cmd.CommandText += "              and vwSUBSCRIPTIONS.SUBSCRIPTION_USER_ID   = @SUBSCRIPTION_USER_ID" + ControlChars.CrLf;
                        Sql.AddParameter(cmd, "@SUBSCRIPTION_USER_ID", Security.USER_ID);
                        arrSelectFields.Add("SUBSCRIPTION_PARENT_ID");
                    }

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                    //Security.Filter(cmd, m_sMODULE, "list");

                    ctlSearchView.SqlSearchClauseCustom(cmd, m_sMODULE);
                    
                    cmd.CommandText = "select " + (Request.Form[ctlExportHeader.ExportUniqueID] != null ? SplendidDynamic.ExportGridColumns(m_sMODULE + ".Export", arrSelectFields) : Sql.FormatSelectFields(arrSelectFields))
                                    + (!this.StreamEnabled() ? "     , null as SUBSCRIPTION_PARENT_ID" + ControlChars.CrLf : String.Empty)
                                    + cmd.CommandText;
                    if (nPageSize > 0)
                    {
                        Sql.PageResults(cmd, sTABLE_NAME, grdMain.OrderByClause(), nCurrentPageIndex, nPageSize);
                    }
                    else
                    {
                        cmd.CommandText += grdMain.OrderByClause();
                    }

                    if (bDebug)
                        RegisterClientScriptBlock("SQLPaged", Sql.ClientScriptBlock(cmd));

                    if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                    {
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);
                                this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);

                                vwMain = dt.DefaultView;
                                grdMain.DataSource = vwMain;
                            }
                        }
                        ctlExportHeader.Visible = true;
                    }
                    else
                    {
                        ctlExportHeader.Visible = false;
                    }
                    ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                    ctlCheckAll.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                }
            }
        }


        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblPERIOD = (Label)e.Item.FindControl("lblPERIOD");
                if (lblPERIOD != null)
                {
                    if (lblPERIOD.Text != string.Empty)
                    {
                        lblPERIOD.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "KPI_ALLOCATE_PERIOD_LIST", lblPERIOD.Text);
                    }
                }

                Label lblAPPROVE_STATUS = (Label)e.Item.FindControl("lblAPPROVE_STATUS");
                if (lblAPPROVE_STATUS != null)
                {
                    if (lblAPPROVE_STATUS.Text != string.Empty)
                    {
                        lblAPPROVE_STATUS.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "M_GROUP_KPIS_APPROVE_STATUS", lblAPPROVE_STATUS.Text);
                    }

                }
                Label lblASSIGN_BY = (Label)e.Item.FindControl("lblASSIGN_BY");
                if (lblASSIGN_BY != null)
                {
                    lblASSIGN_BY.Text = KPIs_Utils.getFullNameFromUser(lblASSIGN_BY.Text);
                }
                Label lblAPPROVED_BY = (Label)e.Item.FindControl("lblAPPROVED_BY");
                if (lblAPPROVED_BY != null)
                {
                    lblAPPROVED_BY.Text = KPIs_Utils.getFullNameFromUser(lblAPPROVED_BY.Text);
                }
                //check seniority
                Label lblFULL_NAME = (Label)e.Item.FindControl("lblFULL_NAME");
                if (lblFULL_NAME != null)
                {
                    HtmlInputHidden hdfSENIORITY = e.Item.FindControl("hdfSENIORITY") as HtmlInputHidden;
                    if (hdfSENIORITY != null)
                    {
                        if (hdfSENIORITY.Value == string.Empty)
                        {
                            lblFULL_NAME.CssClass = "warning";
                            lblFULL_NAME.ToolTip = L10n.Term("KPIB0203.WARNING_EMPLOYEE_MISS_SENIORITY");
                        }
                    }
                }
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
            if (!this.Visible)
                return;

            try
            {
                if (SplendidCRM.Crm.Config.allow_custom_paging() && SplendidCRM.Crm.Modules.CustomPaging(m_sMODULE))
                {
                    grdMain.AllowCustomPaging = (Request.Form[ctlExportHeader.ExportUniqueID] == null || !(ctlExportHeader.ExportRange == "All" || ctlExportHeader.ExportRange == "Selected")) && !ctlCheckAll.SelectAllChecked;
                    grdMain.SelectMethod += new SelectMethodHandler(grdMain_OnSelectMethod);
                    ctlCheckAll.ShowSelectAll = false;
                }

                if (this.IsMobile && grdMain.Columns.Count > 0)
                    grdMain.Columns[0].Visible = false;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        StringBuilder sql = new StringBuilder();
                        grdMain.OrderByClause("ALLOCATE_CODE", "desc");

                        //CASE WHEN KPIA.ID IS NULL THEN U.ID ELSE KPIA.ID END
                        sql.Append("SELECT                                                       " + ControlChars.CrLf);
                        sql.Append("      CASE WHEN A.ID IS NULL THEN E.ID ELSE A.ID END AS ID   " + ControlChars.CrLf);
                        sql.Append("	, A.PERIOD          " + ControlChars.CrLf);
                        sql.Append("	, E.FULL_NAME       " + ControlChars.CrLf);
                        sql.Append("	, E.SENIORITY_C     " + ControlChars.CrLf);
                        sql.Append("    , O.ORGANIZATION_NAME AS DEPARTMENT  " + ControlChars.CrLf);
                        sql.Append("    , OP.ORGANIZATION_NAME AS BRANCH  " + ControlChars.CrLf);
                        sql.Append("	, P.POSITION_NAME AS TITLE           " + ControlChars.CrLf);
                        sql.Append("	, A.ALLOCATE_CODE    " + ControlChars.CrLf);
                        sql.Append("	, A.APPROVE_STATUS   " + ControlChars.CrLf);
                        sql.Append("	, A.ASSIGN_BY        " + ControlChars.CrLf);
                        sql.Append("	, A.ASSIGN_DATE      " + ControlChars.CrLf);
                        sql.Append("	, A.APPROVED_BY      " + ControlChars.CrLf);
                        sql.Append("	, A.APPROVED_DATE    " + ControlChars.CrLf);
                        sql.Append("	, A.CREATED_BY_ID    " + ControlChars.CrLf);
                        sql.Append("	, A.DESCRIPTION      " + ControlChars.CrLf);
                        sql.Append("    , A.ASSIGNED_USER_ID            " + ControlChars.CrLf);
                        sql.Append("    , A.ASSIGNED_TO                 " + ControlChars.CrLf);
                        sql.Append("    , A.DATE_MODIFIED               " + ControlChars.CrLf);//DATE_MODIFIED
                        sql.Append("	, fav.FAVORITE_RECORD_ID        " + ControlChars.CrLf);
                        sql.Append("    , sub.SUBSCRIPTION_PARENT_ID    " + ControlChars.CrLf);
                        sql.Append("  FROM vwEMPLOYEES_List E           " + ControlChars.CrLf);
                        sql.Append("   LEFT JOIN vwB_KPI_ALLOCATES_List A ON (A.EMPLOYEE_ID = E.ID)  " + ControlChars.CrLf);
                        sql.Append("  LEFT OUTER JOIN M_POSITION P ON (P.ID = A.POSITION_ID)            " + ControlChars.CrLf);
                        sql.Append("  LEFT OUTER JOIN M_ORGANIZATION O ON (O.ID = A.ORGANIZATION_ID AND O.LEVEL_NUMBER = 4)    " + ControlChars.CrLf);
                        sql.Append("  INNER JOIN M_ORGANIZATION OP ON (OP.ID = E.ORGANIZATION_PARENT_ID_C AND OP.LEVEL_NUMBER = 3)   " + ControlChars.CrLf);
                        sql.Append("  LEFT OUTER JOIN vwSUGARFAVORITES fav ON (fav.FAVORITE_RECORD_ID = A.ID)                   " + ControlChars.CrLf);
                        sql.Append("  LEFT OUTER JOIN vwSUBSCRIPTIONS sub ON (sub.SUBSCRIPTION_PARENT_ID = A.ID )  WHERE 1=1    " + ControlChars.CrLf);

                        cmd.CommandText = sql.ToString();

                        //Security.Filter(cmd, m_sMODULE, "list");
                        ctlSearchView.SqlSearchClauseCustom(cmd, m_sMODULE, false, true);

                        cmd.CommandText += grdMain.OrderByClause();
                        grdMain.AllowSorting = true;
                        this.ctlSearchView.ShowSearchTabs = false;

                        if (grdMain.AllowCustomPaging)
                        {
                            /*cmd.CommandText = "select count(*)" + ControlChars.CrLf
                                            + cmd.CommandText;

                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                            if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                            {
                                grdMain.VirtualItemCount = Sql.ToInteger(cmd.ExecuteScalar());
                            }*/
                        }
                        else
                        {

                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                            if (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE))
                            {
                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);

                                        vwMain = dt.DefaultView;
                                        grdMain.DataSource = vwMain;
                                        if (!IsPostBack)
                                        {
                                            // 12/14/2007 Paul.  Only set the default sort if it is not already set.  It may have been set by SearchView. 
                                            if (String.IsNullOrEmpty(grdMain.SortColumn))
                                            {
                                                grdMain.SortColumn = "ALLOCATE_CODE";
                                                grdMain.SortOrder = "desc";
                                            }
                                            grdMain.ApplySort();
                                            grdMain.DataBind();
                                        }
                                    }
                                }
                                ctlExportHeader.Visible = true;
                            }
                            else
                            {
                                ctlExportHeader.Visible = false;
                            }
                            ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                            ctlCheckAll.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
                        }


                        ListControl lstORGPARENT = ctlSearchView.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;
                        if (lstORGPARENT != null)
                        {
                            lstORGPARENT.SelectedIndexChanged += new EventHandler(ddrORGPARENT_SelectIndexChange);
                            lstORGPARENT.AutoPostBack = true;
                        }
                    }
                }
                ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "list") > 0 ? true : false;
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        private void ddrORGPARENT_SelectIndexChange(object sender, EventArgs e)
        {
            ListControl lstORG = ctlSearchView.FindControl("ORGANIZATION_ID_C") as ListControl;
            ListControl lstORGPARENT = ctlSearchView.FindControl("ORGANIZATION_PARENT_ID_C") as ListControl;

            if (lstORG != null && lstORGPARENT != null)
            {
                var dtSource = SplendidCache.M_ORGANIZATION_POS(lstORGPARENT.SelectedValue);
                if (dtSource == null || dtSource.Rows.Count == 0)
                {
                    dtSource = SplendidCache.M_ORGANIZATION_POS();
                }      
                lstORG.DataSource = dtSource;
                lstORG.DataTextField = "ORGANIZATION_NAME";
                lstORG.DataValueField = "ID";
                lstORG.DataBind();
                lstORG.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlSearchView.Command += new CommandEventHandler(Page_Command);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);
            ctlExportHeader.Command += new CommandEventHandler(Page_Command);
            ctlCheckAll.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIB0203";
            SetMenu(m_sMODULE);
            arrSelectFields = new UniqueStringCollection();
            arrSelectFields.Add("ID");
            arrSelectFields.Add("ASSIGNED_USER_ID");
            arrSelectFields.Add("FAVORITE_RECORD_ID");
            arrSelectFields.Add("PENDING_PROCESS_ID");
            this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields, Page_Command);
            if (Security.GetUserAccess(m_sMODULE, "delete") < 0 && Security.GetUserAccess(m_sMODULE, "edit") < 0)
                ctlMassUpdate.Visible = false;

            if (SplendidDynamic.StackedLayout(Page.Theme))
            {
                ctlModuleHeader.Command += new CommandEventHandler(Page_Command);
                ctlModuleHeader.AppendButtons(m_sMODULE + "." + LayoutListView, Guid.Empty, null);
                grdMain.IsMobile = this.IsMobile;
                grdMain.MassUpdateView = m_sMODULE + ".MassUpdate";
                grdMain.Command += new CommandEventHandler(Page_Command);
                if (!IsPostBack)
                    pnlMassUpdateSeven.Visible = false;
            }
        }
        #endregion
    }
}
