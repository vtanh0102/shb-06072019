﻿/*
 * Copyright (C) 2013 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;

namespace SplendidCRM.ReportDesigner
{
	[ServiceContract]
	[ServiceBehavior( IncludeExceptionDetailInFaults = true )]
	[AspNetCompatibilityRequirements( RequirementsMode = AspNetCompatibilityRequirementsMode.Required )]
	public class Rest
	{
		#region json utils
		// http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
		private static long UnixTicks(DateTime dt)
		{
			return ( dt.Ticks - 621355968000000000 ) / 10000;
		}

		private static string ToJsonDate(object dt)
		{
			return "\\/Date(" + UnixTicks( Sql.ToDateTime( dt ) ).ToString() + ")\\/";
		}

		// 08/03/2012 Paul.  FromJsonDate is used on Web Capture services. 
		public static DateTime FromJsonDate(string s)
		{
			DateTime dt = DateTime.MinValue;
			if ( s.StartsWith( "\\/Date(" ) && s.EndsWith( ")\\/" ) )
			{
				s = s.Replace( "\\/Date(", "" );
				s = s.Replace( ")\\/", "" );
				long lEpoch = Sql.ToLong( s );
				dt = new DateTime( lEpoch * 10000 + 621355968000000000 );
			}
			else
			{
				dt = Sql.ToDateTime( s );
			}
			return dt;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		// http://schotime.net/blog/index.php/2008/07/27/dataset-datatable-to-json/
		private static List<Dictionary<string, object>> RowsToDictionary(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
			// 10/11/2012 Paul.  dt will be null when no results security filter applied. 
			if ( dt != null )
			{
				foreach ( DataRow dr in dt.Rows )
				{
					// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
					Dictionary<string, object> drow = new Dictionary<string, object>();
					if ( dt.Columns.Contains( "ID" ) )
					{
						Guid gID = Sql.ToGuid( dr["ID"] );
						if ( !Sql.IsEmptyString( sBaseURI ) && !Sql.IsEmptyString( sModuleName ) )
						{
							Dictionary<string, object> metadata = new Dictionary<string, object>();
							metadata.Add( "uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "" );
							metadata.Add( "type", "SplendidCRM." + sModuleName );
							if ( dr.Table.Columns.Contains( "DATE_MODIFIED_UTC" ) )
							{
								DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime( dr["DATE_MODIFIED_UTC"] );
								metadata.Add( "etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
							}
							drow.Add( "__metadata", metadata );
						}
					}

					for ( int i = 0; i < dt.Columns.Count; i++ )
					{
						if ( dt.Columns[i].DataType.FullName == "System.DateTime" )
						{
							// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
							drow.Add( dt.Columns[i].ColumnName, ToJsonDate( T10n.FromServerTime( dr[i] ) ) );
						}
						else
						{
							drow.Add( dt.Columns[i].ColumnName, dr[i] );
						}
					}
					objs.Add( drow );
				}
			}
			return objs;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			results.Add("results", RowsToDictionary(sBaseURI, sModuleName, dt, T10n));
			d.Add("d", results);
			// 04/21/2017 Paul.  Count should be returend as a number. 
			if ( dt != null )
				d.Add("__count", dt.Rows.Count);
			return d;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataRow dr, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			Dictionary<string, object> drow = new Dictionary<string, object>();

			// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
			if ( dr.Table.Columns.Contains( "ID" ) )
			{
				Guid gID = Sql.ToGuid( dr["ID"] );
				if ( !Sql.IsEmptyString( sBaseURI ) && !Sql.IsEmptyString( sModuleName ) )
				{
					Dictionary<string, object> metadata = new Dictionary<string, object>();
					metadata.Add( "uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "" );
					metadata.Add( "type", "SplendidCRM." + sModuleName );
					if ( dr.Table.Columns.Contains( "DATE_MODIFIED_UTC" ) )
					{
						DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime( dr["DATE_MODIFIED_UTC"] );
						metadata.Add( "etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
					}
					drow.Add( "__metadata", metadata );
				}
			}

			for ( int i = 0; i < dr.Table.Columns.Count; i++ )
			{
				if ( dr.Table.Columns[i].DataType.FullName == "System.DateTime" )
				{
					// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
					drow.Add( dr.Table.Columns[i].ColumnName, ToJsonDate( T10n.FromServerTime( dr[i] ) ) );
				}
				else
				{
					drow.Add( dr.Table.Columns[i].ColumnName, dr[i] );
				}
			}

			results.Add( "results", drow );
			d.Add( "d", results );
			return d;
		}

		#endregion

		#region Get
		[OperationContract]
		[WebInvoke( Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json )]
		public Stream GetModules()
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest Request = HttpContext.Current.Request;

			WebOperationContext.Current.OutgoingResponse.Headers.Add( "Cache-Control", "no-cache" );
			WebOperationContext.Current.OutgoingResponse.Headers.Add( "Pragma", "no-cache" );

			HttpSessionState Session = HttpContext.Current.Session;
			Dictionary<string, object> dict = Session["ReportDesigner.Modules"] as Dictionary<string, object>;
#if DEBUG
			dict = null;
#endif
			if ( dict == null )
			{
				try
				{
					if ( Security.IsAuthenticated() )
					{
						// 08/20/2016 Paul.  We need to continually update the SplendidSession so that it expires along with the ASP.NET Session. 
						SplendidSession.CreateSession(HttpContext.Current.Session);
						string sBaseURI = String.Empty;  //Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath;

						TimeZone T10n = TimeZone.CreateTimeZone( Sql.ToGuid( Application["CONFIG.default_timezone"] ) );
						L10N L10n = new L10N( HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string );

						dict = new Dictionary<string, object>();
						Dictionary<string, object> d = new Dictionary<string, object>();
						List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();
						dict.Add( "d", d );
						d.Add( "results", results );

						List<string> lstModules = new List<string>();
						List<string> lstDetailViews = new List<string>();
						Dictionary<string, string> hashModuleNames = new Dictionary<string, string>();
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL;
							// 04/17/2018 Paul.  Add CustomReportView to simplify reporting. 
							sSQL = "select MODULE_NAME  as ModuleName  " + ControlChars.CrLf
							     + "     , DISPLAY_NAME as DisplayName " + ControlChars.CrLf
							     + "     , TABLE_NAME   as TableName   " + ControlChars.CrLf
							     + "     , 'ID'         as PrimaryField" + ControlChars.CrLf
							     + "     , 0            as Relationship" + ControlChars.CrLf
							     + "     , 0            as CustomReportView" + ControlChars.CrLf
							     + "  from vwMODULES_Reporting         " + ControlChars.CrLf
							     + " where USER_ID    = @USER_ID       " + ControlChars.CrLf
							     + "   and TABLE_NAME is not null      " + ControlChars.CrLf
							     + " order by TABLE_NAME               " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter( cmd, "@USER_ID", Security.USER_ID );
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									( (IDbDataAdapter) da ).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill( dt );
										//dict = ToJson(sBaseURI, "Modules", dt, T10n);
										foreach ( DataRow row in dt.Rows )
										{
											string sModuleName = Sql.ToString( row["ModuleName"] );
											string sDisplayName = L10n.Term( Sql.ToString( row["DisplayName"] ) );
											lstModules.Add( sModuleName );
											// 01/08/2015 Paul.  Activities module combines Calls, Meetings, Tasks, Notes and Emails. 
											if ( sModuleName == "Calls" || sModuleName == "Meetings" || sModuleName == "Tasks" || sModuleName == "Notes" || sModuleName == "Emails" )
											{
												if ( !hashModuleNames.ContainsKey("Activities") )
												{
													lstModules.Add( "Activities" );
													hashModuleNames.Add( "Activities", sDisplayName );
												}
											}
											lstDetailViews.Add( sModuleName + ".DetailView" );
											hashModuleNames.Add( sModuleName, sDisplayName );
											row["DisplayName"] = sDisplayName;
											
											Dictionary<string, object> drow = new Dictionary<string, object>();
											for ( int i = 0; i < dt.Columns.Count; i++ )
											{
												drow.Add( dt.Columns[i].ColumnName, row[i] );
											}
											results.Add( drow );
										}
									}
								}
							}
							
							// 04/17/2018 Paul.  Add CustomReportView to simplify reporting. 
							sSQL = "select MODULE_NAME        as ModuleName  " + ControlChars.CrLf
							     + "     , ''                 as DisplayName " + ControlChars.CrLf
							     + "     , TABLE_NAME         as TableName   " + ControlChars.CrLf
							     + "     , PRIMARY_FIELD      as PrimaryField" + ControlChars.CrLf
							     + "     , 1                  as Relationship" + ControlChars.CrLf
							     + "     , DETAIL_NAME        as RelatedName " + ControlChars.CrLf
							     + "     , 0                  as CustomReportView" + ControlChars.CrLf
							     + "  from vwDETAILVIEWS_RELATIONSHIPS       " + ControlChars.CrLf
							     + " where TABLE_NAME is not null            " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AppendParameter(cmd, lstModules.ToArray(), "MODULE_NAME");
								Sql.AppendParameter(cmd, lstDetailViews.ToArray(), "DETAIL_NAME");
								cmd.CommandText += " order by DETAIL_NAME, RELATIONSHIP_ORDER" + ControlChars.CrLf;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill(dt);
										//dict = ToJson(sBaseURI, "Modules", dt, T10n);
										foreach ( DataRow row in dt.Rows )
										{
											string sRelatedName = Sql.ToString(row["RelatedName"]).Replace(".DetailView", "");
											string sModuleName  = Sql.ToString(row["ModuleName" ]);
											string sTableName   = Sql.ToString(row["TableName"  ]);
											if ( sTableName.StartsWith("vw") || sTableName.StartsWith("VW") )
												sTableName = sTableName.Substring(2);
											row["TableName"  ] = sTableName;
											row["DisplayName"] = hashModuleNames[sRelatedName] + " " + hashModuleNames[sModuleName];
											
											Dictionary<string, object> drow = new Dictionary<string, object>();
											for ( int i = 0; i < dt.Columns.Count; i++ )
											{
												drow.Add( dt.Columns[i].ColumnName, row[i] );
											}
											results.Add( drow );
										}
									}
								}
							}
							
							// 04/17/2018 Paul.  Add CustomReportView to simplify reporting. 
							sSQL = "select MODULE_NAME        as ModuleName      " + ControlChars.CrLf
							     + "     , NAME               as DisplayName     " + ControlChars.CrLf
							     + "     , VIEW_NAME          as TableName       " + ControlChars.CrLf
							     + "     , PRIMARY_FIELD      as PrimaryField    " + ControlChars.CrLf
							     + "     , 0                  as Relationship    " + ControlChars.CrLf
							     + "     , 1                  as CustomReportView" + ControlChars.CrLf
							     + "  from vwMODULES_REPORT_VIEWS                " + ControlChars.CrLf
							     + " where 1 = 1                                 " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AppendParameter(cmd, lstModules.ToArray(), "MODULE_NAME");
								cmd.CommandText += " order by DisplayName" + ControlChars.CrLf;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill(dt);
										foreach ( DataRow row in dt.Rows )
										{
											string sTableName = Sql.ToString(row["TableName"]);
											if ( sTableName.StartsWith("vw") || sTableName.StartsWith("VW") )
												sTableName = sTableName.Substring(2);
											row["TableName"] = sTableName;
											
											Dictionary<string, object> drow = new Dictionary<string, object>();
											for ( int i = 0; i < dt.Columns.Count; i++ )
											{
												drow.Add( dt.Columns[i].ColumnName, row[i] );
											}
											results.Add( drow );
										}
									}
								}
							}
							
							//Dictionary<string, object> d             = dict["d"] as Dictionary<string, object>;
							//List<Dictionary<string, object>> results = d["results"] as List<Dictionary<string, object>>;
							for ( int i = 0; i < results.Count; i++ )
							{
								sSQL = "select ColumnName as ColumnName" + ControlChars.CrLf
									 + "     , ColumnType as ColumnType" + ControlChars.CrLf
									 + "     , CsType     as DataType  " + ControlChars.CrLf
									 + "     , length     as DataLength" + ControlChars.CrLf
									 + "     , prec       as Precision " + ControlChars.CrLf
									 + "     , max_length as MaxLength " + ControlChars.CrLf
									 + "  from vwSqlColumns            " + ControlChars.CrLf
									 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
									 + " order by colid                " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Dictionary<string, object> module = results[i];
									string sModuleName = Sql.ToString( module["ModuleName"] );
									string sTableName = Sql.ToString( module["TableName"] );
									// 02/20/2016 Paul.  Make sure to use upper case for Oracle. 
									Sql.AddParameter( cmd, "@OBJECTNAME", Sql.MetadataName(cmd, "vw" + sTableName));
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										( (IDbDataAdapter) da ).SelectCommand = cmd;
										using ( DataTable dtColumns = new DataTable() )
										{
											da.Fill( dtColumns );
											dtColumns.Columns.Add( "DisplayName", typeof( System.String ) );
											dtColumns.Columns.Add( "TableName", typeof( System.String ) );

											//module.Add("Fields", RowsToDictionary(sBaseURI, "Fields", dtColumns, T10n));
											List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
											foreach ( DataRow row in dtColumns.Rows )
											{
												row["TableName"] = sTableName;
												row["DisplayName"] = Utils.TableColumnName( L10n, sModuleName, Sql.ToString( row["ColumnName"] ) );

												Dictionary<string, object> drow = new Dictionary<string, object>();
												for ( int j = 0; j < dtColumns.Columns.Count; j++ )
												{
													drow.Add( dtColumns.Columns[j].ColumnName, row[j] );
												}
												objs.Add( drow );
											}
											module.Add( "Fields", objs );
										}
									}
								}
							}
						}
						Session["ReportDesigner.Modules"] = dict;
					}
				}
				catch ( Exception ex )
				{
					SplendidError.SystemError( new StackTrace( true ).GetFrame( 0 ), ex );
				}
			}
			MemoryStream stm = null;
			if ( dict != null )
			{
				JavaScriptSerializer json = new JavaScriptSerializer();
				string sResponse = json.Serialize( dict );
				byte[] byResponse = Encoding.UTF8.GetBytes( sResponse );
				stm = new MemoryStream( byResponse );
			}
			return stm;
		}

		// 07/04/2016 Paul.  We need a separate method for workflow modules. 
		[OperationContract]
		[WebInvoke( Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json )]
		public Stream GetBusinessProcessModules()
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			HttpSessionState     Session     = HttpContext.Current.Session    ;

			WebOperationContext.Current.OutgoingResponse.Headers.Add("Cache-Control", "no-cache");
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Pragma"       , "no-cache");

			Dictionary<string, object> dict = Session["ReportDesigner.BusinessProcessModules"] as Dictionary<string, object>;
#if DEBUG
			dict = null;
#endif
			if ( dict == null )
			{
				try
				{
					if ( Security.IsAuthenticated() )
					{
						// 08/20/2016 Paul.  We need to continually update the SplendidSession so that it expires along with the ASP.NET Session. 
						SplendidSession.CreateSession(HttpContext.Current.Session);
						string sBaseURI = String.Empty;  //Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath;

						TimeZone T10n = TimeZone.CreateTimeZone(Sql.ToGuid(Application["CONFIG.default_timezone"]));
						L10N     L10n = new L10N(HttpContext.Current.Session["USER_SETTINGS/CULTURE"] as string);

						dict = new Dictionary<string, object>();
						Dictionary<string, object> d = new Dictionary<string, object>();
						List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();
						dict.Add("d", d);
						d.Add("results", results);

						List<string> lstModules     = new List<string>();
						List<string> lstDetailViews = new List<string>();
						Dictionary<string, string> hashModuleNames = new Dictionary<string, string>();
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL;
							sSQL = "select MODULE_NAME  as ModuleName  " + ControlChars.CrLf
								 + "     , DISPLAY_NAME as DisplayName " + ControlChars.CrLf
								 + "     , TABLE_NAME   as TableName   " + ControlChars.CrLf
								 + "     , 'ID'         as PrimaryField" + ControlChars.CrLf
								 + "     , 0            as Relationship" + ControlChars.CrLf
								 + "  from vwMODULES_BusinessProcess   " + ControlChars.CrLf
								 + " where TABLE_NAME is not null      " + ControlChars.CrLf
								 + " order by TABLE_NAME               " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									( (IDbDataAdapter) da ).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill(dt);
										//dict = ToJson(sBaseURI, "Modules", dt, T10n);
										foreach ( DataRow row in dt.Rows )
										{
											string sModuleName  = Sql.ToString(row["ModuleName"]);
											string sDisplayName = L10n.Term(Sql.ToString(row["DisplayName"]));
											lstModules.Add(sModuleName);
											// 01/08/2015 Paul.  Activities module combines Calls, Meetings, Tasks, Notes and Emails. 
											if ( sModuleName == "Calls" || sModuleName == "Meetings" || sModuleName == "Tasks" || sModuleName == "Notes" || sModuleName == "Emails" )
											{
												if ( !hashModuleNames.ContainsKey("Activities") )
												{
													lstModules     .Add("Activities");
													hashModuleNames.Add("Activities", sDisplayName);
												}
											}
											lstDetailViews .Add(sModuleName + ".DetailView");
											hashModuleNames.Add(sModuleName, sDisplayName);
											row["DisplayName"] = sDisplayName;
											
											Dictionary<string, object> drow = new Dictionary<string, object>();
											for ( int i = 0; i < dt.Columns.Count; i++ )
											{
												drow.Add(dt.Columns[i].ColumnName, row[i]);
											}
											results.Add( drow );
										}
									}
								}
							}
							
							//Dictionary<string, object> d             = dict["d"] as Dictionary<string, object>;
							//List<Dictionary<string, object>> results = d["results"] as List<Dictionary<string, object>>;
							for ( int i = 0; i < results.Count; i++ )
							{
								sSQL = "select ColumnName as ColumnName" + ControlChars.CrLf
									 + "     , ColumnType as ColumnType" + ControlChars.CrLf
									 + "     , CsType     as DataType  " + ControlChars.CrLf
									 + "     , length     as DataLength" + ControlChars.CrLf
									 + "     , prec       as Precision " + ControlChars.CrLf
									 + "     , max_length as MaxLength " + ControlChars.CrLf
									 + "  from vwSqlColumns            " + ControlChars.CrLf
									 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
									 + " order by colid                " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Dictionary<string, object> module = results[i];
									string sModuleName = Sql.ToString(module["ModuleName"]);
									string sTableName  = Sql.ToString(module["TableName" ]);
									// 02/20/2016 Paul.  Make sure to use upper case for Oracle. 
									Sql.AddParameter( cmd, "@OBJECTNAME", Sql.MetadataName(cmd, "vw" + sTableName));
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter) da).SelectCommand = cmd;
										using ( DataTable dtColumns = new DataTable() )
										{
											da.Fill(dtColumns);
											dtColumns.Columns.Add("DisplayName", typeof(System.String));
											dtColumns.Columns.Add("TableName"  , typeof(System.String));

											//module.Add("Fields", RowsToDictionary(sBaseURI, "Fields", dtColumns, T10n));
											List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
											foreach ( DataRow row in dtColumns.Rows )
											{
												row["TableName"  ] = sTableName;
												row["DisplayName"] = Utils.TableColumnName(L10n, sModuleName, Sql.ToString(row["ColumnName"]));

												Dictionary<string, object> drow = new Dictionary<string, object>();
												for ( int j = 0; j < dtColumns.Columns.Count; j++ )
												{
													drow.Add( dtColumns.Columns[j].ColumnName, row[j] );
												}
												objs.Add(drow);
											}
											module.Add("Fields", objs);
										}
									}
								}
							}
						}
						Session["ReportDesigner.BusinessProcessModules"] = dict;
					}
				}
				catch ( Exception ex )
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				}
			}
			MemoryStream stm = null;
			if ( dict != null )
			{
				JavaScriptSerializer json = new JavaScriptSerializer();
				string sResponse = json.Serialize(dict);
				byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
				stm = new MemoryStream(byResponse);
			}
			return stm;
		}
		#endregion

	}
}
