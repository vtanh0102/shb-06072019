﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;

namespace SplendidCRM.KPIR010105
{

	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                          ;
		protected HtmlTable       tblMain                      ;
		protected PlaceHolder     plcSubPanel                  ;
        protected ReportViewer rpView;

        protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
						DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							DataRow   rowCurrent = null;
							DataTable dtCurrent  = new DataTable();
							if ( !Sql.IsEmptyGuid(gID) )
							{
								string sSQL ;
								sSQL = "select *           " + ControlChars.CrLf
								     + "  from vwKPIR010105_Edit" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Security.Filter(cmd, m_sMODULE, "edit");
									Sql.AppendParameter(cmd, gID, "ID", false);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											rowCurrent = dtCurrent.Rows[0];
											DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
											if ( Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"])  && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED )
											{
												ctlDynamicButtons.ShowButton("SaveConcurrency", true);
												ctlFooterButtons .ShowButton("SaveConcurrency", true);
												throw(new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
											}
										}
										else
										{
											gID = Guid.Empty;
										}
									}
								}
							}

							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
							bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
							if ( bDUPLICATE_CHECHING_ENABLED )
							{
								if ( Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0 )
								{
									ctlDynamicButtons.ShowButton("SaveDuplicate", true);
									ctlFooterButtons .ShowButton("SaveDuplicate", true);
									throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
								}
							}
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIR010105_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MAIN_POS"                           ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DNTD"                               ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_DU_NO"                         ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_TANG_RONG_DU_NO"               ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_DU_NO_BQ"                      ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TL_NO_XAU_KHCN"                     ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "HDTD"                               ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_HUY_DONG"                      ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_TANG_RONG_HUY_DONG"            ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_HUY_DONG_BQ"                   ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THU_THUAN_KHCN"                     ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_THU_THUAN_KHCN"                ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_SO_LUONG_KHCN"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_SO_LUONG_KHVV"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_SO_LUONG_KHVIP"                ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_THE_NOI_DIA"                   ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_THE_QUOC_TE"                   ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_BQ_THE_NOI_DIA"                ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_KIEU_HOI"                      ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_INTERNET_BANKING"              ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_MOBILE_BANKING"                ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "THKH_DV_SMS"                        ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TONG_HOP_THKH_KHCN"                 ).FloatValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "XEP_LOAI"                           ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "MONTH"                              ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR"                               ).IntegerValue
										, new SplendidCRM.DynamicControl(this, rowCurrent, "DON_VI_KINH_DOANH"                  ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME"                       ).Text
										, trn
										);

									SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
									SplendidCRM.SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
										, "save"
										, trn
										);
									if ( plcSubPanel.Visible )
									{
										foreach ( Control ctl in plcSubPanel.Controls )
										{
											InlineEditControl ctlSubPanel = ctl as InlineEditControl;
											if ( ctlSubPanel != null )
											{
												ctlSubPanel.Save(gID, m_sMODULE, trn);
											}
										}
									}
									trn.Commit();
									SplendidCache.ClearFavorites();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
							rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
							this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
						}
						
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else
							Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyGuid(gID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
            else if (e.CommandName == "Search")
            {
                rpView = this.FindControl("ReportViewer1") as ReportViewer;
                this.rpView.Reset();
                this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010105_Xep_Loai_CN.rdlc");
                // Xuất sắc
                DataTable dt = getData("vwKPIR010105_XUAT_SAC");
                DateTime dtExport = DateTime.Now;
      //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
              
                ReportDataSource rds = new ReportDataSource("KPIR010105_XUAT_SAC", dt);
                this.rpView.LocalReport.DataSources.Clear();
                this.rpView.LocalReport.DataSources.Add(rds);
            
                //TOT
                DataTable dtTOT = getData("vwKPIR010105_TOT");
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
              
                ReportDataSource rdsTOT = new ReportDataSource("KPIR010105_TOT", dtTOT);
 //               this.rpView.LocalReport.DataSources.Clear();
                this.rpView.LocalReport.DataSources.Add(rdsTOT);


                //KHA
                DataTable dtKHA = getData("vwKPIR010105_KHA");
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
              
                ReportDataSource rdsKHA = new ReportDataSource("KPIR010105_KHA", dtKHA);
                this.rpView.LocalReport.DataSources.Add(rdsKHA);

                //TRUNG_BINH
                DataTable dtTRUNG_BINH = getData("vwKPIR010105_TRUNG_BINH");
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
                
                ReportDataSource rdsTRUNG_BINH = new ReportDataSource("KPIR010105_TRUNG_BINH", dtTRUNG_BINH);
                this.rpView.LocalReport.DataSources.Add(rdsTRUNG_BINH);

                // DUOI_TRUNG_BINH
                DataTable dtDUOI_TRUNG_BINH = getData("vwKPIR010105_DUOI_TRUNG_BINH");
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
               
                ReportDataSource rdsDUOI_TRUNG_BINH = new ReportDataSource("KPIR010105_DUOI_TRUNG_BINH", dtDUOI_TRUNG_BINH);
                this.rpView.LocalReport.DataSources.Add(rdsDUOI_TRUNG_BINH);
                //  YEU KEM
                DataTable dtYEU_KEM = getData("vwKPIR010105_YEU_KEM");
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);

                ReportDataSource rdsYEU_KEM = new ReportDataSource("KPIR010105_YEU_KEM", dtYEU_KEM);
                this.rpView.LocalReport.DataSources.Add(rdsYEU_KEM);

                this.rpView.DataBind();
                this.rpView.LocalReport.Refresh();
                //RATE
                DataTable dtRATE = getDataRate();
                //          DateTime dtExport = DateTime.Now;
                //          dt.Columns.Add("DATE_EXPORT", typeof(System.String)).SetOrdinal(25);
             
                ReportDataSource rdsRATE = new ReportDataSource("KPIR010105_RATE", dtRATE);
                this.rpView.LocalReport.DataSources.Add(rdsRATE);

                this.rpView.DataBind();
                this.rpView.LocalReport.Refresh();

            }

        }
        private DataTable getData(string sView)
        {
            DataTable dt = new DataTable();
            try
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "select *           " + ControlChars.CrLf
                         + "  from "+ sView + ControlChars.CrLf;
                    if (!string.IsNullOrEmpty(new DynamicControl(this, "MONTH").SelectedValue) && !string.IsNullOrEmpty(new DynamicControl(this, "YEAR").SelectedValue))
                    {
                        sSQL = string.Format(@"select *           " + ControlChars.CrLf
                      + "  from " + sView + ControlChars.CrLf
                        + "  WHERE MONTH = @MONTH" + ControlChars.CrLf
                       + "  AND YEAR = @YEAR" + ControlChars.CrLf);
                    }
                    else if (string.IsNullOrEmpty(new DynamicControl(this, "MONTH").SelectedValue) && !string.IsNullOrEmpty(new DynamicControl(this, "YEAR").SelectedValue))
                    {
                        sSQL = string.Format(@"select *           " + ControlChars.CrLf
                    + "  from " + sView + ControlChars.CrLf

                     + "  WHERE YEAR = @YEAR" + ControlChars.CrLf);
                    }
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        if (!string.IsNullOrEmpty(new DynamicControl(this, "MONTH").SelectedValue) && !string.IsNullOrEmpty(new DynamicControl(this, "YEAR").SelectedValue))
                        {
                            Sql.AddParameter(cmd, "@MONTH", new DynamicControl(this, "MONTH").SelectedValue);
                            Sql.AddParameter(cmd, "@YEAR", new DynamicControl(this, "YEAR").SelectedValue);
                        }
                        else if (string.IsNullOrEmpty(new DynamicControl(this, "MONTH").SelectedValue) && !string.IsNullOrEmpty(new DynamicControl(this, "YEAR").SelectedValue))
                        {

                            Sql.AddParameter(cmd, "@YEAR", new DynamicControl(this, "YEAR").SelectedValue);
                        }
                        //           Security.Filter(cmd, m_sMODULE, "edit");

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }
        private DataTable getDataRate()
        {
            DataTable dt = new DataTable();
            try
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL;
                    sSQL = "select *           " + ControlChars.CrLf
                         + "  FROM (SELECT [NAME], RATE_C FROM vwM_KPIR010105_RATE) AS t " + ControlChars.CrLf
                    + "  PIVOT( " + ControlChars.CrLf
                    + "  MAX(RATE_C) " + ControlChars.CrLf
                    + "  FOR [NAME] IN(THKH_THE_NOI_DIA " + ControlChars.CrLf
                    + "  , THU_THUAN_KHCN " + ControlChars.CrLf
                    + "  ,THKH_DV_SMS " + ControlChars.CrLf
                    + "  ,THKH_SO_LUONG_KHVV " + ControlChars.CrLf
                    + "  ,THKH_TANG_RONG_HUY_DONG " + ControlChars.CrLf
                    + "  ,THKH_HUY_DONG " + ControlChars.CrLf
                    + "  ,TONG_HOP_THKH_KHCN " + ControlChars.CrLf
                    + "  ,THKH_DU_NO " + ControlChars.CrLf
                    + "  ,THKH_TANG_RONG_DU_NO " + ControlChars.CrLf
                    + "  ,THKH_SO_LUONG_KHCN " + ControlChars.CrLf
                    + "  ,THKH_THE_QUOC_TE " + ControlChars.CrLf
                    + "  ,TL_NO_XAU_KHCN " + ControlChars.CrLf
                    + "  ,THKH_SO_LUONG_KHVIP " + ControlChars.CrLf

                     + "  ,THKH_BQ_THE_NOI_DIA " + ControlChars.CrLf
                    + "  ,XEP_LOAI " + ControlChars.CrLf
                    + "  ,THKH_THU_THUAN_KHCN " + ControlChars.CrLf
                    + "  ,THKH_INTERNET_BANKING " + ControlChars.CrLf
                    + "  ,HDTD " + ControlChars.CrLf
                    + "  ,THKH_DU_NO_BQ " + ControlChars.CrLf
                    + "  ,THKH_MOBILE_BANKING " + ControlChars.CrLf

                     + "  ,THKH_KIEU_HOI " + ControlChars.CrLf
                    + "  ,THKH_HUY_DONG_BQ) " + ControlChars.CrLf
                    + "  ) AS vwM_KPIR010105_RATE " + ControlChars.CrLf;
                   
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Sql.AddParameter(cmd, "@Month", new DynamicControl(this, "MONTH").Text);
                        //Sql.AddParameter(cmd, "@Year", new DynamicControl(this, "YEAR").Text);
                        //           Security.Filter(cmd, m_sMODULE, "edit");

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }
        private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "access") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *           " + ControlChars.CrLf
							     + "  from vwKPIR010105_Edit" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Security.Filter(cmd, m_sMODULE, "edit");
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AppendParameter(cmd, gID, "ID", false);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
											Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											bool bModuleIsAssigned  = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
											Guid gASSIGNED_USER_ID = Guid.Empty;
											if ( bModuleIsAssigned )
												gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

											this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
											this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											TextBox txtNAME = this.FindControl("NAME") as TextBox;
											if ( txtNAME != null )
												txtNAME.Focus();

											ViewState ["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"   ]);
											ViewState ["NAME"              ] = Sql.ToString  (rdr["NAME"            ]);
											ViewState ["ASSIGNED_USER_ID"  ] = gASSIGNED_USER_ID;
											Page.Items["NAME"              ] = ViewState ["NAME"            ];
											Page.Items["ASSIGNED_USER_ID"  ] = ViewState ["ASSIGNED_USER_ID"];
											
											this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
										}
										else
										{
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlDynamicButtons.DisableAll();
											ctlFooterButtons .DisableAll();
											ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
											plcSubPanel.Visible = false;
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("NAME") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIR010105";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
		#endregion
	}
}
