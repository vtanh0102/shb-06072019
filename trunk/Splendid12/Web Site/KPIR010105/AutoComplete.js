
function KPIR010105_KPIR010105_NAME_Changed(fldKPIR010105_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldKPIR010105_NAME.id.substring(0, fldKPIR010105_NAME.id.length - 'KPIR010105_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010105_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_KPIR010105_NAME = document.getElementById(userContext + 'PREV_KPIR010105_NAME');
	if ( fldPREV_KPIR010105_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_KPIR010105_NAME');
	}
	else if ( fldPREV_KPIR010105_NAME.value != fldKPIR010105_NAME.value )
	{
		if ( fldKPIR010105_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIR010105.AutoComplete.KPIR010105_KPIR010105_NAME_Get(fldKPIR010105_NAME.value, KPIR010105_KPIR010105_NAME_Changed_OnSucceededWithContext, KPIR010105_KPIR010105_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('KPIR010105_KPIR010105_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			KPIR010105_KPIR010105_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function KPIR010105_KPIR010105_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'KPIR010105_NAME_AjaxErrors');
		var fldKPIR010105_ID        = document.getElementById(userContext + 'KPIR010105_ID'       );
		var fldKPIR010105_NAME      = document.getElementById(userContext + 'KPIR010105_NAME'     );
		var fldPREV_KPIR010105_NAME = document.getElementById(userContext + 'PREV_KPIR010105_NAME');
		if ( fldKPIR010105_ID        != null ) fldKPIR010105_ID.value        = sID  ;
		if ( fldKPIR010105_NAME      != null ) fldKPIR010105_NAME.value      = sNAME;
		if ( fldPREV_KPIR010105_NAME != null ) fldPREV_KPIR010105_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIR010105.AutoComplete service is null');
	}
}

function KPIR010105_KPIR010105_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'KPIR010105_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldKPIR010105_ID        = document.getElementById(userContext + 'KPIR010105_ID'       );
	var fldKPIR010105_NAME      = document.getElementById(userContext + 'KPIR010105_NAME'     );
	var fldPREV_KPIR010105_NAME = document.getElementById(userContext + 'PREV_KPIR010105_NAME');
	if ( fldKPIR010105_ID        != null ) fldKPIR010105_ID.value        = '';
	if ( fldKPIR010105_NAME      != null ) fldKPIR010105_NAME.value      = '';
	if ( fldPREV_KPIR010105_NAME != null ) fldPREV_KPIR010105_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

