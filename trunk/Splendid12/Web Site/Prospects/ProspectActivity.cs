/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ProspectActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class ProspectActivity: SplendidActivity
	{
		public ProspectActivity()
		{
			this.Name = "ProspectActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.IDProperty))); }
			set { base.SetValue(ProspectActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ProspectActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty SALUTATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SALUTATION", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SALUTATION
		{
			get { return ((string)(base.GetValue(ProspectActivity.SALUTATIONProperty))); }
			set { base.SetValue(ProspectActivity.SALUTATIONProperty, value); }
		}

		public static DependencyProperty FIRST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FIRST_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FIRST_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.FIRST_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.FIRST_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LAST_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.LAST_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.LAST_NAMEProperty, value); }
		}

		public static DependencyProperty TITLEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TITLE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TITLE
		{
			get { return ((string)(base.GetValue(ProspectActivity.TITLEProperty))); }
			set { base.SetValue(ProspectActivity.TITLEProperty, value); }
		}

		public static DependencyProperty DEPARTMENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEPARTMENT", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DEPARTMENT
		{
			get { return ((string)(base.GetValue(ProspectActivity.DEPARTMENTProperty))); }
			set { base.SetValue(ProspectActivity.DEPARTMENTProperty, value); }
		}

		public static DependencyProperty BIRTHDATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BIRTHDATE", typeof(DateTime), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime BIRTHDATE
		{
			get { return ((DateTime)(base.GetValue(ProspectActivity.BIRTHDATEProperty))); }
			set { base.SetValue(ProspectActivity.BIRTHDATEProperty, value); }
		}

		public static DependencyProperty DO_NOT_CALLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DO_NOT_CALL", typeof(bool), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool DO_NOT_CALL
		{
			get { return ((bool)(base.GetValue(ProspectActivity.DO_NOT_CALLProperty))); }
			set { base.SetValue(ProspectActivity.DO_NOT_CALLProperty, value); }
		}

		public static DependencyProperty PHONE_HOMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_HOME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_HOME
		{
			get { return ((string)(base.GetValue(ProspectActivity.PHONE_HOMEProperty))); }
			set { base.SetValue(ProspectActivity.PHONE_HOMEProperty, value); }
		}

		public static DependencyProperty PHONE_MOBILEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_MOBILE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_MOBILE
		{
			get { return ((string)(base.GetValue(ProspectActivity.PHONE_MOBILEProperty))); }
			set { base.SetValue(ProspectActivity.PHONE_MOBILEProperty, value); }
		}

		public static DependencyProperty PHONE_WORKProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_WORK", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_WORK
		{
			get { return ((string)(base.GetValue(ProspectActivity.PHONE_WORKProperty))); }
			set { base.SetValue(ProspectActivity.PHONE_WORKProperty, value); }
		}

		public static DependencyProperty PHONE_OTHERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_OTHER", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_OTHER
		{
			get { return ((string)(base.GetValue(ProspectActivity.PHONE_OTHERProperty))); }
			set { base.SetValue(ProspectActivity.PHONE_OTHERProperty, value); }
		}

		public static DependencyProperty PHONE_FAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_FAX", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_FAX
		{
			get { return ((string)(base.GetValue(ProspectActivity.PHONE_FAXProperty))); }
			set { base.SetValue(ProspectActivity.PHONE_FAXProperty, value); }
		}

		public static DependencyProperty EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL1", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL1
		{
			get { return ((string)(base.GetValue(ProspectActivity.EMAIL1Property))); }
			set { base.SetValue(ProspectActivity.EMAIL1Property, value); }
		}

		public static DependencyProperty EMAIL2Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL2", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL2
		{
			get { return ((string)(base.GetValue(ProspectActivity.EMAIL2Property))); }
			set { base.SetValue(ProspectActivity.EMAIL2Property, value); }
		}

		public static DependencyProperty ASSISTANTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSISTANT", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSISTANT
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSISTANTProperty))); }
			set { base.SetValue(ProspectActivity.ASSISTANTProperty, value); }
		}

		public static DependencyProperty ASSISTANT_PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSISTANT_PHONE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSISTANT_PHONE
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSISTANT_PHONEProperty))); }
			set { base.SetValue(ProspectActivity.ASSISTANT_PHONEProperty, value); }
		}

		public static DependencyProperty EMAIL_OPT_OUTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL_OPT_OUT", typeof(bool), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EMAIL_OPT_OUT
		{
			get { return ((bool)(base.GetValue(ProspectActivity.EMAIL_OPT_OUTProperty))); }
			set { base.SetValue(ProspectActivity.EMAIL_OPT_OUTProperty, value); }
		}

		public static DependencyProperty INVALID_EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INVALID_EMAIL", typeof(bool), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool INVALID_EMAIL
		{
			get { return ((bool)(base.GetValue(ProspectActivity.INVALID_EMAILProperty))); }
			set { base.SetValue(ProspectActivity.INVALID_EMAILProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_STREET", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_STREETProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_CITY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_CITYProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_STATE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_STATEProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_POSTALCODE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_COUNTRY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_STREET", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_STREETProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_CITY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_CITYProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_STATE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_STATEProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_POSTALCODE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_COUNTRY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(ProspectActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(ProspectActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(ProspectActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(ProspectActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.PARENT_IDProperty))); }
			set { base.SetValue(ProspectActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty LEAD_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.LEAD_IDProperty))); }
			set { base.SetValue(ProspectActivity.LEAD_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty TRACKER_KEYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TRACKER_KEY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TRACKER_KEY
		{
			get { return ((string)(base.GetValue(ProspectActivity.TRACKER_KEYProperty))); }
			set { base.SetValue(ProspectActivity.TRACKER_KEYProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.TEAM_IDProperty))); }
			set { base.SetValue(ProspectActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ProspectActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ProspectActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty SMS_OPT_INProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SMS_OPT_IN", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SMS_OPT_IN
		{
			get { return ((string)(base.GetValue(ProspectActivity.SMS_OPT_INProperty))); }
			set { base.SetValue(ProspectActivity.SMS_OPT_INProperty, value); }
		}

		public static DependencyProperty TWITTER_SCREEN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_SCREEN_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TWITTER_SCREEN_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.TWITTER_SCREEN_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.TWITTER_SCREEN_NAMEProperty, value); }
		}

		public static DependencyProperty PICTUREProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PICTURE", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PICTURE
		{
			get { return ((string)(base.GetValue(ProspectActivity.PICTUREProperty))); }
			set { base.SetValue(ProspectActivity.PICTUREProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty PROSPECT_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROSPECT_NUMBER", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PROSPECT_NUMBER
		{
			get { return ((string)(base.GetValue(ProspectActivity.PROSPECT_NUMBERProperty))); }
			set { base.SetValue(ProspectActivity.PROSPECT_NUMBERProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ProspectActivity.CREATED_BYProperty))); }
			set { base.SetValue(ProspectActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(ProspectActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ProspectActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ProspectActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ProspectActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ProspectActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(ProspectActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(ProspectActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ProspectActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ProspectActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.NAMEProperty))); }
			set { base.SetValue(ProspectActivity.NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ProspectActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_HTML", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(ProspectActivity.ALT_ADDRESS_HTMLProperty))); }
			set { base.SetValue(ProspectActivity.ALT_ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CONVERTED_LEAD_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_LEAD_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONVERTED_LEAD_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.CONVERTED_LEAD_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.CONVERTED_LEAD_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(ProspectActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(ProspectActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty LEAD_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ASSIGNED_SET_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.LEAD_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProspectActivity.LEAD_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty LEAD_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_ASSIGNED_USER_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LEAD_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.LEAD_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProspectActivity.LEAD_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProspectActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ProspectActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(ProspectActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(ProspectActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_HTML", typeof(string), typeof(ProspectActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(ProspectActivity.PRIMARY_ADDRESS_HTMLProperty))); }
			set { base.SetValue(ProspectActivity.PRIMARY_ADDRESS_HTMLProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ProspectActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ProspectActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select PROSPECTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwPROSPECTS_AUDIT        PROSPECTS          " + ControlChars.CrLf
							                + " inner join vwPROSPECTS_AUDIT        PROSPECTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on PROSPECTS_AUDIT_OLD.ID = PROSPECTS.ID       " + ControlChars.CrLf
							                + "        and PROSPECTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwPROSPECTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                   from vwPROSPECTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                  where vwPROSPECTS_AUDIT.ID            =  PROSPECTS.ID           " + ControlChars.CrLf
							                + "                                                    and vwPROSPECTS_AUDIT.AUDIT_VERSION <  PROSPECTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                    and vwPROSPECTS_AUDIT.AUDIT_TOKEN   <> PROSPECTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                )" + ControlChars.CrLf
							                + " where PROSPECTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwPROSPECTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwPROSPECTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwPROSPECTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								SALUTATION                     = Sql.ToString  (rdr["SALUTATION"                    ]);
								FIRST_NAME                     = Sql.ToString  (rdr["FIRST_NAME"                    ]);
								LAST_NAME                      = Sql.ToString  (rdr["LAST_NAME"                     ]);
								TITLE                          = Sql.ToString  (rdr["TITLE"                         ]);
								DEPARTMENT                     = Sql.ToString  (rdr["DEPARTMENT"                    ]);
								BIRTHDATE                      = Sql.ToDateTime(rdr["BIRTHDATE"                     ]);
								DO_NOT_CALL                    = Sql.ToBoolean (rdr["DO_NOT_CALL"                   ]);
								PHONE_HOME                     = Sql.ToString  (rdr["PHONE_HOME"                    ]);
								PHONE_MOBILE                   = Sql.ToString  (rdr["PHONE_MOBILE"                  ]);
								PHONE_WORK                     = Sql.ToString  (rdr["PHONE_WORK"                    ]);
								PHONE_OTHER                    = Sql.ToString  (rdr["PHONE_OTHER"                   ]);
								PHONE_FAX                      = Sql.ToString  (rdr["PHONE_FAX"                     ]);
								EMAIL1                         = Sql.ToString  (rdr["EMAIL1"                        ]);
								EMAIL2                         = Sql.ToString  (rdr["EMAIL2"                        ]);
								ASSISTANT                      = Sql.ToString  (rdr["ASSISTANT"                     ]);
								ASSISTANT_PHONE                = Sql.ToString  (rdr["ASSISTANT_PHONE"               ]);
								EMAIL_OPT_OUT                  = Sql.ToBoolean (rdr["EMAIL_OPT_OUT"                 ]);
								INVALID_EMAIL                  = Sql.ToBoolean (rdr["INVALID_EMAIL"                 ]);
								PRIMARY_ADDRESS_STREET         = Sql.ToString  (rdr["PRIMARY_ADDRESS_STREET"        ]);
								PRIMARY_ADDRESS_CITY           = Sql.ToString  (rdr["PRIMARY_ADDRESS_CITY"          ]);
								PRIMARY_ADDRESS_STATE          = Sql.ToString  (rdr["PRIMARY_ADDRESS_STATE"         ]);
								PRIMARY_ADDRESS_POSTALCODE     = Sql.ToString  (rdr["PRIMARY_ADDRESS_POSTALCODE"    ]);
								PRIMARY_ADDRESS_COUNTRY        = Sql.ToString  (rdr["PRIMARY_ADDRESS_COUNTRY"       ]);
								ALT_ADDRESS_STREET             = Sql.ToString  (rdr["ALT_ADDRESS_STREET"            ]);
								ALT_ADDRESS_CITY               = Sql.ToString  (rdr["ALT_ADDRESS_CITY"              ]);
								ALT_ADDRESS_STATE              = Sql.ToString  (rdr["ALT_ADDRESS_STATE"             ]);
								ALT_ADDRESS_POSTALCODE         = Sql.ToString  (rdr["ALT_ADDRESS_POSTALCODE"        ]);
								ALT_ADDRESS_COUNTRY            = Sql.ToString  (rdr["ALT_ADDRESS_COUNTRY"           ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								LEAD_ID                        = Sql.ToGuid    (rdr["LEAD_ID"                       ]);
								ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
								if ( bPast )
									TRACKER_KEY                    = Sql.ToString  (rdr["TRACKER_KEY"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								SMS_OPT_IN                     = Sql.ToString  (rdr["SMS_OPT_IN"                    ]);
								TWITTER_SCREEN_NAME            = Sql.ToString  (rdr["TWITTER_SCREEN_NAME"           ]);
								PICTURE                        = Sql.ToString  (rdr["PICTURE"                       ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								PROSPECT_NUMBER                = Sql.ToString  (rdr["PROSPECT_NUMBER"               ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ALT_ADDRESS_HTML               = Sql.ToString  (rdr["ALT_ADDRESS_HTML"              ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CONVERTED_LEAD_NAME            = Sql.ToString  (rdr["CONVERTED_LEAD_NAME"           ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									LEAD_ASSIGNED_SET_ID           = Sql.ToGuid    (rdr["LEAD_ASSIGNED_SET_ID"          ]);
									LEAD_ASSIGNED_USER_ID          = Sql.ToGuid    (rdr["LEAD_ASSIGNED_USER_ID"         ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									PRIMARY_ADDRESS_HTML           = Sql.ToString  (rdr["PRIMARY_ADDRESS_HTML"          ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProspectActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("PROSPECTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spPROSPECTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, SALUTATION
								, FIRST_NAME
								, LAST_NAME
								, TITLE
								, DEPARTMENT
								, BIRTHDATE
								, DO_NOT_CALL
								, PHONE_HOME
								, PHONE_MOBILE
								, PHONE_WORK
								, PHONE_OTHER
								, PHONE_FAX
								, EMAIL1
								, EMAIL2
								, ASSISTANT
								, ASSISTANT_PHONE
								, EMAIL_OPT_OUT
								, INVALID_EMAIL
								, PRIMARY_ADDRESS_STREET
								, PRIMARY_ADDRESS_CITY
								, PRIMARY_ADDRESS_STATE
								, PRIMARY_ADDRESS_POSTALCODE
								, PRIMARY_ADDRESS_COUNTRY
								, ALT_ADDRESS_STREET
								, ALT_ADDRESS_CITY
								, ALT_ADDRESS_STATE
								, ALT_ADDRESS_POSTALCODE
								, ALT_ADDRESS_COUNTRY
								, DESCRIPTION
								, PARENT_TYPE
								, PARENT_ID
								, LEAD_ID
								, ACCOUNT_NAME
								, TRACKER_KEY
								, TEAM_ID
								, TEAM_SET_LIST
								, SMS_OPT_IN
								, TWITTER_SCREEN_NAME
								, PICTURE
								, TAG_SET_NAME
								, PROSPECT_NUMBER
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProspectActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

