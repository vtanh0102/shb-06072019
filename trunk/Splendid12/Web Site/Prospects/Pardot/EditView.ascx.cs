/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Prospects.Pardot
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.ModuleHeader   ctlModuleHeader  ;
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sID          ;
		protected HtmlTable       tblMain      ;
		protected PlaceHolder     plcSubPanel  ;
		protected Label           lblRawContent;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				Spring.Social.Pardot.Api.Prospect prospect = new Spring.Social.Pardot.Api.Prospect();
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						if ( Spring.Social.Pardot.PardotSync.PardotEnabled(Application) )
						{
							Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
							if ( !Sql.IsEmptyString(sID) )
							{
								prospect = pardot.ProspectOperations.GetById(Sql.ToInteger(sID));
							}
							if ( this.FindControl("salutation"         ) != null ) prospect.salutation          = new DynamicControl(this, "salutation"         ).Text;
							if ( this.FindControl("first_name"         ) != null ) prospect.first_name          = new DynamicControl(this, "first_name"         ).Text;
							if ( this.FindControl("last_name"          ) != null ) prospect.last_name           = new DynamicControl(this, "last_name"          ).Text;
							if ( this.FindControl("email"              ) != null ) prospect.email               = new DynamicControl(this, "email"              ).Text;
							if ( this.FindControl("password"           ) != null ) prospect.password            = new DynamicControl(this, "password"           ).Text;
							if ( this.FindControl("company"            ) != null ) prospect.company             = new DynamicControl(this, "company"            ).Text;
							//if ( this.FindControl("prospect_account_id") != null ) prospect.prospect_account_id = new DynamicControl(this, "prospect_account_id").Text;
							if ( this.FindControl("job_title"          ) != null ) prospect.job_title           = new DynamicControl(this, "job_title"          ).Text;
							if ( this.FindControl("department"         ) != null ) prospect.department          = new DynamicControl(this, "department"         ).Text;
							if ( this.FindControl("country"            ) != null ) prospect.country             = new DynamicControl(this, "country"            ).Text;
							if ( this.FindControl("address_one"        ) != null ) prospect.address_one         = new DynamicControl(this, "address_one"        ).Text;
							if ( this.FindControl("address_two"        ) != null ) prospect.address_two         = new DynamicControl(this, "address_two"        ).Text;
							if ( this.FindControl("city"               ) != null ) prospect.city                = new DynamicControl(this, "city"               ).Text;
							if ( this.FindControl("state"              ) != null ) prospect.state               = new DynamicControl(this, "state"              ).Text;
							if ( this.FindControl("territory"          ) != null ) prospect.territory           = new DynamicControl(this, "territory"          ).Text;
							if ( this.FindControl("zip"                ) != null ) prospect.zip                 = new DynamicControl(this, "zip"                ).Text;
							if ( this.FindControl("phone"              ) != null ) prospect.phone               = new DynamicControl(this, "phone"              ).Text;
							if ( this.FindControl("fax"                ) != null ) prospect.fax                 = new DynamicControl(this, "fax"                ).Text;
							if ( this.FindControl("source"             ) != null ) prospect.source              = new DynamicControl(this, "source"             ).Text;
							if ( this.FindControl("annual_revenue"     ) != null ) prospect.annual_revenue      = new DynamicControl(this, "annual_revenue"     ).Text;
							if ( this.FindControl("employees"          ) != null ) prospect.employees           = new DynamicControl(this, "employees"          ).Text;
							if ( this.FindControl("industry"           ) != null ) prospect.industry            = new DynamicControl(this, "industry"           ).Text;
							if ( this.FindControl("years_in_business"  ) != null ) prospect.years_in_business   = new DynamicControl(this, "years_in_business"  ).Text;
							if ( this.FindControl("comments"           ) != null ) prospect.comments            = new DynamicControl(this, "comments"           ).Text;
							if ( this.FindControl("notes"              ) != null ) prospect.notes               = new DynamicControl(this, "notes"              ).Text;
							if ( this.FindControl("score"              ) != null ) prospect.score               = new DynamicControl(this, "score"              ).IntegerValue;
							if ( this.FindControl("grade"              ) != null ) prospect.grade               = new DynamicControl(this, "grade"              ).Text;
							if ( this.FindControl("crm_lead_fid"       ) != null ) prospect.crm_lead_fid        = new DynamicControl(this, "crm_lead_fid"       ).Text;
							if ( this.FindControl("crm_contact_fid"    ) != null ) prospect.crm_contact_fid     = new DynamicControl(this, "crm_contact_fid"    ).Text;
							if ( this.FindControl("crm_owner_fid"      ) != null ) prospect.crm_owner_fid       = new DynamicControl(this, "crm_owner_fid"      ).Text;
							if ( this.FindControl("crm_account_fid"    ) != null ) prospect.crm_account_fid     = new DynamicControl(this, "crm_account_fid"    ).Text;
							if ( this.FindControl("crm_url"            ) != null ) prospect.crm_url             = new DynamicControl(this, "crm_url"            ).Text;
							if ( this.FindControl("is_do_not_email"    ) != null ) prospect.is_do_not_email     = new DynamicControl(this, "is_do_not_email"    ).Checked;
							if ( this.FindControl("is_do_not_call"     ) != null ) prospect.is_do_not_call      = new DynamicControl(this, "is_do_not_call"     ).Checked;
							if ( this.FindControl("is_reviewed"        ) != null ) prospect.is_reviewed         = new DynamicControl(this, "is_reviewed"        ).Checked;
							if ( this.FindControl("is_starred"         ) != null ) prospect.is_starred          = new DynamicControl(this, "is_starred"         ).Checked;
							if ( this.FindControl("campaign_id"        ) != null ) prospect.campaign_id         = new DynamicControl(this, "campaign_id"        ).IntegerValue;
							if ( !Sql.IsEmptyString(sID) )
							{
								pardot.ProspectOperations.Update(prospect);
							}
							else
							{
								prospect = pardot.ProspectOperations.Insert(prospect);
								sID = prospect.id.ToString();
							}
							Response.Redirect("view.aspx?prospect_id=" + sID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					if ( prospect != null )
					{
#if DEBUG
						lblRawContent.Text = prospect.RawContent;
#endif
					}
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?prospect_id=" + sID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = Spring.Social.Pardot.PardotSync.PardotEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("Pardot", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sID = Sql.ToString(Request["prospect_id"]);
				if ( !IsPostBack )
				{
					string sDuplicateID = Sql.ToString(Request["duplicate_id"]);
					if ( !Sql.IsEmptyString(sID) || !Sql.IsEmptyString(sDuplicateID) )
					{
						Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
						Spring.Social.Pardot.Api.Prospect prospect = null;
						if ( !Sql.IsEmptyString(sDuplicateID) )
							prospect = pardot.ProspectOperations.GetById(Sql.ToInteger(sDuplicateID));
						else
							prospect = pardot.ProspectOperations.GetById(Sql.ToInteger(sID));
						if ( prospect != null )
						{
							DataRow rdr = Spring.Social.Pardot.Api.Prospect.ConvertToRow(prospect);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
							ctlModuleHeader.Title = Sql.ToString(rdr["email"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
							ViewState["ctlModuleHeader.Title"] = ctlModuleHeader.Title;
								
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							TextBox txtNAME = this.FindControl("email") as TextBox;
							if ( txtNAME != null )
								txtNAME.Focus();
								
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("name") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlModuleHeader.Title = Sql.ToString(ViewState["ctlModuleHeader.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Prospects";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

