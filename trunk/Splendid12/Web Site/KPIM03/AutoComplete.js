
function M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed(fldM_ORGANIZATION_ORGANIZATION_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain ORGANIZATION_NAME in the text, so just get the length minus 4. 
	var userContext = fldM_ORGANIZATION_ORGANIZATION_NAME.id.substring(0, fldM_ORGANIZATION_ORGANIZATION_NAME.id.length - 'M_ORGANIZATION_ORGANIZATION_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'M_ORGANIZATION_ORGANIZATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_M_ORGANIZATION_ORGANIZATION_NAME = document.getElementById(userContext + 'PREV_M_ORGANIZATION_ORGANIZATION_NAME');
	if ( fldPREV_M_ORGANIZATION_ORGANIZATION_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_M_ORGANIZATION_ORGANIZATION_NAME');
	}
	else if ( fldPREV_M_ORGANIZATION_ORGANIZATION_NAME.value != fldM_ORGANIZATION_ORGANIZATION_NAME.value )
	{
		if ( fldM_ORGANIZATION_ORGANIZATION_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIM03.AutoComplete.M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Get(fldM_ORGANIZATION_ORGANIZATION_NAME.value, M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed_OnSucceededWithContext, M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sORGANIZATION_NAME = result.ORGANIZATION_NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'M_ORGANIZATION_ORGANIZATION_NAME_AjaxErrors');
		var fldM_ORGANIZATION_ID        = document.getElementById(userContext + 'M_ORGANIZATION_ID'       );
		var fldM_ORGANIZATION_ORGANIZATION_NAME      = document.getElementById(userContext + 'M_ORGANIZATION_ORGANIZATION_NAME'     );
		var fldPREV_M_ORGANIZATION_ORGANIZATION_NAME = document.getElementById(userContext + 'PREV_M_ORGANIZATION_ORGANIZATION_NAME');
		if ( fldM_ORGANIZATION_ID        != null ) fldM_ORGANIZATION_ID.value        = sID  ;
		if ( fldM_ORGANIZATION_ORGANIZATION_NAME      != null ) fldM_ORGANIZATION_ORGANIZATION_NAME.value      = sORGANIZATION_NAME;
		if ( fldPREV_M_ORGANIZATION_ORGANIZATION_NAME != null ) fldPREV_M_ORGANIZATION_ORGANIZATION_NAME.value = sORGANIZATION_NAME;
	}
	else
	{
		alert('result from KPIM03.AutoComplete service is null');
	}
}

function M_ORGANIZATION_M_ORGANIZATION_ORGANIZATION_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'M_ORGANIZATION_ORGANIZATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldM_ORGANIZATION_ID        = document.getElementById(userContext + 'M_ORGANIZATION_ID'       );
	var fldM_ORGANIZATION_ORGANIZATION_NAME      = document.getElementById(userContext + 'M_ORGANIZATION_ORGANIZATION_NAME'     );
	var fldPREV_M_ORGANIZATION_ORGANIZATION_NAME = document.getElementById(userContext + 'PREV_M_ORGANIZATION_ORGANIZATION_NAME');
	if ( fldM_ORGANIZATION_ID        != null ) fldM_ORGANIZATION_ID.value        = '';
	if ( fldM_ORGANIZATION_ORGANIZATION_NAME      != null ) fldM_ORGANIZATION_ORGANIZATION_NAME.value      = '';
	if ( fldPREV_M_ORGANIZATION_ORGANIZATION_NAME != null ) fldPREV_M_ORGANIZATION_ORGANIZATION_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

