/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.Workflow.Runtime;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;

namespace SplendidCRM.Reports
{
	/// <summary>
	///		Summary description for ScheduleView.
	/// </summary>
	public class ScheduleView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.CRON           ctlCRON           ;

		protected Guid            gID                        ;
		protected Guid            gREPORT_ID                 ;
		protected Guid            gPARENT_ID                 ;

		protected DropDownList    STATUS                     ;

		protected TextBox         txtNAME                    ;
		protected DropDownList    lstALERT_TYPE              ;
		protected DropDownList    lstSOURCE_TYPE             ;
		protected DropDownList    lstCUSTOM_TEMPLATE         ;
		protected TextBox         txtALERT_TEXT              ;
		protected RequiredFieldValidator reqNAME      ;

		protected TableRow        trALERT_TEXT               ;
		protected TableRow        trCUSTOM_TEMPLATE          ;

		// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
		protected HiddenField     ASSIGNED_USER_ID             ;
		protected TextBox         ASSIGNED_TO                  ;
		protected TextBox         TEAM_NAME                    ;
		protected HiddenField     TEAM_ID                      ;
		protected _controls.TeamSelect     ctlTeamSelect    ;

		protected RdlDocument     rdl                = null  ;
		protected XomlDocument    xoml               = null  ;
		
		protected DropDownList    lstRELATED                 ;
		protected CheckBox        chkSHOW_XOML               ;

		protected string          sWorkflowXOML              ;
		protected DataGrid        dgFilters                  ;
		protected HtmlInputHidden txtFILTER_ID               ;
		protected DropDownList    lstFILTER_COLUMN_SOURCE    ;
		protected DropDownList    lstFILTER_COLUMN           ;
		protected DropDownList    lstFILTER_OPERATOR         ;
		protected Label           lblRELATED                 ;
		protected Label           lblFILTER_COLUMN_SOURCE    ;
		protected Label           lblFILTER_COLUMN           ;
		protected Label           lblFILTER_OPERATOR_TYPE    ;
		protected Label           lblFILTER_OPERATOR         ;

		// 07/13/2010 Paul.  The new way to manage report attachments is in the Alert RDL. 
		protected HiddenField     txtREPORT_ATTACHMENT_ID    ;
		protected HiddenField     txtREPORT_ID               ;
		protected TextBox         txtREPORT_NAME             ;
		protected TextBox         txtREPORT_PARAMETERS       ;
		// 10/28/2014 Paul.  Allow report render format to be specified. 
		protected DropDownList    lstRENDER_FORMAT           ;
		protected Button          btnAttachmentChanged       ;
		protected DataGrid        dgReportAttachments        ;

		protected Literal         litWORKFLOW_XML            ;
		protected Literal         litREPORT_XML              ;

		public string BaseModule
		{
			get
			{
				return "Reports";
			}
		}

		protected void ResetSearchText()
		{
			lstFILTER_COLUMN_SOURCE.SelectedIndex = 0;
			lstFILTER_COLUMN_SOURCE_Changed(null, null);
			lstFILTER_COLUMN.SelectedIndex = 0;
			lstFILTER_COLUMN_Changed(null, null);
			lstFILTER_OPERATOR.SelectedIndex = 0;
			lstFILTER_OPERATOR_Changed(null, null);

			txtFILTER_ID               .Value    = String.Empty;
		}

		// 07/12/2010 Paul.  Create a valid Workflow RDL so that the Schedule Workflow can be edited in the Workflow editor. 
		protected string BuildWorkflowRDL(string sReportSQL)
		{
			RdlDocument rdl = new RdlDocument(String.Empty, String.Empty, false);
			rdl.SetCustomProperty("Module"           , "Reports"   );
			rdl.SetCustomProperty("Related"          , String.Empty);
			rdl.SetCustomProperty("RelatedModules"   , String.Empty);
			rdl.SetCustomProperty("Relationships"    , String.Empty);
			rdl.SetCustomProperty("Filters"          , String.Empty);
			rdl.SetCustomProperty("FrequencyValue"   , String.Empty);
			rdl.SetCustomProperty("FrequencyInterval", "day"       );
			
			//<Filters><Filter><ID>644e743c-0fd3-44c5-bcd7-bd0b0ea28545</ID><MODULE>Reports</MODULE><MODULE_NAME>Reports REPORTS</MODULE_NAME><TABLE_NAME>REPORTS</TABLE_NAME><DATA_FIELD>REPORTS.ID</DATA_FIELD><FIELD_NAME>ID</FIELD_NAME><DATA_TYPE>guid</DATA_TYPE><OPERATOR>is</OPERATOR><SEARCH_TEXT>15859aab-cdb3-4e20-b95d-6f4a812bafbb, Accounts</SEARCH_TEXT><SEARCH_TEXT_VALUES>15859aab-cdb3-4e20-b95d-6f4a812bafbb</SEARCH_TEXT_VALUES><SEARCH_TEXT_VALUES>Accounts</SEARCH_TEXT_VALUES></Filter></Filters>
			string   sDATA_FIELD    = "REPORTS.ID";
			string   sMODULE_NAME   = "Reports REPORTS";
			string   sDATA_TYPE     = "guid";
			string   sOPERATOR      = "is";
			string[] arrSEARCH_TEXT = new string[] {gREPORT_ID.ToString(), Sql.ToString(ViewState["REPORT_NAME"])};
			string[] arrDATA_FIELD = sDATA_FIELD.Split('.');
			string   sTABLE_NAME   = arrDATA_FIELD[0];
			string   sFIELD_NAME   = arrDATA_FIELD[1];
			
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			XmlNode xFilter = xmlFilters.CreateElement("Filter");
			xmlFilters.DocumentElement.AppendChild(xFilter);
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "ID"         , Guid.NewGuid().ToString());  // The ID of this filter can be randomly generated every time. 
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE"     , sMODULE_NAME.Split(' ')[0]);
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE_NAME", sMODULE_NAME  );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "TABLE_NAME" , sTABLE_NAME   );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_FIELD" , sDATA_FIELD   );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "FIELD_NAME" , sFIELD_NAME   );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_TYPE"  , sDATA_TYPE    );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "OPERATOR"   , sOPERATOR     );
			XmlUtil.SetSingleNode(xmlFilters, xFilter, "SEARCH_TEXT", String.Join(", ", arrSEARCH_TEXT));
			foreach ( string sSEARCH_TEXT in arrSEARCH_TEXT )
			{
				XmlNode xSearchText = xmlFilters.CreateElement("SEARCH_TEXT_VALUES");
				xFilter.AppendChild(xSearchText);
				xSearchText.InnerText = sSEARCH_TEXT;
			}
			
			rdl.SetCustomProperty("Filters", xmlFilters.OuterXml);
			rdl.SetSingleNode("DataSets/DataSet/Query/CommandText", sReportSQL);
			return rdl.OuterXml;
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				gPARENT_ID = Sql.ToGuid(ViewState["PARENT_ID"]);
				if ( e.CommandName == "Save" )
				{
					if ( Page.IsValid )
					{
						// 07/15/2010 Paul.  Use new CRON control. 
						ctlCRON.Validate();
						string sJOB_INTERVAL = ctlCRON.Value;
						
						//rdl.SetCustomProperty("FrequencyValue"   , FREQUENCY_VALUE.Text            );
						//rdl.SetCustomProperty("FrequencyInterval", FREQUENCY_INTERVAL.SelectedValue);
						BuildWorkflowXOML();

						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									string sMODULE_TABLE = "REPORTS";
									string sAUDIT_TABLE  = sMODULE_TABLE + "_AUDIT";
									string sReportSQL    = "select REPORTS.ID    from            vwREPORTS        REPORTS   where 1 = 1     and REPORTS.ID = '" + gREPORT_ID.ToString() + "'  ";
									SqlProcs.spWORKFLOWS_Update
										( ref gPARENT_ID
										, txtNAME.Text
										, "Reports"
										, sAUDIT_TABLE
										, Sql.ToBoolean(STATUS.SelectedValue)
										, "time"
										, "alerts_actions"
										, "all"
										, String.Empty
										, sReportSQL
										, BuildWorkflowRDL(sReportSQL)
										, sJOB_INTERVAL
										, gREPORT_ID  // 06/26/2010 Paul.  The Report is the parent for this workflow. 
										, trn
										);
									// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
									Guid   gASSIGNED_USER_ID = Sql.ToGuid(ASSIGNED_USER_ID.Value);
									Guid   gTEAM_ID          = SplendidCRM.Crm.Config.enable_dynamic_teams() ? gTEAM_ID = ctlTeamSelect.TEAM_ID : Sql.ToGuid(TEAM_ID.Value);
									string sTEAM_SET_LIST    = ctlTeamSelect.TEAM_SET_LIST;
									SqlProcs.spWORKFLOW_ALERT_SHELLS_Update(ref gID, gPARENT_ID, txtNAME.Text, lstALERT_TYPE.SelectedValue, lstSOURCE_TYPE.SelectedValue, Sql.ToGuid(lstCUSTOM_TEMPLATE.SelectedValue), txtALERT_TEXT.Text, rdl.OuterXml, xoml.OuterXml, gASSIGNED_USER_ID, gTEAM_ID, sTEAM_SET_LIST, trn);
									// 06/26/2010 Paul.  We do not need to rebuild the XOML.  It is already valid and customized for report scheduling. 
									//WorkflowBuilder.UpdateMasterWorkflowXoml(Application, gPARENT_ID, trn);
									SqlProcs.spWORKFLOWS_UpdateXOML(gPARENT_ID, false, xoml.OuterXml, trn);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
						}
						// 12/03/2017 Paul.  Redirect to report designer. 
						if ( Sql.ToBoolean(Request["ReportDesigner"]) )
							Response.Redirect("~/ReportDesigner/default.aspx");
						else
							Response.Redirect("~/Reports/default.aspx");
					}
				}
				else if ( e.CommandName == "Delete" )
				{
					SqlProcs.spWORKFLOWS_Delete(gPARENT_ID);
					// 12/03/2017 Paul.  Redirect to report designer. 
					if ( Sql.ToBoolean(Request["ReportDesigner"]) )
						Response.Redirect("~/ReportDesigner/default.aspx");
					else
						Response.Redirect("~/Reports/default.aspx");
				}
				else if ( e.CommandName == "Cancel" )
				{
					// 12/03/2017 Paul.  Redirect to report designer. 
					if ( Sql.ToBoolean(Request["ReportDesigner"]) )
						Response.Redirect("~/ReportDesigner/default.aspx");
					else
						Response.Redirect("~/Reports/default.aspx");
				}
				else if ( e.CommandName == "Filters.Cancel" )
				{
					ctlDynamicButtons.ErrorText = "";
					ResetSearchText();
				}
				else if ( e.CommandName == "Filters.Delete" )
				{
					ctlDynamicButtons.ErrorText = "";
					FiltersDelete(Sql.ToString(e.CommandArgument));
					BuildWorkflowXOML();
					ResetSearchText();
				}
				else if ( e.CommandName == "Filters.Edit" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sFILTER_ID = Sql.ToString(e.CommandArgument);
					string sACTION_TYPE       = String.Empty;
					string sRELATIONSHIP_NAME = String.Empty;
					string sMODULE_NAME       = String.Empty;
					string sDATA_FIELD        = String.Empty;
					string sDATA_TYPE         = String.Empty;
					string sOPERATOR          = String.Empty;
					string sSEARCH_TEXT       = String.Empty;
					string sRECIPIENT_NAME    = String.Empty;
					FiltersGet(sFILTER_ID, ref sACTION_TYPE, ref sRELATIONSHIP_NAME, ref sMODULE_NAME, ref sDATA_FIELD, ref sDATA_TYPE, ref sOPERATOR, ref sRECIPIENT_NAME, ref sSEARCH_TEXT);
					txtFILTER_ID.Value = sFILTER_ID;
					
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstRELATED, sRELATIONSHIP_NAME);
					lstRELATED_Changed(null, null);

					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstFILTER_COLUMN_SOURCE, sMODULE_NAME);
					lstFILTER_COLUMN_SOURCE_Changed(null, null);
					
					string[] arrModule   = sMODULE_NAME.Split(' ');
					string sModule       = arrModule[0];
					string sTableAlias   = arrModule[1];
					string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
					if ( sTableAlias == "USERS_ALL" || sTableAlias == "TEAMS" || sTableAlias == "ACL_ROLES" )
					{
						// 08/19/2010 Paul.  Check the list before assigning the value. 
						Utils.SetSelectedValue(lstFILTER_COLUMN, sSEARCH_TEXT);
					}
					else
					{
						// 08/19/2010 Paul.  Check the list before assigning the value. 
						Utils.SetSelectedValue(lstFILTER_COLUMN, sDATA_FIELD );
					}
					lstFILTER_COLUMN_Changed(null, null);
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstFILTER_OPERATOR     , sOPERATOR   );
					lstFILTER_OPERATOR_Changed(null, null);
				}
				else if ( e.CommandName == "Filters.Update" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sFILTER_ID         = txtFILTER_ID.Value;
					string sUSER_TYPE         = String.Empty;
					string sRELATIONSHIP_NAME = lstRELATED.SelectedValue;
					string sMODULE_NAME       = lstFILTER_COLUMN_SOURCE.SelectedValue;
					string sDATA_FIELD        = lstFILTER_COLUMN       .SelectedValue;
					string sOPERATOR          = lstFILTER_OPERATOR     .SelectedValue;
					string sSEARCH_TEXT       = lstFILTER_COLUMN       .SelectedValue;
					string sRECIPIENT_NAME    = lstFILTER_COLUMN       .SelectedItem.Text;
					string[] arrModule   = sMODULE_NAME.Split(' ');
					string sModule       = arrModule[0];
					string sTableAlias   = arrModule[1];
					if ( sTableAlias == "USERS_ALL" )
						sUSER_TYPE = "specific_user";
					else if ( sTableAlias == "TEAMS" )
						sUSER_TYPE = "specific_team";
					else if ( sTableAlias == "ACL_ROLES" )
						sUSER_TYPE = "specific_role";
					// 12/04/2008 Paul.  We don't need rel_user because a user can be looked up by record. 
					//else if ( sMODULE_NAME != BaseModule + " " + Sql.ToString(Application["Modules." + sModule + ".TableName"]) )
					//	sUSER_TYPE = "rel_user";
					// 10/11/2008 Paul.  Allow sending an alert to records based on ID, PARENT_ID, ACCOUNT_ID or CONTACT_ID. 
					else if ( sDATA_FIELD.EndsWith(".ID") || sDATA_FIELD.EndsWith(".PARENT_ID") || sDATA_FIELD.EndsWith(".CONTACT_ID") || sDATA_FIELD.EndsWith(".ACCOUNT_ID") )
						sUSER_TYPE = "record";
					else
						sUSER_TYPE = "current_user";

					
					FiltersUpdate(sFILTER_ID, sUSER_TYPE, sRELATIONSHIP_NAME, sMODULE_NAME, sDATA_FIELD, String.Empty, sOPERATOR, sRECIPIENT_NAME, sSEARCH_TEXT);
					BuildWorkflowXOML();
					//ResetSearchText();
				}
				else if ( e.CommandName == "ReportAttachments.Delete" )
				{
					ctlDynamicButtons.ErrorText = "";
					ReportAttachmentsDelete(Sql.ToString(e.CommandArgument));
					BuildWorkflowXOML();
				}
				else if ( e.CommandName == "ReportAttachments.Edit" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sATTACHMENT_ID     = Sql.ToString(e.CommandArgument);
					Guid   gREPORT_ID         = Guid.Empty  ;
					Guid   gSCHEDULED_USER_ID = Guid.Empty  ;
					string sREPORT_NAME       = String.Empty;
					string sREPORT_PARAMETERS = String.Empty;
					// 10/28/2014 Paul.  Allow report render format to be specified. 
					string sRENDER_FORMAT     = String.Empty;
					// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
					ReportAttachmentsGet(sATTACHMENT_ID, ref gREPORT_ID, ref gSCHEDULED_USER_ID, ref sREPORT_NAME, ref sREPORT_PARAMETERS, ref sRENDER_FORMAT);
					txtREPORT_ATTACHMENT_ID.Value = sATTACHMENT_ID       ;
					txtREPORT_ID           .Value = gREPORT_ID.ToString();
					txtREPORT_NAME         .Text  = sREPORT_NAME         ;
					txtREPORT_PARAMETERS   .Text  = sREPORT_PARAMETERS   ;
					Utils.SetSelectedValue(lstRENDER_FORMAT, sRENDER_FORMAT);
				}
				else if ( e.CommandName == "ReportAttachments.Update" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sATTACHMENT_ID     = txtREPORT_ATTACHMENT_ID.Value;
					Guid   gREPORT_ID         = Sql.ToGuid(txtREPORT_ID.Value);
					// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
					Guid   gSCHEDULED_USER_ID = Security.USER_ID;
					string sREPORT_NAME       = txtREPORT_NAME.Text  ;
					string sREPORT_PARAMETERS = txtREPORT_PARAMETERS.Text;
					string sRENDER_FORMAT     = lstRENDER_FORMAT.SelectedValue;
					if ( !Sql.IsEmptyGuid(gREPORT_ID) )
					{
						// 10/28/2014 Paul.  Allow report render format to be specified. 
						ReportAttachmentsUpdate(sATTACHMENT_ID, gREPORT_ID, gSCHEDULED_USER_ID, sREPORT_NAME, sREPORT_PARAMETERS, sRENDER_FORMAT);
						txtREPORT_ATTACHMENT_ID.Value = String.Empty;
						txtREPORT_ID           .Value = String.Empty;
						txtREPORT_NAME         .Text  = String.Empty;
						txtREPORT_PARAMETERS   .Text  = String.Empty;
						lstRENDER_FORMAT.SelectedIndex = 0;
						BuildWorkflowXOML();
					}
				}
				else if ( e.CommandName == "ReportAttachments.Cancel" )
				{
					ctlDynamicButtons.ErrorText = "";
					txtREPORT_ATTACHMENT_ID.Value = String.Empty;
					txtREPORT_ID           .Value = String.Empty;
					txtREPORT_NAME         .Text  = String.Empty;
					txtREPORT_PARAMETERS   .Text  = String.Empty;
					lstRENDER_FORMAT.SelectedIndex = 0;
				}
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}
		#region Changed

		protected void lstRELATED_Changed(Object sender, EventArgs e)
		{
			try
			{
				lblRELATED.Text = lstRELATED.SelectedValue;
				rdl.SetCustomProperty("Related"      , lstRELATED.SelectedValue);
				rdl.SetCustomProperty("Relationships", String.Empty);
				lstFILTER_COLUMN_SOURCE_Bind();
				BuildWorkflowXOML();
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void lstFILTER_COLUMN_SOURCE_Changed(Object sender, EventArgs e)
		{
			lblFILTER_COLUMN_SOURCE.Text = lstFILTER_COLUMN_SOURCE.SelectedValue;
			lstFILTER_COLUMN_Bind();
		}

		protected void lstFILTER_COLUMN_Changed(Object sender, EventArgs e)
		{
			lblFILTER_COLUMN.Text = lstFILTER_COLUMN.SelectedValue;
			lblFILTER_OPERATOR_Bind();
		}

		protected void lstFILTER_OPERATOR_Changed(Object sender, EventArgs e)
		{
			lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
			BindSearchText();
		}

		protected void radACTION_TYPE_Changed(Object sender, EventArgs e)
		{
		}
		#endregion

		#region Bind
		private void lstRELATED_Bind()
		{
			DataView vwRelationships = new DataView(SplendidCache.ReportingRelationships());
			vwRelationships.RowFilter = "(RELATIONSHIP_TYPE = 'many-to-many' and LHS_MODULE = \'" + BaseModule + "\')" + ControlChars.CrLf
			                          + " or (RELATIONSHIP_TYPE = 'one-to-many' and LHS_MODULE = \'" + BaseModule + "\')" + ControlChars.CrLf;

			XmlDocument xmlRelationships = new XmlDocument();
			xmlRelationships.AppendChild(xmlRelationships.CreateElement("Relationships"));
			
			XmlNode xRelationship = null;
			foreach(DataRowView row in vwRelationships)
			{
				string sRELATIONSHIP_NAME              = Sql.ToString(row["RELATIONSHIP_NAME"             ]);
				string sLHS_MODULE                     = Sql.ToString(row["LHS_MODULE"                    ]);
				string sLHS_TABLE                      = Sql.ToString(row["LHS_TABLE"                     ]).ToUpper();
				string sLHS_KEY                        = Sql.ToString(row["LHS_KEY"                       ]).ToUpper();
				string sRHS_MODULE                     = Sql.ToString(row["RHS_MODULE"                    ]);
				string sRHS_TABLE                      = Sql.ToString(row["RHS_TABLE"                     ]).ToUpper();
				string sRHS_KEY                        = Sql.ToString(row["RHS_KEY"                       ]).ToUpper();
				string sJOIN_TABLE                     = Sql.ToString(row["JOIN_TABLE"                    ]).ToUpper();
				string sJOIN_KEY_LHS                   = Sql.ToString(row["JOIN_KEY_LHS"                  ]).ToUpper();
				string sJOIN_KEY_RHS                   = Sql.ToString(row["JOIN_KEY_RHS"                  ]).ToUpper();
				// 11/20/2008 Paul.  Quotes, Orders and Invoices have a relationship column. 
				string sRELATIONSHIP_ROLE_COLUMN       = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN"      ]).ToUpper();
				string sRELATIONSHIP_ROLE_COLUMN_VALUE = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN_VALUE"]);
				// 11/25/2008 Paul.  Be careful to only switch when necessary.  Parent accounts should not be switched. 
				if ( sLHS_MODULE != BaseModule && sRHS_MODULE == BaseModule )
				{
					sLHS_MODULE        = Sql.ToString(row["RHS_MODULE"       ]);
					sLHS_TABLE         = Sql.ToString(row["RHS_TABLE"        ]).ToUpper();
					sLHS_KEY           = Sql.ToString(row["RHS_KEY"          ]).ToUpper();
					sRHS_MODULE        = Sql.ToString(row["LHS_MODULE"       ]);
					sRHS_TABLE         = Sql.ToString(row["LHS_TABLE"        ]).ToUpper();
					sRHS_KEY           = Sql.ToString(row["LHS_KEY"          ]).ToUpper();
					sJOIN_TABLE        = Sql.ToString(row["JOIN_TABLE"       ]).ToUpper();
					sJOIN_KEY_LHS      = Sql.ToString(row["JOIN_KEY_RHS"     ]).ToUpper();
					sJOIN_KEY_RHS      = Sql.ToString(row["JOIN_KEY_LHS"     ]).ToUpper();
				}
				string sMODULE_NAME  = sRHS_MODULE + " " + sRHS_TABLE;
				string sDISPLAY_NAME = L10n.Term(".moduleList." + sRHS_MODULE);
				// 10/22/2008 Paul.  An account can be related to an account, but we need to make sure that it gets a separate activity name. 
				if ( sRHS_MODULE == BaseModule )
					sMODULE_NAME += "_REL";
				// 10/26/2011 Paul.  Use the join table so that the display is more descriptive. 
				if ( !Sql.IsEmptyString(sJOIN_TABLE) )
					sDISPLAY_NAME = Sql.CamelCaseModules(L10n, sJOIN_TABLE);
				if ( bDebug )
				{
					sDISPLAY_NAME = "[" + sMODULE_NAME + "] " + sDISPLAY_NAME;
				}
				// 02/18/2009 Paul.  Include the relationship column if provided. 
				// 10/26/2011 Paul.  Include the role. 
				if ( !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) && sRELATIONSHIP_ROLE_COLUMN_VALUE != BaseModule )
					sDISPLAY_NAME += " " + sRELATIONSHIP_ROLE_COLUMN_VALUE;
				// 10/26/2011 Paul.  Add the relationship so that we can have a unique lookup. 
				sMODULE_NAME += " " + sRELATIONSHIP_NAME;
				
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME"             , sRELATIONSHIP_NAME             );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"                    , sLHS_MODULE                    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"                     , sLHS_TABLE                     );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"                       , sLHS_KEY                       );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"                    , sRHS_MODULE                    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"                     , sRHS_TABLE                     );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"                       , sRHS_KEY                       );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_TABLE"                    , sJOIN_TABLE                    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_KEY_LHS"                  , sJOIN_KEY_LHS                  );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_KEY_RHS"                  , sJOIN_KEY_RHS                  );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE"             , "many-to-many"                 );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"                   , sMODULE_NAME                   );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"                  , sDISPLAY_NAME                  );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_ROLE_COLUMN"      , sRELATIONSHIP_ROLE_COLUMN      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_ROLE_COLUMN_VALUE", sRELATIONSHIP_ROLE_COLUMN_VALUE);
			}
			rdl.SetCustomProperty("RelatedModules", xmlRelationships.OuterXml.Replace("</Relationship>", "</Relationship>" + ControlChars.CrLf));

			DataTable dtModules = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
			DataView vwModules = new DataView(dtModules);
			vwModules.Sort = "DISPLAY_NAME";
			lstRELATED.DataSource = vwModules;
			lstRELATED.DataBind();
			lstRELATED.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
			
			lstFILTER_COLUMN_SOURCE_Bind();
		}

		private void lstFILTER_COLUMN_SOURCE_Bind()
		{
			// 07/13/2006 Paul.  Convert the module name to the correct table name. 
			string sModule = BaseModule;
			XmlDocument xmlRelationships = new XmlDocument();
			xmlRelationships.AppendChild(xmlRelationships.CreateElement("Relationships"));
			
			XmlNode xRelationship = null;

			if ( !Sql.IsEmptyString(lstRELATED.SelectedValue) )
			{
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);
				string sRELATED_MODULE    = lstRELATED.SelectedValue.Split(' ')[0];
				string sRELATED_ALIAS     = lstRELATED.SelectedValue.Split(' ')[1];
				// 10/26/2011 Paul.  Add the relationship so that we can have a unique lookup. 
				string sRELATIONSHIP_NAME = lstRELATED.SelectedValue.Split(' ')[2];
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sRELATIONSHIP_NAME);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sRELATED_MODULE   );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sRELATED_ALIAS    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", "many-to-many"    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sRELATED_ALIAS    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sRELATED_MODULE + " " + sRELATED_ALIAS);
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sRELATED_MODULE + " " + sRELATED_ALIAS + "] " + sRELATED_MODULE);
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sRELATED_MODULE);
			}
			else
			{
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);

				string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sMODULE_TABLE);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sMODULE_TABLE);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sModule + " " + sMODULE_TABLE);
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sModule + " " + sMODULE_TABLE + "] " + sModule);
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sModule);
			
				// 07/22/2008 Paul.  Add a relationship to the AUDIT table for the old/previous value. 
				// 06/25/2010 Paul.  Report Scheduling does not require the audit table. 
				/*
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);

				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sModule.ToLower() + "_audit_old");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sMODULE_TABLE + "_AUDIT");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , "ID"         );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , sMODULE_TABLE);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , "ID"         );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sMODULE_TABLE + "_AUDIT_OLD");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sModule + " " + sMODULE_TABLE + "_AUDIT_OLD");
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sModule + " " + sMODULE_TABLE + "_AUDIT_OLD" + "] " + sModule + ": Audit Old");
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sModule + ": Audit Old");
				*/

				// 08/12/2008 Paul.  Add a relationship to the USERS table.
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", "users_all");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , "Users");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , "USERS");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , "USERS_ALL");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , "Users USERS_ALL");
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[Users USERS_ALL] Users All");
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "Users All");

				if ( Crm.Config.enable_team_management() )
				{
					// 08/12/2008 Paul.  Add a relationship to the TEAMS table.
					xRelationship = xmlRelationships.CreateElement("Relationship");
					xmlRelationships.DocumentElement.AppendChild(xRelationship);
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", "teams");
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , "Teams");
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , "TEAMS");
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , "TEAMS");
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , "Teams TEAMS");
					if ( bDebug )
						XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[Teams TEAMS] Teams");
					else
						XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "Teams");
				}

				// 08/12/2008 Paul.  Add a relationship to the ACL_ROLES table.
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", "roles");
				// 03/22/2010 Paul.  The correct Roles module name is ACLRoles. 
				// We get a vw_List error if the wrong module name is used. 
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , "ACLRoles");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , "ACL_ROLES");
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , "ACL_ROLES");
				// 03/22/2010 Paul.  The correct Roles module name is ACLRoles. 
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , "ACLRoles ACL_ROLES");
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[Roles ACL_ROLES] Roles");
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "Roles");
			}
			rdl.SetCustomProperty("Relationships", xmlRelationships.OuterXml.Replace("</Relationship>", "</Relationship>" + ControlChars.CrLf));

			DataTable dtModuleColumnSource = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
			// 05/29/2006 Paul.  Filter column source is always the same as module column source. 
			lstFILTER_COLUMN_SOURCE.DataSource = dtModuleColumnSource;
			lstFILTER_COLUMN_SOURCE.DataBind();
			lblFILTER_COLUMN_SOURCE.Text = lstFILTER_COLUMN_SOURCE.SelectedValue;

			lstFILTER_COLUMN_Bind();
		}
		#endregion

		private void lstFILTER_COLUMN_Bind()
		{
			lstFILTER_COLUMN.DataSource = null;
			lstFILTER_COLUMN.DataBind();

			string[] arrModule   = lstFILTER_COLUMN_SOURCE.SelectedValue.Split(' ');
			string sModule       = arrModule[0];
			string sTableAlias   = arrModule[1];
			string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
			if ( sTableAlias == "USERS_ALL" || sTableAlias == "TEAMS" || sTableAlias == "ACL_ROLES" )
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sSQL;
					sSQL = "select ID   as NAME        " + ControlChars.CrLf
					     + "     , NAME as DISPLAY_NAME" + ControlChars.CrLf
					     + "  from vw" + sMODULE_TABLE + "_List" + ControlChars.CrLf
					     + " order by DISPLAY_NAME     " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								lstFILTER_COLUMN.DataSource = dt;
								lstFILTER_COLUMN.DataBind();
								lblFILTER_COLUMN.Text = lstFILTER_COLUMN.SelectedValue;
							}
						}
					}
				}
			}
			else
			{
				DataTable dtColumns = SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy();
				foreach(DataRow row in dtColumns.Rows)
				{
					row["NAME"        ] = sTableAlias + "." + Sql.ToString(row["NAME"]);
					// 07/04/2006 Paul.  Some columns have global terms. 
					row["DISPLAY_NAME"] = Utils.TableColumnName(L10n, sModule, Sql.ToString(row["DISPLAY_NAME"]));
				}
				ViewState["FILTER_COLUMNS"] = dtColumns;
				
				DataView vwColumns = new DataView(dtColumns);
				// 10/11/2008 Paul.  Allow sending an alert to records based on ID, PARENT_ID, ACCOUNT_ID or CONTACT_ID. 
				// 06/25/2010 Paul.  We will only be sending the report to the user and the team. 
				vwColumns.RowFilter = "NAME in ('" + sTableAlias + ".CREATED_BY_ID', '" + sTableAlias + ".MODIFIED_USER_ID', '" + sTableAlias + ".ASSIGNED_USER_ID', '" + sTableAlias + ".TEAM_ID')";
				lstFILTER_COLUMN.DataSource = vwColumns;
				lstFILTER_COLUMN.DataBind();
				lblFILTER_COLUMN.Text = lstFILTER_COLUMN.SelectedValue;
			}

			lblFILTER_OPERATOR_Bind();
		}

		private void lblFILTER_OPERATOR_Bind()
		{
			lstFILTER_OPERATOR.DataSource = null;
			lstFILTER_OPERATOR.Items.Clear();

			string[] arrModule = lstFILTER_COLUMN_SOURCE.SelectedValue.Split(' ');
			string sModule     = arrModule[0];
			string sTableAlias = arrModule[1];
			if ( sTableAlias == "USERS_ALL" || sTableAlias == "TEAMS" || sTableAlias == "ACL_ROLES" )
			{
				lstFILTER_OPERATOR.DataSource = SplendidCache.List("workflow_alert_operator_dom");
				lstFILTER_OPERATOR.DataBind();
				lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
			}
			else
			{
				string[] arrColumn = lstFILTER_COLUMN.SelectedValue.Split('.');
				string sColumnName = arrColumn[0];
				if ( arrColumn.Length > 1 )
					sColumnName = arrColumn[1];
				
				string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
				DataView vwColumns = new DataView(SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy());
				vwColumns.RowFilter = "ColumnName = '" + sColumnName + "'";
				
				if ( vwColumns.Count > 0 )
				{
					lstFILTER_OPERATOR.DataSource = SplendidCache.List("workflow_alert_operator_dom");
					lstFILTER_OPERATOR.DataBind();
					lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
				}
			}
			BindSearchText();
		}

		private void BindSearchText()
		{
		}

		private void BuildWorkflowXOML()
		{
			// 08/09/2008 Paul.  The primary location for the WORKFLOW_ID will be in the SplendidSequentialWorkflow
			xoml = new XomlDocument("Workflow1", gPARENT_ID);
			if ( xoml.DocumentElement != null )
			{
				try
				{
					string sMODULE_TABLE       = Sql.ToString(Application["Modules." + BaseModule + ".TableName"]);
					string sALERT_NAME         = txtNAME.Text;
					string sALERT_TYPE         = lstALERT_TYPE.SelectedValue;
					string sALERT_TEXT         = txtALERT_TEXT.Text;
					string sSOURCE_TYPE        = lstSOURCE_TYPE.SelectedValue;
					Guid   gCUSTOM_TEMPLATE_ID = (sSOURCE_TYPE == "custom template") ? Sql.ToGuid(lstCUSTOM_TEMPLATE.SelectedValue) : Guid.Empty;
					
					int nIndex = 1;
					// 07/13/2010 Paul.  The Report ID is stored in the Attachments custom property in the RDL. 
					// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
					Guid   gASSIGNED_USER_ID = Sql.ToGuid(ASSIGNED_USER_ID.Value);
					Guid   gTEAM_ID          = SplendidCRM.Crm.Config.enable_dynamic_teams() ? gTEAM_ID = ctlTeamSelect.TEAM_ID : Sql.ToGuid(TEAM_ID.Value);
					string sTEAM_SET_LIST    = ctlTeamSelect.TEAM_SET_LIST;
					WorkflowBuilder.BuildAlertXOML(xoml, null, rdl, "time", BaseModule, sMODULE_TABLE, sALERT_NAME, sALERT_TYPE, sALERT_TEXT, sSOURCE_TYPE, gCUSTOM_TEMPLATE_ID, nIndex, gASSIGNED_USER_ID, gTEAM_ID, sTEAM_SET_LIST);
				}
				catch(Exception ex)
				{
					ctlDynamicButtons.ErrorText = ex.Message;
				}

				try
				{
					Activity rootActivity = null;
					using ( StringReader stm = new StringReader(xoml.OuterXml) )
					{
						using ( XmlReader rdr = XmlReader.Create(stm) )
						{
							WorkflowMarkupSerializer xomlSerializer = new WorkflowMarkupSerializer();
							rootActivity = xomlSerializer.Deserialize(rdr) as Activity;
						}
					}
					WorkflowRuntime runtime = WorkflowInit.GetRuntime(Application);
					Dictionary<string, object> dictParameters = new Dictionary<string, object>();
					//Guid gAUDIT_ID = new Guid("6BC0188C-4F1D-42FE-A303-002B1BC123CE");
					//dictParameters.Add("AUDIT_ID", gAUDIT_ID);
					WorkflowInstance inst = null;
					using ( StringReader stm = new StringReader(xoml.OuterXml) )
					{
						using ( XmlReader rdr = XmlReader.Create(stm) )
						{
							inst = runtime.CreateWorkflow(rdr, null, dictParameters);
						}
					}
				}
				catch(WorkflowValidationFailedException ex)
				{
					StringBuilder sb = new StringBuilder();
					foreach ( ValidationError error in ex.Errors )
					{
						sb.AppendLine(error.ToString() + "<br />");
					}
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), sb.ToString());
					ctlDynamicButtons.ErrorText = sb.ToString();
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = "BuildWorkflowXOML: " + ex.Message;
				}
			}
		}

		private DataTable ReportColumnSource()
		{
			DataTable dtColumnSource = new DataTable();
			XmlDocument xmlRelationships = rdl.GetCustomProperty("Relationships");
			dtColumnSource = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
			return dtColumnSource;
		}

		#region Filter Editing
		private DataTable ReportFilters()
		{
			DataTable dtFilters = new DataTable();
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			dtFilters = XmlUtil.CreateDataTable(xmlFilters.DocumentElement, "Filter", new string[] {"ID", "ACTION_TYPE", "RELATIONSHIP_NAME", "MODULE", "MODULE_NAME", "TABLE_NAME", "DATA_FIELD", "FIELD_NAME", "DATA_TYPE", "OPERATOR", "RECIPIENT_NAME", "SEARCH_TEXT"});
			return dtFilters;
		}

		protected void FiltersGet(string sID, ref string sACTION_TYPE, ref string sRELATIONSHIP_NAME, ref string sMODULE_NAME, ref string sDATA_FIELD, ref string sDATA_TYPE, ref string sOPERATOR, ref string sRECIPIENT_NAME, ref string sSEARCH_TEXT)
		{
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
			if ( xFilter != null )
			{
				sACTION_TYPE       = XmlUtil.SelectSingleNode(xFilter, "ACTION_TYPE"      );
				sRELATIONSHIP_NAME = XmlUtil.SelectSingleNode(xFilter, "RELATIONSHIP_NAME");
				sMODULE_NAME       = XmlUtil.SelectSingleNode(xFilter, "MODULE_NAME"      );
				sDATA_FIELD        = XmlUtil.SelectSingleNode(xFilter, "DATA_FIELD"       );
				sDATA_TYPE         = XmlUtil.SelectSingleNode(xFilter, "DATA_TYPE"        );
				sOPERATOR          = XmlUtil.SelectSingleNode(xFilter, "OPERATOR"         );
				sSEARCH_TEXT       = XmlUtil.SelectSingleNode(xFilter, "SEARCH_TEXT"      );
				sRECIPIENT_NAME    = XmlUtil.SelectSingleNode(xFilter, "RECIPIENT_NAME"   );
			}
		}

		protected void FiltersUpdate(string sID, string sACTION_TYPE, string sRELATIONSHIP_NAME, string sMODULE_NAME, string sDATA_FIELD, string sDATA_TYPE, string sOPERATOR, string sRECIPIENT_NAME, string sSEARCH_TEXT)
		{
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			try
			{
				XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
				if ( xFilter == null || Sql.IsEmptyString(sID) )
				{
					xFilter = xmlFilters.CreateElement("Filter");
					xmlFilters.DocumentElement.AppendChild(xFilter);
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ID", Guid.NewGuid().ToString());
				}
				else
				{
					// 06/12/2006 Paul.  The easiest way to remove the old text values is to delete them all. 
					xFilter.RemoveAll();
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ID", sID);
				}
				string[] arrModule   = sMODULE_NAME.Split(' ');
				string sModule       = arrModule[0];
				string sTableAlias   = arrModule[1];
				if ( sTableAlias == "USERS_ALL" || sTableAlias == "TEAMS" || sTableAlias == "ACL_ROLES" )
				{
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ACTION_TYPE"      , sACTION_TYPE      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "RELATIONSHIP_NAME", sRELATIONSHIP_NAME);
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE"           , sModule           );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE_NAME"      , sMODULE_NAME      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "TABLE_NAME"       , String.Empty      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_FIELD"       , String.Empty      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "FIELD_NAME"       , String.Empty      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_TYPE"        , sDATA_TYPE        );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "OPERATOR"         , sOPERATOR         );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "SEARCH_TEXT"      , sSEARCH_TEXT      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "RECIPIENT_NAME"   , sRECIPIENT_NAME   );
				}
				else
				{
					string[] arrDATA_FIELD = sDATA_FIELD.Split('.');
					string sTABLE_NAME = arrDATA_FIELD[0];
					string sFIELD_NAME = arrDATA_FIELD[1];
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ACTION_TYPE"      , sACTION_TYPE      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "RELATIONSHIP_NAME", sRELATIONSHIP_NAME);
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE"           , sModule           );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE_NAME"      , sMODULE_NAME      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "TABLE_NAME"       , sTABLE_NAME       );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_FIELD"       , sDATA_FIELD       );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "FIELD_NAME"       , sFIELD_NAME       );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_TYPE"        , sDATA_TYPE        );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "OPERATOR"         , sOPERATOR         );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "SEARCH_TEXT"      , String.Empty      );
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "RECIPIENT_NAME"   , String.Empty      );
				}
				rdl.SetCustomProperty("Filters", xmlFilters.OuterXml);
				
				dgFilters.DataSource = ReportFilters();
				dgFilters.DataBind();
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void FiltersDelete(string sID)
		{
			dgFilters.EditItemIndex = -1;
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
			if ( xFilter != null )
			{
				xFilter.ParentNode.RemoveChild(xFilter);
				rdl.SetCustomProperty("Filters", xmlFilters.OuterXml);
			}
			dgFilters.DataSource = ReportFilters();
			dgFilters.DataBind();
		}
		#endregion

		#region Attachment Editing
		// 07/13/2010 Paul.  We are going to store the ReportAttachments in the RDL the same way the we store and process Filters. 
		private DataTable ReportAttachments()
		{
			DataTable dtReportAttachments = new DataTable();
			XmlDocument xmlReportAttachments = rdl.GetCustomProperty("ReportAttachments");
			// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
			// 10/28/2014 Paul.  Allow report render format to be specified. 
			dtReportAttachments = XmlUtil.CreateDataTable(xmlReportAttachments.DocumentElement, "Report", new string[] {"ID", "REPORT_ID", "SCHEDULED_USER_ID", "REPORT_NAME", "REPORT_PARAMETERS", "RENDER_FORMAT"});
			return dtReportAttachments;
		}

		// 10/28/2014 Paul.  Allow report render format to be specified. 
		protected void ReportAttachmentsGet(string sID, ref Guid gREPORT_ID, ref Guid gSCHEDULED_USER_ID, ref string sREPORT_NAME, ref string sREPORT_PARAMETERS, ref string sRENDER_FORMAT)
		{
			XmlDocument xmlReportAttachments = rdl.GetCustomProperty("ReportAttachments");
			XmlNode xReport = xmlReportAttachments.DocumentElement.SelectSingleNode("Report[ID=\'" + sID + "\']");
			if ( xReport != null )
			{
				gREPORT_ID         = Sql.ToGuid(XmlUtil.SelectSingleNode(xReport, "REPORT_ID"        ));
				gSCHEDULED_USER_ID = Sql.ToGuid(XmlUtil.SelectSingleNode(xReport, "SCHEDULED_USER_ID"));
				sREPORT_NAME       =            XmlUtil.SelectSingleNode(xReport, "REPORT_NAME"      );
				sREPORT_PARAMETERS =            XmlUtil.SelectSingleNode(xReport, "REPORT_PARAMETERS");
				sRENDER_FORMAT     =            XmlUtil.SelectSingleNode(xReport, "RENDER_FORMAT"    );
				if ( Sql.IsEmptyString(sRENDER_FORMAT) )
					sRENDER_FORMAT = "PDF";
			}
		}

		// 10/28/2014 Paul.  Allow report render format to be specified. 
		protected void ReportAttachmentsUpdate(string sID, Guid gREPORT_ID, Guid gSCHEDULED_USER_ID, string sREPORT_NAME, string sREPORT_PARAMETERS, string sRENDER_FORMAT)
		{
			XmlDocument xmlReportAttachments = rdl.GetCustomProperty("ReportAttachments");
			try
			{
				XmlNode xReport = xmlReportAttachments.DocumentElement.SelectSingleNode("Report[ID=\'" + sID + "\']");
				if ( xReport == null )
				{
					xReport = xmlReportAttachments.CreateElement("Report");
					xmlReportAttachments.DocumentElement.AppendChild(xReport);
					XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "ID", Guid.NewGuid().ToString());
				}
				else
				{
					xReport.RemoveAll();
					XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "ID", sID);
				}
				XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "REPORT_ID"        , gREPORT_ID.ToString());
				// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
				XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "SCHEDULED_USER_ID", gSCHEDULED_USER_ID.ToString());
				XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "REPORT_NAME"      , sREPORT_NAME         );
				XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "REPORT_PARAMETERS", sREPORT_PARAMETERS   );
				XmlUtil.SetSingleNode(xmlReportAttachments, xReport, "RENDER_FORMAT"    , sRENDER_FORMAT       );
				rdl.SetCustomProperty("ReportAttachments", xmlReportAttachments.OuterXml);
				
				dgReportAttachments.DataSource = ReportAttachments();
				dgReportAttachments.DataBind();
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void ReportAttachmentsDelete(string sID)
		{
			dgReportAttachments.EditItemIndex = -1;
			XmlDocument xmlReportAttachments = rdl.GetCustomProperty("ReportAttachments");
			XmlNode xReport = xmlReportAttachments.DocumentElement.SelectSingleNode("Report[ID=\'" + sID + "\']");
			if ( xReport != null )
			{
				xReport.ParentNode.RemoveChild(xReport);
				rdl.SetCustomProperty("ReportAttachments", xmlReportAttachments.OuterXml);
			}
			dgReportAttachments.DataSource = ReportAttachments();
			dgReportAttachments.DataBind();
		}
		#endregion

		protected void lstSOURCE_TYPE_Changed(object sender, System.EventArgs e)
		{
			trCUSTOM_TEMPLATE.Visible = (lstSOURCE_TYPE.SelectedValue == "custom template");
			trALERT_TEXT     .Visible = !trCUSTOM_TEMPLATE.Visible;
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("Reports.LBL_LIST_SCHEDULE_REPORT"));
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.GetUserAccess("Reports", "edit") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			rdl = new RdlDocument();
			xoml = new XomlDocument();
			lblRELATED             .Visible = bDebug;
			lblFILTER_COLUMN_SOURCE.Visible = bDebug;
			lblFILTER_COLUMN       .Visible = bDebug;
			lblFILTER_OPERATOR_TYPE.Visible = bDebug;
			lblFILTER_OPERATOR     .Visible = bDebug;
			try
			{
				reqNAME.DataBind();
				gREPORT_ID = Sql.ToGuid(Request["REPORT_ID"]);
				if ( !IsPostBack )
				{
					lstALERT_TYPE .DataSource = SplendidCache.List("workflow_alert_type_dom");
					lstALERT_TYPE .DataBind();
					lstSOURCE_TYPE.DataSource = SplendidCache.List("workflow_source_type_dom");
					lstSOURCE_TYPE.DataBind();
					lstSOURCE_TYPE_Changed(null, null);
					// 10/28/2014 Paul.  Allow report render format to be specified. 
					lstRENDER_FORMAT.DataSource = SplendidCache.List("report_render_format");
					lstRENDER_FORMAT.DataBind();
					// 10/28/2014 Paul.  Use terms in the header. 
					foreach(DataGridColumn col in dgReportAttachments.Columns)
					{
						col.HeaderText = L10n.Term(col.HeaderText);
					}
					foreach(DataGridColumn col in dgFilters.Columns)
					{
						col.HeaderText = L10n.Term(col.HeaderText);
					}
					STATUS     .DataSource = SplendidCache.List("workflow_status_dom");
					STATUS     .DataBind();

					// 07/13/2006 Paul.  We don't store the SHOW_QUERY value in the RDL, so we must retrieve it from the session. 
					chkSHOW_XOML.Checked = Sql.ToBoolean(Session["Workflows.SHOW_XOML"]);
#if DEBUG
					chkSHOW_XOML.Checked = true;
#endif
					ctlDynamicButtons.AppendButtons(m_sMODULE + ".ScheduleView", Guid.Empty, null);
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						// 06/26/2010 Paul.  Use the report name in the subject of the alert. 
						sSQL = "select NAME     " + ControlChars.CrLf
						     + "  from vwREPORTS" + ControlChars.CrLf
						     + " where ID = @ID" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gREPORT_ID);
							string sREPORT_NAME = Sql.ToString(cmd.ExecuteScalar());
							ViewState["REPORT_NAME"] = sREPORT_NAME;
							txtNAME.Text = String.Format(L10n.Term("Reports.LBL_SCHEDULED_REPORT"), sREPORT_NAME);
							// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
							ctlDynamicButtons.Title = txtNAME.Text;
							ViewState["ctlDynamicButtons.Title"] = txtNAME.Text;
							SetPageTitle(L10n.Term("Reports.LBL_LIST_SCHEDULE_REPORT") + " - " + sREPORT_NAME);
						}
						sSQL = "select ID                        " + ControlChars.CrLf
						     + "     , NAME                      " + ControlChars.CrLf
						     + "  from vwWORKFLOW_ALERT_TEMPLATES" + ControlChars.CrLf
						     + " order by NAME                   " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									lstCUSTOM_TEMPLATE.DataSource = dt ;
									lstCUSTOM_TEMPLATE.DataBind();
									lstCUSTOM_TEMPLATE.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
								}
							}
						}
						// 06/23/2010 Paul.  Look for a workflow for this report. 
						// 11/16/2008 Paul.  Time-based workflows reference the base module, not the audit table. 
						sSQL = "select ID                    " + ControlChars.CrLf
						     + "     , STATUS                " + ControlChars.CrLf
						     + "     , JOB_INTERVAL          " + ControlChars.CrLf
						     + "  from vwWORKFLOWS           " + ControlChars.CrLf
						     + " where PARENT_ID = @PARENT_ID" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@PARENT_ID", gREPORT_ID);
							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									try
									{
										// 08/19/2010 Paul.  Check the list before assigning the value. 
										Utils.SetSelectedValue(STATUS, Sql.ToString(rdr["STATUS"]));
									}
									catch
									{
									}
									gPARENT_ID           = Sql.ToGuid  (rdr["ID"         ]);
									string sJOB_INTERVAL = Sql.ToString(rdr["JOB_INTERVAL"]);
									// 07/15/2010 Paul.  Use new CRON control. 
									ctlCRON.Value = sJOB_INTERVAL;
									ViewState["PARENT_ID"] = gPARENT_ID;
								}
								else
								{
									ctlDynamicButtons.ShowButton("Delete", false);
								}
							}
						}
						if ( !Sql.IsEmptyGuid(gPARENT_ID) )
						{
							sSQL = "select *                           " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ALERT_SHELLS_Edit" + ControlChars.CrLf
							     + " where PARENT_ID = @PARENT_ID      " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@PARENT_ID", gPARENT_ID);

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									if ( rdr.Read() )
									{
										//ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
										//SetPageTitle(L10n.Term("WorkflowAlertShells.LBL_NAME") + " - " + ctlDynamicButtons.Title);
										//ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
										
										txtNAME      .Text = Sql.ToString(rdr["NAME"      ]);
										txtALERT_TEXT.Text = Sql.ToString(rdr["ALERT_TEXT"]);
										gID = Sql.ToGuid(rdr["ID"]);
										ViewState["ID"] = gID;
										try
										{
											// 08/19/2010 Paul.  Check the list before assigning the value. 
											Utils.SetSelectedValue(lstALERT_TYPE , Sql.ToString(rdr["ALERT_TYPE"]));
										}
										catch
										{
										}
										try
										{
											// 08/19/2010 Paul.  Check the list before assigning the value. 
											Utils.SetSelectedValue(lstSOURCE_TYPE, Sql.ToString(rdr["SOURCE_TYPE"]));
										}
										catch
										{
										}
										try
										{
											// 08/19/2010 Paul.  Check the list before assigning the value. 
											Utils.SetValue(lstCUSTOM_TEMPLATE, Sql.ToString(rdr["CUSTOM_TEMPLATE_ID"]));
										}
										catch
										{
										}
										lstSOURCE_TYPE_Changed(null, null);
										string sRDL = Sql.ToString(rdr["RDL"]);
										try
										{
											if ( !Sql.IsEmptyString(sRDL) )
											{
												rdl.LoadRdl(sRDL);
											}
										}
										catch
										{
										}
										string sXOML = Sql.ToString(rdr["XOML"]);
										try
										{
											if ( !Sql.IsEmptyString(sXOML) )
											{
												xoml.LoadXoml(sXOML);
											}
										}
										catch
										{
										}
										// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
										ASSIGNED_TO     .Text  = Sql.ToString(rdr["ASSIGNED_TO"     ]);
										ASSIGNED_USER_ID.Value = Sql.ToString(rdr["ASSIGNED_USER_ID"]);
										TEAM_NAME       .Text  = Sql.ToString(rdr["TEAM_NAME"       ]);
										TEAM_ID         .Value = Sql.ToString(rdr["TEAM_ID"         ]);
										Guid gTEAM_SET_ID = Sql.ToGuid(rdr["TEAM_SET_ID"]);
										ctlTeamSelect.LoadLineItems(gTEAM_SET_ID, false);
									}
									else
									{
										// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
										ctlTeamSelect.LoadLineItems(Guid.Empty, false);
									}
								}
							}
						}
						else
						{
							// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
							ctlTeamSelect.LoadLineItems(Guid.Empty, false);
						}
					}
					// 05/27/2006 Paul.  This is a catch-all statement to create a new report if all else fails. 
					if ( rdl.DocumentElement == null )
					{
						// 07/22/2008 Paul.  Use a version of the RDL constructor that adds a minimum of tags. 
						rdl = new RdlDocument(String.Empty);
						rdl.SetCustomProperty("Module" , BaseModule  );
						rdl.SetCustomProperty("Related", lstRELATED.SelectedValue );
					}
					XmlDocument xmlReportAttachments = rdl.GetCustomProperty("ReportAttachments");
					XmlNode xReport = xmlReportAttachments.DocumentElement.SelectSingleNode("Report[REPORT_ID=\'" + gREPORT_ID.ToString() + "\']");
					if ( xReport == null )
					{
						// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
						Guid gSCHEDULED_USER_ID = Security.USER_ID;
						ReportAttachmentsUpdate(String.Empty, gREPORT_ID, gSCHEDULED_USER_ID, Sql.ToString(ViewState["REPORT_NAME"]), String.Empty, "PDF");
					}

					if ( xoml.DocumentElement == null )
					{
						xoml = new XomlDocument("Workflow1", gPARENT_ID);
					}
					lstRELATED_Bind();
					lblRELATED.Text = lstRELATED.SelectedValue;
					try
					{
						// 08/19/2010 Paul.  Check the list before assigning the value. 
						Utils.SetSelectedValue(lstRELATED, rdl.GetCustomPropertyValue("Related"));
					}
					catch
					{
					}
					// 07/26/2007 Paul.  The column sources need to be updated after the related has changed. 
					lstFILTER_COLUMN_SOURCE_Bind();
					BuildWorkflowXOML();

					dgFilters.DataSource = ReportFilters();
					dgFilters.DataBind();

					dgReportAttachments.DataSource = ReportAttachments();
					dgReportAttachments.DataBind();
				}
				else
				{
					gID        = Sql.ToGuid(ViewState["ID"       ]);
					gPARENT_ID = Sql.ToGuid(ViewState["PARENT_ID"]);
					// 07/13/2006 Paul.  Save the SHOW_QUERY flag in the Session so that it will be available across redirects. 
					Session["Workflows.SHOW_XOML"] = chkSHOW_XOML.Checked;

					string sRDL = Sql.ToString(ViewState["rdl"]);
					rdl.LoadRdl(sRDL);
					string sXOML = Sql.ToString(ViewState["xoml"]);
					xoml.LoadXoml(sXOML);

					BuildWorkflowXOML();

					dgFilters.DataSource = ReportFilters();
					dgFilters.DataBind();

					dgReportAttachments.DataSource = ReportAttachments();
					dgReportAttachments.DataBind();

					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term("Reports.LBL_LIST_SCHEDULE_REPORT") + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_PreRender(object sender, System.EventArgs e)
		{
			if ( chkSHOW_XOML.Checked )
			{
				// 07/15/2010 Paul.  Use new function to format Xoml. 
				litWORKFLOW_XML.Text = XomlUtils.XomlEncode(xoml.OuterXml);
			}
			else
			{
				// 06/27/2010 Paul.  Make sure to remove the text when disabled. 
				litWORKFLOW_XML.Text = String.Empty;
			}
			if ( bDebug )
			{
				// 07/15/2010 Paul.  Use new function to format Rdl. 
				litREPORT_XML.Text = RdlUtil.RdlEncode(rdl);
			}
			ViewState["rdl"] = rdl.OuterXml;
			ViewState["xoml"] = xoml.OuterXml;
			Session["xoml"] = xoml.OuterXml;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new System.EventHandler(this.Page_PreRender);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Reports";
			// 05/06/2010 Paul.  The menu will show the admin Module Name in the Six theme. 
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				ctlDynamicButtons.AppendButtons(m_sMODULE + ".ScheduleView", Guid.Empty, null);
			}
		}
		#endregion
	}
}
