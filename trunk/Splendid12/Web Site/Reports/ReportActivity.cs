/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ReportActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class ReportActivity: SplendidActivity
	{
		public ReportActivity()
		{
			this.Name = "ReportActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.IDProperty))); }
			set { base.SetValue(ReportActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ReportActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.NAMEProperty))); }
			set { base.SetValue(ReportActivity.NAMEProperty, value); }
		}

		public static DependencyProperty MODULE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODULE_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODULE_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.MODULE_NAMEProperty))); }
			set { base.SetValue(ReportActivity.MODULE_NAMEProperty, value); }
		}

		public static DependencyProperty REPORT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORT_TYPE", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPORT_TYPE
		{
			get { return ((string)(base.GetValue(ReportActivity.REPORT_TYPEProperty))); }
			set { base.SetValue(ReportActivity.REPORT_TYPEProperty, value); }
		}

		public static DependencyProperty RDLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RDL", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RDL
		{
			get { return ((string)(base.GetValue(ReportActivity.RDLProperty))); }
			set { base.SetValue(ReportActivity.RDLProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.TEAM_IDProperty))); }
			set { base.SetValue(ReportActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ReportActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ReportActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty PRE_LOAD_EVENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRE_LOAD_EVENT_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PRE_LOAD_EVENT_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.PRE_LOAD_EVENT_IDProperty))); }
			set { base.SetValue(ReportActivity.PRE_LOAD_EVENT_IDProperty, value); }
		}

		public static DependencyProperty POST_LOAD_EVENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("POST_LOAD_EVENT_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid POST_LOAD_EVENT_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.POST_LOAD_EVENT_IDProperty))); }
			set { base.SetValue(ReportActivity.POST_LOAD_EVENT_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(ReportActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(ReportActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(ReportActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ReportActivity.CREATED_BYProperty))); }
			set { base.SetValue(ReportActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ReportActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ReportActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ReportActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ReportActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ReportActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ReportActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty PUBLISHEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PUBLISHED", typeof(bool), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool PUBLISHED
		{
			get { return ((bool)(base.GetValue(ReportActivity.PUBLISHEDProperty))); }
			set { base.SetValue(ReportActivity.PUBLISHEDProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ReportActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ReportActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ReportActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ReportActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(ReportActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ReportActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ReportActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty POST_LOAD_EVENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("POST_LOAD_EVENT_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string POST_LOAD_EVENT_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.POST_LOAD_EVENT_NAMEProperty))); }
			set { base.SetValue(ReportActivity.POST_LOAD_EVENT_NAMEProperty, value); }
		}

		public static DependencyProperty PRE_LOAD_EVENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRE_LOAD_EVENT_NAME", typeof(string), typeof(ReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRE_LOAD_EVENT_NAME
		{
			get { return ((string)(base.GetValue(ReportActivity.PRE_LOAD_EVENT_NAMEProperty))); }
			set { base.SetValue(ReportActivity.PRE_LOAD_EVENT_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ReportActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ReportActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select REPORTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwREPORTS_AUDIT        REPORTS          " + ControlChars.CrLf
							                + " inner join vwREPORTS_AUDIT        REPORTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on REPORTS_AUDIT_OLD.ID = REPORTS.ID       " + ControlChars.CrLf
							                + "        and REPORTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwREPORTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                 from vwREPORTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                where vwREPORTS_AUDIT.ID            =  REPORTS.ID           " + ControlChars.CrLf
							                + "                                                  and vwREPORTS_AUDIT.AUDIT_VERSION <  REPORTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                  and vwREPORTS_AUDIT.AUDIT_TOKEN   <> REPORTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                              )" + ControlChars.CrLf
							                + " where REPORTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *             " + ControlChars.CrLf
							                + "  from vwREPORTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwREPORTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *             " + ControlChars.CrLf
							                + "  from vwREPORTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								MODULE_NAME                    = Sql.ToString  (rdr["MODULE_NAME"                   ]);
								REPORT_TYPE                    = Sql.ToString  (rdr["REPORT_TYPE"                   ]);
								RDL                            = Sql.ToString  (rdr["RDL"                           ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								PRE_LOAD_EVENT_ID              = Sql.ToGuid    (rdr["PRE_LOAD_EVENT_ID"             ]);
								POST_LOAD_EVENT_ID             = Sql.ToGuid    (rdr["POST_LOAD_EVENT_ID"            ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								PUBLISHED                      = Sql.ToBoolean (rdr["PUBLISHED"                     ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									POST_LOAD_EVENT_NAME           = Sql.ToString  (rdr["POST_LOAD_EVENT_NAME"          ]);
									PRE_LOAD_EVENT_NAME            = Sql.ToString  (rdr["PRE_LOAD_EVENT_NAME"           ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ReportActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("REPORTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spREPORTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, MODULE_NAME
								, REPORT_TYPE
								, RDL
								, TEAM_ID
								, TEAM_SET_LIST
								, PRE_LOAD_EVENT_ID
								, POST_LOAD_EVENT_ID
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ReportActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

