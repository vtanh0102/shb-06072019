/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// PaymentActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class PaymentActivity: SplendidActivity
	{
		public PaymentActivity()
		{
			this.Name = "PaymentActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.IDProperty))); }
			set { base.SetValue(PaymentActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(PaymentActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(PaymentActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty PAYMENT_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_DATE", typeof(DateTime), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime PAYMENT_DATE
		{
			get { return ((DateTime)(base.GetValue(PaymentActivity.PAYMENT_DATEProperty))); }
			set { base.SetValue(PaymentActivity.PAYMENT_DATEProperty, value); }
		}

		public static DependencyProperty PAYMENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_TYPE", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PAYMENT_TYPE
		{
			get { return ((string)(base.GetValue(PaymentActivity.PAYMENT_TYPEProperty))); }
			set { base.SetValue(PaymentActivity.PAYMENT_TYPEProperty, value); }
		}

		public static DependencyProperty CUSTOMER_REFERENCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CUSTOMER_REFERENCE", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CUSTOMER_REFERENCE
		{
			get { return ((string)(base.GetValue(PaymentActivity.CUSTOMER_REFERENCEProperty))); }
			set { base.SetValue(PaymentActivity.CUSTOMER_REFERENCEProperty, value); }
		}

		public static DependencyProperty EXCHANGE_RATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_RATE", typeof(float), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float EXCHANGE_RATE
		{
			get { return ((float)(base.GetValue(PaymentActivity.EXCHANGE_RATEProperty))); }
			set { base.SetValue(PaymentActivity.EXCHANGE_RATEProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(PaymentActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty AMOUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AMOUNT", typeof(decimal), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal AMOUNT
		{
			get { return ((decimal)(base.GetValue(PaymentActivity.AMOUNTProperty))); }
			set { base.SetValue(PaymentActivity.AMOUNTProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(PaymentActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(PaymentActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty CREDIT_CARD_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREDIT_CARD_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREDIT_CARD_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.CREDIT_CARD_IDProperty))); }
			set { base.SetValue(PaymentActivity.CREDIT_CARD_IDProperty, value); }
		}

		public static DependencyProperty PAYMENT_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_NUM", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PAYMENT_NUM
		{
			get { return ((string)(base.GetValue(PaymentActivity.PAYMENT_NUMProperty))); }
			set { base.SetValue(PaymentActivity.PAYMENT_NUMProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.TEAM_IDProperty))); }
			set { base.SetValue(PaymentActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(PaymentActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(PaymentActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty BANK_FEEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BANK_FEE", typeof(decimal), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal BANK_FEE
		{
			get { return ((decimal)(base.GetValue(PaymentActivity.BANK_FEEProperty))); }
			set { base.SetValue(PaymentActivity.BANK_FEEProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.B2C_CONTACT_IDProperty))); }
			set { base.SetValue(PaymentActivity.B2C_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(PaymentActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty AMOUNT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AMOUNT_USDOLLAR", typeof(decimal), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal AMOUNT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(PaymentActivity.AMOUNT_USDOLLARProperty))); }
			set { base.SetValue(PaymentActivity.AMOUNT_USDOLLARProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(PaymentActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty BANK_FEE_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BANK_FEE_USDOLLAR", typeof(decimal), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal BANK_FEE_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(PaymentActivity.BANK_FEE_USDOLLARProperty))); }
			set { base.SetValue(PaymentActivity.BANK_FEE_USDOLLARProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(PaymentActivity.CREATED_BYProperty))); }
			set { base.SetValue(PaymentActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(PaymentActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(PaymentActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(PaymentActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(PaymentActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(PaymentActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(PaymentActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(PaymentActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(PaymentActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(PaymentActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(PaymentActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(PaymentActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(PaymentActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(PaymentActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(PaymentActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.B2C_CONTACT_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.B2C_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty CREDIT_CARD_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREDIT_CARD_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREDIT_CARD_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.CREDIT_CARD_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.CREDIT_CARD_NAMEProperty, value); }
		}

		public static DependencyProperty CREDIT_CARD_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREDIT_CARD_NUMBER", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREDIT_CARD_NUMBER
		{
			get { return ((string)(base.GetValue(PaymentActivity.CREDIT_CARD_NUMBERProperty))); }
			set { base.SetValue(PaymentActivity.CREDIT_CARD_NUMBERProperty, value); }
		}

		public static DependencyProperty CURRENCY_ISO4217Property = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ISO4217", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_ISO4217
		{
			get { return ((string)(base.GetValue(PaymentActivity.CURRENCY_ISO4217Property))); }
			set { base.SetValue(PaymentActivity.CURRENCY_ISO4217Property, value); }
		}

		public static DependencyProperty CURRENCY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.CURRENCY_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.CURRENCY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_SYMBOL", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_SYMBOL
		{
			get { return ((string)(base.GetValue(PaymentActivity.CURRENCY_SYMBOLProperty))); }
			set { base.SetValue(PaymentActivity.CURRENCY_SYMBOLProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.NAMEProperty))); }
			set { base.SetValue(PaymentActivity.NAMEProperty, value); }
		}

		public static DependencyProperty PAYMENT_TYPE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_TYPE_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PAYMENT_TYPE_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.PAYMENT_TYPE_IDProperty))); }
			set { base.SetValue(PaymentActivity.PAYMENT_TYPE_IDProperty, value); }
		}

		public static DependencyProperty PAYMENT_TYPE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_TYPE_NAME", typeof(string), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PAYMENT_TYPE_NAME
		{
			get { return ((string)(base.GetValue(PaymentActivity.PAYMENT_TYPE_NAMEProperty))); }
			set { base.SetValue(PaymentActivity.PAYMENT_TYPE_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(PaymentActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(PaymentActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty TOTAL_ALLOCATED_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_ALLOCATED_USDOLLAR", typeof(decimal), typeof(PaymentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TOTAL_ALLOCATED_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(PaymentActivity.TOTAL_ALLOCATED_USDOLLARProperty))); }
			set { base.SetValue(PaymentActivity.TOTAL_ALLOCATED_USDOLLARProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("PaymentActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("PaymentActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select PAYMENTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwPAYMENTS_AUDIT        PAYMENTS          " + ControlChars.CrLf
							                + " inner join vwPAYMENTS_AUDIT        PAYMENTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on PAYMENTS_AUDIT_OLD.ID = PAYMENTS.ID       " + ControlChars.CrLf
							                + "        and PAYMENTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwPAYMENTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                  from vwPAYMENTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                 where vwPAYMENTS_AUDIT.ID            =  PAYMENTS.ID           " + ControlChars.CrLf
							                + "                                                   and vwPAYMENTS_AUDIT.AUDIT_VERSION <  PAYMENTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                   and vwPAYMENTS_AUDIT.AUDIT_TOKEN   <> PAYMENTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                               )" + ControlChars.CrLf
							                + " where PAYMENTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwPAYMENTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwPAYMENTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwPAYMENTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								PAYMENT_DATE                   = Sql.ToDateTime(rdr["PAYMENT_DATE"                  ]);
								PAYMENT_TYPE                   = Sql.ToString  (rdr["PAYMENT_TYPE"                  ]);
								CUSTOMER_REFERENCE             = Sql.ToString  (rdr["CUSTOMER_REFERENCE"            ]);
								EXCHANGE_RATE                  = Sql.ToFloat   (rdr["EXCHANGE_RATE"                 ]);
								CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								AMOUNT                         = Sql.ToDecimal (rdr["AMOUNT"                        ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								CREDIT_CARD_ID                 = Sql.ToGuid    (rdr["CREDIT_CARD_ID"                ]);
								PAYMENT_NUM                    = Sql.ToString  (rdr["PAYMENT_NUM"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								BANK_FEE                       = Sql.ToDecimal (rdr["BANK_FEE"                      ]);
								B2C_CONTACT_ID                 = Sql.ToGuid    (rdr["B2C_CONTACT_ID"                ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								AMOUNT_USDOLLAR                = Sql.ToDecimal (rdr["AMOUNT_USDOLLAR"               ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								BANK_FEE_USDOLLAR              = Sql.ToDecimal (rdr["BANK_FEE_USDOLLAR"             ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"       ]);
									ACCOUNT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"      ]);
									ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									B2C_CONTACT_ASSIGNED_SET_ID    = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_SET_ID"   ]);
									B2C_CONTACT_ASSIGNED_USER_ID   = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_USER_ID"  ]);
									B2C_CONTACT_NAME               = Sql.ToString  (rdr["B2C_CONTACT_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									CREDIT_CARD_NAME               = Sql.ToString  (rdr["CREDIT_CARD_NAME"              ]);
									CREDIT_CARD_NUMBER             = Sql.ToString  (rdr["CREDIT_CARD_NUMBER"            ]);
									CURRENCY_ISO4217               = Sql.ToString  (rdr["CURRENCY_ISO4217"              ]);
									CURRENCY_NAME                  = Sql.ToString  (rdr["CURRENCY_NAME"                 ]);
									CURRENCY_SYMBOL                = Sql.ToString  (rdr["CURRENCY_SYMBOL"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									NAME                           = Sql.ToString  (rdr["NAME"                          ]);
									PAYMENT_TYPE_ID                = Sql.ToGuid    (rdr["PAYMENT_TYPE_ID"               ]);
									PAYMENT_TYPE_NAME              = Sql.ToString  (rdr["PAYMENT_TYPE_NAME"             ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									TOTAL_ALLOCATED_USDOLLAR       = Sql.ToDecimal (rdr["TOTAL_ALLOCATED_USDOLLAR"      ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("PaymentActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("PAYMENTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spPAYMENTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, ACCOUNT_ID
								, PAYMENT_DATE
								, PAYMENT_TYPE
								, CUSTOMER_REFERENCE
								, EXCHANGE_RATE
								, CURRENCY_ID
								, AMOUNT
								, DESCRIPTION
								, CREDIT_CARD_ID
								, PAYMENT_NUM
								, TEAM_ID
								, TEAM_SET_LIST
								, BANK_FEE
								, B2C_CONTACT_ID
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("PaymentActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

