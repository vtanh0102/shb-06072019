
function B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed(fldB_NETTHUTHUAN_CARD_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldB_NETTHUTHUAN_CARD_NAME.id.substring(0, fldB_NETTHUTHUAN_CARD_NAME.id.length - 'B_NETTHUTHUAN_CARD_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_NETTHUTHUAN_CARD_NAME = document.getElementById(userContext + 'PREV_B_NETTHUTHUAN_CARD_NAME');
	if ( fldPREV_B_NETTHUTHUAN_CARD_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_NETTHUTHUAN_CARD_NAME');
	}
	else if ( fldPREV_B_NETTHUTHUAN_CARD_NAME.value != fldB_NETTHUTHUAN_CARD_NAME.value )
	{
		if ( fldB_NETTHUTHUAN_CARD_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIB0305.AutoComplete.B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Get(fldB_NETTHUTHUAN_CARD_NAME.value, B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed_OnSucceededWithContext, B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_NAME_AjaxErrors');
		var fldB_NETTHUTHUAN_CARD_ID        = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_ID'       );
		var fldB_NETTHUTHUAN_CARD_NAME      = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_NAME'     );
		var fldPREV_B_NETTHUTHUAN_CARD_NAME = document.getElementById(userContext + 'PREV_B_NETTHUTHUAN_CARD_NAME');
		if ( fldB_NETTHUTHUAN_CARD_ID        != null ) fldB_NETTHUTHUAN_CARD_ID.value        = sID  ;
		if ( fldB_NETTHUTHUAN_CARD_NAME      != null ) fldB_NETTHUTHUAN_CARD_NAME.value      = sNAME;
		if ( fldPREV_B_NETTHUTHUAN_CARD_NAME != null ) fldPREV_B_NETTHUTHUAN_CARD_NAME.value = sNAME;
	}
	else
	{
		alert('result from KPIB0305.AutoComplete service is null');
	}
}

function B_NETTHUTHUAN_CARD_B_NETTHUTHUAN_CARD_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_NETTHUTHUAN_CARD_ID        = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_ID'       );
	var fldB_NETTHUTHUAN_CARD_NAME      = document.getElementById(userContext + 'B_NETTHUTHUAN_CARD_NAME'     );
	var fldPREV_B_NETTHUTHUAN_CARD_NAME = document.getElementById(userContext + 'PREV_B_NETTHUTHUAN_CARD_NAME');
	if ( fldB_NETTHUTHUAN_CARD_ID        != null ) fldB_NETTHUTHUAN_CARD_ID.value        = '';
	if ( fldB_NETTHUTHUAN_CARD_NAME      != null ) fldB_NETTHUTHUAN_CARD_NAME.value      = '';
	if ( fldPREV_B_NETTHUTHUAN_CARD_NAME != null ) fldPREV_B_NETTHUTHUAN_CARD_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

