/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace SplendidCRM.Administration.MailChimp
{
	/// <summary>
	///		Summary description for ConfigView.
	/// </summary>
	public class ConfigView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected CheckBox     ENABLED               ;
		protected CheckBox     VERBOSE_STATUS        ;
		protected DropDownList DIRECTION             ;
		protected DropDownList CONFLICT_RESOLUTION   ;
		protected DropDownList SYNC_MODULES          ;
		protected TextBox      MERGE_FIELDS          ;
		protected TextBox      OAUTH_DATA_CENTER     ;
		protected TextBox      OAUTH_CLIENT_ID       ;
		protected TextBox      OAUTH_CLIENT_SECRET   ;
		protected TextBox      OAUTH_ACCESS_TOKEN    ;
		protected TextBox      AUTHORIZATION_CODE    ;
		protected Button       btnGetAccessToken     ;

		public class MailChimpToken
		{
			public string access_token;
			public string expires_in  ;
			public string scope       ;
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "GetAccessToken" )
			{
				try
				{
					// http://developer.mailchimp.com/documentation/mailchimp/guides/how-to-use-oauth2/
					HttpWebRequest objRequest = (HttpWebRequest) WebRequest.Create("https://login.mailchimp.com/oauth2/token");
					objRequest.Headers.Add("cache-control", "no-cache");
					objRequest.KeepAlive         = false;
					objRequest.AllowAutoRedirect = false;
					objRequest.Timeout           = 120000;  // 120 seconds
					objRequest.ContentType       = "application/x-www-form-urlencoded";
					objRequest.Method            = "POST";
					
					string sREDIRECT_URL = Request.Url.Scheme + "://" + Request.Url.Host + Sql.ToString(Application["rootURL"]) + "Administration/MailChimp/OAuthLanding.aspx";
					string sData = "grant_type=authorization_code&client_id=" + OAUTH_CLIENT_ID.Text + "&client_secret=" + OAUTH_CLIENT_SECRET.Text + "&code=" + AUTHORIZATION_CODE.Text + "&redirect_uri=" + HttpUtility.UrlEncode(sREDIRECT_URL);
					objRequest.ContentLength = sData.Length;
					using ( StreamWriter stm = new StreamWriter(objRequest.GetRequestStream(), System.Text.Encoding.ASCII) )
					{
						stm.Write(sData);
					}
					
					string sResponse = String.Empty;
					using ( HttpWebResponse objResponse = (HttpWebResponse) objRequest.GetResponse() )
					{
						if ( objResponse != null )
						{
							if ( objResponse.StatusCode != HttpStatusCode.OK && objResponse.StatusCode != HttpStatusCode.Found )
							{
								throw(new Exception(objResponse.StatusCode + " " + objResponse.StatusDescription));
							}
							else
							{
								using ( StreamReader stm = new StreamReader(objResponse.GetResponseStream()) )
								{
									sResponse = stm.ReadToEnd();
									JavaScriptSerializer json = new JavaScriptSerializer();
									MailChimpToken token = json.Deserialize<MailChimpToken>(sResponse);
									OAUTH_ACCESS_TOKEN.Text = token.access_token;
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Save" || e.CommandName == "Test" )
			{
				try
				{
					if ( Page.IsValid )
					{
						if ( e.CommandName == "Test" )
						{
							StringBuilder sbErrors = new StringBuilder();
							Spring.Social.MailChimp.MailChimpSync.ValidateMailChimp(Application, OAUTH_DATA_CENTER.Text, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, OAUTH_ACCESS_TOKEN.Text, sbErrors);
							//Spring.Social.MailChimp.MailChimpSync.RefreshAccessToken(Application, sbErrors);

							if ( sbErrors.Length > 0 )
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							else
								ctlDynamicButtons.ErrorText = L10n.Term("MailChimp.LBL_TEST_SUCCESSFUL");
						}
						else if ( e.CommandName == "Save" )
						{
							Application["CONFIG.MailChimp.Enabled"           ] = ENABLED            .Checked;
							Application["CONFIG.MailChimp.VerboseStatus"     ] = VERBOSE_STATUS     .Checked;
							Application["CONFIG.MailChimp.Direction"         ] = DIRECTION          .SelectedValue;
							Application["CONFIG.MailChimp.ConflictResolution"] = CONFLICT_RESOLUTION.SelectedValue;
							Application["CONFIG.MailChimp.SyncModules"       ] = SYNC_MODULES       .SelectedValue;
							Application["CONFIG.MailChimp.MergeFields"       ] = MERGE_FIELDS       .Text;
							Application["CONFIG.MailChimp.DataCenter"        ] = OAUTH_DATA_CENTER  .Text;
							Application["CONFIG.MailChimp.ClientID"          ] = OAUTH_CLIENT_ID    .Text;
							Application["CONFIG.MailChimp.ClientSecret"      ] = OAUTH_CLIENT_SECRET.Text;
							Application["CONFIG.MailChimp.OAuthAccessToken"  ] = OAUTH_ACCESS_TOKEN .Text;
						
							SqlProcs.spCONFIG_Update("system", "MailChimp.Enabled"           , Sql.ToString(Application["CONFIG.MailChimp.Enabled"           ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.VerboseStatus"     , Sql.ToString(Application["CONFIG.MailChimp.VerboseStatus"     ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.Direction"         , Sql.ToString(Application["CONFIG.MailChimp.Direction"         ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.ConflictResolution", Sql.ToString(Application["CONFIG.MailChimp.ConflictResolution"]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.SyncModules"       , Sql.ToString(Application["CONFIG.MailChimp.SyncModules"       ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.MergeFields"       , Sql.ToString(Application["CONFIG.MailChimp.MergeFields"       ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.DataCenter"        , Sql.ToString(Application["CONFIG.MailChimp.DataCenter"        ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.ClientID"          , Sql.ToString(Application["CONFIG.MailChimp.ClientID"          ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.ClientSecret"      , Sql.ToString(Application["CONFIG.MailChimp.ClientSecret"      ]));
							SqlProcs.spCONFIG_Update("system", "MailChimp.OAuthAccessToken"  , Sql.ToString(Application["CONFIG.MailChimp.OAuthAccessToken"  ]));
							SplendidCache.ClearSet("CONFIG.MailChimp.List.MergeFields.");
#if !DEBUG
							SqlProcs.spSCHEDULERS_UpdateStatus("function::pollMailChimp", ENABLED.Checked ? "Active" : "Inactive");
#endif
							// 07/15/2017 Paul.  Instead of requiring that the user manually enable the user, do so automatically. 
							if ( Sql.ToBoolean(Application["CONFIG.MailChimp.Enabled"]) )
							{
								Guid gUSER_ID = Spring.Social.MailChimp.MailChimpSync.MailChimpUserID(Application);
								DbProviderFactory dbf = DbProviderFactories.GetFactory();
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL;
									sSQL = "select STATUS       " + ControlChars.CrLf
									     + "  from vwUSERS      " + ControlChars.CrLf
									     + " where ID = @ID     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gUSER_ID);
										string sSTATUS = Sql.ToString(cmd.ExecuteScalar());
										if ( sSTATUS != "Active" )
										{
											SqlProcs.spUSERS_UpdateStatus(gUSER_ID, "Active");
										}
									}
								}
							}
							Response.Redirect("default.aspx");
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("MailChimp.LBL_MANAGE_MailChimp_TITLE"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !IsPostBack )
				{
					DIRECTION.DataSource = SplendidCache.List("mailchimp_sync_direction");
					DIRECTION.DataBind();
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(String.Empty                      ), String.Empty));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.remote"), "remote"    ));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.local" ), "local"     ));
					SYNC_MODULES.DataSource = SplendidCache.List("mailchimp_sync_module");
					SYNC_MODULES.DataBind();
					MERGE_FIELDS       .Text    = Sql.ToString (Application["CONFIG.MailChimp.MergeFields"      ]);
					ENABLED            .Checked = Sql.ToBoolean(Application["CONFIG.MailChimp.Enabled"          ]);
					VERBOSE_STATUS     .Checked = Sql.ToBoolean(Application["CONFIG.MailChimp.VerboseStatus"    ]);
					OAUTH_DATA_CENTER  .Text    = Sql.ToString (Application["CONFIG.MailChimp.DataCenter"       ]);
					OAUTH_CLIENT_ID    .Text    = Sql.ToString (Application["CONFIG.MailChimp.ClientID"         ]);
					OAUTH_CLIENT_SECRET.Text    = Sql.ToString (Application["CONFIG.MailChimp.ClientSecret"     ]);
					OAUTH_ACCESS_TOKEN .Text    = Sql.ToString (Application["CONFIG.MailChimp.OAuthAccessToken" ]);
					try
					{
						Utils.SetSelectedValue(DIRECTION, Sql.ToString(Application["CONFIG.MailChimp.Direction"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(CONFLICT_RESOLUTION, Sql.ToString(Application["CONFIG.MailChimp.ConflictResolution"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(SYNC_MODULES, Sql.ToString(Application["CONFIG.MailChimp.SyncModules"]));
					}
					catch
					{
					}
				}
				
				// 04/26/2016 Paul.  Instead of building the URL in the code-behind, just manually construct in JavaScript so that postback would not be necessary after changing Portal ID or Client ID. 
				//string sRedirectURL = Request.Url.Scheme + "://" + Request.Url.Host + Sql.ToString(Application["rootURL"]) + "Import/OAuthLanding.aspx";
				//Spring.Social.MailChimp.Connect.MailChimpServiceProvider MailChimpServiceProvider = new Spring.Social.MailChimp.Connect.MailChimpServiceProvider(OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text);
				//Spring.Social.OAuth2.OAuth2Parameters parameters = new Spring.Social.OAuth2.OAuth2Parameters()
				//{
				//	RedirectUrl = sRedirectURL,
				//	Scope = "contacts-rw offline"
				//};
				//parameters.Add("DataCenter", OAUTH_DATA_CENTER.Text);
				//string authenticateUrl = MailChimpServiceProvider.OAuthOperations.BuildAuthorizeUrl(Spring.Social.OAuth2.GrantType.ImplicitGrant, parameters);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "MailChimp";
			SetAdminMenu(m_sMODULE);
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
			ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
		}
		#endregion
	}
}
