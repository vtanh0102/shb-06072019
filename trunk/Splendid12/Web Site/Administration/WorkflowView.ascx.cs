/**
 * Copyright (C) 2008-2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration
{
	/// <summary>
	///		Summary description for WorkflowView.
	/// </summary>
	public class WorkflowView : SplendidControl
	{
		protected Label lblError  ;
		protected Label lblEvents ;
		protected Label lblPending;
		protected Label lblBusinessProcessPending;

		// 10/05/2008 Paul.  Provide a quick way to run pending workflows. 
		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Workflows.Process" )
				{
					bool bEnableWorkflow = Sql.ToBoolean(Application["CONFIG.enable_workflow"]);
					// 10/27/2008 Paul.  Pass the context instead of the Application so that more information will be available to the error handling. 
					if ( bEnableWorkflow )
						WorkflowUtils.Process(HttpContext.Current);
				}
				else if ( e.CommandName == "Businessprocesses.Process" )
				{
					bool bEnableWorkflow = Sql.ToBoolean(Application["CONFIG.enable_workflow"]);
					if ( bEnableWorkflow )
						Workflow4Utils.Process(HttpContext.Current);
				}
				Response.Redirect("default.aspx");
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				bool bEnableWorkflow = Sql.ToBoolean(Application["CONFIG.enable_workflow"]);
				if ( bEnableWorkflow )
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL ;
						sSQL = "select count(*)            " + ControlChars.CrLf
						     + "  from vwWORKFLOW_EVENTS   " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							lblEvents.Text = Sql.ToString(cmd.ExecuteScalar());
						}
						sSQL = "select count(*)            " + ControlChars.CrLf
						     + "  from vwWORKFLOW_RUN_Ready" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							lblPending.Text = Sql.ToString(cmd.ExecuteScalar());
						}
						sSQL = "select count(*)            " + ControlChars.CrLf
						     + "  from vwBUSINESS_PROCESSES_RUN_Ready" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							lblBusinessProcessPending.Text = Sql.ToString(cmd.ExecuteScalar());
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
