/**
 * Copyright (C) 2018 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace SplendidCRM.Administration.Watson
{
	/// <summary>
	///		Summary description for ConfigView.
	/// </summary>
	public class ConfigView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected CheckBox     ENABLED               ;
		protected CheckBox     VERBOSE_STATUS        ;
		protected DropDownList DIRECTION             ;
		protected DropDownList CONFLICT_RESOLUTION   ;
		protected DropDownList SYNC_MODULES          ;
		protected TextBox      MERGE_FIELDS          ;
		protected TextBox      OAUTH_POD_NUMBER      ;
		protected TextBox      OAUTH_CLIENT_ID       ;
		protected TextBox      OAUTH_CLIENT_SECRET   ;
		protected TextBox      OAUTH_REFRESH_TOKEN   ;
		protected TextBox      OAUTH_ACCESS_TOKEN    ;
		protected TextBox      OAUTH_EXPIRES_AT      ;
		protected HiddenField  OAUTH_EXPIRES_IN      ;
		protected TextBox      AUTHORIZATION_CODE    ;
		protected Button       btnGetAccessToken     ;
		protected HiddenField  DEFAULT_DATABASE_ID   ;
		protected TextBox      DEFAULT_DATABASE_NAME ;

		public class WatsonToken
		{
			public string refresh_token;
			public string access_token ;
			public string expires_in   ;
			public string scope        ;
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "GetAccessToken" )
			{
				try
				{
					int nPodNumber = Sql.ToInteger(OAUTH_POD_NUMBER.Text);
					HttpWebRequest objRequest = (HttpWebRequest) WebRequest.Create("https://api" + nPodNumber.ToString() + ".silverpop.com/oauth/token");
					objRequest.Headers.Add("cache-control", "no-cache");
					objRequest.KeepAlive         = false;
					objRequest.AllowAutoRedirect = false;
					objRequest.Timeout           = 120000;  // 120 seconds
					objRequest.ContentType       = "application/x-www-form-urlencoded";
					objRequest.Method            = "POST";
					
					string sREDIRECT_URL = Request.Url.Scheme + "://" + Request.Url.Host + Sql.ToString(Application["rootURL"]) + "Administration/Watson/OAuthLanding.aspx";
					string sData = "grant_type=authorization_code&client_id=" + OAUTH_CLIENT_ID.Text + "&client_secret=" + OAUTH_CLIENT_SECRET.Text + "&code=" + AUTHORIZATION_CODE.Text + "&redirect_uri=" + HttpUtility.UrlEncode(sREDIRECT_URL);
					objRequest.ContentLength = sData.Length;
					using ( StreamWriter stm = new StreamWriter(objRequest.GetRequestStream(), System.Text.Encoding.ASCII) )
					{
						stm.Write(sData);
					}
					
					string sResponse = String.Empty;
					using ( HttpWebResponse objResponse = (HttpWebResponse) objRequest.GetResponse() )
					{
						if ( objResponse != null )
						{
							if ( objResponse.StatusCode != HttpStatusCode.OK && objResponse.StatusCode != HttpStatusCode.Found )
							{
								throw(new Exception(objResponse.StatusCode + " " + objResponse.StatusDescription));
							}
							else
							{
								using ( StreamReader stm = new StreamReader(objResponse.GetResponseStream()) )
								{
									sResponse = stm.ReadToEnd();
									JavaScriptSerializer json = new JavaScriptSerializer();
									WatsonToken token = json.Deserialize<WatsonToken>(sResponse);
									OAUTH_REFRESH_TOKEN.Text = token.refresh_token;
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Save" || e.CommandName == "Test" || e.CommandName == "RefreshToken" )
			{
				try
				{
					if ( Page.IsValid )
					{
						if ( e.CommandName == "RefreshToken" )
						{
							StringBuilder sbErrors = new StringBuilder();
							Spring.Social.Watson.WatsonSync.RefreshAccessToken(Application, sbErrors);

							if ( sbErrors.Length > 0 )
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							else
							{
								OAUTH_ACCESS_TOKEN .Text    = Sql.ToString (Application["CONFIG.Watson.OAuthAccessToken" ]);
								OAUTH_REFRESH_TOKEN.Text    = Sql.ToString (Application["CONFIG.Watson.OAuthRefreshToken"]);
								OAUTH_EXPIRES_AT   .Text    = Sql.ToString (Application["CONFIG.Watson.OAuthExpiresAt"   ]);
								ctlDynamicButtons.ErrorText = L10n.Term("Watson.LBL_TEST_SUCCESSFUL");
							}
						}
						else if ( e.CommandName == "Test" )
						{
							StringBuilder sbErrors = new StringBuilder();
							Spring.Social.Watson.WatsonSync.ValidateWatson(Application, OAUTH_POD_NUMBER.Text, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, OAUTH_ACCESS_TOKEN.Text, sbErrors);

							if ( sbErrors.Length > 0 )
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							else
								ctlDynamicButtons.ErrorText = L10n.Term("Watson.LBL_TEST_SUCCESSFUL");
						}
						else if ( e.CommandName == "Save" )
						{
							Application["CONFIG.Watson.Enabled"           ] = ENABLED            .Checked;
							Application["CONFIG.Watson.VerboseStatus"     ] = VERBOSE_STATUS     .Checked;
							Application["CONFIG.Watson.Direction"         ] = DIRECTION          .SelectedValue;
							Application["CONFIG.Watson.ConflictResolution"] = CONFLICT_RESOLUTION.SelectedValue;
							Application["CONFIG.Watson.SyncModules"       ] = SYNC_MODULES       .SelectedValue;
							Application["CONFIG.Watson.MergeFields"       ] = MERGE_FIELDS       .Text;
							Application["CONFIG.Watson.PodNumber"         ] = OAUTH_POD_NUMBER   .Text;
							Application["CONFIG.Watson.ClientID"          ] = OAUTH_CLIENT_ID    .Text;
							Application["CONFIG.Watson.ClientSecret"      ] = OAUTH_CLIENT_SECRET.Text;
							Application["CONFIG.Watson.OAuthAccessToken"  ] = OAUTH_ACCESS_TOKEN .Text;
							Application["CONFIG.Watson.OAuthRefreshToken" ] = OAUTH_REFRESH_TOKEN.Text;
							Application["CONFIG.Watson.OAuthExpiresAt"    ] = OAUTH_EXPIRES_AT   .Text;
							Application["CONFIG.Watson.DefaultDatabaseID" ] = DEFAULT_DATABASE_ID.Value;
						
							SqlProcs.spCONFIG_Update("system", "Watson.Enabled"           , Sql.ToString(Application["CONFIG.Watson.Enabled"           ]));
							SqlProcs.spCONFIG_Update("system", "Watson.VerboseStatus"     , Sql.ToString(Application["CONFIG.Watson.VerboseStatus"     ]));
							SqlProcs.spCONFIG_Update("system", "Watson.Direction"         , Sql.ToString(Application["CONFIG.Watson.Direction"         ]));
							SqlProcs.spCONFIG_Update("system", "Watson.ConflictResolution", Sql.ToString(Application["CONFIG.Watson.ConflictResolution"]));
							SqlProcs.spCONFIG_Update("system", "Watson.SyncModules"       , Sql.ToString(Application["CONFIG.Watson.SyncModules"       ]));
							SqlProcs.spCONFIG_Update("system", "Watson.MergeFields"       , Sql.ToString(Application["CONFIG.Watson.MergeFields"       ]));
							SqlProcs.spCONFIG_Update("system", "Watson.PodNumber"         , Sql.ToString(Application["CONFIG.Watson.PodNumber"         ]));
							SqlProcs.spCONFIG_Update("system", "Watson.ClientID"          , Sql.ToString(Application["CONFIG.Watson.ClientID"          ]));
							SqlProcs.spCONFIG_Update("system", "Watson.ClientSecret"      , Sql.ToString(Application["CONFIG.Watson.ClientSecret"      ]));
							SqlProcs.spCONFIG_Update("system", "Watson.OAuthAccessToken"  , Sql.ToString(Application["CONFIG.Watson.OAuthAccessToken"  ]));
							SqlProcs.spCONFIG_Update("system", "Watson.OAuthRefreshToken" , Sql.ToString(Application["CONFIG.Watson.OAuthRefreshToken" ]));
							SqlProcs.spCONFIG_Update("system", "Watson.OAuthExpiresAt"    , Sql.ToString(Application["CONFIG.Watson.OAuthExpiresAt"    ]));
							SqlProcs.spCONFIG_Update("system", "Watson.DefaultDatabaseID" , Sql.ToString(Application["CONFIG.Watson.DefaultDatabaseID" ]));
							SplendidCache.ClearSet("CONFIG.Watson.List.MergeFields.");
#if !DEBUG
							SqlProcs.spSCHEDULERS_UpdateStatus("function::pollWatson", ENABLED.Checked ? "Active" : "Inactive");
#endif
							// 07/15/2017 Paul.  Instead of requiring that the user manually enable the user, do so automatically. 
							if ( Sql.ToBoolean(Application["CONFIG.Watson.Enabled"]) )
							{
								Guid gUSER_ID = Guid.Empty;
								gUSER_ID = Spring.Social.Watson.WatsonSync.WatsonUserID(Application);
								DbProviderFactory dbf = DbProviderFactories.GetFactory();
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL;
									sSQL = "select STATUS       " + ControlChars.CrLf
									     + "  from vwUSERS      " + ControlChars.CrLf
									     + " where ID = @ID     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gUSER_ID);
										string sSTATUS = Sql.ToString(cmd.ExecuteScalar());
										if ( sSTATUS != "Active" )
										{
											SqlProcs.spUSERS_UpdateStatus(gUSER_ID, "Active");
										}
									}
								}
							}
							Response.Redirect("default.aspx");
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("Watson.LBL_MANAGE_WATSON_TITLE"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !IsPostBack )
				{
					DIRECTION.DataSource = SplendidCache.List("watson_sync_direction");
					DIRECTION.DataBind();
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(String.Empty                      ), String.Empty));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.remote"), "remote"    ));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.local" ), "local"     ));
					SYNC_MODULES.DataSource = SplendidCache.List("watson_sync_module");
					SYNC_MODULES.DataBind();
					MERGE_FIELDS         .Text    = Sql.ToString (Application["CONFIG.Watson.MergeFields"      ]);
					ENABLED              .Checked = Sql.ToBoolean(Application["CONFIG.Watson.Enabled"          ]);
					VERBOSE_STATUS       .Checked = Sql.ToBoolean(Application["CONFIG.Watson.VerboseStatus"    ]);
					OAUTH_POD_NUMBER     .Text    = Sql.ToString (Application["CONFIG.Watson.PodNumber"        ]);
					OAUTH_CLIENT_ID      .Text    = Sql.ToString (Application["CONFIG.Watson.ClientID"         ]);
					OAUTH_CLIENT_SECRET  .Text    = Sql.ToString (Application["CONFIG.Watson.ClientSecret"     ]);
					OAUTH_ACCESS_TOKEN   .Text    = Sql.ToString (Application["CONFIG.Watson.OAuthAccessToken" ]);
					OAUTH_REFRESH_TOKEN  .Text    = Sql.ToString (Application["CONFIG.Watson.OAuthRefreshToken"]);
					OAUTH_EXPIRES_AT     .Text    = Sql.ToString (Application["CONFIG.Watson.OAuthExpiresAt"   ]);
					DEFAULT_DATABASE_ID  .Value   = Sql.ToString (Application["CONFIG.Watson.DefaultDatabaseID"]);
					DEFAULT_DATABASE_NAME.Text    = Sql.ToString (Application["CONFIG.Watson.DefaultDatabaseID"]);
					try
					{
						Utils.SetSelectedValue(DIRECTION, Sql.ToString(Application["CONFIG.Watson.Direction"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(CONFLICT_RESOLUTION, Sql.ToString(Application["CONFIG.Watson.ConflictResolution"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(SYNC_MODULES, Sql.ToString(Application["CONFIG.Watson.SyncModules"]));
					}
					catch
					{
					}
				}
					try
					{
						if ( !Sql.IsEmptyString(DEFAULT_DATABASE_ID.Value) )
						{
							StringBuilder sbErrors = new StringBuilder();
							Spring.Social.Watson.WatsonSync.RefreshAccessToken(Application, sbErrors);
							if ( sbErrors.Length == 0 )
							{
								Spring.Social.Watson.Api.IWatson watson = Spring.Social.Watson.WatsonSync.CreateApi(Application);
								Spring.Social.Watson.Api.Database database = watson.DatabaseOperations.GetById(DEFAULT_DATABASE_ID.Value);
								DEFAULT_DATABASE_NAME.Text = database.NAME;
							}
							else
							{
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							}
						}
					}
					catch
					{
					}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Watson";
			SetAdminMenu(m_sMODULE);
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
			ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
		}
		#endregion
	}
}
