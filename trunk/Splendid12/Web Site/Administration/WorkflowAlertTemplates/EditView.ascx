<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.WorkflowAlertTemplates.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
<script type="text/javascript">
// 08/08/2006 Paul.  Fixed access to selected index.  The line was missing the closing square bracket.
function ShowVariable()
{
	var fldVariableText = document.getElementById('<%= txtVariableText.ClientID %>');
	var sBaseModule   = document.getElementById('<%= lstBASE_MODULE .ClientID %>').options[document.getElementById('<%= lstBASE_MODULE .ClientID %>').selectedIndex].value;
	var sRelated      = document.getElementById('<%= lstRELATED     .ClientID %>').options[document.getElementById('<%= lstRELATED     .ClientID %>').selectedIndex].value;
	var sVariableType = document.getElementById('<%= lstVariableType.ClientID %>').options[document.getElementById('<%= lstVariableType.ClientID %>').selectedIndex].value;
	var sVariableName = document.getElementById('<%= lstVariableName.ClientID %>').options[document.getElementById('<%= lstVariableName.ClientID %>').selectedIndex].value;
	if ( sVariableName == 'href_link' )
	{
		if ( sRelated != '' )
			fldVariableText.value = '{::href_link::' + sBaseModule + '::' + sRelated + '::href_link::}';
		else
			fldVariableText.value = '{::href_link::' + sBaseModule + '::href_link::}';
	}
	else
	{
		if ( sRelated != '' )
			fldVariableText.value = '{::' + sVariableType + '::' + sBaseModule + '::' + sRelated + '::' + sVariableName.toLowerCase() + '::}';
		else
			fldVariableText.value = '{::' + sVariableType + '::' + sBaseModule + '::' + sVariableName.toLowerCase() + '::}';
	}
}
function InsertVariable()
{
	try
	{
		var sVariable = document.getElementById('<%= txtVariableText.ClientID %>').value;
		
		// 09/18/2011 Paul.  Upgrade to CKEditor 3.6.2. 
		var oEditor = CKEDITOR.instances['<%= txtBODY.ClientID %>'];
		// 09/22/2007 Paul.  Only allow insert in Wysiwyg mode. 
		// 10/27/2011 Paul.  Fix for CKEDITOR. 
		if ( oEditor != null && oEditor.mode == 'wysiwyg' )
		{
			// 09/20/2012 Paul.  insertHtml starts with a lowercase I. 
			oEditor.insertHtml(sVariable);
		}
		else
		{
			//var txtBODY = document.getElementsByClassName('cke_source cke_enable_context_menu')[0];
			var txtBODY = document.getElementById('cke_contents_<%= txtBODY.ClientID %>').childNodes[0];
			if ( txtBODY.selectionStart !== undefined )
			{
				var start = txtBODY.selectionStart;
				var end   = txtBODY.selectionEnd  ;
				txtBODY.value = txtBODY.value.substr(0, start) + sVariable + txtBODY.value.substr(end);
				
				var pos = start + sVariable.length;
				txtBODY.selectionStart = pos;
				txtBODY.selectionEnd   = pos;
			}
		}
	}
	catch(e)
	{
		alert(e.message);
	}
}
function AddFile()
{
	for(var i=0;i<10;i++)
	{
		var elem = document.getElementById('file'+i);
		if(elem.style.display == 'none')
		{
			elem.style.display='block';
			break;
		}
	}
}
function DeleteFile(index)
{
	var elem = document.getElementById('file'+index);
	elem.style.display='none';
}
</script>

<asp:UpdatePanel UpdateMode="Conditional" runat="server">
	<ContentTemplate>
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="WorkflowAlertTemplates" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<asp:Table SkinID="tabEditView" runat="server">
					<asp:TableRow>
						<asp:TableCell Width="15%" CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_NAME") %> <asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' Runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataField"><asp:TextBox ID="txtNAME" TabIndex="1" MaxLength="255" size="30" Runat="server" /></asp:TableCell>
						<asp:TableCell Width="15%" CssClass="dataLabel"></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataLabel"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="15%" CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_FROM_NAME") %></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataField"><asp:TextBox ID="txtFROM_NAME" TabIndex="1" MaxLength="100" size="30" Runat="server" /></asp:TableCell>
						<asp:TableCell Width="15%" CssClass="dataLabel"></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataLabel"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="15%" CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_FROM_ADDR") %></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataField"><asp:TextBox ID="txtFROM_ADDR" TabIndex="1" MaxLength="100" size="30" Runat="server" /></asp:TableCell>
						<asp:TableCell Width="15%" CssClass="dataLabel"></asp:TableCell>
						<asp:TableCell Width="35%" CssClass="dataLabel"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_DESCRIPTION") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3"><asp:TextBox ID="txtDESCRIPTION" TabIndex="1" TextMode="MultiLine" Columns="90" Rows="1" style="overflow-y:auto;" Runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell ColumnSpan="4">&nbsp;</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_BASE_MODULE") %></asp:TableCell>
						<asp:TableCell CssClass="dataField"><asp:DropDownList ID="lstBASE_MODULE" TabIndex="1" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstBASE_MODULE_Changed" AutoPostBack="true" Runat="server" /></asp:TableCell>
						<asp:TableCell CssClass="dataLabel"><asp:Label Text='<%# L10n.Term("WorkflowAlertTemplates.LBL_RELATED_MODULE") %>' runat="server" /></asp:TableCell>
						<asp:TableCell CssClass="dataField"><asp:DropDownList ID="lstRELATED" TabIndex="1" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstRELATED_Changed" AutoPostBack="true" Runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<asp:DropDownList ID="lstVariableName"   TabIndex="1" DataValueField="NAME" DataTextField="DISPLAY_NAME" onchange="ShowVariable()" Runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_INSERT_VARIABLE") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<asp:DropDownList ID="lstVariableType"   TabIndex="1" DataValueField="NAME" DataTextField="DISPLAY_NAME" onchange="ShowVariable()" runat="server" />
							&nbsp;
							<asp:TextBox ID="txtVariableText"   size="50" Runat="server" />
							<asp:Button  ID="btnVariableInsert" UseSubmitBehavior="false" OnClientClick="InsertVariable(); return false;" CssClass="button" Text='<%# L10n.Term("WorkflowAlertTemplates.LBL_INSERT") %>' runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_SUBJECT") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3"><asp:TextBox ID="txtSUBJECT" TabIndex="1" TextMode="MultiLine" Columns="90" Rows="1" style="overflow-y:auto;" Runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertTemplates.LBL_BODY") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<%@ Register TagPrefix="CKEditor" Namespace="CKEditor.NET" Assembly="CKEditor.NET" %>
							<CKEditor:CKEditorControl id="txtBODY" Toolbar="SplendidCRM" Language='<%# Session["USER_SETTINGS/CULTURE"] %>' BasePath="~/ckeditor/" FilebrowserUploadUrl="../../ckeditor/upload.aspx" FilebrowserBrowseUrl="../../Images/Popup.aspx" FilebrowserWindowWidth="640" FilebrowserWindowHeight="480" runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%= L10n.Term("Emails.LBL_ATTACHMENTS") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<asp:Repeater id="ctlAttachments" runat="server">
								<HeaderTemplate />
								<ItemTemplate>
										<asp:HyperLink Text='<%# DataBinder.Eval(Container.DataItem, "FILENAME") %>' NavigateUrl='<%# "~/Notes/Attachment.aspx?ID=" + DataBinder.Eval(Container.DataItem, "NOTE_ATTACHMENT_ID") %>' Target="_blank" Runat="server" /><br />
								</ItemTemplate>
								<FooterTemplate />
							</asp:Repeater>
							<div id="uploads_div">
							<div style="display: none" id="file0"><input id="email_attachment0" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('0');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file1"><input id="email_attachment1" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('1');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file2"><input id="email_attachment2" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('2');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file3"><input id="email_attachment3" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('3');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file4"><input id="email_attachment4" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('4');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file5"><input id="email_attachment5" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('5');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file6"><input id="email_attachment6" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('6');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file7"><input id="email_attachment7" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('7');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file8"><input id="email_attachment8" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('8');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							<div style="display: none" id="file9"><input id="email_attachment9" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('9');" CssClass="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							</div>
							<input type="button" CssClass="button" onclick="AddFile();" value="<%= L10n.Term("Emails.LBL_ADD_FILE") %>" />
						</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
	</ContentTemplate>
</asp:UpdatePanel>
</div>
