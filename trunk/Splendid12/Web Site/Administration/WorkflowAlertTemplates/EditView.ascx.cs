/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
// 09/18/2011 Paul.  Upgrade to CKEditor 3.6.2. 
using CKEditor.NET;

namespace SplendidCRM.Administration.WorkflowAlertTemplates
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 01/13/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                          ;
		protected TextBox         txtNAME                      ;
		protected DropDownList    lstBASE_MODULE               ;
		protected DropDownList    lstRELATED                   ;
		protected TextBox         txtFROM_NAME                 ;
		protected TextBox         txtFROM_ADDR                 ;
		protected TextBox         txtDESCRIPTION               ;
		protected TextBox         txtSUBJECT                   ;
		// 09/18/2011 Paul.  Upgrade to CKEditor 3.6.2. 
		protected CKEditorControl txtBODY                      ;
		protected DropDownList    lstVariableType              ;
		protected DropDownList    lstVariableName              ;
		protected TextBox         txtVariableText              ;
		protected Repeater        ctlAttachments               ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					if ( Page.IsValid )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									SqlProcs.spWORKFLOW_ALERT_TEMPLATES_Update
										( ref gID
										, txtNAME.Text
										, lstBASE_MODULE.SelectedValue
										, txtFROM_ADDR.Text
										, txtFROM_NAME.Text
										, txtDESCRIPTION.Text
										, txtSUBJECT.Text
										, String.Empty   // BODY
										, txtBODY.Text   // BODY_HTML
										, trn
										);
									// 06/16/2010 Paul.  There can be a maximum of 10 attachments, not including attachments that were previously saved. 
									for ( int i=0; i < 10; i++ )
									{
										HtmlInputFile fileATTACHMENT = FindControl("email_attachment" + i.ToString()) as HtmlInputFile;
										if ( fileATTACHMENT != null )
										{
											HttpPostedFile pstATTACHMENT = fileATTACHMENT.PostedFile;
											if ( pstATTACHMENT != null )
											{
												long lFileSize      = pstATTACHMENT.ContentLength;
												long lUploadMaxSize = Sql.ToLong(Application["CONFIG.upload_maxsize"]);
												if ( (lUploadMaxSize > 0) && (lFileSize > lUploadMaxSize) )
												{
													throw(new Exception("ERROR: uploaded file was too big: max filesize: " + lUploadMaxSize.ToString()));
												}
												if ( pstATTACHMENT.FileName.Length > 0 )
												{
													string sFILENAME       = Path.GetFileName (pstATTACHMENT.FileName);
													string sFILE_EXT       = Path.GetExtension(sFILENAME);
													string sFILE_MIME_TYPE = pstATTACHMENT.ContentType;
													
													// 06/16/2010 Paul.  Use the same EmailTemplates parent type. 
													Guid gNOTE_ID = Guid.Empty;
													// 04/02/2012 Paul.  Add ASSIGNED_USER_ID. 
													SqlProcs.spNOTES_Update
														( ref gNOTE_ID
														, L10n.Term("EmailTemplates.LBL_EMAIL_ATTACHMENT") + ": " + sFILENAME
														, "EmailTemplates"  // PARENT_TYPE
														, gID               // PARENT_ID
														, Guid.Empty        // CONTACT_ID
														, String.Empty      // DESCRIPTION
														, Security.TEAM_ID
														, String.Empty      // TEAM_SET_LIST
														, Security.USER_ID
														// 05/17/2017 Paul.  Add Tags module. 
														, String.Empty      // TAG_SET_NAME
														// 11/07/2017 Paul.  Add IS_PRIVATE for use by a large customer. 
														, false             // IS_PRIVATE
														// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
														, String.Empty      // ASSIGNED_SET_LIST
														, trn
														);

													Guid gNOTE_ATTACHMENT_ID = Guid.Empty;
													// 01/20/2006 Paul.  Must include in transaction
													SqlProcs.spNOTE_ATTACHMENTS_Insert(ref gNOTE_ATTACHMENT_ID, gNOTE_ID, pstATTACHMENT.FileName, sFILENAME, sFILE_EXT, sFILE_MIME_TYPE, trn);
													// 11/06/2010 Paul.  Move LoadFile() to Crm.NoteAttachments. 
													Crm.NoteAttachments.LoadFile(gNOTE_ATTACHMENT_ID, pstATTACHMENT.InputStream, trn);
												}
											}
										}
									}
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
						}
						Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyGuid(gID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
		}

		protected void lstBASE_MODULE_Changed(Object sender, EventArgs e)
		{
			lstRELATED_Bind();
		}

		private void lstRELATED_Bind()
		{
			try
			{
				DataView vwRelationships = new DataView(SplendidCache.WorkflowRelationships());
				// 11/25/2008 Paul.  Only show one-to-many as we can only lookup one value. 
				vwRelationships.RowFilter = "       RELATIONSHIP_MANY = 0 " + ControlChars.CrLf
				                          + "   and BASE_MODULE       = \'" + lstBASE_MODULE.SelectedValue + "\'" + ControlChars.CrLf;

				vwRelationships.Sort = "DISPLAY_NAME";
				lstRELATED.DataSource = vwRelationships;
				lstRELATED.DataBind();
				lstRELATED.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
				lstRELATED_Changed(null, null);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void lstRELATED_Changed(Object sender, EventArgs e)
		{
			lstVariableName.Items.Clear();
			if ( Sql.IsEmptyString(lstRELATED.SelectedValue) )
			{
				string sMODULE_TABLE = Sql.ToString(Application["Modules." + lstBASE_MODULE.SelectedValue + ".TableName"]);
				DataTable dtColumns = SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy();
				foreach(DataRow row in dtColumns.Rows)
				{
					row["DISPLAY_NAME"] = Utils.TableColumnName(L10n, lstBASE_MODULE.SelectedValue, Sql.ToString(row["DISPLAY_NAME"]));
				}
				lstVariableName.DataSource = dtColumns;
				lstVariableName.DataBind();
				lstVariableName.Items.Insert(0, new ListItem(L10n.Term("WorkflowAlertTemplates.LBL_LINK_TO_RECORD"), "href_link"));

				if ( lstVariableName.Items[0].Value == "href_link" )
					txtVariableText.Text = "{::href_link::" + lstBASE_MODULE.SelectedValue + "::href_link::}";
				else
					txtVariableText.Text = "{::" + lstVariableType.SelectedValue + "::" + lstBASE_MODULE.SelectedValue + "::" + lstVariableName.Items[0].Value.ToLower() + "::}";
			}
			else
			{
				string sMODULE_TABLE = Sql.ToString(Application["Modules." + lstRELATED.SelectedValue + ".TableName"]);
				DataTable dtColumns = SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy();
				foreach(DataRow row in dtColumns.Rows)
				{
					row["DISPLAY_NAME"] = Utils.TableColumnName(L10n, lstRELATED.SelectedValue, Sql.ToString(row["DISPLAY_NAME"]));
				}
				lstVariableName.DataSource = dtColumns;
				lstVariableName.DataBind();
				lstVariableName.Items.Insert(0, new ListItem(L10n.Term("WorkflowAlertTemplates.LBL_LINK_TO_RECORD"), "href_link"));

				if ( lstVariableName.Items[0].Value == "href_link" )
					txtVariableText.Text = "{::href_link::" + lstBASE_MODULE.SelectedValue + "::" + lstRELATED.SelectedValue + "::href_link::}";
				else
					txtVariableText.Text = "{::" + lstVariableType.SelectedValue + "::" + lstBASE_MODULE.SelectedValue + "::" + lstRELATED.SelectedValue + "::" + lstVariableName.Items[0].Value.ToLower() + "::}";
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					lstVariableType.DataSource = SplendidCache.List("value_type");
					lstVariableType.DataBind();
					lstBASE_MODULE.DataSource = SplendidCache.WorkflowModules();
					lstBASE_MODULE.DataBind();
					lstBASE_MODULE_Changed(null, null);

					// 10/16/2009 Paul.  The duplicate button is visible, so we should allow duplicates. 
					Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *                         " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ALERT_TEMPLATES" + ControlChars.CrLf
							     + " where ID = @ID                  " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AddParameter(cmd, "@ID", gDuplicateID);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AddParameter(cmd, "@ID", gID);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								// 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											// 03/04/2006 Paul.  Name was not being set. 
											txtNAME       .Text  = Sql.ToString(rdr["NAME"       ]);
											txtFROM_NAME  .Text  = Sql.ToString(rdr["FROM_NAME"  ]);
											txtFROM_ADDR  .Text  = Sql.ToString(rdr["FROM_ADDR"  ]);
											txtDESCRIPTION.Text  = Sql.ToString(rdr["DESCRIPTION"]);
											txtSUBJECT    .Text  = Sql.ToString(rdr["SUBJECT"    ]);
											// 04/21/2006 Paul.  Change BODY to BODY_HTML. 
											txtBODY       .Text  = Sql.ToString(rdr["BODY_HTML"  ]);
											// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
											try
											{
												// 12/05/2008 Paul.  Base module was not being set. 
												// 08/19/2010 Paul.  Check the list before assigning the value. 
												Utils.SetSelectedValue(lstBASE_MODULE, Sql.ToString(rdr["BASE_MODULE"]));
												lstBASE_MODULE_Changed(null, null);
											}
											catch
											{
											}
										}
										else
										{
											// 11/25/2006 Paul.  If item is not visible, then don't allow save 
											// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlDynamicButtons.DisableAll();
											ctlFooterButtons .DisableAll();
											ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
										}
									}
								}
							}
							// 06/16/2010 Paul.  Add support for Workflow Alert Template Attachments. 
							// 06/16/2010 Paul.  Use the same EmailTemplates parent type. 
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								sSQL = "select *                                     " + ControlChars.CrLf
								     + "  from vwEMAIL_TEMPLATES_Attachments         " + ControlChars.CrLf
								     + " where EMAIL_TEMPLATE_ID = @EMAIL_TEMPLATE_ID" + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@EMAIL_TEMPLATE_ID", gID);

								if ( bDebug )
									RegisterClientScriptBlock("vwEMAIL_TEMPLATES_Attachments", Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill(dt);
										ctlAttachments.DataSource = dt.DefaultView;
										ctlAttachments.DataBind();
									}
								}
							}
						}
					}
					else
					{
						// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
					}
				}
				else
				{
					// 12/02/2005 Paul.  When validation fails, the header title does not retain its value.  Update manually. 
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Meeting is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "WorkflowAlertTemplates";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}
