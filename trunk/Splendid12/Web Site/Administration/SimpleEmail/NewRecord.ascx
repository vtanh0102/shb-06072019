<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NewRecord.ascx.cs" Inherits="SplendidCRM.Administration.SimpleEmail.NewRecord" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script runat="server">
/**
 * Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divNewRecord" style="display: none">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderLeft" Src="~/_controls/HeaderLeft.ascx" %>
	<SplendidCRM:HeaderLeft ID="ctlHeaderLeft" Title="Emails.LBL_NEW_FORM_TITLE" Width="100%" Runat="Server" />

	<asp:Panel ID="pnlMain" Width="100%" CssClass="leftColumnModuleS3" runat="server">
		<asp:Table SkinID="tabEditViewButtons" Visible="<%# !PrintView %>" runat="server">
			<asp:TableRow>
				<asp:TableCell ID="tdButtons" Width="10%" Wrap="false">
					<asp:Button CommandName="SendTest" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("Emails.LBL_SEND_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("Emails.LBL_SEND_BUTTON_TITLE") %>' Runat="server" />
					&nbsp;
					<asp:Button UseSubmitBehavior="false" OnClientClick="toggleDisplay('divNewRecord'); return false;" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
				</asp:TableCell>
				<asp:TableCell>
					<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />
				</asp:TableCell>
				<asp:TableCell ID="tdRequired" HorizontalAlign="Right" Wrap="false" Visible="false">
					<asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' Runat="server" />
					&nbsp;
					<asp:Label Text='<%# L10n.Term(".NTC_REQUIRED") %>' Runat="server" />
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Panel ID="pnlEdit" CssClass="" style="margin-bottom: 4px;" Width="100%" runat="server">
			<asp:HiddenField ID="hidREMOVE_LABEL"     Value='<%# L10n.Term(".LBL_REMOVE") %>' runat="server" />
			<asp:HiddenField ID="hidATTACHMENT_COUNT" Value="0" runat="server" />

			<asp:TableCell>
				<asp:Table SkinID="tabEditView" runat="server">
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%= L10n.Term("Emails.LBL_TO") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<asp:TextBox ID="txtTO_ADDRS" TabIndex="0" TextMode="MultiLine" Columns="80" Rows="1" style="overflow-y:auto;" Runat="server" />&nbsp;
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow >
						<asp:TableCell CssClass="dataLabel"><%= L10n.Term("Emails.LBL_FROM") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3"><asp:TextBox ID="txtFROM_ADDR" TabIndex="0" TextMode="MultiLine" Columns="80" Rows="1" style="overflow-y:auto;" Runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%= L10n.Term("Emails.LBL_SUBJECT") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<asp:TextBox ID="txtNAME" TabIndex="0" TextMode="MultiLine" Columns="100" Rows="1" style="overflow-y:auto;" Runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell CssClass="dataLabel"><%= L10n.Term("Emails.LBL_BODY") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" ColumnSpan="3">
							<%@ Register TagPrefix="CKEditor" Namespace="CKEditor.NET" Assembly="CKEditor.NET" %>
							<CKEditor:CKEditorControl id="txtDESCRIPTION" Toolbar="SplendidCRM" Language='<%# Session["USER_SETTINGS/CULTURE"] %>' BasePath="~/ckeditor/" FilebrowserUploadUrl="../../ckeditor/upload.aspx" FilebrowserBrowseUrl="../../Images/Popup.aspx" FilebrowserWindowWidth="640" FilebrowserWindowHeight="480" runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow Visible="false">
						<asp:TableCell CssClass="dataLabel" VerticalAlign="Top"><%= L10n.Term("Emails.LBL_ATTACHMENTS") %></asp:TableCell>
						<asp:TableCell CssClass="dataField" VerticalAlign="Top" ColumnSpan="3">
							<asp:Repeater id="ctlAttachments" runat="server">
								<HeaderTemplate />
								<ItemTemplate>
									<asp:HyperLink Text='<%# DataBinder.Eval(Container.DataItem, "FILENAME") %>' NavigateUrl='<%# "~/Notes/Attachment.aspx?ID=" + DataBinder.Eval(Container.DataItem, "NOTE_ATTACHMENT_ID") %>' Target="_blank" Runat="server" /><br />
								</ItemTemplate>
								<FooterTemplate />
							</asp:Repeater>
							<div id="<%= this.ClientID %>_attachments_div"></div>
							<div style="display: none">
								<input id="dummy_email_attachment" type="file" tabindex="0" size="40" runat="server" />
							</div>
							<asp:Button UseSubmitBehavior="false" OnClientClick=<%# "AddFile(\'" + this.ClientID + "\', \'" + hidREMOVE_LABEL.ClientID + "\', \'" + hidATTACHMENT_COUNT.ClientID + "\'); return false;" %> Text='<%# L10n.Term("Emails.LBL_ADD_FILE") %>' CssClass="button" runat="server" />
							<br />
						</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:TableCell>
		</asp:Panel>
	</asp:Panel>
</div>

