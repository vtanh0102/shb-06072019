/**
 * Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.SimpleEmail
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected NewRecord     ctlNewRecord     ;

		protected DataView      vwMain           ;
		protected SplendidGrid  grdMain          ;
		protected Label         lblError         ;
		protected TextBox       EMAIL_ADDRESS    ;
		protected Label         MAX_24HOUR_SEND  ;
		protected Label         MAX_SEND_RATE    ;
		protected Label         SENT_LAST_24HOURS;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( !Sql.IsEmptyString(AmazonCache.AmazonAccessKeyID()) )
				{
					AmazonSimpleEmail ses = AmazonCache.CreateAmazonSimpleEmail();
					
					if ( e.CommandName == "EmailAddress.Create" )
					{
						EMAIL_ADDRESS.Text = EMAIL_ADDRESS.Text.Trim();
						if ( !Sql.IsEmptyString(EMAIL_ADDRESS.Text) )
						{
							ses.VerifyEmailAddress(EMAIL_ADDRESS.Text);
							EMAIL_ADDRESS.Text = String.Empty;
							
							Cache.Remove("AmazonSimpleEmail.VerifiedEmailAddresses");
							Bind(true);
							lblError.Text = L10n.Term("SimpleMail.LBL_EMAIL_ADDRESS_CREATED");
						}
					}
					else if ( e.CommandName == "EmailAddress.Delete" )
					{
						EMAIL_ADDRESS.Text = EMAIL_ADDRESS.Text.Trim();
						if ( !Sql.IsEmptyString(EMAIL_ADDRESS.Text) )
						{
							ses.DeleteVerifiedEmailAddress(Sql.ToString(e.CommandArgument));
							EMAIL_ADDRESS.Text = String.Empty;
							
							Cache.Remove("AmazonSimpleEmail.VerifiedEmailAddresses");
							Bind(true);
							lblError.Text = L10n.Term("SimpleMail.LBL_EMAIL_ADDRESS_DELETED");
						}
					}
					else if ( e.CommandName == "SendTest" )
					{
						lblError.Text = ses.SendEmail(ctlNewRecord.FROM_ADDR, ctlNewRecord.TO_ADDRS, ctlNewRecord.NAME, ctlNewRecord.DESCRIPTION);
						ctlNewRecord.ClearForm();
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
				Bind(true);
			}
		}

		private void Bind(bool bBind)
		{
			if ( !Sql.IsEmptyString(AmazonCache.AmazonAccessKeyID()) )
			{
				DataTable dt = Cache.Get("AmazonSimpleEmail.VerifiedEmailAddresses") as DataTable;
				if ( dt == null )
				{
					AmazonSimpleEmail ses = AmazonCache.CreateAmazonSimpleEmail();
					dt = ses.ListVerifiedEmailAddresses();
					// 02/23/2011 Paul.  The list is expected to be small, so there is little need to cache the data. 
					//Cache.Insert("AmazonSimpleEmail.VerifiedEmailAddresses", dt, null, AmazonCache.DefaultCacheExpiration(),System.Web.Caching.Cache.NoSlidingExpiration);
					
					double dMax24HourSend   = 0;
					double dMaxSendRate     = 0;
					double dSentLast24Hours = 0;
					ses.GetSendQuota(ref dMax24HourSend, ref dMaxSendRate, ref dSentLast24Hours);
					MAX_24HOUR_SEND  .Text = dMax24HourSend  .ToString();
					MAX_SEND_RATE    .Text = dMaxSendRate    .ToString();
					SENT_LAST_24HOURS.Text = dSentLast24Hours.ToString();
				}
				vwMain = dt.DefaultView;
				grdMain.DataSource = vwMain ;
				if ( bBind )
				{
					grdMain.SortColumn = "EMAIL_ADDRESS";
					grdMain.SortOrder  = "asc" ;
					grdMain.ApplySort();
					grdMain.DataBind();
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				Bind(!IsPostBack);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.ctlNewRecord.Command += new CommandEventHandler(this.Page_Command);
			m_sMODULE = "SimpleEmail";
			SetMenu(m_sMODULE);
		}
		#endregion
	}
}
