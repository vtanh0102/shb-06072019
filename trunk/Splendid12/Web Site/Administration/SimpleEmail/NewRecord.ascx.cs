/**
 * Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
// 09/18/2011 Paul.  Upgrade to CKEditor 3.6.2. 
using CKEditor.NET;

namespace SplendidCRM.Administration.SimpleEmail
{
	/// <summary>
	///		Summary description for NewRecord.
	/// </summary>
	public class NewRecord : SplendidControl
	{
		protected Label           lblError                     ;
		protected Panel           pnlMain                      ;
		protected Panel           pnlEdit                      ;

		public CommandEventHandler Command     ;

		protected TextBox         txtFROM_ADDR                 ;
		protected TextBox         txtTO_ADDRS                  ;
		protected TextBox         txtNAME                      ;
		// 09/18/2011 Paul.  Upgrade to CKEditor 3.6.2. 
		protected CKEditorControl txtDESCRIPTION               ;
		protected Repeater        ctlAttachments               ;
		protected HiddenField     hidREMOVE_LABEL              ;
		protected HiddenField     hidATTACHMENT_COUNT          ;

		public string FROM_ADDR
		{
			get { return txtFROM_ADDR.Text; }
		}

		public string TO_ADDRS
		{
			get { return txtTO_ADDRS.Text; }
		}

		public string NAME
		{
			get { return txtNAME.Text; }
		}

		public string DESCRIPTION
		{
			get { return txtDESCRIPTION.Text; }
		}

		public void ClearForm()
		{
			txtFROM_ADDR  .Text  = String.Empty;
			txtTO_ADDRS   .Text  = String.Empty;
			txtNAME       .Text  = String.Empty;
			txtDESCRIPTION.Text  = String.Empty;
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "SendTest" )
				{
					this.ValidateEditViewFields(m_sMODULE + ".EditView");
					if ( Page.IsValid )
					{
						txtFROM_ADDR.Text  = txtFROM_ADDR.Text.Trim();
						txtTO_ADDRS .Text  = txtTO_ADDRS .Text.Trim();
						if ( txtTO_ADDRS.Text.Length == 0 )
							throw(new Exception(L10n.Term("Emails.ERR_NOT_ADDRESSED")));
						
						/*
						foreach ( string sHTML_FIELD_NAME in Request.Files.AllKeys )
						{
							if ( sHTML_FIELD_NAME.StartsWith(this.ClientID + "_attachment") )
							{
								HttpPostedFile pstATTACHMENT = Request.Files[sHTML_FIELD_NAME];
								if ( pstATTACHMENT != null )
								{
									long lFileSize      = pstATTACHMENT.ContentLength;
									long lUploadMaxSize = Sql.ToLong(Application["CONFIG.upload_maxsize"]);
									if ( (lUploadMaxSize > 0) && (lFileSize > lUploadMaxSize) )
									{
										throw(new Exception("ERROR: uploaded file was too big: max filesize: " + lUploadMaxSize.ToString()));
									}
									if ( pstATTACHMENT.FileName.Length > 0 )
									{
										string sFILENAME       = Path.GetFileName (pstATTACHMENT.FileName);
										string sFILE_EXT       = Path.GetExtension(sFILENAME);
										string sFILE_MIME_TYPE = pstATTACHMENT.ContentType;
									}
								}
							}
						}
						*/
						e = new CommandEventArgs(e.CommandName, String.Empty);
						if ( Command != null )
							Command(sender, e);
					}
				}
				else if ( Command != null )
				{
					Command(sender, e);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = (SplendidCRM.Security.GetUserAccess("SimpleEmail", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				bool bIsPostBack = this.IsPostBack && !NotPostBack;
				if ( !bIsPostBack )
				{
					if ( NotPostBack )
						this.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "Emails";
		}
		#endregion
	}
}

