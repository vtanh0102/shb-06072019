<%@ Control CodeBehind="StatisticsView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.SimpleEmail.StatisticsView" %>
<script runat="server">
/**
 * Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divListView">
	<%@ Register TagPrefix="SplendidCRM" Tagname="ModuleHeader" Src="~/_controls/ModuleHeader.ascx" %>
	<SplendidCRM:ModuleHeader ID="ctlModuleHeader" Module="SimpleEmail" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" Runat="Server" />
	<br />
	<%@ Register TagPrefix="SplendidCRM" Tagname="ListHeader" Src="~/_controls/ListHeader.ascx" %>
	<SplendidCRM:ListHeader Module="SimpleEmail" Title="SimpleEmail.LBL_SEND_STATISTICS" Runat="Server" />
	
	<asp:Label ID="lblError" ForeColor="Red" EnableViewState="false" Runat="server" />
	<SplendidCRM:SplendidGrid id="grdMain" Width="100%" CssClass="listView"
		CellPadding="3" CellSpacing="0" border="0"
		AllowPaging="<%# !PrintView %>" PageSize='<%# Math.Max(Sql.ToInteger(HttpContext.Current.Application["CONFIG.list_max_entries_per_page"]), 5) %>' AllowSorting="true" 
		AutoGenerateColumns="false" 
		EnableViewState="true" runat="server">
		<ItemStyle            CssClass="oddListRowS1"  />
		<AlternatingItemStyle CssClass="evenListRowS1" />
		<HeaderStyle          CssClass="listViewThS1"  />
		<PagerStyle HorizontalAlign="Right" Mode="NextPrev" PageButtonCount="6" Position="Top" CssClass="listViewPaginationTdS1" PrevPageText=".LNK_LIST_PREVIOUS" NextPageText=".LNK_LIST_NEXT" />
		<Columns>
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_BOUNCES"           DataField="BOUNCES"           SortExpression="BOUNCES"           ItemStyle-Width="20%" />
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_COMPLAINTS"        DataField="COMPLAINTS"        SortExpression="COMPLAINTS"        ItemStyle-Width="20%" />
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_DELIVERY_ATTEMPTS" DataField="DELIVERY_ATTEMPTS" SortExpression="DELIVERY_ATTEMPTS" ItemStyle-Width="20%" />
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_REJECTS"           DataField="REJECTS"           SortExpression="REJECTS"           ItemStyle-Width="20%" />
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_TIMESTAMP"         DataField="TIMESTAMP"         SortExpression="TIMESTAMP"         ItemStyle-Width="20%" />
		</Columns>
	</SplendidCRM:SplendidGrid>
	<br />
</div>
