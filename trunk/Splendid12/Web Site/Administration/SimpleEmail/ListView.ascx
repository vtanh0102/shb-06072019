<%@ Control CodeBehind="ListView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.SimpleEmail.ListView" %>
<script runat="server">
/**
 * Copyright (C) 2011-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<script type="text/javascript">
function ShowSendTest(sEMAIL_ADDRESS)
{
	var fld = document.getElementById('divNewRecord');
	if ( fld != undefined )
	{
		fld.style.display = 'inline';
	}
	var txtFROM_ADDR = document.getElementById('<%= ctlNewRecord.FindControl("txtFROM_ADDR").ClientID %>');
	txtFROM_ADDR.value = sEMAIL_ADDRESS;
	return false;
}
</script>
<div id="divListView">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlModuleHeader" Module="SimpleEmail" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" Runat="Server" />
	<br />
	<%@ Register TagPrefix="SplendidCRM" Tagname="ListHeader" Src="~/_controls/ListHeader.ascx" %>
	<SplendidCRM:ListHeader Module="SimpleEmail" Title="SimpleEmail.LBL_LIST_FORM_TITLE" Runat="Server" />
	
	<asp:Table Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' Width="100%" CellPadding="0" CellSpacing="0" CssClass="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<asp:Table Width="100%" CellPadding="0" CellSpacing="0" runat="server">
					<asp:TableRow>
						<asp:TableCell Width="60%" CssClass="dataLabel" Wrap="false" VerticalAlign="Top" RowSpan="4">
							<asp:TextBox ID="EMAIL_ADDRESS" CssClass="dataField" size="40" Runat="server" />
							&nbsp;
							<asp:Button CommandName="EmailAddress.Create" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("SimpleEmail.LBL_CREATE_EMAIL_ADDRESS") %>' ToolTip='<%# L10n.Term("SimpleEmail.LBL_CREATE_EMAIL_ADDRESS") %>' Runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="20%"><asp:Label Text='<%# L10n.Term("SimpleEmail.LBL_MAX_24HOUR_SEND"  ) %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="20%"><asp:Label ID="MAX_24HOUR_SEND"   runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="20%"><asp:Label Text='<%# L10n.Term("SimpleEmail.LBL_MAX_SEND_RATE"    ) %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="20%"><asp:Label ID="MAX_SEND_RATE"     runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="20%"><asp:Label Text='<%# L10n.Term("SimpleEmail.LBL_SENT_LAST_24HOURS") %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="20%"><asp:Label ID="SENT_LAST_24HOURS" runat="server" /></asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<br />
	
	<%@ Register TagPrefix="SplendidCRM" Tagname="NewRecord" Src="NewRecord.ascx" %>
	<SplendidCRM:NewRecord ID="ctlNewRecord" Runat="Server" />

	<asp:Label ID="lblError" ForeColor="Red" EnableViewState="false" Runat="server" />
	<SplendidCRM:SplendidGrid id="grdMain" Width="100%" CssClass="listView"
		CellPadding="3" CellSpacing="0" border="0"
		AllowPaging="<%# !PrintView %>" PageSize='<%# Math.Max(Sql.ToInteger(HttpContext.Current.Application["CONFIG.list_max_entries_per_page"]), 5) %>' AllowSorting="true" 
		AutoGenerateColumns="false" 
		EnableViewState="true" runat="server">
		<ItemStyle            CssClass="oddListRowS1"  />
		<AlternatingItemStyle CssClass="evenListRowS1" />
		<HeaderStyle          CssClass="listViewThS1"  />
		<PagerStyle HorizontalAlign="Right" Mode="NextPrev" PageButtonCount="6" Position="Top" CssClass="listViewPaginationTdS1" PrevPageText=".LNK_LIST_PREVIOUS" NextPageText=".LNK_LIST_NEXT" />
		<Columns>
			<asp:BoundColumn     HeaderText="SimpleEmail.LBL_LIST_EMAIL_ADDRESS" DataField="EMAIL_ADDRESS" SortExpression="EMAIL_ADDRESS" ItemStyle-Width="25%" />
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:LinkButton OnClientClick=<%# "return ShowSendTest(\'" + Sql.ToString(Eval("EMAIL_ADDRESS")) + "\');" %> Visible='<%# SplendidCRM.Security.AdminUserAccess("SimpleEmail", "edit") >= 0 %>' CssClass="listViewTdToolsS1" Text='<%# L10n.Term("SimpleEmail.LNK_SEND_TEST") %>' Runat="server" />
					&nbsp;
					<span onclick="return confirm('<%= L10n.TermJavaScript(".NTC_DELETE_CONFIRMATION") %>')">
						<asp:ImageButton Visible='<%# SplendidCRM.Security.AdminUserAccess("SimpleEmail", "delete") >= 0 %>' CommandName="EmailAddress.Delete" CommandArgument='<%# Eval("EMAIL_ADDRESS") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' ImageUrl='<%# Session["themeURL"] + "images/delete_inline.gif" %>' BorderWidth="0" Width="12" Height="12" ImageAlign="AbsMiddle" Runat="server" />
						<asp:LinkButton  Visible='<%# SplendidCRM.Security.AdminUserAccess("SimpleEmail", "delete") >= 0 %>' CommandName="EmailAddress.Delete" CommandArgument='<%# Eval("EMAIL_ADDRESS") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_DELETE") %>' Runat="server" />
					</span>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
	<br />
</div>
