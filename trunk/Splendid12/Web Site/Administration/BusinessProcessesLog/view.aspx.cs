/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.BusinessProcessesLog
{
	/// <summary>
	/// Summary description for View.
	/// </summary>
	public class View : SplendidPage
	{
		protected Label     lblError                        ;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Guid gID = Sql.ToGuid(Request["ID"]);
				if ( !Sql.IsEmptyGuid(gID) )
				{
					Guid   gAUDIT_ID    = Guid.Empty;
					string sAUDIT_TABLE = String.Empty;
					Guid   gITEM_ID     = Guid.Empty;
					string sMODULE      = String.Empty;
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL ;
						sSQL = "select *                                " + ControlChars.CrLf
						     + "  from vwBUSINESS_PROCESSES_RUN_EventLog" + ControlChars.CrLf
						     + " where ID = @ID                         " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gID);

							if ( bDebug )
								Page.ClientScript.RegisterClientScriptBlock(System.Type.GetType("System.String"), "SQLCode", Sql.ClientScriptBlock(cmd));

							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									gAUDIT_ID    = Sql.ToGuid  (rdr["AUDIT_ID"   ]);
									sAUDIT_TABLE = Sql.ToString(rdr["AUDIT_TABLE"]);
								}
								else
								{
									lblError.Text = "Process not found with ID " + gID.ToString();
									return;
								}
							}
						}
						if ( sAUDIT_TABLE.EndsWith("_AUDIT") )
						{
							sMODULE = Sql.ToString(Application["Modules." + sAUDIT_TABLE.Replace("_AUDIT", String.Empty) + ".ModuleName"]);
							sSQL = "select ID                  " + ControlChars.CrLf
							     + "  from vw" + sAUDIT_TABLE      + ControlChars.CrLf
							     + " where AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@AUDIT_ID", gAUDIT_ID);
								gITEM_ID = Sql.ToGuid(cmd.ExecuteScalar());
							}
						}
						else
						{
							sMODULE = Sql.ToString(Application["Modules." + sAUDIT_TABLE + ".ModuleName"]);
							gITEM_ID = gAUDIT_ID;
						}
						Response.Redirect("~/" + sMODULE + "/view.aspx?ID="+ gITEM_ID.ToString() );
					}
				}
				if ( !IsPostBack )
				{
					// 06/09/2006 Paul.  The primary data binding will now only occur in the ASPX pages so that this is only one per cycle. 
					// 03/11/2008 Paul.  Move the primary binding to SplendidPage. 
					//Page DataBind();
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

