<%@ Control CodeBehind="ListView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.BusinessProcessesLog.ListView" %>
<script runat="server">
/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */

</script>
<script type="text/javascript">
function BusinessProcessesLogPopup(sID)
{
	window.open('Popup.aspx?ID=' + escape(sID), 'BusinessProcessesLogPopup', '<%= SplendidCRM.Crm.Config.PopupWindowOptions() %>');
	return false;
}
</script>
<div id="divListView">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlModuleHeader" Module="BusinessProcessesLog" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="SearchView" Src="~/_controls/SearchView.ascx" %>
	<SplendidCRM:SearchView ID="ctlSearchView" Module="BusinessProcessesLog" ShowSearchTabs="false" ShowSearchViews="false" Visible="<%# !PrintView %>" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="ExportHeader" Src="~/_controls/ExportHeader.ascx" %>
	<SplendidCRM:ExportHeader ID="ctlExportHeader" Module="Administration" Title="" Runat="Server" />
	
	<asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
		<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />
	</asp:Panel>
	
	<SplendidCRM:SplendidGrid id="grdMain" SkinID="grdSubPanelView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
		<Columns>
			<asp:TemplateColumn  HeaderText="" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 && !Sql.IsEmptyGuid(Eval("ID")) %>' NavigateUrl='<%# "~/Administration/BusinessProcessesLog/view.aspx?ID=" + Eval("ID") %>' CssClass="listViewTdToolsS1" Runat="server">
						<asp:Image SkinID="view_inline" AlternateText='<%# L10n.Term("BusinessProcessesLog.LNK_VIEW_RECORD") %>' Runat="server" />&nbsp;<%# L10n.Term("BusinessProcessesLog.LNK_VIEW_RECORD") %>
					</asp:HyperLink>
					<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 && !Sql.IsEmptyGuid(Eval("BUSINESS_PROCESS_INSTANCE_ID")) %>' NavigateUrl="#" onclick=<%# "return BusinessProcessesLogPopup(\'" + Eval("BUSINESS_PROCESS_INSTANCE_ID") + "\');" %> CssClass="listViewTdToolsS1" Runat="server">
						<asp:Image SkinID="view_inline" AlternateText='<%# L10n.Term("BusinessProcessesLog.LNK_VIEW_LOG") %>' Runat="server" />&nbsp;<%# L10n.Term("BusinessProcessesLog.LNK_VIEW_LOG") %>
					</asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn  HeaderText="" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<span onclick="return confirm('<%= L10n.TermJavaScript(".NTC_DELETE_CONFIRMATION") %>')">
						<asp:ImageButton Visible='<%# Sql.ToBoolean(Eval("ALLOW_TERMINATION")) %>' CommandName="Terminate" CommandArgument='<%# Eval("BUSINESS_PROCESS_INSTANCE_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' SkinID="delete_inline" Runat="server" />
						<asp:LinkButton  Visible='<%# Sql.ToBoolean(Eval("ALLOW_TERMINATION")) %>' CommandName="Terminate" CommandArgument='<%# Eval("BUSINESS_PROCESS_INSTANCE_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term("BusinessProcesses.LNK_TERMINATE") %>' Runat="server" />
					</span>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>

	<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
	<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
</div>
