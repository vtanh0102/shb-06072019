<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.WorkflowActionShells.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="SplendidCRM" Tagname="DatePicker" Src="~/_controls/DatePicker.ascx" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
<asp:UpdatePanel UpdateMode="Conditional" runat="server">
	<ContentTemplate>
		<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
		<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
		<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
		<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Workflows" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top" Wrap="false"><%# L10n.Term("WorkflowActionShells.LBL_NAME") %> <asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' Runat="server" /></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField">
								<asp:TextBox ID="txtNAME" TabIndex="1" size="60" MaxLength="50" Runat="server" />
								&nbsp;<asp:RequiredFieldValidator ID="reqNAME" ControlToValidate="txtNAME" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableClientScript="false" EnableViewState="false" Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top" Wrap="false"><%# L10n.Term("WorkflowActionShells.LBL_BASE_MODULE") %></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField">
								<asp:Label ID="txtBASE_MODULE" Runat="server" />
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<h4><asp:Label Text='<%# L10n.Term("WorkflowActionShells.LBL_ACTION_TYPES") %>' runat="server" /></h4>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<asp:Label Text='<%# L10n.Term("WorkflowActionShells.LBL_SHOW_XOML") %>' CssClass="dataLabel" runat="server" />&nbsp;
					<asp:CheckBox ID="chkSHOW_XOML" CssClass="checkbox" AutoPostBack="true" runat="server" />
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell style="padding-top: 5px; padding-bottom: 5px;">
					<asp:DataGrid ID="dgFilters" AutoGenerateColumns="false"
						CellPadding="3" CellSpacing="0" 
						AllowPaging="false" AllowSorting="false" ShowHeader="true"
						EnableViewState="true" runat="server">
						<Columns>
							<asp:BoundColumn HeaderText="Action"   DataField="ACTION_TYPE"       />
							<asp:BoundColumn HeaderText="Rel Name" DataField="RELATIONSHIP_NAME" />
							<asp:BoundColumn HeaderText="Module"   DataField="MODULE_NAME"       />
							<asp:BoundColumn HeaderText="Field"    DataField="DATA_FIELD"        />
							<asp:BoundColumn HeaderText="Type"     DataField="DATA_TYPE"         />
							<asp:BoundColumn HeaderText="Operator" DataField="OPERATOR"          />
							<asp:BoundColumn HeaderText="Value"    DataField="SEARCH_TEXT"       />
							<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
								<ItemTemplate>
									<asp:Button ID="btnEditFilter" CommandName="Filters.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_EDIT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_EDIT_BUTTON_TITLE") %>' Runat="server" />
									&nbsp;
									<asp:Button ID="btnDeleteFilter" CommandName="Filters.Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_TITLE") %>' Runat="server" />
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:DataGrid>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<input id="txtFILTER_ID" type="hidden" runat="server" />
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstACTION_TYPE" TabIndex="8" OnSelectedIndexChanged="lstACTION_TYPE_Changed" AutoPostBack="true" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstRELATED" TabIndex="9" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstRELATED_Changed" AutoPostBack="true" Runat="server" />
								<asp:TextBox ID="txtCUSTOM_ACTIVITY_NAME" TabIndex="9" Width="200px" Visible="false" Runat="server" />
								&nbsp;<asp:Label ID="reqCUSTOM_ACTIVITY_NAME" CssClass="required" EnableViewState="false" Runat="server" />
								<br />
								<asp:Label ID="lblRELATED" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top" Visible="false">
								<asp:DropDownList ID="lstFILTER_COLUMN_SOURCE" TabIndex="10" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_SOURCE_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_COLUMN_SOURCE" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_COLUMN" TabIndex="11" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_Changed" AutoPostBack="true" Runat="server" />
								<asp:TextBox ID="txtCUSTOM_COLUMN_NAME" TabIndex="9" Width="150px" Visible="false" Runat="server" />
								&nbsp;<asp:Label ID="reqCUSTOM_COLUMN_NAME" CssClass="required" EnableViewState="false" Runat="server" />
								<br />
								<asp:Label ID="lblFILTER_COLUMN" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_OPERATOR" Enabled="false" TabIndex="12" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_OPERATOR_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_OPERATOR_TYPE" Enabled="false" runat="server" /><asp:Image SkinID="Spacer" Width="4" runat="server" />
								<asp:Label ID="lblFILTER_OPERATOR" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top" Wrap="false">
								<asp:Table BorderWidth="0" CellSpacing="0" CellPadding="0" runat="server">
									<asp:TableRow>
										<asp:TableCell>
											<input type="hidden" id="txtFILTER_SEARCH_ID" runat="server" />
											<input type="hidden" id="txtFILTER_SEARCH_DATA_TYPE" runat="server" />
											
											<asp:TextBox ID="txtFILTER_SEARCH_TEXT" Width="200px" runat="server" />
											
											<asp:DropDownList ID="lstFILTER_SEARCH_DROPDOWN" DataValueField="NAME" DataTextField="DISPLAY_NAME" runat="server" />
											<asp:ListBox      ID="lstFILTER_SEARCH_LISTBOX"  DataValueField="NAME" DataTextField="DISPLAY_NAME" SelectionMode="Multiple" runat="server" />
											
											<SplendidCRM:DatePicker ID="ctlFILTER_SEARCH_START_DATE" EnableDateFormat="false" Runat="Server" />
										</asp:TableCell>
										<asp:TableCell>
											<asp:Label ID="lblFILTER_AND_SEPARATOR" Text='<%# L10n.Term("Schedulers.LBL_AND") %>' runat="server" />
										</asp:TableCell>
										<asp:TableCell>
											<SplendidCRM:DatePicker ID="ctlFILTER_SEARCH_END_DATE" EnableDateFormat="false" Runat="Server" />
											
											<asp:TextBox ID="txtFILTER_SEARCH_TEXT2"  runat="server" />
											
											<asp:Button ID="btnFILTER_SEARCH_SELECT" Visible="false" UseSubmitBehavior="false" OnClientClick="SearchPopup(); return false;" CssClass="button" Text='<%# L10n.Term(".LBL_SELECT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_SELECT_BUTTON_TITLE") %>' runat="server" />
										</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
								<asp:Label ID="lblFILTER_ID" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button CommandName="Filters.Update" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_UPDATE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_UPDATE_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button CommandName="Filters.Cancel" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="80%"></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Literal ID="litWORKFLOW_XML" runat="server" />
		<asp:Image ImageUrl="Image.aspx" runat="server" />
		<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
		<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
		<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
		<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
	</ContentTemplate>
</asp:UpdatePanel>
</div>
