/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.Workflow.Runtime;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;

namespace SplendidCRM.Administration.WorkflowActionShells
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 01/13/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                        ;
		protected Guid            gPARENT_ID                 ;
		protected string          sWORKFLOW_TYPE             ;

		protected RdlDocument     rdl                = null  ;
		protected XomlDocument    xoml               = null  ;
		protected TextBox         txtNAME                    ;
		protected Label           txtBASE_MODULE             ;
		protected CheckBox        chkSHOW_XOML               ;
		protected RequiredFieldValidator reqNAME      ;

		protected string          sWorkflowXOML              ;
		protected DataGrid        dgFilters                  ;
		protected HtmlInputHidden txtFILTER_ID               ;
		protected DropDownList    lstACTION_TYPE             ;
		protected DropDownList    lstRELATED                 ;
		protected TextBox         txtCUSTOM_ACTIVITY_NAME    ;
		protected Label           reqCUSTOM_ACTIVITY_NAME    ;
		protected DropDownList    lstFILTER_COLUMN_SOURCE    ;
		protected DropDownList    lstFILTER_COLUMN           ;
		protected TextBox         txtCUSTOM_COLUMN_NAME      ;
		protected Label           reqCUSTOM_COLUMN_NAME      ;
		protected DropDownList    lstFILTER_OPERATOR         ;
		protected Label           lblRELATED                 ;
		protected Label           lblFILTER_COLUMN_SOURCE    ;
		protected Label           lblFILTER_COLUMN           ;
		protected Label           lblFILTER_OPERATOR_TYPE    ;
		protected Label           lblFILTER_OPERATOR         ;
		protected Label           lblFILTER_ID               ;
		
		protected HtmlInputHidden txtFILTER_SEARCH_ID        ;
		protected HtmlInputHidden txtFILTER_SEARCH_DATA_TYPE ;
		protected TextBox         txtFILTER_SEARCH_TEXT      ;
		protected TextBox         txtFILTER_SEARCH_TEXT2     ;
		protected DropDownList    lstFILTER_SEARCH_DROPDOWN  ;
		protected ListBox         lstFILTER_SEARCH_LISTBOX   ;
		protected Button          btnFILTER_SEARCH_SELECT    ;
		protected Label           lblFILTER_AND_SEPARATOR    ;

		protected _controls.DatePicker ctlFILTER_SEARCH_START_DATE;
		protected _controls.DatePicker ctlFILTER_SEARCH_END_DATE  ;

		protected Literal         litWORKFLOW_XML            ;

		public string BaseModule
		{
			get
			{
				return txtBASE_MODULE.Text;
			}
		}

		protected void ResetSearchText()
		{
			lstFILTER_COLUMN_SOURCE.SelectedIndex = 0;
			lstFILTER_COLUMN_SOURCE_Changed(null, null);
			lstFILTER_COLUMN.SelectedIndex = 0;
			lstFILTER_COLUMN_Changed(null, null);
			lstFILTER_OPERATOR.SelectedIndex = 0;
			lstFILTER_OPERATOR_Changed(null, null);

			txtFILTER_ID               .Value    = String.Empty;
			lblFILTER_ID               .Text     = String.Empty;
			txtFILTER_SEARCH_TEXT      .Text     = String.Empty;
			txtFILTER_SEARCH_TEXT2     .Text     = String.Empty;
			ctlFILTER_SEARCH_START_DATE.DateText = String.Empty;
			ctlFILTER_SEARCH_END_DATE  .DateText = String.Empty;

			// 08/03/2012 Paul.  Should have been clearing the custom activity data long ago. 
			txtCUSTOM_ACTIVITY_NAME    .Text     = String.Empty;
			txtCUSTOM_COLUMN_NAME      .Text     = String.Empty;
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				gPARENT_ID = Sql.ToGuid(ViewState["PARENT_ID"]);
				if ( e.CommandName == "Save" )
				{
					if ( Page.IsValid )
					{
						BuildWorkflowXOML();
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									string sACTION_TYPE = lstACTION_TYPE.SelectedValue;
									SqlProcs.spWORKFLOW_ACTION_SHELLS_Update(ref gID, gPARENT_ID, txtNAME.Text, sACTION_TYPE, rdl.OuterXml, xoml.OuterXml, trn);
									WorkflowBuilder.UpdateMasterWorkflowXoml(Application, gPARENT_ID, trn);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
						}
						Response.Redirect("~/Administration/Workflows/view.aspx?ID=" + gPARENT_ID.ToString());
					}
				}
				else if ( e.CommandName == "Cancel" )
				{
					Response.Redirect("~/Administration/Workflows/view.aspx?ID=" + gPARENT_ID.ToString());
				}
				else if ( e.CommandName == "Filters.Cancel" )
				{
					ctlDynamicButtons.ErrorText = "";
					ResetSearchText();
				}
				else if ( e.CommandName == "Filters.Delete" )
				{
					ctlDynamicButtons.ErrorText = "";
					FiltersDelete(Sql.ToString(e.CommandArgument));
					BuildWorkflowXOML();
					ResetSearchText();
				}
				else if ( e.CommandName == "Filters.Edit" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sFILTER_ID         = Sql.ToString(e.CommandArgument);
					string sACTION_TYPE       = String.Empty;
					string sRELATIONSHIP_NAME = String.Empty;
					string sMODULE_NAME       = String.Empty;
					string sDATA_FIELD        = String.Empty;
					string sDATA_TYPE         = String.Empty;
					string sOPERATOR          = String.Empty;
					string sSEARCH_TEXT1      = String.Empty;
					string sSEARCH_TEXT2      = String.Empty;
					string[] arrSEARCH_TEXT = new string[0];
					FiltersGet(sFILTER_ID, ref sACTION_TYPE, ref sRELATIONSHIP_NAME, ref sMODULE_NAME, ref sDATA_FIELD, ref sDATA_TYPE, ref sOPERATOR, ref arrSEARCH_TEXT );
					txtFILTER_ID               .Value    = sFILTER_ID;
					lblFILTER_ID               .Text     = txtFILTER_ID.Value;
					txtFILTER_SEARCH_DATA_TYPE .Value    = sDATA_TYPE;
					txtFILTER_SEARCH_TEXT      .Text     = String.Empty;
					txtFILTER_SEARCH_TEXT2     .Text     = String.Empty;
					ctlFILTER_SEARCH_START_DATE.DateText = String.Empty;
					ctlFILTER_SEARCH_END_DATE  .DateText = String.Empty;
					
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstACTION_TYPE, sACTION_TYPE);
					lstACTION_TYPE_Changed(null, null);
					
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					// 11/01/2010 Paul.  Add Custom Activity Action Type. 
					// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
					if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
						txtCUSTOM_ACTIVITY_NAME.Text = sRELATIONSHIP_NAME;
					else
						Utils.SetSelectedValue(lstRELATED, sRELATIONSHIP_NAME);
					txtCUSTOM_ACTIVITY_NAME.Visible = (lstACTION_TYPE.SelectedValue.StartsWith("custom"));
					lstRELATED.Visible = !txtCUSTOM_ACTIVITY_NAME.Visible;
					lstRELATED_Changed(null, null);
					
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstFILTER_COLUMN_SOURCE, sMODULE_NAME);
					lstFILTER_COLUMN_SOURCE_Changed(null, null);
					
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					// 11/01/2010 Paul.  Add Custom Activity Action Type. 
					// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
					// 08/03/2012 Paul.  Add Custom Stored Procedure. 
					if ( lstACTION_TYPE.SelectedValue == "custom_procedure" )
						txtCUSTOM_COLUMN_NAME.Text = sDATA_FIELD;
					else if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
						txtCUSTOM_COLUMN_NAME.Text = sDATA_FIELD;
					else
						Utils.SetSelectedValue(lstFILTER_COLUMN       , sDATA_FIELD );
					txtCUSTOM_COLUMN_NAME.Visible = (lstACTION_TYPE.SelectedValue.StartsWith("custom"));
					reqCUSTOM_COLUMN_NAME.Enabled = txtCUSTOM_COLUMN_NAME.Visible;
					lstFILTER_COLUMN.Visible = !txtCUSTOM_COLUMN_NAME.Visible;
					
					lstFILTER_COLUMN_Changed(null, null);
					// 08/19/2010 Paul.  Check the list before assigning the value. 
					Utils.SetSelectedValue(lstFILTER_OPERATOR     , sOPERATOR   );
					lstFILTER_OPERATOR_Changed(null, null);
					
					if ( arrSEARCH_TEXT.Length > 0 )
						sSEARCH_TEXT1 = arrSEARCH_TEXT[0];
					if ( arrSEARCH_TEXT.Length > 1 )
						sSEARCH_TEXT2 = arrSEARCH_TEXT[1];
					
					// 07/06/2007 Paul.  ansistring is treated the same as string. 
					string sCOMMON_DATA_TYPE = sDATA_TYPE;
					if ( sCOMMON_DATA_TYPE == "ansistring" )
						sCOMMON_DATA_TYPE = "string";
					switch ( sCOMMON_DATA_TYPE )
					{
						case "string":
						{
							switch ( sOPERATOR )
							{
								case "equals"        :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "contains"      :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "starts_with"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "ends_with"     :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "not_equals_str":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
								// 08/25/2011 Paul.  A customer wants more use of NOT in string filters. 
								case "not_contains"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "not_starts_with":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "not_ends_with"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								// 02/14/2013 Paul.  A customer wants to use like in string filters. 
								case "like"           :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "not_like"       :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less"           :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "less_equal"     :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater"        :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater_equal"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
							}
							break;
						}
						case "datetime":
						{
							/*
							if ( arrSEARCH_TEXT.Length > 0 )
							{
								DateTime dtSEARCH_TEXT1 = DateTime.MinValue;
								DateTime dtSEARCH_TEXT2 = DateTime.MinValue;
								if ( !(sOPERATOR.EndsWith("_after") || sOPERATOR.EndsWith("_before") || sOPERATOR.EndsWith("_old")) )
								{
									dtSEARCH_TEXT1 = DateTime.ParseExact(sSEARCH_TEXT1, "yyyy/MM/dd", Thread.CurrentThread.CurrentCulture.DateTimeFormat);
									dtSEARCH_TEXT2 = DateTime.MinValue;
									if ( arrSEARCH_TEXT.Length > 1 )
										dtSEARCH_TEXT2 = DateTime.ParseExact(sSEARCH_TEXT2, "yyyy/MM/dd", Thread.CurrentThread.CurrentCulture.DateTimeFormat);
								}
								switch ( sOPERATOR )
								{
									case "on"               :  ctlFILTER_SEARCH_START_DATE.DateText = dtSEARCH_TEXT1.ToShortDateString();  break;
									case "before"           :  ctlFILTER_SEARCH_START_DATE.DateText = dtSEARCH_TEXT1.ToShortDateString();  break;
									case "after"            :  ctlFILTER_SEARCH_START_DATE.DateText = dtSEARCH_TEXT1.ToShortDateString();  break;
									case "not_equals_str"   :  ctlFILTER_SEARCH_START_DATE.DateText = dtSEARCH_TEXT1.ToShortDateString();  break;
									case "between_dates"    :
										ctlFILTER_SEARCH_START_DATE.DateText = dtSEARCH_TEXT1.ToShortDateString();
										if ( arrSEARCH_TEXT.Length > 1 )
											ctlFILTER_SEARCH_END_DATE  .DateText = dtSEARCH_TEXT2.ToShortDateString();
										break;
									case "empty"            :  break;
									case "not_empty"        :  break;
									case "is_before"        :  break;
									case "is_after"         :  break;
									case "tp_yesterday"     :  break;
									case "tp_today"         :  break;
									case "tp_tomorrow"      :  break;
									case "tp_last_7_days"   :  break;
									case "tp_next_7_days"   :  break;
									case "tp_last_month"    :  break;
									case "tp_this_month"    :  break;
									case "tp_next_month"    :  break;
									case "tp_last_30_days"  :  break;
									case "tp_next_30_days"  :  break;
									case "tp_last_year"     :  break;
									case "tp_this_year"     :  break;
									case "tp_next_year"     :  break;
									case "changed"          :  break;
									case "unchanged"        :  break;
									case "increased"        :  break;
									case "decreased"        :  break;
									// 11/16/2008 Paul.  Days old 
									case "tp_minutes_after" :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_hours_after"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_days_after"    :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_weeks_after"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_months_after"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_years_after"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_minutes_before":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_hours_before"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_days_before"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_weeks_before"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_months_before" :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_years_before"  :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									// 12/04/2008 Paul.  We need to be able to do an an equals. 
									case "tp_days_old"      :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_weeks_old"     :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_months_old"    :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
									case "tp_years_old"     :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								}
							}
							*/
							break;
						}
						case "int32":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "less"      :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "between"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  txtFILTER_SEARCH_TEXT2.Text = sSEARCH_TEXT2;  break;
								case "not_equals":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater_equal":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
							}
							break;
						}
						case "decimal":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "less"      :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "between"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  txtFILTER_SEARCH_TEXT2.Text = sSEARCH_TEXT2;  break;
								case "not_equals":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater_equal":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
							}
							break;
						}
						case "float":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "less"      :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "between"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  txtFILTER_SEARCH_TEXT2.Text = sSEARCH_TEXT2;  break;
								case "not_equals":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"   :  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
								case "greater_equal":  txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;  break;
							}
							break;
						}
						case "bool":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :
									try
									{
										// 12/20/2006 Paul.  Catch and ignore the exception. 
										// 08/19/2010 Paul.  Check the list before assigning the value. 
										Utils.SetSelectedValue(lstFILTER_SEARCH_DROPDOWN, sSEARCH_TEXT1);
									}
									catch
									{
									}
									break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
							}
							break;
						}
						case "guid":
						{
							switch ( sOPERATOR )
							{
								case "is"            :  txtFILTER_SEARCH_ID  .Value = sSEARCH_TEXT1;  break;
								case "equals"        :  txtFILTER_SEARCH_TEXT.Text  = sSEARCH_TEXT1;  break;
								case "contains"      :  txtFILTER_SEARCH_TEXT.Text  = sSEARCH_TEXT1;  break;
								case "starts_with"   :  txtFILTER_SEARCH_TEXT.Text  = sSEARCH_TEXT1;  break;
								case "ends_with"     :  txtFILTER_SEARCH_TEXT.Text  = sSEARCH_TEXT1;  break;
								case "not_equals_str":  txtFILTER_SEARCH_TEXT.Text  = sSEARCH_TEXT1;  break;
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
							}
							break;
						}
						case "enum":
						{
							switch ( sOPERATOR )
							{
								case "is"            :
									try
									{
										// 12/20/2006 Paul.  Catch and ignore the exception. 
										// 08/19/2010 Paul.  Check the list before assigning the value. 
										Utils.SetSelectedValue(lstFILTER_SEARCH_DROPDOWN, sSEARCH_TEXT1);
									}
									catch
									{
									}
									break;
								case "one_of":
								{
									foreach ( string s in arrSEARCH_TEXT )
									{
										for ( int i = 0; i < lstFILTER_SEARCH_LISTBOX.Items.Count; i++ )
										{
											if ( s == lstFILTER_SEARCH_LISTBOX.Items[i].Value )
												lstFILTER_SEARCH_LISTBOX.Items[i].Selected = true;
										}
									}
									break;
								}
								case "custom":
								{
									// 09/13/2011 Paul.  The operator within a Workflow Action is disabled because we can only do an assignment here. 
									// An exception is the Enum as we want to allow an ActivityBind. 
									txtFILTER_SEARCH_TEXT.Text = sSEARCH_TEXT1;
									break;
								}
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
							}
							break;
						}
					}
				}
				else if ( e.CommandName == "Filters.Update" )
				{
					ctlDynamicButtons.ErrorText = "";
					string sFILTER_ID         = txtFILTER_ID.Value;
					string sACTION_TYPE       = lstACTION_TYPE.SelectedValue;
					string sRELATIONSHIP_NAME = String.Empty;
					string sMODULE_NAME       = String.Empty;
					string sDATA_FIELD        = String.Empty;
					string sDATA_TYPE         = txtFILTER_SEARCH_DATA_TYPE.Value;
					string sOPERATOR          = lstFILTER_OPERATOR     .SelectedValue;

					// 11/01/2010 Paul.  Add Custom Activity Action Type. 
					// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
					// 08/03/2012 Paul.  Add Custom Stored Procedure. 
					if ( lstACTION_TYPE.SelectedValue == "custom_procedure" )
					{
						sDATA_FIELD        = txtCUSTOM_COLUMN_NAME  .Text.Trim();
						reqCUSTOM_COLUMN_NAME  .Text = Sql.IsEmptyString(sDATA_FIELD       ) ? L10n.Term(".ERR_REQUIRED_FIELD") : String.Empty;
						if ( Sql.IsEmptyString(sDATA_FIELD) )
							return;
					}
					else if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
					{
						sRELATIONSHIP_NAME = txtCUSTOM_ACTIVITY_NAME.Text.Trim();
						sDATA_FIELD        = txtCUSTOM_COLUMN_NAME  .Text.Trim();
						// 11/01/2010 Paul.  Can't seem to get the RequiredFieldValidator to work, so use a Label instead and manually validate. 
						reqCUSTOM_ACTIVITY_NAME.Text = Sql.IsEmptyString(sRELATIONSHIP_NAME) ? L10n.Term(".ERR_REQUIRED_FIELD") : String.Empty;
						reqCUSTOM_COLUMN_NAME  .Text = Sql.IsEmptyString(sDATA_FIELD       ) ? L10n.Term(".ERR_REQUIRED_FIELD") : String.Empty;
						if ( Sql.IsEmptyString(sRELATIONSHIP_NAME) || Sql.IsEmptyString(sDATA_FIELD) )
							return;
					}
					else
					{
						sRELATIONSHIP_NAME = lstRELATED             .SelectedValue;
						sMODULE_NAME       = lstFILTER_COLUMN_SOURCE.SelectedValue;
						sDATA_FIELD        = lstFILTER_COLUMN       .SelectedValue;
					}
					
					string[] arrSEARCH_TEXT = new string[0];
					// 07/06/2007 Paul.  ansistring is treated the same as string. 
					string sCOMMON_DATA_TYPE = sDATA_TYPE;
					if ( sCOMMON_DATA_TYPE == "ansistring" )
						sCOMMON_DATA_TYPE = "string";
					switch ( sCOMMON_DATA_TYPE )
					{
						case "string":
						{
							switch ( sOPERATOR )
							{
								case "equals"        :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "contains"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "starts_with"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "ends_with"     :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "not_equals_str":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
								// 08/25/2011 Paul.  A customer wants more use of NOT in string filters. 
								case "not_contains"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "not_starts_with":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "not_ends_with"  :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								// 02/14/2013 Paul.  A customer wants to use like in string filters. 
								case "like"           :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "not_like"       :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less"          :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "less_equal"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater"       :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater_equal" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
							}
							break;
						}
						case "datetime":
						{
							/*
							switch ( sOPERATOR )
							{
								case "on"               :  arrSEARCH_TEXT = new string[] { Sql.ToDateTime(ctlFILTER_SEARCH_START_DATE.DateText).ToString("yyyy/MM/dd") };  break;
								case "before"           :  arrSEARCH_TEXT = new string[] { Sql.ToDateTime(ctlFILTER_SEARCH_START_DATE.DateText).ToString("yyyy/MM/dd") };  break;
								case "after"            :  arrSEARCH_TEXT = new string[] { Sql.ToDateTime(ctlFILTER_SEARCH_START_DATE.DateText).ToString("yyyy/MM/dd") };  break;
								case "not_equals_str"   :  arrSEARCH_TEXT = new string[] { Sql.ToDateTime(ctlFILTER_SEARCH_START_DATE.DateText).ToString("yyyy/MM/dd") };  break;
								case "between_dates"    :  arrSEARCH_TEXT = new string[] { Sql.ToDateTime(ctlFILTER_SEARCH_START_DATE.DateText).ToString("yyyy/MM/dd"), Sql.ToDateTime(ctlFILTER_SEARCH_END_DATE.DateText).ToString("yyyy/MM/dd") };  break;
								case "empty"            :  break;
								case "not_empty"        :  break;
								case "is_before"        :  break;
								case "is_after"         :  break;
								case "tp_yesterday"     :  break;
								case "tp_today"         :  break;
								case "tp_tomorrow"      :  break;
								case "tp_last_7_days"   :  break;
								case "tp_next_7_days"   :  break;
								case "tp_last_month"    :  break;
								case "tp_this_month"    :  break;
								case "tp_next_month"    :  break;
								case "tp_last_30_days"  :  break;
								case "tp_next_30_days"  :  break;
								case "tp_last_year"     :  break;
								case "tp_this_year"     :  break;
								case "tp_next_year"     :  break;
								case "changed"          :  break;
								case "unchanged"        :  break;
								case "increased"        :  break;
								case "decreased"        :  break;
								case "tp_minutes_after" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_hours_after"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_days_after"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_weeks_after"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_months_after"  :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_years_after"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_minutes_before":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_hours_before"  :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_days_before"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_weeks_before"  :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_months_before" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_years_before"  :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								// 12/04/2008 Paul.  We need to be able to do an an equals. 
								case "tp_days_old"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_weeks_old"     :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_months_old"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "tp_years_old"     :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
							}
							*/
							// 05/04/2009 Paul.  Use the text field instead of the date field to allow for activity binding. 
							arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };
							break;
						}
						case "int32":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "less"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "between"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text, txtFILTER_SEARCH_TEXT2.Text };  break;
								case "not_equals":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater_equal" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
							}
							break;
						}
						case "decimal":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "less"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "between"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text, txtFILTER_SEARCH_TEXT2.Text };  break;
								case "not_equals":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater_equal" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
							}
							break;
						}
						case "float":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "less"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "between"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text, txtFILTER_SEARCH_TEXT2.Text };  break;
								case "not_equals":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
								// 07/23/2013 Paul.  Add greater and less than conditions. 
								case "less_equal"    :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "greater_equal" :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
							}
							break;
						}
						case "bool":
						{
							switch ( sOPERATOR )
							{
								case "equals"    :  arrSEARCH_TEXT = new string[] { lstFILTER_SEARCH_DROPDOWN.SelectedValue };  break;
								case "empty"     :  break;
								case "not_empty" :  break;
								case "changed"   :  break;
								case "unchanged" :  break;
								case "increased" :  break;
								case "decreased" :  break;
							}
							break;
						}
						case "guid":
						{
							switch ( sOPERATOR )
							{
								case "is"            :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_ID.Value  };  break;
								case "equals"        :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "contains"      :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "starts_with"   :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "ends_with"     :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "not_equals_str":  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
							}
							break;
						}
						case "enum":
						{
							switch ( sOPERATOR )
							{
								case "is"            :  arrSEARCH_TEXT = new string[] { lstFILTER_SEARCH_DROPDOWN.SelectedValue };  break;
								case "one_of"        :  arrSEARCH_TEXT = Sql.ToStringArray(lstFILTER_SEARCH_LISTBOX);  break;
								// 09/13/2011 Paul.  The operator within a Workflow Action is disabled because we can only do an assignment here. 
								// An exception is the Enum as we want to allow an ActivityBind. 
								case "custom"        :  arrSEARCH_TEXT = new string[] { txtFILTER_SEARCH_TEXT.Text };  break;
								case "empty"         :  break;
								case "not_empty"     :  break;
								case "changed"       :  break;
								case "unchanged"     :  break;
								case "increased"     :  break;
								case "decreased"     :  break;
							}
							break;
						}
					}
					FiltersUpdate(sFILTER_ID, sACTION_TYPE, sRELATIONSHIP_NAME, sMODULE_NAME, sDATA_FIELD, sDATA_TYPE, sOPERATOR, arrSEARCH_TEXT );
					BuildWorkflowXOML();
					//ResetSearchText();
				}
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Changed
		protected void lstACTION_TYPE_Changed(Object sender, EventArgs e)
		{
			txtFILTER_SEARCH_TEXT.Text = "";
			lstRELATED_Bind();
		}

		protected void lstRELATED_Changed(Object sender, EventArgs e)
		{
			try
			{
				txtFILTER_SEARCH_TEXT.Text = "";
				lblRELATED.Text = lstRELATED.SelectedValue;
				rdl.SetCustomProperty("Related"      , lstRELATED.SelectedValue);
				rdl.SetCustomProperty("Relationships", String.Empty);
				lstFILTER_COLUMN_SOURCE_Bind();
				BuildWorkflowXOML();
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void lstFILTER_COLUMN_SOURCE_Changed(Object sender, EventArgs e)
		{
			txtFILTER_SEARCH_TEXT.Text = "";
			lblFILTER_COLUMN_SOURCE.Text = lstFILTER_COLUMN_SOURCE.SelectedValue;
			lstFILTER_COLUMN_Bind();
		}

		protected void lstFILTER_COLUMN_Changed(Object sender, EventArgs e)
		{
			txtFILTER_SEARCH_TEXT.Text = "";
			lblFILTER_COLUMN.Text = lstFILTER_COLUMN.SelectedValue;
			lblFILTER_OPERATOR_Bind();
		}

		protected void lstFILTER_OPERATOR_Changed(Object sender, EventArgs e)
		{
			txtFILTER_SEARCH_TEXT.Text = "";
			lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
			BindSearchText();
		}
		#endregion

		#region Bind
		private void lstRELATED_Bind()
		{
			// 02/21/2009 Paul.  Clear the list before binding so that it will drop back to None when Update is selected. 
			lstRELATED.Items.Clear();
			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			if ( lstACTION_TYPE.SelectedValue != "update" && !lstACTION_TYPE.SelectedValue.StartsWith("custom") )
			{
				DataView vwRelationships = new DataView(SplendidCache.ReportingRelationships());
				vwRelationships.RowFilter = "(RELATIONSHIP_TYPE = 'many-to-many' and LHS_MODULE = \'" + BaseModule + "\')" + ControlChars.CrLf
				                          + " or (RELATIONSHIP_TYPE = 'one-to-many' and LHS_MODULE = \'" + BaseModule + "\')" + ControlChars.CrLf;

				XmlDocument xmlRelationships = new XmlDocument();
				xmlRelationships.AppendChild(xmlRelationships.CreateElement("Relationships"));
				
				XmlNode xRelationship = null;
				foreach(DataRowView row in vwRelationships)
				{
					string sRELATIONSHIP_NAME              = Sql.ToString(row["RELATIONSHIP_NAME"             ]);
					string sLHS_MODULE                     = Sql.ToString(row["LHS_MODULE"                    ]);
					string sLHS_TABLE                      = Sql.ToString(row["LHS_TABLE"                     ]).ToUpper();
					string sLHS_KEY                        = Sql.ToString(row["LHS_KEY"                       ]).ToUpper();
					string sRHS_MODULE                     = Sql.ToString(row["RHS_MODULE"                    ]);
					string sRHS_TABLE                      = Sql.ToString(row["RHS_TABLE"                     ]).ToUpper();
					string sRHS_KEY                        = Sql.ToString(row["RHS_KEY"                       ]).ToUpper();
					string sJOIN_TABLE                     = Sql.ToString(row["JOIN_TABLE"                    ]).ToUpper();
					string sJOIN_KEY_LHS                   = Sql.ToString(row["JOIN_KEY_LHS"                  ]).ToUpper();
					string sJOIN_KEY_RHS                   = Sql.ToString(row["JOIN_KEY_RHS"                  ]).ToUpper();
					// 11/20/2008 Paul.  Quotes, Orders and Invoices have a relationship column. 
					string sRELATIONSHIP_ROLE_COLUMN       = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN"      ]).ToUpper();
					string sRELATIONSHIP_ROLE_COLUMN_VALUE = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN_VALUE"]);
					// 11/25/2008 Paul.  Be careful to only switch when necessary.  Parent accounts should not be switched. 
					if ( sLHS_MODULE != BaseModule && sRHS_MODULE == BaseModule )
					{
						sLHS_MODULE        = Sql.ToString(row["RHS_MODULE"       ]);
						sLHS_TABLE         = Sql.ToString(row["RHS_TABLE"        ]).ToUpper();
						sLHS_KEY           = Sql.ToString(row["RHS_KEY"          ]).ToUpper();
						sRHS_MODULE        = Sql.ToString(row["LHS_MODULE"       ]);
						sRHS_TABLE         = Sql.ToString(row["LHS_TABLE"        ]).ToUpper();
						sRHS_KEY           = Sql.ToString(row["LHS_KEY"          ]).ToUpper();
						sJOIN_TABLE        = Sql.ToString(row["JOIN_TABLE"       ]).ToUpper();
						sJOIN_KEY_LHS      = Sql.ToString(row["JOIN_KEY_RHS"     ]).ToUpper();
						sJOIN_KEY_RHS      = Sql.ToString(row["JOIN_KEY_LHS"     ]).ToUpper();
					}
					string sMODULE_NAME  = sRHS_MODULE + " " + sRHS_TABLE;
					string sDISPLAY_NAME = L10n.Term(".moduleList." + sRHS_MODULE);
					// 10/22/2008 Paul.  An account can be related to an account, but we need to make sure that it gets a separate activity name. 
					if ( sRHS_MODULE == BaseModule )
						sMODULE_NAME += "_REL";
					// 10/26/2011 Paul.  Use the join table so that the display is more descriptive. 
					if ( !Sql.IsEmptyString(sJOIN_TABLE) )
						sDISPLAY_NAME = Sql.CamelCaseModules(L10n, sJOIN_TABLE);
					if ( bDebug )
					{
						sDISPLAY_NAME = "[" + sMODULE_NAME + "] " + sDISPLAY_NAME;
					}
					// 02/18/2009 Paul.  Include the relationship column if provided. 
					// 10/26/2011 Paul.  Include the role. 
					if ( !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) && sRELATIONSHIP_ROLE_COLUMN_VALUE != BaseModule )
						sDISPLAY_NAME += " " + sRELATIONSHIP_ROLE_COLUMN_VALUE;
					// 10/26/2011 Paul.  Add the relationship so that we can have a unique lookup. 
					sMODULE_NAME += " " + sRELATIONSHIP_NAME;
					
					xRelationship = xmlRelationships.CreateElement("Relationship");
					xmlRelationships.DocumentElement.AppendChild(xRelationship);
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME"             , sRELATIONSHIP_NAME             );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"                    , sLHS_MODULE                    );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"                     , sLHS_TABLE                     );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"                       , sLHS_KEY                       );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"                    , sRHS_MODULE                    );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"                     , sRHS_TABLE                     );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"                       , sRHS_KEY                       );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_TABLE"                    , sJOIN_TABLE                    );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_KEY_LHS"                  , sJOIN_KEY_LHS                  );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "JOIN_KEY_RHS"                  , sJOIN_KEY_RHS                  );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE"             , "many-to-many"                 );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"                   , sMODULE_NAME                   );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"                  , sDISPLAY_NAME                  );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_ROLE_COLUMN"      , sRELATIONSHIP_ROLE_COLUMN      );
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_ROLE_COLUMN_VALUE", sRELATIONSHIP_ROLE_COLUMN_VALUE);
				}
				rdl.SetCustomProperty("RelatedModules", xmlRelationships.OuterXml.Replace("</Relationship>", "</Relationship>" + ControlChars.CrLf));

				DataTable dtModules = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
				DataView vwModules = new DataView(dtModules);
				vwModules.Sort = "DISPLAY_NAME";
				lstRELATED.DataSource = vwModules;
				lstRELATED.DataBind();
			}
			if ( lstACTION_TYPE.SelectedValue == "update" || lstRELATED.Items.Count == 0 )
				lstRELATED.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
			
			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			// 08/03/2012 Paul.  Add Custom Stored Procedure. 
			txtCUSTOM_ACTIVITY_NAME.Visible = (lstACTION_TYPE.SelectedValue == "custom_prop" || lstACTION_TYPE.SelectedValue == "custom_method");
			lstRELATED.Visible = !(lstACTION_TYPE.SelectedValue.StartsWith("custom"));
			
			lblRELATED.Text = lstRELATED.SelectedValue;
			lstFILTER_COLUMN_SOURCE_Bind();
		}

		private void lstFILTER_COLUMN_SOURCE_Bind()
		{
			// 07/13/2006 Paul.  Convert the module name to the correct table name. 
			string sModule = BaseModule;
			XmlDocument xmlRelationships = new XmlDocument();
			xmlRelationships.AppendChild(xmlRelationships.CreateElement("Relationships"));
			
			XmlNode xRelationship = null;
			string sMODULE_TABLE = String.Empty;
			if ( lstACTION_TYPE.SelectedValue == "update" )
			{
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);

				sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sModule      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sMODULE_TABLE);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sMODULE_TABLE);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sModule + " " + sMODULE_TABLE);
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sModule + " " + sMODULE_TABLE + "] " + sModule);
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sModule);
			}
			else if ( !Sql.IsEmptyString(lstRELATED.SelectedValue) )
			{
				xRelationship = xmlRelationships.CreateElement("Relationship");
				xmlRelationships.DocumentElement.AppendChild(xRelationship);
				string sRELATED_MODULE    = lstRELATED.SelectedValue.Split(' ')[0];
				string sRELATED_ALIAS     = lstRELATED.SelectedValue.Split(' ')[1];
				// 10/26/2011 Paul.  Add the relationship so that we can have a unique lookup. 
				string sRELATIONSHIP_NAME = lstRELATED.SelectedValue.Split(' ')[2];
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sRELATIONSHIP_NAME);
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sRELATED_MODULE   );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sRELATED_ALIAS    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , String.Empty      );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", "many-to-many"    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sRELATED_ALIAS    );
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sRELATED_MODULE + " " + sRELATED_ALIAS);
				if ( bDebug )
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sRELATED_MODULE + " " + sRELATED_ALIAS + "] " + sRELATED_MODULE);
				else
					XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sRELATED_MODULE);
			}

			// 07/22/2008 Paul.  Add a relationship to the AUDIT table for the old/previous value. 
			// 08/21/2008 Paul.  We cannot assign values to the audit table. 
			/*
			xRelationship = xmlRelationships.CreateElement("Relationship");
			xmlRelationships.DocumentElement.AppendChild(xRelationship);

			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_NAME", sModule.ToLower() + "_audit_old");
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_MODULE"       , sModule      );
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_TABLE"        , sMODULE_TABLE + "_AUDIT");
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "LHS_KEY"          , "ID"         );
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_MODULE"       , sModule      );
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_TABLE"        , sMODULE_TABLE);
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RHS_KEY"          , "ID"         );
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "RELATIONSHIP_TYPE", String.Empty );
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_ALIAS"     , sMODULE_TABLE + "_AUDIT_OLD");
			XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "MODULE_NAME"      , sModule + " " + sMODULE_TABLE + "_AUDIT_OLD");
			if ( bDebug )
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , "[" + sModule + " " + sMODULE_TABLE + "_AUDIT_OLD" + "] " + sModule + ": Audit Old");
			else
				XmlUtil.SetSingleNode(xmlRelationships, xRelationship, "DISPLAY_NAME"     , sModule + ": Audit Old");
			*/
			rdl.SetCustomProperty("Relationships", xmlRelationships.OuterXml.Replace("</Relationship>", "</Relationship>" + ControlChars.CrLf));

			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
			{
				lstFILTER_COLUMN_SOURCE.DataSource = null;
				lstFILTER_COLUMN_SOURCE.Items.Clear();
				lstFILTER_COLUMN_SOURCE.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
			}
			else
			{
				DataTable dtModuleColumnSource = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
				// 05/29/2006 Paul.  Filter column source is always the same as module column source. 
				lstFILTER_COLUMN_SOURCE.DataSource = dtModuleColumnSource;
				lstFILTER_COLUMN_SOURCE.DataBind();
				lblFILTER_COLUMN_SOURCE.Text = lstFILTER_COLUMN_SOURCE.SelectedValue;
			}

			lstFILTER_COLUMN_Bind();
		}
		#endregion

		private void lstFILTER_COLUMN_Bind()
		{
			lstFILTER_COLUMN.DataSource = null;
			lstFILTER_COLUMN.DataBind();

			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
			{
				lstFILTER_COLUMN.DataSource = null;
				lstFILTER_COLUMN.Items.Clear();
				lstFILTER_COLUMN.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
				lblFILTER_COLUMN.Text = String.Empty;
			}
			else
			{
				string[] arrModule = lstFILTER_COLUMN_SOURCE.SelectedValue.Split(' ');
				string sModule     = arrModule[0];
				string sTableAlias = arrModule[1];

				string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
				// 05/04/2009 Paul.  We now allow custom fields to be updated inside a workflow, so show all fields. 
				DataTable dtColumns = SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy();
				foreach(DataRow row in dtColumns.Rows)
				{
					row["NAME"        ] = sTableAlias + "." + Sql.ToString(row["NAME"]);
					// 07/04/2006 Paul.  Some columns have global terms. 
					row["DISPLAY_NAME"] = Utils.TableColumnName(L10n, sModule, Sql.ToString(row["DISPLAY_NAME"]));
				}
				ViewState["FILTER_COLUMNS"] = dtColumns;
				
				// 06/21/2006 Paul.  Do not sort the columns  We want it to remain sorted by COLID. This should keep the NAME at the top. 
				DataView vwColumns = new DataView(dtColumns);
				vwColumns.Sort = "DISPLAY_NAME";
				lstFILTER_COLUMN.DataSource = vwColumns;
				lstFILTER_COLUMN.DataBind();
				lblFILTER_COLUMN.Text = lstFILTER_COLUMN.SelectedValue;
			}

			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			txtCUSTOM_COLUMN_NAME.Visible = (lstACTION_TYPE.SelectedValue.StartsWith("custom"));
			reqCUSTOM_COLUMN_NAME.Enabled = txtCUSTOM_COLUMN_NAME.Visible;
			lstFILTER_COLUMN.Visible = !txtCUSTOM_COLUMN_NAME.Visible;
			
			lblFILTER_OPERATOR_Bind();
		}

		private void lblFILTER_OPERATOR_Bind()
		{
			lstFILTER_OPERATOR.DataSource = null;
			lstFILTER_OPERATOR.Items.Clear();
			// 09/13/2011 Paul.  The operator is disabled because we can only do an assignment here. 
			lstFILTER_OPERATOR.Enabled = false;

			// 11/01/2010 Paul.  Add Custom Activity Action Type. 
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			if ( lstACTION_TYPE.SelectedValue.StartsWith("custom") )
			{
				lstFILTER_OPERATOR.DataSource = SplendidCache.List("string_operator_dom");
				lstFILTER_OPERATOR.DataBind();
				lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
			}
			else
			{
				string[] arrModule = lstFILTER_COLUMN_SOURCE.SelectedValue.Split(' ');
				string sModule     = arrModule[0];
				string sTableAlias = arrModule[1];
				
				string[] arrColumn = lstFILTER_COLUMN.SelectedValue.Split('.');
				string sColumnName = arrColumn[0];
				if ( arrColumn.Length > 1 )
					sColumnName = arrColumn[1];
				
				string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
				// 05/04/2009 Paul.  We now allow custom fields to be updated inside a workflow, so show all fields. 
				DataView vwColumns = new DataView(SplendidCache.WorkflowFilterColumns(sMODULE_TABLE).Copy());
				vwColumns.RowFilter = "ColumnName = '" + sColumnName + "'";
				
				if ( vwColumns.Count > 0 )
				{
					DataRowView row = vwColumns[0];
					string sCsType = Sql.ToString(row["CsType"]);
					lblFILTER_OPERATOR_TYPE.Text = sCsType.ToLower();
					txtFILTER_SEARCH_DATA_TYPE.Value = sCsType.ToLower();
					
					// 09/13/2011 Paul.  The operator is disabled because we can only do an assignment here. 
					if ( sCsType.ToLower() == "enum" )
					{
						// 09/13/2011 Paul.  An exception is the Enum as we want to allow an ActivityBind. 
						lstFILTER_OPERATOR.Enabled = true;
						lstFILTER_OPERATOR.DataSource = SplendidCache.List("custom_enum_operator_dom");
						lstFILTER_OPERATOR.DataBind();
					}
					else
					{
						lstFILTER_OPERATOR.DataSource = SplendidCache.List(sCsType.ToLower() + "_operator_dom");
						lstFILTER_OPERATOR.DataBind();
					}
					lblFILTER_OPERATOR.Text = lstFILTER_OPERATOR.SelectedValue;
				}
			}
			BindSearchText();
		}

		private void BindSearchText()
		{
			// 10/22/2008 Paul.  Make sure to clear the ReadOnly flag that may have been set on the previous operator change. 
			txtFILTER_SEARCH_TEXT      .ReadOnly = false;
			txtFILTER_SEARCH_TEXT      .Visible = false;
			txtFILTER_SEARCH_TEXT2     .Visible = false;
			lstFILTER_SEARCH_LISTBOX   .Visible = false;
			lstFILTER_SEARCH_DROPDOWN  .Visible = false;
			ctlFILTER_SEARCH_START_DATE.Visible = false;
			ctlFILTER_SEARCH_END_DATE  .Visible = false;
			lblFILTER_AND_SEPARATOR    .Visible = false;
			btnFILTER_SEARCH_SELECT    .Visible = false;
			// 02/09/2007 Paul.  Clear the lookups if not used. 
			if ( txtFILTER_SEARCH_DATA_TYPE.Value != "enum" )
			{
				lstFILTER_SEARCH_DROPDOWN.DataSource = null;
				lstFILTER_SEARCH_LISTBOX .DataSource = null;
				lstFILTER_SEARCH_DROPDOWN.DataBind();
				lstFILTER_SEARCH_LISTBOX .DataBind();
			}
			// 07/06/2007 Paul.  ansistring is treated the same as string. 
			string sCOMMON_DATA_TYPE = txtFILTER_SEARCH_DATA_TYPE.Value;
			if ( sCOMMON_DATA_TYPE == "ansistring" )
				sCOMMON_DATA_TYPE = "string";
			// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
			// 08/03/2012 Paul.  Add Custom Stored Procedure. 
			if ( lstACTION_TYPE.SelectedValue == "custom_method" || lstACTION_TYPE.SelectedValue == "custom_procedure" )
			{
				// 11/07/2010 Paul.  Set sCOMMON_DATA_TYPE to an empty string so that the search fields will be hidden. 
				sCOMMON_DATA_TYPE = String.Empty;
			}
			// 03/22/2013 Paul.  When editing an existing action, we need to make sure to hide the custom activity name appropriately. 
			txtCUSTOM_ACTIVITY_NAME.Visible = (lstACTION_TYPE.SelectedValue == "custom_prop" || lstACTION_TYPE.SelectedValue == "custom_method");
			switch ( sCOMMON_DATA_TYPE )
			{
				case "string":
				{
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "equals"        :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "contains"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "starts_with"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "ends_with"     :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "not_equals_str":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "empty"         :  break;
						case "not_empty"     :  break;
						case "changed"       :  break;
						case "unchanged"     :  break;
						case "increased"     :  break;
						case "decreased"     :  break;
						// 08/25/2011 Paul.  A customer wants more use of NOT in string filters. 
						case "not_contains"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "not_starts_with":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "not_ends_with"  :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						// 02/14/2013 Paul.  A customer wants to use like in string filters. 
						case "like"           :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "not_like"       :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						// 07/23/2013 Paul.  Add greater and less than conditions. 
						case "less"          :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "less_equal"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater"       :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater_equal" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
					}
					break;
				}
				case "datetime":
				{
					/*
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "on"               :  ctlFILTER_SEARCH_START_DATE.Visible = true;  break;
						case "before"           :  ctlFILTER_SEARCH_START_DATE.Visible = true;  break;
						case "after"            :  ctlFILTER_SEARCH_START_DATE.Visible = true;  break;
						case "between_dates"    :  ctlFILTER_SEARCH_START_DATE.Visible = true;  lblFILTER_AND_SEPARATOR.Visible = true;  ctlFILTER_SEARCH_END_DATE.Visible = true;  break;
						case "not_equals_str"   :  ctlFILTER_SEARCH_START_DATE.Visible = true;  break;
						case "empty"            :  break;
						case "not_empty"        :  break;
						case "is_before"        :  break;
						case "is_after"         :  break;
						case "tp_yesterday"     :  break;
						case "tp_today"         :  break;
						case "tp_tomorrow"      :  break;
						case "tp_last_7_days"   :  break;
						case "tp_next_7_days"   :  break;
						case "tp_last_month"    :  break;
						case "tp_this_month"    :  break;
						case "tp_next_month"    :  break;
						case "tp_last_30_days"  :  break;
						case "tp_next_30_days"  :  break;
						case "tp_last_year"     :  break;
						case "tp_this_year"     :  break;
						case "tp_next_year"     :  break;
						case "changed"          :  break;
						case "unchanged"        :  break;
						case "increased"        :  break;
						case "decreased"        :  break;
						case "tp_minutes_after" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_hours_after"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_days_after"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_weeks_after"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_months_after"  :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_years_after"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_minutes_before":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_hours_before"  :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_days_before"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_weeks_before"  :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_months_before" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_years_before"  :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						// 12/04/2008 Paul.  We need to be able to do an an equals. 
						case "tp_days_old"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_weeks_old"     :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_months_old"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "tp_years_old"     :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
					}
					*/
					// 05/04/2009 Paul.  Use the text field instead of the date field to allow for activity binding. 
					txtFILTER_SEARCH_TEXT.Visible = true ;
					ctlFILTER_SEARCH_START_DATE.Visible = false;
					break;
				}
				case "int32":
				{
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "equals"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "less"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "between"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  lblFILTER_AND_SEPARATOR.Visible = true;  txtFILTER_SEARCH_TEXT2.Visible = true ;  break;
						case "not_equals":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "empty"     :  break;
						case "not_empty" :  break;
						case "changed"   :  break;
						case "unchanged" :  break;
						case "increased" :  break;
						case "decreased" :  break;
						// 07/23/2013 Paul.  Add greater and less than conditions. 
						case "less_equal"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater_equal" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
					}
					break;
				}
				case "decimal":
				{
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "equals"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "less"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "between"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  lblFILTER_AND_SEPARATOR.Visible = true;  txtFILTER_SEARCH_TEXT2.Visible = true ;  break;
						case "not_equals":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "empty"     :  break;
						case "not_empty" :  break;
						case "changed"   :  break;
						case "unchanged" :  break;
						case "increased" :  break;
						case "decreased" :  break;
						// 07/23/2013 Paul.  Add greater and less than conditions. 
						case "less_equal"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater_equal" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
					}
					break;
				}
				case "float":
				{
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "equals"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "less"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "between"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  lblFILTER_AND_SEPARATOR.Visible = true;  txtFILTER_SEARCH_TEXT2.Visible = true ;  break;
						case "not_equals":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "empty"     :  break;
						case "not_empty" :  break;
						case "changed"   :  break;
						case "unchanged" :  break;
						case "increased" :  break;
						case "decreased" :  break;
						// 07/23/2013 Paul.  Add greater and less than conditions. 
						case "less_equal"    :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "greater_equal" :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
					}
					break;
				}
				case "bool":
				{
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "equals"    :
							// 12/20/2006 Paul.  We need to populate the dropdown for booleans with 1 and 0. 
							lstFILTER_SEARCH_DROPDOWN.Visible = true ;
							lstFILTER_SEARCH_DROPDOWN.Items.Clear();
							lstFILTER_SEARCH_DROPDOWN.Items.Add(new ListItem(L10n.Term(".LBL_YES"), "1"));
							lstFILTER_SEARCH_DROPDOWN.Items.Add(new ListItem(L10n.Term(".LBL_NO" ), "0"));
							break;
						case "empty"     :  break;
						case "not_empty" :  break;
						case "changed"   :  break;
						case "unchanged" :  break;
						case "increased" :  break;
						case "decreased" :  break;
					}
					break;
				}
				case "guid":
				{
					// 11/17/2008 Paul.  Since we don't enable the filter operator, we need to default to equals 
					// so that the edit field is enabled. 
					try
					{
						if ( lstFILTER_OPERATOR.SelectedValue == "is" )
							// 08/19/2010 Paul.  Check the list before assigning the value. 
							Utils.SetSelectedValue(lstFILTER_OPERATOR, "equals");
					}
					catch
					{
					}
					switch ( lstFILTER_OPERATOR.SelectedValue )
					{
						case "is"            :  txtFILTER_SEARCH_TEXT.Visible = true ;  txtFILTER_SEARCH_TEXT.ReadOnly = true ;  btnFILTER_SEARCH_SELECT.Visible = false;  break;
						case "equals"        :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "contains"      :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "starts_with"   :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "ends_with"     :  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "not_equals_str":  txtFILTER_SEARCH_TEXT.Visible = true ;  break;
						case "empty"         :  break;
						case "not_empty"     :  break;
						case "changed"       :  break;
						case "unchanged"     :  break;
						case "increased"     :  break;
						case "decreased"     :  break;
					}
					break;
				}
				case "enum":
				{
					// 02/09/2007 Paul.  If this is an enum, then populate the listbox with list names pulled from EDITVIEWS_FIELDS.
					string[] arrModule = lstFILTER_COLUMN_SOURCE.SelectedValue.Split(' ');
					string sModule = arrModule[0];
					
					string[] arrColumn = lstFILTER_COLUMN.SelectedValue.Split('.');
					string sColumnName = arrColumn[0];
					if ( arrColumn.Length > 1 )
						sColumnName = arrColumn[1];
					
					string sMODULE_TABLE = Sql.ToString(Application["Modules." + sModule + ".TableName"]);
					string sLIST_NAME = SplendidCache.ReportingFilterColumnsListName(sMODULE_TABLE, sColumnName);
					if ( Sql.IsEmptyString(sLIST_NAME) )
					{
						lstFILTER_SEARCH_DROPDOWN.DataSource = null;
						lstFILTER_SEARCH_LISTBOX .DataSource = null;
						lstFILTER_SEARCH_DROPDOWN.DataBind();
						lstFILTER_SEARCH_LISTBOX .DataBind();
					}
					else
					{
						lstFILTER_SEARCH_DROPDOWN.DataSource = SplendidCache.List(sLIST_NAME);
						lstFILTER_SEARCH_DROPDOWN.DataBind();
						lstFILTER_SEARCH_LISTBOX .DataSource = lstFILTER_SEARCH_DROPDOWN.DataSource;
						lstFILTER_SEARCH_LISTBOX .DataBind();
						switch ( lstFILTER_OPERATOR.SelectedValue )
						{
							case "is"            :  lstFILTER_SEARCH_DROPDOWN.Visible = true;  break;
							case "one_of"        :  lstFILTER_SEARCH_LISTBOX .Visible = true;  break;
							case "empty"         :  break;
							case "not_empty"     :  break;
							case "changed"       :  break;
							case "unchanged"     :  break;
							case "increased"     :  break;
							case "decreased"     :  break;
						}
					}
					// 09/13/2011 Paul.  The operator within a Workflow Action is disabled because we can only do an assignment here. 
					// An exception is the Enum as we want to allow an ActivityBind. 
					if ( lstFILTER_OPERATOR.SelectedValue == "custom" )
					{
						lstFILTER_SEARCH_DROPDOWN.Visible = false;
						lstFILTER_SEARCH_LISTBOX .Visible = false;
						txtFILTER_SEARCH_TEXT.Visible     = true ;
					}
					break;
				}
			}
		}

		private void BuildWorkflowXOML()
		{
			// 08/09/2008 Paul.  The primary location for the WORKFLOW_ID will be in the SplendidSequentialWorkflow
			xoml = new XomlDocument("Workflow1", gPARENT_ID);
			if ( xoml.DocumentElement != null )
			{
				try
				{
					string sMODULE_TABLE = Sql.ToString(Application["Modules." + BaseModule + ".TableName"]);
					// 11/16/2008 Paul.  Time-based workflows reference the base module, not the audit table. 
					WorkflowBuilder.BuildActionXOML(xoml, null, rdl, sWORKFLOW_TYPE, BaseModule, sMODULE_TABLE, 1);
				}
				catch(Exception ex)
				{
					ctlDynamicButtons.ErrorText = ex.Message;
				}

				try
				{
					Activity rootActivity = null;
					using ( StringReader stm = new StringReader(xoml.OuterXml) )
					{
						using ( XmlReader rdr = XmlReader.Create(stm) )
						{
							WorkflowMarkupSerializer xomlSerializer = new WorkflowMarkupSerializer();
							rootActivity = xomlSerializer.Deserialize(rdr) as Activity;
						}
					}
					WorkflowRuntime runtime = WorkflowInit.GetRuntime(Application);
					Dictionary<string, object> dictParameters = new Dictionary<string, object>();
					//Guid gAUDIT_ID = new Guid("6BC0188C-4F1D-42FE-A303-002B1BC123CE");
					//dictParameters.Add("AUDIT_ID", gAUDIT_ID);
					WorkflowInstance inst = null;
					using ( StringReader stm = new StringReader(xoml.OuterXml) )
					{
						using ( XmlReader rdr = XmlReader.Create(stm) )
						{
							inst = runtime.CreateWorkflow(rdr, null, dictParameters);
						}
					}
				}
				catch(WorkflowValidationFailedException ex)
				{
					StringBuilder sb = new StringBuilder();
					foreach ( ValidationError error in ex.Errors )
					{
						sb.AppendLine(error.ToString() + "<br />");
					}
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), sb.ToString());
					ctlDynamicButtons.ErrorText = sb.ToString();
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = "BuildWorkflowXOML: " + ex.Message;
				}
			}
		}

		private DataTable ReportFilters()
		{
			DataTable dtFilters = new DataTable();
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			dtFilters = XmlUtil.CreateDataTable(xmlFilters.DocumentElement, "Filter", new string[] {"ID", "ACTION_TYPE", "RELATIONSHIP_NAME", "MODULE", "MODULE_NAME", "TABLE_NAME", "DATA_FIELD", "FIELD_NAME", "DATA_TYPE", "OPERATOR", "SEARCH_TEXT"});
			return dtFilters;
		}

		private DataTable ReportColumnSource()
		{
			DataTable dtColumnSource = new DataTable();
			XmlDocument xmlRelationships = rdl.GetCustomProperty("Relationships");
			dtColumnSource = XmlUtil.CreateDataTable(xmlRelationships.DocumentElement, "Relationship", new string[] {"MODULE_NAME", "DISPLAY_NAME"});
			return dtColumnSource;
		}

		#region Filter Editing
		protected void FiltersGet(string sID, ref string sACTION_TYPE, ref string sRELATIONSHIP_NAME, ref string sMODULE_NAME, ref string sDATA_FIELD, ref string sDATA_TYPE, ref string sOPERATOR, ref string[] arrSEARCH_TEXT)
		{
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
			if ( xFilter != null )
			{
				sACTION_TYPE       = XmlUtil.SelectSingleNode(xFilter, "ACTION_TYPE"      );
				sRELATIONSHIP_NAME = XmlUtil.SelectSingleNode(xFilter, "RELATIONSHIP_NAME");
				sMODULE_NAME       = XmlUtil.SelectSingleNode(xFilter, "MODULE_NAME"      );
				sDATA_FIELD        = XmlUtil.SelectSingleNode(xFilter, "DATA_FIELD"       );
				sDATA_TYPE         = XmlUtil.SelectSingleNode(xFilter, "DATA_TYPE"        );
				sOPERATOR          = XmlUtil.SelectSingleNode(xFilter, "OPERATOR"         );
				//sSEARCH_TEXT     = XmlUtil.GetSingleNode(xFilter, "SEARCH_TEXT");
				XmlNodeList nlValues = xFilter.SelectNodes("SEARCH_TEXT_VALUES");
				arrSEARCH_TEXT = new string[nlValues.Count];
				int i = 0;
				foreach ( XmlNode xValue in nlValues )
				{
					arrSEARCH_TEXT[i++] = xValue.InnerText;
				}
			}
		}

		protected void FiltersUpdate(string sID, string sACTION_TYPE, string sRELATIONSHIP_NAME, string sMODULE_NAME, string sDATA_FIELD, string sDATA_TYPE, string sOPERATOR, string[] arrSEARCH_TEXT)
		{
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			try
			{
				XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
				if ( xFilter == null || Sql.IsEmptyString(sID) )
				{
					xFilter = xmlFilters.CreateElement("Filter");
					xmlFilters.DocumentElement.AppendChild(xFilter);
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ID", Guid.NewGuid().ToString());
				}
				else
				{
					// 06/12/2006 Paul.  The easiest way to remove the old text values is to delete them all. 
					xFilter.RemoveAll();
					XmlUtil.SetSingleNode(xmlFilters, xFilter, "ID", sID);
				}
				string[] arrDATA_FIELD = sDATA_FIELD.Split('.');
				string sTABLE_NAME = String.Empty;
				string sFIELD_NAME = String.Empty;
				// 11/01/2010 Paul.  Add Custom Activity Action Type. 
				if ( arrDATA_FIELD.Length == 1 )
				{
					sFIELD_NAME = arrDATA_FIELD[0];
				}
				else
				{
					sTABLE_NAME = arrDATA_FIELD[0];
					sFIELD_NAME = arrDATA_FIELD[1];
				}
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "ACTION_TYPE"      , sACTION_TYPE                     );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "RELATIONSHIP_NAME", sRELATIONSHIP_NAME               );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE"           , sMODULE_NAME.Split(' ')[0]       );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "MODULE_NAME"      , sMODULE_NAME                     );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "TABLE_NAME"       , sTABLE_NAME                      );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_FIELD"       , sDATA_FIELD                      );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "FIELD_NAME"       , sFIELD_NAME                      );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "DATA_TYPE"        , sDATA_TYPE                       );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "OPERATOR"         , sOPERATOR                        );
				XmlUtil.SetSingleNode(xmlFilters, xFilter, "SEARCH_TEXT"      , String.Join(", ", arrSEARCH_TEXT));
				foreach ( string sSEARCH_TEXT in arrSEARCH_TEXT )
				{
					XmlNode xSearchText = xmlFilters.CreateElement("SEARCH_TEXT_VALUES");
					xFilter.AppendChild(xSearchText);
					xSearchText.InnerText = sSEARCH_TEXT;
				}
				
				rdl.SetCustomProperty("Filters", xmlFilters.OuterXml);
				
				dgFilters.DataSource = ReportFilters();
				dgFilters.DataBind();
			}
			catch(Exception ex)
			{
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void FiltersDelete(string sID)
		{
			dgFilters.EditItemIndex = -1;
			XmlDocument xmlFilters = rdl.GetCustomProperty("Filters");
			XmlNode xFilter = xmlFilters.DocumentElement.SelectSingleNode("Filter[ID=\'" + sID + "\']");
			if ( xFilter != null )
			{
				xFilter.ParentNode.RemoveChild(xFilter);
				rdl.SetCustomProperty("Filters", xmlFilters.OuterXml);
			}
			dgFilters.DataSource = ReportFilters();
			dgFilters.DataBind();
		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("WorkflowActionShells.LBL_NAME"));
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			rdl = new RdlDocument();
			xoml = new XomlDocument();
			lblRELATED             .Visible = bDebug;
			lblFILTER_COLUMN_SOURCE.Visible = bDebug;
			lblFILTER_COLUMN       .Visible = bDebug;
			lblFILTER_OPERATOR_TYPE.Visible = bDebug;
			lblFILTER_OPERATOR     .Visible = bDebug;
			lblFILTER_ID           .Visible = bDebug;
			try
			{
				reqNAME.DataBind();
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					// 07/13/2006 Paul.  We don't store the SHOW_QUERY value in the RDL, so we must retrieve it from the session. 
					chkSHOW_XOML.Checked = Sql.ToBoolean(Session["Workflows.SHOW_XOML"]);
#if DEBUG
					chkSHOW_XOML.Checked = true;
#endif
					// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
					ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
					ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);

					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL ;
						if ( !Sql.IsEmptyGuid(gID) )
						{
							sSQL = "select *                            " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ACTION_SHELLS_Edit" + ControlChars.CrLf
							     + " where ID = @ID                     " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID", gID);

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								// 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term("WorkflowActionShells.LBL_NAME") + " - " + ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											gPARENT_ID          = Sql.ToGuid  (rdr["PARENT_ID" ]);
											ViewState["PARENT_ID"] = gPARENT_ID;
											
											txtNAME.Text = Sql.ToString(rdr["NAME"]);
											string sRDL = Sql.ToString(rdr["RDL"]);
											try
											{
												if ( !Sql.IsEmptyString(sRDL) )
												{
													rdl.LoadRdl(sRDL);
													
													txtBASE_MODULE.Text = rdl.GetCustomPropertyValue("Module");
												}
											}
											catch
											{
											}
											string sXOML = Sql.ToString(rdr["XOML"]);
											try
											{
												if ( !Sql.IsEmptyString(sXOML) )
												{
													xoml.LoadXoml(sXOML);
												}
											}
											catch
											{
											}
										}
									}
								}
							}
						}
						else
						{
							gPARENT_ID = Sql.ToGuid(Request["PARENT_ID"]);
							ViewState["PARENT_ID"] = gPARENT_ID;
						}

						sSQL = "select BASE_MODULE" + ControlChars.CrLf
						     + "     , TYPE       " + ControlChars.CrLf
						     + "  from vwWORKFLOWS" + ControlChars.CrLf
						     + " where ID = @ID   " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gPARENT_ID);
							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									txtBASE_MODULE.Text = Sql.ToString(rdr["BASE_MODULE"]);
									sWORKFLOW_TYPE      = Sql.ToString(rdr["TYPE"       ]);
									ViewState["WORKFLOW_TYPE"] = sWORKFLOW_TYPE;
								}
							}
						}
					}
					if ( Sql.IsEmptyGuid(gPARENT_ID) || Sql.IsEmptyString(txtBASE_MODULE.Text) )
						Response.Redirect("~/Administration/Workflows/");
					
					lstACTION_TYPE.Items.Add(new ListItem(String.Format(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_UPDATE"), txtBASE_MODULE.Text), "update"    ));
					lstACTION_TYPE.Items.Add(new ListItem(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_UPDATE_REL"), "update_rel"));
					lstACTION_TYPE.Items.Add(new ListItem(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_NEW"       ), "new"       ));
					// 11/01/2010 Paul.  Add Custom Activity Action Type. 
					// 11/07/2010 Paul.  Add Custom Method and Custom Property. 
					lstACTION_TYPE.Items.Add(new ListItem(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_CUSTOM_PROPERTY"), "custom_prop"  ));
					lstACTION_TYPE.Items.Add(new ListItem(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_CUSTOM_METHOD"  ), "custom_method"));
					// 08/03/2012 Paul.  Add Custom Stored Procedure. 
					lstACTION_TYPE.Items.Add(new ListItem(L10n.Term("WorkflowActionShells.LBL_ACTION_TYPE_CUSTOM_PROCEDURE"), "custom_procedure"));

					// 05/27/2006 Paul.  This is a catch-all statement to create a new report if all else fails. 
					if ( rdl.DocumentElement == null )
					{
						// 07/22/2008 Paul.  Use a version of the RDL constructor that adds a minimum of tags. 
						rdl = new RdlDocument(String.Empty);
						rdl.SetCustomProperty("Module" , BaseModule  );
						rdl.SetCustomProperty("Related", lstRELATED.SelectedValue );
					}
					if ( xoml.DocumentElement == null )
					{
						xoml = new XomlDocument("Workflow1", gPARENT_ID);
					}
					lstRELATED_Bind();
					lblRELATED.Text = lstRELATED.SelectedValue;
					try
					{
						// 08/19/2010 Paul.  Check the list before assigning the value. 
						Utils.SetSelectedValue(lstRELATED, rdl.GetCustomPropertyValue("Related"));
					}
					catch
					{
					}
					// 07/26/2007 Paul.  The column sources need to be updated after the related has changed. 
					lstFILTER_COLUMN_SOURCE_Bind();
					BuildWorkflowXOML();

					dgFilters.DataSource = ReportFilters();
					dgFilters.DataBind();
				}
				else
				{
					// 07/13/2006 Paul.  Save the SHOW_QUERY flag in the Session so that it will be available across redirects. 
					Session["Workflows.SHOW_XOML"] = chkSHOW_XOML.Checked;

					string sRDL = Sql.ToString(ViewState["rdl"]);
					rdl.LoadRdl(sRDL);
					string sXOML = Sql.ToString(ViewState["xoml"]);
					xoml.LoadXoml(sXOML);
					
					// 11/16/2008 Paul.  Time-based workflows reference the base module, not the audit table. 
					sWORKFLOW_TYPE = Sql.ToString(ViewState["WORKFLOW_TYPE"]);
					BuildWorkflowXOML();

					dgFilters.DataSource = ReportFilters();
					dgFilters.DataBind();

					// 12/02/2005 Paul.  When validation fails, the header title does not retain its value.  Update manually. 
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term("WorkflowActionShells.LBL_NAME") + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_PreRender(object sender, System.EventArgs e)
		{
			if ( chkSHOW_XOML.Checked )
			{
				// 07/15/2010 Paul.  Use new function to format Xoml. 
				litWORKFLOW_XML.Text = XomlUtils.XomlEncode(xoml.OuterXml);
			}
			else
			{
				// 06/27/2010 Paul.  Make sure to remove the text when disabled. 
				litWORKFLOW_XML.Text = String.Empty;
			}
			ViewState["rdl"] = rdl.OuterXml;
			ViewState["xoml"] = xoml.OuterXml;
			Session["xoml"] = xoml.OuterXml;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new System.EventHandler(this.Page_PreRender);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "WorkflowActionShells";
			// 05/06/2010 Paul.  The menu will show the admin Module Name in the Six theme. 
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				// 03/20/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}
