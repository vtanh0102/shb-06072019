/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Diagnostics;

namespace SplendidCRM.Administration.Workflows
{
	/// <summary>
	/// Summary description for Image.
	/// </summary>
	public class Image : SplendidPage
	{
		protected Guid   gID  ;
		protected string sXoml;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Response.ExpiresAbsolute = new DateTime(1980, 1, 1, 0, 0, 0, 0);
			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !Sql.IsEmptyGuid(gID) )
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						string sSQL ;
						sSQL = "select XOML            " + ControlChars.CrLf
						     + "  from vwWORKFLOWS_Edit" + ControlChars.CrLf
						     + " where ID = @ID        " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gID);
							con.Open();
							sXoml = Sql.ToString(cmd.ExecuteScalar());
						}
					}
				}
				else
				{
					sXoml = Sql.ToString(Session["xoml"]);
				}
				if ( (SplendidCRM.Security.AdminUserAccess("Workflows", "view") >= 0) && !Sql.IsEmptyString(sXoml) )
				{
					using (WorkflowImageProvider.WorkflowDesigner.WorkflowDesignerControl ctrl = new WorkflowImageProvider.WorkflowDesigner.WorkflowDesignerControl())
					{
						ctrl.Xoml = sXoml;
						using (MemoryStream ms = new MemoryStream())
						{
							//if (act != String.Empty)
							//	ctrl.HighlightActivity(act);
							ctrl.SaveWorkflowImage(ms, ImageFormat.Png);
							ms.Seek(0, 0);
							Response.ContentType = "image/png";
							Response.BinaryWrite(ms.GetBuffer());
						}
						Response.End();
					}
				}
				else
				{
					Response.ContentType = "image/gif";
					Response.WriteFile(Request.MapPath("~/include/images/blank.gif"));
				}
			}
			catch(Exception ex)
			{
				// 06/16/2016 Paul.  The Response.End() will throw a ThreadAbortException. 
				if ( !(ex is System.Threading.ThreadAbortException) )
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					Response.Write(ex.Message);
					Response.Write("<pre>");
					Response.Write(Server.HtmlEncode(sXoml));
					Response.Write("</pre>");
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
