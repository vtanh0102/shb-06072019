<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.Workflows.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="SplendidCRM" Tagname="DatePicker" Src="~/_controls/DatePicker.ascx" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Workflows" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

<asp:UpdatePanel ID="ctlEditViewPanel" runat="server">
	<ContentTemplate>
		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<table ID="tblMain" class="tabEditView" runat="server">
					</table>
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell Width="15%" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_NAME"  ) %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField">
								<asp:TextBox ID="NAME" size="35" MaxLength="255" Runat="server" />
								<asp:RequiredFieldValidator ID="NAME_REQUIRED" ControlToValidate="NAME" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableViewState="false" EnableClientScript="false" Enabled="false" runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="15%" CssClass="dataLabel"></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField"></asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_TYPE") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataField"><asp:DropDownList ID="TYPE" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="TYPE_Changed" AutoPostBack="true" Runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_STATUS") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataField"><asp:DropDownList ID="STATUS" DataValueField="NAME" DataTextField="DISPLAY_NAME" Runat="server" /></asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_BASE_MODULE") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataField"><asp:DropDownList ID="BASE_MODULE" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstMODULE_Changed" AutoPostBack="true" Runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel">
								<asp:Panel ID="pnlRECORD_TYPE_LABEL" runat="server">
									<%= L10n.Term("Workflows.LBL_RECORD_TYPE") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" />
								</asp:Panel>
								<asp:Label ID="lblFREQUENCY_LABEL" Text='<%# L10n.Term("Workflows.LBL_FREQUENCY_LIMIT") %>' runat="server" />
							</asp:TableCell>
							<asp:TableCell CssClass="dataField">
								<asp:DropDownList ID="RECORD_TYPE"        DataValueField="NAME" DataTextField="DISPLAY_NAME" Runat="server" />
								<asp:TextBox      ID="FREQUENCY_VALUE"    size="3" MaxLength="25" OnSelectedIndexChanged="FREQUENCY_INTERVAL_Changed" AutoPostBack="true" Visible="false" Runat="server" />&nbsp;
								<asp:DropDownList ID="FREQUENCY_INTERVAL" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="FREQUENCY_INTERVAL_Changed" AutoPostBack="true" Visible="false" Runat="server" />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_FIRE_ORDER") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataField"><asp:DropDownList ID="FIRE_ORDER" DataValueField="NAME" DataTextField="DISPLAY_NAME" Runat="server" /></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><asp:Panel ID="pnlINTERVAL_LABEL" Visible="false" runat="server"><%= L10n.Term("Schedulers.LBL_INTERVAL") %> <asp:Label Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' CssClass="required" runat="server" /></asp:Panel></asp:TableCell>
							<asp:TableCell VerticalAlign="Top" RowSpan="2">
								<asp:UpdatePanel UpdateMode="Conditional" runat="server">
									<ContentTemplate>
										<asp:Panel ID="pnlINTERVAL_DATA" Visible="false" runat="server">
											<%@ Register TagPrefix="SplendidCRM" Tagname="CRON" Src="~/_controls/CRON.ascx" %>
											<SplendidCRM:CRON ID="ctlCRON" Runat="Server" />
										</asp:Panel>
									</ContentTemplate>
								</asp:UpdatePanel>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top" CssClass="dataLabel"><%= L10n.Term("Workflows.LBL_DESCRIPTION") %></asp:TableCell>
							<asp:TableCell ColumnSpan="2"><asp:TextBox ID="DESCRIPTION" TextMode="MultiLine" Rows="4" Columns="60" runat="server" /></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<h4><asp:Label Text='<%# L10n.Term("Workflows.LBL_FILTERS") %>' runat="server" /></h4>
					&nbsp;
					<asp:Label Text='<%# L10n.Term("Workflows.LBL_RELATED") %>' CssClass="dataLabel" runat="server" />&nbsp;
					<asp:DropDownList ID="lstRELATED" TabIndex="3" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstRELATED_Changed" AutoPostBack="true" Runat="server" />
					&nbsp;
					<asp:Label ID="lblRELATED" runat="server" />
					&nbsp;
					<asp:Label Text='<%# L10n.Term("Workflows.LBL_SHOW_QUERY") %>' CssClass="dataLabel" runat="server" />&nbsp;<asp:CheckBox ID="chkSHOW_QUERY" CssClass="checkbox" AutoPostBack="true" runat="server" />
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell style="padding-top: 5px; padding-bottom: 5px;">
					<asp:DataGrid ID="dgFilters" AutoGenerateColumns="false"
						CellPadding="3" CellSpacing="0" 
						AllowPaging="false" AllowSorting="false" ShowHeader="true"
						EnableViewState="true" runat="server">
						<Columns>
							<asp:BoundColumn HeaderText="Module"   DataField="MODULE_NAME" />
							<asp:BoundColumn HeaderText="Field"    DataField="DATA_FIELD"  />
							<asp:BoundColumn HeaderText="Type"     DataField="DATA_TYPE"   />
							<asp:BoundColumn HeaderText="Operator" DataField="OPERATOR"    />
							<asp:BoundColumn HeaderText="Search"   DataField="SEARCH_TEXT" />
							<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
								<ItemTemplate>
									<asp:Button ID="btnEditFilter" CommandName="Filters.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_EDIT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_EDIT_BUTTON_TITLE") %>' Runat="server" />
									&nbsp;
									<asp:Button ID="btnDeleteFilter" CommandName="Filters.Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("Workflows.LBL_REMOVE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("Workflows.LBL_REMOVE_BUTTON_TITLE") %>' Runat="server" />
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:DataGrid>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<input id="txtFILTER_ID" type="hidden" runat="server" />
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_COLUMN_SOURCE" TabIndex="10" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_SOURCE_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_COLUMN_SOURCE" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_COLUMN" TabIndex="11" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_COLUMN" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_OPERATOR" TabIndex="12" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_OPERATOR_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_OPERATOR_TYPE" runat="server" /><asp:Image SkinID="Spacer" Width="4" runat="server" />
								<asp:Label ID="lblFILTER_OPERATOR" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top" Wrap="false">
								<asp:Table BorderWidth="0" CellSpacing="0" CellPadding="0" runat="server">
									<asp:TableRow>
										<asp:TableCell>
											<input type="hidden" id="txtFILTER_SEARCH_ID" runat="server" />
											<input type="hidden" id="txtFILTER_SEARCH_DATA_TYPE" runat="server" />
											
											<asp:TextBox ID="txtFILTER_SEARCH_TEXT"   runat="server" />
											
											<asp:DropDownList ID="lstFILTER_SEARCH_DROPDOWN" DataValueField="NAME" DataTextField="DISPLAY_NAME" runat="server" />
											<asp:ListBox      ID="lstFILTER_SEARCH_LISTBOX"  DataValueField="NAME" DataTextField="DISPLAY_NAME" SelectionMode="Multiple" runat="server" />
											
											<SplendidCRM:DatePicker ID="ctlFILTER_SEARCH_START_DATE" EnableDateFormat="false" Runat="Server" />
										</asp:TableCell>
										<asp:TableCell>
											<asp:Label ID="lblFILTER_AND_SEPARATOR" Text='<%# L10n.Term("Workflows.LBL_AND") %>' runat="server" />
										</asp:TableCell>
										<asp:TableCell>
											<SplendidCRM:DatePicker ID="ctlFILTER_SEARCH_END_DATE" EnableDateFormat="false" Runat="Server" />
											
											<asp:TextBox ID="txtFILTER_SEARCH_TEXT2"  runat="server" />
											
											<asp:Button ID="btnFILTER_SEARCH_SELECT" Visible="false" UseSubmitBehavior="false" OnClientClick="SearchPopup(); return false;" CssClass="button" Text='<%# L10n.Term(".LBL_SELECT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_SELECT_BUTTON_TITLE") %>' runat="server" />
										</asp:TableCell>
									</asp:TableRow>
								</asp:Table>
								<asp:Label ID="lblFILTER_ID" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button CommandName="Filters.Update" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_UPDATE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_UPDATE_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button CommandName="Filters.Cancel" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="80%"></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Literal ID="litREPORT_SQL" runat="server" />
		<asp:Literal ID="litREPORT_XML" Visible="false" runat="server" />
	</ContentTemplate>
</asp:UpdatePanel>
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>
