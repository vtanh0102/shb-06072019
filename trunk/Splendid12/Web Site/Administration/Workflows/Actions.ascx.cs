/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.Workflow
{
	/// <summary>
	///		Summary description for Actions.
	/// </summary>
	public class Actions : SplendidControl
	{
		// 06/03/2015 Paul.  Combine ListHeader and DynamicButtons. 
		protected _controls.SubPanelButtons ctlDynamicButtons;
		protected UniqueStringCollection arrSelectFields;
		protected Guid            gID            ;
		protected DataView        vwMain         ;
		protected SplendidGrid    grdMain        ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch ( e.CommandName )
				{
					case "WorkflowActionShells.Create":
					{
						Response.Redirect("~/Administration/WorkflowActionShells/edit.aspx?PARENT_ID=" + gID.ToString());
						break;
					}
					case "WorkflowActionShells.Edit":
					{
						Guid gACTION_ID = Sql.ToGuid(e.CommandArgument);
						Response.Redirect("~/Administration/WorkflowActionShells/edit.aspx?PARENT_ID=" + gID.ToString() + "&ID=" + gACTION_ID.ToString());
						break;
					}
					case "WorkflowActionShells.Delete":
					{
						Guid gACTION_ID = Sql.ToGuid(e.CommandArgument);
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									SqlProcs.spWORKFLOW_ACTION_SHELLS_Delete(gACTION_ID, trn);
									WorkflowBuilder.UpdateMasterWorkflowXoml(Application, gID, trn);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
						}
						//BindGrid();
						// 08/22/2008 Paul.  Rebinding is not sufficient as we need to rebuild the XOML image. 
						Response.Redirect("~/Administration/Workflows/view.aspx?ID=" + gID.ToString());
						break;
					}
					default:
						throw(new Exception("Unknown command: " + e.CommandName));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void BindGrid()
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL;
				// 04/26/2008 Paul.  Build the list of fields to use in the select clause.
				sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
				     + "  from vwWORKFLOW_ACTION_SHELLS" + ControlChars.CrLf
				     + " where 1 = 1                   " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AppendParameter(cmd, gID, "PARENT_ID");
					// 04/26/2008 Paul.  Move Last Sort to the database.
					cmd.CommandText += grdMain.OrderByClause("DATE_ENTERED", "asc");

					if ( bDebug )
						RegisterClientScriptBlock("vwWORKFLOW_ACTION_SHELLS", Sql.ClientScriptBlock(cmd));

					try
					{
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								foreach ( DataRow row in dt.Rows )
								{
									/*
									string sFIELD           = Sql.ToString(row["FIELD"          ]);
									string sTYPE            = Sql.ToString(row["TYPE"           ]);
									string sFRAME_TYPE      = Sql.ToString(row["FRAME_TYPE"     ]);
									string sEVAL            = Sql.ToString(row["EVAL"           ]);
									string sSHOW_PAST       = Sql.ToString(row["SHOW_PAST"      ]);
									string sREL_MODULE      = Sql.ToString(row["REL_MODULE"     ]);
									string sREL_MODULE_TYPE = Sql.ToString(row["REL_MODULE_TYPE"]);
									string sPARAMETERS      = Sql.ToString(row["PARAMETERS"     ]);
									string sDESCRIPTION     = String.Empty;
									switch ( sTYPE.ToLower() )
									{
										case "compare_specific":
											sDESCRIPTION = String.Format("When {0} changes to or from specified value", Utils.TableColumnName(L10n, sREL_MODULE, sFIELD));
											break;
										case "trigger_record_change":
											sDESCRIPTION = "When the target module changes";
											break;
										case "compare_change":
											sDESCRIPTION = String.Format("When {0} changes", Utils.TableColumnName(L10n, sREL_MODULE, sFIELD));
											break;
										case "filter_field":
											break;
										case "filter_rel_field":
											break;
									}
									row["DESCRIPTION"] = sDESCRIPTION;
									*/
								}
								vwMain = dt.DefaultView;
								grdMain.DataSource = vwMain ;
								// 09/05/2005 Paul.  LinkButton controls will not fire an event unless the the grid is bound. 
								// 04/25/2008 Paul.  Enable sorting of sub panel. 
								// 04/26/2008 Paul.  Move Last Sort to the database.
								// 05/16/2008 Paul.  We must always rebind otherwise a postback from an alternate subpanel will cause this grid be incomplete.
								grdMain.DataBind();
							}
						}
					}
					catch(Exception ex)
					{
						SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						ctlDynamicButtons.ErrorText = ex.Message;
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			if ( !(SplendidCRM.Security.AdminUserAccess("Workflows", "view") >= 0) )
				return;
			
			gID = Sql.ToGuid(Request["ID"]);
			BindGrid();

			if ( !IsPostBack )
			{
				ctlDynamicButtons.Visible = !PrintView && (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
				ctlDynamicButtons.AppendButtons("Workflows." + m_sMODULE, Guid.Empty, gID);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "WorkflowActionShells";
			// 04/26/2008 Paul.  We need to build a list of the fields used by the search clause. 
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("ID"          );
			arrSelectFields.Add("DATE_ENTERED");
			arrSelectFields.Add("CUSTOM_XOML" );

			// 11/26/2005 Paul.  Add fields early so that sort events will get called. 
			this.AppendGridColumns(grdMain, "Workflows." + m_sMODULE, arrSelectFields);
			// 04/28/2008 Paul.  Make use of dynamic buttons. 
			if ( IsPostBack )
				ctlDynamicButtons.AppendButtons("Workflows." + m_sMODULE, Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}
