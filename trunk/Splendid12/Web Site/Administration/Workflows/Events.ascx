<%@ Control CodeBehind="Events.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.Workflow.Events" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */

</script>
<%-- 06/03/2015 Paul.  Combine ListHeader and DynamicButtons. --%>
<%@ Register TagPrefix="SplendidCRM" Tagname="SubPanelButtons" Src="~/_controls/SubPanelButtons.ascx" %>
<SplendidCRM:SubPanelButtons ID="ctlDynamicButtons" Module="Workflows" SubPanel="divWorkflowEvents" Title="Workflows.LBL_EVENTS_TITLE" Runat="Server" />

<div id="divWorkflowEvents" style='<%= "display:" + (CookieValue("divWorkflowEvents") != "1" ? "inline" : "none") %>'>
	<SplendidCRM:SplendidGrid id="grdMain" SkinID="grdSubPanelView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
		<Columns>
			<asp:TemplateColumn  HeaderText="" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:HyperLink NavigateUrl='<%# "~/Administration/WorkflowEventLog/Popup.aspx?ID=" + DataBinder.Eval(Container.DataItem, "WORKFLOW_INSTANCE_ID") %>' CssClass="listViewTdToolsS1" Target="WorkflowEventLog" Runat="server">
						<asp:Image SkinID="view_inline" AlternateText='<%# L10n.Term(".LNK_VIEW") %>' Runat="server" />&nbsp;<%# L10n.Term(".LNK_VIEW") %>
					</asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
</div>
