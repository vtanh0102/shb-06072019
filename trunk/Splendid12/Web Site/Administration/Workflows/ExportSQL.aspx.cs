/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SplendidCRM.Administration.Workflows
{
	/// <summary>
	/// Summary description for ExportSQL.
	/// </summary>
	public class ExportSQL : SplendidPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.GetUserAccess("Workflows", "export") >= 0);
			if ( !this.Visible )
				return;
			
			try
			{
				Guid gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					if ( !Sql.IsEmptyGuid(gID) )
					{
						string sFileName = String.Empty;
						string sProcedureName = String.Empty;
						StringBuilder sb = new StringBuilder();
						
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL ;
							sSQL = "select *               " + ControlChars.CrLf
							     + "  from vwWORKFLOWS_Edit" + ControlChars.CrLf
							     + " where ID = @ID        " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID", gID);
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									if ( rdr.Read() )
									{
										string sNAME         = Sql.ToString (rdr["NAME"        ]);
										string sBASE_MODULE  = Sql.ToString (rdr["BASE_MODULE" ]);
										string sAUDIT_TABLE  = Sql.ToString (rdr["AUDIT_TABLE" ]);
										int    nSTATUS       = Sql.ToInteger(rdr["STATUS"      ]);
										string sTYPE         = Sql.ToString (rdr["TYPE"        ]);
										string sFIRE_ORDER   = Sql.ToString (rdr["FIRE_ORDER"  ]);
										string sPARENT_ID    = Sql.ToString (rdr["PARENT_ID"   ]);
										string sRECORD_TYPE  = Sql.ToString (rdr["RECORD_TYPE" ]);
										int    nLIST_ORDER_Y = Sql.ToInteger(rdr["LIST_ORDER_Y"]);
										int    nCUSTOM_XOML  = Sql.ToInteger(rdr["CUSTOM_XOML" ]);
										string sFILTER_SQL   = Sql.ToString (rdr["FILTER_SQL"  ]);
										string sJOB_INTERVAL = Sql.ToString (rdr["JOB_INTERVAL"]);
										string sDESCRIPTION  = Sql.ToString (rdr["DESCRIPTION" ]);
										string sFILTER_XML   = Sql.ToString (rdr["FILTER_XML"  ]);
										string sXOML         = Sql.ToString (rdr["XOML"        ]);

										// 01/23/2012 Paul.  Remove some of the bulk of the XML. 
										int nRelatedModulesStart = sFILTER_XML.IndexOf("<CustomProperty><Name>crm:RelatedModules</Name>");
										if ( nRelatedModulesStart >= 0 )
										{
											int nRelatedModulesEnd = sFILTER_XML.IndexOf("</CustomProperty>", nRelatedModulesStart);
											sFILTER_XML = sFILTER_XML.Substring(0, nRelatedModulesStart) + sFILTER_XML.Substring(nRelatedModulesEnd + "</CustomProperty>".Length);
										}
										int nRelationshipsStart = sFILTER_XML.IndexOf("<CustomProperty><Name>crm:Relationships</Name>");
										if ( nRelationshipsStart >= 0 )
										{
											int nRelationshipsEnd = sFILTER_XML.IndexOf("</CustomProperty>", nRelationshipsStart);
											sFILTER_XML = sFILTER_XML.Substring(0, nRelationshipsStart) + sFILTER_XML.Substring(nRelationshipsEnd + "</CustomProperty>".Length);
										}

										sFileName = sNAME;
										// 01/23/2012 Paul.  We cannot add the header until we know the name of the workflow. 
										sProcedureName = "spWORKFLOWS_" + sNAME.Replace(" ", "_").Replace("\'", "");
										sb.AppendLine("/* -- #if IBM_DB2");
										sb.AppendLine("call dbo.spSqlDropProcedure('" + sProcedureName + "')");
										sb.AppendLine("/");
										sb.AppendLine("");
										sb.AppendLine("Create Procedure dbo." + sProcedureName + "()");
										sb.AppendLine("language sql");
										sb.AppendLine("  begin");
										sb.AppendLine("	declare in_USER_ID char(36);");
										sb.AppendLine("-- #endif IBM_DB2 */");
										sb.AppendLine("");
										sb.AppendLine("/* -- #if Oracle");
										sb.AppendLine("Declare");
										sb.AppendLine("	StoO_selcnt INTEGER := 0;");
										sb.AppendLine("	in_ID char(36);");
										sb.AppendLine("BEGIN");
										sb.AppendLine("	BEGIN");
										sb.AppendLine("-- #endif Oracle */");
										sb.AppendLine("");
										sb.AppendLine("print 'WORKFLOWS " + Sql.EscapeSQL(sNAME) + "';");
										sb.AppendLine("GO");
										sb.AppendLine("");
										sb.AppendLine("set nocount on;");
										sb.AppendLine("GO");
										sb.AppendLine("");
										sb.AppendLine("declare @ID uniqueidentifier;");
										sb.AppendLine("set @ID = '" + gID.ToString() + "';");
										sb.AppendLine("if not exists(select * from WORKFLOW where ID = @ID) begin -- then");
										sb.AppendLine("	exec dbo.spWORKFLOWS_Update @ID, null, "
														+ Sql.FormatSQL(sNAME        , 0) + ", "
														+ Sql.FormatSQL(sBASE_MODULE , 0) + ", "
														+ Sql.FormatSQL(sAUDIT_TABLE , 0) + ", "
														+ nSTATUS.ToString()              + ", "
														+ Sql.FormatSQL(sTYPE        , 0) + ", "
														+ Sql.FormatSQL(sFIRE_ORDER  , 0) + ", "
														+ Sql.FormatSQL(sRECORD_TYPE , 0) + ", "
														+ Sql.FormatSQL(sDESCRIPTION , 0) + ", "
														+ Sql.FormatSQL(sFILTER_SQL  , 0) + ", "
														+ Sql.FormatSQL(sFILTER_XML  , 0) + ", "
														+ Sql.FormatSQL(sJOB_INTERVAL, 0) + ";");
										sb.AppendLine("");
										sb.AppendLine("	exec dbo.spWORKFLOWS_UpdateXOML @ID, null, " + nCUSTOM_XOML.ToString() + ", " + Sql.FormatSQL(sXOML, 0) +";");
										sb.AppendLine("");
									}
								}
							}
							
							sSQL = "select *                           " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ALERT_SHELLS_Edit" + ControlChars.CrLf
							     + " where PARENT_ID = @PARENT_ID      " + ControlChars.CrLf
							     + " order by DATE_ENTERED             " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@PARENT_ID", gID);
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									while ( rdr.Read() )
									{
										string sNAME               = Sql.ToString (rdr["NAME"              ]);
										string sALERT_TEXT         = Sql.ToString (rdr["ALERT_TEXT"        ]);
										string sALERT_TYPE         = Sql.ToString (rdr["ALERT_TYPE"        ]);
										string sSOURCE_TYPE        = Sql.ToString (rdr["SOURCE_TYPE"       ]);
										Guid   gCUSTOM_TEMPLATE_ID = Sql.ToGuid   (rdr["CUSTOM_TEMPLATE_ID"]);
										int    nCUSTOM_XOML        = Sql.ToInteger(rdr["CUSTOM_XOML"       ]);
										string sRDL                = Sql.ToString (rdr["RDL"               ]);
										string sXOML               = Sql.ToString (rdr["XOML"              ]);
										string sCUSTOM_TEMPLATE_ID = String.Empty;
										if ( !Sql.IsEmptyGuid(gCUSTOM_TEMPLATE_ID) )
											sCUSTOM_TEMPLATE_ID = gCUSTOM_TEMPLATE_ID.ToString();
										sb.AppendLine("	exec dbo.spWORKFLOW_ALERT_SHELLS_InsertOnly null, @ID, " 
														+ Sql.FormatSQL(sNAME              , 0) + ", "
														+ Sql.FormatSQL(sALERT_TYPE        , 0) + ", "
														+ Sql.FormatSQL(sSOURCE_TYPE       , 0) + ", "
														+ Sql.FormatSQL(sCUSTOM_TEMPLATE_ID, 0) + ", "
														+ Sql.FormatSQL(sALERT_TEXT        , 0) + ", "
														+ Sql.FormatSQL(sRDL               , 0) + ", "
														+ Sql.FormatSQL(sXOML              , 0) + ";");
										sb.AppendLine("");
									}
								}
							}
							
							sSQL = "select *                           " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ALERT_TEMPLATES  " + ControlChars.CrLf
							     + " where ID = (select CUSTOM_TEMPLATE_ID from vwWORKFLOW_ALERT_SHELLS where PARENT_ID = @PARENT_ID)" + ControlChars.CrLf
							     + " order by DATE_ENTERED             " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@PARENT_ID", gID);
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									while ( rdr.Read() )
									{
										Guid   gTEMPLATE_ID = Sql.ToGuid   (rdr["ID"          ]);
										string sNAME        = Sql.ToString (rdr["NAME"        ]);
										string sBASE_MODULE = Sql.ToString (rdr["BASE_MODULE" ]);
										string sFROM_ADDR   = Sql.ToString (rdr["FROM_ADDR"   ]);
										string sFROM_NAME   = Sql.ToString (rdr["FROM_NAME"   ]);
										string sDESCRIPTION = Sql.ToString (rdr["DESCRIPTION" ]);
										string sSUBJECT     = Sql.ToString (rdr["SUBJECT"     ]);
										string sBODY        = Sql.ToString (rdr["BODY"        ]);
										string sBODY_HTML   = Sql.ToString (rdr["BODY_HTML"   ]);
										sb.AppendLine("	exec dbo.spWORKFLOW_ALERT_TEMPLATES_Update '" + gTEMPLATE_ID.ToString() + "', null, " 
														+ Sql.FormatSQL(sNAME       , 0) + ", "
														+ Sql.FormatSQL(sBASE_MODULE, 0) + ", "
														+ Sql.FormatSQL(sFROM_ADDR  , 0) + ", "
														+ Sql.FormatSQL(sFROM_NAME  , 0) + ", "
														+ Sql.FormatSQL(sDESCRIPTION, 0) + ", "
														+ Sql.FormatSQL(sSUBJECT    , 0) + ", "
														+ Sql.FormatSQL(sBODY       , 0) + ", "
														+ Sql.FormatSQL(sBODY_HTML  , 0) + ";");
										sb.AppendLine("");
									}
								}
							}
							
							sSQL = "select *                            " + ControlChars.CrLf
							     + "  from vwWORKFLOW_ACTION_SHELLS_Edit" + ControlChars.CrLf
							     + " where PARENT_ID = @PARENT_ID       " + ControlChars.CrLf
							     + " order by DATE_ENTERED              " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@PARENT_ID", gID);
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									while ( rdr.Read() )
									{
										string sNAME               = Sql.ToString (rdr["NAME"              ]);
										string sACTION_TYPE        = Sql.ToString (rdr["ACTION_TYPE"       ]);
										int    nCUSTOM_XOML        = Sql.ToInteger(rdr["CUSTOM_XOML"       ]);
										string sRDL                = Sql.ToString (rdr["RDL"               ]);
										string sXOML               = Sql.ToString (rdr["XOML"              ]);
										sb.AppendLine("	exec dbo.spWORKFLOW_ACTION_SHELLS_InsertOnly null, @ID, " 
														+ Sql.FormatSQL(sNAME              , 0) + ", "
														+ Sql.FormatSQL(sACTION_TYPE       , 0) + ", "
														+ Sql.FormatSQL(sRDL               , 0) + ", "
														+ Sql.FormatSQL(sXOML              , 0) + ";");
										sb.AppendLine("");
									}
								}
							}
							
							sSQL = "select *                        " + ControlChars.CrLf
							     + "  from vwWORKFLOW_TRIGGER_SHELLS" + ControlChars.CrLf
							     + " where PARENT_ID = @PARENT_ID   " + ControlChars.CrLf
							     + " order by DATE_ENTERED          " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@PARENT_ID", gID);
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									while ( rdr.Read() )
									{
										string  sFIELD           = Sql.ToString (rdr["FIELD"          ]);
										string  sTYPE            = Sql.ToString (rdr["TYPE"           ]);
										string  sFRAME_TYPE      = Sql.ToString (rdr["FRAME_TYPE"     ]);
										string  sEVAL            = Sql.ToString (rdr["EVAL"           ]);
										int     nSHOW_PAST       = Sql.ToInteger(rdr["SHOW_PAST"      ]);
										string  sREL_MODULE      = Sql.ToString (rdr["REL_MODULE"     ]);
										string  sREL_MODULE_TYPE = Sql.ToString (rdr["REL_MODULE_TYPE"]);
										string  sPARAMETERS      = Sql.ToString (rdr["PARAMETERS"     ]);
										sb.AppendLine("	exec dbo.spWORKFLOW_TRIGGER_SHELLS_InsertOnly null, @ID, " 
														+ Sql.FormatSQL(sFIELD          , 0) + ", "
														+ Sql.FormatSQL(sTYPE           , 0) + ", "
														+ Sql.FormatSQL(sFRAME_TYPE     , 0) + ", "
														+ Sql.FormatSQL(sEVAL           , 0) + ", "
														+ nSHOW_PAST.ToString()              + ", "
														+ Sql.FormatSQL(sREL_MODULE     , 0) + ", "
														+ Sql.FormatSQL(sREL_MODULE_TYPE, 0) + ", "
														+ Sql.FormatSQL(sPARAMETERS     , 0) + ";");
									}
								}
							}
							sb.AppendLine("end -- if;");
							sb.AppendLine("GO");
							sb.AppendLine("");
							sb.AppendLine("set nocount off;");
							sb.AppendLine("GO");
							sb.AppendLine("");
							sb.AppendLine("/* -- #if Oracle");
							sb.AppendLine("	EXCEPTION");
							sb.AppendLine("		WHEN NO_DATA_FOUND THEN");
							sb.AppendLine("			StoO_selcnt := 0;");
							sb.AppendLine("		WHEN OTHERS THEN");
							sb.AppendLine("			RAISE;");
							sb.AppendLine("	END;");
							sb.AppendLine("	COMMIT WORK;");
							sb.AppendLine("END;");
							sb.AppendLine("/");
							sb.AppendLine("-- #endif Oracle */");
							sb.AppendLine("");
							sb.AppendLine("/* -- #if IBM_DB2");
							sb.AppendLine("	commit;");
							sb.AppendLine("  end");
							sb.AppendLine("/");
							sb.AppendLine("");
							sb.AppendLine("call dbo." + sProcedureName + "()");
							sb.AppendLine("/");
							sb.AppendLine("");
							sb.AppendLine("call dbo.spSqlDropProcedure('" + sProcedureName + "')");
							sb.AppendLine("/");
							sb.AppendLine("");
							sb.AppendLine("-- #endif IBM_DB2 */");
							sb.AppendLine("");
						}
						
						Response.ContentType = "text/sql";
						// 12/20/2009 Paul.  Use our own encoding so that a space does not get converted to a +. 
						Response.AddHeader("Content-Disposition", "attachment;filename=" + Utils.ContentDispositionEncode(Request.Browser, sFileName + ".sql"));
						Response.Write(sb.ToString());
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				Response.Write(ex.Message);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
