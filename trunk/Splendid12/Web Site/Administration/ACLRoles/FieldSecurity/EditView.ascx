<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.ACLRoles.FieldSecurity.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2010-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
<asp:UpdatePanel UpdateMode="Conditional" runat="server">
	<ContentTemplate>
		<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
		<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
		<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
		<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="Roles" EnableModuleLabel="false" EnablePrint="false" HelpName="FieldSecurity" EnableHelp="true" Runat="Server" />

		<asp:Table ID="tabSearch" SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<asp:Table SkinID="tabSearchView" runat="server">
						<asp:TableRow>
							<asp:TableCell CssClass="dataLabel" Wrap="false">
								<%= L10n.Term("Roles.LBL_LIST_MODULES") %>&nbsp;&nbsp;<asp:DropDownList ID="lstMODULES" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" AutoPostBack="True" OnSelectedIndexChanged="lstMODULES_Changed" Runat="server" />
							</asp:TableCell>
							<asp:TableCell HorizontalAlign="Right">
								<asp:Button ID="btnSearch" CommandName="Search" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_SEARCH_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_SEARCH_BUTTON_TITLE") %>' Runat="server" />
								<asp:Literal Text="&nbsp;" runat="server" />
								<asp:Button ID="btnCancel" CommandName="CancelFieldSecurity" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>
		
		<table ID="tblMain" class="tabDetailView" runat="server">
		</table>
		<br />
		<%@ Register TagPrefix="SplendidCRM" Tagname="ListHeader" Src="~/_controls/ListHeader.ascx" %>
		<SplendidCRM:ListHeader ID="ctlListHeader" Runat="Server" />

		<%@ Register TagPrefix="SplendidCRM" Tagname="AccessView" Src="AccessView.ascx" %>
		<SplendidCRM:AccessView ID="ctlAccessView" EnableACLEditing="true" Runat="Server" />

		<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
		<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
		<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
	</ContentTemplate>
</asp:UpdatePanel>
</div>
