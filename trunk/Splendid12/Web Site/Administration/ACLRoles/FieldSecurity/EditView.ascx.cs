/**
 * Copyright (C) 2010-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._controls;

namespace SplendidCRM.Administration.ACLRoles.FieldSecurity
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;
		protected _controls.ListHeader     ctlListHeader    ;
		protected AccessView               ctlAccessView    ;

		protected Guid         gID              ;
		protected string       sMODULE_NAME     ;
		protected DropDownList lstMODULES       ;
		protected HtmlTable    tblMain          ;
		protected Table        tabSearch        ;
		protected DataView     vwMain           ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Save" )
				{
					DataTable dtACLACCESS = null;
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						sSQL = "select MODULE_NAME                  " + ControlChars.CrLf
						     + "     , DISPLAY_NAME                 " + ControlChars.CrLf
						     + "     , FIELD_NAME                   " + ControlChars.CrLf
						     + "     , ACLACCESS                    " + ControlChars.CrLf
						     + "  from vwACL_FIELD_ACCESS_RoleFields" + ControlChars.CrLf
						     + " where ROLE_ID     = @ROLE_ID       " + ControlChars.CrLf
						     + "   and MODULE_NAME = @MODULE_NAME   " + ControlChars.CrLf
						     + " order by FIELD_NAME                " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ROLE_ID"    , gID         );
							Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dtACLACCESS = new DataTable();
								da.Fill(dtACLACCESS);
							}
						}
						
						using ( IDbTransaction trn = Sql.BeginTransaction(con) )
						{
							try
							{
								foreach(DataRow row in dtACLACCESS.Rows)
								{
									string sFIELD_NAME = Sql.ToString(row["FIELD_NAME"]);
									// 04/25/2006 Paul.  FindControl needs to be executed on the DataGridItem.  I'm not sure why.
									DropDownList lstPermission = ctlAccessView.FindACLControl(sFIELD_NAME, "permission");
									if ( lstPermission != null )
									{
										Guid gPermissionID = Guid.Empty;
										SqlProcs.spACL_FIELDS_Update(ref gPermissionID, gID, sFIELD_NAME, sMODULE_NAME, Sql.ToInteger(lstPermission.SelectedValue), trn);
									}
									//break;
								}
								trn.Commit();
								// 01/18/2010 Paul.  We can only reset the current user. 
								SplendidInit.ClearUserACL();
								SplendidInit.LoadUserACL(Security.USER_ID);
							}
							catch(Exception ex)
							{
								trn.Rollback();
								SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
								ctlDynamicButtons.ErrorText = ex.Message;
								return;
							}
						}
					}
					Response.Redirect("default.aspx?ID=" + gID.ToString());
				}
				else if ( e.CommandName == "Cancel" )
				{
					Response.Redirect("default.aspx?ID=" + gID.ToString());
				}
				else if ( e.CommandName == "CancelFieldSecurity" )
				{
					Response.Redirect("../view.aspx?ID=" + gID.ToString());
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void lstMODULES_Changed(object sender, System.EventArgs e)
		{
			sMODULE_NAME = lstMODULES.SelectedValue;
			if ( Sql.IsEmptyString(sMODULE_NAME) )
				Response.Redirect("default.aspx?ID=" + gID.ToString());
			else
				Response.Redirect("default.aspx?ID=" + gID.ToString() + "&MODULE_NAME=" + HttpUtility.UrlEncode(sMODULE_NAME));
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				sMODULE_NAME = Sql.ToString(Request["MODULE_NAME"]);
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sSQL;
					// 03/19/2010 Paul.  Must include IS_ADMIN field. 
					sSQL = "select MODULE_NAME          " + ControlChars.CrLf
					     + "     , DISPLAY_NAME         " + ControlChars.CrLf
					     + "     , IS_ADMIN             " + ControlChars.CrLf
					     + "  from vwACL_ACCESS_ByModule" + ControlChars.CrLf
					     + " order by MODULE_NAME       " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						try
						{
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									foreach ( DataRow row in dt.Rows )
									{
										row["DISPLAY_NAME"] = L10n.Term(Sql.ToString(row["DISPLAY_NAME"]));
									}
									// 03/09/2010 Paul.  Admin roles are managed separately. 
									vwMain = dt.DefaultView;
									vwMain.RowFilter = "IS_ADMIN = 0 and MODULE_NAME <> 'Teams'";
									lstMODULES.DataSource = vwMain;
									if ( !IsPostBack )
									{
										lstMODULES.DataBind();
										lstMODULES.Items.Insert(0, String.Empty);
									}
								}
							}
						}
						catch(Exception ex)
						{
							SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						}
						try
						{
							if ( !IsPostBack && !Sql.IsEmptyString(sMODULE_NAME) )
							{
								// 08/19/2010 Paul.  Check the list before assigning the value. 
								Utils.SetSelectedValue(lstMODULES, sMODULE_NAME);
							}
							ctlListHeader.Title      = lstMODULES.SelectedItem.Text;
							ctlListHeader.DataBind();
						}
						catch
						{
						}
					}
				}
	
				if ( !IsPostBack )
				{
					if ( !Sql.IsEmptyGuid(gID) )
					{
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *               " + ControlChars.CrLf
							     + "  from vwACL_ROLES_Edit" + ControlChars.CrLf
							     + " where ID = @ID        " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID", gID);
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								// 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
											ctlDynamicButtons.Title = L10n.Term(m_sMODULE + ".LBL_FIELD_SECURITY") + ": " + Sql.ToString(rdr["NAME"]);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
											SetPageTitle(ctlDynamicButtons.Title);

											this.AppendDetailViewFields(m_sMODULE + ".DetailView", tblMain, rdr);
										}
									}
								}
							}
						}
					}
					ctlDynamicButtons.AppendButtons(m_sMODULE + ".FieldSecurity", Guid.Empty, null);
					ctlFooterButtons .AppendButtons(m_sMODULE + ".FieldSecurity", Guid.Empty, null);
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
				}
				
				// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
				// 05/31/2015 Paul.  Don't want to hide the header itself, just the buttons. 
				//tabSearch.Visible = Sql.IsEmptyString(sMODULE_NAME);
				ctlDynamicButtons.ShowButtons = !Sql.IsEmptyString(sMODULE_NAME);
				ctlFooterButtons .Visible = !Sql.IsEmptyString(sMODULE_NAME);
				ctlListHeader    .Visible = !Sql.IsEmptyString(sMODULE_NAME);
				ctlAccessView.EnableACLEditing = !Sql.IsEmptyString(sMODULE_NAME);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "ACLRoles";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendDetailViewFields(m_sMODULE + ".DetailView", tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + ".FieldSecurity", Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + ".FieldSecurity", Guid.Empty, null);
			}
		}
		#endregion
	}
}
