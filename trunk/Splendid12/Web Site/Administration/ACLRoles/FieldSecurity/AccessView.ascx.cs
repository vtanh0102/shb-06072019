/**
 * Copyright (C) 2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.ACLRoles.FieldSecurity
{
	/// <summary>
	///		Summary description for AccessView.
	/// </summary>
	public class AccessView : SplendidControl
	{
		protected Guid          gID            ;
		protected DataView      vwMain         ;
		protected ACLFieldGrid  grdACL         ;
		protected Label         lblError       ;
		protected Guid          gUSER_ID       ;

		public bool EnableACLEditing
		{
			get { return grdACL.EnableACLEditing; }
			set { grdACL.EnableACLEditing = value; }
		}

		public Guid USER_ID
		{
			get { return gUSER_ID; }
			set { gUSER_ID = value; }
		}

		// 04/25/2006 Paul.  FindControl needs to be executed on the DataGridItem.  I'm not sure why.
		public DropDownList FindACLControl(string sMODULE_NAME, string sACCESS_TYPE)
		{
			return grdACL.FindACLControl(sMODULE_NAME, sACCESS_TYPE);
		}

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		public void BindGrid()
		{
			string sMODULE_NAME = Sql.ToString(Request["MODULE_NAME"]);
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL;
				sSQL = "select *                            " + ControlChars.CrLf
				     + "  from vwACL_FIELD_ACCESS_RoleFields" + ControlChars.CrLf
				     + " where ROLE_ID     = @ROLE_ID       " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ROLE_ID"    , gID         );
					if ( !Sql.IsEmptyString(sMODULE_NAME) )
					{
						Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
						cmd.CommandText += "   and MODULE_NAME = @MODULE_NAME   " + ControlChars.CrLf;
						grdACL.Columns[0].Visible = false;
						grdACL.Columns[1].Visible = false;
					}
					else
					{
						cmd.CommandText += "   and ACLACCESS is not null" + ControlChars.CrLf;
						// 01/16/2010 Paul.  Only show the module name when showing fields for all modules. 
						grdACL.Columns[0].Visible = true;
						grdACL.Columns[1].Visible = true;
					}
					cmd.CommandText += " order by FIELD_NAME                " + ControlChars.CrLf;
	
					if ( bDebug )
						RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));
	
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							vwMain = dt.DefaultView;
							grdACL.DataSource = vwMain ;
							// 04/26/2006 Paul.  Normally, we would only bind if not a postback, 
							// but the ACL grid knows how to handle the postback state, so we must always bind. 
							grdACL.DataBind();
						}
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				BindGrid();
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
			// 05/03/2006 Paul.  Remove the page data binding as it clears the binding on UserRolesView.ascx. 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
