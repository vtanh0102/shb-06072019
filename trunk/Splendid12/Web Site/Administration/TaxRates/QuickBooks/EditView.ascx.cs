/**
 * Copyright (C) 2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.TaxRates.QuickBooks
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sQID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Context.Application);
#if DEBUG
						//bQuickBooksEnabled = true;
#endif
						if ( bQuickBooksEnabled )
						{
							// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
							if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
							{
								Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
								Spring.Social.QuickBooks.Api.TaxRate obj = null;
								if ( !Sql.IsEmptyString(sQID) )
								{
									obj = quickbooks.TaxRateOperations.GetById(sQID);
								}
								else
								{
									obj = new Spring.Social.QuickBooks.Api.TaxRate();
								}
								obj.Name             = new DynamicControl(this, "Name"              ).Text;
								obj.Description      = new DynamicControl(this, "Description"       ).Text;
								obj.Active           = new DynamicControl(this, "Active"            ).Checked;
								obj.Rate             = new DynamicControl(this, "RateValue"         ).DecimalValue;
								obj.AgencyRef        = new Spring.Social.QuickBooks.Api.ReferenceType(new DynamicControl(this, "Agency").Text);
								if ( !Sql.IsEmptyString(sQID) )
								{
									quickbooks.TaxRateOperations.Update(obj);
								}
								else
								{
									obj = quickbooks.TaxRateOperations.Insert(obj);
									sQID = obj.Id;
								}
							}
							else
							{
								QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										string sSQL;
										if ( Sql.IsEmptyString(sQID) )
										{
											sSQL = "insert into TaxRates     " + ControlChars.CrLf
											     + "     ( Name              " + ControlChars.CrLf
											     + "     , Description       " + ControlChars.CrLf
											     + "     , Active            " + ControlChars.CrLf
											     + "     , RateValue         " + ControlChars.CrLf
											     + "     , Agency            " + ControlChars.CrLf
											     + "     )" + ControlChars.CrLf
											     + "values" + ControlChars.CrLf
											     + "     ( @Name              " + ControlChars.CrLf
											     + "     , @Description       " + ControlChars.CrLf
											     + "     , @Active            " + ControlChars.CrLf
											     + "     , @RateValue         " + ControlChars.CrLf
											     + "     , @Agency            " + ControlChars.CrLf
											     + "     ) " + ControlChars.CrLf;
										}
										else
										{
											sSQL = "update TaxRates                                " + ControlChars.CrLf
											     + "   set Name               = @Name              " + ControlChars.CrLf
											     + "     , Description        = @Description       " + ControlChars.CrLf
											     + "     , Active             = @Active            " + ControlChars.CrLf
											     + "     , RateValue          = @RateValue         " + ControlChars.CrLf
											     + "     , Agency             = @Agency            " + ControlChars.CrLf
											     + " where ID                 = @ID                " + ControlChars.CrLf;
										}
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@Name"              , new DynamicControl(this, "Name"              ).Text,   41);
										Sql.AddParameter(cmd, "@Description"       , new DynamicControl(this, "Description"       ).Text, 4095);
										Sql.AddParameter(cmd, "@Active"            , new DynamicControl(this, "Active"            ).Checked   );
										Sql.AddParameter(cmd, "@RateValue"         , new DynamicControl(this, "RateValue"         ).FloatValue);
										Sql.AddParameter(cmd, "@Agency"            , new DynamicControl(this, "Agency"            ).Text,   41);
									
										if ( !Sql.IsEmptyString(sQID) )
											Sql.AddParameter(cmd, "@ID", sQID);
										cmd.ExecuteNonQuery();
										if ( !Sql.IsEmptyString(sQID) )
										{
											Hashtable result = dbf.GetLastResult(con);
											sQID = Sql.ToString(result["id"]);
										}
									}
								}
							}
							Response.Redirect("view.aspx?QID=" + sQID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sQID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?QID=" + sQID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.USER_ID == Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]));
			if ( !this.Visible )
				return;

			try
			{
				sQID = Sql.ToString(Request["QID"]);
				if ( !IsPostBack )
				{
					string sDuplicateQID = Sql.ToString(Request["DuplicateQID"]);
					if ( !Sql.IsEmptyString(sQID) || !Sql.IsEmptyString(sDuplicateQID) )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
#if DEBUG
						//bQuickBooksEnabled = true;
#endif
						if ( bQuickBooksEnabled )
						{
							// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
							if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
							{
								Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
								
								DataRow rdr = Spring.Social.QuickBooks.Api.TaxRate.ConvertToRow(quickbooks.TaxRateOperations.GetById(sQID));
								//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
								// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
								ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
								SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
								ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
								
								this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
								ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
								ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
								TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
								if ( txtNAME != null )
									txtNAME.Focus();
								
								//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							}
							else
							{
								QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL ;
									sSQL = "select *        " + ControlChars.CrLf
									     + "  from TaxRates " + ControlChars.CrLf
									     + " where ID = @ID " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										if ( !Sql.IsEmptyString(sDuplicateQID) )
										{
											Sql.AddParameter(cmd, "@ID", sDuplicateQID);
											sQID = String.Empty;
										}
										else
										{
											Sql.AddParameter(cmd, "@ID", sQID);
										}
										using ( DbDataAdapter da = dbf.CreateDataAdapter() )
										{
											((IDbDataAdapter)da).SelectCommand = cmd;
											using ( DataTable dtCurrent = new DataTable() )
											{
												da.Fill(dtCurrent);
												if ( dtCurrent.Rows.Count > 0 )
												{
													DataRow rdr = dtCurrent.Rows[0];
													//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
												
													// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
													ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
													SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
													ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
												
													this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
													ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
													TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
													if ( txtNAME != null )
														txtNAME.Focus();
												
													//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
												}
												else
												{
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
													ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
													ctlDynamicButtons.DisableAll();
													ctlFooterButtons .DisableAll();
													ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
													plcSubPanel.Visible = false;
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "TaxRates";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.IsEmptyString(Request["QID"]);
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			// 02/06/2014 Paul.  Go back to a single QuickBooks UI. 
			// 02/13/2015 Paul.  New QuickBooks Online code uses a different schema. 
			this.LayoutEditView = "EditView.QuickBooks" + (QuickBooksSync.IsOnlineAppMode(Application) ? "Online" : String.Empty);
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

