/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.Pardot
{
	/// <summary>
	///		Summary description for ConfigView.
	/// </summary>
	public class ConfigView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		private const string sEMPTY_PASSWORD = "**********";
		protected CheckBox     ENABLED               ;
		protected CheckBox     VERBOSE_STATUS        ;
		protected DropDownList DIRECTION             ;
		protected DropDownList CONFLICT_RESOLUTION   ;
		protected DropDownList SYNC_MODULES          ;
		protected TextBox      API_USER_KEY          ;
		protected TextBox      API_USERNAME          ;
		protected TextBox      API_PASSWORD          ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "Test" )
			{
				try
				{
					API_USER_KEY.Text = API_USER_KEY.Text.Trim();
					API_USERNAME.Text = API_USERNAME.Text.Trim();
					API_PASSWORD.Text = API_PASSWORD.Text.Trim();
					string sPASSWORD = API_PASSWORD.Text;
					if ( sPASSWORD == sEMPTY_PASSWORD )
					{
						sPASSWORD = Sql.ToString (Application["CONFIG.Pardot.ApiPassword"]);
					}
					else
					{
						Guid gCREDIT_CARD_KEY = Sql.ToGuid(Application["CONFIG.CreditCardKey"]);
						if ( Sql.IsEmptyGuid(gCREDIT_CARD_KEY) )
						{
							gCREDIT_CARD_KEY = Guid.NewGuid();
							SqlProcs.spCONFIG_Update("system", "CreditCardKey", gCREDIT_CARD_KEY.ToString());
							Application["CONFIG.CreditCardKey"] = gCREDIT_CARD_KEY;
						}
						Guid gCREDIT_CARD_IV = Sql.ToGuid(Application["CONFIG.CreditCardIV"]);
						if ( Sql.IsEmptyGuid(gCREDIT_CARD_IV) )
						{
							gCREDIT_CARD_IV = Guid.NewGuid();
							SqlProcs.spCONFIG_Update("system", "CreditCardIV", gCREDIT_CARD_IV.ToString());
							Application["CONFIG.CreditCardIV"] = gCREDIT_CARD_IV;
						}
						string sENCRYPTED_EMAIL_PASSWORD = Security.EncryptPassword(sPASSWORD, gCREDIT_CARD_KEY, gCREDIT_CARD_IV);
						if ( Security.DecryptPassword(sENCRYPTED_EMAIL_PASSWORD, gCREDIT_CARD_KEY, gCREDIT_CARD_IV) != sPASSWORD )
							throw(new Exception("Decryption failed"));
						sPASSWORD = sENCRYPTED_EMAIL_PASSWORD;
					}
					if ( Page.IsValid )
					{
						if ( e.CommandName == "Test" )
						{
							StringBuilder sbErrors = new StringBuilder();
							Spring.Social.Pardot.PardotSync.ValidatePardot(Application, API_USER_KEY.Text, API_USERNAME.Text, sPASSWORD, sbErrors);

							if ( sbErrors.Length > 0 )
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							else
								ctlDynamicButtons.ErrorText = L10n.Term("Pardot.LBL_TEST_SUCCESSFUL");
						}
						else if ( e.CommandName == "Save" )
						{
							Application["CONFIG.Pardot.Enabled"           ] = ENABLED            .Checked;
							Application["CONFIG.Pardot.VerboseStatus"     ] = VERBOSE_STATUS     .Checked;
							Application["CONFIG.Pardot.Direction"         ] = DIRECTION          .SelectedValue;
							Application["CONFIG.Pardot.ConflictResolution"] = CONFLICT_RESOLUTION.SelectedValue;
							Application["CONFIG.Pardot.SyncModules"       ] = SYNC_MODULES       .SelectedValue;
							Application["CONFIG.Pardot.ApiUserKey"        ] = API_USER_KEY       .Text;
							Application["CONFIG.Pardot.ApiUsername"       ] = API_USERNAME       .Text;
							Application["CONFIG.Pardot.ApiPassword"       ] = sPASSWORD;
						
							SqlProcs.spCONFIG_Update("system", "Pardot.Enabled"           , Sql.ToString(Application["CONFIG.Pardot.Enabled"           ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.VerboseStatus"     , Sql.ToString(Application["CONFIG.Pardot.VerboseStatus"     ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.Direction"         , Sql.ToString(Application["CONFIG.Pardot.Direction"         ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.ConflictResolution", Sql.ToString(Application["CONFIG.Pardot.ConflictResolution"]));
							SqlProcs.spCONFIG_Update("system", "Pardot.SyncModules"       , Sql.ToString(Application["CONFIG.Pardot.SyncModules"       ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.ApiUserKey"        , Sql.ToString(Application["CONFIG.Pardot.ApiUserKey"        ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.ApiUsername"       , Sql.ToString(Application["CONFIG.Pardot.ApiUsername"       ]));
							SqlProcs.spCONFIG_Update("system", "Pardot.ApiPassword"       , Sql.ToString(Application["CONFIG.Pardot.ApiPassword"       ]));
#if !DEBUG
							SqlProcs.spSCHEDULERS_UpdateStatus("function::pollPardot", ENABLED.Checked ? "Active" : "Inactive");
#endif
							// 07/15/2017 Paul.  Instead of requiring that the user manually enable the user, do so automatically. 
							if ( Sql.ToBoolean(Application["CONFIG.Pardot.Enabled"]) )
							{
								Guid gUSER_ID = Spring.Social.Pardot.PardotSync.PardotUserID(Application);
								DbProviderFactory dbf = DbProviderFactories.GetFactory();
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL;
									sSQL = "select STATUS       " + ControlChars.CrLf
									     + "  from vwUSERS      " + ControlChars.CrLf
									     + " where ID = @ID     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gUSER_ID);
										string sSTATUS = Sql.ToString(cmd.ExecuteScalar());
										if ( sSTATUS != "Active" )
										{
											SqlProcs.spUSERS_UpdateStatus(gUSER_ID, "Active");
										}
									}
								}
							}
							Response.Redirect("default.aspx");
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("Pardot.LBL_MANAGE_PARDOT_TITLE"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !IsPostBack )
				{
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(String.Empty                      ), String.Empty));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.remote"), "remote"    ));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.local" ), "local"     ));
					DIRECTION.DataSource = SplendidCache.List("pardot_sync_direction");
					DIRECTION.DataBind();
					SYNC_MODULES.DataSource = SplendidCache.List("pardot_sync_module");
					SYNC_MODULES.DataBind();
					
					ENABLED       .Checked = Sql.ToBoolean(Application["CONFIG.Pardot.Enabled"      ]);
					VERBOSE_STATUS.Checked = Sql.ToBoolean(Application["CONFIG.Pardot.VerboseStatus"]);
					API_USER_KEY  .Text    = Sql.ToString (Application["CONFIG.Pardot.ApiUserKey"   ]);
					API_USERNAME  .Text    = Sql.ToString (Application["CONFIG.Pardot.ApiUsername"  ]);
					string sPASSWORD       = Sql.ToString (Application["CONFIG.Pardot.ApiPassword"  ]);
					if ( !Sql.IsEmptyString(sPASSWORD) )
					{
						API_PASSWORD.Attributes.Add("value", sEMPTY_PASSWORD);
					}
					try
					{
						Utils.SetSelectedValue(DIRECTION, Sql.ToString(Application["CONFIG.Pardot.Direction"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(CONFLICT_RESOLUTION, Sql.ToString(Application["CONFIG.Pardot.ConflictResolution"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(SYNC_MODULES, Sql.ToString(Application["CONFIG.Pardot.SyncModules"]));
					}
					catch
					{
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Pardot";
			SetAdminMenu(m_sMODULE);
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
			ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
		}
		#endregion
	}
}
