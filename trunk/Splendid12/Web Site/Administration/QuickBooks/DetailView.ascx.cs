/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.QuickBooks
{
	/// <summary>
	///		Summary description for DetailView.
	/// </summary>
	public class DetailView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons ctlDynamicButtons;
		protected PlaceHolder plcSubPanel      ;

		protected TableRow     trInstructions          ;
		protected CheckBox     ENABLED                 ;
		protected CheckBox     VERBOSE_STATUS          ;
		protected Label        DIRECTION               ;
		protected Label        CONFLICT_RESOLUTION     ;
		protected CheckBox     SYNC_QUOTES             ;
		protected CheckBox     SYNC_ORDERS             ;
		protected CheckBox     SYNC_INVOICES           ;
		protected CheckBox     SYNC_PAYMENTS           ;
		protected CheckBox     SYNC_CREDIT_MEMOS       ;
		protected Label        PAYMENTS_DEPOSIT_ACCOUNT;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Test" )
			{
				try
				{
					StringBuilder sbErrors = new StringBuilder();
					if ( Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Online" )
						Spring.Social.QuickBooks.QuickBooksSync.ValidateQuickBooks(Application, sbErrors);
					else
						QuickBooksSync.ValidateQuickBooks(Application, sbErrors);
					if ( sbErrors.Length > 0 )
						ctlDynamicButtons.ErrorText = sbErrors.ToString();
					else
						ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_TEST_SUCCESSFUL");
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Sync" )
			{
				if ( Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Online" )
				{
#if false
					Spring.Social.QuickBooks.QuickBooksSync.Sync(Context);
#else
					System.Threading.Thread t = new System.Threading.Thread(Spring.Social.QuickBooks.QuickBooksSync.Sync);
					t.Start(Context);
					ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_SYNC_BACKGROUND");
#endif
				}
				else
				{
					System.Threading.Thread t = new System.Threading.Thread(QuickBooksSync.Sync);
					t.Start(Context);
				}
				Response.Redirect("default.aspx");
			}
			else if ( e.CommandName == "SyncAll" )
			{
				if ( Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Online" )
				{
#if false
					Spring.Social.QuickBooks.QuickBooksSync.SyncAll(Context);
#else
					System.Threading.Thread t = new System.Threading.Thread(Spring.Social.QuickBooks.QuickBooksSync.SyncAll);
					t.Start(Context);
					ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_SYNC_BACKGROUND");
#endif
				}
				else
				{
					System.Threading.Thread t = new System.Threading.Thread(QuickBooksSync.SyncAll);
					t.Start(Context);
					ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_SYNC_BACKGROUND");
				}
				Response.Redirect("default.aspx");
			}
			else if ( e.CommandName == "Edit" )
			{
				Response.Redirect("config.aspx");
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("../default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("QuickBooks.LBL_QUICKBOOKS_SETTINGS"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !IsPostBack )
				{
					ENABLED                 .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.Enabled"               ]);
					VERBOSE_STATUS          .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"         ]);
					SYNC_QUOTES             .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncQuotes"            ]);
					SYNC_ORDERS             .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncOrders"            ]);
					SYNC_INVOICES           .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncInvoices"          ]);
					SYNC_PAYMENTS           .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncPayments"          ]);
					SYNC_CREDIT_MEMOS       .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncCreditMemos"       ]);
					PAYMENTS_DEPOSIT_ACCOUNT.Text    = Sql.ToString (Application["CONFIG.QuickBooks.PaymentsDepositAccount"]);
					DIRECTION               .Text    = Sql.ToString (Application["CONFIG.QuickBooks.Direction"             ]);
					CONFLICT_RESOLUTION     .Text    = Sql.ToString (Application["CONFIG.QuickBooks.ConflictResolution"    ]);
					// 01/31/2015 Paul.  Hide RSS instructions when using Online system. 
					string sAPP_MODE = Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]);
					trInstructions.Visible = (sAPP_MODE != "Online");

					bool bShowSynchronized = Sql.ToBoolean(Request["ShowSynchronized"]);
					ctlDynamicButtons.ShowHyperLink("~/Administration/QuickBooks/default.aspx?ShowSynchronized=1", !bShowSynchronized);
					ctlDynamicButtons.ShowHyperLink("~/Administration/QuickBooks/default.aspx"                   ,  bShowSynchronized);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "QuickBooks";
			SetAdminMenu(m_sMODULE);
			this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
			ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
		}
		#endregion
	}
}
