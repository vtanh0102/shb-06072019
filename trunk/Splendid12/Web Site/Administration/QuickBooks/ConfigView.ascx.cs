/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.QuickBooks
{
	/// <summary>
	///		Summary description for ConfigView.
	/// </summary>
	public class ConfigView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected TableRow     trInstructions        ;
		protected TableRow     trRemote1             ;
		protected TableRow     trRemote2             ;
		protected TableRow     trOnline1             ;
		protected TableRow     trOnline2             ;
		protected TableRow     trOnline3             ;
		protected TableRow     trOnline4             ;
		protected RadioButton  radQuickBooksRemote   ;
		protected RadioButton  radQuickBooksOnline   ;
		protected TextBox      REMOTE_USER           ;
		protected TextBox      REMOTE_PASSWORD       ;
		protected TextBox      REMOTE_URL            ;
		protected TextBox      REMOTE_APPLICATION    ;
		protected TextBox      OAUTH_COMPANY_ID      ;
		protected TextBox      OAUTH_COUNTRY_CODE    ;
		protected TextBox      OAUTH_CLIENT_ID       ;
		protected TextBox      OAUTH_CLIENT_SECRET   ;
		protected TextBox      OAUTH_ACCESS_TOKEN    ;
		protected TextBox      OAUTH_ACCESS_SECRET   ;
		protected TextBox      OAUTH_VERIFIER        ;
		// 04/27/2015 Paul.  We need to keep track of the token expiration. 
		protected TextBox      OAUTH_EXPIRES_AT      ;

		//protected Label        CONNECTION_STRING     ;
		protected CheckBox     ENABLED               ;
		protected CheckBox     VERBOSE_STATUS        ;
		protected DropDownList DIRECTION             ;
		protected DropDownList CONFLICT_RESOLUTION   ;
		protected CheckBox     SYNC_QUOTES           ;
		protected CheckBox     SYNC_ORDERS           ;
		protected CheckBox     SYNC_INVOICES         ;
		protected CheckBox     SYNC_PAYMENTS         ;
		protected CheckBox     SYNC_CREDIT_MEMOS     ;
		protected TextBox      PAYMENTS_DEPOSIT_ACCOUNT;

		protected void radQuickBooksRemote_CheckedChanged(object sender, EventArgs e)
		{
			trRemote1.Visible =  radQuickBooksRemote.Checked;
			trRemote2.Visible =  radQuickBooksRemote.Checked;
			trOnline1.Visible = !radQuickBooksRemote.Checked;
			trOnline2.Visible = !radQuickBooksRemote.Checked;
			trOnline3.Visible = !radQuickBooksRemote.Checked;
			trOnline4.Visible = !radQuickBooksRemote.Checked;
			ctlDynamicButtons.ShowButton("Authorize", !radQuickBooksRemote.Checked);
			ctlFooterButtons .ShowButton("Authorize", !radQuickBooksRemote.Checked);
			// 04/27/2015 Paul.  We need to keep track of the token expiration. 
			ctlDynamicButtons.ShowButton("Reconnect", !radQuickBooksRemote.Checked);
			ctlFooterButtons .ShowButton("Reconnect", !radQuickBooksRemote.Checked);
		}

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			// 04/27/2015 Paul.  We need to keep track of the token expiration. 
			if ( e.CommandName == "Reconnect" )
			{
				try
				{
					if ( Page.IsValid && radQuickBooksOnline.Checked )
					{
						StringBuilder sbErrors = new StringBuilder();
						// 04/27/2015 Paul.  QuickBooks playground that allows testing of OAuth tokens. 
						// https://appcenter.intuit.com/Playground/OAuth/IA/
						Spring.Social.QuickBooks.QuickBooksSync.ReconnectAccessToken(Application, sbErrors);

						if ( sbErrors.Length > 0 )
							ctlDynamicButtons.ErrorText = sbErrors.ToString();
						else
						{
							OAUTH_ACCESS_TOKEN .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthAccessToken"      ]);
							OAUTH_ACCESS_SECRET.Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]);
							OAUTH_VERIFIER     .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthVerifier"         ]);
							OAUTH_EXPIRES_AT   .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthExpiresAt"        ]);
							ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_TEST_SUCCESSFUL");
						}
					}
				}
				catch (Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "Authorize" )
			{
				try
				{
					if ( Page.IsValid && radQuickBooksOnline.Checked )
					{
						StringBuilder sbErrors = new StringBuilder();
						string sAUTH_TOKEN   = String.Empty;
						string sAUTH_KEY     = String.Empty;
						string sAUTH_URL     = String.Empty;
						// https://developer.intuit.com/docs/0150_payments/0060_authentication_and_authorization/connect_from_within_your_app
						string sCALLBACK_URL = Request.Url.Scheme + "://" + Request.Url.Host + Sql.ToString(Application["rootURL"]) + "Administration/QuickBooks/config.aspx";
						//QuickBooksSync.GetOAuthAuthorizationURL(Application, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, sCALLBACK_URL, ref sAUTH_TOKEN, ref sAUTH_KEY, ref sAUTH_URL, sbErrors);
						Spring.Social.QuickBooks.QuickBooksSync.GetOAuthAuthorizationURL(Application, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, sCALLBACK_URL, ref sAUTH_TOKEN, ref sAUTH_KEY, ref sAUTH_URL, sbErrors);
						
						if ( sbErrors.Length > 0 )
						{
							ctlDynamicButtons.ErrorText = sbErrors.ToString();
						}
						else
						{
							OAUTH_VERIFIER     .Text = String.Empty;
							OAUTH_ACCESS_TOKEN .Text = String.Empty;
							OAUTH_ACCESS_SECRET.Text = String.Empty;
							OAUTH_EXPIRES_AT   .Text = String.Empty;
							Session["QuickBooks.OAUTH_COMPANY_ID"   ] = OAUTH_COMPANY_ID   .Text;
							Session["QuickBooks.OAUTH_CLIENT_ID"    ] = OAUTH_CLIENT_ID    .Text;
							Session["QuickBooks.OAUTH_CLIENT_SECRET"] = OAUTH_CLIENT_SECRET.Text;
							Session["QuickBooks.OAUTH_AUTH_TOKEN"   ] = sAUTH_TOKEN             ;
							Session["QuickBooks.OAUTH_AUTH_KEY"     ] = sAUTH_KEY               ;
							Response.Redirect(sAUTH_URL);
						}
					}
				}
				catch (Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "Save" || e.CommandName == "Test" )
			{
				try
				{
					if ( Page.IsValid )
					{
						if ( e.CommandName == "Test" )
						{
							StringBuilder sbErrors = new StringBuilder();
							if ( radQuickBooksOnline.Checked )
								Spring.Social.QuickBooks.QuickBooksSync.ValidateQuickBooks(Application, OAUTH_COMPANY_ID.Text, OAUTH_COUNTRY_CODE.Text, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, OAUTH_ACCESS_TOKEN.Text, OAUTH_ACCESS_SECRET.Text, sbErrors);
							else
								QuickBooksSync.ValidateQuickBooks(Application, REMOTE_USER.Text, REMOTE_PASSWORD.Text, REMOTE_URL.Text, REMOTE_APPLICATION.Text, sbErrors);

							if ( sbErrors.Length > 0 )
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							else
								ctlDynamicButtons.ErrorText = L10n.Term("QuickBooks.LBL_TEST_SUCCESSFUL");
						}
						else if ( e.CommandName == "Save" )
						{
							Application["CONFIG.QuickBooks.AppMode"               ] = (radQuickBooksOnline.Checked ? "Online" : "Remote");
							Application["CONFIG.QuickBooks.Enabled"               ] = ENABLED       .Checked;
							Application["CONFIG.QuickBooks.VerboseStatus"         ] = VERBOSE_STATUS.Checked;
							Application["CONFIG.QuickBooks.Direction"             ] = DIRECTION          .SelectedValue;
							Application["CONFIG.QuickBooks.ConflictResolution"    ] = CONFLICT_RESOLUTION.SelectedValue;
							Application["CONFIG.QuickBooks.SyncQuotes"            ] = SYNC_QUOTES      .Checked;
							Application["CONFIG.QuickBooks.SyncOrders"            ] = SYNC_ORDERS      .Checked;
							Application["CONFIG.QuickBooks.SyncInvoices"          ] = SYNC_INVOICES    .Checked;
							Application["CONFIG.QuickBooks.SyncPayments"          ] = SYNC_PAYMENTS    .Checked;
							Application["CONFIG.QuickBooks.SyncCreditMemos"       ] = SYNC_CREDIT_MEMOS.Checked;
							Application["CONFIG.QuickBooks.PaymentsDepositAccount"] = PAYMENTS_DEPOSIT_ACCOUNT.Text;
							
							Application["CONFIG.QuickBooks.RemoteUser"            ] = REMOTE_USER        .Text.Trim();
							Application["CONFIG.QuickBooks.RemotePassword"        ] = REMOTE_PASSWORD    .Text.Trim();
							Application["CONFIG.QuickBooks.RemoteURL"             ] = REMOTE_URL         .Text.Trim();
							Application["CONFIG.QuickBooks.RemoteApplicationName" ] = REMOTE_APPLICATION .Text.Trim();
							
							Application["CONFIG.QuickBooks.OAuthCompanyID"        ] = OAUTH_COMPANY_ID   .Text.Trim();
							Application["CONFIG.QuickBooks.OAuthCountryCode"      ] = OAUTH_COUNTRY_CODE .Text.Trim();
							Application["CONFIG.QuickBooks.OAuthClientID"         ] = OAUTH_CLIENT_ID    .Text.Trim();
							Application["CONFIG.QuickBooks.OAuthClientSecret"     ] = OAUTH_CLIENT_SECRET.Text.Trim();
							Application["CONFIG.QuickBooks.OAuthAccessToken"      ] = OAUTH_ACCESS_TOKEN .Text.Trim();
							Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"] = OAUTH_ACCESS_SECRET.Text.Trim();
							Application["CONFIG.QuickBooks.OAuthVerifier"         ] = OAUTH_VERIFIER     .Text.Trim();
							// 04/27/2015 Paul.  We need to keep track of the token expiration. 
							Application["CONFIG.QuickBooks.OAuthExpiresAt"        ] = OAUTH_EXPIRES_AT   .Text.Trim();
							
							SqlProcs.spCONFIG_Update("system", "QuickBooks.AppMode"               , Sql.ToString(Application["CONFIG.QuickBooks.AppMode"               ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.Enabled"               , Sql.ToString(Application["CONFIG.QuickBooks.Enabled"               ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.VerboseStatus"         , Sql.ToString(Application["CONFIG.QuickBooks.VerboseStatus"         ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.Direction"             , Sql.ToString(Application["CONFIG.QuickBooks.Direction"             ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.ConflictResolution"    , Sql.ToString(Application["CONFIG.QuickBooks.ConflictResolution"    ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.SyncQuotes"            , Sql.ToString(Application["CONFIG.QuickBooks.SyncQuotes"            ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.SyncOrders"            , Sql.ToString(Application["CONFIG.QuickBooks.SyncOrders"            ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.SyncInvoices"          , Sql.ToString(Application["CONFIG.QuickBooks.SyncInvoices"          ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.SyncPayments"          , Sql.ToString(Application["CONFIG.QuickBooks.SyncPayments"          ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.SyncCreditMemos"       , Sql.ToString(Application["CONFIG.QuickBooks.SyncCreditMemos"       ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.PaymentsDepositAccount", Sql.ToString(Application["CONFIG.QuickBooks.PaymentsDepositAccount"]));
							
							SqlProcs.spCONFIG_Update("system", "QuickBooks.RemoteUser"            , Sql.ToString(Application["CONFIG.QuickBooks.RemoteUser"            ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.RemotePassword"        , Sql.ToString(Application["CONFIG.QuickBooks.RemotePassword"        ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.RemoteURL"             , Sql.ToString(Application["CONFIG.QuickBooks.RemoteURL"             ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.RemoteApplicationName" , Sql.ToString(Application["CONFIG.QuickBooks.RemoteApplicationName" ]));
							
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthCompanyID"        , Sql.ToString(Application["CONFIG.QuickBooks.OAuthCompanyID"        ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthCountryCode"      , Sql.ToString(Application["CONFIG.QuickBooks.OAuthCountryCode"      ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthClientID"         , Sql.ToString(Application["CONFIG.QuickBooks.OAuthClientID"         ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthClientSecret"     , Sql.ToString(Application["CONFIG.QuickBooks.OAuthClientSecret"     ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthAccessToken"      , Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessToken"      ]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthAccessTokenSecret", Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]));
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthVerifier"         , Sql.ToString(Application["CONFIG.QuickBooks.OAuthVerifier"         ]));
							// 04/27/2015 Paul.  We need to keep track of the token expiration. 
							SqlProcs.spCONFIG_Update("system", "QuickBooks.OAuthExpiresAt"        , Sql.ToString(Application["CONFIG.QuickBooks.OAuthExpiresAt"        ]));
							// 11/06/2015 Paul.  Update layout views based on QuickBooks enabled flag. 
							SqlProcs.spCONFIG_QuickBooksChanged();
							
							string sPaymentsDepositAccount = PAYMENTS_DEPOSIT_ACCOUNT.Text;
							string sPaymentsDepositAccountId = Sql.ToString(Application["QuickBooks.PaymentsDepositAccount." + sPaymentsDepositAccount]);
							if ( !Sql.IsEmptyString(sPaymentsDepositAccount) && Sql.IsEmptyString(sPaymentsDepositAccountId) )
							{
								try
								{
									if ( radQuickBooksOnline.Checked && !Sql.IsEmptyString(Application["CONFIG.QuickBooks.OAuthAccessToken"]) && !Sql.IsEmptyString(Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]) )
									{
										Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Application);
										Spring.Social.QuickBooks.Api.Account obj = quickbooks.AccountOperations.GetByName(sPaymentsDepositAccount);
										if ( obj != null )
											Application["QuickBooks.PaymentsDepositAccount." + sPaymentsDepositAccount] = obj.Id;
									}
								}
								catch(Exception ex)
								{
									throw(new Exception("Cound not find Deposit Account " + sPaymentsDepositAccount + ": " + ex.Message));
								}
							}
							
#if !DEBUG
							if ( radQuickBooksOnline.Checked )
								SqlProcs.spSCHEDULERS_UpdateStatus("function::ollQuickBooksOnline", ENABLED.Checked ? "Active" : "Inactive");
							else
								SqlProcs.spSCHEDULERS_UpdateStatus("function::pollQuickBooks", ENABLED.Checked ? "Active" : "Inactive");
#endif
							// 07/15/2017 Paul.  Instead of requiring that the user manually enable the user, do so automatically. 
							if ( Sql.ToBoolean(Application["CONFIG.QuickBooks.Enabled"]) )
							{
								Guid gUSER_ID = Spring.Social.QuickBooks.QuickBooksSync.QuickBooksUserID(Application);
								DbProviderFactory dbf = DbProviderFactories.GetFactory();
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL;
									sSQL = "select STATUS       " + ControlChars.CrLf
									     + "  from vwUSERS      " + ControlChars.CrLf
									     + " where ID = @ID     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gUSER_ID);
										string sSTATUS = Sql.ToString(cmd.ExecuteScalar());
										if ( sSTATUS != "Active" )
										{
											SqlProcs.spUSERS_UpdateStatus(gUSER_ID, "Active");
										}
									}
								}
							}
							Response.Redirect("default.aspx");
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("QuickBooks.LBL_QUICKBOOKS_SETTINGS"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !IsPostBack )
				{
					DIRECTION.DataSource = SplendidCache.List("quickbooks_sync_direction");
					DIRECTION.DataBind();
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(String.Empty                      ), String.Empty));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.remote"), "remote"    ));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.local" ), "local"     ));
					
					ENABLED                 .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.Enabled"               ]);
					VERBOSE_STATUS          .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"         ]);
					SYNC_QUOTES             .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncQuotes"            ]);
					SYNC_ORDERS             .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncOrders"            ]);
					SYNC_INVOICES           .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncInvoices"          ]);
					SYNC_PAYMENTS           .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncPayments"          ]);
					SYNC_CREDIT_MEMOS       .Checked = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncCreditMemos"       ]);
					PAYMENTS_DEPOSIT_ACCOUNT.Text    = Sql.ToString (Application["CONFIG.QuickBooks.PaymentsDepositAccount"]);
					OAUTH_COUNTRY_CODE      .Text    = Sql.ToString (Application["CONFIG.QuickBooks.OAuthCountryCode"      ]);
					try
					{
						Utils.SetSelectedValue(DIRECTION, Sql.ToString(Application["CONFIG.QuickBooks.Direction"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(CONFLICT_RESOLUTION, Sql.ToString(Application["CONFIG.QuickBooks.ConflictResolution"]));
					}
					catch
					{
					}
					
					// 06/03/2014 Paul. QuickBooks OAuth will redirect to self for token processing. 
					// http://localhost/SplendidCRM6_Training/Administration/QuickBooks/config.aspx?oauth_token=qyprdoUw6tPcH8lNGmYhrHUi5BNrPnzt8bpX6uWz5tLq3553&oauth_verifier=px6iz5w
					string sAUTH_VERIFIER = Sql.ToString(Request.QueryString["oauth_verifier"]);
					if ( Sql.IsEmptyString(sAUTH_VERIFIER) )
					{
						string sAPP_MODE = Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]);
						radQuickBooksOnline.Checked = (sAPP_MODE == "Online");
						radQuickBooksRemote.Checked = !radQuickBooksOnline.Checked;
						radQuickBooksRemote_CheckedChanged(null, null);
						
						REMOTE_USER        .Text = Sql.ToString (Application["CONFIG.QuickBooks.RemoteUser"            ]);
						REMOTE_PASSWORD    .Text = Sql.ToString (Application["CONFIG.QuickBooks.RemotePassword"        ]);
						REMOTE_URL         .Text = Sql.ToString (Application["CONFIG.QuickBooks.RemoteURL"             ]);
						REMOTE_APPLICATION .Text = Sql.ToString (Application["CONFIG.QuickBooks.RemoteApplicationName" ]);
						
						OAUTH_COMPANY_ID   .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthCompanyID"        ]);
						OAUTH_COUNTRY_CODE .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthCountryCode"      ]);
						OAUTH_CLIENT_ID    .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthClientID"         ]);
						OAUTH_CLIENT_SECRET.Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthClientSecret"     ]);
						OAUTH_ACCESS_TOKEN .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthAccessToken"      ]);
						OAUTH_ACCESS_SECRET.Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]);
						OAUTH_VERIFIER     .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthVerifier"         ]);
						// 04/27/2015 Paul.  We need to keep track of the token expiration. 
						OAUTH_EXPIRES_AT   .Text = Sql.ToString (Application["CONFIG.QuickBooks.OAuthExpiresAt"        ]);
					}
					else
					{
						radQuickBooksOnline.Checked = true;
						radQuickBooksRemote.Checked = !radQuickBooksOnline.Checked;
						radQuickBooksRemote_CheckedChanged(null, null);
						
						OAUTH_COMPANY_ID   .Text = Sql.ToString (Session["QuickBooks.OAUTH_COMPANY_ID"   ]);
						OAUTH_CLIENT_ID    .Text = Sql.ToString (Session["QuickBooks.OAUTH_CLIENT_ID"    ]);
						OAUTH_CLIENT_SECRET.Text = Sql.ToString (Session["QuickBooks.OAUTH_CLIENT_SECRET"]);
						
						string sAUTH_TOKEN          = Sql.ToString(Session["QuickBooks.OAUTH_AUTH_TOKEN"]);
						string sAUTH_KEY            = Sql.ToString(Session["QuickBooks.OAUTH_AUTH_KEY"  ]);
						string sOAUTH_ACCESS_TOKEN  = String.Empty;
						string sOAUTH_ACCESS_SECRET = String.Empty;
						
						// 01/31/2015 Paul.  Validate the oauth_token parameter. 
						if ( sAUTH_TOKEN == Sql.ToString(Request.QueryString["oauth_token"]) )
						{
							StringBuilder sbErrors = new StringBuilder();
							if ( radQuickBooksOnline.Checked )
								Spring.Social.QuickBooks.QuickBooksSync.GetOAuthAccessToken(Application, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, sAUTH_TOKEN, sAUTH_KEY, sAUTH_VERIFIER, ref sOAUTH_ACCESS_TOKEN, ref sOAUTH_ACCESS_SECRET, sbErrors);
							else
								QuickBooksSync.GetOAuthAccessToken(Application, OAUTH_CLIENT_ID.Text, OAUTH_CLIENT_SECRET.Text, sAUTH_TOKEN, sAUTH_KEY, sAUTH_VERIFIER, ref sOAUTH_ACCESS_TOKEN, ref sOAUTH_ACCESS_SECRET, sbErrors);
							OAUTH_VERIFIER     .Text = sAUTH_VERIFIER      ;
							OAUTH_ACCESS_TOKEN .Text = sOAUTH_ACCESS_TOKEN ;
							OAUTH_ACCESS_SECRET.Text = sOAUTH_ACCESS_SECRET;
							if ( sbErrors.Length > 0 )
							{
								ctlDynamicButtons.ErrorText = sbErrors.ToString();
							}
							else
							{
								// 04/27/2015 Paul.  We need to keep track of the token expiration. Token expires after 180 days. 
								// https://developer.intuit.com/docs/0100_accounting/0060_authentication_and_authorization/connect_from_within_your_app#/manage
								DateTime dtOAuthExpiresAt = DateTime.Now.AddDays(180);
								OAUTH_EXPIRES_AT.Text = dtOAuthExpiresAt.ToShortDateString() + " " + dtOAuthExpiresAt.ToShortTimeString();
							}
						}
						else
						{
							ctlDynamicButtons.ErrorText = "Invalid oauth_token, please Authorize again.";
						}
					}
					// 01/31/2015 Paul.  Hide RSS instructions when using Online system. 
					trInstructions.Visible = radQuickBooksRemote.Checked;
					Session["QuickBooks.OAUTH_COMPANY_ID"   ] = String.Empty;
					Session["QuickBooks.OAUTH_CLIENT_ID"    ] = String.Empty;
					Session["QuickBooks.OAUTH_CLIENT_SECRET"] = String.Empty;
					Session["QuickBooks.OAUTH_AUTH_TOKEN"   ] = String.Empty;
					Session["QuickBooks.OAUTH_AUTH_KEY"     ] = String.Empty;
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "QuickBooks";
			SetAdminMenu(m_sMODULE);
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
			ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
		}
		#endregion
	}
}
