/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.QuickBooks
{
	/// <summary>
	///		Summary description for Orders.
	/// </summary>
	public class Orders : SubPanelControl
	{
		// 06/03/2015 Paul.  Combine ListHeader and DynamicButtons. 
		protected _controls.SubPanelButtons ctlDynamicButtons;
		protected UniqueStringCollection arrSelectFields;
		protected DataView        vwMain         ;
		protected SplendidGrid    grdMain        ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncOrders"]);
			try
			{
				// 07/27/2017 Paul.  Check enabled first. 
				bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
				if ( bQuickBooksEnabled )
				{
					bool bShowSynchronized = Sql.ToBoolean(Request["ShowSynchronized"]);
					if ( !IsPostBack )
					{
						// 06/03/2015 Paul.  Combine ListHeader and DynamicButtons. 
						ctlDynamicButtons.Title = (bShowSynchronized ? "QuickBooks.LBL_SYNCD_ORDERS" : "QuickBooks.LBL_NOT_SYNCD_ORDERS");
						ctlDynamicButtons.DataBind();
						ctlDynamicButtons.ShowButton("Sync"   , Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Online");
						ctlDynamicButtons.ShowButton("SyncAll", Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Online");
					}
					if ( Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]) == "Remote" && Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncOrders"]) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								string sTABLE_NAME = Crm.Modules.TableName(m_sMODULE);
								sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
								     + "  from " + Sql.MetadataName(cmd, "vw" + sTABLE_NAME + "_" + (QuickBooksSync.IsOnlineAppMode(Application) ? "QBOnline" : "QuickBooks")) + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								//Security.Filter(cmd, m_sMODULE, "list");
								Guid gQUICKBOOKS_USER_ID = QuickBooksSync.QuickBooksUserID(Context.Application);
								ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, gQUICKBOOKS_USER_ID);
								ExchangeSecurity.Filter(Application, Session, cmd, gQUICKBOOKS_USER_ID, m_sMODULE, "view");
								// 01/28/2016 Paul.  Must include the SYNC_SERVICE_NAME. 
								cmd.CommandText += "   and ID " + (bShowSynchronized ? String.Empty : "not ") + "in (select ID from vw" + sTABLE_NAME + "_SYNC where SYNC_SERVICE_NAME = @SYNC_SERVICE_NAME and ID is not null)" + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@SYNC_SERVICE_NAME", (QuickBooksSync.IsOnlineAppMode(Application) ? "QuickBooksOnline" : "QuickBooks"));
								cmd.CommandText += grdMain.OrderByClause("ORDER_NUM", "asc");

								if ( bDebug )
									RegisterClientScriptBlock("vw" + sTABLE_NAME, Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dt = new DataTable() )
									{
										da.Fill(dt);
										vwMain = dt.DefaultView;
										grdMain.DataSource = vwMain ;
										grdMain.DataBind();
									}
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Orders";
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("ID"       );
			arrSelectFields.Add("NAME"     );
			arrSelectFields.Add("ORDER_NUM");
			this.AppendGridColumns(grdMain, "QuickBooks." + m_sMODULE, arrSelectFields);
			ctlDynamicButtons.AppendButtons("QuickBooks." + m_sMODULE, Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}

