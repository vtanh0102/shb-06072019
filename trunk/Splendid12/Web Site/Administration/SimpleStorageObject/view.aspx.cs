/**
 * Copyright (C) 2008-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Diagnostics;

namespace SplendidCRM.Administration.SimpleStorageObject
{
	/// <summary>
	/// Summary description for View.
	/// </summary>
	public class View : SplendidPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess("SimpleStorage", "view") >= 0);
			if ( !this.Visible )
				return;
			
			// 02/28/2012 Paul.  Don't query if there are no parameters. 
			string sBUCKET = Sql.ToString(Request["BUCKET"]);
			string sKEY    = Sql.ToString(Request["KEY"   ]);
			if ( !Sql.IsEmptyString(AmazonCache.AmazonAccessKeyID()) && !Sql.IsEmptyString(sBUCKET) )
			{
				string sContentType = String.Empty;

				AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
				Stream stm = s3.GetObject(sBUCKET, sKEY, ref sContentType);

				Response.ContentType = sContentType;
				string sFileName = Path.GetFileName(sKEY);
				// 08/06/2008 yxy21969.  Make sure to encode all URLs.
				// 12/20/2009 Paul.  Use our own encoding so that a space does not get converted to a +. 
				Response.AddHeader("Content-Disposition", "attachment;filename=" + Utils.ContentDispositionEncode(Request.Browser, sFileName));
				if ( stm.Length > 0 )
				{
					byte[] byData = new byte[64*1024];
					using ( BinaryWriter writer = new BinaryWriter(Response.OutputStream) )
					{
						int nOffset = 0;
						while ( nOffset < stm.Length )
						{
							int nReadSize = Math.Min(Convert.ToInt32(stm.Length) - nOffset, 64*1024);
							stm.Read(byData, 0, nReadSize);
							writer.Write(byData, 0, nReadSize);
							nOffset += nReadSize;
						}
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
