<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DetailView.ascx.cs" Inherits="SplendidCRM.Administration.SimpleStorage.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<script type="text/javascript">
function AddFile()
{
	for(var i=0;i<10;i++)
	{
		var elem = document.getElementById('file'+i);
		if(elem.style.display == 'none')
		{
			elem.style.display='block';
			break;
		}
	}
	return false;
}
function DeleteFile(index)
{
	var elem = document.getElementById('file'+index);
	elem.style.display='none';
}
</script>

<div id="divDetailView" runat="server">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="SimpleStorage" EnablePrint="true" HelpName="DetailView" EnableHelp="true" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="SearchBasic" Src="SearchBasic.ascx" %>
	<SplendidCRM:SearchBasic ID="ctlSearch" Runat="Server" />
	<br />

<div id="divCreateKeys">
	<asp:Table Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' Width="100%" CellPadding="0" CellSpacing="0" CssClass="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<asp:Table Width="100%" CellPadding="0" CellSpacing="0" runat="server">
					<asp:TableRow>
						<asp:TableCell Width="50%" CssClass="dataLabel" Wrap="false" VerticalAlign="Top">
							<asp:TextBox ID="FOLDER_NAME" CssClass="dataField" size="40" Runat="server" />
							&nbsp;
							<asp:Button ID="btnCreateFolder" CommandName="Key.CreateFolder" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("SimpleStorage.LBL_CREATE_FOLDER_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("SimpleStorage.LBL_CREATE_FOLDER_BUTTON_TITLE") %>' Runat="server" />
							&nbsp;
							<asp:Label ID="lblFolderNameRequired" ForeColor="Red" EnableViewState="false" Visible="false" Runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="50%" BorderWidth="0" CssClass="dataLabel" Wrap="false" VerticalAlign="Top">
							<div id="uploads_div">
								<div style="display: inline" id="file0"><input id="attachment0" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('0');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>" disabled="true" />&nbsp;
									<asp:Button CssClass="button" UseSubmitBehavior="false" OnClientClick="AddFile(); return false;" Text='<%# L10n.Term("SimpleStorage.LBL_ADD_FILE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("SimpleStorage.LBL_ADD_FILE_BUTTON_LABEL") %>' runat="server" /></div>
								<div style="display: none"   id="file1"><input id="attachment1" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('1');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file2"><input id="attachment2" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('2');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file3"><input id="attachment3" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('3');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file4"><input id="attachment4" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('4');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file5"><input id="attachment5" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('5');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file6"><input id="attachment6" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('6');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file7"><input id="attachment7" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('7');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file8"><input id="attachment8" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('8');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
								<div style="display: none"   id="file9"><input id="attachment9" type="file" tabindex="0" size="40" runat="server" />&nbsp;<input type="button" onclick="DeleteFile('9');" class="button" value="<%= L10n.Term(".LBL_REMOVE") %>"/></div>
							</div>
						</asp:TableCell>
						<asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
							&nbsp;
							<asp:Button ID="btnUpload" CommandName="Key.UploadFiles" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("SimpleStorage.LBL_UPLOAD_FILES_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("SimpleStorage.LBL_UPLOAD_FILES_BUTTON_TITLE") %>' Runat="server" />
						</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<br />
</div>


	<asp:PlaceHolder ID="plcBreadcrumbs" runat="server" /><br />
	<asp:Image SkinID="spacer" Height="4" runat="server" /><br />
	<%@ Register TagPrefix="SplendidCRM" Tagname="ExportHeader" Src="~/_controls/ExportHeader.ascx" %>
	<SplendidCRM:ExportHeader ID="ctlExportHeader" Module="SimpleStorage" Title="SimpleStorage.LBL_LIST_FORM_TITLE" Runat="Server" />

	<SplendidCRM:SplendidGrid id="grdMain" Width="100%" CssClass="listView"
		CellPadding="3" CellSpacing="0" border="0"
		AllowPaging="<%# !PrintView %>" PageSize='<%# Math.Max(Sql.ToInteger(HttpContext.Current.Application["CONFIG.list_max_entries_per_page"]), 5) %>' AllowSorting="true" 
		AutoGenerateColumns="false" 
		EnableViewState="true" runat="server">
		<ItemStyle            CssClass="oddListRowS1"  />
		<AlternatingItemStyle CssClass="evenListRowS1" />
		<HeaderStyle          CssClass="listViewThS1"  />
		<PagerStyle HorizontalAlign="Right" Mode="NextPrev" PageButtonCount="6" Position="Top" CssClass="listViewPaginationTdS1" PrevPageText=".LNK_LIST_PREVIOUS" NextPageText=".LNK_LIST_NEXT" />
		<Columns>
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%">
				<ItemTemplate>
					<input name="chkMain" class="checkbox" type="checkbox" value="<%# Eval("KEY") %>" style="display: <%# Sql.ToInteger(Eval("TYPE")) == 1 ? "inline" : "none" %>" />
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:BoundColumn    HeaderText="SimpleStorage.LBL_LIST_FOLDER" DataField="FOLDER" SortExpression="FOLDER" ItemStyle-Width="10%" Visible="false" />
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%">
				<ItemTemplate>
					<asp:HyperLink Enabled='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' NavigateUrl='<%# "view.aspx?BUCKET=" + sBUCKET + "&PREFIX=" + Eval("KEY"   ) %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) == -1 %>' runat="server"><asp:Image ImageUrl='<%# Session["themeURL"] + "images/folder.gif"                         %>' Width="16" Height="16" runat="server" /></asp:HyperLink>
					<asp:HyperLink                                                                               NavigateUrl='<%# "view.aspx?BUCKET=" + sBUCKET + "&PREFIX=" + Eval("FOLDER") %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  0 %>' runat="server"><asp:Image ImageUrl='<%# Session["themeURL"] + "images/folder.gif"                         %>' Width="16" Height="16" runat="server" /></asp:HyperLink>
					<asp:HyperLink Enabled='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' NavigateUrl='<%# s3.GetObjectUri(sBUCKET, Sql.ToString(Eval("KEY"))) %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  1 %>' runat="server"><asp:Image ImageUrl='<%# Session["themeURL"] + "images/mime-" + Eval("MIME_TYPE") + ".gif" %>' Width="16" Height="16" runat="server" /></asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="SimpleStorage.LBL_LIST_NAME" ItemStyle-Width="60%">
				<ItemTemplate>
					<asp:HyperLink Enabled='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' Text='. .'                   NavigateUrl='<%# "view.aspx?BUCKET=" + sBUCKET + "&PREFIX=" + Eval("KEY"   ) %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) == -1 %>' CssClass="listViewTdLinkS1" runat="server" />
					<asp:HyperLink                                                                               Text='<%# Eval("FOLDER") %>' NavigateUrl='<%# "view.aspx?BUCKET=" + sBUCKET + "&PREFIX=" + Eval("FOLDER") %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  0 %>' CssClass="listViewTdLinkS1" runat="server" />
					<asp:HyperLink Enabled='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' Text='<%# Eval("NAME"  ) %>' NavigateUrl='<%# s3.GetObjectUri(sBUCKET, Sql.ToString(Eval("KEY")))         %>' Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  1 %>' CssClass="listViewTdLinkS1"  Target="_default" runat="server" />
					<asp:HyperLink Enabled='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' ID="imgDownload" Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  1 %>' CssClass="listViewTdToolsS1" ImageUrl='<%# Session["themeURL"] + "images/view_inline.gif" %>' runat="server" />
					<asp:Panel ID="pnlDownloadHover" style="display:none; border: solid 1px black; background-color: yellow; color: Black;" runat="server">
						<asp:Label Visible='<%# Sql.ToInteger(Eval("TYPE")) ==  1 %>' Text='<%# s3.GetObjectUri(sBUCKET, Sql.ToString(Eval("KEY")), DateTime.Now.AddHours(24)) %>' runat="server" />
					</asp:Panel>
					<ajaxToolkit:HoverMenuExtender ID="hovDownload" TargetControlID="imgDownload" PopupControlID="pnlDownloadHover" PopupPosition="Bottom" PopDelay="50" runat="server" />
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:BoundColumn    HeaderText="SimpleStorage.LBL_LIST_SIZE"          DataField="SIZE"          SortExpression="SIZE"          ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
			<asp:BoundColumn    HeaderText="SimpleStorage.LBL_LIST_LAST_MODIFIED" DataField="LAST_MODIFIED" SortExpression="LAST_MODIFIED" ItemStyle-Width="10%" ItemStyle-Wrap="false" />
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
				<ItemTemplate>
					<div style="display: <%# Sql.ToInteger(Eval("TYPE")) == 1 ? "inline" : "none" %>">
						<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>' AlternateText='<%# L10n.Term(".LNK_VIEW") %>' NavigateUrl='<%# "../SimpleStorageObject/view.aspx?BUCKET=" + sBUCKET + "&KEY=" + Eval("KEY") %>' CssClass="listViewTdToolsS1" ImageUrl='<%# Session["themeURL"] + "images/view_inline.gif" %>' BorderWidth="0" Width="12" Height="12" ImageAlign="AbsMiddle" Runat="server" />
						<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "view") >= 0 %>'          Text='<%# L10n.Term(".LNK_VIEW") %>' NavigateUrl='<%# "../SimpleStorageObject/view.aspx?BUCKET=" + sBUCKET + "&KEY=" + Eval("KEY") %>' CssClass="listViewTdToolsS1" Runat="server" />
						&nbsp;
						<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' AlternateText='<%# L10n.Term(".LNK_EDIT") %>' NavigateUrl='<%# "../SimpleStorageObject/edit.aspx?BUCKET=" + sBUCKET + "&KEY=" + Eval("KEY") %>' CssClass="listViewTdToolsS1" ImageUrl='<%# Session["themeURL"] + "images/edit_inline.gif" %>' BorderWidth="0" Width="12" Height="12" ImageAlign="AbsMiddle" Runat="server" />
						<asp:HyperLink Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>'          Text='<%# L10n.Term(".LNK_EDIT") %>' NavigateUrl='<%# "../SimpleStorageObject/edit.aspx?BUCKET=" + sBUCKET + "&KEY=" + Eval("KEY") %>' CssClass="listViewTdToolsS1" Runat="server" />
						&nbsp;
						<span onclick="return confirm('<%= L10n.TermJavaScript(".NTC_DELETE_CONFIRMATION") %>')">
							<asp:ImageButton Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Key.Delete" CommandArgument='<%# Eval("KEY") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' ImageUrl='<%# Session["themeURL"] + "images/delete_inline.gif" %>' BorderWidth="0" Width="12" Height="12" ImageAlign="AbsMiddle" Runat="server" />
							<asp:LinkButton  Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Key.Delete" CommandArgument='<%# Eval("KEY") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_DELETE") %>' Runat="server" />
						</span>
					</div>
					<div style="display: <%# Sql.ToInteger(Eval("TYPE")) == 0 ? "inline" : "none" %>">
						<span onclick="return confirm('<%= L10n.TermJavaScript(".NTC_DELETE_CONFIRMATION") %>')">
							<asp:ImageButton Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Key.DeleteFolder" CommandArgument='<%# Eval("KEY") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_DELETE") %>' ImageUrl='<%# Session["themeURL"] + "images/delete_inline.gif" %>' BorderWidth="0" Width="12" Height="12" ImageAlign="AbsMiddle" Runat="server" />
							<asp:LinkButton  Visible='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "delete") >= 0 %>' CommandName="Key.DeleteFolder" CommandArgument='<%# Eval("KEY") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_DELETE") %>' Runat="server" />
						</span>
					</div>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
	<%@ Register TagPrefix="SplendidCRM" Tagname="CheckAll" Src="~/_controls/CheckAll.ascx" %>
	<SplendidCRM:CheckAll ID="ctlCheckAll" Visible="<%# !PrintView %>" Runat="Server" />
	<%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
	<SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !PrintView %>" Runat="Server" />
</div>
