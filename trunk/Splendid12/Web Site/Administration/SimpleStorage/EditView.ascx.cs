/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.SimpleStorage
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		// 01/13/2010 Paul.  Add footer buttons. 
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string      sBUCKET          ;
		protected Table       tblMain          ;
		protected TextBox     BUCKET           ;
		protected Label       OWNER_NAME       ;
		protected Label       OWNER_TYPE       ;
		protected TextBox     LOGGING_BUCKET   ;
		protected TextBox     LOGGING_PREFIX   ;
		protected RequiredFieldValidator BUCKET_REQUIRED;
		protected SplendidGrid grdACL          ;
		protected Button       btnMakePublic   ;
		protected Button       btnMakePrivate  ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				BUCKET_REQUIRED.Enabled = true;
				if ( Page.IsValid )
				{
					bool bLoggingChanged = false;
					LOGGING_BUCKET.Text = LOGGING_BUCKET.Text.Trim();
					LOGGING_PREFIX.Text = LOGGING_PREFIX.Text.Trim();
					try
					{
						AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
						if ( Sql.IsEmptyString(Request["BUCKET"]) )
						{
							s3.CreateBucket(BUCKET.Text, false);
							Cache.Remove("AmazonS3.Buckets");
							sBUCKET = BUCKET.Text;
							if ( !Sql.IsEmptyString(LOGGING_BUCKET.Text) ) //&& !Sql.IsEmptyString(LOGGING_PREFIX.Text) )
							{
								s3.SetBucketLoggingStatus(sBUCKET, LOGGING_BUCKET.Text, LOGGING_PREFIX.Text);
								// 10/24/2007 Paul.  We need to grant access to the LogDelivery group. 
								s3.SetBucketLoggingACL(sBUCKET, LOGGING_BUCKET.Text, true);
							}
						}
						else
						{
							string sTargetBucket = String.Empty;
							string sTargetPrefix = String.Empty;
							s3.GetBucketLoggingStatus(sBUCKET, ref sTargetBucket, ref sTargetPrefix);
							// 10/24/2007 Paul.  Update the logging only if it has changed. 
							if ( sTargetBucket != LOGGING_BUCKET.Text || sTargetPrefix != LOGGING_PREFIX.Text )
							{
								bLoggingChanged = true;
								if ( !Sql.IsEmptyString(LOGGING_BUCKET.Text) ) // && !Sql.IsEmptyString(LOGGING_PREFIX.Text) )
								{
									s3.SetBucketLoggingStatus(sBUCKET, LOGGING_BUCKET.Text, LOGGING_PREFIX.Text);
									// 10/24/2007 Paul.  We need to grant access to the LogDelivery group. 
									s3.SetBucketLoggingACL(sBUCKET, LOGGING_BUCKET.Text, true);
								}
								else
								{
									s3.SetBucketLoggingStatus(sBUCKET, String.Empty, String.Empty);
									if ( !Sql.IsEmptyString(sTargetBucket) )
										s3.SetBucketLoggingACL(sBUCKET, sTargetBucket, false);
								}
							}
						}
					}
					catch(Exception ex)
					{
						SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						ctlDynamicButtons.ErrorText = ex.Message;
						return;
					}
					if ( Sql.IsEmptyString(Request["BUCKET"]) )
						Response.Redirect("default.aspx");
					else if ( bLoggingChanged )
						Response.Redirect("edit.aspx?BUCKET=" + Server.UrlEncode(sBUCKET));
					else
						Response.Redirect("view.aspx?BUCKET=" + Server.UrlEncode(sBUCKET));
				}
			}
			else if ( e.CommandName == "MakePublic" )
			{
				try
				{
					AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
					s3.SetBucketAccessControlPolicy(sBUCKET, true);
					Response.Redirect("edit.aspx?BUCKET=" + Server.UrlEncode(sBUCKET));
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "MakePrivate" )
			{
				try
				{
					AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
					s3.SetBucketAccessControlPolicy(sBUCKET, false);
					Response.Redirect("edit.aspx?BUCKET=" + Server.UrlEncode(sBUCKET));
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					return;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			// 06/04/2006 Paul.  Visibility is already controlled by the ASPX page, but it is probably a good idea to skip the load. 
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			try
			{
				// 10/18/2010 Paul.  The required fields need to be bound manually. 
				BUCKET_REQUIRED.DataBind();
				sBUCKET = Sql.ToString(Request["BUCKET"]);
				if ( !IsPostBack )
				{
					if ( !Sql.IsEmptyString(sBUCKET) )
					{
						// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
						ctlDynamicButtons.Title = Sql.ToString(sBUCKET);
						SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
						ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

						BUCKET.Text     = sBUCKET;
						BUCKET.Enabled = false;
						
						AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
						try
						{
							string sTargetBucket = String.Empty;
							string sTargetPrefix = String.Empty;

							s3.GetBucketLoggingStatus(sBUCKET, ref sTargetBucket, ref sTargetPrefix);
							LOGGING_BUCKET.Text = sTargetBucket;
							LOGGING_PREFIX.Text = sTargetPrefix;
						}
						catch(Exception ex)
						{
							SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
							ctlDynamicButtons.ErrorText += "GetBucketLoggingStatus: " + ex.Message + "<br/>";
						}

						try
						{
							string sOwnerName = String.Empty;
							string sOwnerType = String.Empty;

							DataTable dtACL = s3.GetBucketAccessControlPolicy(sBUCKET, ref sOwnerName, ref sOwnerType);
							DataView vwACL = new DataView(dtACL);
							vwACL.RowFilter = "GRANTEE_NAME = 'http://acs.amazonaws.com/groups/global/AllUsers'";
							btnMakePublic .Enabled = vwACL.Count == 0;
							btnMakePrivate.Enabled = vwACL.Count > 0;

							grdACL.DataSource = dtACL;
							grdACL.DataBind();
							OWNER_NAME.Text = sOwnerName;
							OWNER_TYPE.Text = sOwnerType;
						}
						catch(Exception ex)
						{
							SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
							ctlDynamicButtons.ErrorText += "GetBucketAccessControlPolicy: " + ex.Message + "<br/>";
						}
					}
				}
				else
				{
					// 12/02/2005 Paul.  When validation fails, the header title does not retain its value.  Update manually. 
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "SimpleStorage";
			SetMenu(m_sMODULE);
			// 06/10/2008 Paul.  Dynamic buttons need to be recreated in order for events to fire. 
			ctlDynamicButtons.AppendButtons(m_sMODULE + ".DetailView", Guid.Empty, null);
			ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
		}
		#endregion
	}
}
