<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.SimpleStorage.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="SimpleStorage" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:Table Width="100%" CellPadding="0" CellSpacing="0" CssClass="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<asp:Table ID="tblMain" Width="100%" CellSpacing="0" CellPadding="0" runat="server">
					<asp:TableRow>
						<asp:TableCell Width="15%" VerticalAlign="Top"><asp:Label Text='<%# L10n.Term("SimpleStorage.LBL_BUCKET"        ) %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" VerticalAlign="Top">
							<asp:TextBox ID="BUCKET" Width="150" runat="server" />
							<asp:RequiredFieldValidator ID="BUCKET_REQUIRED" ControlToValidate="BUCKET" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableViewState="false" EnableClientScript="false" Enabled="false" runat="server" />
						</asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="15%" VerticalAlign="Top"><asp:Label Text='<%# L10n.Term("SimpleStorage.LBL_LOGGING_BUCKET") %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" VerticalAlign="Top"><asp:TextBox ID="LOGGING_BUCKET" Width="150" runat="server" /></asp:TableCell>
						<asp:TableCell Width="15%" VerticalAlign="Top"><asp:Label Text='<%# L10n.Term("SimpleStorage.LBL_OWNER_NAME") %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" VerticalAlign="Top"><asp:Label ID="OWNER_NAME" runat="server" /></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow>
						<asp:TableCell Width="15%" VerticalAlign="Top"><asp:Label Text='<%# L10n.Term("SimpleStorage.LBL_LOGGING_PREFIX") %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" VerticalAlign="Top"><asp:TextBox ID="LOGGING_PREFIX" Width="150" runat="server" /></asp:TableCell>
						<asp:TableCell Width="15%" VerticalAlign="Top"><asp:Label Text='<%# L10n.Term("SimpleStorage.LBL_OWNER_TYPE") %>' runat="server" /></asp:TableCell>
						<asp:TableCell Width="35%" VerticalAlign="Top"><asp:Label ID="OWNER_TYPE" runat="server" /></asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="ListHeader" Src="~/_controls/ListHeader.ascx" %>
	<SplendidCRM:ListHeader Module="SimpleStorage" Title="SimpleStorage.LBL_ACCESS_CONTROL_LIST" Runat="Server" />

	<asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
		<asp:Button ID="btnMakePublic"  CommandName="MakePublic"  OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term("SimpleStorage.LBL_MAKE_PUBLIC_BUTTON_LABEL" ) + "  " %>' ToolTip='<%# L10n.Term("SimpleStorage.LBL_MAKE_PUBLIC_BUTTON_TITLE" ) %>' Enabled="false" Runat="server" />
		<asp:Button ID="btnMakePrivate" CommandName="MakePrivate" OnCommand="Page_Command" CssClass="button" Text='<%# "  " + L10n.Term("SimpleStorage.LBL_MAKE_PRIVATE_BUTTON_LABEL") + "  " %>' ToolTip='<%# L10n.Term("SimpleStorage.LBL_MAKE_PRIVATE_BUTTON_TITLE") %>' Enabled="false" Runat="server" />
	</asp:Panel>

	<SplendidCRM:SplendidGrid id="grdACL" Width="100%" CssClass="listView"
		CellPadding="3" CellSpacing="0" border="0"
		AllowPaging="false" PageSize="20" AllowSorting="false" 
		AutoGenerateColumns="false" 
		EnableViewState="true" runat="server">
		<ItemStyle            CssClass="oddListRowS1"  />
		<AlternatingItemStyle CssClass="evenListRowS1" />
		<HeaderStyle          CssClass="listViewThS1"  />
		<PagerStyle HorizontalAlign="Right" Mode="NextPrev" PageButtonCount="6" Position="Top" CssClass="listViewPaginationTdS1" PrevPageText=".LNK_LIST_PREVIOUS" NextPageText=".LNK_LIST_NEXT" />
		<Columns>
			<asp:BoundColumn HeaderText="SimpleStorage.LBL_LIST_GRANTEE_NAME" DataField="GRANTEE_NAME" ItemStyle-Width="33%" />
			<asp:BoundColumn HeaderText="SimpleStorage.LBL_LIST_PERMISSION"   DataField="PERMISSION"   ItemStyle-Width="33%" />
			<asp:BoundColumn HeaderText="SimpleStorage.LBL_LIST_GRANTEE_TYPE" DataField="GRANTEE_TYPE" ItemStyle-Width="33%" />
		</Columns>
	</SplendidCRM:SplendidGrid>
</div>
