/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.SimpleStorage
{
	/// <summary>
	/// Summary description for DetailView.
	/// </summary>
	public class DetailView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons ctlDynamicButtons;
		protected _controls.ExportHeader ctlExportHeader;
		protected SearchBasic            ctlSearch      ;

		protected string        sBUCKET        ;
		protected string        sPREFIX        ;

		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected MassUpdate    ctlMassUpdate  ;
		protected PlaceHolder   plcBreadcrumbs ;
		protected TextBox       FOLDER_NAME    ;
		protected Label         lblFolderNameRequired;

		protected AmazonSimpleStorage s3;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				string sKEY = Sql.ToString(e.CommandArgument);
				if ( e.CommandName == "Key.Edit" )
				{
					Response.Redirect("../SimpleStorageObject/edit.aspx?BUCKET=" +  sBUCKET + "&KEY=" + Server.UrlEncode(sKEY));
				}
				else if ( e.CommandName == "Key.Delete" )
				{
					s3.DeleteObject(sBUCKET, sKEY);
					Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
					Bind(true);
				}
				else if ( e.CommandName == "Key.DeleteFolder" )
				{
					// 10/02/2007 Paul.  There are no real folders in S3.  
					// At some point we may recurse the folders and delete all data, but for now, just delete the empty directory file. 
					s3.DeleteFolder(sBUCKET, sKEY);
					Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
					Bind(true);
				}
				else if ( e.CommandName == "MassDelete" )
				{
					string[] arrKEY = Request.Form.GetValues("chkMain");
					if ( arrKEY != null )
					{
						foreach ( string sMassKey in arrKEY )
						{
							DateTime dtNow = DateTime.Now;
							s3.DeleteObject(sBUCKET, sMassKey);
						}
						Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
						Bind(true);
					}
				}
				else if ( e.CommandName == "MassPublic" )
				{
					string[] arrKEY = Request.Form.GetValues("chkMain");
					if ( arrKEY != null )
					{
						foreach ( string sMassKey in arrKEY )
						{
							DateTime dtNow = DateTime.Now;
							s3.SetObjectAccessControlPolicy(sBUCKET, sMassKey, true);
						}
						Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
						Bind(true);
					}
				}
				else if ( e.CommandName == "MassPrivate" )
				{
					string[] arrKEY = Request.Form.GetValues("chkMain");
					if ( arrKEY != null )
					{
						foreach ( string sMassKey in arrKEY )
						{
							DateTime dtNow = DateTime.Now;
							s3.SetObjectAccessControlPolicy(sBUCKET, sMassKey, false);
						}
						Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
						Bind(true);
					}
				}
				else if ( e.CommandName == "Key.CreateFolder" )
				{
					FOLDER_NAME.Text = FOLDER_NAME.Text.Trim();
					if ( !Sql.IsEmptyString(FOLDER_NAME) )
					{
						sKEY = sPREFIX + FOLDER_NAME.Text;
						
						s3.CreateFolder(sBUCKET, sKEY, true);

						Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
						Bind(true);
						FOLDER_NAME.Text = String.Empty;
					}
					else
					{
						lblFolderNameRequired.Text = L10n.Term(".ERR_REQUIRED_FIELD");
						lblFolderNameRequired.Visible = true;
					}
				}
				else if ( e.CommandName == "Key.UploadFiles" )
				{
					// 10/02/2007 Paul.  There can be a maximum of 10 attachments, not including attachments that were previously saved. 
					for ( int i=0; i < 10; i++ )
					{
						HtmlInputFile fileATTACHMENT = FindControl("attachment" + i.ToString()) as HtmlInputFile;
						if ( fileATTACHMENT != null )
						{
							HttpPostedFile pstATTACHMENT = fileATTACHMENT.PostedFile;
							if ( pstATTACHMENT != null )
							{
								if ( pstATTACHMENT.FileName.Length > 0 )
								{
									string sFILENAME       = Path.GetFileName (pstATTACHMENT.FileName);
									string sFILE_EXT       = Path.GetExtension(sFILENAME);
									string sFILE_MIME_TYPE = pstATTACHMENT.ContentType;

									sKEY = sPREFIX + sFILENAME;
									s3.PutObject(sBUCKET, sKEY, sFILE_MIME_TYPE, true, pstATTACHMENT.ContentLength, pstATTACHMENT.InputStream);
								}
							}
						}
					}
					Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
					Bind(true);
				}
				else if ( e.CommandName == "Clear" )
				{
					ctlSearch.ClearForm();
					vwMain.RowFilter = String.Empty;
					grdMain.CurrentPageIndex = 0;
					grdMain.DataBind();
				}
				else if ( e.CommandName == "Search" )
				{
					ctlSearch.NAME = ctlSearch.NAME.Trim();
					// 09/30/2007 Paul.  Always include the folder navigation when searching. 
					if ( !Sql.IsEmptyString(ctlSearch.NAME) )
						vwMain.RowFilter = "TYPE < 1 or NAME like '%" + Sql.EscapeSQLLike(ctlSearch.NAME) + "%'";
					else
						vwMain.RowFilter = String.Empty;
					// 10/13/2005 Paul.  Make sure to clear the page index prior to applying search. 
					grdMain.CurrentPageIndex = 0;
					grdMain.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
				Cache.Remove("AmazonS3." + sBUCKET + "." + sPREFIX);
				Bind(true);
			}
		}

		private void Bind(bool bBind)
		{
			plcBreadcrumbs.Controls.Clear();
			if ( !Sql.IsEmptyString(sPREFIX) )
			{
				if ( sPREFIX == "/" )
					sPREFIX = String.Empty;
				else if ( !sPREFIX.EndsWith("/") )
					sPREFIX += "/";

				HyperLink lnk = new HyperLink();
				lnk.Text = sBUCKET;
				lnk.NavigateUrl = "view.aspx?BUCKET=" + sBUCKET;
				plcBreadcrumbs.Controls.Add(lnk);

				StringBuilder sbCurrentPrefix = new StringBuilder();
				string[] arrPREFIX = sPREFIX.Substring(0, sPREFIX.Length-1).Split('/');
				for ( int i = 0; i < arrPREFIX.Length; i++ )
				{
					string sThis = arrPREFIX[i];
					if ( !String.IsNullOrEmpty(sThis) )
					{
						Literal lit = new Literal();
						lit.Text = " / ";
						plcBreadcrumbs.Controls.Add(lit);
						
						lnk = new HyperLink();
						lnk.Text = sThis;
						if ( sbCurrentPrefix.Length > 0 )
							sbCurrentPrefix.Append("/");
						sbCurrentPrefix.Append(sThis);
						if ( i < arrPREFIX.Length-1 )
							lnk.NavigateUrl = "view.aspx?BUCKET=" + sBUCKET + "&PREFIX=" + sbCurrentPrefix.ToString() + "/";
						plcBreadcrumbs.Controls.Add(lnk);
					}
				}
			}
			else
			{
				HyperLink lnk = new HyperLink();
				lnk.Text = sBUCKET;
				plcBreadcrumbs.Controls.Add(lnk);
			}

			DataTable dt = Cache.Get("AmazonS3." + sBUCKET + "." + sPREFIX) as DataTable;
			if ( dt == null )
			{
				dt = s3.ListBucket(sBUCKET, sPREFIX);
				// 10/04/2007 Paul.  Convert the dates to the user's timezone. 
				foreach ( DataRow row in dt.Rows )
				{
					if ( row["LAST_MODIFIED"] != DBNull.Value )
						row["LAST_MODIFIED"] = T10n.FromServerTime(Sql.ToDateTime(row["LAST_MODIFIED"]));
				}
				Cache.Insert("AmazonS3." + sBUCKET + "." + sPREFIX, dt, null, AmazonCache.DefaultCacheExpiration(),System.Web.Caching.Cache.NoSlidingExpiration);
			}
			vwMain = dt.DefaultView;
			vwMain.Sort = "TYPE, FOLDER, NAME";
			grdMain.DataSource = vwMain ;
			if ( bBind )
			{
				grdMain.DataBind();
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			// 03/18/2010 Paul.  This view is really a list of obects in a bucket. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			try
			{
				if ( !Sql.IsEmptyString(AmazonCache.AmazonAccessKeyID()) )
				{
					s3 = AmazonCache.CreateAmazonSimpleStorage();
					sBUCKET = Sql.ToString(Request["BUCKET"]);
					sPREFIX = Sql.ToString(Request["PREFIX"]);
					if ( !IsPostBack )
					{
						// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
						ctlDynamicButtons.Title = Sql.ToString(sBUCKET);
						SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
						ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
					}
					else
					{
						ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
						SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					}
					// 06/05/2015 Paul.  Ignore precompile requests. 
					if ( !Sql.IsEmptyString(sBUCKET) )
						Bind(!IsPostBack);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "SimpleStorage";
			SetMenu(m_sMODULE);
			ctlSearch.Command += new CommandEventHandler(Page_Command);
			// 11/10/2006 Paul.  We need to capture the export events. 
			ctlExportHeader.Command += new CommandEventHandler(Page_Command);
			ctlMassUpdate.Command += new CommandEventHandler(Page_Command);
			// 03/16/2010 Paul.  Allow Admin Delegates. 
			if ( Security.AdminUserAccess(m_sMODULE, "delete") < 0 && Security.AdminUserAccess(m_sMODULE, "edit") < 0 )
				ctlMassUpdate.Visible = false;
		}
		#endregion
	}
}
