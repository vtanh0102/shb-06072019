/**
 * Copyright (C) 2008-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.SimpleStorage
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected Label         lblError       ;
		protected PlaceHolder   plcSearch      ;
		protected int           nAdvanced      ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				string sBUCKET = Sql.ToString(e.CommandArgument);
				if ( e.CommandName == "Bucket.Edit" )
				{
					Response.Redirect("edit.aspx?BUCKET=" + Server.UrlEncode(sBUCKET));
				}
				else if ( e.CommandName == "Bucket.Delete" )
				{
					AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
					s3.DeleteBucket(sBUCKET);
					Cache.Remove("AmazonS3.Buckets");
					Bind(true);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
				Bind(true);
			}
		}

		private void Bind(bool bBind)
		{
			if ( !Sql.IsEmptyString(AmazonCache.AmazonAccessKeyID()) )
			{
				DataTable dt = Cache.Get("AmazonS3.Buckets") as DataTable;
				if ( dt == null )
				{
					AmazonSimpleStorage s3 = AmazonCache.CreateAmazonSimpleStorage();
					dt = s3.ListAllMyBuckets();
					// 10/04/2007 Paul.  We need to convert the time to the user's specified timezone. 
					foreach ( DataRow row in dt.Rows )
					{
						row["DATE_ENTERED"] = T10n.FromServerTime(Sql.ToDateTime(row["DATE_ENTERED"]));
					}

					Cache.Insert("AmazonS3.Buckets", dt, null, AmazonCache.DefaultCacheExpiration(),System.Web.Caching.Cache.NoSlidingExpiration);
				}
				vwMain = dt.DefaultView;
				grdMain.DataSource = vwMain ;
				if ( bBind )
				{
					grdMain.SortColumn = "BUCKET";
					grdMain.SortOrder  = "asc" ;
					grdMain.ApplySort();
					grdMain.DataBind();
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			// 03/10/2010 Paul.  Apply full ACL security rules. 
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
			{
				// 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
				Parent.DataBind();
				return;
			}

			try
			{
				Bind(!IsPostBack);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "SimpleStorage";
			SetMenu(m_sMODULE);
			//this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView);
		}
		#endregion
	}
}
