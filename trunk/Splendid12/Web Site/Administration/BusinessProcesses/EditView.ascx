<%@ Control CodeBehind="EditView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.BusinessProcesses.EditView" %>
<script runat="server">
/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>

<script type='text/javascript'>
var MAX_DUMP_DEPTH = 2;
var sPopupWindowOptions = '<%= SplendidCRM.Crm.Config.PopupWindowOptions() %>';
// 07/03/2016 Paul.  sBaseEditViewID is used by the BusinessProcess/app/index.js to enable the Save button. 
var sBaseEditViewID     = '<%= this.ClientID %>';
var bDebug              = <%# bDebug ? "true" : " false" %>;
var sIMAGE_SERVER       = sREMOTE_SERVER + 'App_Themes/Atlantic/images/';
// 06/24/2017 Paul.  We need a way to turn off bootstrap for BPMN, ReportDesigner and ChatDashboard. 
bDESKTOP_LAYOUT         = true;
// 08/25/2013 Paul.  Move sREMOTE_SERVER definition to the master pages. 
//var sREMOTE_SERVER  = '<%# Application["rootURL"] %>';
// 05/09/2016 Paul.  Move javascript objects to separate file.  Keep data initialization here. 
CONFIG['enable_team_management' ] = '<%# SplendidCRM.Crm.Config.enable_team_management()  %>';
CONFIG['require_team_management'] = '<%# SplendidCRM.Crm.Config.require_team_management() %>';
CONFIG['enable_dynamic_teams'   ] = '<%# SplendidCRM.Crm.Config.enable_dynamic_teams()    %>';
CONFIG['require_user_assignment'] = '<%# SplendidCRM.Crm.Config.require_user_assignment() %>';
CONFIG['enable_speech'          ] = '<%# Utils.SupportsSpeech && Sql.ToBoolean(Application["CONFIG.enable_speech"]) %>';
bIS_MOBILE        = '<%# Utils.IsMobileDevice.ToString() %>';
sUSER_ID          = '<%# Security.USER_ID   %>';
sUSER_NAME        = '<%# Security.USER_NAME %>';
sTEAM_ID          = '<%# Security.TEAM_ID   %>';
sPICTURE          = '<%# Sql.EscapeJavaScript(Security.PICTURE) %>';
sUSER_TIME_FORMAT = '<%# Sql.ToString(Session["USER_SETTINGS/TIMEFORMAT"]) %>';
sUSER_DATE_FORMAT = '<%# Sql.ToString(Session["USER_SETTINGS/DATEFORMAT"]) %>';
sUSER_THEME       = '<%# Sql.ToString(Session["USER_SETTINGS/THEME"     ]) %>';
sUSER_LANG        = '<%# Sql.ToString(Session["USER_SETTINGS/CULTURE"   ]) %>';
if ( sUSER_LANG == '' )
	sUSER_LANG = 'en-US';

sUSER_CurrencyDecimalDigits    = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalDigits     %>';
sUSER_CurrencyDecimalSeparator = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator  %>';
sUSER_CurrencyGroupSeparator   = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator    %>';
sUSER_CurrencyGroupSizes       = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSizes[0]     %>';
sUSER_CurrencyNegativePattern  = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyNegativePattern   %>';
sUSER_CurrencyPositivePattern  = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyPositivePattern   %>';
sUSER_CurrencySymbol           = '<%# System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol            %>';

L10n.Term = function(sTerm)
{
	if ( TERMINOLOGY[sUSER_LANG + '.' + sTerm] === undefined )
		return sTerm;
	return TERMINOLOGY[sUSER_LANG + '.' + sTerm];
};

L10n.GetList = function(sListName)
{
	if ( TERMINOLOGY_LISTS[sUSER_LANG + '.' + sListName] === undefined )
		return sListName;
	return TERMINOLOGY_LISTS[sUSER_LANG + '.' + sListName];
};

L10n.GetListTerms = function(sListName)
{
	if ( TERMINOLOGY_LISTS[sUSER_LANG + '.' + sListName] === undefined )
		return sListName;
	return TERMINOLOGY_LISTS[sUSER_LANG + '.' + sListName];
};

L10n.GetListTerms = function(sLIST_NAME)
{
	if ( TERMINOLOGY_LISTS[sUSER_LANG + '.' + sLIST_NAME] === undefined )
		return sLIST_NAME;
	
	var arrTerms = new Array();
	var arrList  = TERMINOLOGY_LISTS[sUSER_LANG + '.' + sLIST_NAME];
	if ( arrList != null )
	{
		for ( var i = 0; i < arrList.length; i++ )
		{
			var sTerm = L10n.ListTerm(sLIST_NAME, arrList[i]);
			if ( sTerm == null )
				sTerm = '';
			arrTerms.push(sTerm);
		}
	}
	return arrTerms;
}

L10n.ListTerm = function(sLIST_NAME, sNAME)
{
	var sEntryName = '.' + sLIST_NAME + '.' + sNAME;
	return L10n.Term(sEntryName);
}

function DesignerResize()
{
	try
	{
		// 09/19/2016 Paul.  Only include jsDump if in debug mode. 
		var jstDropZone = document.getElementById('js-drop-zone');
		var jsDump      = document.getElementById('js-dump');
		var rect = jstDropZone.getBoundingClientRect();
		var nHeight = $(window).height() - rect.top;
		nHeight -= 42;
		if ( bDebug )
		{
			jstDropZone.style.height = Math.floor(nHeight * 80 / 100).toString() + 'px';
			jsDump.style.height      = Math.floor(nHeight * 20 / 100).toString() + 'px';
			jsDump.style.width       = ($(window).width() - 20) + 'px';
			jsDump.style.backgroundColor = '#eee';
			jsDump.style.border = '1px solid black';
		}
		else
		{
			jstDropZone.style.height = nHeight.toString() + 'px';
		}
	}
	catch(e)
	{
		alert(e.message);
	}
}

window.onload = function()
{
	DesignerResize();
	$(window).resize(DesignerResize);
}

function Cancel()
{
	window.location.href = 'default.aspx';
	return false;
}

$(document).ready(function()
{
	try
	{
		var bgPage = chrome.extension.getBackgroundPage();
		bgPage.IsAuthenticated(function(status, message)
		{
			try
			{
				if ( status == 1 )
				{
					ReportDesigner_InitUI(function(status, message)
					{
						if ( status == 1 )
						{
							SplendidError.SystemMessage('Loading modules.');
							ReportDesigner_LoadBusinessProcessModules(function(status, message)
							{
								if ( status == 1 )
								{
									arrReportDesignerModules = message;
									SplendidError.SystemMessage('Loading BPMN engine.');
									var script = document.createElement('script');
									script.type = 'text/javascript';
									script.src = 'html5/index.js?' + (new Date()).getTime();
									script.async = true;
									script.onload = function()
									{
										SplendidError.SystemMessage('');
									};
									document.head.appendChild(script);
								}
								else
								{
									SplendidError.SystemMessage(message);
								}
							});
						}
						else
						{
							SplendidError.SystemMessage(message);
						}
					});
				}
				else
				{
					SplendidError.SystemMessage(message);
				}
			}
			catch(e)
			{
				SplendidError.SystemError(e, 'BusinessProcesses/edit.aspx IsAuthenticated()');
			}
		});
	}
	catch(e)
	{
		SplendidError.SystemMessage(e.message);
	}
});

function ImportBpmn()
{
	try
	{
		var fileIMPORT = document.getElementById(sBaseEditViewID + '_fileIMPORT');
		fileIMPORT.click();
	}
	catch(e)
	{
		SplendidError.SystemMessage(e.message);
	}
}
</script>

<asp:HiddenField ID="hidBPMN" runat="server" />
<asp:HiddenField ID="hidSVG"  runat="server" />
<div id="divEditView">
	<%@ Register TagPrefix="SplendidCRM" Tagname="ModuleHeader" Src="~/_controls/ModuleHeader.ascx" %>
	<SplendidCRM:ModuleHeader ID="ctlModuleHeader" ShowRequired="true" EditView="true" Module="BusinessProcesses" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />
	<div class="button-panel">
		<asp:Button ID="btnSUBMIT" style="display: none; margin-right: 3px;" OnCommand="Page_Command" CommandName="Save" runat="server" />
		<asp:Button ID="btnSAVE"   Text='<%# "  " + L10n.Term(".LBL_SAVE_BUTTON_LABEL"  ) + "  " %>' ToolTip='<%# L10n.Term(".LBL_SAVE_BUTTON_TITLE"  ) %>' CssClass="button" style="margin-right: 3px;" runat="server" />
		<asp:Button ID="btnCANCEL" Text='<%# "  " + L10n.Term(".LBL_CANCEL_BUTTON_LABEL") + "  " %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' CssClass="button" style="margin-right: 3px;" OnClientClick="return Cancel();" runat="server" />
		<asp:Label  ID="lblError"  CssClass="error" EnableViewState="false" Runat="server" />
		<span id="divError" class="error"></span>
		<span id="lblError" class="error"></span>
		
		<input id="fileIMPORT" type="file" style="display: none;" runat="server" />
		<asp:Button ID="btnIMPORT" Text='<%# "  " + L10n.Term("BusinessProcesses.LBL_IMPORT_BUTTON_LABEL") + "  " %>' ToolTip='<%# L10n.Term("BusinessProcesses.LBL_IMPORT_BUTTON_TITLE") %>' CssClass="button" style="margin-right: 3px; float: right;" OnClientClick="return ImportBpmn();" runat="server" />
	</div>

	<div style="text-align: center;">
		<span id="bpmn-toolbar-copy"                    title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_COPY"                   ) %>" style="cursor: pointer; display: none;" class="glyphicon-copy" ></span>
		<span id="bpmn-toolbar-paste"                   title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_PASTE"                  ) %>" style="cursor: pointer; display: none;" class="glyphicon-paste"></span>
		<span id="bpmn-toolbar-undo"                    title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_UNDO"                   ) %>" style="cursor: pointer;" class="icon-undo"></span>
		<span id="bpmn-toolbar-redo"                    title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_REDO"                   ) %>" style="cursor: pointer;" class="icon-redo"></span>
		<span class="toolbar-separator"></span>
		<span id="bpmn-toolbar-align-left"              title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_LEFT"             ) %>" style="cursor: pointer;" class="icon-align-left-tool"></span>
		<span id="bpmn-toolbar-align-horz-center"       title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_HORIZONTAL_CENTER") %>" style="cursor: pointer;" class="icon-align-horizontal-center-tool"></span>
		<span id="bpmn-toolbar-align-right"             title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_RIGHT"            ) %>" style="cursor: pointer;" class="icon-align-right-tool"></span>
		<span id="bpmn-toolbar-align-bottom"            title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_BOTTOM"           ) %>" style="cursor: pointer;" class="icon-align-bottom-tool"></span>
		<span id="bpmn-toolbar-align-vert-center"       title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_VERTICAL_CENTER"  ) %>" style="cursor: pointer;" class="icon-align-vertical-center-tool"></span>
		<span id="bpmn-toolbar-align-top"               title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_ALIGN_TOP"              ) %>" style="cursor: pointer;" class="icon-align-top-tool"></span>
		<span id="bpmn-toolbar-distribute-horizontally" title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_DISTRIBUTE_HORIZONTALLY") %>" style="cursor: pointer;" class="icon-distribute-horizontally-tool"></span>
		<span id="bpmn-toolbar-distribute-vertically"   title="<%# L10n.Term("BusinessProcesses.LBL_BPMN_DISTRIBUTE_VERTICALLY"  ) %>" style="cursor: pointer;" class="icon-distribute-vertically-tool"></span>
	</div>
	<div class="content" id="js-drop-zone" style="height: 800px;">
		<div class="message intro">
			<div class="note">
			Drop BPMN diagram from your desktop or <a id="js-create-diagram" href>create a new diagram</a> to get started.
			</div>
		</div>
		<div class="message error">
			<div class="note">
				<p>Ooops, we could not display the BPMN 2.0 diagram.</p>
				<div class="details">
					<span>cause of the problem</span>
					<pre></pre>
				</div>
			</div>
		</div>
		<div class="canvas" id="js-canvas"></div>
		<div id="js-properties-panel"></div>
	</div>
	<div id="js-dump" style="white-space: pre; overflow-y: scroll; overflow-x: scroll;<%# bDebug ? "" : " display: none;" %>"></div>

	<!-- ul class="buttons">
		<li>
			download
		</li>
		<li>
			<a id="js-download-diagram" href title="download BPMN diagram">
			BPMN diagram
			</a>
		</li>
		<li>
			<a id="js-download-svg" href title="download as SVG image">
			SVG image
			</a>
		</li>
	</!-->
</div>
