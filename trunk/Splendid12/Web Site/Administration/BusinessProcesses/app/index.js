'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var fs                       = require('fs');
var $                        = require('jquery');
var container                = $('#js-drop-zone');
var canvas                   = $('#js-canvas');
var BpmnModeler              = require('bpmn-js/lib/Modeler');
var propertiesPanelModule    = require('bpmn-js-properties-panel');
var camundaModdleDescriptor  = require('camunda-bpmn-moddle/resources/camunda');
var crmModdleDescriptor      = require('./descriptors/crm');
var propertiesProviderModule = require('./provider');
var paletteProvider          = require('./palette');
var replaceProvider          = require('./replace');
var popupmenuProvider        = require('./popup-menu');
var contextpadProvider       = require('./context-pad');
var bpmnRules                = require('./rules');

var bpmnModeler = new BpmnModeler(
{
	container: canvas,
	propertiesPanel:
	{
		parent: '#js-properties-panel'
	},
	additionalModules:
	[
		propertiesPanelModule,
		propertiesProviderModule,
		paletteProvider,
		replaceProvider,
		popupmenuProvider,
		contextpadProvider,
		bpmnRules,
	],
	moddleExtensions:
	{
		camunda: camundaModdleDescriptor,
		crm: crmModdleDescriptor
	},
	keyboard:
	{
		bindTo: document
	}
});

var newDiagramXML = fs.readFileSync(__dirname + '/../resources/newStartEvent.bpmn', 'utf-8');

function createNewDiagram()
{
	openDiagram(newDiagramXML);
	return newDiagramXML;
}

function openDiagram(xml)
{
	bpmnModeler.importXML(xml, function(err)
	{
		if ( err )
		{
			container.removeClass('with-diagram').addClass('with-error');
			container.find('.error pre').text(err.message);
			console.error(err);
		}
		else
		{
			container.removeClass('with-error').addClass('with-diagram');
		}
	});
}

function saveSVG(done)
{
	bpmnModeler.saveSVG(done);
}

function saveDiagram(done)
{
	bpmnModeler.saveXML({ format: true }, function(err, xml)
	{
		done(err, xml);
	});
}

function FileUploadEvent()
{
	var files = this.files;
	if ( files.length > 0 )
	{
		var file = files[0];
		// http://www.javascripture.com/FileReader
		var reader = new FileReader();
		reader.onload = function()
		{
			var xml = reader.result;
			bpmnModeler.importXML(xml, function(err)
			{
				if ( err )
				{
					container.removeClass('with-diagram').addClass('with-error');
					container.find('.error pre').text(err.message);
					console.error(err);
				}
				else
				{
					container.removeClass('with-error').addClass('with-diagram');
				}
			});
		};
		reader.readAsText(file);
	}
}

function registerFileDrop(container, callback)
{
	function handleFileSelect(e)
	{
		e.stopPropagation();
		e.preventDefault();
		var files = e.dataTransfer.files;
		var file = files[0];
		var reader = new FileReader();
		reader.onload = function(e)
		{
			var xml = e.target.result;
			callback(xml);
		};
		reader.readAsText(file);
	}

	function handleDragOver(e)
	{
		e.stopPropagation();
		e.preventDefault();
		e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
	}

	container.get(0).addEventListener('dragover', handleDragOver, false);
	container.get(0).addEventListener('drop', handleFileSelect, false);
}


////// file drag / drop ///////////////////////
// check file api availability
if ( !window.FileList || !window.FileReader )
{
	$('#' + sBaseEditViewID + '_lblError').text('Looks like you use an older browser that does not support drag and drop.  Try using Chrome, Firefox or the Internet Explorer > 10.');
}
else
{
	registerFileDrop(container, openDiagram);
}

function formatXml(xml)
{
	// 07/22/2016 Paul.  We don't need to see the diagram data, so strip. 
	var nStartDiagram = xml.indexOf('<bpmndi:BPMNDiagram');
	if ( nStartDiagram > 0 )
	{
		var nEndDiagram = xml.indexOf('</bpmndi:BPMNDiagram>');
		if ( nEndDiagram > 0 )
		{
			nEndDiagram += 21;
			xml = xml.substring(0, nStartDiagram) + xml.substring(nEndDiagram, xml.length);
		}
	}

	var formatted = '';
	var reg = /(>)(<)(\/*)/g;
	xml = xml.replace(reg, '$1\r\n$2$3');
	var pad = 0;
	jQuery.each(xml.split('\r\n'), function(index, node)
	{
		var indent = 0;
		if ( node.match( /.+<\/\w[^>]*>$/ ) )
		{
			indent = 0;
		}
		else if ( node.match( /^<\/\w/ ) )
		{
			if ( pad != 0 )
			{
				pad -= 1;
			}
		}
		else if ( node.match( /^<\w[^>]*[^\/]>.*$/ ) )
		{
			indent = 1;
		}
		else
		{
			indent = 0;
		}

		var padding = '';
		for ( var i = 0; i < pad; i++ )
		{
			padding += '    ';
		}

		formatted += padding + node + '\r\n';
		pad += indent;
	});
	return formatted;
}

function ModelChanged()
{
	bpmnModeler.saveXML({ format: true }, function(err, xml)
	{
		if ( xml !== undefined )
		{
			$('#js-dump').text(formatXml(xml));
		}
		else
		{
			$('#js-dump').text('');
		}
	});
}

// bootstrap diagram functions
$(document).on('ready', function()
{
	$('#js-create-diagram').click(function(e)
	{
		e.stopPropagation();
		e.preventDefault();
		createNewDiagram();
	});
	
	/*
	var downloadLink = $('#js-download-diagram');
	var downloadSvgLink = $('#js-download-svg');

	$('.buttons a').click(function(e)
	{
		if (!$(this).is('.active'))
		{
			e.preventDefault();
			e.stopPropagation();
		}
	});

	function setEncoded(link, name, data)
	{
		var encodedData = encodeURIComponent(data);
		if (data)
		{
			link.addClass('active').attr(
			{
				'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
				'download': name
			});
		}
		else
		{
			link.removeClass('active');
		}
	}

	var debounce = require('lodash/function/debounce');

	var exportArtifacts = debounce(function()
	{
		saveSVG(function(err, svg)
		{
			setEncoded(downloadSvgLink, 'diagram.svg', err ? null : svg);
		});
		saveDiagram(function(err, xml)
		{
			setEncoded(downloadLink, 'diagram.bpmn', err ? null : xml);
		});
	}, 500);

	bpmnModeler.on('commandStack.changed', exportArtifacts);
	*/
	bpmnModeler.on('commandStack.changed', ModelChanged);
	$(window).resize(function()
	{
		var canvas = bpmnModeler.get('canvas');
		canvas.resized();
	});

	var xml = $('#' + sBaseEditViewID + '_hidBPMN').val();
	if ( xml == '' )
		xml = createNewDiagram();
	else
		openDiagram(xml);
	$('#js-dump').text(formatXml(xml));

	// 07/16/2016 Paul.  Move logo to the left. 
	var poweredby = document.getElementsByClassName('bjs-powered-by');
	if ( poweredby.length > 0 )
	{
		poweredby[0].style.right = '';
		poweredby[0].style.left  = '50%';
	}

	var fileIMPORT = document.getElementById(sBaseEditViewID + '_fileIMPORT');
	if ( fileIMPORT != null )
		fileIMPORT.onchange = FileUploadEvent;

	var bEnableSubmit = false;
	var btnSAVE = $('#' + sBaseEditViewID + '_btnSAVE');
	btnSAVE.click(function()
	{
		try
		{
			$('#' + sBaseEditViewID + '_lblError').text('');
			bpmnModeler.saveXML({ format: true }, function(err, xml)
			{
				if ( xml !== undefined )
				{
					$('#' + sBaseEditViewID + '_hidBPMN').val(xml);
					bpmnModeler.saveSVG(function(err, svg)
					{
						if ( svg !== undefined )
						{
							bEnableSubmit = true;
							$('#' + sBaseEditViewID + '_hidSVG').val(svg);
							$('#' + sBaseEditViewID + '_btnSUBMIT').click();
						}
						else
						{
							$('#' + sBaseEditViewID + '_lblError').text(err.message);
						}
					});
				}
				else
				{
					$('#' + sBaseEditViewID + '_lblError').text(err.message);
				}
				//done(err, xml);
			});
		}
		catch(e)
		{
			$('#' + sBaseEditViewID + '_lblError').text(e.message);
		}
		return false;
	});

	$('#aspnetForm').submit(function(event)
	{
		// 07/08/2016 Paul.  The Properties Panel Form + button is submitting the form.  Block it. 
		if ( bEnableSubmit )
		{
			// 06/11/2016 Paul. We have to remove the properties panel prior to submit, otherwise the id value may get submitted. 
			$('#js-properties-panel').remove();
		}
		return bEnableSubmit;
	});

	var btnBpmnToolbarCopy                   = document.getElementById('bpmn-toolbar-copy'                   );
	var btnBpmnToolbarPaste                  = document.getElementById('bpmn-toolbar-paste'                  );
	var btnBpmnToolbarUndo                   = document.getElementById('bpmn-toolbar-undo'                   );
	var btnBpmnToolbarRedo                   = document.getElementById('bpmn-toolbar-redo'                   );
	var btnBpmnToolbarAlignBottom            = document.getElementById('bpmn-toolbar-align-bottom'           );
	var btnBpmnToolbarAlignLeft              = document.getElementById('bpmn-toolbar-align-left'             );
	var btnBpmnToolbarAlignHorzCenter        = document.getElementById('bpmn-toolbar-align-horz-center'      );
	var btnBpmnToolbarAlignRight             = document.getElementById('bpmn-toolbar-align-right'            );
	var btnBpmnToolbarAlignTop               = document.getElementById('bpmn-toolbar-align-top'              );
	var btnBpmnToolbarAlignVertCenter        = document.getElementById('bpmn-toolbar-align-vert-center'      );
	var btnBpmnToolbarDistributeHorizontally = document.getElementById('bpmn-toolbar-distribute-horizontally');
	var btnBpmnToolbarDistributeVertically   = document.getElementById('bpmn-toolbar-distribute-vertically'  );

	var editorActions = bpmnModeler.get('editorActions')._actions;
	// 08/16/2015 Paul.  Paste uses current mouse position, so that is not going to work. 
	//if ( btnBpmnToolbarCopy                   != null ) btnBpmnToolbarCopy                  .onclick = function() { editorActions.copy() ; };
	//if ( btnBpmnToolbarPaste                  != null ) btnBpmnToolbarPaste                 .onclick = function() { editorActions.paste(); };
	if ( btnBpmnToolbarUndo                   != null ) btnBpmnToolbarUndo                  .onclick = function() { editorActions.undo() ; };
	if ( btnBpmnToolbarRedo                   != null ) btnBpmnToolbarRedo                  .onclick = function() { editorActions.redo() ; };
	if ( btnBpmnToolbarAlignBottom            != null ) btnBpmnToolbarAlignBottom           .onclick = function() { editorActions.alignElements     ({ type: 'bottom'     }); };
	if ( btnBpmnToolbarAlignLeft              != null ) btnBpmnToolbarAlignLeft             .onclick = function() { editorActions.alignElements     ({ type: 'left'       }); };
	if ( btnBpmnToolbarAlignHorzCenter        != null ) btnBpmnToolbarAlignHorzCenter       .onclick = function() { editorActions.alignElements     ({ type: 'center'     }); };
	if ( btnBpmnToolbarAlignRight             != null ) btnBpmnToolbarAlignRight            .onclick = function() { editorActions.alignElements     ({ type: 'right'      }); };
	if ( btnBpmnToolbarAlignTop               != null ) btnBpmnToolbarAlignTop              .onclick = function() { editorActions.alignElements     ({ type: 'top'        }); };
	if ( btnBpmnToolbarAlignVertCenter        != null ) btnBpmnToolbarAlignVertCenter       .onclick = function() { editorActions.alignElements     ({ type: 'middle'     }); };
	if ( btnBpmnToolbarDistributeHorizontally != null ) btnBpmnToolbarDistributeHorizontally.onclick = function() { editorActions.distributeElements({ type: 'horizontal' }); };
	if ( btnBpmnToolbarDistributeVertically   != null ) btnBpmnToolbarDistributeVertically  .onclick = function() { editorActions.distributeElements({ type: 'vertical'   }); };
});

