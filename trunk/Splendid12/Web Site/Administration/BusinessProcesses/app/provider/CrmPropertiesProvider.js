'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var inherits = require('inherits');
var PropertiesActivator = require('bpmn-js-properties-panel/lib/PropertiesActivator');

// Require all properties you need from existing providers.
// In this case all available bpmn relevant properties without camunda extensions.
//var processProps       = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/ProcessProps'      );
var eventProps         = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/EventProps'          );
var linkProps          = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/LinkProps'           );
var documentationProps = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/DocumentationProps'  );
var idProps            = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/IdProps'             );
var nameProps          = require('bpmn-js-properties-panel/lib/provider/bpmn/parts/NameProps'           );
//var sequenceFlowProps  = require('bpmn-js-properties-panel/lib/provider/camunda/parts/SequenceFlowProps');

// Require your custom property entries.
var crmProcessProps           = require('./parts/CrmProcessProps'               );
var crmProcessVariables       = require('./parts/CrmProcessVariables'           );
var crmStartEventProps        = require('./parts/CrmStartEventProps'            );
var crmTimerStartEventProps   = require('./parts/CrmTimerStartEventProps'       );
var crmModuleFilterProps      = require('./parts/CrmModuleFilterProps'          );
var crmMessageTemplateProps   = require('./parts/CrmMessageTemplateProps'       );
var crmMessageRecipientProps  = require('./parts/CrmMessageRecipientProps'      );
var crmMessageReportProps     = require('./parts/CrmMessageReportProps'         );
var crmTimerIntermediateProps = require('./parts/CrmTimerIntermediateCatchEvent');
var crmEscalationEventProps   = require('./parts/CrmEscalationEventProps'       );
var crmEscalationFieldsProps  = require('./parts/CrmEscalationFieldsProps'      );
var crmTaskProps              = require('./parts/CrmTaskProps'                  );
var crmBusinessRuleTaskProps  = require('./parts/CrmBusinessRuleTaskProps'      );
var sequenceFlowProps         = require('./parts/SequenceFlowProps'             );
var crmUserTaskProps          = require('./parts/CrmUserTaskProps'              );
var crmUserTaskFieldsProps    = require('./parts/CrmUserTaskFieldsProps'        );

// C:\Web.net\SplendidCRM6\Administration\BusinessProcesses\node_modules\bpmn-js-properties-panel\lib\PropertiesPanel.js
function createGeneralTabGroups(element, bpmnFactory, elementRegistry)
{
	var generalGroup =
	{
		id: 'general',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_GENERAL_GROUP'),
		entries: []
	};
	nameProps(generalGroup, element);

	var detailsGroup =
	{
		id: 'details',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_DETAILS_GROUP'),
		entries: []
	};

	var eventsGroup =
	{
		id: 'events',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_EVENTS_GROUP'),
		entries: []
	};

	var modulesGroup =
	{
		id: 'modules',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_MODULES_GROUP'),
		entries: []
	};

	var messageTemplateGroup =
	{
		id: 'messageTemplates',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_TEMPLATE_GROUP'),
		entries: []
	};

	var messageRecipientGroup =
	{
		id: 'messageRecipient',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_RECIPIENT_GROUP'),
		entries: []
	};

	var messageReportGroup =
	{
		id: 'messageReport',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_REPORT_GROUP'),
		entries: []
	};

	var variablesGroup = 
	{
		id: 'variables',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_VARIABLES_GROUP'),
		entries: []
	};

	//linkProps                (detailsGroup, element);
	//eventProps               (detailsGroup, element, bpmnFactory, elementRegistry);
	sequenceFlowProps        (detailsGroup         , element, bpmnFactory);

	crmProcessProps          (generalGroup         , element, bpmnFactory, elementRegistry);
	crmProcessVariables      (variablesGroup       , element, bpmnFactory, elementRegistry);
	
	crmStartEventProps       (eventsGroup          , element, bpmnFactory, elementRegistry);
	crmTimerStartEventProps  (eventsGroup          , element, bpmnFactory, elementRegistry);
	crmTimerIntermediateProps(eventsGroup          , element, bpmnFactory, elementRegistry);
	crmEscalationEventProps  (eventsGroup          , element, bpmnFactory, elementRegistry);

	crmModuleFilterProps     (modulesGroup         , element, bpmnFactory, elementRegistry);
	crmTaskProps             (modulesGroup         , element, bpmnFactory, elementRegistry);
	crmUserTaskProps         (modulesGroup         , element, bpmnFactory, elementRegistry);
	crmBusinessRuleTaskProps (modulesGroup         , element, bpmnFactory, elementRegistry);

	crmMessageTemplateProps  (messageTemplateGroup , element, bpmnFactory, elementRegistry);
	crmMessageRecipientProps (messageRecipientGroup, element, bpmnFactory, elementRegistry);

	crmMessageReportProps    (messageReportGroup   , element, bpmnFactory, elementRegistry);

	// 06/25/2016 Paul.  Array begin cannot be on a new line, otherwise return will return NULL. 
	var arr = 
	[
		generalGroup,
		detailsGroup,
		eventsGroup,
		modulesGroup,
		messageTemplateGroup,
		messageRecipientGroup,
		messageReportGroup,
		variablesGroup
	];
	return arr;
}

function createMetadataTabGroups(element, bpmnFactory, elementRegistry)
{
	var metaDataGroup =
	{
		id: 'metadataGroup',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_METADATA_GROUP'),
		entries: []
	};

	//idProps(metaDataGroup, element, elementRegistry);
	// 06/25/2016 Paul.  We do not use the Executable flag for a process. 
	//processProps(metaDataGroup, element);

	var documentationGroup =
	{
		id: 'documentation',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_DOCUMENTATION_GROUP'),
		entries: []
	};

	documentationProps(documentationGroup, element, bpmnFactory);

	// 06/25/2016 Paul.  Array begin cannot be on a new line, otherwise return will return NULL. 
	var arr =
	[
		metaDataGroup,
		documentationGroup
	];
	return arr;
}

function createFieldsTabGroups(element, bpmnFactory, elementRegistry)
{
	var fieldsGroup =
	{
		id: 'fields',
		label: L10n.Term('BusinessProcesses.LBL_BPMN_FIELDS_GROUP'),
		entries: []
	};

	crmEscalationFieldsProps (fieldsGroup, element, bpmnFactory, elementRegistry);
	crmUserTaskFieldsProps   (fieldsGroup, element, bpmnFactory, elementRegistry);
	var arr =
	[
		fieldsGroup
	];
	return arr;
}

function CrmPropertiesProvider(eventBus, bpmnFactory, elementRegistry)
{
	PropertiesActivator.call(this, eventBus);
	this.getTabs = function(element)
	{
		var generalTab =
		{
			id: 'general',
			label: L10n.Term('BusinessProcesses.LBL_BPMN_GENERAL_TAB'),
			groups: createGeneralTabGroups(element, bpmnFactory, elementRegistry)
		};
		var metadataTab =
		{
			id: 'metadata',
			label: L10n.Term('BusinessProcesses.LBL_BPMN_METADATA_TAB'),
			groups: createMetadataTabGroups(element, bpmnFactory, elementRegistry)
		};
		var fieldsTab =
		{
			id: 'fields',
			label: L10n.Term('BusinessProcesses.LBL_BPMN_FIELDS_TAB'),
			groups: createFieldsTabGroups(element, bpmnFactory, elementRegistry)
		};
		// 06/25/2016 Paul.  Array begin cannot be on a new line, otherwise return will return NULL. 
		var arr =
		[
			generalTab,
			fieldsTab,
			metadataTab
		];
		return arr;
	};
}

inherits(CrmPropertiesProvider, PropertiesActivator);

module.exports = CrmPropertiesProvider;
