'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var entryFactory          = require('bpmn-js-properties-panel/lib/factory/EntryFactory');
var cmdHelper             = require('bpmn-js-properties-panel/lib/helper/CmdHelper');
var getBusinessObject     = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;
var is                    = require('bpmn-js/lib/util/ModelUtil').is;
var queryEntryFactory     = require('./factory/QueryEntryFactory');

function ensureNotNull(prop)
{
	if ( !prop )
	{
		throw new Error(prop + ' must be set.');
	}
	return prop;
}

module.exports = function(group, element, bpmnFactory)
{
	if ( is(element, 'bpmn:StartEvent') )
	{
		var arrSelectOptions = new Array();
		arrSelectOptions.push( { name: '', value: '' } );
		for ( var i = 0; i < arrReportDesignerModules.length; i++ )
		{
			var option = new Object();
			option.value = arrReportDesignerModules[i].ModuleName;
			// 07/05/2016 Paul.  The display name is already translated by the Rest.svc call. 
			option.name  = arrReportDesignerModules[i].DisplayName;
			arrSelectOptions.push(option);
		}
		group.entries.push(entryFactory.selectBox(
		{
			id            : 'BASE_MODULE',
			//description   : L10n.Term('BusinessProcesses.LBL_BPMN_BASE_MODULE_DESCRIPTION'),
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_BASE_MODULE'),
			modelProperty : 'BASE_MODULE',
			selectOptions : arrSelectOptions,
			get : function (element)
			{
				var businessObject = getBusinessObject(element);
				var res = {};
				var prop = ensureNotNull(this.id);
				res[prop] = businessObject.get(prop);
				console.log('CrmModuleFilterProps get ' + prop + ' = ' + res[prop]);
				return res;
			},
			set : function (element, values)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				if ( values[prop] !== '' )
				{
					res[prop] = values[prop];
					var module = ReportDesigner_FindModuleByName(res[prop]);
					if ( module != null )
					{
						var CrLf            = '\r\n';
						var txtCamundaJSON = document.getElementById('camunda-' + 'MODULE_FILTER_JSON');
						var txtCamundaSQL  = document.getElementById('camunda-' + 'MODULE_FILTER_SQL' );
						if ( txtCamundaSQL != null )
						{
							txtCamundaSQL.innerHTML = 'select ' + module.TableName + '.ID' + CrLf + '  from vw' + module.TableName + ' ' + module.TableName;
						}
						if ( txtCamundaJSON != null )
						{
							txtCamundaJSON.value = '{ "GroupAndAggregate": false, "Tables": [ { "ModuleName": "' + module.ModuleName + '", "TableName": "' + module.TableName + '"} ], "SelectedFields": [ { "TableName": "' + module.TableName + '", "ColumnName": "ID", "FieldName": "' + module.TableName + '.ID' + '", "DisplayName": "' + module.ModuleName + ' ID' + '", "AggregateType": null, "DisplayWidth": null, "SortDirection": null } ] }';
							if ( document.createEvent )
							{
								var evt = document.createEvent('HTMLEvents');
								evt.initEvent('change', true, false);
								txtCamundaJSON.dispatchEvent(evt);
							}
							else if ( txtCamundaJSON.fireEvent )
							{
								txtCamundaJSON.fireEvent('onChange');
							}
						}
					}
				}
				else
				{
					res[prop] = undefined;
				}
				console.log('CrmModuleFilterProps set ' + prop + ' = ' + res[prop]);
				return cmdHelper.updateProperties(element, res);
			},
			validate: function(element)
			{
				var value = this.get(element)[this.id];
				if ( Sql.ToString(value) == '' )
				{
					var err = new Object();
					err[this.id] = L10n.Term('.ERR_REQUIRED_FIELD');
					return err;
				}
			},
		}));
		
		group.entries.push(queryEntryFactory(
		{
			id           : 'MODULE_FILTER',
			//description   : L10n.Term('BusinessProcesses.LBL_BPMN_MODULE_FILTER_DESCRIPTION'),
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_MODULE_FILTER'),
			modelProperty: 'MODULE_FILTER',
		}, element, bpmnFactory));
	}
};
