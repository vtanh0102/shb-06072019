'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var find                  = require('lodash/collection/find');
var entryFactory          = require('bpmn-js-properties-panel/lib/factory/EntryFactory');
var is                    = require('bpmn-js/lib/util/ModelUtil').is;
var getBusinessObject     = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;
var cmdHelper             = require('bpmn-js-properties-panel/lib/helper/CmdHelper');
var eventDefinitionHelper = require('bpmn-js-properties-panel/lib/helper/EventDefinitionHelper');
var popupEntryFactory     = require('./factory/ModulePopupEntryFactory');

function ensureNotNull(prop)
{
	if ( !prop )
	{
		throw new Error(prop + ' must be set.');
	}
	return prop;
}

function hasEventDefinition(element, eventDefinition)
{
	var bo = getBusinessObject(element);
	return !!find(bo.eventDefinitions || [], function(definition) { return is(definition, eventDefinition); });
}

module.exports = function(group, element, bpmnFactory, elementRegistry)
{
	if ( (is(element, 'bpmn:IntermediateThrowEvent') || is(element, 'bpmn:EndEvent')) && hasEventDefinition(element, 'bpmn:MessageEventDefinition') )
	{
		var workflow_alert_type_dom = L10n.GetList('workflow_alert_type_dom');
		var arrAlertTypes = new Array();
		for ( var i = 0; i < workflow_alert_type_dom.length; i++ )
		{
			var type = new Object();
			arrAlertTypes.push(type);
			type.value = workflow_alert_type_dom[i];
			type.name  = L10n.Term('.workflow_alert_type_dom.' + workflow_alert_type_dom[i]);
		}

		group.entries.push(entryFactory.selectBox(
		{
			id            : 'ALERT_TYPE',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_ALERT_TYPE'),
			modelProperty : 'ALERT_TYPE',
			emptyParameter: false,
			selectOptions : arrAlertTypes,
			get : function(element)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				res[prop] = messageEventDefinition.get(prop);
				return res;
			},
			set : function(element, values)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				if ( values[prop] !== '' )
				{
					res[prop] = values[prop];
				}
				else
				{
					res[prop] = undefined;
				}
				// Set to child Message element. 
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return cmdHelper.updateBusinessObject(element, messageEventDefinition, res);
			}
		}));

		group.entries.push(popupEntryFactory(
		{
			id            : 'ASSIGNED_USER_',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_ASSIGNED_TO'),
			modelProperty : 'ASSIGNED_USER_',
			module        : 'Users',
			show : function(element, node)
			{
				var prop = 'ALERT_TYPE';
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return messageEventDefinition && messageEventDefinition.get(prop) != 'Notification';
			},
			validate : function(element)
			{
				return [];
			}
		}));

		group.entries.push(popupEntryFactory(
		{
			id            : 'TEAM_',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_TEAM'),
			modelProperty : 'TEAM_',
			module        : 'Teams',
			show : function(element, node)
			{
				var prop = 'ALERT_TYPE';
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return messageEventDefinition && messageEventDefinition.get(prop) != 'Notification';
			},
			validate : function(element)
			{
				return [];
			}
		}));

		var workflow_source_type_dom = L10n.GetList('workflow_source_type_dom');
		var arrSourceType = new Array();
		for ( var i = 0; i < workflow_source_type_dom.length; i++ )
		{
			var type = new Object();
			arrSourceType.push(type);
			type.value = workflow_source_type_dom[i];
			type.name  = L10n.Term('.workflow_source_type_dom.' + workflow_source_type_dom[i]);
		}

		group.entries.push(entryFactory.selectBox(
		{
			id            : 'SOURCE_TYPE',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_SOURCE_TYPE'),
			modelProperty : 'SOURCE_TYPE',
			emptyParameter: false,
			selectOptions : arrSourceType,
			get : function(element)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				res[prop] = messageEventDefinition.get(prop);
				return res;
			},
			set : function(element, values)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				if ( values[prop] !== '' )
				{
					res[prop] = values[prop];
				}
				else
				{
					res[prop] = undefined;
				}
				// Set to child Message element. 
				var commands = [];
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				if ( messageEventDefinition )
				{
					var oldProp = messageEventDefinition.get(prop);
					if ( oldProp != values[prop] )
						commands.push(cmdHelper.updateBusinessObject(element, messageEventDefinition, { CUSTOM_TEMPLATE_ID: undefined, CUSTOM_TEMPLATE_NAME: undefined, ALERT_TEXT: undefined }));
				}
				commands.push(cmdHelper.updateBusinessObject(element, messageEventDefinition, res));
				return commands;
			}
		}));

		group.entries.push(popupEntryFactory(
		{
			id            : 'CUSTOM_TEMPLATE_',
			//description   : L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_TEMPLATE_DESCRIPTION'),
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_MESSAGE_TEMPLATE'),
			modelProperty : 'CUSTOM_TEMPLATE_',
			module        : 'WorkflowAlertTemplates',
			show : function(element, node)
			{
				var prop = 'SOURCE_TYPE';
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return messageEventDefinition && messageEventDefinition.get(prop) == 'custom template';
			}
		}));

		group.entries.push(entryFactory.textField(
		{
			id            : 'ALERT_SUBJECT',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_ALERT_SUBJECT'),
			modelProperty : 'ALERT_SUBJECT',
			disabled : function(element, node)
			{
				var prop = 'SOURCE_TYPE';
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return !(messageEventDefinition && messageEventDefinition.get(prop) != 'custom template');
			},
			get : function(element)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				res[prop] = messageEventDefinition.get(prop);
				return res;
			},
			set : function(element, values)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				if ( values[prop] !== '' )
				{
					res[prop] = values[prop];
				}
				else
				{
					res[prop] = undefined;
				}
				// Set to child Message element. 
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return cmdHelper.updateBusinessObject(element, messageEventDefinition, res);
			},
			validate : function(element)
			{
				var value = this.get(element)[this.id];
				if ( Sql.ToString(value) == '' )
				{
					var err = new Object();
					err[this.id] = L10n.Term('.ERR_REQUIRED_FIELD');
					return err;
				}
			}
		}));

		group.entries.push(entryFactory.textArea(
		{
			id            : 'ALERT_TEXT',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_ALERT_TEXT'),
			modelProperty : 'ALERT_TEXT',
			show : function(element, node)
			{
				var prop = 'SOURCE_TYPE';
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return messageEventDefinition && messageEventDefinition.get(prop) != 'custom template';
			},
			get : function(element)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				res[prop] = messageEventDefinition.get(prop);
				return res;
			},
			set : function(element, values)
			{
				var res = {};
				var prop = ensureNotNull(this.id);
				if ( values[prop] !== '' )
				{
					res[prop] = values[prop];
				}
				else
				{
					res[prop] = undefined;
				}
				// Set to child Message element. 
				var messageEventDefinition = eventDefinitionHelper.getMessageEventDefinition(element);
				return cmdHelper.updateBusinessObject(element, messageEventDefinition, res);
			},
			validate : function(element)
			{
				var value = this.get(element)[this.id];
				if ( Sql.ToString(value) == '' )
				{
					var err = new Object();
					err[this.id] = L10n.Term('.ERR_REQUIRED_FIELD');
					return err;
				}
			}
		}));

	}
};
