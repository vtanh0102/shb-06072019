'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var find              = require('lodash/collection/find');
var entryFactory      = require('bpmn-js-properties-panel/lib/factory/EntryFactory');
var is                = require('bpmn-js/lib/util/ModelUtil').is;
var getBusinessObject = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;

function hasEventDefinition(element, eventDefinition)
{
	var bo = getBusinessObject(element);
	return !!find(bo.eventDefinitions || [], function(definition) { return is(definition, eventDefinition); });
}

module.exports = function(group, element, bpmnFactory, elementRegistry)
{
	if ( is(element, 'bpmn:StartEvent') && !hasEventDefinition(element, 'bpmn:TimerEventDefinition') )
	{
		// C:\Web.net\SplendidCRM6\Administration\BusinessProcesses\node_modules\bpmn-js-properties-panel\lib\factory\SelectEntryFactory.js
		group.entries.push(entryFactory.selectBox(
		{ 
			id            : 'RECORD_TYPE',
			//description   : L10n.Term('BusinessProcesses.LBL_BPMN_RECORD_TYPE_DESCRIPTION'),
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_RECORD_TYPE'),
			modelProperty : 'RECORD_TYPE',
			selectOptions :
			[
				{ value: 'all'   , name: L10n.Term('.workflow_record_type_dom.all'   ) },
				{ value: 'new'   , name: L10n.Term('.workflow_record_type_dom.new'   ) },
				{ value: 'update', name: L10n.Term('.workflow_record_type_dom.update') }
			]
		}));
	}
};
