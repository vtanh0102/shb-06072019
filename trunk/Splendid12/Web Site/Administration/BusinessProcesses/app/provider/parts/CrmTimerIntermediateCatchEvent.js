'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

var find                  = require('lodash/collection/find');
var entryFactory          = require('bpmn-js-properties-panel/lib/factory/EntryFactory');
var textInputField        = require('bpmn-js-properties-panel/lib/factory/TextInputEntryFactory');
var selectBoxField        = require('bpmn-js-properties-panel/lib/factory/SelectEntryFactory');
var is                    = require('bpmn-js/lib/util/ModelUtil').is;
var getBusinessObject     = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;
var cmdHelper             = require('bpmn-js-properties-panel/lib/helper/CmdHelper');
var eventDefinitionHelper = require('bpmn-js-properties-panel/lib/helper/EventDefinitionHelper');
var durationEntryFactory  = require('./factory/DurationEntryFactory');

function hasEventDefinition(element, eventDefinition)
{
	var bo = getBusinessObject(element);
	return !!find(bo.eventDefinitions || [], function(definition) { return is(definition, eventDefinition); });
}

function ensureNotNull(prop)
{
	if ( !prop )
	{
		throw new Error(prop + ' must be set.');
	}
	return prop;
}

module.exports = function(group, element, bpmnFactory, elementRegistry)
{
	if ( is(element, 'bpmn:IntermediateCatchEvent') && hasEventDefinition(element, 'bpmn:TimerEventDefinition') )
	{
		group.entries.push(durationEntryFactory(
		{
			id            : 'DURATION',
			//description   : L10n.Term('BusinessProcesses.LBL_BPMN_DURATION_DESCRIPTION'),
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_DURATION'),
			modelProperty : 'DURATION'
		}));
	}
};
