'use strict';
/* Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

function StringBuilder()
{
	this.value = '';
	this.length = this.value.length;
}

StringBuilder.prototype.Append = function(s)
{
	this.value = this.value + s;
	this.length = this.value.length;
}

StringBuilder.prototype.toString = function(s)
{
	return this.value;
}

var Sql = new Object();
Sql.ToInteger = function(s)
{
	var n = parseInt(s);
	if ( n == NaN )
		n = 0;
	return n;
}

var culture = new Object();
culture.DateTimeFormat = new Object();
culture.DateTimeFormat.DayNames   = L10n.GetListTerms('day_names_dom'  );
culture.DateTimeFormat.MonthNames = L10n.GetListTerms('month_names_dom');

function BindArguments( fn )
{
	var args = [];
	for ( var n = 1; n < arguments.length; n++ )
		args.push( arguments[n] );
	return function () { return fn.apply( this, args ); };
}

function Duration(options)
{
	this.id            = options.id           ;
	this.label         = options.label        ;
	this.description   = options.description  ;
	this.modelProperty = options.modelProperty;
	this.duration      = null;
}

Duration.prototype.DurationChanged = function ()
{
	var sDuration = this.GetDuration();
	var txtCamunda = document.getElementById('camunda-' + this.id);
	txtCamunda.value = sDuration;
}

Duration.prototype.AddRawText = function(parent, sID, sLabel)
{
	var td = document.createElement('div');
	parent.appendChild(td);
	td.style.display = 'table-cell';
	td.appendChild(document.createTextNode(sLabel));
	var br = document.createElement('br');
	td.appendChild(br);
	var txt = document.createElement('input');
	td.appendChild(txt);
	txt.id        = sID;
	txt.type      = 'text';
	txt.size      = 3;
	txt.maxLength = 25;

	txt.onchange = BindArguments( function ( context )
	{
		context.DurationChanged();
	}, this);
}

Duration.prototype.GetDuration = function ()
{
	var ctlDURATION_DURATION_DAYS    = document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_DAYS'   );
	var ctlDURATION_DURATION_HOURS   = document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_HOURS'  );
	var ctlDURATION_DURATION_MINUTES = document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_MINUTES');
	var ctlDURATION_DURATION_SECONDS = document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_SECONDS');

	var sDays    = Sql.ToInteger(ctlDURATION_DURATION_DAYS   .value);
	var sHours   = Sql.ToInteger(ctlDURATION_DURATION_HOURS  .value);
	var sMinutes = Sql.ToInteger(ctlDURATION_DURATION_MINUTES.value);
	var sSeconds = Sql.ToInteger(ctlDURATION_DURATION_SECONDS.value);
	if ( sDays    < 0 ) sDays    = 0;
	if ( sHours   < 0 ) sHours   = 0;
	if ( sMinutes < 0 ) sMinutes = 0;
	if ( sSeconds < 0 ) sSeconds = 0;
	if ( sDays    < 9 ) sDays    = '0' + sDays   ;
	if ( sHours   < 9 ) sHours   = '0' + sHours  ;
	if ( sMinutes < 9 ) sMinutes = '0' + sMinutes;
	if ( sSeconds < 9 ) sSeconds = '0' + sSeconds;

	var sDuration = sDays + ':' + sHours + ':' + sMinutes + ':' + sSeconds;
	return sDuration;
}

Duration.prototype.SetDuration = function(sDuration)
{
	this.duration = sDuration;

	var nDays    = 0;
	var nHours   = 0;
	var nMinutes = 0;
	var nSeconds = 0;
	var arr = this.duration.split( ':' );
	if ( arr.length >= 0 ) nDays    = Sql.ToInteger(arr[0]);
	if ( arr.length >= 1 ) nHours   = Sql.ToInteger(arr[1]);
	if ( arr.length >= 2 ) nMinutes = Sql.ToInteger(arr[2]);
	if ( arr.length >= 3 ) nSeconds = Sql.ToInteger(arr[3]);

	document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_DAYS'   ).value = nDays   ;
	document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_HOURS'  ).value = nHours  ;
	document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_MINUTES').value = nMinutes;
	document.getElementById(this.id + '_' + 'ctlDURATION_DURATION_SECONDS').value = nSeconds;
	this.DurationChanged();
}

Duration.prototype.Render = function (ctlDURATION)
{
	var tbl = document.createElement('div');
	ctlDURATION.appendChild(tbl);
	tbl.style.display = 'table';
	var tr = document.createElement('div');
	tbl.appendChild(tr);
	tr.style.display = 'table-row';

	var divRaw = document.createElement('div');
	ctlDURATION.appendChild(divRaw);
	divRaw.id            = this.id + '_' + 'ctlDURATION_RAW';
	tbl = document.createElement('div');
	divRaw.appendChild(tbl);
	tbl.style.display = 'table';
	tr = document.createElement('div');
	tbl.appendChild(tr);
	tr.style.display = 'table-row';

	this.AddRawText(tr, this.id + '_' + 'ctlDURATION_DURATION_DAYS'   , L10n.Term('Schedulers.LBL_DAYS'   ));
	this.AddRawText(tr, this.id + '_' + 'ctlDURATION_DURATION_HOURS'  , L10n.Term('Schedulers.LBL_HOURS'  ));
	this.AddRawText(tr, this.id + '_' + 'ctlDURATION_DURATION_MINUTES', L10n.Term('Schedulers.LBL_MINS'   ));
	this.AddRawText(tr, this.id + '_' + 'ctlDURATION_DURATION_SECONDS', L10n.Term('Schedulers.LBL_SECONDS'));

	var divCamunda = document.createElement('div');
	divRaw.appendChild(divCamunda);
	divCamunda.style.display = 'none';
	var txtCamunda = document.createElement('input');
	divCamunda.appendChild(txtCamunda);
	txtCamunda.id   = 'camunda-' + this.id;
	// 06/28/2016 Paul.  Using the name field equal to the modelProperty is required for the properties system to save the changes. 
	txtCamunda.name = this.modelProperty;
}

module.exports = Duration;
