'use strict';

var is                = require('bpmn-js/lib/util/ModelUtil').is;
var getBusinessObject = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;
var isAny             = require('bpmn-js/lib/features/modeling/util/ModelingUtil').isAny;
var cmdHelper         = require('bpmn-js-properties-panel/lib/helper/CmdHelper');
var elementHelper     = require('bpmn-js-properties-panel/lib/helper/ElementHelper');
var entryFactory      = require('bpmn-js-properties-panel/lib/factory/EntryFactory');

var CONDITIONAL_SOURCES =
[
	'bpmn:Activity',
	'bpmn:ExclusiveGateway',
	'bpmn:InclusiveGateway',
	'bpmn:ComplexGateway'
];

function isConditionalSource(element)
{
	return isAny(element, CONDITIONAL_SOURCES);
}

module.exports = function(group, element, bpmnFactory)
{
	if ( is(element, 'bpmn:SequenceFlow') && isConditionalSource(element.source) )
	{
		console.log('bpmn:SequenceFlow');
		group.entries.push(entryFactory.textArea(
		{
			id            : 'condition',
			label         : L10n.Term('BusinessProcesses.LBL_BPMN_EXPRESSION'),
			modelProperty : 'condition',
			minRows       : 8,
			expandable    : true,
			get : function(element)
			{
				var bo = getBusinessObject(element);
				var conditionExpression = bo.conditionExpression;
				var values = {};
				if ( conditionExpression )
				{
					values.condition = conditionExpression.get('body');
				}
				return values;
			},
			set : function(element, values)
			{
				var commands = [];
				var conditionProps =
				{
					body: undefined
				};

				var condition = values.condition;
				conditionProps.body = condition;
				var update =
				{
					'conditionExpression': undefined
				};
				var bo = getBusinessObject(element);
				update.conditionExpression = elementHelper.createElement('bpmn:FormalExpression', conditionProps, bo, bpmnFactory );
				var source = element.source;
				// if default-flow, remove default-property from source
				if ( source.businessObject.default === bo )
				{
					commands.push(cmdHelper.updateProperties(source, { 'default': undefined }));
				}
				commands.push(cmdHelper.updateBusinessObject(element, bo, update));
				return commands;
			},
			validate : function(element, values)
			{
				var validationResult = {};
				if ( !values.condition )
				{
					validationResult.condition = 'Must provide a value';
				}
				return validationResult;
			}
		}));
	}
};

