<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DetailView.ascx.cs" Inherits="SplendidCRM.Administration.BusinessProcesses.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */

</script>
<style type="text/css">
.SourceCode
{
	border: 1px solid #ccc;
	border-image: none;
	width: 100%;
	white-space: pre;
	overflow-x: scroll;
	overflow-y: scroll;
	background-color: rgb(238, 238, 238);
	margin-top: 4px;
	margin-bottom: 4px;
}
</style>
<script type='text/javascript'>
function DesignerResize()
{
	try
	{
		var divSQL  = document.getElementById('divSQL' );
		var divBPMN = document.getElementById('divBPMN');
		var divXAML = document.getElementById('divXAML');
		divSQL .style.width =  ($(window).width() - 20) + 'px';
		divBPMN.style.width =  ($(window).width() - 20) + 'px';
		divXAML.style.width =  ($(window).width() - 20) + 'px';
	}
	catch(e)
	{
		alert(e.message);
	}
}

window.onload = function()
{
	DesignerResize();
	$(window).resize(DesignerResize);
}
</script>
<div id="divDetailView" runat="server">
	<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="BusinessProcesses" EnablePrint="true" HelpName="DetailView" EnableHelp="true" Runat="Server" />

	<%@ Register TagPrefix="SplendidCRM" Tagname="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
	<SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="<%# m_sMODULE %>" Visible="<%# !PrintView %>" Runat="Server" />

	<table ID="tblMain" class="tabDetailView" runat="server">
	</table>

	<div style="text-align: center;">
	<asp:Literal ID="litSVG"  runat="server" />
	</div>

	<div id="divDetailSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
	</div>

	<div id="divSQL"  class="SourceCode" style="height: 100px;<%# bDebug ? "" : " display: none;" %>"><asp:Literal ID="litSQL"  runat="server" /></div>
	<div id="divBPMN" class="SourceCode" style="height: 232px;<%# bDebug ? "" : " display: none;" %>"><asp:Literal ID="litBPMN" runat="server" /></div>
	<div id="divXAML" class="SourceCode" style="height: 500px;<%# bDebug ? "" : " display: none;" %>"><asp:Literal ID="litXAML" runat="server" /></div>
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
