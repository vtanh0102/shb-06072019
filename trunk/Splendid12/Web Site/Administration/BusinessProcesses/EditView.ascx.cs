/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Optimization;
using System.Diagnostics;

namespace SplendidCRM.Administration.BusinessProcesses
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.ModuleHeader ctlModuleHeader;
		protected Label                  lblError       ;
		protected Guid                   gID            ;
		protected HiddenField            hidBPMN        ;
		protected HiddenField            hidSVG         ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Save" )
				{
					Guid   gASSIGNED_USER_ID      = Guid.Empty;
					string sNAME                  = String.Empty;
					string sBASE_MODULE           = String.Empty;
					string sMODULE_TABLE          = String.Empty;
					string sAUDIT_TABLE           = String.Empty;
					bool   bSTATUS                = false;
					string sTYPE                  = String.Empty;
					string sRECORD_TYPE           = String.Empty;
					string sJOB_INTERVAL          = String.Empty; 
					string sFILTER_SQL            = String.Empty; 
					string sDESCRIPTION           = String.Empty;
					string sBPMN                  = hidBPMN.Value;
					string sSVG                   = hidSVG .Value;
					string sXAML                  = String.Empty;
					string sFREQUENCY_LIMIT_UNITS = String.Empty;
					int    nFREQUENCY_LIMIT_VALUE = 0;
					
					// <bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn">
					//   <bpmn2:process id="Process_1" isExecutable="false">
					//     <bpmn2:startEvent id="StartEvent_1">
					//       <bpmn2:timerEventDefinition />
					//     </bpmn2:startEvent>
					//   </bpmn2:process>
					
					// Standard options are timeDate, timeCycle, timeDuration
					//    <bpmn2:startEvent id="StartEvent_1">
					//      <bpmn2:timerEventDefinition>
					//        <bpmn2:timeCycle xsi:type="bpmn2:tFormalExpression">3</bpmn2:timeCycle>
					//      </bpmn2:timerEventDefinition>
					//    </bpmn2:startEvent>

					// <bpmn2:timerEventDefinition crm:JOB_INTERVAL="0::6::*::*::*" crm:FREQUENCY_LIMIT_UNITS="days" crm:FREQUENCY_LIMIT_VALUE="2" />
					
					BpmnDocument bpmn = new BpmnDocument();
					bpmn.LoadBpmn(sBPMN);
					XmlNodeList nlProcesses = bpmn.SelectNodesNS("bpmn2:process");
					if ( nlProcesses.Count == 0 )
						throw(new Exception("Process not found"));
					else if ( nlProcesses.Count > 1 )
						throw(new Exception("Only one Process is allowed"));
					else
					{
						string sSTATUS = String.Empty;
						XmlNode xProcess = nlProcesses.Item(0);
						sNAME             =               bpmn.SelectNodeAttribute(xProcess, "name"                );
						gASSIGNED_USER_ID = Sql.ToGuid   (bpmn.SelectNodeAttribute(xProcess, "crm:ASSIGNED_USER_ID"));
						bSTATUS           = Sql.ToBoolean(bpmn.SelectNodeAttribute(xProcess, "crm:PROCESS_STATUS"  ));
						// 10/01/2016 Paul.  SelectNodeValue() can return null, so make sure to convert to a string. 
						sDESCRIPTION      = Sql.ToString (bpmn.SelectNodeValue    (xProcess, "bpmn2:documentation" ));
						
						if ( Sql.IsEmptyString(sNAME) )
							throw(new Exception("Process name is empty."));
						XmlNodeList nlStartEvents = bpmn.SelectNodesNS(xProcess, "bpmn2:startEvent");
						if ( nlStartEvents.Count == 0 )
							throw(new Exception("StartEvent not found"));
						else if ( nlStartEvents.Count > 1 )
							throw(new Exception("Only one StartEvent is allowed"));
						else
						{
							XmlNode xStartEvent = nlStartEvents.Item(0);
							sBASE_MODULE  = bpmn.SelectNodeAttribute(xStartEvent, "crm:BASE_MODULE");
							sRECORD_TYPE  = bpmn.SelectNodeAttribute(xStartEvent, "crm:RECORD_TYPE");
							sMODULE_TABLE = Sql.ToString(Application["Modules." + sBASE_MODULE + ".TableName"]);
							sAUDIT_TABLE  = sMODULE_TABLE + "_AUDIT";
							
							if ( Sql.IsEmptyString(sRECORD_TYPE) )
								sRECORD_TYPE = "all";
							if ( Sql.IsEmptyString(sBASE_MODULE) )
								throw(new Exception("Base Module is empty."));
							
							XmlNodeList nlTimerEvent = bpmn.SelectNodesNS(xStartEvent, "bpmn2:timerEventDefinition");
							if ( nlTimerEvent.Count > 1 )
							{
								throw(new Exception("Only one TimerEventDefinition is allowed"));
							}
							else if ( nlTimerEvent.Count == 0 )
							{
								sTYPE = "normal";
							}
							else
							{
								XmlNode xTimerEvent = nlTimerEvent.Item(0);
								sTYPE                  = "time";
								sJOB_INTERVAL          =               bpmn.SelectNodeAttribute(xTimerEvent, "crm:JOB_INTERVAL"         ) ;
								sFREQUENCY_LIMIT_UNITS =               bpmn.SelectNodeAttribute(xTimerEvent, "crm:FREQUENCY_LIMIT_UNITS") ;
								nFREQUENCY_LIMIT_VALUE = Sql.ToInteger(bpmn.SelectNodeAttribute(xTimerEvent, "crm:FREQUENCY_LIMIT_VALUE"));
								
								if ( Sql.IsEmptyString(sJOB_INTERVAL) )
									throw(new Exception("JOB_INTERVAL is missing from TimerEventDefinition."));
								if ( !Sql.IsEmptyString(sFREQUENCY_LIMIT_UNITS) && nFREQUENCY_LIMIT_VALUE <= 0 )
									throw(new Exception("FREQUENCY_LIMIT_VALUE must be > 0 when FREQUENCY_LIMIT_UNITS is specified."));
							}
							
							XmlNodeList nlExtensionElements = bpmn.SelectNodesNS(xStartEvent, "bpmn2:extensionElements");
							if ( nlExtensionElements.Count == 0 )
							{
								throw(new Exception("ExtensionElements not found with crmModuleFilter"));
							}
							else
							{
								XmlNode xExtensionElements = nlExtensionElements.Item(0);
								XmlNodeList nlModuleFilter = bpmn.SelectNodesNS(xExtensionElements, "crm:crmModuleFilter");
								if ( nlModuleFilter.Count > 1 )
								{
									throw(new Exception("Only one crmModuleFilter is allowed"));
								}
								else if ( nlModuleFilter.Count == 0 )
								{
									throw(new Exception("crmModuleFilter not found"));
								}
								else
								{
									XmlNode xModuleFilter = nlModuleFilter.Item(0);
									string sReportJson = xModuleFilter.InnerText;
									if ( Sql.IsEmptyString(sReportJson) )
									{
										throw(new Exception("crmModuleFilter report definition not found"));
									}
									// 07/14/2016 Paul.  We don't need to get and recalculate the SQL, but to ensure integrity, we are going to rely upon the BuildReportSQL() version. 
									sFILTER_SQL = bpmn.SelectNodeAttribute(xModuleFilter, "MODULE_FILTER_SQL") ;
									sFILTER_SQL = Workflow4BuildXaml.BuildReportSQL(Application, sReportJson, sMODULE_TABLE, sTYPE, sFREQUENCY_LIMIT_UNITS, nFREQUENCY_LIMIT_VALUE);
								}
							}
						}
					}
					sXAML = Workflow4BuildXaml.ConvertBpmnToXaml(Application, sBPMN);
					
					if ( Page.IsValid )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									// 07/30/2016 Paul.  Make the default false so that the process needs to be manually activated. 
									SqlProcs.spBUSINESS_PROCESSES_Update
										( ref gID          
										, gASSIGNED_USER_ID
										, sNAME            
										, sBASE_MODULE     
										, sAUDIT_TABLE     
										, bSTATUS          
										, sTYPE            
										, sRECORD_TYPE     
										, sJOB_INTERVAL    
										, sDESCRIPTION     
										, sFILTER_SQL      
										, sBPMN            
										, sSVG             
										, sXAML            
										, trn              
										);
									SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, sNAME
										, "save"
										, trn
										);
									trn.Commit();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									lblError.Text = ex.Message;
									return;
								}
							}
						}
						Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}
			try
			{
				AjaxControlToolkit.ToolkitScriptManager mgrAjax = ScriptManager.GetCurrent(Page) as AjaxControlToolkit.ToolkitScriptManager;
				
				// 06/10/2016 Paul.  We need to remove jQuery 1.9.1 (added in master page) and let jQuery 2.1.1 load. 
				ScriptReference scrJQuery   = new ScriptReference("~/Include/javascript/jquery-1.9.1.min.js"      );
				ScriptReference scrJQueryUI = new ScriptReference("~/Include/javascript/jquery-ui-1.9.1.custom.js");
				if ( !mgrAjax.Scripts.Contains(scrJQuery  ) ) mgrAjax.Scripts.Remove(scrJQuery  );
				if ( !mgrAjax.Scripts.Contains(scrJQueryUI) ) mgrAjax.Scripts.Remove(scrJQueryUI);
				
				Sql.AddScriptReference(mgrAjax, "~/Include/javascript/json2.min.js"                 );
				Sql.AddScriptReference(mgrAjax, "~/Include/javascript/jquery-2.2.4.min.js"          );
				Sql.AddScriptReference(mgrAjax, "~/Include/javascript/jquery.ztree.all-3.5.min.js"  );
				Sql.AddScriptReference(mgrAjax, "~/html5/jQuery/jquery-ui-1.9.1.custom.js"          );
				// 02/11/2018 Paul.  Popups now require paging for customers with large data sets. 
				Sql.AddScriptReference(mgrAjax, "~/html5/jQuery/jquery.paging.min.js"               );
				
				// 07/01/2017 Paul.  Use Microsoft ASP.NET Web Optimization 1.1.3 to combine stylesheets and javascript. 
				// 01/24/2018 Paul.  Include version in url to ensure updates of combined files. 
				string sBundleName = "~/Dashboard/html5/SplendidScriptsCombined" + "_" + Sql.ToString(Application["SplendidVersion"]);
				Bundle bndSplendidScripts = new Bundle(sBundleName);
				bndSplendidScripts.Include("~/html5/consolelog.min.js"                         );
				bndSplendidScripts.Include("~/html5/Utility.js"                                );
				bndSplendidScripts.Include("~/html5/SplendidUI/Formatting.js"                  );
				bndSplendidScripts.Include("~/html5/SplendidUI/Sql.js"                         );
				// 07/03/2016 Paul.  Additional references needed by the QueryDesigner. 
				bndSplendidScripts.Include("~/html5/SplendidScripts/SystemCacheRequest.js"     );
				bndSplendidScripts.Include("~/html5/SplendidScripts/Application.js"            );
				bndSplendidScripts.Include("~/html5/SplendidScripts/Terminology.js"            );
				bndSplendidScripts.Include("~/html5/SplendidScripts/ListView.js"               );
				bndSplendidScripts.Include("~/html5/SplendidScripts/EditView.js"               );
				bndSplendidScripts.Include("~/html5/SplendidUI/SplendidInitUI.js"              );
				bndSplendidScripts.Include("~/html5/SplendidUI/EditViewUI.js"                  );
				bndSplendidScripts.Include("~/html5/SplendidUI/PopupViewUI.js"                 );
				bndSplendidScripts.Include("~/html5/SplendidUI/SearchViewUI.js"                );
				bndSplendidScripts.Include("~/html5/SplendidUI/SearchBuilder.js"               );
				bndSplendidScripts.Include("~/include/javascript/RestUtils.js"                 );
				bndSplendidScripts.Include("~/ReportDesigner/QueryDesigner.js"                 );
				BundleTable.Bundles.Add(bndSplendidScripts);
				Sql.AddScriptReference(mgrAjax, sBundleName);
				// 06/10/2016 Paul.  Using script manager does not work. 
				//Sql.AddScriptReference(mgrAjax, "~/Administration/BusinessProcesses/html5/index.js"  );
				
				Sql.AddStyleSheet(this.Page, "~/html5/jQuery/jquery-ui-1.9.1.custom.css");
				Sql.AddStyleSheet(this.Page, "~/html5/FullCalendar/fullcalendar.css"    );
				// 07/01/2017 Paul.  We cannot bundle zTreeStyle.css as it will change its relative path to images. 
				Sql.AddStyleSheet(this.Page, "~/Include/javascript/zTreeStyle.css"      );
				// 07/01/2017 Paul.  BPMN stylesheets contain fonts, so they cannot be combined. 
				Sql.AddStyleSheet(this.Page, "~/Administration/BusinessProcesses/html5/css/diagram-js.css");
				Sql.AddStyleSheet(this.Page, "~/Administration/BusinessProcesses/html5/vendor/bpmn-font/css/bpmn-embedded.css");
				Sql.AddStyleSheet(this.Page, "~/Administration/BusinessProcesses/html5/css/app.css");
				Sql.AddStyleSheet(this.Page, "~/Administration/BusinessProcesses/html5/css/app-font.css");
				Sql.AddStyleSheet(this.Page, "~/Administration/BusinessProcesses/html5/css/bootstrap-font.css");
				
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{
					Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *                        " + ControlChars.CrLf
							     + "  from vwBUSINESS_PROCESSES_Edit" + ControlChars.CrLf
							     + " where ID = @ID                 " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AddParameter(cmd, "@ID", gDuplicateID);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AddParameter(cmd, "@ID", gID);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								// 11/22/2010 Paul.  Convert data reader to data table for Rules Wizard. 
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											ctlModuleHeader.Title = Sql.ToString(rdr["NAME"]);
											ViewState["ctlDynamicButtons.Title"] = ctlModuleHeader.Title;
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
											Utils.UpdateTracker(Page, m_sMODULE, gID, ctlModuleHeader.Title);
											
											hidBPMN.Value = Sql.ToString(rdr["BPMN"]);
											hidSVG .Value = Sql.ToString(rdr["SVG" ]);
											ViewState["ctlDynamicButtons.Title"] = Sql.ToString(rdr["NAME"]);
										}
									}
								}
							}
						}
					}
					else
					{
						//this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						/*
						lstMODULE = FindControl("BASE_MODULE") as DropDownList;
						if ( lstMODULE != null )
						{
							lstMODULE.SelectedIndexChanged += new EventHandler(lstMODULE_Changed);
							lstMODULE.AutoPostBack = true;
						}
						*/
					}
				}
				else
				{
					string sTitle = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + sTitle);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "BusinessProcesses";
			SetMenu(m_sMODULE);
		}
		#endregion
	}
}

