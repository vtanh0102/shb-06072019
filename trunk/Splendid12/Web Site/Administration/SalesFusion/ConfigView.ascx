<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ConfigView.ascx.cs" Inherits="SplendidCRM.Administration.SalesFusion.ConfigView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2014 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView">
	<%@ Register TagPrefix="SplendidCRM" Tagname="ModuleHeader" Src="~/_controls/ModuleHeader.ascx" %>
	<SplendidCRM:ModuleHeader ID="ctlModuleHeader" Module="SalesFusion" Title="SalesFusion.LBL_SALESFUSION_SETTINGS" EnableModuleLabel="false" EnablePrint="false" EnableHelp="true" Runat="Server" />
	
	<p>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlDynamicButtons" Visible="<%# !PrintView %>" ShowRequired="true" Runat="Server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_ENABLED") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="35%" CssClass="dataField" VerticalAlign="top">
				<asp:CheckBox ID="ENABLED" CssClass="checkbox" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="20%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_VERBOSE_STATUS") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="dataField" VerticalAlign="top">
				<asp:CheckBox ID="VERBOSE_STATUS" CssClass="checkbox" runat="server" />
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_USER_NAME") %>' runat="server" />
				<asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' EnableViewState="False" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="35%" CssClass="dataField" VerticalAlign="top">
				<asp:TextBox ID="USER_NAME" Size="30" Runat="server" />
				<asp:RequiredFieldValidator ID="reqUSER_NAME" ControlToValidate="USER_NAME" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableClientScript="false" EnableViewState="false" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_PASSWORD") %>' runat="server" />
				<asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' EnableViewState="False" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="35%" CssClass="dataField" VerticalAlign="top">
				<asp:TextBox ID="PASSWORD" Size="30" TextMode="Password" Runat="server" />
				<asp:RequiredFieldValidator ID="reqPASSWORD" ControlToValidate="PASSWORD" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableClientScript="false" EnableViewState="false" Runat="server" />
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_DOMAIN") %>' runat="server" />
				<asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' EnableViewState="False" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="35%" CssClass="dataField" VerticalAlign="top">
				<asp:TextBox ID="DOMAIN" Size="30" Runat="server" />
				<asp:RequiredFieldValidator ID="reqDOMAIN" ControlToValidate="DOMAIN" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableClientScript="false" EnableViewState="false" Runat="server" />
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell Width="20%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_DIRECTION") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="dataField" VerticalAlign="top">
				<asp:DropDownList ID="DIRECTION" DataValueField="NAME" DataTextField="DISPLAY_NAME" runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="20%" CssClass="dataLabel" VerticalAlign="top">
				<asp:Label Text='<%# L10n.Term("SalesFusion.LBL_CONFLICT_RESOLUTION") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="dataField" VerticalAlign="top">
				<asp:DropDownList ID="CONFLICT_RESOLUTION" runat="server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	</p>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !PrintView %>" ShowRequired="false" Runat="Server" />
</div>
