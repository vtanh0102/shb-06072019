/**
 * Copyright (C) 2014 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.SalesFusion
{
	/// <summary>
	///		Summary description for ConfigView.
	/// </summary>
	public class ConfigView : SplendidControl
	{
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		private const string sEMPTY_PASSWORD = "**********";
		protected CheckBox     ENABLED               ;
		protected CheckBox     VERBOSE_STATUS        ;
		protected DropDownList DIRECTION             ;
		protected DropDownList CONFLICT_RESOLUTION   ;
		protected TextBox      USER_NAME             ;
		protected TextBox      PASSWORD              ;
		protected TextBox      DOMAIN                ;
		protected RequiredFieldValidator reqUSER_NAME;
		protected RequiredFieldValidator reqPASSWORD ;
		protected RequiredFieldValidator reqDOMAIN   ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "Test" )
			{
				try
				{
					USER_NAME.Text = USER_NAME.Text.Trim();
					PASSWORD .Text = PASSWORD .Text.Trim();
					DOMAIN   .Text = DOMAIN   .Text.Trim();
					
					string sPASSWORD = PASSWORD.Text;
					if ( sPASSWORD == sEMPTY_PASSWORD )
					{
						sPASSWORD = Sql.ToString (Application["CONFIG.SalesFusion.Password"]);
					}
					else
					{
						// 09/13/2013 Paul.  Create separate keys for credit card numbers. 
						Guid gCREDIT_CARD_KEY = Sql.ToGuid(Application["CONFIG.CreditCardKey"]);
						if ( Sql.IsEmptyGuid(gCREDIT_CARD_KEY) )
						{
							gCREDIT_CARD_KEY = Guid.NewGuid();
							SqlProcs.spCONFIG_Update("system", "CreditCardKey", gCREDIT_CARD_KEY.ToString());
							Application["CONFIG.CreditCardKey"] = gCREDIT_CARD_KEY;
						}
						Guid gCREDIT_CARD_IV = Sql.ToGuid(Application["CONFIG.CreditCardIV"]);
						if ( Sql.IsEmptyGuid(gCREDIT_CARD_IV) )
						{
							gCREDIT_CARD_IV = Guid.NewGuid();
							SqlProcs.spCONFIG_Update("system", "CreditCardIV", gCREDIT_CARD_IV.ToString());
							Application["CONFIG.CreditCardIV"] = gCREDIT_CARD_IV;
						}
						string sENCRYPTED_EMAIL_PASSWORD = Security.EncryptPassword(sPASSWORD, gCREDIT_CARD_KEY, gCREDIT_CARD_IV);
						if ( Security.DecryptPassword(sENCRYPTED_EMAIL_PASSWORD, gCREDIT_CARD_KEY, gCREDIT_CARD_IV) != sPASSWORD )
							throw(new Exception("Decryption failed"));
						sPASSWORD = sENCRYPTED_EMAIL_PASSWORD;
					}
					reqUSER_NAME .Enabled = true;
					reqPASSWORD  .Enabled = true;
					reqDOMAIN    .Enabled = true;
					reqUSER_NAME .Validate();
					reqPASSWORD  .Validate();
					reqDOMAIN    .Validate();
					if ( Page.IsValid )
					{
						if ( e.CommandName == "Test" )
						{
							StringBuilder sbErrors = new StringBuilder();
							bool bValid = Spring.Social.SalesFusion.SalesFusionSync.ValidateSalesFusion(Application, USER_NAME.Text, sPASSWORD, DOMAIN.Text, sbErrors);
							if ( bValid )
							{
								ctlDynamicButtons.ErrorText = L10n.Term("SalesFusion.LBL_CONNECTION_SUCCESSFUL");
								PASSWORD.Attributes.Add("value", sEMPTY_PASSWORD);
							}
							else
							{
								ctlDynamicButtons.ErrorText = String.Format(L10n.Term("SalesFusion.ERR_FAILED_TO_CONNECT"), sbErrors.ToString());
							}
						}
						else if ( e.CommandName == "Save" )
						{
							Application["CONFIG.SalesFusion.Direction"         ] = DIRECTION          .SelectedValue;
							Application["CONFIG.SalesFusion.ConflictResolution"] = CONFLICT_RESOLUTION.SelectedValue;
							Application["CONFIG.SalesFusion.Enabled"           ] = ENABLED            .Checked      ;
							Application["CONFIG.SalesFusion.VerboseStatus"     ] = VERBOSE_STATUS     .Checked      ;
							Application["CONFIG.SalesFusion.UserName"          ] = USER_NAME          .Text         ;
							Application["CONFIG.SalesFusion.Password"          ] = sPASSWORD                        ;
							Application["CONFIG.SalesFusion.Domain"            ] = DOMAIN             .Text         ;
							SqlProcs.spCONFIG_Update("system", "SalesFusion.Direction"         , Sql.ToString(Application["CONFIG.SalesFusion.Direction"         ]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.ConflictResolution", Sql.ToString(Application["CONFIG.SalesFusion.ConflictResolution"]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.Enabled"           , Sql.ToString(Application["CONFIG.SalesFusion.Enabled"           ]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.VerboseStatus"     , Sql.ToString(Application["CONFIG.SalesFusion.VerboseStatus"     ]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.UserName"          , Sql.ToString(Application["CONFIG.SalesFusion.UserName"          ]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.Password"          , Sql.ToString(Application["CONFIG.SalesFusion.Password"          ]));
							SqlProcs.spCONFIG_Update("system", "SalesFusion.Domain"            , Sql.ToString(Application["CONFIG.SalesFusion.Domain"            ]));
#if !DEBUG
							SqlProcs.spSCHEDULERS_UpdateStatus("function::pollSalesFusion", ENABLED.Checked ? "Active" : "Inactive");
#endif
							// 07/15/2017 Paul.  Instead of requiring that the user manually enable the user, do so automatically. 
							if ( Sql.ToBoolean(Application["CONFIG.SalesFusion.Enabled"]) )
							{
								Guid gUSER_ID = Spring.Social.SalesFusion.SalesFusionSync.SalesFusionUserID(Application);
								DbProviderFactory dbf = DbProviderFactories.GetFactory();
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL;
									sSQL = "select STATUS       " + ControlChars.CrLf
									     + "  from vwUSERS      " + ControlChars.CrLf
									     + " where ID = @ID     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", gUSER_ID);
										string sSTATUS = Sql.ToString(cmd.ExecuteScalar());
										if ( sSTATUS != "Active" )
										{
											SqlProcs.spUSERS_UpdateStatus(gUSER_ID, "Active");
										}
									}
								}
							}
							Response.Redirect("../default.aspx");
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			/*
			else if ( e.CommandName == "SyncAll" )
			{
				try
				{
					ctlDynamicButtons.ErrorText = L10n.Term("Users.LBL_SYNC_BACKGROUND");
					System.Threading.Thread t = new System.Threading.Thread(SalesFusionSync.SyncAll);
					t.Start(Context);
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			*/
			else if ( e.CommandName == "Cancel" )
			{
				Response.Redirect("../default.aspx");
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term("SalesFusion.LBL_SALESFUSION_SETTINGS"));
			this.Visible = (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
			{
				Parent.DataBind();
				return;
			}

			try
			{
				reqUSER_NAME .DataBind();
				reqPASSWORD  .DataBind();
				reqDOMAIN    .DataBind();
				if ( !IsPostBack )
				{
					ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
					ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
					
					ENABLED         .Checked = Sql.ToBoolean(Application["CONFIG.SalesFusion.Enabled"      ]);
					VERBOSE_STATUS  .Checked = Sql.ToBoolean(Application["CONFIG.SalesFusion.VerboseStatus"]);
					USER_NAME       .Text    = Sql.ToString (Application["CONFIG.SalesFusion.UserName"     ]);
					DOMAIN          .Text    = Sql.ToString (Application["CONFIG.SalesFusion.Domain"       ]);
					string sPASSWORD         = Sql.ToString (Application["CONFIG.SalesFusion.Password"     ]);
					if ( !Sql.IsEmptyString(sPASSWORD) )
					{
						PASSWORD.Attributes.Add("value", sEMPTY_PASSWORD);
					}
					
					DIRECTION.DataSource = SplendidCache.List("salesfusion_sync_direction");
					DIRECTION.DataBind();
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(String.Empty                      ), String.Empty));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.remote"), "remote"    ));
					CONFLICT_RESOLUTION.Items.Add(new ListItem(L10n.Term(".sync_conflict_resolution.local" ), "local"     ));
					
					try
					{
						Utils.SetSelectedValue(DIRECTION, Sql.ToString(Application["CONFIG.SalesFusion.Direction"]));
					}
					catch
					{
					}
					try
					{
						Utils.SetSelectedValue(CONFLICT_RESOLUTION, Sql.ToString(Application["CONFIG.SalesFusion.ConflictResolution"]));
					}
					catch
					{
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "SalesFusion";
			SetAdminMenu(m_sMODULE);
			if ( IsPostBack )
			{
				ctlDynamicButtons.AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + ".ConfigView", Guid.Empty, null);
			}
		}
		#endregion
	}
}
