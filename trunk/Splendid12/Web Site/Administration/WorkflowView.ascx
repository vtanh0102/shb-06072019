<%@ Control CodeBehind="WorkflowView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Administration.WorkflowView" %>
<script runat="server">
/**
 * Copyright (C) 2008-2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */

</script>
<div id="divWorkflowView" visible='<%# 
(  SplendidCRM.Security.AdminUserAccess("Workflows"             , "access") >= 0 
|| SplendidCRM.Security.AdminUserAccess("WorkflowAlertTemplates", "access") >= 0 
|| SplendidCRM.Security.AdminUserAccess("WorkflowEventLog"      , "access") >= 0 
|| SplendidCRM.Security.AdminUserAccess("BusinessRules"         , "access") >= 0 
|| SplendidCRM.Security.AdminUserAccess("BusinessProcesses"     , "access") >= 0 
) %>' runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="ListHeader" Src="~/_controls/ListHeader.ascx" %>
	<SplendidCRM:ListHeader Title="Administration.LBL_WORKFLOW_TITLE" Runat="Server" />
	<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />

	<asp:Table Width="100%" CssClass="tabDetailView2" runat="server">
		<asp:TableRow>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# (SplendidCRM.Security.AdminUserAccess("Workflows", "access") >= 0 || SplendidCRM.Security.AdminUserAccess("WorkflowAlertTemplates", "access") >= 0) %>'>
				<asp:Image SkinID="Workflows" AlternateText='<%# L10n.Term("Administration.LBL_MANAGE_WORKFLOW_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("Administration.LBL_MANAGE_WORKFLOW_TITLE") %>' NavigateUrl="~/Administration/Workflows/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
				<br />
				<div align="center">
				(
				<asp:LinkButton ID="btnProcess" Text='<%# L10n.Term("Workflows.LBL_PROCESS") %>' CommandName="Workflows.Process" OnCommand="Page_Command" CssClass="tabDetailViewDL2Link" Runat="server" />
				&nbsp;
				<asp:Label ID="lblEvents"  runat="server" />,
				<asp:Label ID="lblPending" runat="server" />
				)
				</div>
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2" Visible='<%# (SplendidCRM.Security.AdminUserAccess("Workflows", "access") >= 0 || SplendidCRM.Security.AdminUserAccess("WorkflowAlertTemplates", "access") >= 0) %>'>
				<asp:Label Text='<%# L10n.Term("Administration.LBL_MANAGE_WORKFLOW") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# SplendidCRM.Security.AdminUserAccess("WorkflowEventLog", "access") >= 0 %>'>
				<asp:Image SkinID="WorkflowEventLog" AlternateText='<%# L10n.Term("Administration.LBL_WORKFLOW_EVENT_LOG_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("Administration.LBL_WORKFLOW_EVENT_LOG_TITLE") %>' NavigateUrl="~/Administration/WorkflowEventLog/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2" Visible='<%# SplendidCRM.Security.AdminUserAccess("WorkflowEventLog", "access") >= 0 %>'>
				<asp:Label Text='<%# L10n.Term("Administration.LBL_WORKFLOW_EVENT_LOG") %>' runat="server" />
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# SplendidCRM.Security.AdminUserAccess("BusinessRules", "access") >= 0 %>'>
				<asp:Image SkinID="Rules" AlternateText='<%# L10n.Term("BusinessRules.LBL_BUSINESS_RULES_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("BusinessRules.LBL_BUSINESS_RULES_TITLE") %>' NavigateUrl="~/Administration/BusinessRules/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2">
				<asp:Label Text='<%# L10n.Term("BusinessRules.LBL_BUSINESS_RULES") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# SplendidCRM.Security.AdminUserAccess("WorkflowAlertTemplates", "access") >= 0 %>'>
				<asp:Image SkinID="WorkflowAlertTemplates" AlternateText='<%# L10n.Term("Administration.LBL_WORKFLOW_ALERT_TEMPLATES_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("Administration.LBL_WORKFLOW_ALERT_TEMPLATES_TITLE") %>' NavigateUrl="~/Administration/WorkflowAlertTemplates/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2" Visible='<%# SplendidCRM.Security.AdminUserAccess("WorkflowAlertTemplates", "access") >= 0 %>'>
				<asp:Label Text='<%# L10n.Term("Administration.LBL_WORKFLOW_ALERT_TEMPLATES") %>' runat="server" />
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# SplendidCRM.Security.AdminUserAccess("BusinessProcesses", "access") >= 0 %>'>
				<asp:Image SkinID="BusinessProcesses" AlternateText='<%# L10n.Term("Administration.LBL_MANAGE_WORKFLOW_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("Administration.LBL_MANAGE_BUSINESS_PROCESSES_TITLE") %>' NavigateUrl="~/Administration/BusinessProcesses/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
				<br />
				<div align="center">
				(
				<asp:LinkButton ID="btnBusinessProcess" Text='<%# L10n.Term("Workflows.LBL_PROCESS") %>' CommandName="BusinessProcesses.Process" OnCommand="Page_Command" CssClass="tabDetailViewDL2Link" Runat="server" />
				&nbsp;
				<asp:Label ID="lblBusinessProcessPending" runat="server" />
				)
				</div>
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2" Visible='<%# SplendidCRM.Security.AdminUserAccess("BusinessProcesses", "access") >= 0  %>'>
				<asp:Label Text='<%# L10n.Term("Administration.LBL_MANAGE_BUSINESS_PROCESSES") %>' runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="20%" CssClass="tabDetailViewDL2" Visible='<%# SplendidCRM.Security.AdminUserAccess("BusinessProcesses", "access") >= 0 %>'>
				<asp:Image SkinID="BusinessProcessesLog" AlternateText='<%# L10n.Term("Administration.LBL_WORKFLOW_EVENT_LOG_TITLE") %>' Runat="server" />
				&nbsp;
				<asp:HyperLink Text='<%# L10n.Term("Administration.LBL_BUSINESS_PROCESS_LOG_TITLE") %>' NavigateUrl="~/Administration/BusinessProcessesLog/default.aspx" CssClass="tabDetailViewDL2Link" Runat="server" />
			</asp:TableCell>
			<asp:TableCell Width="30%" CssClass="tabDetailViewDF2" Visible='<%# SplendidCRM.Security.AdminUserAccess("BusinessProcesses", "access") >= 0 %>'>
				<asp:Label Text='<%# L10n.Term("Administration.LBL_BUSINESS_PROCESS_LOG") %>' runat="server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
</div>
