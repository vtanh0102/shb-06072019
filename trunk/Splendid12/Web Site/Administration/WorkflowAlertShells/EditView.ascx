<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.Administration.WorkflowAlertShells.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="SplendidCRM" Tagname="DatePicker" Src="~/_controls/DatePicker.ascx" %>
<script runat="server">
/**
 * Copyright (C) 2008-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divEditView" runat="server">
<asp:UpdatePanel UpdateMode="Conditional" runat="server">
	<ContentTemplate>
		<%-- 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. --%>
		<%-- 03/16/2016 Paul.  HeaderButtons must be inside UpdatePanel in order to display errors. --%>
		<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
		<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="WorkflowAlertShells" Title="WorkflowAlertShells.LBL_MODULE_NAME" EnableModuleLabel="false" EnablePrint="false" EnableHelp="true" Runat="Server" />

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top" Wrap="false"><%# L10n.Term("WorkflowAlertShells.LBL_NAME") %> <asp:Label CssClass="required" Text='<%# L10n.Term(".LBL_REQUIRED_SYMBOL") %>' Runat="server" /></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField">
								<asp:TextBox ID="txtNAME" TabIndex="1" size="60" MaxLength="50" Runat="server" />
								<asp:RequiredFieldValidator ID="reqNAME" ControlToValidate="txtNAME" ErrorMessage='<%# L10n.Term(".ERR_REQUIRED_FIELD") %>' CssClass="required" EnableClientScript="false" EnableViewState="false" Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="15%" CssClass="dataLabel" VerticalAlign="top" Wrap="false"><%# L10n.Term("WorkflowAlertShells.LBL_BASE_MODULE") %></asp:TableCell>
							<asp:TableCell Width="35%" CssClass="dataField">
								<asp:Label ID="txtBASE_MODULE" Runat="server" />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell CssClass="dataLabel"><%= L10n.Term(".LBL_ASSIGNED_TO") %></asp:TableCell>
							<asp:TableCell CssClass="dataField">
								<asp:TextBox ID="ASSIGNED_TO" ReadOnly="True" Runat="server" />
								<asp:HiddenField ID="ASSIGNED_USER_ID" runat="server" />&nbsp;
								<asp:Button      ID="btnChangeAssigned" UseSubmitBehavior="false" OnClientClick=<%# "return ModulePopup('Users', '" + ASSIGNED_USER_ID.ClientID + "', '" + ASSIGNED_TO.ClientID + "', null, false, null);" %> Text='<%# L10n.Term(".LBL_CHANGE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CHANGE_BUTTON_TITLE") %>' CssClass="button" Runat="server" />&nbsp;
								<asp:Button      ID="btnClearAssigned"  UseSubmitBehavior="false" OnClientClick=<%# "return ClearModuleType('Users', '" + ASSIGNED_USER_ID.ClientID + "', '" + ASSIGNED_TO.ClientID + "', false);" %> Text='<%# L10n.Term(".LBL_CLEAR_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CLEAR_BUTTON_TITLE") %>' CssClass="button" Runat="server" />
							</asp:TableCell>
							<asp:TableCell CssClass="dataLabel" VerticalAlign="top"><div id="divTEAM_LABEL" style="DISPLAY: <%= SplendidCRM.Crm.Config.enable_team_management() ? "INLINE" : "NONE" %>"><%= L10n.Term("Teams.LBL_TEAM") %></div></asp:TableCell>
							<asp:TableCell CssClass="dataField" VerticalAlign="top">
								<asp:Panel Visible="<%# SplendidCRM.Crm.Config.enable_team_management() && !SplendidCRM.Crm.Config.enable_dynamic_teams() %>" runat="server">
									<asp:TextBox     ID="TEAM_NAME"     ReadOnly="True" Runat="server" />
									<asp:HiddenField ID="TEAM_ID"       runat="server" />&nbsp;
									<asp:Button      ID="btnChangeTeam" UseSubmitBehavior="false" OnClientClick=<%# "return ModulePopup('Teams', '" + TEAM_ID.ClientID + "', '" + TEAM_NAME.ClientID + "', null, false, null);" %> Text='<%# L10n.Term(".LBL_CHANGE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CHANGE_BUTTON_TITLE") %>' CssClass="button" runat="server" />
								</asp:Panel>
								<%@ Register TagPrefix="SplendidCRM" Tagname="TeamSelect" Src="~/_controls/TeamSelect.ascx" %>
								<SplendidCRM:TeamSelect ID="ctlTeamSelect" Visible="<%# SplendidCRM.Crm.Config.enable_team_management() && SplendidCRM.Crm.Config.enable_dynamic_teams() %>" Runat="Server" />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertShells.LBL_ALERT_TYPE") %></asp:TableCell>
							<asp:TableCell CssClass="dataField">
								<asp:DropDownList ID="lstALERT_TYPE" DataValueField="NAME" DataTextField="DISPLAY_NAME" Runat="server" />
							</asp:TableCell>
							<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertShells.LBL_SOURCE_TYPE") %></asp:TableCell>
							<asp:TableCell CssClass="dataField">
								<asp:DropDownList ID="lstSOURCE_TYPE" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstSOURCE_TYPE_Changed" AutoPostBack="true" Runat="server" />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell CssClass="dataLabel"></asp:TableCell>
							<asp:TableCell CssClass="dataField"></asp:TableCell>
							<asp:TableCell CssClass="dataLabel"></asp:TableCell>
							<asp:TableCell CssClass="dataField"></asp:TableCell>
						</asp:TableRow>
						<asp:TableRow ID="trALERT_TEXT" runat="server">
							<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertShells.LBL_ALERT_TEXT") %></asp:TableCell>
							<asp:TableCell CssClass="dataField" ColumnSpan="3"><asp:TextBox ID="txtALERT_TEXT" TextMode="MultiLine" Rows="4" Columns="80" runat="server" /></asp:TableCell>
						</asp:TableRow>
						<asp:TableRow ID="trCUSTOM_TEMPLATE" runat="server">
							<asp:TableCell CssClass="dataLabel"><%# L10n.Term("WorkflowAlertShells.LBL_CUSTOM_TEMPLATE") %></asp:TableCell>
							<asp:TableCell CssClass="dataField">
								<asp:DropDownList ID="lstCUSTOM_TEMPLATE" DataValueField="ID" DataTextField="NAME" Runat="server" />
							</asp:TableCell>
							<asp:TableCell CssClass="dataLabel"></asp:TableCell>
							<asp:TableCell CssClass="dataField"></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<h4><asp:Label Text='<%# L10n.Term("WorkflowAlertShells.LBL_REPORT_ATTACHMENTS") %>' runat="server" /></h4>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell style="padding-top: 5px; padding-bottom: 5px;">
					<asp:Button ID="btnAttachmentChanged" Text="Attachment Changed" CommandName="ReportAttachments.Insert" OnCommand="Page_Command" style="DISPLAY:none" runat="server" />
					<asp:DataGrid ID="dgReportAttachments" AutoGenerateColumns="false"
						CellPadding="3" CellSpacing="0" 
						AllowPaging="false" AllowSorting="false" ShowHeader="true"
						EnableViewState="true" runat="server">
						<Columns>
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_REPORT_ID"     DataField="REPORT_ID"         ItemStyle-Width="20%" />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_NAME"          DataField="REPORT_NAME"       ItemStyle-Width="35%" />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_PARAMETERS"    DataField="REPORT_PARAMETERS" ItemStyle-Width="30%" />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_RENDER_FORMAT" DataField="RENDER_FORMAT"     ItemStyle-Width="10%" />
							<asp:TemplateColumn HeaderText="" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
								<ItemTemplate>
									<asp:Button ID="btnEditAttachment" CommandName="ReportAttachments.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_EDIT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_EDIT_BUTTON_TITLE") %>' Runat="server" />
									&nbsp;
									<asp:Button ID="btnDeleteAttachment" CommandName="ReportAttachments.Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_TITLE") %>' Runat="server" />
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:DataGrid>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<asp:HiddenField ID="txtREPORT_ATTACHMENT_ID" runat="server" />
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="top" Wrap="false">
								<asp:HiddenField ID="txtREPORT_ID" runat="server" />
								<asp:Label Text='<%# L10n.Term("WorkflowAlertShells.LBL_REPORT_NAME") %>' style="margin-right: 4px;" runat="server" />
								<asp:TextBox ID="txtREPORT_NAME" Enabled="false" runat="server" />&nbsp;
								<asp:Button ID="btnSelectReport" UseSubmitBehavior="false" OnClientClick=<%# "return ModulePopup('Reports', '" + txtREPORT_ID.ClientID + "', '" + txtREPORT_NAME.ClientID + "', null, false, null);" %> CssClass="button" Text='<%# L10n.Term(".LBL_SELECT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_SELECT_BUTTON_TITLE") %>' style="margin-bottom: 4px;" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top" Wrap="false">
								&nbsp;<asp:Label Text='<%# L10n.Term("WorkflowAlertShells.LBL_REPORT_PARAMETERS") %>' style="margin-right: 4px;" runat="server" />
								<asp:TextBox ID="txtREPORT_PARAMETERS" Width="300px" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top" Wrap="false">
								&nbsp;<asp:Label Text='<%# L10n.Term("WorkflowAlertShells.LBL_RENDER_FORMAT") %>' style="margin-right: 4px;" runat="server" />
								<asp:DropDownList ID="lstRENDER_FORMAT" DataValueField="NAME" DataTextField="DISPLAY_NAME" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button ID="btnAttachmentUpdate" CommandName="ReportAttachments.Update" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_UPDATE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_UPDATE_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button ID="btnAttachmentCancel" CommandName="ReportAttachments.Cancel" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="80%"></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<br />

		<asp:Table SkinID="tabForm" runat="server">
			<asp:TableRow>
				<asp:TableCell>
					<h4><asp:Label Text='<%# L10n.Term("WorkflowAlertShells.LBL_ALERT_USER_TYPES") %>' runat="server" /></h4>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<asp:Label Text='<%# L10n.Term("WorkflowActionShells.LBL_SHOW_XOML") %>' CssClass="dataLabel" runat="server" />&nbsp;
					<asp:CheckBox ID="chkSHOW_XOML" CssClass="checkbox" AutoPostBack="true" runat="server" />
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell style="padding-top: 5px; padding-bottom: 5px;">
					<asp:DataGrid ID="dgFilters" AutoGenerateColumns="false"
						CellPadding="3" CellSpacing="0" 
						AllowPaging="false" AllowSorting="false" ShowHeader="true"
						EnableViewState="true" runat="server">
						<Columns>
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_ACTION_TYPE"       DataField="ACTION_TYPE"       />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_RELATIONSHIP_NAME" DataField="RELATIONSHIP_NAME" />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_MODULE_NAME"       DataField="MODULE_NAME"       />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_DATA_FIELD"        DataField="DATA_FIELD"        />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_DATA_TYPE"         DataField="DATA_TYPE"         />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_OPERATOR"          DataField="OPERATOR"          />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_RECIPIENT_NAME"    DataField="RECIPIENT_NAME"    />
							<asp:BoundColumn HeaderText="WorkflowAlertShells.LBL_LIST_SEARCH_TEXT"       DataField="SEARCH_TEXT"       />
							<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
								<ItemTemplate>
									<asp:Button ID="btnEditFilter" CommandName="Filters.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_EDIT_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_EDIT_BUTTON_TITLE") %>' Runat="server" />
									&nbsp;
									<asp:Button ID="btnDeleteFilter" CommandName="Filters.Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>' OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term("Reports.LBL_REMOVE_BUTTON_TITLE") %>' Runat="server" />
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</asp:DataGrid>
				</asp:TableCell>
			</asp:TableRow>
			<asp:TableRow>
				<asp:TableCell>
					<asp:HiddenField ID="txtFILTER_ID" runat="server" />
					<asp:Table SkinID="tabEditView" runat="server">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstRELATED" TabIndex="9" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstRELATED_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblRELATED" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_COLUMN_SOURCE" TabIndex="10" DataValueField="MODULE_NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_SOURCE_Changed" AutoPostBack="true" Runat="server" /><br />
								<asp:Label ID="lblFILTER_COLUMN_SOURCE" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_COLUMN" TabIndex="11" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_COLUMN_Changed" Runat="server" /><br />
								<asp:Label ID="lblFILTER_COLUMN" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:DropDownList ID="lstFILTER_OPERATOR" Enabled="true" TabIndex="12" DataValueField="NAME" DataTextField="DISPLAY_NAME" OnSelectedIndexChanged="lstFILTER_OPERATOR_Changed" Runat="server" /><br />
								<asp:Label ID="lblFILTER_OPERATOR_TYPE" Enabled="false" runat="server" /><asp:Image SkinID="Spacer" Width="4" runat="server" />
								<asp:Label ID="lblFILTER_OPERATOR" runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button ID="btnFilterUpdate" CommandName="Filters.Update" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_UPDATE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_UPDATE_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell VerticalAlign="top">
								<asp:Button ID="btnFilterCancel" CommandName="Filters.Cancel" OnCommand="Page_Command" CssClass="button" Text='<%# L10n.Term(".LBL_CANCEL_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CANCEL_BUTTON_TITLE") %>' Runat="server" />
							</asp:TableCell>
							<asp:TableCell Width="80%"></asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:TableCell>
			</asp:TableRow>
		</asp:Table>

		<asp:Literal ID="litWORKFLOW_XML" runat="server" />
		<asp:Image ImageUrl="Image.aspx" runat="server" />
		<asp:Literal ID="litREPORT_XML" Visible="false" runat="server" />
	</ContentTemplate>
</asp:UpdatePanel>
</div>
