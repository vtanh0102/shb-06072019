/**
 * Copyright (C) 2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Administration.Shippers.QuickBooks
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected _controls.ExportHeader ctlExportHeader;
		protected _controls.SearchView   ctlSearchView  ;
		protected _controls.CheckAll     ctlCheckAll    ;

		protected UniqueStringCollection arrSelectFields;
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected Label         lblError       ;
		protected MassUpdate    ctlMassUpdate  ;
		// 06/06/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected Panel         pnlMassUpdateSeven;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Search" )
				{
					grdMain.CurrentPageIndex = 0;
					grdMain.DataBind();
				}
				else if ( e.CommandName == "SortGrid" )
				{
					grdMain.SetSortFields(e.CommandArgument as string[]);
					arrSelectFields.AddFields(grdMain.SortColumn);
				}
				else if ( e.CommandName == "SelectAll" )
				{
					if ( vwMain == null )
						grdMain.DataBind();
					ctlCheckAll.SelectAll(vwMain, "ID");
					grdMain.DataBind();
				}
				// 06/06/2015 Paul.  Change standard MassUpdate command to a command to toggle visibility. 
				else if ( e.CommandName == "ToggleMassUpdate" )
				{
					pnlMassUpdateSeven.Visible = !pnlMassUpdateSeven.Visible;
				}
				else if ( e.CommandName == "MassDelete" )
				{
					string[] arrID = ctlCheckAll.SelectedItemsArray;
					if ( arrID != null )
					{
						//Response.Redirect("default.aspx");
					}
				}
				else if ( e.CommandName == "Export" )
				{
					int nACLACCESS = ACL_ACCESS.ALL;  // SplendidCRM.Security.GetUserAccess(m_sMODULE, "export");
					if ( nACLACCESS  >= 0 )
					{
						if ( vwMain == null )
							grdMain.DataBind();
						//if ( nACLACCESS == ACL_ACCESS.OWNER )
						//	vwMain.RowFilter = "ASSIGNED_USER_ID = '" + Security.USER_ID.ToString() + "'";
						string[] arrID = ctlCheckAll.SelectedItemsArray;
						SplendidExport.Export(vwMain, m_sMODULE, ctlExportHeader.ExportFormat, ctlExportHeader.ExportRange, grdMain.CurrentPageIndex, grdMain.PageSize, arrID, grdMain.AllowCustomPaging);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			this.Visible = (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.USER_ID == Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]));
			if ( !this.Visible )
				return;

			try
			{
				if ( !IsPostBack )
				{
					// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
					bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
#if DEBUG
					//bQuickBooksEnabled = true;
#endif
					if ( bQuickBooksEnabled )
					{
						// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
						if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
						{
							Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
							string sFilter = String.Empty;
							DataTable dt = Spring.Social.QuickBooks.Api.ShipMethod.ConvertToTable(quickbooks.ShipMethodOperations.GetAll(sFilter, QuickBooksSync.PrimarySortField(Application, m_sMODULE)));
							vwMain = dt.DefaultView;
							grdMain.DataSource = vwMain ;
							ViewState["Main"] = dt;
						}
						else
						{
							QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								DataTable dt = new DataTable();
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
									grdMain.OrderByClause(QuickBooksSync.PrimarySortField(Application, m_sMODULE), "asc");
									
									cmd.CommandText = "select *          " + ControlChars.CrLf
									                + "  from ShipMethods" + ControlChars.CrLf
									                + " where 1 = 1      " + ControlChars.CrLf
									                + grdMain.OrderByClause();
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dt);
										//this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);
										vwMain = dt.DefaultView;
										grdMain.DataSource = vwMain ;
										ViewState["Main"] = dt;
									}
								}
							}
						}
					}
					
					ctlExportHeader.Visible = true;
					//ctlMassUpdate.Visible = ctlExportHeader.Visible && !PrintView && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE);
					//ctlCheckAll  .Visible = ctlMassUpdate.Visible;
				}
				else
				{
					DataTable dt = ViewState["Main"] as DataTable;
					if ( dt != null )
					{
						vwMain = dt.DefaultView;
						grdMain.DataSource = vwMain ;
					}
				}
				if ( !IsPostBack )
				{
					grdMain.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Account is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlSearchView  .Command += new CommandEventHandler(Page_Command);
			ctlExportHeader.Command += new CommandEventHandler(Page_Command);
			ctlMassUpdate  .Command += new CommandEventHandler(Page_Command);
			ctlCheckAll    .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Shippers";
			SetMenu(m_sMODULE);
			arrSelectFields = new UniqueStringCollection();
			this.LayoutListView = "ListView.QuickBooks" + (QuickBooksSync.IsOnlineAppMode(Application) ? "Online" : String.Empty);
			arrSelectFields.Add(QuickBooksSync.PrimarySortField(Application, m_sMODULE));
			this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields);
			//if ( Security.GetUserAccess(m_sMODULE, "delete") < 0 && Security.GetUserAccess(m_sMODULE, "edit") < 0 )
				ctlMassUpdate.Visible = false;
			
			// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
			if ( SplendidDynamic.StackedLayout(Page.Theme) )
			{
				// 06/05/2015 Paul.  Move MassUpdate buttons to the SplendidGrid. 
				grdMain.IsMobile       = this.IsMobile;
				grdMain.MassUpdateView = m_sMODULE + ".MassUpdate";
				grdMain.Command       += new CommandEventHandler(Page_Command);
				if ( !IsPostBack )
					pnlMassUpdateSeven.Visible = false;
			}
		}
		#endregion
	}
}

