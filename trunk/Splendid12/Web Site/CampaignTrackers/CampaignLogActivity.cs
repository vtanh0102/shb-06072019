/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// CampaignLogActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:27 PM
	/// </summary>
	public class CampaignLogActivity: SplendidActivity
	{
		public CampaignLogActivity()
		{
			this.Name = "CampaignLogActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.IDProperty))); }
			set { base.SetValue(CampaignLogActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(CampaignLogActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.CAMPAIGN_IDProperty))); }
			set { base.SetValue(CampaignLogActivity.CAMPAIGN_IDProperty, value); }
		}

		public static DependencyProperty TARGET_TRACKER_KEYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TARGET_TRACKER_KEY", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TARGET_TRACKER_KEY
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.TARGET_TRACKER_KEYProperty))); }
			set { base.SetValue(CampaignLogActivity.TARGET_TRACKER_KEYProperty, value); }
		}

		public static DependencyProperty TARGET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TARGET_ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TARGET_ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.TARGET_IDProperty))); }
			set { base.SetValue(CampaignLogActivity.TARGET_IDProperty, value); }
		}

		public static DependencyProperty TARGET_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TARGET_TYPE", typeof(string), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TARGET_TYPE
		{
			get { return ((string)(base.GetValue(CampaignLogActivity.TARGET_TYPEProperty))); }
			set { base.SetValue(CampaignLogActivity.TARGET_TYPEProperty, value); }
		}

		public static DependencyProperty ACTIVITY_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTIVITY_TYPE", typeof(string), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACTIVITY_TYPE
		{
			get { return ((string)(base.GetValue(CampaignLogActivity.ACTIVITY_TYPEProperty))); }
			set { base.SetValue(CampaignLogActivity.ACTIVITY_TYPEProperty, value); }
		}

		public static DependencyProperty ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTIVITY_DATE", typeof(DateTime), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(CampaignLogActivity.ACTIVITY_DATEProperty))); }
			set { base.SetValue(CampaignLogActivity.ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty RELATED_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RELATED_ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid RELATED_ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.RELATED_IDProperty))); }
			set { base.SetValue(CampaignLogActivity.RELATED_IDProperty, value); }
		}

		public static DependencyProperty RELATED_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RELATED_TYPE", typeof(string), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RELATED_TYPE
		{
			get { return ((string)(base.GetValue(CampaignLogActivity.RELATED_TYPEProperty))); }
			set { base.SetValue(CampaignLogActivity.RELATED_TYPEProperty, value); }
		}

		public static DependencyProperty ARCHIVEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ARCHIVED", typeof(bool), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool ARCHIVED
		{
			get { return ((bool)(base.GetValue(CampaignLogActivity.ARCHIVEDProperty))); }
			set { base.SetValue(CampaignLogActivity.ARCHIVEDProperty, value); }
		}

		public static DependencyProperty HITSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("HITS", typeof(Int32), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 HITS
		{
			get { return ((Int32)(base.GetValue(CampaignLogActivity.HITSProperty))); }
			set { base.SetValue(CampaignLogActivity.HITSProperty, value); }
		}

		public static DependencyProperty LIST_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_ID", typeof(Guid), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LIST_ID
		{
			get { return ((Guid)(base.GetValue(CampaignLogActivity.LIST_IDProperty))); }
			set { base.SetValue(CampaignLogActivity.LIST_IDProperty, value); }
		}

		public static DependencyProperty MORE_INFORMATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MORE_INFORMATION", typeof(string), typeof(CampaignLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MORE_INFORMATION
		{
			get { return ((string)(base.GetValue(CampaignLogActivity.MORE_INFORMATIONProperty))); }
			set { base.SetValue(CampaignLogActivity.MORE_INFORMATIONProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CampaignLogActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CampaignLogActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CAMPAIGN_LOG_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCAMPAIGN_LOG_AUDIT        CAMPAIGN_LOG          " + ControlChars.CrLf
							                + " inner join vwCAMPAIGN_LOG_AUDIT        CAMPAIGN_LOG_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CAMPAIGN_LOG_AUDIT_OLD.ID = CAMPAIGN_LOG.ID       " + ControlChars.CrLf
							                + "        and CAMPAIGN_LOG_AUDIT_OLD.AUDIT_VERSION = (select max(vwCAMPAIGN_LOG_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                      from vwCAMPAIGN_LOG_AUDIT                   " + ControlChars.CrLf
							                + "                                                     where vwCAMPAIGN_LOG_AUDIT.ID            =  CAMPAIGN_LOG.ID           " + ControlChars.CrLf
							                + "                                                       and vwCAMPAIGN_LOG_AUDIT.AUDIT_VERSION <  CAMPAIGN_LOG.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                       and vwCAMPAIGN_LOG_AUDIT.AUDIT_TOKEN   <> CAMPAIGN_LOG.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                   )" + ControlChars.CrLf
							                + " where CAMPAIGN_LOG.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwCAMPAIGN_LOG_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCAMPAIGN_LOG_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwCAMPAIGN_LOG_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								if ( bPast )
									ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								if ( bPast )
									CAMPAIGN_ID                    = Sql.ToGuid    (rdr["CAMPAIGN_ID"                   ]);
								if ( bPast )
									TARGET_TRACKER_KEY             = Sql.ToGuid    (rdr["TARGET_TRACKER_KEY"            ]);
								if ( bPast )
									TARGET_ID                      = Sql.ToGuid    (rdr["TARGET_ID"                     ]);
								if ( bPast )
									TARGET_TYPE                    = Sql.ToString  (rdr["TARGET_TYPE"                   ]);
								if ( bPast )
									ACTIVITY_TYPE                  = Sql.ToString  (rdr["ACTIVITY_TYPE"                 ]);
								if ( bPast )
									ACTIVITY_DATE                  = Sql.ToDateTime(rdr["ACTIVITY_DATE"                 ]);
								if ( bPast )
									RELATED_ID                     = Sql.ToGuid    (rdr["RELATED_ID"                    ]);
								if ( bPast )
									RELATED_TYPE                   = Sql.ToString  (rdr["RELATED_TYPE"                  ]);
								if ( bPast )
									ARCHIVED                       = Sql.ToBoolean (rdr["ARCHIVED"                      ]);
								if ( bPast )
									HITS                           = Sql.ToInteger (rdr["HITS"                          ]);
								if ( bPast )
									LIST_ID                        = Sql.ToGuid    (rdr["LIST_ID"                       ]);
								if ( bPast )
									MORE_INFORMATION               = Sql.ToString  (rdr["MORE_INFORMATION"              ]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CampaignLogActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CAMPAIGN_LOG", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCAMPAIGN_LOG_Update
								( ref gID
								, CAMPAIGN_ID
								, TARGET_TRACKER_KEY
								, TARGET_ID
								, TARGET_TYPE
								, ACTIVITY_TYPE
								, ACTIVITY_DATE
								, RELATED_ID
								, RELATED_TYPE
								, ARCHIVED
								, HITS
								, LIST_ID
								, MORE_INFORMATION
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CampaignLogActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

