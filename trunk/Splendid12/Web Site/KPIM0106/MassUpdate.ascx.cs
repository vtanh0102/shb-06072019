using System;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.KPIM0106
{
    /// <summary>
    ///		Summary description for MassUpdate.
    /// </summary>
    public class MassUpdate : SplendidCRM.MassUpdate
    {
        protected _controls.MassUpdateButtons ctlDynamicButtons;

        public CommandEventHandler Command;
        protected _controls.TeamAssignedMassUpdate ctlTeamAssignedMassUpdate;
        protected _controls.TagMassUpdate ctlTagMassUpdate;

        protected DropDownList ddlREGION;
        protected DropDownList ddlAREA;

        public Guid ASSIGNED_USER_ID
        {
            get
            {
                return ctlTeamAssignedMassUpdate.ASSIGNED_USER;
            }
        }

        public Guid PRIMARY_TEAM_ID
        {
            get
            {
                return ctlTeamAssignedMassUpdate.PRIMARY_TEAM_ID;
            }
        }

        public string TEAM_SET_LIST
        {
            get
            {
                return ctlTeamAssignedMassUpdate.TEAM_SET_LIST;
            }
        }

        public bool ADD_TEAM_SET
        {
            get
            {
                return ctlTeamAssignedMassUpdate.ADD_TEAM_SET;
            }
        }

        public string TAG_SET_NAME
        {
            get
            {
                return ctlTagMassUpdate.TAG_SET_NAME;
            }
        }

        public bool ADD_TAG_SET
        {
            get
            {
                return ctlTagMassUpdate.ADD_TAG_SET;
            }
        }

        public Guid AREA_ID
        {
            get
            {
                return Sql.ToGuid(ddlAREA.SelectedValue);
            }
        }

        public Guid REGION_ID
        {
            get
            {
                return Sql.ToGuid(ddlREGION.SelectedValue);
            }
        }
        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            // Command is handled by the parent. 
            if (Command != null)
                Command(this, e);
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int nACLACCESS_Delete = Security.GetUserAccess(m_sMODULE, "delete");
                    int nACLACCESS_Edit = Security.GetUserAccess(m_sMODULE, "edit");
                    ctlDynamicButtons.ShowButton("MassUpdate", nACLACCESS_Edit >= 0);
                    ctlDynamicButtons.ShowButton("MassDelete", nACLACCESS_Delete >= 0);
                    LoadDropdownList();
                    //20180920 TuanDN: Add
                    //int submitApproval = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_SUBMIT_APPROVAL);
                    //int approval = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_APPROVAL);
                    //int reject = Security.getUserPermission(Security.CURRENT_EMPLOYEE, ACL_ACCESS.BUTTON_REJECT);
                    //ctlDynamicButtons.ShowButton("MassSubmitApproval", submitApproval >= 0);
                    //ctlDynamicButtons.ShowButton("MassApprove", approval >= 0);
                    //ctlDynamicButtons.ShowButton("MassReject", reject >= 0);
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
            }
        }


        private void LoadDropdownList()
        {
            if (ddlAREA != null)
            {
                var dtSource = SplendidCache.M_AREA();
                ddlAREA.Items.Clear();
                ddlAREA.DataSource = dtSource;
                ddlAREA.DataTextField = "AREA_NAME";
                ddlAREA.DataValueField = "ID";
                ddlAREA.DataBind();
                ddlAREA.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
            }
            if (ddlREGION != null)
            {
                var dtSource = SplendidCache.M_REGION();
                ddlREGION.Items.Clear();
                ddlREGION.DataSource = dtSource;
                ddlREGION.DataTextField = "REGION_NAME";
                ddlREGION.DataValueField = "ID";
                ddlREGION.DataBind();
                ddlREGION.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), ""));
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            m_sMODULE = "KPIM0106";
            ctlDynamicButtons.AppendButtons(m_sMODULE + ".MassUpdate", Guid.Empty, null);
        }
        #endregion
    }
}
