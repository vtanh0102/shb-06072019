﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="SixToolbar.ascx.cs" Inherits="SplendidCRM.Themes.Atlantic.SixToolbar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
    /**
 * Copyright (C) 2005-2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
    protected string getHost()
    {
        if (Request.IsLocal)
        {
            return string.Empty;
        }
        else
        {
            return Request.ApplicationPath;
        }
    }
</script>
<div id="divSixToolbar">
    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" border="0" class="SixToolbar">
                <tr>
                   
                  <%if (IsAdminScreenPage()) { %>   
                    <td style="background-color:#fff;text-align:center"  nowrap>
                        <header class="main-header">
                            <!-- Logo -->
                            <a href="<%= Application["rootURL"] %>Home" class="logo">
                                <asp:Image ID="Image1" Style="border-width: 0px; width: 160px;" ImageUrl='~/Include/images/shb_logo_ico_02.jpg' runat="server" />
                            </a>
                        </header>
                        <%@ Register TagPrefix="SplendidCRM" Tagname="TabMenu" Src="TabMenu.ascx" %>
					    <SplendidCRM:TabMenu ID="ctlTabMenu" Visible='<%# !PrintView %>' Runat="Server" />
                    </td>  
                  <%} else {%>
                    <td nowrap>
                        <!--Tungnx: Fix menu-->
                        <div id="divTabMenu" style="margin-left: 220px;">
                            <table id="ctl00_ctlSixToolbar_ctlTabMenu_tblSixMenu" class="tabToolbarFrame" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td class="delimiterTop">&nbsp;</td>
                                        <td class="delimiterTop">&nbsp;</td>
                                        <td valign="bottom" >
                                            <table id="ctl00_ctlSixToolbar_ctlTabMenu_tabMenuInnerHOME" class="tabToolbarFrame" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="otherTabFixed" nowrap="1"><a class="otherTabLink" href="<%= Application["rootURL"] %>Home"><%# L10n.Term("Home.LBL_LIST_FORM_TITLE") %></a><br>
                                                            <a valign="bottom" href="javascript:void(0);" style="display: inline-block; border-width: 0px; height: 4px; width: 100%;">
                                                                <img src="<%= Application["rootURL"] %>Include/images/blank.gif" style="border-width: 0px; height: 4px; width: 100%;"></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td class="delimiterTop">
                                            |
                                        </td>
                                        <td valign="bottom" >
                                            <table id="ctl00_ctlSixToolbar_ctlTabMenu_tabMenuInnerCRM" class="tabToolbarFrame" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="otherTabFixed" nowrap="1"><a class="otherTabLink" href="#">CRM</a><br>
                                                            <a valign="bottom" href="javascript:void(0);" style="display: inline-block; border-width: 0px; height: 4px; width: 100%;">
                                                                <img src="<%= Application["rootURL"] %>Include/images/blank.gif" style="border-width: 0px; height: 4px; width: 100%;"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td class="delimiterTop">
                                            |
                                        </td>
                                        <td valign="bottom" >
                                            <table id="ctl00_ctlSixToolbar_ctlTabMenu_tabMenuInnerKPI" class="tabToolbarFrame" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="otherTabFixed" nowrap="1"><a class="otherTabLink" href="#">KPIs</a><br>
                                                            <a valign="bottom" href="javascript:void(0);" style="display: inline-block; border-width: 0px; height: 4px; width: 100%;">
                                                                <img src="<%= Application["rootURL"] %>Include/images/blank.gif" style="border-width: 0px; height: 4px; width: 100%;"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td class="delimiterTop">
                                            |
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="ctl00_pnlModuleActionsHOME" class="PanelHoverHidden" style="position: absolute; visibility: hidden; left: 233px; top: 47px; z-index: 1000; display: none;">
                        </div>

                        <div id="ctl00_pnlModuleActionsCRM" class="PanelHoverHidden" style="position: absolute; visibility: hidden; left: 401px; top: 47px; z-index: 1000; display: none;">
                        </div>
                        <div id="ctl00_pnlModuleActionsKPI" class="PanelHoverHidden" style="position: absolute; visibility: hidden; left: 556px; top: 47px; z-index: 1000; display: none;">
                            <table cellpadding="0" cellspacing="0" class="ModuleActionsInnerTable1Col">
                                <tbody>
                                    <tr>
                                        <td class="ModuleActionsInnerCell">
                                            <a class="ModuleActionsMenuItems" href="<%= Application["rootURL"] %>Employees/default.aspx"><%# L10n.Term(".MN_EMPLOYEE_INF") %></a>
                                            <a class="ModuleActionsMenuItems" href="<%= Application["rootURL"] %>KPIB0201/default.aspx"><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT") %></a>
                                            <a class="ModuleActionsMenuItems" href="<%= Application["rootURL"] %>KPIB0302/default.aspx"><%# L10n.Term(".MN_RESULTS_MANAGEMENT") %></a>
                                            <a class="ModuleActionsMenuItems" href="<%= Application["rootURL"] %>Rcharts/default.aspx"><%# L10n.Term(".MN_REPORTS") %></a></td>

                                    </tr>
                                </tbody>
                            </table>

                        </div>

                        <script type="text/javascript">
                            //<![CDATA[

                            Sys.Application.add_init(function () {
                                $create(Sys.Extended.UI.HoverMenuBehavior, { "HoverDelay": 500, "OffsetY": 2, "PopDelay": 250, "PopupPosition": 3, "dynamicServicePath": "/Home/default.aspx", "id": "ctl00_ctlSixToolbar_ctlTabMenu_ctl16", "popupElement": $get("ctl00_pnlModuleActionsCRM") }, null, null, $get("ctl00_ctlSixToolbar_ctlTabMenu_tabMenuInnerCRM"));
                            });
                            Sys.Application.add_init(function () {
                                $create(Sys.Extended.UI.HoverMenuBehavior, { "HoverDelay": 500, "OffsetY": 2, "PopDelay": 250, "PopupPosition": 3, "dynamicServicePath": "/Home/default.aspx", "id": "ctl00_ctlSixToolbar_ctlTabMenu_ctl23", "popupElement": $get("ctl00_pnlModuleActionsKPI") }, null, null, $get("ctl00_ctlSixToolbar_ctlTabMenu_tabMenuInnerKPI"));
                            });
                            //]]>
                          </script>

                        <!--Tungnx: End Fix menu-->
                    </td>
                
                    <td align="left" width="70%" valign="middle" >
                        <asp:Panel ID="cntUnifiedSearch" runat="server">
                            <div id="divUnifiedSearch" class="divUnifiedSearch">
                                <script type="text/javascript">
                                    function UnifiedSearch() {
                                        var frm = document.forms[0];
                                        // 01/21/2014 Paul.  Need to escape the query value to allow for symbols in the query. 
                                        var sUrl = '<%= Application["rootURL"] %>Home/UnifiedSearch.aspx?txtUnifiedSearch=' + escape(frm['<%= txtUnifiedSearch.ClientID %>'].value);
                                        window.location.href = sUrl;
                                        return true;
                                    }
                                </script>
                                <nobr>
                                    <form class="form_search" onsubmit="return UnifiedSearch();">
                                        <asp:TextBox ID="txtUnifiedSearch" placeholder="Nhập từ khóa..." CssClass="sb-search-input" Text='<%# Request["txtUnifiedSearch"] %>' runat="server" />
                                        
                                    </form>
								</nobr>
                            </div>
                        </asp:Panel>
                    </td>
                 <%} %>

                    <td width="30%" class="tabRow">
                        <asp:Image SkinID="blank" Width="1" Height="1" runat="server" /></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" border="0" class="<%# L10n.IsLanguageRTL() ? "SixToolbarUserRTL" : "SixToolbarUser" %>">
                <tr>
                    <td valign="bottom" class="otherUserLeftBorder">
                        <table id="tabToolbarUser" class="tabToolbarFrame" cellspacing="0" cellpadding="0" height="100%" runat="server">
                            <tr>
                                <td class="otherUser" nowrap="1">
                                    <span class="otherTabLink" visible="<%# SplendidCRM.Security.IsImpersonating() %>" runat="server"><%# L10n.Term("Users.LBL_IMPERSONATING") %><br />
                                    </span>
                                    <span class="otherTabLink" style="padding-right: 6px;"><%# SplendidCRM.Security.FULL_NAME %></span>
                                    <asp:Image SkinID="more" class="otherTabMoreArrow" runat="server" /><br />
                                    <asp:HyperLink NavigateUrl="javascript:void(0);" valign="bottom" runat="server">
										<asp:Image SkinID="blank" Width="100%" Height="4" BorderWidth="0" runat="server" />
                                    </asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="bottom" class="otherUserLeftBorder" width="32">
                        <table id="tabToolbarQuickCreate" class="tabToolbarFrame" cellspacing="0" cellpadding="0" height="100%" runat="server">
                            <tr>
                                <td class="otherQuickCreate">
                                    <asp:Image SkinID="ToolbarQuickCreate" class="otherTabMoreArrow" runat="server" /><br />
                                    <asp:HyperLink NavigateUrl="javascript:void(0);" valign="bottom" runat="server">
										<asp:Image SkinID="blank" Width="100%" Height="4" BorderWidth="0" runat="server" />
                                    </asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="height: 43px; width: 100%"></div>
            <!-- 05/18/2013 Paul.  Moving the hidden panels outside the table solves a Chrome problem with z-index. -->
            <asp:Panel ID="pnlToolbarUserHover" CssClass="PanelHoverHidden" runat="server">
                <table cellpadding="0" cellspacing="0" class="MoreActionsInnerTable">
                    <tr>
                        <td class="MoreActionsInnerCell">
                            <asp:HyperLink ID="lnkMyAccount" Text='<%# L10n.Term(".LBL_MY_ACCOUNT") %>' NavigateUrl="~/Users/MyAccount.aspx" CssClass="ModuleActionsMenuItems" runat="server" />
                            <!--
                                <asp:HyperLink ID="lnkEmployees" Text='<%# L10n.Term(".LBL_EMPLOYEES" ) %>' NavigateUrl="~/Employees/default.aspx" CssClass="ModuleActionsMenuItems" Visible='<%# !PortalCache.IsPortal() && SplendidCRM.Security.GetUserAccess("Employees", "access") >= 0                              %>' runat="server" />
                            -->
                            <asp:HyperLink ID="lnkAdmin" Text='<%# L10n.Term(".LBL_ADMIN"     ) %>' NavigateUrl="~/Administration/default.aspx" CssClass="ModuleActionsMenuItems" Visible='<%# !PortalCache.IsPortal() && SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.IS_ADMIN_DELEGATE                     %>' runat="server" />
                            <asp:HyperLink ID="lnkTraining" Text='<%# L10n.Term(".LBL_TRAINING"  ) %>' NavigateUrl="~/Home/TrainingPortal.aspx" CssClass="ModuleActionsMenuItems" Visible='<%# !PortalCache.IsPortal() && !Sql.ToBoolean(Application["CONFIG.hide_training"])                                         %>' runat="server" />
                            <asp:HyperLink ID="lnkAbout" Text='<%# L10n.Term(".LNK_ABOUT"     ) %>' NavigateUrl="~/Home/About.aspx" CssClass="ModuleActionsMenuItems" Visible='<%# !PortalCache.IsPortal() && !Sql.ToBoolean(Application["CONFIG.hide_about"])                                         %>' runat="server" />
                            <asp:HyperLink ID="lnkLogout" Text='<%# L10n.Term(".LBL_LOGOUT"    ) %>' NavigateUrl="~/Users/Logout.aspx" CssClass="ModuleActionsMenuItems" Visible='<%# (!SplendidCRM.Security.IsWindowsAuthentication() || SplendidCRM.Security.IsImpersonating()) && SplendidCRM.Security.IsAuthenticated() %>' runat="server" />
                            <asp:LinkButton ID="lnkReload" Text='<%# L10n.Term(".LBL_RELOAD"    ) %>' CommandName="Admin.Reload" OnCommand="Page_Command" CssClass="ModuleActionsMenuItems" Visible='<%# !PortalCache.IsPortal() && bDebug && (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.IS_ADMIN_DELEGATE) %>' runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:HoverMenuExtender TargetControlID="tabToolbarUser" PopupControlID="pnlToolbarUserHover" PopupPosition="Bottom" PopDelay="250" HoverDelay="500" runat="server" />
            <asp:Panel ID="pnlToolbarQuickCreateHover" CssClass="PanelHoverHidden" runat="server">
                <table cellpadding="0" cellspacing="0" class="MoreActionsInnerTable">
                    <tr>
                        <td class="MoreActionsInnerCell">
                            <asp:PlaceHolder ID="plcSubPanel" runat="server" />
                            <asp:HiddenField ID="hidDynamicNewRecord" Value="" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:HoverMenuExtender TargetControlID="tabToolbarQuickCreate" PopupControlID="pnlToolbarQuickCreateHover" PopupPosition="Bottom" PopDelay="250" HoverDelay="500" OffsetX="<%#  L10n.IsLanguageRTL() ? 0 : -160 %>" runat="server" />
            <asp:PlaceHolder ID="plcDynamicNewRecords" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

