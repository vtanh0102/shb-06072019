﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Actions.ascx.cs" Inherits="SplendidCRM.Themes.Sugar.Shortcuts" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery-2.2.3.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery-ui.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery.slimscroll.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/bootstrap.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/app.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery.loadingModal.js"></script>
<script type="text/javascript">
    $(function () {
        $('[id$=ctl00_cntBody_ctlListView_ctlModuleHeader_ctl00_lblTitle]').html('<%=Sql.EscapeJavaScript(L10n.Term(".LBL_LIST_FORM_TITLE"))%>');
        $("input[name='ctl00$cntBody$ctlDetailView$ctlDynamicButtons$ctl00$ctl04']").hide();
        $("[id$=btnMASSSUBMITAPPROVAL]").click(function () {
            if ($('input[name=chkMain]').val() != undefined) {
                if (SelectedCount('chkMain') < 1) {
                    return false;
                }
                var val = $('[id$=ctl00_cntBody_ctlListView_ctlMassUpdate_ctlTagMassApprove_ASSIGNED_TO]').val();
                if (val == null || val == '') {
                    var msg = '<%= string.Format("{0} {1}", Sql.EscapeJavaScript(L10n.Term("Leads.LNK_SELECT_ACCOUNT")), Sql.EscapeJavaScript(L10n.Term("PayTrace.LBL_LIST_APPROVAL")).ToLower())  %>';
                    alert(msg);
                    return false;
                }
            } else {
                var val = $('[id$=ctl00_cntBody_ctlDetailView_ctlMassUpdate_ctlTagMassApprove_ASSIGNED_TO]').val();
                if (val == null || val == '') {
                    var msg = '<%= string.Format("{0} {1}", Sql.EscapeJavaScript(L10n.Term("Leads.LNK_SELECT_ACCOUNT")), Sql.EscapeJavaScript(L10n.Term("PayTrace.LBL_LIST_APPROVAL")).ToLower())  %>';
                    alert(msg);
                    return false;
                }
            }

        });
    });
    function ValidateOne() {
        var result = confirm('<%= Sql.EscapeJavaScript(L10n.Term(".NTC_DELETE_CONFIRMATION")) %>');
        if (!result) {
            return false;
        }
        return true;
    }
    var default__doPostBack;
    default__doPostBack = __doPostBack;
    __doPostBack = function (eventTarget, eventArgument) {
        console.log('eventTarget: ' + eventTarget);
        if (eventTarget.indexOf("ctlDetailView") != -1) {
            var divToPrint = document.getElementById('ctl00_cntBody_ctlDetailView_divDetailView');

            document.getElementsByClassName('button-panel')[0].style.display = 'none';//$(".button-panel").hide();
            document.getElementsByClassName('utilsLink')[0].style.display = 'none';//$(".utilsLink").hide();

            var newWin = window.open('', 'Print-Window');

            var strHeader = "<script>__doPostBack = function (eventTarget, eventArgument) {}</\script>";
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">');
            newWin.document.write(strHeader);
            newWin.document.write(divToPrint.innerHTML);
            newWin.document.write('</body></html>');

            document.getElementsByClassName('button-panel')[0].style.display = '';//$(".button-panel").hide();
            document.getElementsByClassName('utilsLink')[0].style.display = '';
            //newWin.close();
        } else {
            default__doPostBack.call(this, eventTarget, eventArgument);
        }
    }
</script>
<script runat="server">

    const string _active = "active";
    const string _child_active = "child_active";
    private string MySite()
    {
        return Request.Url.AbsolutePath;
    }
    //Employees
    protected string ActiveMenu()
    {
        return (MySite().Contains("Employees")) ? _active : string.Empty;
    }
    //PHAN GIAO CHI TIEU
    protected string ActiveMenu2()
    {
        return (MySite().Contains("KPIB0201")) || (MySite().Contains("KPIB020202")) || (MySite().Contains("KPIB0203")) ? _active : string.Empty;
    }
    //Kết quả thực hiện 
    protected string ActiveMenu3()
    {
        return (MySite().Contains("KPIB030201")) || (MySite().Contains("KPIB0302")) || (MySite().Contains("KPIB030301")) ? _active : string.Empty;
    }
    //BAO CAO
    protected string ActiveMenu4()
    {
        return (MySite().Contains("KPIR010102")) || (MySite().Contains("Rcharts")) ? _active : string.Empty; ;
    }
    //MASSTER DATA
    protected string ActiveMenu5()
    {
        return (MySite().Contains("KPIM02")) || (MySite().Contains("KPIM0101")) || (MySite().Contains("KPIM0102")) || (MySite().Contains("KPIM0106")) || (MySite().Contains("Documents")) ? _active : string.Empty;
    }
    //SYSTEM MANAGEMENT
    protected string ActiveMenu6()
    {
        return (MySite().Equals("CRM001")) ? _active : string.Empty;
    }

    protected string getHost()
    {
        if (Request.IsLocal)
        {
            return string.Empty;
        }
        else
        {
            return Request.ApplicationPath;
        }
    }
    protected string getActiveModule(string module)
    {
        string isActiveModule = string.Empty;
        try
        {
            string mySitePath = Request.ApplicationPath;
            int startLength = mySitePath.Length + 1;
            //isActiveModule = Request.Url.AbsolutePath.Substring(startLength, module.Length) == module ? _active : string.Empty;
            isActiveModule = Request.Url.AbsolutePath.Contains(module) ? _active : string.Empty;
        }
        catch (Exception)
        {

            isActiveModule = string.Empty;
        }
        return isActiveModule;
    }

    protected string getActiveChildItem(string RawUrl)
    {
        return Request.Url.AbsolutePath.EndsWith(RawUrl) ? _child_active : string.Empty;

    }
</script>


<div id="divShortcuts" width="100%" class="skin-blue sidebar-mini lastView" visible='<%# SplendidCRM.Security.IsAuthenticated() && ((!Sql.IsEmptyString(sSubMenu) && SplendidCRM.Security.AdminUserAccess(sSubMenu, "access") >= 0) || !AdminShortcuts || Sql.IsEmptyString(sSubMenu)) %>' runat="server">
    <header class="main-header">
        <!-- Logo -->
        <a href="<%= Application["rootURL"] %>Home" class="logo">
            <asp:Image ID="imgCompanyLogo" Style="border-width: 0px; width: 160px;" ImageUrl='~/Include/images/shb_logo_ico_02.jpg' runat="server" />
        </a>
    </header>
    <div class="wrapper">
        <aside class="main-sidebar">
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section id="nav" class="sidebar" style="height: auto;">
                    <!-- search form -->
                    <ul class="sidebar-menu" id="side-menu">
                        <!--<li class="treeview"><a href="#"><i class="fas fa-tachometer-alt"></i><span>Sales Dashboard</span> </a></li>-->
                        <%-- Employees --%>
                        <% if (SplendidCRM.Security.GetUserAccess("Employees", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu() %>"><a href="#"><i class="fas fa-users" aria-hidden="true"></i><span><%# L10n.Term(".MN_EMPLOYEE_INF") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("Employees") %>">
                                    <% if ((SplendidCRM.Security.GetUserAccess("Employees", "edit") >= 0) || (SplendidCRM.Security.GetUserAccess("Employees", "list") >= 0))
                                       { %>
                                    <a href="#"><i></i><%# L10n.Term(".MN_EMPLOYEE_INF_IM") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("Employees", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/Employees/edit.aspx")  %>"><a href="<%= Application["rootURL"] %>Employees/edit.aspx"><i></i><%# L10n.Term(".MN_EMPLOYEE_INF_IM_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("Employees", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/Employees/default.aspx") %>"><a href="<%= Application["rootURL"] %>Employees/default.aspx"><i></i><%# L10n.Term(".MN_EMPLOYEE_INF_IM_SEARCH") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("Employees", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/Employees/import.aspx") %>"><a href="<%= Application["rootURL"] %>Employees/import.aspx"><i></i><%# L10n.Term(".MN_EMPLOYEE_INF_IM_IMPORT") %></a></li>
                                        <% } %>
                                    </ul>
                                    <% } %>
                                </li>
                            </ul>

                            <ul class="treeview-menu">
                                <% if (SplendidCRM.Security.GetUserAccess("Employees", "list") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/Employees/official.aspx") %>"><a href="<%= Application["rootURL"] %>Employees/official.aspx"><i></i><%# L10n.Term(".MN_EMPLOYEE_INF_LIST") %></a></li>
                                <% } %>
                            </ul>
                        </li>
                        <% } %>

                        <%-- PHAN GIAO CHI TIEU --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIB020202", "access") >= 0
                                || SplendidCRM.Security.GetUserAccess("KPIB0203", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu2() %>">
                            <a href="#"><i class="fas fa-street-view"></i><span><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0201") %>">
                                    <a href="#"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_STD") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0201/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0201/edit.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_STD_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0201/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0201/default.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_STD_SEARCH") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0201/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0201/import.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_STD_GEN") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB020202") %>"><a href="#"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_POS") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/edit.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_POS_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/default.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_POS_SEARCH") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/import.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_POS_IMPORT") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0203") %>"><a href="#"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_EMPLOYEE") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/default.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_EMPLOYEE_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/search.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/search.aspx"><i></i><%# L10n.Term(".LBL_SEARCH") %></a></li>
                                        <% } %>
                                        <%--Menu import    --%>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/import.aspx"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_EMPLOYEE_IMPORT") %></a></li>
                                        <% } %>
                                        <%-- Menu goi y phan giao --%>
                                        <!--<li><a href="#KPIB020401.html"><i></i><%# L10n.Term(".MN_ALLOCATE_MNG_UTILITIES_SUPPORT_SUGGESTION") %></a></li>-->

                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <%--
                            <ul class="treeview-menu">
                                <li><a href="#KPIB020402.html"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_UTILITIES_HISTORY") %></a></li>
                            </ul>

                                         
                            <ul class="treeview-menu">
                                <li class="treeview"><a href="#"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_UTILITIES") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li class="treeview"><a href="#"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_UTILITIES_SUPPORT") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                            <ul class="treeview-menu">
                                                <li><a href="#KPIB020401.html"><i></i><%# L10n.Term(".MN_ALLOCATE_MNG_UTILITIES_SUPPORT_SUGGESTION") %></a></li>
                                                <li><a href="#KPIB020403.html"><i></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_UTILITIES_SUPPORT_SEARCH") %></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#KPIB020402.html"><i class="fas fa-history" aria-hidden="true"></i><%# L10n.Term(".MN_ALLOCATE_MANAGEMENT_UTILITIES_HISTORY") %></a></li>
                                    </ul>
                                </li>
                            </ul>--%>

                        </li>
                        <% } %>

                        <%-- Quản lý kết quả thực hiện --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIB030201", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu3() %>"><a href="#"><i class="fas fa-pie-chart"></i><span><%# L10n.Term(".MN_RESULTS_MANAGEMENT") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0302/")  %> <%# getActiveModule("KPIB030301")  %>"><a href="#"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_EMPLOYEE") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0302/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0302/default.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_EMPLOYEE_LIST") %></a></li>
                                        <% } %>

                                        <%-- <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0302/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0302/import.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_EMPLOYEE_IMPORT") %></a></li>
                                        <% } %>--%>

                                        <%-- Thu thuan --%>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030301", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030301/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030301/default.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_TOI") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030301", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030301/importNetCard.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030301/importNetCard.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_TOI_CARD_IMPORT") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB030201") %>"><a href="#"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_POS") %><span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030201/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030201/default.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_POS_LIST") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030201/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030201/import.aspx"><i></i><%# L10n.Term(".MN_RESULTS_MANAGEMENT_POS_IMPORT") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                        </li>
                        <% } %>

                        <%-- Bao Cao --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIR010102", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIR010105", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIR010105", "access") <= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu4() %>"><a href="#"><i class="fas fa-bar-chart"></i><span><%# L10n.Term(".MN_REPORTS") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <li class="<%# getActiveChildItem("/KPIR010102/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010102/edit.aspx"><i></i><%# L10n.Term(".MN_REPORTS_TABLE") %></a></li>
                                <%--<li class="<%# getActiveChildItem("/KPIR010103/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010103/edit.aspx"><i></i><%# L10n.Term(".MN_REPORTS_TABLE") %></a></li>--%>
                                <li class="<%# getActiveChildItem("/Rcharts/default.aspx") %>"><a href="<%= Application["rootURL"] %>Rcharts/default.aspx"><i></i><%# L10n.Term(".MN_REPORTS_CHART") %></a></li>
                                <%--   <li class="treeview <%# ActiveMenu4() %>"><a href="#"><span><%# L10n.Term(".LBL_M_REPORT_QT") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                                     <ul class="treeview-menu">--%>
                                <li class="<%# getActiveChildItem("/KPIR010103/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010103/edit.aspx"><i></i><%# L10n.Term(".LBL_M_REPORT_KPIR010103") %></a></li>
                                <li class="<%# getActiveChildItem("/KPIR010104/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010104/edit.aspx"><i></i><%# L10n.Term(".LBL_M_REPORT_KPIR010104") %></a></li>
                                <% if (SplendidCRM.Security.GetUserAccess("KPIR010105", "access") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/KPIR010105/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010105/edit.aspx"><i></i><%# L10n.Term(".LBL_M_REPORT_KPIR010105") %></a></li>
                                <li class="<%# getActiveChildItem("/KPIR010106/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIR010106/edit.aspx"><i></i><%# L10n.Term(".LBL_M_REPORT_KPIR010106") %></a></li>
                                <% } %>
                                <%-- </ul>
                                </li>--%>
                                <%--
                                <li class="treeview"><a href="#"><i ></i>Báo cáo kết quả<span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i ></i>Báo cáo biểu đồ</a></li>
                                        <li><a href="#"><i ></i>Xuất báo cáo</a></li>
                                    </ul>
                                </li>
                                <li class="treeview"><a href="#"><i ></i>Báo cáo dư nợ<span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i ></i>Báo cáo biểu đồ</a></li>
                                        <li><a href="#"><i ></i>Xuất báo cáo</a></li>
                                    </ul>
                                </li>--%>
                            </ul>
                        </li>
                        <% } %>

                        <%-- Master data --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIM0101", "access") >= 0
                                || SplendidCRM.Security.GetUserAccess("KPIM0102", "access") >= 0
                                || SplendidCRM.Security.GetUserAccess("KPIM0106", "access") >= 0 || SplendidCRM.Security.GetUserAccess("Documents", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu5() %>"><a href="#"><i class="fas fa-table"></i><span><%# L10n.Term(".MN_MASTER_DATA") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <%-- Danh sach chi tieu --%>
                                <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM02") %>"><a href="#"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI") %><span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM02/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM02/edit.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM02/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM02/default.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI_SEARCH") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                                <%-- Bo KPIs --%>
                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM0101") %>"><a href="#"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI_GROUP") %><span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0101/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0101/edit.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI_GROUP_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0101/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0101/default.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_KPI_GROUP_SEARCH") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                                <%-- ORG --%>
                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0106", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM0106") %>"><a href="#"><i></i><%# L10n.Term(".MN_MASTER_DATA_ORG") %><span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <%--  <% if (SplendidCRM.Security.GetUserAccess("KPIM0106", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0106/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0106/edit.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_ORG_NEW") %></a></li>
                                        <% } %>--%>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0106", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0106/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0106/default.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_ORG_LIST") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                                <%-- Quan ly he so --%>
                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM0102") %>"><a href="#"><i></i><%# L10n.Term(".MN_MASTER_DATA_COEFFICIENT") %><span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0102/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0102/edit.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_COEFFICIENT_NEW") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0102/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0102/default.aspx"><i></i><%# L10n.Term(".MN_MASTER_DATA_COEFFICIENT_LIST") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                                <%-- Quan ly tai lieu --%>
                                <% if (SplendidCRM.Security.GetUserAccess("Documents", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("Documents") %>"><a href="#"><i></i><%# L10n.Term(".MN_MASTER_DATA_DOCUMENT") %><span class="pull-right-container"> <i class="fas fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("Documents", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/Documents/edit.aspx") %>"><a href="<%= Application["rootURL"] %>Documents/edit.aspx"><i></i><%# L10n.Term("Documents.LNK_NEW_DOCUMENT") %></a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("Documents", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/Documents/default.aspx") %>"><a href="<%= Application["rootURL"] %>Documents/default.aspx"><i></i><%# L10n.Term("Documents.LBL_LIST_FORM_TITLE") %></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                            </ul>
                        </li>
                        <% } %>

                        <%-- Quản lý hệ thống --%>
                        <% if (SplendidCRM.Security.GetUserAccess("CRM001", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu6() %>"><a href="#"><i class="fas fa-cog"></i><span><%# L10n.Term(".MN_SYSTEM_MANAGEMENT") %></span> <span class="pull-right-container"><i class="fas fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <% if (SplendidCRM.Security.GetUserAccess("CRM001", "list") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/CRM001/default.aspx") %>"><a href="<%= Application["rootURL"] %>CRM001/default.aspx"><i></i><%# L10n.Term(".MN_SYSTEM_MANAGEMENT_NOTIFI") %></a></li>
                                <% } %>
                                <%-- FAQ
                                <li><a href="#KPIB010301.html"><i></i><%# L10n.Term(".MN_SYSTEM_MANAGEMENT_FAQ") %></a></li>
                                --%>
                            </ul>
                        </li>
                        <% } %>
                    </ul>
                </section>
            </aside>
        </aside>
        <div class="content-wrapper" style="min-height: 100px;"></div>
    </div>
    <!--
    <asp:Repeater id="ctlRepeater" DataSource='<%# SplendidCache.Shortcuts(Sql.ToString(Page.Items["ActiveTabMenu"])) %>' runat="server">
		<ItemTemplate>
			<%-- 09/26/2017 Paul.  Add Archive access right.  --%>
			<div class="lastViewAction" onclick=<%# "window.location.href=\'" + Sql.ToString(Eval("RELATIVE_PATH")).Replace("~/", Sql.ToString(Application["rootURL"])) + "\'" %> style="cursor: pointer;"  Visible='<%# Sql.ToString(Eval("SHORTCUT_ACLTYPE")) != "archive" || Sql.ToBoolean(Application["Modules." + Sql.ToString(Eval("MODULE_NAME")) + ".ArchiveEnabled"]) %>' runat="server">
				<asp:HyperLink NavigateUrl='<%# Eval("RELATIVE_PATH") %>' ToolTip='<%# L10n.Term(Sql.ToString(Eval("DISPLAY_NAME"))) %>' CssClass="lastViewLink" Runat="server">
					<%# L10n.Term(Sql.ToString(Eval("DISPLAY_NAME"))) %>
				</asp:HyperLink>
			</div>
		</ItemTemplate>
	</asp:Repeater>
    -->
</div>

