﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Actions.ascx.cs" Inherits="SplendidCRM.Themes.Sugar.Shortcuts" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery-2.2.3.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery-ui.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/jquery.slimscroll.min.js"></script>
<script src="<%= Application["rootURL"] %>App_Themes/Bootstrap/app.min.js"></script>

<script type="text/javascript">
   
</script>
<script runat="server">

    const string _active = "active";
    private string MySite()
    {
        return Request.Url.AbsolutePath;
    }
    //Employees
    protected string ActiveMenu()
    {
        return (MySite().Contains("Employees")) ? _active : string.Empty;
    }
    //PHAN GIAO CHI TIEU
    protected string ActiveMenu2()
    {
        return (MySite().Contains("KPIB0201")) || (MySite().Contains("KPIB020202")) || (MySite().Contains("KPIB0203")) ? _active : string.Empty;
    }
    //Kết quả thực hiện
    protected string ActiveMenu3()
    {
        return (MySite().Contains("KPIB030201")) || (MySite().Contains("KPIB0302")) ? _active : string.Empty;
    }
    //BAO CAO
    protected string ActiveMenu4()
    {
        return string.Empty;
    }
    //MASSTER DATA
    protected string ActiveMenu5()
    {
        return (MySite().Contains("KPIM02")) || (MySite().Contains("KPIM0101")) || (MySite().Contains("KPIM0102")) ? _active : string.Empty;
    }
    //SYSTEM MANAGEMENT
    protected string ActiveMenu6()
    {
        return (MySite().Equals("CRM001")) ? _active : string.Empty;
    }

    protected string getHost()
    {
        if (Request.IsLocal)
        {
            return string.Empty;
        }
        else
        {
            return Request.ApplicationPath;
        }
    }
    protected string getActiveModule(string module)
    {
        string isActiveModule = string.Empty;
        try
        {
            string mySitePath = Request.ApplicationPath;
            int startLength = mySitePath.Length + 1;
            isActiveModule = Request.Url.AbsolutePath.Substring(startLength, module.Length) == module ? _active : string.Empty;
        }
        catch (Exception)
        {

            isActiveModule = string.Empty;
        }
        return isActiveModule;
    }

    protected string getActiveChildItem(string RawUrl)
    {
        return Request.Url.AbsolutePath.EndsWith(RawUrl) ? _active : string.Empty;

    }
</script>


<div id="divShortcuts" width="100%" class="skin-blue sidebar-mini lastView" visible='<%# SplendidCRM.Security.IsAuthenticated() && ((!Sql.IsEmptyString(sSubMenu) && SplendidCRM.Security.AdminUserAccess(sSubMenu, "access") >= 0) || !AdminShortcuts || Sql.IsEmptyString(sSubMenu)) %>' runat="server">
    <header class="main-header">
        <!-- Logo -->
        <a href="<%= Application["rootURL"] %>Home" class="logo">
            <asp:Image ID="imgCompanyLogo" Style="border-width: 0px; width: 160px;" ImageUrl='~/Include/images/shb_logo_ico_02.jpg' runat="server" />
        </a>
    </header>
    <div class="wrapper">
        <aside class="main-sidebar">
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section id="nav" class="sidebar" style="height: auto;">
                    <!-- search form -->
                    <ul class="sidebar-menu" id="side-menu">
                        <li class="treeview"><a href="#"><i class="fa  fa-dashboard"></i><span>Sales Dashboard</span> </a></li>
                        <%-- Employees --%>
                        <% if (SplendidCRM.Security.GetUserAccess("Employees", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu() %>"><a href="#"><i class="fa fa-users" aria-hidden="true"></i><span>Thông tin nhân sự</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <% if (SplendidCRM.Security.GetUserAccess("Employees", "edit") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/Employees/edit.aspx")  %>"><a href="<%= Application["rootURL"] %>Employees/edit.aspx"><i class="fa fa-circle-o"></i>Thêm mới NS/TTS</a></li>
                                <% } %>
                                <% if (SplendidCRM.Security.GetUserAccess("Employees", "list") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/Employees/default.aspx") %>"><a href="<%= Application["rootURL"] %>Employees/default.aspx"><i class="fa fa-circle-o"></i>Danh sách NS/TTS</a></li>
                                <% } %>
                            </ul>
                        </li>
                        <% } %>

                        <%-- PHAN GIAO CHI TIEU --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIB020202", "access") >= 0
                                || SplendidCRM.Security.GetUserAccess("KPIB0203", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu2() %>">
                            <a href="#"><i class="fa fa-street-view"></i><span>Quản lý phân giao chỉ tiêu</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0201") %>">
                                    <a href="#"><i class="fa fa-circle-o"></i>Quản lý KPIs tối thiểu <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0201/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0201/edit.aspx"><i class="fa fa-circle-o"></i>Thêm mới KPIs</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0201", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0201/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0201/default.aspx"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB020202") %>"><a href="#"><i class="fa fa-circle-o"></i>Phân giao chỉ tiêu ĐVKD <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/edit.aspx"><i class="fa fa-circle-o"></i>Giao chỉ tiêu mới</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/default.aspx"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB020202", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB020202/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB020202/default.aspx"><i class="fa fa-circle-o"></i>Import</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0203") %>"><a href="#"><i class="fa fa-circle-o"></i>Phân giao chỉ tiêu cá nhân <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/import.aspx"><i class="fa fa-circle-o"></i>Upload chỉ tiêu</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/default.aspx"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0203", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0203/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0203/default.aspx"><i class="fa fa-circle-o"></i>Import</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <ul class="treeview-menu">
                                <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Tiện ích <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Hỗ trợ giao chỉ tiêu KPIs <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                            <ul class="treeview-menu">
                                                <li><a href="KPIB020401.html"><i class="fa fa-circle-o"></i>Tạo gợi ý</a></li>
                                                <li><a href="KPIB020403.html"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="KPIB020402.html"><i class="fa fa-history" aria-hidden="true"></i>Lịch sử KPI</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <% } %>

                        <%-- Quản lý kết quả thực hiện --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIB030201", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu3() %>"><a href="#"><i class="fa fa-pie-chart"></i><span>Quản lý kết quả thực hiện</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB0302/") %>"><a href="#"><i class="fa fa-circle-o"></i>Kết quả KPIs NS<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0302/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0302/default.aspx"><i class="fa fa-circle-o"></i>Danh sách kết quả</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB0302", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB0302/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB0302/import.aspx"><i class="fa fa-circle-o"></i>Import kết quả</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>

                            <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "access") >= 0)
                               { %>
                            <ul class="treeview-menu">
                                <li class="treeview <%# getActiveModule("KPIB030201") %>"><a href="#"><i class="fa fa-circle-o"></i>Kết quả KPIs ĐVKD<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030201/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030201/default.aspx"><i class="fa fa-circle-o"></i>Danh sách kết quả</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIB030201", "import") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIB030201/import.aspx") %>"><a href="<%= Application["rootURL"] %>KPIB030201/import.aspx"><i class="fa fa-circle-o"></i>Import kết quả</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                            <% } %>
                        </li>
                        <% } %>

                        <%-- Bao Cao --%>
                        <li class="treeview"><a href="#"><i class="fa fa-bar-chart"></i><span>Báo cáo</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Báo cáo kết quả<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Báo cáo biểu đồ</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Xuất báo cáo</a></li>
                                    </ul>
                                </li>
                                <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Báo cáo dư nợ<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Báo cáo biểu đồ</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Xuất báo cáo</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <%-- Master data --%>
                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "access") >= 0 || SplendidCRM.Security.GetUserAccess("KPIM0101", "access") >= 0
                                || SplendidCRM.Security.GetUserAccess("KPIM0102", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu5() %>"><a href="#"><i class="fa fa-table"></i><span>Master data</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM02") %>"><a href="#"><i class="fa fa-circle-o"></i>Quản lý KPIs<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM02/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM02/edit.aspx"><i class="fa fa-circle-o"></i>Thêm mới</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM02", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM02/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM02/default.aspx"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>

                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM0101") %>"><a href="#"><i class="fa fa-circle-o"></i>Quản lý bộ KPIs<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0101/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0101/edit.aspx"><i class="fa fa-circle-o"></i>Thêm mới</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0101/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0101/default.aspx"><i class="fa fa-circle-o"></i>Tìm kiếm</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>

                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0101", "access") >= 0)
                                   { %>
                                <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i>Quản lý sơ đồ tổ chức<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Thêm mới phòng ban</a></li>
                                        <li><a href="#"><i class="fa fa-circle-o"></i>Danh sách phòng ban</a></li>
                                    </ul>
                                </li>
                                <% } %>

                                <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "access") >= 0)
                                   { %>
                                <li class="treeview <%# getActiveModule("KPIM0102") %>"><a href="#"><i class="fa fa-circle-o"></i>Quản lý hệ số<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span></a>
                                    <ul class="treeview-menu">
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "edit") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0102/edit.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0102/edit.aspx"><i class="fa fa-circle-o"></i>Thêm mới hệ số</a></li>
                                        <% } %>
                                        <% if (SplendidCRM.Security.GetUserAccess("KPIM0102", "list") >= 0)
                                           { %>
                                        <li class="<%# getActiveChildItem("/KPIM0102/default.aspx") %>"><a href="<%= Application["rootURL"] %>KPIM0102/default.aspx"><i class="fa fa-circle-o"></i>Danh sách hệ số</a></li>
                                        <% } %>
                                    </ul>
                                </li>
                                <% } %>
                            </ul>
                        </li>
                        <% } %>

                        <%-- Quản lý hệ thống --%>
                        <% if (SplendidCRM.Security.GetUserAccess("CRM001", "access") >= 0)
                           { %>
                        <li class="treeview <%# ActiveMenu6() %>"><a href="#"><i class="fa fa-cog"></i><span>Quản lý hệ thống</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                            <ul class="treeview-menu">
                                <% if (SplendidCRM.Security.GetUserAccess("CRM001", "list") >= 0)
                                   { %>
                                <li class="<%# getActiveChildItem("/CRM001/default.aspx") %>"><a href="<%= Application["rootURL"] %>CRM001/default.aspx"><i class="fa fa-circle-o"></i>Quản lý thông báo</a></li>
                                <% } %>
                                <li><a href="KPIB010301.html"><i class="fa fa-circle-o"></i>FAQ</a></li>
                            </ul>
                        </li>
                        <% } %>
                    </ul>
                </section>
            </aside>
        </aside>
        <div class="content-wrapper" style="min-height: 100px;"></div>
    </div>
    <!--
    <asp:Repeater id="ctlRepeater" DataSource='<%# SplendidCache.Shortcuts(Sql.ToString(Page.Items["ActiveTabMenu"])) %>' runat="server">
		<ItemTemplate>
			<%-- 09/26/2017 Paul.  Add Archive access right.  --%>
			<div class="lastViewAction" onclick=<%# "window.location.href=\'" + Sql.ToString(Eval("RELATIVE_PATH")).Replace("~/", Sql.ToString(Application["rootURL"])) + "\'" %> style="cursor: pointer;"  Visible='<%# Sql.ToString(Eval("SHORTCUT_ACLTYPE")) != "archive" || Sql.ToBoolean(Application["Modules." + Sql.ToString(Eval("MODULE_NAME")) + ".ArchiveEnabled"]) %>' runat="server">
				<asp:HyperLink NavigateUrl='<%# Eval("RELATIVE_PATH") %>' ToolTip='<%# L10n.Term(Sql.ToString(Eval("DISPLAY_NAME"))) %>' CssClass="lastViewLink" Runat="server">
					<%# L10n.Term(Sql.ToString(Eval("DISPLAY_NAME"))) %>
				</asp:HyperLink>
			</div>
		</ItemTemplate>
	</asp:Repeater>
    -->
</div>

