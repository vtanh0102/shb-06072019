/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// DocumentActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class DocumentActivity: SplendidActivity
	{
		public DocumentActivity()
		{
			this.Name = "DocumentActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.IDProperty))); }
			set { base.SetValue(DocumentActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(DocumentActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty DOCUMENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DOCUMENT_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DOCUMENT_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.DOCUMENT_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.DOCUMENT_NAMEProperty, value); }
		}

		public static DependencyProperty ACTIVE_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTIVE_DATE", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ACTIVE_DATE
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.ACTIVE_DATEProperty))); }
			set { base.SetValue(DocumentActivity.ACTIVE_DATEProperty, value); }
		}

		public static DependencyProperty EXP_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXP_DATE", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime EXP_DATE
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.EXP_DATEProperty))); }
			set { base.SetValue(DocumentActivity.EXP_DATEProperty, value); }
		}

		public static DependencyProperty CATEGORY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CATEGORY_ID", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CATEGORY_ID
		{
			get { return ((string)(base.GetValue(DocumentActivity.CATEGORY_IDProperty))); }
			set { base.SetValue(DocumentActivity.CATEGORY_IDProperty, value); }
		}

		public static DependencyProperty SUBCATEGORY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBCATEGORY_ID", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUBCATEGORY_ID
		{
			get { return ((string)(base.GetValue(DocumentActivity.SUBCATEGORY_IDProperty))); }
			set { base.SetValue(DocumentActivity.SUBCATEGORY_IDProperty, value); }
		}

		public static DependencyProperty STATUS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS_ID", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS_ID
		{
			get { return ((string)(base.GetValue(DocumentActivity.STATUS_IDProperty))); }
			set { base.SetValue(DocumentActivity.STATUS_IDProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(DocumentActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(DocumentActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty MAIL_MERGE_DOCUMENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_MERGE_DOCUMENT", typeof(bool), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool MAIL_MERGE_DOCUMENT
		{
			get { return ((bool)(base.GetValue(DocumentActivity.MAIL_MERGE_DOCUMENTProperty))); }
			set { base.SetValue(DocumentActivity.MAIL_MERGE_DOCUMENTProperty, value); }
		}

		public static DependencyProperty RELATED_DOC_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RELATED_DOC_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid RELATED_DOC_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.RELATED_DOC_IDProperty))); }
			set { base.SetValue(DocumentActivity.RELATED_DOC_IDProperty, value); }
		}

		public static DependencyProperty RELATED_DOC_REV_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RELATED_DOC_REV_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid RELATED_DOC_REV_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.RELATED_DOC_REV_IDProperty))); }
			set { base.SetValue(DocumentActivity.RELATED_DOC_REV_IDProperty, value); }
		}

		public static DependencyProperty IS_TEMPLATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_TEMPLATE", typeof(bool), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_TEMPLATE
		{
			get { return ((bool)(base.GetValue(DocumentActivity.IS_TEMPLATEProperty))); }
			set { base.SetValue(DocumentActivity.IS_TEMPLATEProperty, value); }
		}

		public static DependencyProperty TEMPLATE_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEMPLATE_TYPE", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEMPLATE_TYPE
		{
			get { return ((string)(base.GetValue(DocumentActivity.TEMPLATE_TYPEProperty))); }
			set { base.SetValue(DocumentActivity.TEMPLATE_TYPEProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.TEAM_IDProperty))); }
			set { base.SetValue(DocumentActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(DocumentActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(DocumentActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty PRIMARY_MODULEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_MODULE", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_MODULE
		{
			get { return ((string)(base.GetValue(DocumentActivity.PRIMARY_MODULEProperty))); }
			set { base.SetValue(DocumentActivity.PRIMARY_MODULEProperty, value); }
		}

		public static DependencyProperty SECONDARY_MODULEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SECONDARY_MODULE", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SECONDARY_MODULE
		{
			get { return ((string)(base.GetValue(DocumentActivity.SECONDARY_MODULEProperty))); }
			set { base.SetValue(DocumentActivity.SECONDARY_MODULEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(DocumentActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(DocumentActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(DocumentActivity.CREATED_BYProperty))); }
			set { base.SetValue(DocumentActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(DocumentActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(DocumentActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(DocumentActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(DocumentActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DOCUMENT_REVISION_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DOCUMENT_REVISION_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid DOCUMENT_REVISION_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.DOCUMENT_REVISION_IDProperty))); }
			set { base.SetValue(DocumentActivity.DOCUMENT_REVISION_IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(DocumentActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(DocumentActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.NAMEProperty))); }
			set { base.SetValue(DocumentActivity.NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(DocumentActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty FILE_MIME_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FILE_MIME_TYPE", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FILE_MIME_TYPE
		{
			get { return ((string)(base.GetValue(DocumentActivity.FILE_MIME_TYPEProperty))); }
			set { base.SetValue(DocumentActivity.FILE_MIME_TYPEProperty, value); }
		}

		public static DependencyProperty FILENAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FILENAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FILENAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.FILENAMEProperty))); }
			set { base.SetValue(DocumentActivity.FILENAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(DocumentActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(DocumentActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty REVISIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION
		{
			get { return ((string)(base.GetValue(DocumentActivity.REVISIONProperty))); }
			set { base.SetValue(DocumentActivity.REVISIONProperty, value); }
		}

		public static DependencyProperty REVISION_CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_CREATED_BY", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION_CREATED_BY
		{
			get { return ((string)(base.GetValue(DocumentActivity.REVISION_CREATED_BYProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_CREATED_BYProperty, value); }
		}

		public static DependencyProperty REVISION_CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_CREATED_BY_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION_CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.REVISION_CREATED_BY_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty REVISION_DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_DATE_ENTERED", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime REVISION_DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.REVISION_DATE_ENTEREDProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty REVISION_DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_DATE_MODIFIED", typeof(DateTime), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime REVISION_DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(DocumentActivity.REVISION_DATE_MODIFIEDProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty REVISION_MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_MODIFIED_BY", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION_MODIFIED_BY
		{
			get { return ((string)(base.GetValue(DocumentActivity.REVISION_MODIFIED_BYProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty REVISION_MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION_MODIFIED_BY_NAME", typeof(string), typeof(DocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION_MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(DocumentActivity.REVISION_MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(DocumentActivity.REVISION_MODIFIED_BY_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("DocumentActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("DocumentActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select DOCUMENTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwDOCUMENTS_AUDIT        DOCUMENTS          " + ControlChars.CrLf
							                + " inner join vwDOCUMENTS_AUDIT        DOCUMENTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on DOCUMENTS_AUDIT_OLD.ID = DOCUMENTS.ID       " + ControlChars.CrLf
							                + "        and DOCUMENTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwDOCUMENTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                   from vwDOCUMENTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                  where vwDOCUMENTS_AUDIT.ID            =  DOCUMENTS.ID           " + ControlChars.CrLf
							                + "                                                    and vwDOCUMENTS_AUDIT.AUDIT_VERSION <  DOCUMENTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                    and vwDOCUMENTS_AUDIT.AUDIT_TOKEN   <> DOCUMENTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                )" + ControlChars.CrLf
							                + " where DOCUMENTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwDOCUMENTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwDOCUMENTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwDOCUMENTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								DOCUMENT_NAME                  = Sql.ToString  (rdr["DOCUMENT_NAME"                 ]);
								ACTIVE_DATE                    = Sql.ToDateTime(rdr["ACTIVE_DATE"                   ]);
								EXP_DATE                       = Sql.ToDateTime(rdr["EXP_DATE"                      ]);
								CATEGORY_ID                    = Sql.ToString  (rdr["CATEGORY_ID"                   ]);
								SUBCATEGORY_ID                 = Sql.ToString  (rdr["SUBCATEGORY_ID"                ]);
								STATUS_ID                      = Sql.ToString  (rdr["STATUS_ID"                     ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								MAIL_MERGE_DOCUMENT            = Sql.ToBoolean (rdr["MAIL_MERGE_DOCUMENT"           ]);
								RELATED_DOC_ID                 = Sql.ToGuid    (rdr["RELATED_DOC_ID"                ]);
								RELATED_DOC_REV_ID             = Sql.ToGuid    (rdr["RELATED_DOC_REV_ID"            ]);
								IS_TEMPLATE                    = Sql.ToBoolean (rdr["IS_TEMPLATE"                   ]);
								TEMPLATE_TYPE                  = Sql.ToString  (rdr["TEMPLATE_TYPE"                 ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								PRIMARY_MODULE                 = Sql.ToString  (rdr["PRIMARY_MODULE"                ]);
								SECONDARY_MODULE               = Sql.ToString  (rdr["SECONDARY_MODULE"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DOCUMENT_REVISION_ID           = Sql.ToGuid    (rdr["DOCUMENT_REVISION_ID"          ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									FILE_MIME_TYPE                 = Sql.ToString  (rdr["FILE_MIME_TYPE"                ]);
									FILENAME                       = Sql.ToString  (rdr["FILENAME"                      ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									REVISION                       = Sql.ToString  (rdr["REVISION"                      ]);
									REVISION_CREATED_BY            = Sql.ToString  (rdr["REVISION_CREATED_BY"           ]);
									REVISION_CREATED_BY_NAME       = Sql.ToString  (rdr["REVISION_CREATED_BY_NAME"      ]);
									REVISION_DATE_ENTERED          = Sql.ToDateTime(rdr["REVISION_DATE_ENTERED"         ]);
									REVISION_DATE_MODIFIED         = Sql.ToDateTime(rdr["REVISION_DATE_MODIFIED"        ]);
									REVISION_MODIFIED_BY           = Sql.ToString  (rdr["REVISION_MODIFIED_BY"          ]);
									REVISION_MODIFIED_BY_NAME      = Sql.ToString  (rdr["REVISION_MODIFIED_BY_NAME"     ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("DocumentActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("DOCUMENTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spDOCUMENTS_Update
								( ref gID
								, DOCUMENT_NAME
								, ACTIVE_DATE
								, EXP_DATE
								, CATEGORY_ID
								, SUBCATEGORY_ID
								, STATUS_ID
								, DESCRIPTION
								, MAIL_MERGE_DOCUMENT
								, RELATED_DOC_ID
								, RELATED_DOC_REV_ID
								, IS_TEMPLATE
								, TEMPLATE_TYPE
								, TEAM_ID
								, TEAM_SET_LIST
								, PRIMARY_MODULE
								, SECONDARY_MODULE
								, ASSIGNED_USER_ID
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("DocumentActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

