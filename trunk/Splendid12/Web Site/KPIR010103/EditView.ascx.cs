﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using Microsoft.Reporting.WebForms;
using System.Web.Configuration;
using System.Globalization;
//using Oracle.ManagedDataAccess.Client;
//using Oracle.ManagedDataAccess.Types;

namespace SplendidCRM.KPIR010103
{

	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected Guid            gID                          ;
		protected HtmlTable       tblMain                      ;
		protected PlaceHolder     plcSubPanel                  ;
        protected ReportViewer rpView;
        protected Label lblError;
        // Khai báo dropdowlist
        protected DropDownList ddlREPORT_TYPE;
        protected DropDownList ddlREPORT;
        protected DropDownList ddlCN_VUNG;

        // Khai báo DatePicker

        protected TextBox dtpFROMDATE;
        protected TextBox dtpTODATE;

        protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( plcSubPanel.Visible )
					{
						foreach ( Control ctl in plcSubPanel.Controls )
						{
							InlineEditControl ctlSubPanel = ctl as InlineEditControl;
							if ( ctlSubPanel != null )
							{
								ctlSubPanel.ValidateEditViewFields();
							}
						}
					}
					if ( Page.IsValid )
					{
						string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
						DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							DataRow   rowCurrent = null;
							DataTable dtCurrent  = new DataTable();
							if ( !Sql.IsEmptyGuid(gID) )
							{
								string sSQL ;
								sSQL = "select *           " + ControlChars.CrLf
								     + "  from vwKPIR010103_Edit" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Security.Filter(cmd, m_sMODULE, "edit");
									Sql.AppendParameter(cmd, gID, "ID", false);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											rowCurrent = dtCurrent.Rows[0];
											DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
											if ( Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"])  && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED )
											{
												ctlDynamicButtons.ShowButton("SaveConcurrency", true);
												ctlFooterButtons .ShowButton("SaveConcurrency", true);
												throw(new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
											}
										}
										else
										{
											gID = Guid.Empty;
										}
									}
								}
							}

							this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
							bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
							if ( bDUPLICATE_CHECHING_ENABLED )
							{
								if ( Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0 )
								{
									ctlDynamicButtons.ShowButton("SaveDuplicate", true);
									ctlFooterButtons .ShowButton("SaveDuplicate", true);
									throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
								}
							}
							
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
									if ( Sql.IsEmptyGuid(gASSIGNED_USER_ID) )
										gASSIGNED_USER_ID = Security.USER_ID;
									Guid gTEAM_ID          = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID"         ).ID;
									if ( Sql.IsEmptyGuid(gTEAM_ID) )
										gTEAM_ID = Security.TEAM_ID;
									SqlProcs.spKPIR010103_Update
										( ref gID
										, gASSIGNED_USER_ID
										, gTEAM_ID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST"                      ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME"                               ).Text
										, new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME"                       ).Text
										, trn
										);

									SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
									SplendidCRM.SqlProcs.spTRACKER_Update
										( Security.USER_ID
										, m_sMODULE
										, gID
										, new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
										, "save"
										, trn
										);
									if ( plcSubPanel.Visible )
									{
										foreach ( Control ctl in plcSubPanel.Controls )
										{
											InlineEditControl ctlSubPanel = ctl as InlineEditControl;
											if ( ctlSubPanel != null )
											{
												ctlSubPanel.Save(gID, m_sMODULE, trn);
											}
										}
									}
									trn.Commit();
									SplendidCache.ClearFavorites();
								}
								catch(Exception ex)
								{
									trn.Rollback();
									SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									ctlDynamicButtons.ErrorText = ex.Message;
									return;
								}
							}
							rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
							this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
						}
						
						if ( !Sql.IsEmptyString(RulesRedirectURL) )
							Response.Redirect(RulesRedirectURL);
						else
							Response.Redirect("view.aspx?ID=" + gID.ToString());
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyGuid(gID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?ID=" + gID.ToString());
			}
            else if (e.CommandName == "Search")
            {
                ddlREPORT_TYPE = this.FindControl("NAME") as DropDownList;
                rpView = this.FindControl("ReportViewer1") as ReportViewer;
                var FROMDATE = new SplendidCRM.DynamicControl(this, "DATE_ENTERED").DateValue;
                var TODATE = new SplendidCRM.DynamicControl(this, "DATE_MODIFIED").DateValue;
                if (FROMDATE.ToString() == "01/01/0001 12:00 SA" || TODATE.ToString() == "01/01/0001 12:00 SA")
                {
                    lblError.Text = "* is required.";
                    return;

                }

                ddlREPORT = this.FindControl("CREATED_BY") as DropDownList;
                //dtpFROMDATE = this.FindControl("DATE_ENTERED") as TextBox;
                //dtpTODATE = this.FindControl("DATE_MODIFIED_txtDATE") as TextBox;
                if (ddlREPORT_TYPE.SelectedValue == "1") // Quy mô dư nợ
                {
                    this.rpView.Reset();
                    if (ddlREPORT.SelectedValue == "2")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMDN_CN.rdlc");
                    }
                    else if (ddlREPORT.SelectedValue == "3")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMDN_VUNG.rdlc");
                    }
                    else 
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMDN.rdlc");
                    }
                   
                    DataTable dt = getDataQMDN(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));
              
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "2") // Quy mô huy động
                {
                    this.rpView.Reset();
                    if (ddlREPORT.SelectedValue == "2")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMHD_CN.rdlc");
                    }
                    else if (ddlREPORT.SelectedValue == "3")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMHD_VUNG.rdlc");
                    }
                    else
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_QMHD.rdlc");
                    }
                  
                    DataTable dt = getDataQMHD(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "3") // NQH
                {
                    this.rpView.Reset();

                    this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_NQH.rdlc");
                    DataTable dt = getDataNQH(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "4") // Nợ xấu
                {
                    this.rpView.Reset();
                    this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_NX.rdlc");
                    DataTable dt = getDataNX(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "5") // Số lượng KH vay vốn mới
                {
                    this.rpView.Reset();
                    if (ddlREPORT.SelectedValue == "2")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_SLKHVV_CN.rdlc");
                    }
                    else if (ddlREPORT.SelectedValue == "3")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_SLKHVV_VUNG.rdlc");
                    }
                    else
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_SLKHVV.rdlc");
                    }
                
                    DataTable dt = getDataSLKHVV(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "6") // Thẻ
                {
                    this.rpView.Reset();
                    if (ddlREPORT.SelectedValue == "2")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_THE_CN.rdlc");
                    }
                    else if (ddlREPORT.SelectedValue == "3")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_THE_VUNG.rdlc");
                    }
                    else
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_THE.rdlc");
                    }
                   
                    DataTable dt = getDataTHE(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }
                if (ddlREPORT_TYPE.SelectedValue == "7") // NHĐT
                {
                    this.rpView.Reset();
                    if (ddlREPORT.SelectedValue == "2")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_NHDT_CN.rdlc");
                    }
                    else if (ddlREPORT.SelectedValue == "3")
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_NHDT_VUNG.rdlc");
                    }
                    else
                    {
                        this.rpView.LocalReport.ReportPath = Server.MapPath("~/ReportView/KPIR010103_NHDT.rdlc");
                    }

                 
                    DataTable dt = getDataNHDT(FROMDATE, TODATE, ddlREPORT.SelectedValue);
                    DateTime dtExport = DateTime.Now;
                    dt.Columns.Add("DATE_EXPORT", typeof(System.String));
                    dt.Columns.Add("USER_EXPORT", typeof(System.String));
                    dt.Columns.Add("STT", typeof(System.String));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["DATE_EXPORT"] = dtExport;
                        dt.Rows[i]["USER_EXPORT"] = Security.FULL_NAME;
                        dt.Rows[i]["STT"] = i + 1;
                        dt.Rows[i].EndEdit();
                        dt.AcceptChanges();
                    }
                    ReportDataSource rds = new ReportDataSource("KPIR010103Dataset", dt);
                    this.rpView.LocalReport.DataSources.Clear();
                    this.rpView.LocalReport.DataSources.Add(rds);
                    this.rpView.DataBind();
                    this.rpView.LocalReport.Refresh();
                }


            }
		}
        private DataTable getDataQMDN(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}
          
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                    
                    // Cấp toàn hàng
                    if (REPORT == "1")
                    {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                            + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                             + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf

                   + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
                   + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                     + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                             + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                              + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                               + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                            //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                        + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                            {
                                if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                                {
                                    sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                                 + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf

                       + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
                       + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                         + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                                 + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                  + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                   + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                   + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf

                                //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                            + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                                }
                            }
                    }
                    // Cấp vùng
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                            + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                             + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf

                   + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
                   + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                     + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                             + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                              + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                               + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                            //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                        + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                       + ", POS_CD" + ControlChars.CrLf
                        + ", POS_NAME" + ControlChars.CrLf

             + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf
             + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
             + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
               + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                       + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                       
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

              + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf
              + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
              + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }

                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

              + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf
              + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
              + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
            /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                         if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


                }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataQMHD(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                    
                    
                    if (REPORT == "1")
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                         + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                            + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
                            + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
                            + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                              + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                                      + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                       + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                        + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                                     //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                                 + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                            {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                                             + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
                                             + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
                                             + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                                               + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                                                       + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                                               + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf
                                    //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                                                  + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            }
                          
                            
                        }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                        + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                           + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
                           + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
                           + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                             + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                                     + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                      + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                       + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                                    //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                                + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                       + ", POS_CD" + ControlChars.CrLf
                        + ", POS_NAME" + ControlChars.CrLf

             + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
              + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
              + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                       + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                       
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

            + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
               + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
               + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                 + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf


            + ", SUM(HUY_DONG_CUOIKY) AS HUY_DONG_CUOIKY" + ControlChars.CrLf
               + ", SUM(HUY_DONG_BQ) AS HUY_DONG_BQ" + ControlChars.CrLf
               + ", SUM(HUY_DONG_DAUKY)AS HUY_DONG_DAUKY" + ControlChars.CrLf
                 + ", SUM(HUY_DONG_TANG_RONG) AS HUY_DONG_TANG_RONG" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                               
                        //            Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);
                              
                        //    }

                        //}
                       

                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataNQH(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                   

                    if (REPORT == "1")
                    {
                                    sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                   + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                          + ", SUM(NO_QH_CUOIKY) AS NO_QH_CUOIKY" + ControlChars.CrLf
                          + ", SUM(TY_LE_NO_QH_CUOIKY) AS TY_LE_NO_QH_CUOIKY" + ControlChars.CrLf
                          + ", SUM(NO_QH_DAUKY)AS NO_QH_DAUKY" + ControlChars.CrLf
                            + ", SUM(TY_LE_NO_QH_DAUKY) AS TY_LE_NO_QH_DAUKY" + ControlChars.CrLf

                                    + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                     + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                      + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                                   //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                               + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                                    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                                    {
                                        if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                                        {
                                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                              + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                                     + ", SUM(NO_QH_CUOIKY) AS NO_QH_CUOIKY" + ControlChars.CrLf
                                     + ", SUM(TY_LE_NO_QH_CUOIKY) AS TY_LE_NO_QH_CUOIKY" + ControlChars.CrLf
                                     + ", SUM(NO_QH_DAUKY)AS NO_QH_DAUKY" + ControlChars.CrLf
                                       + ", SUM(TY_LE_NO_QH_DAUKY) AS TY_LE_NO_QH_DAUKY" + ControlChars.CrLf

                                               + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                                + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                                 + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                                  + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf

                                              //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                                          + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                                        }
                                    }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                 + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                        + ", SUM(NO_QH_CUOIKY) AS NO_QH_CUOIKY" + ControlChars.CrLf
                        + ", SUM(TY_LE_NO_QH_CUOIKY) AS TY_LE_NO_QH_CUOIKY" + ControlChars.CrLf
                        + ", SUM(NO_QH_DAUKY)AS NO_QH_DAUKY" + ControlChars.CrLf
                          + ", SUM(TY_LE_NO_QH_DAUKY) AS TY_LE_NO_QH_DAUKY" + ControlChars.CrLf

                                  + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                   + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                    + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                                 //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                             + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                       + ", POS_CD" + ControlChars.CrLf
                        + ", POS_NAME" + ControlChars.CrLf

           + ", SUM(NO_QH_CUOIKY) AS NO_QH_CUOIKY" + ControlChars.CrLf
              + ", SUM(TY_LE_NO_QH_CUOIKY) AS TY_LE_NO_QH_CUOIKY" + ControlChars.CrLf
              + ", SUM(NO_QH_DAUKY)AS NO_QH_DAUKY" + ControlChars.CrLf
                + ", SUM(TY_LE_NO_QH_DAUKY) AS TY_LE_NO_QH_DAUKY" + ControlChars.CrLf

                       + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                       
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

              + ", SUM(NO_QH_CUOIKY) AS NO_QH_CUOIKY" + ControlChars.CrLf
               + ", SUM(TY_LE_NO_QH_CUOIKY) AS TY_LE_NO_QH_CUOIKY" + ControlChars.CrLf
               + ", SUM(NO_QH_DAUKY)AS NO_QH_DAUKY" + ControlChars.CrLf
                 + ", SUM(TY_LE_NO_QH_DAUKY) AS TY_LE_NO_QH_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

              + ", SUM(DU_NO_CUOIKY) AS DU_NO_CUOIKY" + ControlChars.CrLf
              + ", SUM(DU_NO_BQ) AS DU_NO_BQ" + ControlChars.CrLf
              + ", SUM(DU_NO_DAUKY)AS DU_NO_DAUKY" + ControlChars.CrLf
                + ", SUM(DU_NO_TANG_RONG) AS DU_NO_TANG_RONG" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataNX(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                
                    if (REPORT == "1")
                    {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                        + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

               + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                 + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                         + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                          + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                           + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                    + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                            {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                     + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                            + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
                            + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
                            + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                              + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                                      + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                       + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                        + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                      + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf
                                     //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                                 + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            }
                        }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

              + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
              + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
              + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                   + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else 
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                       + ", POS_CD" + ControlChars.CrLf
                        + ", POS_NAME" + ControlChars.CrLf

               + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
              + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
              + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                       + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                       
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

               + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                 + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

                 + ", SUM(NO_XAU_CUOIKY) AS NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(TY_LE_NO_XAU_CUOIKY) AS TY_LE_NO_XAU_CUOIKY" + ControlChars.CrLf
               + ", SUM(NO_XAU_DAUKY)AS NO_XAU_DAUKY" + ControlChars.CrLf
                 + ", SUM(TY_LE_NO_XAU_DAUKY) AS TY_LE_NO_XAU_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataSLKHVV(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                 

                    if (REPORT == "1")
                    {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                              + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                     + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                               + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                 + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                              //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                          + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                            {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                              + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                     + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                               + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                 + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                      + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf
                              //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                          + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            }
                        }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                            + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

                   + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                             + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                              + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                               + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                            //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                        + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                    + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                     + ", POS_CD" + ControlChars.CrLf
                      + ", POS_NAME" + ControlChars.CrLf

              + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                     + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                      + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                       + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                     //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                     
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

                            + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

                 + ", SUM(SL_KH_VV_MOI) AS SL_KH_VV_MOI" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataTHE(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                   

                    if (REPORT == "1")
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                        + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

               + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
               + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
               + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf


                         + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                          + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                           + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                    + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                            {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                        + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

               + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
               + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
               + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf


                         + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                          + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                           + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                      + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                    + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            }
                        }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf

              + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
              + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
              + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf


                        + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                   + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                      + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                       + ", POS_CD" + ControlChars.CrLf
                        + ", POS_NAME" + ControlChars.CrLf

                           + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
              + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
              + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf

                       + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                        + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                         + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                       //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                       
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf


                            + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
               + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
               + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

               + ", SUM(SL_THE_NOIDIA) AS SL_THE_NOIDIA" + ControlChars.CrLf
               + ", SUM(SL_THE_GHINO_QT) AS SL_THE_GHINO_QT" + ControlChars.CrLf
               + ", SUM(SL_THE_TD_QT)AS DU_NO_DAUKY" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private DataTable getDataNHDT(DateTime FROMDATE, DateTime TODATE, string REPORT)
        {
            //string ConStrOracle = WebConfigurationManager.ConnectionStrings["OracleSHBCore"].ConnectionString;
            //OracleDataReader dr = null;
            DataTable dt = new DataTable();
            try
            {
                //using (OracleConnection conn = new OracleConnection(ConStrOracle))
                //{

                //    string sql = string.Format(@"SELECT * FROM ITSHBHO.CSL_STAFF_COLL_VW WHERE ROWNUM < 20");
                //    OracleCommand cmd = new OracleCommand();
                //    cmd.Connection = conn;
                //    cmd.CommandText = sql;
                //    cmd.CommandType = CommandType.Text;
                //    cmd.CommandTimeout = 600;
                //    conn.Open();

                //    dr = cmd.ExecuteReader();

                //    dt.Load(dr);
                //    conn.Close();
                //}

                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    string sSQL = "";
                  

                    if (REPORT == "1")
                    {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                               + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                                + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
                      + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
                      + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf

                                + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                 + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                  + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                               //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                           + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        {
                            if (!("TAT".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE) || "".Equals(Security.CURRENT_EMPLOYEE.BUSINESS_CODE)))
                            {
                                sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                                + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                                 + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
                       + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
                       + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf

                                 + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH" + ControlChars.CrLf
                                  + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                   + "  AND TODATE <= @TODATE" + ControlChars.CrLf
                                      + " AND ( (LIMIT_DATA_BUSINESS_CODE LIKE '%" + Security.CURRENT_EMPLOYEE.BUSINESS_CODE + "%') OR (LIMIT_DATA_BUSINESS_CODE = 'TAT') OR (LIMIT_DATA_BUSINESS_CODE = '') OR (LIMIT_DATA_BUSINESS_CODE = 'PGD') )" + ControlChars.CrLf
                                //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                            + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                            }
                        }
                    }
                    if (REPORT == "3")
                    {
                        if (string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                              + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                               + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
                     + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
                     + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf

                               + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                                + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                                 + "  AND TODATE <= @TODATE" + ControlChars.CrLf

                              //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf
                          + " GROUP BY REGION_NAME" + ControlChars.CrLf);
                        }
                        else
                        {
                            sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                     + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                      + ", POS_CD" + ControlChars.CrLf
                       + ", POS_NAME" + ControlChars.CrLf

                           + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
             + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
             + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf
                      + "  from vwBAOCAO_KETQUA_THKPI_CN" + ControlChars.CrLf
                       + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                        + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                      //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                            sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                        }
                      
                    }
                    else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

                             + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
               + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
               + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);

                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@" AND MAIN_POS_CODE = @MAIN_POS_CODE" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    }
                    else if (REPORT == "2" && string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                    {
                        sSQL = string.Format(@"select REGION_NAME" + ControlChars.CrLf
                       + ", COUNT(REGION_ID) AS REGION_COUNT" + ControlChars.CrLf
                         + ", POS_CD" + ControlChars.CrLf
                         + ", POS_NAME" + ControlChars.CrLf

                             + ", SUM(SL_KH_EBANK) AS SL_KH_EBANK" + ControlChars.CrLf
               + ", SUM(SL_KH_MOBILE) AS SL_KH_MOBILE" + ControlChars.CrLf
               + ", SUM(SL_KH_SMS) AS SL_KH_SMS" + ControlChars.CrLf

                        + "  from vwBAOCAO_KETQUA_THKPI_KHCN_DVKH_PGD" + ControlChars.CrLf
                         + "  WHERE FROMDATE >= @FROMDATE" + ControlChars.CrLf
                          + "  AND TODATE <= @TODATE" + ControlChars.CrLf);
                        //+ "  AND (POS_NAME <> '' AND POS_NAME IS NOT NULL)" + ControlChars.CrLf);
                        sSQL += string.Format(@"  GROUP BY REGION_NAME, POS_CD, POS_NAME" + ControlChars.CrLf);
                    };
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        //Security.Filter(cmd, m_sMODULE, "edit");
                        /*        string FROM =    String.Format("{0:MM/dd/yyyy}", dt);, dt)*/
                        ;
                        //Sql.AddParameter(cmd, "@FROMDATE", (DateTime.ParseExact(FROMDATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture)));
                        //Sql.AddParameter(cmd, "@TODATE", (DateTime.ParseExact(TODATE.ToString(), "d/M/yyyy", CultureInfo.InvariantCulture).AddDays(1)));

                        Sql.AddParameter(cmd, "@FROMDATE", String.Format("{0:yyyy-MM-dd}", FROMDATE));
                        Sql.AddParameter(cmd, "@TODATE", String.Format("{0:yyyy-MM-dd}", TODATE.AddDays(1)));
                        //if (REPORT == "1")
                        //{
                        //    if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                        //    {
                        //        Sql.AddParameter(cmd, "@LIMIT_DATA_BUSINESS_CODE", Security.CURRENT_EMPLOYEE.BUSINESS_CODE);

                        //    }
                        //}
                        if (REPORT == "3" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            //sSQL += string.Format(@" AND REGION_ID = @REGION_ID" + ControlChars.CrLf);
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@REGION_ID", ddlCN_VUNG.SelectedValue);
                        }
                        else if (REPORT == "2" && !string.IsNullOrEmpty(ddlCN_VUNG.SelectedValue))
                        {
                            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                            Sql.AddParameter(cmd, "@MAIN_POS_CODE", ddlCN_VUNG.SelectedValue);

                        }

                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                dt = dtCurrent;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        private void ddlREPORT_SelectIndexChange(object sender, EventArgs e)
        {
           
            ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
           
            if (ddlREPORT.SelectedValue == "1")
            {
                ddlCN_VUNG.Items.Clear();
              
            }
            else if (ddlREPORT.SelectedValue == "2")
            {
                ddlCN_VUNG.Items.Clear();
                ddlCN_VUNG.DataSource = SplendidCache.M_ORGANIZATION_MAINPOS();//chi nhanh
                ddlCN_VUNG.DataTextField = "ORGANIZATION_NAME";
                ddlCN_VUNG.DataValueField = "ORGANIZATION_CODE";
                ddlCN_VUNG.DataBind();
                ddlCN_VUNG.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlCN_VUNG.SelectedIndex = 1;
            }
            else if (ddlREPORT.SelectedValue == "3")
            {
                ddlCN_VUNG.Items.Clear();
                ddlCN_VUNG.DataSource = SplendidCache.M_REGION();// vùng
                ddlCN_VUNG.DataTextField = "REGION_NAME";
                ddlCN_VUNG.DataValueField = "ID";
                ddlCN_VUNG.DataBind();
                ddlCN_VUNG.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                ddlCN_VUNG.SelectedIndex = 0;
            }
        }
        private void Page_Load(object sender, System.EventArgs e)
		{

           


			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				gID = Sql.ToGuid(Request["ID"]);
				if ( !IsPostBack )
				{


                   
                  
                 


                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
					if ( !Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID) )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory();
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							string sSQL ;
							sSQL = "select *           " + ControlChars.CrLf
							     + "  from vwKPIR010103_Edit" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Security.Filter(cmd, m_sMODULE, "edit");
								if ( !Sql.IsEmptyGuid(gDuplicateID) )
								{
									Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
									gID = Guid.Empty;
								}
								else
								{
									Sql.AppendParameter(cmd, gID, "ID", false);
								}
								con.Open();

								if ( bDebug )
									RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									using ( DataTable dtCurrent = new DataTable() )
									{
										da.Fill(dtCurrent);
										if ( dtCurrent.Rows.Count > 0 )
										{
											DataRow rdr = dtCurrent.Rows[0];
											this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
											
											ctlDynamicButtons.Title = Sql.ToString(rdr["NAME"]);
											SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
											Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
											ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

											bool bModuleIsAssigned  = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
											Guid gASSIGNED_USER_ID = Guid.Empty;
											if ( bModuleIsAssigned )
												gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

											this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
											this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
											TextBox txtNAME = this.FindControl("NAME") as TextBox;
											if ( txtNAME != null )
												txtNAME.Focus();

											ViewState ["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"   ]);
											ViewState ["NAME"              ] = Sql.ToString  (rdr["NAME"            ]);
											ViewState ["ASSIGNED_USER_ID"  ] = gASSIGNED_USER_ID;
											Page.Items["NAME"              ] = ViewState ["NAME"            ];
											Page.Items["ASSIGNED_USER_ID"  ] = ViewState ["ASSIGNED_USER_ID"];
											
											this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
										}
										else
										{
											ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
											ctlDynamicButtons.DisableAll();
											ctlFooterButtons .DisableAll();
											ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
											plcSubPanel.Visible = false;
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("NAME") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
					Page.Items["NAME"            ] = ViewState ["NAME"            ];
					Page.Items["ASSIGNED_USER_ID"] = ViewState ["ASSIGNED_USER_ID"];
				}
                // KHAI BÁO
                ddlREPORT_TYPE = this.FindControl("NAME") as DropDownList;
                // PHẠM VI
                ddlREPORT = this.FindControl("CREATED_BY") as DropDownList;
                //     dtpFROMDATE = this.FindControl("DATE_ENTERED") as DATEP;
                //    dtpTODATE = this.FindControl("DATE_MODIFIED") as TextBox;
                ddlCN_VUNG = this.FindControl("TAG_SET_NAME") as DropDownList;
                if ("CRM_02".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_03".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {

                    //ddlREPORT.SelectedValue = "1";
                    //ddlREPORT.Enabled = false;
                    //ddlCN_VUNG.Enabled = false;
                }
                else if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    ddlREPORT.SelectedValue = "2";
                    ddlREPORT.Enabled = false;
                    ddlCN_VUNG.Enabled = false;
                    ddlCN_VUNG.Items.Clear();
                    ddlCN_VUNG.DataSource = SplendidCache.M_ORGANIZATION_MAINPOS();//chi nhanh
                    ddlCN_VUNG.DataTextField = "ORGANIZATION_NAME";
                    ddlCN_VUNG.DataValueField = "ORGANIZATION_CODE";
                    ddlCN_VUNG.DataBind();
                    //ddlCN_VUNG.Items.Insert(0, new ListItem(L10n.Term(".LBL_NONE"), string.Empty));
                    ddlCN_VUNG.SelectedValue = Security.CURRENT_EMPLOYEE.MAIN_POS_CODE;


                }
               
                if (ddlREPORT != null)
                {
                    ddlREPORT.SelectedIndexChanged += new EventHandler(ddlREPORT_SelectIndexChange);
                    ddlREPORT.AutoPostBack = true;
                   
                }
            }
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This Task is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIR010103";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				Page.Validators.Add(new RulesValidator(this));
			}
		}
		#endregion
	}
}
