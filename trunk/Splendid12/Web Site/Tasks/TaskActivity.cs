/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// TaskActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class TaskActivity: SplendidActivity
	{
		public TaskActivity()
		{
			this.Name = "TaskActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.IDProperty))); }
			set { base.SetValue(TaskActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(TaskActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.NAMEProperty))); }
			set { base.SetValue(TaskActivity.NAMEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(TaskActivity.STATUSProperty))); }
			set { base.SetValue(TaskActivity.STATUSProperty, value); }
		}

		public static DependencyProperty DATE_TIME_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME_DUE", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME_DUE
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_TIME_DUEProperty))); }
			set { base.SetValue(TaskActivity.DATE_TIME_DUEProperty, value); }
		}

		public static DependencyProperty DATE_TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME_START", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME_START
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_TIME_STARTProperty))); }
			set { base.SetValue(TaskActivity.DATE_TIME_STARTProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(TaskActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(TaskActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.PARENT_IDProperty))); }
			set { base.SetValue(TaskActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.CONTACT_IDProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_IDProperty, value); }
		}

		public static DependencyProperty PRIORITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIORITY", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIORITY
		{
			get { return ((string)(base.GetValue(TaskActivity.PRIORITYProperty))); }
			set { base.SetValue(TaskActivity.PRIORITYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(TaskActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(TaskActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.TEAM_IDProperty))); }
			set { base.SetValue(TaskActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(TaskActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(TaskActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(TaskActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REMINDER_TIME", typeof(Int32), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(TaskActivity.REMINDER_TIMEProperty))); }
			set { base.SetValue(TaskActivity.REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty EMAIL_REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL_REMINDER_TIME", typeof(Int32), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 EMAIL_REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(TaskActivity.EMAIL_REMINDER_TIMEProperty))); }
			set { base.SetValue(TaskActivity.EMAIL_REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty SMS_REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SMS_REMINDER_TIME", typeof(Int32), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 SMS_REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(TaskActivity.SMS_REMINDER_TIMEProperty))); }
			set { base.SetValue(TaskActivity.SMS_REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(TaskActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(TaskActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(TaskActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(TaskActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(TaskActivity.CREATED_BYProperty))); }
			set { base.SetValue(TaskActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(TaskActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_DUE", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_DUE
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_DUEProperty))); }
			set { base.SetValue(TaskActivity.DATE_DUEProperty, value); }
		}

		public static DependencyProperty DATE_DUE_FLAGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_DUE_FLAG", typeof(bool), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool DATE_DUE_FLAG
		{
			get { return ((bool)(base.GetValue(TaskActivity.DATE_DUE_FLAGProperty))); }
			set { base.SetValue(TaskActivity.DATE_DUE_FLAGProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(TaskActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(TaskActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(TaskActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.DATE_STARTProperty))); }
			set { base.SetValue(TaskActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty DATE_START_FLAGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START_FLAG", typeof(bool), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool DATE_START_FLAG
		{
			get { return ((bool)(base.GetValue(TaskActivity.DATE_START_FLAGProperty))); }
			set { base.SetValue(TaskActivity.DATE_START_FLAGProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(TaskActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(TaskActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(TaskActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(TaskActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(TaskActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TIME_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_DUE", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_DUE
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.TIME_DUEProperty))); }
			set { base.SetValue(TaskActivity.TIME_DUEProperty, value); }
		}

		public static DependencyProperty TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_START", typeof(DateTime), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_START
		{
			get { return ((DateTime)(base.GetValue(TaskActivity.TIME_STARTProperty))); }
			set { base.SetValue(TaskActivity.TIME_STARTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(TaskActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_EMAIL", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_EMAIL
		{
			get { return ((string)(base.GetValue(TaskActivity.CONTACT_EMAILProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_EMAILProperty, value); }
		}

		public static DependencyProperty CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.CONTACT_NAMEProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CONTACT_PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_PHONE", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_PHONE
		{
			get { return ((string)(base.GetValue(TaskActivity.CONTACT_PHONEProperty))); }
			set { base.SetValue(TaskActivity.CONTACT_PHONEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(TaskActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(TaskActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(TaskActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(TaskActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(TaskActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(TaskActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(TaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(TaskActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(TaskActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("TaskActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("TaskActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select TASKS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwTASKS_AUDIT        TASKS          " + ControlChars.CrLf
							                + " inner join vwTASKS_AUDIT        TASKS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on TASKS_AUDIT_OLD.ID = TASKS.ID       " + ControlChars.CrLf
							                + "        and TASKS_AUDIT_OLD.AUDIT_VERSION = (select max(vwTASKS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwTASKS_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwTASKS_AUDIT.ID            =  TASKS.ID           " + ControlChars.CrLf
							                + "                                                and vwTASKS_AUDIT.AUDIT_VERSION <  TASKS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwTASKS_AUDIT.AUDIT_TOKEN   <> TASKS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where TASKS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwTASKS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwTASKS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwTASKS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								if ( !bPast )
									DATE_TIME_DUE                  = Sql.ToDateTime(rdr["DATE_TIME_DUE"                 ]);
								if ( !bPast )
									DATE_TIME_START                = Sql.ToDateTime(rdr["DATE_TIME_START"               ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								CONTACT_ID                     = Sql.ToGuid    (rdr["CONTACT_ID"                    ]);
								PRIORITY                       = Sql.ToString  (rdr["PRIORITY"                      ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								REMINDER_TIME                  = Sql.ToInteger (rdr["REMINDER_TIME"                 ]);
								EMAIL_REMINDER_TIME            = Sql.ToInteger (rdr["EMAIL_REMINDER_TIME"           ]);
								SMS_REMINDER_TIME              = Sql.ToInteger (rdr["SMS_REMINDER_TIME"             ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_DUE                       = Sql.ToDateTime(rdr["DATE_DUE"                      ]);
								DATE_DUE_FLAG                  = Sql.ToBoolean (rdr["DATE_DUE_FLAG"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								DATE_START_FLAG                = Sql.ToBoolean (rdr["DATE_START_FLAG"               ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TIME_DUE                       = Sql.ToDateTime(rdr["TIME_DUE"                      ]);
								TIME_START                     = Sql.ToDateTime(rdr["TIME_START"                    ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CONTACT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_SET_ID"       ]);
									CONTACT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_USER_ID"      ]);
									CONTACT_EMAIL                  = Sql.ToString  (rdr["CONTACT_EMAIL"                 ]);
									CONTACT_NAME                   = Sql.ToString  (rdr["CONTACT_NAME"                  ]);
									CONTACT_PHONE                  = Sql.ToString  (rdr["CONTACT_PHONE"                 ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("TaskActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("TASKS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spTASKS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, STATUS
								, DATE_TIME_DUE
								, DATE_TIME_START
								, PARENT_TYPE
								, PARENT_ID
								, CONTACT_ID
								, PRIORITY
								, DESCRIPTION
								, TEAM_ID
								, TEAM_SET_LIST
								, TAG_SET_NAME
								, REMINDER_TIME
								, EMAIL_REMINDER_TIME
								, SMS_REMINDER_TIME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("TaskActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

