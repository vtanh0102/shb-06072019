﻿/* Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

function ShowOptionsDialog(cbLoginComplete)
{
	try
	{
		if ( cbLoginComplete === undefined )
		{
			cbLoginComplete = function(status, message)
			{
				if ( status == 1 )
				{
					// 08/26/2014 Paul.  This is not the best place to set the last login value. 
					//localStorage['LastLoginRemote'] = false;
					SplendidError.SystemMessage('');
				}
			};
		}
		LoginViewUI_Load('divMainLayoutPanel', 'divMainActionsPanel', cbLoginComplete, function(status, message)
		{
			if ( status == 1 )
			{
				SplendidError.SystemMessage('');
			}
			else
			{
				SplendidError.SystemMessage(message);
			}
		});
	}
	catch(e)
	{
		SplendidError.SystemError(e, 'Options.js ShowOptionsDialog()');
	}
}

