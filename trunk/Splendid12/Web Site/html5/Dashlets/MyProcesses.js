﻿/* Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License Agreement, or other written agreement between you and SplendidCRM ("License"). 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and trademarks, in and to the contents of this file.  You will not link to or in any way 
 * combine the contents of this file or any derivatives with any Open Source Code in any manner that would require the contents of this file to be made available to any third party. 
 */

define(function()
{
	var sMODULE_NAME    = 'Processes';
	var sGRID_NAME      = sMODULE_NAME + '.My' + sMODULE_NAME;
	var sTABLE_NAME     = 'vw' + sMODULE_NAME.toUpperCase() + '_MyList';
	var sSORT_FIELD     = 'DATE_ENTERED';
	var sSORT_DIRECTION = 'asc';

	function OverdueMessage(dtDATE_ENTERED, sDURATION_UNITS, nDURATION_VALUE)
	{
		var sProcessStatus = '';
		if ( !Sql.IsEmptyString(sDURATION_UNITS) && nDURATION_VALUE > 0 )
		{
			var dtDUE_DATE = dtDATE_ENTERED;
			switch ( sDURATION_UNITS )
			{
				case 'hour'   :  dtDUE_DATE = dtDUE_DATE.setHours(dtDUE_DATE.getHours() +     nDURATION_VALUE);  break;
				case 'day'    :  dtDUE_DATE = dtDUE_DATE.setDay  (dtDUE_DATE.getDay  () +     nDURATION_VALUE);  break;
				case 'week'   :  dtDUE_DATE = dtDUE_DATE.setDay  (dtDUE_DATE.getDay  () + 7 * nDURATION_VALUE);  break;
				case 'month'  :  dtDUE_DATE = dtDUE_DATE.setMonth(dtDUE_DATE.getMonth() +     nDURATION_VALUE);  break;
				case 'quarter':  dtDUE_DATE = dtDUE_DATE.setMonth(dtDUE_DATE.getMonth() + 3 * nDURATION_VALUE);  break;
				case 'year'   :  dtDUE_DATE = dtDUE_DATE.setYear (dtDUE_DATE.getYear () +     nDURATION_VALUE);  break;
			}
			var sDUE_DATE = FromJsonDate(dtDUE_DATE, Security.USER_DATE_FORMAT())
			if ( dtDUE_DATE <= (new Date()) )
			{
				var sOverdue = L10n.Term('Processes.LBL_OVERDUE_FORMAT').replace('{0}', sDUE_DATE);
				sProcessStatus += ' [ <span class=\'ProcessOverdue\'>' + sOverdue + '</span> ]';
			}
		}
		return sProcessStatus;
	}

	return {
		Render: function(sLayoutPanel, sActionsPanel, sSCRIPT_URL, sSETTINGS_EDITVIEW, sDEFAULT_SETTINGS)
		{
			var divDashboardPanel = document.getElementById(sLayoutPanel);
			if ( divDashboardPanel != null )
			{
				var divTabMenu = document.createElement('div');
				divTabMenu.role = 'tabpanel';
				divDashboardPanel.appendChild(divTabMenu);
				var ulActions = document.createElement('ul');
				ulActions.id        = sLayoutPanel + '_pnlModuleActions';
				ulActions.role      = 'tablist';
				ulActions.className = 'nav nav-tabs bar_tabs';
				divTabMenu.appendChild(ulActions);

				var liListMyProcesses = document.createElement('li');
				liListMyProcesses.id        = sLayoutPanel + '_pnlModuleActions_MyProcesses';
				liListMyProcesses.role      = 'presentation';
				liListMyProcesses.className = '';
				ulActions.appendChild(liListMyProcesses);
				var aListMyProcesses = document.createElement('a');
				aListMyProcesses.href      = '#';
				aListMyProcesses.style.textDecoration = 'none';
				liListMyProcesses.appendChild(aListMyProcesses);

				var divMyProcessesFrame = document.createElement('div');
				divMyProcessesFrame.style.textAlign = 'center';
				aListMyProcesses.appendChild(divMyProcessesFrame);
				var divMyProcessesCount = document.createElement('div');
				divMyProcessesCount.id        = sLayoutPanel + '_pnlModuleActions_MyProcesses_Count';
				divMyProcessesCount.className = 'MyProcessCount';
				divMyProcessesCount.innerHTML = '0';
				divMyProcessesFrame.appendChild(divMyProcessesCount);
				var divMyProcessesLabel = document.createElement('div');
				divMyProcessesLabel.className = 'MyProcessCountLabel';
				divMyProcessesFrame.appendChild(divMyProcessesLabel);
				divMyProcessesLabel.innerHTML = L10n.Term('Processes.LBL_MY_PROCESSES');


				var liListSelfService = document.createElement('li');
				liListSelfService.id        = sLayoutPanel + '_pnlModuleActions_SelfService';
				liListSelfService.role      = 'presentation';
				liListSelfService.className = '';
				ulActions.appendChild(liListSelfService);
				var aListSelfService = document.createElement('a');
				aListSelfService.style.textDecoration = 'none';
				aListSelfService.href      = '#';
				liListSelfService.appendChild(aListSelfService);
				
				var divSelfServiceFrame = document.createElement('div');
				divSelfServiceFrame.style.textAlign = 'center';
				aListSelfService.appendChild(divSelfServiceFrame);
				var divSelfServiceCount = document.createElement('div');
				divSelfServiceCount.id        = sLayoutPanel + '_pnlModuleActions_SelfService_Count';
				divSelfServiceCount.className = 'MyProcessCount';
				divSelfServiceCount.innerHTML = '0';
				divSelfServiceFrame.appendChild(divSelfServiceCount);
				var divSelfServiceLabel = document.createElement('div');
				divSelfServiceLabel.className = 'MyProcessCountLabel';
				divSelfServiceFrame.appendChild(divSelfServiceLabel);
				divSelfServiceLabel.innerHTML = L10n.Term('Processes.LBL_SELF_SERVICE_PROCESSES');

				var self = this;
				aListMyProcesses.onclick = function(e)
				{
					liListMyProcesses.className = 'active';
					liListSelfService.className = '';
					if ( !e )
						e = window.event;
					if ( e.stopPropagation )
						e.stopPropagation();
					else if ( window.event )
						e.cancelBubble = true;
					SearchViewUI_SearchForm(sLayoutPanel, sActionsPanel, sSETTINGS_EDITVIEW, self.SearchMyProcesses, self);
				};
				aListSelfService.onclick = function(e)
				{
					liListMyProcesses.className = '';
					liListSelfService.className = 'active';
					if ( !e )
						e = window.event;
					if ( e.stopPropagation )
						e.stopPropagation();
					else if ( window.event )
						e.cancelBubble = true;
					SearchViewUI_SearchForm(sLayoutPanel, sActionsPanel, sSETTINGS_EDITVIEW, self.SearchSelfService, self);
				};

				var divDashletBody = document.createElement('div');
				divDashletBody.id = sLayoutPanel + '_divDashletBody';
				divDashletBody.align = 'center';
				divDashboardPanel.appendChild(divDashletBody);
				var divDashletError = document.createElement('div');
				divDashletError.id = sLayoutPanel + '_divDashletError';
				divDashletError.className = 'error';
				divDashletBody.appendChild(divDashletError);
				var divDashletHTML5 = document.createElement('div');
				divDashletHTML5.id = sLayoutPanel + '_divDashletHTML5';
				divDashletHTML5.style.width = '100%';
				divDashletBody.appendChild(divDashletHTML5);
				
				var rowDefaultSearch = Sql.ParseFormData(sDEFAULT_SETTINGS);
				SearchViewUI_Load(sLayoutPanel, sActionsPanel, sMODULE_NAME, sSETTINGS_EDITVIEW, rowDefaultSearch, false, this.SearchMyProcesses, function(status, message)
				{
					if ( status == 1 )
					{
						liListMyProcesses.className = 'active';
						SearchViewUI_SearchForm(sLayoutPanel, sActionsPanel, sSETTINGS_EDITVIEW, this.SearchMyProcesses, this);
						// 06/15/2017 Paul.  We need to manually get the Self-Service count. 
						var bgPage = chrome.extension.getBackgroundPage();
						var sSELECT_FIELDS = 'PROCESS_NUMBER';
						var sSEARCH_FILTER = 'PROCESS_USER_ID is null';
						bgPage.ListView_LoadTable(sTABLE_NAME, sSORT_FIELD, sSORT_DIRECTION, sSELECT_FIELDS, sSEARCH_FILTER, function(status, message)
						{
							if ( status == 1 )
							{
								var rows = message;
								$('#' + sLayoutPanel + '_pnlModuleActions_SelfService_Count').text(rows.length);
							}
						}, this);
					}
					else
					{
						$('#' + sLayoutPanel + '_divDashletError').text('Dashlet error: ' + message);
					}
				}, this);
			}
		},
		SearchMyProcesses: function(sLayoutPanel, sActionsPanel, sSEARCH_FILTER, rowSEARCH_VALUES)
		{
			var bgPage = chrome.extension.getBackgroundPage();
			bgPage.AuthenticatedMethod(function(status, message)
			{
				if ( status == 1 )
				{
					var divDashletHTML5 = document.getElementById(sLayoutPanel + '_divDashletHTML5');
					while ( divDashletHTML5.childNodes.length > 0 )
					{
						divDashletHTML5.removeChild(divDashletHTML5.firstChild);
					}
			
					try
					{
						var sPRIMARY_ID     = null;
						var oListViewUI = new ListViewUI();
						oListViewUI.BootstrapColumnsFinalize = this.BootstrapColumnsFinalize;
						oListViewUI.AdditionalColumns        = this.AdditionalColumns;
						sSEARCH_FILTER = 'PROCESS_USER_ID is not null';
						oListViewUI.LoadRelatedModule(divDashletHTML5.id, sActionsPanel, sMODULE_NAME, sMODULE_NAME, sGRID_NAME, sTABLE_NAME, sSORT_FIELD, sSORT_DIRECTION, sSEARCH_FILTER, sPRIMARY_ID, function(status, message)
						{
							if ( status == 1 )
								$('#' + sLayoutPanel + '_pnlModuleActions_MyProcesses_Count').text(message);
							else
								$('#' + sLayoutPanel + '_divDashletError').text('Dashlet error: ' + message);
						});
					}
					catch(e)
					{
						$('#' + sLayoutPanel + '_divDashletError').text(e.message);
					}
				}
			}, this);
		},
		SearchSelfService: function(sLayoutPanel, sActionsPanel, sSEARCH_FILTER, rowSEARCH_VALUES)
		{
			var bgPage = chrome.extension.getBackgroundPage();
			bgPage.AuthenticatedMethod(function(status, message)
			{
				if ( status == 1 )
				{
					var divDashletHTML5 = document.getElementById(sLayoutPanel + '_divDashletHTML5');
					while ( divDashletHTML5.childNodes.length > 0 )
					{
						divDashletHTML5.removeChild(divDashletHTML5.firstChild);
					}
			
					try
					{
						var sPRIMARY_ID     = null;
						var oListViewUI = new ListViewUI();
						oListViewUI.BootstrapColumnsFinalize = this.BootstrapColumnsFinalize;
						oListViewUI.AdditionalColumns        = this.AdditionalColumns;
						sSEARCH_FILTER = 'PROCESS_USER_ID is null';
						oListViewUI.LoadRelatedModule(divDashletHTML5.id, sActionsPanel, sMODULE_NAME, sMODULE_NAME, sGRID_NAME, sTABLE_NAME, sSORT_FIELD, sSORT_DIRECTION, sSEARCH_FILTER, sPRIMARY_ID, function(status, message)
						{
							if ( status == 1 )
								$('#' + sLayoutPanel + '_pnlModuleActions_SelfService_Count').text(message);
							else
								$('#' + sLayoutPanel + '_divDashletError').text('Dashlet error: ' + message);
						});
					}
					catch(e)
					{
						$('#' + sLayoutPanel + '_divDashletError').text(e.message);
					}
				}
			}, this);
		},
		AdditionalColumns: function(arrSelectFields)
		{
			if ( $.inArray('PROCESS_NUMBER'         , arrSelectFields) == -1 ) arrSelectFields.push('PROCESS_NUMBER'         );
			if ( $.inArray('ACTIVITY_NAME'          , arrSelectFields) == -1 ) arrSelectFields.push('ACTIVITY_NAME'          );
			if ( $.inArray('BUSINESS_PROCESS_NAME'  , arrSelectFields) == -1 ) arrSelectFields.push('BUSINESS_PROCESS_NAME'  );
			//if ( $.inArray('PROCESS_USER_ID'        , arrSelectFields) == -1 ) arrSelectFields.push('PROCESS_USER_ID'        );
			if ( $.inArray('PARENT_TYPE'            , arrSelectFields) == -1 ) arrSelectFields.push('PARENT_TYPE'            );
			if ( $.inArray('PARENT_ID'              , arrSelectFields) == -1 ) arrSelectFields.push('PARENT_ID'              );
			if ( $.inArray('PARENT_NAME'            , arrSelectFields) == -1 ) arrSelectFields.push('PARENT_NAME'            );
			//if ( $.inArray('PARENT_ASSIGNED_USER_ID', arrSelectFields) == -1 ) arrSelectFields.push('PARENT_ASSIGNED_USER_ID');
			//if ( $.inArray('ASSIGNED_USER_NAME'     , arrSelectFields) == -1 ) arrSelectFields.push('ASSIGNED_USER_NAME'     );
			if ( $.inArray('ASSIGNED_FULL_NAME'     , arrSelectFields) == -1 ) arrSelectFields.push('ASSIGNED_FULL_NAME'     );
			//if ( $.inArray('USER_TASK_TYPE'         , arrSelectFields) == -1 ) arrSelectFields.push('USER_TASK_TYPE'         );
			//if ( $.inArray('USER_ASSIGNMENT_METHOD' , arrSelectFields) == -1 ) arrSelectFields.push('USER_ASSIGNMENT_METHOD' );
			if ( $.inArray('DURATION_UNITS'         , arrSelectFields) == -1 ) arrSelectFields.push('DURATION_UNITS'         );
			if ( $.inArray('DURATION_VALUE'         , arrSelectFields) == -1 ) arrSelectFields.push('DURATION_VALUE'         );
			//if ( $.inArray('STATUS'                 , arrSelectFields) == -1 ) arrSelectFields.push('STATUS'                 );
			if ( $.inArray('DATE_ENTERED'           , arrSelectFields) == -1 ) arrSelectFields.push('DATE_ENTERED'           );
		},
		BootstrapColumnsFinalize: function(sLayoutPanel, sActionsPanel, sMODULE_NAME, arrDataTableColumns)
		{
			try
			{
				var arrNewDataTableColumns = new Array();
				var objDataColumn = new Object();
				objDataColumn.data       = null;
				objDataColumn.title      = '';
				objDataColumn.DATA_FIELD = null;
				objDataColumn.orderable  = false;
				objDataColumn.className  = '';
				objDataColumn.width      = '100%';
				objDataColumn.className += ' MyProcessCell';
				objDataColumn.className  = Trim(objDataColumn.className);
				objDataColumn.orderData  = arrNewDataTableColumns.length;
				objDataColumn.render = function(data, type, full, meta)
				{
					var sDATA_VALUE = '';
					var row = data;
					if ( type == 'display' )
					{
						var sPROCESS_NAME          = L10n.Term('Processes.LBL_MY_PROCESSES_NAME_FORMAT').replace('{0}', Sql.ToString(row['PROCESS_NUMBER'])).replace('{1}', Sql.ToString(row['PARENT_NAME']));
						var dtDATE_ENTERED         = FromJsonDate (row['DATE_ENTERED'         ]);
						var sPARENT_NAME           = Sql.ToString (row['PARENT_NAME'          ]);
						var sPARENT_TYPE           = Sql.ToString (row['PARENT_TYPE'          ]);
						var sPARENT_ID             = Sql.ToString (row['PARENT_ID'            ]);
						var sDURATION_UNITS        = Sql.ToString (row['DURATION_UNITS'       ]);
						var nDURATION_VALUE        = Sql.ToInteger(row['DURATION_VALUE'       ]);
						var sACTIVITY_NAME         = Sql.ToString (row['ACTIVITY_NAME'        ]);
						var sASSIGNED_FULL_NAME    = Sql.ToString (row['ASSIGNED_FULL_NAME'   ]);
						var sBUSINESS_PROCESS_NAME = Sql.ToString (row['BUSINESS_PROCESS_NAME']);
						Sql.ToString(row['ACTIVITY_NAME'])
						sDATA_VALUE += '<div class=\'MyProcessFrame\'>';
						sDATA_VALUE += '	<div>';
						sDATA_VALUE += '		<a href="#" onclick="return ListViewUI_View(\'' + sLayoutPanel + '\', \'' + sActionsPanel + '\', \'' + escape(sPARENT_TYPE) + '\', \'' + sPARENT_ID + '\')" title="' + sPARENT_NAME + '">';
						sDATA_VALUE += '			<span class=\'MyProcessName\'>' + sPROCESS_NAME + '</span>';
						sDATA_VALUE += '		</a>';
						sDATA_VALUE += '	</div>';
						sDATA_VALUE += '	<div class=\'MyProcessOverdue\'>';
						sDATA_VALUE += '		' + OverdueMessage(dtDATE_ENTERED, sDURATION_UNITS, nDURATION_VALUE);
						sDATA_VALUE += '	</div>';
						sDATA_VALUE += '	<div>';
						sDATA_VALUE += '		<span class=\'MyProcessAssignedUser\'>' + sASSIGNED_FULL_NAME + '</span> <span class=\'MyProcessActivityName\'>' + sACTIVITY_NAME + '</span>';
						sDATA_VALUE += '	</div>';
						sDATA_VALUE += '	<div class=\'MyProcessBusinessProcessName\'>';
						sDATA_VALUE += '		' + sBUSINESS_PROCESS_NAME;
						sDATA_VALUE += '	</div>';
						sDATA_VALUE += '</div>';
					}
					return sDATA_VALUE;
				};
				arrNewDataTableColumns.push(objDataColumn);
				
				// 06/14/2017 Paul.  First column is blank and second column is view/edit. 
				/*
				arrNewDataTableColumns.splice(0, 0, arrDataTableColumns[0]);
				arrNewDataTableColumns.splice(1, 0, arrDataTableColumns[1]);
				for ( var i = 2; i < arrDataTableColumns.length; i++ )
				{
					arrNewDataTableColumns.splice(Sql.ToInteger(arrDataTableColumns[i].COLUMN_INDEX) + 1, 0, arrDataTableColumns[i]);
				}
				*/
			}
			catch(e)
			{
				console.log('MyCalls.BootstrapColumnsFinalize:' + e.message);
			}
			return arrNewDataTableColumns;
		}
	};
});
