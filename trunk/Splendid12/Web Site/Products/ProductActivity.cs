/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ProductActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class ProductActivity: SplendidActivity
	{
		public ProductActivity()
		{
			this.Name = "ProductActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.IDProperty))); }
			set { base.SetValue(ProductActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ProductActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty PRODUCT_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRODUCT_TEMPLATE_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PRODUCT_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.PRODUCT_TEMPLATE_IDProperty))); }
			set { base.SetValue(ProductActivity.PRODUCT_TEMPLATE_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.NAMEProperty))); }
			set { base.SetValue(ProductActivity.NAMEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(ProductActivity.STATUSProperty))); }
			set { base.SetValue(ProductActivity.STATUSProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(ProductActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CONTACT_IDProperty))); }
			set { base.SetValue(ProductActivity.CONTACT_IDProperty, value); }
		}

		public static DependencyProperty QUANTITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUANTITY", typeof(Int32), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 QUANTITY
		{
			get { return ((Int32)(base.GetValue(ProductActivity.QUANTITYProperty))); }
			set { base.SetValue(ProductActivity.QUANTITYProperty, value); }
		}

		public static DependencyProperty DATE_PURCHASEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_PURCHASED", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_PURCHASED
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.DATE_PURCHASEDProperty))); }
			set { base.SetValue(ProductActivity.DATE_PURCHASEDProperty, value); }
		}

		public static DependencyProperty DATE_SUPPORT_EXPIRESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_SUPPORT_EXPIRES", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_SUPPORT_EXPIRES
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.DATE_SUPPORT_EXPIRESProperty))); }
			set { base.SetValue(ProductActivity.DATE_SUPPORT_EXPIRESProperty, value); }
		}

		public static DependencyProperty DATE_SUPPORT_STARTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_SUPPORT_STARTS", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_SUPPORT_STARTS
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.DATE_SUPPORT_STARTSProperty))); }
			set { base.SetValue(ProductActivity.DATE_SUPPORT_STARTSProperty, value); }
		}

		public static DependencyProperty MANUFACTURER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MANUFACTURER_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MANUFACTURER_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.MANUFACTURER_IDProperty))); }
			set { base.SetValue(ProductActivity.MANUFACTURER_IDProperty, value); }
		}

		public static DependencyProperty CATEGORY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CATEGORY_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CATEGORY_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CATEGORY_IDProperty))); }
			set { base.SetValue(ProductActivity.CATEGORY_IDProperty, value); }
		}

		public static DependencyProperty TYPE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TYPE_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.TYPE_IDProperty))); }
			set { base.SetValue(ProductActivity.TYPE_IDProperty, value); }
		}

		public static DependencyProperty WEBSITEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WEBSITE", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string WEBSITE
		{
			get { return ((string)(base.GetValue(ProductActivity.WEBSITEProperty))); }
			set { base.SetValue(ProductActivity.WEBSITEProperty, value); }
		}

		public static DependencyProperty MFT_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MFT_PART_NUM", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MFT_PART_NUM
		{
			get { return ((string)(base.GetValue(ProductActivity.MFT_PART_NUMProperty))); }
			set { base.SetValue(ProductActivity.MFT_PART_NUMProperty, value); }
		}

		public static DependencyProperty VENDOR_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("VENDOR_PART_NUM", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string VENDOR_PART_NUM
		{
			get { return ((string)(base.GetValue(ProductActivity.VENDOR_PART_NUMProperty))); }
			set { base.SetValue(ProductActivity.VENDOR_PART_NUMProperty, value); }
		}

		public static DependencyProperty SERIAL_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SERIAL_NUMBER", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SERIAL_NUMBER
		{
			get { return ((string)(base.GetValue(ProductActivity.SERIAL_NUMBERProperty))); }
			set { base.SetValue(ProductActivity.SERIAL_NUMBERProperty, value); }
		}

		public static DependencyProperty ASSET_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSET_NUMBER", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSET_NUMBER
		{
			get { return ((string)(base.GetValue(ProductActivity.ASSET_NUMBERProperty))); }
			set { base.SetValue(ProductActivity.ASSET_NUMBERProperty, value); }
		}

		public static DependencyProperty TAX_CLASSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX_CLASS", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAX_CLASS
		{
			get { return ((string)(base.GetValue(ProductActivity.TAX_CLASSProperty))); }
			set { base.SetValue(ProductActivity.TAX_CLASSProperty, value); }
		}

		public static DependencyProperty WEIGHTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WEIGHT", typeof(float), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float WEIGHT
		{
			get { return ((float)(base.GetValue(ProductActivity.WEIGHTProperty))); }
			set { base.SetValue(ProductActivity.WEIGHTProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(ProductActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty COST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COST_PRICE", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal COST_PRICE
		{
			get { return ((decimal)(base.GetValue(ProductActivity.COST_PRICEProperty))); }
			set { base.SetValue(ProductActivity.COST_PRICEProperty, value); }
		}

		public static DependencyProperty LIST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_PRICE", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal LIST_PRICE
		{
			get { return ((decimal)(base.GetValue(ProductActivity.LIST_PRICEProperty))); }
			set { base.SetValue(ProductActivity.LIST_PRICEProperty, value); }
		}

		public static DependencyProperty BOOK_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BOOK_VALUE", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal BOOK_VALUE
		{
			get { return ((decimal)(base.GetValue(ProductActivity.BOOK_VALUEProperty))); }
			set { base.SetValue(ProductActivity.BOOK_VALUEProperty, value); }
		}

		public static DependencyProperty BOOK_VALUE_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BOOK_VALUE_DATE", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime BOOK_VALUE_DATE
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.BOOK_VALUE_DATEProperty))); }
			set { base.SetValue(ProductActivity.BOOK_VALUE_DATEProperty, value); }
		}

		public static DependencyProperty DISCOUNT_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_PRICE", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_PRICE
		{
			get { return ((decimal)(base.GetValue(ProductActivity.DISCOUNT_PRICEProperty))); }
			set { base.SetValue(ProductActivity.DISCOUNT_PRICEProperty, value); }
		}

		public static DependencyProperty PRICING_FACTORProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FACTOR", typeof(float), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float PRICING_FACTOR
		{
			get { return ((float)(base.GetValue(ProductActivity.PRICING_FACTORProperty))); }
			set { base.SetValue(ProductActivity.PRICING_FACTORProperty, value); }
		}

		public static DependencyProperty PRICING_FORMULAProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FORMULA", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRICING_FORMULA
		{
			get { return ((string)(base.GetValue(ProductActivity.PRICING_FORMULAProperty))); }
			set { base.SetValue(ProductActivity.PRICING_FORMULAProperty, value); }
		}

		public static DependencyProperty SUPPORT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.SUPPORT_NAMEProperty))); }
			set { base.SetValue(ProductActivity.SUPPORT_NAMEProperty, value); }
		}

		public static DependencyProperty SUPPORT_CONTACTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_CONTACT", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_CONTACT
		{
			get { return ((string)(base.GetValue(ProductActivity.SUPPORT_CONTACTProperty))); }
			set { base.SetValue(ProductActivity.SUPPORT_CONTACTProperty, value); }
		}

		public static DependencyProperty SUPPORT_DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_DESCRIPTION", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_DESCRIPTION
		{
			get { return ((string)(base.GetValue(ProductActivity.SUPPORT_DESCRIPTIONProperty))); }
			set { base.SetValue(ProductActivity.SUPPORT_DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty SUPPORT_TERMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_TERM", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_TERM
		{
			get { return ((string)(base.GetValue(ProductActivity.SUPPORT_TERMProperty))); }
			set { base.SetValue(ProductActivity.SUPPORT_TERMProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(ProductActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(ProductActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.TEAM_IDProperty))); }
			set { base.SetValue(ProductActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ProductActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ProductActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProductActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProductActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(ProductActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProductActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CATEGORY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CATEGORY_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CATEGORY_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.CATEGORY_NAMEProperty))); }
			set { base.SetValue(ProductActivity.CATEGORY_NAMEProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProductActivity.CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProductActivity.CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.CONTACT_NAMEProperty))); }
			set { base.SetValue(ProductActivity.CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty COST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COST_USDOLLAR", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal COST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(ProductActivity.COST_USDOLLARProperty))); }
			set { base.SetValue(ProductActivity.COST_USDOLLARProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ProductActivity.CREATED_BYProperty))); }
			set { base.SetValue(ProductActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(ProductActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ProductActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_CONVERSION_RATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_CONVERSION_RATE", typeof(float), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float CURRENCY_CONVERSION_RATE
		{
			get { return ((float)(base.GetValue(ProductActivity.CURRENCY_CONVERSION_RATEProperty))); }
			set { base.SetValue(ProductActivity.CURRENCY_CONVERSION_RATEProperty, value); }
		}

		public static DependencyProperty CURRENCY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.CURRENCY_NAMEProperty))); }
			set { base.SetValue(ProductActivity.CURRENCY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_SYMBOL", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_SYMBOL
		{
			get { return ((string)(base.GetValue(ProductActivity.CURRENCY_SYMBOLProperty))); }
			set { base.SetValue(ProductActivity.CURRENCY_SYMBOLProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ProductActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ProductActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ProductActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DISCOUNT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_USDOLLAR", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(ProductActivity.DISCOUNT_USDOLLARProperty))); }
			set { base.SetValue(ProductActivity.DISCOUNT_USDOLLARProperty, value); }
		}

		public static DependencyProperty LIST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_USDOLLAR", typeof(decimal), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal LIST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(ProductActivity.LIST_USDOLLARProperty))); }
			set { base.SetValue(ProductActivity.LIST_USDOLLARProperty, value); }
		}

		public static DependencyProperty MANUFACTURER_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MANUFACTURER_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MANUFACTURER_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.MANUFACTURER_NAMEProperty))); }
			set { base.SetValue(ProductActivity.MANUFACTURER_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ProductActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ProductActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ProductActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty QUOTE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid QUOTE_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.QUOTE_IDProperty))); }
			set { base.SetValue(ProductActivity.QUOTE_IDProperty, value); }
		}

		public static DependencyProperty QUOTE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUOTE_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.QUOTE_NAMEProperty))); }
			set { base.SetValue(ProductActivity.QUOTE_NAMEProperty, value); }
		}

		public static DependencyProperty QUOTE_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_NUM", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUOTE_NUM
		{
			get { return ((string)(base.GetValue(ProductActivity.QUOTE_NUMProperty))); }
			set { base.SetValue(ProductActivity.QUOTE_NUMProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ProductActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProductActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ProductActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ProductActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TYPE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE_NAME", typeof(string), typeof(ProductActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE_NAME
		{
			get { return ((string)(base.GetValue(ProductActivity.TYPE_NAMEProperty))); }
			set { base.SetValue(ProductActivity.TYPE_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ProductActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ProductActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select PRODUCTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwPRODUCTS_AUDIT        PRODUCTS          " + ControlChars.CrLf
							                + " inner join vwPRODUCTS_AUDIT        PRODUCTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on PRODUCTS_AUDIT_OLD.ID = PRODUCTS.ID       " + ControlChars.CrLf
							                + "        and PRODUCTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwPRODUCTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                  from vwPRODUCTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                 where vwPRODUCTS_AUDIT.ID            =  PRODUCTS.ID           " + ControlChars.CrLf
							                + "                                                   and vwPRODUCTS_AUDIT.AUDIT_VERSION <  PRODUCTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                   and vwPRODUCTS_AUDIT.AUDIT_TOKEN   <> PRODUCTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                               )" + ControlChars.CrLf
							                + " where PRODUCTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwPRODUCTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwPRODUCTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwPRODUCTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								if ( !bPast )
									ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								if ( !bPast )
									MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								if ( !bPast )
									PRODUCT_TEMPLATE_ID            = Sql.ToGuid    (rdr["PRODUCT_TEMPLATE_ID"           ]);
								if ( !bPast )
									NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								if ( !bPast )
									STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								if ( !bPast )
									ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								if ( !bPast )
									CONTACT_ID                     = Sql.ToGuid    (rdr["CONTACT_ID"                    ]);
								if ( !bPast )
									QUANTITY                       = Sql.ToInteger (rdr["QUANTITY"                      ]);
								if ( !bPast )
									DATE_PURCHASED                 = Sql.ToDateTime(rdr["DATE_PURCHASED"                ]);
								if ( !bPast )
									DATE_SUPPORT_EXPIRES           = Sql.ToDateTime(rdr["DATE_SUPPORT_EXPIRES"          ]);
								if ( !bPast )
									DATE_SUPPORT_STARTS            = Sql.ToDateTime(rdr["DATE_SUPPORT_STARTS"           ]);
								if ( !bPast )
									MANUFACTURER_ID                = Sql.ToGuid    (rdr["MANUFACTURER_ID"               ]);
								if ( !bPast )
									CATEGORY_ID                    = Sql.ToGuid    (rdr["CATEGORY_ID"                   ]);
								if ( !bPast )
									TYPE_ID                        = Sql.ToGuid    (rdr["TYPE_ID"                       ]);
								if ( !bPast )
									WEBSITE                        = Sql.ToString  (rdr["WEBSITE"                       ]);
								if ( !bPast )
									MFT_PART_NUM                   = Sql.ToString  (rdr["MFT_PART_NUM"                  ]);
								if ( !bPast )
									VENDOR_PART_NUM                = Sql.ToString  (rdr["VENDOR_PART_NUM"               ]);
								if ( !bPast )
									SERIAL_NUMBER                  = Sql.ToString  (rdr["SERIAL_NUMBER"                 ]);
								if ( !bPast )
									ASSET_NUMBER                   = Sql.ToString  (rdr["ASSET_NUMBER"                  ]);
								if ( !bPast )
									TAX_CLASS                      = Sql.ToString  (rdr["TAX_CLASS"                     ]);
								if ( !bPast )
									WEIGHT                         = Sql.ToFloat   (rdr["WEIGHT"                        ]);
								if ( !bPast )
									CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								if ( !bPast )
									COST_PRICE                     = Sql.ToDecimal (rdr["COST_PRICE"                    ]);
								if ( !bPast )
									LIST_PRICE                     = Sql.ToDecimal (rdr["LIST_PRICE"                    ]);
								if ( !bPast )
									BOOK_VALUE                     = Sql.ToDecimal (rdr["BOOK_VALUE"                    ]);
								if ( !bPast )
									BOOK_VALUE_DATE                = Sql.ToDateTime(rdr["BOOK_VALUE_DATE"               ]);
								if ( !bPast )
									DISCOUNT_PRICE                 = Sql.ToDecimal (rdr["DISCOUNT_PRICE"                ]);
								if ( !bPast )
									PRICING_FACTOR                 = Sql.ToFloat   (rdr["PRICING_FACTOR"                ]);
								if ( !bPast )
									PRICING_FORMULA                = Sql.ToString  (rdr["PRICING_FORMULA"               ]);
								if ( !bPast )
									SUPPORT_NAME                   = Sql.ToString  (rdr["SUPPORT_NAME"                  ]);
								if ( !bPast )
									SUPPORT_CONTACT                = Sql.ToString  (rdr["SUPPORT_CONTACT"               ]);
								if ( !bPast )
									SUPPORT_DESCRIPTION            = Sql.ToString  (rdr["SUPPORT_DESCRIPTION"           ]);
								if ( !bPast )
									SUPPORT_TERM                   = Sql.ToString  (rdr["SUPPORT_TERM"                  ]);
								if ( !bPast )
									DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								if ( !bPast )
									TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								if ( !bPast )
									TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"       ]);
									ACCOUNT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"      ]);
									ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
									ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
									CATEGORY_NAME                  = Sql.ToString  (rdr["CATEGORY_NAME"                 ]);
									CONTACT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_SET_ID"       ]);
									CONTACT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_USER_ID"      ]);
									CONTACT_NAME                   = Sql.ToString  (rdr["CONTACT_NAME"                  ]);
									COST_USDOLLAR                  = Sql.ToDecimal (rdr["COST_USDOLLAR"                 ]);
									CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
									CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									CURRENCY_CONVERSION_RATE       = Sql.ToFloat   (rdr["CURRENCY_CONVERSION_RATE"      ]);
									CURRENCY_NAME                  = Sql.ToString  (rdr["CURRENCY_NAME"                 ]);
									CURRENCY_SYMBOL                = Sql.ToString  (rdr["CURRENCY_SYMBOL"               ]);
									DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
									DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
									DISCOUNT_USDOLLAR              = Sql.ToDecimal (rdr["DISCOUNT_USDOLLAR"             ]);
									LIST_USDOLLAR                  = Sql.ToDecimal (rdr["LIST_USDOLLAR"                 ]);
									MANUFACTURER_NAME              = Sql.ToString  (rdr["MANUFACTURER_NAME"             ]);
									MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									QUOTE_ID                       = Sql.ToGuid    (rdr["QUOTE_ID"                      ]);
									QUOTE_NAME                     = Sql.ToString  (rdr["QUOTE_NAME"                    ]);
									QUOTE_NUM                      = Sql.ToString  (rdr["QUOTE_NUM"                     ]);
									TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
									TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
									TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
									TYPE_NAME                      = Sql.ToString  (rdr["TYPE_NAME"                     ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProductActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("PRODUCTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spPRODUCTS_Update
								( ref gID
								, PRODUCT_TEMPLATE_ID
								, NAME
								, STATUS
								, ACCOUNT_ID
								, CONTACT_ID
								, QUANTITY
								, DATE_PURCHASED
								, DATE_SUPPORT_EXPIRES
								, DATE_SUPPORT_STARTS
								, MANUFACTURER_ID
								, CATEGORY_ID
								, TYPE_ID
								, WEBSITE
								, MFT_PART_NUM
								, VENDOR_PART_NUM
								, SERIAL_NUMBER
								, ASSET_NUMBER
								, TAX_CLASS
								, WEIGHT
								, CURRENCY_ID
								, COST_PRICE
								, LIST_PRICE
								, BOOK_VALUE
								, BOOK_VALUE_DATE
								, DISCOUNT_PRICE
								, PRICING_FACTOR
								, PRICING_FORMULA
								, SUPPORT_NAME
								, SUPPORT_CONTACT
								, SUPPORT_DESCRIPTION
								, SUPPORT_TERM
								, DESCRIPTION
								, TEAM_ID
								, TEAM_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProductActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

