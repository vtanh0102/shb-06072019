/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Users
{
	/// <summary>
	///		Summary description for ProcessHistoryView.
	/// </summary>
	public class ProcessHistoryView : SplendidControl
	{
		protected Label    lblError    ;
		protected Guid     gPROCESS_ID ;
		protected DataView vwMain      ;
		protected Repeater rptMain     ;

		public string TimeFromNow(DateTime dt)
		{
			TimeSpan ts = DateTime.Now - dt;
			return Sql.FormatTimeSpan(ts, L10n);
		}

		private void Bind()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					Guid gBUSINESS_PROCESS_INSTANCE_ID = Guid.Empty;
					sSQL = "select *          " + ControlChars.CrLf
					     + "  from vwPROCESSES" + ControlChars.CrLf
					     + " where ID = @ID   " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@ID", gPROCESS_ID);
						using ( IDataReader rdr = cmd.ExecuteReader() )
						{
							if ( rdr.Read() )
							{
								string sPROCESS_NUMBER = Sql.ToString(rdr["PROCESS_NUMBER"]);
								string sPARENT_NAME    = Sql.ToString(rdr["PARENT_NAME"   ]);
								gBUSINESS_PROCESS_INSTANCE_ID = Sql.ToGuid(rdr["BUSINESS_PROCESS_INSTANCE_ID"]);
								SetPageTitle(String.Format(L10n.Term("Processes.LBL_MY_PROCESSES_NAME_FORMAT"), sPROCESS_NUMBER, sPARENT_NAME));
							}
						}
					}
					sSQL = "select *                       " + ControlChars.CrLf
					     + "  from vwPROCESSES_HISTORY     " + ControlChars.CrLf
					     + " where BUSINESS_PROCESS_INSTANCE_ID = @BUSINESS_PROCESS_INSTANCE_ID" + ControlChars.CrLf
					     + " order by DATE_ENTERED         " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@BUSINESS_PROCESS_INSTANCE_ID", gBUSINESS_PROCESS_INSTANCE_ID);
						
						if ( bDebug )
							RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));
						
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								dt.Columns.Add("DESCRIPTION", typeof(System.String));
								if ( dt.Rows.Count > 0 )
								{
									DataRow row = dt.Rows[0];
									SetPageTitle(String.Format(L10n.Term("Processes.LBL_MY_PROCESSES_NAME_FORMAT"), Sql.ToString(row["PROCESS_NUMBER"]), Sql.ToString(row["PARENT_NAME"])));
								}
								foreach ( DataRow row in dt.Rows )
								{
									string sPROCESS_ACTION     = Sql.ToString(row["PROCESS_ACTION"    ]);
									string sCREATED_BY_NAME    = Sql.ToString(row["CREATED_BY_NAME"   ]);
									string sACTIVITY_NAME      = Sql.ToString(row["ACTIVITY_NAME"     ]);
									string sASSIGNED_USER_NAME = Sql.ToString(row["ASSIGNED_USER_NAME"]);
									string sPROCESS_USER_NAME  = Sql.ToString(row["PROCESS_USER_NAME" ]);
									if ( Sql.IsEmptyString(sCREATED_BY_NAME) )
										sCREATED_BY_NAME = "Process Manager";
									switch ( sPROCESS_ACTION )
									{
										case "Assign":
											// <b>{0}</b> has been assigned to activity "{1}".
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_ASSIGNED_FORMAT"), sCREATED_BY_NAME, sACTIVITY_NAME);
											break;
										case "Approve":
											// <b>{0}</b> has approved activity "{1}".
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_APPROVE_FORMAT"), sCREATED_BY_NAME, sACTIVITY_NAME);
											break;
										case "Reject":
											// <b>{0}</b> has rejected activity "{1}".
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_REJECT_FORMAT"), sCREATED_BY_NAME, sACTIVITY_NAME);
											break;
										case "Claim":
											// <b>{0}</b> has claimed activity "{1}".
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_CLAIM_FORMAT"), sCREATED_BY_NAME, sACTIVITY_NAME);
											break;
										case "Route":
											// <b>{0}</b> has routed activity "{1}".
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_ROUTE_FORMAT"), sCREATED_BY_NAME, sACTIVITY_NAME);
											break;
										case "ChangeAssignedUser":
											// <b>{0}</b> has changed the assigned user to <b>{1}</b>.
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_CHANGE_ASSIGNED_USER_FORMAT"), sCREATED_BY_NAME, sASSIGNED_USER_NAME);
											break;
										case "ChangeProcessUser":
											// <b>{0}</b> has changed the process user to <b>{1}</b>.
											row["DESCRIPTION"] = String.Format(L10n.Term("Processes.LBL_HISTORY_CHANGE_PROCESS_USER_FORMAT"), sCREATED_BY_NAME, sPROCESS_USER_NAME);
											break;
									}
								}
								vwMain = new DataView(dt);
								rptMain.DataSource = vwMain;
								rptMain.DataBind();
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_PROCESS_HISTORY"));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
				return;
			
			gPROCESS_ID = Sql.ToGuid(Request["PROCESS_ID"]);
			if ( !IsPostBack )
			{
				Bind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "Processes";
		}
		#endregion
	}
}

