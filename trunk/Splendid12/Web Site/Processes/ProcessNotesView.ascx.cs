/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Users
{
	/// <summary>
	///		Summary description for ProcessNotesView.
	/// </summary>
	public class ProcessNotesView : SplendidControl
	{
		protected Label    lblError    ;
		protected TextBox  txtNOTES    ;
		protected Guid     gPROCESS_ID ;
		protected DataView vwMain      ;
		protected Repeater rptMain     ;

		public string TimeFromNow(DateTime dt)
		{
			TimeSpan ts = DateTime.Now - dt;
			return Sql.FormatTimeSpan(ts, L10n);
		}

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Processes.AddNotes" )
				{
					string sNOTES = txtNOTES.Text.Trim();
					if ( !Sql.IsEmptyString(sNOTES) )
					{
						SqlProcs.spPROCESSES_NOTES_InsertOnly(gPROCESS_ID, sNOTES);
					}
					Response.Redirect(Request.RawUrl);
				}
				else if ( e.CommandName == "Processes.Delete" )
				{
					Guid gID = Sql.ToGuid(e.CommandArgument);
					SqlProcs.spPROCESSES_NOTES_Delete(gID);
					Response.Redirect(Request.RawUrl);
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void Bind()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					Guid gBUSINESS_PROCESS_INSTANCE_ID = Guid.Empty;
					sSQL = "select *          " + ControlChars.CrLf
					     + "  from vwPROCESSES" + ControlChars.CrLf
					     + " where ID = @ID   " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@ID", gPROCESS_ID);
						using ( IDataReader rdr = cmd.ExecuteReader() )
						{
							if ( rdr.Read() )
							{
								string sPROCESS_NUMBER = Sql.ToString(rdr["PROCESS_NUMBER"]);
								string sPARENT_NAME    = Sql.ToString(rdr["PARENT_NAME"   ]);
								gBUSINESS_PROCESS_INSTANCE_ID = Sql.ToGuid(rdr["BUSINESS_PROCESS_INSTANCE_ID"]);
								SetPageTitle(String.Format(L10n.Term("Processes.LBL_MY_PROCESSES_NAME_FORMAT"), sPROCESS_NUMBER, sPARENT_NAME));
							}
						}
					}
					sSQL = "select *                       " + ControlChars.CrLf
					     + "  from vwPROCESSES_NOTES       " + ControlChars.CrLf
					     + " where BUSINESS_PROCESS_INSTANCE_ID = @BUSINESS_PROCESS_INSTANCE_ID" + ControlChars.CrLf
					     + " order by DATE_ENTERED         " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@BUSINESS_PROCESS_INSTANCE_ID", gBUSINESS_PROCESS_INSTANCE_ID);
						
						if ( bDebug )
							RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));
						
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								vwMain = new DataView(dt);
								rptMain.DataSource = vwMain;
								rptMain.DataBind();
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_PROCESS_HISTORY"));
			this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
				return;
			
			gPROCESS_ID = Sql.ToGuid(Request["PROCESS_ID"]);
			if ( !IsPostBack )
			{
				Bind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			m_sMODULE = "Processes";
		}
		#endregion
	}
}

