﻿/*
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;

namespace SplendidCRM.Processes
{
	[ServiceContract]
	[ServiceBehavior( IncludeExceptionDetailInFaults = true )]
	[AspNetCompatibilityRequirements( RequirementsMode = AspNetCompatibilityRequirementsMode.Required )]
	public class Rest
	{
		#region json utils
		// http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
		private static long UnixTicks(DateTime dt)
		{
			return ( dt.Ticks - 621355968000000000 ) / 10000;
		}

		private static string ToJsonDate(object dt)
		{
			return "\\/Date(" + UnixTicks( Sql.ToDateTime( dt ) ).ToString() + ")\\/";
		}

		// 08/03/2012 Paul.  FromJsonDate is used on Web Capture services. 
		public static DateTime FromJsonDate(string s)
		{
			DateTime dt = DateTime.MinValue;
			if ( s.StartsWith( "\\/Date(" ) && s.EndsWith( ")\\/" ) )
			{
				s = s.Replace( "\\/Date(", "" );
				s = s.Replace( ")\\/", "" );
				long lEpoch = Sql.ToLong( s );
				dt = new DateTime( lEpoch * 10000 + 621355968000000000 );
			}
			else
			{
				dt = Sql.ToDateTime( s );
			}
			return dt;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		// http://schotime.net/blog/index.php/2008/07/27/dataset-datatable-to-json/
		private static List<Dictionary<string, object>> RowsToDictionary(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
			// 10/11/2012 Paul.  dt will be null when no results security filter applied. 
			if ( dt != null )
			{
				foreach ( DataRow dr in dt.Rows )
				{
					// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
					Dictionary<string, object> drow = new Dictionary<string, object>();
					if ( dt.Columns.Contains( "ID" ) )
					{
						Guid gID = Sql.ToGuid( dr["ID"] );
						if ( !Sql.IsEmptyString( sBaseURI ) && !Sql.IsEmptyString( sModuleName ) )
						{
							Dictionary<string, object> metadata = new Dictionary<string, object>();
							metadata.Add( "uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "" );
							metadata.Add( "type", "SplendidCRM." + sModuleName );
							if ( dr.Table.Columns.Contains( "DATE_MODIFIED_UTC" ) )
							{
								DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime( dr["DATE_MODIFIED_UTC"] );
								metadata.Add( "etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
							}
							drow.Add( "__metadata", metadata );
						}
					}

					for ( int i = 0; i < dt.Columns.Count; i++ )
					{
						if ( dt.Columns[i].DataType.FullName == "System.DateTime" )
						{
							// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
							drow.Add( dt.Columns[i].ColumnName, ToJsonDate( T10n.FromServerTime( dr[i] ) ) );
						}
						else
						{
							drow.Add( dt.Columns[i].ColumnName, dr[i] );
						}
					}
					objs.Add( drow );
				}
			}
			return objs;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			results.Add("results", RowsToDictionary(sBaseURI, sModuleName, dt, T10n));
			d.Add("d", results);
			// 04/21/2017 Paul.  Count should be returend as a number. 
			if ( dt != null )
				d.Add("__count", dt.Rows.Count);
			return d;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataRow dr, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			Dictionary<string, object> drow = new Dictionary<string, object>();

			// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
			if ( dr.Table.Columns.Contains( "ID" ) )
			{
				Guid gID = Sql.ToGuid( dr["ID"] );
				if ( !Sql.IsEmptyString( sBaseURI ) && !Sql.IsEmptyString( sModuleName ) )
				{
					Dictionary<string, object> metadata = new Dictionary<string, object>();
					metadata.Add( "uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "" );
					metadata.Add( "type", "SplendidCRM." + sModuleName );
					if ( dr.Table.Columns.Contains( "DATE_MODIFIED_UTC" ) )
					{
						DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime( dr["DATE_MODIFIED_UTC"] );
						metadata.Add( "etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
					}
					drow.Add( "__metadata", metadata );
				}
			}

			for ( int i = 0; i < dr.Table.Columns.Count; i++ )
			{
				if ( dr.Table.Columns[i].DataType.FullName == "System.DateTime" )
				{
					// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
					drow.Add( dr.Table.Columns[i].ColumnName, ToJsonDate( T10n.FromServerTime( dr[i] ) ) );
				}
				else
				{
					drow.Add( dr.Table.Columns[i].ColumnName, dr[i] );
				}
			}

			results.Add( "results", drow );
			d.Add( "d", results );
			return d;
		}

		private static string ConvertODataFilter(string sFILTER, IDbCommand cmd)
		{
			// Logical Operators
			sFILTER = sFILTER.Replace(" eq true" , " eq 1");
			sFILTER = sFILTER.Replace(" eq false", " eq 0");
			sFILTER = sFILTER.Replace(" ne true" , " ne 1");
			sFILTER = sFILTER.Replace(" ne false", " ne 0");
			sFILTER = sFILTER.Replace(" gt ", " > ");
			sFILTER = sFILTER.Replace(" lt ", " < ");
			sFILTER = sFILTER.Replace(" eq ", " = ");
			sFILTER = sFILTER.Replace(" ne ", " <> ");
			// Arithmetic Operators
			sFILTER = sFILTER.Replace(" add ", " + ");
			sFILTER = sFILTER.Replace(" sub ", " - ");
			sFILTER = sFILTER.Replace(" mul ", " * ");
			sFILTER = sFILTER.Replace(" div ", " / ");
			sFILTER = sFILTER.Replace(" mod ", " % ");
			// Date Functions
			if ( Sql.IsSQLServer(cmd) )
			{
				//sFILTER = sFILTER.Replace("year("  , "dbo.fnDatePart('year', "  );
				//sFILTER = sFILTER.Replace("month(" , "dbo.fnDatePart('month', " );
				//sFILTER = sFILTER.Replace("day("   , "dbo.fnDatePart('day', "   );
				sFILTER = sFILTER.Replace("hour("  , "dbo.fnDatePart('hour', "  );
				sFILTER = sFILTER.Replace("minute(", "dbo.fnDatePart('minute', ");
				sFILTER = sFILTER.Replace("second(", "dbo.fnDatePart('second', ");
			}
			else
			{
				//sFILTER = sFILTER.Replace("year("  , "fnDatePart('year', "  );
				//sFILTER = sFILTER.Replace("month(" , "fnDatePart('month', " );
				//sFILTER = sFILTER.Replace("day("   , "fnDatePart('day', "   );
				sFILTER = sFILTER.Replace("hour("  , "fnDatePart('hour', "  );
				sFILTER = sFILTER.Replace("minute(", "fnDatePart('minute', ");
				sFILTER = sFILTER.Replace("second(", "fnDatePart('second', ");
			}
			// Math Functions
			int nStart = sFILTER.IndexOf("round(");
			while ( nStart > 0 )
			{
				int nEnd = sFILTER.IndexOf(")", nStart);
				if ( nEnd > 0 )
				{
					sFILTER = sFILTER.Substring(0, nEnd - 1) + ", 0" + sFILTER.Substring(nEnd - 1);
				}
				nStart = sFILTER.IndexOf("round(", nStart + 1);
			}
			// String Functions
			sFILTER = sFILTER.Replace("tolower(", "lower(");
			sFILTER = sFILTER.Replace("toupper(", "upper(");
			if ( Sql.IsSQLServer(cmd) )
			{
				sFILTER = sFILTER.Replace("length("     , "len(");
				sFILTER = sFILTER.Replace("trim("       , "dbo.fnTrim(");
				sFILTER = sFILTER.Replace("concat("     , "dbo.fnConcat(");
				sFILTER = sFILTER.Replace("startswith(" , "dbo.fnStartsWith(");
				sFILTER = sFILTER.Replace("endswith("   , "dbo.fnEndsWith(");
				sFILTER = sFILTER.Replace("indexof("    , "dbo.fnIndexOf(");
				sFILTER = sFILTER.Replace("substringof(", "dbo.fnSubstringOf(");
			}
			return sFILTER;
		}

		#endregion

		[OperationContract]
		[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Stream GetProcessStatus(Guid ID)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			HttpContext.Current.Response.ExpiresAbsolute = new DateTime(1980, 1, 1, 0, 0, 0, 0);
			WebOperationContext.Current.OutgoingResponse.Headers.Add( "Cache-Control", "no-cache" );
			WebOperationContext.Current.OutgoingResponse.Headers.Add( "Pragma", "no-cache" );
			
			L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
			if ( !Security.IsAuthenticated() )
			{
				throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
			}
			// 08/20/2014 Paul.  We need to continually update the SplendidSession so that it expires along with the ASP.NET Session. 
			SplendidSession.CreateSession(HttpContext.Current.Session);
			
			DataTable dt = new DataTable();
			dt.Columns.Add("PENDING_PROCESS_ID", typeof(System.Guid   ));
			dt.Columns.Add("ProcessStatus"     , typeof(System.String ));
			dt.Columns.Add("ShowApprove"       , typeof(System.Boolean));
			dt.Columns.Add("ShowReject"        , typeof(System.Boolean));
			dt.Columns.Add("ShowRoute"         , typeof(System.Boolean));
			dt.Columns.Add("ShowClaim"         , typeof(System.Boolean));
			dt.Columns.Add("USER_TASK_TYPE"    , typeof(System.String ));
			dt.Columns.Add("PROCESS_USER_ID"   , typeof(System.Guid   ));
			dt.Columns.Add("ASSIGNED_TEAM_ID"  , typeof(System.Guid   ));
			dt.Columns.Add("PROCESS_TEAM_ID"   , typeof(System.Guid   ));
			
			Guid gPENDING_PROCESS_ID = ID;
			string sProcessStatus    = String.Empty;
			bool   bShowApprove      = false;
			bool   bShowReject       = false;
			bool   bShowRoute        = false;
			bool   bShowClaim        = false;
			string sUSER_TASK_TYPE   = String.Empty;
			Guid   gPROCESS_USER_ID  = Guid.Empty;
			Guid   gASSIGNED_TEAM_ID = Guid.Empty;
			Guid   gPROCESS_TEAM_ID  = Guid.Empty;
			bool bFound = WF4ApprovalActivity.GetProcessStatus(Application, L10n, gPENDING_PROCESS_ID, ref sProcessStatus, ref bShowApprove, ref bShowReject, ref bShowRoute, ref bShowClaim, ref sUSER_TASK_TYPE, ref gPROCESS_USER_ID, ref gASSIGNED_TEAM_ID, ref gPROCESS_TEAM_ID);
			{
				DataRow row = dt.NewRow();
				dt.Rows.Add(row);
				row["PENDING_PROCESS_ID"] = gPENDING_PROCESS_ID;
				row["ProcessStatus"     ] = sProcessStatus     ;
				row["ShowApprove"       ] = bShowApprove       ;
				row["ShowReject"        ] = bShowReject        ;
				row["ShowRoute"         ] = bShowRoute         ;
				row["ShowClaim"         ] = bShowClaim         ;
				row["USER_TASK_TYPE"    ] = sUSER_TASK_TYPE    ;
				row["PROCESS_USER_ID"   ] = gPROCESS_USER_ID   ;
				row["ASSIGNED_TEAM_ID"  ] = gASSIGNED_TEAM_ID  ;
				row["PROCESS_TEAM_ID"   ] = gPROCESS_TEAM_ID   ;
			}
			
			string sBaseURI = String.Empty;
			JavaScriptSerializer json = new JavaScriptSerializer();
			json.MaxJsonLength = int.MaxValue;
			
			Guid     gTIMEZONE         = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			string sResponse = json.Serialize(ToJson(sBaseURI, "Processes", dt, T10n));
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		[OperationContract]
		// 03/13/2011 Paul.  Must use octet-stream instead of json, outherwise we get the following error. 
		// Incoming message for operation 'CreateRecord' (contract 'AddressService' with namespace 'http://tempuri.org/') contains an unrecognized http body format value 'Json'. 
		// The expected body format value is 'Raw'. This can be because a WebContentTypeMapper has not been configured on the binding. See the documentation of WebContentTypeMapper for more details.
		//xhr.setRequestHeader('content-type', 'application/octet-stream');
		public void ProcessAction(Stream input)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
			if ( !Security.IsAuthenticated() )
			{
				throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
			}
			// 08/20/2014 Paul.  We need to continually update the SplendidSession so that it expires along with the ASP.NET Session. 
			SplendidSession.CreateSession(HttpContext.Current.Session);
			
			string sRequest = String.Empty;
			using ( StreamReader stmRequest = new StreamReader(input, System.Text.Encoding.UTF8) )
			{
				sRequest = stmRequest.ReadToEnd();
			}
			JavaScriptSerializer json = new JavaScriptSerializer();
			json.MaxJsonLength = int.MaxValue;
			Dictionary<string, object> dict = json.Deserialize<Dictionary<string, object>>(sRequest);
			
			string sACTION             = (dict.ContainsKey("ACTION"            ) ? Sql.ToString(dict["ACTION"            ]) : String.Empty);
			Guid   gPENDING_PROCESS_ID = (dict.ContainsKey("PENDING_PROCESS_ID") ? Sql.ToGuid  (dict["PENDING_PROCESS_ID"]) : Guid.Empty  );
			Guid   gPROCESS_USER_ID    = (dict.ContainsKey("PROCESS_USER_ID"   ) ? Sql.ToGuid  (dict["PROCESS_USER_ID"   ]) : Guid.Empty  );
			string sPROCESS_NOTES      = (dict.ContainsKey("PROCESS_NOTES"     ) ? Sql.ToString(dict["PROCESS_NOTES"     ]) : String.Empty);
			if ( Sql.IsEmptyGuid(gPENDING_PROCESS_ID) )
			{
				throw(new Exception("PENDING_PROCESS_ID is empty"));
			}
			
			string sProcessStatus    = String.Empty;
			bool   bShowApprove      = false;
			bool   bShowReject       = false;
			bool   bShowRoute        = false;
			bool   bShowClaim        = false;
			string sUSER_TASK_TYPE   = String.Empty;
			Guid   gEXISTING_USER_ID = Guid.Empty;
			Guid   gASSIGNED_TEAM_ID = Guid.Empty;
			Guid   gPROCESS_TEAM_ID  = Guid.Empty;
			bool bFound = WF4ApprovalActivity.GetProcessStatus(Application, L10n, gPENDING_PROCESS_ID, ref sProcessStatus, ref bShowApprove, ref bShowReject, ref bShowRoute, ref bShowClaim, ref sUSER_TASK_TYPE, ref gEXISTING_USER_ID, ref gASSIGNED_TEAM_ID, ref gPROCESS_TEAM_ID);
			if ( bFound )
			{
				switch ( sACTION )
				{
					case "Approve":
						if ( bShowApprove )
							WF4ApprovalActivity.Approve(Application, L10n, gPENDING_PROCESS_ID, Security.USER_ID);
						else
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					case "Reject":
						if ( bShowReject )
							WF4ApprovalActivity.Reject(Application, gPENDING_PROCESS_ID, Security.USER_ID);
						else
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					case "Route":
						if ( bShowRoute )
							WF4ApprovalActivity.Route(Application, L10n, gPENDING_PROCESS_ID, Security.USER_ID);
						else
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					case "Claim":
						if ( bShowClaim )
							WF4ApprovalActivity.Claim(Application, gPENDING_PROCESS_ID, Security.USER_ID);
						else
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					case "Cancel":
						WF4ApprovalActivity.Cancel(Application, gPENDING_PROCESS_ID, Security.USER_ID);
						break;
					case "ChangeProcessUser":
						if ( !Sql.IsEmptyGuid(gPROCESS_TEAM_ID) && !Sql.IsEmptyGuid(gPROCESS_USER_ID) )
							WF4ApprovalActivity.ChangeProcessUser(Application, gPENDING_PROCESS_ID, gPROCESS_USER_ID, sPROCESS_NOTES);
						else if ( Sql.IsEmptyGuid(gPROCESS_USER_ID) )
							throw(new Exception("PROCESS_USER_ID was not specified."));
						else if ( Sql.IsEmptyGuid(gPROCESS_TEAM_ID) )
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					case "ChangeAssignedUser":
						if ( !Sql.IsEmptyGuid(gASSIGNED_TEAM_ID) && !Sql.IsEmptyGuid(gPROCESS_USER_ID) )
							WF4ApprovalActivity.ChangeAssignedUser(Application, gPENDING_PROCESS_ID, gPROCESS_USER_ID, sPROCESS_NOTES);
						else if ( Sql.IsEmptyGuid(gPROCESS_USER_ID) )
							throw(new Exception("PROCESS_USER_ID was not specified."));
						else if ( Sql.IsEmptyGuid(gPROCESS_TEAM_ID) )
							throw(new Exception(sACTION + " is an unsupported action."));
						break;
					default:
						throw(new Exception(sACTION + " is an unsupported action."));
				}
			}
			else
			{
				throw(new Exception(gPENDING_PROCESS_ID.ToString() + " is not available."));
			}
		}

		[OperationContract]
		[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Stream ProcessUsers()
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			HttpContext.Current.Response.ExpiresAbsolute = new DateTime(1980, 1, 1, 0, 0, 0, 0);
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Cache-Control", "no-cache");
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Pragma", "no-cache");
			
			Guid   gTEAM_ID  = Sql.ToGuid   (Request.QueryString["TEAM_ID" ]);
			int    nSKIP     = Sql.ToInteger(Request.QueryString["$skip"   ]);
			int    nTOP      = Sql.ToInteger(Request.QueryString["$top"    ]);
			string sFILTER   = Sql.ToString (Request.QueryString["$filter" ]);
			string sORDER_BY = Sql.ToString (Request.QueryString["$orderby"]);
			string sGROUP_BY = Sql.ToString (Request.QueryString["$groupby"]);
			string sSELECT   = Sql.ToString (Request.QueryString["$select" ]);
			Regex r = new Regex(@"[^A-Za-z0-9_]");
			string sFILTER_KEYWORDS = (" " + r.Replace(sFILTER, " ") + " ").ToLower();
			if ( sFILTER_KEYWORDS.Contains(" select ") )
			{
				throw(new Exception("Subqueries are not allowed."));
			}
			if ( sFILTER.Contains(";") )
			{
				throw(new Exception("A semicolon is not allowed anywhere in a filter. "));
			}
			if ( sORDER_BY.Contains(";") )
			{
				throw(new Exception("A semicolon is not allowed anywhere in a sort expression. "));
			}
			if ( !Security.IsAuthenticated() )
			{
				L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
				throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
			}
			SplendidSession.CreateSession(HttpContext.Current.Session);
			
			UniqueStringCollection arrSELECT = new UniqueStringCollection();
			sSELECT = sSELECT.Replace(" ", "");
			if ( !Sql.IsEmptyString(sSELECT) )
			{
				foreach ( string s in sSELECT.Split(',') )
				{
					string sColumnName = r.Replace(s, "");
					if ( !Sql.IsEmptyString(sColumnName) )
						arrSELECT.Add(sColumnName);
				}
			}
			
			DataTable dt = GetTable("vwUSERS_ASSIGNED_TO_List", gTEAM_ID, nSKIP, nTOP, sFILTER, sORDER_BY, sGROUP_BY, arrSELECT);
			
			string sBaseURI = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace("/GetModuleTable", "/GetModuleItem");
			JavaScriptSerializer json = new JavaScriptSerializer();
			json.MaxJsonLength = int.MaxValue;
			
			Guid     gTIMEZONE         = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			string sResponse = json.Serialize(ToJson(sBaseURI, "Users", dt, T10n));
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		private DataTable GetTable(string sTABLE_NAME, Guid gTEAM_ID, int nSKIP, int nTOP, string sFILTER, string sORDER_BY, string sGROUP_BY, UniqueStringCollection arrSELECT)
		{
			HttpContext          Context     = HttpContext.Current;
			HttpSessionState     Session     = HttpContext.Current.Session;
			HttpApplicationState Application = HttpContext.Current.Application;
			DataTable dt = null;
			try
			{
				if ( Security.IsAuthenticated() )
				{
					string sMATCH_NAME = String.Empty;
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					Regex r = new Regex(@"[^A-Za-z0-9_]");
					sTABLE_NAME = r.Replace(sTABLE_NAME, "");
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						using ( DataTable dtSYNC_TABLES = SplendidCache.RestTables(sTABLE_NAME, false) )
						{
							string sSQL = String.Empty;
							bool bEnableTeamHierarchy = Crm.Config.enable_team_hierarchy();
							if ( dtSYNC_TABLES != null && dtSYNC_TABLES.Rows.Count > 0 )
							{
								DataRow rowSYNC_TABLE = dtSYNC_TABLES.Rows[0];
								string sMODULE_NAME         = Sql.ToString (rowSYNC_TABLE["MODULE_NAME"        ]);
								string sVIEW_NAME           = Sql.ToString (rowSYNC_TABLE["VIEW_NAME"          ]);
								bool   bHAS_CUSTOM          = Sql.ToBoolean(rowSYNC_TABLE["HAS_CUSTOM"         ]);
								int    nMODULE_SPECIFIC     = Sql.ToInteger(rowSYNC_TABLE["MODULE_SPECIFIC"    ]);
								string sMODULE_FIELD_NAME   = Sql.ToString (rowSYNC_TABLE["MODULE_FIELD_NAME"  ]);
								bool   bIS_RELATIONSHIP     = Sql.ToBoolean(rowSYNC_TABLE["IS_RELATIONSHIP"    ]);
								string sMODULE_NAME_RELATED = Sql.ToString (rowSYNC_TABLE["MODULE_NAME_RELATED"]);
								string sASSIGNED_FIELD_NAME = Sql.ToString (rowSYNC_TABLE["ASSIGNED_FIELD_NAME"]);
								bool   bIS_SYSTEM           = Sql.ToBoolean(rowSYNC_TABLE["IS_SYSTEM"          ]);
								sTABLE_NAME                 = Sql.ToString (rowSYNC_TABLE["TABLE_NAME"         ]);
								sTABLE_NAME        = r.Replace(sTABLE_NAME       , "");
								sVIEW_NAME         = r.Replace(sVIEW_NAME        , "");
								sMODULE_FIELD_NAME = r.Replace(sMODULE_FIELD_NAME, "");
								
								if ( arrSELECT != null && arrSELECT.Count > 0 )
								{
									foreach ( string sColumnName in arrSELECT )
									{
										if ( Sql.IsEmptyString(sSQL) )
											sSQL += "select " + sVIEW_NAME + "." + sColumnName + ControlChars.CrLf;
										else
											sSQL += "     , " + sVIEW_NAME + "." + sColumnName + ControlChars.CrLf;
									}
								}
								else
								{
									sSQL = "select " + sVIEW_NAME + ".*" + ControlChars.CrLf;
								}
								sSQL += "  from " + sVIEW_NAME        + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									cmd.CommandTimeout = 0;
									if ( !bEnableTeamHierarchy )
									{
										cmd.CommandText += " inner join vwTEAM_MEMBERSHIPS" + ControlChars.CrLf;
										cmd.CommandText += "         on vwTEAM_MEMBERSHIPS.MEMBERSHIP_TEAM_ID = @TEAM_ID                   " + ControlChars.CrLf;
										// 01/07/2018 Paul.  Specify the view name, even though it is hard-coded to be vwUSERS_ASSIGNED_TO_List. 
										cmd.CommandText += "        and vwTEAM_MEMBERSHIPS.MEMBERSHIP_USER_ID = " + sVIEW_NAME + ".ID" + ControlChars.CrLf;
									}
									else
									{
										if ( Sql.IsOracle(cmd) )
										{
											cmd.CommandText += " inner join table(fnTEAM_HIERARCHY_USERS(@TEAM_ID)) vwTEAM_MEMBERSHIPS         " + ControlChars.CrLf;
											// 01/07/2018 Paul.  Specify the view name, even though it is hard-coded to be vwUSERS_ASSIGNED_TO_List. 
											cmd.CommandText += "         on vwTEAM_MEMBERSHIPS.MEMBERSHIP_USER_ID = " + sVIEW_NAME + ".ID" + ControlChars.CrLf;
										}
										else
										{
											string fnPrefix = (Sql.IsSQLServer(cmd) ? "dbo." : String.Empty);
											cmd.CommandText += " inner join " + fnPrefix + "fnTEAM_HIERARCHY_USERS(@TEAM_ID) vwTEAM_MEMBERSHIPS" + ControlChars.CrLf;
											// 01/07/2018 Paul.  Specify the view name, even though it is hard-coded to be vwUSERS_ASSIGNED_TO_List. 
											cmd.CommandText += "         on vwTEAM_MEMBERSHIPS.MEMBERSHIP_USER_ID = " + sVIEW_NAME + ".ID" + ControlChars.CrLf;
										}
									}
									Sql.AddParameter(cmd, "@TEAM_ID", gTEAM_ID);
									Security.Filter(cmd, sMODULE_NAME, "view");
									if ( !Sql.IsEmptyString(sFILTER) )
									{
										string sSQL_FILTER = ConvertODataFilter(sFILTER, cmd);
										cmd.CommandText += "   and (" + sSQL_FILTER + ")" + ControlChars.CrLf;;
									}
									if ( Sql.IsEmptyString(sORDER_BY.Trim()) )
									{
										sORDER_BY = " order by " + sVIEW_NAME + ".DATE_MODIFIED_UTC" + ControlChars.CrLf;
									}
									else
									{
										r = new Regex(@"[^A-Za-z0-9_, ]");
										sORDER_BY = " order by " + r.Replace(sORDER_BY, "");
									}
									if ( !Sql.IsEmptyString(sGROUP_BY) )
									{
										r = new Regex(@"[^A-Za-z0-9_, ]");
										sGROUP_BY = " group by " + r.Replace(sGROUP_BY, "");
									}
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										dt = new DataTable(sTABLE_NAME);
										if ( nTOP > 0 )
										{
											if ( nSKIP > 0 )
											{
												int nCurrentPageIndex = nSKIP / nTOP;
												Sql.PageResults(cmd, sTABLE_NAME, sORDER_BY, nCurrentPageIndex, nTOP);
												da.Fill(dt);
											}
											else
											{
												cmd.CommandText += sGROUP_BY + sORDER_BY;
												using ( DataSet ds = new DataSet() )
												{
													ds.Tables.Add(dt);
													da.Fill(ds, 0, nTOP, sTABLE_NAME);
												}
											}
										}
										else
										{
											cmd.CommandText += sGROUP_BY + sORDER_BY;
											da.Fill(dt);
										}
										if ( SplendidInit.bEnableACLFieldSecurity && !Sql.IsEmptyString(sMODULE_NAME) )
										{
											bool bApplyACL = false;
											foreach ( DataRow row in dt.Rows )
											{
												Guid gASSIGNED_USER_ID = Guid.Empty;
												foreach ( DataColumn col in dt.Columns )
												{
													Security.ACL_FIELD_ACCESS acl = Security.GetUserFieldSecurity(sMODULE_NAME, col.ColumnName, gASSIGNED_USER_ID);
													if ( !acl.IsReadable() )
													{
														row[col.ColumnName] = DBNull.Value;
														bApplyACL = true;
													}
												}
											}
											if ( bApplyACL )
												dt.AcceptChanges();
										}
									}
								}
							}
							else
							{
								SplendidError.SystemError(new StackTrace(true).GetFrame(0), sTABLE_NAME + " cannot be accessed.");
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				string sMessage = "GetTable(" + sTABLE_NAME + ", " + sFILTER + ", " + sORDER_BY + ") " + ex.Message;
				SplendidError.SystemMessage("Error", new StackTrace(true).GetFrame(0), sMessage);
				throw(new Exception(sMessage));
			}
			return dt;
		}
	}
}
