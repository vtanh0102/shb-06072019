<%@ Control CodeBehind="ProcessNotesView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Users.ProcessNotesView" %>
<script runat="server">
/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divProcessNotesView">
	<asp:Label ID="lblError" CssClass="error" EnableViewState="false" runat="server" />
	<div style="display: table; width: 100%;">
		<div style="display: table-row;">
			<div style="display: table-cell; width: 85%;">
				<asp:TextBox ID="txtNOTES" TextMode="MultiLine" Rows="2" Width="98%" runat="server" />
			</div>
			<div style="display: table-cell; width: 15%; text-align: left; vertical-align: middle;">
				<asp:Button Text='<%# L10n.Term("Processes.LBL_ADD_NOTES") %>' OnCommand="Page_Command" CommandName="Processes.AddNotes" CssClass="ProcessAddNotes" runat="server" />
			</div>
		</div>
	</div>
	<br />

	<div style="display: table; width: 100%;">
	<asp:Repeater ID="rptMain" runat="server">
		<ItemTemplate>
		<div style="display: table-row;">
			<div style="display: table-cell; width: 1%; vertical-align: top; padding-top: 6px; padding-right: 4px;">
				<asp:Image Visible='<%# !Sql.IsEmptyString(Eval("PICTURE")) %>' ImageUrl='<%# Eval("PICTURE") %>' Width="36px" Height="36px" style="border-radius: 4px;" runat="server" />
				<asp:Image Visible='<%#  Sql.IsEmptyString(Eval("PICTURE")) %>' ImageUrl='~/Include/images/SplendidCRM_Icon.gif' Height="36px" style="border-radius: 4px;" runat="server" />
			</div>
			<div style="display: table-cell; width: 85%; vertical-align: top; padding-top: 6px; padding-right: 4px;">
				<div><b><%# Eval("CREATED_BY_NAME") %></b> - <%# Eval("DESCRIPTION") %></div>
				<div style="color: #777; padding-top: 4px;"><%# Eval("DATE_ENTERED") %></div>
			</div>
			<div style="display: table-cell; width: 14%; vertical-align: top; padding-top: 6px;">
				<div><%# TimeFromNow(Sql.ToDateTime(Eval("DATE_ENTERED"))) %></div>
				<div><asp:LinkButton CommandName="Processes.Delete" CommandArgument='<%# Eval("ID") %>' Text='<%# L10n.Term(".LBL_DELETE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_DELETE_BUTTON_TITLE") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" runat="server" /></div>
			</div>
		</div>
		</ItemTemplate>
	</asp:Repeater>
	</div>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
	<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
</div>

