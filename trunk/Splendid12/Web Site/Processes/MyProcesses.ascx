<%@ Control CodeBehind="MyProcesses.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.Processes.MyProcesses" %>
<script runat="server">
/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divProcessesMyProcesses">
	<%@ Register TagPrefix="SplendidCRM" Tagname="DashletHeader" Src="~/_controls/DashletHeader.ascx" %>
	<SplendidCRM:DashletHeader ID="ctlDashletHeader" Title="Processes.LBL_LIST_MY_PROCESSES" DivEditName="my_processes_edit" Runat="Server" />
	
	<div id="my_processes_edit" style="DISPLAY: <%= bShowEditDialog ? "inline" : "none" %>">
	</div>
	<asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
		<asp:Label ID="lblError" CssClass="error" EnableViewState="false" Runat="server" />
	</asp:Panel>
	<script type="text/javascript">
	function ActivateMyProcessTab(bSelfService)
	{
		var my_processes   = document.getElementById('my_processes'  );
		var my_selfservice = document.getElementById('my_selfservice');
		my_processes.style.display   = !bSelfService ? 'inline' : 'none';
		my_selfservice.style.display =  bSelfService ? 'inline' : 'none';

		var my_processes_count_panel   = document.getElementById('my_processes_count_panel'  );
		var my_selfservice_count_panel = document.getElementById('my_selfservice_count_panel');
		my_processes_count_panel.className   = !bSelfService ? 'MyProcessActiveTab' : 'MyProcessInactiveTab';
		my_selfservice_count_panel.className =  bSelfService ? 'MyProcessActiveTab' : 'MyProcessInactiveTab';
	}
	</script>
	<%-- 06/15/2017 Paul.  Move Process styles from MyProcesses.ascx user control to stylesheet. --%>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td id="my_processes_count_panel" width="50%" class="MyProcessActiveTab" onclick="ActivateMyProcessTab(false);">
				<div class="MyProcessCount"     ><asp:Label ID="lblMyProcessesCount" runat="server" /></div>
				<div class="MyProcessCountLabel"><asp:Label ID="lblMyProcessesLabel" runat="server" /></div>
			</td>
			<td id="my_selfservice_count_panel" width="50%" class="MyProcessInactiveTab" onclick="ActivateMyProcessTab(true);">
				<div class="MyProcessCount"     ><asp:Label ID="lblMySelfServiceCount" runat="server" /></div>
				<div class="MyProcessCountLabel"><asp:Label ID="lblMySelfServiceLabel" runat="server" /></div>
			</td>
		</tr>
	</table>
	<div id="my_processes" style="display: inline;">
		<SplendidCRM:SplendidGrid id="grdMain" SkinID="MyProcesses" AllowPaging="<%# !PrintView %>" ShowHeader="false" EnableViewState="true" runat="server">
			<ItemStyle            CssClass="MyProcessCell" />
			<AlternatingItemStyle CssClass="MyProcessCell" />
			<SelectedItemStyle    CssClass="MyProcessCell" />
			<Columns>
				<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
					<ItemTemplate>
						<div class="MyProcessFrame">
							<div>
								<asp:HyperLink NavigateUrl='<%# "~/" + Eval("PARENT_TYPE") + "/view.aspx?id=" + Eval("PARENT_ID") %>' ToolTip='<%# Eval("PARENT_NAME") %>' Runat="server">
									<span class="MyProcessName"><%# String.Format(L10n.Term("Processes.LBL_MY_PROCESSES_NAME_FORMAT"), Sql.ToString(Eval("PROCESS_NUMBER")), Sql.ToString(Eval("PARENT_NAME"))) %></span>
								</asp:HyperLink>
							</div>
							<div class="MyProcessOverdue">
								<asp:Label Text='<%# OverdueMessage(Sql.ToDateTime(Eval("DATE_ENTERED")), Sql.ToString(Eval("DURATION_UNITS")), Sql.ToInteger(Eval("DURATION_VALUE")))%>' runat="server" />
							</div>
							<div>
								<asp:Label Text='<%# Eval("ASSIGNED_FULL_NAME") %>' class="MyProcessAssignedUser" runat="server" /> <asp:Label Text='<%# Eval("ACTIVITY_NAME") %>' CssClass="MyProcessActivityName" runat="server" />
							</div>
							<div class="MyProcessBusinessProcessName">
								<asp:Label Text='<%# Eval("BUSINESS_PROCESS_NAME") %>' runat="server" />
							</div>
						</div>
					</ItemTemplate>
				</asp:TemplateColumn>
			</Columns>
		</SplendidCRM:SplendidGrid>
	</div>

	<div id="my_selfservice" style="display: none;">
		<SplendidCRM:SplendidGrid id="grdSelfService" BorderStyle="None" AllowPaging="<%# !PrintView %>" ShowHeader="false" EnableViewState="true" runat="server">
			<ItemStyle            CssClass="" BorderStyle="None" />
			<AlternatingItemStyle CssClass="" BorderStyle="None" />
			<HeaderStyle          CssClass="" BorderStyle="None" />
			<SelectedItemStyle    CssClass="" BorderStyle="None" />
			<Columns>
				<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
					<ItemTemplate>
						<div class="MyProcessFrame">
							<div>
								<asp:HyperLink NavigateUrl='<%# "~/" + Eval("PARENT_TYPE") + "/view.aspx?id=" + Eval("PARENT_ID") %>' ToolTip='<%# Eval("PARENT_NAME") %>' Runat="server">
									<span class="MyProcessName"><%# String.Format(L10n.Term("Processes.LBL_MY_PROCESSES_NAME_FORMAT"), Sql.ToString(Eval("PROCESS_NUMBER")), Sql.ToString(Eval("PARENT_NAME"))) %></span>
								</asp:HyperLink>
							</div>
							<div class="MyProcessOverdue">
								<asp:Label Text='<%# OverdueMessage(Sql.ToDateTime(Eval("DATE_ENTERED")), Sql.ToString(Eval("DURATION_UNITS")), Sql.ToInteger(Eval("DURATION_VALUE")))%>' runat="server" />
							</div>
							<div>
								<asp:Label Text='<%# Eval("ASSIGNED_FULL_NAME") %>' class="MyProcessAssignedUser" runat="server" /> <asp:Label Text='<%# Eval("ACTIVITY_NAME") %>' CssClass="MyProcessActivityName" runat="server" />
							</div>
							<div class="MyProcessBusinessProcessName">
								<asp:Label Text='<%# Eval("BUSINESS_PROCESS_NAME") %>' runat="server" />
							</div>
						</div>
					</ItemTemplate>
				</asp:TemplateColumn>
			</Columns>
		</SplendidCRM:SplendidGrid>
	</div>
</div>
<br />

