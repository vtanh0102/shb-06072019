/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Processes
{
	/// <summary>
	///		Summary description for MyProcesses.
	/// </summary>
	public class MyProcesses : DashletControl
	{
		protected _controls.DashletHeader  ctlDashletHeader ;

		protected UniqueStringCollection arrSelectFields;
		protected DataView      vwMain               ;
		protected DataView      vwSelfService        ;
		protected SplendidGrid  grdMain              ;
		protected SplendidGrid  grdSelfService       ;
		protected Label         lblMyProcessesCount  ;
		protected Label         lblMyProcessesLabel  ;
		protected Label         lblMySelfServiceCount;
		protected Label         lblMySelfServiceLabel;
		protected Label         lblError             ;
		protected bool          bShowEditDialog = false;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Search" )
				{
					bShowEditDialog = true;
					grdMain.CurrentPageIndex = 0;
					Bind(true);
				}
				else if ( e.CommandName == "Refresh" )
				{
					Bind(true);
				}
				else if ( e.CommandName == "Remove" )
				{
					if ( !Sql.IsEmptyString(sDetailView) )
					{
						SqlProcs.spDASHLETS_USERS_InitDisable(Security.USER_ID, sDetailView, m_sMODULE, this.AppRelativeVirtualPath.Substring(0, this.AppRelativeVirtualPath.Length-5));
						SplendidCache.ClearUserDashlets(sDetailView);
						Response.Redirect(Page.AppRelativeVirtualPath + Request.Url.Query);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		protected string OverdueMessage(DateTime dtDATE_ENTERED, string sDURATION_UNITS, int nDURATION_VALUE)
		{
			string sProcessStatus = String.Empty;
			if ( !Sql.IsEmptyString(sDURATION_UNITS) && nDURATION_VALUE > 0 )
			{
				DateTime dtDUE_DATE = dtDATE_ENTERED;
				switch ( sDURATION_UNITS )
				{
					case "hour"   :  dtDUE_DATE = dtDUE_DATE.AddHours (    nDURATION_VALUE);  break;
					case "day"    :  dtDUE_DATE = dtDUE_DATE.AddDays  (    nDURATION_VALUE);  break;
					case "week"   :  dtDUE_DATE = dtDUE_DATE.AddDays  (7 * nDURATION_VALUE);  break;
					case "month"  :  dtDUE_DATE = dtDUE_DATE.AddMonths(    nDURATION_VALUE);  break;
					case "quarter":  dtDUE_DATE = dtDUE_DATE.AddMonths(3 * nDURATION_VALUE);  break;
					case "year"   :  dtDUE_DATE = dtDUE_DATE.AddYears (    nDURATION_VALUE);  break;
				}
				string sDUE_DATE = dtDUE_DATE.ToString();
				if ( dtDUE_DATE <= DateTime.Now )
				{
					sProcessStatus += " [ <span class=ProcessOverdue>" + String.Format(L10n.Term("Processes.LBL_OVERDUE_FORMAT"), sDUE_DATE) + "</span> ]";
				}
			}
			return sProcessStatus;
		}

		protected void Bind(bool bBind)
		{
			int nPageSize = Sql.ToInteger(Application["CONFIG.list_max_entries_per_page"]);
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL;
				sSQL = "select " + Sql.FormatSelectFields(arrSelectFields) 
				     + "  from vwPROCESSES_MyList" + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					WF4ApprovalActivity.Filter(Application, cmd, Security.USER_ID);
					cmd.CommandText += " order by DATE_ENTERED asc" + ControlChars.CrLf;
					if ( bDebug )
						RegisterClientScriptBlock("vwPROCESSES_MyList", Sql.ClientScriptBlock(cmd));
					try
					{
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								vwMain = new DataView(dt);
								vwMain.RowFilter = "PROCESS_USER_ID is not null";
								lblMyProcessesCount.Text = vwMain.Count.ToString();
								lblMyProcessesLabel.Text = L10n.Term("Processes.LBL_MY_PROCESSES");
								grdMain.DataSource = vwMain ;
								// 08/04/2016 Paul.  Only show pagination if reaches page size. 
								//grdMain.AllowPaging = vwMain.Count > nPageSize;
								grdMain.PagerStyle.Visible = vwMain.Count > nPageSize;
								if ( bBind )
									grdMain.DataBind();

								vwSelfService = new DataView(dt);
								vwSelfService.RowFilter = "PROCESS_USER_ID is null";
								lblMySelfServiceCount.Text = vwSelfService.Count.ToString();
								lblMySelfServiceLabel.Text = L10n.Term("Processes.LBL_SELF_SERVICE_PROCESSES");
								grdSelfService.DataSource = vwSelfService ;
								// 08/04/2016 Paul.  Only show pagination if reaches page size. 
								//grdSelfService.AllowPaging = vwSelfService.Count > nPageSize;
								grdSelfService.PagerStyle.Visible = vwMain.Count > nPageSize;
								if ( bBind )
									grdSelfService.DataBind();
							}
						}
					}
					catch(Exception ex)
					{
						SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						lblError.Text = ex.Message;
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			this.Visible = this.Visible && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				Bind(!IsPostBack);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			if ( IsPostBack )
			{
				grdMain.DataBind();
			}
			base.OnPreRender(e);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDashletHeader.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Processes";
			arrSelectFields = new UniqueStringCollection();
			//arrSelectFields.Add("ID"                          );
			arrSelectFields.Add("PROCESS_NUMBER"              );
			//arrSelectFields.Add("BUSINESS_PROCESS_INSTANCE_ID");
			//arrSelectFields.Add("ACTIVITY_INSTANCE"           );
			arrSelectFields.Add("ACTIVITY_NAME"               );
			arrSelectFields.Add("BUSINESS_PROCESS_ID"         );
			arrSelectFields.Add("BUSINESS_PROCESS_NAME"       );
			arrSelectFields.Add("PROCESS_USER_ID"             );
			//arrSelectFields.Add("BOOKMARK_NAME"               );
			arrSelectFields.Add("PARENT_TYPE"                 );
			arrSelectFields.Add("PARENT_ID"                   );
			arrSelectFields.Add("PARENT_NAME"                 );
			arrSelectFields.Add("PARENT_ASSIGNED_USER_ID"     );
			arrSelectFields.Add("ASSIGNED_USER_NAME"          );
			arrSelectFields.Add("ASSIGNED_FULL_NAME"          );
			arrSelectFields.Add("USER_TASK_TYPE"              );
			//arrSelectFields.Add("CHANGE_ASSIGNED_USER"        );
			//arrSelectFields.Add("CHANGE_ASSIGNED_TEAM_ID"     );
			//arrSelectFields.Add("CHANGE_PROCESS_USER"         );
			//arrSelectFields.Add("CHANGE_PROCESS_TEAM_ID"      );
			arrSelectFields.Add("USER_ASSIGNMENT_METHOD"      );
			//arrSelectFields.Add("STATIC_ASSIGNED_USER_ID"     );
			//arrSelectFields.Add("DYNAMIC_PROCESS_TEAM_ID"     );
			//arrSelectFields.Add("DYNAMIC_PROCESS_ROLE_ID"     );
			//arrSelectFields.Add("READ_ONLY_FIELDS"            );
			//arrSelectFields.Add("REQUIRED_FIELDS"             );
			arrSelectFields.Add("DURATION_UNITS"              );
			arrSelectFields.Add("DURATION_VALUE"              );
			arrSelectFields.Add("STATUS"                      );
			//arrSelectFields.Add("APPROVAL_USER_ID"            );
			//arrSelectFields.Add("APPROVAL_DATE"               );
			//arrSelectFields.Add("APPROVAL_RESPONSE"           );
			arrSelectFields.Add("DATE_ENTERED"                );
			arrSelectFields.Add("DATE_MODIFIED"               );
		}
		#endregion
	}
}
