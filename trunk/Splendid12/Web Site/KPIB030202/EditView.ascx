<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB030202.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="divEditView" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIB030202" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:HiddenField ID="LAYOUT_EDIT_VIEW" Runat="server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblMain" class="tabEditView" runat="server">
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<div id="divEditSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
	</div>

     <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="No.">
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                              <asp:HiddenField ID="txtKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>                    
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_KPI_NAME" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_KPI_UNIT" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                            <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_KPI_RATIO" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_RATIO" Text='<%# Bind("KPI_RATIO") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_PLAN_VALUE" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="txtPLAN_VALUE" Text='<%# Bind("PLAN_VALUE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_SYNC_VALUE" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:Label ID="txtSYNC_VALUE" Text='<%# Bind("SYNC_VALUE") %>' runat="server" />
                        </ItemTemplate>                     
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB030202.LBL_LIST_FINAL_VALUE" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:TextBox ID="txtFINAL_VALUE" Text='<%# Bind("FINAL_VALUE") %>' runat="server" />
                        </ItemTemplate>                                              
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIB020101.LBL_LIST_DESCRIPTION" ItemStyle-Width="50%">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>

	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
