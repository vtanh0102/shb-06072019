using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;
using System.Globalization;

namespace SplendidCRM.KPIM0102
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected DropDownList ddlType;
        protected DropDownList ddlFromCode;
        protected DropDownList ddlToCode;

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwM_KPI_CONVERSION_RATE_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;
                                    CultureInfo cul = CultureInfo.CurrentCulture;

                                    ddlFromCode = this.FindControl("FROM_CODE") as DropDownList;
                                    string fromCode = string.Empty;
                                    string fromName = string.Empty;
                                    if (ddlFromCode != null)
                                    {
                                        if (ddlFromCode.Items.Count > 0)
                                        {
                                            fromCode = ddlFromCode.SelectedValue;
                                            fromName = ddlFromCode.SelectedItem.Text;
                                        }

                                    }
                                    ddlToCode = this.FindControl("TO_CODE") as DropDownList;
                                    string toCode = string.Empty;
                                    string toName = string.Empty;
                                    if (ddlToCode != null)
                                    {
                                        if (ddlToCode.Items.Count > 0)
                                        {
                                            toCode = ddlToCode.SelectedValue;
                                            toName = ddlToCode.SelectedItem.Text;
                                        }
                                    }
                                    var conversionRate = new SplendidCRM.DynamicControl(this, rowCurrent, "CONVERSION_RATE").Text;
                                    float fResult = Sql.ToFloat(conversionRate);
                                    if (fromCode == toCode)
                                    {
                                        throw (new Exception(string.Format(L10n.Term("KPIM0102.ERR_DUPLICATE_CODE"), L10n.Term("KPIM0102.LBL_FROM_NAME"), L10n.Term("KPIM0102.LBL_TO_NAME"))));
                                    }
                                    if (Sql.IsEmptyGuid(gID))
                                    {
                                        bool kpiConversionRate = KPIs_Utils.spM_KPI_CONVERSION_RATE_Existed(fromCode, toCode, trn);
                                        if (kpiConversionRate)
                                        {
                                            throw (new Exception(string.Format(L10n.Term("KPIM0102.ERR_KPI_CONVERSION_RATE_DUPLICATE"))));
                                        }
                                    }                                    

                                    KPIM0102_SqlProc.spM_KPI_CONVERSION_RATE_Update
                                        (ref gID
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TYPE").Text
                                        , fromCode//new SplendidCRM.DynamicControl(this, rowCurrent, "FROM_CODE").SelectedValue
                                        , fromName//new SplendidCRM.DynamicControl(this, rowCurrent, "FROM_CODE").Text
                                        , toCode//new SplendidCRM.DynamicControl(this, rowCurrent, "TO_CODE").SelectedValue
                                        , toName//new SplendidCRM.DynamicControl(this, rowCurrent, "TO_CODE").Text
                                        , fResult//new SplendidCRM.DynamicControl(this, rowCurrent, "CONVERSION_RATE").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "CONVERSION_RATE1").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "CONVERSION_RATE2").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                        , trn
                                        );

                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    SplendidCRM.SqlProcs.spTRACKER_Update
                                        (Security.USER_ID
                                        , m_sMODULE
                                        , gID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FROM_NAME").Text
                                        , "save"
                                        , trn
                                        );
                                    if (plcSubPanel.Visible)
                                    {
                                        foreach (Control ctl in plcSubPanel.Controls)
                                        {
                                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                            if (ctlSubPanel != null)
                                            {
                                                ctlSubPanel.Save(gID, m_sMODULE, trn);
                                            }
                                        }
                                    }
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwM_KPI_CONVERSION_RATE_Edit" + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["FROM_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["FROM_NAME"] = Sql.ToString(rdr["FROM_NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["FROM_NAME"] = ViewState["FROM_NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                            //load combobox
                                            Load_DropdownListForSelected();
                                            ddlFromCode = this.FindControl("FROM_CODE") as DropDownList;
                                            if (ddlFromCode != null)
                                            {
                                                ddlFromCode.SelectedValue = Sql.ToString(rdr["FROM_CODE"]);
                                            }
                                            ddlToCode = this.FindControl("TO_CODE") as DropDownList;
                                            if (ddlToCode != null)
                                            {
                                                ddlToCode.SelectedValue = Sql.ToString(rdr["TO_CODE"]);
                                            }
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                    ddlType = this.FindControl("TYPE") as DropDownList;
                    if (ddlType != null)
                    {
                        ddlType.AutoPostBack = true;
                        ddlType.SelectedIndexChanged += new EventHandler(ddlType_SelectedIndexChanged);
                    }
                    if (Sql.IsEmptyGuid(gID))
                    {
                        Load_DropdownListForSelected();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["FROM_NAME"] = ViewState["FROM_NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                    ddlType = this.FindControl("TYPE") as DropDownList;
                    if (ddlType != null)
                    {
                        ddlType.AutoPostBack = true;
                        ddlType.SelectedIndexChanged += new EventHandler(ddlType_SelectedIndexChanged);
                    }

                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_DropdownListForSelected();
        }


        private void Load_DropdownListForSelected()
        {
            ddlType = this.FindControl("TYPE") as DropDownList;
            if (ddlType == null)
            {
                return;
            }
            string strType = ddlType.SelectedValue;
            if (KPIs_Constant.TYPE_HSCV.Equals(strType))
            {
                ddlFromCode = this.FindControl("FROM_CODE") as DropDownList;
                if (ddlFromCode != null)
                {
                    ddlFromCode.Items.Clear();
                    ddlFromCode.Enabled = true;
                    ddlFromCode.DataSource = SplendidCache.M_POSITION();
                    ddlFromCode.DataTextField = "POSITION_NAME";
                    ddlFromCode.DataValueField = "POSITION_CODE";
                    ddlFromCode.DataBind();
                }
                ddlToCode = this.FindControl("TO_CODE") as DropDownList;
                if (ddlToCode != null)
                {
                    ddlToCode.Items.Clear();
                    ddlToCode.Enabled = true;
                    ddlToCode.DataSource = SplendidCache.M_POSITION();
                    ddlToCode.DataTextField = "POSITION_NAME";
                    ddlToCode.DataValueField = "POSITION_CODE";
                    ddlToCode.DataBind();
                }
                return;
            }
            else if (KPIs_Constant.TYPE_HSKV.Equals(strType))
            {
                ddlFromCode = this.FindControl("FROM_CODE") as DropDownList;
                if (ddlFromCode != null)
                {
                    ddlFromCode.Items.Clear();
                    ddlFromCode.Enabled = true;
                    ddlFromCode.DataSource = SplendidCache.M_AREA();
                    ddlFromCode.DataTextField = "AREA_NAME";
                    ddlFromCode.DataValueField = "AREA_CODE";
                    ddlFromCode.DataBind();
                }
                ddlToCode = this.FindControl("TO_CODE") as DropDownList;
                if (ddlToCode != null)
                {
                    ddlToCode.Items.Clear();
                    ddlToCode.Enabled = true;
                    ddlToCode.DataSource = SplendidCache.M_AREA();
                    ddlToCode.DataTextField = "AREA_NAME";
                    ddlToCode.DataValueField = "AREA_CODE";
                    ddlToCode.DataBind();
                }
                return;
            }
            else if (KPIs_Constant.TYPE_HSTN.Equals(strType))
            {
                ddlFromCode = this.FindControl("FROM_CODE") as DropDownList;
                if (ddlFromCode != null)
                {
                    ddlFromCode.Items.Clear();
                    ddlFromCode.Enabled = true;
                    ddlFromCode.DataSource = SplendidCache.M_SENIORITY();
                    ddlFromCode.DataTextField = "SENIORITY_NAME";
                    ddlFromCode.DataValueField = "ID";
                    ddlFromCode.DataBind();
                }
                ddlToCode = this.FindControl("TO_CODE") as DropDownList;
                if (ddlToCode != null)
                {
                    ddlToCode.Items.Clear();
                    ddlToCode.Enabled = true;
                    ddlToCode.DataSource = SplendidCache.M_SENIORITY();
                    ddlToCode.DataTextField = "SENIORITY_NAME";
                    ddlToCode.DataValueField = "ID";
                    ddlToCode.DataBind();
                }
                return;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            m_sMODULE = "KPIM0102";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
