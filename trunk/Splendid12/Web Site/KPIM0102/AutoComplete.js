
function M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed(fldM_KPI_CONVERSION_RATE_TYPE)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain TYPE in the text, so just get the length minus 4. 
	var userContext = fldM_KPI_CONVERSION_RATE_TYPE.id.substring(0, fldM_KPI_CONVERSION_RATE_TYPE.id.length - 'M_KPI_CONVERSION_RATE_TYPE'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_TYPE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_M_KPI_CONVERSION_RATE_TYPE = document.getElementById(userContext + 'PREV_M_KPI_CONVERSION_RATE_TYPE');
	if ( fldPREV_M_KPI_CONVERSION_RATE_TYPE == null )
	{
		//alert('Could not find ' + userContext + 'PREV_M_KPI_CONVERSION_RATE_TYPE');
	}
	else if ( fldPREV_M_KPI_CONVERSION_RATE_TYPE.value != fldM_KPI_CONVERSION_RATE_TYPE.value )
	{
		if ( fldM_KPI_CONVERSION_RATE_TYPE.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIM0102.AutoComplete.M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Get(fldM_KPI_CONVERSION_RATE_TYPE.value, M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed_OnSucceededWithContext, M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sTYPE = result.TYPE;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_TYPE_AjaxErrors');
		var fldM_KPI_CONVERSION_RATE_ID        = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_ID'       );
		var fldM_KPI_CONVERSION_RATE_TYPE      = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_TYPE'     );
		var fldPREV_M_KPI_CONVERSION_RATE_TYPE = document.getElementById(userContext + 'PREV_M_KPI_CONVERSION_RATE_TYPE');
		if ( fldM_KPI_CONVERSION_RATE_ID        != null ) fldM_KPI_CONVERSION_RATE_ID.value        = sID  ;
		if ( fldM_KPI_CONVERSION_RATE_TYPE      != null ) fldM_KPI_CONVERSION_RATE_TYPE.value      = sTYPE;
		if ( fldPREV_M_KPI_CONVERSION_RATE_TYPE != null ) fldPREV_M_KPI_CONVERSION_RATE_TYPE.value = sTYPE;
	}
	else
	{
		alert('result from KPIM0102.AutoComplete service is null');
	}
}

function M_KPI_CONVERSION_RATE_M_KPI_CONVERSION_RATE_TYPE_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_TYPE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldM_KPI_CONVERSION_RATE_ID        = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_ID'       );
	var fldM_KPI_CONVERSION_RATE_TYPE      = document.getElementById(userContext + 'M_KPI_CONVERSION_RATE_TYPE'     );
	var fldPREV_M_KPI_CONVERSION_RATE_TYPE = document.getElementById(userContext + 'PREV_M_KPI_CONVERSION_RATE_TYPE');
	if ( fldM_KPI_CONVERSION_RATE_ID        != null ) fldM_KPI_CONVERSION_RATE_ID.value        = '';
	if ( fldM_KPI_CONVERSION_RATE_TYPE      != null ) fldM_KPI_CONVERSION_RATE_TYPE.value      = '';
	if ( fldPREV_M_KPI_CONVERSION_RATE_TYPE != null ) fldPREV_M_KPI_CONVERSION_RATE_TYPE.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

