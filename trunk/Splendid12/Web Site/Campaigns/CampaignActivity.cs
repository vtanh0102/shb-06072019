/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// CampaignActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class CampaignActivity: SplendidActivity
	{
		public CampaignActivity()
		{
			this.Name = "CampaignActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.IDProperty))); }
			set { base.SetValue(CampaignActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(CampaignActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.NAMEProperty))); }
			set { base.SetValue(CampaignActivity.NAMEProperty, value); }
		}

		public static DependencyProperty REFER_URLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REFER_URL", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REFER_URL
		{
			get { return ((string)(base.GetValue(CampaignActivity.REFER_URLProperty))); }
			set { base.SetValue(CampaignActivity.REFER_URLProperty, value); }
		}

		public static DependencyProperty TRACKER_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TRACKER_TEXT", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TRACKER_TEXT
		{
			get { return ((string)(base.GetValue(CampaignActivity.TRACKER_TEXTProperty))); }
			set { base.SetValue(CampaignActivity.TRACKER_TEXTProperty, value); }
		}

		public static DependencyProperty START_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("START_DATE", typeof(DateTime), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime START_DATE
		{
			get { return ((DateTime)(base.GetValue(CampaignActivity.START_DATEProperty))); }
			set { base.SetValue(CampaignActivity.START_DATEProperty, value); }
		}

		public static DependencyProperty END_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("END_DATE", typeof(DateTime), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime END_DATE
		{
			get { return ((DateTime)(base.GetValue(CampaignActivity.END_DATEProperty))); }
			set { base.SetValue(CampaignActivity.END_DATEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(CampaignActivity.STATUSProperty))); }
			set { base.SetValue(CampaignActivity.STATUSProperty, value); }
		}

		public static DependencyProperty BUDGETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BUDGET", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal BUDGET
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.BUDGETProperty))); }
			set { base.SetValue(CampaignActivity.BUDGETProperty, value); }
		}

		public static DependencyProperty EXPECTED_COSTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPECTED_COST", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXPECTED_COST
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.EXPECTED_COSTProperty))); }
			set { base.SetValue(CampaignActivity.EXPECTED_COSTProperty, value); }
		}

		public static DependencyProperty ACTUAL_COSTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTUAL_COST", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal ACTUAL_COST
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.ACTUAL_COSTProperty))); }
			set { base.SetValue(CampaignActivity.ACTUAL_COSTProperty, value); }
		}

		public static DependencyProperty EXPECTED_REVENUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPECTED_REVENUE", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXPECTED_REVENUE
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.EXPECTED_REVENUEProperty))); }
			set { base.SetValue(CampaignActivity.EXPECTED_REVENUEProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_TYPE", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CAMPAIGN_TYPE
		{
			get { return ((string)(base.GetValue(CampaignActivity.CAMPAIGN_TYPEProperty))); }
			set { base.SetValue(CampaignActivity.CAMPAIGN_TYPEProperty, value); }
		}

		public static DependencyProperty OBJECTIVEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OBJECTIVE", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OBJECTIVE
		{
			get { return ((string)(base.GetValue(CampaignActivity.OBJECTIVEProperty))); }
			set { base.SetValue(CampaignActivity.OBJECTIVEProperty, value); }
		}

		public static DependencyProperty CONTENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTENT", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTENT
		{
			get { return ((string)(base.GetValue(CampaignActivity.CONTENTProperty))); }
			set { base.SetValue(CampaignActivity.CONTENTProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(CampaignActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty IMPRESSIONSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IMPRESSIONS", typeof(Int32), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 IMPRESSIONS
		{
			get { return ((Int32)(base.GetValue(CampaignActivity.IMPRESSIONSProperty))); }
			set { base.SetValue(CampaignActivity.IMPRESSIONSProperty, value); }
		}

		public static DependencyProperty FREQUENCYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FREQUENCY", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FREQUENCY
		{
			get { return ((string)(base.GetValue(CampaignActivity.FREQUENCYProperty))); }
			set { base.SetValue(CampaignActivity.FREQUENCYProperty, value); }
		}

		public static DependencyProperty TRACKER_KEYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TRACKER_KEY", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TRACKER_KEY
		{
			get { return ((string)(base.GetValue(CampaignActivity.TRACKER_KEYProperty))); }
			set { base.SetValue(CampaignActivity.TRACKER_KEYProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.TEAM_IDProperty))); }
			set { base.SetValue(CampaignActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(CampaignActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(CampaignActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_NUMBER", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CAMPAIGN_NUMBER
		{
			get { return ((string)(base.GetValue(CampaignActivity.CAMPAIGN_NUMBERProperty))); }
			set { base.SetValue(CampaignActivity.CAMPAIGN_NUMBERProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(CampaignActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ACTUAL_COST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTUAL_COST_USDOLLAR", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal ACTUAL_COST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.ACTUAL_COST_USDOLLARProperty))); }
			set { base.SetValue(CampaignActivity.ACTUAL_COST_USDOLLARProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(CampaignActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty BUDGET_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BUDGET_USDOLLAR", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal BUDGET_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.BUDGET_USDOLLARProperty))); }
			set { base.SetValue(CampaignActivity.BUDGET_USDOLLARProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(CampaignActivity.CREATED_BYProperty))); }
			set { base.SetValue(CampaignActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(CampaignActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(CampaignActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(CampaignActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(CampaignActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(CampaignActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(CampaignActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(CampaignActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty EXPECTED_COST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPECTED_COST_USDOLLAR", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXPECTED_COST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.EXPECTED_COST_USDOLLARProperty))); }
			set { base.SetValue(CampaignActivity.EXPECTED_COST_USDOLLARProperty, value); }
		}

		public static DependencyProperty EXPECTED_REVENUE_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPECTED_REVENUE_USDOLLAR", typeof(decimal), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXPECTED_REVENUE_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(CampaignActivity.EXPECTED_REVENUE_USDOLLARProperty))); }
			set { base.SetValue(CampaignActivity.EXPECTED_REVENUE_USDOLLARProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(CampaignActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(CampaignActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(CampaignActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TRACKER_COUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TRACKER_COUNT", typeof(Int32), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 TRACKER_COUNT
		{
			get { return ((Int32)(base.GetValue(CampaignActivity.TRACKER_COUNTProperty))); }
			set { base.SetValue(CampaignActivity.TRACKER_COUNTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_CONVERSION_RATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_CONVERSION_RATE", typeof(float), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float CURRENCY_CONVERSION_RATE
		{
			get { return ((float)(base.GetValue(CampaignActivity.CURRENCY_CONVERSION_RATEProperty))); }
			set { base.SetValue(CampaignActivity.CURRENCY_CONVERSION_RATEProperty, value); }
		}

		public static DependencyProperty CURRENCY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.CURRENCY_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.CURRENCY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_SYMBOL", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_SYMBOL
		{
			get { return ((string)(base.GetValue(CampaignActivity.CURRENCY_SYMBOLProperty))); }
			set { base.SetValue(CampaignActivity.CURRENCY_SYMBOLProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(CampaignActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(CampaignActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(CampaignActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(CampaignActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(CampaignActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CampaignActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CampaignActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CAMPAIGNS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCAMPAIGNS_AUDIT        CAMPAIGNS          " + ControlChars.CrLf
							                + " inner join vwCAMPAIGNS_AUDIT        CAMPAIGNS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CAMPAIGNS_AUDIT_OLD.ID = CAMPAIGNS.ID       " + ControlChars.CrLf
							                + "        and CAMPAIGNS_AUDIT_OLD.AUDIT_VERSION = (select max(vwCAMPAIGNS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                   from vwCAMPAIGNS_AUDIT                   " + ControlChars.CrLf
							                + "                                                  where vwCAMPAIGNS_AUDIT.ID            =  CAMPAIGNS.ID           " + ControlChars.CrLf
							                + "                                                    and vwCAMPAIGNS_AUDIT.AUDIT_VERSION <  CAMPAIGNS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                    and vwCAMPAIGNS_AUDIT.AUDIT_TOKEN   <> CAMPAIGNS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                )" + ControlChars.CrLf
							                + " where CAMPAIGNS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwCAMPAIGNS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCAMPAIGNS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *               " + ControlChars.CrLf
							                + "  from vwCAMPAIGNS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								REFER_URL                      = Sql.ToString  (rdr["REFER_URL"                     ]);
								TRACKER_TEXT                   = Sql.ToString  (rdr["TRACKER_TEXT"                  ]);
								START_DATE                     = Sql.ToDateTime(rdr["START_DATE"                    ]);
								END_DATE                       = Sql.ToDateTime(rdr["END_DATE"                      ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								BUDGET                         = Sql.ToDecimal (rdr["BUDGET"                        ]);
								EXPECTED_COST                  = Sql.ToDecimal (rdr["EXPECTED_COST"                 ]);
								ACTUAL_COST                    = Sql.ToDecimal (rdr["ACTUAL_COST"                   ]);
								EXPECTED_REVENUE               = Sql.ToDecimal (rdr["EXPECTED_REVENUE"              ]);
								CAMPAIGN_TYPE                  = Sql.ToString  (rdr["CAMPAIGN_TYPE"                 ]);
								OBJECTIVE                      = Sql.ToString  (rdr["OBJECTIVE"                     ]);
								CONTENT                        = Sql.ToString  (rdr["CONTENT"                       ]);
								CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								IMPRESSIONS                    = Sql.ToInteger (rdr["IMPRESSIONS"                   ]);
								if ( bPast )
									FREQUENCY                      = Sql.ToString  (rdr["FREQUENCY"                     ]);
								TRACKER_KEY                    = Sql.ToString  (rdr["TRACKER_KEY"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								CAMPAIGN_NUMBER                = Sql.ToString  (rdr["CAMPAIGN_NUMBER"               ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ACTUAL_COST_USDOLLAR           = Sql.ToDecimal (rdr["ACTUAL_COST_USDOLLAR"          ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								BUDGET_USDOLLAR                = Sql.ToDecimal (rdr["BUDGET_USDOLLAR"               ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								EXPECTED_COST_USDOLLAR         = Sql.ToDecimal (rdr["EXPECTED_COST_USDOLLAR"        ]);
								EXPECTED_REVENUE_USDOLLAR      = Sql.ToDecimal (rdr["EXPECTED_REVENUE_USDOLLAR"     ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TRACKER_COUNT                  = Sql.ToInteger (rdr["TRACKER_COUNT"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									CURRENCY_CONVERSION_RATE       = Sql.ToFloat   (rdr["CURRENCY_CONVERSION_RATE"      ]);
									CURRENCY_NAME                  = Sql.ToString  (rdr["CURRENCY_NAME"                 ]);
									CURRENCY_SYMBOL                = Sql.ToString  (rdr["CURRENCY_SYMBOL"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CampaignActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CAMPAIGNS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCAMPAIGNS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, REFER_URL
								, TRACKER_TEXT
								, START_DATE
								, END_DATE
								, STATUS
								, BUDGET
								, EXPECTED_COST
								, ACTUAL_COST
								, EXPECTED_REVENUE
								, CAMPAIGN_TYPE
								, OBJECTIVE
								, CONTENT
								, CURRENCY_ID
								, IMPRESSIONS
								, FREQUENCY
								, TRACKER_KEY
								, TEAM_ID
								, TEAM_SET_LIST
								, TAG_SET_NAME
								, CAMPAIGN_NUMBER
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CampaignActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

