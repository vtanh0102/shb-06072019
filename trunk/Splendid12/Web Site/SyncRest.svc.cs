﻿/*
 * Copyright (C) 2001-2014 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace SplendidCRM
{
	// http://www.odata.org/developers/protocols/json-format
	// http://brennan.offwhite.net/blog/2008/10/21/simple-wcf-and-ajax-integration/
	[ServiceContract]
	[ServiceBehavior(IncludeExceptionDetailInFaults=true)]
	[AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Required)]
	public class SyncRest
	{
		#region Scalar functions
		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public string Version()
		{
			return Sql.ToString(HttpContext.Current.Application["SplendidVersion"]);
		}

		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public string Edition()
		{
			return Sql.ToString(HttpContext.Current.Application["CONFIG.service_level"]);
		}

		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public DateTime UtcTime()
		{
			return DateTime.UtcNow;
		}

		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public bool IsAuthenticated()
		{
			return Security.IsAuthenticated();
		}
		#endregion

		#region json utils
		// http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
		private static long UnixTicks(DateTime dt)
		{
			return (dt.Ticks - 621355968000000000) / 10000;
		}

		private static string ToJsonDate(object dt)
		{
			return "\\/Date(" + UnixTicks(Sql.ToDateTime(dt)).ToString() + ")\\/";
		}

		private static string ToJsonUniversalDate(DateTime dtServerTime)
		{
			return "\\/Date(" + UnixTicks(dtServerTime.ToUniversalTime()).ToString() + ")\\/";
		}

		// 08/03/2012 Paul.  FromJsonDate is used on Web Capture services. 
		public static DateTime FromJsonDate(string s)
		{
			DateTime dt = DateTime.MinValue;
			if ( s.StartsWith("\\/Date(") && s.EndsWith(")\\/") )
			{
				s = s.Replace("\\/Date(", "");
				s = s.Replace(")\\/", "");
				long lEpoch = Sql.ToLong(s);
				dt = new DateTime(lEpoch * 10000 + 621355968000000000);
				// 08/28/2014 Paul.  The value is always in UTC, so make sure to set the kind. 
				dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
			}
			else
			{
				dt = Sql.ToDateTime(s);
			}
			return dt;
		}

		// 08/24/2014 Paul.  We need to convert to UTC, except for DATE_MODIFIED_UTC which is already UTC. 
		// http://schotime.net/blog/index.php/2008/07/27/dataset-datatable-to-json/
		private static List<Dictionary<string, object>> RowsToDictionary(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
			// 10/11/2012 Paul.  dt will be null when no results security filter applied. 
			if ( dt != null )
			{
				foreach (DataRow dr in dt.Rows)
				{
					// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
					Dictionary<string, object> drow = new Dictionary<string, object>();
					if ( dt.Columns.Contains("ID") )
					{
						Guid gID = Sql.ToGuid(dr["ID"]);
						if ( !Sql.IsEmptyString(sBaseURI) && !Sql.IsEmptyString(sModuleName) )
						{
							Dictionary<string, object> metadata = new Dictionary<string, object>();
							metadata.Add("uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "");
							metadata.Add("type", "SplendidCRM." + sModuleName);
							if ( dr.Table.Columns.Contains("DATE_MODIFIED_UTC") )
							{
								DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime(dr["DATE_MODIFIED_UTC"]);
								metadata.Add("etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
							}
							drow.Add("__metadata", metadata);
						}
					}
				
					for (int i = 0; i < dt.Columns.Count; i++)
					{
						if ( dt.Columns[i].DataType.FullName == "System.DateTime" )
						{
							// 08/24/2014 Paul.  We need to convert to UTC, except for DATE_MODIFIED_UTC which is already UTC. 
							DateTime dtServerTime = Sql.ToDateTime(dr[i]);
							if ( dt.Columns[i].ColumnName == "DATE_MODIFIED_UTC" )
								dtServerTime = DateTime.SpecifyKind(dtServerTime, DateTimeKind.Utc);
							else
								dtServerTime = DateTime.SpecifyKind(dtServerTime, DateTimeKind.Local);
							drow.Add(dt.Columns[i].ColumnName, ToJsonUniversalDate(dtServerTime) );
						}
						else
						{
							drow.Add(dt.Columns[i].ColumnName, dr[i]);
						}
					}
					objs.Add(drow);
				}
			}
			return objs;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			results.Add("results", RowsToDictionary(sBaseURI, sModuleName, dt, T10n));
			d.Add("d", results);
			// 04/21/2017 Paul.  Count should be returend as a number. 
			if ( dt != null )
				d.Add("__count", dt.Rows.Count);
			return d;
		}

		// 08/24/2014 Paul.  We need to convert to UTC, except for DATE_MODIFIED_UTC which is already UTC. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataRow dr, TimeZone T10n)
		{
			Dictionary<string, object> d       = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			Dictionary<string, object> drow    = new Dictionary<string, object>();
			
			// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
			DataTable dt = dr.Table;
			if ( dt.Columns.Contains("ID") )
			{
				Guid gID = Sql.ToGuid(dr["ID"]);
				if ( !Sql.IsEmptyString(sBaseURI) && !Sql.IsEmptyString(sModuleName) )
				{
					Dictionary<string, object> metadata = new Dictionary<string, object>();
					metadata.Add("uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "");
					metadata.Add("type", "SplendidCRM." + sModuleName);
					if ( dt.Columns.Contains("DATE_MODIFIED_UTC") )
					{
						DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime(dr["DATE_MODIFIED_UTC"]);
						metadata.Add("etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
					}
					drow.Add("__metadata", metadata);
				}
			}
			
			for (int i = 0; i < dt.Columns.Count; i++)
			{
				if ( dt.Columns[i].DataType.FullName == "System.DateTime" )
				{
					// 08/24/2014 Paul.  We need to convert to UTC, except for DATE_MODIFIED_UTC which is already UTC. 
					DateTime dtServerTime = Sql.ToDateTime(dr[i]);
					if ( dt.Columns[i].ColumnName == "DATE_MODIFIED_UTC" )
						dtServerTime = DateTime.SpecifyKind(dtServerTime, DateTimeKind.Utc);
					else
						dtServerTime = DateTime.SpecifyKind(dtServerTime, DateTimeKind.Local);
					drow.Add(dt.Columns[i].ColumnName, ToJsonUniversalDate(dtServerTime) );
				}
				else
				{
					drow.Add(dt.Columns[i].ColumnName, dr[i]);
				}
			}
			
			results.Add("results", drow);
			d.Add("d", results);
			return d;
		}

		private static string ConvertODataFilter(string sFILTER, IDbCommand cmd)
		{
			// Logical Operators
			sFILTER = sFILTER.Replace(" eq true" , " eq 1");
			sFILTER = sFILTER.Replace(" eq false", " eq 0");
			sFILTER = sFILTER.Replace(" ne true" , " ne 1");
			sFILTER = sFILTER.Replace(" ne false", " ne 0");
			sFILTER = sFILTER.Replace(" gt ", " > ");
			sFILTER = sFILTER.Replace(" lt ", " < ");
			sFILTER = sFILTER.Replace(" eq ", " = ");
			sFILTER = sFILTER.Replace(" ne ", " <> ");
			// Arithmetic Operators
			sFILTER = sFILTER.Replace(" add ", " + ");
			sFILTER = sFILTER.Replace(" sub ", " - ");
			sFILTER = sFILTER.Replace(" mul ", " * ");
			sFILTER = sFILTER.Replace(" div ", " / ");
			sFILTER = sFILTER.Replace(" mod ", " % ");
			// Date Functions
			if ( Sql.IsSQLServer(cmd) )
			{
				//sFILTER = sFILTER.Replace("year("  , "dbo.fnDatePart('year', "  );
				//sFILTER = sFILTER.Replace("month(" , "dbo.fnDatePart('month', " );
				//sFILTER = sFILTER.Replace("day("   , "dbo.fnDatePart('day', "   );
				sFILTER = sFILTER.Replace("hour("  , "dbo.fnDatePart('hour', "  );
				sFILTER = sFILTER.Replace("minute(", "dbo.fnDatePart('minute', ");
				sFILTER = sFILTER.Replace("second(", "dbo.fnDatePart('second', ");
			}
			else
			{
				//sFILTER = sFILTER.Replace("year("  , "fnDatePart('year', "  );
				//sFILTER = sFILTER.Replace("month(" , "fnDatePart('month', " );
				//sFILTER = sFILTER.Replace("day("   , "fnDatePart('day', "   );
				sFILTER = sFILTER.Replace("hour("  , "fnDatePart('hour', "  );
				sFILTER = sFILTER.Replace("minute(", "fnDatePart('minute', ");
				sFILTER = sFILTER.Replace("second(", "fnDatePart('second', ");
			}
			// Math Functions
			int nStart = sFILTER.IndexOf("round(");
			while ( nStart > 0 )
			{
				int nEnd = sFILTER.IndexOf(")", nStart);
				if ( nEnd > 0 )
				{
					sFILTER = sFILTER.Substring(0, nEnd - 1) + ", 0" + sFILTER.Substring(nEnd - 1);
				}
				nStart = sFILTER.IndexOf("round(", nStart + 1);
			}
			// String Functions
			sFILTER = sFILTER.Replace("tolower(", "lower(");
			sFILTER = sFILTER.Replace("toupper(", "upper(");
			if ( Sql.IsSQLServer(cmd) )
			{
				sFILTER = sFILTER.Replace("length("     , "len(");
				sFILTER = sFILTER.Replace("trim("       , "dbo.fnTrim(");
				sFILTER = sFILTER.Replace("concat("     , "dbo.fnConcat(");
				sFILTER = sFILTER.Replace("startswith(" , "dbo.fnStartsWith(");
				sFILTER = sFILTER.Replace("endswith("   , "dbo.fnEndsWith(");
				sFILTER = sFILTER.Replace("indexof("    , "dbo.fnIndexOf(");
				sFILTER = sFILTER.Replace("substringof(", "dbo.fnSubstringOf(");
			}
			return sFILTER;
		}

		private static Stream ToJsonStream(Dictionary<string, object> d)
		{
			JavaScriptSerializer json = new JavaScriptSerializer();
			// 05/05/2013 Paul.  No reason to limit the Json result. 
			json.MaxJsonLength = int.MaxValue;
			string sResponse = json.Serialize(d);
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		private static List<string> AccessibleModules()
		{
			List<string> lstMODULES = SplendidCache.AccessibleModules(HttpContext.Current, Security.USER_ID);
			if ( Crm.Config.enable_team_management() )
			{
				if ( !lstMODULES.Contains("Teams") )
					lstMODULES.Add("Teams");
			}
			// 11/08/2009 Paul.  We need to combine the two module lists into a single list. 
			// 11/22/2009 Paul.  Simplify the logic by having a local list of system modules. 
			/*
			string[] arrSystemModules = new string[] { "ACL", "ACLActions", "ACLRoles", "Audit", "Config", "Currencies", "Dashlets"
			                                         , "DocumentRevisions", "DynamicButtons", "Export", "FieldValidators", "Import"
			                                         , "Merge", "Modules", "Offline", "Releases", "Roles", "SavedSearch", "Shortcuts"
			                                         , "TeamNotices", "Terminology", "Users", "SystemSyncLog"
			                                         };
			string[] arrSystemModules = new string[] { "Currencies", "DocumentRevisions", "Releases" };
			foreach ( string sSystemModule in arrSystemModules )
				lstMODULES.Add(sSystemModule);
			*/
			lstMODULES.Add("Currencies"       );
			lstMODULES.Add("DocumentRevisions");
			lstMODULES.Add("Releases"         );
			// 11/30/2012 Paul.  Activities is a supported module so that we can get Open Activities and History to display in the HTML5 Offline Client. 
			lstMODULES.Add("Activities"       );
			return lstMODULES;
		}
		#endregion

		#region Login
		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Guid Login(string UserName, string Password, string Version)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpSessionState     Session     = HttpContext.Current.Session    ;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			string sUSER_NAME   = UserName;
			string sPASSWORD    = Password;
			string sVERSION     = Version ;
			Guid gUSER_ID       = Guid.Empty;
			Guid gUSER_LOGIN_ID = Guid.Empty;
			
			// 02/23/2011 Paul.  SYNC service should check for lockout. 
			if ( SplendidInit.LoginFailures(Application, sUSER_NAME) >= Crm.Password.LoginLockoutCount(Application) )
			{
				L10N L10n = new L10N("en-US");
				throw(new Exception(L10n.Term("Users.ERR_USER_LOCKED_OUT")));
			}
			// 04/16/2013 Paul.  Allow system to be restricted by IP Address. 
			if ( SplendidInit.InvalidIPAddress(Application, Request.UserHostAddress) )
			{
				L10N L10n = new L10N("en-US");
				throw(new Exception(L10n.Term("Users.ERR_INVALID_IP_ADDRESS")));
			}
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				string sSQL;
				sSQL = "select ID                    " + ControlChars.CrLf
				     + "     , USER_NAME             " + ControlChars.CrLf
				     + "     , FULL_NAME             " + ControlChars.CrLf
				     + "     , IS_ADMIN              " + ControlChars.CrLf
				     + "     , STATUS                " + ControlChars.CrLf
				     + "     , PORTAL_ONLY           " + ControlChars.CrLf
				     + "     , TEAM_ID               " + ControlChars.CrLf
				     + "     , TEAM_NAME             " + ControlChars.CrLf
				     + "  from vwUSERS_Login         " + ControlChars.CrLf
				     + " where USER_NAME = @USER_NAME" + ControlChars.CrLf
				     + "   and USER_HASH = @USER_HASH" + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					string sUSER_HASH = Security.HashPassword(sPASSWORD);
					// 12/25/2009 Paul.  Use lowercase username to match the primary authentication function. 
					Sql.AddParameter(cmd, "@USER_NAME", sUSER_NAME.ToLower());
					Sql.AddParameter(cmd, "@USER_HASH", sUSER_HASH);
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							if ( dt.Rows.Count > 0 )
							{
								DataRow row = dt.Rows[0];
								Security.USER_ID     = Sql.ToGuid   (row["ID"         ]);
								Security.USER_NAME   = Sql.ToString (row["USER_NAME"  ]);
								Security.FULL_NAME   = Sql.ToString (row["FULL_NAME"  ]);
								Security.IS_ADMIN    = Sql.ToBoolean(row["IS_ADMIN"   ]);
								Security.PORTAL_ONLY = Sql.ToBoolean(row["PORTAL_ONLY"]);
								Security.TEAM_ID     = Sql.ToGuid   (row["TEAM_ID"    ]);
								Security.TEAM_NAME   = Sql.ToString (row["TEAM_NAME"  ]);
								gUSER_ID = Sql.ToGuid(row["ID"]);

								SplendidInit.LoadUserPreferences(gUSER_ID, String.Empty, String.Empty);
								SplendidInit.LoadUserACL(gUSER_ID);

                                SqlProcs.spUSERS_LOGINS_InsertOnly(ref gUSER_LOGIN_ID, gUSER_ID, sUSER_NAME, "Anonymous", "Succeeded", Session.SessionID, SplendidInit.GetRemoteHostByF5(), SplendidInit.GetServerIPAddresses(), Request.Path, Request.AppRelativeCurrentExecutionFilePath, Request.UserAgent);
								Security.USER_LOGIN_ID = gUSER_LOGIN_ID;
								// 02/20/2011 Paul.  Log the success so that we can lockout the user. 
								SplendidInit.LoginTracking(Application, sUSER_NAME, true);
								SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "SyncUser login for " + sUSER_NAME);
							}
							else
							{
                                SqlProcs.spUSERS_LOGINS_InsertOnly(ref gUSER_LOGIN_ID, Guid.Empty, sUSER_NAME, "Anonymous", "Failed", Session.SessionID, SplendidInit.GetRemoteHostByF5(), SplendidInit.GetServerIPAddresses(), Request.Path, Request.AppRelativeCurrentExecutionFilePath, Request.UserAgent);
								// 02/20/2011 Paul.  Log the failure so that we can lockout the user. 
								SplendidInit.LoginTracking(Application, sUSER_NAME, false);
								SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "SECURITY: failed attempted login for " + sUSER_NAME + " using Sync api");
							}
						}
					}
				}
			}
			if ( gUSER_ID == Guid.Empty )
			{
				SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Invalid username and/or password for " + sUSER_NAME);
				throw(new Exception("Invalid username and/or password for " + sUSER_NAME));
			}
			return gUSER_ID;
		}

		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public void Logout()
		{
			try
			{
				Guid gUSER_LOGIN_ID = Security.USER_LOGIN_ID;
				if ( !Sql.IsEmptyGuid(gUSER_LOGIN_ID) )
					SqlProcs.spUSERS_LOGINS_Logout(gUSER_LOGIN_ID);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}
			HttpContext.Current.Session.Abandon();
			// 11/15/2014 Paul.  Prevent resuse of SessionID. 
			// http://support.microsoft.com/kb/899918
			HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
		}

		public class UserProfile
		{
			public Guid   USER_ID         ;
			public string USER_NAME       ;
			public string FULL_NAME       ;
			public Guid   TEAM_ID         ;
			public string TEAM_NAME       ;
			public string USER_LANG       ;
			public string USER_DATE_FORMAT;
			public string USER_TIME_FORMAT;
			// 04/23/2013 Paul.  The HTML5 Offline Client now supports Atlantic theme. 
			public string USER_THEME      ;
			public string USER_CURRENCY_ID;
			public string USER_TIMEZONE_ID;
		}

		[OperationContract]
		[WebInvoke(Method="POST", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public UserProfile GetUserProfile()
		{
			if ( Security.IsAuthenticated() )
			{
				UserProfile profile = new UserProfile();
				profile.USER_ID          = Security.USER_ID  ;
				profile.USER_NAME        = Security.USER_NAME;
				profile.FULL_NAME        = Security.FULL_NAME;
				profile.TEAM_ID          = Security.TEAM_ID  ;
				profile.TEAM_NAME        = Security.TEAM_NAME;
				profile.USER_LANG        = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"   ]);
				profile.USER_DATE_FORMAT = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/DATEFORMAT"]);
				profile.USER_TIME_FORMAT = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/TIMEFORMAT"]);
				// 04/23/2013 Paul.  The HTML5 Offline Client now supports Atlantic theme. 
				profile.USER_THEME       = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/THEME"     ]);
				profile.USER_CURRENCY_ID = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CURRENCY"  ]);
				profile.USER_TIMEZONE_ID = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"  ]);
				return profile;
			}
			else
			{
				L10N L10n = new L10N("en-US");
				throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
			}
		}
		#endregion

		private void Terminology_AddRow(DataTable dt, string sCULTURE, string sMODULE_NAME, string sNAME, string sDISPLAY_NAME)
		{
			DataRow row = dt.NewRow();
			dt.Rows.Add(row);
			row["ID"               ] = Guid.NewGuid();
			row["DELETED"          ] = 0;
			row["CREATED_BY"       ] = DBNull.Value;
			row["DATE_ENTERED"     ] = DateTime.Now;
			row["MODIFIED_USER_ID" ] = DBNull.Value;
			row["DATE_MODIFIED"    ] = DateTime.Now;
			row["DATE_MODIFIED_UTC"] = DateTime.Now.ToUniversalTime();
			row["NAME"             ] = Sql.ToDBString(sNAME        );
			row["LANG"             ] = Sql.ToDBString(sCULTURE     );
			row["MODULE_NAME"      ] = Sql.ToDBString(sMODULE_NAME );
			row["LIST_NAME"        ] = DBNull.Value                 ;
			row["LIST_ORDER"       ] = DBNull.Value                 ;
			row["DISPLAY_NAME"     ] = Sql.ToDBString(sDISPLAY_NAME);
		}

		private void Terminology_AddRow(DataTable dt, string sCULTURE, string sNAME, string sDISPLAY_NAME, string sLIST_NAME, int nLIST_ORDER)
		{
			DataRow row = dt.NewRow();
			dt.Rows.Add(row);
			row["ID"               ] = Guid.NewGuid();
			row["DELETED"          ] = 0;
			row["CREATED_BY"       ] = DBNull.Value;
			row["DATE_ENTERED"     ] = DateTime.Now;
			row["MODIFIED_USER_ID" ] = DBNull.Value;
			row["DATE_MODIFIED"    ] = DateTime.Now;
			row["DATE_MODIFIED_UTC"] = DateTime.Now.ToUniversalTime();
			row["NAME"             ] = Sql.ToDBString(sNAME        );
			row["LANG"             ] = Sql.ToDBString(sCULTURE     );
			row["MODULE_NAME"      ] = DBNull.Value                 ;
			row["LIST_NAME"        ] = Sql.ToDBString(sLIST_NAME   );
			row["LIST_ORDER"       ] = nLIST_ORDER                  ;
			row["DISPLAY_NAME"     ] = Sql.ToDBString(sDISPLAY_NAME);
		}

		[OperationContract]
		[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Stream GetSystemTable(string TableName, string DateModified, string RequestedModules)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Cache-Control", "no-cache");
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Pragma", "no-cache");
			
			// 08/22/2011 Paul.  Add admin control to REST API. 
			string sMODULE_NAME = Sql.ToString(Application["Modules." + TableName + ".ModuleName"]);
			if ( Sql.IsEmptyString(sMODULE_NAME) )
				sMODULE_NAME = TableName;
			
			DateTime dtDateModifiedUtc = FromJsonDate(DateModified);
			DataTable dt = GetTable(TableName, dtDateModifiedUtc, RequestedModules, -1, null);
			// 09/02/2014 Paul.  We need to add dynamically generated calendar items. 
			if ( sMODULE_NAME == "Terminology" )
			{
				// 09/02/2014 Paul.  In order to prevent endless generation of this calendar data, only fetch when there is other calendar data. 
				// The SQLite trigger should prevent duplicate entries. 
				DataView vwTerminology = new DataView(dt);
				vwTerminology.RowFilter = "MODULE_NAME = 'Calendar'";
				if ( vwTerminology.Count > 0 )
				{
					string sCULTURE  = Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE" ]);
					Terminology_AddRow(dt, sCULTURE, "Calendar", "YearMonthPattern", System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.YearMonthPattern);
					Terminology_AddRow(dt, sCULTURE, "Calendar", "MonthDayPattern" , System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.MonthDayPattern );
					Terminology_AddRow(dt, sCULTURE, "Calendar", "LongDatePattern" , System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern );
					Terminology_AddRow(dt, sCULTURE, "Calendar", "ShortTimePattern", Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/TIMEFORMAT"])               );
					Terminology_AddRow(dt, sCULTURE, "Calendar", "ShortDatePattern", Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/DATEFORMAT"])               );
					Terminology_AddRow(dt, sCULTURE, "Calendar", "FirstDayOfWeek"  , ((int) System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.FirstDayOfWeek).ToString());

					for ( int i = 1; i <= 12; i++ )
					{
						string sID           = i.ToString();
						string sDISPLAY_NAME = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.MonthNames[i- 1];
						Terminology_AddRow(dt, sCULTURE, sID, sDISPLAY_NAME, "month_names_dom", i);
					}
					for ( int i = 1; i <= 12; i++ )
					{
						string sID           = i.ToString();
						string sDISPLAY_NAME = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[i- 1];
						Terminology_AddRow(dt, sCULTURE, sID, sDISPLAY_NAME, "short_month_names_dom", i);
					}
					for ( int i = 0; i <= 6; i++ )
					{
						string sID           = i.ToString();
						string sDISPLAY_NAME = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.DayNames[i];
						Terminology_AddRow(dt, sCULTURE, sID, sDISPLAY_NAME, "day_names_dom", i);
					}
					for ( int i = 0; i <= 6; i++ )
					{
						string sID           = i.ToString();
						string sDISPLAY_NAME = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[i];
						Terminology_AddRow(dt, sCULTURE, sID, sDISPLAY_NAME, "short_day_names_dom", i);
					}
				}
			}
			
			string sBaseURI = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace("/GetSystemTable", "/GetModuleItem");
			JavaScriptSerializer json = new JavaScriptSerializer();
			// 05/05/2013 Paul.  No reason to limit the Json result. 
			json.MaxJsonLength = int.MaxValue;
			
			// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
			Guid     gTIMEZONE         = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			string sResponse = json.Serialize(ToJson(sBaseURI, sMODULE_NAME, dt, T10n));
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		// 11/13/2009 Paul.  We need to be able to get a specific list if items that may be in conflict. 
		[OperationContract]
		//[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		// 08/23/2014 Paul.  Use a POST so that the items list can be unlimited. 
		public Stream GetModuleTable(Stream input)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			string sRequest = String.Empty;
			using ( StreamReader stmRequest = new StreamReader(input, System.Text.Encoding.UTF8) )
			{
				sRequest = stmRequest.ReadToEnd();
			}
			JavaScriptSerializer json = new JavaScriptSerializer();
			string[] Items = json.Deserialize<string[]>(sRequest);
			
			string TableName    = Sql.ToString (Request.QueryString["TableName"      ]);
			string DateModified = Sql.ToString (Request.QueryString["DateModified"   ]);
			int    MaxRecords   = Sql.ToInteger(Request.QueryString["MaxRecords"     ]);
			
			// 08/22/2011 Paul.  Add admin control to REST API. 
			string sMODULE_NAME = Sql.ToString(Application["Modules." + TableName + ".ModuleName"]);
			// 08/22/2011 Paul.  Not all tables will have a module name, such as relationship tables. 
			// Tables will get another security filter later in the code. 
			if ( !Sql.IsEmptyString(sMODULE_NAME) )
			{
				int nACLACCESS = Security.GetUserAccess(sMODULE_NAME, "list");
				if ( !Sql.ToBoolean(Application["Modules." + sMODULE_NAME + ".RestEnabled"]) || nACLACCESS < 0 )
				{
					L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
					throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
				}
			}
			
			List<Guid> lstItems = new List<Guid>();
			if ( Items != null )
			{
				foreach ( string sItem in Items )
				{
					Guid gItem = Sql.ToGuid(sItem);
					if ( !Sql.IsEmptyGuid(gItem) )
						lstItems.Add(gItem);
				}
			}
			DateTime dtDateModifiedUtc = FromJsonDate(DateModified);
			DataTable dt = GetTable(TableName, dtDateModifiedUtc, String.Empty, MaxRecords, lstItems.ToArray());
			
			string sBaseURI = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace("/GetModuleTable", "/GetModuleItem");
			// 05/05/2013 Paul.  No reason to limit the Json result. 
			json.MaxJsonLength = int.MaxValue;
			
			// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
			Guid     gTIMEZONE         = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			Dictionary<string, object> dictJson = ToJson(sBaseURI, sMODULE_NAME, dt, T10n);
			string sResponse = json.Serialize(dictJson);
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		private DataTable GetTable(string sTABLE_NAME, DateTime dtDATE_MODIFIED_UTC, string sREQUESTED_MODULES, int nMAX_RECORDS, Guid[] arrITEMS)
		{
			HttpContext          Context     = HttpContext.Current;
			HttpSessionState     Session     = HttpContext.Current.Session;
			HttpApplicationState Application = HttpContext.Current.Application;
			DataTable dt = null;
			try
			{
				if ( Security.IsAuthenticated() )
				{
					Regex r = new Regex(@"[^A-Za-z0-9_]");
					sTABLE_NAME = r.Replace(sTABLE_NAME, "");
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						// 06/03/2011 Paul.  Cache the Sync Table data. 
						using ( DataTable dtSYNC_TABLES = SplendidCache.SyncTables(sTABLE_NAME, false) )
						{
							string sSQL = String.Empty;
							if ( dtSYNC_TABLES != null && dtSYNC_TABLES.Rows.Count > 0 )
							{
								DataRow rowSYNC_TABLE = dtSYNC_TABLES.Rows[0];
								string sMODULE_NAME         = Sql.ToString (rowSYNC_TABLE["MODULE_NAME"        ]);
								string sVIEW_NAME           = Sql.ToString (rowSYNC_TABLE["VIEW_NAME"          ]);
								bool   bHAS_CUSTOM          = Sql.ToBoolean(rowSYNC_TABLE["HAS_CUSTOM"         ]);
								int    nMODULE_SPECIFIC     = Sql.ToInteger(rowSYNC_TABLE["MODULE_SPECIFIC"    ]);
								string sMODULE_FIELD_NAME   = Sql.ToString (rowSYNC_TABLE["MODULE_FIELD_NAME"  ]);
								bool   bIS_RELATIONSHIP     = Sql.ToBoolean(rowSYNC_TABLE["IS_RELATIONSHIP"    ]);
								string sMODULE_NAME_RELATED = Sql.ToString (rowSYNC_TABLE["MODULE_NAME_RELATED"]);
								string sASSIGNED_FIELD_NAME = Sql.ToString (rowSYNC_TABLE["ASSIGNED_FIELD_NAME"]);
								// 11/01/2009 Paul.  Protect against SQL Injection. A table name will never have a space character.
								sTABLE_NAME                 = Sql.ToString (rowSYNC_TABLE["TABLE_NAME"         ]);
								sTABLE_NAME        = r.Replace(sTABLE_NAME       , "");
								sVIEW_NAME         = r.Replace(sVIEW_NAME        , "");
								sMODULE_FIELD_NAME = r.Replace(sMODULE_FIELD_NAME, "");
								
								// 11/04/2009 Paul.  We cannot select all fields as the filter will join to the teams table. 
								// We need to make sure only to return the fields from the base table and/or the custom field table. 
								if ( bHAS_CUSTOM )
								{
									sSQL = "select " + sVIEW_NAME  + ".*     " + ControlChars.CrLf
									     + "     , " + sTABLE_NAME + "_CSTM.*" + ControlChars.CrLf
									     + "  from " + sVIEW_NAME              + ControlChars.CrLf
									     + "  left outer join " + sTABLE_NAME + "_CSTM" + ControlChars.CrLf
									     + "               on " + sTABLE_NAME + "_CSTM.ID_C = " + sVIEW_NAME + ".ID" + ControlChars.CrLf;
								}
								else
								{
									sSQL = "select " + sVIEW_NAME + ".*" + ControlChars.CrLf
									     + "  from " + sVIEW_NAME        + ControlChars.CrLf;
								}
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									cmd.CommandTimeout = 0;
									// 10/27/2009 Paul.  Apply the standard filters. 
									// 11/03/2009 Paul.  Relationship tables will not have Team or Assigned fields. 
									if ( bIS_RELATIONSHIP )
									{
										cmd.CommandText += " where 1 = 1" + ControlChars.CrLf;
										// 11/06/2009 Paul.  Use the relationship table to get the module information. 
										DataView vwRelationships = new DataView(SplendidCache.ReportingRelationships(Context.Application));
										vwRelationships.RowFilter = "(JOIN_TABLE = '" + sTABLE_NAME + "' and RELATIONSHIP_TYPE = 'many-to-many') or (RHS_TABLE = '" + sTABLE_NAME + "' and RELATIONSHIP_TYPE = 'one-to-many')";
										if ( vwRelationships.Count > 0 )
										{
											foreach ( DataRowView rowRelationship in vwRelationships )
											{
												string sJOIN_KEY_LHS             = Sql.ToString(rowRelationship["JOIN_KEY_LHS"            ]).ToUpper();
												string sJOIN_KEY_RHS             = Sql.ToString(rowRelationship["JOIN_KEY_RHS"            ]).ToUpper();
												string sLHS_MODULE               = Sql.ToString(rowRelationship["LHS_MODULE"              ]);
												string sRHS_MODULE               = Sql.ToString(rowRelationship["RHS_MODULE"              ]);
												string sLHS_TABLE                = Sql.ToString(rowRelationship["LHS_TABLE"               ]).ToUpper();
												string sRHS_TABLE                = Sql.ToString(rowRelationship["RHS_TABLE"               ]).ToUpper();
												string sLHS_KEY                  = Sql.ToString(rowRelationship["LHS_KEY"                 ]).ToUpper();
												string sRHS_KEY                  = Sql.ToString(rowRelationship["RHS_KEY"                 ]).ToUpper();
												string sRELATIONSHIP_TYPE        = Sql.ToString(rowRelationship["RELATIONSHIP_TYPE"       ]);
												string sRELATIONSHIP_ROLE_COLUMN = Sql.ToString(rowRelationship["RELATIONSHIP_ROLE_COLUMN"]).ToUpper();
												sJOIN_KEY_LHS = r.Replace(sJOIN_KEY_LHS, "");
												sJOIN_KEY_RHS = r.Replace(sJOIN_KEY_RHS, "");
												sLHS_MODULE   = r.Replace(sLHS_MODULE  , "");
												sRHS_MODULE   = r.Replace(sRHS_MODULE  , "");
												sLHS_TABLE    = r.Replace(sLHS_TABLE   , "");
												sRHS_TABLE    = r.Replace(sRHS_TABLE   , "");
												sLHS_KEY      = r.Replace(sLHS_KEY     , "");
												sRHS_KEY      = r.Replace(sRHS_KEY     , "");
												if ( sRELATIONSHIP_TYPE == "many-to-many" )
												{
													cmd.CommandText += "   and " + sJOIN_KEY_LHS + " in " + ControlChars.CrLf;
													cmd.CommandText += "(select " + sLHS_KEY + " from " + sLHS_TABLE + ControlChars.CrLf;
													Security.Filter(cmd, sLHS_MODULE, "list");
													cmd.CommandText += ")" + ControlChars.CrLf;
													
													// 11/12/2009 Paul.  We don't want to deal with relationships to multiple tables, so just ignore for now. 
													if ( sRELATIONSHIP_ROLE_COLUMN != "RELATED_TYPE" )
													{
														cmd.CommandText += "   and " + sJOIN_KEY_RHS + " in " + ControlChars.CrLf;
														cmd.CommandText += "(select " + sRHS_KEY + " from " + sRHS_TABLE + ControlChars.CrLf;
														Security.Filter(cmd, sRHS_MODULE, "list");
														cmd.CommandText += ")" + ControlChars.CrLf;
													}
												}
												else if ( sRELATIONSHIP_TYPE == "one-to-many" )
												{
													cmd.CommandText += "   and " + sRHS_KEY + " in " + ControlChars.CrLf;
													cmd.CommandText += "(select " + sLHS_KEY + " from " + sLHS_TABLE + ControlChars.CrLf;
													Security.Filter(cmd, sLHS_MODULE, "list");
													cmd.CommandText += ")" + ControlChars.CrLf;
												}
											}
										}
										else
										{
											// 11/12/2009 Paul.  EMAIL_IMAGES is a special table that is related to EMAILS or KBDOCUMENTS. 
											if ( sTABLE_NAME == "EMAIL_IMAGES" )
											{
												// 11/12/2009 Paul.  There does not appear to be an easy way to filter the EMAIL_IMAGES table. 
												// For now, just return the EMAIL related images. 
												cmd.CommandText += "   and PARENT_ID in " + ControlChars.CrLf;
												cmd.CommandText += "(select ID from EMAILS" + ControlChars.CrLf;
												Security.Filter(cmd, "Emails", "list");
												cmd.CommandText += "union all" + ControlChars.CrLf;
												cmd.CommandText += "select ID from KBDOCUMENTS" + ControlChars.CrLf;
												Security.Filter(cmd, "KBDocuments", "list");
												cmd.CommandText += ")" + ControlChars.CrLf;
											}
											// 11/06/2009 Paul.  If the relationship is not in the RELATIONSHIPS table, then try and build it manually. 
											// 11/05/2009 Paul.  We cannot use the standard filter on the Teams table (or TeamNotices). 
											else if ( !Sql.IsEmptyString(sMODULE_NAME) && !sMODULE_NAME.StartsWith("Team") )
											{
												// 11/05/2009 Paul.  We could query the foreign key tables to perpare the filters, but that is slow. 
												string sMODULE_TABLE_NAME   = Sql.ToString(Context.Application["Modules." + sMODULE_NAME + ".TableName"]).ToUpper();
												if ( !Sql.IsEmptyString(sMODULE_TABLE_NAME) )
												{
													string sMODULE_FIELD_ID = String.Empty;
													if ( sMODULE_TABLE_NAME.EndsWith("IES") )
														sMODULE_FIELD_ID = sMODULE_TABLE_NAME.Substring(0, sMODULE_TABLE_NAME.Length - 3) + "Y_ID";
													else if ( sMODULE_TABLE_NAME.EndsWith("S") )
														sMODULE_FIELD_ID = sMODULE_TABLE_NAME.Substring(0, sMODULE_TABLE_NAME.Length - 1) + "_ID";
													else
														sMODULE_FIELD_ID = sMODULE_TABLE_NAME + "_ID";
													
													cmd.CommandText += "   and " + sMODULE_FIELD_ID + " in " + ControlChars.CrLf;
													cmd.CommandText += "(select ID from " + sMODULE_TABLE_NAME + ControlChars.CrLf;
													Security.Filter(cmd, sMODULE_NAME, "list");
													cmd.CommandText += ")" + ControlChars.CrLf;
												}
											}
											// 11/05/2009 Paul.  We cannot use the standard filter on the Teams table. 
											if ( !Sql.IsEmptyString(sMODULE_NAME_RELATED) && !sMODULE_NAME_RELATED.StartsWith("Team") )
											{
												string sMODULE_TABLE_RELATED = Sql.ToString(Context.Application["Modules." + sMODULE_NAME_RELATED + ".TableName"]).ToUpper();
												if ( !Sql.IsEmptyString(sMODULE_TABLE_RELATED) )
												{
													string sMODULE_RELATED_ID = String.Empty;
													if ( sMODULE_TABLE_RELATED.EndsWith("IES") )
														sMODULE_RELATED_ID = sMODULE_TABLE_RELATED.Substring(0, sMODULE_TABLE_RELATED.Length - 3) + "Y_ID";
													else if ( sMODULE_TABLE_RELATED.EndsWith("S") )
														sMODULE_RELATED_ID = sMODULE_TABLE_RELATED.Substring(0, sMODULE_TABLE_RELATED.Length - 1) + "_ID";
													else
														sMODULE_RELATED_ID = sMODULE_TABLE_RELATED + "_ID";
													// 11/05/2009 Paul.  Some tables use ASSIGNED_USER_ID as the relationship ID instead of the USER_ID. 
													if ( sMODULE_RELATED_ID == "USER_ID" && !Sql.IsEmptyString(sASSIGNED_FIELD_NAME) )
														sMODULE_RELATED_ID = sASSIGNED_FIELD_NAME;
													
													cmd.CommandText += "   and " + sMODULE_RELATED_ID + " in " + ControlChars.CrLf;
													cmd.CommandText += "(select ID from " + sMODULE_TABLE_RELATED + ControlChars.CrLf;
													Security.Filter(cmd, sMODULE_NAME_RELATED, "list");
													cmd.CommandText += ")" + ControlChars.CrLf;
												}
											}
										}
									}
									else
									{
										// 02/14/2010 Paul.  GetTable should only require read-only access. 
										// We were previously requiring Edit access, but that seems to be a high bar. 
										Security.Filter(cmd, sMODULE_NAME, "view");
									}
									if ( !Sql.IsEmptyString(sMODULE_FIELD_NAME) )
									{
										// 11/08/2009 Paul.  We need to combine the two module lists into a single list. 
										List<string> lstMODULES = new List<string>();
										List<string> lstAVAILABLE_MODULES = SplendidCache.AccessibleModules(Context, Security.USER_ID);
										if ( lstAVAILABLE_MODULES != null )
										{
											if ( !Sql.IsEmptyString(sREQUESTED_MODULES) )
											{
												sREQUESTED_MODULES = Regex.Replace(sREQUESTED_MODULES, @"[^A-Za-z0-9_,]", "");
												string[] arrREQUESTED_MODULES = sREQUESTED_MODULES.Split(',');
												foreach ( string s in arrREQUESTED_MODULES )
												{
													// 11/08/2009 Paul.  The module must be in both lists. 
													if ( lstAVAILABLE_MODULES.Contains(s) )
														lstMODULES.Add(s);
													// 09/30/2014 Paul.  Need to add activities manually. 
													if ( s == "Calls" || s == "Meetings" )
														lstMODULES.Add("Activities");
												}
											}
											else
											{
												lstMODULES = lstAVAILABLE_MODULES;
											}
										}
										// 11/14/2009 Paul.  Make sure to add Teams to the list of available modules. 
										if ( Crm.Config.enable_team_management() )
										{
											if ( !lstMODULES.Contains("Teams") )
												lstMODULES.Add("Teams");
											if ( !lstMODULES.Contains("TeamNotices") )
												lstMODULES.Add("TeamNotices");
										}
										// 11/22/2009 Paul.  Simplify the logic by having a local list of system modules. 
										string[] arrSystemModules = new string[] { "ACL", "ACLActions", "ACLRoles", "Audit", "Config", "Currencies", "Dashlets"
										                                         , "DocumentRevisions", "DynamicButtons", "Export", "FieldValidators", "Import"
										                                         , "Merge", "Modules", "Offline", "Releases", "Roles", "SavedSearch", "Shortcuts"
										                                         , "Teams", "TeamNotices", "Terminology", "Users", "SystemSyncLog"
										                                         };
										foreach ( string sSystemModule in arrSystemModules )
											lstMODULES.Add(sSystemModule);
										
										if ( sTABLE_NAME == "MODULES" )
										{
											// 11/27/2009 Paul.  Don't filter the MODULES table. It can cause system tables to get deleted. 
											// 11/28/2009 Paul.  Keep the filter on the Modules table, but add the System Sync Tables to the list. 
											// We should make sure that the clients do not get module records for unnecessary or disabled modules. 
											Sql.AppendParameter(cmd, lstMODULES.ToArray(), sMODULE_FIELD_NAME);
										}
										else if ( nMODULE_SPECIFIC == 1 )
										{
											Sql.AppendParameter(cmd, lstMODULES.ToArray(), sMODULE_FIELD_NAME);
											// 09/30/2014 Paul.  We need to filter target buttons as well. 
											if ( sTABLE_NAME == "DYNAMIC_BUTTONS" )
											{
												cmd.CommandText += "   and ( 1 = 0" + ControlChars.CrLf;
												cmd.CommandText += "         or TARGET_NAME is null" + ControlChars.CrLf;
												cmd.CommandText += "     ";
												Sql.AppendParameter(cmd, lstMODULES.ToArray(), "TARGET_NAME", true);
												cmd.CommandText += "       )" + ControlChars.CrLf;
											}
										}
										else if ( nMODULE_SPECIFIC == 2 )
										{
											// 04/05/2012 Paul.  AppendLikeModules is a special like that assumes that the search is for a module related value 
											Sql.AppendLikeModules(cmd, lstMODULES.ToArray(), sMODULE_FIELD_NAME);
										}
										else if ( nMODULE_SPECIFIC == 3 )
										{
											cmd.CommandText += "   and ( 1 = 0" + ControlChars.CrLf;
											cmd.CommandText += "         or " + sMODULE_FIELD_NAME + " is null" + ControlChars.CrLf;
											// 11/02/2009 Paul.  There are a number of terms with undefined modules. 
											// ACL, ACLActions, Audit, Config, Dashlets, DocumentRevisions, Export, Merge, Roles, SavedSearch, Teams
											cmd.CommandText += "     ";
											Sql.AppendParameter(cmd, lstMODULES.ToArray(), sMODULE_FIELD_NAME, true);
											cmd.CommandText += "       )" + ControlChars.CrLf;
										}
										// 11/22/2009 Paul.  Make sure to only send the selected user language.  This will dramatically reduce the amount of data. 
										if ( sTABLE_NAME == "TERMINOLOGY" || sTABLE_NAME == "TERMINOLOGY_HELP" )
										{
											cmd.CommandText += "   and LANG in ('en-US', @LANG)" + ControlChars.CrLf;
											string sCULTURE  = Sql.ToString(Session["USER_SETTINGS/CULTURE" ]);
											Sql.AddParameter(cmd, "@LANG", sCULTURE);
										}
									}
		
									if ( dtDATE_MODIFIED_UTC != DateTime.MinValue )
									{
										cmd.CommandText += "   and " + sVIEW_NAME + ".DATE_MODIFIED_UTC > @DATE_MODIFIED_UTC" + ControlChars.CrLf;
										Sql.AddParameter(cmd, "@DATE_MODIFIED_UTC", dtDATE_MODIFIED_UTC);
									}
									if ( arrITEMS != null )
									{
										// 11/13/2009 Paul.  If a list of items is provided, then the max records field is ignored. 
										nMAX_RECORDS = -1;
										Sql.AppendGuids(cmd, arrITEMS, "ID");
									}
									else if ( sTABLE_NAME == "IMAGES" )
									{
										// 02/14/2010 Paul.  There is no easy way to filter IMAGES table, so we are simply going to fetch 
										// images that the user has created.  Otherwise, images that are accessible to the user will 
										// need to be retrieved by ID.
										Sql.AppendParameter(cmd, Security.USER_ID, "CREATED_BY");
									}
									cmd.CommandText += " order by " + sVIEW_NAME + ".DATE_MODIFIED_UTC" + ControlChars.CrLf;
#if DEBUG
									SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), Sql.ExpandParameters(cmd));
#endif
									
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										// 11/08/2009 Paul.  The table name is required in order to serialize the DataTable. 
										dt = new DataTable(sTABLE_NAME);
										if ( nMAX_RECORDS > 0 )
										{
											using ( DataSet ds = new DataSet() )
											{
												ds.Tables.Add(dt);
												da.Fill(ds, 0, nMAX_RECORDS, sTABLE_NAME);
											}
										}
										else
										{
											da.Fill(dt);
										}
										// 01/18/2010 Paul.  Apply ACL Field Security. 
										// 02/01/2010 Paul.  System tables may not have a valid Module name, so Field Security will not apply. 
										if ( SplendidInit.bEnableACLFieldSecurity && !Sql.IsEmptyString(sMODULE_NAME) )
										{
											bool bApplyACL = false;
											bool bASSIGNED_USER_ID_Exists = dt.Columns.Contains("ASSIGNED_USER_ID");
											foreach ( DataRow row in dt.Rows )
											{
												Guid gASSIGNED_USER_ID = Guid.Empty;
												if ( bASSIGNED_USER_ID_Exists )
													gASSIGNED_USER_ID = Sql.ToGuid(row["ASSIGNED_USER_ID"]);
												foreach ( DataColumn col in dt.Columns )
												{
													Security.ACL_FIELD_ACCESS acl = Security.GetUserFieldSecurity(sMODULE_NAME, col.ColumnName, gASSIGNED_USER_ID);
													if ( !acl.IsReadable() )
													{
														row[col.ColumnName] = DBNull.Value;
														bApplyACL = true;
													}
												}
											}
											if ( bApplyACL )
												dt.AcceptChanges();
										}
										if ( sTABLE_NAME == "USERS" )
										{
											// 11/12/2009 Paul.  For the USERS table, we are going to limit the data return to the client. 
											foreach ( DataRow row in dt.Rows )
											{
												if ( Sql.ToGuid(row["ID"]) != Security.USER_ID )
												{
													foreach ( DataColumn col in dt.Columns )
													{
														// 11/12/2009 Paul.  Allow auditing fields and basic user info. 
														if (  col.ColumnName != "ID"               
														   && col.ColumnName != "DELETED"          
														   && col.ColumnName != "CREATED_BY"       
														   && col.ColumnName != "DATE_ENTERED"     
														   && col.ColumnName != "MODIFIED_USER_ID" 
														   && col.ColumnName != "DATE_MODIFIED"    
														   && col.ColumnName != "DATE_MODIFIED_UTC"
														   && col.ColumnName != "USER_NAME"        
														   && col.ColumnName != "FIRST_NAME"       
														   && col.ColumnName != "LAST_NAME"        
														   && col.ColumnName != "REPORTS_TO_ID"    
														   && col.ColumnName != "EMAIL1"           
														   && col.ColumnName != "STATUS"           
														   && col.ColumnName != "IS_GROUP"         
														   && col.ColumnName != "PORTAL_ONLY"      
														   && col.ColumnName != "EMPLOYEE_STATUS"  
														   )
														{
															row[col.ColumnName] = DBNull.Value;
														}
													}
												}
											}
											dt.AcceptChanges();
										}
									}
								}
							}
							else
							{
								SplendidError.SystemError(new StackTrace(true).GetFrame(0), sTABLE_NAME + " cannot be synchronized.");
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return dt;
		}

		[OperationContract]
		// 03/13/2011 Paul.  Must use octet-stream instead of json, outherwise we get the following error. 
		// Incoming message for operation 'CreateRecord' (contract 'AddressService' with namespace 'http://tempuri.org/') contains an unrecognized http body format value 'Json'. 
		// The expected body format value is 'Raw'. This can be because a WebContentTypeMapper has not been configured on the binding. See the documentation of WebContentTypeMapper for more details.
		//xhr.setRequestHeader('content-type', 'application/octet-stream');
		public Stream UpdateModuleTable(Stream input)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			string sRequest = String.Empty;
			using ( StreamReader stmRequest = new StreamReader(input, System.Text.Encoding.UTF8) )
			{
				sRequest = stmRequest.ReadToEnd();
			}
			// http://weblogs.asp.net/hajan/archive/2010/07/23/javascriptserializer-dictionary-to-json-serialization-and-deserialization.aspx
			JavaScriptSerializer json = new JavaScriptSerializer();
			Dictionary<string, object>[] dict = json.Deserialize<Dictionary<string, object>[]>(sRequest);

			bool bSaveDuplicate   = Sql.ToBoolean(Request.QueryString["SaveDuplicate"  ]);
			bool bSaveConcurrency = Sql.ToBoolean(Request.QueryString["SaveConcurrency"]);
			string sTableName     = Sql.ToString (Request.QueryString["TableName"      ]);
			if ( Sql.IsEmptyString(sTableName) )
				throw(new Exception("The table name must be specified."));
			if ( !Security.IsAuthenticated() )
			{
				L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
				throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
			}
			// 08/22/2011 Paul.  Add admin control to REST API. 
			string sMODULE_NAME = Sql.ToString(Application["Modules." + sTableName + ".ModuleName"]);
			// 08/22/2011 Paul.  Not all tables will have a module name, such as relationship tables. 
			// Tables will get another security filter later in the code. 
			if ( !Sql.IsEmptyString(sMODULE_NAME) )
			{
				int nACLACCESS = Security.GetUserAccess(sMODULE_NAME, "edit");
				if ( !Sql.ToBoolean(Application["Modules." + sMODULE_NAME + ".RestEnabled"]) || nACLACCESS < 0 )
				{
					L10N L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
					throw(new Exception(L10n.Term("ACL.LBL_INSUFFICIENT_ACCESS")));
				}
			}
			
			DataTable dt = UpdateTable(sTableName, bSaveDuplicate, bSaveConcurrency, dict);

			string sBaseURI = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace("/GetModuleTable", "/GetModuleItem");
			// 05/05/2013 Paul.  No reason to limit the Json result. 
			json.MaxJsonLength = int.MaxValue;
			
			// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
			Guid     gTIMEZONE         = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			string sResponse = json.Serialize(ToJson(sBaseURI, sMODULE_NAME, dt, T10n));
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		private DataTable UpdateTable(string sTABLE_NAME, bool bSaveDuplicate, bool bSaveConcurrency, Dictionary<string, object>[] dictRows)
		{
			HttpSessionState Session = HttpContext.Current.Session;
			DataTable dtResults = null;
			try
			{
				// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
				Guid     gTIMEZONE = Sql.ToGuid  (HttpContext.Current.Session["USER_SETTINGS/TIMEZONE"]);
				TimeZone T10n      = TimeZone.CreateTimeZone(gTIMEZONE);
				// 03/14/2014 Paul.  DUPLICATE_CHECHING_ENABLED enables duplicate checking. 
				DataTable dtUPDATE = new DataTable(sTABLE_NAME);
				for ( int iRow = 0; iRow < dictRows.Length; iRow++ )
				{
					Dictionary<string, object> dict = dictRows[iRow];
					if ( iRow == 0 )
					{
						foreach ( string sColumnName in dict.Keys )
						{
							dtUPDATE.Columns.Add(sColumnName);
						}
					}
					DataRow rowUPDATE = dtUPDATE.NewRow();
					dtUPDATE.Rows.Add(rowUPDATE);
					foreach ( string sColumnName in dict.Keys )
					{
						// 09/09/2011 Paul.  Multi-selection list boxes will come in as an ArrayList. 
						if ( dict[sColumnName] is System.Collections.ArrayList )
						{
							System.Collections.ArrayList lst = dict[sColumnName] as System.Collections.ArrayList;
							XmlDocument xml = new XmlDocument();
							xml.AppendChild(xml.CreateXmlDeclaration("1.0", "UTF-8", null));
							xml.AppendChild(xml.CreateElement("Values"));
							if ( lst.Count > 0 )
							{
								foreach(string item in lst)
								{
									XmlNode xValue = xml.CreateElement("Value");
									xml.DocumentElement.AppendChild(xValue);
									xValue.InnerText = item;
								}
							}
							rowUPDATE[sColumnName] = xml.OuterXml;
						}
						else if ( sColumnName != "SaveDuplicate" && sColumnName != "SaveConcurrency" && sColumnName != "LAST_DATE_MODIFIED" )
						{
							rowUPDATE[sColumnName] = dict[sColumnName];
						}
					}
				}
				
				// 08/30/2014 Paul.  Cloning will not work here as the date values will be treated as strings. Select an empty row instead. 
				//dtResults = dtUPDATE.Clone();
				//dtResults.Columns.Add("SPLENDID_SYNC_STATUS" , typeof(System.String));
				//dtResults.Columns.Add("SPLENDID_SYNC_MESSAGE", typeof(System.String));
				if ( Security.IsAuthenticated() )
				{
					string sCULTURE = Sql.ToString (Session["USER_SETTINGS/CULTURE"]);
					L10N   L10n     = new L10N(sCULTURE);
					Regex  r        = new Regex(@"[^A-Za-z0-9_]");
					sTABLE_NAME = r.Replace(sTABLE_NAME, "").ToUpper();
					
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						// 06/03/2011 Paul.  Cache the Sync Table data. 
						// 11/26/2009 Paul.  System tables cannot be updated. 
						using ( DataTable dtSYNC_TABLES = SplendidCache.SyncTables(sTABLE_NAME, true) )
						{
							string sSQL = String.Empty;
							if ( dtSYNC_TABLES != null && dtSYNC_TABLES.Rows.Count > 0 )
							{
								DataRow rowSYNC_TABLE = dtSYNC_TABLES.Rows[0];
								string sMODULE_NAME = Sql.ToString (rowSYNC_TABLE["MODULE_NAME"]);
								string sVIEW_NAME   = Sql.ToString (rowSYNC_TABLE["VIEW_NAME"  ]);
								bool   bHAS_CUSTOM  = Sql.ToBoolean(rowSYNC_TABLE["HAS_CUSTOM" ]);
								// 02/14/2010 Paul.  GetUserAccess requires a non-null sMODULE_NAME. 
								// Lets catch the exception here so that we can throw a meaningful error. 
								if ( Sql.IsEmptyString(sMODULE_NAME) && !sTABLE_NAME.StartsWith("TEAM_SETS") )
								{
									throw(new Exception("sMODULE_NAME should not be empty for table " + sTABLE_NAME));
								}
								
								// 08/30/2014 Paul.  Cloning will not work here as the date values will be treated as strings. Select an empty rowset instead. 
								if ( bHAS_CUSTOM )
								{
									sSQL = "select " + sVIEW_NAME  + ".*     " + ControlChars.CrLf
									     + "     , " + sTABLE_NAME + "_CSTM.*" + ControlChars.CrLf
									     + "  from " + sVIEW_NAME              + ControlChars.CrLf
									     + "  left outer join " + sTABLE_NAME + "_CSTM" + ControlChars.CrLf
									     + "               on " + sTABLE_NAME + "_CSTM.ID_C = " + sVIEW_NAME + ".ID" + ControlChars.CrLf
									     + " where ID is null"                 + ControlChars.CrLf;
								}
								else
								{
									sSQL = "select " + sVIEW_NAME + ".*" + ControlChars.CrLf
									     + "  from " + sVIEW_NAME        + ControlChars.CrLf
									     + " where ID is null"           + ControlChars.CrLf;
								}
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										dtResults = new DataTable();
										da.Fill(dtResults);
										dtResults.Columns.Add("SPLENDID_SYNC_STATUS" , typeof(System.String));
										dtResults.Columns.Add("SPLENDID_SYNC_MESSAGE", typeof(System.String));
										// 08/30/2014 Paul.  Now we have an empty table that matches the updated table. 
									}
								}
								// 11/11/2009 Paul.  First check if the user has access to this module. 
								// 11/27/2009 Paul.  TEAM_SETS and TEAM_SETS_TEAMS are special module-type tables that do not have the normal security rules. 
								if ( sTABLE_NAME.StartsWith("TEAM_SETS") || SplendidCRM.Security.GetUserAccess(sMODULE_NAME, "edit") >= 0 )
								{
									foreach ( DataRow row in dtUPDATE.Rows )
									{
										bool      bRecordExists              = false;
										bool      bAccessAllowed             = false;
										bool      bConflicted                = false;
										string    sConflictedMessage         = String.Empty;
										Guid      gID                        = Sql.ToGuid    (row["ID"                      ]);
										DateTime  dtREMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(row["REMOTE_DATE_MODIFIED_UTC"]);
										Guid      gLOCAL_ASSIGNED_USER_ID    = Guid.Empty;
										DataRow   rowCurrent                 = null;
										DataTable dtCurrent                  = new DataTable();
										sSQL = "select *"              + ControlChars.CrLf
										     + "  from " + sTABLE_NAME + ControlChars.CrLf
										     + " where 1 = 1"          + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Sql.AppendParameter(cmd, gID, "ID");
											using ( DbDataAdapter da = dbf.CreateDataAdapter() )
											{
												((IDbDataAdapter)da).SelectCommand = cmd;
												// 11/27/2009 Paul.  It may be useful to log the SQL during errors at this location. 
												try
												{
													da.Fill(dtCurrent);
												}
												catch
												{
													SplendidError.SystemError(new StackTrace(true).GetFrame(0), Sql.ExpandParameters(cmd));
													throw;
												}
												if ( dtCurrent.Rows.Count > 0 )
												{
													rowCurrent = dtCurrent.Rows[0];
													bRecordExists = true;
													// 08/31/2014 Paul.  The offline client will already do conflict management, so there is no need to duplicate here. 
													// This allows the user decide if they want to override the conflict on the offline client. 
													/*
													if ( dtREMOTE_DATE_MODIFIED_UTC != DateTime.MinValue )
													{
														// 03/16/2014 Paul.  Throw an exception if the record has been edited since the last load. 
														// 03/16/2014 Paul.  Enable override of concurrency error. 
														if ( Sql.ToBoolean(HttpContext.Current.Application["CONFIG.enable_concurrency_check"])  && !bSaveConcurrency && Sql.ToDateTime(rowCurrent["DATE_MODIFIED_UTC"]) > dtREMOTE_DATE_MODIFIED_UTC )
														{
															bConflicted = true;
															DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(rowCurrent["DATE_MODIFIED_UTC"]);
															sConflictedMessage = String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED);
														}
													}
													*/
													// 01/18/2010 Paul.  Apply ACL Field Security. 
													if ( dtCurrent.Columns.Contains("ASSIGNED_USER_ID") )
													{
														gLOCAL_ASSIGNED_USER_ID = Sql.ToGuid(rowCurrent["ASSIGNED_USER_ID"]);
													}
												}
											}
										}
										// 11/11/2009 Paul.  If we have conflicting edits, exit without generating an error. 
										// The conflict will eventually get reported on the client side. 
										if ( !bConflicted )
										{
											// 11/27/2009 Paul.  TEAM_SETS and TEAM_SETS_TEAMS are special module-type tables that do not have the normal security rules. 
											if ( sTABLE_NAME.StartsWith("TEAM_SETS") )
												bAccessAllowed = true;
											else if ( bRecordExists )
											{
												sSQL = "select count(*)"       + ControlChars.CrLf
												     + "  from " + sTABLE_NAME + ControlChars.CrLf;
												using ( IDbCommand cmd = con.CreateCommand() )
												{
													cmd.CommandText = sSQL;
													Security.Filter(cmd, sMODULE_NAME, "edit");
													Sql.AppendParameter(cmd, gID, "ID");
													// 11/27/2009 Paul.  It may be useful to log the SQL during errors at this location. 
													try
													{
														if ( Sql.ToInteger(cmd.ExecuteScalar()) > 0 )
															bAccessAllowed = true;
													}
													catch
													{
														SplendidError.SystemError(new StackTrace(true).GetFrame(0), Sql.ExpandParameters(cmd));
														throw;
													}
												}
											}
											if ( !bRecordExists || bAccessAllowed )
											{
												bool bDuplicateExists = false;
												// 03/14/2014 Paul.  DUPLICATE_CHECHING_ENABLED enables duplicate checking. 
												HttpApplicationState Application = HttpContext.Current.Application;
												bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + sMODULE_NAME + ".DuplicateCheckingEnabled"]) && !bSaveDuplicate;
												if ( bDUPLICATE_CHECHING_ENABLED )
												{
													if ( Utils.DuplicateCheck(Application, con, sMODULE_NAME, gID, row, rowCurrent) > 0 )
													{
														bDuplicateExists = true;
														// 03/16/2014 Paul.  Put the error name at the end so that we can detect the event. 
														throw(new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION") + ".ERR_DUPLICATE_EXCEPTION"));
													}
												}
												if ( !bDuplicateExists )
												{
													DataTable dtMetadata = SplendidCache.SqlColumns(sTABLE_NAME);
													using ( IDbTransaction trn = Sql.BeginTransaction(con) )
													{
														try
														{
															bool bEnableTeamManagement  = Crm.Config.enable_team_management();
															bool bRequireTeamManagement = Crm.Config.require_team_management();
															bool bRequireUserAssignment = Crm.Config.require_user_assignment();
															//IDbCommand cmdUpdate = SqlProcs.Factory(con, "sp" + sTABLE_NAME + "_Update");
															// 11/26/2009 Paul.  Using the update stored procedures is problematic when relationships are created. 
															// The problem is that the relationship will be sync'd separately and can cause duplicate records. 
															// 08/27/2014 Paul.  There is too much logic to ignore, including normalizing teams and adding to the phone search table. 
															// Most relationship procedures prevent duplicates, so it should not be an issue. 
															/*
															IDbCommand cmdUpdate = con.CreateCommand();
															cmdUpdate.CommandType = CommandType.Text;
															cmdUpdate.Transaction = trn;
															if ( !bRecordExists )
															{
																// 11/26/2009 Paul.  Build the command text first.  This is necessary in order for the parameter function
																// to properly replace the @ symbol with the database-specific token. 
																StringBuilder sb = new StringBuilder();
																sb.Append("insert into " + sTABLE_NAME + "(");
																for( int i=0 ; i < dtMetadata.Rows.Count; i++ )
																{
																	DataRow rowMetadata = dtMetadata.Rows[i];
																	if ( i > 0 )
																		sb.Append(", ");
																	sb.Append(Sql.ToString (rowMetadata["ColumnName"]));
																}
																sb.AppendLine(")" + ControlChars.CrLf);
																sb.AppendLine("values(");
																for( int i=0 ; i < dtMetadata.Rows.Count; i++ )
																{
																	DataRow rowMetadata = dtMetadata.Rows[i];
																	if ( i > 0 )
																		sb.Append(", ");
																	sb.Append(Sql.CreateDbName(cmdUpdate, "@" + Sql.ToString(rowMetadata["ColumnName"])));
																}
																sb.AppendLine(")");
																cmdUpdate.CommandText = sb.ToString();
															
																foreach ( DataRow rowMetadata in dtMetadata.Rows )
																{
																	string sColumnName = Sql.ToString (rowMetadata["ColumnName"]);
																	string sCsType     = Sql.ToString (rowMetadata["CsType"    ]);
																	int    nLength     = Sql.ToInteger(rowMetadata["length"    ]);
																	IDbDataParameter par = Sql.CreateParameter(cmdUpdate, "@" + sColumnName, sCsType, nLength);
																	// 06/04/2009 Paul.  If Team is required, then make sure to initialize the TEAM_ID.  Same is true for ASSIGNED_USER_ID. 
																	if ( String.Compare(sColumnName, "TEAM_ID", true) == 0 && bEnableTeamManagement ) // 02/26/2011 Paul.  Ignore the Required flag. && bRequireTeamManagement )
																		par.Value = Sql.ToDBGuid(Security.TEAM_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																	else if ( String.Compare(sColumnName, "ASSIGNED_USER_ID", true) == 0 ) // 02/26/2011 Paul.  Always set the Assigned User ID. && bRequireUserAssignment )
																		par.Value = Sql.ToDBGuid(Security.USER_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																	// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																	else if ( String.Compare(sColumnName, "DATE_MODIFIED_UTC", true) == 0 )
																		par.Value = DateTime.UtcNow;
																	else
																		par.Value = DBNull.Value;
																}
															}
															else
															{
																// 11/26/2009 Paul.  Build the command text first.  This is necessary in order for the parameter function
																// to properly replace the @ symbol with the database-specific token. 
																StringBuilder sb = new StringBuilder();
																for( int i=0 ; i < dtMetadata.Rows.Count; i++ )
																{
																	DataRow rowMetadata = dtMetadata.Rows[i];
																	string sColumnName = Sql.ToString(rowMetadata["ColumnName"]);
																	if ( String.Compare(sColumnName, "ID", true) != 0 )
																	{
																		if ( sb.Length == 0 )
																			sb.Append("   set ");
																		else
																			sb.Append("     , ");
																		sb.Append(sColumnName);
																		sb.Append(" = @");
																		sb.Append(sColumnName);
																		sb.Append(ControlChars.CrLf);
																	}
																}
																cmdUpdate.CommandText = "update " + sTABLE_NAME + ControlChars.CrLf
																                      + sb.ToString()
																                      + " where ID = @ID";
															
																foreach ( DataRow rowMetadata in dtMetadata.Rows )
																{
																	string sColumnName = Sql.ToString (rowMetadata["ColumnName"]);
																	string sCsType     = Sql.ToString (rowMetadata["CsType"    ]);
																	int    nLength     = Sql.ToInteger(rowMetadata["length"    ]);
																	if ( String.Compare(sColumnName, "ID", true) != 0 )
																	{
																		IDbDataParameter par = Sql.CreateParameter(cmdUpdate, "@" + sColumnName, sCsType, nLength);
																		// 06/04/2009 Paul.  If Team is required, then make sure to initialize the TEAM_ID.  Same is true for ASSIGNED_USER_ID. 
																		if ( String.Compare(sColumnName, "TEAM_ID", true) == 0 && bEnableTeamManagement ) // 02/26/2011 Paul.  Ignore the Required flag. && bRequireTeamManagement )
																			par.Value = Sql.ToDBGuid(Security.TEAM_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																		else if ( String.Compare(sColumnName, "ASSIGNED_USER_ID", true) == 0 ) // 02/26/2011 Paul.  Always set the Assigned User ID. && bRequireUserAssignment )
																			par.Value = Sql.ToDBGuid(Security.USER_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																		// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																		else if ( String.Compare(sColumnName, "DATE_MODIFIED_UTC", true) == 0 )
																			par.Value = DateTime.UtcNow;
																		else
																			par.Value = DBNull.Value;
																	}
																}
																Sql.AddParameter(cmdUpdate, "@ID", gID);
																// 11/11/2009 Paul.  If the record already exists, then the current values are treated as default values. 
																foreach ( DataColumn col in rowCurrent.Table.Columns )
																{
																	IDbDataParameter par = Sql.FindParameter(cmdUpdate, col.ColumnName);
																	// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																	if ( par != null && String.Compare(col.ColumnName, "DATE_MODIFIED_UTC", true) != 0 )
																		par.Value = rowCurrent[col.ColumnName];
																}
															}
															
															foreach ( DataColumn col in row.Table.Columns )
															{
																// 01/18/2010 Paul.  Apply ACL Field Security. 
																// 02/01/2010 Paul.  System tables may not have a valid Module name, so Field Security will not apply. 
																bool bIsWriteable = true;
																if ( SplendidInit.bEnableACLFieldSecurity && !Sql.IsEmptyString(sMODULE_NAME) )
																{
																	Security.ACL_FIELD_ACCESS acl = Security.GetUserFieldSecurity(sMODULE_NAME, col.ColumnName, Guid.Empty);
																	bIsWriteable = acl.IsWriteable();
																}
																if ( bIsWriteable )
																{
																	IDbDataParameter par = Sql.FindParameter(cmdUpdate, col.ColumnName);
																	// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																	if ( par != null && String.Compare(col.ColumnName, "DATE_MODIFIED_UTC", true) != 0 )
																	{
																		switch ( par.DbType )
																		{
																			// 10/08/2011 Paul.  We must use Sql.ToDBDateTime, otherwise we get a an error whe DateTime.MinValue is used. 
																			// SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.
																			// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
																			case DbType.Date                 :  par.Value = Sql.ToDBDateTime(T10n.ToServerTime(FromJsonDate(Sql.ToString(row[col.ColumnName]))));  break;
																			case DbType.DateTime             :  par.Value = Sql.ToDBDateTime(T10n.ToServerTime(FromJsonDate(Sql.ToString(row[col.ColumnName]))));  break;
																			case DbType.Int16                :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.Int32                :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.Int64                :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.UInt16               :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.UInt32               :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.UInt64               :  par.Value = Sql.ToDBInteger(row[col.ColumnName]);  break;
																			case DbType.Single               :  par.Value = Sql.ToDBFloat  (row[col.ColumnName]);  break;
																			case DbType.Double               :  par.Value = Sql.ToDBFloat  (row[col.ColumnName]);  break;
																			case DbType.Decimal              :  par.Value = Sql.ToDBDecimal(row[col.ColumnName]);  break;
																			case DbType.Currency             :  par.Value = Sql.ToDBDecimal(row[col.ColumnName]);  break;
																			case DbType.Boolean              :  par.Value = Sql.ToDBBoolean(row[col.ColumnName]);  break;
																			case DbType.Guid                 :  par.Value = Sql.ToDBGuid   (row[col.ColumnName]);  break;
																			case DbType.String               :  par.Value = Sql.ToDBString (row[col.ColumnName]);  break;
																			case DbType.StringFixedLength    :  par.Value = Sql.ToDBString (row[col.ColumnName]);  break;
																			case DbType.AnsiString           :  par.Value = Sql.ToDBString (row[col.ColumnName]);  break;
																			case DbType.AnsiStringFixedLength:  par.Value = Sql.ToDBString (row[col.ColumnName]);  break;
																		}
																	}
																}
															}
															*/
															// 08/27/2014 Paul.  There is too much logic to ignore, including normalizing teams and adding to the phone search table. 
															// Most relationship procedures prevent duplicates, so it should not be an issue. 
															// 08/27/2014 Paul.  The previous technique updated the DELETED flag.  We need to manage it separately here. 
															if ( row.Table.Columns.Contains("DELETED") && Sql.ToBoolean(row["DELETED"]) )
															{
																IDbCommand cmdDelete = SqlProcs.Factory(con, "sp" + sTABLE_NAME + "_Delete");
																cmdDelete.Transaction = trn;
																Sql.SetParameter(cmdDelete, "@ID"              , gID             );
																Sql.SetParameter(cmdDelete, "@MODIFIED_USER_ID", Security.USER_ID);
																cmdDelete.ExecuteNonQuery();
															}
															else
															{
																IDbCommand cmdUpdate = SqlProcs.Factory(con, "sp" + sTABLE_NAME + "_Update");
																cmdUpdate.Transaction = trn;
																foreach(IDbDataParameter par in cmdUpdate.Parameters)
																{
																	// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
																	string sParameterName = Sql.ExtractDbName(cmdUpdate, par.ParameterName).ToUpper();
																	if ( sParameterName == "TEAM_ID" && bEnableTeamManagement )
																		par.Value = Sql.ToDBGuid(Security.TEAM_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																	else if ( sParameterName == "ASSIGNED_USER_ID" )
																		par.Value = Sql.ToDBGuid(Security.USER_ID);  // 02/26/2011 Paul.  Make sure to convert Guid.Empty to DBNull. 
																	else if ( sParameterName == "MODIFIED_USER_ID" )
																		par.Value = Sql.ToDBGuid(Security.USER_ID);
																	else
																		par.Value = DBNull.Value;
																}
																if ( bRecordExists )
																{
																	// 11/11/2009 Paul.  If the record already exists, then the current values are treated as default values. 
																	foreach ( DataColumn col in rowCurrent.Table.Columns )
																	{
																		IDbDataParameter par = Sql.FindParameter(cmdUpdate, col.ColumnName);
																		// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																		if ( par != null && String.Compare(col.ColumnName, "DATE_MODIFIED_UTC", true) != 0 )
																			par.Value = rowCurrent[col.ColumnName];
																	}
																}
																foreach ( DataColumn col in row.Table.Columns )
																{
																	// 01/18/2010 Paul.  Apply ACL Field Security. 
																	// 02/01/2010 Paul.  System tables may not have a valid Module name, so Field Security will not apply. 
																	bool bIsWriteable = true;
																	if ( SplendidInit.bEnableACLFieldSecurity && !Sql.IsEmptyString(sMODULE_NAME) )
																	{
																		Security.ACL_FIELD_ACCESS acl = Security.GetUserFieldSecurity(sMODULE_NAME, col.ColumnName, Guid.Empty);
																		bIsWriteable = acl.IsWriteable();
																	}
																	if ( bIsWriteable )
																	{
																		IDbDataParameter par = Sql.FindParameter(cmdUpdate, col.ColumnName);
																		// 11/26/2009 Paul.  The UTC modified date should be set to Now. 
																		if ( par != null )
																		{
																			switch ( par.DbType )
																			{
																				// 10/08/2011 Paul.  We must use Sql.ToDBDateTime, otherwise we get a an error whe DateTime.MinValue is used. 
																				// SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.
																				// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
																				// 08/28/2014 Paul.  The JsonDate will always be in UTC and now we set the UTC Kind flag. 
																				case DbType.Date                 :  par.Value = Sql.ToDBDateTime(T10n.ToServerTimeFromUniversalTime(FromJsonDate(Sql.ToString(row[col.ColumnName]))));  break;
																				case DbType.DateTime             :  par.Value = Sql.ToDBDateTime(T10n.ToServerTimeFromUniversalTime(FromJsonDate(Sql.ToString(row[col.ColumnName]))));  break;
																				case DbType.Int16                :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.Int32                :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.Int64                :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.UInt16               :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.UInt32               :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.UInt64               :  par.Value = Sql.ToDBInteger (row[col.ColumnName]);  break;
																				case DbType.Single               :  par.Value = Sql.ToDBFloat   (row[col.ColumnName]);  break;
																				case DbType.Double               :  par.Value = Sql.ToDBFloat   (row[col.ColumnName]);  break;
																				case DbType.Decimal              :  par.Value = Sql.ToDBDecimal (row[col.ColumnName]);  break;
																				case DbType.Currency             :  par.Value = Sql.ToDBDecimal (row[col.ColumnName]);  break;
																				case DbType.Boolean              :  par.Value = Sql.ToDBBoolean (row[col.ColumnName]);  break;
																				case DbType.Guid                 :  par.Value = Sql.ToDBGuid    (row[col.ColumnName]);  break;
																				case DbType.String               :  par.Value = Sql.ToDBString  (row[col.ColumnName]);  break;
																				case DbType.StringFixedLength    :  par.Value = Sql.ToDBString  (row[col.ColumnName]);  break;
																				case DbType.AnsiString           :  par.Value = Sql.ToDBString  (row[col.ColumnName]);  break;
																				case DbType.AnsiStringFixedLength:  par.Value = Sql.ToDBString  (row[col.ColumnName]);  break;
																			}
																		}
																	}
																}
																cmdUpdate.ExecuteScalar();
																if ( bHAS_CUSTOM )
																{
																	DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
																	SplendidDynamic.UpdateCustomFields(row, trn, gID, sTABLE_NAME, dtCustomFields);
																}
															}
															trn.Commit();
														}
														catch
														{
															trn.Rollback();
															throw;
														}
														try
														{
															if ( bHAS_CUSTOM )
															{
																sSQL = "select " + sVIEW_NAME  + ".*     " + ControlChars.CrLf
																     + "     , " + sTABLE_NAME + "_CSTM.*" + ControlChars.CrLf
																     + "  from " + sVIEW_NAME              + ControlChars.CrLf
																     + "  left outer join " + sTABLE_NAME + "_CSTM" + ControlChars.CrLf
																     + "               on " + sTABLE_NAME + "_CSTM.ID_C = " + sVIEW_NAME + ".ID" + ControlChars.CrLf
																     + " where 1 = 1"                      + ControlChars.CrLf;
															}
															else
															{
																sSQL = "select " + sVIEW_NAME + ".*" + ControlChars.CrLf
																     + "  from " + sVIEW_NAME        + ControlChars.CrLf
																     + " where 1 = 1"                + ControlChars.CrLf;
															}
															using ( IDbCommand cmd = con.CreateCommand() )
															{
																cmd.CommandText = sSQL;
																cmd.CommandTimeout = 0;
																Sql.AppendParameter(cmd, gID, "ID");
																using ( DbDataAdapter da = dbf.CreateDataAdapter() )
																{
																	((IDbDataAdapter)da).SelectCommand = cmd;
																	using ( DataTable dtItem = new DataTable() )
																	{
																		da.Fill(dtItem);
																		if ( dtItem.Rows.Count > 0 )
																		{
																			DataRow rowItem   = dtItem.Rows[0];
																			DataRow rowResult = dtResults.NewRow();
																			rowResult["SPLENDID_SYNC_STATUS" ] = "Updated";
																			rowResult["SPLENDID_SYNC_MESSAGE"] = DBNull.Value;
																			foreach ( DataColumn colItem in dtItem.Columns )
																			{
																				if ( rowResult.Table.Columns.Contains(colItem.ColumnName) )
																					rowResult[colItem.ColumnName] = rowItem[colItem.ColumnName];
																			}
																			dtResults.Rows.Add(rowResult);
																		}
																	}
																}
															}
														}
														catch(Exception ex)
														{
															SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
															DataRow rowError = dtResults.NewRow();
															dtResults.Rows.Add(rowError);
															rowError["ID"                   ] = gID;
															rowError["SPLENDID_SYNC_STATUS" ] = "Updated with Read Error";
															rowError["SPLENDID_SYNC_MESSAGE"] = L10n.Term("Offline.LBL_READ_ERROR");
														}
													}
												}
												else
												{
													DataRow rowError = dtResults.NewRow();
													dtResults.Rows.Add(rowError);
													rowError["ID"                   ] = gID;
													rowError["SPLENDID_SYNC_STATUS" ] = "Duplicate Exists";
													rowError["SPLENDID_SYNC_MESSAGE"] = L10n.Term(".ERR_DUPLICATE_EXCEPTION");
												}
											}
											else
											{
												DataRow rowError = dtResults.NewRow();
												dtResults.Rows.Add(rowError);
												rowError["ID"                   ] = gID;
												rowError["SPLENDID_SYNC_STATUS" ] = "Access Denied";
												rowError["SPLENDID_SYNC_MESSAGE"] = L10n.Term("ACL.LBL_NO_ACCESS");
											}
										}
										else
										{
											DataRow rowConflicted = dtResults.NewRow();
											dtResults.Rows.Add(rowConflicted);
											rowConflicted["ID"                   ] = gID;
											rowConflicted["SPLENDID_SYNC_STATUS" ] = "Conflicted";
											rowConflicted["SPLENDID_SYNC_MESSAGE"] = sConflictedMessage;  // L10n.Term("Offline.ERR_CONFLICTED");
										}
									}
								}
								else
								{
									throw(new Exception(L10n.Term("ACL.LBL_NO_ACCESS")));
								}
							}
						}
					}
				}
				else
				{
					throw(new Exception("Authentication Required."));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return dtResults;
		}
	}
}
