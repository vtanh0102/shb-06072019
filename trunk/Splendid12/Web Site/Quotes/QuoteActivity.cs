/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// QuoteActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class QuoteActivity: SplendidActivity
	{
		public QuoteActivity()
		{
			this.Name = "QuoteActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.IDProperty))); }
			set { base.SetValue(QuoteActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.NAMEProperty))); }
			set { base.SetValue(QuoteActivity.NAMEProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.OPPORTUNITY_IDProperty))); }
			set { base.SetValue(QuoteActivity.OPPORTUNITY_IDProperty, value); }
		}

		public static DependencyProperty QUOTE_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_TYPE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUOTE_TYPE
		{
			get { return ((string)(base.GetValue(QuoteActivity.QUOTE_TYPEProperty))); }
			set { base.SetValue(QuoteActivity.QUOTE_TYPEProperty, value); }
		}

		public static DependencyProperty PAYMENT_TERMSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PAYMENT_TERMS", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PAYMENT_TERMS
		{
			get { return ((string)(base.GetValue(QuoteActivity.PAYMENT_TERMSProperty))); }
			set { base.SetValue(QuoteActivity.PAYMENT_TERMSProperty, value); }
		}

		public static DependencyProperty ORDER_STAGEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORDER_STAGE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ORDER_STAGE
		{
			get { return ((string)(base.GetValue(QuoteActivity.ORDER_STAGEProperty))); }
			set { base.SetValue(QuoteActivity.ORDER_STAGEProperty, value); }
		}

		public static DependencyProperty QUOTE_STAGEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_STAGE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUOTE_STAGE
		{
			get { return ((string)(base.GetValue(QuoteActivity.QUOTE_STAGEProperty))); }
			set { base.SetValue(QuoteActivity.QUOTE_STAGEProperty, value); }
		}

		public static DependencyProperty PURCHASE_ORDER_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PURCHASE_ORDER_NUM", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PURCHASE_ORDER_NUM
		{
			get { return ((string)(base.GetValue(QuoteActivity.PURCHASE_ORDER_NUMProperty))); }
			set { base.SetValue(QuoteActivity.PURCHASE_ORDER_NUMProperty, value); }
		}

		public static DependencyProperty ORIGINAL_PO_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORIGINAL_PO_DATE", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ORIGINAL_PO_DATE
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.ORIGINAL_PO_DATEProperty))); }
			set { base.SetValue(QuoteActivity.ORIGINAL_PO_DATEProperty, value); }
		}

		public static DependencyProperty DATE_QUOTE_CLOSEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_QUOTE_CLOSED", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_QUOTE_CLOSED
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_QUOTE_CLOSEDProperty))); }
			set { base.SetValue(QuoteActivity.DATE_QUOTE_CLOSEDProperty, value); }
		}

		public static DependencyProperty DATE_QUOTE_EXPECTED_CLOSEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_QUOTE_EXPECTED_CLOSED", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_QUOTE_EXPECTED_CLOSED
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_QUOTE_EXPECTED_CLOSEDProperty))); }
			set { base.SetValue(QuoteActivity.DATE_QUOTE_EXPECTED_CLOSEDProperty, value); }
		}

		public static DependencyProperty DATE_ORDER_SHIPPEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ORDER_SHIPPED", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ORDER_SHIPPED
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_ORDER_SHIPPEDProperty))); }
			set { base.SetValue(QuoteActivity.DATE_ORDER_SHIPPEDProperty, value); }
		}

		public static DependencyProperty SHOW_LINE_NUMSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHOW_LINE_NUMS", typeof(bool), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool SHOW_LINE_NUMS
		{
			get { return ((bool)(base.GetValue(QuoteActivity.SHOW_LINE_NUMSProperty))); }
			set { base.SetValue(QuoteActivity.SHOW_LINE_NUMSProperty, value); }
		}

		public static DependencyProperty CALC_GRAND_TOTALProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CALC_GRAND_TOTAL", typeof(bool), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool CALC_GRAND_TOTAL
		{
			get { return ((bool)(base.GetValue(QuoteActivity.CALC_GRAND_TOTALProperty))); }
			set { base.SetValue(QuoteActivity.CALC_GRAND_TOTALProperty, value); }
		}

		public static DependencyProperty EXCHANGE_RATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_RATE", typeof(float), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float EXCHANGE_RATE
		{
			get { return ((float)(base.GetValue(QuoteActivity.EXCHANGE_RATEProperty))); }
			set { base.SetValue(QuoteActivity.EXCHANGE_RATEProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(QuoteActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty TAXRATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TAXRATE_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.TAXRATE_IDProperty))); }
			set { base.SetValue(QuoteActivity.TAXRATE_IDProperty, value); }
		}

		public static DependencyProperty SHIPPER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPER_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPER_IDProperty, value); }
		}

		public static DependencyProperty SUBTOTALProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBTOTAL", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal SUBTOTAL
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.SUBTOTALProperty))); }
			set { base.SetValue(QuoteActivity.SUBTOTALProperty, value); }
		}

		public static DependencyProperty DISCOUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.DISCOUNTProperty))); }
			set { base.SetValue(QuoteActivity.DISCOUNTProperty, value); }
		}

		public static DependencyProperty SHIPPINGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal SHIPPING
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.SHIPPINGProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPINGProperty, value); }
		}

		public static DependencyProperty TAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAX
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.TAXProperty))); }
			set { base.SetValue(QuoteActivity.TAXProperty, value); }
		}

		public static DependencyProperty TOTALProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TOTAL
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.TOTALProperty))); }
			set { base.SetValue(QuoteActivity.TOTALProperty, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty BILLING_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_CONTACT_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_STREET", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_STREETProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_CITY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_CITYProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_STATE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_STATEProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_POSTALCODE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_COUNTRY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_STREET", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_STREETProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_CITY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_CITYProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_STATE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_STATEProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_POSTALCODE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_COUNTRY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(QuoteActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(QuoteActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty QUOTE_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUOTE_NUM", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string QUOTE_NUM
		{
			get { return ((string)(base.GetValue(QuoteActivity.QUOTE_NUMProperty))); }
			set { base.SetValue(QuoteActivity.QUOTE_NUMProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.TEAM_IDProperty))); }
			set { base.SetValue(QuoteActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(QuoteActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(QuoteActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(QuoteActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(QuoteActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(QuoteActivity.CREATED_BYProperty))); }
			set { base.SetValue(QuoteActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(QuoteActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(QuoteActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(QuoteActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(QuoteActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DISCOUNT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_USDOLLAR", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.DISCOUNT_USDOLLARProperty))); }
			set { base.SetValue(QuoteActivity.DISCOUNT_USDOLLARProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(QuoteActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(QuoteActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty SHIPPING_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_USDOLLAR", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal SHIPPING_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.SHIPPING_USDOLLARProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_USDOLLARProperty, value); }
		}

		public static DependencyProperty SUBTOTAL_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBTOTAL_USDOLLAR", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal SUBTOTAL_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.SUBTOTAL_USDOLLARProperty))); }
			set { base.SetValue(QuoteActivity.SUBTOTAL_USDOLLARProperty, value); }
		}

		public static DependencyProperty TAX_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX_USDOLLAR", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAX_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.TAX_USDOLLARProperty))); }
			set { base.SetValue(QuoteActivity.TAX_USDOLLARProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TOTAL_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TOTAL_USDOLLAR", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TOTAL_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.TOTAL_USDOLLARProperty))); }
			set { base.SetValue(QuoteActivity.TOTAL_USDOLLARProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_ASSIGNED_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_ASSIGNED_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_ACCOUNT_ASSIGNED_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_IDProperty, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_EMAIL1", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ACCOUNT_EMAIL1
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_EMAIL1Property))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_EMAIL1Property, value); }
		}

		public static DependencyProperty BILLING_ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ACCOUNT_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ACCOUNT_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_HTML", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_ADDRESS_HTMLProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty BILLING_CONTACT_ASSIGNED_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_ASSIGNED_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_CONTACT_ASSIGNED_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_IDProperty, value); }
		}

		public static DependencyProperty BILLING_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty BILLING_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid BILLING_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty BILLING_CONTACT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_EMAIL1", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_CONTACT_EMAIL1
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_CONTACT_EMAIL1Property))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_EMAIL1Property, value); }
		}

		public static DependencyProperty BILLING_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_CONTACT_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.BILLING_CONTACT_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.BILLING_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_ISO4217Property = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ISO4217", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_ISO4217
		{
			get { return ((string)(base.GetValue(QuoteActivity.CURRENCY_ISO4217Property))); }
			set { base.SetValue(QuoteActivity.CURRENCY_ISO4217Property, value); }
		}

		public static DependencyProperty CURRENCY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.CURRENCY_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.CURRENCY_NAMEProperty, value); }
		}

		public static DependencyProperty CURRENCY_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_SYMBOL", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CURRENCY_SYMBOL
		{
			get { return ((string)(base.GetValue(QuoteActivity.CURRENCY_SYMBOLProperty))); }
			set { base.SetValue(QuoteActivity.CURRENCY_SYMBOLProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(QuoteActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(QuoteActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty LEAD_SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_SOURCE", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_SOURCE
		{
			get { return ((string)(base.GetValue(QuoteActivity.LEAD_SOURCEProperty))); }
			set { base.SetValue(QuoteActivity.LEAD_SOURCEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty NEXT_STEPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NEXT_STEP", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NEXT_STEP
		{
			get { return ((string)(base.GetValue(QuoteActivity.NEXT_STEPProperty))); }
			set { base.SetValue(QuoteActivity.NEXT_STEPProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.OPPORTUNITY_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.OPPORTUNITY_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid OPPORTUNITY_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.OPPORTUNITY_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.OPPORTUNITY_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty OPPORTUNITY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OPPORTUNITY_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OPPORTUNITY_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.OPPORTUNITY_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.OPPORTUNITY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(QuoteActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty SHIPPER_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPER_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPER_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPER_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPER_NAMEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_ASSIGNED_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_ASSIGNED_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_ACCOUNT_ASSIGNED_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_EMAIL1", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ACCOUNT_EMAIL1
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_EMAIL1Property))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_EMAIL1Property, value); }
		}

		public static DependencyProperty SHIPPING_ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ACCOUNT_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ACCOUNT_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_HTML", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_ADDRESS_HTMLProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_ASSIGNED_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_ASSIGNED_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_CONTACT_ASSIGNED_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SHIPPING_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_EMAIL1", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_CONTACT_EMAIL1
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_EMAIL1Property))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_EMAIL1Property, value); }
		}

		public static DependencyProperty SHIPPING_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_CONTACT_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.SHIPPING_CONTACT_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.SHIPPING_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty TAXRATE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_NAME", typeof(string), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAXRATE_NAME
		{
			get { return ((string)(base.GetValue(QuoteActivity.TAXRATE_NAMEProperty))); }
			set { base.SetValue(QuoteActivity.TAXRATE_NAMEProperty, value); }
		}

		public static DependencyProperty TAXRATE_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_VALUE", typeof(decimal), typeof(QuoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAXRATE_VALUE
		{
			get { return ((decimal)(base.GetValue(QuoteActivity.TAXRATE_VALUEProperty))); }
			set { base.SetValue(QuoteActivity.TAXRATE_VALUEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("QuoteActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("QuoteActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select QUOTES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwQUOTES_AUDIT        QUOTES          " + ControlChars.CrLf
							                + " inner join vwQUOTES_AUDIT        QUOTES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on QUOTES_AUDIT_OLD.ID = QUOTES.ID       " + ControlChars.CrLf
							                + "        and QUOTES_AUDIT_OLD.AUDIT_VERSION = (select max(vwQUOTES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                from vwQUOTES_AUDIT                   " + ControlChars.CrLf
							                + "                                               where vwQUOTES_AUDIT.ID            =  QUOTES.ID           " + ControlChars.CrLf
							                + "                                                 and vwQUOTES_AUDIT.AUDIT_VERSION <  QUOTES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                 and vwQUOTES_AUDIT.AUDIT_TOKEN   <> QUOTES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                             )" + ControlChars.CrLf
							                + " where QUOTES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *            " + ControlChars.CrLf
							                + "  from vwQUOTES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwQUOTES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *            " + ControlChars.CrLf
							                + "  from vwQUOTES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                                = Sql.ToGuid    (rdr["ID"                               ]);
								MODIFIED_USER_ID                  = Sql.ToGuid    (rdr["MODIFIED_USER_ID"                 ]);
								ASSIGNED_USER_ID                  = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"                 ]);
								NAME                              = Sql.ToString  (rdr["NAME"                             ]);
								if ( !bPast )
									OPPORTUNITY_ID                    = Sql.ToGuid    (rdr["OPPORTUNITY_ID"                   ]);
								QUOTE_TYPE                        = Sql.ToString  (rdr["QUOTE_TYPE"                       ]);
								PAYMENT_TERMS                     = Sql.ToString  (rdr["PAYMENT_TERMS"                    ]);
								ORDER_STAGE                       = Sql.ToString  (rdr["ORDER_STAGE"                      ]);
								QUOTE_STAGE                       = Sql.ToString  (rdr["QUOTE_STAGE"                      ]);
								PURCHASE_ORDER_NUM                = Sql.ToString  (rdr["PURCHASE_ORDER_NUM"               ]);
								ORIGINAL_PO_DATE                  = Sql.ToDateTime(rdr["ORIGINAL_PO_DATE"                 ]);
								DATE_QUOTE_CLOSED                 = Sql.ToDateTime(rdr["DATE_QUOTE_CLOSED"                ]);
								DATE_QUOTE_EXPECTED_CLOSED        = Sql.ToDateTime(rdr["DATE_QUOTE_EXPECTED_CLOSED"       ]);
								DATE_ORDER_SHIPPED                = Sql.ToDateTime(rdr["DATE_ORDER_SHIPPED"               ]);
								SHOW_LINE_NUMS                    = Sql.ToBoolean (rdr["SHOW_LINE_NUMS"                   ]);
								CALC_GRAND_TOTAL                  = Sql.ToBoolean (rdr["CALC_GRAND_TOTAL"                 ]);
								EXCHANGE_RATE                     = Sql.ToFloat   (rdr["EXCHANGE_RATE"                    ]);
								CURRENCY_ID                       = Sql.ToGuid    (rdr["CURRENCY_ID"                      ]);
								TAXRATE_ID                        = Sql.ToGuid    (rdr["TAXRATE_ID"                       ]);
								SHIPPER_ID                        = Sql.ToGuid    (rdr["SHIPPER_ID"                       ]);
								SUBTOTAL                          = Sql.ToDecimal (rdr["SUBTOTAL"                         ]);
								DISCOUNT                          = Sql.ToDecimal (rdr["DISCOUNT"                         ]);
								SHIPPING                          = Sql.ToDecimal (rdr["SHIPPING"                         ]);
								TAX                               = Sql.ToDecimal (rdr["TAX"                              ]);
								TOTAL                             = Sql.ToDecimal (rdr["TOTAL"                            ]);
								if ( !bPast )
									BILLING_ACCOUNT_ID                = Sql.ToGuid    (rdr["BILLING_ACCOUNT_ID"               ]);
								if ( !bPast )
									BILLING_CONTACT_ID                = Sql.ToGuid    (rdr["BILLING_CONTACT_ID"               ]);
								BILLING_ADDRESS_STREET            = Sql.ToString  (rdr["BILLING_ADDRESS_STREET"           ]);
								BILLING_ADDRESS_CITY              = Sql.ToString  (rdr["BILLING_ADDRESS_CITY"             ]);
								BILLING_ADDRESS_STATE             = Sql.ToString  (rdr["BILLING_ADDRESS_STATE"            ]);
								BILLING_ADDRESS_POSTALCODE        = Sql.ToString  (rdr["BILLING_ADDRESS_POSTALCODE"       ]);
								BILLING_ADDRESS_COUNTRY           = Sql.ToString  (rdr["BILLING_ADDRESS_COUNTRY"          ]);
								if ( !bPast )
									SHIPPING_ACCOUNT_ID               = Sql.ToGuid    (rdr["SHIPPING_ACCOUNT_ID"              ]);
								if ( !bPast )
									SHIPPING_CONTACT_ID               = Sql.ToGuid    (rdr["SHIPPING_CONTACT_ID"              ]);
								SHIPPING_ADDRESS_STREET           = Sql.ToString  (rdr["SHIPPING_ADDRESS_STREET"          ]);
								SHIPPING_ADDRESS_CITY             = Sql.ToString  (rdr["SHIPPING_ADDRESS_CITY"            ]);
								SHIPPING_ADDRESS_STATE            = Sql.ToString  (rdr["SHIPPING_ADDRESS_STATE"           ]);
								SHIPPING_ADDRESS_POSTALCODE       = Sql.ToString  (rdr["SHIPPING_ADDRESS_POSTALCODE"      ]);
								SHIPPING_ADDRESS_COUNTRY          = Sql.ToString  (rdr["SHIPPING_ADDRESS_COUNTRY"         ]);
								DESCRIPTION                       = Sql.ToString  (rdr["DESCRIPTION"                      ]);
								QUOTE_NUM                         = Sql.ToString  (rdr["QUOTE_NUM"                        ]);
								TEAM_ID                           = Sql.ToGuid    (rdr["TEAM_ID"                          ]);
								TEAM_SET_LIST                     = Sql.ToString  (rdr["TEAM_SET_LIST"                    ]);
								if ( !bPast )
									TAG_SET_NAME                      = Sql.ToString  (rdr["TAG_SET_NAME"                     ]);
								ASSIGNED_SET_LIST                 = Sql.ToString  (rdr["ASSIGNED_SET_LIST"                ]);
								ASSIGNED_SET_ID                   = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"                  ]);
								ASSIGNED_SET_NAME                 = Sql.ToString  (rdr["ASSIGNED_SET_NAME"                ]);
								ASSIGNED_TO                       = Sql.ToString  (rdr["ASSIGNED_TO"                      ]);
								CREATED_BY                        = Sql.ToString  (rdr["CREATED_BY"                       ]);
								CREATED_BY_ID                     = Sql.ToGuid    (rdr["CREATED_BY_ID"                    ]);
								DATE_ENTERED                      = Sql.ToDateTime(rdr["DATE_ENTERED"                     ]);
								DATE_MODIFIED                     = Sql.ToDateTime(rdr["DATE_MODIFIED"                    ]);
								DATE_MODIFIED_UTC                 = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"                ]);
								DISCOUNT_USDOLLAR                 = Sql.ToDecimal (rdr["DISCOUNT_USDOLLAR"                ]);
								MODIFIED_BY                       = Sql.ToString  (rdr["MODIFIED_BY"                      ]);
								SHIPPING_USDOLLAR                 = Sql.ToDecimal (rdr["SHIPPING_USDOLLAR"                ]);
								SUBTOTAL_USDOLLAR                 = Sql.ToDecimal (rdr["SUBTOTAL_USDOLLAR"                ]);
								TAX_USDOLLAR                      = Sql.ToDecimal (rdr["TAX_USDOLLAR"                     ]);
								TEAM_NAME                         = Sql.ToString  (rdr["TEAM_NAME"                        ]);
								TEAM_SET_ID                       = Sql.ToGuid    (rdr["TEAM_SET_ID"                      ]);
								TEAM_SET_NAME                     = Sql.ToString  (rdr["TEAM_SET_NAME"                    ]);
								TOTAL_USDOLLAR                    = Sql.ToDecimal (rdr["TOTAL_USDOLLAR"                   ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME                  = Sql.ToString  (rdr["ASSIGNED_TO_NAME"                 ]);
									BILLING_ACCOUNT_ASSIGNED_ID       = Sql.ToGuid    (rdr["BILLING_ACCOUNT_ASSIGNED_ID"      ]);
									BILLING_ACCOUNT_ASSIGNED_SET_ID   = Sql.ToGuid    (rdr["BILLING_ACCOUNT_ASSIGNED_SET_ID"  ]);
									BILLING_ACCOUNT_ASSIGNED_USER_ID  = Sql.ToGuid    (rdr["BILLING_ACCOUNT_ASSIGNED_USER_ID" ]);
									BILLING_ACCOUNT_EMAIL1            = Sql.ToString  (rdr["BILLING_ACCOUNT_EMAIL1"           ]);
									BILLING_ACCOUNT_NAME              = Sql.ToString  (rdr["BILLING_ACCOUNT_NAME"             ]);
									BILLING_ADDRESS_HTML              = Sql.ToString  (rdr["BILLING_ADDRESS_HTML"             ]);
									BILLING_CONTACT_ASSIGNED_ID       = Sql.ToGuid    (rdr["BILLING_CONTACT_ASSIGNED_ID"      ]);
									BILLING_CONTACT_ASSIGNED_SET_ID   = Sql.ToGuid    (rdr["BILLING_CONTACT_ASSIGNED_SET_ID"  ]);
									BILLING_CONTACT_ASSIGNED_USER_ID  = Sql.ToGuid    (rdr["BILLING_CONTACT_ASSIGNED_USER_ID" ]);
									BILLING_CONTACT_EMAIL1            = Sql.ToString  (rdr["BILLING_CONTACT_EMAIL1"           ]);
									BILLING_CONTACT_NAME              = Sql.ToString  (rdr["BILLING_CONTACT_NAME"             ]);
									CREATED_BY_NAME                   = Sql.ToString  (rdr["CREATED_BY_NAME"                  ]);
									CURRENCY_ISO4217                  = Sql.ToString  (rdr["CURRENCY_ISO4217"                 ]);
									CURRENCY_NAME                     = Sql.ToString  (rdr["CURRENCY_NAME"                    ]);
									CURRENCY_SYMBOL                   = Sql.ToString  (rdr["CURRENCY_SYMBOL"                  ]);
									LAST_ACTIVITY_DATE                = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"               ]);
									LEAD_SOURCE                       = Sql.ToString  (rdr["LEAD_SOURCE"                      ]);
									MODIFIED_BY_NAME                  = Sql.ToString  (rdr["MODIFIED_BY_NAME"                 ]);
									NEXT_STEP                         = Sql.ToString  (rdr["NEXT_STEP"                        ]);
									OPPORTUNITY_ASSIGNED_SET_ID       = Sql.ToGuid    (rdr["OPPORTUNITY_ASSIGNED_SET_ID"      ]);
									OPPORTUNITY_ASSIGNED_USER_ID      = Sql.ToGuid    (rdr["OPPORTUNITY_ASSIGNED_USER_ID"     ]);
									OPPORTUNITY_NAME                  = Sql.ToString  (rdr["OPPORTUNITY_NAME"                 ]);
									PENDING_PROCESS_ID                = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"               ]);
									SHIPPER_NAME                      = Sql.ToString  (rdr["SHIPPER_NAME"                     ]);
									SHIPPING_ACCOUNT_ASSIGNED_ID      = Sql.ToGuid    (rdr["SHIPPING_ACCOUNT_ASSIGNED_ID"     ]);
									SHIPPING_ACCOUNT_ASSIGNED_SET_ID  = Sql.ToGuid    (rdr["SHIPPING_ACCOUNT_ASSIGNED_SET_ID" ]);
									SHIPPING_ACCOUNT_ASSIGNED_USER_ID = Sql.ToGuid    (rdr["SHIPPING_ACCOUNT_ASSIGNED_USER_ID"]);
									SHIPPING_ACCOUNT_EMAIL1           = Sql.ToString  (rdr["SHIPPING_ACCOUNT_EMAIL1"          ]);
									SHIPPING_ACCOUNT_NAME             = Sql.ToString  (rdr["SHIPPING_ACCOUNT_NAME"            ]);
									SHIPPING_ADDRESS_HTML             = Sql.ToString  (rdr["SHIPPING_ADDRESS_HTML"            ]);
									SHIPPING_CONTACT_ASSIGNED_ID      = Sql.ToGuid    (rdr["SHIPPING_CONTACT_ASSIGNED_ID"     ]);
									SHIPPING_CONTACT_ASSIGNED_SET_ID  = Sql.ToGuid    (rdr["SHIPPING_CONTACT_ASSIGNED_SET_ID" ]);
									SHIPPING_CONTACT_ASSIGNED_USER_ID = Sql.ToGuid    (rdr["SHIPPING_CONTACT_ASSIGNED_USER_ID"]);
									SHIPPING_CONTACT_EMAIL1           = Sql.ToString  (rdr["SHIPPING_CONTACT_EMAIL1"          ]);
									SHIPPING_CONTACT_NAME             = Sql.ToString  (rdr["SHIPPING_CONTACT_NAME"            ]);
									TAXRATE_NAME                      = Sql.ToString  (rdr["TAXRATE_NAME"                     ]);
									TAXRATE_VALUE                     = Sql.ToDecimal (rdr["TAXRATE_VALUE"                    ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("QuoteActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("QUOTES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spQUOTES_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, OPPORTUNITY_ID
								, QUOTE_TYPE
								, PAYMENT_TERMS
								, ORDER_STAGE
								, QUOTE_STAGE
								, PURCHASE_ORDER_NUM
								, ORIGINAL_PO_DATE
								, DATE_QUOTE_CLOSED
								, DATE_QUOTE_EXPECTED_CLOSED
								, DATE_ORDER_SHIPPED
								, SHOW_LINE_NUMS
								, CALC_GRAND_TOTAL
								, EXCHANGE_RATE
								, CURRENCY_ID
								, TAXRATE_ID
								, SHIPPER_ID
								, SUBTOTAL
								, DISCOUNT
								, SHIPPING
								, TAX
								, TOTAL
								, BILLING_ACCOUNT_ID
								, BILLING_CONTACT_ID
								, BILLING_ADDRESS_STREET
								, BILLING_ADDRESS_CITY
								, BILLING_ADDRESS_STATE
								, BILLING_ADDRESS_POSTALCODE
								, BILLING_ADDRESS_COUNTRY
								, SHIPPING_ACCOUNT_ID
								, SHIPPING_CONTACT_ID
								, SHIPPING_ADDRESS_STREET
								, SHIPPING_ADDRESS_CITY
								, SHIPPING_ADDRESS_STATE
								, SHIPPING_ADDRESS_POSTALCODE
								, SHIPPING_ADDRESS_COUNTRY
								, DESCRIPTION
								, QUOTE_NUM
								, TEAM_ID
								, TEAM_SET_LIST
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("QuoteActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

