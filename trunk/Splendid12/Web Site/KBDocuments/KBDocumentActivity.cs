/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// KBDocumentActivity generated from database[SplendidCRM6] on 12/2/2017 12:15:55 AM
	/// </summary>
	public class KBDocumentActivity: SplendidActivity
	{
		public KBDocumentActivity()
		{
			this.Name = "KBDocumentActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.IDProperty))); }
			set { base.SetValue(KBDocumentActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.NAMEProperty, value); }
		}

		public static DependencyProperty KBDOC_APPROVER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("KBDOC_APPROVER_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid KBDOC_APPROVER_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.KBDOC_APPROVER_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.KBDOC_APPROVER_IDProperty, value); }
		}

		public static DependencyProperty IS_EXTERNAL_ARTICLEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_EXTERNAL_ARTICLE", typeof(bool), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_EXTERNAL_ARTICLE
		{
			get { return ((bool)(base.GetValue(KBDocumentActivity.IS_EXTERNAL_ARTICLEProperty))); }
			set { base.SetValue(KBDocumentActivity.IS_EXTERNAL_ARTICLEProperty, value); }
		}

		public static DependencyProperty ACTIVE_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTIVE_DATE", typeof(DateTime), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime ACTIVE_DATE
		{
			get { return ((DateTime)(base.GetValue(KBDocumentActivity.ACTIVE_DATEProperty))); }
			set { base.SetValue(KBDocumentActivity.ACTIVE_DATEProperty, value); }
		}

		public static DependencyProperty EXP_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXP_DATE", typeof(DateTime), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime EXP_DATE
		{
			get { return ((DateTime)(base.GetValue(KBDocumentActivity.EXP_DATEProperty))); }
			set { base.SetValue(KBDocumentActivity.EXP_DATEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.STATUSProperty))); }
			set { base.SetValue(KBDocumentActivity.STATUSProperty, value); }
		}

		public static DependencyProperty REVISIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REVISION", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REVISION
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.REVISIONProperty))); }
			set { base.SetValue(KBDocumentActivity.REVISIONProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(KBDocumentActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.TEAM_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(KBDocumentActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty KBTAG_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("KBTAG_SET_LIST", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string KBTAG_SET_LIST
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.KBTAG_SET_LISTProperty))); }
			set { base.SetValue(KBDocumentActivity.KBTAG_SET_LISTProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.CREATED_BYProperty))); }
			set { base.SetValue(KBDocumentActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(KBDocumentActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(KBDocumentActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(KBDocumentActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(KBDocumentActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(KBDocumentActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(KBDocumentActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(KBDocumentActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty HAS_ATTACHMENTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("HAS_ATTACHMENTS", typeof(Int32), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 HAS_ATTACHMENTS
		{
			get { return ((Int32)(base.GetValue(KBDocumentActivity.HAS_ATTACHMENTSProperty))); }
			set { base.SetValue(KBDocumentActivity.HAS_ATTACHMENTSProperty, value); }
		}

		public static DependencyProperty KBDOC_APPROVER_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("KBDOC_APPROVER_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string KBDOC_APPROVER_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.KBDOC_APPROVER_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.KBDOC_APPROVER_NAMEProperty, value); }
		}

		public static DependencyProperty KBTAG_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("KBTAG_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string KBTAG_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.KBTAG_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.KBTAG_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(KBDocumentActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(KBDocumentActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(KBDocumentActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(KBDocumentActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(KBDocumentActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("KBDocumentActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("KBDocumentActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select KBDOCUMENTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwKBDOCUMENTS_AUDIT        KBDOCUMENTS          " + ControlChars.CrLf
							                + " inner join vwKBDOCUMENTS_AUDIT        KBDOCUMENTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on KBDOCUMENTS_AUDIT_OLD.ID = KBDOCUMENTS.ID       " + ControlChars.CrLf
							                + "        and KBDOCUMENTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwKBDOCUMENTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                     from vwKBDOCUMENTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                    where vwKBDOCUMENTS_AUDIT.ID            =  KBDOCUMENTS.ID           " + ControlChars.CrLf
							                + "                                                      and vwKBDOCUMENTS_AUDIT.AUDIT_VERSION <  KBDOCUMENTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                      and vwKBDOCUMENTS_AUDIT.AUDIT_TOKEN   <> KBDOCUMENTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                  )" + ControlChars.CrLf
							                + " where KBDOCUMENTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                 " + ControlChars.CrLf
							                + "  from vwKBDOCUMENTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwKBDOCUMENTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                 " + ControlChars.CrLf
							                + "  from vwKBDOCUMENTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								KBDOC_APPROVER_ID              = Sql.ToGuid    (rdr["KBDOC_APPROVER_ID"             ]);
								IS_EXTERNAL_ARTICLE            = Sql.ToBoolean (rdr["IS_EXTERNAL_ARTICLE"           ]);
								ACTIVE_DATE                    = Sql.ToDateTime(rdr["ACTIVE_DATE"                   ]);
								EXP_DATE                       = Sql.ToDateTime(rdr["EXP_DATE"                      ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								REVISION                       = Sql.ToString  (rdr["REVISION"                      ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								KBTAG_SET_LIST                 = Sql.ToString  (rdr["KBTAG_SET_LIST"                ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								if ( !bPast )
									ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									HAS_ATTACHMENTS                = Sql.ToInteger (rdr["HAS_ATTACHMENTS"               ]);
									KBDOC_APPROVER_NAME            = Sql.ToString  (rdr["KBDOC_APPROVER_NAME"           ]);
									KBTAG_NAME                     = Sql.ToString  (rdr["KBTAG_NAME"                    ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("KBDocumentActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("KBDOCUMENTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spKBDOCUMENTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, KBDOC_APPROVER_ID
								, IS_EXTERNAL_ARTICLE
								, ACTIVE_DATE
								, EXP_DATE
								, STATUS
								, REVISION
								, DESCRIPTION
								, TEAM_ID
								, TEAM_SET_LIST
								, KBTAG_SET_LIST
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("KBDocumentActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

