/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// CaseActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class CaseActivity: SplendidActivity
	{
		public CaseActivity()
		{
			this.Name = "CaseActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.IDProperty))); }
			set { base.SetValue(CaseActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(CaseActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.NAMEProperty))); }
			set { base.SetValue(CaseActivity.NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(CaseActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(CaseActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(CaseActivity.STATUSProperty))); }
			set { base.SetValue(CaseActivity.STATUSProperty, value); }
		}

		public static DependencyProperty PRIORITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIORITY", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIORITY
		{
			get { return ((string)(base.GetValue(CaseActivity.PRIORITYProperty))); }
			set { base.SetValue(CaseActivity.PRIORITYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(CaseActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(CaseActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty RESOLUTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RESOLUTION", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RESOLUTION
		{
			get { return ((string)(base.GetValue(CaseActivity.RESOLUTIONProperty))); }
			set { base.SetValue(CaseActivity.RESOLUTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(CaseActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(CaseActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.PARENT_IDProperty))); }
			set { base.SetValue(CaseActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty CASE_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CASE_NUMBER", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CASE_NUMBER
		{
			get { return ((string)(base.GetValue(CaseActivity.CASE_NUMBERProperty))); }
			set { base.SetValue(CaseActivity.CASE_NUMBERProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.TEAM_IDProperty))); }
			set { base.SetValue(CaseActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(CaseActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(CaseActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(CaseActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(CaseActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(CaseActivity.TYPEProperty))); }
			set { base.SetValue(CaseActivity.TYPEProperty, value); }
		}

		public static DependencyProperty WORK_LOGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORK_LOG", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string WORK_LOG
		{
			get { return ((string)(base.GetValue(CaseActivity.WORK_LOGProperty))); }
			set { base.SetValue(CaseActivity.WORK_LOGProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.B2C_CONTACT_IDProperty))); }
			set { base.SetValue(CaseActivity.B2C_CONTACT_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(CaseActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(CaseActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(CaseActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(CaseActivity.CREATED_BYProperty))); }
			set { base.SetValue(CaseActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(CaseActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(CaseActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(CaseActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(CaseActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(CaseActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(CaseActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(CaseActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(CaseActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(CaseActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(CaseActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(CaseActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(CaseActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CaseActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CaseActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_EMAIL1", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_EMAIL1
		{
			get { return ((string)(base.GetValue(CaseActivity.ACCOUNT_EMAIL1Property))); }
			set { base.SetValue(CaseActivity.ACCOUNT_EMAIL1Property, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(CaseActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CaseActivity.B2C_CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid B2C_CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CaseActivity.B2C_CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty B2C_CONTACT_EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_EMAIL1", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_EMAIL1
		{
			get { return ((string)(base.GetValue(CaseActivity.B2C_CONTACT_EMAIL1Property))); }
			set { base.SetValue(CaseActivity.B2C_CONTACT_EMAIL1Property, value); }
		}

		public static DependencyProperty B2C_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("B2C_CONTACT_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string B2C_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.B2C_CONTACT_NAMEProperty))); }
			set { base.SetValue(CaseActivity.B2C_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(CaseActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(CaseActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(CaseActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(CaseActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(CaseActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(CaseActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(CaseActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(CaseActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CaseActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CaseActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CASES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCASES_AUDIT        CASES          " + ControlChars.CrLf
							                + " inner join vwCASES_AUDIT        CASES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CASES_AUDIT_OLD.ID = CASES.ID       " + ControlChars.CrLf
							                + "        and CASES_AUDIT_OLD.AUDIT_VERSION = (select max(vwCASES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwCASES_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwCASES_AUDIT.ID            =  CASES.ID           " + ControlChars.CrLf
							                + "                                                and vwCASES_AUDIT.AUDIT_VERSION <  CASES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwCASES_AUDIT.AUDIT_TOKEN   <> CASES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where CASES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwCASES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCASES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwCASES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
								ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								PRIORITY                       = Sql.ToString  (rdr["PRIORITY"                      ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								RESOLUTION                     = Sql.ToString  (rdr["RESOLUTION"                    ]);
								CASE_NUMBER                    = Sql.ToString  (rdr["CASE_NUMBER"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								WORK_LOG                       = Sql.ToString  (rdr["WORK_LOG"                      ]);
								B2C_CONTACT_ID                 = Sql.ToGuid    (rdr["B2C_CONTACT_ID"                ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"       ]);
									ACCOUNT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"      ]);
									ACCOUNT_EMAIL1                 = Sql.ToString  (rdr["ACCOUNT_EMAIL1"                ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									B2C_CONTACT_ASSIGNED_SET_ID    = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_SET_ID"   ]);
									B2C_CONTACT_ASSIGNED_USER_ID   = Sql.ToGuid    (rdr["B2C_CONTACT_ASSIGNED_USER_ID"  ]);
									B2C_CONTACT_EMAIL1             = Sql.ToString  (rdr["B2C_CONTACT_EMAIL1"            ]);
									B2C_CONTACT_NAME               = Sql.ToString  (rdr["B2C_CONTACT_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CaseActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CASES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCASES_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, ACCOUNT_NAME
								, ACCOUNT_ID
								, STATUS
								, PRIORITY
								, DESCRIPTION
								, RESOLUTION
								, PARENT_TYPE
								, PARENT_ID
								, CASE_NUMBER
								, TEAM_ID
								, TEAM_SET_LIST
								, EXCHANGE_FOLDER
								, TYPE
								, WORK_LOG
								, B2C_CONTACT_ID
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CaseActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

