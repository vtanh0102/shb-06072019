/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace SplendidCRM._controls
{
	/// <summary>
	///		Summary description for ExportHeader.
	/// </summary>
	public class ExportHeader : SplendidControl
	{
		public CommandEventHandler Command ;
		protected string       sModule         = String.Empty;
		protected string       sTitle          = String.Empty;
		protected DropDownList lstEXPORT_RANGE ;
		protected DropDownList lstEXPORT_FORMAT;
		protected Button       btnExport       ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			if ( Command != null )
				Command(this, e) ;
		}

		// 02/08/2008 Paul.  We need to determine if the export button has been clicked inside Page_Load. 
		public string ExportUniqueID
		{
			get
			{
				return btnExport.UniqueID;
			}
		}

		public string Module
		{
			get
			{
				return sModule;
			}
			set
			{
				sModule = value;
			}
		}

		public string Title
		{
			get
			{
				return sTitle;
			}
			set
			{
				sTitle = value;
			}
		}

		public string ExportRange
		{
			get
			{
				return lstEXPORT_RANGE.SelectedValue;
			}
		}

		public string ExportFormat
		{
			get
			{
				return lstEXPORT_FORMAT.SelectedValue;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( !IsPostBack )
			{
				lstEXPORT_RANGE.Items.Add(new ListItem(L10n.Term(".LBL_LISTVIEW_OPTION_ENTIRE"  ), "All"     ));
				lstEXPORT_RANGE.Items.Add(new ListItem(L10n.Term(".LBL_LISTVIEW_OPTION_CURRENT" ), "Page"    ));
				lstEXPORT_RANGE.Items.Add(new ListItem(L10n.Term(".LBL_LISTVIEW_OPTION_SELECTED"), "Selected"));
				
				lstEXPORT_FORMAT.Items.Add(new ListItem(L10n.Term("Import.LBL_XML_SPREADSHEET"  ), "Excel"   ));
				lstEXPORT_FORMAT.Items.Add(new ListItem(L10n.Term("Import.LBL_XML"              ), "xml"     ));
				lstEXPORT_FORMAT.Items.Add(new ListItem(L10n.Term("Import.LBL_CUSTOM_CSV"       ), "csv"     ));
				lstEXPORT_FORMAT.Items.Add(new ListItem(L10n.Term("Import.LBL_CUSTOM_TAB"       ), "tab"     ));
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

