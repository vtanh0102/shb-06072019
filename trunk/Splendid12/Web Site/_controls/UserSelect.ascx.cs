/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM._controls
{
	/// <summary>
	///		Summary description for UserSelect.
	/// </summary>
	public class UserSelect : SplendidControl
	{
		protected DataTable       dtLineItems           ;
		protected GridView        grdMain               ;
		protected String          sASSIGNED_SET_LIST    ;
		protected Panel           pnlAddReplace         ;
		protected RadioButton     radUserSetReplace     ;
		protected RadioButton     radUserSetAdd         ;
		protected bool            bShowAddReplace       = false;
		protected bool            bSupportsPopups       = true;
		protected RequiredFieldValidatorForUserSelect valUserSelect;
		protected bool            bEnabled              = true;
		protected bool            bAjaxAutoComplete     = false;
		protected short           nTagIndex             = 12;

		// 12/10/2017 Paul.  Provide a way to set the tab index. 
		public short TabIndex
		{
			get
			{
				return nTagIndex;
			}
			set
			{
				nTagIndex = value;
			}
		}

		public DataTable LineItems
		{
			get
			{
				// 08/31/2009 Paul.  When called from within the SearchView control, dtLineItems is not initialized. 
				if ( dtLineItems == null )
					dtLineItems = ViewState["LineItems"] as DataTable;
				return dtLineItems;
			}
			set
			{
				dtLineItems = value;
				
				// 08/31/2009 Paul.  Add a blank row after loading. 
				DataRow rowNew = dtLineItems.NewRow();
				dtLineItems.Rows.Add(rowNew);
				
				ViewState["LineItems"] = dtLineItems;
				grdMain.DataSource = dtLineItems;
				// 02/03/2007 Paul.  Start with last line enabled for editing. 
				grdMain.EditIndex = dtLineItems.Rows.Count - 1;
				grdMain.DataBind();
			}
		}

		public bool ShowAddReplace
		{
			get { return bShowAddReplace; }
			set { bShowAddReplace = value; }
		}

		public bool Enabled
		{
			get { return bEnabled; }
			set { bEnabled = value; }
		}

		// 08/30/2009 Paul.  The first user will be the primary if none selected. 
		public Guid USER_ID
		{
			get
			{
				Guid gPRIMARY_USER_ID = Guid.Empty;
				// 08/31/2009 Paul.  When called from within the SearchView control, dtLineItems is not initialized. 
				if ( dtLineItems == null )
					dtLineItems = ViewState["LineItems"] as DataTable;
				if ( dtLineItems != null )
				{
					DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					foreach ( DataRow row in aCurrentRows )
					{
						// 08/23/2009 Paul.  Although the USER_ID should never be NULL or Empty, check for this condition anyway. 
						Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
						if ( gUSER_ID != Guid.Empty )
						{
							// 08/23/2009 Paul.  The first User in the list is the primary, unless one is specifically marked as primary. 
							if ( gPRIMARY_USER_ID == Guid.Empty )
								gPRIMARY_USER_ID = gUSER_ID;
							if ( Sql.ToBoolean(row["PRIMARY_USER"]) )
							{
								gPRIMARY_USER_ID = gUSER_ID;
								break;
							}
						}
					}
				}
				return gPRIMARY_USER_ID;
			}
		}

		// 08/30/2009 Paul.  Only a manually selected primary will be the primary. 
		public Guid PRIMARY_USER_ID
		{
			get
			{
				Guid gPRIMARY_USER_ID = Guid.Empty;
				// 08/31/2009 Paul.  When called from within the SearchView control, dtLineItems is not initialized. 
				if ( dtLineItems == null )
					dtLineItems = ViewState["LineItems"] as DataTable;
				if ( dtLineItems != null )
				{
					DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					foreach ( DataRow row in aCurrentRows )
					{
						// 08/23/2009 Paul.  Although the USER_ID should never be NULL or Empty, check for this condition anyway. 
						Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
						if ( gUSER_ID != Guid.Empty )
						{
							if ( Sql.ToBoolean(row["PRIMARY_USER"]) )
							{
								gPRIMARY_USER_ID = gUSER_ID;
								break;
							}
						}
					}
				}
				return gPRIMARY_USER_ID;
			}
		}

		public string ASSIGNED_SET_LIST
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				// 08/31/2009 Paul.  When called from within the SearchView control, dtLineItems is not initialized. 
				if ( dtLineItems == null )
					dtLineItems = ViewState["LineItems"] as DataTable;
				if ( dtLineItems != null )
				{
					DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					foreach ( DataRow row in aCurrentRows )
					{
						// 08/23/2009 Paul.  Although the USER_ID should never be NULL or Empty, check for this condition anyway. 
						Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
						if ( gUSER_ID != Guid.Empty )
						{
							if ( sb.Length > 0 )
								sb.Append(",");
							sb.Append(gUSER_ID.ToString());
						}
					}
				}
				return sb.ToString();
			}
			// 04/14/2013 Paul.  Allow sets to be modified. 
			set
			{
				if ( dtLineItems == null )
				{
					dtLineItems = ViewState["LineItems"] as DataTable;
					if ( dtLineItems == null )
						InitTable();
				}
				string[] arrUSERS = value.Split(',');
				List<Guid> lstNew = new List<Guid>();
				foreach ( string sUSER_ID in arrUSERS )
				{
					try
					{
						if ( !Sql.IsEmptyString(sUSER_ID.Trim()) )
						{
							Guid gUSER_ID = Sql.ToGuid(sUSER_ID.Trim());
							if ( !lstNew.Contains(gUSER_ID) )
								lstNew.Add(gUSER_ID);
						}
					}
					catch
					{
					}
				}
				List<Guid> lstExisting = new List<Guid>();
				DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
				foreach ( DataRow row in aCurrentRows )
				{
					Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
					// 04/14/2013 Paul.  Delete existing records that do not exist in the new list. 
					if ( !lstNew.Contains(gUSER_ID) )
						row.Delete();
					else if ( !lstExisting.Contains(gUSER_ID) )
						lstExisting.Add(gUSER_ID);
				}
				foreach ( Guid gUSER_ID in lstNew )
				{
					// 04/14/2013 Paul.  Add new records not found in existing list. 
					if ( !lstExisting.Contains(gUSER_ID) )
					{
						string sUSER_NAME = Crm.Modules.ItemName(HttpContext.Current.Application, "Users", gUSER_ID);
						if ( !Sql.IsEmptyString(sUSER_NAME) )
						{
							DataRow rowNew = dtLineItems.NewRow();
							dtLineItems.Rows.Add(rowNew);
							rowNew["USER_ID"     ] = gUSER_ID;
							rowNew["USER_NAME"   ] = sUSER_NAME;
							rowNew["PRIMARY_USER"] = false;
						}
					}
				}
				// 04/14/2013 Paul.  The blank row at the bottom is always deleted, so always add it back. 
				DataRow rowBlank = dtLineItems.NewRow();
				dtLineItems.Rows.Add(rowBlank);
				ViewState["LineItems"] = dtLineItems;
			}
		}

		// 04/07/2014 Paul.  When adding or removing a user to a call or meeting, we also need to add the private user to the dynamic users. 
		public void AddUser(Guid gNEW_USER_ID)
		{
			if ( dtLineItems == null )
			{
				dtLineItems = ViewState["LineItems"] as DataTable;
				if ( dtLineItems == null )
					InitTable();
			}
			List<Guid> lstExisting = new List<Guid>();
			DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
			foreach ( DataRow row in aCurrentRows )
			{
				Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
				if ( Sql.IsEmptyGuid(gUSER_ID) )
					row.Delete();
				else if ( !lstExisting.Contains(gUSER_ID) )
					lstExisting.Add(gUSER_ID);
			}
			// 04/08/2014 Paul.  Add new records not found in existing list. 
			if ( !lstExisting.Contains(gNEW_USER_ID) )
			{
				string sUSER_NAME = Crm.Modules.ItemName(HttpContext.Current.Application, "Users", gNEW_USER_ID);
				if ( !Sql.IsEmptyString(sUSER_NAME) )
				{
					DataRow rowNew = dtLineItems.NewRow();
					dtLineItems.Rows.Add(rowNew);
					rowNew["USER_ID"     ] = gNEW_USER_ID;
					rowNew["USER_NAME"   ] = sUSER_NAME;
					rowNew["PRIMARY_USER"] = false;
				}
			}
			// 04/08/2014 Paul.  The blank row at the bottom is always deleted, so always add it back. 
			DataRow rowBlank = dtLineItems.NewRow();
			dtLineItems.Rows.Add(rowBlank);
			ViewState["LineItems"] = dtLineItems;
			
			aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
			grdMain.DataSource = dtLineItems;
			grdMain.EditIndex = aCurrentRows.Length - 1;
			grdMain.DataBind();
		}

		// 04/07/2014 Paul.  When adding or removing a user to a call or meeting, we also need to add the private user to the dynamic users. 
		public void RemoveUser(Guid gREMOVE_USER_ID)
		{
			if ( dtLineItems == null )
			{
				dtLineItems = ViewState["LineItems"] as DataTable;
				if ( dtLineItems == null )
					InitTable();
			}
			List<Guid> lstExisting = new List<Guid>();
			DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
			foreach ( DataRow row in aCurrentRows )
			{
				Guid gUSER_ID = Sql.ToGuid(row["USER_ID"]);
				if ( Sql.IsEmptyGuid(gUSER_ID) || gUSER_ID == gREMOVE_USER_ID )
					row.Delete();
			}
			// 04/08/2014 Paul.  The blank row at the bottom is always deleted, so always add it back. 
			DataRow rowBlank = dtLineItems.NewRow();
			dtLineItems.Rows.Add(rowBlank);
			ViewState["LineItems"] = dtLineItems;
			
			aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
			grdMain.DataSource = dtLineItems;
			grdMain.EditIndex = aCurrentRows.Length - 1;
			grdMain.DataBind();
		}

		public bool ADD_USER_SET
		{
			get
			{
				return pnlAddReplace.Visible && radUserSetAdd.Checked;
			}
		}

		public void InitTable()
		{
			dtLineItems = new DataTable();
			DataColumn colUSER_ID      = new DataColumn("USER_ID"     , Type.GetType("System.Guid"   ));
			DataColumn colUSER_NAME    = new DataColumn("USER_NAME"   , Type.GetType("System.String" ));
			DataColumn colPRIMARY_USER = new DataColumn("PRIMARY_USER", Type.GetType("System.Boolean"));
			dtLineItems.Columns.Add(colUSER_ID     );
			dtLineItems.Columns.Add(colUSER_NAME   );
			dtLineItems.Columns.Add(colPRIMARY_USER);
		}

		public void Clear()
		{
			InitTable();

			DataRow rowNew = dtLineItems.NewRow();
			dtLineItems.Rows.Add(rowNew);

			ViewState["LineItems"] = dtLineItems;
			grdMain.DataSource = dtLineItems;
			// 02/03/2007 Paul.  Start with last line enabled for editing. 
			grdMain.EditIndex = dtLineItems.Rows.Count - 1;
			grdMain.DataBind();
		}

		// 11/11/2010 Paul.  Provide a way to disable validation in a rule. 
		public void Validate()
		{
			Validate(true);
		}

		public void Validate(bool bEnabled)
		{
			valUserSelect.Enabled = bEnabled;
			valUserSelect.Validate();
		}

		#region Line Item Editing
		protected void grdMain_RowCreated(object sender, GridViewRowEventArgs e)
		{
			if ( (e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit )
			{
				// 12/07/2009 Paul.  The Opera Mini browser does not support popups. Use a DropdownList instead. 
				// 07/28/2010 Paul.  Save AjaxAutoComplete and SupportsPopups for use in UserSelect and KBSelect. 
				// We are having issues with the data binding event occurring before the page load. 
				if ( !(bSupportsPopups || Sql.ToBoolean(Page.Items["SupportsPopups"])) )
				{
					DropDownList lstUSER_ID = e.Row.FindControl("lstUSER_ID") as DropDownList;
					if ( lstUSER_ID != null )
					{
						lstUSER_ID.DataSource = SplendidCache.ActiveUsers();
						lstUSER_ID.DataBind();
						lstUSER_ID.Visible = !bSupportsPopups;
					}
					// 12/10/2009 Paul.  The existing user controls were not being hidden on a Blackberry by the span as expected. 
					// So we need to hide the popup fields manually. 
					TextBox USER_NAME = e.Row.FindControl("USER_NAME") as TextBox;
					if ( USER_NAME != null )
						USER_NAME.Visible = bSupportsPopups;
					Button SELECT_NAME = e.Row.FindControl("SELECT_NAME") as Button;
					if ( SELECT_NAME != null )
						SELECT_NAME.Visible = bSupportsPopups;
				}
				else
				{
					// 07/28/2010 Paul.  Save AjaxAutoComplete and SupportsPopups for use in UserSelect and KBSelect. 
					// We are having issues with the data binding event occurring before the page load. 
					if ( bAjaxAutoComplete || Sql.ToBoolean(Page.Items["AjaxAutoComplete"]) )
					{
						// <ajaxToolkit:AutoCompleteExtender ID="autoUSER_NAME" TargetControlID="USER_NAME" ServiceMethod="USERS_USER_NAME_List" OnClientItemSelected="USERS_USER_NAME_ItemSelected" ServicePath="~/Users/AutoComplete.asmx" MinimumPrefixLength="2" CompletionInterval="250" EnableCaching="true" CompletionSetCount="12" runat="server" />
						AjaxControlToolkit.AutoCompleteExtender auto = new AjaxControlToolkit.AutoCompleteExtender();
						e.Row.Cells[0].Controls.Add(auto);
						auto.ID                   = "autoUSER_NAME";
						auto.TargetControlID      = "USER_NAME";
						auto.ServiceMethod        = "USERS_USER_NAME_List";
						auto.OnClientItemSelected = "USERS_USER_NAME_ItemSelected";
						auto.ServicePath          = "~/Users/AutoComplete.asmx";
						auto.MinimumPrefixLength  = 2;
						auto.CompletionInterval   = 250;
						auto.EnableCaching        = true;
						// 12/09/2010 Paul.  Provide a way to customize the AutoComplete.CompletionSetCount. 
						auto.CompletionSetCount   = Crm.Config.CompletionSetCount();
					}
				}
			}
		}

		protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if ( e.Row.RowType == DataControlRowType.DataRow )
			{
				// 12/07/2009 Paul.  The Opera Mini browser does not support popups. Use a DropdownList instead. 
				// 07/28/2010 Paul.  Save AjaxAutoComplete and SupportsPopups for use in UserSelect and KBSelect. 
				// We are having issues with the data binding event occurring before the page load. 
				if ( !(bSupportsPopups || Sql.ToBoolean(Page.Items["SupportsPopups"])) )
				{
					DropDownList lstUSER_ID = e.Row.FindControl("lstUSER_ID") as DropDownList;
					if ( lstUSER_ID != null )
					{
						try
						{
							// 02/07/2010 Paul.  Defensive programming, we don't need to convert the Eval result to a string before using it. 
							// 08/19/2010 Paul.  Check the list before assigning the value. 
							Utils.SetValue(lstUSER_ID, Sql.ToGuid(DataBinder.Eval(e.Row.DataItem, "USER_ID")).ToString() );
						}
						catch
						{
						}
					}
				}
			}
		}

		protected void grdMain_RowEditing(object sender, GridViewEditEventArgs e)
		{
			grdMain.EditIndex = e.NewEditIndex;
			if ( dtLineItems != null )
			{
				grdMain.DataSource = dtLineItems;
				grdMain.DataBind();
			}
		}

		protected void grdMain_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			if ( dtLineItems != null )
			{
				//dtLineItems.Rows.RemoveAt(e.RowIndex);
				//dtLineItems.Rows[e.RowIndex].Delete();
				// 08/07/2007 fhsakai.  There might already be deleted rows, so make sure to first obtain the current rows. 
				DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
				aCurrentRows[e.RowIndex].Delete();
				
				aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
				// 02/04/2007 Paul.  Always allow editing of the last empty row. Add blank row if necessary. 
				// 08/11/2007 Paul.  Allow an item to be manually added.  Require either a product ID or a name. 
				if ( aCurrentRows.Length == 0 || !Sql.IsEmptyString(aCurrentRows[aCurrentRows.Length-1]["USER_NAME"]) || !Sql.IsEmptyGuid(aCurrentRows[aCurrentRows.Length-1]["USER_ID"]) )
				{
					DataRow rowNew = dtLineItems.NewRow();
					dtLineItems.Rows.Add(rowNew);
					aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
				}

				ViewState["LineItems"] = dtLineItems;
				grdMain.DataSource = dtLineItems;
				// 03/15/2007 Paul.  Make sure to use the last row of the current set, not the total rows of the table.  Some rows may be deleted. 
				grdMain.EditIndex = aCurrentRows.Length - 1;
				grdMain.DataBind();
			}
		}

		protected void grdMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
		{
			if ( dtLineItems != null )
			{
				GridViewRow gr = grdMain.Rows[e.RowIndex];
				DropDownList       lstUSER_ID      = gr.FindControl("lstUSER_ID"          ) as DropDownList;
				HiddenField        txtUSER_ID      = gr.FindControl("USER_ID"             ) as HiddenField;
				TextBox            txtUSER_NAME    = gr.FindControl("USER_NAME"           ) as TextBox    ;
				CheckBox           chkPRIMARY_USER = gr.FindControl("PRIMARY_USER"        ) as CheckBox   ;
				HtmlGenericControl spnAjaxErrors   = gr.FindControl("USER_NAME_AjaxErrors") as HtmlGenericControl;
				Guid   gUSER_ID   = Guid.Empty;
				string sUSER_NAME = String.Empty;
				// 12/07/2009 Paul.  The Opera Mini browser does not support popups. Use a DropdownList instead. 
				if ( !bSupportsPopups && lstUSER_ID != null )
				{
					gUSER_ID   = Sql.ToGuid(lstUSER_ID.SelectedValue);
					sUSER_NAME = Sql.ToString(lstUSER_ID.SelectedItem.Text);
				}
				else
				{
					if ( txtUSER_ID != null )
						gUSER_ID = Sql.ToGuid(txtUSER_ID.Value);
					if ( txtUSER_NAME    != null ) 
						sUSER_NAME = txtUSER_NAME.Text;
				}

				if ( gUSER_ID != Guid.Empty )
				{
					//DataRow row = dtLineItems.Rows[e.RowIndex];
					// 12/07/2007 garf.  If there are deleted rows in the set, then the index will be wrong.  Make sure to use the current rowset. 
					DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					DataRow row = aCurrentRows[e.RowIndex];
					row["USER_ID"     ] = gUSER_ID;
					row["USER_NAME"   ] = sUSER_NAME;
					if ( chkPRIMARY_USER != null ) row["PRIMARY_USER"] = chkPRIMARY_USER.Checked;
					// 08/23/2009 Paul.  If the primary user is checked, then make sure that the flag is reset for all other rows. 
					if ( chkPRIMARY_USER.Checked )
					{
						for ( int i = 0; i < aCurrentRows.Length; i++ )
						{
							if ( i != e.RowIndex )
								aCurrentRows[i]["PRIMARY_USER"] = false;
						}
					}
					
					// 12/07/2007 Paul.  aCurrentRows is defined above. 
					//DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					// 03/30/2007 Paul.  Always allow editing of the last empty row. Add blank row if necessary. 
					// 08/11/2007 Paul.  Allow an item to be manually added.  Require either a product ID or a name. 
					if ( aCurrentRows.Length == 0 || !Sql.IsEmptyString(aCurrentRows[aCurrentRows.Length-1]["USER_NAME"]) || !Sql.IsEmptyGuid(aCurrentRows[aCurrentRows.Length-1]["USER_ID"]) )
					{
						DataRow rowNew = dtLineItems.NewRow();
						dtLineItems.Rows.Add(rowNew);
						aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
					}

					ViewState["LineItems"] = dtLineItems;
					grdMain.DataSource = dtLineItems;
					// 03/30/2007 Paul.  Make sure to use the last row of the current set, not the total rows of the table.  Some rows may be deleted. 
					grdMain.EditIndex = aCurrentRows.Length - 1;
					grdMain.DataBind();
				}
				// 02/07/2010 Paul.  Defensive programming, check for valid spnAjaxErrors control. 
				else if ( spnAjaxErrors != null )
				{
					spnAjaxErrors.InnerHtml = "<br />" + L10n.Term("Users.ERR_INVALID_USER");
				}
			}
		}

		protected void grdMain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
		{
			grdMain.EditIndex = -1;
			if ( dtLineItems != null )
			{
				DataRow[] aCurrentRows = dtLineItems.Select(String.Empty, String.Empty, DataViewRowState.CurrentRows);
				grdMain.DataSource = dtLineItems;
				// 03/15/2007 Paul.  Make sure to use the last row of the current set, not the total rows of the table.  Some rows may be deleted. 
				grdMain.EditIndex = aCurrentRows.Length - 1;
				grdMain.DataBind();
			}
		}
		#endregion

		// 04/14/2016 Paul.  Provide a way to reload the line items. 
		public void LoadLineItems(Guid gUSER_SET_ID, bool bAllowDefaults)
		{
			LoadLineItems(gUSER_SET_ID, bAllowDefaults, false);
		}
	
		public void LoadLineItems(Guid gUSER_SET_ID, bool bAllowDefaults, bool bReload)
		{
			// 12/07/2009 Paul.  The Opera Mini browser does not support popups. Use a DropdownList instead. 
			if ( this.IsMobile )
			{
				// 11/24/2010 Paul.  .NET 4 has broken the compatibility of the browser file system. 
				// We are going to minimize our reliance on browser files in order to reduce deployment issues. 
				bSupportsPopups = Utils.SupportsPopups;
			}
			// 05/06/2010 Paul.  Use a special Page flag to override the default IsPostBack behavior. 
			bool bIsPostBack = this.IsPostBack && !NotPostBack;
			if ( !bIsPostBack || bReload )
			{
				// 08/29/2009 Paul.  Not sure why, but we need to manually bind the Add/Replace controls. 
				pnlAddReplace    .DataBind();
				radUserSetReplace.DataBind();
				radUserSetAdd    .DataBind();
				foreach ( DataControlField col in grdMain.Columns )
				{
					if ( !Sql.IsEmptyString(col.HeaderText) )
					{
						col.HeaderText = L10n.Term(col.HeaderText);
					}
					CommandField cf = col as CommandField;
					if ( cf != null )
					{
						// 01/18/2010 Paul.  These fields must be set in code as they are not bindable. 
						cf.ShowEditButton   = bEnabled;
						cf.ShowDeleteButton = bEnabled;
						// 08/31/2009 Paul.  Now that we are using our own ImageButtons in a TemplateField, 
						// we no longer need this CommandField logic. 
						/*
						if ( cf.Visible )
						{
							cf.EditText       = L10n.Term(cf.EditText  );
							cf.DeleteText     = L10n.Term(cf.DeleteText);
							cf.UpdateText     = L10n.Term(cf.UpdateText);
							cf.CancelText     = L10n.Term(cf.CancelText);
							cf.EditImageUrl   = Session["themeURL"] + "images/edit_inline.gif"   ;
							cf.DeleteImageUrl = Session["themeURL"] + "images/delete_inline.gif" ;
							cf.UpdateImageUrl = Session["themeURL"] + "images/accept_inline.gif" ;
							cf.CancelImageUrl = Session["themeURL"] + "images/decline_inline.gif";
						}
						*/
					}
				}
				if ( (!Sql.IsEmptyGuid(gUSER_SET_ID)) )
				{
					// 08/24/2009 Paul.  We need to create another connection, even though there is usually an existing open connection. 
					// This is because we cannot perform another query while the rdr in the existing connection is still open. 
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						string sSQL;
						sSQL = "select *                    " + ControlChars.CrLf
						     + "  from vwASSIGNED_SETS_USERS" + ControlChars.CrLf
						     + " where ID = @ID             " + ControlChars.CrLf
						     + " order by USER_NAME asc     " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gUSER_SET_ID);
							
							if ( bDebug )
								RegisterClientScriptBlock("vwASSIGNED_SETS_USERS", Sql.ClientScriptBlock(cmd));
							
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dtLineItems = new DataTable();
								da.Fill(dtLineItems);
								
								// 03/27/2007 Paul.  Always add blank line to allow quick editing. 
								DataRow rowNew = dtLineItems.NewRow();
								dtLineItems.Rows.Add(rowNew);
								
								ViewState["LineItems"] = dtLineItems;
								grdMain.DataSource = dtLineItems;
								// 03/27/2007 Paul.  Start with last line enabled for editing. 
								grdMain.EditIndex = dtLineItems.Rows.Count - 1;
								grdMain.DataBind();
							}
						}
					}
				}
				else
				{
					InitTable();

					// 08/29/2009 Paul.  If sModule is empty, then we are likely in the MassUpdate panel and we should not provide a default user. 
					// 09/21/2009 Paul.  Only set the defaults if the user exists. 
					if ( bAllowDefaults && !Sql.IsEmptyGuid(Security.USER_ID) && !Sql.IsEmptyString(Security.USER_NAME) )
					{
						DataRow rowDefaultUser = dtLineItems.NewRow();
						dtLineItems.Rows.Add(rowDefaultUser);
						rowDefaultUser["USER_ID"     ] = Security.USER_ID  ;
						rowDefaultUser["USER_NAME"   ] = Security.USER_NAME;
						rowDefaultUser["PRIMARY_USER"] = true;
					}

					DataRow rowNew = dtLineItems.NewRow();
					dtLineItems.Rows.Add(rowNew);

					ViewState["LineItems"] = dtLineItems;
					grdMain.DataSource = dtLineItems;
					// 02/03/2007 Paul.  Start with last line enabled for editing. 
					grdMain.EditIndex = dtLineItems.Rows.Count - 1;
					grdMain.DataBind();
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			valUserSelect.ErrorMessage = L10n.Term(".ERR_REQUIRED_FIELD");
			if ( this.IsMobile )
			{
				// 11/24/2010 Paul.  .NET 4 has broken the compatibility of the browser file system. 
				// We are going to minimize our reliance on browser files in order to reduce deployment issues. 
				bSupportsPopups = Utils.SupportsPopups;
			}
			// 05/06/2010 Paul.  Use a special Page flag to override the default IsPostBack behavior. 
			bool bIsPostBack = this.IsPostBack && !NotPostBack;
			if ( bIsPostBack )
			{
				dtLineItems = ViewState["LineItems"] as DataTable;
				grdMain.DataSource = dtLineItems;
				// 03/31/2007 Paul.  Don't bind the grid, otherwise edits will be lost. 
				//grdMain.DataBind();
			}
			
			// 05/06/2010 Paul.  Move the ajax refence code to Page_Load as it only needs to be called once. 
			ScriptManager mgrAjax = ScriptManager.GetCurrent(this.Page);
			// 11/23/2009 Paul.  SplendidCRM 4.0 is very slow on Blackberry devices.  Lets try and turn off AJAX AutoComplete. 
			bAjaxAutoComplete = (mgrAjax != null);
			if ( this.IsMobile )
			{
				// 11/24/2010 Paul.  .NET 4 has broken the compatibility of the browser file system. 
				// We are going to minimize our reliance on browser files in order to reduce deployment issues. 
				bAjaxAutoComplete = Utils.AllowAutoComplete && (mgrAjax != null);
			}
			if ( bAjaxAutoComplete )
			{
				// 05/12/2016 Paul.  Move AddScriptReference and AddStyleSheet to Sql object. 
				Sql.AddServiceReference(mgrAjax, "~/Users/AutoComplete.asmx");
				Sql.AddScriptReference (mgrAjax, "~/Users/AutoComplete.js"  );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
