﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Copyright.ascx.cs" Inherits="SplendidCRM._controls.Copyright" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2005-2018 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divFooterCopyright" align="left" class="copyRight">
    <!--Copyright &copy; 2005-2018 <asp:HyperLink ID="lnkSplendidCRM" NavigateUrl="http://www.splendidcrm.com" Text="SplendidCRM Software, Inc." Target="_blank" CssClass="copyRightLink" runat="server" /> All Rights Reserved.<br />-->
    <script type="text/javascript" src="<%# Application["scriptURL"] %>numeric.js"></script>
    <script type="text/javascript" src="<%# Application["scriptURL"] %>numeral.min.js"></script>
</div>

