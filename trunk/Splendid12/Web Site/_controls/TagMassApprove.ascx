<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="TagMassApprove.ascx.cs" Inherits="SplendidCRM._controls.TagMassApprove" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script runat="server">
/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<div id="divTeamAssignedMassUpdate">
    <asp:Table Width="100%" CellPadding="0" CellSpacing="0" runat="server">
        <asp:TableRow>
            <asp:TableCell Width="10%" CssClass="dataLabel" Wrap="false">
                <asp:Label ID="lblASSIGNED_TO" Visible="<%# bShowAssigned %>" Text='<%# L10n.Term(".LBL_SUBMITED_TO") %>' runat="server" /></asp:TableCell>
            <asp:TableCell Width="40%" CssClass="dataField" Wrap="false">
                <asp:TextBox ID="ASSIGNED_TO" Visible="<%# bShowAssigned %>" ReadOnly="True" runat="server" />
                <asp:HiddenField ID="ASSIGNED_USER_ID" Visible="<%# bShowAssigned %>" runat="server" />
                &nbsp;
				
                
                <asp:Button ID="btnASSIGNED_USER_ID" Visible="<%# bShowAssigned %>" UseSubmitBehavior="false" OnClientClick=<%# "return WflowSelectApproverPopup('" + ASSIGNED_USER_ID.ClientID + "', '" + ASSIGNED_TO.ClientID + "', null, false, null);" %> Text='<%# L10n.Term(".LBL_CHANGE_BUTTON_LABEL") %>' ToolTip='<%# L10n.Term(".LBL_CHANGE_BUTTON_TITLE") %>' CssClass="button" runat="server" />

                <span id="USER_NAME_AjaxErrors" style="color:Red" EnableViewState="false" runat="server" />
            </asp:TableCell>
            <asp:TableCell Width="15%" CssClass="dataLabel"></asp:TableCell>
            <asp:TableCell Width="35%" CssClass="dataField">
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>

