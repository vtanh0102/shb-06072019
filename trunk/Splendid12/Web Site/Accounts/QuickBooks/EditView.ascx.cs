/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Accounts.QuickBooks
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
		protected _controls.HeaderButtons  ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sQID                            ;
		protected HtmlTable       tblMain                         ;
		protected PlaceHolder     plcSubPanel                     ;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Context.Application);
#if DEBUG
						//bQuickBooksEnabled = true;
#endif
						if ( bQuickBooksEnabled )
						{
							// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
							if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
							{
								Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
								Spring.Social.QuickBooks.Api.Customer obj = null;
								if ( !Sql.IsEmptyString(sQID) )
								{
									obj = quickbooks.CustomerOperations.GetById(sQID);
								}
								else
								{
									obj = new Spring.Social.QuickBooks.Api.Customer();
									obj.Active = true;
								}
								string sBillingStreet      = new DynamicControl(this, "BillingStreet"     ).Text;
								string sBillingCity        = new DynamicControl(this, "BillingCity"       ).Text;
								string sBillingState       = new DynamicControl(this, "BillingState"      ).Text;
								string sBillingPostalCode  = new DynamicControl(this, "BillingPostalCode" ).Text;
								string sBillingCountry     = new DynamicControl(this, "BillingCountry"    ).Text;
								string sShippingStreet     = new DynamicControl(this, "ShippingStreet"    ).Text;
								string sShippingCity       = new DynamicControl(this, "ShippingCity"      ).Text;
								string sShippingState      = new DynamicControl(this, "ShippingState"     ).Text;
								string sShippingPostalCode = new DynamicControl(this, "ShippingPostalCode").Text;
								string sShippingCountry    = new DynamicControl(this, "ShippingCountry"   ).Text;
								
								obj.Title                 = String.Empty;
								obj.GivenName             = String.Empty;
								obj.MiddleName            = String.Empty;
								obj.FamilyName            = String.Empty;
								obj.Suffix                = String.Empty;
								obj.DisplayName           = Sql.Truncate(new DynamicControl(this, "Name" ).Text,  100);
								obj.Notes                 = Sql.Truncate(new DynamicControl(this, "Notes").Text, 4000);
								obj.ParentRefValue        = new DynamicControl(this, "Parent"          ).Text;
								obj.PrimaryEmailAddrValue = new DynamicControl(this, "PrimaryEmailAddr").Text;
								obj.AlternatePhoneValue   = new DynamicControl(this, "AlternatePhone"  ).Text;
								obj.FaxValue              = new DynamicControl(this, "Fax"             ).Text;
								obj.PrimaryPhoneValue     = new DynamicControl(this, "PrimaryPhone"    ).Text;
								obj.SetBillAddr(sBillingStreet , sBillingCity , sBillingState , sBillingPostalCode , sBillingCountry );
								obj.SetShipAddr(sShippingStreet, sShippingCity, sShippingState, sShippingPostalCode, sShippingCountry);
	
								if ( !Sql.IsEmptyString(sQID) )
								{
									quickbooks.CustomerOperations.Update(obj);
								}
								else
								{
									obj = quickbooks.CustomerOperations.Insert(obj);
									sQID = obj.Id;
								}
							}
							else
							{
								QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										string sSQL;
										if ( Sql.IsEmptyString(sQID) )
										{
											sSQL = "insert into Customers    " + ControlChars.CrLf
											     + "     ( Name              " + ControlChars.CrLf
											     + "     , Notes             " + ControlChars.CrLf
											     + "     , Email             " + ControlChars.CrLf
											     + "     , AlternateContact  " + ControlChars.CrLf
											     + "     , AlternatePhone    " + ControlChars.CrLf
											     + "     , Fax               " + ControlChars.CrLf
											     + "     , Phone             " + ControlChars.CrLf
											     + "     , BillingLine1      " + ControlChars.CrLf
											     + "     , BillingLine2      " + ControlChars.CrLf
											     + "     , BillingLine3      " + ControlChars.CrLf
											     + "     , BillingLine4      " + ControlChars.CrLf
											     + "     , BillingLine5      " + ControlChars.CrLf
											     + "     , BillingCity       " + ControlChars.CrLf
											     + "     , BillingState      " + ControlChars.CrLf
											     + "     , BillingPostalCode " + ControlChars.CrLf
											     + "     , BillingCountry    " + ControlChars.CrLf
											     + "     , ShippingLine1     " + ControlChars.CrLf
											     + "     , ShippingLine2     " + ControlChars.CrLf
											     + "     , ShippingLine3     " + ControlChars.CrLf
											     + "     , ShippingLine4     " + ControlChars.CrLf
											     + "     , ShippingLine5     " + ControlChars.CrLf
											     + "     , ShippingCity      " + ControlChars.CrLf
											     + "     , ShippingState     " + ControlChars.CrLf
											     + "     , ShippingPostalCode" + ControlChars.CrLf
											     + "     , ShippingCountry   " + ControlChars.CrLf
											     + "     )" + ControlChars.CrLf
											     + "values" + ControlChars.CrLf
											     + "     ( @Name              " + ControlChars.CrLf
											     + "     , @Notes             " + ControlChars.CrLf
											     + "     , @Email             " + ControlChars.CrLf
											     + "     , @AlternateContact  " + ControlChars.CrLf
											     + "     , @AlternatePhone    " + ControlChars.CrLf
											     + "     , @Fax               " + ControlChars.CrLf
											     + "     , @Phone             " + ControlChars.CrLf
											     + "     , @BillingLine1      " + ControlChars.CrLf
											     + "     , @BillingLine2      " + ControlChars.CrLf
											     + "     , @BillingLine3      " + ControlChars.CrLf
											     + "     , @BillingLine4      " + ControlChars.CrLf
											     + "     , @BillingLine5      " + ControlChars.CrLf
											     + "     , @BillingCity       " + ControlChars.CrLf
											     + "     , @BillingState      " + ControlChars.CrLf
											     + "     , @BillingPostalCode " + ControlChars.CrLf
											     + "     , @BillingCountry    " + ControlChars.CrLf
											     + "     , @ShippingLine1     " + ControlChars.CrLf
											     + "     , @ShippingLine2     " + ControlChars.CrLf
											     + "     , @ShippingLine3     " + ControlChars.CrLf
											     + "     , @ShippingLine4     " + ControlChars.CrLf
											     + "     , @ShippingLine5     " + ControlChars.CrLf
											     + "     , @ShippingCity      " + ControlChars.CrLf
											     + "     , @ShippingState     " + ControlChars.CrLf
											     + "     , @ShippingPostalCode" + ControlChars.CrLf
											     + "     , @ShippingCountry   " + ControlChars.CrLf
											     + "     ) " + ControlChars.CrLf;
										}
										else
										{
											sSQL = "update Customers                               " + ControlChars.CrLf
											     + "   set Name               = @Name              " + ControlChars.CrLf
											     + "     , Notes              = @Notes             " + ControlChars.CrLf
											     + "     , Email              = @Email             " + ControlChars.CrLf
											     + "     , AlternateContact   = @AlternateContact  " + ControlChars.CrLf
											     + "     , AlternatePhone     = @AlternatePhone    " + ControlChars.CrLf
											     + "     , Fax                = @Fax               " + ControlChars.CrLf
											     + "     , Phone              = @Phone             " + ControlChars.CrLf
											     + "     , BillingLine1       = @BillingLine1      " + ControlChars.CrLf
											     + "     , BillingLine2       = @BillingLine2      " + ControlChars.CrLf
											     + "     , BillingLine3       = @BillingLine3      " + ControlChars.CrLf
											     + "     , BillingLine4       = @BillingLine4      " + ControlChars.CrLf
											     + "     , BillingLine5       = @BillingLine5      " + ControlChars.CrLf
											     + "     , BillingCity        = @BillingCity       " + ControlChars.CrLf
											     + "     , BillingState       = @BillingState      " + ControlChars.CrLf
											     + "     , BillingPostalCode  = @BillingPostalCode " + ControlChars.CrLf
											     + "     , BillingCountry     = @BillingCountry    " + ControlChars.CrLf
											     + "     , ShippingLine1      = @ShippingLine1     " + ControlChars.CrLf
											     + "     , ShippingLine2      = @ShippingLine2     " + ControlChars.CrLf
											     + "     , ShippingLine3      = @ShippingLine3     " + ControlChars.CrLf
											     + "     , ShippingLine4      = @ShippingLine4     " + ControlChars.CrLf
											     + "     , ShippingLine5      = @ShippingLine5     " + ControlChars.CrLf
											     + "     , ShippingCity       = @ShippingCity      " + ControlChars.CrLf
											     + "     , ShippingState      = @ShippingState     " + ControlChars.CrLf
											     + "     , ShippingPostalCode = @ShippingPostalCode" + ControlChars.CrLf
											     + "     , ShippingCountry    = @ShippingCountry   " + ControlChars.CrLf
											     + " where ID                 = @ID                " + ControlChars.CrLf;
										}
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@Name"              , new DynamicControl(this, "Name"              ).Text,   41);
										Sql.AddParameter(cmd, "@Notes"             , new DynamicControl(this, "Notes"             ).Text, 4095);
										Sql.AddParameter(cmd, "@Email"             , new DynamicControl(this, "Email"             ).Text, 1000);
										Sql.AddParameter(cmd, "@AlternateContact"  , new DynamicControl(this, "AlternateContact"  ).Text,   41);
										Sql.AddParameter(cmd, "@AlternatePhone"    , new DynamicControl(this, "AlternatePhone"    ).Text,   21);
										Sql.AddParameter(cmd, "@Fax"               , new DynamicControl(this, "Fax"               ).Text,   21);
										Sql.AddParameter(cmd, "@Phone"             , new DynamicControl(this, "Phone"             ).Text,   21);
										Sql.AddParameter(cmd, "@BillingLine1"      , new DynamicControl(this, "BillingLine1"      ).Text,   41);
										Sql.AddParameter(cmd, "@BillingLine2"      , new DynamicControl(this, "BillingLine2"      ).Text,   41);
										Sql.AddParameter(cmd, "@BillingLine3"      , new DynamicControl(this, "BillingLine3"      ).Text,   41);
										Sql.AddParameter(cmd, "@BillingLine4"      , new DynamicControl(this, "BillingLine4"      ).Text,   41);
										Sql.AddParameter(cmd, "@BillingLine5"      , new DynamicControl(this, "BillingLine5"      ).Text,   41);
										Sql.AddParameter(cmd, "@BillingCity"       , new DynamicControl(this, "BillingCity"       ).Text,   31);
										Sql.AddParameter(cmd, "@BillingState"      , new DynamicControl(this, "BillingState"      ).Text,   21);
										Sql.AddParameter(cmd, "@BillingPostalCode" , new DynamicControl(this, "BillingPostalCode" ).Text,   13);
										Sql.AddParameter(cmd, "@BillingCountry"    , new DynamicControl(this, "BillingCountry"    ).Text,   31);
										Sql.AddParameter(cmd, "@ShippingLine1"     , new DynamicControl(this, "ShippingLine1"     ).Text,   41);
										Sql.AddParameter(cmd, "@ShippingLine2"     , new DynamicControl(this, "ShippingLine2"     ).Text,   41);
										Sql.AddParameter(cmd, "@ShippingLine3"     , new DynamicControl(this, "ShippingLine3"     ).Text,   41);
										Sql.AddParameter(cmd, "@ShippingLine4"     , new DynamicControl(this, "ShippingLine4"     ).Text,   41);
										Sql.AddParameter(cmd, "@ShippingLine5"     , new DynamicControl(this, "ShippingLine5"     ).Text,   41);
										Sql.AddParameter(cmd, "@ShippingCity"      , new DynamicControl(this, "ShippingCity"      ).Text,   31);
										Sql.AddParameter(cmd, "@ShippingState"     , new DynamicControl(this, "ShippingState"     ).Text,   21);
										Sql.AddParameter(cmd, "@ShippingPostalCode", new DynamicControl(this, "ShippingPostalCode").Text,   13);
										Sql.AddParameter(cmd, "@ShippingCountry"   , new DynamicControl(this, "ShippingCountry"   ).Text,   31);
									
										if ( !Sql.IsEmptyString(sQID) )
											Sql.AddParameter(cmd, "@ID", sQID);
										cmd.ExecuteNonQuery();
										if ( !Sql.IsEmptyString(sQID) )
										{
											Hashtable result = dbf.GetLastResult(con);
											sQID = Sql.ToString(result["id"]);
										}
									}
								}
							}
							Response.Redirect("view.aspx?QID=" + sQID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sQID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?QID=" + sQID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = (SplendidCRM.Security.IS_ADMIN || SplendidCRM.Security.USER_ID == Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]));
			if ( !this.Visible )
				return;

			try
			{
				sQID = Sql.ToString(Request["QID"]);
				if ( !IsPostBack )
				{
					string sDuplicateQID = Sql.ToString(Request["DuplicateQID"]);
					if ( !Sql.IsEmptyString(sQID) || !Sql.IsEmptyString(sDuplicateQID) )
					{
						// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
						bool bQuickBooksEnabled = QuickBooksSync.QuickBooksEnabled(Application);
#if DEBUG
						//bQuickBooksEnabled = true;
#endif
						if ( bQuickBooksEnabled )
						{
							// 06/21/2014 Paul.  Use Social API for QuickBooks Online. 
							if ( QuickBooksSync.IsOnlineAppMode(Context.Application) )
							{
								Spring.Social.QuickBooks.Api.IQuickBooks quickbooks = Spring.Social.QuickBooks.QuickBooksSync.CreateApi(Context.Application);
								
								DataRow rdr = Spring.Social.QuickBooks.Api.Customer.ConvertToRow(quickbooks.CustomerOperations.GetById(sQID));
								//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
								// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
								ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
								SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
								ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
								
								this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
								ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
								ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
								TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
								if ( txtNAME != null )
									txtNAME.Focus();
								
								//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
							}
							else
							{
								QuickBooksClientFactory dbf = QuickBooksSync.CreateFactory(Application);
								using ( IDbConnection con = dbf.CreateConnection() )
								{
									con.Open();
									string sSQL ;
									sSQL = "select *        " + ControlChars.CrLf
									     + "  from Customers" + ControlChars.CrLf
									     + " where ID = @ID " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										if ( !Sql.IsEmptyString(sDuplicateQID) )
										{
											Sql.AddParameter(cmd, "@ID", sDuplicateQID);
											sQID = String.Empty;
										}
										else
										{
											Sql.AddParameter(cmd, "@ID", sQID);
										}
										using ( DbDataAdapter da = dbf.CreateDataAdapter() )
										{
											((IDbDataAdapter)da).SelectCommand = cmd;
											using ( DataTable dtCurrent = new DataTable() )
											{
												da.Fill(dtCurrent);
												if ( dtCurrent.Rows.Count > 0 )
												{
													DataRow rdr = dtCurrent.Rows[0];
													//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
												
													// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
													ctlDynamicButtons.Title = Sql.ToString(rdr[QuickBooksSync.PrimarySortField(Application, m_sMODULE)]);
													SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
													ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;
												
													this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
													ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
													TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
													if ( txtNAME != null )
														txtNAME.Focus();
												
													//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
												}
												else
												{
													ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
													ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
													ctlDynamicButtons.DisableAll();
													ctlFooterButtons .DisableAll();
													ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
													plcSubPanel.Visible = false;
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl(QuickBooksSync.PrimarySortField(Application, m_sMODULE)) as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					// 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
					ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Accounts";
			SetMenu(m_sMODULE);
			bool bNewRecord = Sql.IsEmptyString(Request["QID"]);
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			// 02/06/2014 Paul.  Go back to a single QuickBooks UI. 
			// 02/13/2015 Paul.  New QuickBooks Online code uses a different schema. 
			this.LayoutEditView = "EditView.QuickBooks" + (QuickBooksSync.IsOnlineAppMode(Application) ? "Online" : String.Empty);
			this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, bNewRecord);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

