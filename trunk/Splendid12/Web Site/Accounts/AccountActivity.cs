/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// AccountActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:27 PM
	/// </summary>
	public class AccountActivity: SplendidActivity
	{
		public AccountActivity()
		{
			this.Name = "AccountActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.IDProperty))); }
			set { base.SetValue(AccountActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(AccountActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.NAMEProperty))); }
			set { base.SetValue(AccountActivity.NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_TYPE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_TYPE
		{
			get { return ((string)(base.GetValue(AccountActivity.ACCOUNT_TYPEProperty))); }
			set { base.SetValue(AccountActivity.ACCOUNT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.PARENT_IDProperty))); }
			set { base.SetValue(AccountActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty INDUSTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INDUSTRY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string INDUSTRY
		{
			get { return ((string)(base.GetValue(AccountActivity.INDUSTRYProperty))); }
			set { base.SetValue(AccountActivity.INDUSTRYProperty, value); }
		}

		public static DependencyProperty ANNUAL_REVENUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ANNUAL_REVENUE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ANNUAL_REVENUE
		{
			get { return ((string)(base.GetValue(AccountActivity.ANNUAL_REVENUEProperty))); }
			set { base.SetValue(AccountActivity.ANNUAL_REVENUEProperty, value); }
		}

		public static DependencyProperty PHONE_FAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_FAX", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_FAX
		{
			get { return ((string)(base.GetValue(AccountActivity.PHONE_FAXProperty))); }
			set { base.SetValue(AccountActivity.PHONE_FAXProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_STREET", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_STREETProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_CITY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_CITYProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_STATE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_STATEProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_POSTALCODE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_COUNTRY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(AccountActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(AccountActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty RATINGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RATING", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RATING
		{
			get { return ((string)(base.GetValue(AccountActivity.RATINGProperty))); }
			set { base.SetValue(AccountActivity.RATINGProperty, value); }
		}

		public static DependencyProperty PHONE_OFFICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_OFFICE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_OFFICE
		{
			get { return ((string)(base.GetValue(AccountActivity.PHONE_OFFICEProperty))); }
			set { base.SetValue(AccountActivity.PHONE_OFFICEProperty, value); }
		}

		public static DependencyProperty PHONE_ALTERNATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_ALTERNATE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_ALTERNATE
		{
			get { return ((string)(base.GetValue(AccountActivity.PHONE_ALTERNATEProperty))); }
			set { base.SetValue(AccountActivity.PHONE_ALTERNATEProperty, value); }
		}

		public static DependencyProperty EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL1", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL1
		{
			get { return ((string)(base.GetValue(AccountActivity.EMAIL1Property))); }
			set { base.SetValue(AccountActivity.EMAIL1Property, value); }
		}

		public static DependencyProperty EMAIL2Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL2", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL2
		{
			get { return ((string)(base.GetValue(AccountActivity.EMAIL2Property))); }
			set { base.SetValue(AccountActivity.EMAIL2Property, value); }
		}

		public static DependencyProperty WEBSITEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WEBSITE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string WEBSITE
		{
			get { return ((string)(base.GetValue(AccountActivity.WEBSITEProperty))); }
			set { base.SetValue(AccountActivity.WEBSITEProperty, value); }
		}

		public static DependencyProperty OWNERSHIPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OWNERSHIP", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string OWNERSHIP
		{
			get { return ((string)(base.GetValue(AccountActivity.OWNERSHIPProperty))); }
			set { base.SetValue(AccountActivity.OWNERSHIPProperty, value); }
		}

		public static DependencyProperty EMPLOYEESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMPLOYEES", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMPLOYEES
		{
			get { return ((string)(base.GetValue(AccountActivity.EMPLOYEESProperty))); }
			set { base.SetValue(AccountActivity.EMPLOYEESProperty, value); }
		}

		public static DependencyProperty SIC_CODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SIC_CODE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SIC_CODE
		{
			get { return ((string)(base.GetValue(AccountActivity.SIC_CODEProperty))); }
			set { base.SetValue(AccountActivity.SIC_CODEProperty, value); }
		}

		public static DependencyProperty TICKER_SYMBOLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TICKER_SYMBOL", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TICKER_SYMBOL
		{
			get { return ((string)(base.GetValue(AccountActivity.TICKER_SYMBOLProperty))); }
			set { base.SetValue(AccountActivity.TICKER_SYMBOLProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_STREET", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_STREETProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_CITY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_CITYProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_STATE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_STATEProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_POSTALCODE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_COUNTRY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NUMBER", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NUMBER
		{
			get { return ((string)(base.GetValue(AccountActivity.ACCOUNT_NUMBERProperty))); }
			set { base.SetValue(AccountActivity.ACCOUNT_NUMBERProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.TEAM_IDProperty))); }
			set { base.SetValue(AccountActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(AccountActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(AccountActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(AccountActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(AccountActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty PICTUREProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PICTURE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PICTURE
		{
			get { return ((string)(base.GetValue(AccountActivity.PICTUREProperty))); }
			set { base.SetValue(AccountActivity.PICTUREProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(AccountActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty NAICS_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAICS_SET_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAICS_SET_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.NAICS_SET_NAMEProperty))); }
			set { base.SetValue(AccountActivity.NAICS_SET_NAMEProperty, value); }
		}

		public static DependencyProperty DO_NOT_CALLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DO_NOT_CALL", typeof(bool), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool DO_NOT_CALL
		{
			get { return ((bool)(base.GetValue(AccountActivity.DO_NOT_CALLProperty))); }
			set { base.SetValue(AccountActivity.DO_NOT_CALLProperty, value); }
		}

		public static DependencyProperty EMAIL_OPT_OUTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL_OPT_OUT", typeof(bool), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EMAIL_OPT_OUT
		{
			get { return ((bool)(base.GetValue(AccountActivity.EMAIL_OPT_OUTProperty))); }
			set { base.SetValue(AccountActivity.EMAIL_OPT_OUTProperty, value); }
		}

		public static DependencyProperty INVALID_EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INVALID_EMAIL", typeof(bool), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool INVALID_EMAIL
		{
			get { return ((bool)(base.GetValue(AccountActivity.INVALID_EMAILProperty))); }
			set { base.SetValue(AccountActivity.INVALID_EMAILProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(AccountActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(AccountActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(AccountActivity.CREATED_BYProperty))); }
			set { base.SetValue(AccountActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(AccountActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(AccountActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(AccountActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(AccountActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(AccountActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(AccountActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(AccountActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(AccountActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(AccountActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(AccountActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(AccountActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(AccountActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(AccountActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty BILLING_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_ADDRESS_HTML", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(AccountActivity.BILLING_ADDRESS_HTMLProperty))); }
			set { base.SetValue(AccountActivity.BILLING_ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CITY", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CITY
		{
			get { return ((string)(base.GetValue(AccountActivity.CITYProperty))); }
			set { base.SetValue(AccountActivity.CITYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(AccountActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(AccountActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(AccountActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(AccountActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(AccountActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(AccountActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(AccountActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(AccountActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(AccountActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(AccountActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE
		{
			get { return ((string)(base.GetValue(AccountActivity.PHONEProperty))); }
			set { base.SetValue(AccountActivity.PHONEProperty, value); }
		}

		public static DependencyProperty SHIPPING_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHIPPING_ADDRESS_HTML", typeof(string), typeof(AccountActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SHIPPING_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(AccountActivity.SHIPPING_ADDRESS_HTMLProperty))); }
			set { base.SetValue(AccountActivity.SHIPPING_ADDRESS_HTMLProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("AccountActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("AccountActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select ACCOUNTS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwACCOUNTS_AUDIT        ACCOUNTS          " + ControlChars.CrLf
							                + " inner join vwACCOUNTS_AUDIT        ACCOUNTS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on ACCOUNTS_AUDIT_OLD.ID = ACCOUNTS.ID       " + ControlChars.CrLf
							                + "        and ACCOUNTS_AUDIT_OLD.AUDIT_VERSION = (select max(vwACCOUNTS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                  from vwACCOUNTS_AUDIT                   " + ControlChars.CrLf
							                + "                                                 where vwACCOUNTS_AUDIT.ID            =  ACCOUNTS.ID           " + ControlChars.CrLf
							                + "                                                   and vwACCOUNTS_AUDIT.AUDIT_VERSION <  ACCOUNTS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                   and vwACCOUNTS_AUDIT.AUDIT_TOKEN   <> ACCOUNTS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                               )" + ControlChars.CrLf
							                + " where ACCOUNTS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwACCOUNTS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwACCOUNTS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *              " + ControlChars.CrLf
							                + "  from vwACCOUNTS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								ACCOUNT_TYPE                   = Sql.ToString  (rdr["ACCOUNT_TYPE"                  ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								INDUSTRY                       = Sql.ToString  (rdr["INDUSTRY"                      ]);
								ANNUAL_REVENUE                 = Sql.ToString  (rdr["ANNUAL_REVENUE"                ]);
								PHONE_FAX                      = Sql.ToString  (rdr["PHONE_FAX"                     ]);
								BILLING_ADDRESS_STREET         = Sql.ToString  (rdr["BILLING_ADDRESS_STREET"        ]);
								BILLING_ADDRESS_CITY           = Sql.ToString  (rdr["BILLING_ADDRESS_CITY"          ]);
								BILLING_ADDRESS_STATE          = Sql.ToString  (rdr["BILLING_ADDRESS_STATE"         ]);
								BILLING_ADDRESS_POSTALCODE     = Sql.ToString  (rdr["BILLING_ADDRESS_POSTALCODE"    ]);
								BILLING_ADDRESS_COUNTRY        = Sql.ToString  (rdr["BILLING_ADDRESS_COUNTRY"       ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								RATING                         = Sql.ToString  (rdr["RATING"                        ]);
								PHONE_OFFICE                   = Sql.ToString  (rdr["PHONE_OFFICE"                  ]);
								PHONE_ALTERNATE                = Sql.ToString  (rdr["PHONE_ALTERNATE"               ]);
								EMAIL1                         = Sql.ToString  (rdr["EMAIL1"                        ]);
								EMAIL2                         = Sql.ToString  (rdr["EMAIL2"                        ]);
								WEBSITE                        = Sql.ToString  (rdr["WEBSITE"                       ]);
								OWNERSHIP                      = Sql.ToString  (rdr["OWNERSHIP"                     ]);
								EMPLOYEES                      = Sql.ToString  (rdr["EMPLOYEES"                     ]);
								SIC_CODE                       = Sql.ToString  (rdr["SIC_CODE"                      ]);
								TICKER_SYMBOL                  = Sql.ToString  (rdr["TICKER_SYMBOL"                 ]);
								SHIPPING_ADDRESS_STREET        = Sql.ToString  (rdr["SHIPPING_ADDRESS_STREET"       ]);
								SHIPPING_ADDRESS_CITY          = Sql.ToString  (rdr["SHIPPING_ADDRESS_CITY"         ]);
								SHIPPING_ADDRESS_STATE         = Sql.ToString  (rdr["SHIPPING_ADDRESS_STATE"        ]);
								SHIPPING_ADDRESS_POSTALCODE    = Sql.ToString  (rdr["SHIPPING_ADDRESS_POSTALCODE"   ]);
								SHIPPING_ADDRESS_COUNTRY       = Sql.ToString  (rdr["SHIPPING_ADDRESS_COUNTRY"      ]);
								ACCOUNT_NUMBER                 = Sql.ToString  (rdr["ACCOUNT_NUMBER"                ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								PICTURE                        = Sql.ToString  (rdr["PICTURE"                       ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								if ( !bPast )
									NAICS_SET_NAME                 = Sql.ToString  (rdr["NAICS_SET_NAME"                ]);
								DO_NOT_CALL                    = Sql.ToBoolean (rdr["DO_NOT_CALL"                   ]);
								EMAIL_OPT_OUT                  = Sql.ToBoolean (rdr["EMAIL_OPT_OUT"                 ]);
								INVALID_EMAIL                  = Sql.ToBoolean (rdr["INVALID_EMAIL"                 ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									BILLING_ADDRESS_HTML           = Sql.ToString  (rdr["BILLING_ADDRESS_HTML"          ]);
									CITY                           = Sql.ToString  (rdr["CITY"                          ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									PHONE                          = Sql.ToString  (rdr["PHONE"                         ]);
									SHIPPING_ADDRESS_HTML          = Sql.ToString  (rdr["SHIPPING_ADDRESS_HTML"         ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("AccountActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("ACCOUNTS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spACCOUNTS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, ACCOUNT_TYPE
								, PARENT_ID
								, INDUSTRY
								, ANNUAL_REVENUE
								, PHONE_FAX
								, BILLING_ADDRESS_STREET
								, BILLING_ADDRESS_CITY
								, BILLING_ADDRESS_STATE
								, BILLING_ADDRESS_POSTALCODE
								, BILLING_ADDRESS_COUNTRY
								, DESCRIPTION
								, RATING
								, PHONE_OFFICE
								, PHONE_ALTERNATE
								, EMAIL1
								, EMAIL2
								, WEBSITE
								, OWNERSHIP
								, EMPLOYEES
								, SIC_CODE
								, TICKER_SYMBOL
								, SHIPPING_ADDRESS_STREET
								, SHIPPING_ADDRESS_CITY
								, SHIPPING_ADDRESS_STATE
								, SHIPPING_ADDRESS_POSTALCODE
								, SHIPPING_ADDRESS_COUNTRY
								, ACCOUNT_NUMBER
								, TEAM_ID
								, TEAM_SET_LIST
								, EXCHANGE_FOLDER
								, PICTURE
								, TAG_SET_NAME
								, NAICS_SET_NAME
								, DO_NOT_CALL
								, EMAIL_OPT_OUT
								, INVALID_EMAIL
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("AccountActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

