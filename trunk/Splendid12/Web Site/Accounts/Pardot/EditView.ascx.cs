/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Accounts.Pardot
{
	/// <summary>
	///		Summary description for EditView.
	/// </summary>
	public class EditView : SplendidControl
	{
		protected _controls.ModuleHeader   ctlModuleHeader  ;
		protected _controls.DynamicButtons ctlDynamicButtons;
		protected _controls.DynamicButtons ctlFooterButtons ;

		protected string          sID          ;
		protected HtmlTable       tblMain      ;
		protected PlaceHolder     plcSubPanel  ;
		protected Label           lblRawContent;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			if ( e.CommandName == "Save" )
			{
				Spring.Social.Pardot.Api.ProspectAccount account = new Spring.Social.Pardot.Api.ProspectAccount();
				try
				{
					this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
					//this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);
					
					if ( Page.IsValid )
					{
						if ( Spring.Social.Pardot.PardotSync.PardotEnabled(Application) )
						{
							Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
							if ( !Sql.IsEmptyString(sID) )
							{
								account = pardot.ProspectAccountOperations.GetById(Sql.ToInteger(sID));
							}
							if ( this.FindControl("name"                ) != null ) account.name                   = new DynamicControl(this, "name"                ).Text;
							if ( this.FindControl("number"              ) != null ) account.number                 = new DynamicControl(this, "number"              ).IntegerValue;
							if ( this.FindControl("description"         ) != null ) account.description            = new DynamicControl(this, "description"         ).Text;
							if ( this.FindControl("phone"               ) != null ) account.phone                  = new DynamicControl(this, "phone"               ).Text;
							if ( this.FindControl("fax"                 ) != null ) account.fax                    = new DynamicControl(this, "fax"                 ).Text;
							if ( this.FindControl("website"             ) != null ) account.website                = new DynamicControl(this, "website"             ).Text;
							if ( this.FindControl("rating"              ) != null ) account.rating                 = new DynamicControl(this, "rating"              ).Text;
							if ( this.FindControl("site"                ) != null ) account.site                   = new DynamicControl(this, "site"                ).Text;
							if ( this.FindControl("type"                ) != null ) account.type                   = new DynamicControl(this, "type"                ).Text;
							if ( this.FindControl("annual_revenue"      ) != null ) account.annual_revenue         = new DynamicControl(this, "annual_revenue"      ).IntegerValue;
							if ( this.FindControl("industry"            ) != null ) account.industry               = new DynamicControl(this, "industry"            ).Text;
							if ( this.FindControl("sic"                 ) != null ) account.sic                    = new DynamicControl(this, "sic"                 ).Text;
							if ( this.FindControl("employees"           ) != null ) account.employees              = new DynamicControl(this, "employees"           ).IntegerValue;
							if ( this.FindControl("ownership"           ) != null ) account.ownership              = new DynamicControl(this, "ownership"           ).Text;
							if ( this.FindControl("ticker_symbol"       ) != null ) account.ticker_symbol          = new DynamicControl(this, "ticker_symbol"       ).Text;
							if ( this.FindControl("billing_address_one" ) != null ) account.billing_address_one    = new DynamicControl(this, "billing_address_one" ).Text;
							if ( this.FindControl("billing_address_two" ) != null ) account.billing_address_two    = new DynamicControl(this, "billing_address_two" ).Text;
							if ( this.FindControl("billing_city"        ) != null ) account.billing_city           = new DynamicControl(this, "billing_city"        ).Text;
							if ( this.FindControl("billing_state"       ) != null ) account.billing_state          = new DynamicControl(this, "billing_state"       ).Text;
							if ( this.FindControl("billing_zip"         ) != null ) account.billing_zip            = new DynamicControl(this, "billing_zip"         ).Text;
							if ( this.FindControl("billing_country"     ) != null ) account.billing_country        = new DynamicControl(this, "billing_country"     ).Text;
							if ( this.FindControl("shipping_address_one") != null ) account.shipping_address_one   = new DynamicControl(this, "shipping_address_one").Text;
							if ( this.FindControl("shipping_address_two") != null ) account.shipping_address_two   = new DynamicControl(this, "shipping_address_two").Text;
							if ( this.FindControl("shipping_city"       ) != null ) account.shipping_city          = new DynamicControl(this, "shipping_city"       ).Text;
							if ( this.FindControl("shipping_state"      ) != null ) account.shipping_state         = new DynamicControl(this, "shipping_state"      ).Text;
							if ( this.FindControl("shipping_zip"        ) != null ) account.shipping_zip           = new DynamicControl(this, "shipping_zip"        ).Text;
							if ( this.FindControl("shipping_country"    ) != null ) account.shipping_country       = new DynamicControl(this, "shipping_country"    ).Text;
							if ( !Sql.IsEmptyString(sID) )
							{
								pardot.ProspectAccountOperations.Update(account);
							}
							else
							{
								account = pardot.ProspectAccountOperations.Insert(account);
								sID = account.id.ToString();
							}
							Response.Redirect("view.aspx?account_id=" + sID);
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
					if ( account != null )
					{
#if DEBUG
						lblRawContent.Text = account.RawContent;
#endif
					}
				}
			}
			else if ( e.CommandName == "Cancel" )
			{
				if ( Sql.IsEmptyString(sID) )
					Response.Redirect("default.aspx");
				else
					Response.Redirect("view.aspx?account_id=" + sID);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = Spring.Social.Pardot.PardotSync.PardotEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0) && (SplendidCRM.Security.GetUserAccess("Pardot", "edit") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sID = Sql.ToString(Request["account_id"]);
				if ( !IsPostBack )
				{
					string sDuplicateID = Sql.ToString(Request["duplicate_id"]);
					if ( !Sql.IsEmptyString(sID) || !Sql.IsEmptyString(sDuplicateID) )
					{
						Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
						Spring.Social.Pardot.Api.ProspectAccount account = null;
						if ( !Sql.IsEmptyString(sDuplicateID) )
							account = pardot.ProspectAccountOperations.GetById(Sql.ToInteger(sDuplicateID));
						else
							account = pardot.ProspectAccountOperations.GetById(Sql.ToInteger(sID));
						if ( account != null )
						{
							DataRow rdr = Spring.Social.Pardot.Api.ProspectAccount.ConvertToRow(account);
							//this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
								
							ctlModuleHeader.Title = Sql.ToString(rdr["name"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
							ViewState["ctlModuleHeader.Title"] = ctlModuleHeader.Title;
								
							this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, rdr);
							TextBox txtNAME = this.FindControl("name") as TextBox;
							if ( txtNAME != null )
								txtNAME.Focus();
								
							//this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
						}
						else
						{
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlFooterButtons .DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
							plcSubPanel.Visible = false;
						}
					}
					else
					{
						this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
						TextBox txtNAME = this.FindControl("name") as TextBox;
						if ( txtNAME != null )
							txtNAME.Focus();
						
						//this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
					}
				}
				else
				{
					ctlModuleHeader.Title = Sql.ToString(ViewState["ctlModuleHeader.Title"]);
					SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlFooterButtons .Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Accounts";
			SetMenu(m_sMODULE);
			if ( IsPostBack )
			{
				this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain       , null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
				ctlFooterButtons .AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

