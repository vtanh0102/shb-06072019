/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Accounts.Pardot
{
	/// <summary>
	/// Summary description for DetailView.
	/// </summary>
	public class DetailView : SplendidControl
	{
		protected _controls.ModuleHeader   ctlModuleHeader  ;
		protected _controls.DynamicButtons ctlDynamicButtons;

		protected string      sID        ;
		protected HtmlTable   tblMain    ;
		protected PlaceHolder plcSubPanel;
		protected Label       lblRawContent;

		protected void Page_Command(Object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Edit" )
				{
					Response.Redirect("edit.aspx?account_id=" + sID);
				}
				else if ( e.CommandName == "Duplicate" )
				{
					Response.Redirect("edit.aspx?duplicate_id=" + sID);
				}
				else if ( e.CommandName == "Delete" )
				{
					Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
					pardot.ProspectAccountOperations.Delete(Sql.ToInteger(sID));
					Response.Redirect("default.aspx");
				}
				else if ( e.CommandName == "Cancel" )
				{
					Response.Redirect("default.aspx");
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			//SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
			this.Visible = Spring.Social.Pardot.PardotSync.PardotEnabled(Application) && (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0) && (SplendidCRM.Security.GetUserAccess("Pardot", "view") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				sID = Sql.ToString(Request["account_id"]);
				if ( Sql.IsEmptyString(sID) )
					sID = Sql.ToString(ViewState["account_id"]);
				if ( !IsPostBack )
				{
					if ( Sql.IsEmptyString(sID) )
					{
						Guid gID = Sql.ToGuid(Request["ID"]);
						if ( !Sql.IsEmptyGuid(gID) )
						{
							DbProviderFactory dbf = DbProviderFactories.GetFactory();
							using ( IDbConnection con = dbf.CreateConnection() )
							{
								con.Open();
								string sSQL ;
								sSQL = "select SYNC_REMOTE_KEY                       " + ControlChars.CrLf
								     + "  from vwACCOUNTS_SYNC                      " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME = @SYNC_SERVICE_NAME" + ControlChars.CrLf
								     + "   and SYNC_LOCAL_ID     = @SYNC_LOCAL_ID    " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_SERVICE_NAME", "Pardot");
									Sql.AddParameter(cmd, "@SYNC_LOCAL_ID"    , gID     );
									sID = Sql.ToString(cmd.ExecuteScalar());
								}
							}
						}
					}
					if ( !Sql.IsEmptyString(sID) )
					{
						ViewState["account_id"] = sID;
						
						Spring.Social.Pardot.Api.IPardot pardot = Spring.Social.Pardot.PardotSync.CreateApi(Application);
						Spring.Social.Pardot.Api.ProspectAccount account = pardot.ProspectAccountOperations.GetById(Sql.ToInteger(sID));
						if ( account != null )
						{
#if DEBUG
							lblRawContent.Text = account.RawContent;
#endif
							DataRow rdr = Spring.Social.Pardot.Api.ProspectAccount.ConvertToRow(account);
							//this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
							
							ctlModuleHeader.Title = Sql.ToString(rdr["name"]);
							SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlModuleHeader.Title);
							
							this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
							this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, rdr);
							
							//this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
						}
						else
						{
							plcSubPanel.Visible = false;
							ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
							ctlDynamicButtons.DisableAll();
							ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
						}
					}
					else
					{
						ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
						ctlDynamicButtons.DisableAll();
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			m_sMODULE = "Accounts";
			SetMenu(m_sMODULE);
			this.LayoutDetailView = "DetailView.Pardot";
			if ( IsPostBack )
			{
				this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
				this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
				ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
			}
		}
		#endregion
	}
}

