/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// CallActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:27 PM
	/// </summary>
	public class CallActivity: SplendidActivity
	{
		public CallActivity()
		{
			this.Name = "CallActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.IDProperty))); }
			set { base.SetValue(CallActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(CallActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(CallActivity.NAMEProperty))); }
			set { base.SetValue(CallActivity.NAMEProperty, value); }
		}

		public static DependencyProperty DURATION_HOURSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DURATION_HOURS", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 DURATION_HOURS
		{
			get { return ((Int32)(base.GetValue(CallActivity.DURATION_HOURSProperty))); }
			set { base.SetValue(CallActivity.DURATION_HOURSProperty, value); }
		}

		public static DependencyProperty DURATION_MINUTESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DURATION_MINUTES", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 DURATION_MINUTES
		{
			get { return ((Int32)(base.GetValue(CallActivity.DURATION_MINUTESProperty))); }
			set { base.SetValue(CallActivity.DURATION_MINUTESProperty, value); }
		}

		public static DependencyProperty DATE_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_TIMEProperty))); }
			set { base.SetValue(CallActivity.DATE_TIMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(CallActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(CallActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.PARENT_IDProperty))); }
			set { base.SetValue(CallActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(CallActivity.STATUSProperty))); }
			set { base.SetValue(CallActivity.STATUSProperty, value); }
		}

		public static DependencyProperty DIRECTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DIRECTION", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DIRECTION
		{
			get { return ((string)(base.GetValue(CallActivity.DIRECTIONProperty))); }
			set { base.SetValue(CallActivity.DIRECTIONProperty, value); }
		}

		public static DependencyProperty REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REMINDER_TIME", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(CallActivity.REMINDER_TIMEProperty))); }
			set { base.SetValue(CallActivity.REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(CallActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(CallActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty INVITEE_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INVITEE_LIST", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string INVITEE_LIST
		{
			get { return ((string)(base.GetValue(CallActivity.INVITEE_LISTProperty))); }
			set { base.SetValue(CallActivity.INVITEE_LISTProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.TEAM_IDProperty))); }
			set { base.SetValue(CallActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(CallActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(CallActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty EMAIL_REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL_REMINDER_TIME", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 EMAIL_REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(CallActivity.EMAIL_REMINDER_TIMEProperty))); }
			set { base.SetValue(CallActivity.EMAIL_REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty ALL_DAY_EVENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALL_DAY_EVENT", typeof(bool), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool ALL_DAY_EVENT
		{
			get { return ((bool)(base.GetValue(CallActivity.ALL_DAY_EVENTProperty))); }
			set { base.SetValue(CallActivity.ALL_DAY_EVENTProperty, value); }
		}

		public static DependencyProperty REPEAT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_TYPE", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPEAT_TYPE
		{
			get { return ((string)(base.GetValue(CallActivity.REPEAT_TYPEProperty))); }
			set { base.SetValue(CallActivity.REPEAT_TYPEProperty, value); }
		}

		public static DependencyProperty REPEAT_INTERVALProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_INTERVAL", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 REPEAT_INTERVAL
		{
			get { return ((Int32)(base.GetValue(CallActivity.REPEAT_INTERVALProperty))); }
			set { base.SetValue(CallActivity.REPEAT_INTERVALProperty, value); }
		}

		public static DependencyProperty REPEAT_DOWProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_DOW", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPEAT_DOW
		{
			get { return ((string)(base.GetValue(CallActivity.REPEAT_DOWProperty))); }
			set { base.SetValue(CallActivity.REPEAT_DOWProperty, value); }
		}

		public static DependencyProperty REPEAT_UNTILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_UNTIL", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime REPEAT_UNTIL
		{
			get { return ((DateTime)(base.GetValue(CallActivity.REPEAT_UNTILProperty))); }
			set { base.SetValue(CallActivity.REPEAT_UNTILProperty, value); }
		}

		public static DependencyProperty REPEAT_COUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_COUNT", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 REPEAT_COUNT
		{
			get { return ((Int32)(base.GetValue(CallActivity.REPEAT_COUNTProperty))); }
			set { base.SetValue(CallActivity.REPEAT_COUNTProperty, value); }
		}

		public static DependencyProperty SMS_REMINDER_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SMS_REMINDER_TIME", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 SMS_REMINDER_TIME
		{
			get { return ((Int32)(base.GetValue(CallActivity.SMS_REMINDER_TIMEProperty))); }
			set { base.SetValue(CallActivity.SMS_REMINDER_TIMEProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(CallActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(CallActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(CallActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(CallActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(CallActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(CallActivity.CREATED_BYProperty))); }
			set { base.SetValue(CallActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(CallActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_END", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_END
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_ENDProperty))); }
			set { base.SetValue(CallActivity.DATE_ENDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(CallActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(CallActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(CallActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(CallActivity.DATE_STARTProperty))); }
			set { base.SetValue(CallActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(CallActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(CallActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty RECURRING_SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RECURRING_SOURCE", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RECURRING_SOURCE
		{
			get { return ((string)(base.GetValue(CallActivity.RECURRING_SOURCEProperty))); }
			set { base.SetValue(CallActivity.RECURRING_SOURCEProperty, value); }
		}

		public static DependencyProperty REPEAT_PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_PARENT_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid REPEAT_PARENT_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.REPEAT_PARENT_IDProperty))); }
			set { base.SetValue(CallActivity.REPEAT_PARENT_IDProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(CallActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(CallActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(CallActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(CallActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(CallActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(CallActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CallActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CallActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(CallActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(CallActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(CallActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty PHONE_WORKProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_WORK", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_WORK
		{
			get { return ((string)(base.GetValue(CallActivity.PHONE_WORKProperty))); }
			set { base.SetValue(CallActivity.PHONE_WORKProperty, value); }
		}

		public static DependencyProperty REPEAT_PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPEAT_PARENT_NAME", typeof(string), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPEAT_PARENT_NAME
		{
			get { return ((string)(base.GetValue(CallActivity.REPEAT_PARENT_NAMEProperty))); }
			set { base.SetValue(CallActivity.REPEAT_PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty SHOULD_REMINDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SHOULD_REMIND", typeof(Int32), typeof(CallActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 SHOULD_REMIND
		{
			get { return ((Int32)(base.GetValue(CallActivity.SHOULD_REMINDProperty))); }
			set { base.SetValue(CallActivity.SHOULD_REMINDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CallActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CallActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CALLS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCALLS_AUDIT        CALLS          " + ControlChars.CrLf
							                + " inner join vwCALLS_AUDIT        CALLS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CALLS_AUDIT_OLD.ID = CALLS.ID       " + ControlChars.CrLf
							                + "        and CALLS_AUDIT_OLD.AUDIT_VERSION = (select max(vwCALLS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwCALLS_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwCALLS_AUDIT.ID            =  CALLS.ID           " + ControlChars.CrLf
							                + "                                                and vwCALLS_AUDIT.AUDIT_VERSION <  CALLS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwCALLS_AUDIT.AUDIT_TOKEN   <> CALLS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where CALLS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwCALLS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCALLS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwCALLS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								DURATION_HOURS                 = Sql.ToInteger (rdr["DURATION_HOURS"                ]);
								DURATION_MINUTES               = Sql.ToInteger (rdr["DURATION_MINUTES"              ]);
								if ( !bPast )
									DATE_TIME                      = Sql.ToDateTime(rdr["DATE_TIME"                     ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								DIRECTION                      = Sql.ToString  (rdr["DIRECTION"                     ]);
								REMINDER_TIME                  = Sql.ToInteger (rdr["REMINDER_TIME"                 ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								EMAIL_REMINDER_TIME            = Sql.ToInteger (rdr["EMAIL_REMINDER_TIME"           ]);
								ALL_DAY_EVENT                  = Sql.ToBoolean (rdr["ALL_DAY_EVENT"                 ]);
								REPEAT_TYPE                    = Sql.ToString  (rdr["REPEAT_TYPE"                   ]);
								REPEAT_INTERVAL                = Sql.ToInteger (rdr["REPEAT_INTERVAL"               ]);
								REPEAT_DOW                     = Sql.ToString  (rdr["REPEAT_DOW"                    ]);
								REPEAT_UNTIL                   = Sql.ToDateTime(rdr["REPEAT_UNTIL"                  ]);
								REPEAT_COUNT                   = Sql.ToInteger (rdr["REPEAT_COUNT"                  ]);
								SMS_REMINDER_TIME              = Sql.ToInteger (rdr["SMS_REMINDER_TIME"             ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_END                       = Sql.ToDateTime(rdr["DATE_END"                      ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								RECURRING_SOURCE               = Sql.ToString  (rdr["RECURRING_SOURCE"              ]);
								REPEAT_PARENT_ID               = Sql.ToGuid    (rdr["REPEAT_PARENT_ID"              ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									PHONE_WORK                     = Sql.ToString  (rdr["PHONE_WORK"                    ]);
									REPEAT_PARENT_NAME             = Sql.ToString  (rdr["REPEAT_PARENT_NAME"            ]);
									SHOULD_REMIND                  = Sql.ToInteger (rdr["SHOULD_REMIND"                 ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CallActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CALLS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCALLS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, DURATION_HOURS
								, DURATION_MINUTES
								, DATE_TIME
								, PARENT_TYPE
								, PARENT_ID
								, STATUS
								, DIRECTION
								, REMINDER_TIME
								, DESCRIPTION
								, INVITEE_LIST
								, TEAM_ID
								, TEAM_SET_LIST
								, EMAIL_REMINDER_TIME
								, ALL_DAY_EVENT
								, REPEAT_TYPE
								, REPEAT_INTERVAL
								, REPEAT_DOW
								, REPEAT_UNTIL
								, REPEAT_COUNT
								, SMS_REMINDER_TIME
								, TAG_SET_NAME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CallActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

