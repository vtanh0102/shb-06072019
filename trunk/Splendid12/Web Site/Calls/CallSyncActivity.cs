/**
 * Copyright (C) 2012-2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	public class CallSyncActivity: SplendidActivity
	{
		public CallSyncActivity()
		{
			this.Name = "CallSyncActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CallSyncActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CallSyncActivity.IDProperty))); }
			set { base.SetValue(CallSyncActivity.IDProperty, value); }
		}
		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CallSyncActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CallSyncActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bAudit )
						{
							cmd.CommandText = "select ID from vwCALLS_AUDIT where AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
							ID = Sql.ToGuid(cmd.ExecuteScalar());
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CallSyncActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
		}

		// 11/07/2010 Paul.  All custom methods must have this signature. 
		public void CustomMethod(object sender, EventArgs e)
		{
			// 09/16/2015 Paul.  Change to Debug as it is automatically not included in a release build. 
			Debug.WriteLine("CallSyncActivity.CustomMethod " + AUDIT_ID.ToString());

			using ( DataTable dt = new DataTable() )
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL ;
					sSQL = "select *                       " + ControlChars.CrLf
					     + "  from vwCALLS_SYNC_ACTIVITY   " + ControlChars.CrLf
					     + " where ID in (select ID from vwCALLS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							da.Fill(dt);
						}
					}
				}
				bool   bGoogleAppsEnabled = Sql.ToBoolean(app.Application["CONFIG.GoogleApps.Enabled"]);
				bool   bICLOUDEnabled     = Sql.ToBoolean(app.Application["CONFIG.iCloud.Enabled"    ]);
				string sExchangeServerURL = Sql.ToString (app.Application["CONFIG.Exchange.ServerURL"]);
				foreach ( DataRow row in dt.Rows )
				{
					ID = Sql.ToGuid(row["ID"]);
					Guid   gUSER_ID                  = Sql.ToGuid   (row["USER_ID"                 ]);
					bool   bGOOGLEAPPS_SYNC_CALENDAR = Sql.ToBoolean(row["GOOGLEAPPS_SYNC_CALENDAR"]);
					string sGOOGLEAPPS_USERNAME      = Sql.ToString (row["GOOGLEAPPS_USERNAME"     ]);
					bool   bICLOUD_SYNC_CALENDAR     = Sql.ToBoolean(row["ICLOUD_SYNC_CALENDAR"    ]);
					string sICLOUD_USERNAME          = Sql.ToString (row["ICLOUD_USERNAME"         ]);
					// 09/16/2015 Paul.  Google APIs use OAUTH and not username/password. 
					bool   bGOOGLEAPPS_USER_ENABLED  = Sql.ToBoolean(row["GOOGLEAPPS_USER_ENABLED" ]);
					if ( bGoogleAppsEnabled && bGOOGLEAPPS_SYNC_CALENDAR && bGOOGLEAPPS_USER_ENABLED )
					{
						SyncError.SystemMessage(app.Context, "Warning", new StackTrace(true).GetFrame(0), "CallSyncActivity: GoogleSync, Call Changed " + ID.ToString() + " for User " + gUSER_ID.ToString());
						GoogleSync.SyncUser(app.Context, gUSER_ID);
					}
					if ( bICLOUDEnabled && bICLOUD_SYNC_CALENDAR && !Sql.IsEmptyString(sICLOUD_USERNAME) )
					{
						SyncError.SystemMessage(app.Context, "Warning", new StackTrace(true).GetFrame(0), "CallSyncActivity: iCloudSync, Call Changed " + ID.ToString() + " for User " + gUSER_ID.ToString());
						iCloudSync.SyncUser(app.Context, gUSER_ID);
					}
					if ( !Sql.IsEmptyString(sExchangeServerURL) )
					{
						SyncError.SystemMessage(app.Context, "Warning", new StackTrace(true).GetFrame(0), "CallSyncActivity: ExchangeSync, Call Changed " + ID.ToString() + " for User " + gUSER_ID.ToString());
						ExchangeSync.SyncUser(app.Context, gUSER_ID);
					}
				}
			}
		}
	}
}
