/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// CreditCardActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:28 PM
	/// </summary>
	public class CreditCardActivity: SplendidActivity
	{
		public CreditCardActivity()
		{
			this.Name = "CreditCardActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.IDProperty))); }
			set { base.SetValue(CreditCardActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(CreditCardActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(CreditCardActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.NAMEProperty, value); }
		}

		public static DependencyProperty CARD_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CARD_TYPE", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CARD_TYPE
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CARD_TYPEProperty))); }
			set { base.SetValue(CreditCardActivity.CARD_TYPEProperty, value); }
		}

		public static DependencyProperty CARD_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CARD_NUMBER", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CARD_NUMBER
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CARD_NUMBERProperty))); }
			set { base.SetValue(CreditCardActivity.CARD_NUMBERProperty, value); }
		}

		public static DependencyProperty CARD_NUMBER_DISPLAYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CARD_NUMBER_DISPLAY", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CARD_NUMBER_DISPLAY
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CARD_NUMBER_DISPLAYProperty))); }
			set { base.SetValue(CreditCardActivity.CARD_NUMBER_DISPLAYProperty, value); }
		}

		public static DependencyProperty SECURITY_CODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SECURITY_CODE", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SECURITY_CODE
		{
			get { return ((string)(base.GetValue(CreditCardActivity.SECURITY_CODEProperty))); }
			set { base.SetValue(CreditCardActivity.SECURITY_CODEProperty, value); }
		}

		public static DependencyProperty EXPIRATION_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPIRATION_DATE", typeof(DateTime), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime EXPIRATION_DATE
		{
			get { return ((DateTime)(base.GetValue(CreditCardActivity.EXPIRATION_DATEProperty))); }
			set { base.SetValue(CreditCardActivity.EXPIRATION_DATEProperty, value); }
		}

		public static DependencyProperty BANK_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BANK_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BANK_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.BANK_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.BANK_NAMEProperty, value); }
		}

		public static DependencyProperty BANK_ROUTING_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BANK_ROUTING_NUMBER", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BANK_ROUTING_NUMBER
		{
			get { return ((string)(base.GetValue(CreditCardActivity.BANK_ROUTING_NUMBERProperty))); }
			set { base.SetValue(CreditCardActivity.BANK_ROUTING_NUMBERProperty, value); }
		}

		public static DependencyProperty IS_PRIMARYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIMARY", typeof(bool), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIMARY
		{
			get { return ((bool)(base.GetValue(CreditCardActivity.IS_PRIMARYProperty))); }
			set { base.SetValue(CreditCardActivity.IS_PRIMARYProperty, value); }
		}

		public static DependencyProperty IS_ENCRYPTEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_ENCRYPTED", typeof(bool), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_ENCRYPTED
		{
			get { return ((bool)(base.GetValue(CreditCardActivity.IS_ENCRYPTEDProperty))); }
			set { base.SetValue(CreditCardActivity.IS_ENCRYPTEDProperty, value); }
		}

		public static DependencyProperty ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_STREET", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ADDRESS_STREETProperty))); }
			set { base.SetValue(CreditCardActivity.ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_CITY", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ADDRESS_CITYProperty))); }
			set { base.SetValue(CreditCardActivity.ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_STATE", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ADDRESS_STATEProperty))); }
			set { base.SetValue(CreditCardActivity.ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_POSTALCODE", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(CreditCardActivity.ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_COUNTRY", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(CreditCardActivity.ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.CONTACT_IDProperty))); }
			set { base.SetValue(CreditCardActivity.CONTACT_IDProperty, value); }
		}

		public static DependencyProperty CARD_TOKENProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CARD_TOKEN", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CARD_TOKEN
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CARD_TOKENProperty))); }
			set { base.SetValue(CreditCardActivity.CARD_TOKENProperty, value); }
		}

		public static DependencyProperty EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL
		{
			get { return ((string)(base.GetValue(CreditCardActivity.EMAILProperty))); }
			set { base.SetValue(CreditCardActivity.EMAILProperty, value); }
		}

		public static DependencyProperty PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE
		{
			get { return ((string)(base.GetValue(CreditCardActivity.PHONEProperty))); }
			set { base.SetValue(CreditCardActivity.PHONEProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CREATED_BYProperty))); }
			set { base.SetValue(CreditCardActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(CreditCardActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(CreditCardActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(CreditCardActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(CreditCardActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(CreditCardActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(CreditCardActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(CreditCardActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CreditCardActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(CreditCardActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CreditCardActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(CreditCardActivity.CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(CreditCardActivity.CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CONTACT_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty EXPIRATION_MONTHProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPIRATION_MONTH", typeof(Int32), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 EXPIRATION_MONTH
		{
			get { return ((Int32)(base.GetValue(CreditCardActivity.EXPIRATION_MONTHProperty))); }
			set { base.SetValue(CreditCardActivity.EXPIRATION_MONTHProperty, value); }
		}

		public static DependencyProperty EXPIRATION_YEARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXPIRATION_YEAR", typeof(Int32), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 EXPIRATION_YEAR
		{
			get { return ((Int32)(base.GetValue(CreditCardActivity.EXPIRATION_YEARProperty))); }
			set { base.SetValue(CreditCardActivity.EXPIRATION_YEARProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.TEAM_IDProperty))); }
			set { base.SetValue(CreditCardActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(CreditCardActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(CreditCardActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(CreditCardActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(CreditCardActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(CreditCardActivity.TEAM_SET_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("CreditCardActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("CreditCardActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select CREDIT_CARDS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwCREDIT_CARDS_AUDIT        CREDIT_CARDS          " + ControlChars.CrLf
							                + " inner join vwCREDIT_CARDS_AUDIT        CREDIT_CARDS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on CREDIT_CARDS_AUDIT_OLD.ID = CREDIT_CARDS.ID       " + ControlChars.CrLf
							                + "        and CREDIT_CARDS_AUDIT_OLD.AUDIT_VERSION = (select max(vwCREDIT_CARDS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                      from vwCREDIT_CARDS_AUDIT                   " + ControlChars.CrLf
							                + "                                                     where vwCREDIT_CARDS_AUDIT.ID            =  CREDIT_CARDS.ID           " + ControlChars.CrLf
							                + "                                                       and vwCREDIT_CARDS_AUDIT.AUDIT_VERSION <  CREDIT_CARDS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                       and vwCREDIT_CARDS_AUDIT.AUDIT_TOKEN   <> CREDIT_CARDS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                   )" + ControlChars.CrLf
							                + " where CREDIT_CARDS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwCREDIT_CARDS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwCREDIT_CARDS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwCREDIT_CARDS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ACCOUNT_ID                     = Sql.ToGuid    (rdr["ACCOUNT_ID"                    ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								CARD_TYPE                      = Sql.ToString  (rdr["CARD_TYPE"                     ]);
								CARD_NUMBER                    = Sql.ToString  (rdr["CARD_NUMBER"                   ]);
								CARD_NUMBER_DISPLAY            = Sql.ToString  (rdr["CARD_NUMBER_DISPLAY"           ]);
								SECURITY_CODE                  = Sql.ToString  (rdr["SECURITY_CODE"                 ]);
								EXPIRATION_DATE                = Sql.ToDateTime(rdr["EXPIRATION_DATE"               ]);
								BANK_NAME                      = Sql.ToString  (rdr["BANK_NAME"                     ]);
								BANK_ROUTING_NUMBER            = Sql.ToString  (rdr["BANK_ROUTING_NUMBER"           ]);
								IS_PRIMARY                     = Sql.ToBoolean (rdr["IS_PRIMARY"                    ]);
								IS_ENCRYPTED                   = Sql.ToBoolean (rdr["IS_ENCRYPTED"                  ]);
								ADDRESS_STREET                 = Sql.ToString  (rdr["ADDRESS_STREET"                ]);
								ADDRESS_CITY                   = Sql.ToString  (rdr["ADDRESS_CITY"                  ]);
								ADDRESS_STATE                  = Sql.ToString  (rdr["ADDRESS_STATE"                 ]);
								ADDRESS_POSTALCODE             = Sql.ToString  (rdr["ADDRESS_POSTALCODE"            ]);
								ADDRESS_COUNTRY                = Sql.ToString  (rdr["ADDRESS_COUNTRY"               ]);
								CONTACT_ID                     = Sql.ToGuid    (rdr["CONTACT_ID"                    ]);
								CARD_TOKEN                     = Sql.ToString  (rdr["CARD_TOKEN"                    ]);
								EMAIL                          = Sql.ToString  (rdr["EMAIL"                         ]);
								PHONE                          = Sql.ToString  (rdr["PHONE"                         ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								if ( !bPast )
								{
									ACCOUNT_NAME                   = Sql.ToString  (rdr["ACCOUNT_NAME"                  ]);
									ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
									ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
									CONTACT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_SET_ID"       ]);
									CONTACT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_USER_ID"      ]);
									CONTACT_NAME                   = Sql.ToString  (rdr["CONTACT_NAME"                  ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									EXPIRATION_MONTH               = Sql.ToInteger (rdr["EXPIRATION_MONTH"              ]);
									EXPIRATION_YEAR                = Sql.ToInteger (rdr["EXPIRATION_YEAR"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
									TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
									TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
									TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CreditCardActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("CREDIT_CARDS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spCREDIT_CARDS_Update
								( ref gID
								, ACCOUNT_ID
								, NAME
								, CARD_TYPE
								, CARD_NUMBER
								, CARD_NUMBER_DISPLAY
								, SECURITY_CODE
								, EXPIRATION_DATE
								, BANK_NAME
								, BANK_ROUTING_NUMBER
								, IS_PRIMARY
								, IS_ENCRYPTED
								, ADDRESS_STREET
								, ADDRESS_CITY
								, ADDRESS_STATE
								, ADDRESS_POSTALCODE
								, ADDRESS_COUNTRY
								, CONTACT_ID
								, CARD_TOKEN
								, EMAIL
								, PHONE
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("CreditCardActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

