/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// LeadActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:29 PM
	/// </summary>
	public class LeadActivity: SplendidActivity
	{
		public LeadActivity()
		{
			this.Name = "LeadActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.IDProperty))); }
			set { base.SetValue(LeadActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty SALUTATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SALUTATION", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SALUTATION
		{
			get { return ((string)(base.GetValue(LeadActivity.SALUTATIONProperty))); }
			set { base.SetValue(LeadActivity.SALUTATIONProperty, value); }
		}

		public static DependencyProperty FIRST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FIRST_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FIRST_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.FIRST_NAMEProperty))); }
			set { base.SetValue(LeadActivity.FIRST_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LAST_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.LAST_NAMEProperty))); }
			set { base.SetValue(LeadActivity.LAST_NAMEProperty, value); }
		}

		public static DependencyProperty TITLEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TITLE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TITLE
		{
			get { return ((string)(base.GetValue(LeadActivity.TITLEProperty))); }
			set { base.SetValue(LeadActivity.TITLEProperty, value); }
		}

		public static DependencyProperty REFERED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REFERED_BY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REFERED_BY
		{
			get { return ((string)(base.GetValue(LeadActivity.REFERED_BYProperty))); }
			set { base.SetValue(LeadActivity.REFERED_BYProperty, value); }
		}

		public static DependencyProperty LEAD_SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_SOURCE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_SOURCE
		{
			get { return ((string)(base.GetValue(LeadActivity.LEAD_SOURCEProperty))); }
			set { base.SetValue(LeadActivity.LEAD_SOURCEProperty, value); }
		}

		public static DependencyProperty LEAD_SOURCE_DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_SOURCE_DESCRIPTION", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_SOURCE_DESCRIPTION
		{
			get { return ((string)(base.GetValue(LeadActivity.LEAD_SOURCE_DESCRIPTIONProperty))); }
			set { base.SetValue(LeadActivity.LEAD_SOURCE_DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(LeadActivity.STATUSProperty))); }
			set { base.SetValue(LeadActivity.STATUSProperty, value); }
		}

		public static DependencyProperty STATUS_DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS_DESCRIPTION", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS_DESCRIPTION
		{
			get { return ((string)(base.GetValue(LeadActivity.STATUS_DESCRIPTIONProperty))); }
			set { base.SetValue(LeadActivity.STATUS_DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty DEPARTMENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEPARTMENT", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DEPARTMENT
		{
			get { return ((string)(base.GetValue(LeadActivity.DEPARTMENTProperty))); }
			set { base.SetValue(LeadActivity.DEPARTMENTProperty, value); }
		}

		public static DependencyProperty REPORTS_TO_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORTS_TO_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid REPORTS_TO_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.REPORTS_TO_IDProperty))); }
			set { base.SetValue(LeadActivity.REPORTS_TO_IDProperty, value); }
		}

		public static DependencyProperty DO_NOT_CALLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DO_NOT_CALL", typeof(bool), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool DO_NOT_CALL
		{
			get { return ((bool)(base.GetValue(LeadActivity.DO_NOT_CALLProperty))); }
			set { base.SetValue(LeadActivity.DO_NOT_CALLProperty, value); }
		}

		public static DependencyProperty PHONE_HOMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_HOME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_HOME
		{
			get { return ((string)(base.GetValue(LeadActivity.PHONE_HOMEProperty))); }
			set { base.SetValue(LeadActivity.PHONE_HOMEProperty, value); }
		}

		public static DependencyProperty PHONE_MOBILEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_MOBILE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_MOBILE
		{
			get { return ((string)(base.GetValue(LeadActivity.PHONE_MOBILEProperty))); }
			set { base.SetValue(LeadActivity.PHONE_MOBILEProperty, value); }
		}

		public static DependencyProperty PHONE_WORKProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_WORK", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_WORK
		{
			get { return ((string)(base.GetValue(LeadActivity.PHONE_WORKProperty))); }
			set { base.SetValue(LeadActivity.PHONE_WORKProperty, value); }
		}

		public static DependencyProperty PHONE_OTHERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_OTHER", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_OTHER
		{
			get { return ((string)(base.GetValue(LeadActivity.PHONE_OTHERProperty))); }
			set { base.SetValue(LeadActivity.PHONE_OTHERProperty, value); }
		}

		public static DependencyProperty PHONE_FAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_FAX", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_FAX
		{
			get { return ((string)(base.GetValue(LeadActivity.PHONE_FAXProperty))); }
			set { base.SetValue(LeadActivity.PHONE_FAXProperty, value); }
		}

		public static DependencyProperty EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL1", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL1
		{
			get { return ((string)(base.GetValue(LeadActivity.EMAIL1Property))); }
			set { base.SetValue(LeadActivity.EMAIL1Property, value); }
		}

		public static DependencyProperty EMAIL2Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL2", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL2
		{
			get { return ((string)(base.GetValue(LeadActivity.EMAIL2Property))); }
			set { base.SetValue(LeadActivity.EMAIL2Property, value); }
		}

		public static DependencyProperty EMAIL_OPT_OUTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL_OPT_OUT", typeof(bool), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EMAIL_OPT_OUT
		{
			get { return ((bool)(base.GetValue(LeadActivity.EMAIL_OPT_OUTProperty))); }
			set { base.SetValue(LeadActivity.EMAIL_OPT_OUTProperty, value); }
		}

		public static DependencyProperty INVALID_EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("INVALID_EMAIL", typeof(bool), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool INVALID_EMAIL
		{
			get { return ((bool)(base.GetValue(LeadActivity.INVALID_EMAILProperty))); }
			set { base.SetValue(LeadActivity.INVALID_EMAILProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_STREET", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_STREETProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_CITY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_CITYProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_STATE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_STATEProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_POSTALCODE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_COUNTRY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_STREET", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_STREETProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_CITY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_CITYProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_STATE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_STATEProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_POSTALCODE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_COUNTRY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(LeadActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(LeadActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.ACCOUNT_NAMEProperty))); }
			set { base.SetValue(LeadActivity.ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CAMPAIGN_IDProperty))); }
			set { base.SetValue(LeadActivity.CAMPAIGN_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.TEAM_IDProperty))); }
			set { base.SetValue(LeadActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(LeadActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(LeadActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONTACT_IDProperty))); }
			set { base.SetValue(LeadActivity.CONTACT_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.ACCOUNT_IDProperty))); }
			set { base.SetValue(LeadActivity.ACCOUNT_IDProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(LeadActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(LeadActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty BIRTHDATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BIRTHDATE", typeof(DateTime), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime BIRTHDATE
		{
			get { return ((DateTime)(base.GetValue(LeadActivity.BIRTHDATEProperty))); }
			set { base.SetValue(LeadActivity.BIRTHDATEProperty, value); }
		}

		public static DependencyProperty ASSISTANTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSISTANT", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSISTANT
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSISTANTProperty))); }
			set { base.SetValue(LeadActivity.ASSISTANTProperty, value); }
		}

		public static DependencyProperty ASSISTANT_PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSISTANT_PHONE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSISTANT_PHONE
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSISTANT_PHONEProperty))); }
			set { base.SetValue(LeadActivity.ASSISTANT_PHONEProperty, value); }
		}

		public static DependencyProperty WEBSITEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WEBSITE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string WEBSITE
		{
			get { return ((string)(base.GetValue(LeadActivity.WEBSITEProperty))); }
			set { base.SetValue(LeadActivity.WEBSITEProperty, value); }
		}

		public static DependencyProperty SMS_OPT_INProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SMS_OPT_IN", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SMS_OPT_IN
		{
			get { return ((string)(base.GetValue(LeadActivity.SMS_OPT_INProperty))); }
			set { base.SetValue(LeadActivity.SMS_OPT_INProperty, value); }
		}

		public static DependencyProperty TWITTER_SCREEN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TWITTER_SCREEN_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TWITTER_SCREEN_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.TWITTER_SCREEN_NAMEProperty))); }
			set { base.SetValue(LeadActivity.TWITTER_SCREEN_NAMEProperty, value); }
		}

		public static DependencyProperty PICTUREProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PICTURE", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PICTURE
		{
			get { return ((string)(base.GetValue(LeadActivity.PICTUREProperty))); }
			set { base.SetValue(LeadActivity.PICTUREProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(LeadActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty LEAD_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LEAD_NUMBER", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LEAD_NUMBER
		{
			get { return ((string)(base.GetValue(LeadActivity.LEAD_NUMBERProperty))); }
			set { base.SetValue(LeadActivity.LEAD_NUMBERProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ACCOUNT_DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_DESCRIPTION", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ACCOUNT_DESCRIPTION
		{
			get { return ((string)(base.GetValue(LeadActivity.ACCOUNT_DESCRIPTIONProperty))); }
			set { base.SetValue(LeadActivity.ACCOUNT_DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(LeadActivity.CREATED_BYProperty))); }
			set { base.SetValue(LeadActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(LeadActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(LeadActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(LeadActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(LeadActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(LeadActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(LeadActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(LeadActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(LeadActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(LeadActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.NAMEProperty))); }
			set { base.SetValue(LeadActivity.NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(LeadActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(LeadActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.ACCOUNT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.ACCOUNT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ACCOUNT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACCOUNT_ASSIGNED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ACCOUNT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.ACCOUNT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.ACCOUNT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty ALT_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALT_ADDRESS_HTML", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALT_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(LeadActivity.ALT_ADDRESS_HTMLProperty))); }
			set { base.SetValue(LeadActivity.ALT_ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(LeadActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ASSIGNED_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CAMPAIGN_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.CAMPAIGN_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_ASSIGNED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CAMPAIGN_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CAMPAIGN_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.CAMPAIGN_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CAMPAIGN_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CAMPAIGN_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CAMPAIGN_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.CAMPAIGN_NAMEProperty))); }
			set { base.SetValue(LeadActivity.CAMPAIGN_NAMEProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONVERTED_ACCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_ACCOUNT_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONVERTED_ACCOUNT_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.CONVERTED_ACCOUNT_NAMEProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_ACCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty CONVERTED_CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_CONTACT_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONVERTED_CONTACT_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.CONVERTED_CONTACT_NAMEProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CONVERTED_OPPORTUNITY_AMOUNTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_OPPORTUNITY_AMOUNT", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONVERTED_OPPORTUNITY_AMOUNT
		{
			get { return ((string)(base.GetValue(LeadActivity.CONVERTED_OPPORTUNITY_AMOUNTProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_OPPORTUNITY_AMOUNTProperty, value); }
		}

		public static DependencyProperty CONVERTED_OPPORTUNITY_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_OPPORTUNITY_ASSIGNED_SET_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONVERTED_OPPORTUNITY_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONVERTED_OPPORTUNITY_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_OPPORTUNITY_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONVERTED_OPPORTUNITY_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_OPPORTUNITY_ASSIGNED_USER_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONVERTED_OPPORTUNITY_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONVERTED_OPPORTUNITY_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_OPPORTUNITY_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONVERTED_OPPORTUNITY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_OPPORTUNITY_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONVERTED_OPPORTUNITY_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.CONVERTED_OPPORTUNITY_IDProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_OPPORTUNITY_IDProperty, value); }
		}

		public static DependencyProperty CONVERTED_OPPORTUNITY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONVERTED_OPPORTUNITY_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONVERTED_OPPORTUNITY_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.CONVERTED_OPPORTUNITY_NAMEProperty))); }
			set { base.SetValue(LeadActivity.CONVERTED_OPPORTUNITY_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(LeadActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(LeadActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(LeadActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(LeadActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(LeadActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(LeadActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(LeadActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty PRIMARY_ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ADDRESS_HTML", typeof(string), typeof(LeadActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(LeadActivity.PRIMARY_ADDRESS_HTMLProperty))); }
			set { base.SetValue(LeadActivity.PRIMARY_ADDRESS_HTMLProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("LeadActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("LeadActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select LEADS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwLEADS_AUDIT        LEADS          " + ControlChars.CrLf
							                + " inner join vwLEADS_AUDIT        LEADS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on LEADS_AUDIT_OLD.ID = LEADS.ID       " + ControlChars.CrLf
							                + "        and LEADS_AUDIT_OLD.AUDIT_VERSION = (select max(vwLEADS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwLEADS_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwLEADS_AUDIT.ID            =  LEADS.ID           " + ControlChars.CrLf
							                + "                                                and vwLEADS_AUDIT.AUDIT_VERSION <  LEADS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwLEADS_AUDIT.AUDIT_TOKEN   <> LEADS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where LEADS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwLEADS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwLEADS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwLEADS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                                     = Sql.ToGuid    (rdr["ID"                                    ]);
								MODIFIED_USER_ID                       = Sql.ToGuid    (rdr["MODIFIED_USER_ID"                      ]);
								ASSIGNED_USER_ID                       = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"                      ]);
								SALUTATION                             = Sql.ToString  (rdr["SALUTATION"                            ]);
								FIRST_NAME                             = Sql.ToString  (rdr["FIRST_NAME"                            ]);
								LAST_NAME                              = Sql.ToString  (rdr["LAST_NAME"                             ]);
								TITLE                                  = Sql.ToString  (rdr["TITLE"                                 ]);
								REFERED_BY                             = Sql.ToString  (rdr["REFERED_BY"                            ]);
								LEAD_SOURCE                            = Sql.ToString  (rdr["LEAD_SOURCE"                           ]);
								LEAD_SOURCE_DESCRIPTION                = Sql.ToString  (rdr["LEAD_SOURCE_DESCRIPTION"               ]);
								STATUS                                 = Sql.ToString  (rdr["STATUS"                                ]);
								STATUS_DESCRIPTION                     = Sql.ToString  (rdr["STATUS_DESCRIPTION"                    ]);
								DEPARTMENT                             = Sql.ToString  (rdr["DEPARTMENT"                            ]);
								REPORTS_TO_ID                          = Sql.ToGuid    (rdr["REPORTS_TO_ID"                         ]);
								DO_NOT_CALL                            = Sql.ToBoolean (rdr["DO_NOT_CALL"                           ]);
								PHONE_HOME                             = Sql.ToString  (rdr["PHONE_HOME"                            ]);
								PHONE_MOBILE                           = Sql.ToString  (rdr["PHONE_MOBILE"                          ]);
								PHONE_WORK                             = Sql.ToString  (rdr["PHONE_WORK"                            ]);
								PHONE_OTHER                            = Sql.ToString  (rdr["PHONE_OTHER"                           ]);
								PHONE_FAX                              = Sql.ToString  (rdr["PHONE_FAX"                             ]);
								EMAIL1                                 = Sql.ToString  (rdr["EMAIL1"                                ]);
								EMAIL2                                 = Sql.ToString  (rdr["EMAIL2"                                ]);
								EMAIL_OPT_OUT                          = Sql.ToBoolean (rdr["EMAIL_OPT_OUT"                         ]);
								INVALID_EMAIL                          = Sql.ToBoolean (rdr["INVALID_EMAIL"                         ]);
								PRIMARY_ADDRESS_STREET                 = Sql.ToString  (rdr["PRIMARY_ADDRESS_STREET"                ]);
								PRIMARY_ADDRESS_CITY                   = Sql.ToString  (rdr["PRIMARY_ADDRESS_CITY"                  ]);
								PRIMARY_ADDRESS_STATE                  = Sql.ToString  (rdr["PRIMARY_ADDRESS_STATE"                 ]);
								PRIMARY_ADDRESS_POSTALCODE             = Sql.ToString  (rdr["PRIMARY_ADDRESS_POSTALCODE"            ]);
								PRIMARY_ADDRESS_COUNTRY                = Sql.ToString  (rdr["PRIMARY_ADDRESS_COUNTRY"               ]);
								ALT_ADDRESS_STREET                     = Sql.ToString  (rdr["ALT_ADDRESS_STREET"                    ]);
								ALT_ADDRESS_CITY                       = Sql.ToString  (rdr["ALT_ADDRESS_CITY"                      ]);
								ALT_ADDRESS_STATE                      = Sql.ToString  (rdr["ALT_ADDRESS_STATE"                     ]);
								ALT_ADDRESS_POSTALCODE                 = Sql.ToString  (rdr["ALT_ADDRESS_POSTALCODE"                ]);
								ALT_ADDRESS_COUNTRY                    = Sql.ToString  (rdr["ALT_ADDRESS_COUNTRY"                   ]);
								DESCRIPTION                            = Sql.ToString  (rdr["DESCRIPTION"                           ]);
								ACCOUNT_NAME                           = Sql.ToString  (rdr["ACCOUNT_NAME"                          ]);
								CAMPAIGN_ID                            = Sql.ToGuid    (rdr["CAMPAIGN_ID"                           ]);
								TEAM_ID                                = Sql.ToGuid    (rdr["TEAM_ID"                               ]);
								TEAM_SET_LIST                          = Sql.ToString  (rdr["TEAM_SET_LIST"                         ]);
								CONTACT_ID                             = Sql.ToGuid    (rdr["CONTACT_ID"                            ]);
								ACCOUNT_ID                             = Sql.ToGuid    (rdr["ACCOUNT_ID"                            ]);
								BIRTHDATE                              = Sql.ToDateTime(rdr["BIRTHDATE"                             ]);
								ASSISTANT                              = Sql.ToString  (rdr["ASSISTANT"                             ]);
								ASSISTANT_PHONE                        = Sql.ToString  (rdr["ASSISTANT_PHONE"                       ]);
								WEBSITE                                = Sql.ToString  (rdr["WEBSITE"                               ]);
								SMS_OPT_IN                             = Sql.ToString  (rdr["SMS_OPT_IN"                            ]);
								TWITTER_SCREEN_NAME                    = Sql.ToString  (rdr["TWITTER_SCREEN_NAME"                   ]);
								PICTURE                                = Sql.ToString  (rdr["PICTURE"                               ]);
								if ( !bPast )
									TAG_SET_NAME                           = Sql.ToString  (rdr["TAG_SET_NAME"                          ]);
								LEAD_NUMBER                            = Sql.ToString  (rdr["LEAD_NUMBER"                           ]);
								ASSIGNED_SET_LIST                      = Sql.ToString  (rdr["ASSIGNED_SET_LIST"                     ]);
								ACCOUNT_DESCRIPTION                    = Sql.ToString  (rdr["ACCOUNT_DESCRIPTION"                   ]);
								ASSIGNED_SET_ID                        = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"                       ]);
								ASSIGNED_SET_NAME                      = Sql.ToString  (rdr["ASSIGNED_SET_NAME"                     ]);
								ASSIGNED_TO                            = Sql.ToString  (rdr["ASSIGNED_TO"                           ]);
								CREATED_BY                             = Sql.ToString  (rdr["CREATED_BY"                            ]);
								CREATED_BY_ID                          = Sql.ToGuid    (rdr["CREATED_BY_ID"                         ]);
								DATE_ENTERED                           = Sql.ToDateTime(rdr["DATE_ENTERED"                          ]);
								DATE_MODIFIED                          = Sql.ToDateTime(rdr["DATE_MODIFIED"                         ]);
								DATE_MODIFIED_UTC                      = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"                     ]);
								MODIFIED_BY                            = Sql.ToString  (rdr["MODIFIED_BY"                           ]);
								NAME                                   = Sql.ToString  (rdr["NAME"                                  ]);
								TEAM_NAME                              = Sql.ToString  (rdr["TEAM_NAME"                             ]);
								TEAM_SET_ID                            = Sql.ToGuid    (rdr["TEAM_SET_ID"                           ]);
								TEAM_SET_NAME                          = Sql.ToString  (rdr["TEAM_SET_NAME"                         ]);
								if ( !bPast )
								{
									ACCOUNT_ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_SET_ID"               ]);
									ACCOUNT_ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ACCOUNT_ASSIGNED_USER_ID"              ]);
									ALT_ADDRESS_HTML                       = Sql.ToString  (rdr["ALT_ADDRESS_HTML"                      ]);
									ASSIGNED_TO_NAME                       = Sql.ToString  (rdr["ASSIGNED_TO_NAME"                      ]);
									CAMPAIGN_ASSIGNED_SET_ID               = Sql.ToGuid    (rdr["CAMPAIGN_ASSIGNED_SET_ID"              ]);
									CAMPAIGN_ASSIGNED_USER_ID              = Sql.ToGuid    (rdr["CAMPAIGN_ASSIGNED_USER_ID"             ]);
									CAMPAIGN_NAME                          = Sql.ToString  (rdr["CAMPAIGN_NAME"                         ]);
									CONTACT_ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_SET_ID"               ]);
									CONTACT_ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_USER_ID"              ]);
									CONVERTED_ACCOUNT_NAME                 = Sql.ToString  (rdr["CONVERTED_ACCOUNT_NAME"                ]);
									CONVERTED_CONTACT_NAME                 = Sql.ToString  (rdr["CONVERTED_CONTACT_NAME"                ]);
									CONVERTED_OPPORTUNITY_AMOUNT           = Sql.ToString  (rdr["CONVERTED_OPPORTUNITY_AMOUNT"          ]);
									CONVERTED_OPPORTUNITY_ASSIGNED_SET_ID  = Sql.ToGuid    (rdr["CONVERTED_OPPORTUNITY_ASSIGNED_SET_ID" ]);
									CONVERTED_OPPORTUNITY_ASSIGNED_USER_ID = Sql.ToGuid    (rdr["CONVERTED_OPPORTUNITY_ASSIGNED_USER_ID"]);
									CONVERTED_OPPORTUNITY_ID               = Sql.ToGuid    (rdr["CONVERTED_OPPORTUNITY_ID"              ]);
									CONVERTED_OPPORTUNITY_NAME             = Sql.ToString  (rdr["CONVERTED_OPPORTUNITY_NAME"            ]);
									CREATED_BY_NAME                        = Sql.ToString  (rdr["CREATED_BY_NAME"                       ]);
									LAST_ACTIVITY_DATE                     = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"                    ]);
									MODIFIED_BY_NAME                       = Sql.ToString  (rdr["MODIFIED_BY_NAME"                      ]);
									PENDING_PROCESS_ID                     = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"                    ]);
									PRIMARY_ADDRESS_HTML                   = Sql.ToString  (rdr["PRIMARY_ADDRESS_HTML"                  ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("LeadActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("LEADS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spLEADS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, SALUTATION
								, FIRST_NAME
								, LAST_NAME
								, TITLE
								, REFERED_BY
								, LEAD_SOURCE
								, LEAD_SOURCE_DESCRIPTION
								, STATUS
								, STATUS_DESCRIPTION
								, DEPARTMENT
								, REPORTS_TO_ID
								, DO_NOT_CALL
								, PHONE_HOME
								, PHONE_MOBILE
								, PHONE_WORK
								, PHONE_OTHER
								, PHONE_FAX
								, EMAIL1
								, EMAIL2
								, EMAIL_OPT_OUT
								, INVALID_EMAIL
								, PRIMARY_ADDRESS_STREET
								, PRIMARY_ADDRESS_CITY
								, PRIMARY_ADDRESS_STATE
								, PRIMARY_ADDRESS_POSTALCODE
								, PRIMARY_ADDRESS_COUNTRY
								, ALT_ADDRESS_STREET
								, ALT_ADDRESS_CITY
								, ALT_ADDRESS_STATE
								, ALT_ADDRESS_POSTALCODE
								, ALT_ADDRESS_COUNTRY
								, DESCRIPTION
								, ACCOUNT_NAME
								, CAMPAIGN_ID
								, TEAM_ID
								, TEAM_SET_LIST
								, CONTACT_ID
								, ACCOUNT_ID
								, EXCHANGE_FOLDER
								, BIRTHDATE
								, ASSISTANT
								, ASSISTANT_PHONE
								, WEBSITE
								, SMS_OPT_IN
								, TWITTER_SCREEN_NAME
								, PICTURE
								, TAG_SET_NAME
								, LEAD_NUMBER
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("LeadActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

