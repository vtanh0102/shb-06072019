/**
 * Copyright (C) 2005-2018 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Audit
{
    /// <summary>
    /// Summary description for Popup.
    /// </summary>
    public class Popup : SplendidPopup
    {
        protected Guid gID;
        protected string sModule;
        protected DataView vwMain;
        protected SplendidGrid grdMain;
        protected Label lblTitle;
        protected Label lblError;

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Search")
                {
                    // 10/13/2005 Paul.  Make sure to clear the page index prior to applying search. 
                    grdMain.CurrentPageIndex = 0;
                    grdMain.ApplySort();
                    grdMain.DataBind();
                }
                // 12/14/2007 Paul.  We need to capture the sort event from the SearchView. 
                else if (e.CommandName == "SortGrid")
                {
                    grdMain.SetSortFields(e.CommandArgument as string[]);
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        // 02/05/2018 Paul.  Provide a way to convert ID to NAME for custom fields. 
        private DataTable BuildChangesTable(DataTable dtAudit, DataTable dtLayoutFields)
        {
            DataTable dtChanges = new DataTable();
            DataColumn colFIELD_NAME = new DataColumn("FIELD_NAME", typeof(System.String));
            DataColumn colBEFORE_VALUE = new DataColumn("BEFORE_VALUE", typeof(System.String));
            DataColumn colAFTER_VALUE = new DataColumn("AFTER_VALUE", typeof(System.String));
            DataColumn colCREATED_BY = new DataColumn("CREATED_BY", typeof(System.String));
            DataColumn colDATE_CREATED = new DataColumn("DATE_CREATED", typeof(System.DateTime));
            dtChanges.Columns.Add(colFIELD_NAME);
            dtChanges.Columns.Add(colBEFORE_VALUE);
            dtChanges.Columns.Add(colAFTER_VALUE);
            dtChanges.Columns.Add(colCREATED_BY);
            dtChanges.Columns.Add(colDATE_CREATED);
            if (dtAudit.Rows.Count > 0)
            {
                StringDictionary dict = new StringDictionary();
                dict.Add("AUDIT_ACTION", String.Empty);
                dict.Add("AUDIT_DATE", String.Empty);
                dict.Add("AUDIT_COLUMNS", String.Empty);
                dict.Add("CSTM_AUDIT_COLUMNS", String.Empty);
                dict.Add("ID", String.Empty);
                dict.Add("ID_C", String.Empty);
                dict.Add("DELETED", String.Empty);
                dict.Add("CREATED_BY", String.Empty);
                dict.Add("DATE_ENTERED", String.Empty);
                dict.Add("MODIFIED_USER_ID", String.Empty);
                dict.Add("DATE_MODIFIED", String.Empty);
                // 09/17/2009 Paul.  No need to audit the UTC date. 
                dict.Add("DATE_MODIFIED_UTC", String.Empty);

                DataView vwLayoutFields = new DataView(dtLayoutFields);
                DataRow rowLast = dtAudit.Rows[0];
                for (int i = 1; i < dtAudit.Rows.Count; i++)
                {
                    DataRow row = dtAudit.Rows[i];
                    foreach (DataColumn col in row.Table.Columns)
                    {
                        if (!dict.ContainsKey(col.ColumnName))
                        {
                            if (Sql.ToString(rowLast[col.ColumnName]) != Sql.ToString(row[col.ColumnName]))
                            {
                                DataRow rowChange = dtChanges.NewRow();
                                dtChanges.Rows.Add(rowChange);
                                // 09/16/2009 Paul.  Localize the field name. 
                                rowChange["FIELD_NAME"] = Utils.TableColumnName(L10n, sModule, col.ColumnName);
                                rowChange["CREATED_BY"] = SplendidCache.AssignedUser(Sql.ToGuid(row["MODIFIED_USER_ID"]));
                                // 06/15/2009 Van.  The change date was not being converted to the time zone of the current user. 
                                rowChange["DATE_CREATED"] = Sql.ToDateTime(row["AUDIT_DATE"]);
                                rowChange["BEFORE_VALUE"] = rowLast[col.ColumnName];
                                rowChange["AFTER_VALUE"] = row[col.ColumnName];
                                // 09/05/2016 Paul.  Convert the Guid to a display name. 
                                if (col.ColumnName.EndsWith("_ID"))
                                {
                                    string sDATA_FIELD = col.ColumnName;
                                    string sTABLE_NAME = sDATA_FIELD.Substring(0, sDATA_FIELD.Length - 3);
                                    if (sDATA_FIELD == "ASSIGNED_USER_ID")
                                        sTABLE_NAME = "USERS";
                                    else if (sTABLE_NAME.EndsWith("Y"))
                                        sTABLE_NAME = sDATA_FIELD.Substring(0, sDATA_FIELD.Length - 4) + "IES";
                                    else if (sTABLE_NAME != "PROJECT" && sTABLE_NAME != "PROJECT_TASK")
                                        sTABLE_NAME += "S";
                                    string sMODULE_NAME = Crm.Modules.ModuleName(sTABLE_NAME);
                                    if (sTABLE_NAME == "TEAM_SETS")
                                        sMODULE_NAME = "TeamSets";
                                    rowChange["BEFORE_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_NAME, Sql.ToGuid(rowLast[col.ColumnName]));
                                    rowChange["AFTER_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_NAME, Sql.ToGuid(row[col.ColumnName]));
                                }
                                // 02/05/2018 Paul.  Provide a way to convert ID to NAME for custom fields. 
                                else if (col.ColumnName.EndsWith("_ID_C"))
                                {
                                    vwLayoutFields.RowFilter = "DATA_FIELD = '" + col.ColumnName + " '";
                                    if (vwLayoutFields.Count > 0)
                                    {
                                        string sMODULE_TYPE = Sql.ToString(vwLayoutFields[0]["MODULE_TYPE"]);
                                        if (!Sql.IsEmptyString(sMODULE_TYPE))
                                        {
                                            rowChange["BEFORE_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_TYPE, Sql.ToGuid(rowLast[col.ColumnName]));
                                            rowChange["AFTER_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_TYPE, Sql.ToGuid(row[col.ColumnName]));
                                        }
                                    }
                                }
                                // 09/15/2014 Paul.  Prevent Cross-Site Scripting by HTML encoding the data. 
                                if (rowChange["BEFORE_VALUE"] != DBNull.Value)
                                {
                                    if (rowChange["BEFORE_VALUE"].GetType() == typeof(System.String))
                                        rowChange["BEFORE_VALUE"] = HttpUtility.HtmlEncode(Sql.ToString(rowChange["BEFORE_VALUE"]));
                                }
                                if (rowChange["AFTER_VALUE"] != DBNull.Value)
                                {
                                    if (rowChange["AFTER_VALUE"].GetType() == typeof(System.String))
                                        rowChange["AFTER_VALUE"] = HttpUtility.HtmlEncode(Sql.ToString(rowChange["AFTER_VALUE"]));
                                }
                            }
                        }
                    }
                    rowLast = row;
                }
            }
            return dtChanges;
        }

        private DataTable BuildChangesTableChild(DataTable dtAudit, DataTable dtLayoutFields, string sModuleChild)
        {
            DataTable dtChanges = new DataTable();
            DataColumn colFIELD_NAME = new DataColumn("FIELD_NAME", typeof(System.String));
            DataColumn colBEFORE_VALUE = new DataColumn("BEFORE_VALUE", typeof(System.String));
            DataColumn colAFTER_VALUE = new DataColumn("AFTER_VALUE", typeof(System.String));
            DataColumn colCREATED_BY = new DataColumn("CREATED_BY", typeof(System.String));
            DataColumn colDATE_CREATED = new DataColumn("DATE_CREATED", typeof(System.DateTime));
            dtChanges.Columns.Add(colFIELD_NAME);
            dtChanges.Columns.Add(colBEFORE_VALUE);
            dtChanges.Columns.Add(colAFTER_VALUE);
            dtChanges.Columns.Add(colCREATED_BY);
            dtChanges.Columns.Add(colDATE_CREATED);
            if (dtAudit.Rows.Count > 0)
            {
                StringDictionary dict = new StringDictionary();
                dict.Add("AUDIT_ACTION", String.Empty);
                dict.Add("AUDIT_DATE", String.Empty);
                dict.Add("AUDIT_COLUMNS", String.Empty);
                dict.Add("CSTM_AUDIT_COLUMNS", String.Empty);
                dict.Add("ID", String.Empty);
                dict.Add("ID_C", String.Empty);
                dict.Add("DELETED", String.Empty);
                dict.Add("CREATED_BY", String.Empty);
                dict.Add("DATE_ENTERED", String.Empty);
                dict.Add("MODIFIED_USER_ID", String.Empty);
                dict.Add("DATE_MODIFIED", String.Empty);
                dict.Add("KPI_NAME", String.Empty);
                // 09/17/2009 Paul.  No need to audit the UTC date. 
                dict.Add("DATE_MODIFIED_UTC", String.Empty);

                DataView vwLayoutFields = new DataView(dtLayoutFields);
                DataRow rowLast = dtAudit.Rows[0];
                for (int i = 1; i < dtAudit.Rows.Count; i++)
                {
                    DataRow row = dtAudit.Rows[i];
                    foreach (DataColumn col in row.Table.Columns)
                    {
                        if (!dict.ContainsKey(col.ColumnName))
                        {
                            if (Sql.ToString(rowLast[col.ColumnName]) != Sql.ToString(row[col.ColumnName]))
                            {
                                DataRow rowChange = dtChanges.NewRow();
                                dtChanges.Rows.Add(rowChange);
                                // 09/16/2009 Paul.  Localize the field name. 
                                //rowChange["FIELD_NAME"] = Sql.ToString(row["KPI_NAME"]) + " - " + Utils.TableColumnName(L10n, sModule, col.ColumnName);
                                rowChange["FIELD_NAME"] = Sql.ToString(row["KPI_NAME"]) + " - " + L10n.Term(sModuleChild + "." + col.ColumnName);
                                rowChange["CREATED_BY"] = SplendidCache.AssignedUser(Sql.ToGuid(row["MODIFIED_USER_ID"]));
                                // 06/15/2009 Van.  The change date was not being converted to the time zone of the current user. 
                                rowChange["DATE_CREATED"] = Sql.ToDateTime(row["AUDIT_DATE"]);
                                rowChange["BEFORE_VALUE"] = rowLast[col.ColumnName];
                                rowChange["AFTER_VALUE"] = row[col.ColumnName];
                                // 09/05/2016 Paul.  Convert the Guid to a display name. 
                                if (col.ColumnName.EndsWith("_ID"))
                                {
                                    string sDATA_FIELD = col.ColumnName;
                                    string sTABLE_NAME = sDATA_FIELD.Substring(0, sDATA_FIELD.Length - 3);
                                    if (sDATA_FIELD == "ASSIGNED_USER_ID")
                                        sTABLE_NAME = "USERS";
                                    else if (sTABLE_NAME.EndsWith("Y"))
                                        sTABLE_NAME = sDATA_FIELD.Substring(0, sDATA_FIELD.Length - 4) + "IES";
                                    else if (sTABLE_NAME != "PROJECT" && sTABLE_NAME != "PROJECT_TASK")
                                        sTABLE_NAME += "S";
                                    string sMODULE_NAME = Crm.Modules.ModuleName(sTABLE_NAME);
                                    if (sTABLE_NAME == "TEAM_SETS")
                                        sMODULE_NAME = "TeamSets";
                                    rowChange["BEFORE_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_NAME, Sql.ToGuid(rowLast[col.ColumnName]));
                                    rowChange["AFTER_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_NAME, Sql.ToGuid(row[col.ColumnName]));
                                }
                                // 02/05/2018 Paul.  Provide a way to convert ID to NAME for custom fields. 
                                else if (col.ColumnName.EndsWith("_ID_C"))
                                {
                                    vwLayoutFields.RowFilter = "DATA_FIELD = '" + col.ColumnName + " '";
                                    if (vwLayoutFields.Count > 0)
                                    {
                                        string sMODULE_TYPE = Sql.ToString(vwLayoutFields[0]["MODULE_TYPE"]);
                                        if (!Sql.IsEmptyString(sMODULE_TYPE))
                                        {
                                            rowChange["BEFORE_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_TYPE, Sql.ToGuid(rowLast[col.ColumnName]));
                                            rowChange["AFTER_VALUE"] = Crm.Modules.ItemName(Application, sMODULE_TYPE, Sql.ToGuid(row[col.ColumnName]));
                                        }
                                    }
                                }
                                // 09/15/2014 Paul.  Prevent Cross-Site Scripting by HTML encoding the data. 
                                if (rowChange["BEFORE_VALUE"] != DBNull.Value)
                                {
                                    if (rowChange["BEFORE_VALUE"].GetType() == typeof(System.String))
                                        rowChange["BEFORE_VALUE"] = HttpUtility.HtmlEncode(Sql.ToString(rowChange["BEFORE_VALUE"]));
                                }
                                if (rowChange["AFTER_VALUE"] != DBNull.Value)
                                {
                                    if (rowChange["AFTER_VALUE"].GetType() == typeof(System.String))
                                        rowChange["AFTER_VALUE"] = HttpUtility.HtmlEncode(Sql.ToString(rowChange["AFTER_VALUE"]));
                                }
                            }
                        }
                    }
                    rowLast = row;
                }
            }
            return dtChanges;
        }

        public DataView LoadData(Guid guidID, string sModuleName)
        {
            DataView vwGroup = null;
            try
            {
                string sTableName = Sql.ToString(Application["Modules." + sModuleName + ".TableName"]);
                // 05/04/2008 Paul.  Protect against SQL Injection. A table name will never have a space character.
                sTableName = sTableName.Replace(" ", "");
                if (!Sql.IsEmptyGuid(guidID) && !Sql.IsEmptyString(sModuleName) && !Sql.IsEmptyString(sTableName))
                {
                    // 12/30/2007 Paul.  The first query should be used just to determine if access is allowed. 
                    bool bAccessAllowed = false;
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        con.Open();
                        string sSQL;
                        sSQL = "select *              " + ControlChars.CrLf
                             + "  from vw" + sTableName + ControlChars.CrLf;
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQL;
                            Security.Filter(cmd, sModuleName, "view");
                            Sql.AppendParameter(cmd, guidID, "ID", false);

                            using (IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                if (rdr.Read())
                                {
                                    bAccessAllowed = true;
                                    string sNAME = String.Empty;
                                    try
                                    {
                                        // 12/30/2007 Paul.  The name field might not be called NAME.
                                        // For now, just ignore the issue. 
                                        sNAME = Sql.ToString(rdr["NAME"]);
                                    }
                                    catch
                                    {
                                    }
                                    // 09/15/2014 Paul.  Prevent Cross-Site Scripting by HTML encoding the data. 
                                    lblTitle.Text = L10n.Term(".moduleList." + sModuleName) + ": " + HttpUtility.HtmlEncode(sNAME);
                                    SetPageTitle(L10n.Term(".moduleList." + sModuleName) + " - " + sNAME);
                                }
                            }
                        }
                        if (bAccessAllowed)
                        {
                            StringBuilder sb = new StringBuilder();
                            DataTable dtTableColumns = new DataTable();
                            DataTable dtCustomColumns = new DataTable();
                            // 02/29/2008 Niall.  Some SQL Server 2005 installations require matching case for the parameters. 
                            // Since we force the parameter to be uppercase, we must also make it uppercase in the command text. 
                            sSQL = "select ColumnName              " + ControlChars.CrLf
                                 + "  from vwSqlColumns            " + ControlChars.CrLf
                                 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
                                 + " order by colid                " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                // 09/02/2008 Paul.  Standardize the case of metadata tables to uppercase.  PostgreSQL defaults to lowercase. 
                                Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, sTableName));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtTableColumns);
                                }
                            }
                            // 02/29/2008 Niall.  Some SQL Server 2005 installations require matching case for the parameters. 
                            // Since we force the parameter to be uppercase, we must also make it uppercase in the command text. 
                            sSQL = "select ColumnName              " + ControlChars.CrLf
                                 + "  from vwSqlColumns            " + ControlChars.CrLf
                                 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
                                 + " order by colid                " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                // 09/02/2008 Paul.  Standardize the case of metadata tables to uppercase.  PostgreSQL defaults to lowercase. 
                                Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, sTableName + "_CSTM"));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtCustomColumns);
                                }
                            }
                            string sAuditName = sTableName + "_AUDIT";
                            string sCustomAuditName = sTableName + "_CSTM_AUDIT";
                            sb.AppendLine("select " + sAuditName + ".AUDIT_ACTION  as AUDIT_ACTION      ");
                            sb.AppendLine("     , " + sAuditName + ".AUDIT_DATE    as AUDIT_DATE        ");
                            sb.AppendLine("     , " + sAuditName + ".AUDIT_COLUMNS as AUDIT_COLUMNS     ");
                            sb.AppendLine("     , " + sCustomAuditName + ".AUDIT_COLUMNS as CSTM_AUDIT_COLUMNS");
                            foreach (DataRow row in dtTableColumns.Rows)
                            {
                                sb.AppendLine("     , " + sAuditName + "." + Sql.ToString(row["ColumnName"]));
                            }
                            foreach (DataRow row in dtCustomColumns.Rows)
                            {
                                sb.AppendLine("     , " + sCustomAuditName + "." + Sql.ToString(row["ColumnName"]));
                            }
                            sb.AppendLine("  from            " + sAuditName);
                            // 05/12/2017 Paul.  Don't join to custom audit table if custom table does not have fields. 
                            if (dtCustomColumns.Rows.Count > 0)
                            {
                                sb.AppendLine("  left outer join " + sCustomAuditName);
                                sb.AppendLine("               on " + sCustomAuditName + ".ID_C        = " + sAuditName + ".ID         ");
                                sb.AppendLine("              and " + sCustomAuditName + ".AUDIT_TOKEN = " + sAuditName + ".AUDIT_TOKEN");
                            }
                            sb.AppendLine(" where " + sAuditName + ".ID = @ID");
                            sb.AppendLine(" order by " + sAuditName + ".AUDIT_VERSION asc");
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sb.ToString();
                                Sql.AddParameter(cmd, "@ID", guidID);

                                if (bDebug)
                                    Page.ClientScript.RegisterClientScriptBlock(System.Type.GetType("System.String"), "SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        // 02/05/2018 Paul.  Provide a way to convert ID to NAME for custom fields. 
                                        DataTable dtLayoutFields = SplendidCache.EditViewFields(sModuleName + ".EditView", Security.PRIMARY_ROLE_NAME);
                                        DataTable dtChanges = BuildChangesTable(dt, dtLayoutFields);
                                        vwGroup = new DataView(dtChanges);
                                        // 06/03/2009 Paul.  We will not import the SugarCRM history, but we should still display it. 
                                        if (Sql.ToBoolean(Application["CONFIG.append_sugarcrm_history"]))
                                        {
                                            try
                                            {
                                                cmd.Parameters.Clear();
                                                using (DataTable dtSugarCRM = new DataTable())
                                                {
                                                    string sSugarAuditName = sAuditName.ToUpper() + "_SUGARCRM";
                                                    sSQL = "select " + sSugarAuditName + ".DATE_CREATED       " + ControlChars.CrLf
                                                         + "     , USERS.USER_NAME      as CREATED_BY         " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".FIELD_NAME         " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".BEFORE_VALUE_STRING" + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".AFTER_VALUE_STRING " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".BEFORE_VALUE_TEXT  " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".AFTER_VALUE_TEXT   " + ControlChars.CrLf
                                                         + "  from      " + sSugarAuditName + ControlChars.CrLf
                                                         + " inner join USERS                                 " + ControlChars.CrLf
                                                         + "         on USERS.ID      = " + sSugarAuditName + ".CREATED_BY" + ControlChars.CrLf
                                                         + "        and USERS.DELETED = 0                     " + ControlChars.CrLf
                                                         + " where " + sSugarAuditName + ".PARENT_ID = @ID    " + ControlChars.CrLf
                                                         + " order by " + sSugarAuditName + ".DATE_CREATED    " + ControlChars.CrLf;
                                                    cmd.CommandText = sSQL;
                                                    Sql.AddParameter(cmd, "@ID", guidID);
                                                    if (bDebug)
                                                        Page.ClientScript.RegisterClientScriptBlock(System.Type.GetType("System.String"), "SugarSQLCode", Sql.ClientScriptBlock(cmd));

                                                    da.Fill(dtSugarCRM);
                                                    foreach (DataRow rowSugar in dtSugarCRM.Rows)
                                                    {
                                                        DataRow rowMerge = dtChanges.NewRow();
                                                        rowMerge["DATE_CREATED"] = Sql.ToString(rowSugar["DATE_CREATED"]);
                                                        rowMerge["CREATED_BY"] = Sql.ToString(rowSugar["CREATED_BY"]);
                                                        rowMerge["FIELD_NAME"] = Sql.ToString(rowSugar["FIELD_NAME"]);
                                                        rowMerge["BEFORE_VALUE"] = Sql.ToString(rowSugar["BEFORE_VALUE_STRING"]) + Sql.ToString(rowSugar["BEFORE_VALUE_TEXT"]);
                                                        rowMerge["AFTER_VALUE"] = Sql.ToString(rowSugar["AFTER_VALUE_STRING"]) + Sql.ToString(rowSugar["AFTER_VALUE_TEXT"]);
                                                        dtChanges.Rows.Add(rowMerge);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                            }
                                        }
                                        vwGroup = new DataView(dtChanges);
                                    }
                                }
                            }
                        }
                    }
                }
                return vwGroup;
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
                return null;
            }
        }

        public DataView LoadData(Guid guidID, string sModuleName, string sTableName)
        {
            DataView vwGroup = null;
            try
            {
                // string sTableName = Sql.ToString(Application["Modules." + sModuleName + ".TableName"]);
                // 05/04/2008 Paul.  Protect against SQL Injection. A table name will never have a space character.
                sTableName = sTableName.Replace(" ", "");
                if (!Sql.IsEmptyGuid(guidID) && !Sql.IsEmptyString(sTableName))
                {
                    // 12/30/2007 Paul.  The first query should be used just to determine if access is allowed. 
                    bool bAccessAllowed = false;
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        con.Open();
                        string sSQL;
                        sSQL = "select *              " + ControlChars.CrLf
                             + "  from vw" + sTableName + ControlChars.CrLf;
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQL;
                            Security.Filter(cmd, sModuleName, "view");
                            Sql.AppendParameter(cmd, guidID, "ID", false);
                            using (IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                if (rdr.Read())
                                {
                                    bAccessAllowed = true;
                                    //string sNAME = String.Empty;
                                    //try
                                    //{
                                    //    // 12/30/2007 Paul.  The name field might not be called NAME.
                                    //    // For now, just ignore the issue. 
                                    //    sNAME = Sql.ToString(rdr["NAME"]);
                                    //}
                                    //catch
                                    //{
                                    //}
                                    // 09/15/2014 Paul.  Prevent Cross-Site Scripting by HTML encoding the data.                                     
                                }
                            }
                        }
                        if (bAccessAllowed)
                        {
                            StringBuilder sb = new StringBuilder();
                            DataTable dtTableColumns = new DataTable();
                            DataTable dtCustomColumns = new DataTable();
                            // 02/29/2008 Niall.  Some SQL Server 2005 installations require matching case for the parameters. 
                            // Since we force the parameter to be uppercase, we must also make it uppercase in the command text. 
                            sSQL = "select ColumnName              " + ControlChars.CrLf
                                 + "  from vwSqlColumns            " + ControlChars.CrLf
                                 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
                                 + " order by colid                " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                // 09/02/2008 Paul.  Standardize the case of metadata tables to uppercase.  PostgreSQL defaults to lowercase. 
                                Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, sTableName));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtTableColumns);
                                }
                            }
                            // 02/29/2008 Niall.  Some SQL Server 2005 installations require matching case for the parameters. 
                            // Since we force the parameter to be uppercase, we must also make it uppercase in the command text. 
                            sSQL = "select ColumnName              " + ControlChars.CrLf
                                 + "  from vwSqlColumns            " + ControlChars.CrLf
                                 + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
                                 + " order by colid                " + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                // 09/02/2008 Paul.  Standardize the case of metadata tables to uppercase.  PostgreSQL defaults to lowercase. 
                                Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, sTableName + "_CSTM"));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    da.Fill(dtCustomColumns);
                                }
                            }
                            string sAuditName = sTableName + "_AUDIT";
                            string sCustomAuditName = sTableName + "_CSTM_AUDIT";
                            sb.AppendLine("select " + sAuditName + ".AUDIT_ACTION  as AUDIT_ACTION      ");
                            sb.AppendLine("     , " + sAuditName + ".AUDIT_DATE    as AUDIT_DATE        ");
                            sb.AppendLine("     , " + sAuditName + ".AUDIT_COLUMNS as AUDIT_COLUMNS     ");
                            sb.AppendLine("     , " + sCustomAuditName + ".AUDIT_COLUMNS as CSTM_AUDIT_COLUMNS");
                            foreach (DataRow row in dtTableColumns.Rows)
                            {
                                sb.AppendLine("     , " + sAuditName + "." + Sql.ToString(row["ColumnName"]));
                            }
                            foreach (DataRow row in dtCustomColumns.Rows)
                            {
                                sb.AppendLine("     , " + sCustomAuditName + "." + Sql.ToString(row["ColumnName"]));
                            }
                            sb.AppendLine("  from            " + sAuditName);
                            // 05/12/2017 Paul.  Don't join to custom audit table if custom table does not have fields. 
                            if (dtCustomColumns.Rows.Count > 0)
                            {
                                sb.AppendLine("  left outer join " + sCustomAuditName);
                                sb.AppendLine("               on " + sCustomAuditName + ".ID_C        = " + sAuditName + ".ID         ");
                                sb.AppendLine("              and " + sCustomAuditName + ".AUDIT_TOKEN = " + sAuditName + ".AUDIT_TOKEN");
                            }
                            sb.AppendLine(" where " + sAuditName + ".ID = @ID");
                            sb.AppendLine(" order by " + sAuditName + ".AUDIT_VERSION asc");
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sb.ToString();
                                Sql.AddParameter(cmd, "@ID", guidID);

                                if (bDebug)
                                    Page.ClientScript.RegisterClientScriptBlock(System.Type.GetType("System.String"), "SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dt = new DataTable())
                                    {
                                        da.Fill(dt);
                                        // 02/05/2018 Paul.  Provide a way to convert ID to NAME for custom fields. 
                                        DataTable dtLayoutFields = SplendidCache.EditViewFields(sModuleName + ".EditView", Security.PRIMARY_ROLE_NAME);
                                        DataTable dtChanges = BuildChangesTableChild(dt, dtLayoutFields, sModuleName);
                                        vwGroup = new DataView(dtChanges);
                                        // 06/03/2009 Paul.  We will not import the SugarCRM history, but we should still display it. 
                                        if (Sql.ToBoolean(Application["CONFIG.append_sugarcrm_history"]))
                                        {
                                            try
                                            {
                                                cmd.Parameters.Clear();
                                                using (DataTable dtSugarCRM = new DataTable())
                                                {
                                                    string sSugarAuditName = sAuditName.ToUpper() + "_SUGARCRM";
                                                    sSQL = "select " + sSugarAuditName + ".DATE_CREATED       " + ControlChars.CrLf
                                                         + "     , USERS.USER_NAME      as CREATED_BY         " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".FIELD_NAME         " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".BEFORE_VALUE_STRING" + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".AFTER_VALUE_STRING " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".BEFORE_VALUE_TEXT  " + ControlChars.CrLf
                                                         + "     , " + sSugarAuditName + ".AFTER_VALUE_TEXT   " + ControlChars.CrLf
                                                         + "  from      " + sSugarAuditName + ControlChars.CrLf
                                                         + " inner join USERS                                 " + ControlChars.CrLf
                                                         + "         on USERS.ID      = " + sSugarAuditName + ".CREATED_BY" + ControlChars.CrLf
                                                         + "        and USERS.DELETED = 0                     " + ControlChars.CrLf
                                                         + " where " + sSugarAuditName + ".PARENT_ID = @ID    " + ControlChars.CrLf
                                                         + " order by " + sSugarAuditName + ".DATE_CREATED    " + ControlChars.CrLf;
                                                    cmd.CommandText = sSQL;
                                                    Sql.AddParameter(cmd, "@ID", guidID);
                                                    if (bDebug)
                                                        Page.ClientScript.RegisterClientScriptBlock(System.Type.GetType("System.String"), "SugarSQLCode", Sql.ClientScriptBlock(cmd));

                                                    da.Fill(dtSugarCRM);
                                                    foreach (DataRow rowSugar in dtSugarCRM.Rows)
                                                    {
                                                        DataRow rowMerge = dtChanges.NewRow();
                                                        rowMerge["DATE_CREATED"] = Sql.ToString(rowSugar["DATE_CREATED"]);
                                                        rowMerge["CREATED_BY"] = Sql.ToString(rowSugar["CREATED_BY"]);
                                                        rowMerge["FIELD_NAME"] = Sql.ToString(rowSugar["FIELD_NAME"]);
                                                        var beforeValue = Sql.ToString(rowSugar["BEFORE_VALUE_STRING"]) + Sql.ToString(rowSugar["BEFORE_VALUE_TEXT"]);
                                                        var afterValue = Sql.ToString(rowSugar["AFTER_VALUE_STRING"]) + Sql.ToString(rowSugar["AFTER_VALUE_TEXT"]);
                                                        rowMerge["BEFORE_VALUE"] = beforeValue;
                                                        rowMerge["AFTER_VALUE"] = afterValue;

                                                        if (afterValue.Trim() != "" || beforeValue.Trim() != "")
                                                        {
                                                            dtChanges.Rows.Add(rowMerge);
                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                            }
                                        }
                                        vwGroup = new DataView(dtChanges);
                                    }
                                }
                            }
                        }
                    }
                }
                return vwGroup;
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
                return null;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            gID = Sql.ToGuid(Request["ID"]);
            sModule = Sql.ToString(Request["Module"]);

            if (sModule == "KPIB0203")
            {
                vwMain = new DataView(GetHistory("KPIB0202_DETAIL", "B_KPI_ALLOCATE_DETAILS", "KPI_ALLOCATE_ID = '" + gID.ToString() + "'", sModule));
            }
            else if (sModule == "KPIB0201")
            {
                vwMain = new DataView(GetHistory("KPIB020101", "B_KPI_STANDARD_DETAILS", "KPI_STANDARD_ID= '" + gID.ToString() + "'", sModule));
            }
            else if (sModule == "KPIB030301")
            {
                vwMain = new DataView(GetHistory("ChiTietThuThuan", "B_KPI_TOI_ACT_RESULT_DETAIL", "ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM B_KPI_TOI_ACTUAL_RESULT WHERE ID = '" + gID.ToString() + "')", sModule));
            }
            else if (sModule == "KPIM0101")
            {
                vwMain = new DataView(GetHistory("KPID0101", "M_GROUP_KPI_DETAILS", "GROUP_KPI_ID= '" + gID.ToString() + "'", sModule));
            }
            else if (sModule == "KPIB0302")
            {
                vwMain = new DataView(GetHistory("B_KPI_ACT_RESULT_DETAIL", "B_KPI_ACT_RESULT_DETAIL", "ACTUAL_RESULT_CODE IN (SELECT ACTUAL_RESULT_CODE FROM B_KPI_ACT_RESULT_DETAIL WHERE ID = '" + gID.ToString() + "')", sModule));
            }

            if (vwMain != null && vwMain.Count > 0)
            {
                vwMain.Sort = "DATE_CREATED desc, FIELD_NAME asc";
                grdMain.DataSource = vwMain;
            }
            if (!IsPostBack)
            {
                grdMain.DataBind();
            }
        }

        public DataTable GetHistory(string sModuleChildName, string sTableChildName, string sqlCheck, string sModuleParent)
        {
            DataTable mergerDt = new DataTable();
            DbProviderFactory dbf = DbProviderFactories.GetFactory();
            using (IDbConnection con = dbf.CreateConnection())
            {
                con.Open();
                string sqlGetId = "select ID from " + sTableChildName + " where " + sqlCheck;
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sqlGetId;
                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        DataTable dt = new DataTable();
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtr = dt.Rows[i];
                                mergerDt.Merge(LoadData(Sql.ToGuid(dtr["ID"]), sModuleChildName, sTableChildName).Table);
                            }
                        }
                    }
                }

                var dtView = LoadData(gID, sModuleParent);
                if (dtView != null)
                    mergerDt.Merge(dtView.Table);
            }
            return mergerDt;

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

