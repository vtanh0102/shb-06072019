﻿if (typeof CRM == "undefined") {
    CRM = {};
}
if (typeof CRM.Cache == "undefined") {
    CRM.Cache = {};
}
CRM.parentWindow = (window.parent == null || window.parent == window ? window : window.parent);

$(document).ready(function () {
    console.log('numeric');
    //numeric(".number");
    //CRM.formatQty(".format-qty");
    //format Qty number focusout
    var numberFormatQty = Number(0);
    /*
    $(".format-qty").keyup(function (e) {
        //numeric true
        numberFormatQty = CRM.replaceAll($(e.target).val(), ',', '');
        numberFormatQty = CRM.formatQtyWithCulture(numberFormatQty);
        $(e.target).val(numberFormatQty);
    });
    */
    /*
    $(".format-qty").focusout(function (e) {
        //numeric true
        var numberFormatQty = $(e.target).val().slice(0, -3);//CRM.replaceAll($(e.target).val(), ',', '');
        if (numberFormatQty == null || numberFormatQty == '')
            return;
        numberFormatQty = CRM.formatQtyWithCulture(numberFormatQty);
        $(e.target).val(numberFormatQty);
    });
   */

});

function numeric(selector) {
    $(selector).keypress(function (e) {
        // validate key code
        if (String.fromCharCode(e.keyCode).match(/[^0123456789.,]/g))
            return false;

    });

    $(selector).focusout(function (e) {
        // validate numeric
        var numberFormatQty = CRM.replaceAll($(e.target).val(), ',', '');
        //console.log(numberFormatQty);
        if (!$.isNumeric(numberFormatQty + String.fromCharCode(e.keyCode)))
            return false;
    });
}


//TuanDN: ADD
CRM.replaceAll = function (target, search, replacement) {
    return target.replace(new RegExp(search, 'g'), replacement);
};


CRM.formatQty = function (number) {
    if (number == null || number == "") {
        return "";
    }
    try {
        var number = parseFloat(number).toFixed(0) + '';
    } catch (e) {
        console.log('exception: ' + e);
        var number = number + '';
    }
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
};


CRM.formatQtyWithCulture = function (number) {
    if (number == null || number == "") {
        return "";
    }
    try {       
        var locale = __cultureInfo['name'];
        var json = __cultureInfo['numberFormat'];
        var cf = new Intl.NumberFormat(locale, json);
        var groupSeparator = __cultureInfo['numberFormat']['NumberGroupSeparator'];
        var pattern = "([" + groupSeparator + "])(?=.*\\1)";
        number = CRM.replaceAll(number, pattern, '');
        return cf.format(number);
    } catch (e) {
        console.log('exception: ' + e);
    }
    return "";
};

CRM.formatNumber1 = function (number) {
    if (number == null || number == "") {
        return "";
    }
    var comma = ',',
    string = Math.max(0, number).toFixed(0).toLocaleString(),
    length = string.length,
    end = /^\d{4,}$/.test(string) ? length % 3 : 0;
    return (end ? string.slice(0, end) + comma : '') + string.slice(end).replace(/(\d{3})(?=\d)/g, '$1' + comma);
}

CRM.formatNumber2 = function (number) {
    if (number === null || number === "") {
        return "";
    }
    return Math.max(0, number).toFixed(0).replace(/(?=(?:\d{3})+$)(?!^)/g, ',');
}

CRM.formatNumber3 = function (n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

Number.prototype.format = function (n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};

CRM.formatNumber = function (number) {
    if (number === null || number === "") {
        return "";
    }
    number = Math.max(0, number).toFixed(2) + '';
    x = number.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function format(number) {

    var decimalSeparator = ".";
    var thousandSeparator = ",";

    // make sure we have a string
    var result = String(number);

    // split the number in the integer and decimals, if any
    var parts = result.split(decimalSeparator);

    // if we don't have decimals, add .00
    if (!parts[1]) {
        parts[1] = "00";
    }

    // reverse the string (1719 becomes 9171)
    result = parts[0].split("").reverse().join("");

    // add thousand separator each 3 characters, except at the end of the string
    result = result.replace(/(\d{3}(?!$))/g, "$1" + thousandSeparator);

    // reverse back the integer and replace the original integer
    parts[0] = result.split("").reverse().join("");

    // recombine integer with decimals
    return parts.join(decimalSeparator);
}

//TuanDN: END
