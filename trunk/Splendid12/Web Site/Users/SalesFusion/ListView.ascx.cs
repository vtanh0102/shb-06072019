/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.Users.SalesFusion
{
	/// <summary>
	///		Summary description for ListView.
	/// </summary>
	public class ListView : SplendidControl
	{
		protected _controls.ExportHeader ctlExportHeader;
		protected _controls.SearchView   ctlSearchView  ;

		protected UniqueStringCollection arrSelectFields;
		protected DataView      vwMain         ;
		protected SplendidGrid  grdMain        ;
		protected Label         lblError       ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				if ( e.CommandName == "Search" )
				{
					grdMain.CurrentPageIndex = 0;
					grdMain.DataBind();
				}
				else if ( e.CommandName == "SortGrid" )
				{
					grdMain.SetSortFields(e.CommandArgument as string[]);
					arrSelectFields.AddFields(grdMain.SortColumn);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		protected void grdMain_OnSelectMethod(int nCurrentPageIndex, int nPageSize)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					// 01/13/2010 Paul.  Allow default search to be disabled. 
					if ( (PrintView || IsPostBack || SplendidCRM.Crm.Modules.DefaultSearch(m_sMODULE)) && grdMain.VirtualItemCount > 0 )
					{
						Spring.Social.SalesFusion.Api.ISalesFusion salesFusion = Spring.Social.SalesFusion.SalesFusionSync.CreateApi(Application);
						IList<Spring.Social.SalesFusion.Api.User> users = salesFusion.UserOperations.GetPage(nCurrentPageIndex + 1);
						//ctlSearchView.SqlSearchClause(cmd);
						using ( DataTable dt = Spring.Social.SalesFusion.Api.User.ConvertToTable(users) )
						{
							//this.ApplyGridViewRules(m_sMODULE + "." + LayoutListView, dt);
							vwMain = dt.DefaultView;
							grdMain.DataSource = vwMain ;
						}
						ctlExportHeader.Visible = true;
					}
					else
					{
						ctlExportHeader.Visible = false;
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
			// 07/23/2017 Paul.  Consistent application of security for cloud service. 
			this.Visible = Spring.Social.SalesFusion.SalesFusionSync.SalesFusionEnabled(Application) && (SplendidCRM.Security.AdminUserAccess(m_sMODULE, "list") >= 0) && (SplendidCRM.Security.AdminUserAccess("SalesFusion", "list") >= 0);
			if ( !this.Visible )
				return;

			try
			{
				if ( !IsPostBack )
				{
					grdMain.OrderByClause("user_id", "asc");
					
					Spring.Social.SalesFusion.Api.ISalesFusion salesFusion = Spring.Social.SalesFusion.SalesFusionSync.CreateApi(Application);
					grdMain.VirtualItemCount = salesFusion.UserOperations.GetCount();
					ctlExportHeader.Visible = true;
				}
				else
				{
					DataTable dt = ViewState[m_sMODULE] as DataTable;
					if ( dt != null )
					{
						vwMain = dt.DefaultView;
						grdMain.DataSource = vwMain ;
					}
				}
				if ( !IsPostBack )
				{
					grdMain.DataBind();
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				lblError.Text = ex.Message;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlSearchView  .Command += new CommandEventHandler(Page_Command);
			ctlExportHeader.Command += new CommandEventHandler(Page_Command);
			grdMain.AllowCustomPaging = true;
			grdMain.PageSize          = 100;
			grdMain.SelectMethod     += new SelectMethodHandler(grdMain_OnSelectMethod);

			m_sMODULE = "Users";
			SetMenu(m_sMODULE);
			arrSelectFields = new UniqueStringCollection();
			this.AppendGridColumns(grdMain, m_sMODULE + "." + LayoutListView, arrSelectFields);
		}
		#endregion
	}
}

