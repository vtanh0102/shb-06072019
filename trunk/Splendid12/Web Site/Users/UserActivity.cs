/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// UserActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:32 PM
	/// </summary>
	public class UserActivity: SplendidActivity
	{
		public UserActivity()
		{
			this.Name = "UserActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.IDProperty))); }
			set { base.SetValue(UserActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(UserActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty USER_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("USER_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string USER_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.USER_NAMEProperty))); }
			set { base.SetValue(UserActivity.USER_NAMEProperty, value); }
		}

		public static DependencyProperty FIRST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FIRST_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FIRST_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.FIRST_NAMEProperty))); }
			set { base.SetValue(UserActivity.FIRST_NAMEProperty, value); }
		}

		public static DependencyProperty LAST_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LAST_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.LAST_NAMEProperty))); }
			set { base.SetValue(UserActivity.LAST_NAMEProperty, value); }
		}

		public static DependencyProperty REPORTS_TO_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORTS_TO_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid REPORTS_TO_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.REPORTS_TO_IDProperty))); }
			set { base.SetValue(UserActivity.REPORTS_TO_IDProperty, value); }
		}

		public static DependencyProperty IS_ADMINProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_ADMIN", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_ADMIN
		{
			get { return ((bool)(base.GetValue(UserActivity.IS_ADMINProperty))); }
			set { base.SetValue(UserActivity.IS_ADMINProperty, value); }
		}

		public static DependencyProperty RECEIVE_NOTIFICATIONSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RECEIVE_NOTIFICATIONS", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool RECEIVE_NOTIFICATIONS
		{
			get { return ((bool)(base.GetValue(UserActivity.RECEIVE_NOTIFICATIONSProperty))); }
			set { base.SetValue(UserActivity.RECEIVE_NOTIFICATIONSProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(UserActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(UserActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TITLEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TITLE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TITLE
		{
			get { return ((string)(base.GetValue(UserActivity.TITLEProperty))); }
			set { base.SetValue(UserActivity.TITLEProperty, value); }
		}

		public static DependencyProperty DEPARTMENTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEPARTMENT", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DEPARTMENT
		{
			get { return ((string)(base.GetValue(UserActivity.DEPARTMENTProperty))); }
			set { base.SetValue(UserActivity.DEPARTMENTProperty, value); }
		}

		public static DependencyProperty PHONE_HOMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_HOME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_HOME
		{
			get { return ((string)(base.GetValue(UserActivity.PHONE_HOMEProperty))); }
			set { base.SetValue(UserActivity.PHONE_HOMEProperty, value); }
		}

		public static DependencyProperty PHONE_MOBILEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_MOBILE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_MOBILE
		{
			get { return ((string)(base.GetValue(UserActivity.PHONE_MOBILEProperty))); }
			set { base.SetValue(UserActivity.PHONE_MOBILEProperty, value); }
		}

		public static DependencyProperty PHONE_WORKProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_WORK", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_WORK
		{
			get { return ((string)(base.GetValue(UserActivity.PHONE_WORKProperty))); }
			set { base.SetValue(UserActivity.PHONE_WORKProperty, value); }
		}

		public static DependencyProperty PHONE_OTHERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_OTHER", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_OTHER
		{
			get { return ((string)(base.GetValue(UserActivity.PHONE_OTHERProperty))); }
			set { base.SetValue(UserActivity.PHONE_OTHERProperty, value); }
		}

		public static DependencyProperty PHONE_FAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PHONE_FAX", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PHONE_FAX
		{
			get { return ((string)(base.GetValue(UserActivity.PHONE_FAXProperty))); }
			set { base.SetValue(UserActivity.PHONE_FAXProperty, value); }
		}

		public static DependencyProperty EMAIL1Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL1", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL1
		{
			get { return ((string)(base.GetValue(UserActivity.EMAIL1Property))); }
			set { base.SetValue(UserActivity.EMAIL1Property, value); }
		}

		public static DependencyProperty EMAIL2Property = System.Workflow.ComponentModel.DependencyProperty.Register("EMAIL2", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMAIL2
		{
			get { return ((string)(base.GetValue(UserActivity.EMAIL2Property))); }
			set { base.SetValue(UserActivity.EMAIL2Property, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(UserActivity.STATUSProperty))); }
			set { base.SetValue(UserActivity.STATUSProperty, value); }
		}

		public static DependencyProperty ADDRESS_STREETProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_STREET", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_STREET
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_STREETProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_STREETProperty, value); }
		}

		public static DependencyProperty ADDRESS_CITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_CITY", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_CITY
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_CITYProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_CITYProperty, value); }
		}

		public static DependencyProperty ADDRESS_STATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_STATE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_STATE
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_STATEProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_STATEProperty, value); }
		}

		public static DependencyProperty ADDRESS_POSTALCODEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_POSTALCODE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_POSTALCODE
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_POSTALCODEProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_POSTALCODEProperty, value); }
		}

		public static DependencyProperty ADDRESS_COUNTRYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_COUNTRY", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_COUNTRY
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_COUNTRYProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_COUNTRYProperty, value); }
		}

		public static DependencyProperty USER_PREFERENCESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("USER_PREFERENCES", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string USER_PREFERENCES
		{
			get { return ((string)(base.GetValue(UserActivity.USER_PREFERENCESProperty))); }
			set { base.SetValue(UserActivity.USER_PREFERENCESProperty, value); }
		}

		public static DependencyProperty PORTAL_ONLYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PORTAL_ONLY", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool PORTAL_ONLY
		{
			get { return ((bool)(base.GetValue(UserActivity.PORTAL_ONLYProperty))); }
			set { base.SetValue(UserActivity.PORTAL_ONLYProperty, value); }
		}

		public static DependencyProperty EMPLOYEE_STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EMPLOYEE_STATUS", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EMPLOYEE_STATUS
		{
			get { return ((string)(base.GetValue(UserActivity.EMPLOYEE_STATUSProperty))); }
			set { base.SetValue(UserActivity.EMPLOYEE_STATUSProperty, value); }
		}

		public static DependencyProperty MESSENGER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MESSENGER_ID", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MESSENGER_ID
		{
			get { return ((string)(base.GetValue(UserActivity.MESSENGER_IDProperty))); }
			set { base.SetValue(UserActivity.MESSENGER_IDProperty, value); }
		}

		public static DependencyProperty MESSENGER_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MESSENGER_TYPE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MESSENGER_TYPE
		{
			get { return ((string)(base.GetValue(UserActivity.MESSENGER_TYPEProperty))); }
			set { base.SetValue(UserActivity.MESSENGER_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(UserActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(UserActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.PARENT_IDProperty))); }
			set { base.SetValue(UserActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty IS_GROUPProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_GROUP", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_GROUP
		{
			get { return ((bool)(base.GetValue(UserActivity.IS_GROUPProperty))); }
			set { base.SetValue(UserActivity.IS_GROUPProperty, value); }
		}

		public static DependencyProperty DEFAULT_TEAMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEFAULT_TEAM", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid DEFAULT_TEAM
		{
			get { return ((Guid)(base.GetValue(UserActivity.DEFAULT_TEAMProperty))); }
			set { base.SetValue(UserActivity.DEFAULT_TEAMProperty, value); }
		}

		public static DependencyProperty IS_ADMIN_DELEGATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_ADMIN_DELEGATE", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_ADMIN_DELEGATE
		{
			get { return ((bool)(base.GetValue(UserActivity.IS_ADMIN_DELEGATEProperty))); }
			set { base.SetValue(UserActivity.IS_ADMIN_DELEGATEProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPUSERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPUSER", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MAIL_SMTPUSER
		{
			get { return ((string)(base.GetValue(UserActivity.MAIL_SMTPUSERProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPUSERProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPPASSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPPASS", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MAIL_SMTPPASS
		{
			get { return ((string)(base.GetValue(UserActivity.MAIL_SMTPPASSProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPPASSProperty, value); }
		}

		public static DependencyProperty SYSTEM_GENERATED_PASSWORDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SYSTEM_GENERATED_PASSWORD", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool SYSTEM_GENERATED_PASSWORD
		{
			get { return ((bool)(base.GetValue(UserActivity.SYSTEM_GENERATED_PASSWORDProperty))); }
			set { base.SetValue(UserActivity.SYSTEM_GENERATED_PASSWORDProperty, value); }
		}

		public static DependencyProperty GOOGLEAPPS_SYNC_CONTACTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GOOGLEAPPS_SYNC_CONTACTS", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool GOOGLEAPPS_SYNC_CONTACTS
		{
			get { return ((bool)(base.GetValue(UserActivity.GOOGLEAPPS_SYNC_CONTACTSProperty))); }
			set { base.SetValue(UserActivity.GOOGLEAPPS_SYNC_CONTACTSProperty, value); }
		}

		public static DependencyProperty GOOGLEAPPS_SYNC_CALENDARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GOOGLEAPPS_SYNC_CALENDAR", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool GOOGLEAPPS_SYNC_CALENDAR
		{
			get { return ((bool)(base.GetValue(UserActivity.GOOGLEAPPS_SYNC_CALENDARProperty))); }
			set { base.SetValue(UserActivity.GOOGLEAPPS_SYNC_CALENDARProperty, value); }
		}

		public static DependencyProperty GOOGLEAPPS_USERNAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GOOGLEAPPS_USERNAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string GOOGLEAPPS_USERNAME
		{
			get { return ((string)(base.GetValue(UserActivity.GOOGLEAPPS_USERNAMEProperty))); }
			set { base.SetValue(UserActivity.GOOGLEAPPS_USERNAMEProperty, value); }
		}

		public static DependencyProperty GOOGLEAPPS_PASSWORDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GOOGLEAPPS_PASSWORD", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string GOOGLEAPPS_PASSWORD
		{
			get { return ((string)(base.GetValue(UserActivity.GOOGLEAPPS_PASSWORDProperty))); }
			set { base.SetValue(UserActivity.GOOGLEAPPS_PASSWORDProperty, value); }
		}

		public static DependencyProperty FACEBOOK_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FACEBOOK_ID", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FACEBOOK_ID
		{
			get { return ((string)(base.GetValue(UserActivity.FACEBOOK_IDProperty))); }
			set { base.SetValue(UserActivity.FACEBOOK_IDProperty, value); }
		}

		public static DependencyProperty ICLOUD_SYNC_CONTACTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_SYNC_CONTACTS", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool ICLOUD_SYNC_CONTACTS
		{
			get { return ((bool)(base.GetValue(UserActivity.ICLOUD_SYNC_CONTACTSProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_SYNC_CONTACTSProperty, value); }
		}

		public static DependencyProperty ICLOUD_SYNC_CALENDARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_SYNC_CALENDAR", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool ICLOUD_SYNC_CALENDAR
		{
			get { return ((bool)(base.GetValue(UserActivity.ICLOUD_SYNC_CALENDARProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_SYNC_CALENDARProperty, value); }
		}

		public static DependencyProperty ICLOUD_USERNAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_USERNAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ICLOUD_USERNAME
		{
			get { return ((string)(base.GetValue(UserActivity.ICLOUD_USERNAMEProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_USERNAMEProperty, value); }
		}

		public static DependencyProperty ICLOUD_PASSWORDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_PASSWORD", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ICLOUD_PASSWORD
		{
			get { return ((string)(base.GetValue(UserActivity.ICLOUD_PASSWORDProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_PASSWORDProperty, value); }
		}

		public static DependencyProperty THEMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("THEME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string THEME
		{
			get { return ((string)(base.GetValue(UserActivity.THEMEProperty))); }
			set { base.SetValue(UserActivity.THEMEProperty, value); }
		}

		public static DependencyProperty DATE_FORMATProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_FORMAT", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DATE_FORMAT
		{
			get { return ((string)(base.GetValue(UserActivity.DATE_FORMATProperty))); }
			set { base.SetValue(UserActivity.DATE_FORMATProperty, value); }
		}

		public static DependencyProperty TIME_FORMATProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_FORMAT", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TIME_FORMAT
		{
			get { return ((string)(base.GetValue(UserActivity.TIME_FORMATProperty))); }
			set { base.SetValue(UserActivity.TIME_FORMATProperty, value); }
		}

		public static DependencyProperty LANGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LANG", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LANG
		{
			get { return ((string)(base.GetValue(UserActivity.LANGProperty))); }
			set { base.SetValue(UserActivity.LANGProperty, value); }
		}

		public static DependencyProperty CURRENCY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CURRENCY_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CURRENCY_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.CURRENCY_IDProperty))); }
			set { base.SetValue(UserActivity.CURRENCY_IDProperty, value); }
		}

		public static DependencyProperty TIMEZONE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIMEZONE_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TIMEZONE_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.TIMEZONE_IDProperty))); }
			set { base.SetValue(UserActivity.TIMEZONE_IDProperty, value); }
		}

		public static DependencyProperty SAVE_QUERYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SAVE_QUERY", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool SAVE_QUERY
		{
			get { return ((bool)(base.GetValue(UserActivity.SAVE_QUERYProperty))); }
			set { base.SetValue(UserActivity.SAVE_QUERYProperty, value); }
		}

		public static DependencyProperty GROUP_TABSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GROUP_TABS", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool GROUP_TABS
		{
			get { return ((bool)(base.GetValue(UserActivity.GROUP_TABSProperty))); }
			set { base.SetValue(UserActivity.GROUP_TABSProperty, value); }
		}

		public static DependencyProperty SUBPANEL_TABSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBPANEL_TABS", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool SUBPANEL_TABS
		{
			get { return ((bool)(base.GetValue(UserActivity.SUBPANEL_TABSProperty))); }
			set { base.SetValue(UserActivity.SUBPANEL_TABSProperty, value); }
		}

		public static DependencyProperty EXTENSIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXTENSION", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string EXTENSION
		{
			get { return ((string)(base.GetValue(UserActivity.EXTENSIONProperty))); }
			set { base.SetValue(UserActivity.EXTENSIONProperty, value); }
		}

		public static DependencyProperty SMS_OPT_INProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SMS_OPT_IN", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SMS_OPT_IN
		{
			get { return ((string)(base.GetValue(UserActivity.SMS_OPT_INProperty))); }
			set { base.SetValue(UserActivity.SMS_OPT_INProperty, value); }
		}

		public static DependencyProperty PICTUREProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PICTURE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PICTURE
		{
			get { return ((string)(base.GetValue(UserActivity.PICTUREProperty))); }
			set { base.SetValue(UserActivity.PICTUREProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPSERVERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPSERVER", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MAIL_SMTPSERVER
		{
			get { return ((string)(base.GetValue(UserActivity.MAIL_SMTPSERVERProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPSERVERProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPPORTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPPORT", typeof(Int32), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 MAIL_SMTPPORT
		{
			get { return ((Int32)(base.GetValue(UserActivity.MAIL_SMTPPORTProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPPORTProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPAUTH_REQProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPAUTH_REQ", typeof(bool), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool MAIL_SMTPAUTH_REQ
		{
			get { return ((bool)(base.GetValue(UserActivity.MAIL_SMTPAUTH_REQProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPAUTH_REQProperty, value); }
		}

		public static DependencyProperty MAIL_SMTPSSLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SMTPSSL", typeof(Int32), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 MAIL_SMTPSSL
		{
			get { return ((Int32)(base.GetValue(UserActivity.MAIL_SMTPSSLProperty))); }
			set { base.SetValue(UserActivity.MAIL_SMTPSSLProperty, value); }
		}

		public static DependencyProperty MAIL_SENDTYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAIL_SENDTYPE", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MAIL_SENDTYPE
		{
			get { return ((string)(base.GetValue(UserActivity.MAIL_SENDTYPEProperty))); }
			set { base.SetValue(UserActivity.MAIL_SENDTYPEProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(UserActivity.CREATED_BYProperty))); }
			set { base.SetValue(UserActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(UserActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(UserActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(UserActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(UserActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(UserActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(UserActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(UserActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty ICLOUD_CTAG_CALENDARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_CTAG_CALENDAR", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ICLOUD_CTAG_CALENDAR
		{
			get { return ((string)(base.GetValue(UserActivity.ICLOUD_CTAG_CALENDARProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_CTAG_CALENDARProperty, value); }
		}

		public static DependencyProperty ICLOUD_CTAG_CONTACTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ICLOUD_CTAG_CONTACTS", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ICLOUD_CTAG_CONTACTS
		{
			get { return ((string)(base.GetValue(UserActivity.ICLOUD_CTAG_CONTACTSProperty))); }
			set { base.SetValue(UserActivity.ICLOUD_CTAG_CONTACTSProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(UserActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(UserActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(UserActivity.NAMEProperty))); }
			set { base.SetValue(UserActivity.NAMEProperty, value); }
		}

		public static DependencyProperty PRIMARY_ROLE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ROLE_ID", typeof(Guid), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PRIMARY_ROLE_ID
		{
			get { return ((Guid)(base.GetValue(UserActivity.PRIMARY_ROLE_IDProperty))); }
			set { base.SetValue(UserActivity.PRIMARY_ROLE_IDProperty, value); }
		}

		public static DependencyProperty ADDRESS_HTMLProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ADDRESS_HTML", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ADDRESS_HTML
		{
			get { return ((string)(base.GetValue(UserActivity.ADDRESS_HTMLProperty))); }
			set { base.SetValue(UserActivity.ADDRESS_HTMLProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(UserActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty DEFAULT_TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEFAULT_TEAM_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DEFAULT_TEAM_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.DEFAULT_TEAM_NAMEProperty))); }
			set { base.SetValue(UserActivity.DEFAULT_TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty FULL_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FULL_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FULL_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.FULL_NAMEProperty))); }
			set { base.SetValue(UserActivity.FULL_NAMEProperty, value); }
		}

		public static DependencyProperty GOOGLEAPPS_OAUTH_ENABLEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("GOOGLEAPPS_OAUTH_ENABLED", typeof(Int32), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 GOOGLEAPPS_OAUTH_ENABLED
		{
			get { return ((Int32)(base.GetValue(UserActivity.GOOGLEAPPS_OAUTH_ENABLEDProperty))); }
			set { base.SetValue(UserActivity.GOOGLEAPPS_OAUTH_ENABLEDProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(UserActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty OFFICE365_OAUTH_ENABLEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("OFFICE365_OAUTH_ENABLED", typeof(Int32), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 OFFICE365_OAUTH_ENABLED
		{
			get { return ((Int32)(base.GetValue(UserActivity.OFFICE365_OAUTH_ENABLEDProperty))); }
			set { base.SetValue(UserActivity.OFFICE365_OAUTH_ENABLEDProperty, value); }
		}

		public static DependencyProperty PRIMARY_ROLE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIMARY_ROLE_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIMARY_ROLE_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.PRIMARY_ROLE_NAMEProperty))); }
			set { base.SetValue(UserActivity.PRIMARY_ROLE_NAMEProperty, value); }
		}

		public static DependencyProperty REPORTS_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORTS_TO_NAME", typeof(string), typeof(UserActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPORTS_TO_NAME
		{
			get { return ((string)(base.GetValue(UserActivity.REPORTS_TO_NAMEProperty))); }
			set { base.SetValue(UserActivity.REPORTS_TO_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("UserActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("UserActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select USERS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwUSERS_AUDIT        USERS          " + ControlChars.CrLf
							                + " inner join vwUSERS_AUDIT        USERS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on USERS_AUDIT_OLD.ID = USERS.ID       " + ControlChars.CrLf
							                + "        and USERS_AUDIT_OLD.AUDIT_VERSION = (select max(vwUSERS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwUSERS_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwUSERS_AUDIT.ID            =  USERS.ID           " + ControlChars.CrLf
							                + "                                                and vwUSERS_AUDIT.AUDIT_VERSION <  USERS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwUSERS_AUDIT.AUDIT_TOKEN   <> USERS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where USERS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwUSERS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwUSERS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwUSERS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								USER_NAME                      = Sql.ToString  (rdr["USER_NAME"                     ]);
								FIRST_NAME                     = Sql.ToString  (rdr["FIRST_NAME"                    ]);
								LAST_NAME                      = Sql.ToString  (rdr["LAST_NAME"                     ]);
								REPORTS_TO_ID                  = Sql.ToGuid    (rdr["REPORTS_TO_ID"                 ]);
								IS_ADMIN                       = Sql.ToBoolean (rdr["IS_ADMIN"                      ]);
								RECEIVE_NOTIFICATIONS          = Sql.ToBoolean (rdr["RECEIVE_NOTIFICATIONS"         ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TITLE                          = Sql.ToString  (rdr["TITLE"                         ]);
								DEPARTMENT                     = Sql.ToString  (rdr["DEPARTMENT"                    ]);
								PHONE_HOME                     = Sql.ToString  (rdr["PHONE_HOME"                    ]);
								PHONE_MOBILE                   = Sql.ToString  (rdr["PHONE_MOBILE"                  ]);
								PHONE_WORK                     = Sql.ToString  (rdr["PHONE_WORK"                    ]);
								PHONE_OTHER                    = Sql.ToString  (rdr["PHONE_OTHER"                   ]);
								PHONE_FAX                      = Sql.ToString  (rdr["PHONE_FAX"                     ]);
								EMAIL1                         = Sql.ToString  (rdr["EMAIL1"                        ]);
								EMAIL2                         = Sql.ToString  (rdr["EMAIL2"                        ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								ADDRESS_STREET                 = Sql.ToString  (rdr["ADDRESS_STREET"                ]);
								ADDRESS_CITY                   = Sql.ToString  (rdr["ADDRESS_CITY"                  ]);
								ADDRESS_STATE                  = Sql.ToString  (rdr["ADDRESS_STATE"                 ]);
								ADDRESS_POSTALCODE             = Sql.ToString  (rdr["ADDRESS_POSTALCODE"            ]);
								ADDRESS_COUNTRY                = Sql.ToString  (rdr["ADDRESS_COUNTRY"               ]);
								USER_PREFERENCES               = Sql.ToString  (rdr["USER_PREFERENCES"              ]);
								PORTAL_ONLY                    = Sql.ToBoolean (rdr["PORTAL_ONLY"                   ]);
								EMPLOYEE_STATUS                = Sql.ToString  (rdr["EMPLOYEE_STATUS"               ]);
								MESSENGER_ID                   = Sql.ToString  (rdr["MESSENGER_ID"                  ]);
								MESSENGER_TYPE                 = Sql.ToString  (rdr["MESSENGER_TYPE"                ]);
								IS_GROUP                       = Sql.ToBoolean (rdr["IS_GROUP"                      ]);
								DEFAULT_TEAM                   = Sql.ToGuid    (rdr["DEFAULT_TEAM"                  ]);
								IS_ADMIN_DELEGATE              = Sql.ToBoolean (rdr["IS_ADMIN_DELEGATE"             ]);
								MAIL_SMTPUSER                  = Sql.ToString  (rdr["MAIL_SMTPUSER"                 ]);
								MAIL_SMTPPASS                  = Sql.ToString  (rdr["MAIL_SMTPPASS"                 ]);
								SYSTEM_GENERATED_PASSWORD      = Sql.ToBoolean (rdr["SYSTEM_GENERATED_PASSWORD"     ]);
								GOOGLEAPPS_SYNC_CONTACTS       = Sql.ToBoolean (rdr["GOOGLEAPPS_SYNC_CONTACTS"      ]);
								GOOGLEAPPS_SYNC_CALENDAR       = Sql.ToBoolean (rdr["GOOGLEAPPS_SYNC_CALENDAR"      ]);
								GOOGLEAPPS_USERNAME            = Sql.ToString  (rdr["GOOGLEAPPS_USERNAME"           ]);
								GOOGLEAPPS_PASSWORD            = Sql.ToString  (rdr["GOOGLEAPPS_PASSWORD"           ]);
								FACEBOOK_ID                    = Sql.ToString  (rdr["FACEBOOK_ID"                   ]);
								ICLOUD_SYNC_CONTACTS           = Sql.ToBoolean (rdr["ICLOUD_SYNC_CONTACTS"          ]);
								ICLOUD_SYNC_CALENDAR           = Sql.ToBoolean (rdr["ICLOUD_SYNC_CALENDAR"          ]);
								ICLOUD_USERNAME                = Sql.ToString  (rdr["ICLOUD_USERNAME"               ]);
								ICLOUD_PASSWORD                = Sql.ToString  (rdr["ICLOUD_PASSWORD"               ]);
								THEME                          = Sql.ToString  (rdr["THEME"                         ]);
								DATE_FORMAT                    = Sql.ToString  (rdr["DATE_FORMAT"                   ]);
								TIME_FORMAT                    = Sql.ToString  (rdr["TIME_FORMAT"                   ]);
								LANG                           = Sql.ToString  (rdr["LANG"                          ]);
								CURRENCY_ID                    = Sql.ToGuid    (rdr["CURRENCY_ID"                   ]);
								TIMEZONE_ID                    = Sql.ToGuid    (rdr["TIMEZONE_ID"                   ]);
								SAVE_QUERY                     = Sql.ToBoolean (rdr["SAVE_QUERY"                    ]);
								GROUP_TABS                     = Sql.ToBoolean (rdr["GROUP_TABS"                    ]);
								SUBPANEL_TABS                  = Sql.ToBoolean (rdr["SUBPANEL_TABS"                 ]);
								EXTENSION                      = Sql.ToString  (rdr["EXTENSION"                     ]);
								SMS_OPT_IN                     = Sql.ToString  (rdr["SMS_OPT_IN"                    ]);
								PICTURE                        = Sql.ToString  (rdr["PICTURE"                       ]);
								if ( !bPast )
									MAIL_SMTPSERVER                = Sql.ToString  (rdr["MAIL_SMTPSERVER"               ]);
								if ( !bPast )
									MAIL_SMTPPORT                  = Sql.ToInteger (rdr["MAIL_SMTPPORT"                 ]);
								if ( !bPast )
									MAIL_SMTPAUTH_REQ              = Sql.ToBoolean (rdr["MAIL_SMTPAUTH_REQ"             ]);
								if ( !bPast )
									MAIL_SMTPSSL                   = Sql.ToInteger (rdr["MAIL_SMTPSSL"                  ]);
								if ( !bPast )
									MAIL_SENDTYPE                  = Sql.ToString  (rdr["MAIL_SENDTYPE"                 ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								ICLOUD_CTAG_CALENDAR           = Sql.ToString  (rdr["ICLOUD_CTAG_CALENDAR"          ]);
								ICLOUD_CTAG_CONTACTS           = Sql.ToString  (rdr["ICLOUD_CTAG_CONTACTS"          ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								PRIMARY_ROLE_ID                = Sql.ToGuid    (rdr["PRIMARY_ROLE_ID"               ]);
								if ( !bPast )
								{
									ADDRESS_HTML                   = Sql.ToString  (rdr["ADDRESS_HTML"                  ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									DEFAULT_TEAM_NAME              = Sql.ToString  (rdr["DEFAULT_TEAM_NAME"             ]);
									FULL_NAME                      = Sql.ToString  (rdr["FULL_NAME"                     ]);
									GOOGLEAPPS_OAUTH_ENABLED       = Sql.ToInteger (rdr["GOOGLEAPPS_OAUTH_ENABLED"      ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									OFFICE365_OAUTH_ENABLED        = Sql.ToInteger (rdr["OFFICE365_OAUTH_ENABLED"       ]);
									PRIMARY_ROLE_NAME              = Sql.ToString  (rdr["PRIMARY_ROLE_NAME"             ]);
									REPORTS_TO_NAME                = Sql.ToString  (rdr["REPORTS_TO_NAME"               ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("UserActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("USERS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spUSERS_Update
								( ref gID
								, USER_NAME
								, FIRST_NAME
								, LAST_NAME
								, REPORTS_TO_ID
								, IS_ADMIN
								, RECEIVE_NOTIFICATIONS
								, DESCRIPTION
								, TITLE
								, DEPARTMENT
								, PHONE_HOME
								, PHONE_MOBILE
								, PHONE_WORK
								, PHONE_OTHER
								, PHONE_FAX
								, EMAIL1
								, EMAIL2
								, STATUS
								, ADDRESS_STREET
								, ADDRESS_CITY
								, ADDRESS_STATE
								, ADDRESS_POSTALCODE
								, ADDRESS_COUNTRY
								, USER_PREFERENCES
								, PORTAL_ONLY
								, EMPLOYEE_STATUS
								, MESSENGER_ID
								, MESSENGER_TYPE
								, PARENT_TYPE
								, PARENT_ID
								, IS_GROUP
								, DEFAULT_TEAM
								, IS_ADMIN_DELEGATE
								, MAIL_SMTPUSER
								, MAIL_SMTPPASS
								, SYSTEM_GENERATED_PASSWORD
								, GOOGLEAPPS_SYNC_CONTACTS
								, GOOGLEAPPS_SYNC_CALENDAR
								, GOOGLEAPPS_USERNAME
								, GOOGLEAPPS_PASSWORD
								, FACEBOOK_ID
								, ICLOUD_SYNC_CONTACTS
								, ICLOUD_SYNC_CALENDAR
								, ICLOUD_USERNAME
								, ICLOUD_PASSWORD
								, THEME
								, DATE_FORMAT
								, TIME_FORMAT
								, LANG
								, CURRENCY_ID
								, TIMEZONE_ID
								, SAVE_QUERY
								, GROUP_TABS
								, SUBPANEL_TABS
								, EXTENSION
								, SMS_OPT_IN
								, PICTURE
								, MAIL_SMTPSERVER
								, MAIL_SMTPPORT
								, MAIL_SMTPAUTH_REQ
								, MAIL_SMTPSSL
								, MAIL_SENDTYPE
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("UserActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

