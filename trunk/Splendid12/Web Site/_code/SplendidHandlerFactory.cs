/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Web.SessionState;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Reflection;

// http://www.codeproject.com/KB/cpp/WebserviceAndJavaProxy.aspx
// http://svn.assembla.com/svn/bankadvisor/trunk/BankAdvisor.Web/ScriptServiceDiscoveryHandler.cs
namespace SplendidCRM
{
	public class SplendidHandlerFactory : IHttpHandlerFactory
	{
		private static System.Reflection.Assembly ajaxAssembly = null;

		private IHttpHandlerFactory _webServiceHandlerFactory = null;
		private IHttpHandlerFactory _reportHandlerFactory = new ReportStreamHandlerFactory();

		public SplendidHandlerFactory()
		{
			if ( ajaxAssembly == null )
			{
				#pragma warning disable 618
				ajaxAssembly = Assembly.LoadWithPartialName("System.Web.Extensions");
				#pragma warning restore 618
			}
			try
			{
				_webServiceHandlerFactory = (IHttpHandlerFactory) System.Activator.CreateInstance(ajaxAssembly.GetType("System.Web.Script.Services.ScriptHandlerFactory"));
			}
			catch
			{
				_webServiceHandlerFactory = new WebServiceHandlerFactory();
			}
		}

		public virtual IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
		{
			IHttpHandlerFactory factory = null;
			// 12/09/2009 Paul.  SQL Server Reporting Services has a number of Execution options that side-step the SOAP Service. 
			if ( ReportStreamHandlerFactory.IsStreamedOperation(context) )
			{
				factory = _reportHandlerFactory;
			}
			else
			{
				factory = _webServiceHandlerFactory;
				// 12/10/2009 Paul.  ReportBuilder will assume that SharePoint is installed when trying to authenticate. 
				// We want to avoid having to map ~/_vti_bin/ReportServer, so just rewrite the URL. 
				// 12/11/2009 Paul.  The request for ReportExecution2005.asmx has a lowercase folder. 
				if ( url.Contains("/_vti_bin/") )
				{
					url = url.Replace("/_vti_bin", "");
				}
			}
			
			IHttpHandler originalHandler = factory.GetHandler(context, requestType, url, pathTranslated);
			bool bRequiresSessionState = originalHandler is IRequiresSessionState;
			if ( originalHandler is IHttpAsyncHandler )
			{
				if ( bRequiresSessionState )
				{
					return new AsyncHandlerWrapperWithSession(originalHandler, factory);
				}
				return new AsyncHandlerWrapper(originalHandler, factory);
			}
			else
			{
				if ( bRequiresSessionState )
				{
					return new HandlerWrapperWithSession(originalHandler, factory);
				}
				return new HandlerWrapper(originalHandler, factory);
			}
		}

		public virtual void ReleaseHandler(IHttpHandler handler)
		{
			if ( handler == null )
			{
				throw new ArgumentNullException("handler");
			}
			((HandlerWrapper)handler).ReleaseHandler();
		}

		#region HandlerWrappers
		private class AsyncHandlerWrapper : SplendidHandlerFactory.HandlerWrapper, IHttpAsyncHandler, IHttpHandler
		{
			internal AsyncHandlerWrapper(IHttpHandler originalHandler, IHttpHandlerFactory originalFactory) : base(originalHandler, originalFactory)
			{
			}

			public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
			{
				return ((IHttpAsyncHandler)base._originalHandler).BeginProcessRequest(context, cb, extraData);
			}

			public void EndProcessRequest(IAsyncResult result)
			{
				((IHttpAsyncHandler)base._originalHandler).EndProcessRequest(result);
			}
		}

		private class AsyncHandlerWrapperWithSession : SplendidHandlerFactory.AsyncHandlerWrapper, IRequiresSessionState
		{
			internal AsyncHandlerWrapperWithSession(IHttpHandler originalHandler, IHttpHandlerFactory originalFactory) : base(originalHandler, originalFactory)
			{
			}
		}

		internal class HandlerWrapper : IHttpHandler
		{
			private IHttpHandlerFactory _originalFactory;
			protected IHttpHandler _originalHandler;

			internal HandlerWrapper(IHttpHandler originalHandler, IHttpHandlerFactory originalFactory)
			{
				this._originalFactory = originalFactory;
				this._originalHandler = originalHandler;
			}

			public void ProcessRequest(HttpContext context)
			{
				this._originalHandler.ProcessRequest(context);
			}

			internal void ReleaseHandler()
			{
				this._originalFactory.ReleaseHandler(this._originalHandler);
			}

			public bool IsReusable
			{
				get
				{
					return this._originalHandler.IsReusable;
				}
			}
		}

		internal class HandlerWrapperWithSession : SplendidHandlerFactory.HandlerWrapper, IRequiresSessionState
		{
			internal HandlerWrapperWithSession(IHttpHandler originalHandler, IHttpHandlerFactory originalFactory) : base(originalHandler, originalFactory)
			{
			}
		}
		#endregion
	}
}

