/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for AmazonUtils.
	/// </summary>
	public class AmazonUtils
	{
		public static DateTime AWSTimestamp(DateTime dtNow)
		{
			DateTime dt = dtNow.ToUniversalTime();
			DateTime dtUniversal = new DateTime
				( dt.Year
				, dt.Month
				, dt.Day
				, dt.Hour
				, dt.Minute
				, dt.Second
				, dt.Millisecond
				);
			return dtUniversal;
		}

		public static string ISOTimestamp(DateTime dtNow)
		{
			DateTime dtUniversal = dtNow.ToUniversalTime();
			return dtUniversal.ToString("yyyy-MM-ddTHH:mm:ss.fffZ", System.Globalization.CultureInfo.InvariantCulture);
		}

		public static string S3OperationSignature(string sSecretAccessKey, string operation, DateTime dtNow)
		{
			UTF8Encoding utf8 = new UTF8Encoding();

			string sSignature        = "AmazonS3" + operation + ISOTimestamp(dtNow);
			byte[] bySignature       = utf8.GetBytes(sSignature.ToCharArray());
			byte[] bySecretAccessKey = utf8.GetBytes(sSecretAccessKey);

			// 02/07/2010 Paul.  Defensive programming, the hash as a dispose interface, so lets use it. 
			using ( HMACSHA1 sha1 = new HMACSHA1(bySecretAccessKey) )
			{
				return Convert.ToBase64String(sha1.ComputeHash(bySignature));
			}
		}

		public static string S3ObjectUri(string sAccessKeyID, string sSecretAccessKey, bool bVirtualHosting, string sBucket, string sKey, DateTime dtExpires)
		{
			UTF8Encoding utf8 = new UTF8Encoding();
			DateTime dtUniversal1970    = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			DateTime dtUniversalExpires = dtExpires.ToUniversalTime();
			TimeSpan tsExpires = dtUniversalExpires - dtUniversal1970;
			int nExpiresSeconds = Convert.ToInt32(tsExpires.TotalSeconds);

			UriBuilder ub = null;
			if ( bVirtualHosting )
				ub = new UriBuilder("http://" + sBucket);
			else
				ub = new UriBuilder( "http://s3.amazonaws.com/" + sBucket);
			ub.Path += "/" + sKey;

			string sSignature = "GET\n\n\n" + nExpiresSeconds.ToString() + "\n" + ub.Path;
			byte[] bySignature       = utf8.GetBytes(sSignature.ToCharArray());
			byte[] bySecretAccessKey = utf8.GetBytes(sSecretAccessKey);

			// 02/07/2010 Paul.  Defensive programming, the hash as a dispose interface, so lets use it. 
			using ( HMACSHA1 sha1 = new HMACSHA1(bySecretAccessKey) )
			{
				string sSignatureHash = Convert.ToBase64String(sha1.ComputeHash(bySignature));
				ub.Query = "AWSAccessKeyId=" + sAccessKeyID + "&Signature=" + HttpUtility.UrlEncode(sSignatureHash) + "&Expires=" + nExpiresSeconds.ToString();
				return ub.Uri.AbsoluteUri;
			}
		}

		public static string S3ObjectUri(bool bVirtualHosting, string sBucket, string sKey)
		{
			UriBuilder ub = null;
			if ( bVirtualHosting )
				ub = new UriBuilder("http://" + sBucket);
			else
				ub = new UriBuilder( "http://s3.amazonaws.com/" + sBucket);
			ub.Path += "/" + sKey;
			return ub.Uri.AbsoluteUri;
		}

		public static byte[] MakeCertPKCS12(string sX509PrivateKey, string sX509Certificate, string sPassword)
		{
			Mono.Security.X509.PKCS12 mPKCS12 = new Mono.Security.X509.PKCS12();
			if ( !String.IsNullOrEmpty(sPassword) )
				mPKCS12.Password = sPassword;

			sX509Certificate = sX509Certificate.Trim();
			if ( !String.IsNullOrEmpty(sX509Certificate) )
			{
				const string sCertHeader = "-----BEGIN CERTIFICATE-----";
				const string sCertFooter = "-----END CERTIFICATE-----";
				if (sX509Certificate.StartsWith(sCertHeader) && sX509Certificate.EndsWith(sCertFooter))
				{
					sX509Certificate = sX509Certificate.Substring(sCertHeader.Length, sX509Certificate.Length - sCertHeader.Length - sCertFooter.Length);
					byte[] byPKS8  = Convert.FromBase64String(sX509Certificate.Trim());
					
					Mono.Security.X509.X509Certificate x509 = new Mono.Security.X509.X509Certificate(byPKS8);
					mPKCS12.AddCertificate(x509);
				}
				else
				{
					throw(new Exception("Invalid X509 Certificate.  Missing BEGIN CERTIFICATE or END CERTIFICATE."));
				}
			}

			sX509PrivateKey = sX509PrivateKey.Trim();
			if ( !String.IsNullOrEmpty(sX509PrivateKey) )
			{
				const string sPemHeader = "-----BEGIN PRIVATE KEY-----";
				const string sPemFooter = "-----END PRIVATE KEY-----";
				if (sX509PrivateKey.StartsWith(sPemHeader) && sX509PrivateKey.EndsWith(sPemFooter))
				{
					sX509PrivateKey = sX509PrivateKey.Substring(sPemHeader.Length, sX509PrivateKey.Length - sPemHeader.Length - sPemFooter.Length);
					byte[] byPKS8  = Convert.FromBase64String(sX509PrivateKey.Trim());
					
					PKCS8.PrivateKeyInfo mPKS8 = new PKCS8.PrivateKeyInfo(byPKS8);
					RSA mRSA = PKCS8.PrivateKeyInfo.DecodeRSA(mPKS8.PrivateKey);
					mPKCS12.AddPkcs8ShroudedKeyBag(mRSA);
				}
				else
				{
					throw(new Exception("Invalid X509 Private Key.  Missing BEGIN PRIVATE KEY or END PRIVATE KEY."));
				}
			}
			return mPKCS12.GetBytes();
		}

		public class CaseSensitiveComparer: IComparer
		{
			public int Compare(object x, object y)
			{
				return String.Compare(x as string, y as string, StringComparison.Ordinal);
			}
		}

		public static SortedList UriQueryParameters(string sQuery)
		{
			SortedList lst = new SortedList(new CaseSensitiveComparer());
			if ( sQuery.StartsWith("?") )
				sQuery = sQuery.Substring(1);
			string[] arrParams = sQuery.Split('&');
			for ( int i = 0; i < arrParams.Length; i++ )
			{
				string   sPair   = arrParams[i];
				string[] arrPair = sPair.Split('=');
				string   sKey    = arrPair[0];
				// 10/15/2007 Paul.  Any existing signature should be ignored. 
				if ( sKey == "awsSignature" )
					continue;
				
				// 10/15/2007 Paul.  Amazon parameters are always name-value pairs.  Skip all else. 
				if ( arrPair.Length > 1 )
				{
					string sValue = HttpUtility.UrlDecode(arrPair[1]);
					if ( !lst.ContainsKey(sKey) )
					{
						ArrayList arrValues = new ArrayList();
						arrValues.Add(sValue);
						lst.Add(sKey, arrValues);
					}
					else
					{
						ArrayList arrValues = lst[sKey] as ArrayList;
						arrValues.Add(sValue);
					}
				}
			}
			return lst;
		}

		public static string UriSortedQueryString(string sQuery)
		{
			StringBuilder sb = new StringBuilder();
			SortedList lst = UriQueryParameters(sQuery);
			if ( lst.Count > 0 )
			{
				for ( int i = 0; i < lst.Count; i++ )
				{
					string sKey = lst.GetKey(i) as string;
					ArrayList arrValues = lst[sKey] as ArrayList;
					for ( int j = 0; j < arrValues.Count; j++ )
					{
						string sValue = arrValues[j] as string;
						if ( sb.Length > 0 )
							sb.Append("&");
						sb.Append(sKey);
						sb.Append("=");
						string sEncodedValue = HttpUtility.UrlEncode(sValue);
						// 10/15/2007 Paul.  Hex encoded data must use upper-case hex characters. 
						string[] arrEncoded = sEncodedValue.Split('%');
						StringBuilder sbValue = new StringBuilder(arrEncoded[0]);
						for ( int k = 1; k < arrEncoded.Length; k++ )
						{
							sbValue.Append('%');
							sbValue.Append(arrEncoded[k].Substring(0, 2).ToUpper() + arrEncoded[k].Substring(2, arrEncoded[k].Length - 2));
						}
						sb.Append(sbValue.ToString());
					}
				}
			}
			return sb.ToString();
		}

		public static string AmazonUriSignature(Uri u, string sSecretAccessKey)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(u.AbsolutePath);
			sb.Append("?");
			sb.Append(UriSortedQueryString(u.Query));

			UTF8Encoding utf8 = new UTF8Encoding();

			byte[] bySignature       = utf8.GetBytes(sb.ToString().ToCharArray());
			byte[] bySecretAccessKey = utf8.GetBytes(sSecretAccessKey);

			// 02/07/2010 Paul.  Defensive programming, the hash as a dispose interface, so lets use it. 
			using ( HMACSHA1 sha1 = new HMACSHA1(bySecretAccessKey) )
			{
				return Convert.ToBase64String(sha1.ComputeHash(bySignature));
			}
		}
	}
}
