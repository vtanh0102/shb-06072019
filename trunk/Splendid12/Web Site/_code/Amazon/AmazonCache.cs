/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Data;
using Mono.Security.Cryptography;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for AmazonCache.
	/// </summary>
	public partial class AmazonCache
	{
		public static string AmazonAccessKeyID()
		{
			return Sql.ToString(HttpContext.Current.Application["CONFIG.AmazonAWS.AccessKeyID"]);
		}

		public static string AmazonSecretAccessKey()
		{
			return Sql.ToString(HttpContext.Current.Application["CONFIG.AmazonAWS.SecretAccessKey"]);
		}

		// 02/23/2011 Paul.  Move creation to the appropriate utility file. 
		
		// 10/15/2007 Paul.  Cache the Amazon data, but for a very short period of time. 
		public static DateTime DefaultCacheExpiration()
		{
			return DateTime.Now.AddMinutes(1);
		}
	}
}
