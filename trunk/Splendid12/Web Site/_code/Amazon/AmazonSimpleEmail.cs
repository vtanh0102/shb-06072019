/**
 * Copyright (C) 2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Web;
using System.Text;
using System.Data;
using System.Collections.Generic;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

namespace SplendidCRM
{
	// 02/23/2011 Paul.  Move creation to the appropriate utility file. 
	public partial class AmazonCache
	{
		public static string AmazonEmailServiceURL()
		{
			return Sql.ToString(HttpContext.Current.Application["CONFIG.AmazonSES.ServiceURL"]);
		}

		public static AmazonSimpleEmail CreateAmazonSimpleEmail()
		{
			AmazonSimpleEmail ses = new AmazonSimpleEmail(AmazonAccessKeyID(), AmazonSecretAccessKey(), AmazonEmailServiceURL());
			return ses;
		}
	}

	/// <summary>
	/// Summary description for AmazonSimpleStorage.
	/// </summary>
	public class AmazonSimpleEmail
	{
		private AmazonSimpleEmailServiceClient ses;

		public AmazonSimpleEmail(string sAccessKeyID, string sSecretAccessKey, string sServiceURL)
		{
			AmazonSimpleEmailServiceConfig clientConfig = new AmazonSimpleEmailServiceConfig();
			if ( !Sql.IsEmptyString(sServiceURL) )
				clientConfig.ServiceURL = sServiceURL;
			ses = new AmazonSimpleEmailServiceClient(sAccessKeyID, sSecretAccessKey, clientConfig);
		}

		public DataTable ListVerifiedEmailAddresses()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("EMAIL_ADDRESS", Type.GetType("System.String"));
			
			ListVerifiedEmailAddressesRequest req = new ListVerifiedEmailAddressesRequest();
			ListVerifiedEmailAddressesResponse oResult = ses.ListVerifiedEmailAddresses(req);
			
			if ( oResult.ListVerifiedEmailAddressesResult != null && oResult.ListVerifiedEmailAddressesResult.VerifiedEmailAddresses != null )
			{
				foreach ( string sEmailAddress in oResult.ListVerifiedEmailAddressesResult.VerifiedEmailAddresses )
				{
					DataRow row = dt.NewRow();
					dt.Rows.Add(row);
					row["EMAIL_ADDRESS"] = sEmailAddress;
				}
			}
			return dt;
		}

		public void VerifyEmailAddress(string sEmailAddress)
		{
			VerifyEmailAddressRequest req = new VerifyEmailAddressRequest();
			req.EmailAddress = sEmailAddress;
			VerifyEmailAddressResponse oResult = ses.VerifyEmailAddress(req);
		}

		public void DeleteVerifiedEmailAddress(string sEmailAddress)
		{
			DeleteVerifiedEmailAddressRequest req = new DeleteVerifiedEmailAddressRequest();
			req.EmailAddress = sEmailAddress;
			DeleteVerifiedEmailAddressResponse oResult = ses.DeleteVerifiedEmailAddress(req);
		}

		public void GetSendQuota(ref double dMax24HourSend, ref double dMaxSendRate, ref double dSentLast24Hours)
		{
			GetSendQuotaRequest req = new GetSendQuotaRequest();
			GetSendQuotaResponse oResult = ses.GetSendQuota(req);
			if ( oResult.GetSendQuotaResult != null )
			{
				dMax24HourSend   = oResult.GetSendQuotaResult.Max24HourSend  ;
				dMaxSendRate     = oResult.GetSendQuotaResult.MaxSendRate    ;
				dSentLast24Hours = oResult.GetSendQuotaResult.SentLast24Hours;
			}
		}

		public DataTable GetSendStatistics()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("BOUNCES"          , Type.GetType("System.Int64"   ));
			dt.Columns.Add("COMPLAINTS"       , Type.GetType("System.Int64"   ));
			dt.Columns.Add("DELIVERY_ATTEMPTS", Type.GetType("System.Int64"   ));
			dt.Columns.Add("REJECTS"          , Type.GetType("System.Int64"   ));
			dt.Columns.Add("TIMESTAMP"        , Type.GetType("System.DateTime"));
			
			GetSendStatisticsRequest req = new GetSendStatisticsRequest();
			GetSendStatisticsResponse oResult = ses.GetSendStatistics(req);
			if ( oResult.GetSendStatisticsResult != null && oResult.GetSendStatisticsResult.SendDataPoints != null )
			{
				foreach ( SendDataPoint dp in oResult.GetSendStatisticsResult.SendDataPoints )
				{
					DataRow row = dt.NewRow();
					dt.Rows.Add(row);
					row["BOUNCES"          ] = dp.Bounces         ;
					row["COMPLAINTS"       ] = dp.Complaints      ;
					row["DELIVERY_ATTEMPTS"] = dp.DeliveryAttempts;
					row["REJECTS"          ] = dp.Rejects         ;
					row["TIMESTAMP"        ] = dp.Timestamp       ;
				}
			}
			return dt;
		}

		// Source:            The sender's email address.
		// ReturnPath:        The email address to which bounce notifications are to be forwarded. If the
		//                    message cannot be delivered to the recipient, then an error message will
		//                    be returned from the recipient's ISP; this message will then be forwarded
		//                    to the email address specified by the ReturnPath parameter.
		// ReplyToAddresses:  The reply-to email address(es) for the message. If the recipient replies
		//                    to the message, each reply-to address will receive the reply.
		// Destination:       The destination for this email, composed of To:, From:, and CC: fields.
		// Message:           The message to be sent.
		public string SendEmail(string sSource, string sReturnPath, List<string> lstReplyToAddresses, List<string> lstToAddresses, List<string> lstCcAddresses, List<string> lstBccAddresses, string sSubject, string sBody)
		{
			SendEmailRequest req = new SendEmailRequest();
			req.Source                    = sSource    ;
			// 02/24/2011 Paul.  Must provide ReturnPath, otherwise we get an Illegal address error. 
			req.ReturnPath                = sReturnPath;
			req.ReplyToAddresses          = lstReplyToAddresses;
			req.Destination               = new Destination();
			req.Destination.ToAddresses   = lstToAddresses;
			req.Destination.CcAddresses   = lstCcAddresses;
			req.Destination.BccAddresses  = lstBccAddresses;
			req.Message                   = new Message();
			req.Message.Subject           = new Content();
			req.Message.Subject.Charset   = "utf-8" ;
			req.Message.Subject.Data      = sSubject;
			req.Message.Body              = new Body();
			req.Message.Body.Html         = new Content();
			req.Message.Body.Html.Charset = "utf-8" ;
			req.Message.Body.Html.Data    = sBody   ;
			
			SendEmailResponse oResult = ses.SendEmail(req);
			// 02/24/2011 Paul.  The Message ID is 60 characters. 
			// 0000012e594cdbc2-b55fed51-9157-4586-b965-3075f4df7297-000000
			string sMessageId = (oResult.SendEmailResult != null) ? oResult.SendEmailResult.MessageId : String.Empty;
			return sMessageId;
		}

		public string SendEmail(string sSource, string sDestination, string sSubject, string sBody)
		{
			List<string> lstReplyToAddresses = new List<string>();
			List<string> lstToAddresses      = new List<string>();
			List<string> lstCcAddresses      = new List<string>();
			List<string> lstBccAddresses     = new List<string>();
			lstToAddresses.Add(sDestination);
			// 02/24/2011 Paul.  Must provide ReturnPath, otherwise we get an Illegal address error. 
			return SendEmail(sSource, sSource, lstReplyToAddresses, lstToAddresses, lstCcAddresses, lstBccAddresses, sSubject, sBody);
		}

		// Source:        The sender's email address.
		// Destinations:  A list of destinations for the message.
		// RawMessage:    The raw text of the message. The client is responsible for ensuring the following:
		//                Message must contain a header and a body, separated by a blank line. All
		//                required header fields must be present. Each part of a multipart MIME message
		//                must be formatted properly. MIME content types must be among those supported
		//                by Amazon SES. Refer to the Amazon SES Developer Guide for more details.
		//                Content must be base64-encoded, if MIME requires it.
		public string SendRawEmail(string sSource, List<string> lstDestinations, string sRawMessage)
		{
			UTF8Encoding utf8 = new UTF8Encoding();
			SendRawEmailRequest req = new SendRawEmailRequest();
			req.Source          = sSource        ;
			req.Destinations    = lstDestinations;
			req.RawMessage      = new RawMessage();
			req.RawMessage.Data = new MemoryStream(utf8.GetBytes(sRawMessage));
			
			SendRawEmailResponse oResult = ses.SendRawEmail(req);
			string sMessageId = (oResult.SendRawEmailResult != null) ? oResult.SendRawEmailResult.MessageId : String.Empty;
			return sMessageId;
		}
	}
}
