/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.IO;
using System.Xml;
using System.Data;
using System.Text;
using System.Collections;
using Microsoft.Web.Services2;
// 10/06/2007 Paul.  Prepare to use version based on Microsoft.Web.Services2. 
//using SplendidCRM.amazonaws.s3;
using SplendidCRM.Amazon;

namespace SplendidCRM
{
	// 02/23/2011 Paul.  Move creation to the appropriate utility file. 
	public partial class AmazonCache
	{
		public static bool AmazonVirtualHosting()
		{
			return Sql.ToBoolean(HttpContext.Current.Application["CONFIG.AmazonAWS.VirtualHosting"]);
		}

		public static AmazonSimpleStorage CreateAmazonSimpleStorage()
		{
			AmazonSimpleStorage s3 = new AmazonSimpleStorage(AmazonAccessKeyID(), AmazonSecretAccessKey(), AmazonVirtualHosting());
			return s3;
		}
	}

	/// <summary>
	/// Summary description for AmazonSimpleStorage.
	/// </summary>
	public class AmazonSimpleStorage
	{
		private AmazonS3 s3;
		private string m_sAccessKeyID    ;
		private string m_sSecretAccessKey;
		private bool   m_bVirtualHosting ;

		// The HeaderOutputFilter class is used to strip specific headers
		// from a SOAP envelope.  This class is needed because not all of
		// the headers used by WSE 2.0 are required for an S3 request.
		// 
		// Remember, we are only using WSE 2.0 for DIME support.
		// WS-Security, WS-Addressing, and WS-Policy are not needed.
		//

		// Used to filter header nodes.
		public class HeaderOutputFilter : SoapOutputFilter
		{
			private string filterString;

			public HeaderOutputFilter(string filterString)
			{
				this.filterString = filterString;
			}

			// Override the ProcessMessage method.
			public override void ProcessMessage(SoapEnvelope envelope)
			{
				if (envelope == null)
					throw new ArgumentNullException("No envelope!");

				if (envelope.Header != null)
				{
					XmlNodeList nodeList = envelope.Header.ChildNodes;

					ArrayList removeList = new ArrayList();

					foreach (XmlNode node in nodeList)
					{
						if ( node is XmlElement )
						{
							// Check for any headers that belong to WSE.
							if (node.Name.IndexOf(this.filterString) == 0)
							{
								removeList.Add(node);
							}
						}
					}
					// Remove headers. 
					foreach (XmlNode node in removeList)
					{
						envelope.Header.RemoveChild(node);
					}
				}
			}
		}

		public AmazonSimpleStorage(object oAccessKeyID, object oSecretAccessKey, object oVirtualHosting) : this(Sql.ToString(oAccessKeyID), Sql.ToString(oSecretAccessKey), Sql.ToBoolean(oVirtualHosting))
		{
		}

		public AmazonSimpleStorage(string sAccessKeyID, string sSecretAccessKey, bool bVirtualHosting)
		{
			s3 = new AmazonS3();
			m_sAccessKeyID     = sAccessKeyID    ;
			m_sSecretAccessKey = sSecretAccessKey;
			m_bVirtualHosting  = bVirtualHosting ;

			// Remove the standard WSE soap headers.
			s3.Pipeline.OutputFilters.Remove(typeof(Microsoft.Web.Services2.Security.SecurityOutputFilter));
			s3.Pipeline.OutputFilters.Remove(typeof(Microsoft.Web.Services2.Referral.ReferralOutputFilter));
			s3.Pipeline.OutputFilters.Remove(typeof(Microsoft.Web.Services2.Policy.PolicyEnforcementOutputFilter));

			// Add our custom filter to remove the unwanted WSE soap headers.
			s3.Pipeline.OutputFilters.Add(new HeaderOutputFilter("wsa:"));
		}

		public void SetBucketGroupACL(string sBucket, string sGroupURI, Permission enumPermission, bool bAllow)
		{
			// 10/20/2007 Paul.  Get the existing policy and add to it. 
			DateTime dtNow = DateTime.Now;
			AccessControlPolicy oExistingPolicy = s3.GetBucketAccessControlPolicy(sBucket, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetBucketAccessControlPolicy", dtNow), null);
			// 10/04/2007 Paul.  Code defensively.  If the result is not returned, just create an empty policy.
			if ( oExistingPolicy == null )
				oExistingPolicy = new AccessControlPolicy();
			if ( oExistingPolicy.AccessControlList == null )
				oExistingPolicy.AccessControlList = new Grant[0];
			
			bool bAlreadyAllow = false;
			for ( int i = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
			{
				Grant grt = oExistingPolicy.AccessControlList[i];
				if ( grt.Grantee is Group )
				{
					Group grp = grt.Grantee as Group;
					// 10/26/2007 Paul.  Match both the URI and the permission. 
					if ( grp.URI == sGroupURI && grt.Permission == enumPermission )
						bAlreadyAllow = true;
				}
			}
			if ( bAllow )
			{
				if ( !bAlreadyAllow )
				{
					AccessControlPolicy oNewPolicy = new AccessControlPolicy();
					oNewPolicy.AccessControlList = new Grant[oExistingPolicy.AccessControlList.Length+1];
					for ( int i = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
					{
						oNewPolicy.AccessControlList[i] = oExistingPolicy.AccessControlList[i];
					}
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length] = new Grant();
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Permission = enumPermission;
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Grantee = new Group();
					(oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Grantee as Group).URI = sGroupURI;
					
					dtNow = DateTime.Now;
					s3.SetBucketAccessControlPolicy(sBucket, oNewPolicy.AccessControlList, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "SetBucketAccessControlPolicy", dtNow), null);
				}
			}
			else
			{
				if ( bAlreadyAllow )
				{
					AccessControlPolicy oNewPolicy = new AccessControlPolicy();
					oNewPolicy.AccessControlList = new Grant[oExistingPolicy.AccessControlList.Length-1];
					for ( int i = 0, j = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
					{
						Grant grt = oExistingPolicy.AccessControlList[i];
						if ( grt.Grantee is Group )
						{
							Group grp = grt.Grantee as Group;
							// 10/26/2007 Paul.  Match both the URI and the permission. 
							if ( grp.URI == sGroupURI && grt.Permission == enumPermission )
								continue;
						}
						oNewPolicy.AccessControlList[j++] = oExistingPolicy.AccessControlList[i];
					}
					dtNow = DateTime.Now;
					s3.SetBucketAccessControlPolicy(sBucket, oNewPolicy.AccessControlList, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "SetBucketAccessControlPolicy", dtNow), null);
				}
			}
		}

		public void SetBucketLoggingACL(string sTargetBucket, string sLoggingBucket, bool bAllow)
		{
			// http://docs.amazonwebservices.com/AmazonS3/2006-03-01/LogDelivery.html
			// 10/24/2007 Paul.  In order to enable logging, the LogDeliver group needs read access to the target bucket and write access to the logging bucket. 
			SetBucketGroupACL(sTargetBucket , "http://acs.amazonaws.com/groups/s3/LogDelivery", Permission.READ_ACP, bAllow);
			SetBucketGroupACL(sLoggingBucket, "http://acs.amazonaws.com/groups/s3/LogDelivery", Permission.WRITE   , bAllow);
			// 10/26/2007 Paul.  Add READ_ACP to the logging bucket. 
			SetBucketGroupACL(sLoggingBucket, "http://acs.amazonaws.com/groups/s3/LogDelivery", Permission.READ_ACP, bAllow);
		}

		public void SetBucketAccessControlPolicy(string sBucket, bool bPublic)
		{
			// 10/20/2007 Paul.  Get the existing policy and add to it. 
			SetBucketGroupACL(sBucket , "http://acs.amazonaws.com/groups/global/AllUsers", Permission.READ, bPublic);
		}

		public void CreateBucket(string sBucket, bool bPublic)
		{
			DateTime dtNow = DateTime.Now;
			s3.CreateBucket(sBucket, null, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "CreateBucket", dtNow));

			SetBucketAccessControlPolicy(sBucket, bPublic);
		}

		public void SetBucketLoggingStatus(string sBucket, string sTargetBucket, string sTargetPrefix)
		{
			BucketLoggingStatus oLoggingStatus = null;
			// 10/04/2007 Paul.  The bucket is required, the prefix is not.  
			// If not bucket is provided, then this is a remove operation. 
			if ( !String.IsNullOrEmpty(sTargetBucket) )
			{
				oLoggingStatus = new BucketLoggingStatus();
				oLoggingStatus.LoggingEnabled = new LoggingSettings();
				oLoggingStatus.LoggingEnabled.TargetBucket = sTargetBucket;
				oLoggingStatus.LoggingEnabled.TargetPrefix = sTargetPrefix;
			}

			DateTime dtNow = DateTime.Now;
			s3.SetBucketLoggingStatus(sBucket, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "SetBucketLoggingStatus", dtNow), String.Empty, oLoggingStatus);
		}

		public void GetBucketLoggingStatus(string sBucket, ref string sTargetBucket, ref string sTargetPrefix)
		{
			sTargetBucket = String.Empty;
			sTargetPrefix = String.Empty;

			DateTime dtNow = DateTime.Now;
			BucketLoggingStatus oLoggingStatus = s3.GetBucketLoggingStatus(sBucket, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetBucketLoggingStatus", dtNow), null);
			if ( oLoggingStatus != null && oLoggingStatus.LoggingEnabled != null )
			{
				sTargetBucket = oLoggingStatus.LoggingEnabled.TargetBucket;
				sTargetPrefix = oLoggingStatus.LoggingEnabled.TargetPrefix;
			}
		}

		public DataTable GetBucketAccessControlPolicy(string sBucket, ref string sOwnerName, ref string sOwnerType)
		{
			DateTime dtNow = DateTime.Now;
			AccessControlPolicy oPolicy = s3.GetBucketAccessControlPolicy(sBucket, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetBucketAccessControlPolicy", dtNow), null);
			
			sOwnerName = oPolicy.Owner.DisplayName;
			sOwnerType = oPolicy.Owner.ToString() ;

			DataTable dt = new DataTable();
			dt.Columns.Add("GRANTEE_NAME", Type.GetType("System.String"));
			dt.Columns.Add("GRANTEE_TYPE", Type.GetType("System.String"));
			dt.Columns.Add("PERMISSION"  , Type.GetType("System.String"));
			foreach ( Grant grant in oPolicy.AccessControlList )
			{
				DataRow row = dt.NewRow();
				dt.Rows.Add(row);
				row["GRANTEE_TYPE"] = grant.Grantee   ;
				row["PERMISSION"  ] = grant.Permission;
				if ( grant.Grantee is CanonicalUser )
					row["GRANTEE_NAME"] = (grant.Grantee as CanonicalUser).DisplayName;
				if ( grant.Grantee is Group )
					row["GRANTEE_NAME"] = (grant.Grantee as Group).URI;
			}
			return dt;
		}

		public void DeleteBucket(string sBucket)
		{
			DateTime dtNow = DateTime.Now;
			Status oResult = s3.DeleteBucket(sBucket, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "DeleteBucket", dtNow), null);
			if ( oResult.Code >= 400 )
				throw(new Exception("AmazonS3.DeleteObject: error " + oResult.Code.ToString() + ": " + oResult.Description));
		}

		public DataTable ListAllMyBuckets()
		{
			DateTime dtNow = DateTime.Now;
			ListAllMyBucketsResult oBucketsResult = s3.ListAllMyBuckets(m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "ListAllMyBuckets", dtNow));
			
			DataTable dt = new DataTable();
			dt.Columns.Add("BUCKET"      , Type.GetType("System.String"  ));
			dt.Columns.Add("DATE_ENTERED", Type.GetType("System.DateTime"));

			foreach ( ListAllMyBucketsEntry bucket in oBucketsResult.Buckets )
			{
				dtNow = DateTime.Now;
				DataRow row = dt.NewRow();
				dt.Rows.Add(row);
				row["BUCKET"      ] = bucket.Name;
				// 10/04/2007 Paul.  Date is returned in UniversalTime, so convert to LocalTime (we call this ServerTime).
				row["DATE_ENTERED"] = bucket.CreationDate.ToLocalTime();
			}
			return dt;
		}

		public void DeleteFolder(string sBucket, string sKey)
		{
			// 10/02/2007 Paul.  There are no real folders in S3.  
			// At some point we may recurse the folders and delete all data, but for now, just delete the empty directory file. 
			sKey += "DirectoryDirectoryDirectory.dir";
			DeleteObject(sBucket, sKey);
		}

		public void SetObjectAccessControlPolicy(string sBucket, string sKey, bool bPublic)
		{
			// 10/04/2007 Paul.  Get the existing policy and add to it. 
			DateTime dtNow = DateTime.Now;
			AccessControlPolicy oExistingPolicy = s3.GetObjectAccessControlPolicy(sBucket, sKey, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetObjectAccessControlPolicy", dtNow), null);
			// 10/04/2007 Paul.  Code defensively.  If the result is not returned, just create an empty policy.
			if ( oExistingPolicy == null )
				oExistingPolicy = new AccessControlPolicy();
			if ( oExistingPolicy.AccessControlList == null )
				oExistingPolicy.AccessControlList = new Grant[0];
			
			bool bAlreadyPublic = false;
			for ( int i = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
			{
				Grant grt = oExistingPolicy.AccessControlList[i];
				if ( grt.Grantee is Group )
				{
					Group grp = grt.Grantee as Group;
					// 10/26/2007 Paul.  Match both the URI and the permission. 
					if ( grp.URI == "http://acs.amazonaws.com/groups/global/AllUsers" && grt.Permission == Permission.READ )
						bAlreadyPublic = true;
				}
			}
			if ( bPublic )
			{
				if ( !bAlreadyPublic )
				{
					AccessControlPolicy oNewPolicy = new AccessControlPolicy();
					oNewPolicy.AccessControlList = new Grant[oExistingPolicy.AccessControlList.Length+1];
					for ( int i = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
					{
						oNewPolicy.AccessControlList[i] = oExistingPolicy.AccessControlList[i];
					}
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length] = new Grant();
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Permission = Permission.READ;
					oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Grantee = new Group();
					(oNewPolicy.AccessControlList[oExistingPolicy.AccessControlList.Length].Grantee as Group).URI = "http://acs.amazonaws.com/groups/global/AllUsers";
					
					dtNow = DateTime.Now;
					s3.SetObjectAccessControlPolicy(sBucket, sKey, oNewPolicy.AccessControlList, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "SetObjectAccessControlPolicy", dtNow), null);
				}
			}
			else
			{
				if ( bAlreadyPublic )
				{
					AccessControlPolicy oNewPolicy = new AccessControlPolicy();
					oNewPolicy.AccessControlList = new Grant[oExistingPolicy.AccessControlList.Length-1];
					for ( int i = 0, j = 0; i < oExistingPolicy.AccessControlList.Length; i++ )
					{
						Grant grt = oExistingPolicy.AccessControlList[i];
						if ( grt.Grantee is Group )
						{
							Group grp = grt.Grantee as Group;
							// 10/26/2007 Paul.  Match both the URI and the permission. 
							if ( grp.URI == "http://acs.amazonaws.com/groups/global/AllUsers" && grt.Permission == Permission.READ )
								continue;
						}
						oNewPolicy.AccessControlList[j++] = oExistingPolicy.AccessControlList[i];
					}
					dtNow = DateTime.Now;
					s3.SetObjectAccessControlPolicy(sBucket, sKey, oNewPolicy.AccessControlList, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "SetObjectAccessControlPolicy", dtNow), null);
				}
			}
		}

		public void CreateFolder(string sBucket, string sKey, bool bPublic)
		{
			DateTime dtNow = DateTime.Now;
			byte[] byData = new byte[0];
			// 10/04/2007 Paul.  Amazon does not support folders, so just create an empty file with a special folder name. 
			sKey += "/DirectoryDirectoryDirectory.dir";
			s3.PutObjectInline(sBucket, sKey, null, byData, byData.Length, null, StorageClass.STANDARD, true, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "PutObjectInline", dtNow), null);

			SetObjectAccessControlPolicy(sBucket, sKey, bPublic);
		}

		public void DeleteObject(string sBucket, string sKey)
		{
			DateTime dtNow = DateTime.Now;
			Status oResult = s3.DeleteObject(sBucket, sKey, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "DeleteObject", dtNow), null);
			// 10/04/2007 Paul.  Error code 204 also means successful.  Generally, errors are 4xx or 5xx. 
			if ( oResult.Code >= 400 )
				throw(new Exception("AmazonS3.DeleteObject: error " + oResult.Code.ToString() + ": " + oResult.Description));
		}

		public DataTable GetObjectAccessControlPolicy(string sBucket, string sKey, ref string sOwnerName, ref string sOwnerType)
		{
			DateTime dtNow = DateTime.Now;
			AccessControlPolicy oPolicy = s3.GetObjectAccessControlPolicy(sBucket, sKey, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetObjectAccessControlPolicy", dtNow), null);
			
			sOwnerName = oPolicy.Owner.DisplayName;
			sOwnerType = oPolicy.Owner.ToString() ;

			DataTable dt = new DataTable();
			dt.Columns.Add("GRANTEE_NAME", Type.GetType("System.String"));
			dt.Columns.Add("GRANTEE_TYPE", Type.GetType("System.String"));
			dt.Columns.Add("PERMISSION"  , Type.GetType("System.String"));
			foreach ( Grant grant in oPolicy.AccessControlList )
			{
				DataRow row = dt.NewRow();
				dt.Rows.Add(row);
				row["GRANTEE_TYPE"] = grant.Grantee   ;
				row["PERMISSION"  ] = grant.Permission;
				if ( grant.Grantee is CanonicalUser )
					row["GRANTEE_NAME"] = (grant.Grantee as CanonicalUser).DisplayName;
				if ( grant.Grantee is Group )
					row["GRANTEE_NAME"] = (grant.Grantee as Group).URI;
			}
			return dt;
		}

		public DataTable GetObjectMetaData(string sBucket, string sKey, ref string sStatus, ref DateTime dtLastModified)
		{
			sStatus        = String.Empty;
			dtLastModified = DateTime.MinValue;

			DateTime dtNow = DateTime.Now;
			GetObjectResult oResult = s3.GetObject(sBucket, sKey, true, false, false, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetObject", dtNow), null);

			DataTable dt = new DataTable();
			dt.Columns.Add("Name" , Type.GetType("System.String"));
			dt.Columns.Add("Value", Type.GetType("System.String"));
			if ( oResult != null )
			{
				sStatus        = oResult.Status.Description;
				dtLastModified = oResult.LastModified.ToLocalTime();
				if ( oResult.Metadata != null )
				{
					foreach ( MetadataEntry meta in oResult.Metadata )
					{
						DataRow row = dt.NewRow();
						dt.Rows.Add(row);
						row["Name" ] = meta.Name + ":";
						row["Value"] = meta.Value;
					}
				}
			}
			return dt;
		}

		public DataTable ListBucket(string sBucket, string sPrefix)
		{
			string sPreviousPrefix = String.Empty;
			if ( !Sql.IsEmptyString(sPrefix) )
			{
				if ( sPrefix == "/" )
					sPrefix = String.Empty;
				else if ( !sPrefix.EndsWith("/") )
					sPrefix += "/";

				StringBuilder sbCurrentPrefix = new StringBuilder();
				string[] arrPrefix = sPrefix.Substring(0, sPrefix.Length-1).Split('/');
				for ( int i = 0; i < arrPrefix.Length; i++ )
				{
					string sThis = arrPrefix[i];
					if ( !String.IsNullOrEmpty(sThis) )
					{
						if ( sbCurrentPrefix.Length > 0 )
							sbCurrentPrefix.Append("/");
						sbCurrentPrefix.Append(sThis);
						if ( i == arrPrefix.Length - 2 )
							sPreviousPrefix = sbCurrentPrefix.ToString() + "/";
					}
				}
			}

			DataTable dt = new DataTable();
			dt = new DataTable();
			dt.Columns.Add("ID"           , Type.GetType("System.Guid"    ));
			dt.Columns.Add("BUCKET"       , Type.GetType("System.String"  ));
			dt.Columns.Add("TYPE"         , Type.GetType("System.Int32"   ));
			dt.Columns.Add("KEY"          , Type.GetType("System.String"  ));
			dt.Columns.Add("FOLDER"       , Type.GetType("System.String"  ));
			dt.Columns.Add("NAME"         , Type.GetType("System.String"  ));
			dt.Columns.Add("MIME_TYPE"    , Type.GetType("System.String"  ));
			dt.Columns.Add("SIZE"         , Type.GetType("System.Int64"   ));
			dt.Columns.Add("LAST_MODIFIED", Type.GetType("System.DateTime"));

			if ( !Sql.IsEmptyString(sPrefix) )
			{
				DataRow row = dt.NewRow();
				dt.Rows.Add(row);
				row["BUCKET"       ] = sBucket;
				row["KEY"          ] = sPreviousPrefix;
				row["TYPE"         ] = -1;
				row["FOLDER"       ] = "..";
				row["NAME"         ] = String.Empty;
				row["MIME_TYPE"    ] = String.Empty;
				row["SIZE"         ] = DBNull.Value;
				row["LAST_MODIFIED"] = DBNull.Value;
			}

			bool   bIsTruncated = true;  // Initialize to true to start the list. 
			string sMarker = String.Empty;
			while ( bIsTruncated )
			{
				DateTime dtNow = DateTime.Now;
				ListBucketResult oResult = s3.ListBucket(sBucket, sPrefix, sMarker, 0, false, "/", m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "ListBucket", dtNow), null);
				if ( oResult.CommonPrefixes != null )
				{
					foreach ( PrefixEntry pe in oResult.CommonPrefixes )
					{
						DataRow row = dt.NewRow();
						dt.Rows.Add(row);
						row["BUCKET"       ] = sBucket;
						row["KEY"          ] = pe.Prefix;
						row["TYPE"         ] = 0;
						row["FOLDER"       ] = pe.Prefix;
						row["NAME"         ] = String.Empty;
						row["MIME_TYPE"    ] = String.Empty;
						row["SIZE"         ] = DBNull.Value;
						row["LAST_MODIFIED"] = DBNull.Value;
					}
				}
				// Exit loop of no files returned. 
				if ( oResult.Contents == null || oResult.Contents.Length == 0 )
					break;
				
				foreach (ListEntry entry in oResult.Contents)
				{
					string sFolder   = String.Empty;
					string sFileName = entry.Key;
					int nFileStart = entry.Key.LastIndexOf('/');
					if ( nFileStart >= 0 )
					{
						sFolder    = entry.Key.Substring(0, nFileStart+1);
						sFileName = entry.Key.Substring(nFileStart+1);
					}
#if !DEBUG
					if ( sFileName == "DirectoryDirectoryDirectory.dir" )
						continue;
#endif

					DataRow row = dt.NewRow();
					dt.Rows.Add(row);
					row["BUCKET"       ] = sBucket;
					row["KEY"          ] = entry.Key;
					row["TYPE"         ] = 1;
					row["FOLDER"       ] = sFolder;
					row["NAME"         ] = sFileName;
					row["MIME_TYPE"    ] = Path.GetExtension(sFileName).Length > 0 ? Path.GetExtension(sFileName).Substring(1) : String.Empty;
					row["SIZE"         ] = entry.Size;
					row["LAST_MODIFIED"] = entry.LastModified.ToLocalTime();
				}
				bIsTruncated = oResult.IsTruncated;
				sMarker      = oResult.NextMarker;
			}
			return dt;
		}

		public void PutObjectInline(string sBucket, string sKey, string sContentType, bool bPublic, int nContentLength, byte[] byData)
		{
			MetadataEntry[] meta = new MetadataEntry[1];
			meta[0] = new MetadataEntry();
			meta[0].Name  = "Content-Type";
			meta[0].Value = sContentType;
		
			// 10/02/2007 Paul.  PutObjectInline is limited to 1M. 
			// Lets get the system working, then research the best way to upload an unlimited amount. 
			// Microsoft supports DIME, but it requires a special service pack 
			// http://www.microsoft.com/downloads/details.aspx?familyid=1ba1f631-c3e7-420a-bc1e-ef18bab66122&displaylang=en
			// Tech. Note: How to use PutObject with WSE 2.0 DIME attachments
			// http://developer.amazonwebservices.com/connect/entry.jspa?externalID=689&categoryID=103
			DateTime dtNow = DateTime.Now;
			s3.PutObjectInline(sBucket, sKey, meta, byData, byData.Length, null, StorageClass.STANDARD, true, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "PutObjectInline", dtNow), null);

			SetObjectAccessControlPolicy(sBucket, sKey, bPublic);
		}

		public void PutObject(string sBucket, string sKey, string sContentType, bool bPublic, int nContentLength, Stream stm)
		{
			MetadataEntry[] meta = new MetadataEntry[1];
			meta[0] = new MetadataEntry();
			meta[0].Name  = "Content-Type";
			meta[0].Value = sContentType;

			// 10/06/2007 Paul.  Using DIME allows us to upload large files. 
			Microsoft.Web.Services2.Dime.DimeAttachment dime = new Microsoft.Web.Services2.Dime.DimeAttachment("S3Object", Microsoft.Web.Services2.Dime.TypeFormat.Unknown, stm);
			s3.RequestSoapContext.Attachments.Add(dime);

			DateTime dtNow = DateTime.Now;
			s3.PutObject(sBucket, sKey, meta, stm.Length, null, StorageClass.STANDARD, true, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "PutObject", dtNow), null);

			SetObjectAccessControlPolicy(sBucket, sKey, bPublic);
		}

		public byte[] GetObjectInline(string sBucket, string sKey, ref string sContentType)
		{
			DateTime dtNow = DateTime.Now;
			GetObjectResult oResult = s3.GetObject(sBucket, sKey, false, true, true, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetObject", dtNow), null);
			return oResult.Data;
		}

		public Stream GetObject(string sBucket, string sKey, ref string sContentType)
		{
			DateTime dtNow = DateTime.Now;
			GetObjectResult oResult = s3.GetObject(sBucket, sKey, false, true, false, m_sAccessKeyID, AmazonUtils.AWSTimestamp(dtNow), true, AmazonUtils.S3OperationSignature(m_sSecretAccessKey, "GetObject", dtNow), null);

			// 10/06/2007 Paul.  Contents of object are returned as a dime attachment.
			// Return a stream so that it can be efficiently read. 
			// 10/06/2007 Paul.  Requires changes to Web.config to allow download of large files. 
			// WSE352 Size of the record exceed its limit
			/*
				<configSections>
					<section name="microsoft.web.services2" type="Microsoft.Web.Services2.Configuration.WebServicesConfiguration, Microsoft.Web.Services2, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
				</configSections>
				<microsoft.web.services2>
					<messaging>
						<maxRequestLength>-1</maxRequestLength>
					</messaging>
				</microsoft.web.services2>
			*/
			Stream stm = null;
			if ( s3.ResponseSoapContext != null )
			{
				if ( s3.ResponseSoapContext.Attachments != null )
				{
					stm = s3.ResponseSoapContext.Attachments[0].Stream;
				}
			}
			return stm;
		}

		public string GetObjectUri(string sBucket, string sKey)
		{
			return AmazonUtils.S3ObjectUri(m_bVirtualHosting, sBucket, sKey);
		}

		public string GetObjectUri(string sBucket, string sKey, DateTime dtExpires)
		{
			return AmazonUtils.S3ObjectUri(m_sAccessKeyID, m_sSecretAccessKey, m_bVirtualHosting, sBucket, sKey, dtExpires);
		}
	}
}
