/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class Estimate : QOrder
	{
		#region Properties
		public double   TotalAmount                { get; set; }
		#endregion

		public Estimate(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtShippers, DataTable dtTaxRates, DataTable dtItems, DataTable dtCustomers, DbProviderFactory dbf, IDbConnection con) : base(qbf, qbcon, dtShippers, dtTaxRates, dtItems, dtCustomers, dbf, con, "Estimates", "Quotes", "QUOTES", "QUOTE_NUM", true)
		{
		}

		public override void Reset()
		{
			base.Reset();
			this.TotalAmount = 0.0;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = base.SetFromCRM(sID, row, sbChanges);
			// 05/31/2012 Paul.  The Name is only used for logging. 
			this.Name = Sql.ToString(row["QUOTE_NUM"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.ReferenceNumber    = Sql.ToString  (row["QUOTE_NUM"                  ]);
				this.Date               = Sql.ToDateTime(row["DATE_QUOTE_EXPECTED_CLOSED" ]);
				// 02/10/2014 Paul.  We don't use the DueDate. 
				//this.DueDate            = Sql.ToDateTime(row["DATE_QUOTE_CLOSED"          ]);
				this.TotalAmount        = Sql.ToDouble  (row["TOTAL_USDOLLAR"             ]);
			}
			else
			{
				if ( Compare(this.ReferenceNumber, row["QUOTE_NUM"                  ], "QUOTE_NUM"                 , sbChanges) ) { this.ReferenceNumber    = Sql.ToString  (row["QUOTE_NUM"                  ]);  bChanged = true; }
				if ( Compare(this.Date           , row["DATE_QUOTE_EXPECTED_CLOSED" ], "DATE_QUOTE_EXPECTED_CLOSED", sbChanges) ) { this.Date               = Sql.ToDateTime(row["DATE_QUOTE_EXPECTED_CLOSED" ]);  bChanged = true; }
				// 02/10/2014 Paul.  We don't use the DueDate. 
				//if ( Compare(this.DueDate        , row["DATE_QUOTE_CLOSED"          ], "DATE_QUOTE_CLOSED"         , sbChanges) ) { this.DueDate            = Sql.ToDateTime(row["DATE_QUOTE_CLOSED"          ]);  bChanged = true; }
				if ( Compare(this.TotalAmount    , row["TOTAL_USDOLLAR"             ], "TOTAL_USDOLLAR"            , sbChanges) ) { this.TotalAmount        = Sql.ToDouble  (row["TOTAL_USDOLLAR"             ]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.Date               = Sql.ToDateTime(row["Date"              ]);
			this.DueDate            = Sql.ToDateTime(row["DueDate"           ]);
			this.TotalAmount        = Sql.ToDouble  (row["TotalAmount"       ]);
		}

		public override string Insert()
		{
			// 06/02/2012 Paul.  Can't use CustomerName as it can include the job as part of the hierarchy. 
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( ReferenceNumber   " + ControlChars.CrLf
				//     + "     , CustomerName      " + ControlChars.CrLf
				     + "     , CustomerId        " + ControlChars.CrLf
				     + "     , Date              " + ControlChars.CrLf
				     + "     , DueDate           " + ControlChars.CrLf
				     + "     , Memo              " + ControlChars.CrLf
				     + "     , POnumber          " + ControlChars.CrLf
				     + "     , Terms             " + ControlChars.CrLf
				//     + "     , ExchangeRate      " + ControlChars.CrLf
				     + "     , Subtotal          " + ControlChars.CrLf
				     + "     , Tax               " + ControlChars.CrLf
				     + "     , TaxAgencyId       " + ControlChars.CrLf
				     + "     , TotalAmount       " + ControlChars.CrLf
				     + "     , BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity       " + ControlChars.CrLf
				     + "     , BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry    " + ControlChars.CrLf
				     + "     , ItemAggregate     " + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @ReferenceNumber   " + ControlChars.CrLf
				//     + "     , @CustomerName      " + ControlChars.CrLf
				     + "     , @CustomerId        " + ControlChars.CrLf
				     + "     , @Date              " + ControlChars.CrLf
				     + "     , @DueDate           " + ControlChars.CrLf
				     + "     , @Memo              " + ControlChars.CrLf
				     + "     , @POnumber          " + ControlChars.CrLf
				     + "     , @Terms             " + ControlChars.CrLf
				//     + "     , @ExchangeRate      " + ControlChars.CrLf
				     + "     , @Subtotal          " + ControlChars.CrLf
				     + "     , @Tax               " + ControlChars.CrLf
				     + "     , @TaxAgencyId       " + ControlChars.CrLf
				     + "     , @TotalAmount       " + ControlChars.CrLf
				     + "     , @BillingLine1      " + ControlChars.CrLf
				     + "     , @BillingLine2      " + ControlChars.CrLf
				     + "     , @BillingLine3      " + ControlChars.CrLf
				     + "     , @BillingLine4      " + ControlChars.CrLf
				     + "     , @BillingLine5      " + ControlChars.CrLf
				     + "     , @BillingCity       " + ControlChars.CrLf
				     + "     , @BillingState      " + ControlChars.CrLf
				     + "     , @BillingPostalCode " + ControlChars.CrLf
				     + "     , @BillingCountry    " + ControlChars.CrLf
				     + "     , @ItemAggregate     " + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ReferenceNumber"   , this.ReferenceNumber   ,   11);
				//Sql.AddParameter(qbcmd, "@CustomerName"      , this.CustomerName      ,   41);
				Sql.AddParameter(qbcmd, "@CustomerId"        , this.CustomerId        );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Date"              , this.Date              );
				Sql.AddParameter(qbcmd, "@DueDate"           , this.DueDate           );
				Sql.AddParameter(qbcmd, "@Memo"              , this.Memo              , 4095);
				Sql.AddParameter(qbcmd, "@POnumber"          , this.POnumber          ,   25);
				Sql.AddParameter(qbcmd, "@Terms"             , this.Terms             ,   21);
				//Sql.AddParameter(qbcmd, "@ExchangeRate"      , this.ExchangeRate      );
				Sql.AddParameter(qbcmd, "@Subtotal"          , this.Subtotal          );
				Sql.AddParameter(qbcmd, "@Tax"               , this.Tax               );
				Sql.AddParameter(qbcmd, "@TaxAgencyId"       , this.TaxAgencyId       );
				Sql.AddParameter(qbcmd, "@TotalAmount"       , this.TotalAmount       );
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				Sql.AddParameter(qbcmd, "@ItemAggregate"     , this.GetItemAggregate(dbf, con, this.LOCAL_ID, vwItems));
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			// 05/22/2012 Paul.  We need to return the of the first line item when inserting an Estimate, SalesOrder or Invoice. 
			GetFirstLineItem();
			return this.ID;
		}

		public override void Update()
		{
			// 06/02/2012 Paul.  Can't use CustomerName as it can include the job as part of the hierarchy. 
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set ReferenceNumber    = @ReferenceNumber   " + ControlChars.CrLf
				//     + "     , CustomerName       = @CustomerName      " + ControlChars.CrLf
				     + "     , CustomerId         = @CustomerId        " + ControlChars.CrLf
				     + "     , Date               = @Date              " + ControlChars.CrLf
				     + "     , DueDate            = @DueDate           " + ControlChars.CrLf
				     + "     , Memo               = @Memo              " + ControlChars.CrLf
				     + "     , POnumber           = @POnumber          " + ControlChars.CrLf
				     + "     , Terms              = @Terms             " + ControlChars.CrLf
				//     + "     , ExchangeRate       = @ExchangeRate      " + ControlChars.CrLf
				     + "     , Subtotal           = @Subtotal          " + ControlChars.CrLf
				     + "     , Tax                = @Tax               " + ControlChars.CrLf
				     + "     , TaxAgencyId        = @TaxAgencyId       " + ControlChars.CrLf
				     + "     , TotalAmount        = @TotalAmount       " + ControlChars.CrLf
				     + "     , BillingLine1       = @BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2       = @BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3       = @BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4       = @BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5       = @BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity        = @BillingCity       " + ControlChars.CrLf
				     + "     , BillingState       = @BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode  = @BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry     = @BillingCountry    " + ControlChars.CrLf
				     + " where ID                 = @ID                " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ReferenceNumber"   , this.ReferenceNumber   ,   11);
				//Sql.AddParameter(qbcmd, "@CustomerName"      , this.CustomerName      ,   41);
				Sql.AddParameter(qbcmd, "@CustomerId"        , this.CustomerId        );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Date"              , this.Date              );
				Sql.AddParameter(qbcmd, "@DueDate"           , this.DueDate           );
				Sql.AddParameter(qbcmd, "@Memo"              , this.Memo              , 4095);
				Sql.AddParameter(qbcmd, "@POnumber"          , this.POnumber          ,   25);
				Sql.AddParameter(qbcmd, "@Terms"             , this.Terms             ,   21);
				//Sql.AddParameter(qbcmd, "@ExchangeRate"      , this.ExchangeRate      );
				Sql.AddParameter(qbcmd, "@Subtotal"          , this.Subtotal          );
				Sql.AddParameter(qbcmd, "@Tax"               , this.Tax               );
				Sql.AddParameter(qbcmd, "@TaxAgencyId"       , this.TaxAgencyId       );
				Sql.AddParameter(qbcmd, "@TotalAmount"       , this.TotalAmount       );
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				Sql.AddParameter(qbcmd, "@ID"                , this.ID                );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterCRM(IDbCommand cmd)
		{
			Sql.AppendParameter(cmd, Sql.ToString(this.ReferenceNumber), "QUOTE_NUM");
		}

		public override void ImportLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
			QuickBooks.EstimateLineItem qo = new QuickBooks.EstimateLineItem(qbf, qbcon, vwItems.Table, this.LOCAL_ID, this.ID);
			this.ImportLineItems(Context, Session, con, gUSER_ID, qo, sbErrors);
		}

		public override void SendLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
			QuickBooks.EstimateLineItem qo = new QuickBooks.EstimateLineItem(qbf, qbcon, vwItems.Table, this.LOCAL_ID, this.ID);
			this.SendLineItems(Context, Session, con, gUSER_ID, qo, sbErrors);
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = base.BuildUpdateProcedure(Application, Session, spUpdate, row, gUSER_ID, gTEAM_ID, gASSIGNED_USER_ID, trn);
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "QUOTE_NUM"                  :  oValue = Sql.ToDBString  (this.ReferenceNumber        );  break;
							case "DATE_QUOTE_EXPECTED_CLOSED" :  oValue = Sql.ToDBDateTime(this.Date                   );  break;
							// 02/10/2014 Paul.  We don't use the DueDate. 
							//case "DATE_QUOTE_CLOSED"          :  oValue = Sql.ToDBDateTime(this.DueDate                );  break;
							case "TOTAL"                      :  oValue = Sql.ToDBDouble  (this.TotalAmount            );  break;
							case "TOTAL_USDOLLAR"             :  oValue = Sql.ToDBDouble  (this.TotalAmount            );  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "QUOTE_NUM"                  :  bChanged = ParameterChanged(par, oValue,   11);  break;
									// 02/10/2014 Paul.  Need to include the default change test. 
									default                           :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
