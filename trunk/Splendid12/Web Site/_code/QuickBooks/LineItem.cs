/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class LineItem : QObject
	{
		#region Properties
		public string   QuickBooksParentField { get; set; }
		public string   ReferenceNumber { get; set; }
		public string   ParentId        { get; set; }
		public string   ItemLineId      { get; set; }
		public string   ItemId          { get; set; }
		public string   ItemName        { get; set; }
		public string   ItemDescription { get; set; }
		public string   ItemTaxCode     { get; set; }
		public double   ItemQuantity    { get; set; }
		public double   ItemRate        { get; set; }
		public double   ItemAmount      { get; set; }
		#endregion
		private DataView vwItems    ;
		private DataView vwParents  ;

		public LineItem(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtItems, DataTable dtParents, string sQuickBooksParentField, string sQuickBooksTableName, string sCRMModuleName, string sCRMTableName, string sCRMTableSort, bool bCRMAssignedUser) : base(qbf, qbcon, sQuickBooksTableName, "ItemLineID", sCRMModuleName, sCRMTableName, sCRMTableSort, bCRMAssignedUser)
		{
			this.QuickBooksParentField = sQuickBooksParentField;
			this.vwItems   = new DataView(dtItems  );
			this.vwParents = new DataView(dtParents);
		}

		public LineItem(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtItems, Guid gSYNC_LOCAL_ID, string sSYNC_REMOTE_KEY, string sQuickBooksParentField, string sQuickBooksTableName, string sCRMModuleName, string sCRMTableName, string sCRMTableSort, bool bCRMAssignedUser) : base(qbf, qbcon, sQuickBooksTableName, "ItemLineID", sCRMModuleName, sCRMTableName, sCRMTableSort, bCRMAssignedUser)
		{
			DataTable dtParents = new DataTable();
			dtParents.Columns.Add("SYNC_LOCAL_ID"  , typeof(Guid  ));
			dtParents.Columns.Add("SYNC_REMOTE_KEY", typeof(string));
			DataRow rowParent = dtParents.NewRow();
			dtParents.Rows.Add(rowParent);
			rowParent["SYNC_LOCAL_ID"  ] = gSYNC_LOCAL_ID  ;
			rowParent["SYNC_REMOTE_KEY"] = sSYNC_REMOTE_KEY;
			
			this.QuickBooksParentField = sQuickBooksParentField;
			this.vwItems   = new DataView(dtItems  );
			this.vwParents = new DataView(dtParents);
		}

		public override void Reset()
		{
			base.Reset();
			this.ReferenceNumber = String.Empty;
			this.ParentId        = String.Empty;
			this.ItemLineId      = String.Empty;
			this.ItemId          = String.Empty;
			this.ItemName        = String.Empty;
			this.ItemDescription = String.Empty;
			this.ItemTaxCode     = String.Empty;
			this.ItemQuantity    = 0;
			this.ItemRate        = 0.0;
			this.ItemAmount      = 0.0;  // This is Quantity * Rate. 
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			string sItemTaxCode = Sql.ToString(row["TAX_CLASS"]) == "Taxable" ? "Tax" : "Non";
			string sItemId = String.Empty;
			vwItems.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["PRODUCT_TEMPLATE_ID"]) + "'";
			if ( vwItems.Count > 0 )
				sItemId = Sql.ToString(vwItems[0]["SYNC_REMOTE_KEY"]);
			
			vwParents.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row[this.CRMTableName.Replace("S_LINE_ITEMS", "") + "_ID"]) + "'";
			if ( vwParents.Count > 0 )
				this.ParentId = Sql.ToString(vwParents[0]["SYNC_REMOTE_KEY"]);
			
			this.ID = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.ItemId          = Sql.ToString(    sItemId             );
				this.ItemName        = Sql.ToString(row["MFT_PART_NUM"     ]);
				this.ItemDescription = Sql.ToString(row["NAME"             ]);
				this.ItemTaxCode     = Sql.ToString(    sItemTaxCode        );
				this.ItemQuantity    = Sql.ToDouble(row["QUANTITY"         ]);
				this.ItemRate        = Sql.ToDouble(row["UNIT_USDOLLAR"    ]);
				this.ItemAmount      = Sql.ToDouble(row["EXTENDED_USDOLLAR"]);
				bChanged = true;
			}
			else
			{
				if ( Compare(this.ItemId         ,     sItemId             , "PRODUCT_TEMPLATE_ID", sbChanges) ) { this.ItemId          = Sql.ToString(    sItemId             );  bChanged = true; }
				if ( Compare(this.ItemName       , row["MFT_PART_NUM"     ], "MFT_PART_NUM"       , sbChanges) ) { this.ItemName        = Sql.ToString(row["MFT_PART_NUM"     ]);  bChanged = true; }
				if ( Compare(this.ItemDescription, row["NAME"             ], "NAME"               , sbChanges) ) { this.ItemDescription = Sql.ToString(row["NAME"             ]);  bChanged = true; }
				if ( Compare(this.ItemTaxCode    ,     sItemTaxCode        , "TAX_CLASS"          , sbChanges) ) { this.ItemTaxCode     = Sql.ToString(    sItemTaxCode        );  bChanged = true; }
				if ( Compare(this.ItemQuantity   , row["QUANTITY"         ], "QUANTITY"           , sbChanges) ) { this.ItemQuantity    = Sql.ToDouble(row["QUANTITY"         ]);  bChanged = true; }
				if ( Compare(this.ItemRate       , row["UNIT_USDOLLAR"    ], "UNIT_USDOLLAR"      , sbChanges) ) { this.ItemRate        = Sql.ToDouble(row["UNIT_USDOLLAR"    ]);  bChanged = true; }
				if ( Compare(this.ItemAmount     , row["EXTENDED_USDOLLAR"], "EXTENDED_USDOLLAR"  , sbChanges) ) { this.ItemAmount      = Sql.ToDouble(row["EXTENDED_USDOLLAR"]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			this.Reset();
			this.ID              = Sql.ToString  (row["ID"             ]);
			this.TimeCreated     = Sql.ToDateTime(row["TimeCreated"    ]);
			this.TimeModified    = Sql.ToDateTime(row["TimeModified"   ]);
			this.ReferenceNumber = Sql.ToString  (row["ReferenceNumber"]);
			this.ParentId        = Sql.ToString  (row[this.QuickBooksTableName.Replace("LineItems", "") + "Id"]);
			this.ItemLineId      = Sql.ToString  (row["ItemLineId"     ]);
			this.ItemId          = Sql.ToString  (row["ItemId"         ]);
			this.ItemName        = Sql.ToString  (row["ItemName"       ]);
			this.ItemDescription = Sql.ToString  (row["ItemDescription"]);
			this.ItemTaxCode     = Sql.ToString  (row["ItemTaxCode"    ]);
			this.ItemQuantity    = Sql.ToDouble  (row["ItemQuantity"   ]);
			this.ItemRate        = Sql.ToDouble  (row["ItemRate"       ]);
			this.ItemAmount      = Sql.ToDouble  (row["ItemAmount"     ]);  // This is Quantity * Rate. 
			// 05/31/2012 Paul.  Subtotal must not result in an extended value, so make sure to zero the quantity. 
			if ( this.ItemName == "Subtotal" )
			{
				this.ItemQuantity = 0;
				this.ItemRate     = this.ItemAmount;
			}
			// 05/31/2012 Paul.  Quantity is sometimes 0 for valid lines.  In that case, it is likely a Markup line. 
			else if ( this.ItemQuantity == 0 && this.ItemAmount != 0 )
			{
				// 05/31/2012 Paul.  If this is a markup line, we could either use a fraction for the quantity or just use the amount as the rate. 
				this.ItemQuantity = 1;
				this.ItemRate     = this.ItemAmount;
			}
			else if ( this.ItemQuantity != 0 && this.ItemAmount != 0 )
			{
				// 05/31/2012 Paul.  The provided rate value seems to be a reference, not a real value. 
				// We need to use the Amount as our stored procedure will calculate the extended amount using rate and quantity. 
				this.ItemRate     = this.ItemAmount / this.ItemQuantity;
			}
		}

		public override string Insert()
		{
			if ( Sql.IsEmptyString(this.ParentId) )
				throw(new Exception("LineItem.ParentId cannot be empty. " + this.LOCAL_ID.ToString()));
			
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( " + this.QuickBooksTableName.Replace("LineItems", "") + "Id" + ControlChars.CrLf
				     + "     , ItemId          " + ControlChars.CrLf
				     + "     , ItemName        " + ControlChars.CrLf
				     + "     , ItemDescription " + ControlChars.CrLf
				     + "     , ItemTaxCode     " + ControlChars.CrLf
				     + "     , ItemQuantity    " + ControlChars.CrLf
				     + "     , ItemRate        " + ControlChars.CrLf
				     + "     , ItemAmount      " + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @ParentId       " + ControlChars.CrLf
				     + "     , @ItemId         " + ControlChars.CrLf
				     + "     , @ItemName       " + ControlChars.CrLf
				     + "     , @ItemDescription" + ControlChars.CrLf
				     + "     , @ItemTaxCode    " + ControlChars.CrLf
				     + "     , @ItemQuantity   " + ControlChars.CrLf
				     + "     , @ItemRate       " + ControlChars.CrLf
				     + "     , @ItemAmount     " + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ParentId"       , this.ParentId        );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@ItemId"         , this.ItemId          );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@ItemName"       , this.ItemName        ,   41);
				Sql.AddParameter(qbcmd, "@ItemDescription", this.ItemDescription , 4095);
				Sql.AddParameter(qbcmd, "@ItemTaxCode"    , this.ItemTaxCode     ,    3);
				Sql.AddParameter(qbcmd, "@ItemQuantity"   , this.ItemQuantity    );
				Sql.AddParameter(qbcmd, "@ItemRate"       , this.ItemRate        );
				Sql.AddParameter(qbcmd, "@ItemAmount"     , this.ItemAmount      );
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public override void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set ItemId          = @ItemId         " + ControlChars.CrLf
				     + "     , ItemName        = @ItemName       " + ControlChars.CrLf
				     + "     , ItemDescription = @ItemDescription" + ControlChars.CrLf
				     + "     , ItemTaxCode     = @ItemTaxCode    " + ControlChars.CrLf
				     + "     , ItemQuantity    = @ItemQuantity   " + ControlChars.CrLf
				     + "     , ItemRate        = @ItemRate       " + ControlChars.CrLf
				     + "     , ItemAmount      = @ItemAmount     " + ControlChars.CrLf
				     + " where ID              = @ID             " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ItemId"         , this.ItemId          );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@ItemName"       , this.ItemName        ,   41);
				Sql.AddParameter(qbcmd, "@ItemDescription", this.ItemDescription , 4095);
				Sql.AddParameter(qbcmd, "@ItemTaxCode"    , this.ItemTaxCode     ,    3);
				Sql.AddParameter(qbcmd, "@ItemQuantity"   , this.ItemQuantity    );
				Sql.AddParameter(qbcmd, "@ItemRate"       , this.ItemRate        );
				Sql.AddParameter(qbcmd, "@ItemAmount"     , this.ItemAmount      );
				Sql.AddParameter(qbcmd, "@ID"             , this.ID              );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterCRM(IDbCommand cmd)
		{
			Guid gPARENT_ID = Guid.Empty;
			vwParents.RowFilter = "SYNC_REMOTE_KEY = '" + this.ParentId + "'";
			if ( vwParents.Count > 0 )
				gPARENT_ID = Sql.ToGuid(vwParents[0]["SYNC_LOCAL_ID"]);

			Sql.AppendParameter(cmd, gPARENT_ID                 , this.CRMTableName.Replace("S_LINE_ITEMS", "") + "_ID");
			Sql.AppendParameter(cmd, Sql.ToString(this.ItemName), "MFT_PART_NUM");
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			
			string sTAX_CLASS = this.ItemTaxCode == "Tax" ? "Taxable" : "Non-Taxable";
			Guid gPRODUCT_TEMPLATE_ID = Guid.Empty;
			vwItems.RowFilter = "SYNC_REMOTE_KEY = '" + this.ItemId + "'";
			if ( vwItems.Count > 0 )
				gPRODUCT_TEMPLATE_ID = Sql.ToGuid(vwItems[0]["SYNC_LOCAL_ID"]);
			string sPARENT_FIELD = this.CRMTableName.Replace("S_LINE_ITEMS", "") + "_ID";
			Guid gPARENT_ID = Guid.Empty;
			vwParents.RowFilter = "SYNC_REMOTE_KEY = '" + this.ParentId + "'";
			if ( vwParents.Count > 0 )
				gPARENT_ID = Sql.ToGuid(vwParents[0]["SYNC_LOCAL_ID"]);
			if ( Sql.IsEmptyGuid(gPARENT_ID) )
				throw(new Exception("BuildUpdateProcedure.gPARENT_ID cannot be empty."));
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "PRODUCT_TEMPLATE_ID":  oValue = Sql.ToDBGuid  (     gPRODUCT_TEMPLATE_ID);  break;
							case "MFT_PART_NUM"       :  oValue = Sql.ToDBString(this.ItemName            );  break;
							case "NAME"               :  oValue = Sql.ToDBString(this.ItemDescription     );  break;
							case "QUANTITY"           :  oValue = Sql.ToDBDouble(this.ItemQuantity        );  break;
							case "TAX_CLASS"          :  oValue = Sql.ToDBString(     sTAX_CLASS          );  break;
							case "COST_PRICE"         :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "COST_USDOLLAR"      :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "LIST_PRICE"         :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "LIST_USDOLLAR"      :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "UNIT_PRICE"         :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "UNIT_USDOLLAR"      :  oValue = Sql.ToDBDouble(this.ItemRate            );  break;
							case "EXTENDED_PRICE"     :  oValue = Sql.ToDBDouble(this.ItemAmount          );  break;
							case "EXTENDED_USDOLLAR"  :  oValue = Sql.ToDBDouble(this.ItemAmount          );  break;
							case "MODIFIED_USER_ID"   :  oValue = gUSER_ID;  break;
						}
						if ( sColumnName == sPARENT_FIELD )
							oValue = Sql.ToDBGuid(gPARENT_ID);
						
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "MFT_PART_NUM":  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "NAME"        :  bChanged = ParameterChanged(par, oValue, 4095);  break;
									default            :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}

		public void ImportLineItem(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, string sDIRECTION, StringBuilder sbErrors)
		{
			HttpApplicationState Application = Context.Application;
			bool   bVERBOSE_STATUS      = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"     ]);
			string sCONFLICT_RESOLUTION = Sql.ToString (Application["CONFIG.QuickBooks.ConflictResolution"]);
			Guid   gTEAM_ID             = Sql.ToGuid   (Session["TEAM_ID"]);
			
			IDbCommand spUpdate = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_Update");

			string   sREMOTE_KEY  = this.ID;
			string   sName        = Sql.ToString(this.Name);
			DateTime dtREMOTE_DATE_MODIFIED     = this.TimeModified;
			DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();

			String sSQL = String.Empty;
			sSQL = "select SYNC_ID                                       " + ControlChars.CrLf
			     + "     , SYNC_LOCAL_ID                                 " + ControlChars.CrLf
			     + "     , SYNC_LOCAL_DATE_MODIFIED_UTC                  " + ControlChars.CrLf
			     + "     , SYNC_REMOTE_DATE_MODIFIED_UTC                 " + ControlChars.CrLf
			     + "     , ID                                            " + ControlChars.CrLf
			     + "     , DATE_MODIFIED_UTC                             " + ControlChars.CrLf
			     + "  from vw" + this.CRMTableName + "_SYNC              " + ControlChars.CrLf
			     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
			     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf
			     + "   and SYNC_REMOTE_KEY       = @SYNC_REMOTE_KEY      " + ControlChars.CrLf;
			DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.CommandText = sSQL;
				Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID   );
				Sql.AddParameter(cmd, "@SYNC_REMOTE_KEY"      , sREMOTE_KEY);
				using ( DbDataAdapter da = dbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = cmd;
					string sSYNC_ACTION   = "remote changed";
					Guid   gSYNC_LOCAL_ID = Guid.Empty;
					using ( DataTable dt = new DataTable() )
					{
						da.Fill(dt);
						if ( dt.Rows.Count > 0 )
						{
							DataRow row = dt.Rows[0];
							this.LOCAL_ID                            = Sql.ToGuid    (row["ID"                           ]);
							gSYNC_LOCAL_ID                           = Sql.ToGuid    (row["SYNC_LOCAL_ID"                ]);
							DateTime dtSYNC_LOCAL_DATE_MODIFIED_UTC  = Sql.ToDateTime(row["SYNC_LOCAL_DATE_MODIFIED_UTC" ]);
							DateTime dtSYNC_REMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(row["SYNC_REMOTE_DATE_MODIFIED_UTC"]);
							DateTime dtDATE_MODIFIED_UTC             = Sql.ToDateTime(row["DATE_MODIFIED_UTC"            ]);
							// 03/28/2010 Paul.  If the ID is NULL and the LOCAL_ID is NOT NULL, then the local item must have been deleted. 
							if ( (Sql.IsEmptyGuid(this.LOCAL_ID) && !Sql.IsEmptyGuid(gSYNC_LOCAL_ID)) )
							{
								sSYNC_ACTION = "remote new";
							}
						}
						else
						{
							sSYNC_ACTION = "remote new";
							
							// 03/25/2010 Paul.  If we find a account, then treat the CRM record as the master and send latest version over to QuickBooks. 
							// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
							// 05/30/2012 Paul.  There can only be one record mapped to QuickBooks, so we don't need to join to the remote key. 
							cmd.Parameters.Clear();
							sSQL = "select vw" + this.CRMTableName + ".ID             " + ControlChars.CrLf
							     + "  from            " + Sql.MetadataName(con, "vw" + this.CRMTableName + "_QuickBooks") + " vw" + this.CRMTableName   + ControlChars.CrLf
							     + "  left outer join vw" + this.CRMTableName + "_SYNC" + ControlChars.CrLf
							     + "               on vw" + this.CRMTableName + "_SYNC.SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
							     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf
							//     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_REMOTE_KEY       = @SYNC_REMOTE_KEY      " + ControlChars.CrLf
							     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_LOCAL_ID         = vw" + this.CRMTableName + ".ID" + ControlChars.CrLf;
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID   );
							//Sql.AddParameter(cmd, "@SYNC_REMOTE_KEY"      , sREMOTE_KEY);
							ExchangeSecurity.Filter(Application, Session, cmd, gUSER_ID, this.CRMModuleName, "view");
							this.FilterCRM(cmd);
							cmd.CommandText += "   and vw" + this.CRMTableName + "_SYNC.ID is null" + ControlChars.CrLf;
							this.LOCAL_ID = Sql.ToGuid(cmd.ExecuteScalar());
						}
					}
					using ( DataTable dt = new DataTable() )
					{
						DataRow row = null;
						Guid gASSIGNED_USER_ID = Guid.Empty;
						if ( sSYNC_ACTION == "remote new" || sSYNC_ACTION == "remote changed" || sSYNC_ACTION == "local changed" || sSYNC_ACTION == "local new" )
						{
							if ( !Sql.IsEmptyGuid(this.LOCAL_ID) )
							{
								cmd.Parameters.Clear();
								sSQL = "select *         " + ControlChars.CrLf
								     + "  from vw" + this.CRMTableName + ControlChars.CrLf
								     + " where ID = @ID  " + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID", this.LOCAL_ID);
								
								((IDbDataAdapter)da).SelectCommand = cmd;
								da.Fill(dt);
								if ( dt.Rows.Count > 0 )
								{
									row = dt.Rows[0];
								}
							}
						}
						if ( sSYNC_ACTION == "remote new" || sSYNC_ACTION == "remote changed" )
						{
							// 05/18/2012 Paul.  Allow control of sync direction. 
							if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
							{
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), this.QuickBooksTableName + ".Import: Retrieving " + this.QuickBooksTableName + " " + sName + ".");
									
										spUpdate.Transaction = trn;
										// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
										// 01/28/2012 Paul.  The transaction is necessary so that an account can be created. 
										bool bChanged = this.BuildUpdateProcedure(Application, Session, spUpdate, row, gUSER_ID, gTEAM_ID, gASSIGNED_USER_ID, trn);
										if ( bChanged )
										{
											spUpdate.ExecuteNonQuery();
											IDbDataParameter parID = Sql.FindParameter(spUpdate, "@ID");
											this.LOCAL_ID = Sql.ToGuid(parID.Value);
											this.ProcedureUpdated(this.LOCAL_ID, sREMOTE_KEY);
										}
										// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
										IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_SYNC_Update");
										cmdSyncUpdate.Transaction = trn;
										Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
										Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
										Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , this.LOCAL_ID             );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , sREMOTE_KEY               );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
										Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
										Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
										cmdSyncUpdate.ExecuteNonQuery();
										trn.Commit();
									}
									catch
									{
										trn.Rollback();
										throw;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
