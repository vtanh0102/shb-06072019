/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM
{
	public class QuickBooksSync
	{
		public  static bool bInsideSyncAll = false;

		public static bool IsOnlineAppMode(HttpApplicationState Application)
		{
			string sAppMode = Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]);
			return sAppMode == "Online";
		}

		public static string PrimarySortField(HttpApplicationState Application, string sModule)
		{
			string sFieldName = String.Empty;
			string sAppMode   = Sql.ToString(Application["CONFIG.QuickBooks.AppMode"]);
			if ( sAppMode == "Online" )
			{
				switch ( sModule )
				{
					// 06/22/2014 Paul.  Treat contacts just like accounts. Both point to customers. 
					case "Accounts"   :  sFieldName = "DisplayName";  break;
					case "Contacts"   :  sFieldName = "DisplayName";  break;
					case "Invoices"   :  sFieldName = "DocNumber"  ;  break;
					case "Orders"     :  sFieldName = "DocNumber"  ;  break;
					case "Quotes"     :  sFieldName = "DocNumber"  ;  break;
					case "Payments"   :  sFieldName = "TxnDate"    ;  break;
					case "CreditMemos":  sFieldName = "DocNumber"  ;  break;
					default           :  sFieldName = "Name"       ;  break;
				}
			}
			else
			{
				switch ( sModule )
				{
					case "Accounts":  sFieldName = "NAME"           ;  break;
					case "Contacts":  sFieldName = "NAME"           ;  break;
					case "Invoices":  sFieldName = "ReferenceNumber";  break;
					case "Orders"  :  sFieldName = "ReferenceNumber";  break;
					case "Quotes"  :  sFieldName = "ReferenceNumber";  break;
				}
			}
			return sFieldName;
		}

		public static QuickBooksClientFactory CreateFactory(HttpApplicationState Application)
		{
			QuickBooksClientFactory dbf = null;
			if ( IsOnlineAppMode(Application) )
			{
				string sOAuthCompanyID         = Sql.ToString(Application["CONFIG.QuickBooks.OAuthCompanyID"        ]);
				string sOAuthCountryCode       = Sql.ToString(Application["CONFIG.QuickBooks.OAuthCountryCode"      ]);
				string sOAuthClientID          = Sql.ToString(Application["CONFIG.QuickBooks.OAuthClientID"         ]);
				string sOAuthClientSecret      = Sql.ToString(Application["CONFIG.QuickBooks.OAuthClientSecret"     ]);
				string sOAuthAccessToken       = Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessToken"      ]);
				string sOAuthAccessTokenSecret = Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]);
				string sConnectionString       = BuildOnlineConnectionString(sOAuthCompanyID, sOAuthCountryCode, sOAuthClientID, sOAuthClientSecret, sOAuthAccessToken, sOAuthAccessTokenSecret);
				dbf = new QuickBooksOnlineFactory(sConnectionString);
			}
			else
			{
				string sRemoteUser             = Sql.ToString(Application["CONFIG.QuickBooks.RemoteUser"            ]);
				string sRemotePassword         = Sql.ToString(Application["CONFIG.QuickBooks.RemotePassword"        ]);
				string sRemoteURL              = Sql.ToString(Application["CONFIG.QuickBooks.RemoteURL"             ]);
				string sRemoteApplicationName  = Sql.ToString(Application["CONFIG.QuickBooks.RemoteApplicationName" ]);
				string sConnectionString       = BuildRemoteConnectionString(sRemoteUser, sRemotePassword, sRemoteURL, sRemoteApplicationName);
				dbf = new QuickBooksRemoteFactory(sConnectionString);
			}
			return dbf;
		}

		public static bool QuickBooksEnabled(HttpApplicationState Application)
		{
			bool bQuickBooksEnabled = Sql.ToBoolean(Application["CONFIG.QuickBooks.Enabled"]);
			if ( bQuickBooksEnabled )
			{
				string sAppMode = Sql.ToString (Application["CONFIG.QuickBooks.AppMode"]);
				if ( sAppMode == "Online" )
				{
					string sOAuthAccessToken       = Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessToken"      ]);
					string sOAuthAccessTokenSecret = Sql.ToString(Application["CONFIG.QuickBooks.OAuthAccessTokenSecret"]);
					bQuickBooksEnabled = !Sql.IsEmptyString(sOAuthAccessToken) && !Sql.IsEmptyString(sOAuthAccessTokenSecret);
				}
				else
				{
					string sRemoteUser = Sql.ToString(Application["CONFIG.QuickBooks.RemoteUser"]);
					string sRemoteURL  = Sql.ToString(Application["CONFIG.QuickBooks.RemoteURL" ]);
					bQuickBooksEnabled = !Sql.IsEmptyString(sRemoteUser) && !Sql.IsEmptyString(sRemoteURL);
				}
			}
			return bQuickBooksEnabled;
		}

		public static Guid QuickBooksUserID(HttpApplicationState Application)
		{
			Guid gQUICKBOOKS_USER_ID = Sql.ToGuid(Application["CONFIG.QuickBooks.UserID"]);
			if ( Sql.IsEmptyGuid(gQUICKBOOKS_USER_ID) )
				gQUICKBOOKS_USER_ID = new Guid("00000000-0000-0000-0000-000000000004");  // Use special QuickBooks user. 
			return gQUICKBOOKS_USER_ID;
		}

		public static bool ValidateQuickBooks(HttpApplicationState Application, StringBuilder sbErrors)
		{
			bool bValidSource = false;
			try
			{
				QuickBooksClientFactory dbf = CreateFactory(Application);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select * from Customers" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								bValidSource = true;
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				sbErrors.AppendLine(ex.Message);
			}
			return bValidSource;
		}

		public static string BuildOnlineConnectionString(string sOAuthCompanyID, string sOAuthCountryCode, string sOAuthClientID, string sOAuthClientSecret, string sOAuthAccessToken, string sOAuthAccessTokenSecret)
		{
			StringBuilder sbConnectionString = new StringBuilder();
			// 06/03/2014 Paul.  V4 requires Schema. 
			sbConnectionString.Append("Schema=Online"                                        );
			sbConnectionString.Append(";Offline=false"                                       );
			sbConnectionString.Append(";OAuth Client ID="           + sOAuthClientID         );
			sbConnectionString.Append(";OAuth Client Secret="       + sOAuthClientSecret     );
			sbConnectionString.Append(";OAuth Access Token="        + sOAuthAccessToken      );
			sbConnectionString.Append(";OAuth Access Token Secret=" + sOAuthAccessTokenSecret);
			sbConnectionString.Append(";Company Id="                + sOAuthCompanyID        );
			if ( !Sql.IsEmptyString(sOAuthCountryCode) )
				sbConnectionString.Append(";Country Code="              + sOAuthCountryCode      );
			return sbConnectionString.ToString();
		}

		public static void GetOAuthAuthorizationURL(HttpApplicationState Application, string sOAuthClientID, string sOAuthClientSecret, string sCALLBACK_URL, ref string sAUTH_TOKEN, ref string sAUTH_KEY, ref string sAUTH_URL, StringBuilder sbErrors)
		{
			try
			{
				StringBuilder sbConnectionString = new StringBuilder();
				sbConnectionString.Append("Schema=Online"                                        );
				sbConnectionString.Append(";Offline=false"                                       );
				sbConnectionString.Append(";OAuth Client ID="           + sOAuthClientID         );
				sbConnectionString.Append(";OAuth Client Secret="       + sOAuthClientSecret     );
				
				QuickBooksClientFactory dbf = new QuickBooksOnlineFactory(sbConnectionString.ToString());
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = "GetOAuthAuthorizationURL";
						Sql.AddParameter(cmd, "CallbackUrl", sCALLBACK_URL);
						using ( IDataReader rdr = cmd.ExecuteReader() )
						{
							if ( rdr.Read() )
							{
								sAUTH_TOKEN = Sql.ToString(rdr["AuthToken"]);
								sAUTH_KEY   = Sql.ToString(rdr["AuthKey"  ]);
								sAUTH_URL   = Sql.ToString(rdr["URL"      ]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				sbErrors.AppendLine(ex.Message);
			}
		}

		public static void GetOAuthAccessToken(HttpApplicationState Application, string sOAuthClientID, string sOAuthClientSecret, string sAUTH_TOKEN, string sAUTH_KEY, string sAUTH_VERIFIER, ref string sOAUTH_ACCESS_TOKEN, ref string sOAUTH_ACCESS_SECRET, StringBuilder sbErrors)
		{
			try
			{
				StringBuilder sbConnectionString = new StringBuilder();
				sbConnectionString.Append("Schema=Online"                                        );
				sbConnectionString.Append(";Offline=false"                                       );
				sbConnectionString.Append(";OAuth Client ID="           + sOAuthClientID         );
				sbConnectionString.Append(";OAuth Client Secret="       + sOAuthClientSecret     );
				
				QuickBooksClientFactory dbf = new QuickBooksOnlineFactory(sbConnectionString.ToString());
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = "GetOAuthAccessToken";
						Sql.AddParameter(cmd, "Authmode" , "WEB"         );
						Sql.AddParameter(cmd, "AuthToken", sAUTH_TOKEN   );
						Sql.AddParameter(cmd, "AuthKey"  , sAUTH_KEY     );
						Sql.AddParameter(cmd, "Verifier" , sAUTH_VERIFIER);
						using ( IDataReader rdr = cmd.ExecuteReader() )
						{
							if ( rdr.Read() )
							{
								sOAUTH_ACCESS_TOKEN  = Sql.ToString(rdr["OAuthAccessToken"      ]);
								sOAUTH_ACCESS_SECRET = Sql.ToString(rdr["OAuthAccessTokenSecret"]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				sbErrors.AppendLine(ex.Message);
			}
		}

		public static bool ValidateQuickBooks(HttpApplicationState Application, string sOAuthCompanyID, string sOAuthCountryCode, string sOAuthClientID, string sOAuthClientSecret, string sOAuthAccessToken, string sOAuthAccessTokenSecret, StringBuilder sbErrors)
		{
			bool bValidSource = false;
			try
			{
				string sConnectionString = BuildOnlineConnectionString(sOAuthCompanyID, sOAuthCountryCode, sOAuthClientID, sOAuthClientSecret, sOAuthAccessToken, sOAuthAccessTokenSecret);
				QuickBooksClientFactory dbf = new QuickBooksOnlineFactory(sConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select * from Customers" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								bValidSource = true;
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				sbErrors.AppendLine(ex.Message);
			}
			return bValidSource;
		}

		public static string BuildRemoteConnectionString(string sRemoteUser, string sRemotePassword, string sRemoteURL, string sRemoteApplicationName)
		{
			StringBuilder sbConnectionString = new StringBuilder();
			// 06/03/2014 Paul.  V4 requires Schema. 
			sbConnectionString.Append("Schema=Desktop"                            );
			sbConnectionString.Append(";User="             + sRemoteUser          );
			sbConnectionString.Append(";Password="        + sRemotePassword       );
			sbConnectionString.Append(";URL="             + sRemoteURL            );
			sbConnectionString.Append(";ApplicationName=" + sRemoteApplicationName);
			return sbConnectionString.ToString();
		}

		public static bool ValidateQuickBooks(HttpApplicationState Application, string sRemoteUser, string sRemotePassword, string sRemoteURL, string sRemoteApplicationName, StringBuilder sbErrors)
		{
			bool bValidSource = false;
			try
			{
				string sConnectionString = BuildRemoteConnectionString(sRemoteUser, sRemotePassword, sRemoteURL, sRemoteApplicationName);
				QuickBooksClientFactory dbf = new QuickBooksRemoteFactory(sConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select * from Customers" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								bValidSource = true;
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				sbErrors.AppendLine(ex.Message);
			}
			return bValidSource;
		}

		public static void Sync(Object sender)
		{
			HttpContext Context = sender as HttpContext;
			Sync(Context, false);
		}

		public static void SyncAll(Object sender)
		{
			HttpContext Context = sender as HttpContext;
			Sync(Context, true);
		}

		public static void Sync(HttpContext Context, bool bSyncAll)
		{
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			bool bQuickBooksEnabled = QuickBooksEnabled(Context.Application);
			if ( !bInsideSyncAll && bQuickBooksEnabled )
			{
				bInsideSyncAll = true;
				try
				{
					StringBuilder sbErrors = new StringBuilder();
					
					Guid gQUICKBOOKS_USER_ID = QuickBooksSync.QuickBooksUserID(Context.Application);
					QuickBooks.UserSync User = new QuickBooks.UserSync(Context, gQUICKBOOKS_USER_ID, bSyncAll);
					Sync(User, sbErrors);
				}
				catch(Exception ex)
				{
					SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
				finally
				{
					bInsideSyncAll = false;
				}
			}
		}

		public static void Sync(QuickBooks.UserSync User, StringBuilder sbErrors)
		{
			HttpApplicationState Application = User.Context.Application;
			bool   bVERBOSE_STATUS      = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"     ]);
			if ( bVERBOSE_STATUS )
				SyncError.SystemMessage(User.Context, "Warning", new StackTrace(true).GetFrame(0), "QuickBooksSync.Sync Start.");
			
			// 02/06/2014 Paul.  New QuickBooks factory to allow Remote and Online. 
			QuickBooksClientFactory qbf = QuickBooksSync.CreateFactory(Application);
			using ( IDbConnection qbcon = qbf.CreateConnection() )
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					try
					{
						qbcon.Open();
						con.Open();
						
						bool bSyncShipper              = true;
						bool bSyncTaxRates             = true;
						bool bSyncItems                = true;
						bool bSyncCustomers            = true;
						bool bSyncEstimates            = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncQuotes"  ]);
						//bool bSyncEstimatesLineItems   = bSyncEstimates;
						bool bSyncSalesOrders          = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncOrders"  ]);
						//bool bSyncSalesOrdersLineItems = bSyncSalesOrders;
						bool bSyncInvoices             = Sql.ToBoolean(Application["CONFIG.QuickBooks.SyncInvoices"]);
						//bool bSyncInvoicesLineItems    = bSyncInvoices;
						
						QuickBooks.ShippingMethod shipper = new QuickBooks.ShippingMethod(qbf, qbcon);
						// 05/19/2012 Paul.  Shipping values never change, but they are not being filtered by date. 
						if ( bSyncShipper && !Sql.ToBoolean(Application["CONFIG.QuickBooks.ShippingUpdated"]) )
						{
							Sync(dbf, con, User, shipper, sbErrors);
							Application["CONFIG.QuickBooks.ShippingUpdated"] = true;
						}
						
						QuickBooks.TaxRate taxrate = new QuickBooks.TaxRate(qbf, qbcon);
						if ( bSyncTaxRates )
						{
							Sync(dbf, con, User, taxrate, sbErrors);
						}
						
						DataTable dtProductTypes = SplendidCache.ProductTypes(Application);
						QuickBooks.Item item = new QuickBooks.Item(qbf, qbcon, dtProductTypes);
						if ( bSyncItems )
							Sync(dbf, con, User, item, sbErrors);
						
						using ( DataTable dtCustomers = new DataTable() )
						{
							string sSQL = String.Empty;
							sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
							     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
							     + "  from vwACCOUNTS_SYNC                               " + ControlChars.CrLf
							     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
							     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									da.Fill(dtCustomers);
								}
							}
							QuickBooks.Customer customer = new QuickBooks.Customer(qbf, qbcon, dtCustomers);
							if ( bSyncCustomers )
								Sync(dbf, con, User, customer, sbErrors);
						
							using ( DataTable dtShippers = new DataTable() )
							{
								sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
								     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
								     + "  from vw" + shipper.CRMTableName + "_SYNC           " + ControlChars.CrLf
								     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
								     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtShippers);
									}
								}
								using ( DataTable dtTaxRates = new DataTable() )
								{
									sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
									     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
									     + "  from vw" + taxrate.CRMTableName + "_SYNC           " + ControlChars.CrLf
									     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
									     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
										using ( DbDataAdapter da = dbf.CreateDataAdapter() )
										{
											((IDbDataAdapter)da).SelectCommand = cmd;
											da.Fill(dtTaxRates);
										}
									}
									using ( DataTable dtItems = new DataTable() )
									{
										sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
										     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
										     + "  from vw" + item.CRMTableName + "_SYNC              " + ControlChars.CrLf
										     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
										     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
											using ( DbDataAdapter da = dbf.CreateDataAdapter() )
											{
												((IDbDataAdapter)da).SelectCommand = cmd;
												da.Fill(dtItems);
											}
										}
										if ( bSyncEstimates )
										{
											QuickBooks.Estimate estimate = new QuickBooks.Estimate(qbf, qbcon, dtShippers, dtTaxRates, dtItems, dtCustomers, dbf, con);
											Sync(dbf, con, User, estimate, sbErrors);
											// 06/03/2012 Paul.  We cannot treat the line items separately because updating the parent will prevent line item updates. 
											/*
											using ( DataTable dtQuotes = new DataTable() )
											{
												sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
												     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
												     + "  from vw" + estimate.CRMTableName + "_SYNC          " + ControlChars.CrLf
												     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
												     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
												using ( IDbCommand cmd = con.CreateCommand() )
												{
													cmd.CommandText = sSQL;
													Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
													using ( DbDataAdapter da = dbf.CreateDataAdapter() )
													{
														((IDbDataAdapter)da).SelectCommand = cmd;
														da.Fill(dtQuotes);
													}
												}
												if ( bSyncEstimatesLineItems )
												{
													QuickBooks.EstimateLineItem estimateLineItems = new QuickBooks.EstimateLineItem(qbf, qbcon, dtItems, dtQuotes);
													Sync(dbf, con, User, estimateLineItems, sbErrors);
												}
											}
											*/
										}
										if ( bSyncSalesOrders )
										{
											QuickBooks.SalesOrder order = new QuickBooks.SalesOrder(qbf, qbcon, dtShippers, dtTaxRates, dtItems, dtCustomers, dbf, con);
											Sync(dbf, con, User, order, sbErrors);
											// 06/03/2012 Paul.  We cannot treat the line items separately because updating the parent will prevent line item updates. 
											/*
											using ( DataTable dtOrders = new DataTable() )
											{
												sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
												     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
												     + "  from vw" + order.CRMTableName + "_SYNC             " + ControlChars.CrLf
												     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
												     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
												using ( IDbCommand cmd = con.CreateCommand() )
												{
													cmd.CommandText = sSQL;
													Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
													using ( DbDataAdapter da = dbf.CreateDataAdapter() )
													{
														((IDbDataAdapter)da).SelectCommand = cmd;
														da.Fill(dtOrders);
													}
												}
												if ( bSyncSalesOrdersLineItems )
												{
													QuickBooks.SalesOrderLineItem orderLineItems = new QuickBooks.SalesOrderLineItem(qbf, qbcon, dtItems, dtOrders);
													Sync(dbf, con, User, orderLineItems, sbErrors);
												}
											}
											*/
										}
										if ( bSyncInvoices )
										{
											QuickBooks.Invoice invoice = new QuickBooks.Invoice(qbf, qbcon, dtShippers, dtTaxRates, dtItems, dtCustomers, dbf, con);
											Sync(dbf, con, User, invoice, sbErrors);
											// 06/03/2012 Paul.  We cannot treat the line items separately because updating the parent will prevent line item updates. 
											/*
											using ( DataTable dtInvoices = new DataTable() )
											{
												sSQL = "select SYNC_LOCAL_ID                                 " + ControlChars.CrLf
												     + "     , SYNC_REMOTE_KEY                               " + ControlChars.CrLf
												     + "  from vw" + invoice.CRMTableName + "_SYNC           " + ControlChars.CrLf
												     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
												     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
												using ( IDbCommand cmd = con.CreateCommand() )
												{
													cmd.CommandText = sSQL;
													Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", User.USER_ID);
													using ( DbDataAdapter da = dbf.CreateDataAdapter() )
													{
														((IDbDataAdapter)da).SelectCommand = cmd;
														da.Fill(dtInvoices);
													}
												}
												if ( bSyncInvoicesLineItems )
												{
													QuickBooks.InvoiceLineItem invoiceLineItems = new QuickBooks.InvoiceLineItem(qbf, qbcon, dtItems, dtInvoices);
													Sync(dbf, con, User, invoiceLineItems, sbErrors);
												}
											}
											*/
										}
									}
								}
							}
						}
					}
					catch(Exception ex)
					{
						SyncError.SystemMessage(User.Context, "Error", new StackTrace(true).GetFrame(0), ex);
						SplendidError.SystemMessage(User.Context, "Error", new StackTrace(true).GetFrame(0), ex);
						sbErrors.AppendLine(Utils.ExpandException(ex));
					}
					finally
					{
						if ( bVERBOSE_STATUS )
							SyncError.SystemMessage(User.Context, "Warning", new StackTrace(true).GetFrame(0), "QuickBooksSync.Sync Done.");
					}
				}
			}
		}

		public static void Sync(DbProviderFactory dbf, IDbConnection con, QuickBooks.UserSync User, QuickBooks.QObject qo, StringBuilder sbErrors)
		{
			HttpContext Context = User.Context;
			HttpApplicationState Application = Context.Application;
			ExchangeSession Session = ExchangeSecurity.LoadUserACL(Application, User.USER_ID);
			Guid gUSER_ID = User.USER_ID;
			bool bSyncAll = User.SyncAll;
			
			bool   bVERBOSE_STATUS      = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"     ]);
			string sCONFLICT_RESOLUTION = Sql.ToString (Application["CONFIG.QuickBooks.ConflictResolution"]);
			string sDIRECTION           = Sql.ToString (Application["CONFIG.QuickBooks.Direction"         ]);
			Guid   gTEAM_ID             = Sql.ToGuid  (Session["TEAM_ID"]);
			if ( sDIRECTION.ToLower().StartsWith("bi") )
				sDIRECTION = "bi-directional";
			else if ( sDIRECTION.ToLower().StartsWith("to crm"  ) || sDIRECTION.ToLower().StartsWith("from quickbooks") )
				sDIRECTION = "to crm only";
			else if ( sDIRECTION.ToLower().StartsWith("from crm") || sDIRECTION.ToLower().StartsWith("to quickbooks"  ) )
				sDIRECTION = "from crm only";
			if ( bVERBOSE_STATUS )
				SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: " + sDIRECTION);
			try
			{
				string sSQL = String.Empty;
				DateTime dtStartModifiedDate = DateTime.MinValue;
				if ( !bSyncAll )
				{
					sSQL = "select max(SYNC_REMOTE_DATE_MODIFIED_UTC)            " + ControlChars.CrLf
					     + "  from vw" + qo.CRMTableName + "_SYNC                " + ControlChars.CrLf
					     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
					     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID);
						DateTime dtMAX_SYNC_REMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(cmd.ExecuteScalar());
						if ( dtMAX_SYNC_REMOTE_DATE_MODIFIED_UTC > DateTime.MinValue )
							dtStartModifiedDate = dtMAX_SYNC_REMOTE_DATE_MODIFIED_UTC.ToLocalTime().AddSeconds(1);
					}
				}
				
				DateTime dtStartSelect = DateTime.Now;
				using ( DataTable dt = qo.Select(dtStartModifiedDate) )
				{
					DateTime dtEndSelect = DateTime.Now;
					if ( bVERBOSE_STATUS )
						SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Query took " + (dtEndSelect - dtStartSelect).Seconds.ToString() + " seconds. Using last modified " + dtStartModifiedDate.ToString());
					DataView vwImport = new DataView(dt);
					qo.FilterQuickBooks(vwImport);
					if ( vwImport.Count > 0 )
						SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: " + vwImport.Count.ToString() + " " + qo.QuickBooksTableName + " to import.");
					// 05/31/2012 Paul.  Sorting should not be necessary as the order of the line items should match the display order. 
					foreach ( DataRowView qbrow in vwImport )
					{
						qo.SetFromQuickBooks(qbrow.Row);
						bool bImported = qo.Import(Context, Session, con, gUSER_ID, sDIRECTION, sbErrors);
						if ( bImported && (qo.QuickBooksTableName == "Estimates" || qo.QuickBooksTableName == "SalesOrders" || qo.QuickBooksTableName == "Invoices") )
							qo.ImportLineItems(Context, Session, con, gUSER_ID, sbErrors);
					}
				}
				
				sSQL = "select vw" + qo.CRMTableName + ".*                                                                   " + ControlChars.CrLf
				     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_ID                                                        " + ControlChars.CrLf
				     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_KEY                                                " + ControlChars.CrLf
				     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_DATE_MODIFIED_UTC                                   " + ControlChars.CrLf
				     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_DATE_MODIFIED_UTC                                  " + ControlChars.CrLf
				     + "  from            " + Sql.MetadataName(con, "vw" + qo.CRMTableName + "_QuickBooks") + " vw" + qo.CRMTableName + ControlChars.CrLf
				     + "  left outer join vw" + qo.CRMTableName + "_SYNC                                                     " + ControlChars.CrLf
				     + "               on vw" + qo.CRMTableName + "_SYNC.SYNC_SERVICE_NAME     = N'QuickBooks'               " + ControlChars.CrLf
				     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID      " + ControlChars.CrLf
				     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_ID         = vw" + qo.CRMTableName + ".ID" + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID);
					ExchangeSecurity.Filter(Application, Session, cmd, gUSER_ID, qo.CRMModuleName, "view");
						
					// 03/28/2010 Paul.  All that is important is that the current date is greater than the last sync date. 
					cmd.CommandText += "   and (vw" + qo.CRMTableName + "_SYNC.ID is null or vw" + qo.CRMTableName + ".DATE_MODIFIED_UTC > vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_DATE_MODIFIED_UTC)" + ControlChars.CrLf;
					if ( !Sql.IsEmptyString(qo.CRMTableSort) )
						cmd.CommandText += " order by vw" + qo.CRMTableName + "." + qo.CRMTableSort + ControlChars.CrLf;
					else
						cmd.CommandText += " order by vw" + qo.CRMTableName + ".DATE_MODIFIED_UTC" + ControlChars.CrLf;
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							if ( dt.Rows.Count > 0 )
								SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: " + dt.Rows.Count.ToString() + " " + qo.CRMModuleName + " to send.");
							for ( int i = 0; i < dt.Rows.Count; i++ )
							{
								DataRow row = dt.Rows[i];
								Guid     gID                             = Sql.ToGuid    (row["ID"                           ]);
								Guid     gASSIGNED_USER_ID               = (qo.CRMAssignedUser ? Sql.ToGuid(row["ASSIGNED_USER_ID"]) : Guid.Empty);
								Guid     gSYNC_ID                        = Sql.ToGuid    (row["SYNC_ID"                      ]);
								string   sSYNC_REMOTE_KEY                = Sql.ToString  (row["SYNC_REMOTE_KEY"              ]);
								DateTime dtSYNC_LOCAL_DATE_MODIFIED_UTC  = Sql.ToDateTime(row["SYNC_LOCAL_DATE_MODIFIED_UTC" ]);
								DateTime dtSYNC_REMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(row["SYNC_REMOTE_DATE_MODIFIED_UTC"]);
								DateTime dtDATE_MODIFIED_UTC             = Sql.ToDateTime(row["DATE_MODIFIED_UTC"            ]);
								string   sSYNC_ACTION                    = Sql.IsEmptyGuid(gSYNC_ID) ? "local new" : "local changed";
								if ( SplendidInit.bEnableACLFieldSecurity )
								{
									bool bApplyACL = false;
									foreach ( DataColumn col in dt.Columns )
									{
										Security.ACL_FIELD_ACCESS acl = ExchangeSecurity.GetUserFieldSecurity(Session, qo.CRMModuleName, col.ColumnName, gASSIGNED_USER_ID);
										if ( !acl.IsReadable() )
										{
											row[col.ColumnName] = DBNull.Value;
											bApplyACL = true;
										}
									}
									if ( bApplyACL )
										dt.AcceptChanges();
								}
								// 02/01/2014 Paul.  Reset in Sync() not in SetFromCRM. 
								qo.Reset();
								StringBuilder sbChanges = new StringBuilder();
								try
								{
									if ( sSYNC_ACTION == "local new" || Sql.IsEmptyString(sSYNC_REMOTE_KEY) )
									{
										// 05/18/2012 Paul.  Allow control of sync direction. 
										if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
										{
											if ( bVERBOSE_STATUS )
												SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Sending new " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ".");
											qo.SetFromCRM(String.Empty, row, sbChanges);
											
											sSYNC_REMOTE_KEY = qo.Insert();
											if ( qo is QuickBooks.QOrder )
											{
												// 03/25/2010 Paul.  Update the modified date after the save. 
												// 03/26/2011 Paul.  Updated is in local time. 
												DateTime dtREMOTE_DATE_MODIFIED     = qo.TimeModified;
												DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();
												using ( IDbTransaction trn = Sql.BeginTransaction(con) )
												{
													try
													{
														// 03/26/2010 Paul.  Make sure to set the Sync flag. 
														// 03/29/2010 Paul.  The Sync flag will be updated in the procedure. 
														// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
														// 03/19/2015 Paul.  A Sales Tax line item is special and may not get a matching QuickBooks line. 
														if ( !Sql.IsEmptyString((qo as QuickBooks.QOrder).NewLineItemID) )
														{
															IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_LINE_ITEMS_SYNC_Update");
															cmdSyncUpdate.Transaction = trn;
															Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
															Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
															Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , (qo as QuickBooks.QOrder).NEW_LINE_ITEM_ID);
															Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , (qo as QuickBooks.QOrder).NewLineItemID   );
															Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
															Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
															Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
															Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
															cmdSyncUpdate.ExecuteNonQuery();
														}
														trn.Commit();
													}
													catch
													{
														trn.Rollback();
														throw;
													}
												}
											}
										}
									}
									else
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Binding " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ".");
											
										try
										{
											qo.Get(sSYNC_REMOTE_KEY);
											// 03/28/2010 Paul.  We need to double-check for conflicts. 
											// 03/26/2011 Paul.  Updated is in local time. 
											DateTime dtREMOTE_DATE_MODIFIED     = qo.TimeModified;
											DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();
											// 03/26/2011 Paul.  The QuickBooks remote date can vary by 1 millisecond, so check for local change first. 
											if ( dtREMOTE_DATE_MODIFIED_UTC > dtSYNC_REMOTE_DATE_MODIFIED_UTC.AddMilliseconds(10) && dtDATE_MODIFIED_UTC > dtSYNC_LOCAL_DATE_MODIFIED_UTC )
											{
												if ( sCONFLICT_RESOLUTION == "remote" )
												{
													// 03/24/2010 Paul.  Remote is the winner of conflicts. 
													sSYNC_ACTION = "remote changed";
												}
												else if ( sCONFLICT_RESOLUTION == "local" )
												{
													// 03/24/2010 Paul.  Local is the winner of conflicts. 
													sSYNC_ACTION = "local changed";
												}
												// 03/26/2011 Paul.  The QuickBooks remote date can vary by 1 millisecond, so check for local change first. 
												else if ( dtDATE_MODIFIED_UTC.AddHours(1) > dtREMOTE_DATE_MODIFIED_UTC )
												{
													// 03/25/2010 Paul.  Attempt to allow the end-user to resolve by editing the local or remote item. 
													sSYNC_ACTION = "local changed";
												}
												else if ( dtREMOTE_DATE_MODIFIED_UTC.AddHours(1) > dtDATE_MODIFIED_UTC )
												{
													// 03/25/2010 Paul.  Attempt to allow the end-user to resolve by editing the local or remote item. 
													sSYNC_ACTION = "remote changed";
												}
												else
												{
													sSYNC_ACTION = "prompt change";
												}
											}
											if ( qo.Deleted )
											{
												sSYNC_ACTION = "remote deleted";
											}
										}
										catch(Exception ex)
										{
											string sError = "Error retrieving QuickBooks " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString  (row["NAME"]) + "." + ControlChars.CrLf;
											sError += Utils.ExpandException(ex) + ControlChars.CrLf;
											SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
											sbErrors.AppendLine(sError);
											sSYNC_ACTION = "remote deleted";
										}
										if ( sSYNC_ACTION == "local changed" )
										{
											// 05/18/2012 Paul.  Allow control of sync direction. 
											if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
											{
												bool bChanged = qo.SetFromCRM(sSYNC_REMOTE_KEY, row, sbChanges);
												if ( bChanged )
												{
													if ( bVERBOSE_STATUS )
														SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Sending " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ". " + sbChanges.ToString());
													qo.Update();
													if ( qo.QuickBooksTableName == "Estimates" || qo.QuickBooksTableName == "SalesOrders" || qo.QuickBooksTableName == "Invoices" )
													{
														qo.SendLineItems(Context, Session, con, gUSER_ID, sbErrors);
														// 06/03/2012 Paul.  Sending line numbers will update the parent record, so make sure to get the current timestamp. 
														qo.GetTimeModified();
													}
												}
											}
										}
									}
									if ( sSYNC_ACTION == "local new" || sSYNC_ACTION == "local changed" )
									{
										if ( !Sql.IsEmptyString(qo.ID) )
										{
											// 03/25/2010 Paul.  Update the modified date after the save. 
											// 03/26/2011 Paul.  Updated is in local time. 
											DateTime dtREMOTE_DATE_MODIFIED     = qo.TimeModified;
											DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();
											using ( IDbTransaction trn = Sql.BeginTransaction(con) )
											{
												try
												{
													// 03/26/2010 Paul.  Make sure to set the Sync flag. 
													// 03/29/2010 Paul.  The Sync flag will be updated in the procedure. 
													// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
													//SqlProcs.spACCOUNTS_SYNC_Update(gUSER_ID, gID, sSYNC_REMOTE_KEY, dtREMOTE_DATE_MODIFIED, dtREMOTE_DATE_MODIFIED_UTC, "QuickBooks", String.Empty, trn);
													IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_SYNC_Update");
													cmdSyncUpdate.Transaction = trn;
													Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
													Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
													Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , gID                       );
													Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , sSYNC_REMOTE_KEY          );
													Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
													Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
													Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
													Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
													cmdSyncUpdate.ExecuteNonQuery();
													trn.Commit();
												}
												catch
												{
													trn.Rollback();
													throw;
												}
											}
										}
									}
									else if ( sSYNC_ACTION == "remote deleted" )
									{
										// 08/05/2014 Paul.  Deletes should follow the same direction rules. 
										if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
										{
											if ( bVERBOSE_STATUS )
												SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Deleting " + qo.CRMTableName + " " + Sql.ToString(row["NAME"]) + ".");
											using ( IDbTransaction trn = Sql.BeginTransaction(con) )
											{
												try
												{
													// 03/26/2010 Paul.  Make sure to clear the Sync flag. 
													// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
													//SqlProcs.spACCOUNTS_SYNC_Delete(gUSER_ID, gID, sSYNC_REMOTE_KEY, "QuickBooks", trn);
													IDbCommand cmdSyncDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_SYNC_Delete");
													cmdSyncDelete.Transaction = trn;
													Sql.SetParameter(cmdSyncDelete, "@MODIFIED_USER_ID", gUSER_ID        );
													Sql.SetParameter(cmdSyncDelete, "@ASSIGNED_USER_ID", gUSER_ID        );
													Sql.SetParameter(cmdSyncDelete, "@LOCAL_ID"        , gID             );
													Sql.SetParameter(cmdSyncDelete, "@REMOTE_KEY"      , sSYNC_REMOTE_KEY);
													Sql.SetParameter(cmdSyncDelete, "@SERVICE_NAME"    , "QuickBooks"    );
													cmdSyncDelete.ExecuteNonQuery();
												
													IDbCommand cmdDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_Delete");
													cmdDelete.Transaction = trn;
													Sql.SetParameter(cmdDelete, "@ID"              , gID           );
													Sql.SetParameter(cmdDelete, "@MODIFIED_USER_ID", gUSER_ID      );
													cmdDelete.ExecuteNonQuery();
													trn.Commit();
												}
												catch
												{
													trn.Rollback();
													throw;
												}
											}
										}
									}
								}
								catch(Exception ex)
								{
									// 03/25/2010 Paul.  Log the error, but don't exit the loop. 
									string sError = "Error creating QuickBooks " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString  (row["NAME"]) + "." + ControlChars.CrLf;
									sError += sbChanges.ToString();
									sError += Utils.ExpandException(ex) + ControlChars.CrLf;
									SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
									//SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
									sbErrors.AppendLine(sError);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				sbErrors.AppendLine(Utils.ExpandException(ex));
			}
		}
	}
}
