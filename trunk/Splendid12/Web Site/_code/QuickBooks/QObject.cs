/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class QObject
	{
		#region Properties
		protected IDbConnection           qbcon;
		protected QuickBooksClientFactory qbf  ;
		public string   QuickBooksTableName    { get; set; }
		public string   QuickBooksTableSort    { get; set; }
		public string   CRMModuleName          { get; set; }
		public string   CRMTableName           { get; set; }
		public string   CRMTableSort           { get; set; }
		public bool     CRMAssignedUser        { get; set; }

		public Guid     LOCAL_ID               { get; set; }
		public string   ID                     { get; set; }
		public bool     Deleted                { get; set; }
		public DateTime TimeCreated            { get; set; }
		public DateTime TimeModified           { get; set; }
		public string   Name                   { get; set; }
		#endregion

		public QObject(QuickBooksClientFactory qbf, IDbConnection qbcon, string sQuickBooksTableName, string sQuickBooksTableSort, string sCRMModuleName, string sCRMTableName, string sCRMTableSort, bool bCRMAssignedUser)
		{
			this.qbf                 = qbf                 ;
			this.qbcon               = qbcon               ;
			this.QuickBooksTableName = sQuickBooksTableName;
			this.QuickBooksTableSort = sQuickBooksTableSort;
			this.CRMModuleName       = sCRMModuleName      ;
			this.CRMTableName        = sCRMTableName       ;
			this.CRMTableSort        = sCRMTableSort       ;
			this.CRMAssignedUser     = bCRMAssignedUser    ;
		}

		public virtual void Reset()
		{
			this.LOCAL_ID     = Guid.Empty       ;
			this.ID           = String.Empty     ;
			this.Deleted      = false            ;
			this.TimeCreated  = DateTime.MinValue;
			this.TimeModified = DateTime.MinValue;
			this.Name         = String.Empty     ;
		}

		public virtual bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			// 02/01/2014 Paul.  Reset in Sync() not in SetFromCRM. 
			//this.Reset();
			this.ID = sID;
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.Name = Sql.ToString(row["NAME"]);
				bChanged = true;
			}
			else
			{
				if ( Compare(this.Name, row["NAME"], "NAME", sbChanges) ) { this.Name = Sql.ToString(row["NAME"]);  bChanged = true; }
			}
			return bChanged;
		}

		protected bool Compare(string sLValue, object oRValue, string sFieldName, StringBuilder sbChanges)
		{
			bool bChanged = false;
			string sRValue = Sql.ToString(oRValue);
			if ( sLValue != sRValue )
			{
				sbChanges.AppendLine(sFieldName + " changed from '" + sLValue + "' to '" + sRValue + "'.");
				bChanged = true;
			}
			return bChanged;
		}

		protected bool Compare(bool bLValue, object oRValue, string sFieldName, StringBuilder sbChanges)
		{
			bool bChanged = false;
			bool bRValue = Sql.ToBoolean(oRValue);
			if ( bLValue != bRValue )
			{
				sbChanges.AppendLine(sFieldName + " changed from '" + bLValue.ToString() + "' to '" + bRValue.ToString() + "'.");
				bChanged = true;
			}
			return bChanged;
		}

		protected bool Compare(DateTime dtLValue, object oRValue, string sFieldName, StringBuilder sbChanges)
		{
			bool bChanged = false;
			DateTime dtRValue = Sql.ToDateTime(oRValue);
			if ( dtLValue != dtRValue )
			{
				sbChanges.AppendLine(sFieldName + " changed from '" + dtLValue.ToString() + "' to '" + dtRValue.ToString() + "'.");
				bChanged = true;
			}
			return bChanged;
		}

		protected bool Compare(double dLValue, object oRValue, string sFieldName, StringBuilder sbChanges)
		{
			bool bChanged = false;
			double dRValue = Sql.ToDouble(oRValue);
			if ( dLValue != dRValue )
			{
				sbChanges.AppendLine(sFieldName + " changed from '" + dLValue.ToString() + "' to '" + dRValue.ToString() + "'.");
				bChanged = true;
			}
			return bChanged;
		}

		public virtual void SetFromQuickBooks(DataRow row)
		{
			this.Reset();
			this.ID                 = Sql.ToString  (row["ID"                ]);
			this.TimeCreated        = Sql.ToDateTime(row["TimeCreated"       ]);
			this.TimeModified       = Sql.ToDateTime(row["TimeModified"      ]);
			this.Name               = Sql.ToString  (row["Name"              ]);
		}

		public virtual string Insert()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( Name " + ControlChars.CrLf
				     + "     )      " + ControlChars.CrLf
				     + "values      " + ControlChars.CrLf
				     + "     ( @Name" + ControlChars.CrLf
				     + "     )      " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name", this.Name);
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public virtual void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set Name = @Name" + ControlChars.CrLf
				     + " where ID   = @ID  " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name", this.Name);
				Sql.AddParameter(qbcmd, "@ID"  , this.ID  );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public void GetTimeModified()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL = "select TimeModified from " + this.QuickBooksTableName + " where ID = @ID";
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ID", this.ID);
				this.TimeModified = Sql.ToDateTime(qbcmd.ExecuteScalar());
			}
		}

		public void Delete()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "delete from " + this.QuickBooksTableName + ControlChars.CrLf
				     + " where ID = @ID" + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ID", this.ID);
				qbcmd.ExecuteNonQuery();
			}
		}

		public void Get(string sID)
		{
			this.Reset();
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL = "select * from " + this.QuickBooksTableName + " where ID = @ID";
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ID", sID);
				using ( DbDataAdapter qda = qbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)qda).SelectCommand = qbcmd;
					using ( DataTable dt = new DataTable() )
					{
						qda.Fill(dt);
						if ( dt.Rows.Count > 0 )
						{
							this.SetFromQuickBooks(dt.Rows[0]);
						}
						else
						{
							this.Deleted = true;
						}
					}
				}
			}
		}

		public virtual void FilterQuickBooks(DataView vw)
		{
		}

		public virtual void FilterCRM(IDbCommand cmd)
		{
			Sql.AppendParameter(cmd, Sql.ToString(this.Name), "NAME");
		}

		public DataTable Select(DateTime dtStartModifiedDate)
		{
			DataTable dt = new DataTable();
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL = "select * from " + this.QuickBooksTableName;
				qbcmd.CommandText = sSQL;
				if ( dtStartModifiedDate > DateTime.MinValue )
				{
					// 05/15/2012 Paul.  Filtering by TimeModified does not work.  Use StartModifiedDate instead. 
					// Must use = and not >. 
					qbcmd.CommandText += " where StartModifiedDate = @StartModifiedDate";
					Sql.AddParameter(qbcmd, "@StartModifiedDate", dtStartModifiedDate);
				}
				using ( DbDataAdapter da = qbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = qbcmd;
					da.Fill(dt);
				}
			}
			return dt;
		}

		public DataTable Select(string sField, string sValue)
		{
			DataTable dt = new DataTable();
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL = "select * from " + this.QuickBooksTableName;
				qbcmd.CommandText = sSQL;
				qbcmd.CommandText += " where " + sField + " = @" + sField;
				Sql.AddParameter(qbcmd, "@" + sField, sValue);
				using ( DbDataAdapter da = qbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = qbcmd;
					da.Fill(dt);
				}
			}
			return dt;
		}

		protected bool ParameterChanged(IDbDataParameter par, object oValue, int nSize)
		{
			bool bChanged = false;
			string sValue = Sql.ToString(oValue);
			if ( sValue.Length > nSize )
				sValue = sValue.Substring(0, nSize);
			if ( Sql.ToString  (par.Value) != sValue )
				bChanged = true;
			return bChanged;
		}

		protected bool ParameterChanged(IDbDataParameter par, object oValue)
		{
			bool bChanged = false;
			switch ( par.DbType )
			{
				case DbType.Guid    :  if ( Sql.ToGuid    (par.Value) != Sql.ToGuid    (oValue) ) bChanged = true;  break;
				case DbType.Int16   :  if ( Sql.ToInteger (par.Value) != Sql.ToInteger (oValue) ) bChanged = true;  break;
				case DbType.Int32   :  if ( Sql.ToInteger (par.Value) != Sql.ToInteger (oValue) ) bChanged = true;  break;
				case DbType.Int64   :  if ( Sql.ToInteger (par.Value) != Sql.ToInteger (oValue) ) bChanged = true;  break;
				case DbType.Double  :  if ( Sql.ToDouble  (par.Value) != Sql.ToDouble  (oValue) ) bChanged = true;  break;
				case DbType.Decimal :  if ( Sql.ToDecimal (par.Value) != Sql.ToDecimal (oValue) ) bChanged = true;  break;
				case DbType.Boolean :  if ( Sql.ToBoolean (par.Value) != Sql.ToBoolean (oValue) ) bChanged = true;  break;
				case DbType.DateTime:  if ( Sql.ToDateTime(par.Value) != Sql.ToDateTime(oValue) ) bChanged = true;  break;
				default             :  if ( Sql.ToString  (par.Value) != Sql.ToString  (oValue) ) bChanged = true;  break;
			}
			return bChanged;
		}

		protected bool InitUpdateProcedure(IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID)
		{
			bool bChanged = false;
			// 03/25/2010 Paul.  We start with the existing record values so that we can apply ACL Field Security rules. 
			if ( row != null && row.Table != null )
			{
				foreach(IDbDataParameter par in spUpdate.Parameters)
				{
					// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
					// 03/28/2010 Paul.  We must assign a value to all parameters. 
					string sParameterName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
					if ( row.Table.Columns.Contains(sParameterName) )
						par.Value = row[sParameterName];
					else
						par.Value = DBNull.Value;
				}
			}
			else
			{
				bChanged = true;
				foreach(IDbDataParameter par in spUpdate.Parameters)
				{
					// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
					string sParameterName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
					if ( sParameterName == "TEAM_ID" )
						par.Value = gTEAM_ID;
					else if ( sParameterName == "ASSIGNED_USER_ID" )
						par.Value = gUSER_ID;
					// 02/20/2013 Paul.  We need to set the MODIFIED_USER_ID. 
					else if ( sParameterName == "MODIFIED_USER_ID" )
						par.Value = gUSER_ID;
					else
						par.Value = DBNull.Value;
				}
			}
			return bChanged;
		}

		public virtual bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "NAME"            :  oValue = Sql.ToDBString(this.Name);  break;
							case "MODIFIED_USER_ID":  oValue = gUSER_ID                 ;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								bChanged = ParameterChanged(par, oValue);
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}

		public virtual void ProcedureUpdated(Guid gID, string sREMOTE_KEY)
		{
		}

		public virtual void ImportLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
		}

		public virtual void SendLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
		}

		public bool Import(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, string sDIRECTION, StringBuilder sbErrors)
		{
			HttpApplicationState Application = Context.Application;
			bool   bVERBOSE_STATUS      = Sql.ToBoolean(Application["CONFIG.QuickBooks.VerboseStatus"     ]);
			string sCONFLICT_RESOLUTION = Sql.ToString (Application["CONFIG.QuickBooks.ConflictResolution"]);
			Guid   gTEAM_ID             = Sql.ToGuid   (Session["TEAM_ID"]);
			
			IDbCommand spUpdate = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_Update");

			bool     bImported    = false;
			string   sREMOTE_KEY  = this.ID;
			string   sName        = Sql.ToString(this.Name);
			DateTime dtREMOTE_DATE_MODIFIED     = this.TimeModified;
			DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();

			String sSQL = String.Empty;
			sSQL = "select SYNC_ID                                       " + ControlChars.CrLf
			     + "     , SYNC_LOCAL_ID                                 " + ControlChars.CrLf
			     + "     , SYNC_LOCAL_DATE_MODIFIED_UTC                  " + ControlChars.CrLf
			     + "     , SYNC_REMOTE_DATE_MODIFIED_UTC                 " + ControlChars.CrLf
			     + "     , ID                                            " + ControlChars.CrLf
			     + "     , DATE_MODIFIED_UTC                             " + ControlChars.CrLf
			     + "  from vw" + this.CRMTableName + "_SYNC              " + ControlChars.CrLf
			     + " where SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
			     + "   and SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf
			     + "   and SYNC_REMOTE_KEY       = @SYNC_REMOTE_KEY      " + ControlChars.CrLf;
			DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.CommandText = sSQL;
				Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID   );
				Sql.AddParameter(cmd, "@SYNC_REMOTE_KEY"      , sREMOTE_KEY);
				using ( DbDataAdapter da = dbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = cmd;
					string sSYNC_ACTION   = String.Empty;
					Guid   gSYNC_LOCAL_ID = Guid.Empty;
					using ( DataTable dt = new DataTable() )
					{
						da.Fill(dt);
						if ( dt.Rows.Count > 0 )
						{
							DataRow row = dt.Rows[0];
							this.LOCAL_ID                            = Sql.ToGuid    (row["ID"                           ]);
							gSYNC_LOCAL_ID                           = Sql.ToGuid    (row["SYNC_LOCAL_ID"                ]);
							DateTime dtSYNC_LOCAL_DATE_MODIFIED_UTC  = Sql.ToDateTime(row["SYNC_LOCAL_DATE_MODIFIED_UTC" ]);
							DateTime dtSYNC_REMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(row["SYNC_REMOTE_DATE_MODIFIED_UTC"]);
							DateTime dtDATE_MODIFIED_UTC             = Sql.ToDateTime(row["DATE_MODIFIED_UTC"            ]);
							// 03/28/2010 Paul.  If the ID is NULL and the LOCAL_ID is NOT NULL, then the local item must have been deleted. 
							if ( (Sql.IsEmptyGuid(this.LOCAL_ID) && !Sql.IsEmptyGuid(gSYNC_LOCAL_ID)) )
							{
								sSYNC_ACTION = "local deleted";
							}
							// 03/26/2011 Paul.  The QuickBooks remote date can vary by 1 millisecond, so check for local change first. 
							else if ( dtREMOTE_DATE_MODIFIED_UTC > dtSYNC_REMOTE_DATE_MODIFIED_UTC.AddMilliseconds(10) && dtDATE_MODIFIED_UTC > dtSYNC_LOCAL_DATE_MODIFIED_UTC )
							{
								if ( sCONFLICT_RESOLUTION == "remote" )
								{
									// 03/24/2010 Paul.  Remote is the winner of conflicts. 
									sSYNC_ACTION = "remote changed";
								}
								else if ( sCONFLICT_RESOLUTION == "local" )
								{
									// 03/24/2010 Paul.  Local is the winner of conflicts. 
									sSYNC_ACTION = "local changed";
								}
								// 03/26/2011 Paul.  The QuickBooks remote date can vary by 1 millisecond, so check for local change first. 
								else if ( dtDATE_MODIFIED_UTC.AddHours(1) > dtREMOTE_DATE_MODIFIED_UTC )
								{
									// 03/25/2010 Paul.  Attempt to allow the end-user to resolve by editing the local or remote item. 
									sSYNC_ACTION = "local changed";
								}
								else if ( dtREMOTE_DATE_MODIFIED_UTC.AddHours(1) > dtDATE_MODIFIED_UTC )
								{
									// 03/25/2010 Paul.  Attempt to allow the end-user to resolve by editing the local or remote item. 
									sSYNC_ACTION = "remote changed";
								}
								else
								{
									sSYNC_ACTION = "prompt change";
								}
								if ( this.Deleted )
								{
									sSYNC_ACTION = "remote deleted";
								}
								
							}
							// 03/26/2011 Paul.  The QuickBooks remote date can vary by 1 millisecond, so check for local change first. 
							else if ( dtREMOTE_DATE_MODIFIED_UTC > dtSYNC_REMOTE_DATE_MODIFIED_UTC.AddMilliseconds(10) )
							{
								// 03/24/2010 Paul.  Remote Record has changed, but Local has not. 
								sSYNC_ACTION = "remote changed";
							}
							else if ( dtDATE_MODIFIED_UTC > dtSYNC_LOCAL_DATE_MODIFIED_UTC )
							{
								// 03/24/2010 Paul.  Local Record has changed, but Remote has not. 
								sSYNC_ACTION = "local changed";
							}
						}
						else if ( this.Deleted )
						{
							sSYNC_ACTION = "remote deleted";
						}
						else
						{
							sSYNC_ACTION = "remote new";
							
							// 03/25/2010 Paul.  If we find a account, then treat the CRM record as the master and send latest version over to QuickBooks. 
							// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
							// 05/30/2012 Paul.  There can only be one record mapped to QuickBooks, so we don't need to join to the remote key. 
							cmd.Parameters.Clear();
							sSQL = "select vw" + this.CRMTableName + ".ID             " + ControlChars.CrLf
							     + "  from            " + Sql.MetadataName(con, "vw" + this.CRMTableName + "_QuickBooks") + " vw" + this.CRMTableName   + ControlChars.CrLf
							     + "  left outer join vw" + this.CRMTableName + "_SYNC" + ControlChars.CrLf
							     + "               on vw" + this.CRMTableName + "_SYNC.SYNC_SERVICE_NAME     = N'QuickBooks'         " + ControlChars.CrLf
							     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID" + ControlChars.CrLf
							//     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_REMOTE_KEY       = @SYNC_REMOTE_KEY      " + ControlChars.CrLf
							     + "              and vw" + this.CRMTableName + "_SYNC.SYNC_LOCAL_ID         = vw" + this.CRMTableName + ".ID" + ControlChars.CrLf;
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID   );
							//Sql.AddParameter(cmd, "@SYNC_REMOTE_KEY"      , sREMOTE_KEY);
							ExchangeSecurity.Filter(Application, Session, cmd, gUSER_ID, this.CRMModuleName, "view");
							this.FilterCRM(cmd);
							cmd.CommandText += "   and vw" + this.CRMTableName + "_SYNC.ID is null" + ControlChars.CrLf;
							this.LOCAL_ID = Sql.ToGuid(cmd.ExecuteScalar());
							if ( !Sql.IsEmptyGuid(this.LOCAL_ID) )
							{
								// 03/25/2010 Paul.  If we find a account, then treat the CRM record as the master and send latest version over to QuickBooks. 
								sSYNC_ACTION = "local new";
							}
						}
					}
					using ( DataTable dt = new DataTable() )
					{
						DataRow row = null;
						Guid gASSIGNED_USER_ID = Guid.Empty;
						if ( sSYNC_ACTION == "remote new" || sSYNC_ACTION == "remote changed" || sSYNC_ACTION == "local changed" || sSYNC_ACTION == "local new" )
						{
							if ( !Sql.IsEmptyGuid(this.LOCAL_ID) )
							{
								cmd.Parameters.Clear();
								sSQL = "select *         " + ControlChars.CrLf
								     + "  from vw" + this.CRMTableName + ControlChars.CrLf
								     + " where ID = @ID  " + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID", this.LOCAL_ID);
								
								((IDbDataAdapter)da).SelectCommand = cmd;
								da.Fill(dt);
								if ( dt.Rows.Count > 0 )
								{
									row = dt.Rows[0];
									if ( this.CRMAssignedUser )
										gASSIGNED_USER_ID = Sql.ToGuid(row["ASSIGNED_USER_ID"]);
								}
							}
						}
						if ( sSYNC_ACTION == "remote new" || sSYNC_ACTION == "remote changed" )
						{
							// 05/18/2012 Paul.  Allow control of sync direction. 
							if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
							{
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), this.QuickBooksTableName + ".Import: Retrieving " + this.QuickBooksTableName + " " + sName + ".");
									
										spUpdate.Transaction = trn;
										// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
										// 01/28/2012 Paul.  The transaction is necessary so that an account can be created. 
										bool bChanged = this.BuildUpdateProcedure(Application, Session, spUpdate, row, gUSER_ID, gTEAM_ID, gASSIGNED_USER_ID, trn);
										if ( bChanged )
										{
											spUpdate.ExecuteNonQuery();
											IDbDataParameter parID = Sql.FindParameter(spUpdate, "@ID");
											this.LOCAL_ID = Sql.ToGuid(parID.Value);
											this.ProcedureUpdated(this.LOCAL_ID, sREMOTE_KEY);
										}
										// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
										IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_SYNC_Update");
										cmdSyncUpdate.Transaction = trn;
										Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
										Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
										Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , this.LOCAL_ID             );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , sREMOTE_KEY               );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
										Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
										Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
										Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
										cmdSyncUpdate.ExecuteNonQuery();
										trn.Commit();
										bImported = true;
									}
									catch
									{
										trn.Rollback();
										throw;
									}
								}
							}
						}
						else if ( (sSYNC_ACTION == "local changed" || sSYNC_ACTION == "local new") && !Sql.IsEmptyGuid(this.LOCAL_ID) )
						{
							// 03/25/2010 Paul.  If we find a account, then treat the CRM record as the master and send latest version over to QuickBooks. 
							if ( dt.Rows.Count > 0 )
							{
								row = dt.Rows[0];
								if ( SplendidInit.bEnableACLFieldSecurity )
								{
									bool bApplyACL = false;
									foreach ( DataColumn col in dt.Columns )
									{
										Security.ACL_FIELD_ACCESS acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, col.ColumnName, gASSIGNED_USER_ID);
										if ( !acl.IsReadable() )
										{
											row[col.ColumnName] = DBNull.Value;
											bApplyACL = true;
										}
									}
									if ( bApplyACL )
										dt.AcceptChanges();
								}
								StringBuilder sbChanges = new StringBuilder();
								try
								{
									if ( bVERBOSE_STATUS )
										SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), this.QuickBooksTableName + ".Import: Syncing " + this.QuickBooksTableName + " " + Sql.ToString(row["NAME"]) + ".");
									// 05/18/2012 Paul.  Allow control of sync direction. 
									if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
									{
										bool bChanged = this.SetFromCRM(sREMOTE_KEY, row, sbChanges);
										if ( bChanged )
										{
											this.Update();
										}
									}
									// 03/25/2010 Paul.  Update the modified date after the save. 
									// 03/26/2011 Paul.  Updated is in local time. 
									dtREMOTE_DATE_MODIFIED     = this.TimeModified;
									dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED_UTC.ToUniversalTime();
									using ( IDbTransaction trn = Sql.BeginTransaction(con) )
									{
										try
										{
											// 03/26/2010 Paul.  Make sure to set the Sync flag. 
											// 03/29/2010 Paul.  The Sync flag will be updated in the procedure. 
											// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
											IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_SYNC_Update");
											cmdSyncUpdate.Transaction = trn;
											Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
											Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
											Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , this.LOCAL_ID             );
											Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , sREMOTE_KEY               );
											Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
											Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
											Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
											Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
											cmdSyncUpdate.ExecuteNonQuery();
											trn.Commit();
										}
										catch
										{
											trn.Rollback();
											throw;
										}
									}
								}
								catch(Exception ex)
								{
									// 03/25/2010 Paul.  Log the error, but don't exit the loop. 
									string sError = "Error saving " + Sql.ToString  (row["NAME"]) + "." + ControlChars.CrLf;
									sError += sbChanges.ToString();
									sError += Utils.ExpandException(ex) + ControlChars.CrLf;
									SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
									//SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
									sbErrors.AppendLine(sError);
								}
							}
						}
						else if ( sSYNC_ACTION == "local deleted" )
						{
							try
							{
								// 05/18/2012 Paul.  Allow control of sync direction. 
								if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
								{
									this.Delete();
								}
							}
							catch(Exception ex)
							{
								string sError = "Error deleting QuickBooks " + this.QuickBooksTableName + " " + sName + "." + ControlChars.CrLf;
								sError += Utils.ExpandException(ex) + ControlChars.CrLf;
								SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
								sbErrors.AppendLine(sError);
							}
							try
							{
								if ( bVERBOSE_STATUS )
									SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), this.QuickBooksTableName + ".Import: Deleting " + sName + ".");
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
										IDbCommand cmdSyncDelete = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_SYNC_Delete");
										cmdSyncDelete.Transaction = trn;
										Sql.SetParameter(cmdSyncDelete, "@MODIFIED_USER_ID", gUSER_ID      );
										Sql.SetParameter(cmdSyncDelete, "@ASSIGNED_USER_ID", gUSER_ID      );
										Sql.SetParameter(cmdSyncDelete, "@LOCAL_ID"        , gSYNC_LOCAL_ID);
										Sql.SetParameter(cmdSyncDelete, "@REMOTE_KEY"      , sREMOTE_KEY   );
										Sql.SetParameter(cmdSyncDelete, "@SERVICE_NAME"    , "QuickBooks"  );
										cmdSyncDelete.ExecuteNonQuery();
										trn.Commit();
									}
									catch
									{
										trn.Rollback();
										throw;
									}
								}
							}
							catch(Exception ex)
							{
								string sError = "Error deleting " + sName + "." + ControlChars.CrLf;
								sError += Utils.ExpandException(ex) + ControlChars.CrLf;
								SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
								sbErrors.AppendLine(sError);
							}
						}
						else if ( sSYNC_ACTION == "remote deleted" && !Sql.IsEmptyGuid(this.LOCAL_ID) )
						{
							// 08/05/2014 Paul.  Deletes should follow the same direction rules. 
							if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
							{
								if ( bVERBOSE_STATUS )
									SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), this.QuickBooksTableName + ".Import: Deleting " + this.CRMTableName + " " + sName + ".");
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										// 12/19/2011 Paul.  Add SERVICE_NAME to allow multiple sync targets. 
										IDbCommand cmdSyncDelete = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_SYNC_Delete");
										cmdSyncDelete.Transaction = trn;
										Sql.SetParameter(cmdSyncDelete, "@MODIFIED_USER_ID", gUSER_ID      );
										Sql.SetParameter(cmdSyncDelete, "@ASSIGNED_USER_ID", gUSER_ID      );
										Sql.SetParameter(cmdSyncDelete, "@LOCAL_ID"        , this.LOCAL_ID );
										Sql.SetParameter(cmdSyncDelete, "@REMOTE_KEY"      , sREMOTE_KEY   );
										Sql.SetParameter(cmdSyncDelete, "@SERVICE_NAME"    , "QuickBooks"  );
										cmdSyncDelete.ExecuteNonQuery();
										
										IDbCommand cmdDelete = SqlProcs.Factory(con, "sp" + this.CRMTableName + "_Delete");
										cmdDelete.Transaction = trn;
										Sql.SetParameter(cmdDelete, "@ID"              , this.LOCAL_ID );
										Sql.SetParameter(cmdDelete, "@MODIFIED_USER_ID", gUSER_ID      );
										cmdDelete.ExecuteNonQuery();
										trn.Commit();
									}
									catch
									{
										trn.Rollback();
										throw;
									}
								}
							}
						}
					}
				}
			}
			return bImported;
		}
	}
}
