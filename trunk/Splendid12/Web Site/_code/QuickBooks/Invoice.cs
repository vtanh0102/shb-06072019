/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class Invoice : QOrder
	{
		#region Properties
		//public string   ShipMethod                 { get; set; }
		public string   ShipMethodId               { get; set; }
		public bool     IsPending                  { get; set; }
		public double   Amount                     { get; set; }
		public double   Balance                    { get; set; }
		public string   ShippingLine1              { get; set; }
		public string   ShippingLine2              { get; set; }
		public string   ShippingLine3              { get; set; }
		public string   ShippingLine4              { get; set; }
		public string   ShippingLine5              { get; set; }
		public string   ShippingCity               { get; set; }
		public string   ShippingState              { get; set; }
		public string   ShippingPostalCode         { get; set; }
		public string   ShippingCountry            { get; set; }
		#endregion

		public Invoice(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtShippers, DataTable dtTaxRates, DataTable dtItems, DataTable dtCustomers, DbProviderFactory dbf, IDbConnection con) : base(qbf, qbcon, dtShippers, dtTaxRates, dtItems, dtCustomers, dbf, con, "Invoices", "Invoices", "INVOICES", "INVOICE_NUM", true)
		{
		}

		public override void Reset()
		{
			base.Reset();
			//this.ShipMethod         = String.Empty;
			this.ShipMethodId       = String.Empty;
			this.IsPending          = true;
			this.Amount             = 0.0;
			this.Balance            = 0.0;
			this.ShippingLine1      = String.Empty;
			this.ShippingLine2      = String.Empty;
			this.ShippingLine3      = String.Empty;
			this.ShippingLine4      = String.Empty;
			this.ShippingLine5      = String.Empty;
			this.ShippingCity       = String.Empty;
			this.ShippingState      = String.Empty;
			this.ShippingPostalCode = String.Empty;
			this.ShippingCountry    = String.Empty;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = base.SetFromCRM(sID, row, sbChanges);
			
			bool bIsPending = Sql.ToString(row["INVOICE_STAGE"]) != "Paid";
			string sShipMethodId = String.Empty;
			vwShippers.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["SHIPPER_ID"]) + "'";
			if ( vwShippers.Count > 0 )
				sShipMethodId = Sql.ToString(vwShippers[0]["SYNC_REMOTE_KEY"]);
			string sShippingAddress = Sql.ToString(row["SHIPPING_ADDRESS_STREET"]).Replace(ControlChars.CrLf, "\n");
			List<string> arrShippingAddress = new List<string>(sShippingAddress.Split('\n'));
			while ( arrShippingAddress.Count < 5 )
				arrShippingAddress.Add(String.Empty);
			
			// 05/31/2012 Paul.  The Name is only used for logging. 
			this.Name = Sql.ToString(row["INVOICE_NUM"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.ReferenceNumber    = Sql.ToString  (row["INVOICE_NUM"                ]);
				this.Date               = Sql.ToDateTime(row["DUE_DATE"                   ]);
				this.DueDate            = Sql.ToDateTime(row["DUE_DATE"                   ]);
				//this.ShipMethod         = Sql.ToString  (row["SHIPPER_NAME"               ]);
				this.ShipMethodId       = Sql.ToString  (    sShipMethodId                 );
				this.IsPending          = Sql.ToBoolean (    bIsPending                    );
				this.Amount             = Sql.ToDouble  (row["TOTAL_USDOLLAR"             ]);
				this.Balance            = Sql.ToDouble  (row["AMOUNT_DUE_USDOLLAR"        ]);
				this.ShippingLine1      = Sql.ToString  (    arrShippingAddress[0]         );
				this.ShippingLine2      = Sql.ToString  (    arrShippingAddress[1]         );
				this.ShippingLine3      = Sql.ToString  (    arrShippingAddress[2]         );
				this.ShippingLine4      = Sql.ToString  (    arrShippingAddress[3]         );
				this.ShippingLine5      = Sql.ToString  (    arrShippingAddress[4]         );
				this.ShippingCity       = Sql.ToString  (row["SHIPPING_ADDRESS_CITY"      ]);
				this.ShippingState      = Sql.ToString  (row["SHIPPING_ADDRESS_STATE"     ]);
				this.ShippingPostalCode = Sql.ToString  (row["SHIPPING_ADDRESS_POSTALCODE"]);
				this.ShippingCountry    = Sql.ToString  (row["SHIPPING_ADDRESS_COUNTRY"   ]);
			}
			else
			{
				if ( Compare(this.ReferenceNumber   , row["INVOICE_NUM"                ], "INVOICE_NUM"                , sbChanges) ) { this.ReferenceNumber    = Sql.ToString  (row["INVOICE_NUM"                ]);  bChanged = true; }
				if ( Compare(this.Date              , row["DUE_DATE"                   ], "DUE_DATE"                   , sbChanges) ) { this.Date               = Sql.ToDateTime(row["DUE_DATE"                   ]);  bChanged = true; }
				if ( Compare(this.DueDate           , row["DUE_DATE"                   ], "DUE_DATE"                   , sbChanges) ) { this.DueDate            = Sql.ToDateTime(row["DUE_DATE"                   ]);  bChanged = true; }
				//if ( Compare(this.ShipMethod        , row["SHIPPER_NAME"               ], "SHIPPER_NAME"               , sbChanges) ) { this.ShipMethod         = Sql.ToString  (row["SHIPPER_NAME"               ]);  bChanged = true; }
				if ( Compare(this.ShipMethodId      ,     sShipMethodId                 , "SHIPPER_ID"                 , sbChanges) ) { this.ShipMethodId       = Sql.ToString  (    sShipMethodId                 );  bChanged = true; }
				if ( Compare(this.IsPending         ,     bIsPending                    , "INVOICE_STAGE"              , sbChanges) ) { this.IsPending          = Sql.ToBoolean (    bIsPending                    );  bChanged = true; }
				if ( Compare(this.Amount            , row["TOTAL_USDOLLAR"             ], "TOTAL_USDOLLAR"             , sbChanges) ) { this.Amount             = Sql.ToDouble  (row["TOTAL_USDOLLAR"             ]);  bChanged = true; }
				if ( Compare(this.Balance           , row["AMOUNT_DUE_USDOLLAR"        ], "AMOUNT_DUE_USDOLLAR"        , sbChanges) ) { this.Balance            = Sql.ToDouble  (row["AMOUNT_DUE_USDOLLAR"        ]);  bChanged = true; }
				if ( Compare(this.ShippingLine1     ,     arrShippingAddress[0]         , "ShippingLine1"              , sbChanges) ) { this.ShippingLine1      = Sql.ToString  (    arrShippingAddress[0]         );  bChanged = true; }
				if ( Compare(this.ShippingLine2     ,     arrShippingAddress[1]         , "ShippingLine2"              , sbChanges) ) { this.ShippingLine2      = Sql.ToString  (    arrShippingAddress[1]         );  bChanged = true; }
				if ( Compare(this.ShippingLine3     ,     arrShippingAddress[2]         , "ShippingLine3"              , sbChanges) ) { this.ShippingLine3      = Sql.ToString  (    arrShippingAddress[2]         );  bChanged = true; }
				if ( Compare(this.ShippingLine4     ,     arrShippingAddress[3]         , "ShippingLine4"              , sbChanges) ) { this.ShippingLine4      = Sql.ToString  (    arrShippingAddress[3]         );  bChanged = true; }
				if ( Compare(this.ShippingLine5     ,     arrShippingAddress[4]         , "ShippingLine5"              , sbChanges) ) { this.ShippingLine5      = Sql.ToString  (    arrShippingAddress[4]         );  bChanged = true; }
				if ( Compare(this.ShippingCity      , row["SHIPPING_ADDRESS_CITY"      ], "SHIPPING_ADDRESS_CITY"      , sbChanges) ) { this.ShippingCity       = Sql.ToString  (row["SHIPPING_ADDRESS_CITY"      ]);  bChanged = true; }
				if ( Compare(this.ShippingState     , row["SHIPPING_ADDRESS_STATE"     ], "SHIPPING_ADDRESS_STATE"     , sbChanges) ) { this.ShippingState      = Sql.ToString  (row["SHIPPING_ADDRESS_STATE"     ]);  bChanged = true; }
				if ( Compare(this.ShippingPostalCode, row["SHIPPING_ADDRESS_POSTALCODE"], "SHIPPING_ADDRESS_POSTALCODE", sbChanges) ) { this.ShippingPostalCode = Sql.ToString  (row["SHIPPING_ADDRESS_POSTALCODE"]);  bChanged = true; }
				if ( Compare(this.ShippingCountry   , row["SHIPPING_ADDRESS_COUNTRY"   ], "SHIPPING_ADDRESS_COUNTRY"   , sbChanges) ) { this.ShippingCountry    = Sql.ToString  (row["SHIPPING_ADDRESS_COUNTRY"   ]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.Date               = Sql.ToDateTime(row["Date"              ]);
			this.DueDate            = Sql.ToDateTime(row["DueDate"           ]);
			//this.ShipMethod         = Sql.ToString  (row["ShipMethod"        ]);
			this.ShipMethodId       = Sql.ToString  (row["ShipMethodId"      ]);
			this.IsPending          = Sql.ToBoolean (row["IsPending"         ]);
			this.Amount             = Sql.ToDouble  (row["Amount"            ]);
			this.Balance            = Sql.ToDouble  (row["Balance"           ]);
			this.ShippingLine1      = Sql.ToString  (row["ShippingLine1"     ]);
			this.ShippingLine2      = Sql.ToString  (row["ShippingLine2"     ]);
			this.ShippingLine3      = Sql.ToString  (row["ShippingLine3"     ]);
			this.ShippingLine4      = Sql.ToString  (row["ShippingLine4"     ]);
			this.ShippingLine5      = Sql.ToString  (row["ShippingLine5"     ]);
			this.ShippingCity       = Sql.ToString  (row["ShippingCity"      ]);
			this.ShippingState      = Sql.ToString  (row["ShippingState"     ]);
			this.ShippingPostalCode = Sql.ToString  (row["ShippingPostalCode"]);
			this.ShippingCountry    = Sql.ToString  (row["ShippingCountry"   ]);
		}

		public override string Insert()
		{
			// 06/02/2012 Paul.  Can't use CustomerName as it can include the job as part of the hierarchy. 
			// 06/02/2012 Paul.  Don't need both ShipMethod and ShipMethodId. 
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( ReferenceNumber   " + ControlChars.CrLf
				//     + "     , CustomerName      " + ControlChars.CrLf
				     + "     , CustomerId        " + ControlChars.CrLf
				     + "     , Date              " + ControlChars.CrLf
				     + "     , DueDate           " + ControlChars.CrLf
				//     + "     , ShipMethod        " + ControlChars.CrLf
				     + "     , ShipMethodId      " + ControlChars.CrLf
				     + "     , Memo              " + ControlChars.CrLf
				     + "     , POnumber          " + ControlChars.CrLf
				     + "     , Terms             " + ControlChars.CrLf
				     + "     , IsPending         " + ControlChars.CrLf
				//     + "     , ExchangeRate      " + ControlChars.CrLf
				     + "     , Subtotal          " + ControlChars.CrLf
				     + "     , Tax               " + ControlChars.CrLf
				     + "     , TaxAgencyId       " + ControlChars.CrLf
				     + "     , Amount            " + ControlChars.CrLf
				     + "     , BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity       " + ControlChars.CrLf
				     + "     , BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry    " + ControlChars.CrLf
				     + "     , ShippingLine1     " + ControlChars.CrLf
				     + "     , ShippingLine2     " + ControlChars.CrLf
				     + "     , ShippingLine3     " + ControlChars.CrLf
				     + "     , ShippingLine4     " + ControlChars.CrLf
				     + "     , ShippingLine5     " + ControlChars.CrLf
				     + "     , ShippingCity      " + ControlChars.CrLf
				     + "     , ShippingState     " + ControlChars.CrLf
				     + "     , ShippingPostalCode" + ControlChars.CrLf
				     + "     , ShippingCountry   " + ControlChars.CrLf
				     + "     , ItemAggregate     " + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @ReferenceNumber   " + ControlChars.CrLf
				//     + "     , @CustomerName      " + ControlChars.CrLf
				     + "     , @CustomerId        " + ControlChars.CrLf
				     + "     , @Date              " + ControlChars.CrLf
				     + "     , @DueDate           " + ControlChars.CrLf
				//     + "     , @ShipMethod        " + ControlChars.CrLf
				     + "     , @ShipMethodId      " + ControlChars.CrLf
				     + "     , @Memo              " + ControlChars.CrLf
				     + "     , @POnumber          " + ControlChars.CrLf
				     + "     , @Terms             " + ControlChars.CrLf
				     + "     , @IsPending         " + ControlChars.CrLf
				//     + "     , @ExchangeRate      " + ControlChars.CrLf
				     + "     , @Subtotal          " + ControlChars.CrLf
				     + "     , @Tax               " + ControlChars.CrLf
				     + "     , @TaxAgencyId       " + ControlChars.CrLf
				     + "     , @Amount            " + ControlChars.CrLf
				     + "     , @BillingLine1      " + ControlChars.CrLf
				     + "     , @BillingLine2      " + ControlChars.CrLf
				     + "     , @BillingLine3      " + ControlChars.CrLf
				     + "     , @BillingLine4      " + ControlChars.CrLf
				     + "     , @BillingLine5      " + ControlChars.CrLf
				     + "     , @BillingCity       " + ControlChars.CrLf
				     + "     , @BillingState      " + ControlChars.CrLf
				     + "     , @BillingPostalCode " + ControlChars.CrLf
				     + "     , @BillingCountry    " + ControlChars.CrLf
				     + "     , @ShippingLine1     " + ControlChars.CrLf
				     + "     , @ShippingLine2     " + ControlChars.CrLf
				     + "     , @ShippingLine3     " + ControlChars.CrLf
				     + "     , @ShippingLine4     " + ControlChars.CrLf
				     + "     , @ShippingLine5     " + ControlChars.CrLf
				     + "     , @ShippingCity      " + ControlChars.CrLf
				     + "     , @ShippingState     " + ControlChars.CrLf
				     + "     , @ShippingPostalCode" + ControlChars.CrLf
				     + "     , @ShippingCountry   " + ControlChars.CrLf
				     + "     , @ItemAggregate     " + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ReferenceNumber"   , this.ReferenceNumber   ,   11);
				//Sql.AddParameter(qbcmd, "@CustomerName"      , this.CustomerName      ,   41);
				Sql.AddParameter(qbcmd, "@CustomerId"        , this.CustomerId        );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Date"              , this.Date              );
				Sql.AddParameter(qbcmd, "@DueDate"           , this.DueDate           );
				//Sql.AddParameter(qbcmd, "@ShipMethod"        , this.ShipMethod        ,   15);
				Sql.AddParameter(qbcmd, "@ShipMethodId"      , this.ShipMethodId      );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Memo"              , this.Memo              , 4095);
				Sql.AddParameter(qbcmd, "@POnumber"          , this.POnumber          ,   25);
				Sql.AddParameter(qbcmd, "@Terms"             , this.Terms             ,   21);
				Sql.AddParameter(qbcmd, "@IsPending"         , this.IsPending         );
				//Sql.AddParameter(qbcmd, "@ExchangeRate"      , this.ExchangeRate      );
				Sql.AddParameter(qbcmd, "@Subtotal"          , this.Subtotal          );
				Sql.AddParameter(qbcmd, "@Tax"               , this.Tax               );
				Sql.AddParameter(qbcmd, "@TaxAgencyId"       , this.TaxAgencyId       );
				Sql.AddParameter(qbcmd, "@Amount"            , this.Amount            );
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				Sql.AddParameter(qbcmd, "@ShippingLine1"     , this.ShippingLine1     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine2"     , this.ShippingLine2     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine3"     , this.ShippingLine3     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine4"     , this.ShippingLine4     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine5"     , this.ShippingLine5     , 41);
				Sql.AddParameter(qbcmd, "@ShippingCity"      , this.ShippingCity      , 31);
				Sql.AddParameter(qbcmd, "@ShippingState"     , this.ShippingState     , 21);
				Sql.AddParameter(qbcmd, "@ShippingPostalCode", this.ShippingPostalCode, 13);
				Sql.AddParameter(qbcmd, "@ShippingCountry"   , this.ShippingCountry   , 31);
				Sql.AddParameter(qbcmd, "@ItemAggregate"     , this.GetItemAggregate(dbf, con, this.LOCAL_ID, vwItems));
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			// 05/22/2012 Paul.  We need to return the of the first line item when inserting an Estimate, SalesOrder or Invoice. 
			GetFirstLineItem();
			return this.ID;
		}

		public override void Update()
		{
			// 06/02/2012 Paul.  Can't use CustomerName as it can include the job as part of the hierarchy. 
			// 06/02/2012 Paul.  Don't need both ShipMethod and ShipMethodId. 
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set ReferenceNumber    = @ReferenceNumber   " + ControlChars.CrLf
				//     + "     , CustomerName       = @CustomerName      " + ControlChars.CrLf
				     + "     , CustomerId         = @CustomerId        " + ControlChars.CrLf
				     + "     , Date               = @Date              " + ControlChars.CrLf
				     + "     , DueDate            = @DueDate           " + ControlChars.CrLf
				//     + "     , ShipMethod         = @ShipMethod        " + ControlChars.CrLf
				     + "     , ShipMethodId       = @ShipMethodId      " + ControlChars.CrLf
				     + "     , Memo               = @Memo              " + ControlChars.CrLf
				     + "     , POnumber           = @POnumber          " + ControlChars.CrLf
				     + "     , Terms              = @Terms             " + ControlChars.CrLf
				     + "     , IsPending          = @IsPending         " + ControlChars.CrLf
				//     + "     , ExchangeRate       = @ExchangeRate      " + ControlChars.CrLf
				     + "     , Subtotal           = @Subtotal          " + ControlChars.CrLf
				     + "     , Tax                = @Tax               " + ControlChars.CrLf
				     + "     , TaxAgencyId        = @TaxAgencyId       " + ControlChars.CrLf
				     + "     , Amount             = @Amount            " + ControlChars.CrLf
				     + "     , BillingLine1       = @BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2       = @BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3       = @BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4       = @BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5       = @BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity        = @BillingCity       " + ControlChars.CrLf
				     + "     , BillingState       = @BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode  = @BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry     = @BillingCountry    " + ControlChars.CrLf
				     + "     , ShippingLine1      = @ShippingLine1     " + ControlChars.CrLf
				     + "     , ShippingLine2      = @ShippingLine2     " + ControlChars.CrLf
				     + "     , ShippingLine3      = @ShippingLine3     " + ControlChars.CrLf
				     + "     , ShippingLine4      = @ShippingLine4     " + ControlChars.CrLf
				     + "     , ShippingLine5      = @ShippingLine5     " + ControlChars.CrLf
				     + "     , ShippingCity       = @ShippingCity      " + ControlChars.CrLf
				     + "     , ShippingState      = @ShippingState     " + ControlChars.CrLf
				     + "     , ShippingPostalCode = @ShippingPostalCode" + ControlChars.CrLf
				     + "     , ShippingCountry    = @ShippingCountry   " + ControlChars.CrLf
				     + " where ID                 = @ID                " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ReferenceNumber"   , this.ReferenceNumber   ,   11);
				//Sql.AddParameter(qbcmd, "@CustomerName"      , this.CustomerName      ,   41);
				Sql.AddParameter(qbcmd, "@CustomerId"        , this.CustomerId        );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Date"              , this.Date              );
				Sql.AddParameter(qbcmd, "@DueDate"           , this.DueDate           );
				//Sql.AddParameter(qbcmd, "@ShipMethod"        , this.ShipMethod        ,   15);
				Sql.AddParameter(qbcmd, "@ShipMethodId"      , this.ShipMethodId      );  // 05/24/2012 Paul.  Do not truncate Id lengths. 
				Sql.AddParameter(qbcmd, "@Memo"              , this.Memo              , 4095);
				Sql.AddParameter(qbcmd, "@POnumber"          , this.POnumber          ,   25);
				Sql.AddParameter(qbcmd, "@Terms"             , this.Terms             ,   21);
				Sql.AddParameter(qbcmd, "@IsPending"         , this.IsPending         );
				//Sql.AddParameter(qbcmd, "@ExchangeRate"      , this.ExchangeRate      );
				Sql.AddParameter(qbcmd, "@Subtotal"          , this.Subtotal          );
				Sql.AddParameter(qbcmd, "@Tax"               , this.Tax               );
				Sql.AddParameter(qbcmd, "@TaxAgencyId"       , this.TaxAgencyId       );
				Sql.AddParameter(qbcmd, "@Amount"            , this.Amount            );
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				Sql.AddParameter(qbcmd, "@ShippingLine1"     , this.ShippingLine1     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine2"     , this.ShippingLine2     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine3"     , this.ShippingLine3     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine4"     , this.ShippingLine4     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine5"     , this.ShippingLine5     , 41);
				Sql.AddParameter(qbcmd, "@ShippingCity"      , this.ShippingCity      , 31);
				Sql.AddParameter(qbcmd, "@ShippingState"     , this.ShippingState     , 21);
				Sql.AddParameter(qbcmd, "@ShippingPostalCode", this.ShippingPostalCode, 13);
				Sql.AddParameter(qbcmd, "@ShippingCountry"   , this.ShippingCountry   , 31);
				Sql.AddParameter(qbcmd, "@ID"                , this.ID                );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterCRM(IDbCommand cmd)
		{
			Sql.AppendParameter(cmd, Sql.ToString(this.ReferenceNumber), "INVOICE_NUM");
		}

		public override void ImportLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
			QuickBooks.InvoiceLineItem qo = new QuickBooks.InvoiceLineItem(qbf, qbcon, vwItems.Table, this.LOCAL_ID, this.ID);
			this.ImportLineItems(Context, Session, con, gUSER_ID, qo, sbErrors);
		}

		public override void SendLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, StringBuilder sbErrors)
		{
			QuickBooks.InvoiceLineItem qo = new QuickBooks.InvoiceLineItem(qbf, qbcon, vwItems.Table, this.LOCAL_ID, this.ID);
			this.SendLineItems(Context, Session, con, gUSER_ID, qo, sbErrors);
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = base.BuildUpdateProcedure(Application, Session, spUpdate, row, gUSER_ID, gTEAM_ID, gASSIGNED_USER_ID, trn);
			
			string sINVOICE_STAGE = Sql.ToString(this.IsPending ? "Due" : "Paid");
			Guid gSHIPPER_ID = Guid.Empty;
			vwShippers.RowFilter = "SYNC_REMOTE_KEY = '" + this.ShipMethodId + "'";
			if ( vwShippers.Count > 0 )
				gSHIPPER_ID = Sql.ToGuid(vwShippers[0]["SYNC_LOCAL_ID"]);
			StringBuilder sbShippingAddress = new StringBuilder();
			if ( !Sql.IsEmptyString(this.ShippingLine1) ) sbShippingAddress.AppendLine(this.ShippingLine1);
			if ( !Sql.IsEmptyString(this.ShippingLine2) ) sbShippingAddress.AppendLine(this.ShippingLine2);
			if ( !Sql.IsEmptyString(this.ShippingLine3) ) sbShippingAddress.AppendLine(this.ShippingLine3);
			if ( !Sql.IsEmptyString(this.ShippingLine4) ) sbShippingAddress.AppendLine(this.ShippingLine4);
			if ( !Sql.IsEmptyString(this.ShippingLine5) ) sbShippingAddress.AppendLine(this.ShippingLine5);
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "INVOICE_NUM"                :  oValue = Sql.ToDBString  (this.ReferenceNumber        );  break;
							case "DUE_DATE"                   :  oValue = Sql.ToDBDateTime(this.DueDate                );  break;
							case "SHIPPER_ID"                 :  oValue = Sql.ToDBGuid    (     gSHIPPER_ID            );  break;
							case "INVOICE_STAGE"              :  oValue = Sql.ToDBString  (     sINVOICE_STAGE         );  break;
							case "TOTAL"                      :  oValue = Sql.ToDBDouble  (this.Amount                 );  break;
							case "TOTAL_USDOLLAR"             :  oValue = Sql.ToDBDouble  (this.Amount                 );  break;
							case "AMOUNT_DUE"                 :  oValue = Sql.ToDBDouble  (this.Balance                );  break;
							case "AMOUNT_DUE_USDOLLAR"        :  oValue = Sql.ToDBDouble  (this.Balance                );  break;
							case "SHIPPING_ADDRESS_STREET"    :  oValue = Sql.ToDBString  (sbShippingAddress.ToString());  break;
							case "SHIPPING_ADDRESS_CITY"      :  oValue = Sql.ToDBString  (this.ShippingCity           );  break;
							case "SHIPPING_ADDRESS_STATE"     :  oValue = Sql.ToDBString  (this.ShippingState          );  break;
							case "SHIPPING_ADDRESS_POSTALCODE":  oValue = Sql.ToDBString  (this.ShippingPostalCode     );  break;
							case "SHIPPING_ADDRESS_COUNTRY"   :  oValue = Sql.ToDBString  (this.ShippingCountry        );  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "INVOICE_NUM"                :  bChanged = ParameterChanged(par, oValue,   11);  break;
									case "INVOICE_STAGE"              :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "SHIPPING_ADDRESS_STREET"    :  bChanged = ParameterChanged(par, oValue,  215);  break;
									case "SHIPPING_ADDRESS_CITY"      :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "SHIPPING_ADDRESS_STATE"     :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "SHIPPING_ADDRESS_POSTALCODE":  bChanged = ParameterChanged(par, oValue,   13);  break;
									case "SHIPPING_ADDRESS_COUNTRY"   :  bChanged = ParameterChanged(par, oValue,   31);  break;
									// 02/10/2014 Paul.  Need to include the default change test. 
									default                           :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
