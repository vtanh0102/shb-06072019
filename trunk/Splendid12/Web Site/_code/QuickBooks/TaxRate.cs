/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class TaxRate : QObject
	{
		#region Properties
		public bool   IsActive      { get; set; }
		public double Value         { get; set; }
		public string Description   { get; set; }
		public string TaxVendorName { get; set; }
		#endregion

		public TaxRate(QuickBooksClientFactory qbf, IDbConnection qbcon) : base(qbf, qbcon, "Items", "Name", "TaxRates", "TAX_RATES", "NAME", false)
		{
		}

		public override void Reset()
		{
			base.Reset();
			this.IsActive      = false;
			this.Value         = 0.0;
			this.Description   = String.Empty;
			this.TaxVendorName = String.Empty;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			this.ID       = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.Name          = Sql.ToString(row["NAME"                 ]);
				this.IsActive      = Sql.ToString(row["STATUS"]) == "Active";
				this.Value         = Sql.ToDouble(row["VALUE"                ]);
				this.Description   = Sql.ToString(row["DESCRIPTION"          ]);
				this.TaxVendorName = Sql.ToString(row["QUICKBOOKS_TAX_VENDOR"]);
				bChanged = true;
			}
			else
			{
				if ( Compare(this.Name         ,               row["NAME"                 ], "NAME"                 , sbChanges) ) { this.Name          = Sql.ToString(row["NAME"                 ]);  bChanged = true; }
				if ( Compare(this.IsActive     , (Sql.ToString(row["STATUS"]) == "Active") , "STATUS"               , sbChanges) ) { this.IsActive      = Sql.ToString(row["STATUS"]) == "Active"   ;  bChanged = true; }
				if ( Compare(this.Value        ,               row["VALUE"                ], "VALUE"                , sbChanges) ) { this.Value         = Sql.ToDouble(row["VALUE"                ]);  bChanged = true; }
				if ( Compare(this.Description  ,               row["DESCRIPTION"          ], "DESCRIPTION"          , sbChanges) ) { this.Description   = Sql.ToString(row["DESCRIPTION"          ]);  bChanged = true; }
				if ( Compare(this.TaxVendorName,               row["QUICKBOOKS_TAX_VENDOR"], "QUICKBOOKS_TAX_VENDOR", sbChanges) ) { this.TaxVendorName = Sql.ToString(row["QUICKBOOKS_TAX_VENDOR"]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.IsActive      = Sql.ToBoolean(row["IsActive"     ]);
			this.Value         = Sql.ToDouble (row["TaxRate"      ]);
			this.Description   = Sql.ToString (row["Description"  ]);
			this.TaxVendorName = Sql.ToString (row["TaxVendorName"]);
		}

		public override string Insert()
		{
			string sTaxVendorId = String.Empty;
			// 06/03/2012 Paul.  The vendor is required.  If it does not exist, then create it. 
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "select ID from Vendors where Name = @Name";
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"    , this.TaxVendorName,   31);
				sTaxVendorId = Sql.ToString(qbcmd.ExecuteScalar());
				if ( Sql.IsEmptyString(sTaxVendorId) )
				{
					qbcmd.Parameters.Clear();
					sSQL = "insert into Vendors " + ControlChars.CrLf
					     + "     ( Name         " + ControlChars.CrLf
					     + "     , IsActive     " + ControlChars.CrLf
					     + "     )" + ControlChars.CrLf
					     + "values" + ControlChars.CrLf
					     + "     ( @Name         " + ControlChars.CrLf
					     + "     , @IsActive     " + ControlChars.CrLf
					     + "     ) " + ControlChars.CrLf;
					qbcmd.CommandText = sSQL;
					Sql.AddParameter(qbcmd, "@Name"    , this.TaxVendorName,   31);
					Sql.AddParameter(qbcmd, "@IsActive", true);
					qbcmd.ExecuteNonQuery();
					Hashtable result = qbf.GetLastResult(qbcon);
					sTaxVendorId = Sql.ToString(result["id"]);
				}
			}

			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				// 06/02/2012 Paul.  All sales taxes created from CRM should be of type Sales Tax Item. 
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( Name         " + ControlChars.CrLf
				     + "     , IsActive     " + ControlChars.CrLf
				     + "     , TaxRate      " + ControlChars.CrLf
				     + "     , Type         " + ControlChars.CrLf
				     + "     , Description  " + ControlChars.CrLf
				     + "     , TaxVendorName" + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @Name         " + ControlChars.CrLf
				     + "     , @IsActive     " + ControlChars.CrLf
				     + "     , @TaxRate      " + ControlChars.CrLf
				     + "     , 'SalesTax'    " + ControlChars.CrLf
				     + "     , @Description  " + ControlChars.CrLf
				     + "     , @TaxVendorName" + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"         , this.Name         ,   21);
				Sql.AddParameter(qbcmd, "@IsActive"     , this.IsActive     );
				Sql.AddParameter(qbcmd, "@TaxRate"      , this.Value        );
				Sql.AddParameter(qbcmd, "@Description"  , this.Description  , 4095);
				Sql.AddParameter(qbcmd, "@TaxVendorName", this.TaxVendorName,   31);
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public override void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set Name          = @Name         " + ControlChars.CrLf
				     + "     , IsActive      = @IsActive     " + ControlChars.CrLf
				     + "     , TaxRate       = @TaxRate      " + ControlChars.CrLf
				     + "     , Description   = @Description  " + ControlChars.CrLf
				     + "     , TaxVendorName = @TaxVendorName" + ControlChars.CrLf
				     + " where ID       = @ID      " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"         , this.Name         ,   21);
				Sql.AddParameter(qbcmd, "@IsActive"     , this.IsActive     );
				Sql.AddParameter(qbcmd, "@TaxRate"      , this.Value        );
				Sql.AddParameter(qbcmd, "@Description"  , this.Description  , 4095);
				Sql.AddParameter(qbcmd, "@TaxVendorName", this.TaxVendorName,   31);
				Sql.AddParameter(qbcmd, "@ID"           , this.ID           );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterQuickBooks(DataView vw)
		{
			vw.RowFilter = "Type like 'SalesTax%'";
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "NAME"                 :  oValue = Sql.ToDBString(this.Name                            );  break;
							case "STATUS"               :  oValue = Sql.ToDBString(this.IsActive ? "Active" : "Inactive");  break;
							case "VALUE"                :  oValue = Sql.ToDBDouble(this.Value                           );  break;
							case "QUICKBOOKS_TAX_VENDOR":  oValue = Sql.ToDBString(this.TaxVendorName                   );  break;
							case "DESCRIPTION"          :  oValue = Sql.ToDBString(this.Description                     );  break;
							case "MODIFIED_USER_ID"     :  oValue = gUSER_ID;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "NAME"                 :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "DESCRIPTION"          :  bChanged = ParameterChanged(par, oValue, 4095);  break;
									case "QUICKBOOKS_TAX_VENDOR":  bChanged = ParameterChanged(par, oValue,   31);  break;
									default                     :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
