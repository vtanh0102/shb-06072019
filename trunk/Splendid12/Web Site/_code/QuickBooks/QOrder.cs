/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Xml;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class QOrder : QObject
	{
		#region Properties
		public string   ReferenceNumber            { get; set; }
		//public string   CustomerName               { get; set; }
		public string   CustomerId                 { get; set; }
		public DateTime Date                       { get; set; }
		public DateTime DueDate                    { get; set; }
		public string   Memo                       { get; set; }
		public string   POnumber                   { get; set; }
		public string   Terms                      { get; set; }
		//public double   ExchangeRate               { get; set; }
		public double   Subtotal                   { get; set; }
		public double   Tax                        { get; set; }
		public string   TaxAgencyId                { get; set; }
		public string   BillingLine1               { get; set; }
		public string   BillingLine2               { get; set; }
		public string   BillingLine3               { get; set; }
		public string   BillingLine4               { get; set; }
		public string   BillingLine5               { get; set; }
		public string   BillingCity                { get; set; }
		public string   BillingState               { get; set; }
		public string   BillingPostalCode          { get; set; }
		public string   BillingCountry             { get; set; }
		#endregion
		protected DataView vwShippers ;
		protected DataView vwTaxRates ;
		protected DataView vwItems    ;
		protected DataView vwCustomers;
		protected DbProviderFactory dbf;
		protected IDbConnection     con;

		// 05/22/2012 Paul.  We need to return the of the first line item when inserting an Estimate, SalesOrder or Invoice. 
		public string NewLineItemID    { get; set; }
		public Guid   NEW_LINE_ITEM_ID { get; set; }

		public QOrder(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtShippers, DataTable dtTaxRates, DataTable dtItems, DataTable dtCustomers, DbProviderFactory dbf, IDbConnection con, string sQuickBooksTableName, string sCRMModuleName, string sCRMTableName, string sCRMTableSort, bool bCRMAssignedUser) : base(qbf, qbcon, sQuickBooksTableName, "ReferenceNumber", sCRMModuleName, sCRMTableName, sCRMTableSort, bCRMAssignedUser)
		{
			this.vwShippers  = new DataView(dtShippers );
			this.vwTaxRates  = new DataView(dtTaxRates );
			this.vwItems     = new DataView(dtItems    );
			this.vwCustomers = new DataView(dtCustomers);
			this.dbf = dbf;
			this.con = con;
		}

		public override void Reset()
		{
			base.Reset();
			this.ReferenceNumber    = String.Empty;
			//this.CustomerName       = String.Empty;
			this.CustomerId         = String.Empty;
			this.Date               = DateTime.MinValue;
			this.DueDate            = DateTime.MinValue;
			this.Memo               = String.Empty;
			this.POnumber           = String.Empty;
			this.Terms              = String.Empty;
			//this.ExchangeRate       = 0.0;
			this.Subtotal           = 0.0;
			this.Tax                = 0.0;
			this.TaxAgencyId        = String.Empty;
			this.BillingLine1       = String.Empty;
			this.BillingLine2       = String.Empty;
			this.BillingLine3       = String.Empty;
			this.BillingLine4       = String.Empty;
			this.BillingLine5       = String.Empty;
			this.BillingCity        = String.Empty;
			this.BillingState       = String.Empty;
			this.BillingPostalCode  = String.Empty;
			this.BillingCountry     = String.Empty;
			this.NewLineItemID      = String.Empty;
			this.NEW_LINE_ITEM_ID   = Guid.Empty;
		}

		private string ToQuickBooksTerms(string sPAYMENT_TERMS)
		{
			string sTerms = sPAYMENT_TERMS;
			switch ( sTerms )
			{
				case "Due on Receipt":  sTerms = "Due on receipt";  break;
				case "Net 7 Days"    :  sTerms = "Net 7"         ;  break;
				case "Net 15 Days"   :  sTerms = "Net 15"        ;  break;
				case "Net 30 Days"   :  sTerms = "Net 30"        ;  break;
				case "Net 45 Days"   :  sTerms = "Net 45"        ;  break;
				case "Net 60 Days"   :  sTerms = "Net 60"        ;  break;
			}
			return sTerms;
		}

		private string ToCRMPaymentTerms(string sTerms)
		{
			string sPAYMENT_TERMS = sTerms;
			switch ( sPAYMENT_TERMS )
			{
				case "Due on receipt":  sPAYMENT_TERMS = "Due on Receipt";  break;
				case "Net 7"         :  sPAYMENT_TERMS = "Net 7 Days"    ;  break;
				case "Net 15"        :  sPAYMENT_TERMS = "Net 15 Days"   ;  break;
				case "Net 30"        :  sPAYMENT_TERMS = "Net 30 Days"   ;  break;
				case "Net 45"        :  sPAYMENT_TERMS = "Net 45 Days"   ;  break;
				case "Net 60"        :  sPAYMENT_TERMS = "Net 60 Days"   ;  break;
			}
			return sPAYMENT_TERMS;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			string sTerms           = ToQuickBooksTerms(Sql.ToString(row["PAYMENT_TERMS"]));
			string sBillingAddress  = Sql.ToString(row["BILLING_ADDRESS_STREET" ]).Replace(ControlChars.CrLf, "\n");
			List<string> arrBillingAddress  = new List<string>(sBillingAddress .Split('\n'));
			while ( arrBillingAddress.Count < 5 )
				arrBillingAddress.Add(String.Empty);
			
			string sCustomerId = String.Empty;
			vwCustomers.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["BILLING_ACCOUNT_ID"]) + "'";
			if ( vwCustomers.Count > 0 )
				sCustomerId = Sql.ToString(vwCustomers[0]["SYNC_REMOTE_KEY"]);
			
			string sTaxAgencyId = String.Empty;
			vwTaxRates.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["TAXRATE_ID"]) + "'";
			if ( vwTaxRates.Count > 0 )
				sTaxAgencyId = Sql.ToString(vwTaxRates[0]["SYNC_REMOTE_KEY"]);
			
			this.ID       = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				//this.CustomerName       = Sql.ToString  (row["BILLING_ACCOUNT_NAME"       ]);
				this.CustomerId         = Sql.ToString  (    sCustomerId                   );
				this.Memo               = Sql.ToString  (row["DESCRIPTION"                ]);
				this.POnumber           = Sql.ToString  (row["PURCHASE_ORDER_NUM"         ]);
				this.Terms              = Sql.ToString  (    sTerms                        );
				//this.ExchangeRate       = Sql.ToDouble  (row["EXCHANGE_RATE"              ]);
				this.Subtotal           = Sql.ToDouble  (row["SUBTOTAL_USDOLLAR"          ]);
				this.Tax                = Sql.ToDouble  (row["TAX_USDOLLAR"               ]);
				this.TaxAgencyId        = Sql.ToString  (    sTaxAgencyId                  );
				this.BillingLine1       = Sql.ToString  (    arrBillingAddress[0]          );
				this.BillingLine2       = Sql.ToString  (    arrBillingAddress[1]          );
				this.BillingLine3       = Sql.ToString  (    arrBillingAddress[2]          );
				this.BillingLine4       = Sql.ToString  (    arrBillingAddress[3]          );
				this.BillingLine5       = Sql.ToString  (    arrBillingAddress[4]          );
				this.BillingCity        = Sql.ToString  (row["BILLING_ADDRESS_CITY"       ]);
				this.BillingState       = Sql.ToString  (row["BILLING_ADDRESS_STATE"      ]);
				this.BillingPostalCode  = Sql.ToString  (row["BILLING_ADDRESS_POSTALCODE" ]);
				this.BillingCountry     = Sql.ToString  (row["BILLING_ADDRESS_COUNTRY"    ]);
				bChanged = true;
			}
			else
			{
				//if ( Compare(this.CustomerName     , row["BILLING_ACCOUNT_NAME"       ], "BILLING_ACCOUNT_NAME"      , sbChanges) ) { this.CustomerName       = Sql.ToString  (row["BILLING_ACCOUNT_NAME"       ]);  bChanged = true; }
				if ( Compare(this.CustomerId       ,     sCustomerId                   , "BILLING_ACCOUNT_ID"        , sbChanges) ) { this.CustomerId         = Sql.ToString  (    sCustomerId                   );  bChanged = true; }
				if ( Compare(this.Memo             , row["DESCRIPTION"                ], "DESCRIPTION"               , sbChanges) ) { this.Memo               = Sql.ToString  (row["DESCRIPTION"                ]);  bChanged = true; }
				if ( Compare(this.POnumber         , row["PURCHASE_ORDER_NUM"         ], "PURCHASE_ORDER_NUM"        , sbChanges) ) { this.POnumber           = Sql.ToString  (row["PURCHASE_ORDER_NUM"         ]);  bChanged = true; }
				if ( Compare(this.Terms            ,     sTerms                        , "PAYMENT_TERMS"             , sbChanges) ) { this.Terms              = Sql.ToString  (    sTerms                        );  bChanged = true; }
				//if ( Compare(this.ExchangeRate     , row["EXCHANGE_RATE"              ], "EXCHANGE_RATE"             , sbChanges) ) { this.ExchangeRate       = Sql.ToDouble  (row["EXCHANGE_RATE"              ]);  bChanged = true; }
				if ( Compare(this.Subtotal         , row["SUBTOTAL_USDOLLAR"          ], "SUBTOTAL_USDOLLAR"         , sbChanges) ) { this.Subtotal           = Sql.ToDouble  (row["SUBTOTAL_USDOLLAR"          ]);  bChanged = true; }
				if ( Compare(this.Tax              , row["TAX_USDOLLAR"               ], "TAX_USDOLLAR"              , sbChanges) ) { this.Tax                = Sql.ToDouble  (row["TAX_USDOLLAR"               ]);  bChanged = true; }
				if ( Compare(this.TaxAgencyId      ,     sTaxAgencyId                  , "TAXRATE_ID"                , sbChanges) ) { this.TaxAgencyId        = Sql.ToString  (    sTaxAgencyId                  );  bChanged = true; }
				if ( Compare(this.BillingLine1     ,     arrBillingAddress[0]          , "BillingLine1"              , sbChanges) ) { this.BillingLine1       = Sql.ToString  (    arrBillingAddress[0]          );  bChanged = true; }
				if ( Compare(this.BillingLine2     ,     arrBillingAddress[1]          , "BillingLine2"              , sbChanges) ) { this.BillingLine2       = Sql.ToString  (    arrBillingAddress[1]          );  bChanged = true; }
				if ( Compare(this.BillingLine3     ,     arrBillingAddress[2]          , "BillingLine3"              , sbChanges) ) { this.BillingLine3       = Sql.ToString  (    arrBillingAddress[2]          );  bChanged = true; }
				if ( Compare(this.BillingLine4     ,     arrBillingAddress[3]          , "BillingLine4"              , sbChanges) ) { this.BillingLine4       = Sql.ToString  (    arrBillingAddress[3]          );  bChanged = true; }
				if ( Compare(this.BillingLine5     ,     arrBillingAddress[4]          , "BillingLine5"              , sbChanges) ) { this.BillingLine5       = Sql.ToString  (    arrBillingAddress[4]          );  bChanged = true; }
				if ( Compare(this.BillingCity      , row["BILLING_ADDRESS_CITY"       ], "BILLING_ADDRESS_CITY"      , sbChanges) ) { this.BillingCity        = Sql.ToString  (row["BILLING_ADDRESS_CITY"       ]);  bChanged = true; }
				if ( Compare(this.BillingState     , row["BILLING_ADDRESS_STATE"      ], "BILLING_ADDRESS_STATE"     , sbChanges) ) { this.BillingState       = Sql.ToString  (row["BILLING_ADDRESS_STATE"      ]);  bChanged = true; }
				if ( Compare(this.BillingPostalCode, row["BILLING_ADDRESS_POSTALCODE" ], "BILLING_ADDRESS_POSTALCODE", sbChanges) ) { this.BillingPostalCode  = Sql.ToString  (row["BILLING_ADDRESS_POSTALCODE" ]);  bChanged = true; }
				if ( Compare(this.BillingCountry   , row["BILLING_ADDRESS_COUNTRY"    ], "BILLING_ADDRESS_COUNTRY"   , sbChanges) ) { this.BillingCountry     = Sql.ToString  (row["BILLING_ADDRESS_COUNTRY"    ]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			this.Reset();
			this.ID                 = Sql.ToString  (row["ID"                ]);
			this.TimeCreated        = Sql.ToDateTime(row["TimeCreated"       ]);
			this.TimeModified       = Sql.ToDateTime(row["TimeModified"      ]);
			// 05/31/2012 Paul.  The Name is only used for logging. 
			this.Name               = Sql.ToString  (row["ReferenceNumber"   ]);
			this.ReferenceNumber    = Sql.ToString  (row["ReferenceNumber"   ]);
			//this.CustomerName       = Sql.ToString  (row["CustomerName"      ]);
			this.CustomerId         = Sql.ToString  (row["CustomerId"        ]);
			this.Memo               = Sql.ToString  (row["Memo"              ]);
			this.POnumber           = Sql.ToString  (row["POnumber"          ]);
			this.Terms              = Sql.ToString  (row["Terms"             ]);
			//this.ExchangeRate       = Sql.ToDouble  (row["ExchangeRate"      ]);
			this.Subtotal           = Sql.ToDouble  (row["Subtotal"          ]);
			this.Tax                = Sql.ToDouble  (row["Tax"               ]);
			this.TaxAgencyId        = Sql.ToString  (row["TaxAgencyId"       ]);
			this.BillingLine1       = Sql.ToString  (row["BillingLine1"      ]);
			this.BillingLine2       = Sql.ToString  (row["BillingLine2"      ]);
			this.BillingLine3       = Sql.ToString  (row["BillingLine3"      ]);
			this.BillingLine4       = Sql.ToString  (row["BillingLine4"      ]);
			this.BillingLine5       = Sql.ToString  (row["BillingLine5"      ]);
			this.BillingCity        = Sql.ToString  (row["BillingCity"       ]);
			this.BillingState       = Sql.ToString  (row["BillingState"      ]);
			this.BillingPostalCode  = Sql.ToString  (row["BillingPostalCode" ]);
			this.BillingCountry     = Sql.ToString  (row["BillingCountry"    ]);
		}

		protected string GetItemAggregate(DbProviderFactory dbf, IDbConnection con, Guid gID, DataView vwItems)
		{
			XmlDocument xml = new XmlDocument();
			xml.AppendChild(xml.CreateElement( this.QuickBooksTableName.Substring(0, this.QuickBooksTableName.Length-1) + "LineItems"));
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				string sSQL;
				sSQL = "select *    " + ControlChars.CrLf
				     + "  from " + Sql.MetadataName(con, "vw" + this.CRMTableName + "_LINE_ITEMS_QuickBooks") + ControlChars.CrLf
				     + " where 1 = 1" + ControlChars.CrLf;
				cmd.CommandText = sSQL;
				Sql.AppendParameter(cmd, gID, this.CRMTableName.Substring(0, this.CRMTableName.Length-1) + "_ID", false);
				cmd.CommandText += " order by POSITION, ID" + ControlChars.CrLf;
				using ( DbDataAdapter da = dbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = cmd;
					using ( DataTable dt = new DataTable() )
					{
						da.Fill(dt);
						// 05/22/2012 Paul.  When inserting an Estimate, SalesOrder or Invoice, we will start with the first line item 
						// and the the sync process add the additional line items. 
						//foreach ( DataRow row in dt.Rows )
						if ( dt.Rows.Count > 0 )
						{
							DataRow row = dt.Rows[0];
							XmlNode xRow = xml.CreateElement("Row");
							xml.DocumentElement.AppendChild(xRow);
							
							XmlNode xItemId          = xml.CreateElement("ItemId"         );
							XmlNode xItemName        = xml.CreateElement("ItemName"       );
							XmlNode xItemQuantity    = xml.CreateElement("ItemQuantity"   );
							XmlNode xItemDescription = xml.CreateElement("ItemDescription");
							XmlNode xItemTaxCode     = xml.CreateElement("ItemTaxCode"    );
							XmlNode xItemAmount      = xml.CreateElement("ItemAmount"     );
							
							vwItems.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["PRODUCT_TEMPLATE_ID"]) + "'";
							if ( vwItems.Count > 0 )
							{
								xRow.AppendChild(xItemId);
								xItemId.InnerText = Sql.ToString(vwItems[0]["SYNC_REMOTE_KEY"]);
							}
							xRow.AppendChild(xItemName       );
							xRow.AppendChild(xItemQuantity   );
							xRow.AppendChild(xItemDescription);
							xRow.AppendChild(xItemTaxCode    );
							xRow.AppendChild(xItemAmount     );
							xItemName       .InnerText = Sql.ToString (row["MFT_PART_NUM" ]);
							xItemQuantity   .InnerText = Sql.ToString (row["QUANTITY"     ]);
							xItemDescription.InnerText = Sql.ToString (row["NAME"         ]);
							xItemTaxCode    .InnerText = Sql.ToString (row["TAX_CLASS"    ]) == "Taxable" ? "Tax" : "Non";
							xItemAmount     .InnerText = Sql.ToDecimal(row["UNIT_USDOLLAR"]).ToString("0.00");
							this.NEW_LINE_ITEM_ID = Sql.ToGuid(row["ID"]);
						}
					}
				}
			}
			return xml.OuterXml;
		}

		public override string Insert()
		{
			throw(new Exception("Insert must be overwritten"));
		}

		public override void Update()
		{
			throw(new Exception("Update must be overwritten"));
		}

		public void GetFirstLineItem()
		{
			this.NewLineItemID = String.Empty;
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSingularTableName = this.QuickBooksTableName.Substring(0, this.QuickBooksTableName.Length - 1);
				string sSQL = "select ID from " + sSingularTableName + "LineItems" + " where " + sSingularTableName + "Id = @ID";
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@ID", this.ID);
				using ( DbDataAdapter qda = qbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)qda).SelectCommand = qbcmd;
					using ( DataTable dt = new DataTable() )
					{
						qda.Fill(dt);
						if ( dt.Rows.Count > 0 )
						{
							this.NewLineItemID = Sql.ToString(dt.Rows[0]["ID"]);
						}
					}
				}
			}
		}

		public void ImportLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, QuickBooks.LineItem qo, StringBuilder sbErrors)
		{
			bool   bVERBOSE_STATUS = Sql.ToBoolean(Context.Application["CONFIG.QuickBooks.VerboseStatus"]);
			string sDIRECTION      = "to crm only";
			
			List<string> lstLineItems = new List<string>();
			DateTime dtStartSelect = DateTime.Now;
			using ( DataTable dt = qo.Select(this.QuickBooksTableName.Substring(0, this.QuickBooksTableName.Length-1) + "Id", this.ID) )
			{
				DateTime dtEndSelect = DateTime.Now;
				if ( bVERBOSE_STATUS )
					SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Query took " + (dtEndSelect - dtStartSelect).Seconds.ToString() + " seconds. ");
				if ( dt.Rows.Count > 0 )
					SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: " + dt.Rows.Count.ToString() + " " + qo.QuickBooksTableName + " to import.");
				foreach ( DataRow qbrow in dt.Rows )
				{
					qo.SetFromQuickBooks(qbrow);
					lstLineItems.Add(qo.ID);
					qo.ImportLineItem(Context, Session, con, gUSER_ID, sDIRECTION, sbErrors);
				}
			}

			// 06/02/2012 Paul.  We need to check for deleted line items. 
			string sSQL;
			sSQL = "select vw" + qo.CRMTableName + ".*                                                                   " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_ID                                                        " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_KEY                                                " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_DATE_MODIFIED_UTC                                   " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_DATE_MODIFIED_UTC                                  " + ControlChars.CrLf
			     + "  from            " + Sql.MetadataName(con, "vw" + qo.CRMTableName + "_QuickBooks") + " vw" + qo.CRMTableName + ControlChars.CrLf
			     + "  left outer join vw" + qo.CRMTableName + "_SYNC                                                     " + ControlChars.CrLf
			     + "               on vw" + qo.CRMTableName + "_SYNC.SYNC_SERVICE_NAME     = N'QuickBooks'               " + ControlChars.CrLf
			     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID      " + ControlChars.CrLf
			     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_ID         = vw" + qo.CRMTableName + ".ID" + ControlChars.CrLf;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.CommandText = sSQL;
				Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID);
				ExchangeSecurity.Filter(Context.Application, Session, cmd, gUSER_ID, qo.CRMModuleName, "view");
				Sql.AppendParameter(cmd, this.LOCAL_ID, this.CRMTableName.Substring(0, this.CRMTableName.Length-1) + "_ID");
				cmd.CommandText += " order by vw" + qo.CRMTableName + ".POSITION" + ControlChars.CrLf;
				using ( DbDataAdapter da = dbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = cmd;
					using ( DataTable dt = new DataTable() )
					{
						da.Fill(dt);
						for ( int i = 0; i < dt.Rows.Count; i++ )
						{
							DataRow row = dt.Rows[i];
							Guid   gID              = Sql.ToGuid  (row["ID"             ]);
							string sSYNC_REMOTE_KEY = Sql.ToString(row["SYNC_REMOTE_KEY"]);
							StringBuilder sbChanges = new StringBuilder();
							try
							{
								if ( !lstLineItems.Contains(sSYNC_REMOTE_KEY) )
								{
									// 08/05/2014 Paul.  Deletes should follow the same direction rules. 
									if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Deleting " + qo.CRMTableName + " " + Sql.ToString(row["NAME"]) + ".");
										using ( IDbTransaction trn = Sql.BeginTransaction(con) )
										{
											try
											{
												IDbCommand cmdSyncDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_SYNC_Delete");
												cmdSyncDelete.Transaction = trn;
												Sql.SetParameter(cmdSyncDelete, "@MODIFIED_USER_ID", gUSER_ID        );
												Sql.SetParameter(cmdSyncDelete, "@ASSIGNED_USER_ID", gUSER_ID        );
												Sql.SetParameter(cmdSyncDelete, "@LOCAL_ID"        , gID             );
												Sql.SetParameter(cmdSyncDelete, "@REMOTE_KEY"      , sSYNC_REMOTE_KEY);
												Sql.SetParameter(cmdSyncDelete, "@SERVICE_NAME"    , "QuickBooks"    );
												cmdSyncDelete.ExecuteNonQuery();
											
												IDbCommand cmdDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_Delete");
												cmdDelete.Transaction = trn;
												Sql.SetParameter(cmdDelete, "@ID"              , gID           );
												Sql.SetParameter(cmdDelete, "@MODIFIED_USER_ID", gUSER_ID      );
												cmdDelete.ExecuteNonQuery();
												trn.Commit();
											}
											catch
											{
												trn.Rollback();
												throw;
											}
										}
									}
								}
							}
							catch(Exception ex)
							{
								string sError = "Error deleting CRM " + qo.CRMTableName + " " + Sql.ToString(row["ID"]) + "." + ControlChars.CrLf;
								sError += sbChanges.ToString();
								sError += Utils.ExpandException(ex) + ControlChars.CrLf;
								SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
								sbErrors.AppendLine(sError);
							}
						}
					}
				}
			}
		}

		public void SendLineItems(HttpContext Context, ExchangeSession Session, IDbConnection con, Guid gUSER_ID, QuickBooks.LineItem qo, StringBuilder sbErrors)
		{
			bool   bVERBOSE_STATUS = Sql.ToBoolean(Context.Application["CONFIG.QuickBooks.VerboseStatus"]);
			string sDIRECTION      = "from crm only";

			List<string> lstLineItems = new List<string>();
			DateTime dtStartSelect = DateTime.Now;
			using ( DataTable dt = qo.Select(this.QuickBooksTableName.Substring(0, this.QuickBooksTableName.Length-1) + "Id", this.ID) )
			{
				DateTime dtEndSelect = DateTime.Now;
				if ( bVERBOSE_STATUS )
					SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Query took " + (dtEndSelect - dtStartSelect).Seconds.ToString() + " seconds. ");
				foreach ( DataRow qbrow in dt.Rows )
				{
					lstLineItems.Add(Sql.ToString(qbrow["ID"]));
				}
			}

			string sSQL;
			sSQL = "select vw" + qo.CRMTableName + ".*                                                                   " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_ID                                                        " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_KEY                                                " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_DATE_MODIFIED_UTC                                   " + ControlChars.CrLf
			     + "     , vw" + qo.CRMTableName + "_SYNC.SYNC_REMOTE_DATE_MODIFIED_UTC                                  " + ControlChars.CrLf
			     + "  from            " + Sql.MetadataName(con, "vw" + qo.CRMTableName + "_QuickBooks") + " vw" + qo.CRMTableName + ControlChars.CrLf
			     + "  left outer join vw" + qo.CRMTableName + "_SYNC                                                     " + ControlChars.CrLf
			     + "               on vw" + qo.CRMTableName + "_SYNC.SYNC_SERVICE_NAME     = N'QuickBooks'               " + ControlChars.CrLf
			     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_ASSIGNED_USER_ID = @SYNC_ASSIGNED_USER_ID      " + ControlChars.CrLf
			     + "              and vw" + qo.CRMTableName + "_SYNC.SYNC_LOCAL_ID         = vw" + qo.CRMTableName + ".ID" + ControlChars.CrLf;
			using ( IDbCommand cmd = con.CreateCommand() )
			{
				cmd.CommandText = sSQL;
				Sql.AddParameter(cmd, "@SYNC_ASSIGNED_USER_ID", gUSER_ID);
				ExchangeSecurity.Filter(Context.Application, Session, cmd, gUSER_ID, qo.CRMModuleName, "view");
				Sql.AppendParameter(cmd, this.LOCAL_ID, this.CRMTableName.Substring(0, this.CRMTableName.Length-1) + "_ID");
				cmd.CommandText += " order by vw" + qo.CRMTableName + ".POSITION" + ControlChars.CrLf;
				using ( DbDataAdapter da = dbf.CreateDataAdapter() )
				{
					((IDbDataAdapter)da).SelectCommand = cmd;
					using ( DataTable dt = new DataTable() )
					{
						da.Fill(dt);
						if ( dt.Rows.Count > 0 )
							SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: " + dt.Rows.Count.ToString() + " " + qo.CRMModuleName + " to send.");
						for ( int i = 0; i < dt.Rows.Count; i++ )
						{
							DataRow row = dt.Rows[i];
							Guid     gID                             = Sql.ToGuid    (row["ID"                           ]);
							Guid     gSYNC_ID                        = Sql.ToGuid    (row["SYNC_ID"                      ]);
							string   sSYNC_REMOTE_KEY                = Sql.ToString  (row["SYNC_REMOTE_KEY"              ]);
							DateTime dtSYNC_LOCAL_DATE_MODIFIED_UTC  = Sql.ToDateTime(row["SYNC_LOCAL_DATE_MODIFIED_UTC" ]);
							DateTime dtSYNC_REMOTE_DATE_MODIFIED_UTC = Sql.ToDateTime(row["SYNC_REMOTE_DATE_MODIFIED_UTC"]);
							DateTime dtDATE_MODIFIED_UTC             = Sql.ToDateTime(row["DATE_MODIFIED_UTC"            ]);
							string   sSYNC_ACTION                    = Sql.IsEmptyGuid(gSYNC_ID) ? "local new" : "local changed";
							StringBuilder sbChanges = new StringBuilder();
							try
							{
								if ( sSYNC_ACTION == "local new" || Sql.IsEmptyString(sSYNC_REMOTE_KEY) )
								{
									// 05/18/2012 Paul.  Allow control of sync direction. 
									if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Sending new " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ".");
										qo.SetFromCRM(String.Empty, row, sbChanges);
										
										sSYNC_REMOTE_KEY = qo.Insert();
									}
								}
								else
								{
									if ( bVERBOSE_STATUS )
										SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Binding " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ".");
									
									// 06/03/2012 Paul.  Remove this item from the list to delete. 
									if ( lstLineItems.Contains(sSYNC_REMOTE_KEY) )
										lstLineItems.Remove(sSYNC_REMOTE_KEY);
									
									try
									{
										qo.Get(sSYNC_REMOTE_KEY);
										if ( qo.Deleted )
										{
											sSYNC_ACTION = "remote deleted";
										}
									}
									catch(Exception ex)
									{
										string sError = "Error retrieving QuickBooks " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString  (row["NAME"]) + "." + ControlChars.CrLf;
										sError += Utils.ExpandException(ex) + ControlChars.CrLf;
										SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
										sbErrors.AppendLine(sError);
										sSYNC_ACTION = "remote deleted";
									}
									if ( sSYNC_ACTION == "local changed" )
									{
										// 05/18/2012 Paul.  Allow control of sync direction. 
										if (sDIRECTION == "bi-directional" || sDIRECTION == "from crm only" )
										{
											bool bChanged = qo.SetFromCRM(sSYNC_REMOTE_KEY, row, sbChanges);
											if ( bChanged )
											{
												if ( bVERBOSE_STATUS )
													SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Sending " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString(row["NAME"]) + ". " + sbChanges.ToString());
												qo.Update();
											}
										}
									}
								}
								if ( sSYNC_ACTION == "local new" || sSYNC_ACTION == "local changed" )
								{
									if ( !Sql.IsEmptyString(qo.ID) )
									{
										DateTime dtREMOTE_DATE_MODIFIED     = qo.TimeModified;
										DateTime dtREMOTE_DATE_MODIFIED_UTC = dtREMOTE_DATE_MODIFIED.ToUniversalTime();
										using ( IDbTransaction trn = Sql.BeginTransaction(con) )
										{
											try
											{
												IDbCommand cmdSyncUpdate = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_SYNC_Update");
												cmdSyncUpdate.Transaction = trn;
												Sql.SetParameter(cmdSyncUpdate, "@MODIFIED_USER_ID"        , gUSER_ID                  );
												Sql.SetParameter(cmdSyncUpdate, "@ASSIGNED_USER_ID"        , gUSER_ID                  );
												Sql.SetParameter(cmdSyncUpdate, "@LOCAL_ID"                , gID                       );
												Sql.SetParameter(cmdSyncUpdate, "@REMOTE_KEY"              , sSYNC_REMOTE_KEY          );
												Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED"    , dtREMOTE_DATE_MODIFIED    );
												Sql.SetParameter(cmdSyncUpdate, "@REMOTE_DATE_MODIFIED_UTC", dtREMOTE_DATE_MODIFIED_UTC);
												Sql.SetParameter(cmdSyncUpdate, "@SERVICE_NAME"            , "QuickBooks"              );
												Sql.SetParameter(cmdSyncUpdate, "@RAW_CONTENT"             , String.Empty              );
												cmdSyncUpdate.ExecuteNonQuery();
												trn.Commit();
											}
											catch
											{
												trn.Rollback();
												throw;
											}
										}
									}
								}
								else if ( sSYNC_ACTION == "remote deleted" )
								{
									// 08/05/2014 Paul.  Deletes should follow the same direction rules. 
									if (sDIRECTION == "bi-directional" || sDIRECTION == "to crm only" )
									{
										if ( bVERBOSE_STATUS )
											SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Deleting " + qo.CRMTableName + " " + Sql.ToString(row["NAME"]) + ".");
										using ( IDbTransaction trn = Sql.BeginTransaction(con) )
										{
											try
											{
												IDbCommand cmdSyncDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_SYNC_Delete");
												cmdSyncDelete.Transaction = trn;
												Sql.SetParameter(cmdSyncDelete, "@MODIFIED_USER_ID", gUSER_ID        );
												Sql.SetParameter(cmdSyncDelete, "@ASSIGNED_USER_ID", gUSER_ID        );
												Sql.SetParameter(cmdSyncDelete, "@LOCAL_ID"        , gID             );
												Sql.SetParameter(cmdSyncDelete, "@REMOTE_KEY"      , sSYNC_REMOTE_KEY);
												Sql.SetParameter(cmdSyncDelete, "@SERVICE_NAME"    , "QuickBooks"    );
												cmdSyncDelete.ExecuteNonQuery();
											
												IDbCommand cmdDelete = SqlProcs.Factory(con, "sp" + qo.CRMTableName + "_Delete");
												cmdDelete.Transaction = trn;
												Sql.SetParameter(cmdDelete, "@ID"              , gID           );
												Sql.SetParameter(cmdDelete, "@MODIFIED_USER_ID", gUSER_ID      );
												cmdDelete.ExecuteNonQuery();
												trn.Commit();
											}
											catch
											{
												trn.Rollback();
												throw;
											}
										}
									}
								}
							}
							catch(Exception ex)
							{
								string sError = "Error creating QuickBooks " + qo.QuickBooksTableName + " (" + i.ToString() + ") " + Sql.ToString  (row["NAME"]) + "." + ControlChars.CrLf;
								sError += sbChanges.ToString();
								sError += Utils.ExpandException(ex) + ControlChars.CrLf;
								SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
								sbErrors.AppendLine(sError);
							}
						}
					}
				}
			}
			// 06/03/2012 Paul.  If there are any line items that were found in QuickBooks but not in the CRM, then delete the QuickBooks line. 
			foreach ( string sID in lstLineItems )
			{
				try
				{
					if ( bVERBOSE_STATUS )
						SyncError.SystemMessage(Context, "Warning", new StackTrace(true).GetFrame(0), qo.QuickBooksTableName + ".Sync: Deleting " + qo.CRMTableName + " " + sID);
					qo.ID = sID;
					qo.Delete();
				}
				catch(Exception ex)
				{
					string sError = "Error deleting QuickBooks " + qo.QuickBooksTableName + " " + sID + "." + ControlChars.CrLf;
					sError += Utils.ExpandException(ex) + ControlChars.CrLf;
					SyncError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), sError);
					sbErrors.AppendLine(sError);
				}
			}
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			
			string sPAYMENT_TERMS = ToCRMPaymentTerms(this.Terms);
			StringBuilder sbBillingAddress  = new StringBuilder();
			if ( !Sql.IsEmptyString(this.BillingLine1 ) ) sbBillingAddress .AppendLine(this.BillingLine1);
			if ( !Sql.IsEmptyString(this.BillingLine2 ) ) sbBillingAddress .AppendLine(this.BillingLine2);
			if ( !Sql.IsEmptyString(this.BillingLine3 ) ) sbBillingAddress .AppendLine(this.BillingLine3);
			if ( !Sql.IsEmptyString(this.BillingLine4 ) ) sbBillingAddress .AppendLine(this.BillingLine4);
			if ( !Sql.IsEmptyString(this.BillingLine5 ) ) sbBillingAddress .AppendLine(this.BillingLine5);
			
			Guid gBILLING_ACCOUNT_ID = Guid.Empty;
			vwCustomers.RowFilter = "SYNC_REMOTE_KEY = '" + this.CustomerId + "'";
			if ( vwCustomers.Count > 0 )
				gBILLING_ACCOUNT_ID = Sql.ToGuid(vwCustomers[0]["SYNC_LOCAL_ID"]);
			
			Guid gTAXRATE_ID = Guid.Empty;
			vwTaxRates.RowFilter = "SYNC_REMOTE_KEY = '" + this.TaxAgencyId + "'";
			if ( vwTaxRates.Count > 0 )
				gTAXRATE_ID = Sql.ToGuid(vwTaxRates[0]["SYNC_LOCAL_ID"]);
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "NAME"                       :  oValue = Sql.ToDBString  (this.ReferenceNumber        );  break;
							case "BILLING_ACCOUNT_ID"         :  oValue = Sql.ToDBGuid    (     gBILLING_ACCOUNT_ID    );  break;
							case "DESCRIPTION"                :  oValue = Sql.ToDBString  (this.Memo                   );  break;
							case "PURCHASE_ORDER_NUM"         :  oValue = Sql.ToDBString  (this.POnumber               );  break;
							case "PAYMENT_TERMS"              :  oValue = Sql.ToDBString  (     sPAYMENT_TERMS         );  break;
							//case "EXCHANGE_RATE"              :  oValue = Sql.ToDBDouble  (this.ExchangeRate           );  break;
							case "SUBTOTAL"                   :  oValue = Sql.ToDBDouble  (this.Subtotal               );  break;
							case "SUBTOTAL_USDOLLAR"          :  oValue = Sql.ToDBDouble  (this.Subtotal               );  break;
							case "TAX"                        :  oValue = Sql.ToDBDouble  (this.Tax                    );  break;
							case "TAX_USDOLLAR"               :  oValue = Sql.ToDBDouble  (this.Tax                    );  break;
							case "TAXRATE_ID"                 :  oValue = Sql.ToDBGuid    (     gTAXRATE_ID            );  break;
							case "BILLING_ADDRESS_STREET"     :  oValue = Sql.ToDBString  (sbBillingAddress.ToString() );  break;
							case "BILLING_ADDRESS_CITY"       :  oValue = Sql.ToDBString  (this.BillingCity            );  break;
							case "BILLING_ADDRESS_STATE"      :  oValue = Sql.ToDBString  (this.BillingState           );  break;
							case "BILLING_ADDRESS_POSTALCODE" :  oValue = Sql.ToDBString  (this.BillingPostalCode      );  break;
							case "BILLING_ADDRESS_COUNTRY"    :  oValue = Sql.ToDBString  (this.BillingCountry         );  break;
							case "MODIFIED_USER_ID"           :  oValue = gUSER_ID;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "NAME"                       :  bChanged = ParameterChanged(par, oValue,   11);  break;
									case "DESCRIPTION"                :  bChanged = ParameterChanged(par, oValue, 4095);  break;
									case "PURCHASE_ORDER_NUM"         :  bChanged = ParameterChanged(par, oValue,   25);  break;
									case "PAYMENT_TERMS"              :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "BILLING_ADDRESS_STREET"     :  bChanged = ParameterChanged(par, oValue,  215);  break;
									case "BILLING_ADDRESS_CITY"       :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "BILLING_ADDRESS_STATE"      :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "BILLING_ADDRESS_POSTALCODE" :  bChanged = ParameterChanged(par, oValue,   13);  break;
									case "BILLING_ADDRESS_COUNTRY"    :  bChanged = ParameterChanged(par, oValue,   31);  break;
									default                           :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
