/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class Customer : QObject
	{
		#region Properties
		public string   ParentId                   { get; set; }
		public string   ParentName                 { get; set; }
		public string   Salutation                 { get; set; }
		public string   FirstName                  { get; set; }
		public string   MiddleInitial              { get; set; }
		public string   LastName                   { get; set; }
		public string   Contact                    { get; set; }
		public string   Phone                      { get; set; }
		public string   Fax                        { get; set; }
		public string   AlternateContact           { get; set; }
		public string   AlternatePhone             { get; set; }
		public string   Email                      { get; set; }
		public string   Notes                      { get; set; }
		public string   BillingLine1               { get; set; }
		public string   BillingLine2               { get; set; }
		public string   BillingLine3               { get; set; }
		public string   BillingLine4               { get; set; }
		public string   BillingLine5               { get; set; }
		public string   BillingCity                { get; set; }
		public string   BillingState               { get; set; }
		public string   BillingPostalCode          { get; set; }
		public string   BillingCountry             { get; set; }
		public string   ShippingLine1              { get; set; }
		public string   ShippingLine2              { get; set; }
		public string   ShippingLine3              { get; set; }
		public string   ShippingLine4              { get; set; }
		public string   ShippingLine5              { get; set; }
		public string   ShippingCity               { get; set; }
		public string   ShippingState              { get; set; }
		public string   ShippingPostalCode         { get; set; }
		public string   ShippingCountry            { get; set; }
		#endregion
		protected DataView vwParents;

		public Customer(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtParents) : base(qbf, qbcon, "Customers", "Name", "Accounts", "ACCOUNTS", "NAME", true)
		{
			this.vwParents = new DataView(dtParents);
		}

		public override void Reset()
		{
			base.Reset();
			this.ParentId           = String.Empty;
			this.ParentName         = String.Empty;
			this.Salutation         = String.Empty;
			this.FirstName          = String.Empty;
			this.MiddleInitial      = String.Empty;
			this.LastName           = String.Empty;
			this.Contact            = String.Empty;
			this.Phone              = String.Empty;
			this.Fax                = String.Empty;
			this.AlternateContact   = String.Empty;
			this.AlternatePhone     = String.Empty;
			this.Email              = String.Empty;
			this.Notes              = String.Empty;
			this.BillingLine1       = String.Empty;
			this.BillingLine2       = String.Empty;
			this.BillingLine3       = String.Empty;
			this.BillingLine4       = String.Empty;
			this.BillingLine5       = String.Empty;
			this.BillingCity        = String.Empty;
			this.BillingState       = String.Empty;
			this.BillingPostalCode  = String.Empty;
			this.BillingCountry     = String.Empty;
			this.ShippingLine1      = String.Empty;
			this.ShippingLine2      = String.Empty;
			this.ShippingLine3      = String.Empty;
			this.ShippingLine4      = String.Empty;
			this.ShippingLine5      = String.Empty;
			this.ShippingCity       = String.Empty;
			this.ShippingState      = String.Empty;
			this.ShippingPostalCode = String.Empty;
			this.ShippingCountry    = String.Empty;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			string sBillingAddress  = Sql.ToString(row["BILLING_ADDRESS_STREET" ]).Replace(ControlChars.CrLf, "\n");
			string sShippingAddress = Sql.ToString(row["SHIPPING_ADDRESS_STREET"]).Replace(ControlChars.CrLf, "\n");
			List<string> arrBillingAddress  = new List<string>(sBillingAddress .Split('\n'));
			List<string> arrShippingAddress = new List<string>(sShippingAddress.Split('\n'));
			while ( arrBillingAddress.Count < 5 )
				arrBillingAddress.Add(String.Empty);
			while ( arrShippingAddress.Count < 5 )
				arrShippingAddress.Add(String.Empty);
			
			vwParents.RowFilter = "SYNC_LOCAL_ID = '" + Sql.ToString(row["PARENT_ID"]) + "'";
			if ( vwParents.Count > 0 )
				this.ParentId = Sql.ToString(vwParents[0]["SYNC_REMOTE_KEY"]);
			
			this.ID = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.Name               = Sql.ToString(row["NAME"                       ]);
				this.Notes              = Sql.ToString(row["DESCRIPTION"                ]);
				this.Email              = Sql.ToString(row["EMAIL1"                     ]);
				this.AlternatePhone     = Sql.ToString(row["PHONE_ALTERNATE"            ]);
				this.Fax                = Sql.ToString(row["PHONE_FAX"                  ]);
				this.Phone              = Sql.ToString(row["PHONE_OFFICE"               ]);
				this.BillingLine1       = Sql.ToString(arrBillingAddress[0]              );
				this.BillingLine2       = Sql.ToString(arrBillingAddress[1]              );
				this.BillingLine3       = Sql.ToString(arrBillingAddress[2]              );
				this.BillingLine4       = Sql.ToString(arrBillingAddress[3]              );
				this.BillingLine5       = Sql.ToString(arrBillingAddress[4]              );
				this.BillingCity        = Sql.ToString(row["BILLING_ADDRESS_CITY"       ]);
				this.BillingState       = Sql.ToString(row["BILLING_ADDRESS_STATE"      ]);
				this.BillingPostalCode  = Sql.ToString(row["BILLING_ADDRESS_POSTALCODE" ]);
				this.BillingCountry     = Sql.ToString(row["BILLING_ADDRESS_COUNTRY"    ]);
				this.ShippingLine1      = Sql.ToString(arrShippingAddress[0]             );
				this.ShippingLine2      = Sql.ToString(arrShippingAddress[1]             );
				this.ShippingLine3      = Sql.ToString(arrShippingAddress[2]             );
				this.ShippingLine4      = Sql.ToString(arrShippingAddress[3]             );
				this.ShippingLine5      = Sql.ToString(arrShippingAddress[4]             );
				this.ShippingCity       = Sql.ToString(row["SHIPPING_ADDRESS_CITY"      ]);
				this.ShippingState      = Sql.ToString(row["SHIPPING_ADDRESS_STATE"     ]);
				this.ShippingPostalCode = Sql.ToString(row["SHIPPING_ADDRESS_POSTALCODE"]);
				this.ShippingCountry    = Sql.ToString(row["SHIPPING_ADDRESS_COUNTRY"   ]);
				bChanged = true;
			}
			else
			{
				if ( Compare(this.Name              , row["NAME"                       ], "NAME"                       , sbChanges) ) { this.Name               = Sql.ToString(row["NAME"                       ]);  bChanged = true; }
				if ( Compare(this.Notes             , row["DESCRIPTION"                ], "DESCRIPTION"                , sbChanges) ) { this.Notes              = Sql.ToString(row["DESCRIPTION"                ]);  bChanged = true; }
				if ( Compare(this.Email             , row["EMAIL1"                     ], "EMAIL1"                     , sbChanges) ) { this.Email              = Sql.ToString(row["EMAIL1"                     ]);  bChanged = true; }
				if ( Compare(this.AlternatePhone    , row["PHONE_ALTERNATE"            ], "PHONE_ALTERNATE"            , sbChanges) ) { this.AlternatePhone     = Sql.ToString(row["PHONE_ALTERNATE"            ]);  bChanged = true; }
				if ( Compare(this.Fax               , row["PHONE_FAX"                  ], "PHONE_FAX"                  , sbChanges) ) { this.Fax                = Sql.ToString(row["PHONE_FAX"                  ]);  bChanged = true; }
				if ( Compare(this.Phone             , row["PHONE_OFFICE"               ], "PHONE_OFFICE"               , sbChanges) ) { this.Phone              = Sql.ToString(row["PHONE_OFFICE"               ]);  bChanged = true; }
				if ( Compare(this.BillingLine1      ,     arrBillingAddress[0]          , "BillingLine1"               , sbChanges) ) { this.BillingLine1       = Sql.ToString(    arrBillingAddress[0]          );  bChanged = true; }
				if ( Compare(this.BillingLine2      ,     arrBillingAddress[1]          , "BillingLine2"               , sbChanges) ) { this.BillingLine2       = Sql.ToString(    arrBillingAddress[1]          );  bChanged = true; }
				if ( Compare(this.BillingLine3      ,     arrBillingAddress[2]          , "BillingLine3"               , sbChanges) ) { this.BillingLine3       = Sql.ToString(    arrBillingAddress[2]          );  bChanged = true; }
				if ( Compare(this.BillingLine4      ,     arrBillingAddress[3]          , "BillingLine4"               , sbChanges) ) { this.BillingLine4       = Sql.ToString(    arrBillingAddress[3]          );  bChanged = true; }
				if ( Compare(this.BillingLine5      ,     arrBillingAddress[4]          , "BillingLine5"               , sbChanges) ) { this.BillingLine5       = Sql.ToString(    arrBillingAddress[4]          );  bChanged = true; }
				if ( Compare(this.BillingCity       , row["BILLING_ADDRESS_CITY"       ], "BILLING_ADDRESS_CITY"       , sbChanges) ) { this.BillingCity        = Sql.ToString(row["BILLING_ADDRESS_CITY"       ]);  bChanged = true; }
				if ( Compare(this.BillingState      , row["BILLING_ADDRESS_STATE"      ], "BILLING_ADDRESS_STATE"      , sbChanges) ) { this.BillingState       = Sql.ToString(row["BILLING_ADDRESS_STATE"      ]);  bChanged = true; }
				if ( Compare(this.BillingPostalCode , row["BILLING_ADDRESS_POSTALCODE" ], "BILLING_ADDRESS_POSTALCODE" , sbChanges) ) { this.BillingPostalCode  = Sql.ToString(row["BILLING_ADDRESS_POSTALCODE" ]);  bChanged = true; }
				if ( Compare(this.BillingCountry    , row["BILLING_ADDRESS_COUNTRY"    ], "BILLING_ADDRESS_COUNTRY"    , sbChanges) ) { this.BillingCountry     = Sql.ToString(row["BILLING_ADDRESS_COUNTRY"    ]);  bChanged = true; }
				if ( Compare(this.ShippingLine1     ,     arrShippingAddress[0]         , "ShippingLine1"              , sbChanges) ) { this.ShippingLine1      = Sql.ToString(    arrShippingAddress[0]         );  bChanged = true; }
				if ( Compare(this.ShippingLine2     ,     arrShippingAddress[1]         , "ShippingLine2"              , sbChanges) ) { this.ShippingLine2      = Sql.ToString(    arrShippingAddress[1]         );  bChanged = true; }
				if ( Compare(this.ShippingLine3     ,     arrShippingAddress[2]         , "ShippingLine3"              , sbChanges) ) { this.ShippingLine3      = Sql.ToString(    arrShippingAddress[2]         );  bChanged = true; }
				if ( Compare(this.ShippingLine4     ,     arrShippingAddress[3]         , "ShippingLine4"              , sbChanges) ) { this.ShippingLine4      = Sql.ToString(    arrShippingAddress[3]         );  bChanged = true; }
				if ( Compare(this.ShippingLine5     ,     arrShippingAddress[4]         , "ShippingLine5"              , sbChanges) ) { this.ShippingLine5      = Sql.ToString(    arrShippingAddress[4]         );  bChanged = true; }
				if ( Compare(this.ShippingCity      , row["SHIPPING_ADDRESS_CITY"      ], "SHIPPING_ADDRESS_CITY"      , sbChanges) ) { this.ShippingCity       = Sql.ToString(row["SHIPPING_ADDRESS_CITY"      ]);  bChanged = true; }
				if ( Compare(this.ShippingState     , row["SHIPPING_ADDRESS_STATE"     ], "SHIPPING_ADDRESS_STATE"     , sbChanges) ) { this.ShippingState      = Sql.ToString(row["SHIPPING_ADDRESS_STATE"     ]);  bChanged = true; }
				if ( Compare(this.ShippingPostalCode, row["SHIPPING_ADDRESS_POSTALCODE"], "SHIPPING_ADDRESS_POSTALCODE", sbChanges) ) { this.ShippingPostalCode = Sql.ToString(row["SHIPPING_ADDRESS_POSTALCODE"]);  bChanged = true; }
				if ( Compare(this.ShippingCountry   , row["SHIPPING_ADDRESS_COUNTRY"   ], "SHIPPING_ADDRESS_COUNTRY"   , sbChanges) ) { this.ShippingCountry    = Sql.ToString(row["SHIPPING_ADDRESS_COUNTRY"   ]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.ParentId           = Sql.ToString  (row["ParentId"          ]);
			this.ParentName         = Sql.ToString  (row["ParentName"        ]);
			this.Notes              = Sql.ToString  (row["Notes"             ]);
			this.Email              = Sql.ToString  (row["Email"             ]);
			this.AlternateContact   = Sql.ToString  (row["AlternateContact"  ]);
			this.AlternatePhone     = Sql.ToString  (row["AlternatePhone"    ]);
			this.Fax                = Sql.ToString  (row["Fax"               ]);
			this.Phone              = Sql.ToString  (row["Phone"             ]);
			this.BillingLine1       = Sql.ToString  (row["BillingLine1"      ]);
			this.BillingLine2       = Sql.ToString  (row["BillingLine2"      ]);
			this.BillingLine3       = Sql.ToString  (row["BillingLine3"      ]);
			this.BillingLine4       = Sql.ToString  (row["BillingLine4"      ]);
			this.BillingLine5       = Sql.ToString  (row["BillingLine5"      ]);
			this.BillingCity        = Sql.ToString  (row["BillingCity"       ]);
			this.BillingState       = Sql.ToString  (row["BillingState"      ]);
			this.BillingPostalCode  = Sql.ToString  (row["BillingPostalCode" ]);
			this.BillingCountry     = Sql.ToString  (row["BillingCountry"    ]);
			this.ShippingLine1      = Sql.ToString  (row["ShippingLine1"     ]);
			this.ShippingLine2      = Sql.ToString  (row["ShippingLine2"     ]);
			this.ShippingLine3      = Sql.ToString  (row["ShippingLine3"     ]);
			this.ShippingLine4      = Sql.ToString  (row["ShippingLine4"     ]);
			this.ShippingLine5      = Sql.ToString  (row["ShippingLine5"     ]);
			this.ShippingCity       = Sql.ToString  (row["ShippingCity"      ]);
			this.ShippingState      = Sql.ToString  (row["ShippingState"     ]);
			this.ShippingPostalCode = Sql.ToString  (row["ShippingPostalCode"]);
			this.ShippingCountry    = Sql.ToString  (row["ShippingCountry"   ]);
		}

		public override string Insert()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( Name              " + ControlChars.CrLf
				     + "     , ParentId          " + ControlChars.CrLf
				     + "     , Notes             " + ControlChars.CrLf
				     + "     , Email             " + ControlChars.CrLf
				     + "     , AlternateContact  " + ControlChars.CrLf
				     + "     , AlternatePhone    " + ControlChars.CrLf
				     + "     , Fax               " + ControlChars.CrLf
				     + "     , Phone             " + ControlChars.CrLf
				     + "     , BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity       " + ControlChars.CrLf
				     + "     , BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry    " + ControlChars.CrLf
				     + "     , ShippingLine1     " + ControlChars.CrLf
				     + "     , ShippingLine2     " + ControlChars.CrLf
				     + "     , ShippingLine3     " + ControlChars.CrLf
				     + "     , ShippingLine4     " + ControlChars.CrLf
				     + "     , ShippingLine5     " + ControlChars.CrLf
				     + "     , ShippingCity      " + ControlChars.CrLf
				     + "     , ShippingState     " + ControlChars.CrLf
				     + "     , ShippingPostalCode" + ControlChars.CrLf
				     + "     , ShippingCountry   " + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @Name              " + ControlChars.CrLf
				     + "     , @ParentId          " + ControlChars.CrLf
				     + "     , @Notes             " + ControlChars.CrLf
				     + "     , @Email             " + ControlChars.CrLf
				     + "     , @AlternateContact  " + ControlChars.CrLf
				     + "     , @AlternatePhone    " + ControlChars.CrLf
				     + "     , @Fax               " + ControlChars.CrLf
				     + "     , @Phone             " + ControlChars.CrLf
				     + "     , @BillingLine1      " + ControlChars.CrLf
				     + "     , @BillingLine2      " + ControlChars.CrLf
				     + "     , @BillingLine3      " + ControlChars.CrLf
				     + "     , @BillingLine4      " + ControlChars.CrLf
				     + "     , @BillingLine5      " + ControlChars.CrLf
				     + "     , @BillingCity       " + ControlChars.CrLf
				     + "     , @BillingState      " + ControlChars.CrLf
				     + "     , @BillingPostalCode " + ControlChars.CrLf
				     + "     , @BillingCountry    " + ControlChars.CrLf
				     + "     , @ShippingLine1     " + ControlChars.CrLf
				     + "     , @ShippingLine2     " + ControlChars.CrLf
				     + "     , @ShippingLine3     " + ControlChars.CrLf
				     + "     , @ShippingLine4     " + ControlChars.CrLf
				     + "     , @ShippingLine5     " + ControlChars.CrLf
				     + "     , @ShippingCity      " + ControlChars.CrLf
				     + "     , @ShippingState     " + ControlChars.CrLf
				     + "     , @ShippingPostalCode" + ControlChars.CrLf
				     + "     , @ShippingCountry   " + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"              , this.Name              , 41);
				Sql.AddParameter(qbcmd, "@ParentId"          , this.ParentId          );
				//Sql.AddParameter(qbcmd, "@CompanyName"       , this.CompanyName       , 41);
				//Sql.AddParameter(qbcmd, "@Salutation"        , this.Salutation        , 15);
				//Sql.AddParameter(qbcmd, "@FirstName"         , this.FirstName         , 25);
				//Sql.AddParameter(qbcmd, "@MiddleName"        , this.MiddleName        ,  5);
				//Sql.AddParameter(qbcmd, "@LastName"          , this.LastName          , 25);
				//Sql.AddParameter(qbcmd, "@Contact"           , this.Contact           , 41);
				Sql.AddParameter(qbcmd, "@Notes"             , this.Notes             , 4095);
				Sql.AddParameter(qbcmd, "@Email"             , this.Email             , 1000);
				Sql.AddParameter(qbcmd, "@AlternateContact"  , this.AlternateContact  , 41);
				Sql.AddParameter(qbcmd, "@AlternatePhone"    , this.AlternatePhone    , 21);
				Sql.AddParameter(qbcmd, "@Fax"               , this.Fax               , 21);
				Sql.AddParameter(qbcmd, "@Phone"             , this.Phone             , 21);
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				//Sql.AddParameter(qbcmd, "@BillingNote"       , this.BillingNote       , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine1"     , this.ShippingLine1     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine2"     , this.ShippingLine2     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine3"     , this.ShippingLine3     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine4"     , this.ShippingLine4     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine5"     , this.ShippingLine5     , 41);
				Sql.AddParameter(qbcmd, "@ShippingCity"      , this.ShippingCity      , 31);
				Sql.AddParameter(qbcmd, "@ShippingState"     , this.ShippingState     , 21);
				Sql.AddParameter(qbcmd, "@ShippingPostalCode", this.ShippingPostalCode, 13);
				Sql.AddParameter(qbcmd, "@ShippingCountry"   , this.ShippingCountry   , 31);
				//Sql.AddParameter(qbcmd, "@CreditCardNumber"    , this.CreditCardNumber    , 25);
				//Sql.AddParameter(qbcmd, "@CreditCardName"      , this.CreditCardName      , 41);
				//Sql.AddParameter(qbcmd, "@CreditCardAddress"   , this.CreditCardAddress   , 41);
				//Sql.AddParameter(qbcmd, "@CreditCardPostalCode", this.CreditCardPostalCode, 41);
				//Sql.AddParameter(qbcmd, "@JobDescription"      , this.JobDescription      , 99);
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public override void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set Name               = @Name              " + ControlChars.CrLf
				     + "     , ParentId           = @ParentId          " + ControlChars.CrLf
				     + "     , Notes              = @Notes             " + ControlChars.CrLf
				     + "     , Email              = @Email             " + ControlChars.CrLf
				     + "     , AlternateContact   = @AlternateContact  " + ControlChars.CrLf
				     + "     , AlternatePhone     = @AlternatePhone    " + ControlChars.CrLf
				     + "     , Fax                = @Fax               " + ControlChars.CrLf
				     + "     , Phone              = @Phone             " + ControlChars.CrLf
				     + "     , BillingLine1       = @BillingLine1      " + ControlChars.CrLf
				     + "     , BillingLine2       = @BillingLine2      " + ControlChars.CrLf
				     + "     , BillingLine3       = @BillingLine3      " + ControlChars.CrLf
				     + "     , BillingLine4       = @BillingLine4      " + ControlChars.CrLf
				     + "     , BillingLine5       = @BillingLine5      " + ControlChars.CrLf
				     + "     , BillingCity        = @BillingCity       " + ControlChars.CrLf
				     + "     , BillingState       = @BillingState      " + ControlChars.CrLf
				     + "     , BillingPostalCode  = @BillingPostalCode " + ControlChars.CrLf
				     + "     , BillingCountry     = @BillingCountry    " + ControlChars.CrLf
				     + "     , ShippingLine1      = @ShippingLine1     " + ControlChars.CrLf
				     + "     , ShippingLine2      = @ShippingLine2     " + ControlChars.CrLf
				     + "     , ShippingLine3      = @ShippingLine3     " + ControlChars.CrLf
				     + "     , ShippingLine4      = @ShippingLine4     " + ControlChars.CrLf
				     + "     , ShippingLine5      = @ShippingLine5     " + ControlChars.CrLf
				     + "     , ShippingCity       = @ShippingCity      " + ControlChars.CrLf
				     + "     , ShippingState      = @ShippingState     " + ControlChars.CrLf
				     + "     , ShippingPostalCode = @ShippingPostalCode" + ControlChars.CrLf
				     + "     , ShippingCountry    = @ShippingCountry   " + ControlChars.CrLf
				     + " where ID                 = @ID                " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"              , this.Name              , 41);
				Sql.AddParameter(qbcmd, "@ParentId"          , this.ParentId          );
				Sql.AddParameter(qbcmd, "@Notes"             , this.Notes             , 4095);
				Sql.AddParameter(qbcmd, "@Email"             , this.Email             , 1000);
				Sql.AddParameter(qbcmd, "@AlternateContact"  , this.AlternateContact  , 41);
				Sql.AddParameter(qbcmd, "@AlternatePhone"    , this.AlternatePhone    , 21);
				Sql.AddParameter(qbcmd, "@Fax"               , this.Fax               , 21);
				Sql.AddParameter(qbcmd, "@Phone"             , this.Phone             , 21);
				Sql.AddParameter(qbcmd, "@BillingLine1"      , this.BillingLine1      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine2"      , this.BillingLine2      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine3"      , this.BillingLine3      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine4"      , this.BillingLine4      , 41);
				Sql.AddParameter(qbcmd, "@BillingLine5"      , this.BillingLine5      , 41);
				Sql.AddParameter(qbcmd, "@BillingCity"       , this.BillingCity       , 31);
				Sql.AddParameter(qbcmd, "@BillingState"      , this.BillingState      , 21);
				Sql.AddParameter(qbcmd, "@BillingPostalCode" , this.BillingPostalCode , 13);
				Sql.AddParameter(qbcmd, "@BillingCountry"    , this.BillingCountry    , 31);
				Sql.AddParameter(qbcmd, "@ShippingLine1"     , this.ShippingLine1     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine2"     , this.ShippingLine2     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine3"     , this.ShippingLine3     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine4"     , this.ShippingLine4     , 41);
				Sql.AddParameter(qbcmd, "@ShippingLine5"     , this.ShippingLine5     , 41);
				Sql.AddParameter(qbcmd, "@ShippingCity"      , this.ShippingCity      , 31);
				Sql.AddParameter(qbcmd, "@ShippingState"     , this.ShippingState     , 21);
				Sql.AddParameter(qbcmd, "@ShippingPostalCode", this.ShippingPostalCode, 13);
				Sql.AddParameter(qbcmd, "@ShippingCountry"   , this.ShippingCountry   , 31);
				Sql.AddParameter(qbcmd, "@ID"                , this.ID                );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterCRM(IDbCommand cmd)
		{
			Sql.AppendParameter(cmd, Sql.ToString(this.Name), "NAME");
			// 05/29/2012 Paul.  Now that we support the ParentId, we need to make sure to filter on this value as children can have identical names. 
			Guid gPARENT_ID = Guid.Empty;
			if ( !Sql.IsEmptyString(this.ParentId) )
			{
				vwParents.RowFilter = "SYNC_REMOTE_KEY = '" + this.ParentId + "'";
				if ( vwParents.Count > 0 )
					gPARENT_ID = Sql.ToGuid(vwParents[0]["SYNC_LOCAL_ID"]);
			}
			if ( Sql.IsEmptyGuid(gPARENT_ID) )
				cmd.CommandText += "   and PARENT_ID is null" + ControlChars.CrLf;
			else
				Sql.AppendParameter(cmd, gPARENT_ID, "PARENT_ID");
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			
			StringBuilder sbBillingAddress  = new StringBuilder();
			StringBuilder sbShippingAddress = new StringBuilder();
			if ( !Sql.IsEmptyString(this.BillingLine1 ) ) sbBillingAddress .AppendLine(this.BillingLine1);
			if ( !Sql.IsEmptyString(this.BillingLine2 ) ) sbBillingAddress .AppendLine(this.BillingLine2);
			if ( !Sql.IsEmptyString(this.BillingLine3 ) ) sbBillingAddress .AppendLine(this.BillingLine3);
			if ( !Sql.IsEmptyString(this.BillingLine4 ) ) sbBillingAddress .AppendLine(this.BillingLine4);
			if ( !Sql.IsEmptyString(this.BillingLine5 ) ) sbBillingAddress .AppendLine(this.BillingLine5);
			if ( !Sql.IsEmptyString(this.ShippingLine1) ) sbShippingAddress.AppendLine(this.ShippingLine1);
			if ( !Sql.IsEmptyString(this.ShippingLine2) ) sbShippingAddress.AppendLine(this.ShippingLine2);
			if ( !Sql.IsEmptyString(this.ShippingLine3) ) sbShippingAddress.AppendLine(this.ShippingLine3);
			if ( !Sql.IsEmptyString(this.ShippingLine4) ) sbShippingAddress.AppendLine(this.ShippingLine4);
			if ( !Sql.IsEmptyString(this.ShippingLine5) ) sbShippingAddress.AppendLine(this.ShippingLine5);
			
			Guid gPARENT_ID = Guid.Empty;
			if ( !Sql.IsEmptyString(this.ParentId) )
			{
				vwParents.RowFilter = "SYNC_REMOTE_KEY = '" + this.ParentId + "'";
				if ( vwParents.Count > 0 )
					gPARENT_ID = Sql.ToGuid(vwParents[0]["SYNC_LOCAL_ID"]);
			}
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( SplendidInit.bEnableACLFieldSecurity )
				{
					acl = ExchangeSecurity.GetUserFieldSecurity(Session, this.CRMModuleName, sColumnName, gASSIGNED_USER_ID);
				}
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "PARENT_ID"                  :  oValue = Sql.ToDBGuid  (     gPARENT_ID             );  break;
							case "NAME"                       :  oValue = Sql.ToDBString(this.Name                   );  break;
							case "DESCRIPTION"                :  oValue = Sql.ToDBString(this.Notes                  );  break;
							case "EMAIL1"                     :  oValue = Sql.ToDBString(this.Email                  );  break;
							case "ASSISTANT"                  :  oValue = Sql.ToDBString(this.AlternateContact       );  break;
							case "PHONE_ALTERNATE"            :  oValue = Sql.ToDBString(this.AlternatePhone         );  break;
							case "PHONE_FAX"                  :  oValue = Sql.ToDBString(this.Fax                    );  break;
							case "PHONE_OFFICE"               :  oValue = Sql.ToDBString(this.Phone                  );  break;
							case "BILLING_ADDRESS_STREET"     :  oValue = Sql.ToDBString(sbBillingAddress.ToString() );  break;
							case "BILLING_ADDRESS_CITY"       :  oValue = Sql.ToDBString(this.BillingCity            );  break;
							case "BILLING_ADDRESS_STATE"      :  oValue = Sql.ToDBString(this.BillingState           );  break;
							case "BILLING_ADDRESS_POSTALCODE" :  oValue = Sql.ToDBString(this.BillingPostalCode      );  break;
							case "BILLING_ADDRESS_COUNTRY"    :  oValue = Sql.ToDBString(this.BillingCountry         );  break;
							case "SHIPPING_ADDRESS_STREET"    :  oValue = Sql.ToDBString(sbShippingAddress.ToString());  break;
							case "SHIPPING_ADDRESS_CITY"      :  oValue = Sql.ToDBString(this.ShippingCity           );  break;
							case "SHIPPING_ADDRESS_STATE"     :  oValue = Sql.ToDBString(this.ShippingState          );  break;
							case "SHIPPING_ADDRESS_POSTALCODE":  oValue = Sql.ToDBString(this.ShippingPostalCode     );  break;
							case "SHIPPING_ADDRESS_COUNTRY"   :  oValue = Sql.ToDBString(this.ShippingCountry        );  break;
							case "MODIFIED_USER_ID"           :  oValue = gUSER_ID;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "NAME"                       :  bChanged = ParameterChanged(par, oValue,   41);  break;
									case "DESCRIPTION"                :  bChanged = ParameterChanged(par, oValue, 4095);  break;
									case "EMAIL1"                     :  bChanged = ParameterChanged(par, oValue, 1000);  break;
									case "ASSISTANT"                  :  bChanged = ParameterChanged(par, oValue,   41);  break;
									case "PHONE_ALTERNATE"            :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "PHONE_FAX"                  :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "PHONE_OFFICE"               :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "BILLING_ADDRESS_STREET"     :  bChanged = ParameterChanged(par, oValue,  215);  break;
									case "BILLING_ADDRESS_CITY"       :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "BILLING_ADDRESS_STATE"      :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "BILLING_ADDRESS_POSTALCODE" :  bChanged = ParameterChanged(par, oValue,   13);  break;
									case "BILLING_ADDRESS_COUNTRY"    :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "SHIPPING_ADDRESS_STREET"    :  bChanged = ParameterChanged(par, oValue,  215);  break;
									case "SHIPPING_ADDRESS_CITY"      :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "SHIPPING_ADDRESS_STATE"     :  bChanged = ParameterChanged(par, oValue,   21);  break;
									case "SHIPPING_ADDRESS_POSTALCODE":  bChanged = ParameterChanged(par, oValue,   13);  break;
									case "SHIPPING_ADDRESS_COUNTRY"   :  bChanged = ParameterChanged(par, oValue,   31);  break;
									default                           :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}

		public override void ProcedureUpdated(Guid gID, string sREMOTE_KEY)
		{
			vwParents.RowFilter = "SYNC_LOCAL_ID = '" + gID.ToString() + "'";
			if ( vwParents.Count == 0 )
			{
				DataRow row = vwParents.Table.NewRow();
				row["SYNC_LOCAL_ID"  ] = gID        ;
				row["SYNC_REMOTE_KEY"] = sREMOTE_KEY;
				vwParents.Table.Rows.Add(row);
			}
		}
	}
}
