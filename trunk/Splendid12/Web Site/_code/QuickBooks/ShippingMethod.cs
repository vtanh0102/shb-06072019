/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class ShippingMethod : QObject
	{
		#region Properties
		public bool IsActive { get; set; }
		#endregion

		public ShippingMethod(QuickBooksClientFactory qbf, IDbConnection qbcon) : base(qbf, qbcon, "ShippingMethods", "Name", "Shippers", "SHIPPERS", "NAME", false)
		{
		}

		public override void Reset()
		{
			base.Reset();
			this.IsActive = false;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			this.ID       = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.Name     = Sql.ToString(row["NAME"]);
				this.IsActive = Sql.ToString(row["STATUS"]) == "Active";
				bChanged = true;
			}
			else
			{
				if ( Compare(this.Name    ,               row["NAME"  ]              , "NAME"  , sbChanges) ) { this.Name     = Sql.ToString(row["NAME"  ])            ;  bChanged = true; }
				if ( Compare(this.IsActive, (Sql.ToString(row["STATUS"]) == "Active"), "STATUS", sbChanges) ) { this.IsActive = Sql.ToString(row["STATUS"]) == "Active";  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.IsActive = Sql.ToBoolean(row["IsActive"]);
		}

		public override string Insert()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( Name    " + ControlChars.CrLf
				     + "     , IsActive" + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @Name    " + ControlChars.CrLf
				     + "     , @IsActive" + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"    , this.Name    , 15);
				Sql.AddParameter(qbcmd, "@IsActive", this.IsActive);
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public override void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set Name     = @Name    " + ControlChars.CrLf
				     + "     , IsActive = @IsActive" + ControlChars.CrLf
				     + " where ID       = @ID      " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"    , this.Name    , 15);
				Sql.AddParameter(qbcmd, "@IsActive", this.IsActive);
				Sql.AddParameter(qbcmd, "@ID"      , this.ID      );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "NAME"            :  oValue = Sql.ToDBString(this.Name                            );  break;
							case "STATUS"          :  oValue = Sql.ToDBString(this.IsActive ? "Active" : "Inactive");  break;
							case "MODIFIED_USER_ID":  oValue = gUSER_ID;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								if ( sColumnName == "NAME" )
									bChanged = ParameterChanged(par, oValue, 15);
								else
									bChanged = ParameterChanged(par, oValue);
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
