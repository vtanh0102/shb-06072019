/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM.QuickBooks
{
	public class Item : QObject
	{
		#region Properties
		public string   Type                       { get; set; }
		public string   Account                    { get; set; }
		public string   Description                { get; set; }
		public double   Price                      { get; set; }
		public bool     IsActive                   { get; set; }
		public double   PurchaseCost               { get; set; }
		public double   QuantityOnHand             { get; set; }
		public string   TaxCode                    { get; set; }
		public string   PartNumber                 { get; set; }
		#endregion
		protected DataView vwProductTypes;

		public Item(QuickBooksClientFactory qbf, IDbConnection qbcon, DataTable dtProductTypes) : base(qbf, qbcon, "Items", "Name", "ProductTemplates", "PRODUCT_TEMPLATES", "NAME", false)
		{
			this.vwProductTypes = new DataView(dtProductTypes);
		}

		public override void Reset()
		{
			base.Reset();
			this.Type                 = String.Empty;
			this.Account              = String.Empty;
			this.Description          = String.Empty;
			this.Price                = 0.0;
			this.IsActive             = false;
			this.PurchaseCost         = 0.0;
			this.QuantityOnHand       = 0;
			this.TaxCode              = String.Empty;
			this.PartNumber           = String.Empty;
		}

		public override bool SetFromCRM(string sID, DataRow row, StringBuilder sbChanges)
		{
			bool bChanged = false;
			this.ID       = sID;
			this.LOCAL_ID = Sql.ToGuid(row["ID"]);
			string sTaxCode  = Sql.ToString(row["TAX_CLASS"]) == "Taxable" ? "Tax" : "Non";
			bool   bIsActive = Sql.ToString(row["STATUS"   ]) == "Available";
			if ( Sql.IsEmptyString(this.ID) )
			{
				this.Name                 = Sql.ToString (row["MFT_PART_NUM"      ]);
				this.Type                 = Sql.ToString (row["TYPE_NAME"         ]);
				this.Account              = Sql.ToString (row["QUICKBOOKS_ACCOUNT"]);
				this.Description          = Sql.ToString (row["NAME"              ]);
				this.Price                = Sql.ToDouble (row["DISCOUNT_PRICE"    ]);
				this.IsActive             = Sql.ToBoolean(bIsActive                );
				this.PurchaseCost         = Sql.ToDouble (row["COST_PRICE"        ]);
				this.QuantityOnHand       = Sql.ToDouble (row["QUANTITY"          ]);
				this.TaxCode              = Sql.ToString (sTaxCode                 );
				this.PartNumber           = Sql.ToString (row["VENDOR_PART_NUM"   ]);
				bChanged = true;
			}
			else
			{
				if ( Compare(this.Name          , row["MFT_PART_NUM"      ], "MFT_PART_NUM"      , sbChanges) ) { this.Name           = Sql.ToString (row["MFT_PART_NUM"      ]);  bChanged = true; }
				if ( Compare(this.Type          , row["TYPE_NAME"         ], "TYPE_NAME"         , sbChanges) ) { this.Type           = Sql.ToString (row["TYPE_NAME"         ]);  bChanged = true; }
				if ( Compare(this.Account       , row["QUICKBOOKS_ACCOUNT"], "QUICKBOOKS_ACCOUNT", sbChanges) ) { this.Account        = Sql.ToString (row["QUICKBOOKS_ACCOUNT"]);  bChanged = true; }
				if ( Compare(this.Description   , row["NAME"              ], "NAME"              , sbChanges) ) { this.Description    = Sql.ToString (row["NAME"              ]);  bChanged = true; }
				if ( Compare(this.Price         , row["DISCOUNT_PRICE"    ], "DISCOUNT_PRICE"    , sbChanges) ) { this.Price          = Sql.ToDouble (row["DISCOUNT_PRICE"    ]);  bChanged = true; }
				if ( Compare(this.IsActive      ,     bIsActive            , "STATUS"            , sbChanges) ) { this.IsActive       = Sql.ToBoolean(     bIsActive           );  bChanged = true; }
				if ( Compare(this.PurchaseCost  , row["COST_PRICE"        ], "COST_PRICE"        , sbChanges) ) { this.PurchaseCost   = Sql.ToDouble (row["COST_PRICE"        ]);  bChanged = true; }
				if ( Compare(this.QuantityOnHand, row["QUANTITY"          ], "QUANTITY"          , sbChanges) ) { this.QuantityOnHand = Sql.ToDouble (row["QUANTITY"          ]);  bChanged = true; }
				if ( Compare(this.TaxCode       ,     sTaxCode             , "TAX_CLASS"         , sbChanges) ) { this.TaxCode        = Sql.ToString (     sTaxCode            );  bChanged = true; }
				if ( Compare(this.PartNumber    , row["VENDOR_PART_NUM"   ], "VENDOR_PART_NUM"   , sbChanges) ) { this.PartNumber     = Sql.ToString (row["VENDOR_PART_NUM"   ]);  bChanged = true; }
			}
			return bChanged;
		}

		public override void SetFromQuickBooks(DataRow row)
		{
			base.SetFromQuickBooks(row);
			this.Type                 = Sql.ToString  (row["Type"                ]);
			this.Account              = Sql.ToString  (row["Account"             ]);
			this.Description          = Sql.ToString  (row["Description"         ]);
			this.Price                = Sql.ToDouble  (row["Price"               ]);
			this.IsActive             = Sql.ToBoolean (row["IsActive"            ]);
			this.PurchaseCost         = Sql.ToDouble  (row["PurchaseCost"        ]);
			this.QuantityOnHand       = Sql.ToDouble  (row["QuantityOnHand"      ]);
			this.TaxCode              = Sql.ToString  (row["TaxCode"             ]);
			this.PartNumber           = Sql.ToString  (row["PartNumber"          ]);
			// 05/25/2012 Paul.  The QuickBooks product sample has an item with an empty description. 
			// We use the description as the name, so we cannot allow this. 
			if ( Sql.IsEmptyString(this.Description) )
				this.Description = this.Name;
		}

		public override string Insert()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "insert into " + this.QuickBooksTableName + ControlChars.CrLf
				     + "     ( Name                " + ControlChars.CrLf
				     + "     , Type                " + ControlChars.CrLf
				     + "     , Account             " + ControlChars.CrLf
				     + "     , Description         " + ControlChars.CrLf
				     + "     , Price               " + ControlChars.CrLf
				     + "     , IsActive            " + ControlChars.CrLf
				     + "     , PurchaseCost        " + ControlChars.CrLf
				     + "     , QuantityOnHand      " + ControlChars.CrLf
				     + "     , TaxCode             " + ControlChars.CrLf
				     + "     , PartNumber          " + ControlChars.CrLf
				     + "     )" + ControlChars.CrLf
				     + "values" + ControlChars.CrLf
				     + "     ( @Name                " + ControlChars.CrLf
				     + "     , @Type                " + ControlChars.CrLf
				     + "     , @Account             " + ControlChars.CrLf
				     + "     , @Description         " + ControlChars.CrLf
				     + "     , @Price               " + ControlChars.CrLf
				     + "     , @IsActive            " + ControlChars.CrLf
				     + "     , @PurchaseCost        " + ControlChars.CrLf
				     + "     , @QuantityOnHand      " + ControlChars.CrLf
				     + "     , @TaxCode             " + ControlChars.CrLf
				     + "     , @PartNumber          " + ControlChars.CrLf
				     + "     ) " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@Name"                , this.Name                ,   41);
				Sql.AddParameter(qbcmd, "@Type"                , this.Type                );
				Sql.AddParameter(qbcmd, "@Account"             , this.Account             ,   31);
				Sql.AddParameter(qbcmd, "@Description"         , this.Description         , 4095);
				Sql.AddParameter(qbcmd, "@Price"               , this.Price               );
				Sql.AddParameter(qbcmd, "@IsActive"            , this.IsActive            );
				Sql.AddParameter(qbcmd, "@PurchaseCost"        , this.PurchaseCost        );
				Sql.AddParameter(qbcmd, "@QuantityOnHand"      , this.QuantityOnHand      );
				Sql.AddParameter(qbcmd, "@TaxCode"             , this.TaxCode             ,    3);
				Sql.AddParameter(qbcmd, "@PartNumber"          , this.PartNumber          ,   41);
				
				qbcmd.ExecuteNonQuery();
				Hashtable result = qbf.GetLastResult(qbcon);
				this.ID = Sql.ToString(result["id"]);
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
			return this.ID;
		}

		public override void Update()
		{
			using ( IDbCommand qbcmd = qbcon.CreateCommand() )
			{
				string sSQL;
				sSQL = "update " + this.QuickBooksTableName + ControlChars.CrLf
				     + "   set Name                 = @Name                " + ControlChars.CrLf
				     + "     , Type                 = @Type                " + ControlChars.CrLf
				     + "     , Account              = @Account             " + ControlChars.CrLf
				     + "     , Description          = @Description         " + ControlChars.CrLf
				     + "     , Price                = @Price               " + ControlChars.CrLf
				     + "     , IsActive             = @IsActive            " + ControlChars.CrLf
				     + "     , PurchaseCost         = @PurchaseCost        " + ControlChars.CrLf
				     + "     , QuantityOnHand       = @QuantityOnHand      " + ControlChars.CrLf
				     + "     , TaxCode              = @TaxCode             " + ControlChars.CrLf
				     + "     , PartNumber           = @PartNumber          " + ControlChars.CrLf
				     + " where ID                   = @ID                  " + ControlChars.CrLf;
				qbcmd.CommandText = sSQL;
				Sql.AddParameter(qbcmd, "@NAME"                , this.Name                ,   41);
				Sql.AddParameter(qbcmd, "@Type"                , this.Type                );
				Sql.AddParameter(qbcmd, "@Account"             , this.Account             ,   31);
				Sql.AddParameter(qbcmd, "@Description"         , this.Description         , 4095);
				Sql.AddParameter(qbcmd, "@Price"               , this.Price               );
				Sql.AddParameter(qbcmd, "@IsActive"            , this.IsActive            );
				Sql.AddParameter(qbcmd, "@PurchaseCost"        , this.PurchaseCost        );
				Sql.AddParameter(qbcmd, "@QuantityOnHand"      , this.QuantityOnHand      );
				Sql.AddParameter(qbcmd, "@TaxCode"             , this.TaxCode             ,    3);
				Sql.AddParameter(qbcmd, "@PartNumber"          , this.PartNumber          ,   41);
				Sql.AddParameter(qbcmd, "@ID"                  , this.ID                  );
				qbcmd.ExecuteNonQuery();
			}
			// 05/20/2012 Paul.  After updating a QuickBooks record, we need to get the modified date. 
			GetTimeModified();
		}

		public override void FilterQuickBooks(DataView vw)
		{
			vw.RowFilter = "Type not like 'SalesTax%'";
		}

		public override void FilterCRM(IDbCommand cmd)
		{
			Sql.AppendParameter(cmd, Sql.ToString(this.Name), "MFT_PART_NUM");
		}

		public override bool BuildUpdateProcedure(HttpApplicationState Application, ExchangeSession Session, IDbCommand spUpdate, DataRow row, Guid gUSER_ID, Guid gTEAM_ID, Guid gASSIGNED_USER_ID, IDbTransaction trn)
		{
			bool bChanged = this.InitUpdateProcedure(spUpdate, row, gUSER_ID, gTEAM_ID);
			
			Guid gTYPE_ID = Guid.Empty;
			vwProductTypes.RowFilter = "NAME = '" + Sql.EscapeSQL(this.Type) + "'";
			if ( vwProductTypes.Count > 0 )
			{
				gTYPE_ID = Sql.ToGuid(vwProductTypes[0]["ID"]);
			}
			else
			{
				// 05/22/2012 Paul.  If the product type does not existing the CRM, then add it. 
				gTYPE_ID = Guid.NewGuid();
				IDbCommand cmdPRODUCT_TYPES_Update = SqlProcs.cmdPRODUCT_TYPES_Update(trn.Connection);
				cmdPRODUCT_TYPES_Update.Transaction = trn;
				Sql.SetParameter(cmdPRODUCT_TYPES_Update, "@ID"              , gTYPE_ID            );
				Sql.SetParameter(cmdPRODUCT_TYPES_Update, "@MODIFIED_USER_ID", gUSER_ID            );
				Sql.SetParameter(cmdPRODUCT_TYPES_Update, "@NAME"            , this.Type           );
				Sql.SetParameter(cmdPRODUCT_TYPES_Update, "@DESCRIPTION"     , this.Type           );
				Sql.SetParameter(cmdPRODUCT_TYPES_Update, "@LIST_ORDER"      , vwProductTypes.Count);
				cmdPRODUCT_TYPES_Update.ExecuteNonQuery();
				
				DataRow rowPRODUCT_TYPE = vwProductTypes.Table.NewRow();
				rowPRODUCT_TYPE["ID"  ] = gTYPE_ID ;
				rowPRODUCT_TYPE["NAME"] = this.Type;
				vwProductTypes.Table.Rows.Add(rowPRODUCT_TYPE);
			}
			
			foreach(IDbDataParameter par in spUpdate.Parameters)
			{
				Security.ACL_FIELD_ACCESS acl = new Security.ACL_FIELD_ACCESS(Security.ACL_FIELD_ACCESS.FULL_ACCESS, Guid.Empty);
				// 03/27/2010 Paul.  The ParameterName will start with @, so we need to remove it. 
				string sColumnName = Sql.ExtractDbName(spUpdate, par.ParameterName).ToUpper();
				if ( acl.IsWriteable() )
				{
					try
					{
						// 01/24/2012 Paul.  Only update the record if it has changed.  This is to prevent an endless loop caused by sync operations. 
						object oValue = null;
						switch ( sColumnName )
						{
							case "MFT_PART_NUM"      :  oValue = Sql.ToDBString(this.Name                );  break;
							case "TYPE_ID"           :  oValue = Sql.ToDBGuid  (     gTYPE_ID            );  break;
							case "QUICKBOOKS_ACCOUNT":  oValue = Sql.ToDBString(this.Account             );  break;
							case "NAME"              :  oValue = Sql.ToDBString(this.Description         );  break;
							case "LIST_PRICE"        :  oValue = Sql.ToDBDouble(this.Price               );  break;
							case "DISCOUNT_PRICE"    :  oValue = Sql.ToDBDouble(this.Price               );  break;
							case "STATUS"            :  oValue = Sql.ToDBString(this.IsActive ? "Available" : "Unavailable");  break;
							case "COST_PRICE"        :  oValue = Sql.ToDBDouble(this.PurchaseCost        );  break;
							case "QUANTITY"          :  oValue = Sql.ToDBDouble(this.QuantityOnHand      );  break;
							case "TAX_CLASS"         :  oValue = Sql.ToDBString(this.TaxCode == "Tax" ? "Taxable" : "Non-Taxable");  break;
							case "VENDOR_PART_NUM"   :  oValue = Sql.ToDBString(this.PartNumber          );  break;
							case "MODIFIED_USER_ID"  :  oValue = gUSER_ID;  break;
						}
						// 01/24/2012 Paul.  Only set the parameter value if the value is being set. 
						if ( oValue != null )
						{
							if ( !bChanged )
							{
								switch ( sColumnName )
								{
									case "MFT_PART_NUM"      :  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "NAME"              :  bChanged = ParameterChanged(par, oValue, 4095);  break;
									case "QUICKBOOKS_ACCOUNT":  bChanged = ParameterChanged(par, oValue,   31);  break;
									case "TAX_CLASS"         :  bChanged = ParameterChanged(par, oValue,    3);  break;
									case "VENDOR_PART_NUM"   :  bChanged = ParameterChanged(par, oValue,   31);  break;
									default                  :  bChanged = ParameterChanged(par, oValue);  break;
								}
							}
							par.Value = oValue;
						}
					}
					catch
					{
						// 03/27/2010 Paul.  Some fields are not available.  Lets just ignore them. 
					}
				}
			}
			return bChanged;
		}
	}
}
