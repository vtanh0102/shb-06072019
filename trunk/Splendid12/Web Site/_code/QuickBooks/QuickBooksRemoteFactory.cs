/**
 * Copyright (C) 2012-2014 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Collections;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for QuickBooksRemoteFactory.
	/// </summary>
	public class QuickBooksRemoteFactory : QuickBooksClientFactory
	{
		public QuickBooksRemoteFactory(string sConnectionString)
			: base( sConnectionString
			      , "System.Data.RSSBus.QuickBooks"
			      , "System.Data.RSSBus.QuickBooks.QuickBooksConnection"    
			      , "System.Data.RSSBus.QuickBooks.QuickBooksCommand"       
			      , "System.Data.RSSBus.QuickBooks.QuickBooksDataAdapter"   
			      , "System.Data.RSSBus.QuickBooks.QuickBooksParameter"     
			      , "System.Data.RSSBus.QuickBooks.QuickBooksCommandBuilder"
			      )
		{
		}
	}
}
