/**
 * Copyright (C) 2013 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Xml;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Diagnostics;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Security.Cryptography.X509Certificates;

using Avaya.ApplicationEnablement.DMCC;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for AvayaManager.
	/// </summary>
	public class AvayaManager : IDisposable
	{
		protected class SplendidDevice
		{
			private HttpContext Context;
			public Avaya.ApplicationEnablement.DMCC.Device Device { get; set; }
			public string Extension         { get; set; }
			public string DeviceID          { get; set; }
			public string PhoneMonitorID    { get; set; }
			public string CallCtrlMonitorID { get; set; }
			public string CallInfoMonitorID { get; set; }

			public SplendidDevice(HttpContext Context, Avaya.ApplicationEnablement.DMCC.Device device, string sExtension)
			{
				this.Context           = Context     ;
				this.Device            = device      ;
				this.Extension         = sExtension  ;
				this.DeviceID          = String.Empty;
				this.PhoneMonitorID    = String.Empty;
				this.CallCtrlMonitorID = String.Empty;
				this.CallInfoMonitorID = String.Empty;
			}

			public void Release()
			{
				try
				{
					if ( !Sql.IsEmptyString(this.PhoneMonitorID) )
					{
						this.Device.getPhone.StopMonitor(this.PhoneMonitorID, this);
						System.Threading.Thread.Sleep(100);
					}
					this.Device.getPhone.UnregisterTerminal(null);
					System.Threading.Thread.Sleep(100);
					this.Device.ReleaseDeviceId(null);
				}
				catch(Exception ex)
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
		}

		protected class SplendidConnection
		{
			public string   CallID           { get; set; }
			public string   CallingDeviceID  { get; set; }
			public string   CallingExtension { get; set; }
			public string   CalledDeviceID   { get; set; }
			public string   CalledExtension  { get; set; }
		
			public bool     Connected        { get; set; }
			public DateTime DateReceived     { get; set; }
			public Guid     CALL_ID          { get; set; }
			public Guid     PARENT_ID        { get; set; }
			public string   PARENT_TYPE      { get; set; }

			public SplendidConnection(ThirdPartyCallController.DeliveredEventEventArgs Event)
			{
				this.CallID           = Event.getConnectionId.getCallId   ;
				this.CallingDeviceID  = Event.getCallingDeviceId          ;
				this.CallingExtension = GetExtension(this.CallingDeviceID);
				this.CalledDeviceID   = Event.getCalledDeviceId           ;
				this.CalledExtension  = GetExtension(this.CalledDeviceID );
				this.Connected        = false       ;
				this.DateReceived     = DateTime.Now;
				this.PARENT_ID        = Guid.Empty  ;
				this.PARENT_TYPE      = String.Empty;
			}

			public SplendidConnection(ThirdPartyCallController.EstablishedEventArgs Event)
			{
				this.CallID           = Event.getEstablishedConnectionId.getCallId;
				this.CallingDeviceID  = Event.getCallingDeviceId          ;
				this.CallingExtension = GetExtension(this.CallingDeviceID);
				this.CalledDeviceID   = Event.getCalledDeviceId           ;
				this.CalledExtension  = GetExtension(this.CalledDeviceID );
				this.Connected        = true        ;
				this.DateReceived     = DateTime.Now;
				this.PARENT_ID        = Guid.Empty  ;
				this.PARENT_TYPE      = String.Empty;
			}

			public static string GetExtension(string sDeviceID)
			{
				string sExtension = String.Empty;
				if ( !Sql.IsEmptyString(sDeviceID) )
				{
					string[] arr = sDeviceID.Split(':');
					sExtension = arr[0];
				}
				return sExtension;
			}
		}

		protected class SplendidCallData
		{
			public string   CallingDeviceID  { get; set; }
			public string   CalledDeviceID   { get; set; }
			public string   EXTENSION        { get; set; }
			public string   PHONE            { get; set; }
			public Guid     PARENT_ID        { get; set; }
			public string   PARENT_TYPE      { get; set; }

			public SplendidCallData(string sEXTENSION, string sPHONE, Guid gPARENT_ID, string sPARENT_TYPE, string sCallingDeviceID)
			{
				this.CallingDeviceID  = sCallingDeviceID;
				this.CalledDeviceID   = String.Empty;
				this.EXTENSION        = sEXTENSION  ;
				this.PHONE            = sPHONE      ;
				this.PARENT_ID        = gPARENT_ID  ;
				this.PARENT_TYPE      = sPARENT_TYPE;
			}
		}

		#region Properties
		private HttpContext           Context                 ;
		private ServiceProvider       _avayaConnection        ;
		private bool                  _bVerboseStatus         ;
		private string                _sSwitchName            ;
		private bool                  _bLogIncomingMissedCalls;
		private bool                  _bLogOutgoingMissedCalls;
		private bool                  _bLogCallDetails        ;
		private string                _sCallerIdFormat        ;
		private IHubConnectionContext Clients { get; set; }
		private Dictionary<string, SplendidDevice>    _devices;
		private Dictionary<string, SplendidConnection> _outgoingCalls;
		private Dictionary<string, SplendidConnection> _incomingCalls;

		// Singleton instance
		private static AvayaManager _instance = null;

		public static AvayaManager Instance
		{
			get { return _instance; }
		}
		#endregion

		#region Initialization
		public static void InitApp(HttpContext Context)
		{
			_instance = new AvayaManager(Context, GlobalHost.ConnectionManager.GetHubContext<AvayaManagerHub>().Clients);
			System.Threading.Thread t = new System.Threading.Thread(_instance.Start);
			t.Start();
		}

		public static void RegisterScripts(HttpContext Context, ScriptManager mgrAjax)
		{
			if ( mgrAjax != null )
			{
				HttpApplicationState Application = Context.Application;
				string sAvayaHost = Sql.ToString(Application["CONFIG.Avaya.Host"]);
				if ( !Sql.IsEmptyString(sAvayaHost) )
				{
					string sAvayaUsername = Sql.ToString(Application["CONFIG.Avaya.UserName"]);
					string sAvayaPassword = Sql.ToString(Application["CONFIG.Avaya.Password"]);
					if ( !Sql.IsEmptyString(sAvayaUsername) && !Sql.IsEmptyString(sAvayaPassword) )
					{
						// 12/03/2013 Paul.  Only include Avaya code if enabled. 
						if ( Utils.CachedFileExists(Context, "~/Include/javascript/AvayaManagerHubJS.aspx") )
						{
							SignalRUtils.RegisterSignalR(mgrAjax);
							// 12/03/2013 Paul.  IE is caching during development.  Add the version to the URL. 
							// 12/03/2013 Paul.  Put the labels in the javascript file because they will only change based on the language. 
							ScriptReference scrAvayaManagerHub = new ScriptReference("~/Include/javascript/AvayaManagerHubJS.aspx?" + Sql.ToString(Application["SplendidVersion"]) + "_" + Sql.ToString(Context.Session["USER_SETTINGS/CULTURE"]));
							if ( !mgrAjax.Scripts.Contains(scrAvayaManagerHub) )
								mgrAjax.Scripts.Add(scrAvayaManagerHub);
						}
					}
				}
			}
		}

		private void InitializeCallbacks()
		{
			this._avayaConnection.OnServerConnectionDownEvent                          += new ServerConnectionDownEventHandler                (getThirdPartyCallController_ServerConnectionDownEvent);
			this._avayaConnection.getThirdPartyCallController.OnStartMonitorResponse   += new ThirdPartyCallControlStartMonitorResponseHandler(getThirdPartyCallController_OnStartMonitorResponse   );
			this._avayaConnection.getThirdPartyCallController.OnStopMonitorResponse    += new ThirdPartyCallControlStopMonitorResponseHandler (getThirdPartyCallController_OnStopMonitorResponse    );
			this._avayaConnection.getThirdPartyCallController.OnSnapshotDeviceResponse += new SnapshotDeviceResponseHandler                   (getThirdPartyCallController_OnSnapshotDeviceResponse );
			this._avayaConnection.getThirdPartyCallController.OnFailedEvent            += new FailedEventHandler                              (getThirdPartyCallController_OnFailedEvent            );
			this._avayaConnection.getThirdPartyCallController.OnDeliveredEvent         += new DeliveredEventHandler                           (getThirdPartyCallController_OnDeliveredEvent         );
			this._avayaConnection.getThirdPartyCallController.OnEstablishedEvent       += new EstablishedEventHandler                         (getThirdPartyCallController_EstablishedEvent         );
			this._avayaConnection.getThirdPartyCallController.OnConnectionClearedEvent += new ConnectionClearedEventHandler                   (getThirdPartyCallController_ConnectionClearedEvent   );
			this._avayaConnection.getThirdPartyCallController.OnMakeCallResponse       += new MakeCallResponseHandler                         (getThirdPartyCallController_MakeCallResponse         );
			this._avayaConnection.GetCallInformationLink.OnStartMonitorResponse        += new StartMonitorResponseHandler                     (callInfoLink_OnStartMonitorResponse                  );
			this._avayaConnection.GetCallInformationLink.OnStopMonitorResponse         += new StopMonitorResponseHandler                      (callInfoLink_OnStopMonitorResponse                   );
			this._avayaConnection.GetCallInformationLink.OnLinkDownEvent               += new LinkDownEventHandler                            (callInfoLink_OnLinkDownEvent                         );
			this._avayaConnection.GetCallInformationLink.OnLinkUpEvent                 += new LinkUpEventHandler                              (callInfoLink_OnLinkUpEvent                           );
			this._avayaConnection.getXmlProcessor.OnXmlMessageReceivedEvent            += new XmlMessageReceivedEventHandler                  (xmlProcessor_OnXmlMessageReceivedEvent               );
			this._avayaConnection.getXmlProcessor.OnXmlMessageSentEvent                += new XmlMessageSentEventHandler                      (xmlProcessor_OnXmlMessageSentEvent                   );
		}

		private void Start()
		{
			HttpApplicationState Application = this.Context.Application;
			string sAvayaHost     = Sql.ToString (Application["CONFIG.Avaya.Host"         ]);
			int    nAvayaPort     = Sql.ToInteger(Application["CONFIG.Avaya.Port"         ]);
			string sAvayaUsername = Sql.ToString (Application["CONFIG.Avaya.UserName"     ]);
			string sAvayaPassword = Sql.ToString (Application["CONFIG.Avaya.Password"     ]);
			bool   bSecureSocket  = Sql.ToBoolean(Application["CONFIG.Avaya.SecureSocket" ]);
			int    nAutoKeepAlive = Sql.ToInteger(Application["CONFIG.Avaya.AutoKeepAlive"]);
			if ( !Sql.IsEmptyString(sAvayaHost) && nAvayaPort > 0 && !Sql.IsEmptyString(sAvayaUsername) && !Sql.IsEmptyString(sAvayaPassword) )
			{
				try
				{
					if ( !Sql.IsEmptyString(sAvayaPassword) )
					{
						Guid gINBOUND_EMAIL_KEY = Sql.ToGuid(Application["CONFIG.InboundEmailKey"]);
						Guid gINBOUND_EMAIL_IV  = Sql.ToGuid(Application["CONFIG.InboundEmailIV" ]);
						sAvayaPassword = Security.DecryptPassword(sAvayaPassword, gINBOUND_EMAIL_KEY, gINBOUND_EMAIL_IV);
					}
					if ( nAutoKeepAlive == 0 )
						nAutoKeepAlive = 15000;
					
					this._avayaConnection         = new ServiceProvider();
					this._bVerboseStatus          = Sql.ToBoolean(Application["CONFIG.Avaya.VerboseStatus"         ]);
					this._sSwitchName             = Sql.ToString (Application["CONFIG.Avaya.SwitchName"            ]).ToUpper();
					this._bLogIncomingMissedCalls = Sql.ToBoolean(Application["CONFIG.Avaya.LogIncomingMissedCalls"]);
					this._bLogOutgoingMissedCalls = Sql.ToBoolean(Application["CONFIG.Avaya.LogOutgoingMissedCalls"]);
					this._bLogCallDetails         = Sql.ToBoolean(Application["CONFIG.Avaya.LogCallDetails"        ]);
					this._sCallerIdFormat         = Sql.ToString (Application["CONFIG.Avaya.CallerIdFormat"        ]);
					if ( Sql.IsEmptyString(this._sCallerIdFormat) )
						this._sCallerIdFormat = "{0} {1}";
					try
					{
						bool   bStartupDone  = false;
						string sStartupError = "Failed to respond to startup.";
						this._avayaConnection.OnStartApplicationSessionResponse += (object sender, ServiceProvider.StartApplicationSessionResponseArgs e) =>
						{
							Debug.WriteLine("OnStartApplicationSessionResponse");
							bStartupDone = true;
							sStartupError = e.getError;
							if ( !Sql.IsEmptyString(sStartupError) )
								sStartupError = GetError(sStartupError, "http://www.ecma-international.org/standards/ecma-354/appl_session");
						};
						InitializeCallbacks();
						
						this._avayaConnection.StartAutoKeepAlive(nAutoKeepAlive);
						this._avayaConnection.StartApplicationSession(sAvayaHost, nAvayaPort, "SplendidCRM", sAvayaUsername, sAvayaPassword, 50, 300, "http://www.ecma-international.org/standards/ecma-323/csta/ed3/priv4", bSecureSocket, null, true, true, new System.Net.Security.LocalCertificateSelectionCallback(certificateCallback));
						for ( int n = 0; n < 100 && !bStartupDone ; n++ )
						{
							System.Threading.Thread.Sleep(300);
						}
						if ( !Sql.IsEmptyString(sStartupError) )
							throw(new Exception(sStartupError));
					}
					catch
					{
						if ( _avayaConnection != null )
						{
							this._avayaConnection.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "Application failed to start");
							this._avayaConnection = null;
						}
						throw;
					}
					Debug.WriteLine("Avaya login successful. " + DateTime.Now.ToString());
					MonitorDevices();
				}
#if DEBUG
				catch(Exception ex)
				{
					Debug.WriteLine("Avaya login failed: " + ex.Message);
				}
#else
				catch
				{
				}
#endif
			}
		}
		#endregion

		#region Login
		public void Login()
		{
			if ( _avayaConnection == null )
			{
				this._devices       = new Dictionary<string, SplendidDevice>();
				this._outgoingCalls = new Dictionary<string, SplendidConnection>();
				this._incomingCalls = new Dictionary<string, SplendidConnection>();
				
				HttpApplicationState Application = this.Context.Application;
				string sAvayaHost     = Sql.ToString (Application["CONFIG.Avaya.Host"         ]);
				int    nAvayaPort     = Sql.ToInteger(Application["CONFIG.Avaya.Port"         ]);
				string sAvayaUsername = Sql.ToString (Application["CONFIG.Avaya.UserName"     ]);
				string sAvayaPassword = Sql.ToString (Application["CONFIG.Avaya.Password"     ]);
				bool   bSecureSocket  = Sql.ToBoolean(Application["CONFIG.Avaya.SecureSocket" ]);
				int    nAutoKeepAlive = Sql.ToInteger(Application["CONFIG.Avaya.AutoKeepAlive"]);
				if ( Sql.IsEmptyString(sAvayaHost    ) ) throw(new Exception("Avaya host not specified."));
				if ( nAvayaPort == 0                   ) throw(new Exception("Avaya port not specified."));
				if ( Sql.IsEmptyString(sAvayaUsername) ) throw(new Exception("Avaya username not specified."));
				if ( Sql.IsEmptyString(sAvayaPassword) ) throw(new Exception("Avaya password not specified."));
				
				if ( !Sql.IsEmptyString(sAvayaPassword) )
				{
					Guid gINBOUND_EMAIL_KEY = Sql.ToGuid(Application["CONFIG.InboundEmailKey"]);
					Guid gINBOUND_EMAIL_IV  = Sql.ToGuid(Application["CONFIG.InboundEmailIV" ]);
					sAvayaPassword = Security.DecryptPassword(sAvayaPassword, gINBOUND_EMAIL_KEY, gINBOUND_EMAIL_IV);
				}
				if ( nAutoKeepAlive == 0 )
					nAutoKeepAlive = 15000;
				
				this._avayaConnection         = new ServiceProvider();
				this._bVerboseStatus          = Sql.ToBoolean(Application["CONFIG.Avaya.VerboseStatus"         ]);
				this._sSwitchName             = Sql.ToString (Application["CONFIG.Avaya.SwitchName"            ]).ToUpper();
				this._bLogIncomingMissedCalls = Sql.ToBoolean(Application["CONFIG.Avaya.LogIncomingMissedCalls"]);
				this._bLogOutgoingMissedCalls = Sql.ToBoolean(Application["CONFIG.Avaya.LogOutgoingMissedCalls"]);
				this._bLogCallDetails         = Sql.ToBoolean(Application["CONFIG.Avaya.LogCallDetails"        ]);
				this._sCallerIdFormat         = Sql.ToString (Application["CONFIG.Avaya.CallerIdFormat"        ]);
				if ( Sql.IsEmptyString(this._sCallerIdFormat) )
					this._sCallerIdFormat = "{0} {1}";
				try
				{
					bool   bStartupDone  = false;
					string sStartupError = "Failed to respond to startup.";
					this._avayaConnection.OnStartApplicationSessionResponse += (object sender, ServiceProvider.StartApplicationSessionResponseArgs e) =>
					{
						Debug.WriteLine("OnStartApplicationSessionResponse");
						bStartupDone = true;
						sStartupError = e.getError;
						if ( !Sql.IsEmptyString(sStartupError) )
							sStartupError = GetError(sStartupError, "http://www.ecma-international.org/standards/ecma-354/appl_session");
					};
					InitializeCallbacks();
					
					this._avayaConnection.StartAutoKeepAlive(nAutoKeepAlive);
					this._avayaConnection.StartApplicationSession(sAvayaHost, nAvayaPort, "SplendidCRM", sAvayaUsername, sAvayaPassword, 50, 300, "http://www.ecma-international.org/standards/ecma-323/csta/ed3/priv4", bSecureSocket, null, false, true, new System.Net.Security.LocalCertificateSelectionCallback(certificateCallback));
					for ( int n = 0; n < 100 && !bStartupDone ; n++ )
					{
						System.Threading.Thread.Sleep(300);
					}
					if ( !Sql.IsEmptyString(sStartupError) )
						throw(new Exception(sStartupError));
				}
				catch
				{
					if ( _avayaConnection != null )
					{
						this._avayaConnection.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "Application failed to start");
						this._avayaConnection = null;
					}
					throw;
				}
				Debug.WriteLine("Avaya login successful. " + DateTime.Now.ToString());
				MonitorDevices();
			}
		}

		public void Logout()
		{
			try
			{
				// 12/05/2013 Paul.  Release existing devices. 
				foreach ( string sExtension in this._devices.Keys )
				{
					SplendidDevice deviceBeingMonitored = this._devices[sExtension];
					this._devices.Remove(sExtension);
					deviceBeingMonitored.Release();
				}
				if ( _avayaConnection != null )
				{
					this._avayaConnection.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "Logout");
					this._avayaConnection = null;
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
			this._devices       = new Dictionary<string, SplendidDevice>();
			this._outgoingCalls = new Dictionary<string, SplendidConnection>();
			this._incomingCalls = new Dictionary<string, SplendidConnection>();
		}

		private string GetError(string sXml, string sNamespace)
		{
			string sError = String.Empty;
			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			xml.LoadXml(sXml);
			if ( Sql.IsEmptyString(sNamespace) )
			{
				if ( xml.DocumentElement.ChildNodes.Count > 0 )
				{
					XmlNode xErrorCode = xml.DocumentElement.ChildNodes[0];
					if ( xErrorCode.Name == "errorCode" && xErrorCode.ChildNodes.Count > 0 )
					{
						XmlNode xApplError = xErrorCode.ChildNodes[0];
						if ( xApplError.Name == "applError" )
						{
							sError = Sql.ToString(xApplError.InnerText);
						}
					}
					else if ( xml.DocumentElement.Name == "CSTAErrorCode" && xErrorCode.ChildNodes.Count > 0 )
					{
						//<CSTAErrorCode xmlns="http://www.ecma-international.org/standards/ecma-323/csta/ed3">
						//	<stateIncompatibility>invalidDeviceState</stateIncompatibility>
						//</CSTAErrorCode>
						//<CSTAErrorCode xmlns="http://www.ecma-international.org/standards/ecma-323/csta/ed3">
						//	<systemResourceAvailibility>resourceBusy</systemResourceAvailibility>
						//</CSTAErrorCode>
						XmlNode xApplError = xErrorCode.ChildNodes[0];
						sError = Sql.ToString(xApplError.Name + "." + xApplError.InnerText);
					}
				}
			}
			else
			{
				XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
				nsmgr.AddNamespace("defaultns", sNamespace );
				XmlNode xApplError = xml.DocumentElement.SelectSingleNode("defaultns:errorCode/defaultns:applError", nsmgr);
				if ( xApplError != null )
				{
					sError = Sql.ToString(xApplError.InnerText);
				}
			}
			return sError;
		}

		public string ValidateLogin(string sAvayaHost, int nAvayaPort, string sAvayaUsername, string sAvayaPassword, bool bSecureSocket)
		{
			string sResult = String.Empty;
			try
			{
				if ( Sql.IsEmptyString(sAvayaHost    ) ) throw(new Exception("Avaya host not specified."));
				if ( nAvayaPort == 0                   ) throw(new Exception("Avaya port not specified."));
				if ( Sql.IsEmptyString(sAvayaUsername) ) throw(new Exception("Avaya username not specified."));
				if ( Sql.IsEmptyString(sAvayaPassword) ) throw(new Exception("Avaya password not specified."));
				if ( !Sql.IsEmptyString(sAvayaPassword) )
				{
					Guid gINBOUND_EMAIL_KEY = Sql.ToGuid(Context.Application["CONFIG.InboundEmailKey"]);
					Guid gINBOUND_EMAIL_IV  = Sql.ToGuid(Context.Application["CONFIG.InboundEmailIV" ]);
					sAvayaPassword = Security.DecryptPassword(sAvayaPassword, gINBOUND_EMAIL_KEY, gINBOUND_EMAIL_IV);
				}
				
				ServiceProvider avayaConnection = new ServiceProvider();
				try
				{
					bool   bStartupDone  = false;
					string sStartupError = "Failed to respond to startup.";
					XmlProcessor xmlProcessor = avayaConnection.getXmlProcessor;
					xmlProcessor.OnXmlMessageReceivedEvent += new XmlMessageReceivedEventHandler(xmlProcessor_OnXmlMessageReceivedEvent);
					xmlProcessor.OnXmlMessageSentEvent     += new XmlMessageSentEventHandler    (xmlProcessor_OnXmlMessageSentEvent    );
					
					avayaConnection.OnStartApplicationSessionResponse += (object sender, ServiceProvider.StartApplicationSessionResponseArgs e) =>
					{
						Debug.WriteLine("OnStartApplicationSessionResponse");
						bStartupDone = true;
						sStartupError = e.getError;
						if ( !Sql.IsEmptyString(sStartupError) )
							sStartupError = GetError(sStartupError, "http://www.ecma-international.org/standards/ecma-354/appl_session");
					};
					avayaConnection.StartApplicationSession(sAvayaHost, nAvayaPort, "SplendidCRM", sAvayaUsername, sAvayaPassword, 50, 300, "http://www.ecma-international.org/standards/ecma-323/csta/ed3/priv4", bSecureSocket, null, true, true, new System.Net.Security.LocalCertificateSelectionCallback(certificateCallback));
					for ( int n = 0; n < 100 && !bStartupDone ; n++ )
					{
						System.Threading.Thread.Sleep(300);
					}
					if ( !Sql.IsEmptyString(sStartupError) )
						throw(new Exception(sStartupError));
				}
				finally
				{
					if ( avayaConnection != null )
					{
						avayaConnection.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "ValidateLogin");
						avayaConnection = null;
					}
				}
			}
			catch(Exception ex)
			{
				sResult = ex.Message;
			}
			return sResult;
		}
		#endregion

		public void MonitorDevices()
		{
			if ( this._avayaConnection != null )
			{
				Debug.WriteLine("Avaya MonitorDevices " + DateTime.Now.ToString());
				try
				{
					string sSwitchName = this._sSwitchName;
					string sSwitchIpInterface = String.Empty;
					List<string> lstExtensions = new List<string>();
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						string sSQL;
						sSQL = "select EXTENSION            " + ControlChars.CrLf
						     + "  from vwUSERS_Login        " + ControlChars.CrLf
						     + " where EXTENSION is not null" + ControlChars.CrLf
						     + " order by EXTENSION         " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									foreach ( DataRow row in dt.Rows )
									{
										string sEXTENSION = Sql.ToString(row["EXTENSION"]);
										if ( !Sql.IsEmptyString(sEXTENSION) )
										{
											lstExtensions.Add(sEXTENSION);
											if ( !this._devices.ContainsKey(sEXTENSION) )
											{
												SplendidDevice deviceBeingMonitored = new SplendidDevice(this.Context, this._avayaConnection.GetNewDevice(), sEXTENSION);
												this._devices.Add(sEXTENSION, deviceBeingMonitored);
												deviceBeingMonitored.Device.OnGetDeviceIdResponse                += new GetDeviceIdResponseHandler      (Terminal_OnGetDeviceIdResponse      );
												deviceBeingMonitored.Device.getPhone.OnStartMonitorResponse      += new PhoneStartMonitorResponseHandler(Terminal_OnStartMonitorResponse     );
												deviceBeingMonitored.Device.getPhone.OnStopMonitorResponse       += new PhoneStopMonitorResponseHandler (Terminal_OnStopMonitorResponse      );
												deviceBeingMonitored.Device.getPhone.OnRegisterTerminalResponse  += new RegisterTerminalResponseHandler (Terminal_OnRegisterTerminalResponse );
												deviceBeingMonitored.Device.getPhone.OnTerminalUnregisteredEvent += new TerminalUnregisteredEventHandler(Terminal_OnTerminalUnregisteredEvent);
												//deviceBeingMonitored.getPhone.OnDisplayUpdatedEvent       += new DisplayUpdatedEventHandler      (Terminal_OnDisplayUpdatedEvent      );
												//deviceBeingMonitored.OnReleaseDeviceIdResponse            += new ReleaseDeviceIdResponseHandler  (Terminal_OnReleaseDeviceIdResponse  );
												deviceBeingMonitored.Device.GetDeviceId(sEXTENSION, sSwitchName, sSwitchIpInterface, false, deviceBeingMonitored);
											}
										}
									}
								}
							}
						}
					}
					// 12/05/2013 Paul.  Remove any unused extensions. 
					foreach ( string sExtension in this._devices.Keys )
					{
						if ( !lstExtensions.Contains(sExtension) )
						{
							SplendidDevice deviceBeingMonitored = this._devices[sExtension];
							this._devices.Remove(sExtension);
							deviceBeingMonitored.Release();
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
		}

		// ServiceProvider_OnStartApplicationSessionResponse
		// Terminal_OnGetDeviceIdResponse
		// getThirdPartyCallController_OnStartMonitorResponse
		// callInfoLink_OnStartMonitorResponse
		// Terminal_OnStartMonitorResponse
		// Terminal_OnRegisterTerminalResponse
		// getThirdPartyCallController_OnSnapshotDeviceResponse
		#region Start Monitor Events
		// 12/05/2013 Paul.  1. First event is after StartApplicationSession as we get the DeviceID for each extension. 
		private void Terminal_OnGetDeviceIdResponse(object sender, Device.GetDeviceIdResponseArgs e)
		{
			Debug.WriteLine("Terminal_OnGetDeviceIdResponse(" + e.getDevice.getExtension + ", " + e.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "GetDeviceID: " + GetError(e.getError, ""));
					this._devices.Remove(e.getDevice.getExtension);
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.DeviceID = e.getDevice.getDeviceIdAsString;
					
						ThirdPartyCallController.ThirdPartyCallControlEvents CallControlEvents = new ThirdPartyCallController.ThirdPartyCallControlEvents(true);
						// 12/05/2013 Paul.  Make sure to pass the SplendidDevice object so that we can save the CallCtrlMonitorID. 
						this._avayaConnection.getThirdPartyCallController.StartMonitor(deviceBeingMonitored.DeviceID, CallControlEvents, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		// 12/05/2013 Paul.  2. Second event is after GetDeviceID as we start monitoring at the CallController level. 
		private void getThirdPartyCallController_OnStartMonitorResponse(object Sender, ThirdPartyCallController.StartMonitorResponseArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_OnStartMonitorResponse(" + e.getDeviceId + ", " + e.getMonitorId + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "CallController_StartMonitor: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.CallCtrlMonitorID = e.getMonitorId;
						CallInformationLink.CallInformationEvents callInfoEvents = new CallInformationLink.CallInformationEvents(true);
						// 12/05/2013 Paul.  Make sure to pass the SplendidDevice object so that we can save the CallInfoMonitorID. 
						this._avayaConnection.GetCallInformationLink.StartMonitor(callInfoEvents, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		// 12/05/2013 Paul.  3. Third event is after CallController Start Monitor as we start monitoring at the CallInformationLink level. 
		private void callInfoLink_OnStartMonitorResponse(object Sender, CallInformationLink.StartMonitorResponseArgs e)
		{
			Debug.WriteLine("callInfoLink_OnStartMonitorResponse(" + e.getMonitorId + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "StartMonitor: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.CallInfoMonitorID = e.getMonitorId;

						Phone.PhoneEvents PhoneMonitorEvents = new Phone.PhoneEvents(true);
						// 12/05/2013 Paul.  Make sure to pass the SplendidDevice object so that we can save the PhoneMonitorID. 
						deviceBeingMonitored.Device.getPhone.StartMonitor(PhoneMonitorEvents, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		// 12/05/2013 Paul.  4. Fourth event is after CallInformationLink Start Monitor as we start monitoring at the Terminal/Phone level. 
		private void Terminal_OnStartMonitorResponse(object Sender, Phone.StartMonitorResponseArgs e)
		{
			Debug.WriteLine("Terminal_OnStartMonitorResponse(" + e.getMonitorId + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "StartMonitor: " + GetError(e.getError, ""));
					this._devices.Remove(e.getPhone.getDevice.getExtension);
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.PhoneMonitorID = e.getMonitorId;
						
						// 12/05/2013 Paul.  We should not need to RegisterTerminal because we are not interested in first party call control. 
						// 12/06/2013 Paul.  When the SnapshotDevice fails, we get a CSTAErrorCode response. 
						this._avayaConnection.getThirdPartyCallController.SnapshotDevice(deviceBeingMonitored.Extension, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		// 12/05/2013 Paul.  5. Fifth event is after SnapshotDevice. 
		private void getThirdPartyCallController_OnSnapshotDeviceResponse(object sender, ThirdPartyCallController.SnapshotDeviceResponseArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_OnSnapshotDeviceResponse(" + e.getDeviceIdAsString + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "SnapshotDevice: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					// This list will contain the list of all the devices information which are present on the phone display
					List<ThirdPartyCallController.SnapshotDeviceResponseArgs.DeviceInfo> list = e.getDeviceInfoList;
					if ( list != null )
					{
						// We need to check each of the lines of the phone
						for ( int loopCount = 0; loopCount < list.Count; loopCount++ )
						{
							// To extract information from the list, we need to convert them into deviceinfo object
							ThirdPartyCallController.SnapshotDeviceResponseArgs.DeviceInfo ss = list[loopCount] as ThirdPartyCallController.SnapshotDeviceResponseArgs.DeviceInfo;
							if ( ss.getLocalConnectionStateList.Count == 2 )
							{
								// If monitored party is in Connected callState, Ringing callState, Alerting callState or Held callState, set the snapflag to true
								if ((ss.getLocalConnectionStateList[0].Equals("connected") && ss.getLocalConnectionStateList[1].Equals("connected")) 
								||  (ss.getLocalConnectionStateList[0].Equals("connected") && ss.getLocalConnectionStateList[1].Equals("alerting" )) 
								||  (ss.getLocalConnectionStateList[0].Equals("alerting" ) && ss.getLocalConnectionStateList[1].Equals("connected")) 
								||  (ss.getLocalConnectionStateList[0].Equals("hold"     ) && ss.getLocalConnectionStateList[1].Equals("connected")))
								{
									//snapShot = true;
									//snapShotCallID = ss.getConnectionId;
									//mainCallID = ss.getConnectionId.getCallId.ToString();
									break;
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		#endregion

		#region Stop Monitor Events
		private void Terminal_OnStopMonitorResponse(object Sender, Phone.PhoneStopMonitorResponseArgs e)
		{
			Debug.WriteLine("Terminal_OnStopMonitorResponse(" + e.getInvokeId + ", " + e.getPhone.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "StopMonitor: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.PhoneMonitorID = String.Empty;
						this._avayaConnection.GetCallInformationLink.StopMonitor(deviceBeingMonitored.CallInfoMonitorID, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void callInfoLink_OnStopMonitorResponse(object Sender, CallInformationLink.StopMonitorResponseArgs e)
		{
			Debug.WriteLine("callInfoLink_OnStopMonitorResponse(" + e.getInvokeId + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "StopMonitor: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.CallInfoMonitorID = String.Empty;
						this._avayaConnection.getThirdPartyCallController.StopMonitor(deviceBeingMonitored.CallCtrlMonitorID, deviceBeingMonitored);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void getThirdPartyCallController_OnStopMonitorResponse(object Sender, ThirdPartyCallController.ThirdPartyCallControlStopMonitorResponseArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_OnStopMonitorResponse(" + e.getInvokeId + ")\n");
			try 
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "CallController_StopMonitor: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidDevice deviceBeingMonitored = e.getUserState as SplendidDevice;
					if ( deviceBeingMonitored != null )
					{
						deviceBeingMonitored.CallCtrlMonitorID = String.Empty;
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		#endregion

		#region ThirdPartyCallController
		private void getThirdPartyCallController_ServerConnectionDownEvent(object Sender, ServiceProvider.ServerConnectionDownEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_ServerConnectionDownEvent");
			try
			{
				string sAvayaHost     = Sql.ToString (Context.Application["CONFIG.Avaya.Host"    ]);
				string sAvayaUsername = Sql.ToString (Context.Application["CONFIG.Avaya.UserName"]);
				
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append(String.Format("ServerConnectionDown - {0}@{1}, Service Provider = {2}, Code = {3}, Reason = {4}", sAvayaHost, sAvayaUsername, e.getServiceProvider, e.getCode, e.getReason));

				Debug.WriteLine(sbLog.ToString());
				//Clients.All.newState(sbLog.ToString());
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		private void getThirdPartyCallController_OnFailedEvent(object sender, ThirdPartyCallController.FailedEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_OnFailedEvent(" + e.getCallingDeviceId + ", " + e.getCalledDeviceId + ")\n");
			try
			{
				/*
				// Till the pre-initialization call is active, skip event processing and second case if call is made from physical phone and second party didnt pick up the phone then control come here 
				if (snapShot || (currentCallState == TelephonyCallState.NoCall && !UIOperation))
					return;

				if (currentCallState == TelephonyCallState.Conferenced)
				{
					String droppedExtension = GetExtension(e.getFailingDeviceId.ToString());

					String message = "\nExtension " + droppedExtension + " dropped from the conference.\n";

					UpdatePopupUI updatePopupUI = new UpdatePopupUI(callManager.UpdatePopupUI);

					if (conferenceSize == 3)
					{
						updatePopupUI.BeginInvoke(CallState.Answered, message, null, null);
						currentCallState = TelephonyCallState.Established;
					}
					else if (conferenceSize > 3)
					{
						updatePopupUI.BeginInvoke(CallState.Conferenced, message, null, null);
					}

					conferenceSize--;
					return;
				}

				if (outBoundCall || e.getEventCause.Equals("busy"))
				{
					// Outbound call failed... Just updated flags here.  Corresponding connection cleat event will take care of the rest. 
					outBoundCall = false;
					UIOperation = false;
					currentCallState = TelephonyCallState.Failed;
					// First condition when the second party called is busy and second condition when that second party called is given as monitored party number 
					if (e.getEventCause.Equals("busy") && !e.getFailedConnectionId.getDeviceIdAsString.StartsWith(GetExtension(DeviceId)))
						serviceProvider.getThirdPartyCallController.ClearConnection(new ThirdPartyCallController.CallIdentifier(DeviceId, e.getFailedConnectionId.getCallId), null);
					else
					{
						currentCallState = TelephonyCallState.Held;
						serviceProvider.getThirdPartyCallController.ClearConnection(new ThirdPartyCallController.CallIdentifier(DeviceId, e.getFailedConnectionId.getCallId), null);
					}
					return;
				}
				else if (e.getEventCause.Equals("keyOperation"))
				{
					consultationFlag = false;
					currentCallState = TelephonyCallState.Held;
					serviceProvider.getThirdPartyCallController.ClearConnection(new ThirdPartyCallController.CallIdentifier(DeviceId, e.getFailedConnectionId.getCallId), null);
				}
				*/
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void getThirdPartyCallController_OnDeliveredEvent(object sender, ThirdPartyCallController.DeliveredEventEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_OnDeliveredEvent(" + e.getCallingDeviceId + ", " + e.getCalledDeviceId + ")\n");
			try
			{
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append("Ringing    - " + e.getCalledDeviceId);
				Debug.WriteLine(sbLog.ToString());

				string sCallID           = e.getConnectionId.getCallId;
				string sCallingExtension = SplendidConnection.GetExtension(e.getCallingDeviceId);
				string sCalledExtension  = SplendidConnection.GetExtension(e.getCalledDeviceId );
				if ( this._devices.ContainsKey(sCalledExtension) )
				{
					// Incoming call. 
					string sConnectedLineNum  = sCalledExtension;
					string sConnectedLineName = sCalledExtension;
					string sCallerIdNum       = sCallingExtension;
					string sCallerID          = sCallingExtension;
					Guid   gCALL_ID      = Guid.Empty;
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Context.Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Context.Application, sCallerIdNum, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
					
					if ( !this._incomingCalls.ContainsKey(sCallID) )
						this._incomingCalls.Add(sCallID, new SplendidConnection(e));
					Clients.Group(sConnectedLineNum).incomingCall(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID));
				}
				if ( this._devices.ContainsKey(sCallingExtension) )
				{
					// Outgoing call. 
					string sConnectedLineNum  = sCallingExtension;
					string sConnectedLineName = sCallingExtension;
					string sCallerIdNum       = sCalledExtension;
					string sCallerID          = sCalledExtension;
					Guid   gCALL_ID      = Guid.Empty;
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Context.Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Context.Application, sCallerID, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
					
					if ( !this._outgoingCalls.ContainsKey(sCallID) )
						this._outgoingCalls.Add(sCallID, new SplendidConnection(e));
					Clients.Group(sConnectedLineNum).outgoingCall(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		private void getThirdPartyCallController_EstablishedEvent(object sender, ThirdPartyCallController.EstablishedEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_EstablishedEvent(" + e.getCallingDeviceId + ", " + e.getCalledDeviceId + ", " + e.getAnsweringDeviceId + ")\n");
			try
			{
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append("Answer Call- " + e.getAnsweringDeviceId);
				Debug.WriteLine(sbLog.ToString());
				
				string sCallID           = e.getEstablishedConnectionId.getCallId;
				string sCallingExtension = SplendidConnection.GetExtension(e.getCallingDeviceId);
				string sCalledExtension  = SplendidConnection.GetExtension(e.getCalledDeviceId );
				if ( this._devices.ContainsKey(sCalledExtension) )
				{
					// Incoming call. 
					string sConnectedLineNum  = sCalledExtension;
					string sConnectedLineName = sCalledExtension;
					string sCallerIdNum       = sCallingExtension;
					string sCallerID          = sCallingExtension;
					Guid   gCALL_ID      = Guid.Empty;
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Context.Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Context.Application, sCallerIdNum, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
					
					if ( this._incomingCalls.ContainsKey(sCallID) )
					{
						SplendidConnection channel = _incomingCalls[sCallID];
						channel.Connected = true;
					}
					else
					{
						this._incomingCalls.Add(sCallID, new SplendidConnection(e));
					}
					Clients.Group(sConnectedLineNum).incomingCall(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID));
				}
				if ( this._devices.ContainsKey(sCallingExtension) )
				{
					// Outgoing call. 
					string sConnectedLineNum  = sCallingExtension;
					string sConnectedLineName = sCallingExtension;
					string sCallerIdNum       = sCalledExtension;
					string sCallerID          = sCalledExtension;
					Guid   gCALL_ID      = Guid.Empty;
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Context.Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Context.Application, sCallerID, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
					
					if ( this._outgoingCalls.ContainsKey(sCallID) )
					{
						SplendidConnection channel = _outgoingCalls[sCallID];
						channel.Connected = true;
					}
					else
					{
						this._outgoingCalls.Add(sCallID, new SplendidConnection(e));
					}
					Clients.Group(sConnectedLineNum).outgoingCall(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		private void getThirdPartyCallController_RingerStatusUpdatedEvent(object sender, Phone.RingerStatusUpdatedEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_RingerStatusUpdatedEvent(" + e.getPhone.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append("NewState   - " +  e.getPhone.getDevice.getDeviceIdAsString);
				Debug.WriteLine(sbLog.ToString());
				//Clients.All.newState(sbLog.ToString());
				//Clients.Group(e.Channel).newState(sbLog.ToString());
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		private void getThirdPartyCallController_ConnectionClearedEvent(object sender, ThirdPartyCallController.ConnectionClearedEventArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_ConnectionClearedEvent(" + e.getReleasingDeviceId + ")\n");
			try
			{
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append("Hangup     - " + e.getEventCause + " " + e.getReleasingDeviceId);
				Debug.WriteLine(sbLog.ToString());
				
				string sCallID = e.getDroppedConnectionId.getCallId;
				//if ( e.getEventCause == "normalClearing" )
				//{
				//}
				//else
				//{
				//}

				HttpApplicationState Application = Context.Application;
				if ( _outgoingCalls.ContainsKey(sCallID) )
				{
					SplendidConnection channel = _outgoingCalls[sCallID];
					_outgoingCalls.Remove(sCallID);
					string sCallingExtension = channel.CallingExtension;
					string sCalledExtension  = channel.CalledExtension ;
					
					string sConnectedLineNum  = sCallingExtension;
					string sConnectedLineName = sCallingExtension;
					string sCallerIdNum       = sCalledExtension;
					string sCallerID          = sCalledExtension;
					if ( !Sql.IsEmptyString(sConnectedLineNum) )
					{
						Guid   gCALL_ID      = Guid.Empty;  // Sql.ToGuid(channel.AccountCode);
						Guid   gUSER_ID      = Guid.Empty;
						Guid   gTEAM_ID      = Guid.Empty;
						Guid   gCALLER_ID    = Guid.Empty;
						string sCALLER_TYPE  = String.Empty;
						string sINVITEE_LIST = String.Empty;
						Crm.Users.GetUserByExtension(Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
						GetCaller(Application, sCallerID, ref gCALLER_ID, ref sCALLER_TYPE);
						if ( !Sql.IsEmptyGuid(gCALLER_ID) )
							sINVITEE_LIST = gCALLER_ID.ToString();
						
						Guid   gPARENT_ID   = Guid.Empty;
						string sPARENT_TYPE = String.Empty;
						// 12/23/2013 Paul.  The parent values would be parameters passed during OriginateCall.
						if ( !Sql.IsEmptyGuid(channel.PARENT_ID) )
						{
							gPARENT_ID   = channel.PARENT_ID  ;
							sPARENT_TYPE = channel.PARENT_TYPE;
						}
						else
						{
							gPARENT_ID   = gCALLER_ID  ;
							sPARENT_TYPE = sCALLER_TYPE;
						}
						
						if ( channel.Connected )
						{
							int nDURATION_HOURS   = 0;
							int nDURATION_MINUTES = 0;
							try
							{
								DateTime dtStart    = channel.DateReceived;
								DateTime dtEnd      = DateTime.Now;
								TimeSpan tsDuration = dtEnd - dtStart;
								nDURATION_HOURS     = tsDuration.Hours  ;
								nDURATION_MINUTES   = tsDuration.Minutes;
							}
							catch
							{
							}
							Clients.Group(sConnectedLineNum).outgoingComplete(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID), nDURATION_HOURS, nDURATION_MINUTES);
						}
						else if ( this._bLogOutgoingMissedCalls )
						{
							string sNAME = String.Format(L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.LBL_MISSED_OUTGOING_CALL_TEMPLATE"), sConnectedLineName, sCallerID);
							CreateCall(Application, ref gCALL_ID, sNAME, "Not Answered", "Outbound", gUSER_ID, gTEAM_ID, sINVITEE_LIST, gPARENT_ID, sPARENT_TYPE);
							Clients.Group(sConnectedLineNum).outgoingIncomplete(sCallID, sConnectedLineName, sCallerID, NullID(gCALL_ID), String.Empty);
						}
					}
				}
				else if ( _incomingCalls.ContainsKey(sCallID) )
				{
					SplendidConnection channel = _incomingCalls[sCallID];
					_incomingCalls.Remove(sCallID);
					string sCallingExtension = channel.CallingExtension;
					string sCalledExtension  = channel.CalledExtension ;
					
					string sConnectedLineNum  = sCalledExtension;
					string sConnectedLineName = sCalledExtension;
					string sCallerIdNum       = sCallingExtension;
					string sCallerID          = sCallingExtension;
					
					if ( !Sql.IsEmptyString(sConnectedLineName) )
					{
						Guid   gCALL_ID      = Guid.Empty;  // Sql.ToGuid(channel.AccountCode);
						Guid   gUSER_ID      = Guid.Empty;
						Guid   gTEAM_ID      = Guid.Empty;
						Guid   gCALLER_ID    = Guid.Empty;
						string sCALLER_TYPE  = String.Empty;
						string sINVITEE_LIST = String.Empty;
						Crm.Users.GetUserByExtension(Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
						GetCaller(Application, sCallerIdNum, ref gCALLER_ID, ref sCALLER_TYPE);
						if ( !Sql.IsEmptyGuid(gCALLER_ID) )
							sINVITEE_LIST = gCALLER_ID.ToString();
						
						Guid   gPARENT_ID   = Guid.Empty;
						string sPARENT_TYPE = String.Empty;
						// 12/23/2013 Paul.  The parent values would be parameters passed during OriginateCall.
						if ( !Sql.IsEmptyGuid(channel.PARENT_ID) )
						{
							gPARENT_ID   = channel.PARENT_ID  ;
							sPARENT_TYPE = channel.PARENT_TYPE;
						}
						else
						{
							gPARENT_ID   = gCALLER_ID  ;
							sPARENT_TYPE = sCALLER_TYPE;
						}
						
						if ( channel.Connected )
						{
							int nDURATION_HOURS   = 0;
							int nDURATION_MINUTES = 0;
							try
							{
								DateTime dtStart    = channel.DateReceived;
								DateTime dtEnd      = DateTime.Now;
								TimeSpan tsDuration = dtEnd - dtStart;
								nDURATION_HOURS     = tsDuration.Hours  ;
								nDURATION_MINUTES   = tsDuration.Minutes;
							}
							catch
							{
							}
							Clients.Group(sConnectedLineNum).incomingComplete(sCallID, sConnectedLineName, sCallerIdNum, NullID(gCALL_ID), nDURATION_HOURS, nDURATION_MINUTES);
						}
						else if ( this._bLogIncomingMissedCalls )
						{
							string sNAME = String.Format(L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.LBL_MISSED_INCOMING_CALL_TEMPLATE"), sCallerID, sConnectedLineName);
							CreateCall(Application, ref gCALL_ID, sNAME, "Not Answered", "Inbound", gUSER_ID, gTEAM_ID, sINVITEE_LIST, gPARENT_ID, sPARENT_TYPE);
							Clients.Group(sConnectedLineNum).incomingIncomplete(sCallID, sConnectedLineName, sCallerIdNum, NullID(gCALL_ID));
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		private void getThirdPartyCallController_MakeCallResponse(object sender, ThirdPartyCallController.MakeCallResponseArgs e)
		{
			Debug.WriteLine("getThirdPartyCallController_MakeCallResponse(" + e.getCallingDeviceConnectionId + ", " + e.getInvokeId + ", " + e.getGloballyUniqueCallLinkageId + ")\n");
			try
			{
				HttpApplicationState Application = Context.Application;
				SplendidCallData data = e.getUserState as SplendidCallData;
				string sEXTENSION   = String.Empty;
				string sPHONE       = String.Empty;
				Guid   gPARENT_ID   = Guid.Empty;
				string sPARENT_TYPE = String.Empty;
				if ( data != null )
				{
					sEXTENSION   = data.EXTENSION  ;
					sPHONE       = data.PHONE      ;
					gPARENT_ID   = data.PARENT_ID  ;
					sPARENT_TYPE = data.PARENT_TYPE;
				}
				if ( !Sql.IsEmptyString(e.getError) )
				{
					string sCallID        = String.Empty;  // Error result does not provide CallID. 
					string sMakeCallError = GetError(e.getError, "");
					
					// 12/03/2013 Paul.  Get the internal extension from the NewChannel event. 
					// 12/03/2013 Paul.  Get the phone number from the NewChannel event. 
					string sConnectedLineNum  = sEXTENSION;
					string sConnectedLineName = sEXTENSION;
					if ( !Sql.IsEmptyString(sConnectedLineNum) )
					{
						Guid   gCALL_ID      = Guid.Empty;
						Guid   gUSER_ID      = Guid.Empty;
						Guid   gTEAM_ID      = Guid.Empty;
						Guid   gCALLER_ID    = Guid.Empty;
						string sCALLER_TYPE  = String.Empty;
						string sINVITEE_LIST = String.Empty;
						Crm.Users.GetUserByExtension(Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
						GetCaller(Application, sPHONE, ref gCALLER_ID, ref sCALLER_TYPE);
						if ( !Sql.IsEmptyGuid(gCALLER_ID) )
							sINVITEE_LIST = gCALLER_ID.ToString();
						if ( Sql.IsEmptyGuid(gPARENT_ID) )
						{
							gPARENT_ID   = gCALLER_ID  ;
							sPARENT_TYPE = sCALLER_TYPE;
						}
						
						if ( this._bLogOutgoingMissedCalls )
						{
							string sNAME = String.Format(L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.LBL_MISSED_OUTGOING_CALL_TEMPLATE"), sConnectedLineName, sPHONE);
							CreateCall(Application, ref gCALL_ID, sNAME, "Not Answered", "Outbound", gUSER_ID, gTEAM_ID, sINVITEE_LIST, gPARENT_ID, sPARENT_TYPE);
						}
						// 12/26/2013 Paul.  Send the error to the client. 
						Clients.Group(sConnectedLineNum).outgoingIncomplete(sCallID, sConnectedLineName, sPHONE, NullID(gCALL_ID), sMakeCallError);
					}
				}
				else
				{
					string sCallID = e.getCallingDeviceConnectionId.getCallId;
					if ( _outgoingCalls.ContainsKey(sCallID) && !Sql.IsEmptyString(sPHONE) )
					{
						SplendidConnection channel = _outgoingCalls[sCallID];
						if ( !Sql.IsEmptyGuid(gPARENT_ID) )
						{
							channel.PARENT_ID   = gPARENT_ID  ;
							channel.PARENT_TYPE = sPARENT_TYPE;
						}
						Guid gCALL_ID = Guid.Empty;
						Clients.Group(channel.CallingExtension).outgoingCall(sCallID, sEXTENSION, sPHONE, NullID(gCALL_ID));
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}

		#endregion

		#region CallInfoLink
		private void callInfoLink_OnLinkDownEvent(object Sender, CallInformationLink.LinkDownEventArgs e)
		{
			Debug.WriteLine("callInfoLink_OnLinkDownEvent(" + e.getLinkName + ", " + e.getMonitorId + ")\n");
			try
			{
				//if (e.getLinkName.ToString() == cmName)
				//	MessageBox.Show("Receive Switch Connection Link Down Event...\nSwitch Connection '" + cmName + "' is down. Clik OK to wait for switch connection coming back up.", "Error");
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void callInfoLink_OnLinkUpEvent(object Sender, CallInformationLink.LinkUpEventArgs e)
		{
			Debug.WriteLine("callInfoLink_OnLinkUpEvent(" + e.getLinkName + ", " + e.getMonitorId + ")\n");
			try
			{
				//if (e.getLinkName.ToString() == cmName)
				//{
				//	linkUp = true;
				//	MessageBox.Show("Receive Switch Connection Link Up Event...\nSwitch Connection '" + cmName + "' has come back up.\n\nClick OK to complete restoring Call Control event notifications", "Notification");
				//	serviceProvider.getThirdPartyCallController.StopMonitor(callCtrlMonitorID, null);
				//}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		#endregion

		#region Terminal
		private void Terminal_OnRegisterTerminalResponse(object sender, Phone.RegisterTerminalResponseArgs e)
		{
			Debug.WriteLine("Terminal_OnRegisterTerminalResponse(" + e.getPhone.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "RegisterTerminal: " + GetError(e.getError, ""));
				}
				else
				{
					// 12/05/2013 Paul.  If we get a RegisterTerminal event when not monitoring, then start monitoring. 
					// This may not make sense as the event may come through the ThirdParty object. 
					string sExtension = e.getPhone.getDevice.getExtension;
					SplendidDevice deviceBeingMonitored = this._devices[sExtension];
					if ( Sql.IsEmptyString(deviceBeingMonitored.PhoneMonitorID) )
					{
						ThirdPartyCallController.ThirdPartyCallControlEvents CallControlEvents = new ThirdPartyCallController.ThirdPartyCallControlEvents(true);
						this._avayaConnection.getThirdPartyCallController.StartMonitor(deviceBeingMonitored.DeviceID, CallControlEvents, null);
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		private void Terminal_OnTerminalUnregisteredEvent(object Sender, Phone.TerminalUnregisteredEventArgs e)
		{
			Debug.WriteLine("Terminal_OnTerminalUnregisteredEvent(" + e.getPhone.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				string sExtension = e.getPhone.getDevice.getExtension;
				SplendidDevice deviceBeingMonitored = this._devices[sExtension];
				if ( !Sql.IsEmptyString(deviceBeingMonitored.PhoneMonitorID) )
				{
					deviceBeingMonitored.PhoneMonitorID = String.Empty;
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		#endregion

		#region Helpers
		private X509Certificate certificateCallback(Object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
		{
			Debug.WriteLine("certificateCallback");
			X509Certificate cert = null;
			try
			{
				string sX509Certificate = Sql.ToString (Context.Application["CONFIG.Avaya.Certificate"]);
				sX509Certificate = sX509Certificate.Trim();
				if ( !Sql.IsEmptyString(sX509Certificate) )
				{
					const string sCertHeader = "-----BEGIN CERTIFICATE-----";
					const string sCertFooter = "-----END CERTIFICATE-----";
					if (sX509Certificate.StartsWith(sCertHeader) && sX509Certificate.EndsWith(sCertFooter))
					{
						sX509Certificate = sX509Certificate.Substring(sCertHeader.Length, sX509Certificate.Length - sCertHeader.Length - sCertFooter.Length);
						byte[] byPKS8  = Convert.FromBase64String(sX509Certificate.Trim());
						
						cert = new X509Certificate(byPKS8);
					}
				}
			}
			catch
			{
			}
			return cert;
		}

		private void xmlProcessor_OnXmlMessageSentEvent(object sender, XmlProcessor.XmlMessageSentEventArgs e)
		{
			if ( this._bVerboseStatus )
				Debug.WriteLine(e.getMessage);
		}

		private void xmlProcessor_OnXmlMessageReceivedEvent(object sender, XmlProcessor.XmlMessageReceivedEventArgs e)
		{
			if ( this._bVerboseStatus )
				Debug.WriteLine(e.getMessageNicelyFormatted);
		}


		private object NullID(Guid gID)
		{
			return Sql.IsEmptyGuid(gID) ? null : gID.ToString();
		}

		private void GetCaller(HttpApplicationState Application, string sPhoneNumber, ref Guid gPARENT_ID, ref string sPARENT_TYPE)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL;
				sSQL = "select PARENT_ID                             " + ControlChars.CrLf
				     + "     , PARENT_TYPE                           " + ControlChars.CrLf
				     + "  from vwPHONE_NUMBERS                       " + ControlChars.CrLf
				     + " where NORMALIZED_NUMBER = @NORMALIZED_NUMBER" + ControlChars.CrLf
				     + "   and PHONE_TYPE in ('Work', 'Mobile', 'Office', 'Home')" + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@NORMALIZED_NUMBER", sPhoneNumber);
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							if ( dt.Rows.Count > 0 )
							{
								DataView vw = dt.DefaultView;
								vw.RowFilter = "PARENT_TYPE = 'Contacts'";
								if ( vw.Count > 0 )
								{
									gPARENT_ID   = Sql.ToGuid  (vw[0]["PARENT_ID"  ]);
									sPARENT_TYPE = Sql.ToString(vw[0]["PARENT_TYPE"]);
									return;
								}
								vw.RowFilter = "PARENT_TYPE = 'Leads'";
								if ( vw.Count > 0 )
								{
									gPARENT_ID   = Sql.ToGuid  (vw[0]["PARENT_ID"  ]);
									sPARENT_TYPE = Sql.ToString(vw[0]["PARENT_TYPE"]);
									return;
								}
								vw.RowFilter = "PARENT_TYPE = 'Prospects'";
								if ( vw.Count > 0 )
								{
									gPARENT_ID   = Sql.ToGuid  (vw[0]["PARENT_ID"  ]);
									sPARENT_TYPE = Sql.ToString(vw[0]["PARENT_TYPE"]);
									return;
								}
								vw.RowFilter = "PARENT_TYPE = 'Accounts'";
								if ( vw.Count > 0 )
								{
									gPARENT_ID   = Sql.ToGuid  (vw[0]["PARENT_ID"  ]);
									sPARENT_TYPE = Sql.ToString(vw[0]["PARENT_TYPE"]);
									return;
								}
							}
						}
					}
				}
			}
		}

		private void CreateCall(HttpApplicationState Application, ref Guid gCALL_ID, string sNAME, string sSTATUS, string sDIRECTION, Guid gUSER_ID, Guid gTEAM_ID, string sINVITEE_LIST, Guid gPARENT_ID, string sPARENT_TYPE)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						// 12/23/2013 Paul.  Add SMS_REMINDER_TIME. 
						// 09/14/2015 Paul.  Default for reminders should be 0. 
						SqlProcs.spCALLS_Update
							( ref gCALL_ID
							, gUSER_ID          // @ASSIGNED_USER_ID    uniqueidentifier
							, sNAME             // @NAME                nvarchar(50)
							, 0                 // @DURATION_HOURS      int
							, 0                 // @DURATION_MINUTES    int
							, DateTime.Now      // @DATE_TIME           datetime
							, sPARENT_TYPE      // @PARENT_TYPE         nvarchar(25)
							, gPARENT_ID        // @PARENT_ID           uniqueidentifier
							, sSTATUS           // @STATUS              nvarchar(25)
							, sDIRECTION        // @DIRECTION           nvarchar(25)
							, 0                 // @REMINDER_TIME       int
							, null              // @DESCRIPTION         nvarchar(max)
							, sINVITEE_LIST     // @INVITEE_LIST        varchar(8000)
							, gTEAM_ID          // @TEAM_ID             uniqueidentifier = null
							, null              // @TEAM_SET_LIST       varchar(8000) = null
							, 0                 // @EMAIL_REMINDER_TIME int = null
							, false             // @ALL_DAY_EVENT       bit = null
							, null              // @REPEAT_TYPE         nvarchar(25) = null
							, 0                 // @REPEAT_INTERVAL     int = null
							, null              // @REPEAT_DOW          nvarchar(7) = null
							, DateTime.MinValue // @REPEAT_UNTIL        datetime = null
							, 0                 // @REPEAT_COUNT        int = null
							, 0                 // @SMS_REMINDER_TIME   int = null
							// 05/17/2017 Paul.  Add Tags module. 
							, String.Empty      // TAG_SET_NAME
							// 11/07/2017 Paul.  Add IS_PRIVATE for use by a large customer. 
							, false             // IS_PRIVATE
							// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
							, String.Empty      // ASSIGNED_SET_LIST
							, trn
							);
						trn.Commit();
					}
					catch(Exception ex)
					{
						trn.Rollback();
						throw(new Exception(ex.Message, ex.InnerException));
					}
				}
			}
		}
		#endregion

		private AvayaManager(HttpContext Context, IHubConnectionContext clients)
		{
			this._devices       = new Dictionary<string, SplendidDevice>();
			this._outgoingCalls = new Dictionary<string, SplendidConnection>();
			this._incomingCalls = new Dictionary<string, SplendidConnection>();
			this.Context = Context;
			this.Clients = clients;
		}

		#region disposal
		private bool disposed = false;
		
		~AvayaManager()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected void Dispose(bool disposing)
		{
			if ( !disposed )
			{
				if ( disposing )
				{
					if ( _avayaConnection != null )
					{
						try
						{
							this._avayaConnection.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "Application shutting down");
						}
						catch
						{
						}
						this._avayaConnection = null;
					}
				}
			}
			disposed = true;
		}
		#endregion

		/*
		private void getThirdPartyCallController_CallDataRecord(object sender, CdrEvent e)
		{
		 * Debug.WriteLine("getThirdPartyCallController_CallDataRecord");
			try
			{
				// 09/09/2013 Paul.  The Account Code may need to be pulled from the originating record. 
				if ( Sql.IsEmptyString(e.AccountCode) )
				{
					if ( _outgoingCalls.ContainsKey(sCallID) )
					{
						ThirdPartyCallController.EstablishedEventArgs channel = _outgoingCalls[sCallID];
						e.AccountCode = channel.AccountCode;
					}
					else if ( _incomingCalls.ContainsKey(sCallID) )
					{
						ThirdPartyCallController.EstablishedEventArgs channel = _incomingCalls[sCallID];
						e.AccountCode = channel.AccountCode;
					}
				}
				StringBuilder sbLog = new StringBuilder();
				sbLog.Append("CDR        - ");
				Debug.WriteLine(sbLog.ToString());
				
				HttpApplicationState Application = Context.Application;
				bool bLogCallDetails = Sql.ToBoolean(Application["CONFIG.Avaya.LogCallDetails"]);
				if ( bLogCallDetails )
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						using ( IDbTransaction trn = Sql.BeginTransaction(con) )
						{
							try
							{
								SqlProcs.spCALL_DETAIL_RECORDS_InsertOnly
									( sCallID                             // @UNIQUEID             nvarchar(32)
									, Sql.ToGuid(e.AccountCode)           // @ACCOUNT_CODE_ID      uniqueidentifier
									, e.Src                               // @SOURCE               nvarchar(80)
									, e.Destination                       // @DESTINATION          nvarchar(80)
									, e.DestinationContext                // @DESTINATION_CONTEXT  nvarchar(80)
									, e.CallerId                          // @CALLERID             nvarchar(80)
									, e.Channel                           // @SOURCE_CHANNEL       nvarchar(80)
									, e.DestinationChannel                // @DESTINATION_CHANNEL  nvarchar(80)
									, Sql.ToDateTime (e.StartTime      )  // @START_TIME           datetime
									, Sql.ToDateTime (e.AnswerTime     )  // @ANSWER_TIME          datetime
									, Sql.ToDateTime (e.EndTime        )  // @END_TIME             datetime
									, Convert.ToInt32(e.Duration       )  // @DURATION             int
									, Convert.ToInt32(e.BillableSeconds)  // @BILLABLE_SECONDS     int
									, e.Disposition                       // @DISPOSITION          nvarchar(45)
									, e.AmaFlags                          // @AMA_FLAGS            nvarchar(80)
									, e.LastApplication                   // @LAST_APPLICATION     nvarchar(80)
									, e.LastData                          // @LAST_DATA            nvarchar(80)
									, e.UserField                         // @USER_FIELD           nvarchar(255)
									, trn
									);
								trn.Commit();
							}
							catch(Exception ex)
							{
								trn.Rollback();
								throw(new Exception(ex.Message, ex.InnerException));
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
			}
		}
		*/

		class SplendidCalledDevice
		{
			public Avaya.ApplicationEnablement.DMCC.Device Device { get; set; }
			public string DeviceID { get; set; }
			public string PHONE    { get; set; }

			public SplendidCalledDevice(Avaya.ApplicationEnablement.DMCC.Device Device, string sPHONE)
			{
				this.Device = Device;
				this.PHONE  = sPHONE;
			}
		}

		private void OriginateCall_OnGetDeviceIdResponse(object sender, Device.GetDeviceIdResponseArgs e)
		{
			Debug.WriteLine("OriginateCall_OnGetDeviceIdResponse(" + e.getDevice.getExtension + ", " + e.getDevice.getDeviceIdAsString + ")\n");
			try
			{
				if ( !Sql.IsEmptyString(e.getError) )
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), "GetDeviceID: " + GetError(e.getError, ""));
				}
				else
				{
					SplendidCallData callData = e.getUserState as SplendidCallData;
					callData.CalledDeviceID = e.getDevice.getDeviceIdAsString;
					
					int error = _avayaConnection.getThirdPartyCallController.MakeCall(callData.CallingDeviceID, callData.CalledDeviceID, callData);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
			}
		}

		public string OriginateCall(string sUSER_EXTENSION, string sUSER_FULL_NAME, string sUSER_PHONE_WORK, string sPHONE, string sPARENT_ID, string sPARENT_TYPE)
		{
			if ( _avayaConnection == null )
				Login();
			
			Regex r = new Regex(@"[^0-9_]");
			sPHONE = r.Replace(sPHONE, "");
			try
			{
				if ( this._devices.ContainsKey(sUSER_EXTENSION) )
				{
					SplendidDevice deviceBeingMonitored = this._devices[sUSER_EXTENSION];
					
					SplendidCallData callData = new SplendidCallData(sUSER_EXTENSION, sPHONE, Sql.ToGuid(sPARENT_ID), sPARENT_TYPE, deviceBeingMonitored.DeviceID);
					
					Device device = this._avayaConnection.GetNewDevice();
					device.OnGetDeviceIdResponse += new GetDeviceIdResponseHandler(OriginateCall_OnGetDeviceIdResponse);
					device.GetDeviceId(sPHONE, this._sSwitchName, String.Empty, false, callData);
					return "Connected";
				}
				else
				{
					Clients.Group(sUSER_EXTENSION).outgoingIncomplete(String.Empty, sUSER_EXTENSION, sPHONE, NullID(Guid.Empty), "No device for " + sUSER_EXTENSION);
					return "No device for " + sUSER_EXTENSION;
				}
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}

		#region CreateCall
		public Guid CreateCall(string sUniqueId)
		{
			Guid gCALL_ID = Guid.Empty;
			
			HttpApplicationState Application = Context.Application;
			if ( _outgoingCalls.ContainsKey(sUniqueId) )
			{
				SplendidConnection channel = _outgoingCalls[sUniqueId];
				
				string sConnectedLineNum  = channel.CallingExtension;
				string sConnectedLineName = channel.CallingExtension;
				string sCallerID          = channel.CalledExtension;
				gCALL_ID = channel.CALL_ID;
				if ( Sql.IsEmptyGuid(gCALL_ID) )
				{
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Application, sCallerID, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
						
					Guid   gPARENT_ID   = Guid.Empty;
					string sPARENT_TYPE = String.Empty;
					if ( !Sql.IsEmptyGuid(channel.PARENT_ID) )
					{
						gPARENT_ID   = channel.PARENT_ID  ;
						sPARENT_TYPE = channel.PARENT_TYPE;
					}
					else
					{
						gPARENT_ID   = gCALLER_ID  ;
						sPARENT_TYPE = sCALLER_TYPE;
					}
						
					string sNAME = String.Format(L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.LBL_ANSWERED_OUTGOING_CALL_TEMPLATE"), sConnectedLineName, sCallerID);
					CreateCall(Application, ref gCALL_ID, sNAME, "Held", "Outbound", gUSER_ID, gTEAM_ID, sINVITEE_LIST, gPARENT_ID, sPARENT_TYPE);
					channel.CALL_ID = gCALL_ID;
				}
			}
			else if ( _incomingCalls.ContainsKey(sUniqueId) )
			{
				SplendidConnection channel = _incomingCalls[sUniqueId];
				
				string sConnectedLineNum  = channel.CalledExtension;
				string sConnectedLineName = channel.CalledExtension;
				string sCallerID          = channel.CallingExtension;
				gCALL_ID = channel.CALL_ID;
				if ( Sql.IsEmptyGuid(gCALL_ID) )
				{
					Guid   gUSER_ID      = Guid.Empty;
					Guid   gTEAM_ID      = Guid.Empty;
					Guid   gCALLER_ID    = Guid.Empty;
					string sCALLER_TYPE  = String.Empty;
					string sINVITEE_LIST = String.Empty;
					Crm.Users.GetUserByExtension(Application, sConnectedLineNum, ref gUSER_ID, ref gTEAM_ID);
					GetCaller(Application, channel.CallingExtension, ref gCALLER_ID, ref sCALLER_TYPE);
					if ( !Sql.IsEmptyGuid(gCALLER_ID) )
						sINVITEE_LIST = gCALLER_ID.ToString();
					
						Guid   gPARENT_ID   = Guid.Empty;
						string sPARENT_TYPE = String.Empty;
						if ( !Sql.IsEmptyGuid(channel.PARENT_ID) )
						{
							gPARENT_ID   = channel.PARENT_ID  ;
							sPARENT_TYPE = channel.PARENT_TYPE;
						}
					else
					{
						gPARENT_ID   = gCALLER_ID  ;
						sPARENT_TYPE = sCALLER_TYPE;
					}
					
					string sNAME = String.Format(L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.LBL_ANSWERED_INCOMING_CALL_TEMPLATE"), sCallerID, sConnectedLineName);
					CreateCall(Application, ref gCALL_ID, sNAME, "Held", "Inbound", gUSER_ID, gTEAM_ID, sINVITEE_LIST, gPARENT_ID, sPARENT_TYPE);
					channel.CALL_ID = gCALL_ID;
				}
			}
			else
			{
				string sERR_CALL_NOT_FOUND = L10N.Term(Application, Sql.ToString(Application["CONFIG.default_language"]), "Avaya.ERR_CALL_NOT_FOUND");
				throw(new Exception(sERR_CALL_NOT_FOUND));
			}
			return gCALL_ID;
		}
		#endregion
	}
}

