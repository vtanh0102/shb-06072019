/**
 * Copyright (C) 2005-2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
//using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace SplendidCRM
{
	public class ReportStreamHandler : IHttpHandler, IRequiresSessionState
	{
		public bool IsReusable
		{
			get { return false; }
		}

		public static IHttpHandler CreateHandler(HttpContext context)
		{
			return new ReportStreamHandler();
		}

		private static void InitializeCachePolicy(HttpContext context)
		{
			context.Response.Cache.SetNoServerCaching();
			context.Response.Cache.SetMaxAge(TimeSpan.Zero);
		}

		public void ProcessRequest(HttpContext context)
		{
			try
			{
				InitializeCachePolicy(context);
				context.Response.ContentType = "text/xml";
				
				HttpRequest Request = context.Request;
				// rs%3aCommand=ExecuteQuery&rs%3aDataSourceName=%2fSplendidCRM6+Model&rs%3aGetUserModel=True
				string sCommand = Sql.ToString (Request.Form["rs:Command"]);
				// 12/09/2009 Paul.  SQL Server Reporting Services has a number of Execution options that side-step the SOAP Service. 
				if ( String.Compare(sCommand, "ExecuteQuery", true) == 0 )
				{
					string sDataSourceName = Sql.ToString(Request.Form["rs:DataSourceName"]);
					bool bGetUserModel = Sql.ToBoolean(Request.Form["rs:GetUserModel"]);
					if ( bGetUserModel )
					{
						string sXML = ReportService2005.GetUserModel(context, sDataSourceName, String.Empty);
						using ( StreamWriter writer = new StreamWriter(context.Response.OutputStream, new UTF8Encoding(false)) )
						{
							writer.Write(sXML);
							writer.Flush();
						}
					}
					else
					{
						int    nTimeout     = Sql.ToInteger(Request["rs:Timeout"    ]);
						string sCommandText = Sql.ToString (Request["rs:CommandText"]);
						// 12/09/2009 Paul.  Execute query and return XML attachment output. 
						context.Response.AddHeader("FileExtension", "xml");
						context.Response.AddHeader("Content-Disposition", "attachment; filename=\""+ context.Server.UrlEncode(sDataSourceName + "_result.xml") + "\"");
						string sXML = ReportService2005.ExecuteQuery(context, sDataSourceName, sCommandText, nTimeout);
						using ( StreamWriter writer = new StreamWriter(context.Response.OutputStream, new UTF8Encoding(false)) )
						{
							writer.Write(sXML);
							writer.Flush();
						}
					}
				}
			}
			catch (Exception ex)
			{
				context.Response.ClearHeaders();
				context.Response.ClearContent();
				context.Response.Clear();
				context.Response.StatusCode = 500;
				context.Response.StatusDescription = HttpWorkerRequest.GetStatusDescription(500);
				context.Response.ContentType = "text/plain";
				using ( StreamWriter writer = new StreamWriter(context.Response.OutputStream, new UTF8Encoding(false)) )
				{
					writer.Write(ex.Message + ControlChars.CrLf + ex.StackTrace);
					writer.Flush();
				}
			}
		}
	}

	public class ReportStreamHandlerFactory : IHttpHandlerFactory
	{
		public virtual IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
		{
			if ( context == null )
			{
				throw new ArgumentNullException("context");
			}
			return ReportStreamHandler.CreateHandler(context);
		}

		public static bool IsStreamedOperation(HttpContext context)
		{
			HttpRequest Request = context.Request;
			if ( Request.HttpMethod == "POST" && Request.Headers["SOAPAction"] == null )
			{
				// 12/09/2009 Paul.  The purpose of ReportStreamHandler is to allow a stream output to be byte-efficient.  It is specific to ReportService2005. 
				// 12/11/2009 Paul.  
				if ( Request.ContentType == "application/x-www-form-urlencoded" )
				{
					// 09/25/2010 Paul.  Add ReportService2010.asmx. 
					if ( Request.AppRelativeCurrentExecutionFilePath.EndsWith("/ReportService2005.asmx") 
					  || Request.AppRelativeCurrentExecutionFilePath.EndsWith("/ReportService2006.asmx") 
					  || Request.AppRelativeCurrentExecutionFilePath.EndsWith("/ReportService2010.asmx") )
						return true;
				}
			}
			return false;
		}

		public virtual void ReleaseHandler(IHttpHandler handler)
		{
		}
	}

}

