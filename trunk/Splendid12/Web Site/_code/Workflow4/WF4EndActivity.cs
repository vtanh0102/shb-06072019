﻿/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Activities;
using System.Xml;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.ComponentModel;
using System.Diagnostics;

namespace SplendidCRM
{
	public class WF4EndActivity : CodeActivity
	{
		protected override void Execute(CodeActivityContext context)
		{
			SplendidApplicationService app = context.GetExtension<SplendidApplicationService>();
			try
			{
				Debug.WriteLine("WF4EndActivity");
			
				// 09/05/2016 Paul.  Instead of relying upon the tracking record to mark as completed, use the End activity. 
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							SqlProcs.spBUSINESS_PROCESSES_RUN_Completed(context.WorkflowInstanceId, trn);
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("WF4EndActivity.Execute failed: " + ex.Message, ex));
			}
		}
	}

}
