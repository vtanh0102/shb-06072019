/**
 * Copyright (C) 2016 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Activities.DurableInstancing;

namespace SplendidCRM
{
	public class DefaultObjectSerializer : IObjectSerializer
	{
		NetDataContractSerializer serializer;

		public DefaultObjectSerializer()
		{
			this.serializer = new NetDataContractSerializer();
		}

		public Dictionary<XName, object> DeserializePropertyBag(byte[] serializedValue)
		{
			using ( MemoryStream stm = new MemoryStream(serializedValue) )
			{
				return this.DeserializePropertyBag(stm);
			}
		}

		public object DeserializeValue(byte[] serializedValue)
		{
			using ( MemoryStream stm = new MemoryStream(serializedValue) )
			{
				return this.DeserializeValue(stm);
			}
		}

		public ArraySegment<byte> SerializePropertyBag(Dictionary<XName, object> value)
		{
			using ( MemoryStream stm = new MemoryStream(4096) )
			{
				this.SerializePropertyBag(stm, value);
				return new ArraySegment<byte>(stm.GetBuffer(), 0, Convert.ToInt32(stm.Length));
			}
		}

		public ArraySegment<byte> SerializeValue(object value)
		{
			using ( MemoryStream stm = new MemoryStream(4096) )
			{
				this.SerializeValue(stm, value);
				return new ArraySegment<byte>(stm.GetBuffer(), 0, Convert.ToInt32(stm.Length));
			}
		}

		protected virtual Dictionary<XName, object> DeserializePropertyBag(Stream stream)
		{
			using ( XmlDictionaryReader rdr = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max) )
			{
				Dictionary<XName, object> propertyBag = new Dictionary<XName, object>();
				if ( rdr.ReadToDescendant("Property") )
				{
					do
					{
						rdr.Read();
						KeyValuePair<XName, object> property = (KeyValuePair<XName, object>) this.serializer.ReadObject(rdr);
						propertyBag.Add(property.Key, property.Value);
					}
					while (rdr.ReadToNextSibling("Property"));
				}

				return propertyBag;
			}
		}

		protected virtual object DeserializeValue(Stream stream)
		{
			using ( XmlDictionaryReader rdr = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max) )
			{
				return this.serializer.ReadObject(rdr);
			}
		}

		protected virtual void SerializePropertyBag(Stream stream, Dictionary<XName, object> propertyBag)
		{
			using ( XmlDictionaryWriter wtr = XmlDictionaryWriter.CreateBinaryWriter(stream, null, null, false) )
			{
				wtr.WriteStartElement("Properties");
				foreach (KeyValuePair<XName, object> property in propertyBag)
				{
					wtr.WriteStartElement("Property");
					this.serializer.WriteObject(wtr, property);
					wtr.WriteEndElement();
				}
				wtr.WriteEndElement();
			}
		}

		protected virtual void SerializeValue(Stream stream, object value)
		{
			using ( XmlDictionaryWriter dictionaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream, null, null, false) )
			{
				this.serializer.WriteObject(dictionaryWriter, value);
			}
		}
	}
}
