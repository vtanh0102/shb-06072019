/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;

namespace SplendidCRM
{
	/// <summary>
	/// Summary description for WizardControl.
	/// </summary>
	public class WizardControl : SplendidControl
	{
		public CommandEventHandler Command;

		protected XmlDocument GetXml()
		{
			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			try
			{
				HiddenField txtWizardXML = Parent.FindControl("txtWizardXML") as HiddenField;
				if ( txtWizardXML != null )
				{
					xml.LoadXml(Server.HtmlDecode(txtWizardXML.Value));
				}
			}
			catch
			{
			}
			return xml;
		}

		protected void SetXml(XmlDocument xml)
		{
			try
			{
				HiddenField txtWizardXML = Parent.FindControl("txtWizardXML") as HiddenField;
				if ( txtWizardXML != null )
				{
					txtWizardXML.Value = Server.HtmlEncode(xml.OuterXml);
				}
			}
			catch
			{
			}
		}

		public virtual void Activate(bool bNext)
		{
		}
	}
}

