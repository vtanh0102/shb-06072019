/**
 * Copyright (C) 2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Net.Mail;
using System.Xml;
using System.Diagnostics;

namespace SplendidCRM
{
	public class AttachReportActivity: Activity
	{
		protected string sALERT_NAME    ;
		protected string sRENDER_FORMAT ;

		public AttachReportActivity()
		{
			this.Name = "AttachReportActivity";
			this.sRENDER_FORMAT = "PDF";
		}

		#region Public workflow properties
		public static DependencyProperty REPORT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORT_ID", typeof(Guid), typeof(AttachReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid REPORT_ID
		{
			get { return ((Guid)(base.GetValue(AttachReportActivity.REPORT_IDProperty))); }
			set { base.SetValue(AttachReportActivity.REPORT_IDProperty, value); }
		}

		// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
		public static DependencyProperty SCHEDULED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SCHEDULED_USER_ID", typeof(Guid), typeof(AttachReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid SCHEDULED_USER_ID
		{
			get { return ((Guid)(base.GetValue(AttachReportActivity.SCHEDULED_USER_IDProperty))); }
			set { base.SetValue(AttachReportActivity.SCHEDULED_USER_IDProperty, value); }
		}

		public static DependencyProperty REPORT_PARAMETERSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("REPORT_PARAMETERS", typeof(string), typeof(AttachReportActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string REPORT_PARAMETERS
		{
			get { return ((string)(base.GetValue(AttachReportActivity.REPORT_PARAMETERSProperty))); }
			set { base.SetValue(AttachReportActivity.REPORT_PARAMETERSProperty, value); }
		}

		public string ALERT_NAME
		{
			get { return sALERT_NAME; }
			set { sALERT_NAME = value; }
		}

		public string RENDER_FORMAT
		{
			get { return sRENDER_FORMAT; }
			set { sRENDER_FORMAT = value; }
		}
		#endregion

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			if ( Sql.IsEmptyString(sALERT_NAME) )
				throw(new Exception("ALERT_NAME  not specified."));
			
			Activity act = this.Parent.GetActivityByName(sALERT_NAME );
			if ( act == null )
				throw(new Exception(sALERT_NAME + " not found."));
			
			AlertActivity actAlert = act as AlertActivity;
			if ( actAlert == null )
				throw(new Exception(sALERT_NAME + " of type AlertActivity was not found."));
			
			SplendidApplicationService app = executionContext.GetService<SplendidApplicationService>();
			TimeZone T10n = TimeZone.CreateTimeZone(app.Application, Guid.Empty);
			// 12/04/2010 Paul.  L10n is needed by the Rules Engine to allow translation of list terms. 
			L10N L10n = new L10N(SplendidDefaults.Culture(app.Application));

			DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL;
				sSQL = "select *             " + ControlChars.CrLf
				     + "  from vwREPORTS_Edit" + ControlChars.CrLf
				     + " where ID = @ID      " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", REPORT_ID);
					con.Open();

					using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
					{
						if ( rdr.Read() )
						{
							string sRDL         = Sql.ToString(rdr["RDL"        ]);
							string sMODULE_NAME = Sql.ToString(rdr["MODULE_NAME"]);
							string sFILENAME    = Sql.ToString(rdr["NAME"       ]);
							if ( !Sql.IsEmptyString(sRDL) )
							{
								switch ( sRENDER_FORMAT.ToUpper() )
								{
									// 05/13/2014 Paul.  Word format is supported by ReportViewer 2012. 
									// http://msdn.microsoft.com/en-us/library/ms251671(v=vs.110).aspx
									// 09/13/2016 Paul.  Possible render formats "Excel" "EXCELOPENXML" "IMAGE" "PDF" "WORD" "WORDOPENXML". 
									// http://stackoverflow.com/questions/3494009/creating-a-custom-export-to-excel-for-reportviewer-rdlc
									// 05/07/2018 Paul.  Include all possible values. 
									case "WORD"        :  sRENDER_FORMAT = "WORDOPENXML" ;  break;
									case "WORDOPENXML" :  sRENDER_FORMAT = "WORDOPENXML" ;  break;
									case "EXCEL"       :  sRENDER_FORMAT = "EXCELOPENXML";  break;
									case "EXCELOPENXML":  sRENDER_FORMAT = "EXCELOPENXML";  break;
									case "IMAGE"       :  sRENDER_FORMAT = "Image"       ;  break;
									case "PDF"         :  sRENDER_FORMAT = "PDF"         ;  break;
									default            :  sRENDER_FORMAT = "PDF"         ;  break;
								}
								string sFILE_MIME_TYPE;
								string sFILE_EXT      ;
								
								// 07/13/2010 Paul.  If the paramters exist, then insert them directly into the RDL. 
								// Since the parameters use the URL syntax, it is important that any bindings get escaped. 
								if ( REPORT_PARAMETERS.Length > 0 )
								{
									RdlDocument rdl = new RdlDocument();
									rdl.LoadRdl(sRDL);
									
									string[] arrParameterPairs = REPORT_PARAMETERS.Split('&');
									Hashtable hashParameters = new Hashtable();
									foreach ( string sParameterPair in arrParameterPairs )
									{
										string[] arrParameter = sParameterPair.Split('=');
										string sName = arrParameter[0].ToUpper();
										// 07/13/2010 Paul.  We only need to set the value if there is a value and if it does not already exist. 
										if ( arrParameter.Length > 1 && !hashParameters.ContainsKey(sName) )
											hashParameters.Add(sName, arrParameter[1]);
									}
									XmlNodeList nlReportParameters = rdl.SelectNodesNS("ReportParameters/ReportParameter");
									foreach ( XmlNode xReportParameter in nlReportParameters )
									{
										string sName  = xReportParameter.Attributes.GetNamedItem("Name").Value.ToUpper();
										string sValue = String.Empty;
										if ( hashParameters.ContainsKey(sName) )
										{
											sValue = Sql.ToString(hashParameters[sName]);
										}
										rdl.SetSingleNode(xReportParameter, "DefaultValue/Values/Value", sValue);
									}
									sRDL = rdl.OuterXml;
								}
								// 12/04/2010 Paul.  L10n is needed by the Rules Engine to allow translation of list terms. 
								// 04/13/2011 Paul.  A scheduled report does not have a Session, so we need to create a session using the same approach used for ExchangeSync. 
								// 10/06/2012 Paul.  REPORT_ID is needed for sub-report caching. 
								byte[] byContent = Reports.AttachmentView.Render(app.Context, L10n, T10n, REPORT_ID, sRDL, sRENDER_FORMAT, sMODULE_NAME, SCHEDULED_USER_ID, out sFILE_MIME_TYPE, out sFILE_EXT);
								// 05/11/2010 Paul.  Include the extension in the file name so that it will appear in emails.
								sFILENAME += "." + sFILE_EXT;
								AlertAttachment a = new AlertAttachment(sFILENAME, sFILE_EXT, sFILE_MIME_TYPE, byContent);
								actAlert.ATTACHMENTS.Add(a);
							}
						}
					}
				}
			}
			return ActivityExecutionStatus.Closed;
		}
	}
}
