/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	// 08/09/2008 Paul.  The purpose of the WorkflowLogActivity is to log the module table/audit table, the audit token and the workflow ID of the root activity. 
	// The goal is to use this record to block any activity in the workflow from firing this same workflow. 
	// The trick is to use the audit token of the root activity to get the workflow instance ID, then use the audit table and instance id. 
	public class WorkflowLogActivity: Activity
	{
		[NonSerialized]
		protected SplendidApplicationService app;

		public WorkflowLogActivity()
		{
			this.Name = "WorkflowLogActivity";
		}

		#region Public workflow properties
		public static DependencyProperty MODULE_TABLEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODULE_TABLE", typeof(string), typeof(WorkflowLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODULE_TABLE
		{
			get { return ((string)(base.GetValue(WorkflowLogActivity.MODULE_TABLEProperty))); }
			set { base.SetValue(WorkflowLogActivity.MODULE_TABLEProperty, value); }
		}

		public static DependencyProperty WORKFLOW_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORKFLOW_ID", typeof(Guid), typeof(WorkflowLogActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid WORKFLOW_ID
		{
			get { return ((Guid)(base.GetValue(WorkflowLogActivity.WORKFLOW_IDProperty))); }
			set { base.SetValue(WorkflowLogActivity.WORKFLOW_IDProperty, value); }
		}
		#endregion

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			app = executionContext.GetService<SplendidApplicationService>();
			// 09/16/2015 Paul.  Change to Debug as it is automatically not included in a release build. 
			Debug.WriteLine("ActivityExecutionStatus Execute: MODULE_TABLE = " + MODULE_TABLE.ToString());

			DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly(MODULE_TABLE, WORKFLOW_ID, this.WorkflowInstanceId, trn);
						trn.Commit();
					}
					catch
					{
						trn.Rollback();
						// 12/25/2008 Paul.  Re-throw the original exception so as to retain the call stack. 
						throw;
					}
				}
			}
			return base.Execute(executionContext);
		}
	}
}
