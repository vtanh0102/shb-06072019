/**
 * Copyright (C) 2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using System.Reflection;
using System.Diagnostics;

namespace SplendidCRM
{
	public class SetRuleActivity: Activity
	{
		public SetRuleActivity()
		{
			this.Name = "SetRuleActivity";
		}

		#region Public workflow properties
		public static DependencyProperty LVALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LVALUE", typeof(object), typeof(SetRuleActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public object LVALUE
		{
			get { return ((object)(base.GetValue(SetRuleActivity.LVALUEProperty))); }
			set { base.SetValue(SetRuleActivity.LVALUEProperty, value); }
		}

		public static DependencyProperty RULEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RULE", typeof(string), typeof(SetRuleActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RULE
		{
			get { return ((string)(base.GetValue(SetRuleActivity.RULEProperty))); }
			set { base.SetValue(SetRuleActivity.RULEProperty, value); }
		}

		public static DependencyProperty PARENT_ACTIVITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ACTIVITY", typeof(string), typeof(SetRuleActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_ACTIVITY
		{
			get { return ((string)(base.GetValue(SetRuleActivity.PARENT_ACTIVITYProperty))); }
			set { base.SetValue(SetRuleActivity.PARENT_ACTIVITYProperty, value); }
		}
		#endregion

		// 10/11/2008 Paul.  We must convert the values to the correct data type, otherwise an exception will be thrown. 
		private void SetLVALUE(object o)
		{
			// 10/23/2008 Paul.  We can't get the type if the LVALUE is null. 
			if ( LVALUE != null )
			{
				Type t = LVALUE.GetType();
				if ( t == typeof(System.String) )
					LVALUE = Sql.ToString(o);
				else if ( t == typeof(System.Guid    ) )
					LVALUE = Sql.ToGuid(o);
				else if ( t == typeof(System.DateTime) )
					LVALUE = Sql.ToDateTime(o);
				else if ( t == typeof(System.Boolean ) )
					LVALUE = Sql.ToBoolean(o);
				else if ( t == typeof(System.Double  ) )
					LVALUE = Sql.ToDouble(o);
				// 10/19/2010 Paul.  Add support for System.Single. 
				else if ( t == typeof(System.Single  ) )
					LVALUE = Sql.ToFloat(o);
				else if ( t == typeof(System.Decimal ) )
					LVALUE = Sql.ToDecimal(o);
				else if ( t == typeof(System.Int16) || t == typeof(System.Int32) || t == typeof(System.Int64) )
					LVALUE = Sql.ToInteger(o);
				else
					LVALUE = o;
			}
			else
			{
				LVALUE = o;
			}
		}

		public class WorkflowRuleThis : SqlObj
		{
			private string   sActivity;
			private Activity act      ;
			private object   oValue   ;
			
			public WorkflowRuleThis(string sActivity, Activity act, object oValue)
			{
				this.sActivity = sActivity;
				this.act       = act      ;
				this.oValue    = oValue   ;
			}

			public object Value
			{
				get { return oValue; }
				set { oValue = value; }
			}

			public object this[string columnName]
			{
				get
				{
					if ( Sql.IsEmptyString(sActivity) )
						throw(new Exception("PARENT_ACTIVITY was not set."));
					
					// 11/10/2010 Paul.  The NAME property can collide with the Name property, so don't ignore case and always promote to uppercase. 
					PropertyInfo pi = act.GetType().GetProperty(columnName.ToUpper(), BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
					//object oValue = pi.GetGetMethod().Invoke(act, null);
					object oValue = pi.GetValue(act, null);
					return oValue;
				}
				set
				{
					if ( Sql.IsEmptyString(sActivity) )
						throw(new Exception("PARENT_ACTIVITY was not set."));
					
					// 11/10/2010 Paul.  The NAME property can collide with the Name property, so don't ignore case and always promote to uppercase. 
					PropertyInfo pi = act.GetType().GetProperty(columnName.ToUpper(), BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
					//pi.GetSetMethod().Invoke(act, value, null);
					pi.SetValue(act, value, null);
				}
			}
		}

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			Activity actPARENT = this.Parent.GetActivityByName(PARENT_ACTIVITY);
			
			RuleValidation validation = new RuleValidation(typeof(WorkflowRuleThis), null);
			RuleSet        rules      = new RuleSet("RuleSet 1");
			RulesParser    parser     = new RulesParser(validation);
			
			RuleExpressionCondition condition      = parser.ParseCondition    ("true");
			// 11/09/2010 Paul.  The rule should already start with an equals (=), so we just need to prepend the this. 
			List<RuleAction>        lstThenActions = parser.ParseStatementList("this.Value " + RULE);
			System.Workflow.Activities.Rules.Rule r = new System.Workflow.Activities.Rules.Rule(Guid.NewGuid().ToString(), condition, lstThenActions);
			r.Priority = 1;
			r.Active   = true;
			r.ReevaluationBehavior = RuleReevaluationBehavior.Never;
			rules.Rules.Add(r);
			
			rules.Validate(validation);
			if ( validation.Errors.HasErrors )
			{
				throw(new Exception(RulesUtil.GetValidationErrors(validation)));
			}
			
			WorkflowRuleThis value = new WorkflowRuleThis(PARENT_ACTIVITY, actPARENT, LVALUE);
			RuleExecution exec = new RuleExecution(validation, value);
			rules.Execute(exec);
			SetLVALUE(value.Value);
			return ActivityExecutionStatus.Closed;
		}
	}
}
