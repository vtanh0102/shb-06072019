/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Web.Caching;
using System.Workflow.Runtime.Hosting;

namespace SplendidCRM
{
	public class SplendidApplicationService: WorkflowRuntimeService
	{
		public    HttpContext Context     ;
		protected string      sCultureName;

		public string SplendidProvider
		{
			get { return Sql.ToString(Context.Application["SplendidProvider"]); }
		}

		public string ConnectionString
		{
			get { return Sql.ToString(Context.Application["ConnectionString"]); }
		}

		public string Term(string sEntryName)
		{
			return L10N.Term(Context.Application, sCultureName, sEntryName);
		}

		public object Term(string sListName, object oField)
		{
			return L10N.Term(Context.Application, sCultureName, sListName, oField);
		}

		public HttpApplicationState Application
		{
			get { return Context.Application; }
		}

		public Cache Cache
		{
			get { return Context.Cache; }
		}

		public SplendidApplicationService(HttpContext Context)
		{
			this.Context      = Context;
			this.sCultureName = SplendidDefaults.Culture(Context.Application);
		}
	}
}
