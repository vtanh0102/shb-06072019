/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Web;
using System.Data;
using System.Reflection;
using System.Workflow.Runtime;
using System.Workflow.Runtime.Tracking;
using System.Workflow.Runtime.Hosting;
using System.Workflow.ComponentModel.Compiler;
using System.Diagnostics;

namespace SplendidCRM
{
	public class WorkflowInit
	{
		public static void StartRuntime(HttpContext Context)
		{
			//string sConnectionString = Sql.ToString(Application["ConnectionString"]);
			WorkflowRuntime runtime = new WorkflowRuntime();
			// 10/09/2008 Paul.  Capture not handled exceptions. 
			runtime.ServicesExceptionNotHandled += new EventHandler<ServicesExceptionNotHandledEventArgs>(ExceptionNotHandled);

			SplendidTrackingService ts = new SplendidTrackingService(Context);
			//SqlTrackingService ts = new SqlTrackingService(sConnectionString);
			//ts.UseDefaultProfile = true;
			//ts.IsTransactional = true;
			Context.Application["SplendidTrackingService"   ] = ts;

			TimeSpan tsInstanceOwnershipDuration = new TimeSpan(0, 0, 60);
			TimeSpan tsLoadingInterval           = new TimeSpan(0, 0, 60);
			SplendidPersistenceService ps = new SplendidPersistenceService(Context, true, tsInstanceOwnershipDuration);  //, tsLoadingInterval);
			//SqlWorkflowPersistenceService ps = new SqlWorkflowPersistenceService(sConnectionString);
			Context.Application["SplendidPersistenceService"] = ps;

			// 07/15/2008 Paul.  Shared connection was required to get tracking to work.
			//SharedConnectionWorkflowCommitWorkBatchService sc = new SharedConnectionWorkflowCommitWorkBatchService(sConnectionString);
			//Application["SplendidSharedConnection"] = sc;

			// 07/29/2008 Paul.  Add this DLL to the services available to the workflow runtime. 
			TypeProvider tp = new TypeProvider(runtime);
			Assembly asm = Assembly.GetExecutingAssembly();
			#pragma warning disable 618
			//Assembly asm = Assembly.LoadWithPartialName("SplendidWorkflows");
			#pragma warning restore 618
			tp.AddAssembly(asm);
			runtime.AddService(tp);

			SplendidApplicationService sa = new SplendidApplicationService(Context);
			runtime.AddService(sa);

			runtime.AddService(ts);
			runtime.AddService(ps);
			//runtime.AddService(sc);
			runtime.StartRuntime();
			Context.Application["WorkflowRuntime"] = runtime;
		}

		static void ExceptionNotHandled(object sender, ServicesExceptionNotHandledEventArgs e)
		{
			try
			{
				WorkflowRuntime runtime = sender as WorkflowRuntime;
				if ( runtime != null )
				{
					SplendidApplicationService app = runtime.GetService<SplendidApplicationService>();
					
					string sError = Utils.ExpandException(e.Exception);
					SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), sError);
					
					DbProviderFactory dbf = DbProviderFactories.GetFactory(app.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
						using ( IDbTransaction trn = Sql.BeginTransaction(con) )
						{
							try
							{
								SqlProcs.spWORKFLOW_RUN_FailedByInstance(e.WorkflowInstanceId, "Exception Not Handled", sError, trn);
								trn.Commit();
							}
							catch
							{
								trn.Rollback();
							}
						}
					}
				}
			}
			catch
			{
			}
		}

		public static void StopRuntime(HttpApplicationState Application)
		{
			WorkflowRuntime runtime = Application["WorkflowRuntime"] as WorkflowRuntime;
			if ( runtime != null )
			{
				// 09/21/2013 Paul.  Changing a file can cause .NET 4.0 to restart the app and generate an exception when stopping the runtime. 
				// Just ignore the error as the app is being restarted. 
				try
				{
					runtime.StopRuntime();
					SplendidTrackingService ts = Application["SplendidTrackingService"] as SplendidTrackingService;
					//TrackingService ts = Application["SplendidTrackingService"] as TrackingService;
					if ( ts != null )
					{
						runtime.RemoveService(ts);
						Application.Remove("SplendidTrackingService");
					}

					SplendidPersistenceService ps = Application["SplendidPersistenceService"] as SplendidPersistenceService;
					//WorkflowPersistenceService ps = Application["SplendidPersistenceService"] as WorkflowPersistenceService;
					if ( ps != null )
					{
						runtime.RemoveService(ps);
						Application.Remove("SplendidPersistenceService");
					}

					SplendidApplicationService sa = runtime.GetService<SplendidApplicationService>();
					if ( sa != null )
					{
						runtime.RemoveService(sa);
					}

					TypeProvider tp = runtime.GetService<TypeProvider>();
					if ( tp != null )
						runtime.RemoveService(sa);

					//SharedConnectionWorkflowCommitWorkBatchService sc = Application["SplendidSharedConnection"] as SharedConnectionWorkflowCommitWorkBatchService;
					//if ( sc != null )
					//{
					//	runtime.RemoveService(sc);
					//	Application.Remove("SplendidSharedConnection");
					//}
				}
				catch
				{
				}
				Application.Remove("WorkflowRuntime");
			}
		}

		public static WorkflowRuntime GetRuntime(HttpApplicationState Application)
		{
			return Application["WorkflowRuntime"] as WorkflowRuntime;
		}

	}
}
