/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	public class SetValueActivity: Activity
	{
		protected string sOPERATOR;
		protected string sTYPE;

		public SetValueActivity()
		{
			this.Name = "SetValueActivity";
			this.sOPERATOR = "equals";
			this.sTYPE     = "object";
		}

		#region Public workflow properties
		public static DependencyProperty LVALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LVALUE", typeof(object), typeof(SetValueActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public object LVALUE
		{
			get { return ((object)(base.GetValue(SetValueActivity.LVALUEProperty))); }
			set { base.SetValue(SetValueActivity.LVALUEProperty, value); }
		}

		public static DependencyProperty RVALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RVALUE", typeof(object), typeof(SetValueActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public object RVALUE
		{
			get { return ((object)(base.GetValue(SetValueActivity.RVALUEProperty))); }
			set { base.SetValue(SetValueActivity.RVALUEProperty, value); }
		}

		public string OPERATOR
		{
			get { return sOPERATOR; }
			set { sOPERATOR = value; }
		}

		public string TYPE
		{
			get { return sTYPE; }
			set { sTYPE = value; }
		}
		#endregion

		// 10/11/2008 Paul.  We must convert the values to the correct data type, otherwise an exception will be thrown. 
		private void SetLVALUE(object o)
		{
			// 10/23/2008 Paul.  We can't get the type if the LVALUE is null. 
			if ( LVALUE != null )
			{
				Type t = LVALUE.GetType();
				if ( t == typeof(System.String) )
					LVALUE = Sql.ToString(o);
				else if ( t == typeof(System.Guid    ) )
					LVALUE = Sql.ToGuid(o);
				else if ( t == typeof(System.DateTime) )
					LVALUE = Sql.ToDateTime(o);
				else if ( t == typeof(System.Boolean ) )
					LVALUE = Sql.ToBoolean(o);
				else if ( t == typeof(System.Double  ) )
					LVALUE = Sql.ToDouble(o);
				// 10/19/2010 Paul.  Add support for System.Single. 
				else if ( t == typeof(System.Single  ) )
					LVALUE = Sql.ToFloat(o);
				else if ( t == typeof(System.Decimal ) )
					LVALUE = Sql.ToDecimal(o);
				else if ( t == typeof(System.Int16) || t == typeof(System.Int32) || t == typeof(System.Int64) )
					LVALUE = Sql.ToInteger(o);
				else
					LVALUE = o;
			}
			else
			{
				LVALUE = o;
			}
		}

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			switch ( sOPERATOR.ToLower() )
			{
				// 07/13/2010 Paul.  When creating a URL, we need to make sure that binded values are escaped. 
				// 11/01/2010 Paul.  equalsURL must be in lower case. 
				case "equalsurl":
				{
					SetLVALUE(System.Web.HttpUtility.UrlEncode(Sql.ToString(RVALUE)));
					break;
				}
				case "equals"  :
				case "="       :
				{
					SetLVALUE(RVALUE);
					break;
				}
				case "append"  :
				{
					SetLVALUE(Sql.ToString(LVALUE) + Sql.ToString(RVALUE));
					break;
				}
				// 07/13/2010 Paul.  When creating a URL, we need to make sure that binded values are escaped. 
				// 11/01/2010 Paul.  appendURL must be in lower case. 
				case "appendurl":
				{
					SetLVALUE(Sql.ToString(LVALUE) + System.Web.HttpUtility.UrlEncode(Sql.ToString(RVALUE)));
					break;
				}
				case "prepend" :
				{
					SetLVALUE(Sql.ToString(RVALUE) + Sql.ToString(LVALUE));
					break;
				}
				case "add"     :
				case "+"       :
				{
					switch ( sTYPE.ToLower() )
					{
						case "float"  :  SetLVALUE(Sql.ToFloat  (LVALUE) + Sql.ToFloat  (RVALUE));  break;
						case "double" :  SetLVALUE(Sql.ToDouble (LVALUE) + Sql.ToDouble (RVALUE));  break;
						case "int"    :  SetLVALUE(Sql.ToInteger(LVALUE) + Sql.ToInteger(RVALUE));  break;
						case "long"   :  SetLVALUE(Sql.ToLong   (LVALUE) + Sql.ToLong   (RVALUE));  break;
						case "decimal":  SetLVALUE(Sql.ToDecimal(LVALUE) + Sql.ToDecimal(RVALUE));  break;
						default       : throw(new Exception("SetValueActivity.Execute: Unknown type '" + sTYPE + "'"));
					}
					break;
				}
				case "subtract":
				case "-"       :
				{
					switch ( sTYPE.ToLower() )
					{
						case "float"  :  SetLVALUE(Sql.ToFloat  (LVALUE) - Sql.ToFloat  (RVALUE));  break;
						case "double" :  SetLVALUE(Sql.ToDouble (LVALUE) - Sql.ToDouble (RVALUE));  break;
						case "int"    :  SetLVALUE(Sql.ToInteger(LVALUE) - Sql.ToInteger(RVALUE));  break;
						case "long"   :  SetLVALUE(Sql.ToLong   (LVALUE) - Sql.ToLong   (RVALUE));  break;
						case "decimal":  SetLVALUE(Sql.ToDecimal(LVALUE) - Sql.ToDecimal(RVALUE));  break;
						default       : throw(new Exception("SetValueActivity.Execute: Unknown type '" + sTYPE + "'"));
					}
					break;
				}
				case "multiply":
				case "*"       :
				{
					switch ( sTYPE.ToLower() )
					{
						case "float"  :  SetLVALUE(Sql.ToFloat  (LVALUE) * Sql.ToFloat  (RVALUE));  break;
						case "double" :  SetLVALUE(Sql.ToDouble (LVALUE) * Sql.ToDouble (RVALUE));  break;
						case "int"    :  SetLVALUE(Sql.ToInteger(LVALUE) * Sql.ToInteger(RVALUE));  break;
						case "long"   :  SetLVALUE(Sql.ToLong   (LVALUE) * Sql.ToLong   (RVALUE));  break;
						case "decimal":  SetLVALUE(Sql.ToDecimal(LVALUE) * Sql.ToDecimal(RVALUE));  break;
						default       : throw(new Exception("SetValueActivity.Execute: Unknown type '" + sTYPE + "'"));
					}
					break;
				}
				case "divide"  :
				case "/"       :
				{
					switch ( sTYPE.ToLower() )
					{
						case "float"  :  SetLVALUE(Sql.ToFloat  (LVALUE) / Sql.ToFloat  (RVALUE));  break;
						case "double" :  SetLVALUE(Sql.ToDouble (LVALUE) / Sql.ToDouble (RVALUE));  break;
						case "int"    :  SetLVALUE(Sql.ToInteger(LVALUE) / Sql.ToInteger(RVALUE));  break;
						case "long"   :  SetLVALUE(Sql.ToLong   (LVALUE) / Sql.ToLong   (RVALUE));  break;
						case "decimal":  SetLVALUE(Sql.ToDecimal(LVALUE) / Sql.ToDecimal(RVALUE));  break;
						default       : throw(new Exception("SetValueActivity.Execute: Unknown type '" + sTYPE + "'"));
					}
					break;
				}
				default:
					throw(new Exception("SetValueActivity.Execute: Unknown operator '" + sOPERATOR + "'"));
			}
			return ActivityExecutionStatus.Closed;
		}
	}
}
