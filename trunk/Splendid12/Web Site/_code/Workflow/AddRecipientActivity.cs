/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Collections.Generic;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Net.Mail;
using System.Diagnostics;

namespace SplendidCRM
{
	public class AddRecipientActivity: Activity
	{
		protected string sALERT_NAME    ;
		protected string sRECIPIENT_NAME;
		protected string sRECIPIENT_TYPE;
		protected string sSEND_TYPE     ;
		// 06/20/2014 Paul.  Allow text columns to be added to the list of recipients. 
		protected string sRECIPIENT_TABLE;
		protected string sRECIPIENT_FIELD;

		public AddRecipientActivity()
		{
			this.Name = "AddRecipientActivity";
			this.sRECIPIENT_TYPE = "User";
			this.sSEND_TYPE      = "to"  ;
		}

		#region Public workflow properties
		public static DependencyProperty RECIPIENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RECIPIENT_ID", typeof(Guid), typeof(AddRecipientActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid RECIPIENT_ID
		{
			get { return ((Guid)(base.GetValue(AddRecipientActivity.RECIPIENT_IDProperty))); }
			set { base.SetValue(AddRecipientActivity.RECIPIENT_IDProperty, value); }
		}

		public string ALERT_NAME
		{
			get { return sALERT_NAME; }
			set { sALERT_NAME = value; }
		}

		public string RECIPIENT_NAME
		{
			get { return sRECIPIENT_NAME; }
			set { sRECIPIENT_NAME = value; }
		}

		public string RECIPIENT_TYPE
		{
			get { return sRECIPIENT_TYPE; }
			set { sRECIPIENT_TYPE = value; }
		}

		public string SEND_TYPE
		{
			get { return sSEND_TYPE; }
			set { sSEND_TYPE = value; }
		}

		// 06/20/2014 Paul.  Allow text columns to be added to the list of recipients. 
		public string RECIPIENT_TABLE
		{
			get { return sRECIPIENT_TABLE; }
			set { sRECIPIENT_TABLE = value; }
		}

		public string RECIPIENT_FIELD
		{
			get { return sRECIPIENT_FIELD; }
			set { sRECIPIENT_FIELD = value; }
		}

		#endregion

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			if ( !Sql.IsEmptyString(sALERT_NAME) )
			{
				Activity act = this.Parent.GetActivityByName(sALERT_NAME);
				if ( act != null )
				{
					AlertActivity actAlert = act as AlertActivity;
					if ( actAlert != null )
					{
						SplendidApplicationService app = executionContext.GetService<SplendidApplicationService>();
						DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							string sSQL ;
							switch ( RECIPIENT_TYPE.ToLower() )
							{
								case "team":
								{
									sSQL = "select USER_ID                " + ControlChars.CrLf
									     + "     , FULL_NAME              " + ControlChars.CrLf
									     + "     , EMAIL1                 " + ControlChars.CrLf
									     + "  from vwTEAM_MEMBERSHIPS_List" + ControlChars.CrLf
									     + " where TEAM_ID = @TEAM_ID     " + ControlChars.CrLf
									     + "   and EMAIL1 is not null     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@TEAM_ID", RECIPIENT_ID);
										// 07/08/2010 Paul.  We also need to return all rows.  Not just a single row. 
										using ( IDataReader rdr = cmd.ExecuteReader() )
										{
											// 01/19/2010 Paul.  We need to use a while loop to send to all team members. 
											while ( rdr.Read() )
											{
												Guid   gPARENT_ID = Sql.ToGuid  (rdr["USER_ID"  ]);
												string sFULL_NAME = Sql.ToString(rdr["FULL_NAME"]);
												string sEMAIL1    = Sql.ToString(rdr["EMAIL1"   ]);
												// 10/11/2008 Paul.  Reduce send errors by only sending to valid email addresses. 
												if ( EmailUtils.IsValidEmail(sEMAIL1) )
												{
													AlertRecipient r = new AlertRecipient(gPARENT_ID, "Users", sFULL_NAME, sEMAIL1, SEND_TYPE);
													actAlert.RECIPIENTS.Add(r);
												}
											}
										}
									}
									break;
								}
								case "role":
								{
									// 10/30/2009 Paul.  We should be using ACL Roles, not the old deprecated ROLES_USERS table. 
									sSQL = "select USER_ID           " + ControlChars.CrLf
									     + "     , FULL_NAME         " + ControlChars.CrLf
									     + "     , EMAIL1            " + ControlChars.CrLf
									     + "  from vwACL_ROLES_USERS " + ControlChars.CrLf
									     + " where ROLE_ID = @ROLE_ID" + ControlChars.CrLf
									     + "   and EMAIL1 is not null" + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ROLE_ID", RECIPIENT_ID);
										// 07/08/2010 Paul.  We also need to return all rows.  Not just a single row. 
										using ( IDataReader rdr = cmd.ExecuteReader() )
										{
											// 01/19/2010 Paul.  We need to use a while loop to send to all role members. 
											while ( rdr.Read() )
											{
												Guid   gPARENT_ID = Sql.ToGuid  (rdr["USER_ID"  ]);
												string sFULL_NAME = Sql.ToString(rdr["FULL_NAME"]);
												string sEMAIL1    = Sql.ToString(rdr["EMAIL1"   ]);
												// 10/11/2008 Paul.  Reduce send errors by only sending to valid email addresses. 
												if ( EmailUtils.IsValidEmail(sEMAIL1) )
												{
													AlertRecipient r = new AlertRecipient(gPARENT_ID, "Users", sFULL_NAME, sEMAIL1, SEND_TYPE);
													actAlert.RECIPIENTS.Add(r);
												}
											}
										}
									}
									break;
								}
								case "record":
								{
									// 11/17/2008 Paul.  FULL_NAME is not available.  use PARENT_NAME instead. 
									sSQL = "select PARENT_ID              " + ControlChars.CrLf
									     + "     , PARENT_TYPE            " + ControlChars.CrLf
									     + "     , PARENT_NAME            " + ControlChars.CrLf
									     + "     , EMAIL1                 " + ControlChars.CrLf
									     + "  from vwPARENTS_EMAIL_ADDRESS" + ControlChars.CrLf
									     + " where PARENT_ID = @PARENT_ID " + ControlChars.CrLf
									     + "   and EMAIL1 is not null     " + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@PARENT_ID", RECIPIENT_ID);
										using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
										{
											if ( rdr.Read() )
											{
												Guid   gPARENT_ID   = Sql.ToGuid  (rdr["PARENT_ID"  ]);
												string sPARENT_TYPE = Sql.ToString(rdr["PARENT_TYPE"]);
												string sFULL_NAME   = Sql.ToString(rdr["PARENT_NAME"]);
												string sEMAIL1      = Sql.ToString(rdr["EMAIL1"     ]);
												// 10/11/2008 Paul.  Reduce send errors by only sending to valid email addresses. 
												if ( EmailUtils.IsValidEmail(sEMAIL1) )
												{
													AlertRecipient r = new AlertRecipient(gPARENT_ID, sPARENT_TYPE, sFULL_NAME, sEMAIL1, SEND_TYPE);
													actAlert.RECIPIENTS.Add(r);
												}
											}
										}
									}
									break;
								}
								// 06/20/2014 Paul.  Allow text columns to be added to the list of recipients. 
								case "record_custom":
								{
									Guid gRECIPIENT_ID = RECIPIENT_ID;
									// 12/26/2017 Paul.  Add ASSIGNED_SET_ID and TEAM_SET_ID. 
									if ( !Sql.IsEmptyGuid(gRECIPIENT_ID) && sRECIPIENT_FIELD == "ASSIGNED_SET_ID" )
									{
										sSQL = "select vwUSERS.ID                                     " + ControlChars.CrLf
										     + "     , vwUSERS.FULL_NAME                              " + ControlChars.CrLf
										     + "     , vwUSERS.EMAIL1                                 " + ControlChars.CrLf
										     + "  from      vwASSIGNED_SETS_USERS                     " + ControlChars.CrLf
										     + " inner join vwUSERS                                   " + ControlChars.CrLf
										     + "         on vwUSERS.ID = vwASSIGNED_SETS_USERS.USER_ID" + ControlChars.CrLf
										     + "        and vwUSERS.EMAIL1 is not null                " + ControlChars.CrLf
										     + " where vwASSIGNED_SETS_USERS.ID = @ASSIGNED_SET_ID    " + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Sql.AddParameter(cmd, "@ASSIGNED_SET_ID", gRECIPIENT_ID);
											using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
											{
												if ( rdr.Read() )
												{
													Guid   gPARENT_ID = Sql.ToGuid  (rdr["ID"       ]);
													string sFULL_NAME = Sql.ToString(rdr["FULL_NAME"]);
													string sEMAIL1    = Sql.ToString(rdr["EMAIL1"   ]);
													if ( EmailUtils.IsValidEmail(sEMAIL1) )
													{
														AlertRecipient r = new AlertRecipient(gPARENT_ID, "Users", sFULL_NAME, sEMAIL1, SEND_TYPE);
														actAlert.RECIPIENTS.Add(r);
													}
												}
											}
										}
									}
									else if ( !Sql.IsEmptyGuid(gRECIPIENT_ID) && sRECIPIENT_FIELD == "TEAM_SET_ID" )
									{
										sSQL = "select vwTEAM_MEMBERSHIPS_List.USER_ID                " + ControlChars.CrLf
										     + "     , vwTEAM_MEMBERSHIPS_List.FULL_NAME              " + ControlChars.CrLf
										     + "     , vwTEAM_MEMBERSHIPS_List.EMAIL1                 " + ControlChars.CrLf
										     + "  from      vwTEAM_SETS_TEAMS                         " + ControlChars.CrLf
										     + " inner join vwTEAM_MEMBERSHIPS_List                   " + ControlChars.CrLf
										     + "         on vwTEAM_MEMBERSHIPS_List.TEAM_ID = vwTEAM_SETS_TEAMS.TEAM_ID" + ControlChars.CrLf
										     + "        and vwTEAM_MEMBERSHIPS_List.EMAIL1 is not null" + ControlChars.CrLf
										     + " where vwTEAM_SETS_TEAMS.ID = @TEAM_SET_ID            " + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Sql.AddParameter(cmd, "@TEAM_SET_ID", gRECIPIENT_ID);
											using ( IDataReader rdr = cmd.ExecuteReader() )
											{
												while ( rdr.Read() )
												{
													Guid   gPARENT_ID = Sql.ToGuid  (rdr["USER_ID"  ]);
													string sFULL_NAME = Sql.ToString(rdr["FULL_NAME"]);
													string sEMAIL1    = Sql.ToString(rdr["EMAIL1"   ]);
													if ( EmailUtils.IsValidEmail(sEMAIL1) )
													{
														AlertRecipient r = new AlertRecipient(gPARENT_ID, "Users", sFULL_NAME, sEMAIL1, SEND_TYPE);
														actAlert.RECIPIENTS.Add(r);
													}
												}
											}
										}
									}
									else if ( !Sql.IsEmptyGuid(gRECIPIENT_ID) && !Sql.IsEmptyString(sRECIPIENT_TABLE) && !Sql.IsEmptyString(sRECIPIENT_FIELD) )
									{
										System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^A-Za-z0-9_]");
										sRECIPIENT_TABLE = reg.Replace(sRECIPIENT_TABLE, "");
										sRECIPIENT_FIELD = reg.Replace(sRECIPIENT_FIELD, "");
										if ( sRECIPIENT_TABLE.EndsWith("_AUDIT_OLD") )
											sRECIPIENT_TABLE = sRECIPIENT_TABLE.Substring(sRECIPIENT_TABLE.Length - 4);
										
										sSQL = "select *       " + ControlChars.CrLf
										     + "  from vw" + sRECIPIENT_TABLE + ControlChars.CrLf
										     + " where ID = @ID" + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Sql.AddParameter(cmd, "@ID", gRECIPIENT_ID);
											using ( DbDataAdapter da = dbf.CreateDataAdapter() )
											{
												((IDbDataAdapter)da).SelectCommand = cmd;
												using ( DataTable dt = new DataTable() )
												{
													da.Fill(dt);
													// 06/20/2014 Paul.  Delay the field lookup until after getting the data to prevent a SQL exception. 
													if ( dt.Rows.Count > 0 && dt.Columns.Contains(sRECIPIENT_FIELD) )
													{
														DataRow row = dt.Rows[0];
														string sEMAIL1 = Sql.ToString(row[sRECIPIENT_FIELD]);
														if ( EmailUtils.IsValidEmail(sEMAIL1) )
														{
															AlertRecipient r = new AlertRecipient(gRECIPIENT_ID, sRECIPIENT_TABLE, String.Empty, sEMAIL1, SEND_TYPE);
															actAlert.RECIPIENTS.Add(r);
														}
													}
												}
											}
										}
									}
									break;
								}
								default:
								{
									sSQL = "select ID                " + ControlChars.CrLf
									     + "     , FULL_NAME         " + ControlChars.CrLf
									     + "     , EMAIL1            " + ControlChars.CrLf
									     + "  from vwUSERS           " + ControlChars.CrLf
									     + " where ID = @ID          " + ControlChars.CrLf
									     + "   and EMAIL1 is not null" + ControlChars.CrLf;
									using ( IDbCommand cmd = con.CreateCommand() )
									{
										cmd.CommandText = sSQL;
										Sql.AddParameter(cmd, "@ID", RECIPIENT_ID);
										using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
										{
											if ( rdr.Read() )
											{
												Guid   gPARENT_ID = Sql.ToGuid  (rdr["ID"       ]);
												string sFULL_NAME = Sql.ToString(rdr["FULL_NAME"]);
												string sEMAIL1    = Sql.ToString(rdr["EMAIL1"   ]);
												// 10/11/2008 Paul.  Reduce send errors by only sending to valid email addresses. 
												if ( EmailUtils.IsValidEmail(sEMAIL1) )
												{
													AlertRecipient r = new AlertRecipient(gPARENT_ID, "Users", sFULL_NAME, sEMAIL1, SEND_TYPE);
													actAlert.RECIPIENTS.Add(r);
												}
											}
										}
									}
									break;
								}
							}
						}
					}
					else
					{
						throw(new Exception(sALERT_NAME + " not found."));
					}
				}
				else
				{
					throw(new Exception(sALERT_NAME + " not found."));
				}
			}
			else
			{
				throw(new Exception("ALERT_NAME  not specified."));
			}
			return ActivityExecutionStatus.Closed;
		}
	}
}
