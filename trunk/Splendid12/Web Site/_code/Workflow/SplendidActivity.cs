/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	public abstract class SplendidActivity: SequenceActivity
	{
		[NonSerialized]
		protected SplendidApplicationService app;

		public SplendidActivity()
		{
			this.Name = "SplendidActivity";
		}

		#region Public workflow properties
		// 07/30/2008 Paul.  The property name must match the actual name with Property appended, otherwise we get the following error. 
		// error 347: CreateInstance failed for type 'SplendidCRM.AccountActivity'. Exception has been thrown by the target of an invocation..
		public static DependencyProperty AUDIT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AUDIT_ID", typeof(Guid), typeof(SplendidActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid AUDIT_ID
		{
			get { return ((Guid)(base.GetValue(SplendidActivity.AUDIT_IDProperty))); }
			set { base.SetValue(SplendidActivity.AUDIT_IDProperty, value); }
		}

		public static DependencyProperty WORKFLOW_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORKFLOW_ID", typeof(Guid), typeof(SplendidActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid WORKFLOW_ID
		{
			get { return ((Guid)(base.GetValue(SplendidActivity.WORKFLOW_IDProperty))); }
			set { base.SetValue(SplendidActivity.WORKFLOW_IDProperty, value); }
		}
		#endregion

		public void LoadByID(object sender, EventArgs e)
		{
			Load(false, false);
		}

		public void LoadByAUDIT_ID(object sender, EventArgs e)
		{
			Load(true, false);
		}

		public void LoadPastByAUDIT_ID(object sender, EventArgs e)
		{
			Load(true, true);
		}

		public void Save(object sender, EventArgs e)
		{
			Save();
		}

		protected abstract void Load(bool bAudit, bool bPast);

		protected abstract void Save();

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			app = executionContext.GetService<SplendidApplicationService>();
			// 09/16/2015 Paul.  Change to Debug as it is automatically not included in a release build. 
			Debug.WriteLine("ActivityExecutionStatus Execute: AUDIT_ID = " + AUDIT_ID.ToString());

			return base.Execute(executionContext);
		}

		// 06/12/2016 Paul.  OnActivityExecutionContextLoad is the better place to load the context as it works when resuming from idle, like when DelayActivity is used. 
		// We don't really need the above Execute override, but we will not remove as we want to change the least. 
		protected override void OnActivityExecutionContextLoad(IServiceProvider provider)
		{
			Debug.WriteLine("OnActivityExecutionContextLoad: AUDIT_ID = " + AUDIT_ID.ToString());
			if ( app == null )
				app = provider.GetService(typeof(SplendidApplicationService)) as SplendidApplicationService;
			base.OnActivityExecutionContextLoad(provider);
		}
	}
}
