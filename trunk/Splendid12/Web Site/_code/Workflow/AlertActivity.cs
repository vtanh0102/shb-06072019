/**
 * Copyright (C) 2008-2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Web;
using System.Diagnostics;

namespace SplendidCRM
{
	[Serializable]
	public class AlertRecipient
	{
		// 11/19/2008 Paul.  When sending an email, we need the record or user ID. 
		protected Guid   gRecipientID  ;
		protected string sRecipientType;
		protected string sDisplayName  ;
		protected string sAddress      ;
		protected string sSendType     ;

		public Guid RecipientID
		{
			get { return gRecipientID; }
			set { gRecipientID = value; }
		}

		public string RecipientType
		{
			get { return sRecipientType; }
			set { sRecipientType = value; }
		}

		public string DisplayName
		{
			get { return sDisplayName; }
			set { sDisplayName = value; }
		}

		public string Address
		{
			get { return sAddress; }
			set { sAddress = value; }
		}

		public string SendType
		{
			get { return sSendType; }
			set { sSendType = value; }
		}

		public AlertRecipient(Guid gRecipientID, string sRecipientType, string sDisplayName, string sAddress, string sSendType)
		{
			this.gRecipientID   = gRecipientID  ;
			this.sRecipientType = sRecipientType;
			this.sDisplayName   = sDisplayName  ;
			this.sAddress       = sAddress      ;
			this.sSendType      = sSendType     ;
		}
	}

	[Serializable]
	public class AlertAttachment
	{
		protected string sFileName     ;
		protected string sExtension    ;
		protected string sFileMimeType ;
		protected byte[] byContent     ;

		public string FileName
		{
			get { return sFileName; }
			set { sFileName = value; }
		}

		public string Extension
		{
			get { return sExtension; }
			set { sExtension = value; }
		}

		public string FileMimeType
		{
			get { return sFileMimeType; }
			set { sFileMimeType = value; }
		}

		public byte[] Content
		{
			get { return byContent; }
			set { byContent = value; }
		}

		public AlertAttachment(string sFileName, string sExtension, string sFileMimeType, byte[] byContent)
		{
			this.sFileName     = sFileName    ;
			this.sExtension    = sExtension   ;
			this.sFileMimeType = sFileMimeType;
			this.byContent     = byContent    ;
		}
	}

	public class AlertActivity: SequenceActivity
	{
		[NonSerialized]
		protected SplendidApplicationService app ;
		protected List<AlertRecipient>       lstRECIPIENTS ;
		// 06/26/2010 Paul.  The attachments seem to be causing persistence errors.  Lets try not serializing them. 
		[NonSerialized]
		protected List<AlertAttachment>      lstATTACHMENTS;

		public AlertActivity()
		{
			this.Name = "AlertActivity";
			this.lstRECIPIENTS  = new List<AlertRecipient>();
			this.lstATTACHMENTS = new List<AlertAttachment>();
		}

		#region Public workflow properties
		public static DependencyProperty WORKFLOW_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORKFLOW_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid WORKFLOW_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.WORKFLOW_IDProperty))); }
			set { base.SetValue(AlertActivity.WORKFLOW_IDProperty, value); }
		}

		public static DependencyProperty SUBJECTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUBJECT", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUBJECT
		{
			get { return ((string)(base.GetValue(AlertActivity.SUBJECTProperty))); }
			set { base.SetValue(AlertActivity.SUBJECTProperty, value); }
		}

		public static DependencyProperty FROM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_NAME", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_NAME
		{
			get { return ((string)(base.GetValue(AlertActivity.FROM_NAMEProperty))); }
			set { base.SetValue(AlertActivity.FROM_NAMEProperty, value); }
		}

		public static DependencyProperty FROM_ADDRESSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_ADDRESS", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_ADDRESS
		{
			get { return ((string)(base.GetValue(AlertActivity.FROM_ADDRESSProperty))); }
			set { base.SetValue(AlertActivity.FROM_ADDRESSProperty, value); }
		}

		public static DependencyProperty ALERT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALERT_TYPE", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALERT_TYPE
		{
			get { return ((string)(base.GetValue(AlertActivity.ALERT_TYPEProperty))); }
			set { base.SetValue(AlertActivity.ALERT_TYPEProperty, value); }
		}

		public static DependencyProperty ALERT_TEXTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ALERT_TEXT", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ALERT_TEXT
		{
			get { return ((string)(base.GetValue(AlertActivity.ALERT_TEXTProperty))); }
			set { base.SetValue(AlertActivity.ALERT_TEXTProperty, value); }
		}

		public static DependencyProperty SOURCE_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SOURCE_TYPE", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SOURCE_TYPE
		{
			get { return ((string)(base.GetValue(AlertActivity.SOURCE_TYPEProperty))); }
			set { base.SetValue(AlertActivity.SOURCE_TYPEProperty, value); }
		}

		public static DependencyProperty CUSTOM_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CUSTOM_TEMPLATE_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CUSTOM_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.CUSTOM_TEMPLATE_IDProperty))); }
			set { base.SetValue(AlertActivity.CUSTOM_TEMPLATE_IDProperty, value); }
		}

		// 11/22/2008 Paul.  Also set the AUDIT_ID so that we can get the history. 
		public static DependencyProperty AUDIT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AUDIT_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid AUDIT_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.AUDIT_IDProperty))); }
			set { base.SetValue(AlertActivity.AUDIT_IDProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.PARENT_IDProperty))); }
			set { base.SetValue(AlertActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(AlertActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(AlertActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_ACTIVITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ACTIVITY", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_ACTIVITY
		{
			get { return ((string)(base.GetValue(AlertActivity.PARENT_ACTIVITYProperty))); }
			set { base.SetValue(AlertActivity.PARENT_ACTIVITYProperty, value); }
		}

		// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(AlertActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(AlertActivity.TEAM_IDProperty))); }
			set { base.SetValue(AlertActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(AlertActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(AlertActivity.TEAM_SET_LISTProperty, value); }
		}

		// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(AlertActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(AlertActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(AlertActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		#endregion

		public List<AlertRecipient> RECIPIENTS
		{
			get { return lstRECIPIENTS; }
			set { lstRECIPIENTS = value; }
		}

		public List<AlertAttachment> ATTACHMENTS
		{
			get { return lstATTACHMENTS; }
			set { lstATTACHMENTS= value; }
		}

		private void AddInsertionValues(HttpApplicationState Application, Hashtable hash, string sFieldName, object oValue, string sAge, string sModuleName, string sRelationshipName, DataView vwColumns, string sSiteURL)
		{
			if ( oValue.GetType() == typeof(System.DateTime) )
			{
				if ( Sql.ToDateTime(oValue) == DateTime.MinValue )
					oValue = String.Empty;
			}
			else if ( oValue.GetType() == typeof(System.String) )
			{
				// 10/11/2008 Paul.  Convert list values using the default culture. 
				vwColumns.RowFilter = "ColumnName = '" + sFieldName + "' and CsType = 'enum'";
				if ( vwColumns.Count > 0 )
				{
					string sListName = String.Empty;
					if ( Sql.IsEmptyString(sRelationshipName) )
						sListName = SplendidCache.ReportingFilterColumnsListName(Application, sModuleName, sFieldName);
					else
						sListName = SplendidCache.ReportingFilterColumnsListName(Application, sRelationshipName, sFieldName);
					// 04/23/2011 Paul.  A list name must have leading and trailing dots. 
					oValue = app.Term("." + sListName + ".", oValue);
				}
			}
			if ( Sql.IsEmptyString(sRelationshipName) )
			{
				string sInsertion = sAge + "::" + sModuleName + "::" + sFieldName.ToLower();
				if ( hash.ContainsKey(sInsertion) )
				{
					hash[sInsertion] = oValue;
				}
				if ( sAge == "future" )
				{
					if ( sFieldName.ToUpper() == "ID" )
					{
						sInsertion = "href_link::" + sModuleName + "::href_link";
						if ( hash.ContainsKey(sInsertion) )
						{
							string sViewURL = sSiteURL + sModuleName + "/view.aspx?ID=" + Sql.ToString(oValue);
							// 12/16/2009 Paul.  Project and ProjectTask require special handling as the folder names are plural. 
							if ( sModuleName == "Project" || sModuleName == "ProjectTask" )
							{
								sViewURL = sSiteURL + sModuleName + "s/view.aspx?ID=" + Sql.ToString(oValue);
							}
							hash[sInsertion] = "<a href=\"" + sViewURL + "\">" + sViewURL + "</a>";
						}
					}
				}
			}
			else
			{
				string sInsertion = sAge + "::" + sModuleName + "::" + sRelationshipName + "::" + sFieldName.ToLower();
				if ( hash.ContainsKey(sInsertion) )
				{
					hash[sInsertion] = oValue;
				}
				if ( sAge == "future" )
				{
					if ( sFieldName.ToUpper() == "ID" )
					{
						sInsertion = "href_link::" + sModuleName + "::" + sRelationshipName + "::href_link";
						if ( hash.ContainsKey(sInsertion) )
						{
							string sViewURL = sSiteURL + sRelationshipName + "/view.aspx?ID=" + Sql.ToString(oValue);
							// 12/16/2009 Paul.  Project and ProjectTask require special handling as the folder names are plural. 
							if ( sRelationshipName == "Project" || sRelationshipName == "ProjectTask" )
							{
								sViewURL = sSiteURL + sRelationshipName + "s/view.aspx?ID=" + Sql.ToString(oValue);
							}
							hash[sInsertion] = "<a href=\"" + sViewURL + "\">" + sViewURL + "</a>";
						}
					}
				}
			}
		}

		public void Send(object sender, EventArgs e)
		{
			if ( lstRECIPIENTS.Count > 0 )
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();

					// 11/25/2008 Paul.  Before loading past and future values, first determine if they are necessary. 
					string sFromName     = FROM_NAME   ;
					string sFromAddress  = FROM_ADDRESS;
					string sSubject      = SUBJECT     ;
					string sBodyHtml     = ALERT_TEXT  ;
					sBodyHtml = sBodyHtml.Replace(ControlChars.CrLf, "<br />" + ControlChars.CrLf);
					DataTable dtTemplateAttachments = null;
					if ( SOURCE_TYPE == "custom template" && !Sql.IsEmptyGuid(CUSTOM_TEMPLATE_ID) )
					{
						string sSQL = String.Empty;
						sSQL = "select FROM_ADDR                 " + ControlChars.CrLf
						     + "     , FROM_NAME                 " + ControlChars.CrLf
						     + "     , SUBJECT                   " + ControlChars.CrLf
						     + "     , BODY_HTML                 " + ControlChars.CrLf
						     + "  from vwWORKFLOW_ALERT_TEMPLATES" + ControlChars.CrLf
						     + " where ID = @ID                  " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", CUSTOM_TEMPLATE_ID);
							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									// 11/25/2008 Paul.  The template from address can over-ride the workflow values. 
									sFromName    = Sql.ToString(rdr["FROM_NAME"]);
									sFromAddress = Sql.ToString(rdr["FROM_ADDR"]);
									sSubject     = Sql.ToString(rdr["SUBJECT"  ]);
									sBodyHtml    = Sql.ToString(rdr["BODY_HTML"]);
								}
							}
						}
						// 06/16/2010 Paul.  Add support for Workflow Alert Template Attachments. 
						// 06/16/2010 Paul.  Use the same EmailTemplates parent type. 
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							sSQL = "select *                                     " + ControlChars.CrLf
							     + "  from vwEMAIL_TEMPLATES_Attachments         " + ControlChars.CrLf
							     + " where EMAIL_TEMPLATE_ID = @EMAIL_TEMPLATE_ID" + ControlChars.CrLf;
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@EMAIL_TEMPLATE_ID", CUSTOM_TEMPLATE_ID);
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dtTemplateAttachments = new DataTable();
								da.Fill(dtTemplateAttachments);
							}
						}
					}
					// 12/04/2008 Paul.  Only use config settings if not provided in XOML and not provided in alert template. 
					if ( Sql.IsEmptyString(sFromAddress) )
					{
						sFromName    = Sql.ToString(app.Application["CONFIG.fromname"    ]);
						sFromAddress = Sql.ToString(app.Application["CONFIG.fromaddress" ]);
					}

					string sSiteURL = Crm.Config.SiteURL(app.Application);
					//string sViewURL = sSiteURL + PARENT_TYPE + "/view.aspx?ID=";
					//string sEditURL = sSiteURL + PARENT_TYPE + "/edit.aspx?ID=";
					//string sListURL = sSiteURL + PARENT_TYPE + "/default.aspx" ;

					bool bLoadPastValues      = false;
					bool bLoadFutureValues    = false;
					bool bLoadPastRelValues   = false;
					bool bLoadFutureRelValues = false;
					StringCollection arrRelationships         = new StringCollection();
					Hashtable hashInsertionStrings     = new Hashtable();
					Hashtable hashRelationshipsStrings = new Hashtable();
					int nInsertionStart = sSubject.IndexOf("{::");
					while ( nInsertionStart >= 0 && nInsertionStart < sSubject.Length )
					{
						nInsertionStart += 3;
						int nInsertionEnd = sSubject.IndexOf("::}", nInsertionStart);
						if ( nInsertionEnd > nInsertionStart )
						{
							string sInsertion = sSubject.Substring(nInsertionStart, nInsertionEnd - nInsertionStart);
							if ( !Sql.IsEmptyString(sInsertion) )
							{
								if ( !hashInsertionStrings.ContainsKey(sInsertion) )
								{
									string[] arrInsertionParts = Strings.Split(sInsertion, "::", -1, CompareMethod.Text);
									if ( arrInsertionParts.Length == 3 )
									{
										// 11/25/2008 Paul.  Regular insertions will have 3 parts. 
										// {::href_link::Accounts::href_link::}
										// {::past::Accounts::id::}
										// {::future::Accounts::id::}
										hashInsertionStrings.Add(sInsertion, null);
										if ( String.Compare(arrInsertionParts[0], "href_link", true) == 0 )
											bLoadFutureValues = true;
										else if ( String.Compare(arrInsertionParts[0], "past", true) == 0 )
											bLoadPastValues = true;
										else if ( String.Compare(arrInsertionParts[0], "future", true) == 0 )
											bLoadFutureValues = true;
									}
									else if ( arrInsertionParts.Length == 4 )
									{
										// 11/25/2008 Paul.  Relationships will have 4 parts. 
										// {::href_link::Accounts::Bugs::href_link::}
										// {::past::Accounts::Bugs::id::}
										// {::future::Accounts::Bugs::id::}
										hashRelationshipsStrings.Add(sInsertion, null);
										if ( !arrRelationships.Contains(arrInsertionParts[2]) )
										{
											arrRelationships.Add(arrInsertionParts[2]);
										}
										if ( String.Compare(arrInsertionParts[0], "href_link", true) == 0 )
											bLoadFutureRelValues = true;
										else if ( String.Compare(arrInsertionParts[0], "past", true) == 0 )
											bLoadPastRelValues = true;
										else if ( String.Compare(arrInsertionParts[0], "future", true) == 0 )
											bLoadFutureRelValues = true;
									}
								}
							}
							nInsertionStart = nInsertionEnd + 3;
						}
						nInsertionStart = sSubject.IndexOf("{::", nInsertionStart);
					}
					nInsertionStart =  sBodyHtml.IndexOf("{::");
					while ( nInsertionStart >= 0 && nInsertionStart < sBodyHtml.Length )
					{
						nInsertionStart += 3;
						int nInsertionEnd = sBodyHtml.IndexOf("::}", nInsertionStart);
						if ( nInsertionEnd > nInsertionStart )
						{
							string sInsertion = sBodyHtml.Substring(nInsertionStart, nInsertionEnd - nInsertionStart);
							if ( !Sql.IsEmptyString(sInsertion) )
							{
								if ( !hashInsertionStrings.ContainsKey(sInsertion) )
								{
									string[] arrInsertionParts = Strings.Split(sInsertion, "::", -1, CompareMethod.Text);
									if ( arrInsertionParts.Length == 3 )
									{
										// 11/25/2008 Paul.  Regular insertions will have 3 parts. 
										// {::href_link::Accounts::href_link::}
										// {::past::Accounts::id::}
										// {::future::Accounts::id::}
										hashInsertionStrings.Add(sInsertion, null);
										if ( String.Compare(arrInsertionParts[0], "href_link", true) == 0 )
											bLoadFutureValues = true;
										else if ( String.Compare(arrInsertionParts[0], "past", true) == 0 )
											bLoadPastValues = true;
										else if ( String.Compare(arrInsertionParts[0], "future", true) == 0 )
											bLoadFutureValues = true;
									}
									else if ( arrInsertionParts.Length == 4 )
									{
										// 11/25/2008 Paul.  Relationships will have 4 parts. 
										// {::href_link::Accounts::Bugs::href_link::}
										// {::past::Accounts::Bugs::id::}
										// {::future::Accounts::Bugs::id::}
										hashRelationshipsStrings.Add(sInsertion, null);
										if ( !arrRelationships.Contains(arrInsertionParts[2]) )
										{
											arrRelationships.Add(arrInsertionParts[2]);
										}
										if ( String.Compare(arrInsertionParts[0], "href_link", true) == 0 )
											bLoadFutureRelValues = true;
										else if ( String.Compare(arrInsertionParts[0], "past", true) == 0 )
											bLoadPastRelValues = true;
										else if ( String.Compare(arrInsertionParts[0], "future", true) == 0 )
											bLoadFutureRelValues = true;
									}
								}
							}
							nInsertionStart = nInsertionEnd + 3;
						}
						nInsertionStart = sBodyHtml.IndexOf("{::", nInsertionStart);
					}

					string sMODULE_NAME  = PARENT_TYPE;
					string sMODULE_TABLE = Sql.ToString(app.Application["Modules." + PARENT_TYPE + ".TableName"]);
					if ( !Sql.IsEmptyString(sMODULE_TABLE) )
					{
						DataView vwColumns = new DataView(SplendidCache.WorkflowFilterColumns(app.Application, sMODULE_TABLE));
						// 11/25/2008 Paul.  First try and load the values from the current audit record. 
						if ( !Sql.IsEmptyGuid(PARENT_ID) && !Sql.IsEmptyGuid(AUDIT_ID) )
						{
							if ( bLoadFutureValues )
							{
								string sSQL = String.Empty;
								// 06/09/2009 Paul.  The base view has relationship fields that we may need to access, such as Shipper Account Name. 
								// While it may make sense not to use the audit table at all, we will continue to do so as the audit record is the most accurate. 
								// It is possible that the main record has changed before the workflow runs. 
								sSQL = "select *                            " + ControlChars.CrLf
								     + "  from vw" + sMODULE_TABLE            + ControlChars.CrLf
								     + " where ID       = @ID               " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@ID"      , PARENT_ID);
									using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
									{
										if ( rdr.Read() )
										{
											for ( int i = 0; i < rdr.FieldCount; i++ )
											{
												string sFieldName = rdr.GetName(i);
												object oValue = !rdr.IsDBNull(i) ? rdr.GetValue(i) : String.Empty;
												AddInsertionValues(app.Application, hashInsertionStrings, sFieldName, oValue, "future", sMODULE_NAME, String.Empty, vwColumns, sSiteURL);
											}
										}
									}
								}

								sSQL = "select *                            " + ControlChars.CrLf
								     + "  from vw" + sMODULE_TABLE + "_AUDIT" + ControlChars.CrLf
								     + " where ID       = @ID               " + ControlChars.CrLf
								     + "   and AUDIT_ID = @AUDIT_ID         " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@ID"      , PARENT_ID);
									Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID );
									using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
									{
										if ( rdr.Read() )
										{
											for ( int i = 0; i < rdr.FieldCount; i++ )
											{
												string sFieldName = rdr.GetName(i);
												object oValue = !rdr.IsDBNull(i) ? rdr.GetValue(i) : String.Empty;
												AddInsertionValues(app.Application, hashInsertionStrings, sFieldName, oValue, "future", sMODULE_NAME, String.Empty, vwColumns, sSiteURL);
											}
										}
									}
								}
							}
						}
						else if ( !Sql.IsEmptyString(PARENT_ACTIVITY) )
						{
							// 10/04/2008 Paul.  Use reflection to get the parent values as the Workflow engine does not appear to provide a solution. 
							// 11/25/2008 Paul.  If we do not have an audit ID, then use the parent activity. 
							// It may not make sense to conitnue to load values from the activity, but lets keep the code anyway. 
							Activity act = this.Parent.GetActivityByName(PARENT_ACTIVITY);
							if ( act != null )
							{
								PropertyInfo[] pis = act.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
								foreach ( PropertyInfo pi in pis )
								{
									// 10/06/2008 Paul.  Table fields will be in all upper case. 
									if ( pi.Name == pi.Name.ToUpper() )
									{
										// 11/09/2010 Paul.  Should convert to GetValue, but then we would need to retest.
										// object oValue = pi.GetValue(act, null);
										object oValue = pi.GetGetMethod().Invoke(act, null);
										// 11/16/2008 Paul.  The field might not be found, possibly a custom field. 
										if ( oValue != null )
										{
											string sFieldName = pi.Name;
											AddInsertionValues(app.Application, hashInsertionStrings, sFieldName, oValue, "future", sMODULE_NAME, String.Empty, vwColumns, sSiteURL);
											// 11/19/2008 Paul.  Just in case the parent ID has not been set, grab it from the parent activity. 
											if ( pi.Name.ToUpper() == "ID" && Sql.IsEmptyGuid(PARENT_ID) )
												PARENT_ID = Sql.ToGuid(oValue);
											if ( pi.Name.ToUpper() == "AUDIT_ID" && Sql.IsEmptyGuid(AUDIT_ID) )
												AUDIT_ID = Sql.ToGuid(oValue);
										}
									}
								}
							}
						}
						// 11/25/2008 Paul.  Now try and load the past values. 
						if ( !Sql.IsEmptyGuid(PARENT_ID) && !Sql.IsEmptyGuid(AUDIT_ID) )
						{
							if ( bLoadPastValues )
							{
								// 12/03/2008 Paul.  When loading past values, make sure that the AUDIT_TOKEN does not match. 
								string sSQL = String.Empty;
								sSQL = "select *                                                  " + ControlChars.CrLf
								     + "  from vw" + sMODULE_TABLE + "_AUDIT                      " + ControlChars.CrLf
								     + " where ID            = @ID                                " + ControlChars.CrLf
								     + "   and AUDIT_VERSION = (select max(AUDIT_VERSION)         " + ControlChars.CrLf
								     + "                          from " + sMODULE_TABLE + "_AUDIT" + ControlChars.CrLf
								     + "                         where ID            = @ID        " + ControlChars.CrLf
								     + "                           and AUDIT_VERSION < (select AUDIT_VERSION              " + ControlChars.CrLf
								     + "                                                  from " + sMODULE_TABLE + "_AUDIT" + ControlChars.CrLf
								     + "                                                 where AUDIT_ID = @AUDIT_ID       " + ControlChars.CrLf
								     + "                                               )                                  " + ControlChars.CrLf
								     + "                           and AUDIT_TOKEN  <> (select AUDIT_TOKEN                " + ControlChars.CrLf
								     + "                                                  from " + sMODULE_TABLE + "_AUDIT" + ControlChars.CrLf
								     + "                                                 where AUDIT_ID = @AUDIT_ID       " + ControlChars.CrLf
								     + "                                               )                                  " + ControlChars.CrLf
								     + "                       )                                  " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@ID"      , PARENT_ID);
									Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID );
									using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
									{
										if ( rdr.Read() )
										{
											for ( int i = 0; i < rdr.FieldCount; i++ )
											{
												string sFieldName = rdr.GetName(i);
												object oValue = !rdr.IsDBNull(i) ? rdr.GetValue(i) : String.Empty;
												AddInsertionValues(app.Application, hashInsertionStrings, sFieldName, oValue, "past", sMODULE_NAME, String.Empty, vwColumns, sSiteURL);
											}
										}
									}
								}
							}
						}
						if ( !Sql.IsEmptyGuid(PARENT_ID) && hashRelationshipsStrings.Count > 0 )
						{
							DataView vwRelationships = new DataView(SplendidCache.ReportingRelationships(app.Application));
							vwRelationships.RowFilter = "RELATIONSHIP_TYPE = \'one-to-many\' and (LHS_MODULE = \'" + sMODULE_NAME + "\' or RHS_MODULE = \'" + sMODULE_NAME + "\')";

							foreach(DataRowView row in vwRelationships)
							{
								string sRELATIONSHIP_NAME              = Sql.ToString(row["RELATIONSHIP_NAME"             ]);
								string sLHS_MODULE                     = Sql.ToString(row["LHS_MODULE"                    ]);
								string sLHS_TABLE                      = Sql.ToString(row["LHS_TABLE"                     ]).ToUpper();
								string sLHS_KEY                        = Sql.ToString(row["LHS_KEY"                       ]).ToUpper();
								string sRHS_MODULE                     = Sql.ToString(row["RHS_MODULE"                    ]);
								string sRHS_TABLE                      = Sql.ToString(row["RHS_TABLE"                     ]).ToUpper();
								string sRHS_KEY                        = Sql.ToString(row["RHS_KEY"                       ]).ToUpper();
								string sJOIN_TABLE                     = Sql.ToString(row["JOIN_TABLE"                    ]).ToUpper();
								string sJOIN_KEY_LHS                   = Sql.ToString(row["JOIN_KEY_LHS"                  ]).ToUpper();
								string sJOIN_KEY_RHS                   = Sql.ToString(row["JOIN_KEY_RHS"                  ]).ToUpper();
								// 11/20/2008 Paul.  Quotes, Orders and Invoices have a relationship column. 
								string sRELATIONSHIP_ROLE_COLUMN       = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN"      ]).ToUpper();
								string sRELATIONSHIP_ROLE_COLUMN_VALUE = Sql.ToString(row["RELATIONSHIP_ROLE_COLUMN_VALUE"]);
								// 11/25/2008 Paul.  Be careful to only switch when necessary.  Parent accounts should not be switched. 
								if ( sLHS_MODULE != sMODULE_NAME && sRHS_MODULE == sMODULE_NAME )
								{
									sLHS_MODULE        = Sql.ToString(row["RHS_MODULE"       ]);
									sLHS_TABLE         = Sql.ToString(row["RHS_TABLE"        ]).ToUpper();
									sLHS_KEY           = Sql.ToString(row["RHS_KEY"          ]).ToUpper();
									sRHS_MODULE        = Sql.ToString(row["LHS_MODULE"       ]);
									sRHS_TABLE         = Sql.ToString(row["LHS_TABLE"        ]).ToUpper();
									sRHS_KEY           = Sql.ToString(row["LHS_KEY"          ]).ToUpper();
									sJOIN_TABLE        = Sql.ToString(row["JOIN_TABLE"       ]).ToUpper();
									sJOIN_KEY_LHS      = Sql.ToString(row["JOIN_KEY_RHS"     ]).ToUpper();
									sJOIN_KEY_RHS      = Sql.ToString(row["JOIN_KEY_LHS"     ]).ToUpper();
								}
								if ( arrRelationships.Contains(sRHS_MODULE) )
								{
									vwColumns = new DataView(SplendidCache.WorkflowFilterColumns(app.Application, sRHS_TABLE));
									if ( bLoadFutureRelValues )
									{
										StringBuilder sb = new StringBuilder();
										sb.AppendLine("select " + sRHS_TABLE + ".*");
										sb.AppendLine("  from            vw" + sLHS_TABLE + " " + sLHS_TABLE);
										if ( Sql.IsEmptyString(sJOIN_TABLE) )
										{
											sb.AppendLine("       inner join vw" + sRHS_TABLE + " " + sRHS_TABLE);
											sb.AppendLine("               on "   + sRHS_TABLE + "." + sRHS_KEY  + " = " + sLHS_TABLE + "." + sLHS_KEY);
										}
										else
										{
											sb.AppendLine("       inner join vw" + sJOIN_TABLE + " " + sJOIN_TABLE);
											sb.AppendLine("               on "   + sJOIN_TABLE + "." + sJOIN_KEY_LHS + " = " + sLHS_TABLE  + "." + sLHS_KEY);
											// 10/31/2009 Paul.  The value should be escaped. 
											if ( !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) )
												sb.AppendLine("              and "   + sJOIN_TABLE + "." + sRELATIONSHIP_ROLE_COLUMN + " = N'" + Sql.EscapeSQL(sRELATIONSHIP_ROLE_COLUMN_VALUE) + "'");
											sb.AppendLine("       inner join vw" + sRHS_TABLE + " " + sRHS_TABLE);
											sb.AppendLine("               on "   + sRHS_TABLE + "." + sRHS_KEY   + " = " + sJOIN_TABLE + "." + sJOIN_KEY_RHS);
										}
										// 07/13/2009 Paul.  The PARENT_ID is the primary key of the LHS table, not the LHS_KEY. 
										sb.AppendLine(" where " + sLHS_TABLE + ".ID = @ID");
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											// 06/03/2009 Paul.  We switched to using a StringBuilder, but we did not stop using sSQL. 
											cmd.CommandText = sb.ToString();
											Sql.AddParameter(cmd, "@ID", PARENT_ID);
											using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
											{
												if ( rdr.Read() )
												{
													for ( int i = 0; i < rdr.FieldCount; i++ )
													{
														string sFieldName = rdr.GetName(i);
														object oValue = !rdr.IsDBNull(i) ? rdr.GetValue(i) : String.Empty;
														AddInsertionValues(app.Application, hashRelationshipsStrings, sFieldName, oValue, "future", sMODULE_NAME, sRHS_MODULE, vwColumns, sSiteURL);
													}
												}
											}
										}
									}
									else if ( bLoadPastRelValues && !Sql.IsEmptyGuid(AUDIT_ID) )
									{
										// 11/26/2008 Paul.  Loading past relationship values is a bit more complicated 
										// in that we have to get the past values for the current module, then we join to get the relationship. 
										StringBuilder sb = new StringBuilder();
										sb.AppendLine("select " + sRHS_TABLE + ".*");
										sb.AppendLine("  from            vw" + sLHS_TABLE + "_AUDIT " + sLHS_TABLE);
										if ( Sql.IsEmptyString(sJOIN_TABLE) )
										{
											sb.AppendLine("       inner join vw" + sRHS_TABLE + " " + sRHS_TABLE);
											sb.AppendLine("               on "   + sRHS_TABLE + "." + sRHS_KEY   + " = " + sLHS_TABLE + "." + sLHS_KEY);
										}
										else
										{
											sb.AppendLine("       inner join vw" + sJOIN_TABLE + " " + sJOIN_TABLE  );
											sb.AppendLine("               on "   + sJOIN_TABLE + "." + sJOIN_KEY_LHS + " = " + sLHS_TABLE  + "." + sLHS_KEY);
											// 10/31/2009 Paul.  The value should be escaped. 
											if ( !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) )
												sb.AppendLine("              and "   + sJOIN_TABLE + "." + sRELATIONSHIP_ROLE_COLUMN + " = N'" + Sql.EscapeSQL(sRELATIONSHIP_ROLE_COLUMN_VALUE) + "'");
											sb.AppendLine("       inner join vw" + sRHS_TABLE + " " + sRHS_TABLE);
											sb.AppendLine("               on "   + sRHS_TABLE + "." + sRHS_KEY   + " = " + sJOIN_TABLE + "." + sJOIN_KEY_RHS);
										}
										// 07/13/2009 Paul.  The PARENT_ID is the primary key of the LHS table, not the LHS_KEY. 
										sb.AppendLine(" where " + sLHS_TABLE + ".ID = @ID"                                             );
										sb.AppendLine("   and AUDIT_VERSION = (select max(AUDIT_VERSION)"                              );
										sb.AppendLine("                          from " + sLHS_TABLE + "_AUDIT"                        );
										sb.AppendLine("                         where ID = @ID"                                        );
										sb.AppendLine("                           and AUDIT_VERSION < (select AUDIT_VERSION"           );
										sb.AppendLine("                                                  from " + sLHS_TABLE + "_AUDIT");
										sb.AppendLine("                                                 where AUDIT_ID = @AUDIT_ID"    );
										sb.AppendLine("                                               )"                               );
										// 12/03/2008 Paul.  When loading past values, make sure that the AUDIT_TOKEN does not match. 
										sb.AppendLine("                           and AUDIT_TOKEN  <> (select AUDIT_TOKEN"             );
										sb.AppendLine("                                                  from " + sLHS_TABLE + "_AUDIT");
										sb.AppendLine("                                                 where AUDIT_ID = @AUDIT_ID"    );
										sb.AppendLine("                                               )"                               );
										sb.AppendLine("                       )"                                                       );
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											// 06/03/2009 Paul.  We switched to using a StringBuilder, but we did not stop using sSQL. 
											cmd.CommandText = sb.ToString();
											Sql.AddParameter(cmd, "@ID"      , PARENT_ID);
											Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID );
											using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
											{
												if ( rdr.Read() )
												{
													for ( int i = 0; i < rdr.FieldCount; i++ )
													{
														string sFieldName = rdr.GetName(i);
														object oValue = !rdr.IsDBNull(i) ? rdr.GetValue(i) : String.Empty;
														AddInsertionValues(app.Application, hashRelationshipsStrings, sFieldName, oValue, "past", sMODULE_NAME, sRHS_MODULE, vwColumns, sSiteURL);
													}
												}
											}
										}
									}
								}
							}
						}
					}
					foreach ( string sInsertion in hashInsertionStrings.Keys )
					{
						if ( sSubject.IndexOf(sInsertion) >= 0 )
							sSubject = sSubject.Replace("{::" + sInsertion + "::}", Sql.ToString(hashInsertionStrings[sInsertion]));
						if ( sBodyHtml.IndexOf(sInsertion) >= 0 )
							sBodyHtml = sBodyHtml.Replace("{::" + sInsertion + "::}", Sql.ToString(hashInsertionStrings[sInsertion]));
					}
					foreach ( string sInsertion in hashRelationshipsStrings.Keys )
					{
						if ( sSubject.IndexOf(sInsertion) >= 0 )
							sSubject = sSubject.Replace("{::" + sInsertion + "::}", Sql.ToString(hashRelationshipsStrings[sInsertion]));
						if ( sBodyHtml.IndexOf(sInsertion) >= 0 )
							sBodyHtml = sBodyHtml.Replace("{::" + sInsertion + "::}", Sql.ToString(hashRelationshipsStrings[sInsertion]));
					}
					
					// 11/19/2009 Paul.  View URL and Edit URL should be available to both an Email and a Notification. 
					string sViewURL    = sSiteURL + PARENT_TYPE + "/view.aspx?ID=" + PARENT_ID.ToString();
					string sEditURL    = sSiteURL + PARENT_TYPE + "/edit.aspx?ID=" + PARENT_ID.ToString();
					// 12/16/2009 Paul.  Project and ProjectTask require special handling as the folder names are plural. 
					if ( PARENT_TYPE == "Project" || PARENT_TYPE == "ProjectTask" )
					{
						sViewURL = sSiteURL + PARENT_TYPE + "s/view.aspx?ID=" + PARENT_ID.ToString();
						sEditURL = sSiteURL + PARENT_TYPE + "s/edit.aspx?ID=" + PARENT_ID.ToString();
					}
					sBodyHtml = sBodyHtml.Replace("$view_url", sViewURL);
					sBodyHtml = sBodyHtml.Replace("$edit_url", sEditURL);
					// 12/21/2012 Paul.  We need a way to generate a Site URL so that a link can be constructed. 
					sBodyHtml = sBodyHtml.Replace("href=\"~/", "href=\"" + sSiteURL);
					sBodyHtml = sBodyHtml.Replace("href=\'~/", "href=\'" + sSiteURL);
					if ( ALERT_TYPE == "Notification" )
					{
						// 06/03/2009 Paul.  We are using simple insertions in our default module notifications. 
						// 07/05/2011 Paul.  A timed workflow will not have an AUDIT_ID.  Use the base view as the source of the data. 
						if ( !Sql.IsEmptyGuid(PARENT_ID) )
						{
							string sSQL = String.Empty;
							if ( !Sql.IsEmptyGuid(AUDIT_ID) )
							{
								sSQL = "select *                            " + ControlChars.CrLf
								     + "  from vw" + sMODULE_TABLE + "_AUDIT" + ControlChars.CrLf
								     + " where ID       = @ID               " + ControlChars.CrLf
								     + "   and AUDIT_ID = @AUDIT_ID         " + ControlChars.CrLf;
							}
							else
							{
								sSQL = "select *                            " + ControlChars.CrLf
								     + "  from vw" + sMODULE_TABLE            + ControlChars.CrLf
								     + " where ID       = @ID               " + ControlChars.CrLf;
							}
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@ID"      , PARENT_ID);
								if ( !Sql.IsEmptyGuid(AUDIT_ID) )
									Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID );
								using ( DataTable dtParent = new DataTable() )
								{
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtParent);
										// 11/19/2009 Paul.  Contacts, Prospects and Leads do not have a NAME field.  We need to manually add a NAME field. 
										if ( dtParent.Columns.Contains("FIRST_NAME") && dtParent.Columns.Contains("LAST_NAME") && !dtParent.Columns.Contains("NAME") )
										{
											dtParent.Columns.Add("NAME", typeof(System.String));
											foreach ( DataRow row in dtParent.Rows )
											{
												string sNAME = Sql.ToString(row["FIRST_NAME"]) + " " + Sql.ToString(row["LAST_NAME"]);
												row["NAME"] = sNAME.Trim();
											}
										}
										DataView  vwParentColumns     = EmailUtils.SortedTableColumns(dtParent);
										Hashtable hashCurrencyColumns = EmailUtils.CurrencyColumns(vwParentColumns);
										if ( dtParent.Rows.Count > 0 )
										{
											string sFillPrefix = String.Empty;

											Hashtable hashEnumsColumns = EmailUtils.EnumColumns(app.Application, sMODULE_NAME);
											switch ( PARENT_TYPE )
											{
												case "Leads"    :
													sFillPrefix = "lead";
													sSubject  = EmailUtils.FillEmail(app.Application, sSubject , sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sBodyHtml = EmailUtils.FillEmail(app.Application, sBodyHtml, sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sFillPrefix = "contact";
													sSubject  = EmailUtils.FillEmail(app.Application, sSubject , sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sBodyHtml = EmailUtils.FillEmail(app.Application, sBodyHtml, sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													break;
												case "Prospects":
													sFillPrefix = "prospect";
													sSubject  = EmailUtils.FillEmail(app.Application, sSubject , sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sBodyHtml = EmailUtils.FillEmail(app.Application, sBodyHtml, sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sFillPrefix = "contact";
													sSubject  = EmailUtils.FillEmail(app.Application, sSubject , sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sBodyHtml = EmailUtils.FillEmail(app.Application, sBodyHtml, sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													break;
												default:
													sFillPrefix = PARENT_TYPE.ToLower();
													if ( sFillPrefix.EndsWith("ies") )
														sFillPrefix = sFillPrefix.Substring(0, sFillPrefix.Length-3) + "y";
													else if ( sFillPrefix.EndsWith("s") )
														sFillPrefix = sFillPrefix.Substring(0, sFillPrefix.Length-1);
													sSubject  = EmailUtils.FillEmail(app.Application, sSubject , sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													sBodyHtml = EmailUtils.FillEmail(app.Application, sBodyHtml, sFillPrefix, dtParent.Rows[0], vwParentColumns, hashCurrencyColumns, hashEnumsColumns);
													break;
											}
										}
									}
								}
							}
						}

						MailMessage mail = new MailMessage();
						if ( !Sql.IsEmptyString(sFromAddress) && !Sql.IsEmptyString(sFromName) )
							mail.From = new MailAddress(sFromAddress, sFromName);
						else
							mail.From = new MailAddress(sFromAddress);
						foreach ( AlertRecipient r in lstRECIPIENTS )
						{
							MailAddress addr = null;
							if ( !Sql.IsEmptyString(r.Address) )
							{
								if ( !Sql.IsEmptyString(r.DisplayName) )
									addr = new MailAddress(r.Address, r.DisplayName);
								else
									addr = new MailAddress(r.Address);
								switch ( r.SendType.ToLower() )
								{
									case "cc" :  mail.CC .Add(addr);  break;
									case "bcc":  mail.Bcc.Add(addr);  break;
									default   :  mail.To .Add(addr);  break;
								}
							}
						}
						// 10/05/2008 Paul.  If there are no recipients, then exit early. 
						if ( mail.To.Count == 0 && mail.CC.Count == 0 && mail.Bcc.Count == 0 )
							return;
						
						mail.Subject      = sSubject ;
						mail.Body         = sBodyHtml;
						mail.IsBodyHtml   = true;
						mail.BodyEncoding = System.Text.Encoding.UTF8;
						mail.Headers.Add("X-SplendidCRM-ID", WORKFLOW_ID.ToString());
						// 06/16/2010 Paul.  Add support for Workflow Alert Template Attachments. 
						if ( dtTemplateAttachments != null )
						{
							foreach ( DataRow row in dtTemplateAttachments.Rows )
							{
								string sFILENAME           = Sql.ToString(row["FILENAME"          ]);
								string sFILE_MIME_TYPE     = Sql.ToString(row["FILE_MIME_TYPE"    ]);
								Guid   gNOTE_ATTACHMENT_ID = Sql.ToGuid  (row["NOTE_ATTACHMENT_ID"]);

								// 07/30/2006 Paul.  We cannot close the streams until the message is sent. 
								MemoryStream mem = new MemoryStream();
								BinaryWriter writer = new BinaryWriter(mem);
								Notes.Attachment.WriteStream(gNOTE_ATTACHMENT_ID, con, writer);
								writer.Flush();
								mem.Seek(0, SeekOrigin.Begin);
								Attachment att = new Attachment(mem, sFILENAME, sFILE_MIME_TYPE);
								// 06/02/2014 Tomi.  Make sure to use UTF8 encoding for the name. 
								att.NameEncoding = System.Text.Encoding.UTF8;
								mail.Attachments.Add(att);
							}
						}
						// 06/26/2010 Paul.  Now attach files generated within the workflow. 
						foreach ( AlertAttachment a in lstATTACHMENTS )
						{
							if ( a.Content != null && a.Content.Length > 0 )
							{
								string sFILENAME       = a.FileName;
								string sFILE_MIME_TYPE = a.FileMimeType;
								MemoryStream mem = new MemoryStream(a.Content);
								Attachment att = new Attachment(mem, sFILENAME, sFILE_MIME_TYPE);
								// 06/02/2014 Tomi.  Make sure to use UTF8 encoding for the name. 
								att.NameEncoding = System.Text.Encoding.UTF8;
								mail.Attachments.Add(att);
							}
						}
						
						// 10/04/2008 Paul.  Move SmtpClient code to a shared function. 
						// 01/17/2017 Paul.  New SplendidMailClient object to encapsulate SMTP, Exchange and Google mail. 
						SplendidMailClient client = SplendidMailClient.CreateMailClient(app.Application);
						client.Send(mail);
					}
					else if ( ALERT_TYPE == "Email" )
					{
						StringBuilder sbTO_ADDRS         = new StringBuilder();
						StringBuilder sbCC_ADDRS         = new StringBuilder();
						StringBuilder sbBCC_ADDRS        = new StringBuilder();
						StringBuilder sbTO_ADDRS_IDS     = new StringBuilder();
						StringBuilder sbTO_ADDRS_NAMES   = new StringBuilder();
						StringBuilder sbTO_ADDRS_EMAILS  = new StringBuilder();
						StringBuilder sbCC_ADDRS_IDS     = new StringBuilder();
						StringBuilder sbCC_ADDRS_NAMES   = new StringBuilder();
						StringBuilder sbCC_ADDRS_EMAILS  = new StringBuilder();
						StringBuilder sbBCC_ADDRS_IDS    = new StringBuilder();
						StringBuilder sbBCC_ADDRS_NAMES  = new StringBuilder();
						StringBuilder sbBCC_ADDRS_EMAILS = new StringBuilder();
						foreach ( AlertRecipient r in lstRECIPIENTS )
						{
							if ( r.SendType.ToLower() == "cc" )
							{
								if ( !Sql.IsEmptyGuid(r.RecipientID) )
								{
									if ( sbCC_ADDRS_IDS.Length > 0 )
										sbCC_ADDRS_IDS.Append(";");
									sbCC_ADDRS_IDS.Append(r.RecipientID.ToString());
								}
								if ( !Sql.IsEmptyString(r.Address) )
								{
									if ( sbCC_ADDRS_EMAILS.Length > 0 )
										sbCC_ADDRS_EMAILS.Append(";");
									sbCC_ADDRS_EMAILS.Append(r.Address);
									if ( !Sql.IsEmptyString(r.DisplayName) )
									{
										if ( sbCC_ADDRS_NAMES.Length > 0 )
											sbCC_ADDRS_NAMES.Append(";");
										sbCC_ADDRS_NAMES.Append(r.DisplayName);
										if ( sbCC_ADDRS.Length > 0 )
											sbCC_ADDRS.Append(";");
										sbCC_ADDRS.Append(r.DisplayName + " <" + r.Address + ">");
									}
									else
									{
										if ( sbCC_ADDRS.Length > 0 )
											sbCC_ADDRS.Append(";");
										sbCC_ADDRS.Append("<" + r.Address + ">");
									}
								}
							}
							else if ( r.SendType.ToLower() == "bcc" )
							{
								if ( !Sql.IsEmptyGuid(r.RecipientID) )
								{
									if ( sbBCC_ADDRS_IDS.Length > 0 )
										sbBCC_ADDRS_IDS.Append(";");
									sbBCC_ADDRS_IDS.Append(r.RecipientID.ToString());
								}
								if ( !Sql.IsEmptyString(r.Address) )
								{
									if ( sbBCC_ADDRS_EMAILS.Length > 0 )
										sbBCC_ADDRS_EMAILS.Append(";");
									sbBCC_ADDRS_EMAILS.Append(r.Address);
									if ( !Sql.IsEmptyString(r.DisplayName) )
									{
										if ( sbBCC_ADDRS_NAMES.Length > 0 )
											sbBCC_ADDRS_NAMES.Append(";");
										sbBCC_ADDRS_NAMES.Append(r.DisplayName);
										if ( sbBCC_ADDRS.Length > 0 )
											sbBCC_ADDRS.Append(";");
										sbBCC_ADDRS.Append(r.DisplayName + " <" + r.Address + ">");
									}
									else
									{
										if ( sbBCC_ADDRS.Length > 0 )
											sbBCC_ADDRS.Append(";");
										sbBCC_ADDRS.Append("<" + r.Address + ">");
									}
								}
							}
							else
							{
								if ( !Sql.IsEmptyGuid(r.RecipientID) )
								{
									if ( sbTO_ADDRS_IDS.Length > 0 )
										sbTO_ADDRS_IDS.Append(";");
									sbTO_ADDRS_IDS.Append(r.RecipientID.ToString());
								}
								if ( !Sql.IsEmptyString(r.Address) )
								{
									if ( sbTO_ADDRS_EMAILS.Length > 0 )
										sbTO_ADDRS_EMAILS.Append(";");
									sbTO_ADDRS_EMAILS.Append(r.Address);
									if ( !Sql.IsEmptyString(r.DisplayName) )
									{
										if ( sbTO_ADDRS_NAMES.Length > 0 )
											sbTO_ADDRS_NAMES.Append(";");
										sbTO_ADDRS_NAMES.Append(r.DisplayName);
										if ( sbTO_ADDRS.Length > 0 )
											sbTO_ADDRS.Append(";");
										sbTO_ADDRS.Append(r.DisplayName + " <" + r.Address + ">");
									}
									else
									{
										if ( sbTO_ADDRS.Length > 0 )
											sbTO_ADDRS.Append(";");
										sbTO_ADDRS.Append("<" + r.Address + ">");
									}
								}
							}
						}
						try
						{
							Guid gEMAIL_ID = Guid.Empty;
							// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
							using ( IDbTransaction trn = Sql.BeginTransaction(con) )
							{
								try
								{
									// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
									// 12/03/2008 Paul.  Since the Plug-in saves body in DESCRIPTION, we need to continue to use it as the primary source of data. 
									// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
									SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("EMAILS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
									SqlProcs.spEMAILS_Update
										( ref gEMAIL_ID
										, ASSIGNED_USER_ID
										, sSubject
										, DateTime.Now
										, PARENT_TYPE
										, PARENT_ID
										, sBodyHtml     // DESCRIPTION
										, sBodyHtml     // DESCRIPTION_HTML
										, sFromAddress
										, sFromName   
										, sbTO_ADDRS        .ToString()
										, sbCC_ADDRS        .ToString()
										, sbBCC_ADDRS       .ToString()
										, sbTO_ADDRS_IDS    .ToString()
										, sbTO_ADDRS_NAMES  .ToString()
										, sbTO_ADDRS_EMAILS .ToString()
										, sbCC_ADDRS_IDS    .ToString()
										, sbCC_ADDRS_NAMES  .ToString()
										, sbCC_ADDRS_EMAILS .ToString()
										, sbBCC_ADDRS_IDS   .ToString()
										, sbBCC_ADDRS_NAMES .ToString()
										, sbBCC_ADDRS_EMAILS.ToString()
										, "out"         // TYPE
										, String.Empty  // MESSAGE_ID
										, String.Empty  // REPLY_TO_NAME
										, String.Empty  // REPLY_TO_ADDR
										, String.Empty  // INTENT
										, Guid.Empty    // MAILBOX_ID
										, TEAM_ID
										, TEAM_SET_LIST
										// 05/17/2017 Paul.  Add Tags module. 
										, String.Empty  // TAG_SET_NAME
										// 11/07/2017 Paul.  Add IS_PRIVATE for use by a large customer. 
										, false         // IS_PRIVATE
										// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
										, ASSIGNED_SET_LIST
										, trn
										);

									foreach ( AlertRecipient r in lstRECIPIENTS )
									{
										Guid   gRECIPIENT_ID   = r.RecipientID  ;
										string sRECIPIENT_TYPE = r.RecipientType;
										// 11/19/2008 Paul.  No need to add the relationship if the recipient is also the parent. 
										if ( !Sql.IsEmptyGuid(gRECIPIENT_ID) && gRECIPIENT_ID != PARENT_ID )
										{
											// 06/26/2010 Paul.  Use stored procedure to assign relationships. 
											SqlProcs.spEMAILS_RELATED_Update(gEMAIL_ID, sRECIPIENT_TYPE, gRECIPIENT_ID, trn);
										}
									}
									// 06/16/2010 Paul.  Add support for Workflow Alert Template Attachments. 
									if ( dtTemplateAttachments != null )
									{
										foreach ( DataRow row in dtTemplateAttachments.Rows )
										{
											Guid gNOTE_ID = Guid.Empty;
											Guid gCOPY_ID = Sql.ToGuid(row["ID"]);
											SqlProcs.spNOTES_Copy(ref gNOTE_ID, gCOPY_ID, "Emails", gEMAIL_ID, trn);
										}
									}
									// 06/26/2010 Paul.  Now attach files generated within the workflow. 
									foreach ( AlertAttachment a in lstATTACHMENTS )
									{
										if ( a.Content != null && a.Content.Length > 0 )
										{
											string sFILENAME       = a.FileName    ;
											string sFILE_EXT       = a.Extension   ;
											string sFILE_MIME_TYPE = a.FileMimeType;
											
											Guid gNOTE_ID = Guid.Empty;
											// 04/02/2012 Paul.  Add ASSIGNED_USER_ID. 
											// 03/15/2013 Paul.  Allow the user to specify an ASSIGNED_USER_ID or TEAM_ID. 
											SqlProcs.spNOTES_Update
												( ref gNOTE_ID
												, app.Term("Emails.LBL_EMAIL_ATTACHMENT") + ": " + sFILENAME
												, "Emails"      // PARENT_TYPE
												, gEMAIL_ID     // PARENT_ID
												, Guid.Empty    // CONTACT_ID
												, String.Empty  // DESCRIPTION
												, TEAM_ID
												, TEAM_SET_LIST
												, ASSIGNED_USER_ID
												// 05/17/2017 Paul.  Add Tags module. 
												, String.Empty  // TAG_SET_NAME
												// 11/07/2017 Paul.  Add IS_PRIVATE for use by a large customer. 
												, false         // IS_PRIVATE
												// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
												, ASSIGNED_SET_LIST
												, trn
												);
											
											Guid gNOTE_ATTACHMENT_ID = Guid.Empty;
											SqlProcs.spNOTE_ATTACHMENTS_Insert(ref gNOTE_ATTACHMENT_ID, gNOTE_ID, sFILENAME, sFILENAME, sFILE_EXT, sFILE_MIME_TYPE, trn);
											//SqlProcs.spNOTES_ATTACHMENT_Update(gNOTE_ATTACHMENT_ID, a.Content, trn);
											// 11/06/2010 Paul.  Use our streamable function. 
											// 11/06/2010 Paul.  Move LoadFile() to Crm.NoteAttachments. 
											Crm.NoteAttachments.LoadFile(gNOTE_ATTACHMENT_ID, a.Content, trn);
										}
									}
									trn.Commit();
								}
								catch
								{
									trn.Rollback();
									// 12/25/2008 Paul.  Re-throw the original exception so as to retain the call stack. 
									throw;
								}
							}
							// 11/20/2008 Paul.  Send the email immediately after the record was created to decrease response time. 
							// Otherwise, the delay for the outbound email scheduler could be 5 minutes. 
							try
							{
								int nEmailsSent = 0;
								EmailUtils.SendEmail(app.Context, gEMAIL_ID, String.Empty, String.Empty, ref nEmailsSent);
								// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("EMAILS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
										SqlProcs.spEMAILS_UpdateStatus(gEMAIL_ID, "sent", trn);
										trn.Commit();
									}
									catch(Exception ex1)
									{
										trn.Rollback();
										throw(new Exception(ex1.Message, ex1.InnerException));
									}
								}
							}
							catch(Exception ex)
							{
								// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
								using ( IDbTransaction trn = Sql.BeginTransaction(con) )
								{
									try
									{
										SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("EMAILS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
										SqlProcs.spEMAILS_UpdateStatus(gEMAIL_ID, "send_error", trn);
										trn.Commit();
									}
									catch(Exception ex1)
									{
										trn.Rollback();
										throw(new Exception(ex1.Message, ex1.InnerException));
									}
								}
								// 11/20/2008 Paul.  If the message fails, don't abort the workflow. 
								SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
							}
						}
						catch(Exception ex)
						{
							SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
							throw(new Exception("AlertActivity.Send failed: " + ex.Message, ex));
						}
					}
				}
			}
			else
			{
				SplendidError.SystemMessage(app.Context, "Warning", new StackTrace(true).GetFrame(0), "AlertActivity: No recipients for WorkflowInstanceId " + this.WorkflowInstanceId.ToString());
			}
		}

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			app = executionContext.GetService<SplendidApplicationService>();
			return base.Execute(executionContext);
		}
	}
}
