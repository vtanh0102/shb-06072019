/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Activities;

namespace SplendidCRM
{
	// 07/29/2008 Paul.  XOML-only workflow with parameters. 
	// http://forums.microsoft.com/MSDN/ShowPost.aspx?PostID=959650&SiteID=1
	public partial class SplendidSequentialWorkflow: SequentialWorkflowActivity
	{
		public SplendidSequentialWorkflow()
		{
			this.Name = "SplendidSequentialWorkflow";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(SplendidSequentialWorkflow));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(SplendidSequentialWorkflow.IDProperty))); }
			set { base.SetValue(SplendidSequentialWorkflow.IDProperty, value); }
		}

		public static DependencyProperty AUDIT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("AUDIT_ID", typeof(Guid), typeof(SplendidSequentialWorkflow));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid AUDIT_ID
		{
			get { return ((Guid)(base.GetValue(SplendidSequentialWorkflow.AUDIT_IDProperty))); }
			set { base.SetValue(SplendidSequentialWorkflow.AUDIT_IDProperty, value); }
		}

		public static DependencyProperty WORKFLOW_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORKFLOW_ID", typeof(Guid), typeof(SplendidSequentialWorkflow));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid WORKFLOW_ID
		{
			get { return ((Guid)(base.GetValue(SplendidSequentialWorkflow.WORKFLOW_IDProperty))); }
			set { base.SetValue(SplendidSequentialWorkflow.WORKFLOW_IDProperty, value); }
		}
		#endregion
	}
}
