/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	public class StoredProcedureActivity: SplendidActivity
	{
		public StoredProcedureActivity()
		{
			this.Name = "StoredProcedureActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(StoredProcedureActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(StoredProcedureActivity.IDProperty))); }
			set { base.SetValue(StoredProcedureActivity.IDProperty, value); }
		}

		public static DependencyProperty MODULE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODULE_NAME", typeof(string), typeof(StoredProcedureActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODULE_NAME
		{
			get { return ((string)(base.GetValue(StoredProcedureActivity.MODULE_NAMEProperty))); }
			set { base.SetValue(StoredProcedureActivity.MODULE_NAMEProperty, value); }
		}

		public static DependencyProperty PROCEDURE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROCEDURE_NAME", typeof(string), typeof(StoredProcedureActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PROCEDURE_NAME
		{
			get { return ((string)(base.GetValue(StoredProcedureActivity.PROCEDURE_NAMEProperty))); }
			set { base.SetValue(StoredProcedureActivity.PROCEDURE_NAMEProperty, value); }
		}
		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
		}

		protected override void Save()
		{
		}

		public void ExecuteStoredProcedure(object sender, EventArgs e)
		{
			// 09/16/2015 Paul.  Change to Debug as it is automatically not included in a release build. 
			Debug.WriteLine("StoredProcedureActivity.ExecuteStoredProcedure " + AUDIT_ID.ToString());

			DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbTransaction trn = Sql.BeginTransaction(con) )
				{
					try
					{
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.Transaction = trn;
							cmd.CommandType    = CommandType.StoredProcedure;
							cmd.CommandText    = PROCEDURE_NAME             ;
							cmd.CommandTimeout = 0                          ;
							// 10/17/2013 Paul.  AUDIT_ID is only available (not null) during an event-based workflow. ID is only available (not null) during an time-based workflow. 
							Sql.AddParameter(cmd, "@AUDIT_ID"   , AUDIT_ID   );
							Sql.AddParameter(cmd, "@ID"         , ID         );
							Sql.AddParameter(cmd, "@MODULE_NAME", MODULE_NAME);
							cmd.ExecuteNonQuery();
						}
						trn.Commit();
					}
					catch
					{
						trn.Rollback();
						throw;
					}
				}
			}
		}
	}
}
