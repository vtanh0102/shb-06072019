/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	public class RelationshipActivity: SequenceActivity
	{
		protected string sLHS_MODULE  ;
		protected string sLHS_TABLE   ;
		protected string sLHS_KEY     ;
		protected string sRHS_MODULE  ;
		protected string sRHS_TABLE   ;
		protected string sRHS_KEY     ;
		protected string sJOIN_TABLE  ;
		protected string sJOIN_KEY_LHS;
		protected string sJOIN_KEY_RHS;
		protected string sRELATIONSHIP_ROLE_COLUMN      ;
		protected string sRELATIONSHIP_ROLE_COLUMN_VALUE;

		[NonSerialized]
		protected SplendidApplicationService app;

		public RelationshipActivity()
		{
			this.Name = "RelationshipActivity";
		}

		#region Public workflow properties
		public static DependencyProperty WORKFLOW_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORKFLOW_ID", typeof(Guid), typeof(RelationshipActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid WORKFLOW_ID
		{
			get { return ((Guid)(base.GetValue(RelationshipActivity.WORKFLOW_IDProperty))); }
			set { base.SetValue(RelationshipActivity.WORKFLOW_IDProperty, value); }
		}

		public static DependencyProperty LHS_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LHS_VALUE", typeof(Guid), typeof(RelationshipActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LHS_VALUE
		{
			get { return ((Guid)(base.GetValue(RelationshipActivity.LHS_VALUEProperty))); }
			set { base.SetValue(RelationshipActivity.LHS_VALUEProperty, value); }
		}

		public static DependencyProperty RHS_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RHS_VALUE", typeof(Guid), typeof(RelationshipActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid RHS_VALUE
		{
			get { return ((Guid)(base.GetValue(RelationshipActivity.RHS_VALUEProperty))); }
			set { base.SetValue(RelationshipActivity.RHS_VALUEProperty, value); }
		}

		public string LHS_MODULE
		{
			get { return sLHS_MODULE; }
			set { sLHS_MODULE = value; }
		}

		public string LHS_TABLE
		{
			get { return sLHS_TABLE; }
			set { sLHS_TABLE = value; }
		}

		public string LHS_KEY
		{
			get { return sLHS_KEY; }
			set { sLHS_KEY = value; }
		}

		public string RHS_MODULE
		{
			get { return sRHS_MODULE; }
			set { sRHS_MODULE = value; }
		}

		public string RHS_TABLE
		{
			get { return sRHS_TABLE; }
			set { sRHS_TABLE = value; }
		}

		public string RHS_KEY
		{
			get { return sRHS_KEY; }
			set { sRHS_KEY = value; }
		}

		public string JOIN_TABLE
		{
			get { return sJOIN_TABLE; }
			set { sJOIN_TABLE = value; }
		}

		public string JOIN_KEY_LHS
		{
			get { return sJOIN_KEY_LHS; }
			set { sJOIN_KEY_LHS = value; }
		}

		public string JOIN_KEY_RHS
		{
			get { return sJOIN_KEY_RHS; }
			set { sJOIN_KEY_RHS = value; }
		}

		public string RELATIONSHIP_ROLE_COLUMN
		{
			get { return sRELATIONSHIP_ROLE_COLUMN; }
			set { sRELATIONSHIP_ROLE_COLUMN = value; }
		}

		public string RELATIONSHIP_ROLE_COLUMN_VALUE
		{
			get { return sRELATIONSHIP_ROLE_COLUMN_VALUE; }
			set { sRELATIONSHIP_ROLE_COLUMN_VALUE = value; }
		}
		#endregion

		public void LoadByLHS(object sender, EventArgs e)
		{
			try
			{
				if ( Sql.IsEmptyString(RHS_TABLE) )
					throw(new Exception("RelationshipActivity.LoadByRHS: RHS_TABLE was not set"));
				if ( Sql.IsEmptyString(RHS_KEY) )
					throw(new Exception("RelationshipActivity.LoadByRHS: RHS_KEY was not set"));
				if ( Sql.IsEmptyString(LHS_TABLE) )
					throw(new Exception("RelationshipActivity.LoadByRHS: LHS_TABLE was not set"));
				if ( Sql.IsEmptyString(LHS_KEY) )
					throw(new Exception("RelationshipActivity.LoadByRHS: LHS_KEY was not set"));
				if ( !Sql.IsEmptyString(JOIN_TABLE) )
				{
					if ( Sql.IsEmptyString(JOIN_KEY_RHS) )
						throw(new Exception("RelationshipActivity.LoadByRHS: JOIN_KEY_RHS was not set"));
					if ( Sql.IsEmptyString(JOIN_KEY_LHS) )
						throw(new Exception("RelationshipActivity.LoadByRHS: JOIN_KEY_LHS was not set"));
				}

				StringBuilder sb = new StringBuilder();
				int nMaxLen = Math.Max(LHS_TABLE.Length, 15);
				sb.AppendLine("select " + RHS_TABLE + "." + RHS_KEY);
				sb.AppendLine("  from            vw" + LHS_TABLE + " " + Strings.Space(nMaxLen - LHS_TABLE.Length) + LHS_TABLE);
				if ( Sql.IsEmptyString(JOIN_TABLE) )
				{
					nMaxLen = Math.Max(nMaxLen, RHS_TABLE.Length + RHS_KEY.Length + 1);
					sb.AppendLine("       inner join vw" + RHS_TABLE + " "           + Strings.Space(nMaxLen - RHS_TABLE.Length                     ) + RHS_TABLE);
					sb.AppendLine("               on "   + RHS_TABLE + "." + RHS_KEY + Strings.Space(nMaxLen - RHS_TABLE.Length - RHS_KEY.Length - 1) + " = " + LHS_TABLE + "." + LHS_KEY);
				}
				else
				{
					nMaxLen = Math.Max(nMaxLen, JOIN_TABLE.Length + JOIN_KEY_LHS.Length + 1);
					nMaxLen = Math.Max(nMaxLen, RHS_TABLE.Length  + RHS_KEY.Length      + 1);
					sb.AppendLine("       inner join vw" + JOIN_TABLE + " "                + Strings.Space(nMaxLen - JOIN_TABLE.Length                          ) + JOIN_TABLE);
					sb.AppendLine("               on "   + JOIN_TABLE + "." + JOIN_KEY_LHS + Strings.Space(nMaxLen - JOIN_TABLE.Length - JOIN_KEY_LHS.Length - 1) + " = " + LHS_TABLE  + "." + LHS_KEY     );
					if ( !Sql.IsEmptyString(RELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(RELATIONSHIP_ROLE_COLUMN_VALUE) )
						sb.AppendLine("              and "   + JOIN_TABLE + "." + RELATIONSHIP_ROLE_COLUMN + " = N'" + RELATIONSHIP_ROLE_COLUMN_VALUE  + "'");
					sb.AppendLine("       inner join vw" + RHS_TABLE + " "                 + Strings.Space(nMaxLen - RHS_TABLE.Length                           ) + RHS_TABLE);
					sb.AppendLine("               on "   + RHS_TABLE + "." + RHS_KEY       + Strings.Space(nMaxLen - RHS_TABLE.Length - RHS_KEY.Length - 1      ) + " = " + JOIN_TABLE + "." + JOIN_KEY_RHS);
				}
				sb.AppendLine(" where " + LHS_TABLE + "." + LHS_KEY + " = @ID");
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						// 12/04/2008 Paul.  Forgot to set CommandText. 
						cmd.CommandText = sb.ToString();
						Sql.AddParameter(cmd, "@ID", LHS_VALUE);

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								RHS_VALUE = Sql.ToGuid(rdr[0]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("RelationshipActivity.LoadByRHS failed: " + ex.Message, ex));
			}
		}

		public void LoadByRHS(object sender, EventArgs e)
		{
			try
			{
				if ( Sql.IsEmptyString(RHS_TABLE) )
					throw(new Exception("RelationshipActivity.LoadByRHS: RHS_TABLE was not set"));
				if ( Sql.IsEmptyString(RHS_KEY) )
					throw(new Exception("RelationshipActivity.LoadByRHS: RHS_KEY was not set"));
				if ( Sql.IsEmptyString(LHS_TABLE) )
					throw(new Exception("RelationshipActivity.LoadByRHS: LHS_TABLE was not set"));
				if ( Sql.IsEmptyString(LHS_KEY) )
					throw(new Exception("RelationshipActivity.LoadByRHS: LHS_KEY was not set"));
				if ( !Sql.IsEmptyString(JOIN_TABLE) )
				{
					if ( Sql.IsEmptyString(JOIN_KEY_RHS) )
						throw(new Exception("RelationshipActivity.LoadByRHS: JOIN_KEY_RHS was not set"));
					if ( Sql.IsEmptyString(JOIN_KEY_LHS) )
						throw(new Exception("RelationshipActivity.LoadByRHS: JOIN_KEY_LHS was not set"));
				}

				StringBuilder sb = new StringBuilder();
				int nMaxLen = Math.Max(LHS_TABLE.Length, 15);
				sb.AppendLine("select " + LHS_TABLE + "." + LHS_KEY);
				sb.AppendLine("  from            vw" + LHS_TABLE + " " + Strings.Space(nMaxLen - LHS_TABLE.Length) + LHS_TABLE);
				if ( Sql.IsEmptyString(JOIN_TABLE) )
				{
					nMaxLen = Math.Max(nMaxLen, RHS_TABLE.Length + RHS_KEY.Length + 1);
					sb.AppendLine("       inner join vw" + RHS_TABLE + " "           + Strings.Space(nMaxLen - RHS_TABLE.Length                     ) + RHS_TABLE);
					sb.AppendLine("               on "   + RHS_TABLE + "." + RHS_KEY + Strings.Space(nMaxLen - RHS_TABLE.Length - RHS_KEY.Length - 1) + " = " + LHS_TABLE + "." + LHS_KEY);
				}
				else
				{
					nMaxLen = Math.Max(nMaxLen, JOIN_TABLE.Length + JOIN_KEY_LHS.Length + 1);
					nMaxLen = Math.Max(nMaxLen, RHS_TABLE.Length  + RHS_KEY.Length      + 1);
					sb.AppendLine("       inner join vw" + JOIN_TABLE + " "                + Strings.Space(nMaxLen - JOIN_TABLE.Length                          ) + JOIN_TABLE);
					sb.AppendLine("               on "   + JOIN_TABLE + "." + JOIN_KEY_LHS + Strings.Space(nMaxLen - JOIN_TABLE.Length - JOIN_KEY_LHS.Length - 1) + " = " + LHS_TABLE  + "." + LHS_KEY     );
					if ( !Sql.IsEmptyString(RELATIONSHIP_ROLE_COLUMN) && !Sql.IsEmptyString(RELATIONSHIP_ROLE_COLUMN_VALUE) )
						sb.AppendLine("              and "   + JOIN_TABLE + "." + RELATIONSHIP_ROLE_COLUMN + " = N'" + RELATIONSHIP_ROLE_COLUMN_VALUE  + "'");
					sb.AppendLine("       inner join vw" + RHS_TABLE + " "                 + Strings.Space(nMaxLen - RHS_TABLE.Length                           ) + RHS_TABLE);
					sb.AppendLine("               on "   + RHS_TABLE + "." + RHS_KEY       + Strings.Space(nMaxLen - RHS_TABLE.Length - RHS_KEY.Length - 1      ) + " = " + JOIN_TABLE + "." + JOIN_KEY_RHS);
				}
				sb.AppendLine(" where " + RHS_TABLE + "." + RHS_KEY + " = @ID");
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						// 12/04/2008 Paul.  Forgot to set CommandText. 
						cmd.CommandText = sb.ToString();
						Sql.AddParameter(cmd, "@ID", RHS_VALUE);

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								LHS_VALUE = Sql.ToGuid(rdr[0]);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("RelationshipActivity.LoadByRHS failed: " + ex.Message, ex));
			}
		}

		// 06/28/2011 Paul.  Since we are not based on the SplendidActivity, we need to use the proper signature for a workflow-callable method. 
		public void Save(object sender, EventArgs e)
		{
			try
			{
				if ( !Sql.IsEmptyString(JOIN_TABLE) )
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						
						IDbCommand cmd = SqlProcs.Factory(con, "sp" + JOIN_TABLE + "_Update");
						// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
						using ( IDbTransaction trn = Sql.BeginTransaction(con) )
						{
							try
							{
								// 11/26/2008 Paul.  Make sure to log the transaction. 
								SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly(JOIN_TABLE, WORKFLOW_ID, this.WorkflowInstanceId, trn);
								// 10/22/2008 Paul.  Set all values to NULL to cover additional parameters that must be set. 
								foreach ( IDbDataParameter par in cmd.Parameters )
								{
									par.Value = DBNull.Value;
								}
								Sql.SetParameter(cmd, JOIN_KEY_LHS, LHS_VALUE);
								Sql.SetParameter(cmd, JOIN_KEY_RHS, RHS_VALUE);
								cmd.ExecuteNonQuery();
								trn.Commit();
							}
							catch
							{
								trn.Rollback();
								// 12/25/2008 Paul.  Re-throw the original exception so as to retain the call stack. 
								throw;
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("AccountActivity.Save failed: " + ex.Message, ex));
			}
		}

		protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
		{
			app = executionContext.GetService<SplendidApplicationService>();
			return base.Execute(executionContext);
		}
	}
}
