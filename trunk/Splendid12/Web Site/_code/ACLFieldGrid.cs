/**
 * Copyright (C) 2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Diagnostics;

namespace SplendidCRM
{
	public class CreateItemTemplateACLColumnAlias : ITemplate
	{
		protected string sMODULE_FIELD;
		protected string sDATA_FIELD ;
		
		public CreateItemTemplateACLColumnAlias(string sMODULE_FIELD, string sDATA_FIELD)
		{
			this.sMODULE_FIELD = sMODULE_FIELD;
			this.sDATA_FIELD   = sDATA_FIELD ;
		}
		public void InstantiateIn(Control objContainer)
		{
			Literal lit = new Literal();
			lit.DataBinding += new EventHandler(OnDataBinding);
			objContainer.Controls.Add(lit);
		}
		private void OnDataBinding(object sender, EventArgs e)
		{
			Literal lbl = (Literal)sender;
			DataGridItem objContainer = (DataGridItem) lbl.NamingContainer;
			DataRowView row = objContainer.DataItem as DataRowView;
			if ( row != null )
			{
				if ( row[sDATA_FIELD] != DBNull.Value )
				{
					string sFIELD_NAME  = Sql.ToString(row[sDATA_FIELD  ]);
					string sMODULE_NAME = Sql.ToString(row[sMODULE_FIELD]);
					
					lbl.Text = sFIELD_NAME;
					DataView vwACL_FIELDS_ALIASES = new DataView(SplendidCache.ACLFieldAliases(HttpContext.Current));
					vwACL_FIELDS_ALIASES.Sort      = "NAME";
					vwACL_FIELDS_ALIASES.RowFilter = "NAME = '" + sFIELD_NAME + "' and (MODULE_NAME is null or MODULE_NAME = '" + sMODULE_NAME + "')";
					if ( vwACL_FIELDS_ALIASES.Count > 0 )
					{
						foreach ( DataRowView rowAlias in vwACL_FIELDS_ALIASES )
						{
							lbl.Text += "<br />" + Sql.ToString(rowAlias["ALIAS_NAME"]);
						}
					}
				}
			}
			else
			{
				lbl.Text = sDATA_FIELD;
			}
		}
	}

	public class CreateItemTemplateACLFieldAlias : ITemplate
	{
		protected string sMODULE_FIELD;
		protected string sDATA_FIELD  ;
		protected string sDISPLAY_NAME;
		
		public CreateItemTemplateACLFieldAlias(string sMODULE_FIELD, string sDATA_FIELD, string sDISPLAY_NAME)
		{
			this.sMODULE_FIELD = sMODULE_FIELD;
			this.sDATA_FIELD   = sDATA_FIELD  ;
			this.sDISPLAY_NAME = sDISPLAY_NAME;
		}
		public void InstantiateIn(Control objContainer)
		{
			Literal lit = new Literal();
			lit.DataBinding += new EventHandler(OnDataBinding);
			objContainer.Controls.Add(lit);
		}
		private void OnDataBinding(object sender, EventArgs e)
		{
			Literal lbl = (Literal)sender;
			DataGridItem objContainer = (DataGridItem) lbl.NamingContainer;
			DataRowView row = objContainer.DataItem as DataRowView;
			if ( row != null )
			{
				// 04/30/2006 Paul.  Use the Context to store pointers to the localization objects.
				// This is so that we don't need to require that the page inherits from SplendidPage. 
				L10N L10n = HttpContext.Current.Items["L10n"] as L10N;
				if ( L10n == null )
				{
					// 04/26/2006 Paul.  We want to have the AccessView on the SystemCheck page. 
					L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
				}
				if ( row[sDATA_FIELD] != DBNull.Value )
				{
					lbl.Text = L10n.Term(Sql.ToString(row[sDISPLAY_NAME]));
					string sFIELD_NAME  = Sql.ToString(row[sDATA_FIELD  ]);
					string sMODULE_NAME = Sql.ToString(row[sMODULE_FIELD]);
					
					DataView vwACL_FIELDS_ALIASES = new DataView(SplendidCache.ACLFieldAliases(HttpContext.Current));
					vwACL_FIELDS_ALIASES.Sort      = "NAME";
					vwACL_FIELDS_ALIASES.RowFilter = "NAME = '" + sFIELD_NAME + "' and (MODULE_NAME is null or MODULE_NAME = '" + sMODULE_NAME + "')";
					if ( vwACL_FIELDS_ALIASES.Count > 0 )
					{
						foreach ( DataRowView rowAlias in vwACL_FIELDS_ALIASES )
						{
							string sALIAS_FIELD_NAME  = Sql.ToString(rowAlias["ALIAS_NAME"]);
							string sALIAS_MODULE_NAME = Sql.ToString(rowAlias["ALIAS_MODULE_NAME"]);
							string sALIAS_NAME  = L10n.Term(sALIAS_MODULE_NAME + ".LBL_" + sALIAS_FIELD_NAME);
							lbl.Text += "<br />" + sALIAS_NAME;
						}
					}
				}
			}
			else
			{
				lbl.Text = sDATA_FIELD;
			}
		}
	}

	public class CreateHeaderTemplateACLField : ITemplate
	{
		protected string sLABEL;
		
		public CreateHeaderTemplateACLField(string sLABEL)
		{
			this.sLABEL  = sLABEL ;
		}
		public void InstantiateIn(Control objContainer)
		{
			HtmlGenericControl divLabel = new HtmlGenericControl("div");
			objContainer.Controls.Add(divLabel);
			divLabel.Attributes.Add("align", "center");

			Literal lit = new Literal();
			lit.DataBinding += new EventHandler(lit_OnDataBinding);
			divLabel.Controls.Add(lit);
			lit.Text = sLABEL;
		}
		private void lit_OnDataBinding(object sender, EventArgs e)
		{
			Literal lbl = (Literal)sender;
			DataGridItem objContainer = (DataGridItem) lbl.NamingContainer;
			// 04/30/2006 Paul.  Use the Context to store pointers to the localization objects.
			// This is so that we don't need to require that the page inherits from SplendidPage. 
			L10N L10n = HttpContext.Current.Items["L10n"] as L10N;
			if ( L10n == null )
			{
				// 04/26/2006 Paul.  We want to have the AccessView on the SystemCheck page. 
				L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
			}
			lbl.Text = L10n.Term(sLABEL);
		}
	}


	public class CreateItemTemplateACLField : ITemplate
	{
		protected string sDATA_FIELD ;
		protected string sACCESS_TYPE;
		protected HtmlGenericControl divList ;
		protected HtmlGenericControl divLabel;
		protected Literal            lbl;
		protected HtmlInputHidden    hid;
		protected DropDownList       lst;
		
		public CreateItemTemplateACLField(string sDATA_FIELD, string sACCESS_TYPE)
		{
			this.sDATA_FIELD  = sDATA_FIELD ;
			this.sACCESS_TYPE = sACCESS_TYPE;
		}

		#region Helpers
		private int NormalizeAccessValue(string sACCESS_TYPE, int nACCESS)
		{
			if ( sACCESS_TYPE == "access" )
			{
				// 04/25/2006 Paul.  Be flexible with the values, so don't compare directly to 89 and -98.
				if ( nACCESS > 0 )
					nACCESS = 89;
				else
					nACCESS = -98;
			}
			else if ( sACCESS_TYPE == "permission" )
			{
				if      ( nACCESS > Security.ACL_FIELD_ACCESS.READ_OWNER_WRITE      ) nACCESS = Security.ACL_FIELD_ACCESS.READ_WRITE            ;
				else if ( nACCESS > Security.ACL_FIELD_ACCESS.READ_ONLY             ) nACCESS = Security.ACL_FIELD_ACCESS.READ_OWNER_WRITE      ;
				else if ( nACCESS > Security.ACL_FIELD_ACCESS.OWNER_READ_OWNER_WRITE) nACCESS = Security.ACL_FIELD_ACCESS.READ_ONLY             ;
				else if ( nACCESS > Security.ACL_FIELD_ACCESS.OWNER_READ_ONLY       ) nACCESS = Security.ACL_FIELD_ACCESS.OWNER_READ_OWNER_WRITE;
				else if ( nACCESS > Security.ACL_FIELD_ACCESS.NOT_SET               ) nACCESS = Security.ACL_FIELD_ACCESS.OWNER_READ_ONLY       ;
				else if ( nACCESS > Security.ACL_FIELD_ACCESS.NONE                  ) nACCESS = Security.ACL_FIELD_ACCESS.NOT_SET               ;
				else                                                                  nACCESS = Security.ACL_FIELD_ACCESS.NONE                  ;
			}
			return nACCESS;
		}
		private string AccessClassName(string sACCESS_TYPE, int nACCESS)
		{
			string sClass = "aclNormal";
			if ( sACCESS_TYPE == "access" )
			{
				if ( nACCESS > 0 )
					sClass = "aclEnabled";
				else
					sClass = "aclDisabled";
			}
			else if ( sACCESS_TYPE == "permission" )
			{
				nACCESS = NormalizeAccessValue(sACCESS_TYPE, nACCESS);
				if      ( nACCESS == Security.ACL_FIELD_ACCESS.READ_WRITE             ) sClass = "aclAll"   ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.READ_OWNER_WRITE       ) sClass = "aclOwner" ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.READ_ONLY              ) sClass = "aclOwner" ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.OWNER_READ_OWNER_WRITE ) sClass = "aclOwner" ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.OWNER_READ_ONLY        ) sClass = "aclOwner" ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.NOT_SET                ) sClass = "";
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.NONE                   ) sClass = "aclNone"  ;
			}
			return sClass;
		}
		private string AccessLabel(string sACCESS_TYPE, int nACCESS)
		{
			string sACCESS = String.Empty;
			if ( sACCESS_TYPE == "access" )
			{
				if ( nACCESS > 0 )
					sACCESS = "ACLActions.LBL_ACCESS_ENABLED";
				else
					sACCESS = "ACLActions.LBL_ACCESS_DISABLED";
			}
			else if ( sACCESS_TYPE == "permission" )
			{
				nACCESS = NormalizeAccessValue(sACCESS_TYPE, nACCESS);
				if      ( nACCESS == Security.ACL_FIELD_ACCESS.READ_WRITE             ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_READ_WRITE"            ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.READ_OWNER_WRITE       ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_READ_OWNER_WRITE"      ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.READ_ONLY              ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_READ_ONLY"             ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.OWNER_READ_OWNER_WRITE ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_OWNER_READ_OWNER_WRITE";
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.OWNER_READ_ONLY        ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_OWNER_READ"            ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.NOT_SET                ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_NOT_SET"               ;
				else if ( nACCESS == Security.ACL_FIELD_ACCESS.NONE                   ) sACCESS = "ACLActions.LBL_FIELD_ACCESS_NONE"                  ;
			}
			return sACCESS;
		}
		#endregion

		public void InstantiateIn(Control objContainer)
		{
			// 04/25/2006 Paul.  The label needs to be created first as the List will need to access it. 
			divLabel = new HtmlGenericControl("div");
			objContainer.Controls.Add(divLabel);
			divLabel.Attributes.Add("style", "display: inline");

			lbl = new Literal();
			lbl.DataBinding += new EventHandler(lit_OnDataBinding);
			divLabel.Controls.Add(lbl);

			hid = new HtmlInputHidden();
			objContainer.Controls.Add(hid);

			divList = new HtmlGenericControl("div");
			objContainer.Controls.Add(divList);
			divList.Attributes.Add("style", "display: none");

			lst = new DropDownList();
			lst.DataBinding += new EventHandler(lst_OnDataBinding);
			divList.Controls.Add(lst);
			
			if ( sACCESS_TYPE == "access" )
			{
				lst.Items.Add(new ListItem("ACLActions.LBL_ACCESS_ENABLED" , ACL_ACCESS.ENABLED .ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_ACCESS_DISABLED", ACL_ACCESS.DISABLED.ToString()));
			}
			else if ( sACCESS_TYPE == "permission" )
			{
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_NOT_SET"               , Security.ACL_FIELD_ACCESS.NOT_SET               .ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_READ_WRITE"            , Security.ACL_FIELD_ACCESS.READ_WRITE            .ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_READ_OWNER_WRITE"      , Security.ACL_FIELD_ACCESS.READ_OWNER_WRITE      .ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_READ_ONLY"             , Security.ACL_FIELD_ACCESS.READ_ONLY             .ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_OWNER_READ_OWNER_WRITE", Security.ACL_FIELD_ACCESS.OWNER_READ_OWNER_WRITE.ToString()));
				lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_NONE"                  , Security.ACL_FIELD_ACCESS.NONE                  .ToString()));
			}
		}
		private void lit_OnDataBinding(object sender, EventArgs e)
		{
			DataGridItem objContainer = (DataGridItem) lbl.NamingContainer;
			DataRowView row = objContainer.DataItem as DataRowView;

			// 04/25/2006 Paul.  We don't have access to the ACLFieldGrid in InstantiateIn(), so do so here. 
			ACLFieldGrid grd = objContainer.Parent.Parent as ACLFieldGrid;
			if ( !grd.EnableACLEditing )
			{
				divList.Controls.Clear();
				// 04/25/2006 Paul.  I'd like to remove the dvList, but I can't do it here. 
				//divList.Parent.Controls.Remove(divList);
				//divList = null;
			}
			if ( row != null )
			{
				string sFIELD_NAME = Sql.ToString(row["FIELD_NAME"]);
				// 04/25/2006 Paul.  We could use GUIDS like SugarCRM, but since there is only one ACL grid, 
				// there is no need for that kind of uniqueness. 
				divLabel.ID = sFIELD_NAME + "_" + sACCESS_TYPE + "link";
				hid.ID = "hid" + sFIELD_NAME + "_" + sACCESS_TYPE;
				
				// 01/16/2010 Paul.  We want to show NULL values as NOT_SET. 
				//if ( row[sDATA_FIELD] != DBNull.Value )
				{
					// 04/30/2006 Paul.  Use the Context to store pointers to the localization objects.
					// This is so that we don't need to require that the page inherits from SplendidPage. 
					L10N L10n = HttpContext.Current.Items["L10n"] as L10N;
					if ( L10n == null )
					{
						// 04/26/2006 Paul.  We want to have the AccessView on the SystemCheck page. 
						L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
					}
					int nACCESS = Sql.ToInteger(row[sDATA_FIELD]);
					divLabel.Attributes.Add("class", AccessClassName(sACCESS_TYPE, nACCESS));
					// 04/25/2006 Paul.  Don't update values on postback, otherwise it will over-write modified values. 
					// 04/26/2006 Paul.  Make sure to set the division color, even on postback. 
					// 05/03/2006 Paul.  If we are not editing, then always bind to the query. 
					// This is so that the AccessView control can be placed in an unrelated control that have postbacks. 
					// This was first noticed in the UserRolesView.ascx. 
					if ( !objContainer.Page.IsPostBack || !grd.EnableACLEditing )
					{
						lbl.Text  = L10n.Term(AccessLabel(sACCESS_TYPE, nACCESS));
						hid.Value = lbl.Text;
					}
					else
					{
						// 04/25/2006 Paul.  The label will not retain its value, so restore from the hidden field.
						// We use the hidden field because the value my have been changed by the user. 
						// 04/25/2006 Paul.  We are too early in the ASP.NET lifecycle to access hid.Value,
						// so go directly to the Request to get the submitted value. 
						lbl.Text = hid.Page.Request[hid.UniqueID];
					}
				}
			}
		}
		private void lst_OnDataBinding(object sender, EventArgs e)
		{
			DataGridItem objContainer = (DataGridItem) lst.NamingContainer;
			DataRowView row = objContainer.DataItem as DataRowView;
			if ( row != null )
			{
				string sFIELD_NAME = Sql.ToString(row["FIELD_NAME"]);
				// 04/25/2006 Paul.  We could use GUIDS like SugarCRM, but since there is only one ACL grid, 
				// there is no need for that kind of uniqueness. 
				// 04/25/2006 Paul.  toggleDisplay() will automatically reverse toggle of a linked control. 
				divList.ID = sFIELD_NAME + "_" + sACCESS_TYPE;
				lst.ID = "lst" + sFIELD_NAME + "_" + sACCESS_TYPE;
				// 04/25/2006 Paul.  OnChange, update the the innerHTML and the hidden field. 
				lst.Attributes.Add("onchange", "document.getElementById('" + divLabel.ClientID + "').innerHTML=this.options[this.selectedIndex].text; document.getElementById('" + hid.ClientID + "').value=document.getElementById('" + divLabel.ClientID + "').innerHTML; toggleDisplay('" + divList.ClientID + "');");
				// 04/25/2006 Paul.  The first parent is the DIV, the second is the TableCell. 
				TableCell td = divList.Parent as TableCell;
				if ( td != null )
					td.Attributes.Add("ondblclick", "toggleDisplay('" + divList.ClientID + "')");
				
				// 01/16/2010 Paul.  We want to show NULL values as NOT_SET. 
				//if ( row[sDATA_FIELD] != DBNull.Value )
				{
					int nACCESS = Sql.ToInteger(row[sDATA_FIELD]);
					lst.Attributes.Add("class", AccessClassName(sACCESS_TYPE, nACCESS));
					try
					{
						// 04/25/2006 Paul.  Don't update values on postback, otherwise it will over-write modified values. 
						if ( !objContainer.Page.IsPostBack )
						{
							// 01/18/2010 Paul.  Some fields are not Writeable, so recreate the items. 
							if ( sFIELD_NAME == "DATE_ENTERED" || sFIELD_NAME == "DATE_MODIFIED" || sFIELD_NAME == "DATE_MODIFIED_UTC" || sFIELD_NAME == "CREATED_BY_ID" || sFIELD_NAME == "MODIFIED_USER_ID" )
							{
								lst.Items.Clear();
								lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_NOT_SET"               , Security.ACL_FIELD_ACCESS.NOT_SET               .ToString()));
								lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_READ_ONLY"             , Security.ACL_FIELD_ACCESS.READ_ONLY             .ToString()));
								lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_OWNER_READ_ONLY"       , Security.ACL_FIELD_ACCESS.OWNER_READ_ONLY       .ToString()));
								lst.Items.Add(new ListItem("ACLActions.LBL_FIELD_ACCESS_NONE"                  , Security.ACL_FIELD_ACCESS.NONE                  .ToString()));
							}
							// 08/19/2010 Paul.  Check the list before assigning the value. 
							Utils.SetSelectedValue(lst, NormalizeAccessValue(sACCESS_TYPE, nACCESS).ToString());
						}
					}
					catch
					{
					}
				}
				
				// 04/25/2006 Paul.  We always need to translate the items, even during postback.
				// This is because we always build the DropDownList. 
				// 04/30/2006 Paul.  Use the Context to store pointers to the localization objects.
				// This is so that we don't need to require that the page inherits from SplendidPage. 
				L10N L10n = HttpContext.Current.Items["L10n"] as L10N;
				if ( L10n == null )
				{
					// 04/26/2006 Paul.  We want to have the AccessView on the SystemCheck page. 
					L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
				}
				// 04/25/2006 Paul.  Make sure to translate the text.  
				// It cannot be translated in InstantiateIn() because the Page is not defined. 
				foreach(ListItem itm in lst.Items )
				{
					itm.Text = L10n.Term(itm.Text);
				}
			}
		}
	}

	/// <summary>
	/// Summary description for ACLFieldGrid.
	/// </summary>
	public class ACLFieldGrid : SplendidGrid
	{
		protected bool bEnableACLEditing = false;

		public ACLFieldGrid()
		{
			this.Init += new EventHandler(OnInit);
		}

		public bool EnableACLEditing
		{
			get { return bEnableACLEditing; }
			set { bEnableACLEditing = value; }
		}

		// 04/25/2006 Paul.  FindControl needs to be executed on the DataGridItem.  I'm not sure why.
		public DropDownList FindACLControl(string sFIELD_NAME, string sACCESS_TYPE)
		{
			DropDownList lst = null;
			foreach(DataGridItem itm in Items)
			{
				lst = itm.FindControl("lst" + sFIELD_NAME + "_" + sACCESS_TYPE) as DropDownList;
				if ( lst != null )
					break;
			}
			return lst;
		}

		private void AppendACLColumn(string sLABEL, string sDATA_FIELD, string sACCESS_TYPE)
		{
			TemplateColumn tpl = new TemplateColumn();
			//tpl.HeaderText                  = sLABEL;
			tpl.ItemStyle.Width             = new Unit("12%");
			//tpl.ItemStyle.CssClass          = "tabDetailViewDF";
			tpl.ItemStyle.HorizontalAlign   = HorizontalAlign.Center   ;
			tpl.ItemStyle.VerticalAlign     = VerticalAlign.NotSet     ;
			tpl.ItemStyle.Wrap              = false;
			tpl.ItemTemplate   = new CreateItemTemplateACLField(sDATA_FIELD, sACCESS_TYPE);
			tpl.HeaderTemplate = new CreateHeaderTemplateACLField(sLABEL);
			this.Columns.Add(tpl);
		}

		protected void OnInit(object sender, System.EventArgs e)
		{
			L10N L10n = HttpContext.Current.Items["L10n"] as L10N;
			if ( L10n == null )
			{
				// 04/26/2006 Paul.  We want to have the AccessView on the SystemCheck page. 
				L10n = new L10N(Sql.ToString(HttpContext.Current.Session["USER_SETTINGS/CULTURE"]));
			}

			TemplateColumn tpl = new TemplateColumn();
			tpl.ItemStyle.Wrap     = false;
			tpl.ItemStyle.CssClass = "tabDetailViewDL";
			tpl.ItemTemplate       = new CreateItemTemplateACLColumnAlias("MODULE_NAME", "FIELD_NAME");
			tpl.HeaderText         = L10n.Term("ACLRoles.LBL_LIST_COLUMN_NAME");
			this.Columns.Add(tpl);

			tpl = new TemplateColumn();
			tpl.ItemStyle.Wrap     = false;
			tpl.ItemStyle.CssClass = "tabDetailViewDL";
			tpl.ItemTemplate       = new CreateItemTemplateACLFieldAlias("MODULE_NAME", "FIELD_NAME", "DISPLAY_NAME");
			tpl.HeaderText         = L10n.Term("ACLRoles.LBL_LIST_FIELD_NAME");
			this.Columns.Add(tpl);

			AppendACLColumn("ACLActions.LBL_FIELD_PERMISSION", "ACLACCESS", "permission");
		}

	}
}

