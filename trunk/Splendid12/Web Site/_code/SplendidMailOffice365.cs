﻿/**
 * Copyright (C) 2017 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;
using Microsoft.Exchange.WebServices.Data;

namespace SplendidCRM
{
	public class SplendidMailOffice365 : SplendidMailClient
	{
		Office365AccessToken token;

		public SplendidMailOffice365(HttpApplicationState Application, Guid gOAUTH_TOKEN_ID)
		{
			string sOAuthClientID     = Sql.ToString(Application["CONFIG.Exchange.ClientID"    ]);
			string sOAuthClientSecret = Sql.ToString(Application["CONFIG.Exchange.ClientSecret"]);
			token = SplendidCRM.ActiveDirectory.Office365RefreshAccessToken(Application, sOAuthClientID, sOAuthClientSecret, gOAUTH_TOKEN_ID, false);
		}

		override public void Send(MailMessage mail)
		{
			ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1, TimeZoneInfo.Utc);
			// 02/10/2017 Paul.  https://outlook.office.com/EWS/Exchange.asmx does not work. 
			service.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");
			service.Credentials = new OAuthCredentials(token.access_token);

			EmailMessage msg = new EmailMessage(service);
			msg.Subject = mail.Subject;
			msg.Body    = mail.Body   ;
			msg.From    = new EmailAddress(mail.From.DisplayName, mail.From.Address);
			foreach ( MailAddress to in mail.To )
			{
				msg.ToRecipients.Add(new EmailAddress(to.DisplayName, to.Address));
			}
			foreach ( MailAddress to in mail.CC )
			{
				msg.CcRecipients.Add(new EmailAddress(to.DisplayName, to.Address));
			}
			foreach ( MailAddress to in mail.Bcc )
			{
				msg.BccRecipients.Add(new EmailAddress(to.DisplayName, to.Address));
			}
			for ( int iHeader = 0; iHeader < mail.Headers.Count; iHeader++ )
			{
				string sHeaderKey   = mail.Headers.GetKey(iHeader);
				string sHeaderValue = mail.Headers.Get   (iHeader);
				ExtendedPropertyDefinition headerProperty = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.InternetHeaders, sHeaderKey, MapiPropertyType.String);
				PropertySet columns = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.InternetMessageId, headerProperty);
				msg.SetExtendedProperty(headerProperty, sHeaderValue);
			}
			foreach ( System.Net.Mail.Attachment attachment in mail.Attachments )
			{
				// https://msdn.microsoft.com/en-us/library/office/dn726694(v=exchg.150).aspx
				msg.Attachments.AddFileAttachment(attachment.Name, attachment.ContentStream);
			}
			msg.SendAndSaveCopy();
		}
	}
}
