
function B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed(fldB_EMP_EVALUATION_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldB_EMP_EVALUATION_NAME.id.substring(0, fldB_EMP_EVALUATION_NAME.id.length - 'B_EMP_EVALUATION_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_EMP_EVALUATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_EMP_EVALUATION_NAME = document.getElementById(userContext + 'PREV_B_EMP_EVALUATION_NAME');
	if ( fldPREV_B_EMP_EVALUATION_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_EMP_EVALUATION_NAME');
	}
	else if ( fldPREV_B_EMP_EVALUATION_NAME.value != fldB_EMP_EVALUATION_NAME.value )
	{
		if ( fldB_EMP_EVALUATION_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.EVAB0101.AutoComplete.B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Get(fldB_EMP_EVALUATION_NAME.value, B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed_OnSucceededWithContext, B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_EMP_EVALUATION_NAME_AjaxErrors');
		var fldB_EMP_EVALUATION_ID        = document.getElementById(userContext + 'B_EMP_EVALUATION_ID'       );
		var fldB_EMP_EVALUATION_NAME      = document.getElementById(userContext + 'B_EMP_EVALUATION_NAME'     );
		var fldPREV_B_EMP_EVALUATION_NAME = document.getElementById(userContext + 'PREV_B_EMP_EVALUATION_NAME');
		if ( fldB_EMP_EVALUATION_ID        != null ) fldB_EMP_EVALUATION_ID.value        = sID  ;
		if ( fldB_EMP_EVALUATION_NAME      != null ) fldB_EMP_EVALUATION_NAME.value      = sNAME;
		if ( fldPREV_B_EMP_EVALUATION_NAME != null ) fldPREV_B_EMP_EVALUATION_NAME.value = sNAME;
	}
	else
	{
		alert('result from EVAB0101.AutoComplete service is null');
	}
}

function B_EMP_EVALUATION_B_EMP_EVALUATION_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_EMP_EVALUATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_EMP_EVALUATION_ID        = document.getElementById(userContext + 'B_EMP_EVALUATION_ID'       );
	var fldB_EMP_EVALUATION_NAME      = document.getElementById(userContext + 'B_EMP_EVALUATION_NAME'     );
	var fldPREV_B_EMP_EVALUATION_NAME = document.getElementById(userContext + 'PREV_B_EMP_EVALUATION_NAME');
	if ( fldB_EMP_EVALUATION_ID        != null ) fldB_EMP_EVALUATION_ID.value        = '';
	if ( fldB_EMP_EVALUATION_NAME      != null ) fldB_EMP_EVALUATION_NAME.value      = '';
	if ( fldPREV_B_EMP_EVALUATION_NAME != null ) fldPREV_B_EMP_EVALUATION_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

