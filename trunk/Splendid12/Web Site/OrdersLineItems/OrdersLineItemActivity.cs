/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// OrdersLineItemActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:29 PM
	/// </summary>
	public class OrdersLineItemActivity: SplendidActivity
	{
		public OrdersLineItemActivity()
		{
			this.Name = "OrdersLineItemActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ORDER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORDER_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ORDER_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.ORDER_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.ORDER_IDProperty, value); }
		}

		public static DependencyProperty LINE_GROUP_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LINE_GROUP_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid LINE_GROUP_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.LINE_GROUP_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.LINE_GROUP_IDProperty, value); }
		}

		public static DependencyProperty LINE_ITEM_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LINE_ITEM_TYPE", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string LINE_ITEM_TYPE
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.LINE_ITEM_TYPEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.LINE_ITEM_TYPEProperty, value); }
		}

		public static DependencyProperty POSITIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("POSITION", typeof(Int32), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 POSITION
		{
			get { return ((Int32)(base.GetValue(OrdersLineItemActivity.POSITIONProperty))); }
			set { base.SetValue(OrdersLineItemActivity.POSITIONProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.NAMEProperty, value); }
		}

		public static DependencyProperty MFT_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MFT_PART_NUM", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MFT_PART_NUM
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.MFT_PART_NUMProperty))); }
			set { base.SetValue(OrdersLineItemActivity.MFT_PART_NUMProperty, value); }
		}

		public static DependencyProperty VENDOR_PART_NUMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("VENDOR_PART_NUM", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string VENDOR_PART_NUM
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.VENDOR_PART_NUMProperty))); }
			set { base.SetValue(OrdersLineItemActivity.VENDOR_PART_NUMProperty, value); }
		}

		public static DependencyProperty PRODUCT_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRODUCT_TEMPLATE_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PRODUCT_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.PRODUCT_TEMPLATE_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.PRODUCT_TEMPLATE_IDProperty, value); }
		}

		public static DependencyProperty TAX_CLASSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX_CLASS", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAX_CLASS
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.TAX_CLASSProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAX_CLASSProperty, value); }
		}

		public static DependencyProperty QUANTITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("QUANTITY", typeof(float), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float QUANTITY
		{
			get { return ((float)(base.GetValue(OrdersLineItemActivity.QUANTITYProperty))); }
			set { base.SetValue(OrdersLineItemActivity.QUANTITYProperty, value); }
		}

		public static DependencyProperty COST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COST_PRICE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal COST_PRICE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.COST_PRICEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.COST_PRICEProperty, value); }
		}

		public static DependencyProperty LIST_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_PRICE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal LIST_PRICE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.LIST_PRICEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.LIST_PRICEProperty, value); }
		}

		public static DependencyProperty UNIT_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("UNIT_PRICE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal UNIT_PRICE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.UNIT_PRICEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.UNIT_PRICEProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty PARENT_TEMPLATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TEMPLATE_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_TEMPLATE_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.PARENT_TEMPLATE_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.PARENT_TEMPLATE_IDProperty, value); }
		}

		public static DependencyProperty DISCOUNT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid DISCOUNT_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.DISCOUNT_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DISCOUNT_IDProperty, value); }
		}

		public static DependencyProperty DISCOUNT_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_PRICE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_PRICE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.DISCOUNT_PRICEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DISCOUNT_PRICEProperty, value); }
		}

		public static DependencyProperty PRICING_FORMULAProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FORMULA", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRICING_FORMULA
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.PRICING_FORMULAProperty))); }
			set { base.SetValue(OrdersLineItemActivity.PRICING_FORMULAProperty, value); }
		}

		public static DependencyProperty PRICING_FACTORProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRICING_FACTOR", typeof(float), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float PRICING_FACTOR
		{
			get { return ((float)(base.GetValue(OrdersLineItemActivity.PRICING_FACTORProperty))); }
			set { base.SetValue(OrdersLineItemActivity.PRICING_FACTORProperty, value); }
		}

		public static DependencyProperty TAXRATE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TAXRATE_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.TAXRATE_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAXRATE_IDProperty, value); }
		}

		public static DependencyProperty ASSET_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSET_NUMBER", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSET_NUMBER
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.ASSET_NUMBERProperty))); }
			set { base.SetValue(OrdersLineItemActivity.ASSET_NUMBERProperty, value); }
		}

		public static DependencyProperty BILLING_TYPE_CProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BILLING_TYPE_C", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BILLING_TYPE_C
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.BILLING_TYPE_CProperty))); }
			set { base.SetValue(OrdersLineItemActivity.BILLING_TYPE_CProperty, value); }
		}

		public static DependencyProperty COST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("COST_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal COST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.COST_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.COST_USDOLLARProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.CREATED_BYProperty))); }
			set { base.SetValue(OrdersLineItemActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(OrdersLineItemActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_ORDER_SHIPPEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ORDER_SHIPPED", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ORDER_SHIPPED
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_ORDER_SHIPPEDProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_ORDER_SHIPPEDProperty, value); }
		}

		public static DependencyProperty DATE_SUPPORT_EXPIRESProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_SUPPORT_EXPIRES", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_SUPPORT_EXPIRES
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_SUPPORT_EXPIRESProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_SUPPORT_EXPIRESProperty, value); }
		}

		public static DependencyProperty DATE_SUPPORT_STARTSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_SUPPORT_STARTS", typeof(DateTime), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_SUPPORT_STARTS
		{
			get { return ((DateTime)(base.GetValue(OrdersLineItemActivity.DATE_SUPPORT_STARTSProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DATE_SUPPORT_STARTSProperty, value); }
		}

		public static DependencyProperty DISCOUNT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal DISCOUNT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.DISCOUNT_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DISCOUNT_USDOLLARProperty, value); }
		}

		public static DependencyProperty EXTENDED_PRICEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXTENDED_PRICE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXTENDED_PRICE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.EXTENDED_PRICEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.EXTENDED_PRICEProperty, value); }
		}

		public static DependencyProperty EXTENDED_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXTENDED_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal EXTENDED_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.EXTENDED_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.EXTENDED_USDOLLARProperty, value); }
		}

		public static DependencyProperty LIST_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LIST_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal LIST_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.LIST_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.LIST_USDOLLARProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(OrdersLineItemActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty SERIAL_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SERIAL_NUMBER", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SERIAL_NUMBER
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.SERIAL_NUMBERProperty))); }
			set { base.SetValue(OrdersLineItemActivity.SERIAL_NUMBERProperty, value); }
		}

		public static DependencyProperty SUPPORT_CONTACTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_CONTACT", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_CONTACT
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.SUPPORT_CONTACTProperty))); }
			set { base.SetValue(OrdersLineItemActivity.SUPPORT_CONTACTProperty, value); }
		}

		public static DependencyProperty SUPPORT_DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_DESCRIPTION", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_DESCRIPTION
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.SUPPORT_DESCRIPTIONProperty))); }
			set { base.SetValue(OrdersLineItemActivity.SUPPORT_DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty SUPPORT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.SUPPORT_NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.SUPPORT_NAMEProperty, value); }
		}

		public static DependencyProperty SUPPORT_TERMProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SUPPORT_TERM", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SUPPORT_TERM
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.SUPPORT_TERMProperty))); }
			set { base.SetValue(OrdersLineItemActivity.SUPPORT_TERMProperty, value); }
		}

		public static DependencyProperty TAXProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAX
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.TAXProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAXProperty, value); }
		}

		public static DependencyProperty TAX_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAX_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAX_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.TAX_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAX_USDOLLARProperty, value); }
		}

		public static DependencyProperty UNIT_USDOLLARProperty = System.Workflow.ComponentModel.DependencyProperty.Register("UNIT_USDOLLAR", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal UNIT_USDOLLAR
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.UNIT_USDOLLARProperty))); }
			set { base.SetValue(OrdersLineItemActivity.UNIT_USDOLLARProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty DISCOUNT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DISCOUNT_NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DISCOUNT_NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.DISCOUNT_NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.DISCOUNT_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty TAXRATE_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_NAME", typeof(string), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAXRATE_NAME
		{
			get { return ((string)(base.GetValue(OrdersLineItemActivity.TAXRATE_NAMEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAXRATE_NAMEProperty, value); }
		}

		public static DependencyProperty TAXRATE_VALUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAXRATE_VALUE", typeof(decimal), typeof(OrdersLineItemActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public decimal TAXRATE_VALUE
		{
			get { return ((decimal)(base.GetValue(OrdersLineItemActivity.TAXRATE_VALUEProperty))); }
			set { base.SetValue(OrdersLineItemActivity.TAXRATE_VALUEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("OrdersLineItemActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("OrdersLineItemActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select ORDERS_LINE_ITEMS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwORDERS_LINE_ITEMS_AUDIT        ORDERS_LINE_ITEMS          " + ControlChars.CrLf
							                + " inner join vwORDERS_LINE_ITEMS_AUDIT        ORDERS_LINE_ITEMS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on ORDERS_LINE_ITEMS_AUDIT_OLD.ID = ORDERS_LINE_ITEMS.ID       " + ControlChars.CrLf
							                + "        and ORDERS_LINE_ITEMS_AUDIT_OLD.AUDIT_VERSION = (select max(vwORDERS_LINE_ITEMS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                           from vwORDERS_LINE_ITEMS_AUDIT                   " + ControlChars.CrLf
							                + "                                                          where vwORDERS_LINE_ITEMS_AUDIT.ID            =  ORDERS_LINE_ITEMS.ID           " + ControlChars.CrLf
							                + "                                                            and vwORDERS_LINE_ITEMS_AUDIT.AUDIT_VERSION <  ORDERS_LINE_ITEMS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                            and vwORDERS_LINE_ITEMS_AUDIT.AUDIT_TOKEN   <> ORDERS_LINE_ITEMS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                        )" + ControlChars.CrLf
							                + " where ORDERS_LINE_ITEMS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                       " + ControlChars.CrLf
							                + "  from vwORDERS_LINE_ITEMS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwORDERS_LINE_ITEMS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                       " + ControlChars.CrLf
							                + "  from vwORDERS_LINE_ITEMS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ORDER_ID                       = Sql.ToGuid    (rdr["ORDER_ID"                      ]);
								LINE_GROUP_ID                  = Sql.ToGuid    (rdr["LINE_GROUP_ID"                 ]);
								LINE_ITEM_TYPE                 = Sql.ToString  (rdr["LINE_ITEM_TYPE"                ]);
								POSITION                       = Sql.ToInteger (rdr["POSITION"                      ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								MFT_PART_NUM                   = Sql.ToString  (rdr["MFT_PART_NUM"                  ]);
								VENDOR_PART_NUM                = Sql.ToString  (rdr["VENDOR_PART_NUM"               ]);
								PRODUCT_TEMPLATE_ID            = Sql.ToGuid    (rdr["PRODUCT_TEMPLATE_ID"           ]);
								TAX_CLASS                      = Sql.ToString  (rdr["TAX_CLASS"                     ]);
								QUANTITY                       = Sql.ToFloat   (rdr["QUANTITY"                      ]);
								COST_PRICE                     = Sql.ToDecimal (rdr["COST_PRICE"                    ]);
								LIST_PRICE                     = Sql.ToDecimal (rdr["LIST_PRICE"                    ]);
								UNIT_PRICE                     = Sql.ToDecimal (rdr["UNIT_PRICE"                    ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								PARENT_TEMPLATE_ID             = Sql.ToGuid    (rdr["PARENT_TEMPLATE_ID"            ]);
								DISCOUNT_ID                    = Sql.ToGuid    (rdr["DISCOUNT_ID"                   ]);
								DISCOUNT_PRICE                 = Sql.ToDecimal (rdr["DISCOUNT_PRICE"                ]);
								PRICING_FORMULA                = Sql.ToString  (rdr["PRICING_FORMULA"               ]);
								PRICING_FACTOR                 = Sql.ToFloat   (rdr["PRICING_FACTOR"                ]);
								TAXRATE_ID                     = Sql.ToGuid    (rdr["TAXRATE_ID"                    ]);
								ASSET_NUMBER                   = Sql.ToString  (rdr["ASSET_NUMBER"                  ]);
								BILLING_TYPE_C                 = Sql.ToString  (rdr["BILLING_TYPE_C"                ]);
								COST_USDOLLAR                  = Sql.ToDecimal (rdr["COST_USDOLLAR"                 ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_ORDER_SHIPPED             = Sql.ToDateTime(rdr["DATE_ORDER_SHIPPED"            ]);
								DATE_SUPPORT_EXPIRES           = Sql.ToDateTime(rdr["DATE_SUPPORT_EXPIRES"          ]);
								DATE_SUPPORT_STARTS            = Sql.ToDateTime(rdr["DATE_SUPPORT_STARTS"           ]);
								DISCOUNT_USDOLLAR              = Sql.ToDecimal (rdr["DISCOUNT_USDOLLAR"             ]);
								EXTENDED_PRICE                 = Sql.ToDecimal (rdr["EXTENDED_PRICE"                ]);
								EXTENDED_USDOLLAR              = Sql.ToDecimal (rdr["EXTENDED_USDOLLAR"             ]);
								LIST_USDOLLAR                  = Sql.ToDecimal (rdr["LIST_USDOLLAR"                 ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								SERIAL_NUMBER                  = Sql.ToString  (rdr["SERIAL_NUMBER"                 ]);
								SUPPORT_CONTACT                = Sql.ToString  (rdr["SUPPORT_CONTACT"               ]);
								SUPPORT_DESCRIPTION            = Sql.ToString  (rdr["SUPPORT_DESCRIPTION"           ]);
								SUPPORT_NAME                   = Sql.ToString  (rdr["SUPPORT_NAME"                  ]);
								SUPPORT_TERM                   = Sql.ToString  (rdr["SUPPORT_TERM"                  ]);
								TAX                            = Sql.ToDecimal (rdr["TAX"                           ]);
								TAX_USDOLLAR                   = Sql.ToDecimal (rdr["TAX_USDOLLAR"                  ]);
								UNIT_USDOLLAR                  = Sql.ToDecimal (rdr["UNIT_USDOLLAR"                 ]);
								if ( !bPast )
								{
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									DISCOUNT_NAME                  = Sql.ToString  (rdr["DISCOUNT_NAME"                 ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									TAXRATE_NAME                   = Sql.ToString  (rdr["TAXRATE_NAME"                  ]);
									TAXRATE_VALUE                  = Sql.ToDecimal (rdr["TAXRATE_VALUE"                 ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("OrdersLineItemActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("ORDERS_LINE_ITEMS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spORDERS_LINE_ITEMS_Update
								( ref gID
								, ORDER_ID
								, LINE_GROUP_ID
								, LINE_ITEM_TYPE
								, POSITION
								, NAME
								, MFT_PART_NUM
								, VENDOR_PART_NUM
								, PRODUCT_TEMPLATE_ID
								, TAX_CLASS
								, QUANTITY
								, COST_PRICE
								, LIST_PRICE
								, UNIT_PRICE
								, DESCRIPTION
								, PARENT_TEMPLATE_ID
								, DISCOUNT_ID
								, DISCOUNT_PRICE
								, PRICING_FORMULA
								, PRICING_FACTOR
								, TAXRATE_ID
								, trn
								);
							ID = gID;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.Transaction = trn;
								cmd.CommandType = CommandType.Text;
								cmd.CommandText =  "update ORDERS_LINE_ITEMS_CSTM";
								cmd.CommandText += "   set BILLING_TYPE_C                 = @BILLING_TYPE_C"                ;
								cmd.CommandText += " where ID_C = @ID_C";
								Sql.AddParameter(cmd, "@BILLING_TYPE_C"                , BILLING_TYPE_C                );
								Sql.AddParameter(cmd, "@ID_C"                          , gID                           );
								cmd.ExecuteNonQuery();
							}
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("OrdersLineItemActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

