<%@ Control CodeBehind="Accounts.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.KPIB0301.Accounts" %>
<script type="text/javascript">
function AccountPopup()
{
	return ModulePopup('Accounts', '<%= txtACCOUNT_ID.ClientID %>', null, 'ClearDisabled=1', true, null);
}
</script>
<input ID="txtACCOUNT_ID" type="hidden" Runat="server" />
<%@ Register TagPrefix="SplendidCRM" Tagname="SubPanelButtons" Src="~/_controls/SubPanelButtons.ascx" %>
<SplendidCRM:SubPanelButtons ID="ctlDynamicButtons" Module="Accounts" SubPanel="divKPIB0301Accounts" Title="Accounts.LBL_MODULE_NAME" Runat="Server" />

<div id="divKPIB0301Accounts" style='<%= "display:" + (CookieValue("divKPIB0301Accounts") != "1" ? "inline" : "none") %>'>
	<asp:Panel ID="pnlNewRecordInline" Visible='<%# !Sql.ToBoolean(Application["CONFIG.disable_editview_inline"]) %>' Style="display:none" runat="server">
		<%@ Register TagPrefix="SplendidCRM" Tagname="NewRecord" Src="~/Accounts/NewRecord.ascx" %>
		<SplendidCRM:NewRecord ID="ctlNewRecord" Width="100%" EditView="EditView.Inline" ShowCancel="true" ShowHeader="false" ShowFullForm="true" ShowTopButtons="true" Runat="Server" />
	</asp:Panel>
	
	<%@ Register TagPrefix="SplendidCRM" Tagname="SearchView" Src="~/_controls/SearchView.ascx" %>
	<SplendidCRM:SearchView ID="ctlSearchView" Module="Accounts" SearchMode="SearchSubpanel" IsSubpanelSearch="true" ShowSearchTabs="false" ShowDuplicateSearch="false" ShowSearchViews="false" Visible="false" Runat="Server" />
	
	<SplendidCRM:SplendidGrid id="grdMain" SkinID="grdSubPanelView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
		<Columns>
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:ImageButton Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("Accounts", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="Accounts.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ACCOUNT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_EDIT") %>' SkinID="edit_inline" Runat="server" />
					<asp:LinkButton  Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("Accounts", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="Accounts.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ACCOUNT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_EDIT") %>' Runat="server" />
					&nbsp;
					<span onclick="return confirm('<%= L10n.TermJavaScript("KPIB0301.NTC_REMOVE_B_KPI_ACT_PERCENT_ALLOCATE_CONFIRMATION") %>')">
						<asp:ImageButton Visible='<%# SplendidCRM.Security.GetUserAccess("KPIB0301", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_ACT_PERCENT_ALLOCATE_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="Accounts.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ACCOUNT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_REMOVE") %>' SkinID="delete_inline" Runat="server" />
						<asp:LinkButton  Visible='<%# SplendidCRM.Security.GetUserAccess("KPIB0301", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_ACT_PERCENT_ALLOCATE_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="Accounts.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ACCOUNT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_REMOVE") %>' Runat="server" />
					</span>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
</div>
