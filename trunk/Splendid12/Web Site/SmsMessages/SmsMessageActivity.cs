/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// SmsMessageActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:31 PM
	/// </summary>
	public class SmsMessageActivity: SplendidActivity
	{
		public SmsMessageActivity()
		{
			this.Name = "SmsMessageActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.IDProperty))); }
			set { base.SetValue(SmsMessageActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.TEAM_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(SmsMessageActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty MAILBOX_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MAILBOX_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MAILBOX_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.MAILBOX_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.MAILBOX_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.NAMEProperty, value); }
		}

		public static DependencyProperty DATE_TIMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.DATE_TIMEProperty))); }
			set { base.SetValue(SmsMessageActivity.DATE_TIMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(SmsMessageActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.PARENT_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty FROM_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_NUMBER", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_NUMBER
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.FROM_NUMBERProperty))); }
			set { base.SetValue(SmsMessageActivity.FROM_NUMBERProperty, value); }
		}

		public static DependencyProperty TO_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_NUMBER", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_NUMBER
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TO_NUMBERProperty))); }
			set { base.SetValue(SmsMessageActivity.TO_NUMBERProperty, value); }
		}

		public static DependencyProperty TO_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TO_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.TO_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.TO_IDProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TYPEProperty))); }
			set { base.SetValue(SmsMessageActivity.TYPEProperty, value); }
		}

		public static DependencyProperty MESSAGE_SIDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MESSAGE_SID", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MESSAGE_SID
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.MESSAGE_SIDProperty))); }
			set { base.SetValue(SmsMessageActivity.MESSAGE_SIDProperty, value); }
		}

		public static DependencyProperty FROM_LOCATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FROM_LOCATION", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FROM_LOCATION
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.FROM_LOCATIONProperty))); }
			set { base.SetValue(SmsMessageActivity.FROM_LOCATIONProperty, value); }
		}

		public static DependencyProperty TO_LOCATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TO_LOCATION", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TO_LOCATION
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TO_LOCATIONProperty))); }
			set { base.SetValue(SmsMessageActivity.TO_LOCATIONProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(SmsMessageActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(SmsMessageActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.CREATED_BYProperty))); }
			set { base.SetValue(SmsMessageActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(SmsMessageActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(SmsMessageActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(SmsMessageActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.DATE_STARTProperty))); }
			set { base.SetValue(SmsMessageActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(SmsMessageActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.STATUSProperty))); }
			set { base.SetValue(SmsMessageActivity.STATUSProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_START", typeof(DateTime), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_START
		{
			get { return ((DateTime)(base.GetValue(SmsMessageActivity.TIME_STARTProperty))); }
			set { base.SetValue(SmsMessageActivity.TIME_STARTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(SmsMessageActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(SmsMessageActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(SmsMessageActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(SmsMessageActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(SmsMessageActivity.PARENT_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("SmsMessageActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("SmsMessageActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select SMS_MESSAGES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwSMS_MESSAGES_AUDIT        SMS_MESSAGES          " + ControlChars.CrLf
							                + " inner join vwSMS_MESSAGES_AUDIT        SMS_MESSAGES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on SMS_MESSAGES_AUDIT_OLD.ID = SMS_MESSAGES.ID       " + ControlChars.CrLf
							                + "        and SMS_MESSAGES_AUDIT_OLD.AUDIT_VERSION = (select max(vwSMS_MESSAGES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                      from vwSMS_MESSAGES_AUDIT                   " + ControlChars.CrLf
							                + "                                                     where vwSMS_MESSAGES_AUDIT.ID            =  SMS_MESSAGES.ID           " + ControlChars.CrLf
							                + "                                                       and vwSMS_MESSAGES_AUDIT.AUDIT_VERSION <  SMS_MESSAGES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                       and vwSMS_MESSAGES_AUDIT.AUDIT_TOKEN   <> SMS_MESSAGES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                   )" + ControlChars.CrLf
							                + " where SMS_MESSAGES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwSMS_MESSAGES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwSMS_MESSAGES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwSMS_MESSAGES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								MAILBOX_ID                     = Sql.ToGuid    (rdr["MAILBOX_ID"                    ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								if ( !bPast )
									DATE_TIME                      = Sql.ToDateTime(rdr["DATE_TIME"                     ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								FROM_NUMBER                    = Sql.ToString  (rdr["FROM_NUMBER"                   ]);
								TO_NUMBER                      = Sql.ToString  (rdr["TO_NUMBER"                     ]);
								TO_ID                          = Sql.ToGuid    (rdr["TO_ID"                         ]);
								TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								MESSAGE_SID                    = Sql.ToString  (rdr["MESSAGE_SID"                   ]);
								FROM_LOCATION                  = Sql.ToString  (rdr["FROM_LOCATION"                 ]);
								TO_LOCATION                    = Sql.ToString  (rdr["TO_LOCATION"                   ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TIME_START                     = Sql.ToDateTime(rdr["TIME_START"                    ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SmsMessageActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("SMS_MESSAGES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spSMS_MESSAGES_Update
								( ref gID
								, ASSIGNED_USER_ID
								, TEAM_ID
								, TEAM_SET_LIST
								, MAILBOX_ID
								, NAME
								, DATE_TIME
								, PARENT_TYPE
								, PARENT_ID
								, FROM_NUMBER
								, TO_NUMBER
								, TO_ID
								, TYPE
								, MESSAGE_SID
								, FROM_LOCATION
								, TO_LOCATION
								, TAG_SET_NAME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("SmsMessageActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

