
function B_NOTIFICATION_B_NOTIFICATION_NAME_Changed(fldB_NOTIFICATION_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldB_NOTIFICATION_NAME.id.substring(0, fldB_NOTIFICATION_NAME.id.length - 'B_NOTIFICATION_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_NOTIFICATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_NOTIFICATION_NAME = document.getElementById(userContext + 'PREV_B_NOTIFICATION_NAME');
	if ( fldPREV_B_NOTIFICATION_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_NOTIFICATION_NAME');
	}
	else if ( fldPREV_B_NOTIFICATION_NAME.value != fldB_NOTIFICATION_NAME.value )
	{
		if ( fldB_NOTIFICATION_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.CRM001.AutoComplete.B_NOTIFICATION_B_NOTIFICATION_NAME_Get(fldB_NOTIFICATION_NAME.value, B_NOTIFICATION_B_NOTIFICATION_NAME_Changed_OnSucceededWithContext, B_NOTIFICATION_B_NOTIFICATION_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_NOTIFICATION_B_NOTIFICATION_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_NOTIFICATION_B_NOTIFICATION_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_NOTIFICATION_B_NOTIFICATION_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_NOTIFICATION_NAME_AjaxErrors');
		var fldB_NOTIFICATION_ID        = document.getElementById(userContext + 'B_NOTIFICATION_ID'       );
		var fldB_NOTIFICATION_NAME      = document.getElementById(userContext + 'B_NOTIFICATION_NAME'     );
		var fldPREV_B_NOTIFICATION_NAME = document.getElementById(userContext + 'PREV_B_NOTIFICATION_NAME');
		if ( fldB_NOTIFICATION_ID        != null ) fldB_NOTIFICATION_ID.value        = sID  ;
		if ( fldB_NOTIFICATION_NAME      != null ) fldB_NOTIFICATION_NAME.value      = sNAME;
		if ( fldPREV_B_NOTIFICATION_NAME != null ) fldPREV_B_NOTIFICATION_NAME.value = sNAME;
	}
	else
	{
		alert('result from CRM001.AutoComplete service is null');
	}
}

function B_NOTIFICATION_B_NOTIFICATION_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_NOTIFICATION_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_NOTIFICATION_ID        = document.getElementById(userContext + 'B_NOTIFICATION_ID'       );
	var fldB_NOTIFICATION_NAME      = document.getElementById(userContext + 'B_NOTIFICATION_NAME'     );
	var fldPREV_B_NOTIFICATION_NAME = document.getElementById(userContext + 'PREV_B_NOTIFICATION_NAME');
	if ( fldB_NOTIFICATION_ID        != null ) fldB_NOTIFICATION_ID.value        = '';
	if ( fldB_NOTIFICATION_NAME      != null ) fldB_NOTIFICATION_NAME.value      = '';
	if ( fldPREV_B_NOTIFICATION_NAME != null ) fldPREV_B_NOTIFICATION_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

