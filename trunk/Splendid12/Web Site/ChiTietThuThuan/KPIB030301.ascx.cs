using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM.ChiTietThuThuan
{

	/// <summary>
	///		Summary description for KPIB030301.
	/// </summary>
	public class KPIB030301 : SubPanelControl
	{
		protected _controls.SubPanelButtons ctlDynamicButtons;
		protected _controls.SearchView     ctlSearchView    ;
		protected UniqueStringCollection arrSelectFields;
		protected Guid            gID            ;
		protected DataView        vwMain         ;
		protected SplendidGrid    grdMain        ;
		protected HtmlInputHidden txtB_KPI_TOI_ACTUAL_RESULT_ID      ;
		protected Button          btnCreateInline   ;
		protected Panel           pnlNewRecordInline;
		protected SplendidCRM.KPIB030301.NewRecord ctlNewRecord   ;

		protected void Page_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch ( e.CommandName )
				{
					case "KPIB030301.Edit":
					{
						Guid gB_KPI_TOI_ACTUAL_RESULT_ID = Sql.ToGuid(e.CommandArgument);
						Response.Redirect("~/KPIB030301/edit.aspx?ID=" + gB_KPI_TOI_ACTUAL_RESULT_ID.ToString());
						break;
					}
					case "KPIB030301.Remove":
					{
						Guid gB_KPI_TOI_ACTUAL_RESULT_ID = Sql.ToGuid(e.CommandArgument);
						if ( bEditView )
						{
							this.DeleteEditViewRelationship(gB_KPI_TOI_ACTUAL_RESULT_ID);
						}
						else
						{
							SqlProcs.spB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT_Delete(gID, gB_KPI_TOI_ACTUAL_RESULT_ID);
						}
						BindGrid();
						break;
					}
					case "KPIB030301.Create":
						if ( this.IsMobile || Sql.ToBoolean(Application["CONFIG.disable_editview_inline"]) )
							Response.Redirect("~/" + m_sMODULE + "/edit.aspx?PARENT_ID=" + gID.ToString());
						else
						{
							pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "inline");
							ctlDynamicButtons.HideAll();
						}
						break;
					case "NewRecord.Cancel":
						pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "none");
						ctlDynamicButtons.ShowAll();
						break;
					case "NewRecord.FullForm":
						Response.Redirect("~/" + m_sMODULE + "/edit.aspx?PARENT_ID=" + gID.ToString());
						break;
					case "NewRecord":
					{
						Guid gB_KPI_TOI_ACTUAL_RESULT_ID = Sql.ToGuid(e.CommandArgument);
						if ( !Sql.IsEmptyGuid(gB_KPI_TOI_ACTUAL_RESULT_ID) )
						{
							SqlProcs.spB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT_Update(gID, gB_KPI_TOI_ACTUAL_RESULT_ID);
							Response.Redirect(Request.RawUrl);
						}
						else
						{
							pnlNewRecordInline.Style.Add(HtmlTextWriterStyle.Display, "none");
							ctlDynamicButtons.ShowAll();
						}
						break;
					}
					case "KPIB030301.Search":
						ctlSearchView.Visible = !ctlSearchView.Visible;
						break;
					case "Search":
						break;
					case "Clear":
						BindGrid();
						break;
					case "SortGrid":
						break;
					case "Preview":
						if ( Page.Master is SplendidMaster )
						{
							CommandEventArgs ePreview = new CommandEventArgs(e.CommandName, new PreviewData(m_sMODULE, Sql.ToGuid(e.CommandArgument)));
							(Page.Master as SplendidMaster).Page_Command(sender, ePreview);
						}
						break;
					default:
						throw(new Exception("Unknown command: " + e.CommandName));
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}
		}

		protected void BindGrid()
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				string sSQL;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					UniqueGuidCollection arrUPDATED = this.GetUpdatedEditViewRelationships();
					if ( bEditView && IsPostBack && arrUPDATED.Count > 0 )
					{
						arrSelectFields.Remove("B_KPI_TOI_ACTUAL_RESULT_ID"                  );
						arrSelectFields.Remove("B_KPI_TOI_ACTUAL_RESULT_NAME"                );
						arrSelectFields.Remove("B_KPI_TOI_ACT_RESULT_DETAIL_ID"              );
						arrSelectFields.Remove("B_KPI_TOI_ACT_RESULT_DETAIL_NAME"            );
						arrSelectFields.Remove("B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID");
						sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
						     + "     , ID                        as B_KPI_TOI_ACTUAL_RESULT_ID                  " + ControlChars.CrLf
						     + "     , NAME                      as B_KPI_TOI_ACTUAL_RESULT_NAME                " + ControlChars.CrLf
						     + "     , @B_KPI_TOI_ACT_RESULT_DETAIL_ID               as B_KPI_TOI_ACT_RESULT_DETAIL_ID              " + ControlChars.CrLf
						     + "     , @B_KPI_TOI_ACT_RESULT_DETAIL_NAME             as B_KPI_TOI_ACT_RESULT_DETAIL_NAME            " + ControlChars.CrLf
						     + "     , @B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID as B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID" + ControlChars.CrLf
						     + "  from vwB_KPI_TOI_ACTUAL_RESULT" + ControlChars.CrLf;
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@B_KPI_TOI_ACT_RESULT_DETAIL_ID"              , gID);
						Sql.AddParameter(cmd, "@B_KPI_TOI_ACT_RESULT_DETAIL_NAME"            , Sql.ToString(Page.Items["NAME"            ]));
						Sql.AddParameter(cmd, "@B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID", Sql.ToGuid  (Page.Items["ASSIGNED_USER_ID"]));
						Security.Filter(cmd, m_sMODULE, "list");
						Sql.AppendParameter(cmd, arrUPDATED.ToArray(), "ID");
					}
					else
					{
						sSQL = "select " + Sql.FormatSelectFields(arrSelectFields)
						     + "  from vwB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT" + ControlChars.CrLf;
						cmd.CommandText = sSQL;
						Security.Filter(cmd, m_sMODULE, "list");
						Sql.AppendParameter(cmd, gID, "B_KPI_TOI_ACT_RESULT_DETAIL_ID");
					}
					ctlSearchView.SqlSearchClause(cmd);
					cmd.CommandText += grdMain.OrderByClause("DATE_ENTERED", "desc");

					if ( bDebug )
						RegisterClientScriptBlock("vwB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT", Sql.ClientScriptBlock(cmd));

					try
					{
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								this.ApplyGridViewRules("ChiTietThuThuan." + m_sMODULE, dt);
								vwMain = dt.DefaultView;
								grdMain.DataSource = vwMain ;
								grdMain.DataBind();
								if ( bEditView && !IsPostBack )
								{
									this.CreateEditViewRelationships(dt, "B_KPI_TOI_ACTUAL_RESULT_ID");
								}
							}
						}
					}
					catch(Exception ex)
					{
						SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						ctlDynamicButtons.ErrorText = ex.Message;
					}
				}
			}
		}

		// 01/27/2010 Paul.  This method is only calld when in EditMode. 
		public override void Save(Guid gPARENT_ID, string sPARENT_TYPE, IDbTransaction trn)
		{
			UniqueGuidCollection arrDELETED = this.GetDeletedEditViewRelationships();
			foreach ( Guid gDELETE_ID in arrDELETED )
			{
				if ( !Sql.IsEmptyGuid(gDELETE_ID) )
					SqlProcs.spB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT_Delete(gPARENT_ID, gDELETE_ID, trn);
			}

			UniqueGuidCollection arrUPDATED = this.GetUpdatedEditViewRelationships();
			foreach ( Guid gUPDATE_ID in arrUPDATED )
			{
				if ( !Sql.IsEmptyGuid(gUPDATE_ID) )
					SqlProcs.spB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT_Update(gPARENT_ID, gUPDATE_ID, trn);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			gID = Sql.ToGuid(Request["ID"]);
			Guid gB_KPI_TOI_ACTUAL_RESULT_ID = Sql.ToGuid(txtB_KPI_TOI_ACTUAL_RESULT_ID.Value);
			if ( !Sql.IsEmptyGuid(gB_KPI_TOI_ACTUAL_RESULT_ID) )
			{
				try
				{
					if ( bEditView )
					{
						this.UpdateEditViewRelationship(gB_KPI_TOI_ACTUAL_RESULT_ID);
					}
					else
					{
						SqlProcs.spB_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT_Update(gID, gB_KPI_TOI_ACTUAL_RESULT_ID);
					}
					txtB_KPI_TOI_ACTUAL_RESULT_ID.Value = String.Empty;
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					ctlDynamicButtons.ErrorText = ex.Message;
				}
			}
			try
			{
				BindGrid();
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				ctlDynamicButtons.ErrorText = ex.Message;
			}

			if ( !IsPostBack )
			{
				Guid gASSIGNED_USER_ID = Sql.ToGuid(Page.Items["ASSIGNED_USER_ID"]);
				ctlDynamicButtons.AppendButtons("ChiTietThuThuan." + m_sMODULE, gASSIGNED_USER_ID, gID);
				ctlNewRecord.PARENT_ID    = gID;
				ctlNewRecord.PARENT_TYPE = "ChiTietThuThuan";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
			ctlNewRecord.Command      += new CommandEventHandler(Page_Command);
			ctlSearchView.Command     += new CommandEventHandler(Page_Command);
			m_sMODULE = "KPIB030301";
			arrSelectFields = new UniqueStringCollection();
			arrSelectFields.Add("DATE_ENTERED"            );
			arrSelectFields.Add("PENDING_PROCESS_ID"      );
			arrSelectFields.Add("B_KPI_TOI_ACTUAL_RESULT_ID"                  );
			bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
			if ( bModuleIsAssigned )
				arrSelectFields.Add("ASSIGNED_USER_ID"        );
			arrSelectFields.Add("B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID");
			this.AppendGridColumns(grdMain, "ChiTietThuThuan." + m_sMODULE, arrSelectFields, Page_Command);
			if ( IsPostBack )
				ctlDynamicButtons.AppendButtons("ChiTietThuThuan." + m_sMODULE, Guid.Empty, Guid.Empty);
		}
		#endregion
	}
}
