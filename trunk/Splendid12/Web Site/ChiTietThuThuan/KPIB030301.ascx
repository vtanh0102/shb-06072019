<%@ Control CodeBehind="KPIB030301.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.ChiTietThuThuan.KPIB030301" %>
<script type="text/javascript">
function KPIB030301Popup()
{
	return ModulePopup('KPIB030301', '<%= txtB_KPI_TOI_ACTUAL_RESULT_ID.ClientID %>', null, 'ClearDisabled=1', true, null);
}
</script>
<input ID="txtB_KPI_TOI_ACTUAL_RESULT_ID" type="hidden" Runat="server" />
<%@ Register TagPrefix="SplendidCRM" Tagname="SubPanelButtons" Src="~/_controls/SubPanelButtons.ascx" %>
<SplendidCRM:SubPanelButtons ID="ctlDynamicButtons" Module="KPIB030301" SubPanel="divChiTietThuThuanKPIB030301" Title="KPIB030301.LBL_MODULE_NAME" Runat="Server" />

<div id="divChiTietThuThuanKPIB030301" style='<%= "display:" + (CookieValue("divChiTietThuThuanKPIB030301") != "1" ? "inline" : "none") %>'>
	<asp:Panel ID="pnlNewRecordInline" Visible='<%# !Sql.ToBoolean(Application["CONFIG.disable_editview_inline"]) %>' Style="display:none" runat="server">
		<%@ Register TagPrefix="SplendidCRM" Tagname="NewRecord" Src="~/KPIB030301/NewRecord.ascx" %>
		<SplendidCRM:NewRecord ID="ctlNewRecord" Width="100%" EditView="EditView.Inline" ShowCancel="true" ShowHeader="false" ShowFullForm="true" ShowTopButtons="true" Runat="Server" />
	</asp:Panel>
	
	<%@ Register TagPrefix="SplendidCRM" Tagname="SearchView" Src="~/_controls/SearchView.ascx" %>
	<SplendidCRM:SearchView ID="ctlSearchView" Module="KPIB030301" SearchMode="SearchSubpanel" IsSubpanelSearch="true" ShowSearchTabs="false" ShowDuplicateSearch="false" ShowSearchViews="false" Visible="false" Runat="Server" />
	
	<SplendidCRM:SplendidGrid id="grdMain" SkinID="grdSubPanelView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
		<Columns>
			<asp:TemplateColumn HeaderText="" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
				<ItemTemplate>
					<asp:ImageButton Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("KPIB030301", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="KPIB030301.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACTUAL_RESULT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_EDIT") %>' SkinID="edit_inline" Runat="server" />
					<asp:LinkButton  Visible='<%# !bEditView && SplendidCRM.Security.GetUserAccess("KPIB030301", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "ASSIGNED_USER_ID"))) >= 0 && !Sql.IsProcessPending(Container) %>' CommandName="KPIB030301.Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACTUAL_RESULT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_EDIT") %>' Runat="server" />
					&nbsp;
					<span onclick="return confirm('<%= L10n.TermJavaScript("ChiTietThuThuan.NTC_REMOVE_B_KPI_TOI_ACT_RESULT_DETAIL_CONFIRMATION") %>')">
						<asp:ImageButton Visible='<%# SplendidCRM.Security.GetUserAccess("ChiTietThuThuan", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="KPIB030301.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACTUAL_RESULT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" AlternateText='<%# L10n.Term(".LNK_REMOVE") %>' SkinID="delete_inline" Runat="server" />
						<asp:LinkButton  Visible='<%# SplendidCRM.Security.GetUserAccess("ChiTietThuThuan", "edit", Sql.ToGuid(DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACT_RESULT_DETAIL_ASSIGNED_USER_ID"))) >= 0 %>' CommandName="KPIB030301.Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "B_KPI_TOI_ACTUAL_RESULT_ID") %>' OnCommand="Page_Command" CssClass="listViewTdToolsS1" Text='<%# L10n.Term(".LNK_REMOVE") %>' Runat="server" />
					</span>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</SplendidCRM:SplendidGrid>
</div>
