
function B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed(fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain ACTUAL_RESULT_CODE in the text, so just get the length minus 4. 
	var userContext = fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.id.substring(0, fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.id.length - 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE = document.getElementById(userContext + 'PREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE');
	if ( fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE');
	}
	else if ( fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value != fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value )
	{
		if ( fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value.length > 0 )
		{
			try
			{
				SplendidCRM.ChiTietThuThuan.AutoComplete.B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Get(fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value, B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed_OnSucceededWithContext, B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sACTUAL_RESULT_CODE = result.ACTUAL_RESULT_CODE;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_AjaxErrors');
		var fldB_KPI_TOI_ACT_RESULT_DETAIL_ID        = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ID'       );
		var fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE      = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE'     );
		var fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE = document.getElementById(userContext + 'PREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE');
		if ( fldB_KPI_TOI_ACT_RESULT_DETAIL_ID        != null ) fldB_KPI_TOI_ACT_RESULT_DETAIL_ID.value        = sID  ;
		if ( fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE      != null ) fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value      = sACTUAL_RESULT_CODE;
		if ( fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE != null ) fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value = sACTUAL_RESULT_CODE;
	}
	else
	{
		alert('result from ChiTietThuThuan.AutoComplete service is null');
	}
}

function B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_KPI_TOI_ACT_RESULT_DETAIL_ID        = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ID'       );
	var fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE      = document.getElementById(userContext + 'B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE'     );
	var fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE = document.getElementById(userContext + 'PREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE');
	if ( fldB_KPI_TOI_ACT_RESULT_DETAIL_ID        != null ) fldB_KPI_TOI_ACT_RESULT_DETAIL_ID.value        = '';
	if ( fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE      != null ) fldB_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value      = '';
	if ( fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE != null ) fldPREV_B_KPI_TOI_ACT_RESULT_DETAIL_ACTUAL_RESULT_CODE.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

