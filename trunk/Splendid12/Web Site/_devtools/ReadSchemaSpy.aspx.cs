/**
 * Copyright (C) 2012 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace SplendidCRM._devtools
{
	/// <summary>
	/// Summary description for ReadSchemaSpy.
	/// </summary>
	public class ReadSchemaSpy : System.Web.UI.Page
	{
		protected HtmlInputFile fileUNC ;
		protected Label         lblError;
		protected Literal       litOutput;

		protected void Page_ItemCommand(Object sender, CommandEventArgs e)
		{
			// 01/11/2006 Paul.  Only a developer/administrator should see this. 
			// 12/22/2007 Paul.  Allow an admin to import data. 
			if ( !SplendidCRM.Security.IS_ADMIN )
				return;
			try
			{
				if ( e.CommandName == "Upload" )
				{
					if ( fileUNC.PostedFile == null || Sql.IsEmptyString(fileUNC.PostedFile.FileName) )
					{
						throw(new Exception("File was not provided"));
					}
			
					HttpPostedFile pstFile  = fileUNC.PostedFile;
					string sFILENAME = Path.GetFileNameWithoutExtension (pstFile.FileName);

					StringBuilder sbSQL = new StringBuilder();
					XmlDocument xml = new XmlDocument();
					// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
					// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
					// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
					xml.XmlResolver = null;
					xml.Load(pstFile.InputStream);
					XmlNodeList nlTables = xml.DocumentElement.SelectNodes("tables/table");
					foreach ( XmlNode table in nlTables )
					{
						string sTABLE_NAME = XmlUtil.GetNamedItem(table, "name");
						if ( sTABLE_NAME.EndsWith("_audit") )
							continue;
						
						StringBuilder sbTable = new StringBuilder();
						sTABLE_NAME = sTABLE_NAME.ToUpper();
						sbTable.AppendLine("if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '" + sTABLE_NAME + "' and TABLE_TYPE = 'BASE TABLE')");
						sbTable.AppendLine("  begin");
						sbTable.AppendLine("	print 'Create Table dbo." + sTABLE_NAME + "';");
						sbTable.AppendLine("	Create Table dbo." + sTABLE_NAME + "");
						
						string sCOLUMN_SEPARATOR = "		( ";
						if ( table.SelectSingleNode("column[@name='id']"              ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "ID                                 uniqueidentifier not null default(newid()) constraint PK_" + sTABLE_NAME + " primary key");  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='deleted']"         ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "DELETED                            bit not null default(0)"             );  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='created_by']"      ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "CREATED_BY                         uniqueidentifier null"               );  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='date_entered']"    ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "DATE_ENTERED                       datetime not null default(getdate())");  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='modified_user_id']") != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "MODIFIED_USER_ID                   uniqueidentifier null"               );  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='date_modified']"   ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "DATE_MODIFIED                      datetime not null default(getdate())");  sCOLUMN_SEPARATOR = "		, "; }
						if ( table.SelectSingleNode("column[@name='date_modified']"   ) != null ) { sbTable.AppendLine(sCOLUMN_SEPARATOR + "DATE_MODIFIED_UTC                  datetime null default(getutcdate())" );  sCOLUMN_SEPARATOR = "		, "; }
						sbTable.AppendLine();
						
						XmlNodeList nlColumns = table.SelectNodes("column");
						foreach ( XmlNode column in nlColumns )
						{
							string sCOLUMN_NAME = XmlUtil.GetNamedItem(column, "name"    );
							string sCOLUMN_TYPE = XmlUtil.GetNamedItem(column, "type"    );
							string sNULLABLE    = XmlUtil.GetNamedItem(column, "nullable");
							string sSIZE        = XmlUtil.GetNamedItem(column, "size"    );
							string sDIGITS      = XmlUtil.GetNamedItem(column, "digits"  );
							if ( sCOLUMN_NAME == "id" || sCOLUMN_NAME == "deleted" || sCOLUMN_NAME == "created_by" || sCOLUMN_NAME == "date_entered" || sCOLUMN_NAME == "modified_user_id" || sCOLUMN_NAME == "date_modified" )
								continue;
							sCOLUMN_NAME = sCOLUMN_NAME.ToUpper();
							if      ( sCOLUMN_TYPE == "char"    && sSIZE == "36" && (sCOLUMN_NAME == "ID" || sCOLUMN_NAME.EndsWith("_ID")) ) sCOLUMN_TYPE = "uniqueidentifier";
							else if ( sCOLUMN_TYPE == "varchar" && sSIZE == "36" && (sCOLUMN_NAME == "ID" || sCOLUMN_NAME.EndsWith("_ID")) ) sCOLUMN_TYPE = "uniqueidentifier";
							else if ( sCOLUMN_NAME == "ASSIGNED_USER_ID" ) sCOLUMN_TYPE = "uniqueidentifier";
							else if ( sCOLUMN_TYPE == "char"             ) sCOLUMN_TYPE = "nchar";
							else if ( sCOLUMN_TYPE == "varchar"          ) sCOLUMN_TYPE = "nvarchar";
							else if ( sCOLUMN_TYPE == "datetime"         ) sCOLUMN_TYPE = "datetime";
							else if ( sCOLUMN_TYPE == "date"             ) sCOLUMN_TYPE = "datetime";
							else if ( sCOLUMN_TYPE == "time"             ) sCOLUMN_TYPE = "datetime";
							else if ( sCOLUMN_TYPE == "text"             ) sCOLUMN_TYPE = "nvarchar(max)";
							else if ( sCOLUMN_TYPE == "longtext"         ) sCOLUMN_TYPE = "nvarchar(max)";
							else if ( sCOLUMN_TYPE == "BIT"              ) sCOLUMN_TYPE = "bit";
							else if ( sCOLUMN_TYPE == "int"              ) sCOLUMN_TYPE = "int";
							else if ( sCOLUMN_TYPE == "double"           ) sCOLUMN_TYPE = "float";
							else if ( sCOLUMN_TYPE == "tinyint"          ) sCOLUMN_TYPE = "int";
							else if ( sCOLUMN_TYPE == "smallint"         ) sCOLUMN_TYPE = "int";
							else if ( sCOLUMN_TYPE == "int unsigned"     ) sCOLUMN_TYPE = "int";
							else if ( sCOLUMN_TYPE == "bigint"           ) sCOLUMN_TYPE = "bigint";
							else if ( sCOLUMN_TYPE == "decimal"          ) sCOLUMN_TYPE = "decimal";
							else sCOLUMN_TYPE += "****";
							sbTable.Append(sCOLUMN_SEPARATOR + sCOLUMN_NAME + Strings.Space(34 - sCOLUMN_NAME.Length) + " " + sCOLUMN_TYPE);
							if ( sCOLUMN_TYPE == "nchar" || sCOLUMN_TYPE == "nvarchar" )
								sbTable.Append("(" + sSIZE + ")");
							else if ( sCOLUMN_TYPE == "decimal" )
								sbTable.Append("(" + sSIZE + "," + sDIGITS + ")");
							sbTable.Append(" " + (sNULLABLE == "true" ? "null" : "not null"));
							sbTable.AppendLine();
							sCOLUMN_SEPARATOR = "		, ";
						}
						
						sbTable.AppendLine("		)");
						sbTable.AppendLine();
						XmlNodeList nlIndexes = table.SelectNodes("index");
						foreach ( XmlNode index in nlIndexes )
						{
							string sINDEX_NAME = XmlUtil.GetNamedItem(index, "name");
							string sUNIQUE     = XmlUtil.GetNamedItem(index, "unique");
							if ( sINDEX_NAME == "PRIMARY" )
								continue;
							sINDEX_NAME = sINDEX_NAME.ToUpper();
							if ( sUNIQUE == "true" )
								sbTable.Append("	create unique index " + sINDEX_NAME + Strings.Space(60 - sINDEX_NAME.Length) + " on dbo." + sTABLE_NAME + " (");
							else
								sbTable.Append("	create index " + sINDEX_NAME + Strings.Space(60 - sINDEX_NAME.Length) + " on dbo." + sTABLE_NAME + " (");
							XmlNodeList nlIndexColumns = index.SelectNodes("column");
							for ( int i = 0; i < nlIndexColumns.Count; i++ )
							{
								XmlNode indexcolumn = nlIndexColumns[i];
								string sINDEX_COLUMN_NAME = XmlUtil.GetNamedItem(indexcolumn, "name");
								string sASCENDING         = XmlUtil.GetNamedItem(indexcolumn, "ascending");
								sINDEX_COLUMN_NAME = sINDEX_COLUMN_NAME.ToUpper();
								if ( i > 0 )
									sbTable.Append(", ");
								sbTable.Append(sINDEX_COLUMN_NAME);
								if ( sASCENDING == "false" )
									sbTable.Append(" desc");
							}
							sbTable.AppendLine(")");
						}
						
						sbTable.AppendLine("  end");
						sbTable.AppendLine("GO");
						sbTable.AppendLine();

						sbSQL.Append(sbTable);
					}
					
					//litOutput.Text = sbSQL.ToString();
					Response.ContentType = "text/sql";
					Response.AddHeader("Content-Disposition", "attachment;filename=" + Utils.ContentDispositionEncode(Request.Browser, sFILENAME + ".sql"));
					Response.Write(sbSQL.ToString());
					Response.End();
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message + "<br>" + ex.StackTrace;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
