/**
 * Copyright (C) 2008-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SplendidCRM._devtools
{
	/// <summary>
	/// Summary description for GenerateActivities.
	/// </summary>
	public class GenerateActivities : System.Web.UI.Page
	{
		private void BuildActivity(ref StringBuilder sb, string sDATABASE, string sSINGULAR_NAME, string sTABLE_NAME, DataView vwProc, DataView vwAllColumns, Hashtable hashViewColumns, Hashtable hashAuditColumns)
		{
			sb.AppendLine("/**");
			sb.AppendLine(" * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. ");
			sb.AppendLine(" *");
			sb.AppendLine(" * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License ");
			sb.AppendLine(" * Agreement, or other written agreement between you and SplendidCRM (\"License\"). By installing or ");
			sb.AppendLine(" * using this file, you have unconditionally agreed to the terms and conditions of the License, ");
			sb.AppendLine(" * including but not limited to restrictions on the number of users therein, and you may not use this ");
			sb.AppendLine(" * file except in compliance with the License. ");
			sb.AppendLine(" * ");
			sb.AppendLine(" * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and ");
			sb.AppendLine(" * trademarks, in and to the contents of this file.  You will not link to or in any way combine the ");
			sb.AppendLine(" * contents of this file or any derivatives with any Open Source Code in any manner that would require ");
			sb.AppendLine(" * the contents of this file to be made available to any third party. ");
			sb.AppendLine(" * ");
			sb.AppendLine(" * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, ");
			sb.AppendLine(" * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability ");
			sb.AppendLine(" * and disclaimers set forth in the License. ");
			sb.AppendLine(" * ");
			sb.AppendLine(" */");
			sb.AppendLine("using System;");
			sb.AppendLine("using System.Data;");
			sb.AppendLine("using System.ComponentModel;");
			sb.AppendLine("using System.Workflow.ComponentModel;");
			sb.AppendLine("using System.Workflow.Runtime;");
			sb.AppendLine("using System.Workflow.Activities;");
			sb.AppendLine("using System.Diagnostics;");
			sb.AppendLine();
			sb.AppendLine("namespace SplendidCRM");
			sb.AppendLine("{");
			sb.AppendLine("	/// <summary>");
			sb.AppendLine("	/// " + sSINGULAR_NAME + "Activity generated from database[" + sDATABASE + "] on " + DateTime.Now.ToString());
			sb.AppendLine("	/// </summary>");
			sb.AppendLine("	public class " + sSINGULAR_NAME + "Activity: SplendidActivity");
			sb.AppendLine("	{");
			sb.AppendLine("		public " + sSINGULAR_NAME + "Activity()");
			sb.AppendLine("		{");
			sb.AppendLine("			this.Name = \"" + sSINGULAR_NAME + "Activity\";");
			sb.AppendLine("		}");
			sb.AppendLine();
			sb.AppendLine("		#region Public workflow properties");

			// 02/21/2013 Paul.  Protect against long field names. 
			int nMaxSpace = 30;
			foreach ( DataRowView row in vwProc )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				nMaxSpace = Math.Max(nMaxSpace, sColumnName.Length);
			}
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				nMaxSpace = Math.Max(nMaxSpace, sColumnName.Length);
			}

			// 10/09/2008 Paul.  Update columns. 
			foreach ( DataRowView row in vwProc )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				sCsType = sCsType.Replace("ansi", "");
				sb.AppendLine("		public static DependencyProperty " + sColumnName + "Property = System.Workflow.ComponentModel.DependencyProperty.Register(\"" + sColumnName + "\", typeof(" + sCsType + "), typeof(" + sSINGULAR_NAME + "Activity));");
				sb.AppendLine();
				sb.AppendLine("		[Browsable(true)]");
				sb.AppendLine("		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]");
				sb.AppendLine("		public " + sCsType + " " + sColumnName + "");
				sb.AppendLine("		{");
				sb.AppendLine("			get { return ((" + sCsType + ")(base.GetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property))); }");
				sb.AppendLine("			set { base.SetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property, value); }");
				sb.AppendLine("		}");
				sb.AppendLine();
			}
			
			// 10/09/2008 Paul.  View and Audit columns. 
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
				{
					sCsType = sCsType.Replace("ansi", "");
					sb.AppendLine("		public static DependencyProperty " + sColumnName + "Property = System.Workflow.ComponentModel.DependencyProperty.Register(\"" + sColumnName + "\", typeof(" + sCsType + "), typeof(" + sSINGULAR_NAME + "Activity));");
					sb.AppendLine();
					sb.AppendLine("		[Browsable(true)]");
					sb.AppendLine("		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]");
					sb.AppendLine("		public " + sCsType + " " + sColumnName + "");
					sb.AppendLine("		{");
					sb.AppendLine("			get { return ((" + sCsType + ")(base.GetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property))); }");
					sb.AppendLine("			set { base.SetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property, value); }");
					sb.AppendLine("		}");
					sb.AppendLine();
				}
			}

			// 10/09/2008 Paul.  Audit only columns. 
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && !hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
				{
					sCsType = sCsType.Replace("ansi", "");
					sb.AppendLine("		public static DependencyProperty " + sColumnName + "Property = System.Workflow.ComponentModel.DependencyProperty.Register(\"" + sColumnName + "\", typeof(" + sCsType + "), typeof(" + sSINGULAR_NAME + "Activity));");
					sb.AppendLine();
					sb.AppendLine("		[Browsable(true)]");
					sb.AppendLine("		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]");
					sb.AppendLine("		public " + sCsType + " " + sColumnName + "");
					sb.AppendLine("		{");
					sb.AppendLine("			get { return ((" + sCsType + ")(base.GetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property))); }");
					sb.AppendLine("			set { base.SetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property, value); }");
					sb.AppendLine("		}");
					sb.AppendLine();
				}
			}

			// 10/09/2008 Paul.  View only columns. 
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && hashViewColumns.ContainsKey(sColumnName) && !hashAuditColumns.ContainsKey(sColumnName) )
				{
					sCsType = sCsType.Replace("ansi", "");
					sb.AppendLine("		public static DependencyProperty " + sColumnName + "Property = System.Workflow.ComponentModel.DependencyProperty.Register(\"" + sColumnName + "\", typeof(" + sCsType + "), typeof(" + sSINGULAR_NAME + "Activity));");
					sb.AppendLine();
					sb.AppendLine("		[Browsable(true)]");
					sb.AppendLine("		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]");
					sb.AppendLine("		public " + sCsType + " " + sColumnName + "");
					sb.AppendLine("		{");
					sb.AppendLine("			get { return ((" + sCsType + ")(base.GetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property))); }");
					sb.AppendLine("			set { base.SetValue(" + sSINGULAR_NAME + "Activity." + sColumnName + "Property, value); }");
					sb.AppendLine("		}");
					sb.AppendLine();
				}
			}

			sb.AppendLine("		#endregion");
			sb.AppendLine();
			sb.AppendLine("		protected override void Load(bool bAudit, bool bPast)");
			sb.AppendLine("		{");
			sb.AppendLine("			try");
			sb.AppendLine("			{");
			sb.AppendLine("				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )");
			sb.AppendLine("					throw(new Exception(\"" + sSINGULAR_NAME + "Activity.Load: AUDIT_ID was not set\"));");
			sb.AppendLine("				else if ( !bAudit && Sql.IsEmptyGuid(ID) )");
			sb.AppendLine("					throw(new Exception(\"" + sSINGULAR_NAME + "Activity.Load: ID was not set\"));");
			sb.AppendLine();
			sb.AppendLine("				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);");
			sb.AppendLine("				using ( IDbConnection con = dbf.CreateConnection() )");
			sb.AppendLine("				{");
			sb.AppendLine("					con.Open();");
			sb.AppendLine("					using ( IDbCommand cmd = con.CreateCommand() )");
			sb.AppendLine("					{");
			// 10/08/2008 Paul.  It is easier to select all fields, rather than try and build the select for both the edit view and the audit table. 
			sb.AppendLine("						if ( bPast )");
			sb.AppendLine("						{");
			sb.AppendLine("							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. ");
			sb.AppendLine("							// In order to get the OLD record, we need to use the version value. ");
//			sb.AppendLine("							cmd.CommandText = \"select *" + Strings.Space(7 + sTABLE_NAME.Length) + "\" + ControlChars.CrLf");
//			sb.AppendLine("							                + \"  from vw" + sTABLE_NAME + "_AUDIT\" + ControlChars.CrLf");
//			sb.AppendLine("							                + \" where AUDIT_ID = @AUDIT_ID\" + ControlChars.CrLf;");
			sb.AppendLine("							cmd.CommandText = \"select " + sTABLE_NAME + "_AUDIT_OLD.*\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"  from      vw" + sTABLE_NAME + "_AUDIT        " + sTABLE_NAME + "          \" + ControlChars.CrLf");
			sb.AppendLine("							                + \" inner join vw" + sTABLE_NAME + "_AUDIT        " + sTABLE_NAME + "_AUDIT_OLD\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"         on " + sTABLE_NAME + "_AUDIT_OLD.ID = " + sTABLE_NAME + ".ID       \" + ControlChars.CrLf");
			sb.AppendLine("							                + \"        and " + sTABLE_NAME + "_AUDIT_OLD.AUDIT_VERSION = (select max(vw" + sTABLE_NAME + "_AUDIT.AUDIT_VERSION)\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"" + Strings.Space(27 + sTABLE_NAME.Length) + "               from vw" + sTABLE_NAME + "_AUDIT                   \" + ControlChars.CrLf");
			sb.AppendLine("							                + \"" + Strings.Space(27 + sTABLE_NAME.Length) + "              where vw" + sTABLE_NAME + "_AUDIT.ID            =  " + sTABLE_NAME + ".ID           \" + ControlChars.CrLf");
			sb.AppendLine("							                + \"" + Strings.Space(27 + sTABLE_NAME.Length) + "                and vw" + sTABLE_NAME + "_AUDIT.AUDIT_VERSION <  " + sTABLE_NAME + ".AUDIT_VERSION\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"" + Strings.Space(27 + sTABLE_NAME.Length) + "                and vw" + sTABLE_NAME + "_AUDIT.AUDIT_TOKEN   <> " + sTABLE_NAME + ".AUDIT_TOKEN  \" + ControlChars.CrLf");
			sb.AppendLine("							                + \"" + Strings.Space(27 + sTABLE_NAME.Length) + "            )\" + ControlChars.CrLf");
			sb.AppendLine("							                + \" where " + sTABLE_NAME + ".AUDIT_ID = @AUDIT_ID\" + ControlChars.CrLf;");
			sb.AppendLine("							Sql.AddParameter(cmd, \"@AUDIT_ID\", AUDIT_ID);");
			sb.AppendLine("						}");
			sb.AppendLine("						else if ( bAudit )");
			sb.AppendLine("						{");
			sb.AppendLine("							cmd.CommandText = \"select *" + Strings.Space(6 + sTABLE_NAME.Length) + "\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"  from vw" + sTABLE_NAME + "_Edit\" + ControlChars.CrLf");
			sb.AppendLine("							                + \" where ID in (select ID from vw" + sTABLE_NAME + "_AUDIT where AUDIT_ID = @AUDIT_ID)\" + ControlChars.CrLf;");
			sb.AppendLine("							Sql.AddParameter(cmd, \"@AUDIT_ID\", AUDIT_ID);");
			sb.AppendLine("						}");
			sb.AppendLine("						else");
			sb.AppendLine("						{");
			sb.AppendLine("							cmd.CommandText = \"select *" + Strings.Space(6 + sTABLE_NAME.Length) + "\" + ControlChars.CrLf");
			sb.AppendLine("							                + \"  from vw" + sTABLE_NAME + "_Edit\" + ControlChars.CrLf");
			sb.AppendLine("							                + \" where ID = @ID\" + ControlChars.CrLf;");
			sb.AppendLine("							Sql.AddParameter(cmd, \"@ID\", ID);");
			sb.AppendLine("						}");
			sb.AppendLine();
			sb.AppendLine("						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )");
			sb.AppendLine("						{");
			sb.AppendLine("							if ( rdr.Read() )");
			sb.AppendLine("							{");

			// 10/09/2008 Paul.  Update columns. 
			vwProc.RowFilter = String.Empty;
			foreach ( DataRowView row in vwProc )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				switch ( sCsType )
				{
					case "string"    :  sCsType = "String" ;  break;
					case "ansistring":  sCsType = "String" ;  break;
					case "bool"      :  sCsType = "Boolean";  break;
					case "decimal"   :  sCsType = "Decimal";  break;
					// 03/04/2014 Paul.  Add support for Int64. 
					case "Int64"     :  sCsType = "Long"   ;  break;
					case "Int32"     :  sCsType = "Integer";  break;
					case "float"     :  sCsType = "Float"  ;  break;
				}
				sCsType = sCsType.Replace("ansi", "");
				// 10/08/2008 Paul.  Make sure only to fetch the field if it is available in the select. 
				if ( hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
				{
					sb.AppendLine("								" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
				}
				else if ( hashAuditColumns.ContainsKey(sColumnName) )
				{
					sb.AppendLine("								if ( bPast )");
					sb.AppendLine("									" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
				}
				else if ( hashViewColumns.ContainsKey(sColumnName) )
				{
					sb.AppendLine("								if ( !bPast )");
					sb.AppendLine("									" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
				}
			}

			// 10/09/2008 Paul.  View and Audit columns. 
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				string sCsType     = Sql.ToString (row["CsType"    ]);
				switch ( sCsType )
				{
					case "string"    :  sCsType = "String" ;  break;
					case "ansistring":  sCsType = "String" ;  break;
					case "bool"      :  sCsType = "Boolean";  break;
					case "decimal"   :  sCsType = "Decimal";  break;
					case "Int32"     :  sCsType = "Integer";  break;
					case "float"     :  sCsType = "Float"  ;  break;
				}
				sCsType = sCsType.Replace("ansi", "");
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
				{
					// 10/10/2008 Paul.  CREATED_BY_ID is a special case that needs to be manually added. 
					// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
					//if ( sColumnName != "CREATED_BY_ID" )
						sb.AppendLine("								" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
				}
			}

			int nAuditOnlyCount = 0;
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && !hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
					nAuditOnlyCount++;
			}
			if ( nAuditOnlyCount > 0 )
			{
				// 10/09/2008 Paul.  Audit only columns. 
				sb.AppendLine("								if ( bPast )");
				sb.AppendLine("								{");
				// 10/10/2008 Paul.  CREATED_BY_ID is a special case that needs to be manually added. 
				// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
				//if ( hashAuditColumns.ContainsKey("CREATED_BY_ID") )
				//	sb.AppendLine("									CREATED_BY_ID                  = Sql.ToGuid    (rdr[\"CREATED_BY\"                    ]);");
				foreach ( DataRowView row in vwAllColumns )
				{
					string sColumnName = Sql.ToString (row["ColumnName"]);
					string sCsType     = Sql.ToString (row["CsType"    ]);
					switch ( sCsType )
					{
						case "string"    :  sCsType = "String" ;  break;
						case "ansistring":  sCsType = "String" ;  break;
						case "bool"      :  sCsType = "Boolean";  break;
						case "decimal"   :  sCsType = "Decimal";  break;
						case "Int32"     :  sCsType = "Integer";  break;
						case "float"     :  sCsType = "Float"  ;  break;
					}
					sCsType = sCsType.Replace("ansi", "");
					vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
					if ( vwProc.Count == 0 && !hashViewColumns.ContainsKey(sColumnName) && hashAuditColumns.ContainsKey(sColumnName) )
					{
						sb.AppendLine("									" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
					}
				}
				sb.AppendLine("								}");
			}

			int nViewOnlyCount = 0;
			foreach ( DataRowView row in vwAllColumns )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
				if ( vwProc.Count == 0 && hashViewColumns.ContainsKey(sColumnName) && !hashAuditColumns.ContainsKey(sColumnName) )
					nViewOnlyCount++;
			}
			if ( nViewOnlyCount > 0 )
			{
				// 10/09/2008 Paul.  View only columns. 
				sb.AppendLine("								if ( !bPast )");
				sb.AppendLine("								{");
				// 10/10/2008 Paul.  CREATED_BY_ID is a special case that needs to be manually added. 
				// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
				//if ( hashViewColumns.ContainsKey("CREATED_BY_ID") )
				//	sb.AppendLine("									CREATED_BY_ID                  = Sql.ToGuid    (rdr[\"CREATED_BY_ID\"                 ]);");
				foreach ( DataRowView row in vwAllColumns )
				{
					string sColumnName = Sql.ToString (row["ColumnName"]);
					string sCsType     = Sql.ToString (row["CsType"    ]);
					switch ( sCsType )
					{
						case "string"    :  sCsType = "String" ;  break;
						case "ansistring":  sCsType = "String" ;  break;
						case "bool"      :  sCsType = "Boolean";  break;
						case "decimal"   :  sCsType = "Decimal";  break;
						case "Int32"     :  sCsType = "Integer";  break;
						case "float"     :  sCsType = "Float"  ;  break;
					}
					sCsType = sCsType.Replace("ansi", "");
					vwProc.RowFilter = "ColumnName = '" + sColumnName + "'";
					if ( vwProc.Count == 0 && hashViewColumns.ContainsKey(sColumnName) && !hashAuditColumns.ContainsKey(sColumnName) )
					{
						sb.AppendLine("									" + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = Sql.To" + sCsType + Strings.Space(8-sCsType.Length) + "(rdr[\"" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + "]);");
					}
				}
				sb.AppendLine("								}");
			}

			sb.AppendLine("							}");
			sb.AppendLine("						}");
			sb.AppendLine("					}");
			sb.AppendLine("				}");
			sb.AppendLine("			}");
			sb.AppendLine("			catch(Exception ex)");
			sb.AppendLine("			{");
			sb.AppendLine("				SplendidError.SystemMessage(app.Context, \"Error\", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));");
			sb.AppendLine("				throw(new Exception(\"" + sSINGULAR_NAME + "Activity.Load failed: \" + ex.Message, ex));");
			sb.AppendLine("			}");
			sb.AppendLine("		}");
			sb.AppendLine();
			sb.AppendLine("		protected override void Save()");
			sb.AppendLine("		{");
			sb.AppendLine("			try");
			sb.AppendLine("			{");
			sb.AppendLine("				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);");
			sb.AppendLine("				using ( IDbConnection con = dbf.CreateConnection() )");
			sb.AppendLine("				{");
			sb.AppendLine("					con.Open();");
			// 10/07/2009 Paul.  We need to create our own global transaction ID to support auditing and workflow on SQL Azure, PostgreSQL, Oracle, DB2 and MySQL. 
			sb.AppendLine("					using ( IDbTransaction trn = Sql.BeginTransaction(con) )");
			sb.AppendLine("					{");
			sb.AppendLine("						try");
			sb.AppendLine("						{");
			sb.AppendLine("							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. ");
			sb.AppendLine("							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly(\"" + sTABLE_NAME + "\", WORKFLOW_ID, this.WorkflowInstanceId, trn);");
			sb.AppendLine("							Guid gID = ID;");

			if ( sTABLE_NAME == "PROJECT" || sTABLE_NAME == "PROJECT_TASK" )
				sb.AppendLine("							SqlProcs.sp" + sTABLE_NAME + "S_Update");
			else
				sb.AppendLine("							SqlProcs.sp" + sTABLE_NAME + "_Update");
			// 12/21/2012 Paul.  ID field might not be an output parameter. 
			vwProc.RowFilter = "ColumnName = 'ID' and isoutparam = 1";
			if ( vwProc.Count == 1 )
				sb.AppendLine("								( ref gID");
			else
				sb.AppendLine("								( gID");
			vwProc.RowFilter = String.Empty;
			foreach ( DataRowView row in vwProc )
			{
				string sColumnName = Sql.ToString (row["ColumnName"]);
				if ( sColumnName != "MODIFIED_USER_ID" && sColumnName != "ID" )
				{
					sb.AppendLine("								, " + sColumnName);
				}
			}
			sb.AppendLine("								, trn");
			sb.AppendLine("								);");
			sb.AppendLine("							ID = gID;");

			// 02/15/2009 Paul.  Add support for updating custom fields in the workflow engine. 
			// 02/18/2009 Paul.  We need to know if the column is an identity so the workflow engine can avoid updating it.
			DataView vwCustomFields = new DataView(SplendidCache.FieldsMetaData_Validated(sTABLE_NAME));
			vwCustomFields.RowFilter = "IsIdentity = 0";
			if ( vwCustomFields.Count > 0 )
			{
				sb.AppendLine("							using ( IDbCommand cmd = con.CreateCommand() )");
				sb.AppendLine("							{");
				sb.AppendLine("								cmd.Transaction = trn;");
				sb.AppendLine("								cmd.CommandType = CommandType.Text;");
				sb.AppendLine("								cmd.CommandText =  \"update " + sTABLE_NAME + "_CSTM\";");
				int nFieldIndex = 0;
				foreach(DataRowView row in vwCustomFields)
				{
					string sColumnName = Sql.ToString(row["NAME"]).ToUpper();
					if ( nFieldIndex == 0 )
						sb.AppendLine("								cmd.CommandText += \"   set " + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = @" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + ";");
					else
						sb.AppendLine("								cmd.CommandText += \"     , " + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + " = @" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + ";");
					nFieldIndex++;
				}
				sb.AppendLine("								cmd.CommandText += \" where ID_C = @ID_C\";");
				foreach(DataRowView row in vwCustomFields)
				{
					string sColumnName = Sql.ToString(row["NAME"]).ToUpper();
					sb.AppendLine("								Sql.AddParameter(cmd, \"@" + sColumnName + "\"" + Strings.Space(nMaxSpace-sColumnName.Length) + ", " + sColumnName + Strings.Space(nMaxSpace-sColumnName.Length) + ");");
				}
				sb.AppendLine("								Sql.AddParameter(cmd, \"@ID_C\"                          , gID                           );");
				sb.AppendLine("								cmd.ExecuteNonQuery();");
				sb.AppendLine("							}");
			}
			
			sb.AppendLine("							trn.Commit();");
			sb.AppendLine("						}");
			sb.AppendLine("						catch");
			sb.AppendLine("						{");
			sb.AppendLine("							trn.Rollback();");
			// 12/25/2008 Paul.  Re-throw the original exception so as to retain the call stack. 
			// The difference between these two variations is subtle but important. With the first example, the higher level
			// caller isn�t going to get all the information about the original error. The call stack in the exception is replaced
			// with a new call stack that originates at the �throw ex�Estatement �Ewhich is not what we want to record. The
			// second example is the only one that actually re-throws the original exception, preserving the stack trace where
			// the original error occurred.
			//sb.AppendLine("							throw(ex);");
			sb.AppendLine("							throw;");
			sb.AppendLine("						}");
			sb.AppendLine("					}");
			sb.AppendLine("				}");
			sb.AppendLine("			}");
			sb.AppendLine("			catch(Exception ex)");
			sb.AppendLine("			{");
			sb.AppendLine("				SplendidError.SystemMessage(app.Context, \"Error\", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));");
			sb.AppendLine("				throw(new Exception(\"" + sSINGULAR_NAME + "Activity.Save failed: \" + ex.Message, ex));");
			sb.AppendLine("			}");
			sb.AppendLine("		}");
			sb.AppendLine("	}");
			sb.AppendLine("}");
			sb.AppendLine();
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// 01/11/2006 Paul.  Only a developer/administrator should see this. 
			if ( !SplendidCRM.Security.IS_ADMIN || Request.ServerVariables["SERVER_NAME"] != "localhost" )
				return;
			
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( SqlConnection con = dbf.CreateConnection() as SqlConnection )
			{
				string sSQL;
				ArrayList arrProcedures = new ArrayList();
				// 08/13/2014 Paul.  Add relative path for generation of CampaignLog. 
				sSQL = "select MODULE_NAME       " + ControlChars.CrLf
				     + "     , TABLE_NAME        " + ControlChars.CrLf
				     + "     , RELATIVE_PATH     " + ControlChars.CrLf
				     + "  from vwMODULES_Workflow" + ControlChars.CrLf
				     + " where 1 = 1             " + ControlChars.CrLf;
				using ( DataTable dtModules = new DataTable() )
				{
					using ( SqlCommand cmd = new SqlCommand(sSQL, con) )
					{
						// 10/18/2010 Paul.  We may want to only update a single module. 
						string sModules = Sql.ToString(Request["Module"]);
						if ( !Sql.IsEmptyString(sModules) )
						{
							string[] arrModules = sModules.Split(',');
							Sql.AppendParameter(cmd, arrModules, "MODULE_NAME");
						}
						cmd.CommandText += " order by MODULE_NAME    " + ControlChars.CrLf;
						using ( SqlDataAdapter da = new SqlDataAdapter(cmd) )
						{
							da.Fill(dtModules);
						}
					}
					foreach ( DataRow rowModule in dtModules.Rows )
					{
						string sMODULE_NAME = Sql.ToString(rowModule["MODULE_NAME"]);
						string sTABLE_NAME  = Sql.ToString(rowModule["TABLE_NAME" ]);
						// 08/13/2014 Paul.  Add relative path for generation of CampaignLog. 
						string sRELATIVE_PATH = Sql.ToString(rowModule["RELATIVE_PATH"]);
						string sSINGULAR_NAME = sMODULE_NAME;
						// 10/22/2008 Paul.  Fix to support Opportunities. 
						if ( sSINGULAR_NAME.EndsWith("ies") )
							sSINGULAR_NAME = sSINGULAR_NAME.Substring(0, sSINGULAR_NAME.Length-3) + "y";
						else if ( sSINGULAR_NAME.EndsWith("s") )
							sSINGULAR_NAME = sSINGULAR_NAME.Substring(0, sSINGULAR_NAME.Length-1);
						// 12/21/2012 Paul.  If the module ends in Y, then don't append an s. 
						if ( !sMODULE_NAME.EndsWith("s") && !sMODULE_NAME.EndsWith("y") && !sMODULE_NAME.EndsWith("Log") )
							sMODULE_NAME += "s";

						// 10/08/2008 Paul.  When selecting the data, we need to make sure that it will be available in the view and the audit table. 
						Hashtable hashViewColumns = new Hashtable();
						Hashtable hashAuditColumns = new Hashtable();
						sSQL = "select ColumnName              " + ControlChars.CrLf
						     + "  from vwSqlColumns            " + ControlChars.CrLf
						     + " where ObjectName = @ViewName  " + ControlChars.CrLf
						     + "   and ObjectType = 'V'        " + ControlChars.CrLf
						     + "   and ColumnName <> 'ID_C'    " + ControlChars.CrLf
						     + " order by colid                " + ControlChars.CrLf;
						using ( SqlCommand cmd = new SqlCommand(sSQL, con) )
						{
							if ( sTABLE_NAME == "PROJECT" || sTABLE_NAME == "PROJECT_TASK" )
								Sql.AddParameter(cmd, "@ViewName", "vw" + sTABLE_NAME + "S_Edit");
							else
								Sql.AddParameter(cmd, "@ViewName", "vw" + sTABLE_NAME + "_Edit");
							using ( SqlDataAdapter da = new SqlDataAdapter(cmd) )
							{
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									foreach ( DataRow row in dt.Rows )
									{
										string sColumnName = Sql.ToString(row["ColumnName"]);
										if ( !hashViewColumns.ContainsKey(sColumnName) )
										{
											hashViewColumns.Add(sColumnName, null);
										}
									}
								}
							}
						}
						// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
						sSQL = "select ColumnName                   " + ControlChars.CrLf
						     + "  from vwSqlColumns                 " + ControlChars.CrLf
						     + " where ObjectName = @AuditName      " + ControlChars.CrLf
						     + "   and ObjectType = 'V'             " + ControlChars.CrLf
						     + "   and ColumnName not like 'AUDIT_%'" + ControlChars.CrLf
						     + "   and ColumnName not in ('DELETED')" + ControlChars.CrLf
						     + " order by colid                     " + ControlChars.CrLf;
						using ( SqlCommand cmd = new SqlCommand(sSQL, con) )
						{
							Sql.AddParameter(cmd, "@AuditName", "vw" + sTABLE_NAME + "_AUDIT");
							using ( SqlDataAdapter da = new SqlDataAdapter(cmd) )
							{
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									foreach ( DataRow row in dt.Rows )
									{
										string sColumnName = Sql.ToString(row["ColumnName"]);
										if ( !hashAuditColumns.ContainsKey(sColumnName) )
										{
											hashAuditColumns.Add(sColumnName, null);
										}
									}
								}
							}
						}
						// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
						//hashAuditColumns.Add("CREATED_BY_ID", null);

						// 11/22/2008 Paul.  Now that we are using the audit view, we don't need to treat CREATED_BY as a special case. 
						sSQL = "select replace(ColumnName, '@', '') as ColumnName" + ControlChars.CrLf
						     + "     , CsType                       " + ControlChars.CrLf
						     + "  from vwSqlColumns                 " + ControlChars.CrLf
						     + " where ObjectName = @UPDATE_PROC    " + ControlChars.CrLf
						     + "   and ObjectType = 'P'             " + ControlChars.CrLf
						     + " union                              " + ControlChars.CrLf
						     + "select ColumnName                   " + ControlChars.CrLf
						     + "     , CsType                       " + ControlChars.CrLf
						     + "  from vwSqlColumns                 " + ControlChars.CrLf
						     + " where ObjectName = @ViewName       " + ControlChars.CrLf
						     + "   and ObjectType = 'V'             " + ControlChars.CrLf
						     + "   and ColumnName <> 'ID_C'         " + ControlChars.CrLf
						     + " union                              " + ControlChars.CrLf
						     + "select ColumnName                   " + ControlChars.CrLf
						     + "     , CsType                       " + ControlChars.CrLf
						     + "  from vwSqlColumns                 " + ControlChars.CrLf
						     + " where ObjectName = @AuditName      " + ControlChars.CrLf
						     + "   and ObjectType = 'U'             " + ControlChars.CrLf
						     + "   and ColumnName not like 'AUDIT_%'" + ControlChars.CrLf
						     + "   and ColumnName not in ('DELETED')" + ControlChars.CrLf
						     + " order by ColumnName                " + ControlChars.CrLf;
						DataView vwAllColumns = null;
						using ( SqlCommand cmd = new SqlCommand(sSQL, con) )
						{
							if ( sTABLE_NAME == "PROJECT" || sTABLE_NAME == "PROJECT_TASK" )
							{
								Sql.AddParameter(cmd, "@UPDATE_PROC", "sp" + sTABLE_NAME + "S_Update");
								Sql.AddParameter(cmd, "@ViewName"   , "vw" + sTABLE_NAME + "S_Edit"  );
								Sql.AddParameter(cmd, "@AuditName"  , "vw" + sTABLE_NAME + "_AUDIT"  );
							}
							else
							{
								Sql.AddParameter(cmd, "@UPDATE_PROC", "sp" + sTABLE_NAME + "_Update");
								Sql.AddParameter(cmd, "@ViewName"   , "vw" + sTABLE_NAME + "_Edit"  );
								Sql.AddParameter(cmd, "@AuditName"  , "vw" + sTABLE_NAME + "_AUDIT" );
							}
							using ( SqlDataAdapter da = new SqlDataAdapter(cmd) )
							{
								DataTable dt = new DataTable();
								da.Fill(dt);
								vwAllColumns = new DataView(dt);
							}
						}
						// 12/21/2012 Paul.  Add isoutparam. 
						sSQL = "select replace(ColumnName, '@', '') as ColumnName" + ControlChars.CrLf
						     + "     , CsType                   " + ControlChars.CrLf
						     + "     , isoutparam               " + ControlChars.CrLf
						     + "  from vwSqlColumns             " + ControlChars.CrLf
						     + " where ObjectName = @UPDATE_PROC" + ControlChars.CrLf
						     + "   and ObjectType = 'P'         " + ControlChars.CrLf
						     + " order by colid                 " + ControlChars.CrLf;
						using ( SqlCommand cmd = new SqlCommand(sSQL, con) )
						{
							if ( sTABLE_NAME == "PROJECT" || sTABLE_NAME == "PROJECT_TASK" )
								Sql.AddParameter(cmd, "@UPDATE_PROC", "sp" + sTABLE_NAME + "S_Update");
							else
								Sql.AddParameter(cmd, "@UPDATE_PROC", "sp" + sTABLE_NAME + "_Update");
							using ( SqlDataAdapter da = new SqlDataAdapter(cmd) )
							{
								using ( DataTable dt = new DataTable() )
								{
									da.Fill(dt);
									DataView vwProc = new DataView(dt);
									StringBuilder sb = new StringBuilder();
									BuildActivity(ref sb, con.Database, sSINGULAR_NAME, sTABLE_NAME, vwProc, vwAllColumns, hashViewColumns, hashAuditColumns);
									try
									{
										// 01/16/2008 Paul.  Procedures.aspx was moved to the _devtools folder, 
										// so we need to make sure to reference SqlProcs in the _code folder. 
										// 08/13/2014 Paul.  Add relative path for generation of CampaignLog. 
										string sSqlProcsPath = Server.MapPath(sRELATIVE_PATH + "/" + sSINGULAR_NAME + "Activity.cs");
										Response.Write(sSqlProcsPath);
										using(StreamWriter stm = File.CreateText(sSqlProcsPath))
										{
											stm.Write(sb.ToString());
										}
									}
									catch(Exception ex)
									{
										Response.Write("<font color=red> &nbsp; " + ex.Message + "</font>");
										SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
									}
									Response.Write("<br />" + ControlChars.CrLf);
								}
							}
						}
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
