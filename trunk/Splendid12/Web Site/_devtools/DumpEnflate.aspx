<%@ Page Language="C#" AutoEventWireup="true" ClassName="SplendidCRM.DumpEnflate" Inherits="System.Web.UI.Page" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>
<script runat="server">
/**
 * Copyright (C) 2011-2013 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
protected void btnUpload_Click(object sender, EventArgs e)
{
	try
	{
		HttpPostedFile pstIMPORT = fileIMPORT.PostedFile;
		if ( pstIMPORT != null )
		{
			if ( pstIMPORT.FileName.Length > 0 )
			{
				Stream stm = pstIMPORT.InputStream;
				// 12/12/2011 Paul.  Need to remove the first 2 bytes of a deflated stream. 
				// http://connect.microsoft.com/VisualStudio/feedback/details/97064/deflatestream-throws-exception-when-inflating-pdf-streams
				stm.ReadByte();
				stm.ReadByte();
				DeflateStream dec = new DeflateStream(stm, CompressionMode.Decompress);
				using ( BinaryReader reader = new BinaryReader(dec) )
				{
					byte[] binBYTES = reader.ReadBytes((int) stm.Length);
					txtBase64.Text = UTF8Encoding.ASCII.GetString(binBYTES);
				}
			}
		}
	}
	catch(Exception ex)
	{
		lblMsg.Text = ex.Message;
	}
}
</script>
<%@ Import Namespace="System.Xml" %>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Dump Enflate</title>
</head>
<body>
<form id="frm" runat="server">
	<div>
		<asp:Label ID="lblMsg" CssClass="err" EnableViewState="false" runat="server" />
	</div>
	<input id="fileIMPORT" type="file" size="60" runat="server" />
	<asp:Button ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" runat="server" />

	<asp:TextBox ID="txtBase64" TextMode="MultiLine" Rows="30" Width="100%" runat="server" />
</form>
</body>
</html>
