/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Diagnostics;

namespace SplendidCRM._devtools
{
	/// <summary>
	/// Summary description for Terminology_en_US.
	/// </summary>
	public class Terminology_en_US : System.Web.UI.Page
	{
		protected void DumpTerminology(StringBuilder sb, IDbCommand cmd, string sProcedureName, string sMODULE_NAME, int nNAME_MaxLength, int nLIST_NAME_MaxLength)
		{
			string sLAST_LIST_NAME = String.Empty;
			using ( SqlDataReader rdr = (SqlDataReader) cmd.ExecuteReader() )
			{
				while ( rdr.Read() )
				{
					if ( sLAST_LIST_NAME != Sql.ToString(rdr["LIST_NAME"]) )
					{
						sLAST_LIST_NAME = Sql.ToString(rdr["LIST_NAME"]);
						if ( !sLAST_LIST_NAME.StartsWith("moduleList") )
							sb.AppendLine();
					}
					sb.Append("exec dbo." + sProcedureName + " ");
					for ( int nColumn=0 ; nColumn < rdr.FieldCount ; nColumn++ )
					{
						if ( nColumn > 0 )
							sb.Append(", ");
						if ( rdr.IsDBNull(nColumn) )
							sb.Append("null");
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Boolean" ) ) sb.Append(rdr.GetBoolean (nColumn) ? "1" : "0" );
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Single"  ) ) sb.Append(rdr.GetDouble  (nColumn).ToString());
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Double"  ) ) sb.Append(rdr.GetDouble  (nColumn).ToString());
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Int16"   ) ) sb.Append(rdr.GetInt16   (nColumn).ToString().PadLeft(3));
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Int32"   ) ) sb.Append(rdr.GetInt32   (nColumn).ToString().PadLeft(3));
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Int64"   ) ) sb.Append(rdr.GetInt64   (nColumn).ToString().PadLeft(3));
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Decimal" ) ) sb.Append(rdr.GetDecimal (nColumn).ToString());
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.DateTime") ) sb.Append("\'" + rdr.GetDateTime(nColumn).ToString("yyyy-MM-dd HH:mm:ss") + "\'");
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.Guid"    ) ) sb.Append("\'" + rdr.GetGuid  (nColumn).ToString().ToUpper() + "\'");
						// 05/19/2008 Paul.  Unicode strings must be marked as such, otherwise unicode will go in as ???.
						// http://www.microsoft.com/globaldev/DrIntl/columns/001/default.mspx#E6B
						else if ( rdr.GetFieldType(nColumn) == Type.GetType("System.String"  ) ) sb.Append("N\'" + rdr.GetString(nColumn).Replace("\'", "\'\'") + "\'");
						else sb.Append("null");
						// 11/21/2005 Paul.  Align the name field. 
						if ( rdr.GetName(nColumn) == "NAME" )
						{
							// 08/20/2010 Paul.  NAME will be null in some lists, such as published_reports_dom and saved_reports_dom. 
							string sNAME = String.Empty;
							if ( !rdr.IsDBNull(nColumn) )
								sNAME = rdr.GetString(nColumn);
							if ( nNAME_MaxLength - sNAME.Length > 0 )
								sb.Append(Strings.Space(nNAME_MaxLength - sNAME.Length));
						}
						else if ( rdr.GetName(nColumn) == "LIST_NAME" )
						{
							string sNAME = String.Empty;
							if ( !rdr.IsDBNull(nColumn) )
							{
								sNAME = rdr.GetString(nColumn);
								if ( nLIST_NAME_MaxLength - sNAME.Length > 0 )
									sb.Append(Strings.Space(nLIST_NAME_MaxLength - sNAME.Length));
							}
						}
					}
					sb.AppendLine(";");
				}
			}
		}

		protected void DumpAllTerms(string sSqlScriptPath, string sLANG)
		{
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					string sSQL;
					sSQL = "select max(len(NAME     )) as NAME_LENGTH     " + ControlChars.CrLf
					     + "     , max(len(LIST_NAME)) as LIST_NAME_LENGTH" + ControlChars.CrLf
					     + "  from TERMINOLOGY                            " + ControlChars.CrLf
					     + " where lower(LANG) = @LANG                    " + ControlChars.CrLf;
					cmd.CommandText = sSQL;
					cmd.Parameters.Clear();
					Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
					int nNAME_MaxLength      = 0;
					int nLIST_NAME_MaxLength = 0;
					using ( SqlDataReader rdr = (SqlDataReader) cmd.ExecuteReader() )
					{
						if ( rdr.Read() )
						{
							nNAME_MaxLength      = Sql.ToInteger(rdr["NAME_LENGTH"     ]) + 2;
							nLIST_NAME_MaxLength = Sql.ToInteger(rdr["LIST_NAME_LENGTH"]) + 2;
						}
					}
					
					sSQL = "select distinct            " + ControlChars.CrLf
					     + "       MODULE_NAME         " + ControlChars.CrLf
					     + "  from vwTERMINOLOGY       " + ControlChars.CrLf
					     + " where lower(LANG) = @LANG " + ControlChars.CrLf
					     + " order by MODULE_NAME      " + ControlChars.CrLf;
					cmd.CommandText = sSQL;
					cmd.Parameters.Clear();
					// 03/06/2006 Paul.  Oracle is case sensitive, and we modify the case of L10n.NAME to be lower. 
					Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							foreach ( DataRow row in dt.Rows )
							{
								string sMODULE_NAME = Sql.ToString(row["MODULE_NAME"]);
								string sMODULE_FILE_NAME = sMODULE_NAME;
								if ( Sql.IsEmptyString(sMODULE_FILE_NAME) )
									sMODULE_FILE_NAME = "Global";
								
								StringBuilder sb = new StringBuilder();
								if ( Sql.IsOracle(con) )
								{
									sb.AppendLine("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS';");
									sb.AppendLine("BEGIN");
								}
								else
								{
									sb.AppendLine("/* -- #if IBM_DB2");
									sb.AppendLine("call dbo.spSqlDropProcedure('spTERMINOLOGY_" + sMODULE_FILE_NAME + "_" + sLANG.Replace("-", "_") + "')");
									sb.AppendLine("/");
									sb.AppendLine("");
									sb.AppendLine("Create Procedure dbo.spTERMINOLOGY_" + sMODULE_FILE_NAME + "_" + sLANG.Replace("-", "_") + "()");
									sb.AppendLine("language sql");
									sb.AppendLine("  begin");
									sb.AppendLine("-- #endif IBM_DB2 */");
									sb.AppendLine();
									
									sb.AppendLine("/* -- #if Oracle");
									sb.AppendLine("BEGIN");
									sb.AppendLine("-- #endif Oracle */");
									sb.AppendLine();
									
									sb.AppendLine("-- Terminology generated from database [" + con.Database + "] on " + DateTime.Now.ToString() + ".");
									sb.AppendLine("print 'TERMINOLOGY " + sMODULE_FILE_NAME + " " + sLANG + "';");
									sb.AppendLine("GO");
									sb.AppendLine();
									sb.AppendLine("set nocount on;");
									sb.AppendLine("GO");
									sb.AppendLine();
								}
								sSQL = "select NAME              " + ControlChars.CrLf
								     + "     , LANG              " + ControlChars.CrLf
								     + "     , MODULE_NAME       " + ControlChars.CrLf
								     + "     , LIST_NAME         " + ControlChars.CrLf
								     + "     , LIST_ORDER        " + ControlChars.CrLf
								     + "     , DISPLAY_NAME      " + ControlChars.CrLf
								     + "  from vwTERMINOLOGY     " + ControlChars.CrLf
								     + " where lower(LANG)= @LANG" + ControlChars.CrLf
								     + "   and LIST_NAME is null " + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								cmd.Parameters.Clear();
								Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
								if ( Sql.IsEmptyString(sMODULE_NAME) )
								{
									cmd.CommandText += "   and MODULE_NAME is null" + ControlChars.CrLf;
								}
								else
								{
									cmd.CommandText += "   and lower(MODULE_NAME) = @MODULE_NAME" + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
								}
								cmd.CommandText += " order by LANG, MODULE_NAME, LIST_NAME, LIST_ORDER, NAME" + ControlChars.CrLf;
								
								// 11/17/2010 Paul.  Dump the module-specific terms. 
								DumpTerminology(sb, cmd, "spTERMINOLOGY_InsertOnly", sMODULE_NAME, nNAME_MaxLength, nLIST_NAME_MaxLength);
								
								// 11/17/2010 Paul.  Put the moduleList entries at the top. 
								int nModuleListEntries = 0;
								if ( !Sql.IsEmptyString(sMODULE_NAME) )
								{
									sSQL = "select count(*)                    " + ControlChars.CrLf
									     + "  from vwTERMINOLOGY               " + ControlChars.CrLf
									     + " where lower(LANG)= @LANG          " + ControlChars.CrLf
									     + "   and LIST_NAME like 'moduleList%'" + ControlChars.CrLf
									     + "   and NAME = @MODULE_NAME         " + ControlChars.CrLf;
									cmd.CommandText = sSQL;
									cmd.Parameters.Clear();
									Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
									Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
									nModuleListEntries = Sql.ToInteger(cmd.ExecuteScalar());
									if ( nModuleListEntries > 0 )
									{
										sb.AppendLine("GO");
										sb.AppendLine("/* -- #if Oracle");
										sb.AppendLine("	COMMIT WORK;");
										sb.AppendLine("END;");
										sb.AppendLine("/");
										sb.AppendLine();
										sb.AppendLine("BEGIN");
										sb.AppendLine("-- #endif Oracle */");
										
										sSQL = "select NAME                        " + ControlChars.CrLf
										     + "     , LANG                        " + ControlChars.CrLf
										     + "     , MODULE_NAME                 " + ControlChars.CrLf
										     + "     , LIST_NAME                   " + ControlChars.CrLf
										     + "     , LIST_ORDER                  " + ControlChars.CrLf
										     + "     , DISPLAY_NAME                " + ControlChars.CrLf
										     + "  from vwTERMINOLOGY               " + ControlChars.CrLf
										     + " where lower(LANG)= @LANG          " + ControlChars.CrLf;
										cmd.CommandText = sSQL;
										cmd.Parameters.Clear();
										Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
										if ( sMODULE_NAME == "Invoices" )
										{
											cmd.CommandText += "   and LIST_NAME like 'moduleList%'" + ControlChars.CrLf
											                +  "   and NAME in (@MODULE_NAME, 'InvoicesLineItems')" + ControlChars.CrLf;
										}
										else if ( sMODULE_NAME == "Orders" )
										{
											cmd.CommandText += "   and LIST_NAME like 'moduleList%'" + ControlChars.CrLf
											                +  "   and NAME in (@MODULE_NAME, 'OrdersLineItems')" + ControlChars.CrLf;
										}
										else if ( sMODULE_NAME == "Quotes" )
										{
											cmd.CommandText += "   and LIST_NAME like 'moduleList%'" + ControlChars.CrLf
											                +  "   and NAME in (@MODULE_NAME, 'QuotesLineItems')" + ControlChars.CrLf;
										}
										else
										{
											cmd.CommandText += "   and LIST_NAME like 'moduleList%'" + ControlChars.CrLf
											                +  "   and NAME = @MODULE_NAME         " + ControlChars.CrLf;
										}
										Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
										cmd.CommandText += " order by LANG, MODULE_NAME, LIST_NAME, LIST_ORDER, NAME" + ControlChars.CrLf;
										
										DumpTerminology(sb, cmd, "spTERMINOLOGY_InsertOnly", sMODULE_NAME, nNAME_MaxLength, nLIST_NAME_MaxLength);
									}
								}
								// 11/17/2010 Paul.  Try and isolate the lists that are specific to a module. 
								sSQL = "select NAME              " + ControlChars.CrLf
								     + "     , LANG              " + ControlChars.CrLf
								     + "     , MODULE_NAME       " + ControlChars.CrLf
								     + "     , LIST_NAME         " + ControlChars.CrLf
								     + "     , LIST_ORDER        " + ControlChars.CrLf
								     + "     , DISPLAY_NAME      " + ControlChars.CrLf
								     + "  from vwTERMINOLOGY     " + ControlChars.CrLf
								     + " where lower(LANG)= @LANG" + ControlChars.CrLf;
								cmd.CommandText = sSQL;
								cmd.Parameters.Clear();
								Sql.AddParameter(cmd, "@LANG", sLANG.ToLower());
								if ( Sql.IsEmptyString(sMODULE_NAME) )
								{
									// 11/17/2010 Paul.  Keep countries in a separate file. 
									cmd.CommandText += "   and LIST_NAME in ('states_dom')" + ControlChars.CrLf;
								}
								else
								{
									if ( sMODULE_NAME == "Activities" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('activity_dom', 'appointment_filter_dom', 'dom_meeting_accept_options', 'dom_meeting_accept_status')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Reports" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ( 'dom_report_types'  , 'published_reports_dom', 'saved_reports_dom'   , 'ansistring_operator_dom'" 
										                +  "                        , 'bool_operator_dom' , 'datetime_operator_dom', 'float_operator_dom'  , 'guid_operator_dom'" 
										                +  "                        , 'int32_operator_dom', 'enum_operator_dom'    , 'decimal_operator_dom', 'string_operator_dom'" 
										                +  "                        )" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Audit" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('audit_action_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "CampaignLog" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('campainglog_activity_type_dom', 'campainglog_target_type_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Campaigns" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('roi_type_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Emails" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('dom_email_status', 'dom_email_types')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "UserLogins" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('login_type_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Users" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('notifymail_sendtype')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Manufacturers" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('manufacturer_status_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Shippers" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('shipper_status_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "TaxRates" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('tax_rate_status_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Modules" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('record_type_display')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Teams" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('team_membership_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Schedulers" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('scheduler_day_dom', 'scheduler_frequency_dom', 'scheduler_month_dom', 'scheduler_status_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "PayPal" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME like 'paypal_%'" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Rules" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME in ('rules_reevaluation_dom', 'rules_chaining_dom')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else if ( sMODULE_NAME == "Workflows" )
									{
										cmd.CommandText += "   and (   LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" 
										                +  "        or LIST_NAME like 'workflow_%'" + ControlChars.CrLf
										                +  "        or LIST_NAME in ('value_type')" + ControlChars.CrLf
										                +  "       )" + ControlChars.CrLf;
									}
									else
									{
										cmd.CommandText += "   and LIST_NAME in (select CACHE_NAME from vwEDITVIEWS_FIELDS where EDIT_NAME like @MODULE_NAME + '.%')" + ControlChars.CrLf;
									}
									cmd.CommandText += "   and LIST_NAME not in ('countries_dom', 'states_dom')" + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@MODULE_NAME", sMODULE_NAME);
								}
								cmd.CommandText += " order by LANG, MODULE_NAME, LIST_NAME, LIST_ORDER, NAME" + ControlChars.CrLf;
								
								int nListItems = 0;
								using ( SqlDataReader rdr = (SqlDataReader) cmd.ExecuteReader() )
								{
									if ( rdr.Read() )
									{
										nListItems = 1;
									}
								}
								if ( nListItems > 0 )
								{
									if ( nModuleListEntries == 0 )
									{
										sb.AppendLine("GO");
										sb.AppendLine("/* -- #if Oracle");
										sb.AppendLine("	COMMIT WORK;");
										sb.AppendLine("END;");
										sb.AppendLine("/");
										sb.AppendLine();
										sb.AppendLine("BEGIN");
										sb.AppendLine("-- #endif Oracle */");
									}
									DumpTerminology(sb, cmd, "spTERMINOLOGY_InsertOnly", sMODULE_NAME, nNAME_MaxLength, nLIST_NAME_MaxLength);
								}
								
								if ( Sql.IsDB2(cmd) )
								{
									sb.AppendLine("/");
									sb.AppendLine();
								}
								else if ( Sql.IsOracle(cmd) )
								{
									sb.AppendLine("	COMMIT WORK;");
									sb.AppendLine("END;");
									sb.AppendLine("/");
								}
								else
								{
									sb.AppendLine("GO");
									sb.AppendLine();
									sb.AppendLine();
									sb.AppendLine("set nocount off;");
									sb.AppendLine("GO");
									sb.AppendLine();
									sb.AppendLine("/* -- #if Oracle");
									sb.AppendLine("	COMMIT WORK;");
									sb.AppendLine("END;");
									sb.AppendLine("/");
									sb.AppendLine("-- #endif Oracle */");

									sb.AppendLine("");
									sb.AppendLine("/* -- #if IBM_DB2");
									sb.AppendLine("	commit;");
									sb.AppendLine("  end");
									sb.AppendLine("/");
									sb.AppendLine("");

									sb.AppendLine("call dbo.spTERMINOLOGY_" + sMODULE_FILE_NAME + "_" + sLANG.Replace("-", "_") + "()");
									sb.AppendLine("/");
									sb.AppendLine("");

									sb.AppendLine("call dbo.spSqlDropProcedure('spTERMINOLOGY_" + sMODULE_FILE_NAME + "_" + sLANG.Replace("-", "_") + "')");
									sb.AppendLine("/");
									sb.AppendLine("-- #endif IBM_DB2 */");
								}
								
								string sSqlScriptFile = Path.Combine(sSqlScriptPath, (Sql.IsEmptyString(sMODULE_NAME) ? "_Global" : sMODULE_NAME) + "." + sLANG + ".1.sql");
								try
								{
									if ( File.Exists(sSqlScriptFile) )
										File.Delete(sSqlScriptFile);
									using(StreamWriter stm = File.CreateText(sSqlScriptFile))
									{
										stm.Write(sb.ToString());
									}
									Response.Write(sSqlScriptFile + "<br>" + ControlChars.CrLf);
								}
								catch(Exception ex)
								{
									Response.Write("<font class=error>" + sSqlScriptFile + ":" + ex.Message + "</font><br>" + ControlChars.CrLf);
								}
							}
						}
					}
				}
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( !SplendidCRM.Security.IS_ADMIN )  //|| Request.ServerVariables["SERVER_NAME"] != "localhost" )
				return;
			try
			{
				string sLANG = "en-us";
				string sSqlScriptPath = Path.Combine(Server.MapPath("~/"), "..\\SQL Scripts\\Terminology." + sLANG);
				try
				{
					if ( !Directory.Exists(sSqlScriptPath) )
					{
						Directory.CreateDirectory(sSqlScriptPath);
					}
				}
				catch(Exception ex)
				{
					string sText = "<font class=error>Failed to create " + sSqlScriptPath + ":" + ex.Message + "</font><br>" + ControlChars.CrLf;
					Response.Write(sText + ControlChars.CrLf + ex.StackTrace);
					return;
				}
				DumpAllTerms(sSqlScriptPath, sLANG);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				Response.Write(ex.Message + ControlChars.CrLf + ex.StackTrace);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

