<%@ Page language="c#" MasterPageFile="~/ListView.Master" Codebehind="ModuleColors.aspx.cs" AutoEventWireup="false" Inherits="SplendidCRM._devtools.ModuleColors" %>
<script runat="server">
/**
 * Copyright (C) 2015 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<asp:Content ID="cntSidebar" ContentPlaceHolderID="cntSidebar" runat="server">
</asp:Content>

<asp:Content ID="cntBody" ContentPlaceHolderID="cntBody" runat="server">
<br />
<div>Home<br />
<span class="ModuleHeaderModuleTags                      ">ModuleHeaderModuleTags                      </span><br />
<span class="ModuleHeaderModuleDashboard                 ">ModuleHeaderModuleDashboard                 </span><br />
<span class="ModuleHeaderModuleFeeds                     ">ModuleHeaderModuleFeeds                     </span><br />
<span class="ModuleHeaderModuleHome                      ">ModuleHeaderModuleHome                      </span><br />
<span class="ModuleHeaderModuleiFrames                   ">ModuleHeaderModuleiFrames                   </span><br />
<span class="ModuleHeaderModuleHelp                      ">ModuleHeaderModuleHelp                      </span><br />
</div>

<div></div>Marketing<br />
<span class="ModuleHeaderModuleCampaigns                 ">ModuleHeaderModuleCampaigns                 </span><br />
<span class="ModuleHeaderModuleCampaignLog               ">ModuleHeaderModuleCampaignLog               </span><br />
<span class="ModuleHeaderModuleCampaignTrackers          ">ModuleHeaderModuleCampaignTrackers          </span><br />
<span class="ModuleHeaderModuleCallMarketing             ">ModuleHeaderModuleCallMarketing             </span><br />
<span class="ModuleHeaderModuleEmailMarketing            ">ModuleHeaderModuleEmailMarketing            </span><br />
<span class="ModuleHeaderModuleEmailTemplates            ">ModuleHeaderModuleEmailTemplates            </span><br />
<span class="ModuleHeaderModuleProspects                 ">ModuleHeaderModuleProspects                 </span><br />
<span class="ModuleHeaderModuleProspectLists             ">ModuleHeaderModuleProspectLists             </span><br />
</div>

<div>Marketing Admin<br />
<span class="ModuleHeaderModuleEmailMan                  ">ModuleHeaderModuleEmailMan                  </span><br />
<span class="ModuleHeaderModuleSimpleEmail               ">ModuleHeaderModuleSimpleEmail               </span><br />
<span class="ModuleHeaderModuleOutboundEmail             ">ModuleHeaderModuleOutboundEmail             </span><br />
<span class="ModuleHeaderModuleOutboundSms               ">ModuleHeaderModuleOutboundSms               </span><br />

<span class="ModuleHeaderModuleSurveys                   ">ModuleHeaderModuleSurveys                   </span><br />
<span class="ModuleHeaderModuleSurveyPages               ">ModuleHeaderModuleSurveyPages               </span><br />
<span class="ModuleHeaderModuleSurveyQuestions           ">ModuleHeaderModuleSurveyQuestions           </span><br />
<span class="ModuleHeaderModuleSurveyResults             ">ModuleHeaderModuleSurveyResults             </span><br />
<span class="ModuleHeaderModuleSurveyQuestionResults     ">ModuleHeaderModuleSurveyQuestionResults     </span><br />
<span class="ModuleHeaderModuleSurveyThemes              ">ModuleHeaderModuleSurveyThemes              </span><br />
</div>

<div>Sales<br />
<span class="ModuleHeaderModuleOpportunities             ">ModuleHeaderModuleOpportunities             </span><br />
<span class="ModuleHeaderModuleRevenueLineItems          ">ModuleHeaderModuleRevenueLineItems          </span><br />
<span class="ModuleHeaderModuleAccounts                  ">ModuleHeaderModuleAccounts                  </span><br />
<span class="ModuleHeaderModuleContacts                  ">ModuleHeaderModuleContacts                  </span><br />
<span class="ModuleHeaderModuleLeads                     ">ModuleHeaderModuleLeads                     </span><br />
</div>

<div>Order Management<br />
<span class="ModuleHeaderModuleQuotes                    ">ModuleHeaderModuleQuotes                    </span><br />
<span class="ModuleHeaderModuleQuotesLineItems           ">ModuleHeaderModuleQuotesLineItems           </span><br />
<span class="ModuleHeaderModuleOrders                    ">ModuleHeaderModuleOrders                    </span><br />
<span class="ModuleHeaderModuleOrdersLineItems           ">ModuleHeaderModuleOrdersLineItems           </span><br />
<span class="ModuleHeaderModuleInvoices                  ">ModuleHeaderModuleInvoices                  </span><br />
<span class="ModuleHeaderModuleInvoicesLineItems         ">ModuleHeaderModuleInvoicesLineItems         </span><br />
<span class="ModuleHeaderModulePayments                  ">ModuleHeaderModulePayments                  </span><br />
<span class="ModuleHeaderModuleCreditCards               ">ModuleHeaderModuleCreditCards               </span><br />
</div>

<div>Product Admin<br />
<span class="ModuleHeaderModuleProducts                  ">ModuleHeaderModuleProducts                  </span><br />
<span class="ModuleHeaderModuleProductTemplates          ">ModuleHeaderModuleProductTemplates          </span><br />
<span class="ModuleHeaderModuleProductCatalog            ">ModuleHeaderModuleProductCatalog            </span><br />
<span class="ModuleHeaderModuleProductCategories         ">ModuleHeaderModuleProductCategories         </span><br />
<span class="ModuleHeaderModuleProductTypes              ">ModuleHeaderModuleProductTypes              </span><br />
<span class="ModuleHeaderModuleManufacturers             ">ModuleHeaderModuleManufacturers             </span><br />
<span class="ModuleHeaderModuleShippers                  ">ModuleHeaderModuleShippers                  </span><br />
<span class="ModuleHeaderModuleTaxRates                  ">ModuleHeaderModuleTaxRates                  </span><br />
<span class="ModuleHeaderModuleDiscounts                 ">ModuleHeaderModuleDiscounts                 </span><br />
<span class="ModuleHeaderModulePaymentGateway            ">ModuleHeaderModulePaymentGateway            </span><br />
<span class="ModuleHeaderModulePaymentTerms              ">ModuleHeaderModulePaymentTerms              </span><br />
<span class="ModuleHeaderModulePaymentTypes              ">ModuleHeaderModulePaymentTypes              </span><br />
</div>

<div>Productivity / Collaboration<br />
<span class="ModuleHeaderModuleActivities                ">ModuleHeaderModuleActivities                </span><br />
<span class="ModuleHeaderModuleCalls                     ">ModuleHeaderModuleCalls                     </span><br />
<span class="ModuleHeaderModuleMeetings                  ">ModuleHeaderModuleMeetings                  </span><br />
<span class="ModuleHeaderModuleEmails                    ">ModuleHeaderModuleEmails                    </span><br />
<span class="ModuleHeaderModuleEmailClient               ">ModuleHeaderModuleEmailClient               </span><br />
<span class="ModuleHeaderModuleSmsMessages               ">ModuleHeaderModuleSmsMessages               </span><br />
<span class="ModuleHeaderModuleNotes                     ">ModuleHeaderModuleNotes                     </span><br />
<span class="ModuleHeaderModuleTasks                     ">ModuleHeaderModuleTasks                     </span><br />
<span class="ModuleHeaderModuleCalendar                  ">ModuleHeaderModuleCalendar                  </span><br />
<span class="ModuleHeaderModuleDocuments                 ">ModuleHeaderModuleDocuments                 </span><br />
<span class="ModuleHeaderModuleImages                    ">ModuleHeaderModuleImages                    </span><br />
</div>

<div>Collaboration<br />
<span class="ModuleHeaderModuleChatDashboard             ">ModuleHeaderModuleChatDashboard             </span><br />
<span class="ModuleHeaderModuleChatChannels              ">ModuleHeaderModuleChatChannels              </span><br />
<span class="ModuleHeaderModuleChatMessages              ">ModuleHeaderModuleChatMessages              </span><br />
<span class="ModuleHeaderModuleForums                    ">ModuleHeaderModuleForums                    </span><br />
<span class="ModuleHeaderModuleForumTopics               ">ModuleHeaderModuleForumTopics               </span><br />
<span class="ModuleHeaderModulePosts                     ">ModuleHeaderModulePosts                     </span><br />
<span class="ModuleHeaderModuleThreads                   ">ModuleHeaderModuleThreads                   </span><br />
</div>

<div>Service and Support<br />
<span class="ModuleHeaderModuleBugs                      ">ModuleHeaderModuleBugs                      </span><br />
<span class="ModuleHeaderModuleReleases                  ">ModuleHeaderModuleReleases                  </span><br />
<span class="ModuleHeaderModuleCases                     ">ModuleHeaderModuleCases                     </span><br />
<span class="ModuleHeaderModuleContracts                 ">ModuleHeaderModuleContracts                 </span><br />
<span class="ModuleHeaderModuleContractTypes             ">ModuleHeaderModuleContractTypes             </span><br />
<span class="ModuleHeaderModuleKBDocuments               ">ModuleHeaderModuleKBDocuments               </span><br />
<span class="ModuleHeaderModuleKBTags                    ">ModuleHeaderModuleKBTags                    </span><br />
<span class="ModuleHeaderModuleProject                   ">ModuleHeaderModuleProject                   </span><br />
<span class="ModuleHeaderModuleProjectTask               ">ModuleHeaderModuleProjectTask               </span><br />
<span class="ModuleHeaderModuleInboundEmail              ">ModuleHeaderModuleInboundEmail              </span><br />
</div>

<div>Integration and Management<br />
Communications<br />
<span class="ModuleHeaderModuleAsterisk                  ">ModuleHeaderModuleAsterisk                  </span><br />
<span class="ModuleHeaderModuleAvaya                     ">ModuleHeaderModuleAvaya                     </span><br />
<span class="ModuleHeaderModuleExchange                  ">ModuleHeaderModuleExchange                  </span><br />
<span class="ModuleHeaderModuleTwilio                    ">ModuleHeaderModuleTwilio                    </span><br />
<span class="ModuleHeaderModuleTwitterMessages           ">ModuleHeaderModuleTwitterMessages           </span><br />
<span class="ModuleHeaderModuleTwitterTracks             ">ModuleHeaderModuleTwitterTracks             </span><br />
<span class="ModuleHeaderModuleFacebook                  ">ModuleHeaderModuleFacebook                  </span><br />
</div>

<div>Financial<br />
<span class="ModuleHeaderModulePayPal                    ">ModuleHeaderModulePayPal                    </span><br />
<span class="ModuleHeaderModulePayTrace                  ">ModuleHeaderModulePayTrace                  </span><br />
<span class="ModuleHeaderModuleQuickBooks                ">ModuleHeaderModuleQuickBooks                </span><br />
</div>

<div>Marketing Automation<br />
<span class="ModuleHeaderModuleHubSpot                   ">ModuleHeaderModuleHubSpot                   </span><br />
<span class="ModuleHeaderModuleMarketo                   ">ModuleHeaderModuleMarketo                   </span><br />
<span class="ModuleHeaderModuleConstantContact           ">ModuleHeaderModuleConstantContact           </span><br />
<span class="ModuleHeaderModuleiContact                  ">ModuleHeaderModuleiContact                  </span><br />
<span class="ModuleHeaderModuleGetResponse               ">ModuleHeaderModuleGetResponse               </span><br />
<span class="ModuleHeaderModuleMailChimp                 ">ModuleHeaderModuleMailChimp                 </span><br />
</div>

<div>Reporting<br />
<span class="ModuleHeaderModuleReports                   ">ModuleHeaderModuleReports                   </span><br />
<span class="ModuleHeaderModuleReportDesigner            ">ModuleHeaderModuleReportDesigner            </span><br />
<span class="ModuleHeaderModuleReportRules               ">ModuleHeaderModuleReportRules               </span><br />
<span class="ModuleHeaderModuleCharts                    ">ModuleHeaderModuleCharts                    </span><br />
</div>

<div>Admin Users<br />
<span class="ModuleHeaderModuleUsers                     ">ModuleHeaderModuleUsers                     </span><br />
<span class="ModuleHeaderModuleEmployees                 ">ModuleHeaderModuleEmployees                 </span><br />
<span class="ModuleHeaderModuleUserLogins                ">ModuleHeaderModuleUserLogins                </span><br />
<span class="ModuleHeaderModuleUserSignatures            ">ModuleHeaderModuleUserSignatures            </span><br />
<span class="ModuleHeaderModuleTeams                     ">ModuleHeaderModuleTeams                     </span><br />
<span class="ModuleHeaderModuleTeamNotices               ">ModuleHeaderModuleTeamNotices               </span><br />
<span class="ModuleHeaderModuleACLRoles                  ">ModuleHeaderModuleACLRoles                  </span><br />
</div>

<div>Admin Layout<br />
<span class="ModuleHeaderModuleDetailViewsRelationships  ">ModuleHeaderModuleDetailViewsRelationships  </span><br />
<span class="ModuleHeaderModuleDynamicLayout             ">ModuleHeaderModuleDynamicLayout             </span><br />
<span class="ModuleHeaderModuleEditCustomFields          ">ModuleHeaderModuleEditCustomFields          </span><br />
<span class="ModuleHeaderModuleFieldValidators           ">ModuleHeaderModuleFieldValidators           </span><br />
<span class="ModuleHeaderModuleDynamicButtons            ">ModuleHeaderModuleDynamicButtons            </span><br />
<span class="ModuleHeaderModuleShortcuts                 ">ModuleHeaderModuleShortcuts                 </span><br />
<span class="ModuleHeaderModuleLanguages                 ">ModuleHeaderModuleLanguages                 </span><br />
<span class="ModuleHeaderModuleBusinessRules             ">ModuleHeaderModuleBusinessRules             </span><br />
<span class="ModuleHeaderModuleRulesWizard               ">ModuleHeaderModuleRulesWizard               </span><br />
<span class="ModuleHeaderModuleDropdown                  ">ModuleHeaderModuleDropdown                  </span><br />
<span class="ModuleHeaderModuleTerminology               ">ModuleHeaderModuleTerminology               </span><br />
</div>

<div>Admin<br />
<span class="ModuleHeaderModuleAdministration            ">ModuleHeaderModuleAdministration            </span><br />
<span class="ModuleHeaderModuleAuditEvents               ">ModuleHeaderModuleAuditEvents               </span><br />
<span class="ModuleHeaderModuleConfig                    ">ModuleHeaderModuleConfig                    </span><br />
<span class="ModuleHeaderModuleCurrencies                ">ModuleHeaderModuleCurrencies                </span><br />
<span class="ModuleHeaderModuleImport                    ">ModuleHeaderModuleImport                    </span><br />
<span class="ModuleHeaderModuleModuleBuilder             ">ModuleHeaderModuleModuleBuilder             </span><br />
<span class="ModuleHeaderModuleModules                   ">ModuleHeaderModuleModules                   </span><br />
<span class="ModuleHeaderModuleNumberSequences           ">ModuleHeaderModuleNumberSequences           </span><br />
<span class="ModuleHeaderModuleRegions                   ">ModuleHeaderModuleRegions                   </span><br />
<span class="ModuleHeaderModuleSchedulers                ">ModuleHeaderModuleSchedulers                </span><br />
<span class="ModuleHeaderModuleSimpleStorage             ">ModuleHeaderModuleSimpleStorage             </span><br />
<span class="ModuleHeaderModuleSystemLog                 ">ModuleHeaderModuleSystemLog                 </span><br />
<span class="ModuleHeaderModuleSystemSyncLog             ">ModuleHeaderModuleSystemSyncLog             </span><br />
<span class="ModuleHeaderModuleUndelete                  ">ModuleHeaderModuleUndelete                  </span><br />
<span class="ModuleHeaderModuleZipCodes                  ">ModuleHeaderModuleZipCodes                  </span><br />
<span class="ModuleHeaderModuleCurrencyLayer             ">ModuleHeaderModuleCurrencyLayer             </span><br />
</div>

<div>Admin Workflow<br />
<span class="ModuleHeaderModuleWorkflows                 ">ModuleHeaderModuleWorkflows                 </span><br />
<span class="ModuleHeaderModuleWorkflowActionShells      ">ModuleHeaderModuleWorkflowActionShells      </span><br />
<span class="ModuleHeaderModuleWorkflowAlertShells       ">ModuleHeaderModuleWorkflowAlertShells       </span><br />
<span class="ModuleHeaderModuleWorkflowTriggerShells     ">ModuleHeaderModuleWorkflowTriggerShells     </span><br />
<span class="ModuleHeaderModuleWorkflowAlertTemplates    ">ModuleHeaderModuleWorkflowAlertTemplates    </span><br />
<span class="ModuleHeaderModuleWorkflowEventLog          ">ModuleHeaderModuleWorkflowEventLog          </span><br />
</div>

<div>Admin Azure<br />
<span class="ModuleHeaderModuleAzure                     ">ModuleHeaderModuleAzure                     </span><br />
<span class="ModuleHeaderModuleCloudServices             ">ModuleHeaderModuleCloudServices             </span><br />
<span class="ModuleHeaderModuleVirtualMachines           ">ModuleHeaderModuleVirtualMachines           </span><br />
<span class="ModuleHeaderModuleSqlDatabases              ">ModuleHeaderModuleSqlDatabases              </span><br />
<span class="ModuleHeaderModuleDnsNames                  ">ModuleHeaderModuleDnsNames                  </span><br />
<span class="ModuleHeaderModuleSqlServers                ">ModuleHeaderModuleSqlServers                </span><br />
<span class="ModuleHeaderModuleResourceGroups            ">ModuleHeaderModuleResourceGroups            </span><br />
<span class="ModuleHeaderModuleStorageAccounts           ">ModuleHeaderModuleStorageAccounts           </span><br />
<span class="ModuleHeaderModuleStorageFiles              ">ModuleHeaderModuleStorageFiles              </span><br />

<span class="ModuleHeaderModuleAzureSystemLog            ">ModuleHeaderModuleAzureSystemLog            </span><br />
<span class="ModuleHeaderModuleAzureAppPrices            ">ModuleHeaderModuleAzureAppPrices            </span><br />
<span class="ModuleHeaderModuleAzureOrders               ">ModuleHeaderModuleAzureOrders               </span><br />
<span class="ModuleHeaderModuleAzureRegions              ">ModuleHeaderModuleAzureRegions              </span><br />
<span class="ModuleHeaderModuleAzureServiceLevels        ">ModuleHeaderModuleAzureServiceLevels        </span><br />
<span class="ModuleHeaderModuleAzureSqlPrices            ">ModuleHeaderModuleAzureSqlPrices            </span><br />
<span class="ModuleHeaderModuleAzureSqlServers           ">ModuleHeaderModuleAzureSqlServers           </span><br />
<span class="ModuleHeaderModuleAzureStorage              ">ModuleHeaderModuleAzureStorage              </span><br />
<span class="ModuleHeaderModuleAzureVmPrices             ">ModuleHeaderModuleAzureVmPrices             </span><br />
<span class="ModuleHeaderModuleAzurePriceNames           ">ModuleHeaderModuleAzurePriceNames           </span><br />


</div>
</asp:Content>

