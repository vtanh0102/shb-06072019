<%@ Page language="c#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Collections.Generic" %>
<script runat="server">
/**
 * Copyright (C) 2010-2013 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<!DOCTYPE HTML>
<html>
<head runat="server">
	<title>ACT Blob</title>
</head>
<body>
<%
XmlDocument xml = new XmlDocument();
string sTempPath = Path.Combine(Path.GetTempPath(), "Splendid");
Response.Write(sTempPath + "<br>");
string sDBPathName = "Contacts.dbf";
DateTime dtNow = new DateTime(2010, 5, 10, 13, 46, 30);
long lNow = 0;
lNow += (long) (dtNow.Year      - 1869) * 365 * 24 * 60 * 60 * 256;
lNow += (long) (dtNow.DayOfYear +  200)       * 24 * 60 * 60 * 256;
lNow += (long) (dtNow.Hour            )            * 60 * 60 * 256;
lNow += (long) (dtNow.Minute          )                 * 60 * 256;
lNow += (long) (dtNow.Second          )                      * 256;

Response.Write( Convert.ToString((long)(new DateTime(2001,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2002,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2003,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2004,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2005,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2006,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2007,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2008,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2009,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2010,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
Response.Write( Convert.ToString((long)(new DateTime(2011,  1, 1)).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 2) + "<br>");
/*
	    1     2       C       E           A     a     0   0   (4 bytes)
	    1  0010    110011    10    10    10  0110  00000000
	    1  0010  00110011  0011  1001  0001  0100  00010001   Paul 1980/01/01 00:00:01 
	    1  0010  00110011  0011  1001  0001  0100  00010100   Paul 1980/01/01 00:00:05 
	    1  0010  00110011  0011  1001  0001  0100  00011000   Paul 1980/01/01 00:00:08 
	    1  0010  00110011  0011  1001  0001  0100  00011001   Paul 1980/01/01 00:00:10 
	    1  0010  00110011  0011  1001  0001  0100  00101001   Paul 1980/01/01 00:00:25 
	    1  0010  00110011  0011  1001  0001  0100  00101110   Paul 1980/01/01 00:00:30 
	    1  0010  00110011  0011  1001  0001  0100  00110010   Paul 1980/01/01 00:00:34 
	    1  0010  00110011  0010  1110  0011  0001  00111110   Paul 1980/01/01 00:00:45 
	    1  0010  00110011  0011  1001  0001  0100  00111110   Paul 1980/01/01 00:00:47 
	    1  0010  00110011  0010  1110  0011  0010  00000000   Paul 1980/01/01 00:00:48 
	    1  0010  00110011  0010  1110  0011  0010  00000001   Paul 1980/01/01 00:00:49 
	    1  0010  00110011  0010  1110  0011  0010  00000011   Paul 1980/01/01 00:00:50 
	    1  0010  00110011  0010  1110  0011  0010  00001000   Paul 1980/01/01 00:00:55 
	 4444  4444  33333333  2222  2222  1111  1111  00000000

	    1  0010    110011    10    10    10  0111  00110110
	    1  0010  00110011  0010  1110  0011  0110  00000111   Paul 1980/01/01 00:05:10 
	    1  0010    110011    10    10    11  0101  01000110
	    1  0010  00110011  0010  1111  0010  1110  00010111   Paul 1980/01/01 01:05:10 
	    1  0010    110011    10    11    00  0011  01010110
	    1  0010  00110011  0011  0000  0010  0110  00100111   Paul 1980/01/01 02:05:10 
	    1  0010    110011    10    11    01  0001  01100110
	    1  0010  00110011  0011  0001  0001  1110  00110110   Paul 1980/01/01 03:05:10 
	    1  0010    110011    10    11    01  1111  01110110
	    1  0010  00110011  0011  0010  0001  0111  00000111   Paul 1980/01/01 04:05:10 
	    1  0010    110011    10    11    11  1011  10010110
	    1  0010  00110011  0011  0100  0000  0111  00100111   Paul 1980/01/01 06:05:10 
	    1  0010    110011    11    00    00  1001  10100110
	    1  0010  00110011  0011  0100  0011  1111  00110111   Paul 1980/01/01 07:05:10 
	    1  0010    110011    11    00    01  0111  10110110
	    1  0010  00110011  0011  0101  0011  1000  00000111   Paul 1980/01/01 08:05:10 

	    1  0010    111101    11    10    00  0100  10000000
	    1  0010  00111101  0011  1100  0010  1011  00010001   Paul 1980/02/01 00:00:02 
	    1  0011    000111    01    11    00  0000  00000000
	    1  0011  00000111  0010  0000  0001  1001  00010001   Paul 1980/03/01 00:00:02 
	    1  0011    010001    10    10    01  1110  10000000
	    1  0101  00001010  0000  0000  0010  1001  00000010   Paul 1980/04/01 00:00:02 
	    1  0011    011011    10    00    10  1011  10000000
	    1  0101  00010011  0011  1001  0001  1101  00000001   Paul 1980/05/01 00:00:02 
	 4444  4444  33333333  2222  2222  1111  1111  00000000

	    1  0100    101100    01    00    10  1011  00000000
	    1  0100  00101100  0001  0111  0000  0101  00010000   Paul 1981/01/01 00:00:02 
	    1  0110    100100    10    01    01  1110  10000000
	    1  0110  00100100  0010  1010  0001  0011  00010000   Paul 1982/01/01 00:00:02 
	    1  1000    011100    11    10    01  0010  00000000
	    1  1000  00011100  0011  1101  0010  0001  00010011   Paul 1983/01/01 00:00:02 
	    1  1010    010101    00    11    00  0101  10000000
	    1  1010  00010101  0001  0000  0010  1111  00010010   Paul 1984/01/01 00:00:02 

	   10  0101    100111    10    10    01  1101  10000000
	   10  0101  00100111  0010  1110  0000  1111  00010001   Paul 1990/01/01 00:00:02 
	   10  0101    110001    11    01    11  1100  00000000
	   10  0101  00110001  0011  1100  0000  1001  00010001   Paul 1990/02/01 00:00:02 
	   10  0101    111011    00    01    10  0110  00000000
	   10  0101  00111011  0000  1010  0011  0001  00010001   Paul 1990/03/01 00:00:02 
	   10  0110    000101    01    01    00  0100  10000000
	   10  0110  00000101  0001  0111  0011  0011  00000001   Paul 1990/04/01 00:00:02 
	   10  0110    001111    00    11    01  0001  10000000
	   10  0110  00001111  0001  0000  0010  0111  00000001   Paul 1990/05/01 00:00:02 
	   10  0110    011001    01    10    11  0000  00000000
	   10  0110  00011001  0001  1110  0010  0001  00000001   Paul 1990/06/01 00:00:02 
	   10  0110    100011    01    00    11  1101  00000000
	   10  0110  00100011  0001  0111  0001  0101  00000001   Paul 1990/07/01 00:00:02 
	   10  0110    101101    10    00    01  1011  10000000
	   10  0110  00101101  0010  0101  0000  1111  00000001   Paul 1990/08/01 00:00:02 
	   10  0110    110111    10    11    11  1010  00000000
	   10  0110  00110111  0011  0011  0000  1001  00000001   Paul 1990/09/01 00:00:02 
	   10  0111    000001    10    10    00  0111  00000000
	   10  0111  00000001  0010  1011  0011  1101  00000001   Paul 1990/10/01 00:00:02 
	   10  0111    001011    11    01    10  0101  10000000
	   10  0111  00001011  0011  1001  0011  0111  00000001   Paul 1990/11/01 00:00:02 
	   10  0111    010101    10    11    11  0010  10000000
	   10  0111  00010101  0011  0011  0010  0011  00010001   Paul 1990/12/01 00:00:02 

	   11  1000    011011    01    01    00  0011  10000000
	   11  1000  00011011  0001  1000  0010  0111  00010010   Paul 2000/01/01 00:00:02 
	   11  1000    100101    10    00    10  0010  00000000
	   11  1000  00100101  0010  0110  0010  0001  00010010   Paul 2000/02/01 00:00:02 
	   11  1000    101111    00    01    01  1101  10000000
	   11  1000  00101111  0000  1010  0000  1111  00010001   Paul 2000/03/01 00:00:02 
	   11  1000    111001    01    00    11  1100  00000000
	   11  1000  00111001  0001  0111  0001  0001  00000001   Paul 2000/04/01 00:00:02 
	   11  1001    000011    00    11    00  1001  00000000
	   11  1001  00000011  0001  0000  0000  0101  00000001   Paul 2000/05/01 00:00:02 
	   11  1001    001101    01    10    10  0111  10000000
	   11  1001  00001101  0001  1101  0011  1111  00000001   Paul 2000/06/01 00:00:02 
	   11  1001    010111    01    00    11  0100  10000000
	   11  1001  00010111  0001  0110  0011  0011  00000001   Paul 2000/07/01 00:00:02 
	   11  1001    100001    10    00    01  0011  00000000
	   11  1001  00100001  0010  0100  0010  1101  00000001   Paul 2000/08/01 00:00:02 
	   11  1001    101011    10    11    11  0001  10000000
	   11  1001  00101011  0011  0010  0010  0111  00000010   Paul 2000/09/01 00:00:02 
	   11  1001    110101    10    01    11  1110  10000000
	   11  1001  00110101  0010  1011  0001  1011  00000001   Paul 2000/10/01 00:00:02 
	   11  1001    111111    11    01    01  1101  00000000
	   11  1001  00111111  0011  1001  0001  0101  00000001   Paul 2000/11/01 00:00:02 
	    3     a       2       6           e     a     0   0   (4 bytes)
	   11  1010    001001    10    11    10  1010  00000000
	   11  1010  00001001  0011  0011  0000  0001  00010000   Paul 2000/12/01 00:00:02 
	   44  4444  33333333  2222  2222  1111  1111  00000000

	   11  1010    010011    11    11    00  1000  10000000
	   11  1010  00010100  0000  0000  0011  1011  00010001   Paul 2001/01/01 00:00:02 
	   11  1100    001100    00    11    11  1100  00000000
	   11  1100  00001100  0001  0100  0000  1001  00011000   Paul 2002/01/01 00:00:02 
	   11  1110    000100    10    00    10  1111  10000000
	   11  1110  00000100  0010  0111  0001  0111  00011110   Paul 2003/01/01 00:00:02 
	   11  1111    111100    11    01    10  0011  00000000
	   11  1111  00111100  0011  1010  0010  0101  00100111   Paul 2004/01/01 00:00:02 

	  100  0001    110101    01    11    10  1000  00000000
	10000  0001  00110101  0010  0010  0011  1001  00101110   Paul 2005/01/01 00:00:02 
	  100  0011    101101    11    00    01  1011  10000000
	10000  0011  00101101  0011  0110  0000  0111  00110100   Paul 2006/01/01 00:00:02 
	  100  0101    100110    00    01    00  1111  00000000
	10000  0101  00100110  0000  1001  0001  0101  00111101   Paul 2007/01/01 00:00:02 
	  100  0111    011110    01    10    00  0010  10000000
	10000  0111  00011110  0001  1100  0010  0100  00000101   Paul 2008/01/01 00:00:02 
	  100  1001    010111    00    00    00  0111  10000000
	10000  1001  00010111  0000  0100  0011  1000  00001100   Paul 2009/01/01 00:00:02 
	  100  1011    001111    01    00    11  1011  00000000
	10000  1011  00001111  0001  1000  0000  0110  00010010   Paul 2010/01/01 00:00:02 
	  100  1101    000111    10    01    10  1110  10000000
	10000  1101  00000111  0010  1011  0001  0100  00011000   Paul 2011/01/01 00:00:02 
	54444  4444  33333333  2222  2222  1111  1111  00000000
*/
using ( OdbcConnection con = new OdbcConnection() )
{
	con.ConnectionString = @"Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF;SourceDB=" + sTempPath + ";Exclusive=No;Collate=Machine;NULL=NO;DELETED=NO;BACKGROUNDFETCH=NO;";
	con.Open();
	using ( OdbcCommand cmd = con.CreateCommand() )
	{
		cmd.CommandText = @"select * from " + Path.Combine(sTempPath, sDBPathName);
		using ( DataTable dt = new DataTable() )
		{
			try
			{
				dt.Load(cmd.ExecuteReader());
				Response.Write("<table border=1 cellpadding=2 cellspacing=0 style='font-family: courier new; font-size: 8pt;' nowrap>" + ControlChars.CrLf);
				Response.Write("	<tr><th>UNIQUE_ID</th><th>CTIME</th><th>CTIME binary</th><th>ETIME</th><th>ETIME binary</th><th>MTIME</th><th>NAME</th><th>COMPANY</th></tr>" + ControlChars.CrLf);
				DataView vw = new DataView(dt);
				vw.Sort = "COMPANY, NAME";
				foreach ( DataRowView row in vw )
				{
					string sUNIQUE_ID = Sql.ToString(row["UNIQUE_ID"]);
					string sCOMPANY   = Sql.ToString(row["COMPANY"  ]);
					string sNAME      = Sql.ToString(row["NAME"     ]);
					string sCTIME     = Sql.ToString(row["CTIME"    ]);
					string sETIME     = Sql.ToString(row["ETIME"    ]);
					string sMTIME     = Sql.ToString(row["MTIME"    ]);
					if ( sCOMPANY == "" )
					{
						UInt64 uCTIME = SplendidCRM.ACTImport.ACTDateValue(sCTIME);
						UInt64 uETIME = SplendidCRM.ACTImport.ACTDateValue(sETIME);
						UInt64 uMTIME = SplendidCRM.ACTImport.ACTDateValue(sMTIME);
						DateTime dtCTIME = new DateTime(Convert.ToInt64(uCTIME));
						byte[] arrETIME = SplendidCRM.ACTImport.ACTDateArray(sETIME);
						
						arrETIME[4] = (byte) (arrETIME[5] << 6 | arrETIME[4] & 0x3f);
						arrETIME[3] = (byte) (arrETIME[3] << 2 | (arrETIME[2] >> 4 & 0x03));
						arrETIME[2] = (byte) (arrETIME[2] << 4);
						arrETIME[1] = 0;
						arrETIME[5] = 0;
						UInt64 uDate = (Convert.ToUInt64(arrETIME[5]) << 32) + (Convert.ToUInt64(arrETIME[4]) << 24) + (Convert.ToUInt64(arrETIME[3]) << 16) + (Convert.ToUInt64(arrETIME[2]) << 8) + (Convert.ToUInt64(arrETIME[1]));
						//uCTIME >>= 5;
						//uETIME >>= 5;
						
						Response.Write("<tr><td nowrap>");
						//Response.Write(Server.HtmlEncode(sUNIQUE_ID));
						Response.Write("</td><td nowrap>");
						//Response.Write(Server.HtmlEncode(sCTIME));
						Response.Write("</td><td nowrap>");
						//Response.Write(uCTIME.ToString() + "<br>");
						//Response.Write("0x" + uCTIME.ToString("x8") + "<br>");
						//Response.Write(Convert.ToString((long)uCTIME, 2));
						Response.Write("</td><td nowrap>");
						//Response.Write(Server.HtmlEncode(sETIME));
						Response.Write("</td><td nowrap>");
						//Response.Write(uETIME.ToString() + "<br>");
						//Response.Write("0x" + uETIME.ToString("x8") + "<br>");
						Response.Write(Convert.ToString((long)uETIME, 2));
						Response.Write("</td><td nowrap>");
						//Response.Write(       ((uETIME - uCTIME)).ToString() + "<br>");
						//Response.Write("0x" + ((uETIME - uCTIME)).ToString("x8") + "<br>");
						//Response.Write(sMTIME);
						Response.Write(SplendidCRM.ACTImport.ACTTimestamp(sETIME).ToString("yyyy/MM/dd HH:mm:ss"));
						Response.Write("</td><td nowrap>");
						Response.Write(Server.HtmlEncode(sNAME));
						Response.Write(Server.HtmlEncode(sCOMPANY));
						Response.Write("</td><td nowrap>");
						Response.Write("</td></tr>");
					}
				}
				Response.Write("</table>" + ControlChars.CrLf);
			}
			catch(Exception ex)
			{
				Response.Write(con.ConnectionString + "<br>" + cmd.CommandText + "<br>" + ex.Message);
			}
			//xml = SplendidCRM.SplendidImport.ConvertTableToXml(dt, "emails", false, true, null);
		}
	}
	con.Close();
}
/*
string sBlobPathName  = Path.Combine(sTempPath, "ACT.blb");
// 02/02/2010 Paul.  File name follow DOS file naming conventions, otherwise ODBC will reject file. 
string sDBKeyName  = String.Empty;
string[] arrDBTypes = new string[] {"edb", "hdb", "adb"};
foreach ( string sDBType in arrDBTypes )
{
	switch ( sDBType )
	{
		case "edb":  sDBKeyName = "logon"    ;  sDBPathName = Path.Combine(sTempPath, "ACTedb.dbf");  break;
		case "hdb":  sDBKeyName = "regarding";  sDBPathName = Path.Combine(sTempPath, "ACThdb.dbf");  break;
		case "adb":  sDBKeyName = "details"  ;  sDBPathName = Path.Combine(sTempPath, "ACTadb.dbf");  break;
	}
	if ( File.Exists(sDBPathName) && File.Exists(sBlobPathName) )
	{
		using ( FileStream stmBlob = File.OpenRead(sBlobPathName) )
		{
			UInt32 uLookupTableSize = 0x0000;
			UInt32 uLookupTablePtr  = 0x0000;
			SplendidCRM.ACTImport.ACTBlobLookupTable(stmBlob, ref uLookupTableSize, ref uLookupTablePtr);
			using ( OdbcConnection con = new OdbcConnection() )
			{
				con.ConnectionString = @"Driver={Microsoft dBase Driver (*.dbf)};SourceType=DBF;SourceDB=" + sTempPath + ";Exclusive=No;Collate=Machine;NULL=NO;DELETED=NO;BACKGROUNDFETCH=NO;";
				con.Open();
				using ( OdbcCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = @"select * from " + sDBPathName;
					using ( DataTable dt = new DataTable() )
					{
						try
						{
							dt.Load(cmd.ExecuteReader());
							Response.Write("<table border=1 cellpadding=2 cellspacing=0>" + ControlChars.CrLf);
							Response.Write("	<tr><th>Value</th><th>Ptr</th><th>Position</th><th>Size</th><th>Lookup</th></tr>" + ControlChars.CrLf);
							foreach ( DataRow row in dt.Rows )
							{
								string suLookupKey    = Sql.ToString(row[sDBKeyName]);
								if ( !Sql.IsEmptyString(suLookupKey.Trim()) )
								{
									UInt32 uLookupKey     = SplendidCRM.ACTImport.ACTBlobLookupOffset(suLookupKey);
									UInt32 uValueSize     = 0x0000;
									UInt32 uValuePosition = 0x0000;
									SplendidCRM.ACTImport.ACTBlobValuePosition(stmBlob, uLookupTableSize, uLookupTablePtr, uLookupKey, ref uValueSize, ref uValuePosition);
									string sValue    = SplendidCRM.ACTImport.ACTBlobReadString(stmBlob, uValueSize, uValuePosition);
									//string sValue    = SplendidCRM.ACTImport.ACTBlobLookupString(stmBlob, uLookupTableSize, uLookupTablePtr, sLOGON);
									
									Response.Write("<tr><td>");
									Response.Write(suLookupKey);
									Response.Write("</td><td>0x");
									Response.Write(uLookupKey.ToString("x8"));
									Response.Write("</td><td>0x");
									Response.Write(uValuePosition.ToString("x8"));
									Response.Write("</td><td>");
									Response.Write(uValueSize.ToString());
									Response.Write("</td><td>");
									Response.Write(sValue);
									Response.Write("</td></tr>");
								}
							}
							Response.Write("</table>" + ControlChars.CrLf);
						}
						catch(Exception ex)
						{
							Response.Write(con.ConnectionString + "<br>" + cmd.CommandText + "<br>" + ex.Message);
						}
						//xml = SplendidCRM.SplendidImport.ConvertTableToXml(dt, "emails", false, true, null);
					}
				}
				con.Close();
			}
		}
	}
}
*/
/*
SortedList<UInt32, UInt32> ptrs = new SortedList<UInt32, UInt32>();
SortedList<UInt32, UInt32> start = new SortedList<UInt32, UInt32>();
if ( File.Exists(sBlobPathName) )
{
	using ( FileStream stm = File.OpenRead(sBlobPathName) )
	{
		stm.Seek(0x1000, SeekOrigin.Begin);
		for ( int i = 0; i < 0xac00/8; i++ )
		{
			long lPosition = stm.Position;
			byte[] bySize = new byte[4];
			byte[] byPtr  = new byte[4];
			stm.Read(bySize, 0, 4);
			stm.Read(byPtr , 0, 4);
			Array.Reverse(bySize);
			Array.Reverse(byPtr );
			UInt32 uSize = BitConverter.ToUInt32(bySize, 0);
			UInt32 uPtr  = BitConverter.ToUInt32(byPtr , 0);
			if ( !ptrs.ContainsKey(uPtr) )
				ptrs.Add(uPtr, uSize);
			if ( !start.ContainsKey(uPtr) )
				start.Add(uPtr, Convert.ToUInt32(lPosition));
		}
	}
}
else
{
	Response.Write("File not found: " + sBlobPathName);
}
Response.Write("<table border=1 cellpadding=2 cellspacing=0>" + ControlChars.CrLf);
Response.Write("	<tr><th>Ptr</th><th>Size (x)</th><th>Size</th><th>Start</th></tr>" + ControlChars.CrLf);
foreach ( UInt32 uPtr in ptrs.Keys )
{
	UInt32 uSize = ptrs[uPtr];
	UInt32 uPosition = start[uPtr];
	Response.Write("<tr><td>0x" + uPtr.ToString("x8") + "</td><td>0x" + uSize.ToString("x4") + "</td><td>" + uSize.ToString() + "</td><td>0x" + uPosition.ToString("x8") + "</td></tr>" + ControlChars.CrLf);
}
Response.Write("</table>" + ControlChars.CrLf);
*/
%>
</body>
</html>
