/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// ProjectTaskActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:30 PM
	/// </summary>
	public class ProjectTaskActivity: SplendidActivity
	{
		public ProjectTaskActivity()
		{
			this.Name = "ProjectTaskActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.NAMEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.STATUSProperty))); }
			set { base.SetValue(ProjectTaskActivity.STATUSProperty, value); }
		}

		public static DependencyProperty DATE_TIME_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME_DUE", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME_DUE
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_TIME_DUEProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_TIME_DUEProperty, value); }
		}

		public static DependencyProperty DATE_TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_TIME_START", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_TIME_START
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_TIME_STARTProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_TIME_STARTProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.PARENT_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty PRIORITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIORITY", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIORITY
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.PRIORITYProperty))); }
			set { base.SetValue(ProjectTaskActivity.PRIORITYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(ProjectTaskActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty ORDER_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ORDER_NUMBER", typeof(Int32), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 ORDER_NUMBER
		{
			get { return ((Int32)(base.GetValue(ProjectTaskActivity.ORDER_NUMBERProperty))); }
			set { base.SetValue(ProjectTaskActivity.ORDER_NUMBERProperty, value); }
		}

		public static DependencyProperty TASK_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TASK_NUMBER", typeof(Int32), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 TASK_NUMBER
		{
			get { return ((Int32)(base.GetValue(ProjectTaskActivity.TASK_NUMBERProperty))); }
			set { base.SetValue(ProjectTaskActivity.TASK_NUMBERProperty, value); }
		}

		public static DependencyProperty DEPENDS_ON_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEPENDS_ON_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid DEPENDS_ON_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.DEPENDS_ON_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.DEPENDS_ON_IDProperty, value); }
		}

		public static DependencyProperty MILESTONE_FLAGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MILESTONE_FLAG", typeof(bool), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool MILESTONE_FLAG
		{
			get { return ((bool)(base.GetValue(ProjectTaskActivity.MILESTONE_FLAGProperty))); }
			set { base.SetValue(ProjectTaskActivity.MILESTONE_FLAGProperty, value); }
		}

		public static DependencyProperty ESTIMATED_EFFORTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ESTIMATED_EFFORT", typeof(float), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float ESTIMATED_EFFORT
		{
			get { return ((float)(base.GetValue(ProjectTaskActivity.ESTIMATED_EFFORTProperty))); }
			set { base.SetValue(ProjectTaskActivity.ESTIMATED_EFFORTProperty, value); }
		}

		public static DependencyProperty ACTUAL_EFFORTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ACTUAL_EFFORT", typeof(float), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public float ACTUAL_EFFORT
		{
			get { return ((float)(base.GetValue(ProjectTaskActivity.ACTUAL_EFFORTProperty))); }
			set { base.SetValue(ProjectTaskActivity.ACTUAL_EFFORTProperty, value); }
		}

		public static DependencyProperty UTILIZATIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("UTILIZATION", typeof(Int32), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 UTILIZATION
		{
			get { return ((Int32)(base.GetValue(ProjectTaskActivity.UTILIZATIONProperty))); }
			set { base.SetValue(ProjectTaskActivity.UTILIZATIONProperty, value); }
		}

		public static DependencyProperty PERCENT_COMPLETEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PERCENT_COMPLETE", typeof(Int32), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 PERCENT_COMPLETE
		{
			get { return ((Int32)(base.GetValue(ProjectTaskActivity.PERCENT_COMPLETEProperty))); }
			set { base.SetValue(ProjectTaskActivity.PERCENT_COMPLETEProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.TEAM_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(ProjectTaskActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.CREATED_BYProperty))); }
			set { base.SetValue(ProjectTaskActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_DUE", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_DUE
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_DUEProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_DUEProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty DATE_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_START", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_START
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.DATE_STARTProperty))); }
			set { base.SetValue(ProjectTaskActivity.DATE_STARTProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(ProjectTaskActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty TIME_DUEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_DUE", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_DUE
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.TIME_DUEProperty))); }
			set { base.SetValue(ProjectTaskActivity.TIME_DUEProperty, value); }
		}

		public static DependencyProperty TIME_STARTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TIME_START", typeof(DateTime), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime TIME_START
		{
			get { return ((DateTime)(base.GetValue(ProjectTaskActivity.TIME_STARTProperty))); }
			set { base.SetValue(ProjectTaskActivity.TIME_STARTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty DEPENDS_ON_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DEPENDS_ON_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DEPENDS_ON_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.DEPENDS_ON_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.DEPENDS_ON_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.PENDING_PROCESS_IDProperty, value); }
		}

		public static DependencyProperty PROJECT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROJECT_ASSIGNED_SET_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PROJECT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.PROJECT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.PROJECT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PROJECT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROJECT_ASSIGNED_USER_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PROJECT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.PROJECT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.PROJECT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PROJECT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROJECT_ID", typeof(Guid), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PROJECT_ID
		{
			get { return ((Guid)(base.GetValue(ProjectTaskActivity.PROJECT_IDProperty))); }
			set { base.SetValue(ProjectTaskActivity.PROJECT_IDProperty, value); }
		}

		public static DependencyProperty PROJECT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PROJECT_NAME", typeof(string), typeof(ProjectTaskActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PROJECT_NAME
		{
			get { return ((string)(base.GetValue(ProjectTaskActivity.PROJECT_NAMEProperty))); }
			set { base.SetValue(ProjectTaskActivity.PROJECT_NAMEProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("ProjectTaskActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("ProjectTaskActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select PROJECT_TASK_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwPROJECT_TASK_AUDIT        PROJECT_TASK          " + ControlChars.CrLf
							                + " inner join vwPROJECT_TASK_AUDIT        PROJECT_TASK_AUDIT_OLD" + ControlChars.CrLf
							                + "         on PROJECT_TASK_AUDIT_OLD.ID = PROJECT_TASK.ID       " + ControlChars.CrLf
							                + "        and PROJECT_TASK_AUDIT_OLD.AUDIT_VERSION = (select max(vwPROJECT_TASK_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                                      from vwPROJECT_TASK_AUDIT                   " + ControlChars.CrLf
							                + "                                                     where vwPROJECT_TASK_AUDIT.ID            =  PROJECT_TASK.ID           " + ControlChars.CrLf
							                + "                                                       and vwPROJECT_TASK_AUDIT.AUDIT_VERSION <  PROJECT_TASK.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                       and vwPROJECT_TASK_AUDIT.AUDIT_TOKEN   <> PROJECT_TASK.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                                   )" + ControlChars.CrLf
							                + " where PROJECT_TASK.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwPROJECT_TASK_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwPROJECT_TASK_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *                  " + ControlChars.CrLf
							                + "  from vwPROJECT_TASK_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								if ( !bPast )
									DATE_TIME_DUE                  = Sql.ToDateTime(rdr["DATE_TIME_DUE"                 ]);
								if ( !bPast )
									DATE_TIME_START                = Sql.ToDateTime(rdr["DATE_TIME_START"               ]);
								if ( bPast )
									PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								PRIORITY                       = Sql.ToString  (rdr["PRIORITY"                      ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								ORDER_NUMBER                   = Sql.ToInteger (rdr["ORDER_NUMBER"                  ]);
								TASK_NUMBER                    = Sql.ToInteger (rdr["TASK_NUMBER"                   ]);
								DEPENDS_ON_ID                  = Sql.ToGuid    (rdr["DEPENDS_ON_ID"                 ]);
								MILESTONE_FLAG                 = Sql.ToBoolean (rdr["MILESTONE_FLAG"                ]);
								ESTIMATED_EFFORT               = Sql.ToFloat   (rdr["ESTIMATED_EFFORT"              ]);
								ACTUAL_EFFORT                  = Sql.ToFloat   (rdr["ACTUAL_EFFORT"                 ]);
								UTILIZATION                    = Sql.ToInteger (rdr["UTILIZATION"                   ]);
								PERCENT_COMPLETE               = Sql.ToInteger (rdr["PERCENT_COMPLETE"              ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_DUE                       = Sql.ToDateTime(rdr["DATE_DUE"                      ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								DATE_START                     = Sql.ToDateTime(rdr["DATE_START"                    ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								TIME_DUE                       = Sql.ToDateTime(rdr["TIME_DUE"                      ]);
								TIME_START                     = Sql.ToDateTime(rdr["TIME_START"                    ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									DEPENDS_ON_NAME                = Sql.ToString  (rdr["DEPENDS_ON_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
									PROJECT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["PROJECT_ASSIGNED_SET_ID"       ]);
									PROJECT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["PROJECT_ASSIGNED_USER_ID"      ]);
									PROJECT_ID                     = Sql.ToGuid    (rdr["PROJECT_ID"                    ]);
									PROJECT_NAME                   = Sql.ToString  (rdr["PROJECT_NAME"                  ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProjectTaskActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("PROJECT_TASK", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spPROJECT_TASKS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, STATUS
								, DATE_TIME_DUE
								, DATE_TIME_START
								, PARENT_ID
								, PRIORITY
								, DESCRIPTION
								, ORDER_NUMBER
								, TASK_NUMBER
								, DEPENDS_ON_ID
								, MILESTONE_FLAG
								, ESTIMATED_EFFORT
								, ACTUAL_EFFORT
								, UTILIZATION
								, PERCENT_COMPLETE
								, TEAM_ID
								, TEAM_SET_LIST
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("ProjectTaskActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

