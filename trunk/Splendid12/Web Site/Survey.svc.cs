﻿/*
 * Copyright (C) 2013 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;

namespace SplendidCRM
{
	[ServiceContract]
	[ServiceBehavior(IncludeExceptionDetailInFaults=true)]
	[AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Required)]
	public class Survey
	{
		#region json utils
		// http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
		private static long UnixTicks(DateTime dt)
		{
			return (dt.Ticks - 621355968000000000) / 10000;
		}

		private static string ToJsonDate(object dt)
		{
			return "\\/Date(" + UnixTicks(Sql.ToDateTime(dt)).ToString() + ")\\/";
		}

		// 08/03/2012 Paul.  FromJsonDate is used on Web Capture services. 
		public static DateTime FromJsonDate(string s)
		{
			DateTime dt = DateTime.MinValue;
			if ( s.StartsWith("\\/Date(") && s.EndsWith(")\\/") )
			{
				s = s.Replace("\\/Date(", "");
				s = s.Replace(")\\/", "");
				long lEpoch = Sql.ToLong(s);
				dt = new DateTime(lEpoch * 10000 + 621355968000000000);
			}
			else
			{
				dt = Sql.ToDateTime(s);
			}
			return dt;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		// http://schotime.net/blog/index.php/2008/07/27/dataset-datatable-to-json/
		private static List<Dictionary<string, object>> RowsToDictionary(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			List<Dictionary<string, object>> objs = new List<Dictionary<string, object>>();
			// 10/11/2012 Paul.  dt will be null when no results security filter applied. 
			if ( dt != null )
			{
				foreach (DataRow dr in dt.Rows)
				{
					// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
					Dictionary<string, object> drow = new Dictionary<string, object>();
					if ( dt.Columns.Contains("ID") )
					{
						Guid gID = Sql.ToGuid(dr["ID"]);
						if ( !Sql.IsEmptyString(sBaseURI) && !Sql.IsEmptyString(sModuleName) )
						{
							Dictionary<string, object> metadata = new Dictionary<string, object>();
							metadata.Add("uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "");
							metadata.Add("type", "SplendidCRM." + sModuleName);
							if ( dr.Table.Columns.Contains("DATE_MODIFIED_UTC") )
							{
								DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime(dr["DATE_MODIFIED_UTC"]);
								metadata.Add("etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
							}
							drow.Add("__metadata", metadata);
						}
					}
				
					for (int i = 0; i < dt.Columns.Count; i++)
					{
						if ( dt.Columns[i].DataType.FullName == "System.DateTime" )
						{
							// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
							drow.Add(dt.Columns[i].ColumnName, ToJsonDate(T10n.FromServerTime(dr[i])) );
						}
						else
						{
							drow.Add(dt.Columns[i].ColumnName, dr[i]);
						}
					}
					objs.Add(drow);
				}
			}
			return objs;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataTable dt, TimeZone T10n)
		{
			Dictionary<string, object> d = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			results.Add("results", RowsToDictionary(sBaseURI, sModuleName, dt, T10n));
			d.Add("d", results);
			// 04/21/2017 Paul.  Count should be returend as a number. 
			if ( dt != null )
				d.Add("__count", dt.Rows.Count);
			return d;
		}

		// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
		private static Dictionary<string, object> ToJson(string sBaseURI, string sModuleName, DataRow dr, TimeZone T10n)
		{
			Dictionary<string, object> d       = new Dictionary<string, object>();
			Dictionary<string, object> results = new Dictionary<string, object>();
			Dictionary<string, object> drow    = new Dictionary<string, object>();
			
			// 06/28/2011 Paul.  Now that we have switched to using views, the results may not have an ID column. 
			if ( dr.Table.Columns.Contains("ID") )
			{
				Guid gID = Sql.ToGuid(dr["ID"]);
				if ( !Sql.IsEmptyString(sBaseURI) && !Sql.IsEmptyString(sModuleName) )
				{
					Dictionary<string, object> metadata = new Dictionary<string, object>();
					metadata.Add("uri", sBaseURI + "?ModuleName=" + sModuleName + "&ID=" + gID.ToString() + "");
					metadata.Add("type", "SplendidCRM." + sModuleName);
					if ( dr.Table.Columns.Contains("DATE_MODIFIED_UTC") )
					{
						DateTime dtDATE_MODIFIED_UTC = Sql.ToDateTime(dr["DATE_MODIFIED_UTC"]);
						metadata.Add("etag", gID.ToString() + "." + dtDATE_MODIFIED_UTC.Ticks.ToString() );
					}
					drow.Add("__metadata", metadata);
				}
			}
			
			for (int i = 0; i < dr.Table.Columns.Count; i++)
			{
				if ( dr.Table.Columns[i].DataType.FullName == "System.DateTime" )
				{
					// 05/05/2013 Paul.  We need to convert the date to the user's timezone. 
					drow.Add(dr.Table.Columns[i].ColumnName, ToJsonDate(T10n.FromServerTime(dr[i])) );
				}
				else
				{
					drow.Add(dr.Table.Columns[i].ColumnName, dr[i]);
				}
			}
			
			results.Add("results", drow);
			d.Add("d", results);
			return d;
		}

		#endregion

		#region Get
		[OperationContract]
		[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Stream GetSurveyQuestion(Guid ID)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Cache-Control", "no-cache");
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Pragma", "no-cache");
			
			string sBaseURI = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath;
			JavaScriptSerializer json = new JavaScriptSerializer();
			
			Guid     gTIMEZONE         = Sql.ToGuid  (Application["CONFIG.default_timezone"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			Dictionary<string, object> dict = null;
			
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				string sSQL ;
				sSQL = "select *                 " + ControlChars.CrLf
				     + "  from vwSURVEY_QUESTIONS" + ControlChars.CrLf
				     + " where ID = @ID          " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", ID);
					con.Open();
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							if ( dt.Rows.Count == 0 )
								throw(new Exception("Survey Question not found: " + ID.ToString()));
							
							dt.Columns.Add("RANDOMIZE_COUNT", typeof(int));
							DataRow row = dt.Rows[0];
							int nRANDOMIZE_COUNT = Sql.ToInteger(Application["SurveyQuestion_Randomize_Count_" + Sql.ToString(row["ID"])]);
							row["RANDOMIZE_COUNT"] = nRANDOMIZE_COUNT;
							nRANDOMIZE_COUNT++;
							Application["SurveyQuestion_Randomize_Count_" + Sql.ToString(row["ID"])] = nRANDOMIZE_COUNT;
							dict = ToJson(sBaseURI, "SurveyQuestions", dt.Rows[0], T10n);
						}
					}
				}
			}
			
			
			/*
			string sEXPAND = Sql.ToString (Request.QueryString["$expand"]);
			if ( sEXPAND == "*" )
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					Dictionary<string, object> d       = dict["d"] as Dictionary<string, object>;
					Dictionary<string, object> results = d["results"] as Dictionary<string, object>;
					DataTable dtRelationships = SplendidCache.DetailViewRelationships(ModuleName + ".DetailView");
					foreach ( DataRow row in dtRelationships.Rows )
					{
						try
						{
							string sRELATED_MODULE     = Sql.ToString(row["MODULE_NAME"]);
							string sRELATED_TABLE      = Sql.ToString(Application["Modules." + sRELATED_MODULE + ".TableName"]);
							string sRELATED_FIELD_NAME = Crm.Modules.SingularTableName(sRELATED_TABLE) + "_ID";
							if ( !d.ContainsKey(sRELATED_MODULE) && SplendidCRM.Security.GetUserAccess(sRELATED_MODULE, "list") >= 0 )
							{
								using ( DataTable dtSYNC_TABLES = SplendidCache.RestTables(sRELATED_TABLE, true) )
								{
									string sSQL;
									if ( dtSYNC_TABLES != null && dtSYNC_TABLES.Rows.Count > 0 )
									{
										UniqueStringCollection arrSearchFields = new UniqueStringCollection();
										SplendidDynamic.SearchGridColumns(ModuleName + "." + sRELATED_MODULE, arrSearchFields);
										
										sSQL = "select " + Sql.FormatSelectFields(arrSearchFields)
										     + "  from vw" + sTABLE_NAME + "_" + sRELATED_TABLE + ControlChars.CrLf;
										using ( IDbCommand cmd = con.CreateCommand() )
										{
											cmd.CommandText = sSQL;
											Security.Filter(cmd, sRELATED_MODULE, "list");
											Sql.AppendParameter(cmd, ID, sRELATED_FIELD_NAME);
											using ( DbDataAdapter da = dbf.CreateDataAdapter() )
											{
												((IDbDataAdapter)da).SelectCommand = cmd;
												using ( DataTable dtSubPanel = new DataTable() )
												{
													da.Fill(dtSubPanel);
													results.Add(sRELATED_MODULE, RowsToDictionary(sBaseURI, sRELATED_MODULE, dtSubPanel, T10n));
												}
											}
										}
									}
								}
							}
						}
						catch(Exception ex)
						{
							SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
						}
					}
				}
			}
			*/
			
			string sResponse = json.Serialize(dict);
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		[OperationContract]
		[WebInvoke(Method="GET", BodyStyle=WebMessageBodyStyle.WrappedRequest, RequestFormat=WebMessageFormat.Json, ResponseFormat=WebMessageFormat.Json)]
		public Stream GetSurvey(Guid ID)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Cache-Control", "no-cache");
			WebOperationContext.Current.OutgoingResponse.Headers.Add("Pragma", "no-cache");
			
			string sBaseURI = String.Empty;  //Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath;
			JavaScriptSerializer json = new JavaScriptSerializer();
			
			Guid     gTIMEZONE         = Sql.ToGuid  (Application["CONFIG.default_timezone"]);
			TimeZone T10n              = TimeZone.CreateTimeZone(gTIMEZONE);
			Dictionary<string, object> dict = null;
			
			DbProviderFactory dbf = DbProviderFactories.GetFactory();
			using ( IDbConnection con = dbf.CreateConnection() )
			{
				con.Open();
				string sSQL ;
				sSQL = "select ID                " + ControlChars.CrLf
				     + "     , NAME              " + ControlChars.CrLf
				     + "     , STATUS            " + ControlChars.CrLf
				     + "     , SURVEY_STYLE      " + ControlChars.CrLf
				     + "     , SURVEY_THEME_ID   " + ControlChars.CrLf
				     + "     , PAGE_RANDOMIZATION" + ControlChars.CrLf
				     + "     , DESCRIPTION       " + ControlChars.CrLf
				     + "  from vwSURVEYS         " + ControlChars.CrLf
				     + " where ID = @ID          " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@ID", ID);
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dt = new DataTable() )
						{
							da.Fill(dt);
							if ( dt.Rows.Count == 0 )
								throw(new Exception("Survey not found: " + ID.ToString()));
							
							dt.Columns.Add("RANDOMIZE_COUNT", typeof(int));
							DataRow row = dt.Rows[0];
							int nRANDOMIZE_COUNT = Sql.ToInteger(Application["Survey_Randomize_Count_" + Sql.ToString(row["ID"])]);
							row["RANDOMIZE_COUNT"] = nRANDOMIZE_COUNT;
							nRANDOMIZE_COUNT++;
							Application["Survey_Randomize_Count_" + Sql.ToString(row["ID"])] = nRANDOMIZE_COUNT;
							dict = ToJson(sBaseURI, "Surveys", dt.Rows[0], T10n);
						}
					}
				}
				
				Dictionary<string, object> d       = dict["d"] as Dictionary<string, object>;
				Dictionary<string, object> results = d["results"] as Dictionary<string, object>;
				sSQL = "select ID                    " + ControlChars.CrLf
				     + "     , SURVEY_ID             " + ControlChars.CrLf
				     + "     , NAME                  " + ControlChars.CrLf
				     + "     , PAGE_NUMBER           " + ControlChars.CrLf
				     + "     , QUESTION_RANDOMIZATION" + ControlChars.CrLf
				     + "     , DESCRIPTION           " + ControlChars.CrLf
				     + "  from vwSURVEY_PAGES        " + ControlChars.CrLf
				     + " where SURVEY_ID = @SURVEY_ID" + ControlChars.CrLf
				     + " order by PAGE_NUMBER        " + ControlChars.CrLf;
				using ( IDbCommand cmd = con.CreateCommand() )
				{
					cmd.CommandText = sSQL;
					Sql.AddParameter(cmd, "@SURVEY_ID", ID);
					using ( DbDataAdapter da = dbf.CreateDataAdapter() )
					{
						((IDbDataAdapter)da).SelectCommand = cmd;
						using ( DataTable dtPages = new DataTable() )
						{
							da.Fill(dtPages);
							dtPages.Columns.Add("RANDOMIZE_COUNT", typeof(int));
							foreach ( DataRow row in dtPages.Rows )
							{
								int nRANDOMIZE_COUNT = Sql.ToInteger(Application["SurveyPage_Randomize_Count_" + Sql.ToString(row["ID"])]);
								row["RANDOMIZE_COUNT"] = nRANDOMIZE_COUNT;
								nRANDOMIZE_COUNT++;
								Application["SurveyPage_Randomize_Count_" + Sql.ToString(row["ID"])] = nRANDOMIZE_COUNT;
							}
							results.Add("SURVEY_PAGES", RowsToDictionary(sBaseURI, "SURVEY_PAGES", dtPages, T10n));
						}
					}
				}
				List<Dictionary<string, object>> SURVEY_PAGES = results["SURVEY_PAGES"] as List<Dictionary<string, object>>;
				for ( int i = 0; i < SURVEY_PAGES.Count; i++ )
				{
					Guid gSURVEY_PAGE_ID = Sql.ToGuid(SURVEY_PAGES[i]["ID"]);
					sSQL = "select ID                              " + ControlChars.CrLf
					     + "     , SURVEY_ID                       " + ControlChars.CrLf
					     + "     , SURVEY_PAGE_ID                  " + ControlChars.CrLf
					     + "     , NAME                            " + ControlChars.CrLf
					     + "     , QUESTION_NUMBER                 " + ControlChars.CrLf
					     + "     , DESCRIPTION                     " + ControlChars.CrLf
					     + "     , QUESTION_TYPE                   " + ControlChars.CrLf
					     + "     , DISPLAY_FORMAT                  " + ControlChars.CrLf
					     + "     , ANSWER_CHOICES                  " + ControlChars.CrLf
					     + "     , COLUMN_CHOICES                  " + ControlChars.CrLf
					     + "     , FORCED_RANKING                  " + ControlChars.CrLf
					     + "     , INVALID_DATE_MESSAGE            " + ControlChars.CrLf
					     + "     , INVALID_NUMBER_MESSAGE          " + ControlChars.CrLf
					     + "     , NA_ENABLED                      " + ControlChars.CrLf
					     + "     , NA_LABEL                        " + ControlChars.CrLf
					     + "     , OTHER_ENABLED                   " + ControlChars.CrLf
					     + "     , OTHER_LABEL                     " + ControlChars.CrLf
					     + "     , OTHER_HEIGHT                    " + ControlChars.CrLf
					     + "     , OTHER_WIDTH                     " + ControlChars.CrLf
					     + "     , OTHER_AS_CHOICE                 " + ControlChars.CrLf
					     + "     , OTHER_ONE_PER_ROW               " + ControlChars.CrLf
					     + "     , OTHER_REQUIRED_MESSAGE          " + ControlChars.CrLf
					     + "     , OTHER_VALIDATION_TYPE           " + ControlChars.CrLf
					     + "     , OTHER_VALIDATION_MIN            " + ControlChars.CrLf
					     + "     , OTHER_VALIDATION_MAX            " + ControlChars.CrLf
					     + "     , OTHER_VALIDATION_MESSAGE        " + ControlChars.CrLf
					     + "     , REQUIRED                        " + ControlChars.CrLf
					     + "     , REQUIRED_TYPE                   " + ControlChars.CrLf
					     + "     , REQUIRED_RESPONSES_MIN          " + ControlChars.CrLf
					     + "     , REQUIRED_RESPONSES_MAX          " + ControlChars.CrLf
					     + "     , REQUIRED_MESSAGE                " + ControlChars.CrLf
					     + "     , VALIDATION_TYPE                 " + ControlChars.CrLf
					     + "     , VALIDATION_MIN                  " + ControlChars.CrLf
					     + "     , VALIDATION_MAX                  " + ControlChars.CrLf
					     + "     , VALIDATION_MESSAGE              " + ControlChars.CrLf
					     + "     , VALIDATION_SUM_ENABLED          " + ControlChars.CrLf
					     + "     , VALIDATION_NUMERIC_SUM          " + ControlChars.CrLf
					     + "     , VALIDATION_SUM_MESSAGE          " + ControlChars.CrLf
					     + "     , RANDOMIZE_TYPE                  " + ControlChars.CrLf
					     + "     , RANDOMIZE_NOT_LAST              " + ControlChars.CrLf
					     + "     , SIZE_WIDTH                      " + ControlChars.CrLf
					     + "     , SIZE_HEIGHT                     " + ControlChars.CrLf
					     + "     , BOX_WIDTH                       " + ControlChars.CrLf
					     + "     , BOX_HEIGHT                      " + ControlChars.CrLf
					     + "     , COLUMN_WIDTH                    " + ControlChars.CrLf
					     + "     , PLACEMENT                       " + ControlChars.CrLf
					     + "     , SPACING_LEFT                    " + ControlChars.CrLf
					     + "     , SPACING_TOP                     " + ControlChars.CrLf
					     + "     , SPACING_RIGHT                   " + ControlChars.CrLf
					     + "     , SPACING_BOTTOM                  " + ControlChars.CrLf
					     + "     , IMAGE_URL                       " + ControlChars.CrLf
					     + "  from vwSURVEY_PAGES_QUESTIONS        " + ControlChars.CrLf
					     + " where SURVEY_PAGE_ID = @SURVEY_PAGE_ID" + ControlChars.CrLf
					     + " order by QUESTION_NUMBER              " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						Sql.AddParameter(cmd, "@SURVEY_PAGE_ID", gSURVEY_PAGE_ID);
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dtQuestions = new DataTable() )
							{
								da.Fill(dtQuestions);
								dtQuestions.Columns.Add("RANDOMIZE_COUNT", typeof(int));
								foreach ( DataRow row in dtQuestions.Rows )
								{
									int nRANDOMIZE_COUNT = Sql.ToInteger(Application["SurveyQuestion_Randomize_Count_" + Sql.ToString(row["ID"])]);
									row["RANDOMIZE_COUNT"] = nRANDOMIZE_COUNT;
									nRANDOMIZE_COUNT++;
									Application["SurveyQuestion_Randomize_Count_" + Sql.ToString(row["ID"])] = nRANDOMIZE_COUNT;
								}
								SURVEY_PAGES[i].Add("SURVEY_QUESTIONS", RowsToDictionary(sBaseURI, "SURVEY_QUESTIONS", dtQuestions, T10n));
							}
						}
					}
				}
			}
			string sResponse = json.Serialize(dict);
			byte[] byResponse = Encoding.UTF8.GetBytes(sResponse);
			return new MemoryStream(byResponse);
		}

		#endregion

		public static string md5(string sValue)
		{
			UTF8Encoding utf8 = new UTF8Encoding();
			byte[] aby = utf8.GetBytes(sValue);
			
			using ( MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider() )
			{
				byte[] binMD5 = md5.ComputeHash(aby);
				return Sql.HexEncode(binMD5);
			}
		}

		public static Guid ConvertAnswerID(string sANSWER_ID)
		{
			sANSWER_ID = sANSWER_ID.Substring(0, 8) + "-" + sANSWER_ID.Substring(8, 4) + "-" + sANSWER_ID.Substring(12, 4) + "-" + sANSWER_ID.Substring(16, 4) + "-" + sANSWER_ID.Substring(20, 12);
			Guid gANSWER_ID = Sql.ToGuid(sANSWER_ID);
#if DEBUG
			if ( sANSWER_ID.ToLower() != gANSWER_ID.ToString().ToLower() )
			{
				throw(new Exception(sANSWER_ID + " != " + gANSWER_ID.ToString()));
			}
#endif
			return gANSWER_ID;
		}

		private static void SplitAnswer(string sQUESTION_TYPE, string sVALUE, bool bOTHER_AS_CHOICE, ref Guid gANSWER_ID, ref string sANSWER_TEXT, ref Guid gCOLUMN_ID, ref string sCOLUMN_TEXT, ref Guid gMENU_ID, ref string sMENU_TEXT, ref int nWEIGHT, ref string sOTHER_TEXT)
		{
			if ( !Sql.IsEmptyString(sVALUE) )
			{
				string[] arrANSWER = sVALUE.Split(',');
				if ( arrANSWER.Length >= 2 )
				{
					if ( arrANSWER[0].Length == 32 )
					{
						string sANSWER_ID = arrANSWER[0];
						if ( sANSWER_ID == md5("Other") )
						{
							sOTHER_TEXT = sVALUE.Substring(33);
							if ( bOTHER_AS_CHOICE )
							{
								gANSWER_ID = ConvertAnswerID(sANSWER_ID);
							}
						}
						else
						{
							gANSWER_ID = ConvertAnswerID(sANSWER_ID);
						}
						sANSWER_TEXT = arrANSWER[1];
						if ( sQUESTION_TYPE == "Ranking" && arrANSWER.Length == 3 )
						{
							nWEIGHT = Sql.ToInteger(arrANSWER[2]);
						}
					}
					else if ( arrANSWER[0].Length == 32+1+32 )
					{
						string[] arrCOLUMN = arrANSWER[0].Split('_');
						if ( arrCOLUMN.Length == 2 )
						{
							string sANSWER_ID = arrCOLUMN[0];
							string sCOLUMN_ID = arrCOLUMN[1];
							gANSWER_ID = ConvertAnswerID(sANSWER_ID);
							gCOLUMN_ID = ConvertAnswerID(sCOLUMN_ID);
							if ( sCOLUMN_ID == md5("Other") )
							{
								sANSWER_TEXT = arrANSWER[1];
							}
							else
							{
								sANSWER_TEXT = arrANSWER[1];
								if ( arrANSWER.Length > 2 )
									sCOLUMN_TEXT = arrANSWER[2];
								if ( arrANSWER.Length > 3 )
									nWEIGHT = Sql.ToInteger(arrANSWER[3]);
							}
						}
					}
					else if ( arrANSWER[0].Length == 32+1+32+1+32 )
					{
						string[] arrCOLUMN = arrANSWER[0].Split('_');
						if ( arrCOLUMN.Length == 3 )
						{
							string sANSWER_ID = arrCOLUMN[0];
							string sCOLUMN_ID = arrCOLUMN[1];
							string sMENU_ID   = arrCOLUMN[2];
							gANSWER_ID = ConvertAnswerID(sANSWER_ID);
							gCOLUMN_ID = ConvertAnswerID(sCOLUMN_ID);
							gMENU_ID   = ConvertAnswerID(sMENU_ID  );
							sANSWER_TEXT = arrANSWER[1];
							if ( arrANSWER.Length > 2 )
								sCOLUMN_TEXT = arrANSWER[2];
							if ( arrANSWER.Length > 3 )
								sMENU_TEXT = arrANSWER[3];
						}
					}
				}
			}
		}

		#region Update
		[OperationContract]
		public Guid UpdateSurvey(Stream input)
		{
			HttpApplicationState Application = HttpContext.Current.Application;
			HttpRequest          Request     = HttpContext.Current.Request    ;
			HttpSessionState     Session     = HttpContext.Current.Session    ;
			
			string sRequest = String.Empty;
			using ( StreamReader stmRequest = new StreamReader(input, System.Text.Encoding.UTF8) )
			{
				sRequest = stmRequest.ReadToEnd();
			}
			// http://weblogs.asp.net/hajan/archive/2010/07/23/javascriptserializer-dictionary-to-json-serialization-and-deserialization.aspx
			JavaScriptSerializer json = new JavaScriptSerializer();
			Dictionary<string, object> dict = json.Deserialize<Dictionary<string, object>>(sRequest);

			Guid gID = Guid.Empty;
			try
			{
				         gID           = (dict.ContainsKey("SURVEY_RESULT_ID") ?              Sql.ToGuid   (dict["SURVEY_RESULT_ID"])  : Guid.Empty       );
				Guid     gSURVEY_ID    = (dict.ContainsKey("ID"              ) ?              Sql.ToGuid   (dict["ID"              ])  : Guid.Empty       );
				Guid     gPARENT_ID    = (dict.ContainsKey("PARENT_ID"       ) ?              Sql.ToGuid   (dict["PARENT_ID"       ])  : Guid.Empty       );
				bool     bIS_COMPLETE  = (dict.ContainsKey("IS_COMPLETE"     ) ?              Sql.ToBoolean(dict["IS_COMPLETE"     ])  : false            );
				DateTime dtSTART_DATE  = (dict.ContainsKey("START_DATE"      ) ? FromJsonDate (Sql.ToString(dict["START_DATE"      ])) : DateTime.MinValue);
				DateTime dtSUBMIT_DATE = (dict.ContainsKey("SUBMIT_DATE"     ) ? FromJsonDate (Sql.ToString(dict["SUBMIT_DATE"     ])) : DateTime.MinValue);
                string sIP_ADDRESS = SplendidInit.GetRemoteHostByF5();
				string   sUSER_AGENT   = Request.UserAgent;
				if ( Sql.IsEmptyGuid(gPARENT_ID) && Security.IsAuthenticated() )
					gPARENT_ID = Security.USER_ID;
				
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					SqlProcs.spSURVEY_RESULTS_Update(ref gID, gSURVEY_ID, gPARENT_ID, dtSTART_DATE, dtSUBMIT_DATE, bIS_COMPLETE, sIP_ADDRESS, sUSER_AGENT);
					ArrayList arrSURVEY_PAGES = (dict.ContainsKey("SURVEY_PAGES") ? dict["SURVEY_PAGES"] as ArrayList : new ArrayList());
					if ( arrSURVEY_PAGES.Count > 0 )
					{
						for ( int nPageIndex = 0; nPageIndex < arrSURVEY_PAGES.Count; nPageIndex++ )
						{
							Dictionary<string, object> dictPage = arrSURVEY_PAGES[nPageIndex] as Dictionary<string, object>;
							Guid      gSURVEY_RESULT_ID    = gID;
							Guid      gSURVEY_PAGE_ID      = (dictPage.ContainsKey("ID"              ) ? Sql.ToGuid(dictPage["ID"])                : Guid.Empty     );
							ArrayList arrSURVEY_QUESTIONS  = (dictPage.ContainsKey("SURVEY_QUESTIONS") ? dictPage["SURVEY_QUESTIONS"] as ArrayList : new ArrayList());
							if ( arrSURVEY_QUESTIONS.Count > 0 )
							{
								for ( int nQuestionIndex = 0; nQuestionIndex < arrSURVEY_QUESTIONS.Count; nQuestionIndex++ )
								{
									Dictionary<string, object> dictQuestion = arrSURVEY_QUESTIONS[nQuestionIndex] as Dictionary<string, object>;
									// 06/15/2013 Paul.  Use a transaction for each question so that a single question failure will not rollback any previous valid questions. 
									using ( IDbTransaction trn = Sql.BeginTransaction(con) )
									{
										try
										{
											Guid   gSURVEY_QUESTION_ID = (dictQuestion.ContainsKey("ID"             ) ? Sql.ToGuid   (dictQuestion["ID"             ]) : Guid.Empty  );
											string sQUESTION_TYPE      = (dictQuestion.ContainsKey("QUESTION_TYPE"  ) ? Sql.ToString (dictQuestion["QUESTION_TYPE"  ]) : String.Empty);
											string sANSWER_CHOICES     = (dictQuestion.ContainsKey("ANSWER_CHOICES" ) ? Sql.ToString (dictQuestion["ANSWER_CHOICES" ]) : String.Empty);
											string sCOLUMN_CHOICES     = (dictQuestion.ContainsKey("COLUMN_CHOICES" ) ? Sql.ToString (dictQuestion["COLUMN_CHOICES" ]) : String.Empty);
											bool   bOTHER_AS_CHOICE    = (dictQuestion.ContainsKey("OTHER_AS_CHOICE") ? Sql.ToBoolean(dictQuestion["OTHER_AS_CHOICE"]) : false       );
											// 06/15/2013 Paul.  Even if no values, log that the user saw the question. 
											if ( !dictQuestion.ContainsKey("VALUES") )
											{
												Guid gQUESTIONS_RESULT_ID = Guid.Empty;
												SqlProcs.spSURVEY_QUESTIONS_RESULTS_Update(ref gQUESTIONS_RESULT_ID, gSURVEY_RESULT_ID, gSURVEY_ID, gSURVEY_PAGE_ID, gSURVEY_QUESTION_ID, Guid.Empty, String.Empty, Guid.Empty, String.Empty, Guid.Empty, String.Empty, 0, String.Empty, trn);
											}
											// 06/15/2013 Paul.  Single value questions are still returned in an array. 
											else if ( dictQuestion["VALUES"] is ArrayList )
											{
												ArrayList arrVALUES = dictQuestion["VALUES"] as ArrayList;
												switch ( sQUESTION_TYPE )
												{
													case "Text Area"        :
													case "Textbox"          :
													// 12/26/2015 Paul.  Range not officially supported until now.  Was not saving the data. 
													case "Range"            :
													{
														if ( arrVALUES.Count > 0 )
														{
															Guid   gANSWER_ID   = Guid.Empty  ;
															string sANSWER_TEXT = Sql.ToString(arrVALUES[0]);
															Guid   gCOLUMN_ID   = Guid.Empty  ;
															string sCOLUMN_TEXT = String.Empty;
															Guid   gMENU_ID     = Guid.Empty  ;
															string sMENU_TEXT   = String.Empty;
															int    nWEIGHT      = 0           ;
															string sOTHER_TEXT  = String.Empty;
															Guid gQUESTIONS_RESULT_ID = Guid.Empty;
															SqlProcs.spSURVEY_QUESTIONS_RESULTS_Update(ref gQUESTIONS_RESULT_ID, gSURVEY_RESULT_ID, gSURVEY_ID, gSURVEY_PAGE_ID, gSURVEY_QUESTION_ID, gANSWER_ID, sANSWER_TEXT, gCOLUMN_ID, sCOLUMN_TEXT, gMENU_ID, sMENU_TEXT, nWEIGHT, sOTHER_TEXT, trn);
														}
														break;
													}
													case "Radio"            :
													case "Dropdown"         :
													case "Checkbox"         :
													case "Ranking"          :
													case "Rating Scale"     :
													case "Radio Matrix"     :
													case "Checkbox Matrix"  :
													case "Dropdown Matrix"  :
													case "Textbox Multiple" :
													case "Textbox Numerical":
													case "Date"             :
													case "Demographic"      :
													{
														// 6311ae17c1ee52b36e68aaf4ad066387,aaaa
														// 6311ae17c1ee52b36e68aaf4ad066387,bbbb
														// 6311ae17c1ee52b36e68aaf4ad066387,cccc
														foreach ( string sVALUE in arrVALUES )
														{
															Guid   gANSWER_ID   = Guid.Empty  ;
															string sANSWER_TEXT = String.Empty;
															Guid   gCOLUMN_ID   = Guid.Empty  ;
															string sCOLUMN_TEXT = String.Empty;
															Guid   gMENU_ID     = Guid.Empty  ;
															string sMENU_TEXT   = String.Empty;
															int    nWEIGHT      = 0           ;
															string sOTHER_TEXT  = String.Empty;
															if ( !Sql.IsEmptyString(sVALUE) )
															{
																SplitAnswer(sQUESTION_TYPE, sVALUE, bOTHER_AS_CHOICE, ref gANSWER_ID, ref sANSWER_TEXT, ref gCOLUMN_ID, ref sCOLUMN_TEXT, ref gMENU_ID, ref sMENU_TEXT, ref nWEIGHT, ref sOTHER_TEXT);
																if ( sQUESTION_TYPE == "Date" )
																{
																	string sDISPLAY_FORMAT = (dictQuestion.ContainsKey("DISPLAY_FORMAT") ? Sql.ToString (dictQuestion["DISPLAY_FORMAT"]) : String.Empty);
																	switch ( sDISPLAY_FORMAT )
																	{
																		case "Date"    :  sANSWER_TEXT = FromJsonDate(sANSWER_TEXT).ToShortDateString();  break;
																		case "Time"    :  sANSWER_TEXT = FromJsonDate(sANSWER_TEXT).ToShortTimeString();  break;
																		case "DateTime":  sANSWER_TEXT = FromJsonDate(sANSWER_TEXT).ToString()         ;  break;
																	}
																}
																Guid gQUESTIONS_RESULT_ID = Guid.Empty;
																SqlProcs.spSURVEY_QUESTIONS_RESULTS_Update(ref gQUESTIONS_RESULT_ID, gSURVEY_RESULT_ID, gSURVEY_ID, gSURVEY_PAGE_ID, gSURVEY_QUESTION_ID, gANSWER_ID, sANSWER_TEXT, gCOLUMN_ID, sCOLUMN_TEXT, gMENU_ID, sMENU_TEXT, nWEIGHT, sOTHER_TEXT, trn);
															}
														}
														break;
													}
													case "Plain Text"       :
													case "Image"            :
														break;
												}
											}
											trn.Commit();
										}
										catch
										{
											trn.Rollback();
											throw;
										}
									}
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return gID;
		}
		#endregion
	}
}
