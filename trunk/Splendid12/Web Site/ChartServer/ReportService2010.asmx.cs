/**
 * Copyright (C) 2010-2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Caching;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SplendidCRM.ChartServer
{
	/// <summary>
	/// Summary description for ReportService2010
	/// </summary>
	[WebService(Namespace = "http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Name="ReportService2010", Description="SplendidCRM Report Service 2010.")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ReportService2010 : System.Web.Services.WebService
	{
		#region Enums
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public enum CredentialRetrievalEnum
		{
			Prompt,
			Store,
			Integrated,
			None,
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public enum SensitivityEnum
		{
			True,
			False,
			Auto,
		}

		#endregion

		#region Structures
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ModelPerspective
		{
			public string ID         ;
			public string Name       ;
			public string Description;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ModelCatalogItem
		{
			public string             Model       ;
			public string             Description ;
			public ModelPerspective[] Perspectives;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class CatalogItem
		{
			public string     ID          ;
			public string     Name        ;
			public string     Path        ;
			public string     VirtualPath ;
			public string     TypeName    ;
			public int        Size        ;
			public string     Description ;
			public bool       Hidden      ;
			public DateTime   CreationDate;
			public DateTime   ModifiedDate;
			public string     CreatedBy   ;
			public string     ModifiedBy  ;
			public Property[] ItemMetadata;
		}

		[XmlIncludeAttribute(typeof(DataSourceReference       ))]
		[XmlIncludeAttribute(typeof(InvalidDataSourceReference))]
		[XmlIncludeAttribute(typeof(DataSourceDefinition      ))]
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSourceDefinitionOrReference
		{
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class InvalidDataSourceReference : DataSourceDefinitionOrReference
		{
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSourceReference : DataSourceDefinitionOrReference
		{
			public string Reference;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSourceDefinition : DataSourceDefinitionOrReference
		{
			public string                  Extension                           ;
			public string                  ConnectString                       ;
			public bool                    UseOriginalConnectString            ;
			public bool                    OriginalConnectStringExpressionBased;
			public CredentialRetrievalEnum CredentialRetrieval                 ;
			public bool                    WindowsCredentials                  ;
			public bool                    ImpersonateUser                     ;
			public string                  Prompt                              ;
			public string                  UserName                            ;
			public string                  Password                            ;
			public bool                    Enabled                             ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSource
		{
			public string Name;
			[XmlElementAttribute("DataSourceDefinition"      , typeof(DataSourceDefinition      ))]
			[XmlElementAttribute("DataSourceReference"       , typeof(DataSourceReference       ))]
			[XmlElementAttribute("InvalidDataSourceReference", typeof(InvalidDataSourceReference))]
			public DataSourceDefinitionOrReference Item;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class Warning
		{
			public string Code      ;
			public string Severity  ;
			public string ObjectName;
			public string ObjectType;
			public string Message   ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class Property
		{
			public string Name ;
			public string Value;

			public Property()
			{
			}

			public Property(string Name)
			{
				this.Name  = Name;
			}

			public Property(string Name, string Value)
			{
				this.Name  = Name ;
				this.Value = Value;
			}
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class SYSTEMTIME
		{
			public short year        ;
			public short month       ;
			public short dayOfWeek   ;
			public short day         ;
			public short hour        ;
			public short minute      ;
			public short second      ;
			public short milliseconds;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class TimeZoneInformation
		{
			public int        Bias        ;
			public int        StandardBias;
			public SYSTEMTIME StandardDate;
			public int        DaylightBias;
			public SYSTEMTIME DaylightDate;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", IsNullable=false)]
		public class ServerInfoHeader : SoapHeader
		{
			public string ReportServerVersionNumber;
			public string ReportServerEdition      ;
			public string ReportServerVersion      ;
			public string ReportServerDateTime     ;
			public TimeZoneInformation ReportServerTimeZoneInfo;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", IsNullable=false)]
		public class TrustedUserHeader : SoapHeader
		{
			public string UserName ;
			[XmlElementAttribute(DataType="base64Binary")]
			public byte[] UserToken;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class QueryDefinition
		{
			public string CommandType;
			public string CommandText;
			public int    Timeout    ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class Field
		{
			public string Alias;
			public string Name ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSetDefinition
		{
			public Field[]         Fields             ;
			public QueryDefinition Query              ;
			public SensitivityEnum CaseSensitivity    ;
			public string          Collation          ;
			public SensitivityEnum AccentSensitivity  ;
			public SensitivityEnum KanatypeSensitivity;
			public SensitivityEnum WidthSensitivity   ;
			public string          Name               ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ValidValue
		{
			public string Label;
			public string Value;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ParameterFieldReference : ParameterValueOrFieldReference
		{
			public string ParameterName;
			public string FieldAlias   ;
		}

		[XmlIncludeAttribute(typeof(ParameterFieldReference))]
		[XmlIncludeAttribute(typeof(ParameterValue))]
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ParameterValueOrFieldReference
		{
		}

		/// <remarks/>
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ParameterValue : ParameterValueOrFieldReference
		{
			public string Name ;
			public string Value;
			public string Label;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class DataSourceCredentials
		{
			public string DataSourceName;
			public string UserName      ;
			public string Password      ;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public class ItemReferenceData
		{
			public string Name         ;
			public string Reference    ;
			public string ReferenceType;
		}

		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public partial class ItemReference
		{
			public string Name     ;
			public string Reference;
		}

		// 03/29/2012 Paul.  We need to add Parameter related methods as stubs. 
		[SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer")]
		public partial class ItemParameter
		{
			public string       Name                   ;
			public string       ParameterTypeName      ;
			public bool         Nullable               ;
			public bool         AllowBlank             ;
			public bool         MultiValue             ;
			public bool         QueryParameter         ;
			public string       Prompt                 ;
			public bool         PromptUser             ;
			public string[]     Dependencies           ;
			public bool         ValidValuesQueryBased  ;
			public ValidValue[] ValidValues            ;
			public bool         DefaultValuesQueryBased;
			public string[]     DefaultValues          ;
			public string       ParameterStateName     ;
			public string       ErrorMessage           ;
		}

		#endregion

		#region Headers
		public ServerInfoHeader    ServerInfoHeaderValue;
		public TrustedUserHeader   TrustedUserHeaderValue;

		private void InitServerInfoHeader()
		{
			ServerInfoHeaderValue = new ServerInfoHeader();
			// 09/25/2010 Paul.  We will need to return SQL Server 2008 R2 information in order for ReportBuilder 3.0 to work properly. 
			ServerInfoHeaderValue.ReportServerVersionNumber = "2009.100.1600.01";
			ServerInfoHeaderValue.ReportServerEdition       = "Enterprise";
			ServerInfoHeaderValue.ReportServerVersion       = "Microsoft SQL Server Reporting Services Version 10.50.1600.1";
			ServerInfoHeaderValue.ReportServerDateTime      = DateTime.Now.ToString("s"); // "2009-12-06T03:49:35";
			
			Guid     gTIMEZONE = Sql.ToGuid(Session["USER_SETTINGS/TIMEZONE"]);
			TimeZone T10n      = TimeZone.CreateTimeZone(Application, gTIMEZONE);
			ServerInfoHeaderValue.ReportServerTimeZoneInfo = new TimeZoneInformation();
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.Bias                      =         T10n.Bias                 ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardBias              =         T10n.StandardBias         ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate = new SYSTEMTIME();
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.year         = (short) T10n.StandardDateYear     ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.month        = (short) T10n.StandardDateMonth    ;
			// 01/02/2012 Paul.  We do not store the DayOfWeek value. 
			//ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.dayOfWeek    = (short) T10n.StandardDateDayOfWeek;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.day          = (short) T10n.StandardDateDay      ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.hour         = (short) T10n.StandardDateHour     ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.minute       = (short) T10n.StandardDateMinute   ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.second       = 0;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.StandardDate.milliseconds = 0;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightBias              =         T10n.DaylightBias         ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate = new SYSTEMTIME();
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.year         = (short) T10n.DaylightDateYear     ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.month        = (short) T10n.DaylightDateMonth    ;
			// 01/02/2012 Paul.  We do not store the DayOfWeek value. 
			//ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.dayOfWeek    = (short) T10n.DaylightDateDayOfWeek;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.day          = (short) T10n.DaylightDateDay      ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.hour         = (short) T10n.DaylightDateHour     ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.minute       = (short) T10n.DaylightDateMinute   ;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.second       = 0;
			ServerInfoHeaderValue.ReportServerTimeZoneInfo.DaylightDate.milliseconds = 0;

			// 12/06/2009 Paul.  Since all web methods call this init method, it is a common place to validate the user's access. 
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));
		}
		#endregion

		#region Simple Methods
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/IsSSLRequired", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public bool IsSSLRequired()
		{
			InitServerInfoHeader();
			return false;
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListModelPerspectives"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListModelPerspectives", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("ModelCatalogItems")]
		public ModelCatalogItem[] ListModelPerspectives(string Path)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any perspectives, so we can hard-code the result. 
			ModelCatalogItem[] arr = new ModelCatalogItem[1];
			arr[0] = new ModelCatalogItem();
			arr[0].Model = "/SplendidCRM Model";
			arr[0].Perspectives = new ModelPerspective[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("DataSources")]
		public DataSource[] GetItemDataSources(string Item)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore the Item and hard-code the return value. 
			DataSourceReference dsr = new DataSourceReference();
			dsr.Reference = "/SplendidCRM Data Source";
			DataSource[] arr = new DataSource[1];
			arr[0] = new DataSource();
			arr[0].Name = "SplendidCRM Data Source";
			arr[0].Item = dsr;
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/SetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/SetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public void SetItemDataSources(string Item, DataSource[] DataSources)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore this call. 
		}

		// "http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetSystemProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetSystemProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetSystemProperties(Property[] Properties)
		{
			InitServerInfoHeader();
			// 12/07/2009 Paul.  A default installation of SQL Server 2008 returns nothing. 
			// Typical input properties include:
			// DefaultDataSourceFolder = DataSources
			// DefaultModelFolder = Models
			// 09/25/2010 Paul.  ReportBuilder 3.0 requests SharePointIntegrated. 
			List<Property> lst = new List<Property>();
			foreach ( Property p in Properties )
			{
				switch ( p.Name )
				{
					case "SharePointIntegrated":
					{
						lst.Add(new Property(p.Name, "False"));
						break;
					}
				}
			}
			return lst.ToArray();
		}

		// SOAPAction: "http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetDataSourceContents"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetDataSourceContents", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition")]
		public DataSourceDefinition GetDataSourceContents(string DataSource)
		{
			InitServerInfoHeader();
			// 12/07/2009 Paul.  SplendidCRM is the only data source, so return static information.
			DataSourceDefinition ds = new DataSourceDefinition();
			// 01/21/2010 Paul.  Specify an Extension to fix the following ReportBuilder 2.0 error: 
			// The selected data extension  cannot be loaded. Verify that the selected data extension is correctly registered in RSReportDesigner.config, and is installed on the client for local reports and on the report server for published reports.
			ds.Extension           = "SQL";
			ds.ConnectString       = String.Empty;  // "data source=(local)\\SplendidCRM;initial catalog=SplendidCRM;";
			ds.CredentialRetrieval = CredentialRetrievalEnum.None;
			ds.WindowsCredentials  = false;
			ds.ImpersonateUser     = false;
			ds.Prompt              = "Enter a user name and password to access the data source:";
			ds.UserName            = String.Empty;
			ds.Password            = String.Empty;
			ds.Enabled             = true;
			ds.UseOriginalConnectString = false;
			ds.OriginalConnectStringExpressionBased = false;
			
			// 01/21/2010 Paul.  Allow Integrated security, but default to Prompt. 
			if ( Sql.ToString(Application["CONFIG.ReportServer.CredentialRetrieval"]).ToLower() == "integrated" )
				ds.CredentialRetrieval = CredentialRetrievalEnum.Integrated;
			
			// 01/21/2010 Paul.  SplendidCRM Data Source must be enabled manually, for security reasons. 
			if ( Sql.ToBoolean(Application["CONFIG.ReportServer.DataSource"]) )
			{
				// 01/21/2010 Paul.  Lets try and return the correct datasource and catalog. 
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sDataSource     = String.Empty;
					string sInitialCatalog = String.Empty;
					string sUserName       = String.Empty;
					string sPassword       = String.Empty;
					string[] arrProperties = con.ConnectionString.Split(';');
					foreach ( string sProperty in arrProperties )
					{
						string[] arrNameValue = sProperty.Trim().Split('=');
						if ( arrNameValue.Length == 2 )
						{
							switch ( arrNameValue[0].ToLower() )
							{
								case "data source"    :  sDataSource     = arrNameValue[1];  break;
								case "initial catalog":  sInitialCatalog = arrNameValue[1];  break;
								case "user id"        :  sUserName       = arrNameValue[1];  break;
								case "password"       :  sPassword       = arrNameValue[1];  break;
							}
						}
					}
					ds.ConnectString = "data source=" + sDataSource + ";initial catalog=" + sInitialCatalog + ";";
					if ( ds.CredentialRetrieval == CredentialRetrievalEnum.Integrated )
					{
						ds.UserName = sUserName;
						ds.Password = sPassword;
					}
				}
			}
			return ds;
		}

		// 09/25/2010 Paul.  The following methods are new in ReportBuilder 3.0. 
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/TestConnectForDataSourceDefinition", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public bool TestConnectForDataSourceDefinition(DataSourceDefinition DataSourceDefinition, string UserName, string Password, out string ConnectError)
		{
			InitServerInfoHeader();
			// 09/25/2010 Paul.  SplendidCRM will not use any other connection, so always return true. 
			ConnectError = String.Empty;
			return true;
		}

		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/CreateReportEditSession", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		[return: System.Xml.Serialization.XmlElementAttribute("EditSessionID")]
		public string CreateReportEditSession(string Report, string Parent, [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] Definition, out Warning[] Warnings)
		{
			InitServerInfoHeader();
			// 09/25/2010 Paul.  SplendidCRM does not manage with Report Edit Sessions, so just return a random string. 
			Warnings = null;
			return Guid.NewGuid().ToString();
		}

		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/DeleteItem", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public void DeleteItem(string ItemPath)
		{
			InitServerInfoHeader();
			// 09/25/2010 Paul.  SplendidCRM does not manage with Report Edit Sessions, so there is no need to implement the delete method. 
		}

		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemReferences", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		[return: System.Xml.Serialization.XmlArrayAttribute("ItemReferences")]
		public ItemReferenceData[] GetItemReferences(string ItemPath, string ReferenceItemType)
		{
			InitServerInfoHeader();
			// 09/25/2010 Paul.  SplendidCRM does not support references, so return an empty array. 
			ItemReferenceData[] arr = new ItemReferenceData[0];
			return arr;
		}
		
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/SetItemReferences", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public void SetItemReferences(string ItemPath, ItemReference[] ItemReferences)
		{
			InitServerInfoHeader();
			// 09/25/2010 Paul.  SplendidCRM does not support references, so do nothing. 
		}
		
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListItemTypes", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public string[] ListItemTypes()
		{
			List<string> arr = new List<string>();
			arr.Add("Unknown"     );
			arr.Add("Folder"      );
			arr.Add("Report"      );
			arr.Add("Resource"    );
			arr.Add("LinkedReport");
			arr.Add("DataSource"  );
			arr.Add("Model"       );
			arr.Add("Site"        );
			arr.Add("DataSet"     );
			arr.Add("Component"   );
			return arr.ToArray();
		}
		
		// 03/29/2012 Paul.  We need to add Parameter related methods as stubs. 
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/SetItemParameters", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public void SetItemParameters(string ItemPath, ItemParameter[] Parameters)
		{
			// 03/29/2012 Paul.  We can safely ignore the parameters. 
		}
		
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListParents", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		public CatalogItem[] ListParents(string ItemPath)
		{
			List<CatalogItem> lst = new List<CatalogItem>();
			return lst.ToArray();
		}

		#endregion

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemType"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemType", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Type")]
		public string GetItemType(string ItemPath)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  I'm not sure if the Item will always start with a slash. 
			// Instead of assuming that the item is a report, we may want to query the database for the item name. 
			if ( ItemPath == "/" )
			{
				return "Folder";
			}
			else if ( ItemPath == "/SplendidCRM Model" )
			{
				return "Model";
			}
			else if ( ItemPath == "/SplendidCRM Data Source" )
			{
				return "DataSource";
			}
			else
			{
				string Report = ItemPath;
				int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
				// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
				if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
					Report = Report.Substring(nVtiBin);
				if ( Report.StartsWith("/") )
					Report = Report.Substring(1);
				
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					if ( Report.StartsWith("vw") && Report.EndsWith(".rds") )
					{
						string sDataSet = Report.Substring(2, Report.Length - 6);
						sSQL = "select count(*)                " + ControlChars.CrLf
						     + "  from vwMODULES_Reporting     " + ControlChars.CrLf
						     + " where USER_ID    = @USER_ID   " + ControlChars.CrLf
						     + "   and TABLE_NAME = @TABLE_NAME" + ControlChars.CrLf
						     + "   and TABLE_NAME is not null  " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@USER_ID"   , Security.USER_ID);
							Sql.AddParameter(cmd, "@TABLE_NAME", sDataSet        );
							if ( Sql.ToInteger(cmd.ExecuteScalar()) > 0 )
							{
								return "DataSet";
							}
						}
					}
					else
					{
						if ( Report.EndsWith(".rdl") )
							Report = Report.Substring(0, Report.Length - 4);
						// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
						Guid gReportID = Guid.Empty;
						try
						{
							if ( Report.Length == 36 )
								gReportID = Sql.ToGuid(Report);
						}
						catch
						{
						}
						sSQL = "select count(*)      " + ControlChars.CrLf
						     + "  from vwCHARTS_Edit " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							if ( Sql.IsEmptyGuid(gReportID) )
							{
								cmd.CommandText += " where NAME = @NAME  " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@NAME", Report);
							}
							else
							{
								cmd.CommandText += " where ID = @ID    " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@ID", gReportID);
							}
							if ( Sql.ToInteger(cmd.ExecuteScalar()) > 0 )
							{
								return "Report";
							}
						}
					}
				}
			}
			return "Unknown";
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetProperties(string ItemPath, Property[] Properties)
		{
			InitServerInfoHeader();
			bool bItemFound = false;
			List<Property> lst = new List<Property>();
			if ( ItemPath == "/SplendidCRM Model" )
			{
				foreach ( Property p in Properties )
				{
					switch ( p.Name )
					{
						case "Name"              :  lst.Add(new Property(p.Name, "SplendidCRM"                   ));  break;
						case "Description"       :  lst.Add(new Property(p.Name, "SplendidCRM Reporting Services"));  break;
						case "ModifiedDate"      :  lst.Add(new Property(p.Name, DateTime.Now.ToString("s")      ));  break;
						case "MustUsePerspective":  lst.Add(new Property(p.Name, "false"                         ));  break;
					}
				}
				bItemFound = true;
			}
			else
			{
				try
				{
					string Report = ItemPath;
					int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
					// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
					if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
						Report = Report.Substring(nVtiBin);
					if ( Report.StartsWith("/") )
						Report = Report.Substring(1);
					
					// 12/10/2009 Paul.  The report name will include the full URL. 
					// http://developer6/SplendidCRM6_Sugar/Bugs.rdl
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						if ( Report.StartsWith("vw") && Report.EndsWith(".rds") )
						{
							string sDataSet = Report.Substring(2, Report.Length - 6);
							sSQL = "select *                       " + ControlChars.CrLf
							     + "  from vwMODULES_Reporting     " + ControlChars.CrLf
							     + " where USER_ID    = @USER_ID   " + ControlChars.CrLf
							     + "   and TABLE_NAME = @TABLE_NAME" + ControlChars.CrLf
							     + "   and TABLE_NAME is not null  " + ControlChars.CrLf
							     + " order by TABLE_NAME           " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								Sql.AddParameter(cmd, "@USER_ID"   , Security.USER_ID);
								Sql.AddParameter(cmd, "@TABLE_NAME", sDataSet        );
								
								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									if ( rdr.Read() )
									{
										lst.Add(new Property("ID"          , Sql.ToString(rdr["ID"]   )));
										lst.Add(new Property("Name"        , "vw" + Sql.ToString(rdr["TABLE_NAME"]) + ".rds"));
										lst.Add(new Property("CreatedBy"   , Security.USER_NAME        ));
										lst.Add(new Property("CreationDate", DateTime.Now.ToString("s")));
										lst.Add(new Property("ModifiedBy"  , Security.USER_NAME        ));
										lst.Add(new Property("ModifiedDate", DateTime.Now.ToString("s")));
										lst.Add(new Property("Description"                    ));
										lst.Add(new Property("Hidden"               , "False" ));
										lst.Add(new Property("Path"                 , ItemPath));
										lst.Add(new Property("QueryExecutionTimeOut", "0"     ));
										bItemFound = true;
									}
								}
							}
						}
						else
						{
							if ( Report.EndsWith(".rdl") )
								Report = Report.Substring(0, Report.Length - 4);
							// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
							Guid gReportID = Guid.Empty;
							try
							{
								if ( Report.Length == 36 )
									gReportID = Sql.ToGuid(Report);
							}
							catch
							{
							}
							sSQL = "select *             " + ControlChars.CrLf
							     + "  from vwCHARTS_Edit " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								if ( Sql.IsEmptyGuid(gReportID) )
								{
									cmd.CommandText += " where NAME = @NAME  " + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@NAME", Report);
								}
								else
								{
									cmd.CommandText += " where ID = @ID    " + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@ID", gReportID);
								}
								cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;

								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									if ( rdr.Read() )
									{
										string sRDL = Sql.ToString(rdr["RDL"]);
										// 02/12/2010 Paul.  Loading the RDL will remove the Name property from the Report tag, 
										// which causes an exception in Report Builder 2.0. 
										RdlDocument rdl = new RdlDocument();
										rdl.LoadRdl(sRDL);
										
										lst.Add(new Property("Language"    , rdl.SelectNodeValue("Language"     )));
										lst.Add(new Property("PageHeight"  , rdl.SelectNodeValue("PageHeight"   )));
										lst.Add(new Property("PageWidth"   , rdl.SelectNodeValue("PageWidth"    )));
										lst.Add(new Property("TopMargin"   , rdl.SelectNodeValue("TopMargin"    )));
										lst.Add(new Property("BottomMargin", rdl.SelectNodeValue("BottomMargin" )));
										lst.Add(new Property("LeftMargin"  , rdl.SelectNodeValue("LeftMargin"   )));
										lst.Add(new Property("RightMargin" , rdl.SelectNodeValue("RightMargin"  )));
										lst.Add(new Property("Name"        , Sql.ToString  (rdr["NAME"         ])));
										lst.Add(new Property("Size"        , sRDL.Length.ToString()              ));
										lst.Add(new Property("ID"          , Sql.ToString  (rdr["ID"           ])));
										lst.Add(new Property("CreatedBy"   , Sql.ToString  (rdr["CREATED_BY"   ])));
										lst.Add(new Property("CreationDate", Sql.ToDateTime(rdr["DATE_ENTERED" ]).ToString("s")));
										lst.Add(new Property("ModifiedBy"  , Sql.ToString  (rdr["MODIFIED_BY"  ])));
										lst.Add(new Property("ModifiedDate", Sql.ToDateTime(rdr["DATE_MODIFIED"]).ToString("s")));
										lst.Add(new Property("MIMEType"              ));
										lst.Add(new Property("Description"           ));
										lst.Add(new Property("Hidden"      , "False" ));
										lst.Add(new Property("Path"        , ItemPath));
										bItemFound = true;
									}
									else
									{
									}
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
					throw;
				}
				if ( !bItemFound )
				{
					throw(new Exception("The item '" + ItemPath + "' cannot be found."));
				}
			}
			return lst.ToArray();
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListChildren"
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/ListChildren", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("CatalogItems")]
		public CatalogItem[] ListChildren(string ItemPath, bool Recursive)
		{
			InitServerInfoHeader();

			List<CatalogItem> lst = new List<CatalogItem>();
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select *             " + ControlChars.CrLf
					     + "  from vwCHARTS_Edit " + ControlChars.CrLf
					     + " order by NAME       " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								CatalogItem itmDataSource = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM data source. 
								itmDataSource.ID           = "385e5bf1-fe2f-41b0-83e9-6ea2157e2499";
								itmDataSource.Name         = "SplendidCRM Data Source";
								itmDataSource.Path         = "/" + itmDataSource.Name;
								itmDataSource.TypeName     = "DataSource";
								itmDataSource.CreationDate = DateTime.Now;
								itmDataSource.ModifiedDate = DateTime.Now;
								itmDataSource.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmDataSource.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								CatalogItem itmModel      = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM model. 
								itmModel.ID           = "ffe3817a-2578-44ce-9279-9d92c18fc913";
								itmModel.Name         = "SplendidCRM Model";
								itmModel.Path         = "/" + itmModel.Name;
								itmModel.TypeName     = "Model";
								itmModel.Size         = 1;  // 12/06/2009 Paul.  We are going to hard-code the size until we are ready to retrieve the cached version. 
								itmModel.CreationDate = DateTime.Now;
								itmModel.ModifiedDate = DateTime.Now;
								itmModel.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmModel.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								lst.Add(itmDataSource);
								lst.Add(itmModel);

								for ( int i=0; i < dt.Rows.Count; i++ )
								{
									CatalogItem itm = new CatalogItem();
									DataRow row = dt.Rows[i];
									itm.ID           = Sql.ToString(row["ID"]);
									itm.Name         = Sql.ToString(row["NAME"]);
									itm.Path         = "/" + itm.Name;
									itm.TypeName     = "Report";
									itm.Size         = Sql.ToString  (row["RDL"          ]).Length;
									itm.CreationDate = Sql.ToDateTime(row["DATE_ENTERED" ]);
									itm.ModifiedDate = Sql.ToDateTime(row["DATE_MODIFIED"]);
									itm.CreatedBy    = Sql.ToString  (row["CREATED_BY"   ]);
									itm.ModifiedBy   = Sql.ToString  (row["MODIFIED_BY"  ]);
									lst.Add(itm);
								}
							}
						}
					}
					sSQL = "select ID                      " + ControlChars.CrLf
					     + "     , TABLE_NAME              " + ControlChars.CrLf
					     + "     , MODULE_NAME             " + ControlChars.CrLf
					     + "  from vwMODULES_Reporting     " + ControlChars.CrLf
					     + " where USER_ID    = @USER_ID   " + ControlChars.CrLf
					     + "   and TABLE_NAME is not null  " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						cmd.CommandText += " order by TABLE_NAME" + ControlChars.CrLf;
						Sql.AddParameter(cmd, "@USER_ID", Security.USER_ID);
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dtModules = new DataTable() )
							{
								da.Fill(dtModules);
								foreach ( DataRow row in dtModules.Rows )
								{
									CatalogItem itm = new CatalogItem();
									itm.ID           = Sql.ToString(row["ID"]);
									itm.Name         = "vw" + Sql.ToString(row["TABLE_NAME"]).ToUpper() + ".rds";
									itm.Path         = "/" + itm.Name;
									itm.TypeName     = "DataSet";
									itm.Size         = 10240;
									itm.CreationDate = DateTime.Now;
									itm.ModifiedDate = DateTime.Now;
									itm.CreatedBy    = Security.USER_NAME;
									itm.ModifiedBy   = Security.USER_NAME;
									lst.Add(itm);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return lst.ToArray();
		}

		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetItemDefinition", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		[return: System.Xml.Serialization.XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetItemDefinition(string ItemPath)
		{
			InitServerInfoHeader();

			byte[] abyRDL = null;
			try
			{
				string Report = ItemPath;
				int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
				// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
				if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
					Report = Report.Substring(nVtiBin);
				if ( Report.StartsWith("/") )
					Report = Report.Substring(1);
				
				// 12/10/2009 Paul.  The report name will include the full URL. 
				// http://developer6/SplendidCRM6_Sugar/Bugs.rdl
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					if ( Report.StartsWith("vw") && Report.EndsWith(".rds") )
					{
						string sDataSet = Report.Substring(2, Report.Length - 6);
						sSQL = "select TABLE_NAME              " + ControlChars.CrLf
						     + "  from vwMODULES_Reporting     " + ControlChars.CrLf
						     + " where USER_ID    = @USER_ID   " + ControlChars.CrLf
						     + "   and TABLE_NAME = @TABLE_NAME" + ControlChars.CrLf
						     + "   and TABLE_NAME is not null  " + ControlChars.CrLf
						     + " order by TABLE_NAME           " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@USER_ID"   , Security.USER_ID);
							Sql.AddParameter(cmd, "@TABLE_NAME", sDataSet        );
							
							string sTABLE_NAME = Sql.ToString(cmd.ExecuteScalar());
							if ( !Sql.IsEmptyString(sTABLE_NAME) )
							{
								cmd.Parameters.Clear();
								string sVIEW_NAME = Sql.MetadataName(cmd, "vw" + sTABLE_NAME);
								RdsDocument rds = new RdsDocument(sVIEW_NAME);
								using ( DataTable dtColumns = new DataTable() )
								{
									sSQL = "select *                       " + ControlChars.CrLf
									     + "  from vwSqlColumns            " + ControlChars.CrLf
									     + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
									     + " order by colid                " + ControlChars.CrLf;
									
									cmd.CommandText = sSQL;
									// 02/20/2016 Paul.  Make sure to use upper case for Oracle. 
									Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, sVIEW_NAME));
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtColumns);
									}
									StringBuilder sbCommandText = new StringBuilder();
									XmlNode xFields = rds.SelectNode("DataSet/Fields");
									XmlNode xSelectedColumns = rds.SelectNode("DataSet/Query/rd:DesignerState/qd:QueryDefinition/qd:SelectedColumns");
									foreach ( DataRow rowColumn in dtColumns.Rows )
									{
										string sColumnName = Sql.ToString (rowColumn["ColumnName"]).ToUpper();
										string sColumnType = Sql.ToString (rowColumn["ColumnType"]);
										string sCsType     = Sql.ToString (rowColumn["CsType"    ]);
										string sFieldType  = String.Empty;
										switch ( sCsType )
										{
											case "Guid"      :  sFieldType = "System.Guid"    ;  break;
											case "string"    :  sFieldType = "System.String"  ;  break;
											case "ansistring":  sFieldType = "System.String"  ;  break;
											case "DateTime"  :  sFieldType = "System.DateTime";  break;
											case "bool"      :  sFieldType = "System.Boolean" ;  break;
											case "float"     :  sFieldType = "System.Double"  ;  break;
											case "decimal"   :  sFieldType = "System.Decimal" ;  break;
											case "short"     :  sFieldType = "System.Int16"   ;  break;
											case "Int32"     :  sFieldType = "System.Int32"   ;  break;
											case "Int64"     :  sFieldType = "System.Int64"   ;  break;
											default          :  sFieldType = "System.String"  ;  break;
										}

										if ( sbCommandText.Length == 0 )
											sbCommandText.AppendLine("select " + sVIEW_NAME + "." + sColumnName);
										else
											sbCommandText.AppendLine("     , " + sVIEW_NAME + "." + sColumnName);
										rds.AppendField           (xFields, sColumnName, sFieldType);
										rds.AppendColumnExpression(xSelectedColumns, sVIEW_NAME, sColumnName);
									}
									sbCommandText.AppendLine("  from " + sVIEW_NAME);
									rds.SetSingleNode("DataSet/Query/CommandText", sbCommandText.ToString());
								}
								//rds.Save("c:\\temp\\" + sVIEW_NAME + ".rds.xml");
								abyRDL = UTF8Encoding.UTF8.GetBytes(rds.OuterXml);
							}
						}
					}
					else
					{
						if ( Report.EndsWith(".rdl") )
							Report = Report.Substring(0, Report.Length - 4);
						// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
						Guid gReportID = Guid.Empty;
						try
						{
							if ( Report.Length == 36 )
								gReportID = Sql.ToGuid(Report);
						}
						catch
						{
						}
						sSQL = "select RDL           " + ControlChars.CrLf
						     + "  from vwCHARTS_Edit " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							if ( Sql.IsEmptyGuid(gReportID) )
							{
								cmd.CommandText += " where NAME = @NAME  " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@NAME", Report);
							}
							else
							{
								cmd.CommandText += " where ID = @ID    " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@ID", gReportID);
							}
							cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;
							
							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									string sRDL = Sql.ToString(rdr["RDL"]);
									// 02/12/2010 Paul.  Loading the RDL will remove the Name property from the Report tag, 
									// which causes an exception in Report Builder 2.0. 
									RdlDocument rdl = new RdlDocument();
									rdl.LoadRdl(sRDL);
									abyRDL = UTF8Encoding.UTF8.GetBytes(rdl.OuterXml);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return abyRDL;
		}

		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/CreateCatalogItem", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
		[return: System.Xml.Serialization.XmlElementAttribute("ItemInfo")]
		public CatalogItem CreateCatalogItem(string ItemType, string Name, string Parent, bool Overwrite, [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] Definition, Property[] Properties, out Warning[] Warnings)
		{
			InitServerInfoHeader();
			CatalogItem itm = new CatalogItem();
			try
			{
				Warnings = new Warning[0];
				if ( ItemType == "Report" )
				{
					string Report = Name;
					int nVtiBin = Context.Request.Url.AbsoluteUri.IndexOf("_vti_bin/");
					// 09/25/2010 Paul.  _vti_bin is not always part of the Uri. 
					if ( nVtiBin > 0 && Report.StartsWith(Context.Request.Url.AbsoluteUri.Substring(0, nVtiBin)) )
						Report = Report.Substring(nVtiBin);
					if ( Report.EndsWith(".rdl") )
						Report = Report.Substring(0, Report.Length - 4);
					// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
					Guid gReportID = Guid.Empty;
					try
					{
						if ( Report.StartsWith("/") )
							Report = Report.Substring(1);
						if ( Report.Length == 36 )
							gReportID = Sql.ToGuid(Report);
					}
					catch
					{
					}
					
					DbProviderFactory dbf = DbProviderFactories.GetFactory();
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						Guid   gID                 = Guid.Empty;
						Guid   gASSIGNED_USER_ID   = Security.USER_ID;
						// 06/17/2010 Paul.  Add support for Team Management. 
						Guid   gTEAM_ID            = Security.TEAM_ID;
						string sTEAM_SET_LIST      = String.Empty;
						// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
						string sASSIGNED_SET_LIST  = String.Empty;
						// 12/06/2009 Paul.  We may want to extract the view from the RDL and determine the module name. 
						string sMODULE_NAME        = String.Empty;
						Guid   gPRE_LOAD_EVENT_ID  = Guid.Empty;
						Guid   gPOST_LOAD_EVENT_ID = Guid.Empty;
						if ( Overwrite )
						{
							sSQL = "select *                    " + ControlChars.CrLf
							     + "  from vwCHARTS             " + ControlChars.CrLf;
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								cmd.CommandText = sSQL;
								// 06/17/2010 Paul.  Use new Security.Filter() function to apply Team and ACL security rules.
								Security.Filter(cmd, "Reports", "edit");
								if ( Sql.IsEmptyGuid(gReportID) )
								{
									cmd.CommandText += "   and NAME = @NAME  " + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@NAME", Report);
								}
								else
								{
									cmd.CommandText += "   and ID = @ID    " + ControlChars.CrLf;
									Sql.AddParameter(cmd, "@ID", gReportID);
								}
								cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;

								using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
								{
									if ( rdr.Read() )
									{
										gID                 = Sql.ToGuid  (rdr["ID"                ]);
										// 12/06/2009 Paul.  Preserve the old module name until we can extract from the RDL. 
										sMODULE_NAME        = Sql.ToString(rdr["MODULE_NAME"       ]);
										gTEAM_ID            = Sql.ToGuid  (rdr["TEAM_ID"           ]);
										sTEAM_SET_LIST      = Sql.ToString(rdr["TEAM_SET_LIST"     ]);
										// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
										sASSIGNED_SET_LIST  = Sql.ToString(rdr["ASSIGNED_SET_LIST" ]);
										// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
										gPRE_LOAD_EVENT_ID  = Sql.ToGuid  (rdr["PRE_LOAD_EVENT_ID" ]);
										gPOST_LOAD_EVENT_ID = Sql.ToGuid  (rdr["POST_LOAD_EVENT_ID"]);
									}
								}
							}
						}
						string sRDL = String.Empty;
						// 12/08/2009 Paul.  ReportBuilder 2.0 is saving the data with the Unicode Byte Order Mark. 
						// The XmlDocument.LoadXml() method does not understand this mark and throws an exception. 
						// http://www.filepie.us/?title=Byte_Order_Mark
						if ( Definition[0] == '\xEF' && Definition[1] == '\xBB' && Definition[2] == '\xBF' )  // EF BB BF
							sRDL = UTF8Encoding.UTF8.GetString(Definition, 3, Definition.Length - 3);
						else if ( Definition[0] == '\xFE' && Definition[1] == '\xFF' )
							sRDL = Encoding.BigEndianUnicode.GetString(Definition, 2, Definition.Length - 2);
						else if ( Definition[0] == '\xFF' && Definition[1] == '\xFE' )
							sRDL = Encoding.Unicode.GetString(Definition, 2, Definition.Length - 2);
						else
							sRDL = UTF8Encoding.UTF8.GetString(Definition);

						string sCHART_TYPE = "Bar";
						try
						{
							RdlDocument rdl = new RdlDocument();
							rdl.LoadRdl(sRDL);
							sCHART_TYPE = rdl.SelectNodeValue("Body/ReportItems/Chart/ChartData/ChartSeriesCollection/ChartSeries/Type");
							// 03/12/2012 Paul.  An early release used Columns and not Column and broke compatibility with MS Report Builder 3.0. 
							if ( sCHART_TYPE == "Columns" )
								sCHART_TYPE = "Column";
						}
						catch
						{
						}
						// 06/17/2010 Paul.  Add support for Team Management. 
						// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
						SqlProcs.spCHARTS_Update
							( ref gID
							, gASSIGNED_USER_ID
							, Report
							, sMODULE_NAME
							, sCHART_TYPE
							, sRDL
							, gTEAM_ID
							, sTEAM_SET_LIST
							, gPRE_LOAD_EVENT_ID
							, gPOST_LOAD_EVENT_ID
							// 05/17/2017 Paul.  Add Tags module. 
							, String.Empty      // TAG_SET_NAME
							// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
							, sASSIGNED_SET_LIST
							);
						// 04/06/2011 Paul.  Cache reports. 
						SplendidCache.ClearReport(gID);
						
						sSQL = "select *                    " + ControlChars.CrLf
						     + "  from vwCHARTS_Edit        " + ControlChars.CrLf
						     + " where ID = @ID             " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							Sql.AddParameter(cmd, "@ID", gID);

							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									itm.ID           = Sql.ToString(rdr["ID"]);
									itm.Name         = Sql.ToString(rdr["NAME"]);
									itm.Path         = "/" + Sql.ToString(rdr["NAME"]);
									itm.TypeName     = "Report";
									itm.Size         = Sql.ToString  (rdr["RDL"          ]).Length;
									itm.CreationDate = Sql.ToDateTime(rdr["DATE_ENTERED" ]);
									itm.ModifiedDate = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
									itm.CreatedBy    = Sql.ToString  (rdr["CREATED_BY"   ]);
									itm.ModifiedBy   = Sql.ToString  (rdr["MODIFIED_BY"  ]);
								}
							}
						}
					}
				}
				else if ( ItemType == "DataSet" )
				{
					itm.ID           = Guid.NewGuid().ToString();
					itm.Name         = Name;
					itm.Path         = "/" + Name;
					itm.TypeName     = "DataSet";
					itm.Size         = Definition.Length;
					itm.CreationDate = DateTime.Now;
					itm.ModifiedDate = DateTime.Now;
					itm.CreatedBy    = Security.USER_NAME;
					itm.ModifiedBy   = Security.USER_NAME;
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return itm;
		}

		//"http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetUserModel"
		[WebMethod(EnableSession=true)]
		[SoapHeader("TrustedUserHeaderValue", Direction=SoapHeaderDirection.In)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer/GetUserModel", RequestNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", ResponseNamespace="http://schemas.microsoft.com/sqlserver/reporting/2010/03/01/ReportServer", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetUserModel(string Model, string Perspective)
		{
			InitServerInfoHeader();
			string sSemanticModel = ReportService2005.GetUserModel(this.Context, Model, Perspective);
			return UTF8Encoding.UTF8.GetBytes(sSemanticModel);
		}

	}
}

