/**
 * Copyright (C) 2005-2011 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Caching;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SplendidCRM.ChartServer
{
	/// <summary>
	/// Summary description for ReportService2005
	/// </summary>
	[WebService(Namespace = "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Name="SplendidReportService2005", Description="SplendidCRM Report Service 2005.")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ReportService2005 : WebService
	{
		#region Enums
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ItemTypeEnum
		{
			Unknown,
			Folder,
			Report,
			Resource,
			LinkedReport,
			DataSource,
			Model,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum CredentialRetrievalEnum
		{
			Prompt,
			Store,
			Integrated,
			None,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ConditionEnum
		{
			Contains,
			Equals,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ItemNamespaceEnum
		{
			PathBased,
			GUIDBased,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum SensitivityEnum
		{
			True,
			False,
			Auto,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ParameterTypeEnum
		{
			Boolean,
			DateTime,
			Integer,
			Float,
			String,
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public enum ParameterStateEnum
		{
			HasValidValue,
			MissingValidValue,
			HasOutstandingDependencies,
			DynamicValuesUnavailable,
		}

		#endregion

		#region Structures
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ModelPerspective
		{
			public string ID;
			public string Name;
			public string Description;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ModelCatalogItem
		{
			public string Model;
			public string Description;
			public ModelPerspective[] Perspectives;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class CatalogItem
		{
			public string ID;
			public string Name;
			public string Path;
			public string VirtualPath;
			public ItemTypeEnum Type;
			public int Size;
			public string Description;
			public bool Hidden;
			public DateTime CreationDate;
			public DateTime ModifiedDate;
			public string CreatedBy;
			public string ModifiedBy;
			public string MimeType;
			public DateTime ExecutionDate;
		}

		[XmlIncludeAttribute(typeof(InvalidDataSourceReference))]
		[XmlIncludeAttribute(typeof(DataSourceReference))]
		[XmlIncludeAttribute(typeof(DataSourceDefinition))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSourceDefinitionOrReference
		{
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class InvalidDataSourceReference : DataSourceDefinitionOrReference
		{
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSourceReference : DataSourceDefinitionOrReference
		{
			public string Reference;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSourceDefinition : DataSourceDefinitionOrReference
		{
			public string Extension;
			public string ConnectString;
			public bool UseOriginalConnectString;
			public bool OriginalConnectStringExpressionBased;
			public CredentialRetrievalEnum CredentialRetrieval;
			public bool WindowsCredentials;
			public bool ImpersonateUser;
			public string Prompt;
			public string UserName;
			public string Password;
			public bool Enabled;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSource
		{
			public string Name;
			[XmlElementAttribute("DataSourceDefinition", typeof(DataSourceDefinition))]
			[XmlElementAttribute("DataSourceReference", typeof(DataSourceReference))]
			[XmlElementAttribute("InvalidDataSourceReference", typeof(InvalidDataSourceReference))]
			public DataSourceDefinitionOrReference Item;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class Warning
		{
			public string Code;
			public string Severity;
			public string ObjectName;
			public string ObjectType;
			public string Message;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class SearchCondition : Property
		{
			public ConditionEnum Condition;
		}

		[XmlIncludeAttribute(typeof(SearchCondition))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class Property
		{
			public string Name;
			public string Value;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public class ServerInfoHeader : SoapHeader
		{
			public string ReportServerVersionNumber;
			public string ReportServerEdition;
			public string ReportServerVersion;
			public string ReportServerDateTime;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public class BatchHeader : SoapHeader
		{
			public string BatchID;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		[XmlRootAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", IsNullable=false)]
		public class ItemNamespaceHeader : SoapHeader
		{
			public ItemNamespaceEnum ItemNamespace;
			[XmlAnyAttributeAttribute()]
			public System.Xml.XmlAttribute[] AnyAttr;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class QueryDefinition
		{
			public string CommandType;
			public string CommandText;
			public int    Timeout;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class Field
		{
			public string Alias;
			public string Name;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSetDefinition
		{
			public Field[]         Fields;
			public QueryDefinition Query;
			public SensitivityEnum CaseSensitivity;
			public string          Collation;
			public SensitivityEnum AccentSensitivity;
			public SensitivityEnum KanatypeSensitivity;
			public SensitivityEnum WidthSensitivity;
			public string          Name;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ReportParameter
		{
			public string             Name;
			public ParameterTypeEnum  Type;
			public bool               Nullable;
			public bool               AllowBlank;
			public bool               MultiValue;
			public bool               QueryParameter;
			public string             Prompt;
			public bool               PromptUser;
			[XmlArrayItemAttribute("Dependency")]
			public string[]           Dependencies;
			public bool               ValidValuesQueryBased;
			public ValidValue[]       ValidValues;
			public bool               DefaultValuesQueryBased;
			[XmlArrayItemAttribute("Value")]
			public string[]           DefaultValues;
			public ParameterStateEnum State;
			public string             ErrorMessage;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ValidValue
		{
			public string Label;
			public string Value;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ParameterFieldReference : ParameterValueOrFieldReference
		{
			public string ParameterName;
			public string FieldAlias;
		}

		[XmlIncludeAttribute(typeof(ParameterFieldReference))]
		[XmlIncludeAttribute(typeof(ParameterValue))]
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ParameterValueOrFieldReference
		{
		}

		/// <remarks/>
		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class ParameterValue : ParameterValueOrFieldReference
		{
			public string Name;
			public string Value;
			public string Label;
		}

		[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082")]
		[System.SerializableAttribute()]
		[XmlTypeAttribute(Namespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
		public class DataSourceCredentials
		{
			public string DataSourceName;
			public string UserName;
			public string Password;
		}

		#endregion

		#region Headers
		public ServerInfoHeader    ServerInfoHeaderValue;
		public BatchHeader         BatchHeaderValue;
		public ItemNamespaceHeader ItemNamespaceHeaderValue;

		private void InitServerInfoHeader()
		{
			ServerInfoHeaderValue = new ServerInfoHeader();
			// 12/07/2009 Paul.  In order for ReportBuilder 2.0 to connect, we need to return SQL Server 2008 information. 
			ServerInfoHeaderValue.ReportServerVersionNumber = "2007.100.1600.22";  //"2005.090.1399.00";
			ServerInfoHeaderValue.ReportServerEdition       = "Standard";
			ServerInfoHeaderValue.ReportServerVersion       = "Microsoft SQL Server Reporting Services Version 10.0.1600.22";  //"Microsoft SQL Server Reporting Services Version 9.00.1399.00";
			ServerInfoHeaderValue.ReportServerDateTime      = DateTime.Now.ToString("s"); // "2009-12-06T03:49:35";

			// 12/06/2009 Paul.  Since all web methods call this init method, it is a common place to validate the user's access. 
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));
		}
		#endregion

		#region Simple Methods
		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListSecureMethods"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListSecureMethods", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public string[] ListSecureMethods()
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any secure methods. 
			string[] arr = new string[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeader("ItemNamespaceHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetProperties(string Item, Property[] Properties)
		{
			InitServerInfoHeader();
			List<Property> lst = new List<Property>();
			if ( Item == "/SplendidCRM Model" )
			{
				foreach ( Property p in Properties )
				{
					switch ( p.Name )
					{
						case "Name":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "SplendidCRM";
							lst.Add(pReturn);
							break;
						}
						case "Description":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "SplendidCRM Reporting Services";
							lst.Add(pReturn);
							break;
						}
						case "ModifiedDate":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = DateTime.Now.ToString("s");
							lst.Add(pReturn);
							break;
						}
						case "MustUsePerspective":
						{
							Property pReturn = new Property();
							pReturn.Name  = p.Name;
							pReturn.Value = "false";
							lst.Add(pReturn);
							break;
						}
					}
				}
			}
			else if ( Properties == null )
			{
				// 12/10/2009 Paul.  If Properties is null, then assume we are retrieving the properties of a report. 
				Property pReturn = new Property();
				pReturn.Name  = "Name";
				pReturn.Value = Item;
				lst.Add(pReturn);
			}
			return lst.ToArray();
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetReportParameters"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetReportParameters", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Parameters")]
		public ReportParameter[] GetReportParameters(string Report, string HistoryID, bool ForRendering, ParameterValue[] Values, DataSourceCredentials[] Credentials)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/10/2009 Paul.  We need to pull the parameters out of the report RDL. 
			ReportParameter[] arr = new ReportParameter[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/SetReportParameters"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("BatchHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/SetReportParameters", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public void SetReportParameters(string Report, ReportParameter[] Parameters)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/10/2009 Paul.  We do not store the report parameters separately, so we will do nothing. 
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListModelPerspectives"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListModelPerspectives", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("ModelCatalogItems")]
		public ModelCatalogItem[] ListModelPerspectives(string Model)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM does not have any perspectives, so we can hard-code the result. 
			ModelCatalogItem[] arr = new ModelCatalogItem[1];
			arr[0] = new ModelCatalogItem();
			arr[0].Model = "/SplendidCRM Model";
			arr[0].Perspectives = new ModelPerspective[0];
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetItemType"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetItemType", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Type")]
		public ItemTypeEnum GetItemType(string Item)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  I'm not sure if the Item will always start with a slash. 
			// Instead of assuming that the item is a report, we may want to query the database for the item name. 
			switch ( Item )
			{
				case "/"                       :  return ItemTypeEnum.Folder    ;
				case "/SplendidCRM Model"      :  return ItemTypeEnum.Model     ;
				case "/SplendidCRM Data Source":  return ItemTypeEnum.DataSource;
			}
			return ItemTypeEnum.Report;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("DataSources")]
		public DataSource[] GetItemDataSources(string Item)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore the Item and hard-code the return value. 
			DataSourceReference dsr = new DataSourceReference();
			dsr.Reference = "/SplendidCRM Data Source";
			DataSource[] arr = new DataSource[1];
			arr[0] = new DataSource();
			arr[0].Name = "SplendidCRM Data Source";
			arr[0].Item = dsr;
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/SetItemDataSources"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeader("BatchHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/SetItemDataSources", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		public void SetItemDataSources(string Item, DataSource[] DataSources)
		{
			InitServerInfoHeader();
			// 12/06/2009 Paul.  SplendidCRM is the only data source, so we can ignore this call. 
		}

		// "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetSystemProperties"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetSystemProperties", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Values")]
		public Property[] GetSystemProperties(Property[] Properties)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/07/2009 Paul.  A default installation of SQL Server 2008 returns nothing. 
			// Typical input properties include:
			// DefaultDataSourceFolder = DataSources
			// DefaultModelFolder = Models
			Property[] arr = new Property[0];
			return arr;
		}

		// SOAPAction: "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetDataSourceContents"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetDataSourceContents", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition")]
		public DataSourceDefinition GetDataSourceContents(string DataSource)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			// 12/07/2009 Paul.  SplendidCRM is the only data source, so return static information.
			DataSourceDefinition ds = new DataSourceDefinition();
			// 01/21/2010 Paul.  Specify an Extension to fix the following ReportBuilder 2.0 error: 
			// The selected data extension  cannot be loaded. Verify that the selected data extension is correctly registered in RSReportDesigner.config, and is installed on the client for local reports and on the report server for published reports.
			ds.Extension           = "SQL";
			ds.ConnectString       = String.Empty;  // "data source=(local)\\SplendidCRM;initial catalog=SplendidCRM;";
			ds.CredentialRetrieval = CredentialRetrievalEnum.Prompt;
			ds.WindowsCredentials  = false;
			ds.ImpersonateUser     = false;
			ds.Prompt              = "Enter a user name and password to access the data source:";
			ds.UserName            = String.Empty;
			ds.Password            = String.Empty;
			ds.Enabled             = true;
			ds.UseOriginalConnectString = false;
			ds.OriginalConnectStringExpressionBased = false;
			
			// 01/21/2010 Paul.  Allow Integrated security, but default to Prompt. 
			if ( Sql.ToString(Application["CONFIG.ReportServer.CredentialRetrieval"]).ToLower() == "integrated" )
				ds.CredentialRetrieval = CredentialRetrievalEnum.Integrated;
			
			// 01/21/2010 Paul.  SplendidCRM Data Source must be enabled manually, for security reasons. 
			if ( Sql.ToBoolean(Application["CONFIG.ReportServer.DataSource"]) )
			{
				// 01/21/2010 Paul.  Lets try and return the correct datasource and catalog. 
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sDataSource     = String.Empty;
					string sInitialCatalog = String.Empty;
					string sUserName       = String.Empty;
					string sPassword       = String.Empty;
					string[] arrProperties = con.ConnectionString.Split(';');
					foreach ( string sProperty in arrProperties )
					{
						string[] arrNameValue = sProperty.Trim().Split('=');
						if ( arrNameValue.Length == 2 )
						{
							switch ( arrNameValue[0].ToLower() )
							{
								case "data source"    :  sDataSource     = arrNameValue[1];  break;
								case "initial catalog":  sInitialCatalog = arrNameValue[1];  break;
								case "user id"        :  sUserName       = arrNameValue[1];  break;
								case "password"       :  sPassword       = arrNameValue[1];  break;
							}
						}
					}
					ds.ConnectString  = "data source=" + sDataSource + ";initial catalog=" + sInitialCatalog + ";";
					if ( ds.CredentialRetrieval == CredentialRetrievalEnum.Integrated )
					{
						ds.UserName = sUserName;
						ds.Password = sPassword;
					}
				}
			}
			return ds;
		}
		#endregion

		#region Core Methods
		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListChildren"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/ListChildren", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("CatalogItems")]
		public CatalogItem[] ListChildren(string Item, bool Recursive)
		{
			InitServerInfoHeader();

			CatalogItem[] arr = null;
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select *             " + ControlChars.CrLf
					     + "  from vwCHARTS_Edit " + ControlChars.CrLf
					     + " order by NAME       " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(dt);
								arr = new CatalogItem[dt.Rows.Count + 2];
								CatalogItem itmDataSource = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM data source. 
								itmDataSource.ID           = "385e5bf1-fe2f-41b0-83e9-6ea2157e2499";
								itmDataSource.Name         = "SplendidCRM Data Source";
								itmDataSource.Path         = "/SplendidCRM Data Source";
								itmDataSource.Type         = ItemTypeEnum.DataSource;
								itmDataSource.CreationDate = DateTime.Now;
								itmDataSource.ModifiedDate = DateTime.Now;
								itmDataSource.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmDataSource.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								CatalogItem itmModel      = new CatalogItem();
								// 12/06/2009 Paul.  We are going to hard-code the ID of the SplendidCRM model. 
								itmModel.ID           = "ffe3817a-2578-44ce-9279-9d92c18fc913";
								itmModel.Name         = "SplendidCRM Model";
								itmModel.Path         = "/SplendidCRM Model";
								itmModel.Type         = ItemTypeEnum.Model;
								itmModel.Size         = 1;  // 12/06/2009 Paul.  We are going to hard-code the size until we are ready to retrieve the cached version. 
								itmModel.CreationDate = DateTime.Now;
								itmModel.ModifiedDate = DateTime.Now;
								itmModel.CreatedBy    = SplendidCRM.Security.USER_NAME;
								itmModel.ModifiedBy   = SplendidCRM.Security.USER_NAME;

								arr[0] = itmDataSource;
								arr[1] = itmModel;

								for ( int i=0; i < dt.Rows.Count; i++ )
								{
									CatalogItem itm = new CatalogItem();
									DataRow row = dt.Rows[i];
									itm.ID           = Sql.ToString(row["ID"]);
									itm.Name         = Sql.ToString(row["NAME"]);
									itm.Path         = "/" + Sql.ToString(row["NAME"]);
									itm.Type         = ItemTypeEnum.Report;
									itm.Size         = Sql.ToString  (row["RDL"          ]).Length;
									itm.CreationDate = Sql.ToDateTime(row["DATE_ENTERED" ]);
									itm.ModifiedDate = Sql.ToDateTime(row["DATE_MODIFIED"]);
									itm.CreatedBy    = Sql.ToString  (row["CREATED_BY"   ]);
									itm.ModifiedBy   = Sql.ToString  (row["MODIFIED_BY"  ]);
									arr[i + 2] = itm;
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return arr;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetReportDefinition"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetReportDefinition", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetReportDefinition(string Report)
		{
			InitServerInfoHeader();

			byte[] abyRDL = null;
			try
			{
				// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
				Guid gReportID = Guid.Empty;
				try
				{
					if ( Report.StartsWith("/") )
						Report = Report.Substring(1);
					if ( Report.Length == 36 )
						gReportID = Sql.ToGuid(Report);
				}
				catch
				{
				}
				
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select RDL           " + ControlChars.CrLf
					     + "  from vwCHARTS_Edit " + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;
						if ( Sql.IsEmptyGuid(gReportID) )
						{
							cmd.CommandText += " where NAME = @NAME  " + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@NAME", Report);
						}
						else
						{
							cmd.CommandText += " where ID = @ID    " + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", gReportID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								string sRDL = Sql.ToString(rdr["RDL"]);
								// 02/12/2010 Paul.  Loading the RDL will remove the Name property from the Report tag, 
								// which causes an exception in Report Builder 2.0. 
								RdlDocument rdl = new RdlDocument();
								rdl.LoadRdl(sRDL);
								abyRDL = UTF8Encoding.UTF8.GetBytes(rdl.OuterXml);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return abyRDL;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/CreateReport"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeader("BatchHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/CreateReport", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlArrayAttribute("Warnings")]
		public Warning[] CreateReport(string Report, string Parent, bool Overwrite, [XmlElementAttribute(DataType="base64Binary")] byte[] Definition, Property[] Properties)
		{
			InitServerInfoHeader();
			try
			{
				// 12/14/2009 Paul.  Report names are not unique, so allow a GUID to be specified. 
				Guid gReportID = Guid.Empty;
				try
				{
					if ( Report.StartsWith("/") )
						Report = Report.Substring(1);
					if ( Report.Length == 36 )
						gReportID = Sql.ToGuid(Report);
				}
				catch
				{
				}
				
				DbProviderFactory dbf = DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					Guid   gID                 = Guid.Empty;
					Guid   gASSIGNED_USER_ID   = Security.USER_ID;
					// 06/17/2010 Paul.  Add support for Team Management. 
					Guid   gTEAM_ID            = Security.TEAM_ID;
					string sTEAM_SET_LIST      = String.Empty;
					// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
					string sASSIGNED_SET_LIST  = String.Empty;
					// 12/06/2009 Paul.  We may want to extract the view from the RDL and determine the module name. 
					string sMODULE_NAME        = String.Empty;
					Guid   gPRE_LOAD_EVENT_ID  = Guid.Empty;
					Guid   gPOST_LOAD_EVENT_ID = Guid.Empty;
					if ( Overwrite )
					{
						string sSQL;
						sSQL = "select *                    " + ControlChars.CrLf
						     + "  from vwCHARTS             " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							// 06/17/2010 Paul.  Use new Security.Filter() function to apply Team and ACL security rules.
							Security.Filter(cmd, "Reports", "edit");
							if ( Sql.IsEmptyGuid(gReportID) )
							{
								cmd.CommandText += "   and NAME = @NAME  " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@NAME", Report);
							}
							else
							{
								cmd.CommandText += "   and ID = @ID    " + ControlChars.CrLf;
								Sql.AddParameter(cmd, "@ID", gReportID);
							}
							cmd.CommandText += " order by DATE_MODIFIED desc" + ControlChars.CrLf;

							using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
							{
								if ( rdr.Read() )
								{
									gID                 = Sql.ToGuid  (rdr["ID"                ]);
									// 12/06/2009 Paul.  Preserve the old module name until we can extract from the RDL. 
									sMODULE_NAME        = Sql.ToString(rdr["MODULE_NAME"       ]);
									gTEAM_ID            = Sql.ToGuid  (rdr["TEAM_ID"           ]);
									sTEAM_SET_LIST      = Sql.ToString(rdr["TEAM_SET_LIST"     ]);
									// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
									sASSIGNED_SET_LIST  = Sql.ToString(rdr["ASSIGNED_SET_LIST" ]);
									// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
									gPRE_LOAD_EVENT_ID  = Sql.ToGuid  (rdr["PRE_LOAD_EVENT_ID" ]);
									gPOST_LOAD_EVENT_ID = Sql.ToGuid  (rdr["POST_LOAD_EVENT_ID"]);
								}
							}
						}
					}
					string sRDL = String.Empty;
					// 12/08/2009 Paul.  ReportBuilder 2.0 is saving the data with the Unicode Byte Order Mark. 
					// The XmlDocument.LoadXml() method does not understand this mark and throws an exception. 
					// http://www.filepie.us/?title=Byte_Order_Mark
					if ( Definition[0] == '\xEF' && Definition[1] == '\xBB' && Definition[2] == '\xBF' )  // EF BB BF
						sRDL = UTF8Encoding.UTF8.GetString(Definition, 3, Definition.Length - 3);
					else if ( Definition[0] == '\xFE' && Definition[1] == '\xFF' )
						sRDL = Encoding.BigEndianUnicode.GetString(Definition, 2, Definition.Length - 2);
					else if ( Definition[0] == '\xFF' && Definition[1] == '\xFE' )
						sRDL = Encoding.Unicode.GetString(Definition, 2, Definition.Length - 2);
					else
						sRDL = UTF8Encoding.UTF8.GetString(Definition);

					string sCHART_TYPE = "Bar";
					try
					{
						RdlDocument rdl = new RdlDocument();
						rdl.LoadRdl(sRDL);
						sCHART_TYPE = rdl.SelectNodeValue("Body/ReportItems/Chart/ChartData/ChartSeriesCollection/ChartSeries/Type");
						// 03/12/2012 Paul.  An early release used Columns and not Column and broke compatibility with MS Report Builder 3.0. 
						if ( sCHART_TYPE == "Columns" )
							sCHART_TYPE = "Column";
					}
					catch
					{
					}
					// 06/17/2010 Paul.  Add support for Team Management. 
					// 12/04/2010 Paul.  Add support for Business Rules Framework to Reports. 
					SqlProcs.spCHARTS_Update
						( ref gID
						, gASSIGNED_USER_ID
						, Report
						, sMODULE_NAME
						, sCHART_TYPE
						, sRDL
						, gTEAM_ID
						, sTEAM_SET_LIST
						, gPRE_LOAD_EVENT_ID
						, gPOST_LOAD_EVENT_ID
						// 05/17/2017 Paul.  Add Tags module. 
						, String.Empty      // TAG_SET_NAME
						// 11/30/2017 Paul.  Add ASSIGNED_SET_ID for Dynamic User Assignment. 
						, sASSIGNED_SET_LIST
						);
					// 04/06/2011 Paul.  Cache reports. 
					SplendidCache.ClearReport(gID);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
			return null;
		}
		#endregion

		#region GetUserModel
		// 12/06/2009 Paul.  All IDs start with a G. 
		private static string ModelID(Guid gID)
		{
			return "G" + gID.ToString();
		}

		private static Guid ModelGuid(string sID)
		{
			if ( sID != null && sID.Length == 37 && sID.StartsWith("G") )
				return new Guid(sID.Substring(1));
			return Guid.Empty;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetUserModel"
		[WebMethod(EnableSession=true)]
		[SoapHeader("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/GetUserModel", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("Definition", DataType="base64Binary")]
		public byte[] GetUserModel(string Model, string Perspective)
		{
			InitServerInfoHeader();
			string sSemanticModel = GetUserModel(this.Context, Model, Perspective);
			return UTF8Encoding.UTF8.GetBytes(sSemanticModel);
		}

		#region GetUserModel Prototype
		public static string GetUserModel_Prototype(HttpContext Context, string sModel, string sPerspective)
		{
			// 12/06/2009 Paul.  SplendidCRM only has one model and no perspectives, so we can ignore the inputs. 
			Guid gUSER_ID = Security.USER_ID;
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));

			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			xml.LoadXml("<SemanticModel ID=\"Gcc05a305-28ac-4f3f-aa38-d4fec0610f7b\" xmlns=\"http://schemas.microsoft.com/sqlserver/2004/10/semanticmodeling\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" />");
			
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
			string sDefaultNamespace = "http://schemas.microsoft.com/sqlserver/2004/10/semanticmodeling";
			nsmgr.AddNamespace(""         , sDefaultNamespace);
			nsmgr.AddNamespace("defaultns", sDefaultNamespace);
			nsmgr.AddNamespace("xsd"      , "http://www.w3.org/2001/XMLSchema");
			nsmgr.AddNamespace("xsi"      , "http://www.w3.org/2001/XMLSchema-instance");
			try
			{
				XmlUtil.SetSingleNode(xml, xml.DocumentElement, "Culture", "en-US", nsmgr, sDefaultNamespace);
				
				XmlElement xEntities = xml.CreateElement("Entities", sDefaultNamespace);
				xml.DocumentElement.AppendChild(xEntities);
				DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					using ( DataTable dtModules = new DataTable() )
					{
						// 12/06/2009 Paul.  We need the ID and TABLE_NAME when generating the SemanticModel for the ReportBuilder. 
						sSQL = "select ID                      " + ControlChars.CrLf
						     + "     , TABLE_NAME              " + ControlChars.CrLf
						     + "     , MODULE_NAME             " + ControlChars.CrLf
						     + "     , DISPLAY_NAME            " + ControlChars.CrLf
						     + "  from vwMODULES_Reporting     " + ControlChars.CrLf
						     + " where USER_ID    = @USER_ID   " + ControlChars.CrLf
						     + "   and TABLE_NAME is not null  " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
#if false
							cmd.CommandText += "   and MODULE_NAME in ('Accounts', 'Contacts', 'Contracts')" + ControlChars.CrLf;
#endif
							cmd.CommandText += " order by TABLE_NAME" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@USER_ID", gUSER_ID);
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								da.Fill(dtModules);
								// 12/07/2009 Paul.  Using DISPLAY_NAME does not look right when the core modules are all table names. 
								/*
								foreach(DataRow row in dtModules.Rows)
								{
									row["DISPLAY_NAME"] = L10N.Term(this.Application, "en-US", Sql.ToString(row["DISPLAY_NAME"]));
								}
								*/
							}
						}
						foreach ( DataRow rowModule in dtModules.Rows )
						{
							Guid   gMODULE_ID    = Sql.ToGuid  (rowModule["ID"          ]);
							string sMODULE_NAME  = Sql.ToString(rowModule["MODULE_NAME" ]);
							string sTABLE_NAME   = Sql.ToString(rowModule["TABLE_NAME"  ]).ToUpper();
							string sDISPLAY_NAME = Sql.ToString(rowModule["DISPLAY_NAME"]);
							
							XmlElement xEntity = xml.CreateElement("Entity", sDefaultNamespace);
							xEntities.AppendChild(xEntity);
							
							XmlAttribute xID = xml.CreateAttribute("ID");
							xID.Value = ModelID(gMODULE_ID);
							xEntity.Attributes.SetNamedItem(xID);
							
							XmlElement xName                       = xml.CreateElement("Name"                      , sDefaultNamespace);
							XmlElement xCollectionName             = xml.CreateElement("CollectionName"            , sDefaultNamespace);
							XmlElement xIdentifyingAttributes      = xml.CreateElement("IdentifyingAttributes"     , sDefaultNamespace);
							XmlElement xDefaultDetailAttributes    = xml.CreateElement("DefaultDetailAttributes"   , sDefaultNamespace);
							XmlElement xDefaultAggregateAttributes = xml.CreateElement("DefaultAggregateAttributes", sDefaultNamespace);
							XmlElement xInstanceSelection          = xml.CreateElement("InstanceSelection"         , sDefaultNamespace);
							XmlElement xFields                     = xml.CreateElement("Fields"                    , sDefaultNamespace);
							xEntity.AppendChild(xName                      );
							xEntity.AppendChild(xCollectionName            );
							xEntity.AppendChild(xIdentifyingAttributes     );
							xEntity.AppendChild(xDefaultDetailAttributes   );
							xEntity.AppendChild(xDefaultAggregateAttributes);
							xEntity.AppendChild(xInstanceSelection         );
							xEntity.AppendChild(xFields                    );
							xName             .InnerText = sTABLE_NAME;
							// 12/07/2009 Paul.  Using DISPLAY_NAME does not look right when the core modules are all table names. 
							//xCollectionName   .InnerText = sDISPLAY_NAME;
							xCollectionName   .InnerText = sTABLE_NAME;
							xInstanceSelection.InnerText = "List";
							
							// 12/06/2009 Paul.  Add Count of records. 
							Guid gAttributeID = Guid.NewGuid();
							XmlElement xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
							xFields.AppendChild(xAttribute);
							xID = xml.CreateAttribute("ID");
							xID.Value = ModelID(gAttributeID);
							xAttribute.Attributes.SetNamedItem(xID);

							XmlUtil.AppendNode   (xml, xDefaultAggregateAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "Name"         , "# " + sMODULE_NAME       , nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "DataType"     , "Integer"                 , nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/FunctionName", "Count", nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/Arguments/Expression/EntityRef/EntityID", ModelID(gMODULE_ID), nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "IsAggregate"  , "true"                    , nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection", "Descending"              , nsmgr, sDefaultNamespace);
							XmlUtil.SetSingleNode(xml, xAttribute, "Format"       , "n0"                      , nsmgr, sDefaultNamespace);

							using ( DataTable dtColumns = new DataTable() )
							{
								sSQL = "select *                       " + ControlChars.CrLf
								     + "  from vwSqlColumns            " + ControlChars.CrLf
								     + " where ObjectName = @OBJECTNAME" + ControlChars.CrLf
								     + " order by colid                " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@OBJECTNAME", Sql.MetadataName(cmd, "vw" + sTABLE_NAME));
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtColumns);
									}
								}

								foreach ( DataRow rowColumn in dtColumns.Rows )
								{
									string sColumnName = Sql.ToString (rowColumn["ColumnName"]).ToUpper();
									string sColumnType = Sql.ToString (rowColumn["ColumnType"]);
									string sCsType     = Sql.ToString (rowColumn["CsType"    ]);
									int    nLength     = Sql.ToInteger(rowColumn["length"    ]);
									bool   bIsNullable = Sql.ToBoolean(rowColumn["IsNullable"]);

									gAttributeID = Guid.NewGuid();
									xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
									xFields.AppendChild(xAttribute);
									XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "ID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);

									XmlUtil.SetSingleNode(xml, xAttribute, "Name", sColumnName, nsmgr, sDefaultNamespace);
									if ( bIsNullable )
									{
										XmlUtil.SetSingleNode(xml, xAttribute, "Nullable", "true", nsmgr, sDefaultNamespace);
									}
									if ( String.Compare(sColumnName, "ID", true) == 0 )
									{
										XmlUtil.AppendNode   (xml, xIdentifyingAttributes  , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										XmlUtil.AppendNode   (xml, xDefaultDetailAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttribute              , "ValueSelection"                , "List"                 , nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttribute              , "ContextualName"                , "Merge"                , nsmgr, sDefaultNamespace);
									}
									if ( String.Compare(sColumnName, "NAME", true) == 0 )
									{
										XmlUtil.AppendNode(   xml, xIdentifyingAttributes  , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										XmlUtil.AppendNode   (xml, xDefaultDetailAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttribute              , "ValueSelection"                , "List"                 , nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttribute              , "ContextualName"                , "Role"                 , nsmgr, sDefaultNamespace);
									}
									
									XmlElement xVariations = null;
									XmlElement xVariation  = null;
									Guid gVariationID = Guid.Empty;
									switch ( sCsType )
									{
										case "Guid"    :
											//XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Guid"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "String"          , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "36"              , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "DiscourageGrouping", "true"            , nsmgr, sDefaultNamespace);
											break;
										case "short"   :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "n0"              , nsmgr, sDefaultNamespace);
											break;
										case "Int32"   :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "3"               , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "g"               , nsmgr, sDefaultNamespace);
											
											xVariations = xml.CreateElement("Variations", sDefaultNamespace);
											xAttribute.AppendChild(xVariations);
											
											// Total
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Total " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Sum", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											// Total is the default aggregation. 
											XmlUtil.SetSingleNode(xml, xAttribute, "DefaultAggregateAttributeID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											
											// Avg
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Avg " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Avg", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Min
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Min " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Min", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Max
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Max " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Max", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											break;
										case "Int64"   :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "n0"              , nsmgr, sDefaultNamespace);
											break;
										case "float"   :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Float"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "7"               , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "f3"              , nsmgr, sDefaultNamespace);
											
											xVariations = xml.CreateElement("Variations", sDefaultNamespace);
											xAttribute.AppendChild(xVariations);
											
											// Total
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Total " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Float"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Sum", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											// Total is the default aggregation. 
											XmlUtil.SetSingleNode(xml, xAttribute, "DefaultAggregateAttributeID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											
											// Avg
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Avg " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Float"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Avg", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Min
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Min " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Float"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Min", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Max
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Max " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Float"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Max", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											break;
										case "decimal" :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Decimal"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "8"               , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "g0"              , nsmgr, sDefaultNamespace);
											
											xVariations = xml.CreateElement("Variations", sDefaultNamespace);
											xAttribute.AppendChild(xVariations);
											
											// Total
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Total " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Decimal"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Sum", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											// Total is the default aggregation. 
											XmlUtil.SetSingleNode(xml, xAttribute, "DefaultAggregateAttributeID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											
											// Avg
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Avg " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Decimal"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Avg", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Min
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Min " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Decimal"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Min", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// Max
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Max " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Decimal"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Max", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											
											// 12/06/2009 Paul.  Decimal is always money. 
											XmlUtil.AppendNode(xml, xDefaultDetailAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											break;
										case "bool"    :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Boolean"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											break;
										case "DateTime":
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "DateTime"        , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Format"            , "d"               , nsmgr, sDefaultNamespace);
											
											xVariations = xml.CreateElement("Variations", sDefaultNamespace);
											xAttribute.AppendChild(xVariations);
											
											// Day
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , sColumnName + " Day", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Day", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											
											// Month
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , sColumnName + " Month", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Month", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											
											// Year
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , sColumnName + " Year", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Year", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Format"            , "0000"            , nsmgr, sDefaultNamespace);
											
											// Quarter
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , sColumnName + " Quarter", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "Integer"         , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Quarter", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Format"            , "Q0"              , nsmgr, sDefaultNamespace);
											
											// First
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "First " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "DateTime"        , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Min", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Format"            , "d"               , nsmgr, sDefaultNamespace);
											
											// Last
											gVariationID = Guid.NewGuid();
											xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Name"              , "Last " + sColumnName, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"          , "DateTime"        , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Nullable"          , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName", "Max", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"       , "true"            , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"     , "Descending"      , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Format"            , "d"               , nsmgr, sDefaultNamespace);
											
											break;
										case "enum"    :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "String"          , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , nLength.ToString(), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "ValueSelection"    , "Dropdown"        , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											break;
										case "byte[]"  :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Image"           , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , nLength.ToString(), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "DiscourageGrouping", "true"            , nsmgr, sDefaultNamespace);
											break;
										default        :
											XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "String"          , nsmgr, sDefaultNamespace);
											if ( nLength > 0 && nLength < 40 )
												XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , nLength.ToString(), nsmgr, sDefaultNamespace);
											// 12/07/2009 Paul.  Skip the width if it is too long. The builder will use a default width. 
											//else
											//	XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "40", nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
											if ( String.Compare(sColumnName, "NAME", true) == 0 || sCsType == "text" || sCsType == "ntext" || sCsType == "nvarchar(max)" )
											{
												XmlUtil.SetSingleNode(xml, xAttribute, "DiscourageGrouping", "true"            , nsmgr, sDefaultNamespace);
											}
											break;
									}
								}
							}
						}
					}
					using ( DataTable dtRelationships = new DataTable() )
					{
						sSQL = "select ID                            " + ControlChars.CrLf
						     + "     , LHS_MODULE                    " + ControlChars.CrLf
						     + "     , LHS_TABLE                     " + ControlChars.CrLf
						     + "     , LHS_KEY                       " + ControlChars.CrLf
						     + "     , RHS_MODULE                    " + ControlChars.CrLf
						     + "     , RHS_TABLE                     " + ControlChars.CrLf
						     + "     , RHS_KEY                       " + ControlChars.CrLf
						     + "     , JOIN_TABLE                    " + ControlChars.CrLf
						     + "     , JOIN_KEY_LHS                  " + ControlChars.CrLf
						     + "     , JOIN_KEY_RHS                  " + ControlChars.CrLf
						     + "     , RELATIONSHIP_TYPE             " + ControlChars.CrLf
						     + "     , RELATIONSHIP_ROLE_COLUMN      " + ControlChars.CrLf
						     + "     , RELATIONSHIP_ROLE_COLUMN_VALUE" + ControlChars.CrLf
						     + "  from vwRELATIONSHIPS_Reporting     " + ControlChars.CrLf
						     + " where LHS_MODULE in (select MODULE_NAME from vwMODULES_Reporting where USER_ID = @USER_ID1 and TABLE_NAME is not null)" + ControlChars.CrLf
						     + "   and RHS_MODULE in (select MODULE_NAME from vwMODULES_Reporting where USER_ID = @USER_ID2 and TABLE_NAME is not null)" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
#if false
							cmd.CommandText += "   and (JOIN_TABLE = 'accounts_contacts' or RELATIONSHIP_NAME = 'account_contracts')" + ControlChars.CrLf;
#endif
							cmd.CommandText += " order by LHS_MODULE, RHS_MODULE" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@USER_ID1", gUSER_ID);
							Sql.AddParameter(cmd, "@USER_ID2", gUSER_ID);
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								da.Fill(dtRelationships);
							}
						}
						foreach ( DataRow rowRel in dtRelationships.Rows )
						{
							Guid   gJOIN_ID                        = Sql.ToGuid  (rowRel["ID"                            ]);
							string sLHS_MODULE                     = Sql.ToString(rowRel["LHS_MODULE"                    ]);
							string sLHS_TABLE                      = Sql.ToString(rowRel["LHS_TABLE"                     ]).ToUpper();
							string sLHS_KEY                        = Sql.ToString(rowRel["LHS_KEY"                       ]).ToUpper();
							string sRHS_MODULE                     = Sql.ToString(rowRel["RHS_MODULE"                    ]);
							string sRHS_TABLE                      = Sql.ToString(rowRel["RHS_TABLE"                     ]).ToUpper();
							string sRHS_KEY                        = Sql.ToString(rowRel["RHS_KEY"                       ]).ToUpper();
							string sJOIN_TABLE                     = Sql.ToString(rowRel["JOIN_TABLE"                    ]).ToUpper();
							string sJOIN_KEY_LHS                   = Sql.ToString(rowRel["JOIN_KEY_LHS"                  ]).ToUpper();
							string sJOIN_KEY_RHS                   = Sql.ToString(rowRel["JOIN_KEY_RHS"                  ]).ToUpper();
							string sRELATIONSHIP_ROLE_COLUMN_VALUE = Sql.ToString(rowRel["RELATIONSHIP_ROLE_COLUMN_VALUE"]);
							Guid gLHS_ID = Guid.NewGuid();
							Guid gRHS_ID = Guid.NewGuid();
							XmlNode xLHS_Fields = xml.DocumentElement.SelectSingleNode("defaultns:Entities/defaultns:Entity[defaultns:Name=\'" + sLHS_TABLE + "\']/defaultns:Fields", nsmgr);
							XmlNode xRHS_Fields = xml.DocumentElement.SelectSingleNode("defaultns:Entities/defaultns:Entity[defaultns:Name=\'" + sRHS_TABLE + "\']/defaultns:Fields", nsmgr);
							if ( Sql.IsEmptyString(sJOIN_TABLE) )
							{
								if ( xLHS_Fields != null && xRHS_Fields != null && sLHS_TABLE != sRHS_TABLE )
								{
									XmlElement xLHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xLHS_Fields.AppendChild(xLHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xLHS_Role, "ID"  , ModelID(gLHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xLHS_Role, "RelatedRoleID", ModelID(gRHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xLHS_Role, "Cardinality"  , (sLHS_KEY == "ID" ? "OptionalMany" : "OptionalOne"), nsmgr, sDefaultNamespace);
									if ( sLHS_KEY != "ID" )
										XmlUtil.SetSingleNode(xml, xLHS_Role, "Name"         , sLHS_KEY + (Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) ? "" : " " + sRELATIONSHIP_ROLE_COLUMN_VALUE), nsmgr, sDefaultNamespace);
									
									XmlElement xRHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xRHS_Fields.AppendChild(xRHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xRHS_Role, "ID"  , ModelID(gRHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xRHS_Role, "RelatedRoleID", ModelID(gLHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xRHS_Role, "Cardinality"  , (sRHS_KEY == "ID" ? "OptionalMany" : "OptionalOne"), nsmgr, sDefaultNamespace);
									if ( sRHS_KEY != "ID" )
										XmlUtil.SetSingleNode(xml, xRHS_Role, "Name"         , sRHS_KEY + (Sql.IsEmptyString(sRELATIONSHIP_ROLE_COLUMN_VALUE) ? "" : " " + sRELATIONSHIP_ROLE_COLUMN_VALUE), nsmgr, sDefaultNamespace);
								}
							}
							else
							{
								XmlNode xJOIN_Fields = xml.DocumentElement.SelectSingleNode("defaultns:Entities/defaultns:Entity[defaultns:Name=\'" + sJOIN_TABLE + "\']/defaultns:Fields", nsmgr);
								if ( xJOIN_Fields == null )
								{
									XmlElement xEntity = xml.CreateElement("Entity", sDefaultNamespace);
									xEntities.AppendChild(xEntity);
									
									XmlAttribute xID = xml.CreateAttribute("ID");
									xID.Value = ModelID(gJOIN_ID);
									xEntity.Attributes.SetNamedItem(xID);
									
									XmlElement xName                       = xml.CreateElement("Name"                      , sDefaultNamespace);
									XmlElement xCollectionName             = xml.CreateElement("CollectionName"            , sDefaultNamespace);
									XmlElement xIdentifyingAttributes      = xml.CreateElement("IdentifyingAttributes"     , sDefaultNamespace);
									XmlElement xDefaultDetailAttributes    = xml.CreateElement("DefaultDetailAttributes"   , sDefaultNamespace);
									XmlElement xDefaultAggregateAttributes = xml.CreateElement("DefaultAggregateAttributes", sDefaultNamespace);
									XmlElement xInstanceSelection          = xml.CreateElement("InstanceSelection"         , sDefaultNamespace);
									XmlElement xFields                     = xml.CreateElement("Fields"                    , sDefaultNamespace);
									xEntity.AppendChild(xName                      );
									xEntity.AppendChild(xCollectionName            );
									xEntity.AppendChild(xIdentifyingAttributes     );
									xEntity.AppendChild(xDefaultDetailAttributes   );
									xEntity.AppendChild(xDefaultAggregateAttributes);
									xEntity.AppendChild(xInstanceSelection         );
									xEntity.AppendChild(xFields                    );
									xName             .InnerText = sJOIN_TABLE;
									xCollectionName   .InnerText = sJOIN_TABLE;
									xInstanceSelection.InnerText = "FilteredList";
									
									Guid gAttributeID = Guid.Empty;
									XmlElement xAttribute = null;
									// 12/06/2009 Paul.  Add Count of records. 
									gAttributeID = Guid.NewGuid();
									xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
									xFields.AppendChild(xAttribute);
									xID = xml.CreateAttribute("ID");
									xID.Value = ModelID(gAttributeID);
									xAttribute.Attributes.SetNamedItem(xID);

									// 12/06/2009 Paul.  The Aggregate Count is required, otherwise the model will not load. 
									XmlUtil.AppendNode   (xml, xDefaultAggregateAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Name"         , "# " + sJOIN_TABLE        , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "DataType"     , "Integer"                 , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/FunctionName", "Count", nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/Arguments/Expression/EntityRef/EntityID", ModelID(gJOIN_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "IsAggregate"  , "true"                    , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection", "Descending"              , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Format"       , "n0"                      , nsmgr, sDefaultNamespace);
									
									// 12/06/2009 Paul.  Add the join keys. 
									gAttributeID = Guid.NewGuid();
									xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
									xFields.AppendChild(xAttribute);
									XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "ID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									
									// 12/06/2009 Paul.  DefaultDetailAttributes are required. 
									XmlUtil.SetSingleNode(xml, xAttribute, "Name", sJOIN_KEY_LHS, nsmgr, sDefaultNamespace);
									XmlUtil.AppendNode   (xml, xIdentifyingAttributes  , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									XmlUtil.AppendNode   (xml, xDefaultDetailAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									
									//XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Guid"            , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "String"          , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "36"              , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "DiscourageGrouping", "true"            , nsmgr, sDefaultNamespace);
									
									gAttributeID = Guid.NewGuid();
									xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
									xFields.AppendChild(xAttribute);
									XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "ID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									
									XmlUtil.SetSingleNode(xml, xAttribute, "Name", sJOIN_KEY_RHS, nsmgr, sDefaultNamespace);
									XmlUtil.AppendNode   (xml, xIdentifyingAttributes  , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									XmlUtil.AppendNode   (xml, xDefaultDetailAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
									
									//XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "Guid"            , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "DataType"          , "String"          , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "Width"             , "36"              , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "SortDirection"     , "Ascending"       , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xAttribute, "DiscourageGrouping", "true"            , nsmgr, sDefaultNamespace);
									
									xJOIN_Fields = xFields;
								}
								if ( xJOIN_Fields != null && xLHS_Fields != null && xRHS_Fields != null )
								{
									Guid gJOIN_LHS_ID = Guid.NewGuid();
									XmlElement xLHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xLHS_Fields.AppendChild(xLHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xLHS_Role, "ID"  , ModelID(gLHS_ID     ), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xLHS_Role, "RelatedRoleID", ModelID(gJOIN_LHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xLHS_Role, "Cardinality"  , "OptionalMany"       , nsmgr, sDefaultNamespace);
									
									XmlElement xJOIN_LHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xJOIN_Fields.AppendChild(xJOIN_LHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xJOIN_LHS_Role, "ID"  , ModelID(gJOIN_LHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_LHS_Role, "Name"         , sJOIN_KEY_LHS        , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_LHS_Role, "RelatedRoleID", ModelID(gLHS_ID     ), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_LHS_Role, "Cardinality"  , "One"                , nsmgr, sDefaultNamespace);
									
									Guid gJOIN_RHS_ID = Guid.NewGuid();
									XmlElement xRHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xRHS_Fields.AppendChild(xRHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xRHS_Role, "ID"  , ModelID(gRHS_ID     ), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xRHS_Role, "RelatedRoleID", ModelID(gJOIN_RHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xRHS_Role, "Cardinality"  , "OptionalMany"       , nsmgr, sDefaultNamespace);
									
									XmlElement xJOIN_RHS_Role = xml.CreateElement("Role", sDefaultNamespace);
									xJOIN_Fields.AppendChild(xJOIN_RHS_Role);
									XmlUtil.SetSingleNodeAttribute(xml, xJOIN_RHS_Role, "ID"  , ModelID(gJOIN_RHS_ID), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_RHS_Role, "Name"         , sJOIN_KEY_RHS        , nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_RHS_Role, "RelatedRoleID", ModelID(gRHS_ID     ), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode(xml, xJOIN_RHS_Role, "Cardinality"  , "One"                , nsmgr, sDefaultNamespace);
									/*
									// 12/06/2009 Paul.  Can't seem to get this to work.  ReportBuilder keeps crashing.
									// This may not be necessary.  The ReportBuilder seems to join the relationship table properly without it. 
									XmlNode xJOIN_IdentifyingAttributes = xJOIN_Fields.ParentNode.SelectSingleNode("defaultns:IdentifyingAttributes", nsmgr);
									if ( xJOIN_IdentifyingAttributes != null )
									{
										XmlElement xAttributeReference = xml.CreateElement("AttributeReference", sDefaultNamespace);
										xJOIN_IdentifyingAttributes.AppendChild(xAttributeReference);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "Path/RolePathItem/RoleID", ModelID(gJOIN_LHS_ID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "AttributeID"             , xLHS_Fields.ParentNode.Attributes.GetNamedItem("ID").Value, nsmgr, sDefaultNamespace);
										
										xAttributeReference = xml.CreateElement("AttributeReference", sDefaultNamespace);
										xJOIN_IdentifyingAttributes.AppendChild(xAttributeReference);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "Path/RolePathItem/RoleID", ModelID(gJOIN_RHS_ID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "AttributeID"             , xRHS_Fields.ParentNode.Attributes.GetNamedItem("ID").Value, nsmgr, sDefaultNamespace);
									}
									
									XmlNode xJOIN_DefaultDetailAttributes = xJOIN_Fields.ParentNode.SelectSingleNode("defaultns:DefaultDetailAttributes", nsmgr);
									if ( xJOIN_DefaultDetailAttributes != null )
									{
										XmlElement xAttributeReference = xml.CreateElement("AttributeReference", sDefaultNamespace);
										xJOIN_DefaultDetailAttributes.AppendChild(xAttributeReference);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "Path/RolePathItem/RoleID", ModelID(gJOIN_LHS_ID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "AttributeID"             , xLHS_Fields.ParentNode.Attributes.GetNamedItem("ID").Value, nsmgr, sDefaultNamespace);
										
										xAttributeReference = xml.CreateElement("AttributeReference", sDefaultNamespace);
										xJOIN_DefaultDetailAttributes.AppendChild(xAttributeReference);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "Path/RolePathItem/RoleID", ModelID(gJOIN_RHS_ID), nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttributeReference, "AttributeID"             , xRHS_Fields.ParentNode.Attributes.GetNamedItem("ID").Value, nsmgr, sDefaultNamespace);
									}
									*/
								}
							}
						}
					}
				}
				
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
#if DEBUG
			xml.Save(Context.Server.MapPath("~/SemanticModel LAST.xml"));
#endif
			return xml.OuterXml;
		}
		#endregion

		public static DataTable SemanticModelTables(HttpContext Context)
		{
			System.Web.Caching.Cache Cache = HttpRuntime.Cache;
			DataTable dt = Cache.Get("vwSEMANTIC_MODEL_TABLES") as DataTable;
			if ( dt == null )
			{
				try
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						sSQL = "select ID                     " + ControlChars.CrLf
						     + "     , NAME                   " + ControlChars.CrLf
						     + "     , INSTANCE_SELECTION     " + ControlChars.CrLf
						     + "  from vwSEMANTIC_MODEL_TABLES" + ControlChars.CrLf
						     + " order by NAME                " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dt = new DataTable();
								da.Fill(dt);
								Cache.Insert("vwSEMANTIC_MODEL_TABLES", dt, null, SplendidCache.DefaultCacheExpiration(), Cache.NoSlidingExpiration);
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
			return dt;
		}

		public static DataTable SemanticModelFields(HttpContext Context)
		{
			System.Web.Caching.Cache Cache = HttpRuntime.Cache;
			DataTable dt = Cache.Get("vwSEMANTIC_MODEL_FIELDS") as DataTable;
			if ( dt == null )
			{
				try
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						sSQL = "select *                      " + ControlChars.CrLf
						     + "  from vwSEMANTIC_MODEL_FIELDS" + ControlChars.CrLf
						     + " order by NAME                " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dt = new DataTable();
								da.Fill(dt);
								Cache.Insert("vwSEMANTIC_MODEL_FIELDS", dt, null, SplendidCache.DefaultCacheExpiration(), Cache.NoSlidingExpiration);
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
			return dt;
		}

		public static DataTable SemanticModelRoles(HttpContext Context)
		{
			System.Web.Caching.Cache Cache = HttpRuntime.Cache;
			DataTable dt = Cache.Get("vwSEMANTIC_MODEL_ROLES") as DataTable;
			if ( dt == null )
			{
				try
				{
					DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
					using ( IDbConnection con = dbf.CreateConnection() )
					{
						con.Open();
						string sSQL;
						sSQL = "select *                      " + ControlChars.CrLf
						     + "  from vwSEMANTIC_MODEL_ROLES " + ControlChars.CrLf
						     + " order by LHS_TABLE, RHS_TABLE" + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								dt = new DataTable();
								da.Fill(dt);
								Cache.Insert("vwSEMANTIC_MODEL_ROLES", dt, null, SplendidCache.DefaultCacheExpiration(), Cache.NoSlidingExpiration);
							}
						}
					}
				}
				catch(Exception ex)
				{
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
			return dt;
		}

		// 12/12/2009 Paul.  This function will try to retrieve a cached model. 
		public static string GetUserModel(HttpContext Context, string sModel, string sPerspective)
		{
			// 12/06/2009 Paul.  SplendidCRM only has one model and no perspectives, so we can ignore the inputs. 
			Guid gUSER_ID = Security.USER_ID;
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));

			System.Web.Caching.Cache Cache = HttpRuntime.Cache;
			string sXML = Cache.Get("SEMANTIC_MODEL." + gUSER_ID.ToString()) as string;
			if ( sXML == null )
			{
				sXML = GetUserModel(Context, gUSER_ID, sModel, sPerspective);
				Cache.Insert("SEMANTIC_MODEL." + gUSER_ID.ToString(), sXML, null, SplendidCache.DefaultCacheExpiration(), Cache.NoSlidingExpiration);
			}
			return sXML;
		}

		public static string GetUserModel(HttpContext Context, Guid gUSER_ID, string sModel, string sPerspective)
		{
			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			xml.LoadXml("<SemanticModel ID=\"Gcc05a305-28ac-4f3f-aa38-d4fec0610f7b\" xmlns=\"http://schemas.microsoft.com/sqlserver/2004/10/semanticmodeling\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" />");
			
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
			string sDefaultNamespace = "http://schemas.microsoft.com/sqlserver/2004/10/semanticmodeling";
			nsmgr.AddNamespace(""         , sDefaultNamespace);
			nsmgr.AddNamespace("defaultns", sDefaultNamespace);
			nsmgr.AddNamespace("xsd"      , "http://www.w3.org/2001/XMLSchema");
			nsmgr.AddNamespace("xsi"      , "http://www.w3.org/2001/XMLSchema-instance");
			try
			{
				XmlUtil.SetSingleNode(xml, xml.DocumentElement, "Culture", "en-US", nsmgr, sDefaultNamespace);
				
				XmlElement xEntities = xml.CreateElement("Entities", sDefaultNamespace);
				xml.DocumentElement.AppendChild(xEntities);
				DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					using ( DataTable dtModules = new DataTable() )
					{
						// 12/06/2009 Paul.  We need the ID and TABLE_NAME when generating the SemanticModel for the ReportBuilder. 
						sSQL = "select *                             " + ControlChars.CrLf
						     + "  from vwSEMANTIC_MODEL_TABLES_ByUser" + ControlChars.CrLf
						     + " where USER_ID = @USER_ID            " + ControlChars.CrLf;
						using ( IDbCommand cmd = con.CreateCommand() )
						{
							cmd.CommandText = sSQL;
#if false
							cmd.CommandText += "   and MODULE_NAME in ('Accounts', 'Contacts', 'Contracts')" + ControlChars.CrLf;
#endif
							cmd.CommandText += " order by INSTANCE_SELECTION desc, COLLECTION_NAME" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@USER_ID", gUSER_ID);
							using ( DbDataAdapter da = dbf.CreateDataAdapter() )
							{
								((IDbDataAdapter)da).SelectCommand = cmd;
								da.Fill(dtModules);
							}
						}
						foreach ( DataRow rowModule in dtModules.Rows )
						{
							// 12/13/2009 Paul.  Exclude module name to prevent duplicates caused by relationship tables. 
							Guid   gMODULE_ID          = Sql.ToGuid  (rowModule["ID"                ]);
							string sTABLE_NAME         = Sql.ToString(rowModule["NAME"              ]);
							string sCOLLECTION_NAME    = Sql.ToString(rowModule["COLLECTION_NAME"   ]);
							string sINSTANCE_SELECTION = Sql.ToString(rowModule["INSTANCE_SELECTION"]);
							
							XmlElement xEntity = xml.CreateElement("Entity", sDefaultNamespace);
							xEntities.AppendChild(xEntity);
							
							XmlAttribute xID = xml.CreateAttribute("ID");
							xID.Value = ModelID(gMODULE_ID);
							xEntity.Attributes.SetNamedItem(xID);
							
							XmlElement xName                       = xml.CreateElement("Name"                      , sDefaultNamespace);
							XmlElement xCollectionName             = xml.CreateElement("CollectionName"            , sDefaultNamespace);
							XmlElement xIdentifyingAttributes      = xml.CreateElement("IdentifyingAttributes"     , sDefaultNamespace);
							XmlElement xDefaultDetailAttributes    = xml.CreateElement("DefaultDetailAttributes"   , sDefaultNamespace);
							XmlElement xDefaultAggregateAttributes = xml.CreateElement("DefaultAggregateAttributes", sDefaultNamespace);
							XmlElement xInstanceSelection          = xml.CreateElement("InstanceSelection"         , sDefaultNamespace);
							XmlElement xFields                     = xml.CreateElement("Fields"                    , sDefaultNamespace);
							xEntity.AppendChild(xName                      );
							xEntity.AppendChild(xCollectionName            );
							xEntity.AppendChild(xIdentifyingAttributes     );
							xEntity.AppendChild(xDefaultDetailAttributes   );
							xEntity.AppendChild(xDefaultAggregateAttributes);
							xEntity.AppendChild(xInstanceSelection         );
							xEntity.AppendChild(xFields                    );
							xName             .InnerText = sTABLE_NAME        ;
							xCollectionName   .InnerText = sCOLLECTION_NAME   ;
							xInstanceSelection.InnerText = sINSTANCE_SELECTION;
							
							using ( DataTable dtColumns = new DataTable() )
							{
								sSQL = "select *                       " + ControlChars.CrLf
								     + "  from vwSEMANTIC_MODEL_FIELDS " + ControlChars.CrLf
								     + " where TABLE_NAME = @TABLE_NAME" + ControlChars.CrLf
								     + " order by COLUMN_INDEX, NAME   " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@TABLE_NAME", sTABLE_NAME);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtColumns);
									}
								}

								foreach ( DataRow rowColumn in dtColumns.Rows )
								{
									Guid   gFIELD_ID                = Sql.ToGuid   (rowColumn["ID"                     ]);
									string sFIELD_NAME              = Sql.ToString (rowColumn["NAME"                   ]);
									string sDATA_TYPE               = Sql.ToString (rowColumn["DATA_TYPE"              ]);
									string sFORMAT                  = Sql.ToString (rowColumn["FORMAT"                 ]);
									int    nWIDTH                   = Sql.ToInteger(rowColumn["WIDTH"                  ]);
									bool   bNULLABLE                = Sql.ToBoolean(rowColumn["NULLABLE"               ]);
									bool   bIS_AGGREGATE            = Sql.ToBoolean(rowColumn["IS_AGGREGATE"           ]);
									bool   bSORT_DESCENDING         = Sql.ToBoolean(rowColumn["SORT_DESCENDING"        ]);
									bool   bIDENTIFYING_ATTRIBUTE   = Sql.ToBoolean(rowColumn["IDENTIFYING_ATTRIBUTE"  ]);
									bool   bDETAIL_ATTRIBUTE        = Sql.ToBoolean(rowColumn["DETAIL_ATTRIBUTE"       ]);
									string sVALUE_SELECTION         = Sql.ToString (rowColumn["VALUE_SELECTION"        ]);
									string sCONTEXTUAL_NAME         = Sql.ToString (rowColumn["CONTEXTUAL_NAME"        ]);
									bool   bDISCOURAGE_GROUPING     = Sql.ToBoolean(rowColumn["DISCOURAGE_GROUPING"    ]);
									bool   bAGGREGATE_ATTRIBUTE     = Sql.ToBoolean(rowColumn["AGGREGATE_ATTRIBUTE"    ]);
									string sAGGREGATE_FUNCTION_NAME = Sql.ToString (rowColumn["AGGREGATE_FUNCTION_NAME"]);
									bool   bDEFAULT_AGGREGATE       = Sql.ToBoolean(rowColumn["DEFAULT_AGGREGATE"      ]);
									Guid   gVARIATION_PARENT_ID     = Sql.ToGuid   (rowColumn["VARIATION_PARENT_ID"    ]);
									bool   bSORT_DESCENDING_Exists  = (rowColumn["SORT_DESCENDING"] != DBNull.Value);

									XmlNode xAttribute = null;
									// 12/13/2009 Paul.  The module count is not a variation, but a primary attribute. 
									// It just uses the same parent field. 
									if ( Sql.IsEmptyGuid(gVARIATION_PARENT_ID) || gVARIATION_PARENT_ID == gMODULE_ID )
									{
										Guid gAttributeID = gFIELD_ID;
										xAttribute = xml.CreateElement("Attribute", sDefaultNamespace);
										xFields.AppendChild(xAttribute);
										XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "ID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);

										XmlUtil.SetSingleNode(xml, xAttribute, "Name"         , sFIELD_NAME, nsmgr, sDefaultNamespace);
										XmlUtil.SetSingleNode(xml, xAttribute, "DataType"     , sDATA_TYPE , nsmgr, sDefaultNamespace);
										if ( !Sql.IsEmptyString(sFORMAT)          ) XmlUtil.SetSingleNode(xml, xAttribute, "Format"       , sFORMAT    , nsmgr, sDefaultNamespace);
										if ( bSORT_DESCENDING_Exists              ) XmlUtil.SetSingleNode(xml, xAttribute                 , "SortDirection"                 , (bSORT_DESCENDING ? "Descending" : "Ascending"), nsmgr, sDefaultNamespace);
										if ( nWIDTH > 0                           ) XmlUtil.SetSingleNode(xml, xAttribute                 , "Width"                         , nWIDTH.ToString()    , nsmgr, sDefaultNamespace);
										if ( bNULLABLE                            ) XmlUtil.SetSingleNode(xml, xAttribute                 , "Nullable"                      , "true"               , nsmgr, sDefaultNamespace);
										if ( bIS_AGGREGATE                        ) XmlUtil.SetSingleNode(xml, xAttribute                 , "IsAggregate"                   , "true"               , nsmgr, sDefaultNamespace);
										if ( !Sql.IsEmptyString(sVALUE_SELECTION) ) XmlUtil.SetSingleNode(xml, xAttribute                 , "ValueSelection"                , sVALUE_SELECTION     , nsmgr, sDefaultNamespace);
										if ( !Sql.IsEmptyString(sCONTEXTUAL_NAME) ) XmlUtil.SetSingleNode(xml, xAttribute                 , "ContextualName"                , sCONTEXTUAL_NAME     , nsmgr, sDefaultNamespace);
										if ( bDISCOURAGE_GROUPING                 ) XmlUtil.SetSingleNode(xml, xAttribute                 , "DiscourageGrouping"            , "true"               , nsmgr, sDefaultNamespace);
										if ( bIDENTIFYING_ATTRIBUTE               ) XmlUtil.AppendNode   (xml, xIdentifyingAttributes     , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										if ( bDETAIL_ATTRIBUTE                    ) XmlUtil.AppendNode   (xml, xDefaultDetailAttributes   , "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										if ( bAGGREGATE_ATTRIBUTE                 ) XmlUtil.SetSingleNode(xml, xDefaultAggregateAttributes, "AttributeReference/AttributeID", ModelID(gAttributeID), nsmgr, sDefaultNamespace);
										if ( !Sql.IsEmptyString(sAGGREGATE_FUNCTION_NAME) )
										{
											XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/FunctionName"                           , sAGGREGATE_FUNCTION_NAME, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xAttribute, "Expression/Function/Arguments/Expression/EntityRef/EntityID", ModelID(gMODULE_ID)     , nsmgr, sDefaultNamespace);
										}
									}
									else
									{
										Guid gAttributeID = gVARIATION_PARENT_ID;
										Guid gVariationID = gFIELD_ID;
										xAttribute = xFields.SelectSingleNode("defaultns:Attribute[@ID=\'" + ModelID(gAttributeID) + "\']", nsmgr);
										if ( xAttribute != null )
										{
											XmlNode xVariations = xAttribute.SelectSingleNode("defaultns:Variations", nsmgr);
											if ( xVariations == null )
											{
												xVariations = xml.CreateElement("Variations", sDefaultNamespace);
												xAttribute.AppendChild(xVariations);
											}
											XmlElement xVariation = xml.CreateElement("Attribute", sDefaultNamespace);
											xVariations.AppendChild(xVariation);
											XmlUtil.SetSingleNodeAttribute(xml, xVariation, "ID", ModelID(gVariationID), nsmgr, sDefaultNamespace);
											
											XmlUtil.SetSingleNode(xml, xVariation, "Name"         , sFIELD_NAME, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "DataType"     , sDATA_TYPE , nsmgr, sDefaultNamespace);
											if ( !Sql.IsEmptyString(sFORMAT)          ) XmlUtil.SetSingleNode(xml, xVariation, "Format"       , sFORMAT    , nsmgr, sDefaultNamespace);
											if ( bSORT_DESCENDING_Exists              ) XmlUtil.SetSingleNode(xml, xVariation, "SortDirection"                 , (bSORT_DESCENDING ? "Descending" : "Ascending"), nsmgr, sDefaultNamespace);
											if ( nWIDTH > 0                           ) XmlUtil.SetSingleNode(xml, xVariation, "Width"                         , nWIDTH.ToString()    , nsmgr, sDefaultNamespace);
											if ( bNULLABLE                            ) XmlUtil.SetSingleNode(xml, xVariation, "Nullable"     , "true"     , nsmgr, sDefaultNamespace);
											if ( bIS_AGGREGATE                        ) XmlUtil.SetSingleNode(xml, xVariation, "IsAggregate"                   , "true"               , nsmgr, sDefaultNamespace);
											if ( !Sql.IsEmptyString(sVALUE_SELECTION) ) XmlUtil.SetSingleNode(xml, xVariation, "ValueSelection"                , sVALUE_SELECTION     , nsmgr, sDefaultNamespace);
											if ( !Sql.IsEmptyString(sCONTEXTUAL_NAME) ) XmlUtil.SetSingleNode(xml, xVariation, "ContextualName"                , sCONTEXTUAL_NAME     , nsmgr, sDefaultNamespace);
											if ( bDISCOURAGE_GROUPING                 ) XmlUtil.SetSingleNode(xml, xVariation, "DiscourageGrouping"            , "true"               , nsmgr, sDefaultNamespace);
											if ( bDEFAULT_AGGREGATE                   ) XmlUtil.SetSingleNode(xml, xAttribute, "DefaultAggregateAttributeID"   , ModelID(gVariationID), nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/FunctionName"                                 , sAGGREGATE_FUNCTION_NAME, nsmgr, sDefaultNamespace);
											XmlUtil.SetSingleNode(xml, xVariation, "Expression/Function/Arguments/Expression/AttributeRef/AttributeID", ModelID(gAttributeID)   , nsmgr, sDefaultNamespace);
										}
									}
								}
							}
							using ( DataTable dtRoles = new DataTable() )
							{
								sSQL = "select *                             " + ControlChars.CrLf
								     + "  from vwSEMANTIC_MODEL_ROLES_Related" + ControlChars.CrLf
								     + " where LHS_TABLE = @LHS_TABLE        " + ControlChars.CrLf
								     + " order by RHS_TABLE, RHS_KEY         " + ControlChars.CrLf;
								using ( IDbCommand cmd = con.CreateCommand() )
								{
									cmd.CommandText = sSQL;
									Sql.AddParameter(cmd, "@LHS_TABLE", sTABLE_NAME);
									using ( DbDataAdapter da = dbf.CreateDataAdapter() )
									{
										((IDbDataAdapter)da).SelectCommand = cmd;
										da.Fill(dtRoles);
									}
								}
								
								foreach ( DataRow rowRole in dtRoles.Rows )
								{
									Guid   gROLE_ID     = Sql.ToGuid  (rowRole["ID"         ]);
									string sROLE_NAME   = Sql.ToString(rowRole["NAME"       ]);
									string sCARDINALITY = Sql.ToString(rowRole["CARDINALITY"]);
									Guid   gRELATED_ID  = Sql.ToGuid  (rowRole["RELATED_ID" ]);
									
									// 12/12/2009 Paul.  We don't have to worry about the RHS because it will be handled when processed as a LHS Table. 
									XmlElement xRole = xml.CreateElement("Role", sDefaultNamespace);
									xFields.AppendChild(xRole);
									
									XmlUtil.SetSingleNodeAttribute(xml, xRole, "ID"           , ModelID(gROLE_ID   ), nsmgr, sDefaultNamespace);
									XmlUtil.SetSingleNode         (xml, xRole, "RelatedRoleID", ModelID(gRELATED_ID), nsmgr, sDefaultNamespace);
									if ( !Sql.IsEmptyString(sCARDINALITY) ) XmlUtil.SetSingleNode(xml, xRole, "Cardinality", sCARDINALITY, nsmgr, sDefaultNamespace);
									if ( !Sql.IsEmptyString(sROLE_NAME  ) ) XmlUtil.SetSingleNode(xml, xRole, "Name"       , sROLE_NAME  , nsmgr, sDefaultNamespace);
								}
							}
						}
					}
				}
				
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
#if DEBUG
			xml.Save(Context.Server.MapPath("~/SemanticModel LAST.xml"));
#endif
			return xml.OuterXml;
		}
		#endregion

		private static string FieldNameFromAttribute(DataView vwSEMANTIC_MODEL_FIELDS, string sFieldID)
		{
			return FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, ModelGuid(sFieldID));
		}

		private static string FieldNameFromAttribute(DataView vwSEMANTIC_MODEL_FIELDS, Guid gFieldID)
		{
			string sFieldName = String.Empty;
			vwSEMANTIC_MODEL_FIELDS.RowFilter = "ID = '" + gFieldID.ToString() + "'";
			if ( vwSEMANTIC_MODEL_FIELDS.Count > 0 )
			{
				sFieldName = Sql.ToString(vwSEMANTIC_MODEL_FIELDS[0]["TABLE_NAME"]) + "." + Sql.ToString(vwSEMANTIC_MODEL_FIELDS[0]["NAME"]);
			}
			return sFieldName;
		}

		private static string TableNameFromAttribute(DataView vwSEMANTIC_MODEL_TABLES, string sTableID)
		{
			return TableNameFromAttribute(vwSEMANTIC_MODEL_TABLES, ModelGuid(sTableID));
		}

		private static string TableNameFromAttribute(DataView vwSEMANTIC_MODEL_TABLES, Guid gTableID)
		{
			string sTableName = String.Empty;
			vwSEMANTIC_MODEL_TABLES.RowFilter = "ID = '" + gTableID.ToString() + "'";
			if ( vwSEMANTIC_MODEL_TABLES.Count > 0 )
			{
				sTableName = Sql.ToString(vwSEMANTIC_MODEL_TABLES[0]["NAME"]);
			}
			return sTableName;
		}

		private static void AddExpression(XmlNode xExpression, XmlNamespaceManager nsmgr, DataView vwSEMANTIC_MODEL_FIELDS, StringBuilder sbSQL, List<Field> lstFields)
		{
			string sFieldAlias   = XmlUtil.SelectAttribute(xExpression, "Name");
			string sFieldID      = XmlUtil.SelectSingleNode(xExpression, "AttributeRef/AttributeID", nsmgr);
			string sTableName    = String.Empty;
			string sFieldName    = String.Empty;
			string sFunctionName = String.Empty;
			Guid   gParentID     = Guid.Empty;
			bool   bIsAggregate  = false;
			vwSEMANTIC_MODEL_FIELDS.RowFilter = "ID = '" + ModelGuid(sFieldID).ToString() + "'";
			if ( vwSEMANTIC_MODEL_FIELDS.Count > 0 )
			{
				sTableName    = Sql.ToString (vwSEMANTIC_MODEL_FIELDS[0]["TABLE_NAME"             ]);
				sFieldName    = Sql.ToString (vwSEMANTIC_MODEL_FIELDS[0]["NAME"                   ]);
				sFunctionName = Sql.ToString (vwSEMANTIC_MODEL_FIELDS[0]["AGGREGATE_FUNCTION_NAME"]);
				bIsAggregate  = Sql.ToBoolean(vwSEMANTIC_MODEL_FIELDS[0]["IS_AGGREGATE"           ]);
				gParentID     = Sql.ToGuid   (vwSEMANTIC_MODEL_FIELDS[0]["VARIATION_PARENT_ID"    ]);
				if ( !Sql.IsEmptyGuid(gParentID) )
					sFieldName = FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, gParentID);
				else
					sFieldName = sTableName + "." + sFieldName;
				if ( sbSQL.Length == 0 )
					sbSQL.Append("select ");
				else
					sbSQL.Append("     , ");
				switch ( sFunctionName )
				{
					case "Day"    :  sbSQL.AppendLine("dbo.fnDatePart('day'    , " + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Month"  :  sbSQL.AppendLine("dbo.fnDatePart('month'  , " + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Year"   :  sbSQL.AppendLine("dbo.fnDatePart('year'   , " + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Quarter":  sbSQL.AppendLine("dbo.fnDatePart('quarter', " + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Min"    :  sbSQL.AppendLine("min("   + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Max"    :  sbSQL.AppendLine("max("   + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Avg"    :  sbSQL.AppendLine("avg("   + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Sum"    :  sbSQL.AppendLine("sum("   + sFieldName + ") as '" + sFieldAlias + "'");  break;
					case "Count"  :  sbSQL.AppendLine("count(" + sFieldName + ") as '" + sFieldAlias + "'");  break;
					default       :  sbSQL.AppendLine(sFieldName + " as '" + sFieldAlias + "'");  break;
				}
			}
			Field fld = new Field();
			fld.Alias = sFieldAlias;
			fld.Name  = sFieldAlias;
			lstFields.Add(fld);
		}

		private static void FilterExpression(XmlDocument xml, XmlNamespaceManager nsmgr, XmlNodeList nlFilters, DataView vwSEMANTIC_MODEL_FIELDS, StringBuilder sbSQL, string sSqlOperator, int nSqlIndent)
		{
			foreach ( XmlNode xFilter in nlFilters )
			{
				XmlNode xFunction = XmlUtil.SelectNode(xFilter, "Function", nsmgr);
				if ( xFunction != null )
				{
					string sFunctionName = XmlUtil.SelectSingleNode(xFunction, "FunctionName", nsmgr);
					if ( sFunctionName == "In" )
					{
						string sFieldID      = XmlUtil.SelectSingleNode(xFunction, "Arguments/Expression/AttributeRef/AttributeID", nsmgr);
						string sFieldName    = FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, sFieldID);
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						if ( nlExpressions.Count == 2 )
						{
							string sValueType = XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/DataType", nsmgr);
							XmlNodeList nlValues = nlExpressions[1].SelectNodes("defaultns:Literal/defaultns:Values/defaultns:Value", nsmgr);
							if( nlValues.Count > 0 && !Sql.IsEmptyString(sFieldName) )
							{
								sbSQL.Append(Strings.Space(nSqlIndent) + sSqlOperator + " " + sFieldName + " in (");
								for ( int i=0; i < nlValues.Count; i++ )
								{
									if ( i > 0 )
										sbSQL.Append(", ");
									string sValue = Sql.ToString(nlValues[i].InnerText);
									sbSQL.Append("'" + Sql.EscapeSQL(sValue) + "'");
								}
								sbSQL.AppendLine(")");
							}
						}
					}
					else if ( sFunctionName == "Equals" || sFunctionName == "GreaterThan" || sFunctionName == "GreaterThanOrEquals" || sFunctionName == "LessThan" || sFunctionName == "LessThanOrEquals" )
					{
						string sFieldID      = XmlUtil.SelectSingleNode(xFunction, "Arguments/Expression/AttributeRef/AttributeID", nsmgr);
						string sFieldName    = FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, sFieldID);
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						if ( nlExpressions.Count == 2 )
						{
							if ( nlExpressions[1].SelectSingleNode("defaultns:Null", nsmgr) != null )
							{
								sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " " + sFieldName + " is null");
							}
							else
							{
								string sOperator = " = ";
								switch ( sFunctionName )
								{
									case "GreaterThan"        :  sOperator = " > " ;  break;
									case "GreaterThanOrEquals":  sOperator = " >= ";  break;
									case "LessThan"           :  sOperator = " < " ;  break;
									case "LessThanOrEquals"   :  sOperator = " <= ";  break;
								}
								string sValueType = XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/DataType", nsmgr);
								string sValue     = Sql.ToString(XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/Value", nsmgr));
								sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " " + sFieldName + sOperator + "'" + Sql.EscapeSQL(sValue) + "'");
							}
						}
					}
					else if ( sFunctionName == "NotEquals" )
					{
						string sFieldID      = XmlUtil.SelectSingleNode(xFunction, "Arguments/Expression/AttributeRef/AttributeID", nsmgr);
						string sFieldName    = FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, sFieldID);
						string sFind         = XmlUtil.SelectSingleNode(xFunction, "Arguments/Expression/Function/FunctionName", nsmgr);
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						// 12/13/2009 Paul.  The contains filter is placed under a NotEquals tag. 
						if ( sFind == "Find" )
						{
							FilterExpression(xml, nsmgr, nlExpressions, vwSEMANTIC_MODEL_FIELDS, sbSQL, sSqlOperator, nSqlIndent);
						}
						else if ( nlExpressions.Count == 2 )
						{
							string sValueType = XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/DataType", nsmgr);
							string sValue     = Sql.ToString(XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/Value", nsmgr));
							sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " " + sFieldName + " <> '" + Sql.EscapeSQL(sValue) + "'");
						}
					}
					else if ( sFunctionName == "Find" )
					{
						string sFieldID      = XmlUtil.SelectSingleNode(xFunction, "Arguments/Expression/AttributeRef/AttributeID", nsmgr);
						string sFieldName    = FieldNameFromAttribute(vwSEMANTIC_MODEL_FIELDS, sFieldID);
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						if ( nlExpressions.Count == 2 )
						{
							string sValueType = XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/DataType", nsmgr);
							string sValue     = Sql.ToString(XmlUtil.SelectSingleNode(nlExpressions[1], "Literal/Value", nsmgr));
							sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " " + sFieldName + " like '%" + Sql.EscapeSQL(sValue) + "%'");
						}
					}
					else if ( sFunctionName == "Or" )
					{
						sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " ( 1 = 0");
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						FilterExpression(xml, nsmgr, nlExpressions, vwSEMANTIC_MODEL_FIELDS, sbSQL, "    or", nSqlIndent + 7);
						sbSQL.AppendLine(Strings.Space(nSqlIndent) + "       )");
					}
					else if ( sFunctionName == "And" )
					{
						sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " ( 1 = 1");
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						FilterExpression(xml, nsmgr, nlExpressions, vwSEMANTIC_MODEL_FIELDS, sbSQL, "   and", nSqlIndent + 7);
						sbSQL.AppendLine(Strings.Space(nSqlIndent) + "       )");
					}
					else if ( sFunctionName == "Not" )
					{
						//sbSQL.AppendLine(Strings.Space(nSqlIndent) + sSqlOperator + " not ( 1 = 1");
						//XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						//FilterExpression(xml, nsmgr, nlExpressions, vwSEMANTIC_MODEL_FIELDS, sbSQL, "         and", nSqlIndent + 7);
						//sbSQL.AppendLine(Strings.Space(nSqlIndent) + "       )");
						// 12/13/2009 Paul.  Simplify the not clause. 
						XmlNodeList nlExpressions = xFunction.SelectNodes("defaultns:Arguments/defaultns:Expression", nsmgr);
						FilterExpression(xml, nsmgr, nlExpressions, vwSEMANTIC_MODEL_FIELDS, sbSQL, sSqlOperator + " not ", nSqlIndent + 7);
					}
				}
			}
		}

		private static void AddRelationship(XmlNode xExpression, XmlNamespaceManager nsmgr, DataView vwSEMANTIC_MODEL_TABLES, DataView vwSEMANTIC_MODEL_ROLES, StringBuilder sbSqlTables, StringDictionary dictTables)
		{
			string sRoleID = XmlUtil.SelectSingleNode(xExpression, "Path/RolePathItem/RoleID", nsmgr);
			vwSEMANTIC_MODEL_ROLES.RowFilter = "ID = '" + ModelGuid(sRoleID).ToString() + "'";
			if ( vwSEMANTIC_MODEL_ROLES.Count > 0 )
			{
				string sLHS_TABLE   = Sql.ToString(vwSEMANTIC_MODEL_ROLES[0]["LHS_TABLE"  ]);
				string sLHS_KEY     = Sql.ToString(vwSEMANTIC_MODEL_ROLES[0]["LHS_KEY"    ]);
				string sRHS_TABLE   = Sql.ToString(vwSEMANTIC_MODEL_ROLES[0]["RHS_TABLE"  ]);
				string sRHS_KEY     = Sql.ToString(vwSEMANTIC_MODEL_ROLES[0]["RHS_KEY"    ]);
				string sCARDINALITY = Sql.ToString(vwSEMANTIC_MODEL_ROLES[0]["CARDINALITY"]);
				
				string sTableName = sRHS_TABLE;
				string sTableID = XmlUtil.SelectSingleNode(xExpression, "EntityRef/EntityID", nsmgr);
				if ( !Sql.IsEmptyString(sTableID) )
					sTableName = TableNameFromAttribute(vwSEMANTIC_MODEL_TABLES, sTableID);
				
				if ( !dictTables.ContainsKey(sLHS_TABLE) || !dictTables.ContainsKey(sRHS_TABLE) )
				{
					// 12/13/2009 Paul.  We will likely need to bring in @RELATIONSHIP_ROLE_COLUMN and @RELATIONSHIP_ROLE_COLUMN_VALUE. 
					if ( sLHS_TABLE == sTableName )
					{
						if ( sCARDINALITY == "One" )
							sbSqlTables.AppendLine("       inner join vw" + sLHS_TABLE + " " + sLHS_TABLE);
						else
							sbSqlTables.AppendLine("  left outer join vw" + sLHS_TABLE + " " + sLHS_TABLE);
						sbSqlTables.AppendLine("               on " + sLHS_TABLE + "." + sLHS_KEY + " = " + sRHS_TABLE + "." + sRHS_KEY);
						dictTables.Add(sTableName, String.Empty);
					}
					else if ( sRHS_TABLE == sTableName )
					{
						if ( sCARDINALITY == "One" )
							sbSqlTables.AppendLine("       inner join vw" + sRHS_TABLE + " " + sRHS_TABLE);
						else
							sbSqlTables.AppendLine("  left outer join vw" + sRHS_TABLE + " " + sRHS_TABLE);
						sbSqlTables.AppendLine("               on " + sRHS_TABLE + "." + sRHS_KEY + " = " + sLHS_TABLE + "." + sLHS_KEY);
						dictTables.Add(sTableName, String.Empty);
					}
				}
			}
		}

		private static string ConvertSemanticQuery(HttpContext Context, string sSemanticQuery, ref List<Field> lstFields)
		{
			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			xml.LoadXml(sSemanticQuery);
#if DEBUG
			xml.Save(Context.Server.MapPath("~/SemanticQuery LAST.xml"));
#endif
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
			string sDefaultNamespace = "http://schemas.microsoft.com/sqlserver/2004/10/semanticmodeling";
			nsmgr.AddNamespace(""         , sDefaultNamespace);
			nsmgr.AddNamespace("defaultns", sDefaultNamespace);
			nsmgr.AddNamespace("xsd"      , "http://www.w3.org/2001/XMLSchema");
			nsmgr.AddNamespace("xsi"      , "http://www.w3.org/2001/XMLSchema-instance");
			nsmgr.AddNamespace("rb"       , "http://schemas.microsoft.com/sqlserver/2004/11/reportbuilder");
			nsmgr.AddNamespace("qd"       , "http://schemas.microsoft.com/sqlserver/2004/11/semanticquerydesign");

			DataView vwSEMANTIC_MODEL_TABLES = new DataView(SemanticModelTables(Context));
			DataView vwSEMANTIC_MODEL_FIELDS = new DataView(SemanticModelFields(Context));
			DataView vwSEMANTIC_MODEL_ROLES  = new DataView(SemanticModelRoles (Context));
			
			StringBuilder sbSqlFields = new StringBuilder();
			StringBuilder sbSqlTables = new StringBuilder();
			StringBuilder sbSqlFilter = new StringBuilder();
			// 12/13/2009 Paul.  The defaultns prefix is automatic in XmlUtil.SelectSingleNode(). 
			string sBaseEntityID  = XmlUtil.SelectSingleNode(xml.DocumentElement, "Hierarchies/Hierarchy/BaseEntity/EntityID", nsmgr);
			string sBaseTableName = String.Empty;
			sBaseTableName = TableNameFromAttribute(vwSEMANTIC_MODEL_TABLES, sBaseEntityID);
			sbSqlTables.AppendLine("  from            vw" + sBaseTableName + " " + sBaseTableName);

			StringDictionary dictTables = new StringDictionary();
			dictTables.Add(sBaseTableName, String.Empty);
			// 12/13/2009 Paul.  The defaultns prefix is required when using the direct XmlPath call. 
			XmlNodeList nlGroupings = xml.DocumentElement.SelectNodes("defaultns:Hierarchies/defaultns:Hierarchy/defaultns:Groupings/defaultns:Grouping", nsmgr);
			foreach ( XmlNode xGrouping in nlGroupings )
			{
				XmlNodeList nlExpressions = xGrouping.SelectNodes("defaultns:Details/defaultns:Expression", nsmgr);
				foreach ( XmlNode xExpression in nlExpressions )
				{
					AddExpression(xExpression, nsmgr, vwSEMANTIC_MODEL_FIELDS, sbSqlFields, lstFields);
				}
				nlExpressions = xGrouping.SelectNodes("defaultns:Expression", nsmgr);
				foreach ( XmlNode xExpression in nlExpressions )
				{
					AddRelationship(xExpression, nsmgr, vwSEMANTIC_MODEL_TABLES, vwSEMANTIC_MODEL_ROLES, sbSqlTables, dictTables);
					AddExpression(xExpression, nsmgr, vwSEMANTIC_MODEL_FIELDS, sbSqlFields, lstFields);
				}
			}
			sbSqlFilter.AppendLine(" where 1 = 1");
			XmlNodeList nlFilters = xml.DocumentElement.SelectNodes("defaultns:Hierarchies/defaultns:Hierarchy/defaultns:Filter/defaultns:Expression", nsmgr);
			FilterExpression(xml, nsmgr, nlFilters, vwSEMANTIC_MODEL_FIELDS, sbSqlFilter, "   and", 0);

			string sSQL = sbSqlFields.ToString() + sbSqlTables.ToString() + sbSqlFilter.ToString();
#if DEBUG
			File.WriteAllText(Context.Server.MapPath("~/SemanticQuery LAST.sql"), sSQL);
#endif
			return sSQL;
		}

		//"http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/PrepareQuery"
		[WebMethod(EnableSession=true)]
		[SoapHeaderAttribute("ServerInfoHeaderValue", Direction=SoapHeaderDirection.Out)]
		[SoapHeaderAttribute("BatchHeaderValue")]
		[SoapDocumentMethodAttribute("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices/PrepareQuery", RequestNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", ResponseNamespace="http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Wrapped)]
		[return: XmlElementAttribute("DataSettings")]
		public DataSetDefinition PrepareQuery(DataSource DataSource, DataSetDefinition DataSet, out bool Changed, out string[] ParameterNames)
		{
			// 09/25/2010 Paul.  InitServerInfoHeader was not begin called, but this was not a critical error. 
			InitServerInfoHeader();
			List<Field> lstFields = new List<Field>();
			// DataSet.Query.Timeout
			if ( DataSet.Query.CommandType == "Text" )
			{
				if ( DataSet.Query.CommandText.StartsWith("<SemanticQuery") )
				{
					DataSet.Query.CommandText = ConvertSemanticQuery(this.Context, DataSet.Query.CommandText, ref lstFields);
				}
				else
				{
				}
			}
			
			DataSet.Fields = lstFields.ToArray();
			DataSet.CaseSensitivity     = SensitivityEnum.False;
			DataSet.AccentSensitivity   = SensitivityEnum.False;
			DataSet.KanatypeSensitivity = SensitivityEnum.False;
			DataSet.WidthSensitivity    = SensitivityEnum.False;
			// 12/13/2009 Paul.  Must set output parameters. 
			Changed        = true;
			ParameterNames = new string[0];
			return DataSet;
		}

		public static string ExecuteQuery(HttpContext Context, string sModel, string sSemanticQuery, int nTimeout)
		{
			// 12/06/2009 Paul.  SplendidCRM only has one model and no perspectives, so we can ignore these inputs. 
			Guid gUSER_ID = Security.USER_ID;
			if ( !SplendidCRM.Security.IsAuthenticated() )
				throw(new Exception("Authentication Required"));
			if ( SplendidCRM.Security.GetUserAccess("Reports", "edit") < 0 )
				throw(new Exception("Access Denied"));

			XmlDocument xml = new XmlDocument();
			// 01/20/2015 Paul.  Disable XmlResolver to prevent XML XXE. 
			// https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Processing
			// http://stackoverflow.com/questions/14230988/how-to-prevent-xxe-attack-xmldocument-in-net
			xml.XmlResolver = null;
			//xml.AppendChild(xml.CreateProcessingInstruction("xml" , "version=\"1.0\" encoding=\"UTF-8\""));
			// 12/10/2009 Paul.  We are having trouble creating the xs:schema tag with both attributes. 
			string sXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			            + "<Data>"
			            + "	<xs:schema id=\"NewDataSet\" xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" />"
			            + "</Data>";
			xml.LoadXml(sXML);
			
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
			string sDefaultNamespace = "";
			string sXsNamespace      = "http://www.w3.org/2001/XMLSchema"    ;
			string sMsdataNamespace  = "urn:schemas-microsoft-com:xml-msdata";
			nsmgr.AddNamespace("xs"    , sXsNamespace    );
			nsmgr.AddNamespace("msdata", sMsdataNamespace);
			try
			{
				List<Field> lstFields = new List<Field>();
				string sCommandText = ConvertSemanticQuery(Context, sSemanticQuery, ref lstFields);
				/*
				﻿<?xml version="1.0" encoding="utf-8"?>
				<Data>
					<xs:schema id="NewDataSet" xmlns="" >
						<xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:MainDataTable="Row" msdata:UseCurrentLocale="true">
							<xs:complexType>
								<xs:choice minOccurs="0" maxOccurs="unbounded">
									<xs:element name="Row">
										<xs:complexType>
											<xs:sequence>
												<xs:element name="ACCOUNTS" type="xs:string" minOccurs="0" msdata:Ordinal="0" />
												<xs:element name="NAME" type="xs:string" minOccurs="0" msdata:Ordinal="1" />
												<xs:element name="ACCOUNT_x0020_NUMBER" type="xs:string" minOccurs="0" msdata:Ordinal="2" />
												<xs:element name="ACCOUNT_x0020_TYPE" type="xs:string" minOccurs="0" msdata:Ordinal="3" />
											</xs:sequence>
											<xs:attribute name="AggInfo" type="xs:string" />
										</xs:complexType>
									</xs:element>
								</xs:choice>
							</xs:complexType>
						</xs:element>
					</xs:schema>
					<DataSet />
				</Data>
				*/
				XmlElement xData = xml.DocumentElement;
				
				XmlNode xSchema = xData.ChildNodes[0];
				//XmlElement xSchema = xml.CreateElement("xs", "schema", sXsNamespace);
				// 12/10/2009 Paul.  We are having trouble creating the xs:schema tag with both attributes. 
				//xData.AppendChild(xSchema);
				//XmlUtil.SetSingleNodeAttribute(xml, xSchema,          "id"    , "NewDataSet"    , nsmgr, sDefaultNamespace);
				//XmlUtil.SetSingleNodeAttribute(xml, xSchema, "xmlns", "msdata", sMsdataNamespace, nsmgr, sDefaultNamespace);
				
				XmlElement xDataSet = xml.CreateElement("DataSet");
				xData.AppendChild(xDataSet);
				
				XmlElement xElement = xml.CreateElement("xs", "element", sXsNamespace);
				xSchema.AppendChild(xElement);
				XmlUtil.SetSingleNodeAttribute(xml, xElement,           "name"            , "NewDataSet", nsmgr, sDefaultNamespace);
				XmlUtil.SetSingleNodeAttribute(xml, xElement, "msdata", "IsDataSet"       , "true"      , nsmgr, sMsdataNamespace );
				XmlUtil.SetSingleNodeAttribute(xml, xElement, "msdata", "MainDataTable"   , "Row"       , nsmgr, sMsdataNamespace );
				XmlUtil.SetSingleNodeAttribute(xml, xElement, "msdata", "UseCurrentLocale", "true"      , nsmgr, sMsdataNamespace );
				
				XmlElement xComplexType = xml.CreateElement("xs", "complexType", sXsNamespace);
				xElement.AppendChild(xComplexType);
				
				XmlElement xChoice = xml.CreateElement("xs", "choice", sXsNamespace);
				xComplexType.AppendChild(xChoice);
				XmlUtil.SetSingleNodeAttribute(xml, xChoice, "minOccurs", "0"        , nsmgr, sDefaultNamespace);
				XmlUtil.SetSingleNodeAttribute(xml, xChoice, "maxOccurs", "unbounded", nsmgr, sDefaultNamespace);
				
				XmlElement xElementRow = xml.CreateElement("xs", "element", sXsNamespace);
				xChoice.AppendChild(xElementRow);
				XmlUtil.SetSingleNodeAttribute(xml, xElementRow, "name", "Row", nsmgr, sDefaultNamespace);
				
				xComplexType = xml.CreateElement("xs", "complexType", sXsNamespace);
				xElementRow.AppendChild(xComplexType);
				
				XmlElement xSequence = xml.CreateElement("xs", "sequence", sXsNamespace);
				xComplexType.AppendChild(xSequence);
				
				XmlElement xAttribute = xml.CreateElement("xs", "attribute", sXsNamespace);
				xComplexType.AppendChild(xAttribute);
				XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "name", "AggInfo"  , nsmgr, sDefaultNamespace);
				XmlUtil.SetSingleNodeAttribute(xml, xAttribute, "type", "xs:string", nsmgr, sDefaultNamespace);

				try
				{
					using ( DataTable dt = new DataTable() )
					{
						DbProviderFactory dbf = DbProviderFactories.GetFactory(Context.Application);
						using ( IDbConnection con = dbf.CreateConnection() )
						{
							con.Open();
							using ( IDbCommand cmd = con.CreateCommand() )
							{
								// 12/13/2009 Paul.  This is potentially dangerous as we are not going to apply access rights to the SemanticQuery. 
								cmd.CommandText = sCommandText;
								using ( DbDataAdapter da = dbf.CreateDataAdapter() )
								{
									((IDbDataAdapter)da).SelectCommand = cmd;
									da.Fill(dt);
								}
							}
						}
						// 12/13/2009 Paul.  Use a regular expression to ensure that the tag name of the field is valid. 
						Regex r = new Regex(@"[^A-Za-z0-9_]");
						StringDictionary dict = new StringDictionary();
						foreach ( Field fld in lstFields )
						{
							dict.Add(fld.Alias, r.Replace(fld.Name, "_"));
						}
						for ( int i=0; i < dt.Columns.Count; i++ )
						{
							DataColumn col = dt.Columns[i];
							
							XmlElement xElementField = xml.CreateElement("xs", "element", sXsNamespace);
							xSequence.AppendChild(xElementField);
							string sFieldAlias = col.ColumnName;
							string sFieldName  = sFieldAlias;
							if ( dict.ContainsKey(sFieldAlias) )
								sFieldName  = dict[sFieldAlias];
							
							// http://www.w3schools.com/schema/schema_elements_ref.asp
							string sDataType = "xs:string";
							switch ( col.DataType.FullName )
							{
								case "System.Boolean" :  sDataType = "xs:boolean"     ;  break;
								case "System.Single"  :  sDataType = "xs:float"       ;  break;
								case "System.Double"  :  sDataType = "xs:double"      ;  break;
								case "System.Int16"   :  sDataType = "xs:integer"     ;  break;
								case "System.Int32"   :  sDataType = "xs:integer"     ;  break;
								case "System.Int64"   :  sDataType = "xs:integer"     ;  break;
								case "System.Guid"    :  sDataType = "xs:string"      ;  break;
								case "System.String"  :  sDataType = "xs:string"      ;  break;
								case "System.Decimal" :  sDataType = "xs:decimal"     ;  break;
								case "System.DateTime":  sDataType = "xs:date"        ;  break;
								case "System.Byte[]"  :  sDataType = "xs:base64Binary";  break;
							}
							XmlUtil.SetSingleNodeAttribute(xml, xElementField, "name"     , sFieldName, nsmgr       , sDefaultNamespace);
							XmlUtil.SetSingleNodeAttribute(xml, xElementField, "type"     , sDataType , nsmgr       , sDefaultNamespace);
							XmlUtil.SetSingleNodeAttribute(xml, xElementField, "minOccurs", "0"       , nsmgr       , sDefaultNamespace);
							XmlUtil.SetSingleNodeAttribute(xml, xElementField, "msdata"   , "Ordinal" , i.ToString(), nsmgr, sMsdataNamespace);
						}
						foreach ( DataRow row in dt.Rows )
						{
							XmlElement xRow = xml.CreateElement("Row");
							xDataSet.AppendChild(xRow);
							for ( int i=0; i < dt.Columns.Count; i++ )
							{
								DataColumn col = dt.Columns[i];
								
								string sFieldAlias = col.ColumnName;
								string sFieldName  = sFieldAlias;
								if ( dict.ContainsKey(sFieldAlias) )
									sFieldName  = dict[sFieldAlias];
								
								XmlElement xField = xml.CreateElement(sFieldName);
								xRow.AppendChild(xField);
								if ( row[i] != DBNull.Value )
								{
									if ( col.DataType.FullName == "System.DateTime" )
										xField.InnerText = Sql.ToDateTime(row[i]).ToString("s");
									else if ( col.DataType.FullName == "System.Byte[]" )
										xField.InnerText = Convert.ToBase64String(row[i] as byte[]);
									else
										xField.InnerText = Sql.ToString(row[i]);
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					// 12/13/2009 Paul.  Log the error, but don't throw an exception. 
					SplendidError.SystemMessage(Context, "Error", new StackTrace(true).GetFrame(0), ex);
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
				throw;
			}
#if DEBUG
			xml.Save(Context.Server.MapPath("~/SemanticQuery Results LAST.xml"));
#endif
			return xml.OuterXml;
		}

	}
}
