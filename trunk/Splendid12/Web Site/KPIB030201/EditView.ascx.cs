using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using SplendidCRM._modules;

namespace SplendidCRM.KPIB030201
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected SplendidGrid grdMain;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKpiUnit");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("lblKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblPLAN_VALUE = (Label)e.Item.FindControl("lblPLAN_VALUE");
                if (lblPLAN_VALUE != null)
                {
                    lblPLAN_VALUE.Text = KPIs_Utils.FormatFloat(lblPLAN_VALUE.Text, format_number);
                }
                Label lblSYNC_VALUE = (Label)e.Item.FindControl("lblSYNC_VALUE");
                if (lblSYNC_VALUE != null)
                {
                    lblSYNC_VALUE.Text = KPIs_Utils.FormatFloat(lblSYNC_VALUE.Text, format_number);
                }
                TextBox txtFINAL_VALUE = (TextBox)e.Item.FindControl("txtFINAL_VALUE");
                if (txtFINAL_VALUE != null)
                {
                    txtFINAL_VALUE.Text = KPIs_Utils.FormatFloat(txtFINAL_VALUE.Text, format_number);
                }
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();
                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwB_KPI_ORG_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;
                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;


                                    int sYear = new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue;
                                    string sMonthPeriod = new SplendidCRM.DynamicControl(this, rowCurrent, "MONTH_PERIOD").Text;
                                    string sVersionNumber = new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text;

                                    SqlProcs.spB_KPI_ORG_ACTUAL_RESULT_Update
                                        (ref gID
                                        , gASSIGNED_USER_ID
                                        , gTEAM_ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ACTUAL_RESULT_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "MONTH_PERIOD").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ALLOCATE_CODE").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_BY").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGN_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_SYNC_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_FINAL_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "PERCENT_MANUAL_TOTAL").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_01").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_02").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TOTAL_AMOUNT_03").FloatValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FILE_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "LATEST_SYNC_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_ID").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVE_STATUS").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX1").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX2").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX3").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX4").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "FLEX5").Text
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                        , trn
                                        );

                                    //Begin detail
                                    if (ViewState["CurrentTable"] != null)
                                    {
                                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                                        if (dtCurrentTable.Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                            {
                                                Guid gIDT;

                                                HiddenField hdfACTUAL_RESULT_ID = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfACTUAL_RESULT_ID");
                                                HiddenField hdfKPI_ID = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfKPI_ID");
                                                HiddenField hdfKPI_CODE = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfKPI_CODE");
                                                HiddenField hdfLEVEL_NUMBER = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfLEVEL_NUMBER");

                                                HiddenField hdfKpiUnit = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfKpiUnit");
                                                HiddenField hdfRatio = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfRatio");
                                                HiddenField hdfPlanValue = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfPlanValue");
                                                HiddenField hdfSyncValue = (HiddenField)grdMain.Items[i].Cells[1].FindControl("hdfSyncValue");

                                                Label txtKpiName = (Label)grdMain.Items[i].Cells[0].FindControl("txtKpiName");
                                                TextBox txtFinalValue = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtFINAL_VALUE");
                                                TextBox txtDescription = (TextBox)grdMain.Items[i].Cells[3].FindControl("txtDescription");


                                                if (hdfACTUAL_RESULT_ID.Value == string.Empty)
                                                {
                                                    gIDT = Guid.NewGuid();
                                                }
                                                else
                                                {
                                                    gIDT = Guid.Parse(hdfACTUAL_RESULT_ID.Value);
                                                }
                                                //save details
                                                SqlProcs.spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update
                                                    (
                                                        ref gIDT //ref Guid gID
                                                        , gASSIGNED_USER_ID //, Guid gASSIGNED_USER_ID
                                                        , gTEAM_ID // , Guid gTEAM_ID
                                                        , string.Empty // , string sTEAM_SET_LIST
                                                        , string.Empty   //, string sNAME
                                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "ACTUAL_RESULT_CODE").Text //, string sACTUAL_RESULT_CODE
                                                        , sYear //, Int32 nYEAR
                                                        , sMonthPeriod //, string sMONTH_PERIOD
                                                        , sVersionNumber //, string sVERSION_NUMBER
                                                        , hdfKPI_ID.Value == string.Empty ? Guid.NewGuid() : Guid.Parse(hdfKPI_ID.Value)// , Guid gKPI_ID
                                                        , hdfKPI_CODE.Value //, string sKPI_CODE
                                                        , txtKpiName.Text //, string sKPI_NAME
                                                        , hdfKpiUnit.Value == string.Empty ? 0 : int.Parse(hdfKpiUnit.Value) // , Int32 nKPI_UNIT
                                                        , hdfLEVEL_NUMBER.Value == string.Empty ? 0 : int.Parse(hdfLEVEL_NUMBER.Value) //, Int32 nLEVEL_NUMBER
                                                        , hdfRatio.Value == string.Empty ? 0 : float.Parse(hdfRatio.Value)//, float flRATIO
                                                        , hdfPlanValue.Value == string.Empty ? 0 : Sql.ToDecimal(hdfPlanValue.Value)//, float flPLAN_VALUE
                                                        , hdfSyncValue.Value == string.Empty ? 0 : Sql.ToDecimal(hdfSyncValue.Value)//, float flSYNC_VALUE
                                                        , txtFinalValue.Text == string.Empty ? 0 : Sql.ToDecimal(txtFinalValue.Text)//, float flFINAL_VALUE
                                                        , 0 //, float flPERCENT_SYNC_VALUE
                                                        , 0 //, float flPERCENT_FINAL_VALUE
                                                        , 0 //, float flPERCENT_MANUAL_VALUE
                                                        , txtDescription.Text //, string sDESCRIPTION
                                                        , string.Empty// , string sREMARK
                                                        , DateTime.Now//, DateTime dtLATEST_SYNC_DATE
                                                        , string.Empty //, string sFLEX1
                                                        , string.Empty //, string sFLEX2
                                                        , string.Empty //, string sFLEX3
                                                        , string.Empty //, string sFLEX4
                                                        , string.Empty //, string sFLEX5
                                                        , string.Empty //, string sTAG_SET_NAME
                                                        , trn
                                                    );
                                            }
                                            ViewState["CurrentTable"] = dtCurrentTable;
                                        }
                                    }
                                    //End detail

                                    SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                    SplendidCRM.SqlProcs.spTRACKER_Update
                                        (Security.USER_ID
                                        , m_sMODULE
                                        , gID
                                        , new SplendidCRM.DynamicControl(this, rowCurrent, "NAME").Text
                                        , "save"
                                        , trn
                                        );
                                    if (plcSubPanel.Visible)
                                    {
                                        foreach (Control ctl in plcSubPanel.Controls)
                                        {
                                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                            if (ctlSubPanel != null)
                                            {
                                                ctlSubPanel.Save(gID, m_sMODULE, trn);
                                            }
                                        }
                                    }
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();

                        //Xac dinh truong hop tao moi, can lay danh sach kpi tu man hinh phan giao?
                        bool loadDetailFromResultTable = false;

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_ORG_ACTUAL_RESULT_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                                //Security.Filter(cmd, m_sMODULE, "edit");

                                if (!Sql.IsEmptyGuid(gDuplicateID))
                                {
                                    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                    gID = Guid.Empty;
                                }
                                else
                                {
                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    cmd.CommandText += string.Format(" AND ISNULL(APPROVE_STATUS,'') <> '{0}' AND ISNULL(APPROVE_STATUS,'') <> '{1}' ", KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                                }
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            //Truong hop Edit se lay thong tin detail tu bang ket qua chi tiet
                                            loadDetailFromResultTable = true;

                                            DataRow rdr = dtCurrent.Rows[0];

                                            int iYear = Sql.ToInteger(rdr["YEAR"]);
                                            string sMonthPeriod = Sql.ToString(rdr["MONTH_PERIOD"]);
                                            string sOrganizationCode = Sql.ToString(rdr["ORGANIZATION_CODE"]);

                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(rdr["ORGANIZATION_ID"]), "M_ORGANIZATION", "ORGANIZATION_NAME");
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();
                                            Label txtORGANIZATION_NAME = this.FindControl("ORGANIZATION_NAME") as Label;
                                            if (txtORGANIZATION_NAME != null)
                                                txtORGANIZATION_NAME.Text = KPIs_Utils.getNameFromGuidID(Sql.ToGuid(rdr["ORGANIZATION_ID"]), "M_ORGANIZATION", "ORGANIZATION_NAME"); ;

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["NAME"] = Sql.ToString(rdr["NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["NAME"] = ViewState["NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);


                                            string actualResultCode = Sql.ToString(rdr["ACTUAL_RESULT_CODE"]);
                                            if (actualResultCode != string.Empty)
                                            {
                                                string monthPeriod = Sql.ToString(rdr["MONTH_PERIOD"]);
                                                //Load ket qua chi tiet
                                                this.LoadDelivery_KPIsForEdit(dbf, actualResultCode, monthPeriod);
                                            }
                                            else
                                            {
                                                //Load thong tin phan giao chi tiet, cho phep sua hoac nhap gia tri moi
                                                this.LoadDelivery_KPIsForAddNew(dbf, iYear, sMonthPeriod, sOrganizationCode);
                                            }

                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;

                                            //Truong hop tao moi se lay thong tin tu bang phan giao chi tiet
                                            loadDetailFromResultTable = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();

                        //Tu dong sinh ma cap nhat ket qua thuc hien
                        Label lblCODE = this.FindControl("ACTUAL_RESULT_CODE") as Label;
                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_org_actual_result_frefix"]);
                        string sub_code_prefix = Sql.ToString(Application["CONFIG.kpi_org_actual_result_sub_frefix"]);
                        string strActualResultCode = KPIs_Utils.GenerateAllocatedCode(code_prefix, sub_code_prefix, "vwB_KPI_ORG_ACTUAL_RESULT_Edit", "ACTUAL_RESULT_CODE");
                        if (lblCODE != null && lblCODE.Text == string.Empty)
                        {
                            lblCODE.Text = strActualResultCode;
                        }

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["NAME"] = ViewState["NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        //Truong hop Edit se lay thong tin detail tu bang ket qua chi tiet
        protected void LoadDelivery_KPIsForEdit(DbProviderFactory dbf, string actualResultCode, string monthPeriod)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT ID,CREATED_BY,DATE_ENTERED,MODIFIED_USER_ID,DATE_MODIFIED,DATE_MODIFIED_UTC  " + ControlChars.CrLf
                       + " , ASSIGNED_USER_ID,TEAM_ID,TEAM_SET_ID,NAME " + ControlChars.CrLf
                       + " , ACTUAL_RESULT_CODE,YEAR,MONTH_PERIOD,VERSION_NUMBER  " + ControlChars.CrLf
                       + " , KPI_ID,KPI_CODE,KPI_NAME,KPI_UNIT,LEVEL_NUMBER,RATIO " + ControlChars.CrLf
                       + " , PLAN_VALUE,SYNC_VALUE,FINAL_VALUE,PERCENT_SYNC_VALUE,PERCENT_FINAL_VALUE,PERCENT_MANUAL_VALUE  " + ControlChars.CrLf
                       + " , DESCRIPTION,REMARK,LATEST_SYNC_DATE  " + ControlChars.CrLf
                       + " , FLEX1,FLEX2,FLEX3,FLEX4,FLEX5 ,'' AS No" + ControlChars.CrLf
                       + "  FROM vwB_KPI_ORG_ACTUAL_RESULT_DETAIL_Edit   " + ControlChars.CrLf;

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                    //Security.Filter(cmd, m_sMODULE, "edit");

                    Sql.AppendParameter(cmd, actualResultCode, Sql.SqlFilterMode.Exact, "ACTUAL_RESULT_CODE");
                    Sql.AppendParameter(cmd, monthPeriod, Sql.SqlFilterMode.Exact, "MONTH_PERIOD");//MONTH_PERIOD
                    cmd.CommandText += "  order by DATE_ENTERED   " + ControlChars.CrLf;
                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                if (dtCurrent.Rows.Count > 0)
                                {
                                    ViewState["CurrentTable"] = dtCurrent;
                                    for (int i = 0; i < grdMain.Items.Count; i++)
                                    {
                                        Label lblNO = (Label)grdMain.Items[i].FindControl("txtNo");

                                        lblNO.Text = (i + 1).ToString();

                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        //Truong hop Addnew se lay thong tin detail tu bang phan giao chi tiet
        protected void LoadDelivery_KPIsForAddNew(DbProviderFactory dbf, int iYear, string sMonthPeriod, string sOrganizationCode)
        {
            using (IDbConnection con = dbf.CreateConnection())
            {
                string sSQL = " SELECT alc_dtl.ID,alc_dtl.NAME,alc_dtl.KPI_ALLOCATE_ID,alc_dtl.ALLOCATE_CODE,alc_dtl.VERSION_NUMBER " + ControlChars.CrLf
                       + " , alc_dtl.ORGANIZATION_CODE,alc_dtl.KPI_CODE,alc_dtl.KPI_NAME,alc_dtl.LEVEL_NUMBER,alc_dtl.KPI_UNIT,alc_dtl.UNIT,alc_dtl.RADIO " + ControlChars.CrLf
                       + " , alc_dtl.MAX_RATIO_COMPLETE,alc_dtl.KPI_GROUP_DETAIL_ID,alc_dtl.DESCRIPTION,alc_dtl.REMARK,alc_dtl.TOTAL_VALUE " + ControlChars.CrLf
                       + " , alc_dtl.MONTH_1,alc_dtl.MONTH_2,alc_dtl.MONTH_3,alc_dtl.MONTH_4,alc_dtl.MONTH_5 " + ControlChars.CrLf
                       + " , alc_dtl.MONTH_6,alc_dtl.MONTH_7,alc_dtl.MONTH_8,alc_dtl.MONTH_9,alc_dtl.MONTH_10,alc_dtl.MONTH_11,alc_dtl.MONTH_12 " + ControlChars.CrLf
                       + " , alc_dtl.FLEX1,alc_dtl.FLEX2,alc_dtl.FLEX3,alc_dtl.FLEX4,alc_dtl.FLEX5 " + ControlChars.CrLf
                       + " , alc.YEAR,'' AS No " + ControlChars.CrLf
                       + "  FROM vwB_KPI_ORG_ALLOCATE_DETAILS_Edit alc_dtl " + ControlChars.CrLf
                       + "  INNER JOIN vwB_KPI_ORG_ALLOCATES_Edit alc ON alc_dtl.ALLOCATE_CODE = alc.ALLOCATE_CODE " + ControlChars.CrLf;

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sSQL;

                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;
                    //Security.Filter(cmd, m_sMODULE, "edit");

                    Sql.AppendParameter(cmd, iYear, "YEAR", false);
                    Sql.AppendParameter(cmd, sOrganizationCode, Sql.SqlFilterMode.Exact, "ORGANIZATION_CODE");
                    cmd.CommandText += "  order by alc_dtl.DATE_ENTERED   " + ControlChars.CrLf;

                    con.Open();

                    if (bDebug)
                        RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                    {
                        ((IDbDataAdapter)da).SelectCommand = cmd;
                        using (DataTable dtCurrent = new DataTable())
                        {
                            da.Fill(dtCurrent);
                            if (dtCurrent.Rows.Count > 0)
                            {
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();

                                if (dtCurrent.Rows.Count > 0)
                                {
                                    ViewState["CurrentTable"] = dtCurrent;
                                    for (int i = 0; i < grdMain.Items.Count; i++)
                                    {
                                        Label lblNO = (Label)grdMain.Items[i].FindControl("txtNo");

                                        lblNO.Text = (i + 1).ToString();

                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            m_sMODULE = "KPIB030201";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}
