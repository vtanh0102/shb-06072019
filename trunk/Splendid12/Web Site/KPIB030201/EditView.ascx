<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB030201.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="divEditView" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
	<SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="true" Module="KPIB030201" EnablePrint="false" HelpName="EditView" EnableHelp="true" Runat="Server" />

	<asp:HiddenField ID="LAYOUT_EDIT_VIEW" Runat="server" />
	<asp:Table SkinID="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
				<table ID="tblMain" class="tabEditView" runat="server">
				</table>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>

	<div id="divEditSubPanel">
		<asp:PlaceHolder ID="plcSubPanel" Runat="server" />
        <!--Begin Detail-->
        <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
            <Columns>
                <asp:TemplateColumn HeaderText="#">
                    <ItemTemplate>
                        <asp:Label ID="txtNo" Text='<%# Bind("No") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                    <ItemTemplate>
                        <asp:HiddenField  ID="hdfACTUAL_RESULT_ID" Value='<%# Bind("ID") %>' runat="server"/>
                        <asp:HiddenField  ID="hdfKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server"/>
                        <asp:HiddenField  ID="hdfKPI_CODE" Value='<%# Bind("KPI_CODE") %>' runat="server"/>
                        <asp:HiddenField  ID="hdfLEVEL_NUMBER" Value='<%# Bind("LEVEL_NUMBER") %>' runat="server"/>

                        <asp:Label ID="txtKpiName" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                    <ItemTemplate>
                        <asp:Label ID="txtKpiUnit" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                        <asp:HiddenField  ID="hdfKpiUnit" Value='<%# Bind("KPI_UNIT") %>' runat="server"/>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                    <ItemTemplate>
                        <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" CssClass="right"/>
                        <asp:HiddenField ID="hdfRatio" Value='<%# Bind("RATIO") %>' runat="server"/>
                    </ItemTemplate>
                </asp:TemplateColumn>
                
                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_PLAN_VALUE">
                    <ItemTemplate>
                        <asp:Label ID="lblPLAN_VALUE" Text='<%# Bind("PLAN_VALUE") %>' runat="server" CssClass="right"/>
                        <asp:HiddenField ID="hdfPlanValue" Value='<%# Bind("PLAN_VALUE") %>' runat="server"/>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_SYNC_VALUE">
                    <ItemTemplate>
                        <asp:Label ID="lblSYNC_VALUE" Text='<%# Bind("SYNC_VALUE") %>' runat="server" CssClass="right"/>
                        <asp:HiddenField ID="hdfSyncValue" Value='<%# Bind("SYNC_VALUE") %>' runat="server"/>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_FINAL_VALUE">
                    <ItemTemplate>
                        <asp:TextBox ID="txtFINAL_VALUE" Text='<%# Bind("FINAL_VALUE") %>' runat="server" CssClass="format-qty right"/>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' TextMode="MultiLine" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </SplendidCRM:SplendidGrid>
        <!--End Detail-->

	</div>

	<%@ Register TagPrefix="SplendidCRM" Tagname="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
	<SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" Runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
