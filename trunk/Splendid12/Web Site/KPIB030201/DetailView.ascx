<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIB030201.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divDetailView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIB030201" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIB030201" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
        <!--Begin Detail-->
        <SplendidCRM:SplendidGrid ID="grdMain" SkinID="grdListView" AllowPaging="<%# !PrintView %>" EnableViewState="true" runat="server">
            <Columns>
                <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdfACTUAL_RESULT_ID" Value='<%# Bind("ID") %>' runat="server" />
                        <asp:HiddenField ID="hdfKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                        <asp:HiddenField ID="hdfKPI_CODE" Value='<%# Bind("KPI_CODE") %>' runat="server" />
                        <asp:Label ID="txtKpiName" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                    <ItemTemplate>
                        <asp:Label ID="txtKpiUnit" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                        <asp:HiddenField ID="hdfKpiUnit" Value='<%# Bind("KPI_UNIT") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_RATIO">
                    <ItemTemplate>
                        <asp:Label ID="lblKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                        <asp:HiddenField ID="hdfRatio" Value='<%# Bind("RATIO") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_PLAN_VALUE">
                    <ItemTemplate>
                        <asp:Label ID="lblPLAN_VALUE" Text='<%# Bind("PLAN_VALUE") %>' runat="server" />
                        <asp:HiddenField ID="hdfPlanValue" Value='<%# Bind("PLAN_VALUE") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_SYNC_VALUE">
                    <ItemTemplate>
                        <asp:Label ID="lblSYNC_VALUE" Text='<%# Bind("SYNC_VALUE") %>' runat="server" />
                        <asp:HiddenField ID="hdfSyncValue" Value='<%# Bind("SYNC_VALUE") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KPIB030201_DETAIL.LBL_LIST_FINAL_VALUE">
                    <ItemTemplate>
                        <asp:Label ID="lblFINAL_VALUE" Text='<%# Bind("FINAL_VALUE") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_NEED_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblNEED_VALUE" Text='<%# Bind("NEED_VALUE") %>' runat="server" CssClass="right format-qty" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_LIST_PERCENT_FINAL_VALUE">
                    <HeaderStyle CssClass="gridHeaderLabel-Gray right format-qty" />
                    <ItemStyle CssClass="dataField right format-qty" Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblPERCENT_FINAL_VALUE" Text='<%# Bind("PERCENT_FINAL_VALUE") %>' runat="server" CssClass="right format-qty" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                    <ItemTemplate>
                        <asp:Label ID="txtDescription" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </SplendidCRM:SplendidGrid>
        <!--End Detail-->
    </div>
</div>
<SplendidCRM:InlineScript runat="server">
    <script type="text/javascript">
        $(function () {
            $("[id$=ctl00_cntBody_ctlDetailView_ctlDynamicButtons_ctl00_btnDUPLICATE]").hide();
        });
    </script>
</SplendidCRM:InlineScript>
<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
