<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MassUpdate.ascx.cs" Inherits="SplendidCRM.KPIB030201.MassUpdate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdateButtons" Src="~/_controls/MassUpdateButtons.ascx" %>
<SplendidCRM:MassUpdateButtons ID="ctlDynamicButtons" SubPanel="divKPIB030201MassUpdate" Title=".LBL_MASS_UPDATE_TITLE" Runat="Server" />

<div id="divKPIB030201MassUpdate" style='<%= "display:" + (CookieValue("divKPIB030201MassUpdate") != "1" ? "inline" : "none") %>'>
	<asp:Table Width="100%" CellPadding="0" CellSpacing="0" CssClass="tabForm" runat="server">
		<asp:TableRow>
			<asp:TableCell>
                <%@ Register TagPrefix="SplendidCRM" Tagname="TagMassApprove" Src="~/_controls/TagMassApprove.ascx" %>
				<SplendidCRM:TagMassApprove ID="ctlTagMassApprove" Runat="Server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
</div>
