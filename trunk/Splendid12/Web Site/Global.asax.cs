/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.IO;
using System.Web;
using System.Threading;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Diagnostics;
using log4net.Config;

namespace SplendidCRM 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private Timer tSchedulerManager = null;
		private Timer tWorkflowManager  = null;
		private Timer tEmailManager     = null;

		public void InitSchedulerManager()
		{
			if ( tSchedulerManager == null )
			{
				// 05/19/2008 Paul.  The timer will fire every 5 minutes.  If decreased to 1 minute, then vwSCHEDULERS_Run must be modified to round to 1 minute. 
				// 10/27/2008 Paul.  Pass the context instead of the Application so that more information will be available to the error handling. 
				tSchedulerManager = new Timer(SchedulerUtils.OnTimer, this.Context, new TimeSpan(0, 1, 0), new TimeSpan(0, 5, 0));
				SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "The Scheduler Manager timer has been activated.");
			}
		}

		// 12/25/2012 Paul.  Use a separate timer for email reminders as they are timely and cannot be stuck behind other scheduler tasks. 
		// 09/16/2015 Paul.  Google notifications will also be processed in the email timer. 
		public void InitEmailManager()
		{
			if ( tEmailManager == null )
			{
				tEmailManager = new Timer(EmailUtils.OnTimer, this.Context, new TimeSpan(0, 1, 0), new TimeSpan(0, 1, 0));
				SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "The Email Manager timer has been activated.");
			}
		}

		// 10/13/2008 Paul.  Use a separate timer for the workflow engine so that we can poll every 30 seconds. 
		public void InitWorkflowManager()
		{
			if ( tWorkflowManager == null )
			{
				// 10/27/2008 Paul.  Pass the context instead of the Application so that more information will be available to the error handling. 
				tWorkflowManager = new Timer(WorkflowUtils.OnTimer, this.Context, new TimeSpan(0, 1, 0), new TimeSpan(0, 0, 30));
				SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "The Workflow Manager timer has been activated.");
			}
		}

		public Global()
		{
			InitializeComponent();
		}

		protected void Application_OnError(Object sender, EventArgs e)
		{
			//SplendidInit.Application_OnError(this.Context);
		}
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			// 11/04/2008 Paul.  IIS7 does not provide access to Request object, so we cannot determine the database connection from the URL. 
			// 09/02/2013 Paul.  SignalR may throw exceptions and we need to make sure that they don't crash the entire app. 
			System.Threading.Tasks.TaskScheduler.UnobservedTaskException += new EventHandler<System.Threading.Tasks.UnobservedTaskExceptionEventArgs>(TaskScheduler_UnobservedTaskException);
		}

		private void TaskScheduler_UnobservedTaskException(object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e)
		{
			e.SetObserved();
		}

		protected void Session_Start(Object sender, EventArgs e)
		{
            XmlConfigurator.Configure();
			SplendidInit.InitSession(this.Context);
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
			// 10/26/2008 Paul.  IIS7 does not provide access to Request object from Application_Start. Move code to Application_BeginRequest. 
			if ( Application.Count == 0 )
			{
				// 08/31/2011 Paul.  If an HttpModule is used, then it will get the call first and 
				// we will need to InitApp there. 
				SplendidInit.InitApp(this.Context);
				WorkflowInit.StartRuntime(this.Context);
				InitSchedulerManager();
				InitEmailManager();
				InitWorkflowManager();
				// 08/28/2013 Paul.  Add support for Asterisk, Twilio and SignalR. 
				AsteriskManager.InitApp(this.Context);
				// 12/03/2013 Paul.  Add support for Avaya. 
				AvayaManager.InitApp(this.Context);
				TwilioManager.InitApp(this.Context);
				TwitterManager.InitApp(this.Context);
				// 11/10/2014 Paul.  Add ChatManager support. 
				ChatManager.InitApp(this.Context);
				SignalRUtils.InitApp();
			}
			// 09/24/2011 Paul.  We need to define a privacy policy so that cookies will not get rejected when SplendidCRM is used in an iframe. 
			// http://petesbloggerama.blogspot.com/2007/08/aspnet-loss-of-session-cookies-with.html
			string sP3P = Sql.ToString(Application["CONFIG.p3p"]);
			if ( Sql.IsEmptyString(sP3P) )
				sP3P = "CP=\"CAO PSA OUR\"";
			HttpContext.Current.Response.AddHeader("p3p", sP3P);

			// 12/29/2005 Paul.  vCalendar support is not going to be easy.
			// Outlook will automatically use FrontPage extensions to place the file. 
			// When connecting to a Apache server, it will make HTTP GET/PUT requests. 
			/*
			string sPath = HttpContext.Current.Request.Path.ToLower();
			Regex regex = new Regex("/vcal_server/(\\w+)", RegexOptions.IgnoreCase);
			MatchCollection matches = regex.Matches(sPath);
			//if ( sPath.IndexOf("/vcal_server/") >= 0 )
			if ( matches.Count > 0 )
			{
				//sPath = sPath.Replace("/vcal_server/", "/vcal_server.aspx?");
				sPath = "~/vcal_server.aspx?" + matches[0].Groups[1].ToString();
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), sPath);
				HttpContext.Current.RewritePath(sPath);
			}
			*/
		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AcquireRequestState(Object sender, EventArgs e)
		{
			// 03/04/2007 Paul.  The Session will be NULL during web service calls. 
			// We noticed this problem when AJAX failed in ScriptResource.axd. 
			if ( HttpContext.Current.Session != null )
			{
				// 02/28/2007 Paul.  Although Application_AuthenticateRequest might seem like the best place for this code,
				// we have to wait until the Session variables have been initialized to determine if the user has been authenticated. 
				if ( !Sql.IsEmptyString(HttpContext.Current.Session["USER_NAME"]) )
				{
					// 02/28/2007 Paul.  WebParts requires a valid User identity.  
					// We must store the USER_NAME as this will be the lookup key when updating preferences. 
					if ( !HttpContext.Current.User.Identity.IsAuthenticated )
						HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(Security.USER_NAME, "Forms"), null);
				}
			}
		}

		/// <summary>
		/// 2018-09-08 Truong. Remove cac header khong can thiet theo chuan Security
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
		{
			if (HttpContext.Current != null)
			{
				HttpContext.Current.Response.Headers.Remove("X-Powered-By");
				HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
				HttpContext.Current.Response.Headers.Remove("Server");
			}
		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{
			// 03/02/2008 Paul.  Log the logout. 
			// 09/29/2013 Paul.  Special note, Session_End only fires when using InProc sessionState mode. 
			try
			{
				Guid gUSER_LOGIN_ID = Security.USER_LOGIN_ID;
				if ( !Sql.IsEmptyGuid(gUSER_LOGIN_ID) )
					SqlProcs.spUSERS_LOGINS_Logout(gUSER_LOGIN_ID);
			}
			catch(Exception ex)
			{
				SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
			}

			// 10/29/2006 Paul.  Delete temp files. 
			foreach ( string sKey in Session.Keys )
			{
				if ( sKey.StartsWith("TempFile.") )
				{
					string sTempFileName = Sql.ToString(Session[sKey]);
					string sTempPathName = Path.Combine(Path.GetTempPath(), sTempFileName);
					if ( File.Exists(sTempPathName) )
					{
						try
						{
							File.Delete(sTempPathName);
						}
						catch(Exception ex)
						{
							SplendidError.SystemError(new StackTrace(true).GetFrame(0), "Could not delete temp file: " + sTempPathName + ControlChars.CrLf + ex.Message);
						}
					}
				}
			}
		}

		protected void Application_End(Object sender, EventArgs e)
		{
			if ( tSchedulerManager != null )
				tSchedulerManager.Dispose();
			if ( tWorkflowManager != null )
				tWorkflowManager.Dispose();
			if ( tEmailManager != null )
				tEmailManager.Dispose();
			// 07/18/2010 Paul.  Move Exchange Sync functions to a separate class. 
			ExchangeSync.StopSubscriptions(this.Context);
			WorkflowInit.StopRuntime(this.Application);
			SplendidInit.StopApp(this.Context);
		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

