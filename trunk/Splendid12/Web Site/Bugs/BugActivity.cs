/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// BugActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:27 PM
	/// </summary>
	public class BugActivity: SplendidActivity
	{
		public BugActivity()
		{
			this.Name = "BugActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.IDProperty))); }
			set { base.SetValue(BugActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(BugActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(BugActivity.NAMEProperty))); }
			set { base.SetValue(BugActivity.NAMEProperty, value); }
		}

		public static DependencyProperty STATUSProperty = System.Workflow.ComponentModel.DependencyProperty.Register("STATUS", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string STATUS
		{
			get { return ((string)(base.GetValue(BugActivity.STATUSProperty))); }
			set { base.SetValue(BugActivity.STATUSProperty, value); }
		}

		public static DependencyProperty PRIORITYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRIORITY", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRIORITY
		{
			get { return ((string)(base.GetValue(BugActivity.PRIORITYProperty))); }
			set { base.SetValue(BugActivity.PRIORITYProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(BugActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(BugActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty RESOLUTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("RESOLUTION", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string RESOLUTION
		{
			get { return ((string)(base.GetValue(BugActivity.RESOLUTIONProperty))); }
			set { base.SetValue(BugActivity.RESOLUTIONProperty, value); }
		}

		public static DependencyProperty FOUND_IN_RELEASEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FOUND_IN_RELEASE", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FOUND_IN_RELEASE
		{
			get { return ((string)(base.GetValue(BugActivity.FOUND_IN_RELEASEProperty))); }
			set { base.SetValue(BugActivity.FOUND_IN_RELEASEProperty, value); }
		}

		public static DependencyProperty TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TYPE", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TYPE
		{
			get { return ((string)(base.GetValue(BugActivity.TYPEProperty))); }
			set { base.SetValue(BugActivity.TYPEProperty, value); }
		}

		public static DependencyProperty FIXED_IN_RELEASEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FIXED_IN_RELEASE", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FIXED_IN_RELEASE
		{
			get { return ((string)(base.GetValue(BugActivity.FIXED_IN_RELEASEProperty))); }
			set { base.SetValue(BugActivity.FIXED_IN_RELEASEProperty, value); }
		}

		public static DependencyProperty WORK_LOGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("WORK_LOG", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string WORK_LOG
		{
			get { return ((string)(base.GetValue(BugActivity.WORK_LOGProperty))); }
			set { base.SetValue(BugActivity.WORK_LOGProperty, value); }
		}

		public static DependencyProperty SOURCEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("SOURCE", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string SOURCE
		{
			get { return ((string)(base.GetValue(BugActivity.SOURCEProperty))); }
			set { base.SetValue(BugActivity.SOURCEProperty, value); }
		}

		public static DependencyProperty PRODUCT_CATEGORYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PRODUCT_CATEGORY", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PRODUCT_CATEGORY
		{
			get { return ((string)(base.GetValue(BugActivity.PRODUCT_CATEGORYProperty))); }
			set { base.SetValue(BugActivity.PRODUCT_CATEGORYProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(BugActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(BugActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.PARENT_IDProperty))); }
			set { base.SetValue(BugActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty BUG_NUMBERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("BUG_NUMBER", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string BUG_NUMBER
		{
			get { return ((string)(base.GetValue(BugActivity.BUG_NUMBERProperty))); }
			set { base.SetValue(BugActivity.BUG_NUMBERProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.TEAM_IDProperty))); }
			set { base.SetValue(BugActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(BugActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(BugActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty EXCHANGE_FOLDERProperty = System.Workflow.ComponentModel.DependencyProperty.Register("EXCHANGE_FOLDER", typeof(bool), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool EXCHANGE_FOLDER
		{
			get { return ((bool)(base.GetValue(BugActivity.EXCHANGE_FOLDERProperty))); }
			set { base.SetValue(BugActivity.EXCHANGE_FOLDERProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(BugActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(BugActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(BugActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(BugActivity.CREATED_BYProperty))); }
			set { base.SetValue(BugActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(BugActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(BugActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(BugActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(BugActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(BugActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(BugActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(BugActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(BugActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(BugActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(BugActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(BugActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(BugActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(BugActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(BugActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty FIXED_IN_RELEASE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FIXED_IN_RELEASE_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid FIXED_IN_RELEASE_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.FIXED_IN_RELEASE_IDProperty))); }
			set { base.SetValue(BugActivity.FIXED_IN_RELEASE_IDProperty, value); }
		}

		public static DependencyProperty FOUND_IN_RELEASE_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FOUND_IN_RELEASE_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid FOUND_IN_RELEASE_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.FOUND_IN_RELEASE_IDProperty))); }
			set { base.SetValue(BugActivity.FOUND_IN_RELEASE_IDProperty, value); }
		}

		public static DependencyProperty LAST_ACTIVITY_DATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("LAST_ACTIVITY_DATE", typeof(DateTime), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime LAST_ACTIVITY_DATE
		{
			get { return ((DateTime)(base.GetValue(BugActivity.LAST_ACTIVITY_DATEProperty))); }
			set { base.SetValue(BugActivity.LAST_ACTIVITY_DATEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(BugActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(BugActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(BugActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(BugActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(BugActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("BugActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("BugActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select BUGS_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwBUGS_AUDIT        BUGS          " + ControlChars.CrLf
							                + " inner join vwBUGS_AUDIT        BUGS_AUDIT_OLD" + ControlChars.CrLf
							                + "         on BUGS_AUDIT_OLD.ID = BUGS.ID       " + ControlChars.CrLf
							                + "        and BUGS_AUDIT_OLD.AUDIT_VERSION = (select max(vwBUGS_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                              from vwBUGS_AUDIT                   " + ControlChars.CrLf
							                + "                                             where vwBUGS_AUDIT.ID            =  BUGS.ID           " + ControlChars.CrLf
							                + "                                               and vwBUGS_AUDIT.AUDIT_VERSION <  BUGS.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                               and vwBUGS_AUDIT.AUDIT_TOKEN   <> BUGS.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                           )" + ControlChars.CrLf
							                + " where BUGS.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *          " + ControlChars.CrLf
							                + "  from vwBUGS_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwBUGS_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *          " + ControlChars.CrLf
							                + "  from vwBUGS_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								STATUS                         = Sql.ToString  (rdr["STATUS"                        ]);
								PRIORITY                       = Sql.ToString  (rdr["PRIORITY"                      ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								RESOLUTION                     = Sql.ToString  (rdr["RESOLUTION"                    ]);
								FOUND_IN_RELEASE               = Sql.ToString  (rdr["FOUND_IN_RELEASE"              ]);
								TYPE                           = Sql.ToString  (rdr["TYPE"                          ]);
								FIXED_IN_RELEASE               = Sql.ToString  (rdr["FIXED_IN_RELEASE"              ]);
								WORK_LOG                       = Sql.ToString  (rdr["WORK_LOG"                      ]);
								SOURCE                         = Sql.ToString  (rdr["SOURCE"                        ]);
								PRODUCT_CATEGORY               = Sql.ToString  (rdr["PRODUCT_CATEGORY"              ]);
								BUG_NUMBER                     = Sql.ToString  (rdr["BUG_NUMBER"                    ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									FIXED_IN_RELEASE_ID            = Sql.ToGuid    (rdr["FIXED_IN_RELEASE_ID"           ]);
									FOUND_IN_RELEASE_ID            = Sql.ToGuid    (rdr["FOUND_IN_RELEASE_ID"           ]);
									LAST_ACTIVITY_DATE             = Sql.ToDateTime(rdr["LAST_ACTIVITY_DATE"            ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("BugActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("BUGS", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spBUGS_Update
								( ref gID
								, ASSIGNED_USER_ID
								, NAME
								, STATUS
								, PRIORITY
								, DESCRIPTION
								, RESOLUTION
								, FOUND_IN_RELEASE
								, TYPE
								, FIXED_IN_RELEASE
								, WORK_LOG
								, SOURCE
								, PRODUCT_CATEGORY
								, PARENT_TYPE
								, PARENT_ID
								, BUG_NUMBER
								, TEAM_ID
								, TEAM_SET_LIST
								, EXCHANGE_FOLDER
								, TAG_SET_NAME
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("BugActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

