﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;
using SplendidCRM._modules;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class EmpTOISummaryChart : DashletControl
    {
        protected Label lbTotalToiResult;
        protected Label lbMaxTotalToiResult;
        protected Label lbMinTotalToiResult;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;
           
        }

        //Lay ket qua thu thuan
        public void SetEmpTOISummaryChart(ListControl lstYear, ListControl lstMonth, TextBox tbFullName, ListControl lstPosotion, ListControl lstArea, ListControl lstRegion, ListControl lstOrganization, ListControl lstOrganizationParent)
        {
            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");

            var EMPLOYEE_ID = string.Empty;
            var FULL_NAME = string.Empty;
            var MAIN_POS_ID = string.Empty;
            var POS_ID = string.Empty;
            var POSITION_ID = string.Empty;
            var POSITION_NAME = string.Empty;
            var AREA_ID = string.Empty;
            var REGION_ID = string.Empty;
            var BUSINESS_CODE = string.Empty;

            if (lstYear != null) year = lstYear.SelectedValue;
            if (lstMonth != null) month = lstMonth.SelectedValue;
            if (tbFullName != null) FULL_NAME = tbFullName.Text;
            if (lstOrganizationParent != null) MAIN_POS_ID = lstOrganizationParent.SelectedValue;
            if (lstOrganization != null) POS_ID = lstOrganization.SelectedValue;
            if (lstPosotion != null) POSITION_ID = lstPosotion.SelectedValue;
            if (lstArea != null) AREA_ID = lstArea.SelectedValue;
            if (lstRegion != null) REGION_ID = lstRegion.SelectedValue;

            //Check role
            if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
            {
                MAIN_POS_ID = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;

                if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    POS_ID = Security.CURRENT_EMPLOYEE.POS_ID;
                }
                if ("CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    EMPLOYEE_ID = Security.CURRENT_EMPLOYEE.EMPLOYEE_ID;
                }
            }


            var chartCode1 = "EmpTOISummaryChart";
            //Ket qua thu thuan
            decimal dTotalToiResult;
            decimal dMaxTotalToiResult;
            decimal dMinTotalToiResult;
            int iTotalEmployee;

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode1 + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);

                    sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", EMPLOYEE_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$FULL_NAME", FULL_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", MAIN_POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POS_ID", POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_ID", POSITION_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_NAME", POSITION_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$REGION_ID", REGION_ID);

                    sqlGetDataChart = sqlGetDataChart.Replace("$BUSINESS_CODE", BUSINESS_CODE);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        string sTotalToiResult = dt1.Rows[0][ykeysJ[0]].ToString();
                        string sMaxTotalToiResult = dt1.Rows[0][ykeysJ[1]].ToString();
                        string sMinTotalToiResult = dt1.Rows[0][ykeysJ[2]].ToString();
                        string sTotalEmployee = dt1.Rows[0][ykeysJ[3].Trim()].ToString();

                        dTotalToiResult = Decimal.Parse((sTotalToiResult == "")?"0":sTotalToiResult);
                        dMaxTotalToiResult = Decimal.Parse((sMaxTotalToiResult == "") ? "0" : sMaxTotalToiResult);
                        dMinTotalToiResult = Decimal.Parse((sMinTotalToiResult == "") ? "0" : sMinTotalToiResult);

                        iTotalEmployee = int.Parse((sTotalEmployee == "") ? "0" : sTotalEmployee);
                        string format_number = Sql.ToString(Application["CONFIG.format_number"]);

                        lbTotalToiResult.Text = KPIs_Utils.FormatFloat(dTotalToiResult.ToString(), format_number);
                        lbMaxTotalToiResult.Text = KPIs_Utils.FormatFloat(dMaxTotalToiResult.ToString(), format_number);
                        lbMinTotalToiResult.Text = KPIs_Utils.FormatFloat(dMinTotalToiResult.ToString(), format_number);
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

