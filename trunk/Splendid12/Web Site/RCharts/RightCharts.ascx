<%@ Control CodeBehind="RightCharts.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.RightCharts" %>

<script src="../Include/dist/Chart.bundle.js"></script>

<asp:Panel runat="server" Style="background-color: white">
    <asp:HiddenField runat="server" ID="lblName2"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblType2"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblDataCharJs2"></asp:HiddenField>
    <div style="background-color: white;">
        <canvas id="canvas2" style="height: 220px;"></canvas>
    </div>
    <script>
        var ctx2 = document.getElementById('canvas2').getContext('2d');
        var data2 = $('#<%=lblDataCharJs2.ClientID %>').val();
        var type2 = $('#<%=lblType2.ClientID %>').val();
        var name2 = $('#<%=lblName2.ClientID %>').val();
        var typechart2 = '';
        if (type2 == '1') typechart2 = 'line'
        else if (type2 == '2') typechart2 = 'bar'
        else if (type2 == '3') typechart2 = 'pie';        
        var config2 = {
            type: typechart2,
            data: JSON.parse(data2),
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: name2
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: false,
                        scaleLabel: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.fillStyle = '#444';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];                            
                                ctx.fillText(data + "%", bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }
            }
        };
        var myChart2 = new Chart(ctx2, config2);

    </script>
</asp:Panel>


