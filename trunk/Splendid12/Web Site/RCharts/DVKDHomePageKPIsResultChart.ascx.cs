﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class DVKDHomePageKPIsResultChart : DashletControl
    {
        protected HiddenField lblType1;
        protected HiddenField lblName1;
        protected HiddenField lblDataCharJs1;

        protected HiddenField lblType2;
        protected HiddenField lblName2;
        protected HiddenField lblDataCharJs2;

        protected HiddenField lblTypeY2YKQTH;
        protected HiddenField lblNameY2YKQTH;
        protected HiddenField lblDataCharJsY2YKQTH;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;

            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");
            string main_pos_id = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;
            string pos_id = Security.CURRENT_EMPLOYEE.POS_ID;

            this.SearchDVKDHomePageKPIsResultPOSChart(year, month, main_pos_id, pos_id);
        }

        //Tien do thuc hien KPIs DVKD
        public void SearchDVKDHomePageKPIsResultPOSChart(string year, string month, string main_pos_id, string pos_id)
        {
            lblType2.Value = string.Empty;
            lblDataCharJs2.Value = string.Empty;
            lblName2.Value = string.Empty;
            

            var chartCode = "DVKDHomePageKPIsResultPOSChart";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    lblName2.Value = dt.Rows[0]["NAME"].ToString() + ": "+ Security.CURRENT_EMPLOYEE.POS_NAME;
                    lblType2.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);

                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", "");
                    sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", main_pos_id);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POS_ID", pos_id);                        

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    // using char js                                        
                    var datacharts = new DataChartJs();
                    string[] labels = new string[dt1.Rows.Count];
                    
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        labels[i] = dt1.Rows[i][xkeyJ].ToString();
                    }
                    datacharts.labels = labels;
                    List<ChartDataset> chartDatasets = new List<ChartDataset>();
                    for (int i = 0; i < ykeysJ.Length; i++)
                    {
                        var chartDataset = new ChartDataset();
                        chartDataset.label = labelsJ[i];

                        float[] values = new float[dt1.Rows.Count];
                        string[] backgroundColors = new string[dt1.Rows.Count];
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            values[j] = float.Parse(dt1.Rows[j][ykeysJ[i].Trim()].ToString());

                            backgroundColors[j] = bgColorsJ[i];
                        }
                        chartDataset.data = values;
                        chartDataset.backgroundColor = backgroundColors;
                        chartDataset.fill = dt.Rows[0]["TYPE"].ToString().Equals("1") ? "false" : "true";
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                    }
                    datacharts.datasets = chartDatasets;

                    lblDataCharJs2.Value = datacharts.ToJSON();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

