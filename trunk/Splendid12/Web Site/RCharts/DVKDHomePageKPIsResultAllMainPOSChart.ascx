﻿<%@ Control CodeBehind="DVKDHomePageKPIsResultAllMainPOSChart.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.DVKDHomePageKPIsResultAllMainPOSChart" %>
<style type="text/css">
    .chartContainer {
        width: 100%;
        max-width: 1500px;
        height: 300px;
        overflow: auto;
    }
</style>
<script src="../Include/dist/Chart.bundle.js"></script>

<asp:Panel runat="server" Style="background-color: white">
    <asp:HiddenField runat="server" ID="lblName1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblType1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblDataCharJs1"></asp:HiddenField>
    <div class="chartContainer" style="background-color: white;">
        <canvas id="canvas1" ></canvas>
    </div>
    <script>
        var ctx1 = document.getElementById('canvas1').getContext('2d');
        var data1 = $('#<%=lblDataCharJs1.ClientID %>').val();
        var type1 = $('#<%=lblType1.ClientID %>').val();
        var name1 = $('#<%=lblName1.ClientID %>').val();
        var typechart1 = '';
        if (type1 == '1') typechart1 = 'line'
        else if (type1 == '2') typechart1 = 'bar'
        else if (type1 == '3') typechart1 = 'pie';        
        var config1 = {
            type: typechart1,
            data: JSON.parse(data1),
            options: {
                responsive: true,
                legend: {
                    display: true
                },
                title: {
                    display: true,
                    text: name1
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false
                        },
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.fillStyle = '#444';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data + "%", bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }
            }
        };
        var myChart1 = new Chart(ctx1, config1);

    </script>
</asp:Panel>


