﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class LeftCharts : DashletControl
    {

        protected HiddenField lblType1;
        protected HiddenField lblName1;
        protected HiddenField lblDataCharJs1;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;

            lblType1.Value = string.Empty;
            lblDataCharJs1.Value = string.Empty;
            lblName1.Value = string.Empty;

            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");
            var chartCode = "EmpHomepageCompletedPercent";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "select TOP 1 *   " + ControlChars.CrLf
                         + "  from M_Map_Report     " + ControlChars.CrLf
                         + "  WHERE 1=1    " + ControlChars.CrLf
                         + "    AND CHART_CODE = '"+ chartCode+ "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    lblName1.Value = dt.Rows[0]["NAME"].ToString();
                    lblType1.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);
                    
                    //Tungnx: must be removed
                    if (Security.CURRENT_EMPLOYEE.IS_ADMIN)
                    {
                        sqlGetDataChart = sqlGetDataChart.Replace("$MA_NHAN_VIEN", "07862");
                    }//Tungnx: must be removed

                    sqlGetDataChart = sqlGetDataChart.Replace("$MA_NHAN_VIEN", Security.CURRENT_EMPLOYEE.MA_NHAN_VIEN);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    // using char js                                        
                    var datacharts = new DataChartJs();
                    string[] labels = new string[ykeysJ.Length];
                    string[] backgroundColors = new string[bgColorsJ.Length];

                    datacharts.labels = labels;
                    List<ChartDataset> chartDatasets = new List<ChartDataset>();

                    var chartDataset = new ChartDataset();
                    float[] values = new float[ykeysJ.Length];
                    var random = new ThreadSafeRandom();
                    for (int i = 0; i < ykeysJ.Length; i++)
                    {
                        if  (dt1.Rows.Count > 0)
                        {
                            values[i] = float.Parse(dt1.Rows[0][ykeysJ[i].Trim()].ToString());
                            labels[i] = labelsJ[i].Trim().ToString();
                            backgroundColors[i] = bgColorsJ[i].Trim().ToString(); //String.Format("#{0:X6}", random.Next(0x1000000));
                        }
                    }

                    chartDataset.data = values;
                    chartDataset.backgroundColor = backgroundColors;
                    chartDataset.label = lblName1.Value;
                    chartDataset.fill = dt.Rows[0]["TYPE"].ToString().Equals("1") ? "false" : "true";
                    if (dt1.Rows.Count > 0)
                    {
                        lblName1.Value = lblName1.Value.ToString() + " " + dt1.Rows[0]["MONTH_PERIOD"].ToString() + "-" + dt1.Rows[0]["YEAR"].ToString();
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                        datacharts.datasets = chartDatasets;
                        lblDataCharJs1.Value = datacharts.ToJSON();
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                //lblError.Text = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

