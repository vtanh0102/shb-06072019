﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SplendidCRM.RCharts
{
    public class GraphData
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class GraphDataList
    {
        public List<GraphData> ListOfGraphData { get; set; }
    }

    /// <summary>
    /// Summary description for GetDataCharts
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GetDataCharts : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetData(string sql)
        {
            return "Hello World";
        }
    }
}
