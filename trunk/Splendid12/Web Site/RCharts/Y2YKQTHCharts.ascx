<%@ Control CodeBehind="Y2YKQTHCharts.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.Y2YKQTHCharts" %>

<script src="../Include/dist/Chart.bundle.js"></script>

<asp:Panel runat="server" Style="background-color: white">
    <asp:HiddenField runat="server" ID="lblNameY2YKQTH"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblTypeY2YKQTH"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblDataCharJsY2YKQTH"></asp:HiddenField>
    <div style="background-color: white;">
        <canvas id="canvasY2YKQTH" style="height:200px;"></canvas>
    </div>
    <script>
        var ctxY2YKQTH = document.getElementById('canvasY2YKQTH').getContext('2d');
        var dataY2YKQTH = $('#<%=lblDataCharJsY2YKQTH.ClientID %>').val();
        var typeY2YKQTH = $('#<%=lblTypeY2YKQTH.ClientID %>').val();
        var nameY2YKQTH = $('#<%=lblNameY2YKQTH.ClientID %>').val();
        var typechartY2YKQTH = '';
        if (typeY2YKQTH == '1') typechartY2YKQTH = 'line'
        else if (typeY2YKQTH == '2') typechartY2YKQTH = 'bar'
        else if (typeY2YKQTH == '3') typechartY2YKQTH = 'pie';
        var configY2YKQTH = {
            type: typechartY2YKQTH,
            data: JSON.parse(dataY2YKQTH),
            options: {
                maintainAspectRatio: false,
                spanGaps: false,
                title: {
                    display: true,
                    position: 'top',
                    text: nameY2YKQTH
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                elements: {
                    line: {
                        tension: 0.000001
                    }
                },
                legend: {
                    position: 'bottom',
                },
                scales: {
                    yAxes: [{
                        stacked: false
                    }]
                }
            }
        };
        var myChartY2YKQTH = new Chart(ctxY2YKQTH, configY2YKQTH);

    </script>
</asp:Panel>
<br />

