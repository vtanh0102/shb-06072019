<%@ Control CodeBehind="EmpKPIsResultChart.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.EmpKPIsResultChart" %>

<script src="../Include/dist/Chart.bundle.js"></script>
<asp:Panel runat="server" Style="background-color: white">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="true" runat="server">
        <tr>
            <td colspan="2">
               <!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<asp:HiddenField runat="server" ID="lblName2"></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="lblType2"></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="lblDataCharJs2"></asp:HiddenField>
                                        <div style="background-color: white;">
                                            <canvas id="canvas2" style="height: 220px;"></canvas>
                                        </div>
                                        <script>
                                            var ctx2 = document.getElementById('canvas2').getContext('2d');
                                            var data2 = $('#<%=lblDataCharJs2.ClientID %>').val();
                                            var type2 = $('#<%=lblType2.ClientID %>').val();
                                            var name2 = $('#<%=lblName2.ClientID %>').val();
                                            var typechart2 = '';
                                            if (type2 == '1') typechart2 = 'line'
                                            else if (type2 == '2') typechart2 = 'bar'
                                            else if (type2 == '3') typechart2 = 'pie';        
                                            var config2 = {
                                                type: typechart2,
                                                data: JSON.parse(data2),
                                                options: {
                                                    responsive: true,
                                                    legend: {
                                                        display: true
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: name2
                                                    },
                                                    tooltips: {
                                                        mode: 'index',
                                                        intersect: false,
                                                    },
                                                    hover: {
                                                        mode: 'nearest',
                                                        intersect: true
                                                    },
                                                    scales: {
                                                        xAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: false
                                                            },
                                                            ticks: {
                                                                stepSize: 1,
                                                                min: 0,
                                                                autoSkip: false
                                                            }
                                                        }],
                                                        yAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: true
                                                            },
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                    animation: {
                                                        duration: 1,
                                                        onComplete: function () {
                                                            var chartInstance = this.chart,
                                                                ctx = chartInstance.ctx;
                                                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                                            ctx.textAlign = 'center';
                                                            ctx.textBaseline = 'middle';
                                                            ctx.fillStyle = '#444';

                                                            this.data.datasets.forEach(function (dataset, i) {
                                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                                meta.data.forEach(function (bar, index) {
                                                                    var data = dataset.data[index];                            
                                                                    ctx.fillText(data + "%", bar._model.x, bar._model.y - 5);
                                                                });
                                                            });
                                                        }
                                                    }
                                                }
                                            };
                                            var myChart2 = new Chart(ctx2, config2);

                                        </script>

                                        <br>
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    
				</div>
				<!-- /.row -->
            </td>
        </tr>
        <tr>
			<td colspan="2">
				<!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										 <asp:HiddenField runat="server" ID="lblName1"></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="lblType1"></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="lblDataCharJs1"></asp:HiddenField>
                                        <div style="background-color: white;">
                                            <canvas id="canvas1" style="height: 220px;"></canvas>
                                        </div>
                                        <script>
                                            var ctx1 = document.getElementById('canvas1').getContext('2d');
                                            var data1 = $('#<%=lblDataCharJs1.ClientID %>').val();
                                            var type1 = $('#<%=lblType1.ClientID %>').val();
                                            var name1 = $('#<%=lblName1.ClientID %>').val();
                                            var typechart1 = '';
                                            if (type1 == '1') typechart1 = 'line'
                                            else if (type1 == '2') typechart1 = 'bar'
                                            else if (type1 == '3') typechart1 = 'pie';        
                                            var config1 = {
                                                type: typechart1,
                                                data: JSON.parse(data1),
                                                options: {
                                                    responsive: true,
                                                    legend: {
                                                        display: true
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: name1
                                                    },
                                                    tooltips: {
                                                        mode: 'index',
                                                        intersect: false,
                                                    },
                                                    hover: {
                                                        mode: 'nearest',
                                                        intersect: true
                                                    },
                                                    scales: {
                                                        xAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: false
                                                            },
                                                            ticks: {
                                                                stepSize: 1,
                                                                min: 0,
                                                                autoSkip: false
                                                            }
                                                        }],
                                                        yAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: true
                                                            },
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    }
                                                }
                                            };
                                            var myChart1 = new Chart(ctx1, config1);

                                        </script>
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    
				</div>
				<!-- /.row -->
			</td>
		</tr>
	</table>
</asp:Panel>


