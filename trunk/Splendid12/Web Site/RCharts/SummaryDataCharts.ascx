<%@ Control CodeBehind="SummaryDataCharts.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.SummaryDataCharts" %>

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>
<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-green"><i
			class="fas fa-money-check-alt"></i></span>
		<div class="info-box-content">
			<span class="info-box-text"><%# L10n.Term(".DARDBOAD_TOI_RESULT") %></span> 
            <asp:Label id="lbTotalToiResult" CssClass="info-box-number" runat="server"></asp:Label>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-yellow"><i
			class="fas fa-list-ol"></i></span>
		<div class="info-box-content">
			<span class="info-box-text"><%# L10n.Term(".DARDBOAD_KPI_RESULT_RANK") %></span>
			<asp:Label id="lbToiRankNumber" CssClass="info-box-number" runat="server"></asp:Label>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" style="display:none;">
		<span class="info-box-icon bg-aqua"><i
			class="fas fa-money-check-alt"></i></span>
		<div class="info-box-content">
			<span class="info-box-text">Temp1</span> <span
				class="info-box-number">90<small>%</small></span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" style="display:none;">
		<span class="info-box-icon bg-red"><i
			class="fas fa-list-ol"></i></span>
		<div class="info-box-content">
			<span class="info-box-text">Temp2</span> <span
				class="info-box-number">10/100</span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->

