using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using DocumentFormat.OpenXml.Office2013.Word;

namespace SplendidCRM.RCharts
{
    public class ThreadSafeRandom
    {
        private static readonly Random _global = new Random();
        [ThreadStatic]
        private static Random _local;

        public ThreadSafeRandom()
        {
            if (_local == null)
            {
                int seed;
                lock (_global)
                {
                    seed = _global.Next();
                }
                _local = new Random(seed);
            }
        }
        public int Next(int min, int max)
        {
            return _local.Next(min, max);
        }

        public int Next(int maxValue)
        {
            return _local.Next(maxValue);
        }
    }

    public static class JSONHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }
    }

    public class DataChartJs
    {
        public string[] labels;
        public List<ChartDataset> datasets;
    }

    public class DataLineChartJs
    {
        public string[] labels;
        public List<LineChartDataset> datasets;
    }

    public class DataMixedChartJs
    {
        public string[] labels;
        public List<MixedChartDataset> datasets;
    }

    public class ChartDataset
    {
        public string label;
        public string[] backgroundColor;
        public float[] data;
        public string type;
        public string fill = "false";
        //public string fillColor;
        //public string borderColor;
    }

    public class LineChartDataset
    {
        public string label;
        public string backgroundColor;
        public float[] data;
        public string type;
        public string fill = "false";
        public string fillColor;
        public string borderColor = "rgb(54, 162, 235)";
    }

    public class MixedChartDataset
    {
        public string label;
        public string backgroundColor;
        public float[] data;
        public string type;
        public string fill = "false";
        public int borderWidth = 1;
        public string borderColor;
    }
    /// <summary>
    ///		Summary description for ListView.
    /// </summary>
    public class ListView : SplendidControl
    {
        protected _controls.HeaderButtons ctlModuleHeader;
        protected _controls.SearchView ctlSearchView;

        protected EmpKPIsResultChart ctlEmpKPIsResultChart;
        protected EmpTOISummaryChart ctlEmpTOISummaryChart;

        protected UniqueStringCollection arrSelectFields;
        protected Label lblError;

        ListControl lstYear;
        ListControl lstMonth;
        TextBox tbFullName;
        ListControl lstPosotion;
        ListControl lstArea;
        ListControl lstRegion;
        ListControl lstOrganization;
        ListControl lstOrganizationParent;
        ListControl lstBusinessCode;

        protected void Page_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Search")
                {
                    //Check role
                    Security.FixDefaultValueByRole(
                            ctlSearchView,
                            tbFullName,
                            lstPosotion,
                            lstArea,
                            lstRegion,
                            lstOrganization,
                            lstOrganizationParent,
                            lstBusinessCode
                    );

                    this.SearchToGenerateDataChart();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(m_sMODULE + ".LBL_LIST_FORM_TITLE"));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "list") >= 0);
            if (!this.Visible)
                return;

            if (!IsPostBack)
            {
                var curMonth = DateTime.Now.ToString("MM");
                var curYear = DateTime.Now.ToString("yyyy");
                ctlSearchView.InitializeDynamicView();
                ListControl lstMonth = ctlSearchView.FindControl("MONTH_PERIOD") as ListControl;
                ListControl lstYear = ctlSearchView.FindControl("YEAR") as ListControl;
                if (lstMonth != null)
                    lstMonth.SelectedValue = curMonth;
                if (lstYear != null)
                    lstYear.SelectedValue = curYear;

                //Check role
                Security.FixDefaultValueByRole(
                            ctlSearchView,
                            tbFullName,
                            lstPosotion,
                            lstArea,
                            lstRegion,
                            lstOrganization,
                            lstOrganizationParent,
                            lstBusinessCode
                    );

                this.SearchToGenerateDataChart();

            }
        }

        private void SearchToGenerateDataChart ()
        {
            try
            {
                lstYear = ctlSearchView.FindControl("YEAR") as ListControl;
                lstMonth = ctlSearchView.FindControl("MONTH_PERIOD") as ListControl;
                tbFullName = ctlSearchView.FindControl("FULL_NAME") as TextBox;
                lstPosotion = ctlSearchView.FindControl("POSITION_ID") as ListControl;
                lstArea = ctlSearchView.FindControl("AREA_ID") as ListControl;
                lstRegion = ctlSearchView.FindControl("REGION_ID") as ListControl;
                lstOrganization = ctlSearchView.FindControl("ORGANIZATION_ID") as ListControl;
                lstOrganizationParent = ctlSearchView.FindControl("ORGANIZATION_PARENT_ID") as ListControl;

                ctlEmpKPIsResultChart.SearchEmpKPIsResultChart(lstYear, lstMonth, tbFullName, lstPosotion, lstArea, lstRegion, lstOrganization, lstOrganizationParent);
                ctlEmpKPIsResultChart.SearchEmpKPIsPlanActualChart(lstYear, lstMonth, tbFullName, lstPosotion, lstArea, lstRegion, lstOrganization, lstOrganizationParent);

                ctlEmpTOISummaryChart.SetEmpTOISummaryChart(lstYear, lstMonth, tbFullName, lstPosotion, lstArea, lstRegion, lstOrganization, lstOrganizationParent);
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                lblError.Text = ex.Message;
            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlSearchView.Command += new CommandEventHandler(Page_Command);
            m_sMODULE = "RCharts";
            SetMenu(m_sMODULE);
            arrSelectFields = new UniqueStringCollection();
            arrSelectFields.Add("ID");
            arrSelectFields.Add("ASSIGNED_USER_ID");
            arrSelectFields.Add("FAVORITE_RECORD_ID");
            arrSelectFields.Add("PENDING_PROCESS_ID");

            if (SplendidDynamic.StackedLayout(Page.Theme))
            {
                ctlModuleHeader.Command += new CommandEventHandler(Page_Command);
                ctlModuleHeader.AppendButtons(m_sMODULE + "." + LayoutListView, Guid.Empty, null);
            }
        }
        #endregion
    }
}
