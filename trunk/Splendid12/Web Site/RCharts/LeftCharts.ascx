<%@ Control CodeBehind="LeftCharts.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.LeftCharts" %>

<script src="../Include/dist/Chart.bundle.js"></script>

<asp:Panel runat="server" Style="background-color: white">
    <asp:HiddenField runat="server" ID="lblName1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblType1"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblDataCharJs1"></asp:HiddenField>

    <div style="background-color: white;">
        <canvas id="canvas1" style="height: 250px;"></canvas>
    </div>
    <script>
        //charjs  
        var ctx1 = document.getElementById('canvas1').getContext('2d');
        var data1 = $('#<%=lblDataCharJs1.ClientID %>').val();
        var type1 = $('#<%=lblType1.ClientID %>').val();
        var name1 = $('#<%=lblName1.ClientID %>').val();

        var typechart1 = '';
        if (type1 == '1') typechart1 = 'line'
        else if (type1 == '2') typechart1 = 'bar'
        else if (type1 == '3') typechart1 = 'pie';

        var config1 = {
            type: typechart1,
            data: JSON.parse(data1),
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: name1
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: false,
                        scaleLabel: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        display: false,
                        scaleLabel: {
                            display: false
                        },
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                animation: {
                    duration: 500,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset) {

                            for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius) / 2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle) / 2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                if (i == 3) { // Darker text color for lighter background
                                    ctx.fillStyle = '#444';
                                }
                                var percent = String(Math.round(dataset.data[i] / total * 100)) + "%";
                                //ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                                // Display percent in another line, line break doesn't work for fillText
                                ctx.fillText(percent, model.x + x, model.y + y + 15);
                            }
                        });
                    }
                }
            }
        };
        var myChart1 = new Chart(ctx1, config1);
    </script>
</asp:Panel>


