﻿<%@ Page language="c#" MasterPageFile="~/DefaultView.Master" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="SplendidCRM.Home.Default" %>
<script runat="server">
/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
</script>
<asp:Content ID="cntSidebar" ContentPlaceHolderID="cntSidebar" runat="server">
	<%@ Register TagPrefix="SplendidCRM" Tagname="Shortcuts" Src="~/_controls/Shortcuts.ascx" %>
	<SplendidCRM:Shortcuts ID="ctlShortcuts" SubMenu="Home" Title=".LBL_SHORTCUTS" Runat="Server" />
	<asp:PlaceHolder ID="plcSubPanelLeft" Runat="server" />
	<%= Application["CONFIG.home_left_banner"] %>
</asp:Content>

<asp:Content ID="cntBody" ContentPlaceHolderID="cntBody" runat="server">
	<asp:Table Width="100%" BorderStyle="None" BorderWidth="0" CellPadding="2" CellSpacing="0" runat="server">
		<asp:TableRow>
			<asp:TableCell Width="90%">
				<asp:Button ID="btnAddDashlets" CommandName="AddDashlets" Text='<%# L10n.Term("Home.LBL_ADD_DASHLETS") %>' OnCommand="Page_Command"  runat="server" />
				&nbsp;
				<asp:PlaceHolder Visible='<%# !Sql.ToBoolean(Application["CONFIG.hide_upgrade_warning"]) %>' runat="server">
					<asp:Label ID="lblUpgradeWarning" CssClass="error" Visible="false" runat="server" />
				</asp:PlaceHolder>
			</asp:TableCell>
			<asp:TableCell Width="10%" HorizontalAlign="Right" Wrap="false">
				<script type="text/javascript">
				function PopupHelp()
				{
					var url = document.getElementById('<%= lnkHelpText.ClientID %>').href;
					window.open(url,'helpwin','width=600,height=600,status=0,resizable=1,scrollbars=1,toolbar=0,location=1');
				}
				</script>
				<asp:PlaceHolder Visible='<%# !Sql.ToBoolean(Application["CONFIG.hide_help"]) %>' runat="server">
					<asp:HyperLink ID="lnkHelpImage" onclick="PopupHelp(); return false;" CssClass="utilsLink" Target="_blank" Runat="server">
						<asp:Image AlternateText='<%# L10n.Term(".LNK_HELP") %>' SkinID="help" Runat="server" />
					</asp:HyperLink>
					<asp:HyperLink ID="lnkHelpText" onclick="PopupHelp(); return false;" CssClass="utilsLink" Target="_blank" Runat="server"><%# L10n.Term(".LNK_HELP") %></asp:HyperLink>
				</asp:PlaceHolder>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="false" runat="server">
		<tr>
			<td width="60%" valign="top">
				<asp:PlaceHolder ID="plcSubPanelBody" Runat="server" />
			</td>
			<td style="padding-left: 10px; vertical-align: top;">
				<asp:PlaceHolder ID="plcSubPanelRight" Runat="server" />
				<%= Application["CONFIG.home_right_banner"] %>
			</td>
		</tr>
	</table>
<%
    //2018/08/19 Tungnx: check role
    if ("CRM_07".Equals(primaryRoleName))
    {
%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="true" runat="server">
        <tr>
            <td colspan="2">
                <!-- Info boxes -->
				<div class="row">					
					<%@ Register TagPrefix="SplendidCRM" TagName="SummaryDataCharts" Src="~/RCharts/SummaryDataCharts.ascx" %>
                    <SplendidCRM:SummaryDataCharts ID="SummaryDataCharts" runat="Server" />
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title"><%# L10n.Term(".DARDBOAD_KPI_RESULT") %></h3>
								<div class="box-tools pull-right">
									<button
										type="button"
										class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button
										type="button"
										class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">	
                                    <div class="col-md-5">
										<%@ Register TagPrefix="SplendidCRM" TagName="EmpLeftChart" Src="~/RCharts/LeftCharts.ascx" %>
                                        <SplendidCRM:EmpLeftChart ID="ctlEmpLeftChart" runat="Server" />
										<!-- /.chart-responsive -->
									</div>
									<!-- /.col -->								
									<div class="col-md-5">
										<%@ Register TagPrefix="SplendidCRM" TagName="EmpRightChart" Src="~/RCharts/RightCharts.ascx" %>
                                        <SplendidCRM:EmpRightChart ID="EmpRightChart" runat="Server" />
									</div>
									<!-- /.col -->                                                                        
								</div>
								<!-- /.row -->
                                <hr style="width:96%;" align="center"/>
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="Y2YKQTHChart" Src="~/RCharts/Y2YKQTHCharts.ascx" %>
                                        <SplendidCRM:Y2YKQTHChart ID="Y2YKQTHChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
            </td>
        </tr>
        <tr>
			<td colspan="2">
				
			</td>
		</tr>
		<tr>
			<td width="60%" valign="top">
				
			</td>
			<td style="padding-left: 10px; vertical-align: top;">
				
			</td>
		</tr>
	</table>
<%
    } //.CRM_07
%>

<%
    //2018/09/24 Tungnx: check role CRM_06
    if ("CRM_06".Equals(primaryRoleName))
    {
%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="true" runat="server">
        <tr>
            <td colspan="2">
               <!-- /.row -->
				<div class="row">
					<div class="col-md-6">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="DVKDHomePageKPIsResultChart" Src="~/RCharts/DVKDHomePageKPIsResultChart.ascx" %>
                                        <SplendidCRM:DVKDHomePageKPIsResultChart ID="DVKDHomePageKPIsResultChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    <div class="col-md-6">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="DVKDHomePageTop10LowestInPOSChart" Src="~/RCharts/DVKDHomePageTop10LowestInPOSChart.ascx" %>
                                        <SplendidCRM:DVKDHomePageTop10LowestInPOSChart ID="DVKDHomePageTop10LowestInPOSChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
				</div>
				<!-- /.row -->
            </td>
        </tr>
        <tr>
			<td colspan="2">
				
			</td>
		</tr>
		<tr>
			<td width="60%" valign="top">
				
			</td>
			<td style="padding-left: 10px; vertical-align: top;">
				
			</td>
		</tr>
	</table>
<%
    } //.CRM_06
%>

<%
    //2018/09/24 Tungnx: check role CRM_05
    if ("CRM_05".Equals(primaryRoleName))
    {
%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="true" runat="server">
        <tr>
            <td colspan="2">
               <!-- /.row -->
				<div class="row">
					<div class="col-md-6">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="DVKDHomePageKPIsResultMainPosChart" Src="~/RCharts/DVKDHomePageKPIsResultMainPosChart.ascx" %>
                                        <SplendidCRM:DVKDHomePageKPIsResultMainPosChart ID="DVKDHomePageKPIsResultMainPosChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    <div class="col-md-6">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="DVKDHomePageTop5LowestInMainPOSChart" Src="~/RCharts/DVKDHomePageTop5LowestInMainPOSChart.ascx" %>
                                        <SplendidCRM:DVKDHomePageTop5LowestInMainPOSChart ID="DVKDHomePageTop5LowestInMainPOSChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
				</div>
				<!-- /.row -->
            </td>
        </tr>
        <tr>
			<td colspan="2">
				
			</td>
		</tr>
		<tr>
			<td width="60%" valign="top">
				
			</td>
			<td style="padding-left: 10px; vertical-align: top;">
				
			</td>
		</tr>
	</table>
<%
    } //.CRM_05
%>

<%
    //2018/09/24 Tungnx: check role CRM_01-03
    if ("CRM_01".Equals(primaryRoleName) || "CRM_02".Equals(primaryRoleName) || "CRM_03".Equals(primaryRoleName))
    {
%>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" Visible="true" runat="server">
        <tr>
            <td colspan="2">
               <!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%@ Register TagPrefix="SplendidCRM" TagName="DVKDHomePageKPIsResultAllMainPosChart" Src="~/RCharts/DVKDHomePageKPIsResultAllMainPosChart.ascx" %>
                                        <SplendidCRM:DVKDHomePageKPIsResultAllMainPosChart ID="DVKDHomePageKPIsResultAllMainPosChart" runat="Server" />
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    
				</div>
				<!-- /.row -->
            </td>
        </tr>
        <tr>
			<td colspan="2">
				<!-- /.row -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">							
							<!-- /.box-header -->
							<div class="box-body">
								<!-- /.row -->
                                <div class="row">	
                                    <div class="col-md-12">
										<%--<%@ Register TagPrefix="SplendidCRM" TagName="EmpHomePageKPIsResultChart" Src="~/RCharts/EmpHomePageKPIsResultChart.ascx" %>
                                        <SplendidCRM:EmpHomePageKPIsResultChart ID="ctlEmpHomePageKPIsResultChart" runat="Server" />--%>
									</div>
									<!-- /.col -->	                                                                       
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
                    
				</div>
				<!-- /.row -->
			</td>
		</tr>
		<tr>
			<td width="60%" valign="top">
				
			</td>
			<td style="padding-left: 10px; vertical-align: top;">
				
			</td>
		</tr>
	</table>
<%
    } //.CRM_01-03
%>
	<%@ Register TagPrefix="SplendidCRM" Tagname="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
	<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" Runat="Server" />
</asp:Content>

