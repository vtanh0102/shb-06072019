﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0201
{

    /// <summary>
    ///		Summary description for EditView.
    /// </summary>
    public class EditView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;
        protected _controls.DynamicButtons ctlFooterButtons;

        protected Guid gID;
        protected Guid gDuplicateID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;

        protected SplendidGrid grdMain;

        protected Button loadData;
        protected ListControl ddrType;
        protected ListControl ddrPositon;
        protected ListControl ddrBusiness;
        protected Label lbPositon;
        protected ListControl ddrOrganization;
        protected Label lbOrganization;
        protected ListControl ddrSeniority;
        ListControl ddrContractType;

        protected ListControl ddlYEAR;

        protected string sqlGetDetail = "";

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtMAX_RATIO_COMPL = (TextBox)e.Item.FindControl("txtMAX_RATIO_COMPL");
                if (txtMAX_RATIO_COMPL != null)
                {
                    if ("0".Equals(txtMAX_RATIO_COMPL.Text) || "00".Equals(txtMAX_RATIO_COMPL.Text))
                    {
                        txtMAX_RATIO_COMPL.Text = string.Empty;
                    }
                }
            }
        }

        private static object _lockObject = new object();

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Save" || e.CommandName == "SaveDuplicate" || e.CommandName == "SaveConcurrency")
            {
                try
                {
                    this.ValidateEditViewFields(m_sMODULE + "." + LayoutEditView);
                    this.ApplyEditViewValidationEventRules(m_sMODULE + "." + LayoutEditView);

                    if (plcSubPanel.Visible)
                    {
                        foreach (Control ctl in plcSubPanel.Controls)
                        {
                            InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                            if (ctlSubPanel != null)
                            {
                                ctlSubPanel.ValidateEditViewFields();
                            }
                        }
                    }
                    if (Page.IsValid)
                    {
                        string sTABLE_NAME = SplendidCRM.Crm.Modules.TableName(m_sMODULE);
                        DataTable dtCustomFields = SplendidCache.FieldsMetaData_Validated(sTABLE_NAME);
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();

                            DataRow rowCurrent = null;
                            DataTable dtCurrent = new DataTable();

                            var year = new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue;
                            var position = new SplendidCRM.DynamicControl(this, rowCurrent, "POSITION_ID").ID;
                            var contracType = new SplendidCRM.DynamicControl(this, rowCurrent, "CONTRACT_TYPE").SelectedValue;
                            var seniority = new SplendidCRM.DynamicControl(this, rowCurrent, "SENIORITY_ID").SelectedValue;
                            var bussinessCode = new SplendidCRM.DynamicControl(this, rowCurrent, "BUSINESS_CODE").SelectedValue;
                            var area = new SplendidCRM.DynamicControl(this, rowCurrent, "AREA_ID_C").ID;

                            //string sSQLCheckKPIs;
                            //sSQLCheckKPIs = "select *           " + ControlChars.CrLf
                            //     + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf
                            //     + "  where YEAR = '" + year + "'" + ControlChars.CrLf
                            //     + "  AND POSITION_ID = '" + position.ToString() + "'" + ControlChars.CrLf
                            //     + "  AND APPROVE_STATUS = '" + KPIs_Constant.KPI_APPROVE_STATUS_APPROVE + "'" + ControlChars.CrLf;
                            //using (IDbCommand cmdCheckApprove = con.CreateCommand())
                            //{
                            //    cmdCheckApprove.CommandText = sSQLCheckKPIs;

                            //    using (DbDataAdapter da = dbf.CreateDataAdapter())
                            //    {
                            //        ((IDbDataAdapter)da).SelectCommand = cmdCheckApprove;
                            //        da.Fill(dtCurrent);
                            //        if (dtCurrent.Rows.Count > 0)
                            //        {
                            //            throw (new Exception(String.Format(L10n.Term("KPIB0201.ERR_KPI_STANDAR_DUPLICATE_YEAR_POSITION"))));
                            //        }
                            //    }
                            //}                           
                            if (!Sql.IsEmptyGuid(gID))
                            {
                                string sSQL;
                                sSQL = "select *           " + ControlChars.CrLf
                                     + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf;
                                using (IDbCommand cmd = con.CreateCommand())
                                {
                                    cmd.CommandText = sSQL;

                                    //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                    cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                    //Security.Filter(cmd, m_sMODULE, "edit");

                                    Sql.AppendParameter(cmd, gID, "ID", false);
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmd;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            rowCurrent = dtCurrent.Rows[0];
                                            DateTime dtLAST_DATE_MODIFIED = Sql.ToDateTime(ViewState["LAST_DATE_MODIFIED"]);
                                            if (Sql.ToBoolean(Application["CONFIG.enable_concurrency_check"]) && (e.CommandName != "SaveConcurrency") && dtLAST_DATE_MODIFIED != DateTime.MinValue && Sql.ToDateTime(rowCurrent["DATE_MODIFIED"]) > dtLAST_DATE_MODIFIED)
                                            {
                                                ctlDynamicButtons.ShowButton("SaveConcurrency", true);
                                                ctlFooterButtons.ShowButton("SaveConcurrency", true);
                                                throw (new Exception(String.Format(L10n.Term(".ERR_CONCURRENCY_OVERRIDE"), dtLAST_DATE_MODIFIED)));
                                            }
                                        }
                                        else
                                        {
                                            gID = Guid.Empty;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string sqlCheckDuplite = "select *           " + ControlChars.CrLf
                               + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf
                               + "  where YEAR = '" + year + "'" + ControlChars.CrLf
                               + "  AND POSITION_ID = '" + position.ToString() + "'" + ControlChars.CrLf
                               + "  AND CONTRACT_TYPE = '" + contracType.ToString() + "'" + ControlChars.CrLf
                               + "  AND SENIORITY_ID = '" + seniority.ToString() + "'" + ControlChars.CrLf
                               + "  AND BUSINESS_CODE = '" + bussinessCode.ToString() + "'" + ControlChars.CrLf
                               + "  AND AREA_ID_C = '" + area.ToString() + "'" + ControlChars.CrLf;

                                using (IDbCommand cmdCheckSave = con.CreateCommand())
                                {
                                    cmdCheckSave.CommandText = sqlCheckDuplite;
                                    using (DbDataAdapter da = dbf.CreateDataAdapter())
                                    {
                                        ((IDbDataAdapter)da).SelectCommand = cmdCheckSave;
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            throw (new Exception(String.Format(L10n.Term("KPIB0201.ERR_KPI_STANDAR_DUPLICATE_ALL"))));
                                        }
                                    }
                                }

                            }

                            this.ApplyEditViewPreSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                            bool bDUPLICATE_CHECHING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && Sql.ToBoolean(Application["Modules." + m_sMODULE + ".DuplicateCheckingEnabled"]) && (e.CommandName != "SaveDuplicate");
                            if (bDUPLICATE_CHECHING_ENABLED)
                            {
                                if (Utils.DuplicateCheck(Application, con, m_sMODULE, gID, this, rowCurrent) > 0)
                                {
                                    ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                    ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                    throw (new Exception(L10n.Term(".ERR_DUPLICATE_EXCEPTION")));
                                }
                            }                           

                            if (grdMain.Items.Count <= 0)
                            {
                                throw (new Exception(L10n.Term("KPIB0201.ERR_GROUP_KPI_NOT_EXIST")));
                            }

                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    Guid gASSIGNED_USER_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ASSIGNED_USER_ID").ID;
                                    if (Sql.IsEmptyGuid(gASSIGNED_USER_ID))
                                        gASSIGNED_USER_ID = Security.USER_ID;
                                    Guid gTEAM_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_ID").ID;

                                    var ORGANIZATION_ID = new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID;
                                    string ORGANIZATION_CODE = KPIs_Utils.getNameFromGuidID(ORGANIZATION_ID, "M_ORGANIZATION", "ORGANIZATION_CODE");
                                    var isBASE = new SplendidCRM.DynamicControl(this, rowCurrent, "IS_BASE").Text;

                                    if (Sql.IsEmptyGuid(gTEAM_ID))
                                        gTEAM_ID = Security.TEAM_ID;

                                    lock (_lockObject)
                                    {
                                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_standard_code_prefix"]);
                                        string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_STANDARD_List", "KPI_STANDARD_CODE", ddlYEAR != null ? ddlYEAR.SelectedValue : "");
                                        //bool bDUPLICATE_CHECKING_ENABLED = Sql.ToBoolean(Application["CONFIG.enable_duplicate_check"]) && (e.CommandName != "SaveDuplicate") && Sql.IsEmptyGuid(gID);
                                        //if (bDUPLICATE_CHECKING_ENABLED)
                                        //{
                                        //    //validate duplicate code                                            
                                        //    if (Utils.DuplicateCheckCode(Application, con, m_sMODULE, strKPIsCode, "KPI_STANDARD_CODE", this, rowCurrent) > 0)
                                        //    {
                                        //        //ctlDynamicButtons.ShowButton("SaveDuplicate", true);
                                        //        //ctlFooterButtons.ShowButton("SaveDuplicate", true);
                                        //        throw (new Exception(string.Format(L10n.Term(".ERR_DUPLICATE_CODE_EXCEPTION"), strKPIsCode)));
                                        //    }
                                        //}

                                        string kpiStrandarCode = new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_STANDARD_CODE").Text;

                                        KPIB0201_SqlProc.spB_KPI_STANDARD_Update
                                            (ref gID
                                            , gASSIGNED_USER_ID
                                            , gTEAM_ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TEAM_SET_LIST").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_STANDARD_NAME").Text
                                            , string.IsNullOrEmpty(kpiStrandarCode) ? strKPIsCode : kpiStrandarCode
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "YEAR").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "VERSION_NUMBER").Text
                                            , KPIs_Constant.KPI_APPROVE_STATUS_DONTSEND
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_BY").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "APPROVED_DATE").DateValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "SENIORITY_ID").SelectedValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TYPE").IntegerValue
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "POSITION_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "BUSINESS_CODE").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "ORGANIZATION_ID").ID
                                            , ORGANIZATION_CODE
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "CONTRACT_TYPE").Text
                                            , isBASE == string.Empty ? KPIs_Constant.KPI_IS_BASE_ACTIVE : isBASE
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "BASE_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "DESCRIPTION").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "REMARK").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "STATUS").Text
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "GROUP_KPI_ID").ID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "TAG_SET_NAME").Text
                                            , trn
                                            );

                                        if (ViewState["CurrentTable"] != null)
                                        {
                                            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                                            float totalRatio = 0;
                                            if (dtCurrentTable.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                                                {
                                                    HiddenField hdnfID = (HiddenField)grdMain.Items[i].FindControl("txtID");
                                                    HiddenField hdnGroupKpiDetailID = (HiddenField)grdMain.Items[i].FindControl("txtGROUP_KPI_DETAIL_ID");
                                                    Label kpiName = (Label)grdMain.Items[i].FindControl("txtKPI_NAME");
                                                    Label unit = (Label)grdMain.Items[i].FindControl("txtKPI_UNIT");
                                                    HiddenField kpiID = (HiddenField)grdMain.Items[i].FindControl("txtKPI_ID");
                                                    HiddenField kpiCode = (HiddenField)grdMain.Items[i].FindControl("txtKPI_CODE");
                                                    HiddenField levelNumber = (HiddenField)grdMain.Items[i].FindControl("txtLEVEL_NUMBER");
                                                    HiddenField unitID = (HiddenField)grdMain.Items[i].FindControl("hdUnitId");
                                                    TextBox ratio = (TextBox)grdMain.Items[i].FindControl("txtKPI_RATIO");
                                                    TextBox valuePerMonth = (TextBox)grdMain.Items[i].FindControl("txtVALUE_STD_PER_MONTH");
                                                    TextBox maxRatioComplete = (TextBox)grdMain.Items[i].FindControl("txtMAX_RATIO_COMPL");
                                                    TextBox description = (TextBox)grdMain.Items[i].FindControl("txtDESCRIPTION");
                                                    Guid gIDT;
                                                    Guid gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                                                    if (!Sql.IsEmptyGuid(gDuplicateID))
                                                    {
                                                        gIDT = Guid.NewGuid();
                                                    }
                                                    else
                                                    {
                                                        if (hdnfID.Value == null || hdnfID.Value == string.Empty)
                                                        {
                                                            gIDT = Guid.NewGuid();
                                                        }
                                                        else
                                                        {
                                                            gIDT = Guid.Parse(hdnfID.Value);
                                                        }
                                                    }

                                                    if (ratio.Text == string.Empty)
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_RATIO"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                    }

                                                    float fRatio = 0;
                                                    if (!float.TryParse(ratio.Text, out fRatio))
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_RATIO"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                                    }

                                                    if (fRatio <= 0)
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_RATIO"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                                    }

                                                    totalRatio += fRatio;

                                                    if (valuePerMonth.Text == string.Empty)
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_VALUE_STD_PER_MONTH"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                    }

                                                    float valueMonth = 0;
                                                    if (!float.TryParse(valuePerMonth.Text, out valueMonth))
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_VALUE_STD_PER_MONTH"), L10n.Term(".ERR_INVALID_DECIMAL"))));
                                                    }

                                                    if (valueMonth < 0)
                                                    {
                                                        throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_VALUE_STD_PER_MONTH"), L10n.Term("KPIB020101.ERR_VALUE_PER_MONTH_NOT_IN_RANGE"))));
                                                    }

                                                    //if (maxRatioComplete.Text == string.Empty)
                                                    //{
                                                    //    throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_VALUE_STD_PER_MONTH"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                    //}

                                                    //float maxComplete = 0;
                                                    //if (!float.TryParse(maxRatioComplete.Text, out maxComplete))
                                                    //{
                                                    //    throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_MAX_RATIO_COMPLETE"), L10n.Term(".ERR_REQUIRED_FIELD"))));
                                                    //}

                                                    //if (maxComplete < 100)
                                                    //{
                                                    //    throw (new Exception(string.Format("{0} {1}", L10n.Term("KPIB020101.LBL_LIST_MAX_RATIO_COMPLETE"), L10n.Term("KPIB020101.ERR_MAX_RATIO_COMP_NOT_IN_RANGE"))));
                                                    //}

                                                    var level = 0;
                                                    int.TryParse(levelNumber.Value, out level);

                                                    KPIB0201_SqlProc.spB_KPI_STANDARD_DETAILS_Update
                                                         (ref gIDT
                                                         , gASSIGNED_USER_ID
                                                         , gTEAM_ID
                                                         , string.Empty
                                                         , kpiName.Text
                                                         , gID
                                                         , Guid.Parse(kpiID.Value)
                                                         , kpiCode.Value
                                                         , level
                                                         , Sql.ToInteger(unitID.Value)
                                                         , Sql.ToFloat(ratio.Text)
                                                         , Sql.ToFloat(maxRatioComplete.Text)
                                                         , hdnGroupKpiDetailID.Value
                                                         , description.Text
                                                         , string.Empty
                                                         , Sql.ToDouble(valuePerMonth.Text)
                                                         , string.Empty
                                                         , trn
                                                         );
                                                }

                                                if (totalRatio != 100)
                                                {
                                                    throw (new Exception(string.Format("{0}", L10n.Term("KPIB020101.ERR_TOTAL_VALUE_PER_MONTH_NOT_EXC"))));
                                                }

                                                ViewState["CurrentTable"] = dtCurrentTable;
                                            }
                                            else
                                            {
                                                trn.Rollback();

                                                throw (new Exception(string.Format("{0}", L10n.Term("KPIB0201.ERR_KPI_HAS_NEED"))));
                                            }
                                        }

                                        SplendidDynamic.UpdateCustomFields(this, trn, gID, sTABLE_NAME, dtCustomFields);
                                        SplendidCRM.SqlProcs.spTRACKER_Update
                                            (Security.USER_ID
                                            , m_sMODULE
                                            , gID
                                            , new SplendidCRM.DynamicControl(this, rowCurrent, "KPI_STANDARD_NAME").Text
                                            , "save"
                                            , trn
                                            );
                                        if (plcSubPanel.Visible)
                                        {
                                            foreach (Control ctl in plcSubPanel.Controls)
                                            {
                                                InlineEditControl ctlSubPanel = ctl as InlineEditControl;
                                                if (ctlSubPanel != null)
                                                {
                                                    ctlSubPanel.Save(gID, m_sMODULE, trn);
                                                }
                                            }
                                        }
                                        trn.Commit();
                                        SplendidCache.ClearFavorites();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText = ex.Message;
                                    return;
                                }
                            }
                            rowCurrent = SplendidCRM.Crm.Modules.ItemEdit(m_sMODULE, gID);
                            this.ApplyEditViewPostSaveEventRules(m_sMODULE + "." + LayoutEditView, rowCurrent);
                        }

                        if (!Sql.IsEmptyString(RulesRedirectURL))
                            Response.Redirect(RulesRedirectURL);
                        else
                            Response.Redirect("view.aspx?ID=" + gID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                    ctlDynamicButtons.ErrorText = ex.Message;
                }
            }
            else if (e.CommandName == "Cancel")
            {
                if (Sql.IsEmptyGuid(gID))
                    Response.Redirect("default.aspx");
                else
                    Response.Redirect("view.aspx?ID=" + gID.ToString());
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "edit") >= 0);
            if (!this.Visible)
                return;

            try
            {

                gID = Sql.ToGuid(Request["ID"]);

                loadData.Visible = Sql.IsEmptyGuid(gID);
                loadData.Text = L10n.Term("KPIB0201.LOAD_DATA");
                if (!IsPostBack)
                {
                    gDuplicateID = Sql.ToGuid(Request["DuplicateID"]);
                    if (!Sql.IsEmptyGuid(gID) || !Sql.IsEmptyGuid(gDuplicateID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            if (!Sql.IsEmptyGuid(gDuplicateID))
                            {
                                sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf;
                                //+ " where ID = '" + gDuplicateID.ToString() + "'        " + ControlChars.CrLf
                                //+ " AND ISNULL(APPROVE_STATUS,'') <> '" + KPIs_Constant.KPI_APPROVE_STATUS_APPROVE + "'        " + ControlChars.CrLf;
                                sSQL += string.Format(" where ID = '{0}' AND ISNULL(APPROVE_STATUS,'') <> '{1}' AND ISNULL(APPROVE_STATUS,'') <> '{2}'", gDuplicateID.ToString(), KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                            }
                            else
                            {
                                sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf;
                                //+ " where ID = '" + gID.ToString() + "'        " + ControlChars.CrLf
                                //+ " AND ISNULL(APPROVE_STATUS,'') <> '" + KPIs_Constant.KPI_APPROVE_STATUS_APPROVE + "'        " + ControlChars.CrLf;
                                sSQL += string.Format(" where ID = '{0}' AND ISNULL(APPROVE_STATUS,'') <> '{1}' AND ISNULL(APPROVE_STATUS,'') <> '{2}'", gID.ToString(), KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT);
                            }
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;
                                //Security.Filter(cmd, m_sMODULE, "edit");
                                //if (!Sql.IsEmptyGuid(gDuplicateID))
                                //{
                                //    Sql.AppendParameter(cmd, gDuplicateID, "ID", false);
                                //    gID = Guid.Empty;
                                //}
                                //else
                                //{
                                //    Sql.AppendParameter(cmd, gID, "ID", false);
                                //}
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyEditViewPreLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["KPI_STANDARD_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                                            this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, rdr);
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, gASSIGNED_USER_ID, rdr);
                                            TextBox txtNAME = this.FindControl("KPI_STANDARD_NAME") as TextBox;
                                            if (txtNAME != null)
                                                txtNAME.Focus();

                                            ViewState["LAST_DATE_MODIFIED"] = Sql.ToDateTime(rdr["DATE_MODIFIED"]);
                                            ViewState["KPI_STANDARD_NAME"] = Sql.ToString(rdr["KPI_STANDARD_NAME"]);
                                            ViewState["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;
                                            Page.Items["KPI_STANDARD_NAME"] = ViewState["KPI_STANDARD_NAME"];
                                            Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];

                                            this.ApplyEditViewPostLoadEventRules(m_sMODULE + "." + LayoutEditView, rdr);
                                            if (!Sql.IsEmptyGuid(gDuplicateID))
                                            {
                                                //code
                                                Label txtCODE = this.FindControl("KPI_STANDARD_CODE") as Label;
                                                string code_prefix = Sql.ToString(Application["CONFIG.kpi_standard_code_prefix"]);
                                                //string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_STANDARD_List", "KPI_STANDARD_CODE", ddlYEAR != null ? ddlYEAR.SelectedValue : DateTime.Now.Year.ToString());
                                                if (txtCODE != null)
                                                {
                                                    txtCODE.Text = string.Empty;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlFooterButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                            plcSubPanel.Visible = false;
                                            return;
                                        }
                                    }
                                }
                            }
                        }


                        if (Sql.IsEmptyGuid(gID))
                        {
                            Load_KPI_Stander_Detail(gDuplicateID);
                        }
                        else
                        {
                            ddlYEAR = this.FindControl("YEAR") as ListControl;
                            if (ddlYEAR != null)
                            {
                                ddlYEAR.Enabled = false;
                            }

                            ddrType = this.FindControl("TYPE") as ListControl;
                            if (ddrType != null)
                            {
                                ddrType.Enabled = false;
                            }
                            ddrBusiness = this.FindControl("BUSINESS_CODE") as ListControl;
                            if (ddrBusiness != null)
                            {
                                ddrBusiness.Enabled = false;
                            }
                            Load_KPI_Stander_Detail(gID);
                        }

                    }
                    else
                    {
                        this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                        this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                        TextBox txtNAME = this.FindControl("KPI_STANDARD_NAME") as TextBox;
                        if (txtNAME != null)
                            txtNAME.Focus();
                        //code
                        Label txtCODE = this.FindControl("KPI_STANDARD_CODE") as Label;
                        string code_prefix = Sql.ToString(Application["CONFIG.kpi_standard_code_prefix"]);
                        //string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_STANDARD_List", "KPI_STANDARD_CODE", ddlYEAR != null ? ddlYEAR.SelectedValue : "");
                        if (txtCODE != null)
                        {
                            txtCODE.Text = string.Empty;
                        }

                        this.ApplyEditViewNewEventRules(m_sMODULE + "." + LayoutEditView);

                        grdMain.DataSource = new DataTable();
                        grdMain.DataBind();
                    }

                    if (ddrType != null && ddrPositon != null)
                        ddrType_SelectIndexChange(null, null);
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                    Page.Items["KPI_STANDARD_NAME"] = ViewState["KPI_STANDARD_NAME"];
                    Page.Items["ASSIGNED_USER_ID"] = ViewState["ASSIGNED_USER_ID"];
                }

                //Positon
                ddrPositon = this.FindControl("POSITION_ID") as ListControl;
                //type
                ddrType = this.FindControl("TYPE") as ListControl;
                // tham nien
                ddrSeniority = this.FindControl("SENIORITY_ID") as ListControl;
                //organization 
                //ddrOrganization = this.FindControl("ORGANIZATION_ID") as ListControl;
                //loai hop dong
                ddrContractType = this.FindControl("CONTRACT_TYPE") as ListControl;
                //Busines
                ddrBusiness = this.FindControl("BUSINESS_CODE") as ListControl;
                if (ddrBusiness != null)
                {
                    ddrBusiness.AutoPostBack = true;
                    ddrBusiness.SelectedIndexChanged += new EventHandler(ddrBusiness_SelectIndexChange);
                }

                if (ddrType != null)
                {
                    ddrType.AutoPostBack = true;
                    ddrType.SelectedIndexChanged += new EventHandler(ddrType_SelectIndexChange);
                }

                ddlYEAR = this.FindControl("YEAR") as ListControl;
                if (ddlYEAR != null)
                {
                    ddlYEAR.AutoPostBack = true;
                }

                if (Sql.IsEmptyGuid(gID) || Sql.IsEmptyGuid(gDuplicateID))
                {
                    ListControl ddrArea = this.FindControl("AREA_ID_C") as ListControl;
                    if (ddrArea != null)
                    {
                        ddrArea.SelectedValue = "07fe7a09-4df1-4fa1-8d44-8d0c52ebef96";
                        ddrArea.Enabled = false;
                    }
                }

                //grdMain.DataSource = null;
                //grdMain.DataBind();

                //if (ddrSeniority != null)
                //{
                //    ddrSeniority.AutoPostBack = true;
                //    ddrSeniority.SelectedIndexChanged += new EventHandler(ddrSeniority_SelectIndexChange);
                //}

                //lbPositon = this.FindControl("POSITION_ID_LABEL") as Label;
                //if (ddrPositon != null)
                //{
                //    ddrPositon.AutoPostBack = true;
                //    ddrPositon.SelectedIndexChanged += new EventHandler(ddrPositon_SelectIndexChange);
                //}

                //lbOrganization = this.FindControl("ORGANIZATION_ID_LABEL") as Label;
                //if (ddrOrganization != null)
                //{
                //    ddrOrganization.AutoPostBack = true;
                //    ddrOrganization.SelectedIndexChanged += new EventHandler(ddrOrganization_SelectIndexChange);
                //}                  
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void ddrBusiness_SelectIndexChange(object sender, EventArgs e)
        {
            grdMain.DataSource = null;
            grdMain.DataBind();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This Task is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlFooterButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            m_sMODULE = "KPIB0201";
            SetMenu(m_sMODULE);

            if (IsPostBack)
            {
                this.AppendEditViewRelationships(m_sMODULE + "." + LayoutEditView, plcSubPanel, Sql.IsEmptyGuid(Request["ID"]));
                this.AppendEditViewFields(m_sMODULE + "." + LayoutEditView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                ctlFooterButtons.AppendButtons(m_sMODULE + "." + LayoutEditView, Guid.Empty, null);
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion

        #region event add more

        protected void ddlYEAR_SelectIndexChange(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlYEAR.SelectedValue))
            {
                string code_prefix = Sql.ToString(Application["CONFIG.kpi_standard_code_prefix"]);
                string strKPIsCode = KPIs_Utils.GenerateKPIsCode(code_prefix, "vwB_KPI_STANDARD_List", "KPI_STANDARD_CODE", ddlYEAR.SelectedValue);
                Label txtCODE = this.FindControl("KPI_STANDARD_CODE") as Label;
                if (txtCODE != null) txtCODE.Text = strKPIsCode;
            }
        }


        protected void ddrType_SelectIndexChange(object sender, EventArgs e)
        {
            if (ddrType.SelectedValue != null)
            {
                int sl = -1;
                Int32.TryParse(ddrType.SelectedValue, out sl);
                if (string.IsNullOrEmpty(ddrType.SelectedValue))
                {
                    //ddrOrganization.Enabled = false;
                    ddrPositon.Enabled = false;
                    return;
                }

                if (sl == 0)
                {
                    //ddrOrganization.Enabled = false;
                    ddrPositon.Enabled = true;

                    ddrPositon.DataSource = SplendidCache.M_POSITION();
                    ddrPositon.DataTextField = "POSITION_NAME";
                    ddrPositon.DataValueField = "ID";
                    ddrPositon.DataBind();
                    return;
                }

                //if (sl == 1 || sl == 2 || sl == 3 || sl == 4)
                //{
                //    ddrOrganization.Enabled = true;
                //    ddrPositon.Enabled = false;

                //    var dtSource = SplendidCache.M_ORGANIZATION(sl);
                //    if (dtSource != null && dtSource.Rows.Count > 0)
                //    {
                //        ddrOrganization.DataSource = dtSource;
                //        ddrOrganization.DataTextField = "ORGANIZATION_NAME";
                //        ddrOrganization.DataValueField = "ID";
                //        ddrOrganization.DataBind();
                //    }
                //    return;
                //}
            }
        }

        protected void ddrSeniority_SelectIndexChange(object sender, EventArgs e)
        {
            Load_KPI_Stander_Detail(gID);
        }

        private void Load_KPI_Stander_Detail(Guid gID)
        {
            if (!Sql.IsEmptyGuid(gID) || (ddrPositon != null && ddrPositon.SelectedValue != ""))
            {
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                string sSQL_DETAILS = "";

                //DETAILS LIST                           
                using (IDbConnection con = dbf.CreateConnection())
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        if (Sql.IsEmptyGuid(gID))
                        {
                            if (ddrType != null)
                            {
                                var sqlMore = " AND YEAR='" + ddlYEAR.SelectedValue + "' ";
                                if (ddrBusiness != null && !string.IsNullOrEmpty(ddrBusiness.SelectedValue))
                                {
                                    sqlMore += string.Format(" AND BUSINESS_CODE='{0}' AND CONTRACT_TYPE_C='{1}'", ddrBusiness.SelectedValue, ddrContractType.SelectedValue);
                                }
                                int sl = -1;
                                Int32.TryParse(ddrType.SelectedValue, out sl);
                                if (sl == 0)
                                {
                                    sqlGetDetail = "APPROVE_STATUS = '" + KPIs_Constant.KPI_APPROVE_STATUS_APPROVE + "' AND (POSITION_ID = '" + ddrPositon.SelectedValue + "' OR (IS_COMMON = 1 AND TYPE = 0)) " + sqlMore;
                                }
                                //if (sl == 1 || sl == 2 || sl == 3 || sl == 4)
                                //{
                                //    sqlGetDetail = "(ORGANIZATION_ID = '" + ddrOrganization.SelectedValue + "' OR (IS_COMMON = 1 AND TYPE = '" + sl + "')) " + sqlMore;
                                //}

                                sSQL_DETAILS = "SELECT Row_Number() OVER(order by DATE_ENTERED) AS NO , '' AS ID, ID AS GROUP_KPI_DETAIL_ID, KPI_ID, KPI_CODE, '' AS LEVEL_NUMBER, KPI_NAME, UNIT AS KPI_UNIT, RATIO, MAX_RATIO_COMPLETE, DESCRIPTION, REMARK, '' AS VALUE_STD_PER_MONTH     " + ControlChars.CrLf
                                     + "  FROM vwM_GROUP_KPI_DETAILS_List  WHERE GROUP_KPI_ID IN (SELECT DISTINCT [ID] FROM vwM_GROUP_KPIS_Edit WHERE " + sqlGetDetail + ") " + ControlChars.CrLf;

                                cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                                cmd.CommandText = sSQL_DETAILS;
                            }
                        }
                        if (!Sql.IsEmptyGuid(gID))
                        {
                            sSQL_DETAILS = "SELECT Row_Number() OVER(order by DATE_ENTERED) AS NO, ID, GROUP_KPI_DETAIL_ID, KPI_NAME, KPI_ID, KPI_CODE, LEVEL_NUMBER, KPI_UNIT, RATIO, MAX_RATIO_COMPLETE, DESCRIPTION, REMARK, VALUE_STD_PER_MONTH    " + ControlChars.CrLf
                                                + "  FROM vwB_KPI_STANDARD_DETAILS_Edit " + ControlChars.CrLf;
                            cmd.CommandText = sSQL_DETAILS;

                            //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                            cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                            //Security.Filter(cmd, m_sMODULE, "edit");

                            Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                            cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                        }
                        //Security.Filter(cmd, m_sMODULE, "edit");

                        //Sql.AppendParameter(cmd, 0, "DELETED", false);//NO SELECT DELETED                    
                        con.Open();

                        if (bDebug)
                            RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            using (DataTable dtCurrent = new DataTable())
                            {
                                da.Fill(dtCurrent);
                                grdMain.DataSource = dtCurrent;
                                grdMain.DataBind();
                                if (dtCurrent.Rows.Count > 0)
                                {
                                    string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                                    ViewState["CurrentTable"] = dtCurrent;
                                    for (int i = 0; i < grdMain.Items.Count; i++)
                                    {
                                        //FirstDataGridViewRow();
                                        Label lblNO = (Label)grdMain.Items[i].FindControl("lblNO");
                                        HiddenField id = (HiddenField)grdMain.Items[i].FindControl("txtID");
                                        HiddenField groupKpiDetailID = (HiddenField)grdMain.Items[i].FindControl("txtGROUP_KPI_DETAIL_ID");
                                        Label kpiName = (Label)grdMain.Items[i].FindControl("txtKPI_NAME");
                                        Label unit = (Label)grdMain.Items[i].FindControl("txtKPI_UNIT");
                                        HiddenField unitID = (HiddenField)grdMain.Items[i].FindControl("hdUnitId");

                                        HiddenField kpiID = (HiddenField)grdMain.Items[i].FindControl("txtKPI_ID");
                                        HiddenField kpiCode = (HiddenField)grdMain.Items[i].FindControl("txtKPI_CODE");
                                        HiddenField levelNumber = (HiddenField)grdMain.Items[i].FindControl("txtLEVEL_NUMBER");

                                        TextBox ratio = (TextBox)grdMain.Items[i].FindControl("txtRATIO");
                                        TextBox valuePerMonth = (TextBox)grdMain.Items[i].FindControl("txtVALUE_STD_PER_MONTH");
                                        TextBox maxRatioComplete = (TextBox)grdMain.Items[i].FindControl("txtMAX_RATIO_COMPL");
                                        TextBox description = (TextBox)grdMain.Items[i].FindControl("txtDESCRIPTION");
                                        //SET VALUE
                                        lblNO.Text = (i + 1).ToString();
                                        if (!Sql.IsEmptyGuid(gID))
                                            id.Value = dtCurrent.Rows[i]["ID"].ToString();
                                        else
                                            id.Value = string.Empty;
                                        groupKpiDetailID.Value = dtCurrent.Rows[i]["GROUP_KPI_DETAIL_ID"].ToString();
                                        kpiName.Text = dtCurrent.Rows[i]["KPI_NAME"].ToString();
                                        unit.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", dtCurrent.Rows[i]["KPI_UNIT"].ToString());
                                        unitID.Value = dtCurrent.Rows[i]["KPI_UNIT"].ToString();
                                        string valueMonth = dtCurrent.Rows[i]["VALUE_STD_PER_MONTH"] != null ? dtCurrent.Rows[i]["VALUE_STD_PER_MONTH"].ToString() : string.Empty;
                                        string maxRatio = dtCurrent.Rows[i]["MAX_RATIO_COMPLETE"] != null ? dtCurrent.Rows[i]["MAX_RATIO_COMPLETE"].ToString() : string.Empty;
                                        valuePerMonth.Text = KPIs_Utils.FormatFloat(valueMonth, format_number);
                                        maxRatioComplete.Text = KPIs_Utils.FormatFloat(maxRatio, format_number);
                                        description.Text = dtCurrent.Rows[i]["DESCRIPTION"].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                //End Details list
            }
        }
        #endregion

        protected void LoaData_Click(object sender, EventArgs e)
        {
            Load_KPI_Stander_Detail(gID);
        }
    }
}
