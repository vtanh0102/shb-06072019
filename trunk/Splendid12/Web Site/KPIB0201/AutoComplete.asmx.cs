using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Web.Services;
using System.ComponentModel;
using SplendidCRM;

namespace SplendidCRM.KPIB0201
{
	public class KPIB0201
	{
		public Guid    ID  ;
		public string  KPI_STANDARD_NAME;

		public KPIB0201()
		{
			ID   = Guid.Empty  ;
			KPI_STANDARD_NAME = String.Empty;
		}
	}

	/// <summary>
	/// Summary description for AutoComplete
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.Web.Script.Services.ScriptService]
	[ToolboxItem(false)]
	public class AutoComplete : System.Web.Services.WebService
	{
		[WebMethod(EnableSession=true)]
		public KPIB0201 B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Get(string sKPI_STANDARD_NAME)
		{
			KPIB0201 item = new KPIB0201();
			//try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					string sSQL;
					sSQL = "select ID        " + ControlChars.CrLf
					     + "     , KPI_STANDARD_NAME      " + ControlChars.CrLf
					     + "  from vwB_KPI_STANDARD" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, "KPIB0201", "list");

                        Sql.AppendParameter(cmd, sKPI_STANDARD_NAME, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "KPI_STANDARD_NAME");
						cmd.CommandText += " order by KPI_STANDARD_NAME" + ControlChars.CrLf;
						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								item.ID   = Sql.ToGuid   (rdr["ID"  ]);
								item.KPI_STANDARD_NAME = Sql.ToString (rdr["KPI_STANDARD_NAME"]);
							}
						}
					}
				}
				if ( Sql.IsEmptyGuid(item.ID) )
				{
					string sCULTURE = Sql.ToString (Session["USER_SETTINGS/CULTURE"]);
					L10N L10n = new L10N(sCULTURE);
					throw(new Exception(L10n.Term("KPIB0201.ERR_B_KPI_STANDARD_NOT_FOUND")));
				}
			}
			//catch
			{
				// 02/04/2007 Paul.  Don't catch the exception.  
				// It is a web service, so the exception will be handled properly by the AJAX framework. 
			}
			return item;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_List(string prefixText, int count)
		{
			string[] arrItems = new string[0];
			try
			{
				if ( !Security.IsAuthenticated() )
					throw(new Exception("Authentication required"));

				SplendidCRM.DbProviderFactory dbf = SplendidCRM.DbProviderFactories.GetFactory();
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					string sSQL;
					sSQL = "select distinct  " + ControlChars.CrLf
					     + "       KPI_STANDARD_NAME      " + ControlChars.CrLf
					     + "  from vwB_KPI_STANDARD" + ControlChars.CrLf;
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						cmd.CommandText = sSQL;

                        //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                        cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                        //Security.Filter(cmd, "KPIB0201", "list");

                        Sql.AppendParameter(cmd, prefixText, (Sql.ToBoolean(Application["CONFIG.AutoComplete.Contains"]) ? Sql.SqlFilterMode.Contains : Sql.SqlFilterMode.StartsWith), "KPI_STANDARD_NAME");
						cmd.CommandText += " order by KPI_STANDARD_NAME" + ControlChars.CrLf;
						using ( DbDataAdapter da = dbf.CreateDataAdapter() )
						{
							((IDbDataAdapter)da).SelectCommand = cmd;
							using ( DataTable dt = new DataTable() )
							{
								da.Fill(0, count, dt);
								arrItems = new string[dt.Rows.Count];
								for ( int i=0; i < dt.Rows.Count; i++ )
									arrItems[i] = Sql.ToString(dt.Rows[i]["KPI_STANDARD_NAME"]);
							}
						}
					}
				}
			}
			catch
			{
			}
			return arrItems;
		}

		[WebMethod(EnableSession=true)]
		public string[] B_KPI_STANDARD_KPI_STANDARD_NAME_List(string prefixText, int count)
		{
			return B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_List(prefixText, count);
		}
	}
}

