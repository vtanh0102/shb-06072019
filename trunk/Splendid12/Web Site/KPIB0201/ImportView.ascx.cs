
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.Workflow.Activities.Rules;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0201
{
    /// <summary>
    ///		Summary description for ImportView.
    /// </summary>
    public class ImportView : SplendidControl
    {
        #region Properties
        // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected PlaceHolder phDefaultsView;
        protected SplendidControl ctlDefaultsView;

        protected Guid gID;

        protected SplendidGrid grdMain;

        protected XmlDocument xml;
        protected XmlDocument xmlMapping;
        protected string sImportModule;
        protected HtmlInputFile fileIMPORT;
        protected RequiredFieldValidator reqFILENAME;
        protected CheckBox chkHasHeader;
        protected HtmlTable tblImportMappings;
        protected StringBuilder sbImport;

        protected Label lblStatus;
        protected Label lblSuccessCount;
        protected Label lblDuplicateCount;
        protected Label lblFailedCount;
        protected CheckBox chkUseTransaction;

        protected HiddenField txtACTIVE_TAB;
        protected bool bDuplicateFields = false;
        protected int nMAX_ERRORS = 200;

        protected DataTable dtRuleColumns;

        public string Module
        {
            get { return sImportModule; }
            set { sImportModule = value; }
        }
        #endregion


        protected string SourceType()
        {
            string sSourceType = "excel";
            return sSourceType;
        }

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            string format_number = Sql.ToString(Application["CONFIG.format_number"]);
            Label lblCONVERSION_RATE = (Label)e.Item.FindControl("lblCONVERSION_RATE");
            if (lblCONVERSION_RATE != null)
            {
                lblCONVERSION_RATE.Text = KPIs_Utils.FormatFloat(lblCONVERSION_RATE.Text, format_number);
            }
        }
        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Import.Run")
                {
                    if (Page.IsValid)
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();

                        int year = DateTime.Now.Year;
                        DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                        if (ddlYEAR != null)
                        {
                            year = Sql.ToInteger(ddlYEAR.SelectedValue);
                        }
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            con.Open();

                            
                            //28/10/2018 Tungnx: Bo dieu kien check co phan giao bat ky thi khong cho Sinh KPI
                            // check dk
                            //string sqlDataAllocate = "select *           " + ControlChars.CrLf
                            //  + "  from vwB_KPI_ALLOCATES_Edit" + ControlChars.CrLf
                            //  + "  where YEAR = '" + year + "'" + ControlChars.CrLf;

                            //using (IDbCommand cmdCheckGen = con.CreateCommand())
                            //{
                            //    cmdCheckGen.CommandText = sqlDataAllocate;
                            //    DataTable dtCurrent = new DataTable();
                            //    using (DbDataAdapter da = dbf.CreateDataAdapter())
                            //    {
                            //        ((IDbDataAdapter)da).SelectCommand = cmdCheckGen;
                            //        da.Fill(dtCurrent);
                            //        if (dtCurrent.Rows.Count > 0)
                            //        {
                            //            throw (new Exception(String.Format(L10n.Term("KPIB0201.ERR_ALLOCATE_HAS_EXIST"))));
                            //        }
                            //    }
                            //}

                            // 11/01/2006 Paul.  The transaction is optional, just make sure to always dispose it. 
                            using (IDbTransaction trn = Sql.BeginTransaction(con))
                            {
                                try
                                {
                                    SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "Begin Generate KPI Standard");

                                    KPIB0201_SqlProc.Gen_KPI_STANDARD_FULL(year, trn);

                                    SplendidError.SystemWarning(new StackTrace(true).GetFrame(0), "End Generate KPI Standard");

                                    lblStatus.Text = L10n.Term(".LBL_GENERATE_SUCCESSFUL");
                                    trn.Commit();
                                    SplendidCache.ClearFavorites();
                                }
                                catch (Exception ex)
                                {
                                    trn.Rollback();
                                    SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                                    ctlDynamicButtons.ErrorText += ex.Message;
                                    return;
                                }
                                finally
                                {
                                    if (trn != null)
                                        trn.Dispose();
                                }
                            }
                        }
                    }
                }
                else if (e.CommandName == "Import.Upload")
                {

                }
                else if (e.CommandName == "Cancel")
                {
                    string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
                    if (Sql.IsEmptyString(sRelativePath))
                        sRelativePath = "~/" + sImportModule + "/";
                    Response.Redirect(sRelativePath);
                }
            }
            catch (Exception ex)
            {
                //SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText += ex.Message;
                return;
            }
        }


        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(sImportModule + ".LBL_MODULE_NAME"));

            this.Visible = Security.IS_ADMIN || (SplendidCRM.Security.GetUserAccess(sImportModule, "import") >= 0);
            if (!this.Visible)
            {
                // 03/17/2010 Paul.  We need to rebind the parent in order to get the error message to display. 
                Parent.DataBind();
                return;
            }

            try
            {
                gID = Sql.ToGuid(Request["ID"]);
                if (!IsPostBack)
                {
                    DropDownList ddlYEAR = this.FindControl("YEAR") as DropDownList;
                    if (ddlYEAR != null)
                    {
                        KPIs_Utils.FillDropdownListOfYear(ddlYEAR);
                    }
                    string sSQL;
                    sSQL = "select row_number() over (order by from_code) AS NO, *       " + ControlChars.CrLf
                         + "  from M_KPI_CONVERSION_RATE where status = 1 and deleted=0 " + ControlChars.CrLf;
                    DbProviderFactory dbf = DbProviderFactories.GetFactory();
                    using (IDbConnection con = dbf.CreateConnection())
                    {
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandText = sSQL;
                            using (DbDataAdapter da = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da).SelectCommand = cmd;
                                using (DataTable dtCurrent = new DataTable())
                                {
                                    da.Fill(dtCurrent);
                                    if (dtCurrent.Rows.Count > 0)
                                    {
                                        grdMain.DataSource = dtCurrent;
                                        grdMain.DataBind();
                                    }
                                }
                            }
                        }
                    }
                    // 09/06/2012 Paul.  Allow direct import into prospect list. 
                    // 10/22/2013 Paul.  Title was not getting set properly. 
                    //ViewState["ctlDynamicButtons.Title"] = L10n.Term(sImportModule + ".LBL_MODULE_NAME");
                    string sMODULE_TABLE = Sql.ToString(Application["Modules." + sImportModule + ".TableName"]);
                    dtRuleColumns = SplendidCache.SqlColumns("vw" + sMODULE_TABLE + "_List");
                    ViewState["RULE_COLUMNS"] = dtRuleColumns;
                }
                else
                {
                    // 05/31/2015 Paul.  Combine ModuleHeader and DynamicButtons. 
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(ctlDynamicButtons.Title);

                }
            }
            catch (Exception ex)
            {
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // 09/17/2013 Paul.  Add Business Rules to import. 

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);

            this.m_sMODULE = "Import";
            // 07/21/2010 Paul.  Make sure to highlight the correct menu item. 
            SetMenu(sImportModule);

            string sRelativePath = Sql.ToString(Application["Modules." + sImportModule + ".RelativePath"]);
            if (Sql.IsEmptyString(sRelativePath))
            {
                // 10/14/2014 Paul.  Correct module name. 
                if (sImportModule == "Project" || sImportModule == "ProjectTask")
                    sImportModule += "s";
                sRelativePath = "~/" + sImportModule + "/";
            }

            // 04/29/2008 Paul.  Make use of dynamic buttons. 
            ctlDynamicButtons.AppendButtons(m_sMODULE + ".ImportView", Guid.Empty, Guid.Empty);
            // 04/27/2018 Paul.  We need to be able to generate an error message. 
            if (IsPostBack)
            {
                Page.Validators.Add(new RulesValidator(this));
            }
        }
        #endregion
    }
}

