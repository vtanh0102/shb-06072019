<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="EditView.ascx.cs" Inherits="SplendidCRM.KPIB0201.EditView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="divEditView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" ShowRequired="true" EditView="false" Module="KPIB0201" EnablePrint="false" HelpName="EditView" EnableHelp="false" runat="Server" />

    <asp:HiddenField ID="LAYOUT_EDIT_VIEW" runat="server" />
    <asp:Table SkinID="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <table id="tblMain" class="tabEditView" runat="server">
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <asp:Button CssClass="button" ID="loadData" Visible="true" OnClick="LoaData_Click" runat="server" />

    <div id="divEditSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" ShowFooter='<%# SplendidCRM.Security.AdminUserAccess(m_sMODULE, "edit") >= 0 %>' runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_NO">
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                            <asp:HiddenField ID="txtGROUP_KPI_DETAIL_ID" Value='<%# Bind("GROUP_KPI_DETAIL_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                            <asp:HiddenField ID="txtKPI_ID" Value='<%# Bind("KPI_ID") %>' runat="server" />
                            <asp:HiddenField ID="txtKPI_CODE" Value='<%# Bind("KPI_CODE") %>' runat="server" />
                            <asp:HiddenField ID="txtLEVEL_NUMBER" Value='<%# Bind("LEVEL_NUMBER") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                            <asp:HiddenField ID="hdUnitId" Value="" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_RATIO">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle CssClass="format-qty right" HorizontalAlign="Right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" CssClass="right"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_VALUE_MONTH">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle CssClass="format-qty right" HorizontalAlign="Right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtVALUE_STD_PER_MONTH" Text='<%# Bind("VALUE_STD_PER_MONTH") %>' runat="server" CssClass="right"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_MAX_RATIO_COMPL">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle CssClass="format-qty right" HorizontalAlign="Right" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMAX_RATIO_COMPL" Text='<%# Bind("MAX_RATIO_COMPLETE") %>' runat="server" CssClass="right"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                        <ItemTemplate>
                            <asp:TextBox ID="txtDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
    <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />
    <%--<script src="../Include/javascript/jquery.maskMoney.js"></script>--%>
    <SplendidCRM:InlineScript runat="server">
        <script type="text/javascript">
            $(function () {
                $("[id$=ctl00_cntBody_ctlEditView_ORGANIZATION_ID]").chosen();
                //$("[id*=<%= grdMain.ClientID   %>]input[type=text][id*=txtVALUE_STD_PER_MONTH]").maskMoney({ decimal: ',', thousands: '.' });                
            });
        </script>
    </SplendidCRM:InlineScript>

    <%@ Register TagPrefix="SplendidCRM" TagName="DynamicButtons" Src="~/_controls/DynamicButtons.ascx" %>
    <SplendidCRM:DynamicButtons ID="ctlFooterButtons" Visible="<%# !SplendidDynamic.StackedLayout(this.Page.Theme) && !PrintView %>" ShowRequired="false" runat="Server" />
</div>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
