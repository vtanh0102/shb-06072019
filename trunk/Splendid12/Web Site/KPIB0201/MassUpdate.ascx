<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="MassUpdate.ascx.cs" Inherits="SplendidCRM.KPIB0201.MassUpdate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="SplendidCRM" TagName="MassUpdateButtons" Src="~/_controls/MassUpdateButtons.ascx" %>
<SplendidCRM:MassUpdateButtons ID="ctlDynamicButtons" SubPanel="divKPIB0201MassUpdate" Title=".LBL_MASS_UPDATE_TITLE" runat="Server" />

<div id="divKPIB0201MassUpdate" style='<%= "display:" + (CookieValue("divKPIB0201MassUpdate") != "1" ? "inline": "none") %>'>
    <asp:Table Width="100%" CellPadding="0" CellSpacing="0" CssClass="tabForm" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <%@ Register TagPrefix="SplendidCRM" Tagname="TagMassApprove" Src="~/_controls/TagMassApprove.ascx" %>
                <SplendidCRM:TagMassApprove ID="ctlTagMassApprove" runat="Server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
