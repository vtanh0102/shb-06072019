using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using SplendidCRM._modules;

namespace SplendidCRM.KPIB0201
{

    /// <summary>
    /// Summary description for DetailView.
    /// </summary>
    public class DetailView : SplendidControl
    {
        protected _controls.HeaderButtons ctlDynamicButtons;

        protected Guid gID;
        protected HtmlTable tblMain;
        protected PlaceHolder plcSubPanel;
        protected SplendidGrid grdMain;
        protected MassUpdate ctlMassUpdate;

        protected void grdMain_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUNIT = (Label)e.Item.FindControl("txtKPI_UNIT");
                if (lblUNIT != null)
                {
                    lblUNIT.Text = KPIs_Utils.Get_DisplayName(L10n.NAME, "CURRENCY_UNIT_LIST", lblUNIT.Text);
                }
                string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                Label lblKPI_RATIO = (Label)e.Item.FindControl("txtKPI_RATIO");
                if (lblKPI_RATIO != null)
                {
                    lblKPI_RATIO.Text = KPIs_Utils.FormatFloat(lblKPI_RATIO.Text, format_number);
                }
                Label lblVALUE_STD_PER_MONTH = (Label)e.Item.FindControl("txtVALUE_STD_PER_MONTH");
                if (lblVALUE_STD_PER_MONTH != null)
                {
                    //int valueMonth = (int)Math.Ceiling(Sql.ToFloat(lblVALUE_STD_PER_MONTH.Text));
                    //double valueMonth = Math.Round(Sql.ToFloat(lblVALUE_STD_PER_MONTH.Text), MidpointRounding.AwayFromZero);
                    double valueMonth = KPIs_Utils.RoundNumber(Sql.ToDouble(lblVALUE_STD_PER_MONTH.Text));
                    lblVALUE_STD_PER_MONTH.Text = KPIs_Utils.FormatFloat(Sql.ToString(valueMonth), format_number);
                }
                Label lblMAX_RATIO_COMPL = (Label)e.Item.FindControl("txtMAX_RATIO_COMPL");
                if (lblMAX_RATIO_COMPL != null)
                {
                    if (lblMAX_RATIO_COMPL.Text.Trim().Equals("0") || lblMAX_RATIO_COMPL.Text.Trim().Equals("0.000") || lblMAX_RATIO_COMPL.Text.Trim().Equals("0,000"))
                        lblMAX_RATIO_COMPL.Text = string.Empty;
                    else
                        lblMAX_RATIO_COMPL.Text = KPIs_Utils.FormatFloat(lblMAX_RATIO_COMPL.Text, format_number);
                }
            }
        }

        protected void Page_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect("edit.aspx?ID=" + gID.ToString());
                }
                else if (e.CommandName == "Duplicate")
                {
                    Response.Redirect("edit.aspx?DuplicateID=" + gID.ToString());
                }
                else if (e.CommandName == "Delete")
                {
                    KPIB0201_SqlProc.spB_KPI_STANDARD_Delete(gID);
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "Cancel")
                {
                    Response.Redirect("default.aspx");
                }
                else if (e.CommandName == "MassSubmitApproval")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "edit", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0201_SqlProc.spB_KPI_STANDARD_MassApprove(sIDs, ctlMassUpdate.APPROVED_BY, KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
                //MassApprove
                else if (e.CommandName == "MassApprove")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0201_SqlProc.spB_KPI_STANDARD_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_APPROVE, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }

                //MassReject
                else if (e.CommandName == "MassReject")
                {
                    string[] arrID = new string[] { Sql.ToString(gID) };
                    if (arrID != null)
                    {
                        System.Collections.Stack stk = Utils.FilterByACL_Stack(m_sMODULE, "list", arrID, SplendidCRM.Crm.Modules.TableName(m_sMODULE));
                        if (stk.Count > 0)
                        {
                            DbProviderFactory dbf = DbProviderFactories.GetFactory();
                            using (IDbConnection con = dbf.CreateConnection())
                            {
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    try
                                    {
                                        while (stk.Count > 0)
                                        {
                                            string sIDs = Utils.BuildMassIDs(stk);
                                            KPIB0201_SqlProc.spB_KPI_STANDARD_MassApprove(sIDs, Security.USER_ID, KPIs_Constant.KPI_APPROVE_STATUS_REJECT, trn);
                                        }
                                        trn.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        trn.Rollback();
                                        throw (new Exception(ex.Message, ex.InnerException));
                                    }
                                }
                            }
                            Response.Redirect(string.Format("view.aspx?ID={0}", gID));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE));
            this.Visible = (SplendidCRM.Security.GetUserAccess(m_sMODULE, "view") >= 0);
            if (!this.Visible)
                return;

            try
            {
                gID = Sql.ToGuid(Request["ID"]);

                if (!IsPostBack)
                {
                    if (!Sql.IsEmptyGuid(gID))
                    {
                        DbProviderFactory dbf = DbProviderFactories.GetFactory();
                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            string sSQL;
                            sSQL = "select *           " + ControlChars.CrLf
                                 + "  from vwB_KPI_STANDARD_Edit" + ControlChars.CrLf;
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID, "ID", false);
                                con.Open();
                                using (IDbTransaction trn = Sql.BeginTransaction(con))
                                {
                                    bool checkApproval = KPIs_Utils.spCheckApproval(gID, "vwB_KPI_STANDARD_Edit", trn);
                                    if (!checkApproval)
                                    {
                                        ctlDynamicButtons.ShowButton("MassApprove", checkApproval);
                                    }
                                }
                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            DataRow rdr = dtCurrent.Rows[0];
                                            this.ApplyDetailViewPreLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);

                                            ctlDynamicButtons.Title = Sql.ToString(rdr["KPI_STANDARD_NAME"]);
                                            SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                                            Utils.UpdateTracker(Page, m_sMODULE, gID, ctlDynamicButtons.Title);
                                            ViewState["ctlDynamicButtons.Title"] = ctlDynamicButtons.Title;

                                            bool bModuleIsAssigned = Sql.ToBoolean(Application["Modules." + m_sMODULE + ".Assigned"]);
                                            Guid gASSIGNED_USER_ID = Guid.Empty;
                                            if (bModuleIsAssigned)
                                                gASSIGNED_USER_ID = Sql.ToGuid(rdr["ASSIGNED_USER_ID"]);

                                            this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                                            this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, rdr);
                                            Page.Items["ASSIGNED_USER_ID"] = gASSIGNED_USER_ID;

                                            ctlDynamicButtons.AppendProcessButtons(rdr);

                                            string approveStatus = rdr["APPROVE_STATUS"].ToString();
                                            if (!KPIs_Constant.KPI_APPROVE_STATUS_APPROVE.Equals(approveStatus) && !KPIs_Constant.KPI_APPROVE_STATUS_SUBMIT.Equals(approveStatus))
                                            {
                                                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, gASSIGNED_USER_ID, rdr);
                                            }

                                            this.ApplyDetailViewPostLoadEventRules(m_sMODULE + "." + LayoutDetailView, rdr);
                                        }
                                        else
                                        {
                                            plcSubPanel.Visible = false;

                                            ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                                            ctlDynamicButtons.AppendProcessButtons(null);
                                            ctlDynamicButtons.DisableAll();
                                            ctlDynamicButtons.ErrorText = L10n.Term("ACL.LBL_NO_ACCESS");
                                        }
                                    }
                                }
                            }
                        }

                        using (IDbConnection con = dbf.CreateConnection())
                        {
                            var sSQL_DETAILS = "SELECT Row_Number() OVER(order by DATE_ENTERED) AS NO, " + ControlChars.CrLf
                                 + " GROUP_KPI_DETAIL_ID AS ID, VALUE_STD_PER_MONTH, KPI_NAME, KPI_UNIT, RATIO, MAX_RATIO_COMPLETE, DESCRIPTION, REMARK     " + ControlChars.CrLf
                                 + "  FROM vwB_KPI_STANDARD_DETAILS_Edit " + ControlChars.CrLf;

                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.CommandText = sSQL_DETAILS;

                                //25/09/2018 Tungnx: Fix tu dong loc theo dieu kien assigned to
                                cmd.CommandText += "  WHERE 1=1                  " + ControlChars.CrLf;

                                //Security.Filter(cmd, m_sMODULE, "view");

                                Sql.AppendParameter(cmd, gID.ToString(), "KPI_STANDARD_ID");
                                cmd.CommandText += "  order by NO   " + ControlChars.CrLf;
                                con.Open();

                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd));

                                using (DbDataAdapter da = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da).SelectCommand = cmd;
                                    using (DataTable dtCurrent = new DataTable())
                                    {
                                        da.Fill(dtCurrent);
                                        if (dtCurrent.Rows.Count > 0)
                                        {
                                            grdMain.DataSource = dtCurrent;
                                            grdMain.DataBind();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                        ctlDynamicButtons.AppendProcessButtons(null);
                        ctlDynamicButtons.DisableAll();
                    }
                }
                else
                {
                    ctlDynamicButtons.Title = Sql.ToString(ViewState["ctlDynamicButtons.Title"]);
                    SetPageTitle(L10n.Term(".moduleList." + m_sMODULE) + " - " + ctlDynamicButtons.Title);
                }
                ctlMassUpdate.Visible = Security.GetUserAccess(m_sMODULE, "list") > 0 ? true : false;

                //01/10/2018 Tungnx: check approval button
                ctlMassUpdate.detailGID = gID.ToString();
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                ctlDynamicButtons.ErrorText = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            ctlDynamicButtons.Command += new CommandEventHandler(Page_Command);
            ctlMassUpdate.Command += new CommandEventHandler(Page_Command);
            grdMain.ItemDataBound += new DataGridItemEventHandler(grdMain_ItemDataBound);
            m_sMODULE = "KPIB0201";
            SetMenu(m_sMODULE);
            if (IsPostBack)
            {
                this.AppendDetailViewRelationships(m_sMODULE + "." + LayoutDetailView, plcSubPanel);
                this.AppendDetailViewFields(m_sMODULE + "." + LayoutDetailView, tblMain, null);
                ctlDynamicButtons.AppendButtons(m_sMODULE + "." + LayoutDetailView, Guid.Empty, null);
                ctlDynamicButtons.AppendProcessButtons(null);
            }
        }
        #endregion
    }
}
