
function B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed(fldB_KPI_STANDARD_KPI_STANDARD_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain KPI_STANDARD_NAME in the text, so just get the length minus 4. 
	var userContext = fldB_KPI_STANDARD_KPI_STANDARD_NAME.id.substring(0, fldB_KPI_STANDARD_KPI_STANDARD_NAME.id.length - 'B_KPI_STANDARD_KPI_STANDARD_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_STANDARD_KPI_STANDARD_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME = document.getElementById(userContext + 'PREV_B_KPI_STANDARD_KPI_STANDARD_NAME');
	if ( fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_B_KPI_STANDARD_KPI_STANDARD_NAME');
	}
	else if ( fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME.value != fldB_KPI_STANDARD_KPI_STANDARD_NAME.value )
	{
		if ( fldB_KPI_STANDARD_KPI_STANDARD_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.KPIB0201.AutoComplete.B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Get(fldB_KPI_STANDARD_KPI_STANDARD_NAME.value, B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed_OnSucceededWithContext, B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sKPI_STANDARD_NAME = result.KPI_STANDARD_NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'B_KPI_STANDARD_KPI_STANDARD_NAME_AjaxErrors');
		var fldB_KPI_STANDARD_ID        = document.getElementById(userContext + 'B_KPI_STANDARD_ID'       );
		var fldB_KPI_STANDARD_KPI_STANDARD_NAME      = document.getElementById(userContext + 'B_KPI_STANDARD_KPI_STANDARD_NAME'     );
		var fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME = document.getElementById(userContext + 'PREV_B_KPI_STANDARD_KPI_STANDARD_NAME');
		if ( fldB_KPI_STANDARD_ID        != null ) fldB_KPI_STANDARD_ID.value        = sID  ;
		if ( fldB_KPI_STANDARD_KPI_STANDARD_NAME      != null ) fldB_KPI_STANDARD_KPI_STANDARD_NAME.value      = sKPI_STANDARD_NAME;
		if ( fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME != null ) fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME.value = sKPI_STANDARD_NAME;
	}
	else
	{
		alert('result from KPIB0201.AutoComplete service is null');
	}
}

function B_KPI_STANDARD_B_KPI_STANDARD_KPI_STANDARD_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'B_KPI_STANDARD_KPI_STANDARD_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldB_KPI_STANDARD_ID        = document.getElementById(userContext + 'B_KPI_STANDARD_ID'       );
	var fldB_KPI_STANDARD_KPI_STANDARD_NAME      = document.getElementById(userContext + 'B_KPI_STANDARD_KPI_STANDARD_NAME'     );
	var fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME = document.getElementById(userContext + 'PREV_B_KPI_STANDARD_KPI_STANDARD_NAME');
	if ( fldB_KPI_STANDARD_ID        != null ) fldB_KPI_STANDARD_ID.value        = '';
	if ( fldB_KPI_STANDARD_KPI_STANDARD_NAME      != null ) fldB_KPI_STANDARD_KPI_STANDARD_NAME.value      = '';
	if ( fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME != null ) fldPREV_B_KPI_STANDARD_KPI_STANDARD_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

