<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="DetailView.ascx.cs" Inherits="SplendidCRM.KPIB0201.DetailView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style type="text/css">  
    .listViewThS1 .headerDes, .itemDes{        
        padding-left: 15px;
    }
</style>
<div id="divDetailView" runat="server">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlDynamicButtons" Module="KPIB0201" EnablePrint="true" HelpName="DetailView" EnableHelp="true" EnableFavorites="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="DetailNavigation" Src="~/_controls/DetailNavigation.ascx" %>
    <SplendidCRM:DetailNavigation ID="ctlDetailNavigation" Module="KPIB0201" Visible="<%# !PrintView %>" runat="Server" />

    <asp:HiddenField ID="LAYOUT_DETAIL_VIEW" runat="server" />
    <table id="tblMain" class="tabDetailView" runat="server">
    </table>

    <div id="divDetailSubPanel">
        <asp:PlaceHolder ID="plcSubPanel" runat="server" />
    </div>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
                <asp:HiddenField ID="txtINDEX" runat="server" />
                <asp:Button ID="btnINDEX_MOVE" Style="display: none" runat="server" />
                <asp:Label ID="Label1" CssClass="error" EnableViewState="false" runat="server" />
            </asp:Panel>

            <SplendidCRM:SplendidGrid ID="grdMain" AllowPaging="false" AllowSorting="false" EnableViewState="true" runat="server">
                <Columns>
                    <asp:TemplateColumn ItemStyle-CssClass="dragHandle">
                        <ItemTemplate>
                            <asp:Image SkinID="blank" Width="14px" runat="server" />                            
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_LIST_NO">
                        <ItemTemplate>
                            <asp:Label ID="lblNO" runat="server" Text='<%# Bind("NO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="KPIM0101.KPIS_TAGET_TYPE" Visible="false">
                        <ItemTemplate>
                            <%--<asp:Label ID="txtKPI_ID" Text='<%# Bind("KPI_ID") %>' runat="server" />--%>
                            <asp:HiddenField ID="txtID" Value='<%# Bind("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_NAME">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_NAME" Text='<%# Bind("KPI_NAME") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_KPI_UNIT">
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_UNIT" Text='<%# Bind("KPI_UNIT") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_RATIO">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" HorizontalAlign="Right"/>
                        <ItemStyle CssClass="dataField format-qty" Width="10%" HorizontalAlign="Right"/>
                        <ItemTemplate>
                            <asp:Label ID="txtKPI_RATIO" Text='<%# Bind("RATIO") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_VALUE_MONTH">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" HorizontalAlign="Right"/>
                        <ItemStyle CssClass="dataField format-qty" Width="12%" HorizontalAlign="Right"/>
                        <ItemTemplate>
                            <asp:Label ID="txtVALUE_STD_PER_MONTH" Text='<%# Bind("VALUE_STD_PER_MONTH") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_MAX_RATIO_COMPL">
                        <HeaderStyle CssClass="gridHeaderLabel-Gray" HorizontalAlign="Right"/>
                        <ItemStyle CssClass="dataField format-qty" Width="12%" HorizontalAlign="Right"/>
                        <ItemTemplate>
                            <asp:Label ID="txtMAX_RATIO_COMPL" Text='<%# Bind("MAX_RATIO_COMPLETE") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText=".LBL_DESCRIPTION">
                        <ItemStyle CssClass="itemDes" Width="20%" />  
                        <HeaderStyle CssClass="headerDes"/>                      
                        <ItemTemplate>
                            <asp:Label ID="txtDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </SplendidCRM:SplendidGrid>

            <SplendidCRM:InlineScript runat="server">
                <script type="text/javascript" src="../Include/javascript/jquery.tablednd_0_5.js"></script>
            </SplendidCRM:InlineScript>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<%-- Mass Update Seven --%>
<asp:Panel ID="pnlMassUpdateSeven" runat="server">
    <%@ Register TagPrefix="SplendidCRM" Tagname="MassUpdate" Src="MassUpdate.ascx" %>
    <SplendidCRM:MassUpdate ID="ctlMassUpdate" Visible="<%# !SplendidCRM.Crm.Config.enable_dynamic_mass_update() && !PrintView && !IsMobile && SplendidCRM.Crm.Modules.MassUpdate(m_sMODULE) %>" runat="Server" />
</asp:Panel>

<%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
<SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />
