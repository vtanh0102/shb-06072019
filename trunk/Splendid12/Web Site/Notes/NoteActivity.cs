/**
 * Copyright (C) 2008 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Enterprise Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Diagnostics;

namespace SplendidCRM
{
	/// <summary>
	/// NoteActivity generated from database[SplendidCRM6_Azure] on 12/4/2017 6:31:29 PM
	/// </summary>
	public class NoteActivity: SplendidActivity
	{
		public NoteActivity()
		{
			this.Name = "NoteActivity";
		}

		#region Public workflow properties
		public static DependencyProperty IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.IDProperty))); }
			set { base.SetValue(NoteActivity.IDProperty, value); }
		}

		public static DependencyProperty MODIFIED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_USER_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid MODIFIED_USER_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.MODIFIED_USER_IDProperty))); }
			set { base.SetValue(NoteActivity.MODIFIED_USER_IDProperty, value); }
		}

		public static DependencyProperty NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.NAMEProperty))); }
			set { base.SetValue(NoteActivity.NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_TYPE", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_TYPE
		{
			get { return ((string)(base.GetValue(NoteActivity.PARENT_TYPEProperty))); }
			set { base.SetValue(NoteActivity.PARENT_TYPEProperty, value); }
		}

		public static DependencyProperty PARENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.PARENT_IDProperty))); }
			set { base.SetValue(NoteActivity.PARENT_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.CONTACT_IDProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_IDProperty, value); }
		}

		public static DependencyProperty DESCRIPTIONProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DESCRIPTION", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string DESCRIPTION
		{
			get { return ((string)(base.GetValue(NoteActivity.DESCRIPTIONProperty))); }
			set { base.SetValue(NoteActivity.DESCRIPTIONProperty, value); }
		}

		public static DependencyProperty TEAM_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.TEAM_IDProperty))); }
			set { base.SetValue(NoteActivity.TEAM_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_LIST", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_LIST
		{
			get { return ((string)(base.GetValue(NoteActivity.TEAM_SET_LISTProperty))); }
			set { base.SetValue(NoteActivity.TEAM_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_USER_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty TAG_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TAG_SET_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TAG_SET_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.TAG_SET_NAMEProperty))); }
			set { base.SetValue(NoteActivity.TAG_SET_NAMEProperty, value); }
		}

		public static DependencyProperty IS_PRIVATEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("IS_PRIVATE", typeof(bool), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool IS_PRIVATE
		{
			get { return ((bool)(base.GetValue(NoteActivity.IS_PRIVATEProperty))); }
			set { base.SetValue(NoteActivity.IS_PRIVATEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_LISTProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_LIST", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_LIST
		{
			get { return ((string)(base.GetValue(NoteActivity.ASSIGNED_SET_LISTProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_SET_LISTProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty ASSIGNED_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_SET_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_SET_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.ASSIGNED_SET_NAMEProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TOProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO
		{
			get { return ((string)(base.GetValue(NoteActivity.ASSIGNED_TOProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_TOProperty, value); }
		}

		public static DependencyProperty CREATED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY
		{
			get { return ((string)(base.GetValue(NoteActivity.CREATED_BYProperty))); }
			set { base.SetValue(NoteActivity.CREATED_BYProperty, value); }
		}

		public static DependencyProperty CREATED_BY_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CREATED_BY_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.CREATED_BY_IDProperty))); }
			set { base.SetValue(NoteActivity.CREATED_BY_IDProperty, value); }
		}

		public static DependencyProperty DATE_ENTEREDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_ENTERED", typeof(DateTime), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_ENTERED
		{
			get { return ((DateTime)(base.GetValue(NoteActivity.DATE_ENTEREDProperty))); }
			set { base.SetValue(NoteActivity.DATE_ENTEREDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIEDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED", typeof(DateTime), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED
		{
			get { return ((DateTime)(base.GetValue(NoteActivity.DATE_MODIFIEDProperty))); }
			set { base.SetValue(NoteActivity.DATE_MODIFIEDProperty, value); }
		}

		public static DependencyProperty DATE_MODIFIED_UTCProperty = System.Workflow.ComponentModel.DependencyProperty.Register("DATE_MODIFIED_UTC", typeof(DateTime), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public DateTime DATE_MODIFIED_UTC
		{
			get { return ((DateTime)(base.GetValue(NoteActivity.DATE_MODIFIED_UTCProperty))); }
			set { base.SetValue(NoteActivity.DATE_MODIFIED_UTCProperty, value); }
		}

		public static DependencyProperty FILE_MIME_TYPEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FILE_MIME_TYPE", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FILE_MIME_TYPE
		{
			get { return ((string)(base.GetValue(NoteActivity.FILE_MIME_TYPEProperty))); }
			set { base.SetValue(NoteActivity.FILE_MIME_TYPEProperty, value); }
		}

		public static DependencyProperty FILENAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("FILENAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string FILENAME
		{
			get { return ((string)(base.GetValue(NoteActivity.FILENAMEProperty))); }
			set { base.SetValue(NoteActivity.FILENAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY
		{
			get { return ((string)(base.GetValue(NoteActivity.MODIFIED_BYProperty))); }
			set { base.SetValue(NoteActivity.MODIFIED_BYProperty, value); }
		}

		public static DependencyProperty NOTE_ATTACHMENT_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("NOTE_ATTACHMENT_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid NOTE_ATTACHMENT_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.NOTE_ATTACHMENT_IDProperty))); }
			set { base.SetValue(NoteActivity.NOTE_ATTACHMENT_IDProperty, value); }
		}

		public static DependencyProperty PORTAL_FLAGProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PORTAL_FLAG", typeof(bool), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public bool PORTAL_FLAG
		{
			get { return ((bool)(base.GetValue(NoteActivity.PORTAL_FLAGProperty))); }
			set { base.SetValue(NoteActivity.PORTAL_FLAGProperty, value); }
		}

		public static DependencyProperty TEAM_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.TEAM_NAMEProperty))); }
			set { base.SetValue(NoteActivity.TEAM_NAMEProperty, value); }
		}

		public static DependencyProperty TEAM_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid TEAM_SET_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.TEAM_SET_IDProperty))); }
			set { base.SetValue(NoteActivity.TEAM_SET_IDProperty, value); }
		}

		public static DependencyProperty TEAM_SET_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("TEAM_SET_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string TEAM_SET_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.TEAM_SET_NAMEProperty))); }
			set { base.SetValue(NoteActivity.TEAM_SET_NAMEProperty, value); }
		}

		public static DependencyProperty ASSIGNED_TO_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ASSIGNED_TO_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string ASSIGNED_TO_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.ASSIGNED_TO_NAMEProperty))); }
			set { base.SetValue(NoteActivity.ASSIGNED_TO_NAMEProperty, value); }
		}

		public static DependencyProperty ATTACHMENT_READYProperty = System.Workflow.ComponentModel.DependencyProperty.Register("ATTACHMENT_READY", typeof(Int32), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Int32 ATTACHMENT_READY
		{
			get { return ((Int32)(base.GetValue(NoteActivity.ATTACHMENT_READYProperty))); }
			set { base.SetValue(NoteActivity.ATTACHMENT_READYProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_SET_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.CONTACT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_ASSIGNED_USER_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid CONTACT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.CONTACT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty CONTACT_EMAILProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_EMAIL", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_EMAIL
		{
			get { return ((string)(base.GetValue(NoteActivity.CONTACT_EMAILProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_EMAILProperty, value); }
		}

		public static DependencyProperty CONTACT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.CONTACT_NAMEProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_NAMEProperty, value); }
		}

		public static DependencyProperty CONTACT_PHONEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CONTACT_PHONE", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CONTACT_PHONE
		{
			get { return ((string)(base.GetValue(NoteActivity.CONTACT_PHONEProperty))); }
			set { base.SetValue(NoteActivity.CONTACT_PHONEProperty, value); }
		}

		public static DependencyProperty CREATED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("CREATED_BY_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string CREATED_BY_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.CREATED_BY_NAMEProperty))); }
			set { base.SetValue(NoteActivity.CREATED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty MODIFIED_BY_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("MODIFIED_BY_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string MODIFIED_BY_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.MODIFIED_BY_NAMEProperty))); }
			set { base.SetValue(NoteActivity.MODIFIED_BY_NAMEProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_SET_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_SET_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_SET_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.PARENT_ASSIGNED_SET_IDProperty))); }
			set { base.SetValue(NoteActivity.PARENT_ASSIGNED_SET_IDProperty, value); }
		}

		public static DependencyProperty PARENT_ASSIGNED_USER_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_ASSIGNED_USER_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PARENT_ASSIGNED_USER_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.PARENT_ASSIGNED_USER_IDProperty))); }
			set { base.SetValue(NoteActivity.PARENT_ASSIGNED_USER_IDProperty, value); }
		}

		public static DependencyProperty PARENT_NAMEProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PARENT_NAME", typeof(string), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public string PARENT_NAME
		{
			get { return ((string)(base.GetValue(NoteActivity.PARENT_NAMEProperty))); }
			set { base.SetValue(NoteActivity.PARENT_NAMEProperty, value); }
		}

		public static DependencyProperty PENDING_PROCESS_IDProperty = System.Workflow.ComponentModel.DependencyProperty.Register("PENDING_PROCESS_ID", typeof(Guid), typeof(NoteActivity));

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public Guid PENDING_PROCESS_ID
		{
			get { return ((Guid)(base.GetValue(NoteActivity.PENDING_PROCESS_IDProperty))); }
			set { base.SetValue(NoteActivity.PENDING_PROCESS_IDProperty, value); }
		}

		#endregion

		protected override void Load(bool bAudit, bool bPast)
		{
			try
			{
				if ( bAudit && Sql.IsEmptyGuid(AUDIT_ID) )
					throw(new Exception("NoteActivity.Load: AUDIT_ID was not set"));
				else if ( !bAudit && Sql.IsEmptyGuid(ID) )
					throw(new Exception("NoteActivity.Load: ID was not set"));

				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbCommand cmd = con.CreateCommand() )
					{
						if ( bPast )
						{
							// 02/20/2010 Paul.  The previous Past select statement was returning the current audit record, which is not the OLD record. 
							// In order to get the OLD record, we need to use the version value. 
							cmd.CommandText = "select NOTES_AUDIT_OLD.*" + ControlChars.CrLf
							                + "  from      vwNOTES_AUDIT        NOTES          " + ControlChars.CrLf
							                + " inner join vwNOTES_AUDIT        NOTES_AUDIT_OLD" + ControlChars.CrLf
							                + "         on NOTES_AUDIT_OLD.ID = NOTES.ID       " + ControlChars.CrLf
							                + "        and NOTES_AUDIT_OLD.AUDIT_VERSION = (select max(vwNOTES_AUDIT.AUDIT_VERSION)" + ControlChars.CrLf
							                + "                                               from vwNOTES_AUDIT                   " + ControlChars.CrLf
							                + "                                              where vwNOTES_AUDIT.ID            =  NOTES.ID           " + ControlChars.CrLf
							                + "                                                and vwNOTES_AUDIT.AUDIT_VERSION <  NOTES.AUDIT_VERSION" + ControlChars.CrLf
							                + "                                                and vwNOTES_AUDIT.AUDIT_TOKEN   <> NOTES.AUDIT_TOKEN  " + ControlChars.CrLf
							                + "                                            )" + ControlChars.CrLf
							                + " where NOTES.AUDIT_ID = @AUDIT_ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else if ( bAudit )
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwNOTES_Edit" + ControlChars.CrLf
							                + " where ID in (select ID from vwNOTES_AUDIT where AUDIT_ID = @AUDIT_ID)" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@AUDIT_ID", AUDIT_ID);
						}
						else
						{
							cmd.CommandText = "select *           " + ControlChars.CrLf
							                + "  from vwNOTES_Edit" + ControlChars.CrLf
							                + " where ID = @ID" + ControlChars.CrLf;
							Sql.AddParameter(cmd, "@ID", ID);
						}

						using ( IDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow) )
						{
							if ( rdr.Read() )
							{
								ID                             = Sql.ToGuid    (rdr["ID"                            ]);
								MODIFIED_USER_ID               = Sql.ToGuid    (rdr["MODIFIED_USER_ID"              ]);
								NAME                           = Sql.ToString  (rdr["NAME"                          ]);
								PARENT_TYPE                    = Sql.ToString  (rdr["PARENT_TYPE"                   ]);
								PARENT_ID                      = Sql.ToGuid    (rdr["PARENT_ID"                     ]);
								CONTACT_ID                     = Sql.ToGuid    (rdr["CONTACT_ID"                    ]);
								DESCRIPTION                    = Sql.ToString  (rdr["DESCRIPTION"                   ]);
								TEAM_ID                        = Sql.ToGuid    (rdr["TEAM_ID"                       ]);
								TEAM_SET_LIST                  = Sql.ToString  (rdr["TEAM_SET_LIST"                 ]);
								ASSIGNED_USER_ID               = Sql.ToGuid    (rdr["ASSIGNED_USER_ID"              ]);
								if ( !bPast )
									TAG_SET_NAME                   = Sql.ToString  (rdr["TAG_SET_NAME"                  ]);
								IS_PRIVATE                     = Sql.ToBoolean (rdr["IS_PRIVATE"                    ]);
								ASSIGNED_SET_LIST              = Sql.ToString  (rdr["ASSIGNED_SET_LIST"             ]);
								ASSIGNED_SET_ID                = Sql.ToGuid    (rdr["ASSIGNED_SET_ID"               ]);
								ASSIGNED_SET_NAME              = Sql.ToString  (rdr["ASSIGNED_SET_NAME"             ]);
								ASSIGNED_TO                    = Sql.ToString  (rdr["ASSIGNED_TO"                   ]);
								CREATED_BY                     = Sql.ToString  (rdr["CREATED_BY"                    ]);
								CREATED_BY_ID                  = Sql.ToGuid    (rdr["CREATED_BY_ID"                 ]);
								DATE_ENTERED                   = Sql.ToDateTime(rdr["DATE_ENTERED"                  ]);
								DATE_MODIFIED                  = Sql.ToDateTime(rdr["DATE_MODIFIED"                 ]);
								DATE_MODIFIED_UTC              = Sql.ToDateTime(rdr["DATE_MODIFIED_UTC"             ]);
								FILE_MIME_TYPE                 = Sql.ToString  (rdr["FILE_MIME_TYPE"                ]);
								FILENAME                       = Sql.ToString  (rdr["FILENAME"                      ]);
								MODIFIED_BY                    = Sql.ToString  (rdr["MODIFIED_BY"                   ]);
								NOTE_ATTACHMENT_ID             = Sql.ToGuid    (rdr["NOTE_ATTACHMENT_ID"            ]);
								PORTAL_FLAG                    = Sql.ToBoolean (rdr["PORTAL_FLAG"                   ]);
								TEAM_NAME                      = Sql.ToString  (rdr["TEAM_NAME"                     ]);
								TEAM_SET_ID                    = Sql.ToGuid    (rdr["TEAM_SET_ID"                   ]);
								TEAM_SET_NAME                  = Sql.ToString  (rdr["TEAM_SET_NAME"                 ]);
								if ( !bPast )
								{
									ASSIGNED_TO_NAME               = Sql.ToString  (rdr["ASSIGNED_TO_NAME"              ]);
									ATTACHMENT_READY               = Sql.ToInteger (rdr["ATTACHMENT_READY"              ]);
									CONTACT_ASSIGNED_SET_ID        = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_SET_ID"       ]);
									CONTACT_ASSIGNED_USER_ID       = Sql.ToGuid    (rdr["CONTACT_ASSIGNED_USER_ID"      ]);
									CONTACT_EMAIL                  = Sql.ToString  (rdr["CONTACT_EMAIL"                 ]);
									CONTACT_NAME                   = Sql.ToString  (rdr["CONTACT_NAME"                  ]);
									CONTACT_PHONE                  = Sql.ToString  (rdr["CONTACT_PHONE"                 ]);
									CREATED_BY_NAME                = Sql.ToString  (rdr["CREATED_BY_NAME"               ]);
									MODIFIED_BY_NAME               = Sql.ToString  (rdr["MODIFIED_BY_NAME"              ]);
									PARENT_ASSIGNED_SET_ID         = Sql.ToGuid    (rdr["PARENT_ASSIGNED_SET_ID"        ]);
									PARENT_ASSIGNED_USER_ID        = Sql.ToGuid    (rdr["PARENT_ASSIGNED_USER_ID"       ]);
									PARENT_NAME                    = Sql.ToString  (rdr["PARENT_NAME"                   ]);
									PENDING_PROCESS_ID             = Sql.ToGuid    (rdr["PENDING_PROCESS_ID"            ]);
								}
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("NoteActivity.Load failed: " + ex.Message, ex));
			}
		}

		protected override void Save()
		{
			try
			{
				DbProviderFactory dbf = DbProviderFactories.GetFactory(app.SplendidProvider, app.ConnectionString);
				using ( IDbConnection con = dbf.CreateConnection() )
				{
					con.Open();
					using ( IDbTransaction trn = Sql.BeginTransaction(con) )
					{
						try
						{
							// 08/09/2008 Paul.  Log the workflow instance so that it can be used to block circular/recursive workflows. 
							SqlProcs.spWORKFLOW_TRANS_LOG_InsertOnly("NOTES", WORKFLOW_ID, this.WorkflowInstanceId, trn);
							Guid gID = ID;
							SqlProcs.spNOTES_Update
								( ref gID
								, NAME
								, PARENT_TYPE
								, PARENT_ID
								, CONTACT_ID
								, DESCRIPTION
								, TEAM_ID
								, TEAM_SET_LIST
								, ASSIGNED_USER_ID
								, TAG_SET_NAME
								, IS_PRIVATE
								, ASSIGNED_SET_LIST
								, trn
								);
							ID = gID;
							trn.Commit();
						}
						catch
						{
							trn.Rollback();
							throw;
						}
					}
				}
			}
			catch(Exception ex)
			{
				SplendidError.SystemMessage(app.Context, "Error", new StackTrace(true).GetFrame(0), Utils.ExpandException(ex));
				throw(new Exception("NoteActivity.Save failed: " + ex.Message, ex));
			}
		}
	}
}

