
function CRM0002_CRM0002_NAME_Changed(fldCRM0002_NAME)
{
	// 02/04/2007 Paul.  We need to have an easy way to locate the correct text fields, 
	// so use the current field to determine the label prefix and send that in the userContact field. 
	// 08/24/2009 Paul.  One of the base controls can contain NAME in the text, so just get the length minus 4. 
	var userContext = fldCRM0002_NAME.id.substring(0, fldCRM0002_NAME.id.length - 'CRM0002_NAME'.length)
	var fldAjaxErrors = document.getElementById(userContext + 'CRM0002_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '';
	
	var fldPREV_CRM0002_NAME = document.getElementById(userContext + 'PREV_CRM0002_NAME');
	if ( fldPREV_CRM0002_NAME == null )
	{
		//alert('Could not find ' + userContext + 'PREV_CRM0002_NAME');
	}
	else if ( fldPREV_CRM0002_NAME.value != fldCRM0002_NAME.value )
	{
		if ( fldCRM0002_NAME.value.length > 0 )
		{
			try
			{
				SplendidCRM.RCharts.AutoComplete.CRM0002_CRM0002_NAME_Get(fldCRM0002_NAME.value, CRM0002_CRM0002_NAME_Changed_OnSucceededWithContext, CRM0002_CRM0002_NAME_Changed_OnFailed, userContext);
			}
			catch(e)
			{
				alert('CRM0002_CRM0002_NAME_Changed: ' + e.Message);
			}
		}
		else
		{
			var result = { 'ID' : '', 'NAME' : '' };
			CRM0002_CRM0002_NAME_Changed_OnSucceededWithContext(result, userContext, null);
		}
	}
}

function CRM0002_CRM0002_NAME_Changed_OnSucceededWithContext(result, userContext, methodName)
{
	if ( result != null )
	{
		var sID   = result.ID  ;
		var sNAME = result.NAME;
		
		var fldAjaxErrors        = document.getElementById(userContext + 'CRM0002_NAME_AjaxErrors');
		var fldCRM0002_ID        = document.getElementById(userContext + 'CRM0002_ID'       );
		var fldCRM0002_NAME      = document.getElementById(userContext + 'CRM0002_NAME'     );
		var fldPREV_CRM0002_NAME = document.getElementById(userContext + 'PREV_CRM0002_NAME');
		if ( fldCRM0002_ID        != null ) fldCRM0002_ID.value        = sID  ;
		if ( fldCRM0002_NAME      != null ) fldCRM0002_NAME.value      = sNAME;
		if ( fldPREV_CRM0002_NAME != null ) fldPREV_CRM0002_NAME.value = sNAME;
	}
	else
	{
		alert('result from RCharts.AutoComplete service is null');
	}
}

function CRM0002_CRM0002_NAME_Changed_OnFailed(error, userContext)
{
	// Display the error.
	var fldAjaxErrors = document.getElementById(userContext + 'CRM0002_NAME_AjaxErrors');
	if ( fldAjaxErrors != null )
		fldAjaxErrors.innerHTML = '<br />' + error.get_message();

	var fldCRM0002_ID        = document.getElementById(userContext + 'CRM0002_ID'       );
	var fldCRM0002_NAME      = document.getElementById(userContext + 'CRM0002_NAME'     );
	var fldPREV_CRM0002_NAME = document.getElementById(userContext + 'PREV_CRM0002_NAME');
	if ( fldCRM0002_ID        != null ) fldCRM0002_ID.value        = '';
	if ( fldCRM0002_NAME      != null ) fldCRM0002_NAME.value      = '';
	if ( fldPREV_CRM0002_NAME != null ) fldPREV_CRM0002_NAME.value = '';
}

if ( typeof(Sys) !== 'undefined' )
	Sys.Application.notifyScriptLoaded();

