﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class Y2YKQTHCharts : DashletControl
    {
        protected HiddenField lblTypeY2YKQTH;
        protected HiddenField lblNameY2YKQTH;
        protected HiddenField lblDataCharJsY2YKQTH;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;

            lblTypeY2YKQTH.Value = string.Empty;
            lblDataCharJsY2YKQTH.Value = string.Empty;
            lblNameY2YKQTH.Value = string.Empty;

            var currentYear = DateTime.Now.ToString("yyyy");
            var previousYear = DateTime.Now.AddYears(-1).ToString("yyyy");

            var chartCode = "EMP_DASHLET_03";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    lblNameY2YKQTH.Value = dt.Rows[0]["NAME"].ToString();
                    lblTypeY2YKQTH.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var xLabelsJ = dt.Rows[0]["XLABELS"].ToString().Split(',');

                    // using char js                                        
                    var datacharts = new DataLineChartJs();
                    string[] labels = new string[xLabelsJ.Length];
                    for (int i = 0; i < xLabelsJ.Length; i++)
                    {
                        labels[i] = xLabelsJ[i].ToString();                        
                    }
                    datacharts.labels = labels;
                    
                    List<LineChartDataset> chartDatasets = new List<LineChartDataset>();
                    for (int i = 0; i < 2; i++)
                    {
                        var chartDataset = new LineChartDataset();
                        var sYear = (i == 0) ? previousYear : currentYear;

                        chartDataset.label = sYear;

                        var sqlCol = "COMMANDSQL" + ((i == 0) ? "" : "1");
                        var sqlGetDataChart = dt.Rows[0][sqlCol].ToString();

                        sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", sYear);
                        sqlGetDataChart = sqlGetDataChart.Replace("$MA_NHAN_VIEN", Security.CURRENT_EMPLOYEE.MA_NHAN_VIEN);

                        DataTable dt1 = null;
                        using (IDbConnection con1 = dbf.CreateConnection())
                        {
                            con1.Open();
                            using (IDbCommand cmd1 = con1.CreateCommand())
                            {
                                cmd1.CommandText = sqlGetDataChart;
                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                                using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                    dt1 = new DataTable();
                                    da1.Fill(dt1);
                                }
                            }
                        }

                        float[] values = new float[dt1.Rows.Count];
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            values[j] = float.Parse(dt1.Rows[j][ykeysJ[0].Trim()].ToString());
                        }
                        chartDataset.data = values;
                        chartDataset.backgroundColor = bgColorsJ[i];
                        chartDataset.fill = "false";
                        chartDataset.fillColor = bgColorsJ[i];
                        chartDataset.borderColor = bgColorsJ[i];
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                    }
                    datacharts.datasets = chartDatasets;

                    lblDataCharJsY2YKQTH.Value = datacharts.ToJSON();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                //lblError.Text = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

