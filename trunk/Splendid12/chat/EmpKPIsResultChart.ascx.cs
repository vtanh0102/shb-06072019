﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class EmpKPIsResultChart : DashletControl
    {
        protected HiddenField lblType1;
        protected HiddenField lblName1;
        protected HiddenField lblDataCharJs1;

        protected HiddenField lblType2;
        protected HiddenField lblName2;
        protected HiddenField lblDataCharJs2;

        protected HiddenField lblTypeY2YKQTH;
        protected HiddenField lblNameY2YKQTH;
        protected HiddenField lblDataCharJsY2YKQTH;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;

        }

        //Tien do thuc hien KPIs
        public void SearchEmpKPIsResultChart(ListControl lstYear, ListControl lstMonth, TextBox tbFullName, ListControl lstPosotion, ListControl lstArea, ListControl lstRegion, ListControl lstOrganization, ListControl lstOrganizationParent)
        {
            lblType2.Value = string.Empty;
            lblDataCharJs2.Value = string.Empty;
            lblName2.Value = string.Empty;

            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");

            var EMPLOYEE_ID = string.Empty;
            var FULL_NAME = string.Empty;
            var MAIN_POS_ID = string.Empty;
            var POS_ID = string.Empty;
            var POSITION_ID = string.Empty;
            var POSITION_NAME = string.Empty;
            var AREA_ID = string.Empty;
            var REGION_ID = string.Empty;

            if (lstYear != null) year = lstYear.SelectedValue;
            if (lstMonth != null) month = lstMonth.SelectedValue;
            if (tbFullName != null) FULL_NAME = tbFullName.Text;
            if (lstOrganizationParent != null) MAIN_POS_ID = lstOrganizationParent.SelectedValue;
            if (lstOrganization != null) POS_ID = lstOrganization.SelectedValue;
            if (lstPosotion != null) POSITION_ID = lstPosotion.SelectedValue;
            if (lstArea != null) AREA_ID = lstArea.SelectedValue;
            if (lstRegion != null) REGION_ID = lstRegion.SelectedValue;

            //Check role
            if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
            {
                MAIN_POS_ID = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;

                if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    POS_ID = Security.CURRENT_EMPLOYEE.POS_ID;
                }
                if ("CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    EMPLOYEE_ID = Security.CURRENT_EMPLOYEE.EMPLOYEE_ID;
                }
            }


            var chartCode = "EmpKPIsResultChart";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    lblName2.Value = dt.Rows[0]["NAME"].ToString();
                    lblType2.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);

                    sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", EMPLOYEE_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$FULL_NAME", FULL_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", MAIN_POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POS_ID", POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_ID", POSITION_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_NAME", POSITION_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$REGION_ID", REGION_ID);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    // using char js                                        
                    var datacharts = new DataChartJs();
                    string[] labels = new string[dt1.Rows.Count];
                    string[] backgroundColors = new string[dt1.Rows.Count];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        labels[i] = dt1.Rows[i][xkeyJ].ToString();
                    }
                    datacharts.labels = labels;
                    List<ChartDataset> chartDatasets = new List<ChartDataset>();
                    for (int i = 0; i < ykeysJ.Length; i++)
                    {
                        var chartDataset = new ChartDataset();
                        chartDataset.label = L10n.Term(".LBL_COMPLETED_PERCENT");

                        float[] values = new float[dt1.Rows.Count];
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            values[j] = float.Parse(dt1.Rows[j][ykeysJ[i].Trim()].ToString());

                            backgroundColors[j] = bgColorsJ[i];
                        }
                        chartDataset.data = values;
                        chartDataset.backgroundColor = backgroundColors;
                        chartDataset.fill = dt.Rows[0]["TYPE"].ToString().Equals("1") ? "false" : "true";
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                    }
                    datacharts.datasets = chartDatasets;

                    lblDataCharJs2.Value = datacharts.ToJSON();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                //lblError.Text = ex.Message;
            }
        }


        //Ke hoach và thuc te thuc hien KPIs
        public void SearchEmpKPIsPlanActualChart(ListControl lstYear, ListControl lstMonth, TextBox tbFullName, ListControl lstPosotion, ListControl lstArea, ListControl lstRegion, ListControl lstOrganization, ListControl lstOrganizationParent)
        {
            lblType1.Value = string.Empty;
            lblDataCharJs1.Value = string.Empty;
            lblName1.Value = string.Empty;

            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");
                       
            var EMPLOYEE_ID = string.Empty;
            var FULL_NAME = string.Empty;
            var MAIN_POS_ID = string.Empty;
            var POS_ID = string.Empty;
            var POSITION_ID = string.Empty;
            var POSITION_NAME = string.Empty;
            var AREA_ID = string.Empty;
            var REGION_ID = string.Empty;

            var BUSINESS_CODE = string.Empty;

            if (lstYear != null) year = lstYear.SelectedValue;
            if (lstMonth != null) month = lstMonth.SelectedValue;
            if (tbFullName != null) FULL_NAME = tbFullName.Text;
            if (lstOrganizationParent != null) MAIN_POS_ID = lstOrganizationParent.SelectedValue;
            if (lstOrganization != null) POS_ID = lstOrganization.SelectedValue;
            if (lstPosotion != null) POSITION_ID = lstPosotion.SelectedValue;
            if (lstArea != null) AREA_ID = lstArea.SelectedValue;
            if (lstRegion != null) REGION_ID = lstRegion.SelectedValue;

            //Check role
            if ("CRM_05".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
            {
                MAIN_POS_ID = Security.CURRENT_EMPLOYEE.MAIN_POS_ID;

                if ("CRM_06".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME) || "CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    POS_ID = Security.CURRENT_EMPLOYEE.POS_ID;
                }
                if ("CRM_07".Equals(Security.CURRENT_EMPLOYEE.PRIMARY_ROLE_NAME))
                {
                    EMPLOYEE_ID = Security.CURRENT_EMPLOYEE.EMPLOYEE_ID;
                }
            }

            var chartCode = "EmpKPIsPlanActualChart";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    lblName1.Value = dt.Rows[0]["NAME"].ToString();
                    lblType1.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');
                    
                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);
                    
                    sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", EMPLOYEE_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$FULL_NAME", FULL_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", MAIN_POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POS_ID", POS_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_ID", POSITION_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_NAME", POSITION_NAME);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                    sqlGetDataChart = sqlGetDataChart.Replace("$REGION_ID", REGION_ID);

                    sqlGetDataChart = sqlGetDataChart.Replace("$BUSINESS_CODE", BUSINESS_CODE);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    // using char js                                        
                    var datacharts = new DataMixedChartJs();
                    string[] labels = new string[dt1.Rows.Count];
                   
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        labels[i] = dt1.Rows[i][xkeyJ].ToString();
                    }
                    datacharts.labels = labels;
                    List<MixedChartDataset> chartDatasets = new List<MixedChartDataset>();
                    for (int i = 0; i < ykeysJ.Length; i++)
                    {
                        string backgroundColors = bgColorsJ[i];
                        var chartDataset = new MixedChartDataset();
                        chartDataset.label = labelsJ[i];

                        float[] values = new float[dt1.Rows.Count];
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            values[j] = float.Parse(dt1.Rows[j][ykeysJ[i].Trim()].ToString());
                        }

                        

                        chartDataset.data = values;
                        chartDataset.backgroundColor = backgroundColors;
                        chartDataset.borderColor = borderColorsJ[i];
                        chartDataset.fill = mixedTypesJ[i].ToString().Equals("line") ? "false" : "true";
                        chartDataset.type = mixedTypesJ[i];
                        chartDataset.borderWidth = mixedTypesJ[i].ToString().Equals("line") ? 3 : 1;
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                    }
                    datacharts.datasets = chartDatasets;

                    lblDataCharJs1.Value = datacharts.ToJSON();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                //lblError.Text = ex.Message;
            }
        }


        //So sanh ket qua thuc hien KPI voi nam truoc
        public void SearchEmpKPIsY2YChart(ListControl lstYear, ListControl lstMonth, TextBox tbFullName, ListControl lstPosotion, ListControl lstArea, ListControl lstRegion, ListControl lstOrganization, ListControl lstOrganizationParent)
        {
            lblTypeY2YKQTH.Value = string.Empty;
            lblDataCharJsY2YKQTH.Value = string.Empty;
            lblNameY2YKQTH.Value = string.Empty;

            var month = DateTime.Now.ToString("MM");
            var year = DateTime.Now.ToString("yyyy");

            var previousYear = DateTime.Now.AddYears(-1).ToString("yyyy");

            var EMPLOYEE_ID = string.Empty;
            var FULL_NAME = string.Empty;
            var MAIN_POS_ID = string.Empty;
            var POS_ID = string.Empty;
            var POSITION_ID = string.Empty;
            var POSITION_NAME = string.Empty;
            var AREA_ID = string.Empty;
            var REGION_ID = string.Empty;

            if (lstYear != null)
            {
                year = lstYear.SelectedValue;
                previousYear = (int.Parse(year) - 1).ToString();
            }
            if (lstMonth != null) month = lstMonth.SelectedValue;
            if (tbFullName != null) FULL_NAME = tbFullName.Text;
            if (lstOrganizationParent != null) MAIN_POS_ID = lstOrganizationParent.SelectedValue;
            if (lstOrganization != null) POS_ID = lstOrganization.SelectedValue;
            if (lstPosotion != null) POSITION_ID = lstPosotion.SelectedValue;
            if (lstArea != null) AREA_ID = lstArea.SelectedValue;
            if (lstRegion != null) REGION_ID = lstRegion.SelectedValue;

            var chartCode = "EmpKPIsY2YChart";

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    lblNameY2YKQTH.Value = dt.Rows[0]["NAME"].ToString();
                    lblTypeY2YKQTH.Value = dt.Rows[0]["TYPE"].ToString();
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var xLabelsJ = dt.Rows[0]["XLABELS"].ToString().Split(',');

                    // using char js                                        
                    var datacharts = new DataLineChartJs();
                    string[] labels = new string[xLabelsJ.Length];
                    for (int i = 0; i < xLabelsJ.Length; i++)
                    {
                        labels[i] = xLabelsJ[i].ToString();
                    }
                    datacharts.labels = labels;

                    List<LineChartDataset> chartDatasets = new List<LineChartDataset>();
                    for (int i = 0; i < 2; i++)
                    {
                        var chartDataset = new LineChartDataset();
                        var sYear = (i == 0) ? previousYear : year;

                        chartDataset.label = sYear;

                        var sqlCol = "COMMANDSQL" + ((i == 0) ? "" : "1");
                        var sqlGetDataChart = dt.Rows[0][sqlCol].ToString();

                        sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", sYear);
                        sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);

                        sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", EMPLOYEE_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$FULL_NAME", FULL_NAME);
                        sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", MAIN_POS_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$POS_ID", POS_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_ID", POSITION_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$POSITION_NAME", POSITION_NAME);
                        sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$AREA_ID", AREA_ID);
                        sqlGetDataChart = sqlGetDataChart.Replace("$REGION_ID", REGION_ID);

                        DataTable dt1 = null;
                        using (IDbConnection con1 = dbf.CreateConnection())
                        {
                            con1.Open();
                            using (IDbCommand cmd1 = con1.CreateCommand())
                            {
                                cmd1.CommandText = sqlGetDataChart;
                                if (bDebug)
                                    RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                                using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                                {
                                    ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                    dt1 = new DataTable();
                                    da1.Fill(dt1);
                                }
                            }
                        }

                        float[] values = new float[dt1.Rows.Count];
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            values[j] = float.Parse(dt1.Rows[j][ykeysJ[0].Trim()].ToString());
                        }
                        chartDataset.data = values;
                        chartDataset.backgroundColor = bgColorsJ[i];
                        chartDataset.fill = "false";
                        chartDataset.fillColor = bgColorsJ[i];
                        chartDataset.borderColor = bgColorsJ[i];
                        //dataset.type = 
                        chartDatasets.Add(chartDataset);
                    }
                    datacharts.datasets = chartDatasets;

                    lblDataCharJsY2YKQTH.Value = datacharts.ToJSON();
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
                //lblError.Text = ex.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

