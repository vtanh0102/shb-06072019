<%@ Control CodeBehind="DVKDHomePageKPIsResultChart.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.DVKDHomePageKPIsResultChart" %>

<script src="../Include/dist/Chart.bundle.js"></script>

<asp:Panel runat="server" Style="background-color: white">
    <asp:HiddenField runat="server" ID="lblName2"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblType2"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="lblDataCharJs2"></asp:HiddenField>
    <div style="background-color: white;">
        <canvas id="canvas2" style="width: 200px;height: 100px;"></canvas>
    </div>
    <script>
        var ctx2 = document.getElementById('canvas2').getContext('2d');
        var data2 = $('#<%=lblDataCharJs2.ClientID %>').val();
        var type2 = $('#<%=lblType2.ClientID %>').val();
        var name2 = $('#<%=lblName2.ClientID %>').val();
        var typechart2 = '';
        if (type2 == '1') typechart2 = 'line'
        else if (type2 == '2') typechart2 = 'bar'
        else if (type2 == '3') typechart2 = 'pie';        
        var config2 = {
            type: typechart2,
            data: JSON.parse(data2),
            options: {
                responsive: true,
                legend: {
                    display: true
                },
                title: {
                    display: true,
                    text: name2
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        };
        var myChart2 = new Chart(ctx2, config2);

    </script>
</asp:Panel>


