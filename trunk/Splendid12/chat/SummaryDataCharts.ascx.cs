﻿/**
 * Copyright (C) 2005-2010 SplendidCRM Software, Inc. All Rights Reserved. 
 *
 * Any use of the contents of this file are subject to the SplendidCRM Professional Source Code License 
 * Agreement, or other written agreement between you and SplendidCRM ("License"). By installing or 
 * using this file, you have unconditionally agreed to the terms and conditions of the License, 
 * including but not limited to restrictions on the number of users therein, and you may not use this 
 * file except in compliance with the License. 
 * 
 * SplendidCRM owns all proprietary rights, including all copyrights, patents, trade secrets, and 
 * trademarks, in and to the contents of this file.  You will not link to or in any way combine the 
 * contents of this file or any derivatives with any Open Source Code in any manner that would require 
 * the contents of this file to be made available to any third party. 
 * 
 * IN NO EVENT SHALL SPLENDIDCRM BE RESPONSIBLE FOR ANY DAMAGES OF ANY KIND, INCLUDING ANY DIRECT, 
 * SPECIAL, PUNITIVE, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES.  Other limitations of liability 
 * and disclaimers set forth in the License. 
 * 
 */
using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Collections.Generic;
using SplendidCRM._modules;

namespace SplendidCRM.RCharts
{
    /// <summary>
    ///		Summary description for MyCalendar.
    /// </summary>
    public class SummaryDataCharts : DashletControl
    {
        protected Label lbTotalToiResult;
        protected Label lbToiRankNumber;
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.Visible)
                return;

            lbTotalToiResult.Text = string.Empty;
            lbToiRankNumber.Text = string.Empty;

            var sMonth = DateTime.Now.ToString("MM");
            var sYear = DateTime.Now.ToString("yyyy");
            string sEmployeeId = Security.CURRENT_EMPLOYEE.EMPLOYEE_ID; //"A7F5EE80-B668-44E4-B348-B6E8AB7A4E79";
            string sMainPosId = Security.CURRENT_EMPLOYEE.MAIN_POS_ID; //"";

            this.setToiRankNumber(sEmployeeId, sYear, sMonth, sMainPosId);
            this.setToiResultByMonth(sEmployeeId, sYear, sMonth);
        }

        //Tinh xep hang thu thuan trong chi nhanh
        private void setToiRankNumber (string sEmployeeId, string sYear, string sMonthPeriod, string sMainPosId)
        {
            var month = sMonthPeriod;
            var year = sYear;
            var chartCode1 = "EmpHomepageRankTOI";
            //Xep hang cua nhan vien
            int iToiRankNumber;
            //Tong nhan vien trong chi nhanh
            int iTotalEmployeeInMainPos;

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode1 + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(';');
                    var borderColorsJ = dt.Rows[0]["BORDER_COLORS"].ToString().Split(';');
                    var mixedTypesJ = dt.Rows[0]["MIXED_TYPES"].ToString().Split(',');

                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);
                    sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", sEmployeeId);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MAIN_POS_ID", sMainPosId);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        iToiRankNumber = int.Parse(dt1.Rows[0][ykeysJ[0].Trim()].ToString()) + 1;
                        iTotalEmployeeInMainPos = int.Parse(dt1.Rows[0][ykeysJ[1].Trim()].ToString());

                        lbToiRankNumber.Text = iToiRankNumber.ToString() + "/" + iTotalEmployeeInMainPos.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
            }
        }

        //Lay ket qua thu thuan
        private void setToiResultByMonth(string sEmployeeId, string sYear, string sMonthPeriod)
        {
            var month = sMonthPeriod;
            var year = sYear;
            var chartCode1 = "EmpHomepageTotalTOI";
            //Ket qua thu thuan
            decimal fTotalToiResult;

            try
            {
                DataTable dt = null;
                DbProviderFactory dbf = DbProviderFactories.GetFactory();
                using (IDbConnection con = dbf.CreateConnection())
                {
                    con.Open();
                    string sSQL;
                    sSQL = "SELECT TOP 1 *   " + ControlChars.CrLf
                          + "  FROM M_Map_Report     " + ControlChars.CrLf
                          + "  WHERE 1=1    " + ControlChars.CrLf
                           + "    AND CHART_CODE = '" + chartCode1 + "'" + ControlChars.CrLf;

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = sSQL;
                        using (DbDataAdapter da = dbf.CreateDataAdapter())
                        {
                            ((IDbDataAdapter)da).SelectCommand = cmd;
                            dt = new DataTable();
                            da.Fill(dt);
                        }
                    }
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    var xkeyJ = dt.Rows[0]["XVALUE"].ToString();
                    var ykeysJ = dt.Rows[0]["YVALUES"].ToString().Split(',');
                    var labelsJ = dt.Rows[0]["LABELS"].ToString().Split(',');
                    var bgColorsJ = dt.Rows[0]["BG_COLORS"].ToString().Split(',');
                    var sqlGetDataChart = dt.Rows[0]["COMMANDSQL"].ToString();
                    sqlGetDataChart = sqlGetDataChart.Replace("$YEAR", year);
                    sqlGetDataChart = sqlGetDataChart.Replace("$MONTH_PERIOD", month);
                    sqlGetDataChart = sqlGetDataChart.Replace("$EMPLOYEE_ID", sEmployeeId);

                    DataTable dt1 = null;
                    using (IDbConnection con1 = dbf.CreateConnection())
                    {
                        con1.Open();
                        using (IDbCommand cmd1 = con1.CreateCommand())
                        {
                            cmd1.CommandText = sqlGetDataChart;
                            if (bDebug)
                                RegisterClientScriptBlock("SQLCode", Sql.ClientScriptBlock(cmd1));
                            using (DbDataAdapter da1 = dbf.CreateDataAdapter())
                            {
                                ((IDbDataAdapter)da1).SelectCommand = cmd1;
                                dt1 = new DataTable();
                                da1.Fill(dt1);
                            }
                        }
                    }

                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        fTotalToiResult = Decimal.Parse(dt1.Rows[0][ykeysJ[0].Trim()].ToString());
                        string format_number = Sql.ToString(Application["CONFIG.format_number"]);
                        lbTotalToiResult.Text = KPIs_Utils.FormatFloat(fTotalToiResult.ToString(), format_number);
                    }
                }
            }
            catch (Exception ex)
            {
                SplendidError.SystemError(new StackTrace(true).GetFrame(0), ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

