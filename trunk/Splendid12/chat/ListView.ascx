﻿<%@ Control CodeBehind="ListView.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.ListView" %>

<script src="../Include/dist/Chart.bundle.js"></script>
<style type="text/css">/* Chart.js */
    @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
</style>
<div id="divListView">
    <%@ Register TagPrefix="SplendidCRM" TagName="HeaderButtons" Src="~/_controls/HeaderButtons.ascx" %>
    <SplendidCRM:HeaderButtons ID="ctlModuleHeader" Module="RCharts" Title=".moduleList.Home" EnablePrint="true" HelpName="index" EnableHelp="true" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="SearchView" Src="~/_controls/SearchView.ascx" %>
    <SplendidCRM:SearchView ID="ctlSearchView" Module="RCharts" ShowDuplicateSearch="true" Visible="<%# !PrintView %>" runat="Server" />
    
    <asp:Panel CssClass="button-panel" Visible="<%# !PrintView %>" runat="server">
        <asp:Label ID="lblError" CssClass="error" EnableViewState="false" runat="server" />
    </asp:Panel>

    <%@ Register TagPrefix="SplendidCRM" TagName="EmpTOISummaryChart" Src="~/RCharts/EmpTOISummaryChart.ascx" %>
    <SplendidCRM:EmpTOISummaryChart ID="ctlEmpTOISummaryChart" runat="Server" />

    <%@ Register TagPrefix="SplendidCRM" TagName="EmpKPIsResultChart" Src="~/RCharts/EmpKPIsResultChart.ascx" %>
    <SplendidCRM:EmpKPIsResultChart ID="ctlEmpKPIsResultChart" runat="Server" />
    
    <%@ Register TagPrefix="SplendidCRM" TagName="DumpSQL" Src="~/_controls/DumpSQL.ascx" %>
    <SplendidCRM:DumpSQL ID="ctlDumpSQL" Visible="<%# !PrintView %>" runat="Server" />

    <SplendidCRM:InlineScript runat="server">
        <script type="text/javascript" src="../Include/javascript/chosen-bootstrap/chosen.jquery.min.js"></script>
        <link href="../Include/javascript/chosen-bootstrap/chosen.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('[id$=ctl00_cntBody_ctlListView_ctlSearchView_lnkAdvancedSearch]').closest("li").hide();
                $("[id$=ctl00_cntBody_ctlListView_ctlSearchView_ORGANIZATION_ID]").chosen();
                $("[id$=ctl00_cntBody_ctlListView_ctlSearchView_ORGANIZATION_PARENT_ID]").chosen();
                $('[id$=ctl00_cntBody_ctlListView_ctlExportHeader_divExport]').closest("td").hide();
            });
        </script>
    </SplendidCRM:InlineScript>
</div>
