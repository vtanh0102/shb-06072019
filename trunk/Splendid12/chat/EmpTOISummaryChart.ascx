﻿<%@ Control CodeBehind="EmpTOISummaryChart.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="SplendidCRM.RCharts.EmpTOISummaryChart" %>

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>
<div style="background-color:white;padding-top:15px;">
<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-green"><i
			class="fas fa-money-check-alt"></i></span>
		<div class="info-box-content">
			<span class="info-box-text">Tổng thu thuần</span> 
            <asp:Label id="lbTotalToiResult" CssClass="info-box-number" runat="server"></asp:Label>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box">
		<div class="info-box-content">
			<span class="info-box-text">Người đạt thu thuần cao nhất</span>
			<asp:Label id="lbMaxTotalToiResult" CssClass="info-box-number" runat="server"></asp:Label>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->

<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box" style="display:;">
		<div class="info-box-content">
			<span class="info-box-text">Người đạt thu thuần thấp nhất</span>
			<asp:Label id="lbMinTotalToiResult" CssClass="info-box-number" runat="server"></asp:Label>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" style="display:none;">
		<span class="info-box-icon bg-red"><i
			class="fas fa-list-ol"></i></span>
		<div class="info-box-content">
			<span class="info-box-text">Temp2</span> <span
				class="info-box-number">10/100</span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<!-- /.col -->
</div>
