USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import]    Script Date: 7/7/2018 11:49:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ACTUAL_RESULT_CODE                 nvarchar(50)
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @KPI_ID                             uniqueidentifier
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @KPI_UNIT                           int
	, @LEVEL_NUMBER                       int
	, @RATIO                              float
	, @PLAN_VALUE                         float
	, @SYNC_VALUE                         float
	, @FINAL_VALUE                        float
	, @PERCENT_SYNC_VALUE                 float
	, @PERCENT_FINAL_VALUE                float
	, @PERCENT_MANUAL_VALUE               float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @LATEST_SYNC_DATE                   datetime
	, @FLEX1                              nvarchar(500)
	, @FLEX2                              nvarchar(500)
	, @FLEX3                              nvarchar(500)
	, @FLEX4                              nvarchar(500)
	, @FLEX5                              nvarchar(500)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL where ACTUAL_RESULT_CODE = @ACTUAL_RESULT_CODE AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND KPI_CODE = @KPI_CODE) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ORG_ACTUAL_RESULT_DETAIL
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ACTUAL_RESULT_CODE                 
			, YEAR                               
			, MONTH_PERIOD                       
			, VERSION_NUMBER                     
			, KPI_ID                             
			, KPI_CODE                           
			, KPI_NAME                           
			, KPI_UNIT                           
			, LEVEL_NUMBER                       
			, RATIO                              
			, PLAN_VALUE                         
			, SYNC_VALUE                         
			, FINAL_VALUE                        
			, PERCENT_SYNC_VALUE                 
			, PERCENT_FINAL_VALUE                
			, PERCENT_MANUAL_VALUE               
			, DESCRIPTION                        
			, REMARK                             
			, LATEST_SYNC_DATE                   
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @ACTUAL_RESULT_CODE                 
			, @YEAR                               
			, @MONTH_PERIOD                       
			, @VERSION_NUMBER                     
			, (SELECT TOP 1 ID FROM vwM_KPIS_Edit WHERE KPI_CODE = @KPI_CODE)--@KPI_ID                             
			, @KPI_CODE                           
			, @KPI_NAME                           
			, (SELECT TOP 1 UNIT FROM vwM_KPIS_Edit WHERE KPI_CODE = @KPI_CODE)--@KPI_UNIT                           
			, @LEVEL_NUMBER                       
			, @RATIO                              
			, @PLAN_VALUE                         
			, @SYNC_VALUE                         
			, @FINAL_VALUE                        
			, @PERCENT_SYNC_VALUE                 
			, @PERCENT_FINAL_VALUE                
			, @PERCENT_MANUAL_VALUE               
			, @DESCRIPTION                        
			, @REMARK                             
			, @LATEST_SYNC_DATE                   
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , ACTUAL_RESULT_CODE                   = @ACTUAL_RESULT_CODE                 
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD                       
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , KPI_ID                               = (SELECT TOP 1 ID FROM vwM_KPIS_Edit WHERE KPI_CODE = @KPI_CODE)--@KPI_ID                             
		     , KPI_CODE                             = @KPI_CODE                           
		     , KPI_NAME                             = @KPI_NAME                           
		     , KPI_UNIT                             = (SELECT TOP 1 UNIT FROM vwM_KPIS_Edit WHERE KPI_CODE = @KPI_CODE)--@KPI_UNIT                           
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , RATIO                                = @RATIO                              
		     , PLAN_VALUE                           = @PLAN_VALUE                         
		     , SYNC_VALUE                           = @SYNC_VALUE                         
		     , FINAL_VALUE                          = @FINAL_VALUE                        
		     , PERCENT_SYNC_VALUE                   = @PERCENT_SYNC_VALUE                 
		     , PERCENT_FINAL_VALUE                  = @PERCENT_FINAL_VALUE                
		     , PERCENT_MANUAL_VALUE                 = @PERCENT_MANUAL_VALUE               
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , LATEST_SYNC_DATE                     = @LATEST_SYNC_DATE                   
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 --where ID                                   = @ID                                 ;
		 where ACTUAL_RESULT_CODE = @ACTUAL_RESULT_CODE AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND KPI_CODE = @KPI_CODE
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB030201_DETAIL', @TAG_SET_NAME;
	end -- if;

  end

GO


