USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_Update_Import]    Script Date: 7/17/2018 11:12:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_Update_Import]
	( @ID                                 uniqueidentifier output	
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @ORGANIZATION_CODE                  nvarchar(50)	
	, @PERCENT_FINAL_TOTAL                float	
	, @DESCRIPTION                        nvarchar(max)
	)
as
  begin
	set nocount on

	if not exists(select * from B_KPI_ORG_ACTUAL_RESULT where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;		
		update B_KPI_ORG_ACTUAL_RESULT
		   set                 
		       DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()          
		     , PERCENT_FINAL_TOTAL                  = @PERCENT_FINAL_TOTAL          
		     , DESCRIPTION                          = @DESCRIPTION
			 , REMARK								= @DESCRIPTION
		   where YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND ORGANIZATION_CODE = @ORGANIZATION_CODE;
	end else begin
		   update B_KPI_ORG_ACTUAL_RESULT
		   set                 
		       DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()		            
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD	                  
		     , ORGANIZATION_CODE                    = @ORGANIZATION_CODE          
		     , PERCENT_FINAL_TOTAL                  = @PERCENT_FINAL_TOTAL          
		     , DESCRIPTION                          = @DESCRIPTION
			 , REMARK								= @DESCRIPTION
		   where ID                                 = @ID;		
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ACTUAL_RESULT_CSTM ( ID_C ) values ( @ID );
		end -- if;
	end -- if;
  end

GO


