USE [SplendidCRM1]
GO
/****** Object:  StoredProcedure [dbo].[Gen_KPI_ORG_ACTUAL_RESULT]    Script Date: 7/17/2018 7:37:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tungnx
-- Description:	Khoi tao ket qua KPI de bat dau dong bo tu cac he thong khac
-- =============================================
ALTER PROCEDURE [dbo].[Gen_KPI_ORG_ACTUAL_RESULT] 
    @ID    uniqueidentifier output,
	@iYear int,
	@sMONTH_PERIOD varchar(2),
	@sORGANIZATION_CODE varchar(50)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @iYear int = YEAR(getdate())
	--DECLARE @sMonth varchar(2) = CONVERT(char(2), getdate(), 101)
	--DECLARE @sMONTH_PERIOD varchar(8) = @sMonth -- + '.' + convert(nvarchar(4), @iYear)
	--DECLARE @sACTUAL_RESULT_CODE varchar(50) = convert(nvarchar(4), @iYear) + @sMonth

	--DECLARE @sORGANIZATION_CODE varchar(50) = ''
	DECLARE @iNumberRec int

	--Check da khoi tao chua?
	SET @iNumberRec = (SELECT COUNT(*)
		FROM [B_KPI_ORG_ACTUAL_RESULT]
		WHERE 1=1
				AND [YEAR] = @iYear
				AND [MONTH_PERIOD] = @sMONTH_PERIOD
				AND ORGANIZATION_CODE = @sORGANIZATION_CODE AND DELETED = 0
		);
	
	--Neu da khoi tao thi khong khoi tao lai
	if @iNumberRec > 0 return

	if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
		set @ID = newid();
	end -- if;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[B_KPI_ORG_ACTUAL_RESULT]
           ([ID]
           ,[DELETED]
           ,[CREATED_BY]
           ,[DATE_ENTERED]
           ,[MODIFIED_USER_ID]
           ,[DATE_MODIFIED]
           ,[DATE_MODIFIED_UTC]
           ,[ASSIGNED_USER_ID]
           ,[TEAM_ID]
           ,[TEAM_SET_ID]
           ,[NAME]
           ,[ACTUAL_RESULT_CODE]
           ,[YEAR]
           ,[MONTH_PERIOD]
           ,[ORGANIZATION_ID]
           ,[ORGANIZATION_CODE]
           ,[VERSION_NUMBER]
           ,[ALLOCATE_CODE]
           ,[ASSIGN_BY]
           ,[ASSIGN_DATE]
           --,[PERCENT_SYNC_TOTAL]
           --,[PERCENT_FINAL_TOTAL]
           --,[PERCENT_MANUAL_TOTAL]
           --,[TOTAL_AMOUNT_01]
           --,[TOTAL_AMOUNT_02]
           --,[TOTAL_AMOUNT_03]
           --,[DESCRIPTION]
           --,[REMARK]
           ,[FILE_ID]
           ,[LATEST_SYNC_DATE]
           --,[APPROVE_ID]
           --,[APPROVE_STATUS]
           --,[APPROVED_BY]
           --,[APPROVED_DATE]
           ,[FLEX1]
           ,[FLEX2]
           ,[FLEX3]
           ,[FLEX4]
           ,[FLEX5])
	SELECT	@ID
			,ALC.[DELETED]
			,ALC.[CREATED_BY]
			,ALC.[DATE_ENTERED]
			,ALC.[MODIFIED_USER_ID]
			,ALC.[DATE_MODIFIED]
			,ALC.[DATE_MODIFIED_UTC]
			,ALC.[ASSIGNED_USER_ID]
			,ALC.[TEAM_ID]
			,ALC.[TEAM_SET_ID]
			,ALC.[ALLOCATE_NAME]
			,ALC.[ALLOCATE_CODE]
			, @iYear
			, @sMONTH_PERIOD
			,MORG.[ID]
			,ALC.[ORGANIZATION_CODE]
			,ALC.[VERSION_NUMBER]
			,ALC.[ALLOCATE_CODE]
			,ALC.[ASSIGN_BY]
			,ALC.[ASSIGN_DATE]
			--,[PERCENT_SYNC_TOTAL]
			--,[PERCENT_FINAL_TOTAL]
			--,[PERCENT_MANUAL_TOTAL]
			--,[TOTAL_AMOUNT_01]
			--,[TOTAL_AMOUNT_02]
			--,[TOTAL_AMOUNT_03]
			--,[DESCRIPTION]
			--,[REMARK]
			,ALC.[FILE_ID]
			, getdate()
			--,[APPROVE_ID]
			--,[APPROVE_STATUS]
			--,[APPROVED_BY]
			--,[APPROVED_DATE]
			,ALC.[FLEX1]
			,ALC.[FLEX2]
			,ALC.[FLEX3]
			,ALC.[FLEX4]
			,ALC.[FLEX5]
	FROM [dbo].[B_KPI_ORG_ALLOCATES] ALC
	INNER JOIN M_ORGANIZATION MORG ON MORG.[ORGANIZATION_CODE] = ALC.[ORGANIZATION_CODE]
	WHERE 1=1 
		AND ALC.[ORGANIZATION_CODE] = @sORGANIZATION_CODE
		AND (ALC.[ORGANIZATION_CODE] IS NOT NULL and ALC.[ORGANIZATION_CODE] <> '')
		AND ALC.[YEAR] = @iYear
		AND ALC.[APPROVE_STATUS] = '65' AND ALC.DELETED =0
		--AND ALC.[VERSION_NUMBER] = '0'
	;

	EXEC [dbo].Gen_KPI_ORG_ACTUAL_RESULT_DETAIL @iYear, @sMONTH_PERIOD, @sORGANIZATION_CODE
END
