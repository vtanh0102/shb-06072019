USE [SplendidCRM1]
GO
/****** Object:  StoredProcedure [dbo].[Gen_KPI_ORG_ACTUAL_RESULT_DETAIL]    Script Date: 7/17/2018 8:07:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tungnx
-- Description:	Khoi tao chi tiet ket qua thuc hien KPI de bat dau thuc hien dong bo
-- =============================================
ALTER PROCEDURE [dbo].[Gen_KPI_ORG_ACTUAL_RESULT_DETAIL] 
	@iYear int,
	@sMONTH_PERIOD varchar(2),
	@sORGANIZATION_CODE varchar(50)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @guiACTUAL_RESULT_ID uniqueidentifier;
	DECLARE @sACTUAL_RESULT_CODE varchar(50);

	DECLARE @guiORGANIZATION_ID uniqueidentifier;
	--DECLARE @sORGANIZATION_CODE varchar(50)
	DECLARE @sALLOCATE_CODE varchar(50);
	--DECLARE @TYPE INT, @KPI_GROUP_ID uniqueidentifier;
		
    -- Insert statements for procedure here	

	DECLARE actualResultCursor CURSOR 
	  LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
		SELECT [ID], [ACTUAL_RESULT_CODE], [ORGANIZATION_ID], [ORGANIZATION_CODE],[ALLOCATE_CODE]
		FROM [B_KPI_ORG_ACTUAL_RESULT]
		WHERE 1=1
				AND [YEAR] = @iYear
				AND [MONTH_PERIOD] = @sMONTH_PERIOD AND ORGANIZATION_CODE = @sORGANIZATION_CODE AND DELETED = 0

	OPEN actualResultCURSOR
	FETCH NEXT FROM actualResultCURSOR 
		INTO @guiACTUAL_RESULT_ID, @sACTUAL_RESULT_CODE, @guiORGANIZATION_ID, @sORGANIZATION_CODE, @sALLOCATE_CODE
	WHILE @@FETCH_STATUS = 0
	BEGIN 

	PRINT @sALLOCATE_CODE
		---Khoi tao du lieu ket qua KPI detail--
		INSERT INTO [dbo].[B_KPI_ORG_ACTUAL_RESULT_DETAIL]
           ([ID]
           ,[DELETED]
           ,[CREATED_BY]
           ,[DATE_ENTERED]
           ,[MODIFIED_USER_ID]
           ,[DATE_MODIFIED]
           ,[DATE_MODIFIED_UTC]
           ,[ASSIGNED_USER_ID]
           ,[TEAM_ID]
           ,[TEAM_SET_ID]
           ,[NAME]
           ,[ACTUAL_RESULT_CODE]
           ,[YEAR]
           ,[MONTH_PERIOD]
           ,[VERSION_NUMBER]
		   ,[KPI_ID]
           ,[KPI_CODE]
           ,[KPI_NAME]
           ,[KPI_UNIT]
           ,[LEVEL_NUMBER]
           ,[RATIO]
           ,[PLAN_VALUE]
           ,[SYNC_VALUE]
           ,[FINAL_VALUE]
           --,[PERCENT_SYNC_VALUE]
           --,[PERCENT_FINAL_VALUE]
           --,[PERCENT_MANUAL_VALUE]
           --,[DESCRIPTION]
           --,[REMARK]
           ,[LATEST_SYNC_DATE]
           ,[FLEX1]
           ,[FLEX2]
           ,[FLEX3]
           ,[FLEX4]
           ,[FLEX5])
		SELECT NEWID()
			,AlcDetail.[DELETED]
			,AlcDetail.[CREATED_BY]
			,AlcDetail.[DATE_ENTERED]
			,AlcDetail.[MODIFIED_USER_ID]
			,AlcDetail.[DATE_MODIFIED]
			,AlcDetail.[DATE_MODIFIED_UTC]
			,AlcDetail.[ASSIGNED_USER_ID]
			,AlcDetail.[TEAM_ID]
			,AlcDetail.[TEAM_SET_ID]
			,AlcDetail.[NAME]
			,@sACTUAL_RESULT_CODE
			,@iYear
			,@sMONTH_PERIOD
			,[VERSION_NUMBER]
			,mKpi.[ID]
			,AlcDetail.[KPI_CODE]
			,AlcDetail.[KPI_NAME]
			,[KPI_UNIT]
			,0
			,AlcDetail.RADIO
			,CASE
			   WHEN @sMONTH_PERIOD='01' THEN MONTH_1
			   WHEN @sMONTH_PERIOD='02' THEN MONTH_2
			   WHEN @sMONTH_PERIOD='03' THEN MONTH_3
			   WHEN @sMONTH_PERIOD='04' THEN MONTH_4
			   WHEN @sMONTH_PERIOD='05' THEN MONTH_5
			   WHEN @sMONTH_PERIOD='06' THEN MONTH_6
			   WHEN @sMONTH_PERIOD='07' THEN MONTH_7
			   WHEN @sMONTH_PERIOD='08' THEN MONTH_8
			   WHEN @sMONTH_PERIOD='09' THEN MONTH_9
			   WHEN @sMONTH_PERIOD='10' THEN MONTH_10
			   WHEN @sMONTH_PERIOD='11' THEN MONTH_11
			   WHEN @sMONTH_PERIOD='12' THEN MONTH_12
			END
			,0--[SYNC_VALUE]
			,0--[FINAL_VALUE]
			--,[PERCENT_SYNC_VALUE]
			--,[PERCENT_FINAL_VALUE]
			--,[PERCENT_MANUAL_VALUE]
			--,[DESCRIPTION]
			--,[REMARK]
			,getdate()
			,[FLEX1]
			,[FLEX2]
			,[FLEX3]
			,[FLEX4]
			,[FLEX5]
		FROM [dbo].[B_KPI_ORG_ALLOCATE_DETAILS] AlcDetail
		INNER JOIN vwM_KPIS mKpi ON mKpi.[KPI_CODE]=AlcDetail.[KPI_CODE]
		WHERE 1=1 
			AND AlcDetail.[ALLOCATE_CODE] = @sALLOCATE_CODE AND AlcDetail.DELETED = 0

		FETCH NEXT FROM actualResultCURSOR 
			INTO @guiACTUAL_RESULT_ID, @sACTUAL_RESULT_CODE, @guiORGANIZATION_ID, @sORGANIZATION_CODE, @sALLOCATE_CODE
	END
	CLOSE actualResultCURSOR
	DEALLOCATE actualResultCURSOR
END