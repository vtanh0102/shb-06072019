USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import]    Script Date: 7/17/2018 11:27:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ORG_ACTUAL_RESULT_DETAIL_Update_Import]
	( @ID                                 uniqueidentifier output	
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @KPI_CODE                           nvarchar(50)	
	, @FINAL_VALUE                        float
	, @REMARK                             nvarchar(max)
	, @LATEST_SYNC_DATE                   datetime
	)
as
  begin
	set nocount on
	
	if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set                   
		       DATE_MODIFIED                        = getdate()                          
		     , DATE_MODIFIED_UTC                    = getutcdate()		                 
		     , FINAL_VALUE                          = @FINAL_VALUE   
		     , DESCRIPTION                          = @REMARK                        
		     , REMARK                               = @REMARK                             
		     , LATEST_SYNC_DATE                     = getdate()
		where YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD	AND KPI_CODE = @KPI_CODE;
		
	end else begin
		update B_KPI_ORG_ACTUAL_RESULT_DETAIL
		   set                   
		       DATE_MODIFIED                        = getdate()                          
		     , DATE_MODIFIED_UTC                    = getutcdate()        
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD		               
		     , KPI_CODE                             = @KPI_CODE                  
		     , FINAL_VALUE                          = @FINAL_VALUE   
		     , DESCRIPTION                          = @REMARK                        
		     , REMARK                               = @REMARK                             
		     , LATEST_SYNC_DATE                     = getdate()
		where ID                                    = @ID;		
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ACTUAL_RESULT_DETAIL_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;

  end

GO


