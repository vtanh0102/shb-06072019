USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ALLOCATES_Import]    Script Date: 7/21/2018 2:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ALLOCATES_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ALLOCATE_CODE                      nvarchar(50) output
	, @ALLOCATE_NAME                      nvarchar(200)
	, @YEAR                               int
	, @PERIOD                             nvarchar(5)
	, @VERSION_NUMBER                     nvarchar(50)
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @ALLOCATE_TYPE                      nvarchar(5)
	, @ORGANIZATION_ID                    uniqueidentifier
	, @EMPLOYEE_ID                        uniqueidentifier
	, @STATUS                             nvarchar(5)
	, @KPI_STANDARD_ID                    uniqueidentifier
	, @FILE_ID                            uniqueidentifier
	, @ASSIGN_BY                          nvarchar(50)--uniqueidentifier
	, @ASSIGN_DATE                        datetime
	, @STAFT_NUMBER                       int
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)

	, @ORGANIZATION_CODE				  nvarchar(50)
	, @MA_NHAN_VIEN						  nvarchar(50)
	, @TOTAL_PLAN_PERCENT				  float
	, @TOTAL_PLAN_VALUE					  float
	, @KPI_GROUP_ID						  uniqueidentifier
	, @FLEX1							 text
	, @FLEX2							 text
	, @FLEX3							 text
	, @FLEX4							 text
	, @FLEX5							 text	
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ALLOCATES where [YEAR] = @YEAR AND PERIOD = @PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		--setting allocate code
		DECLARE @iNumberMax int

		--Check da khoi tao chua?
		SET @iNumberMax = (SELECT COUNT(ALLOCATE_CODE)
			FROM B_KPI_ALLOCATES
			WHERE 1=1
					AND DELETED = 0				
			);
		print @iNumberMax
		if @iNumberMax = 0 begin
			set @iNumberMax = 1
		end	

		set @ALLOCATE_CODE = CONVERT(VARCHAR(50), CONCAT('AKPICNKI-', @YEAR, '-', RIGHT('00000' + CONVERT(VARCHAR(5), @iNumberMax + 1), 5)));
		print @ALLOCATE_CODE

		insert into B_KPI_ALLOCATES
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ALLOCATE_CODE                      
			, ALLOCATE_NAME                      
			, YEAR                               
			, PERIOD                             
			, VERSION_NUMBER                     
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, ALLOCATE_TYPE                      
			, ORGANIZATION_ID                    
			, EMPLOYEE_ID                        
			, STATUS                             
			, KPI_STANDARD_ID                    
			, FILE_ID                            
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, STAFT_NUMBER                       
			, DESCRIPTION                        
			, REMARK                             
			 , ORGANIZATION_CODE
			 , MA_NHAN_VIEN
			 , TOTAL_PLAN_PERCENT
			 , TOTAL_PLAN_VALUE
			 , KPI_GROUP_ID
			 , FLEX1
			 , FLEX2
			 , FLEX3
			 , FLEX4
			 , FLEX5
			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, LTRIM(RTRIM(@ALLOCATE_CODE))                   
			, @ALLOCATE_NAME                      
			, @YEAR                               
			, @PERIOD                             
			, @VERSION_NUMBER                     
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @ALLOCATE_TYPE                      
			, @ORGANIZATION_ID                    
			, (SELECT ID FROM vwEMPLOYEES_Edit where USER_CODE_C =@MA_NHAN_VIEN)--@EMPLOYEE_ID                        
			, @STATUS                             
			, @KPI_STANDARD_ID                    
			, @FILE_ID                            
			, (SELECT ID FROM vwEMPLOYEES_Edit where USER_NAME =@ASSIGN_BY)--@ASSIGN_BY                          
			, @ASSIGN_DATE                        
			, @STAFT_NUMBER                       
			, @DESCRIPTION                        
			, @REMARK                             
			 , @ORGANIZATION_CODE
			 , @MA_NHAN_VIEN
			 , @TOTAL_PLAN_PERCENT
			 , @TOTAL_PLAN_VALUE
			 , @KPI_GROUP_ID
			 , @FLEX1
			 , @FLEX2
			 , @FLEX3
			 , @FLEX4
			 , @FLEX5
			);
	end else begin
		select TOP 1 @ALLOCATE_CODE=ALLOCATE_CODE from B_KPI_ALLOCATES where [YEAR] = @YEAR AND PERIOD = @PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN;
		update B_KPI_ALLOCATES
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     --, ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , ALLOCATE_NAME                        = @ALLOCATE_NAME                      
		     --, YEAR                                 = @YEAR                               
		     --, PERIOD                               = @PERIOD                             
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , ALLOCATE_TYPE                        = @ALLOCATE_TYPE                      
		     , ORGANIZATION_ID                      = @ORGANIZATION_ID                    
		     , EMPLOYEE_ID                          = (SELECT ID FROM vwEMPLOYEES_Edit where USER_CODE_C =@MA_NHAN_VIEN)--@EMPLOYEE_ID                        
		     , STATUS                               = @STATUS                             
		     , KPI_STANDARD_ID                      = @KPI_STANDARD_ID                    
		     , FILE_ID                              = @FILE_ID                            
		     , ASSIGN_BY                            = (SELECT ID FROM vwEMPLOYEES_Edit where USER_NAME =@ASSIGN_BY)--@ASSIGN_BY                          
		     , ASSIGN_DATE                          = @ASSIGN_DATE                        
		     , STAFT_NUMBER                         = @STAFT_NUMBER                       
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK       
			 , ORGANIZATION_CODE					= @ORGANIZATION_CODE
			 --, MA_NHAN_VIEN							= @MA_NHAN_VIEN
			 , TOTAL_PLAN_PERCENT					= @TOTAL_PLAN_PERCENT
			 , TOTAL_PLAN_VALUE						= @TOTAL_PLAN_VALUE
			 , KPI_GROUP_ID							= @KPI_GROUP_ID
			 , FLEX1								= @FLEX1
			 , FLEX2								= @FLEX2
			 , FLEX3								= @FLEX3
			 , FLEX4								= @FLEX4
			 , FLEX5								= @FLEX5                      

		-- where ID                                   = @ID                       ;
		where [YEAR] = @YEAR AND PERIOD = @PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATES_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATES_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0203', @TAG_SET_NAME;
	end -- if;

  end

GO


