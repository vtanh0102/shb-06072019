USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ALLOCATE_DETAILS_Import]    Script Date: 7/21/2018 3:13:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ALLOCATE_DETAILS_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_ALLOCATE_ID                    uniqueidentifier
	, @ALLOCATE_CODE                      nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @EMPLOYEE_ID                        uniqueidentifier
	, @MA_NHAN_VIEN                       nvarchar(50)
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @YEAR			                      int
	, @KPI_UNIT                           int
	, @UNIT                               int
	, @RADIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @KPI_GROUP_DETAIL_ID                uniqueidentifier
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @TOTAL_VALUE                        float
	, @MONTH_1                            float
	, @MONTH_2                            float
	, @MONTH_3                            float
	, @MONTH_4                            float
	, @MONTH_5                            float
	, @MONTH_6                            float
	, @MONTH_7                            float
	, @MONTH_8                            float
	, @MONTH_9                            float
	, @MONTH_10                           float
	, @MONTH_11                           float
	, @MONTH_12                           float
	, @FLEX1                              nvarchar(1000)
	, @FLEX2                              nvarchar(1000)
	, @FLEX3                              nvarchar(1000)
	, @FLEX4                              nvarchar(1000)
	, @FLEX5                              nvarchar(1000)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier, @LEVEL_NUMBER int, @POSITION_ID uniqueidentifier, @AREA_ID uniqueidentifier, @KPI_STANDARD_ID uniqueidentifier;
	--exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;
	--SELECT POSITION USER
	SELECT TOP 1 @POSITION_ID = POSITION_ID_C FROM vwEMPLOYEES_Edit WHERE USER_CODE_C = @MA_NHAN_VIEN;
	--SELECT AREA TAM THOI CHUA DUNG
	--SELECT TOP 1 @AREA_ID = AREA_C FROM vwEMPLOYEES_Edit WHERE USER_CODE_C = @MA_NHAN_VIEN;
	--SELECT KPI STANDARD
	--SELECT TOP 1 @KPI_STANDARD_ID = ID FROM vwB_KPI_STANDARD_Edit where AREA_ID_C = @AREA_ID AND POSITION_ID = @POSITION_ID AND APPROVE_STATUS = '65';
	SELECT TOP 1 @KPI_STANDARD_ID = ID FROM vwB_KPI_STANDARD_Edit where POSITION_ID = @POSITION_ID AND APPROVE_STATUS = '65';


	if not exists(select * from B_KPI_ALLOCATE_DETAILS where ALLOCATE_CODE = @ALLOCATE_CODE AND MA_NHAN_VIEN = @MA_NHAN_VIEN AND KPI_CODE = @KPI_CODE) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ALLOCATE_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_ALLOCATE_ID                    
			, ALLOCATE_CODE                      
			, VERSION_NUMBER                     
			, EMPLOYEE_ID                        
			, MA_NHAN_VIEN                       
			, KPI_CODE                           
			, KPI_NAME                           
			, LEVEL_NUMBER                       
			, KPI_UNIT                           
			, UNIT                               
			, RADIO                              
			, MAX_RATIO_COMPLETE                 
			, KPI_GROUP_DETAIL_ID                
			, DESCRIPTION                        
			, REMARK                             
			, TOTAL_VALUE                        
			, MONTH_1                            
			, MONTH_2                            
			, MONTH_3                            
			, MONTH_4                            
			, MONTH_5                            
			, MONTH_6                            
			, MONTH_7                            
			, MONTH_8                            
			, MONTH_9                            
			, MONTH_10                           
			, MONTH_11                           
			, MONTH_12                           
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_ALLOCATE_ID                    
			, @ALLOCATE_CODE                      
			, @VERSION_NUMBER                     
			, (SELECT TOP 1 ID FROM vwEMPLOYEES_Edit WHERE USER_CODE_C = @MA_NHAN_VIEN) --@EMPLOYEE_ID                        
			, @MA_NHAN_VIEN                       
			, @KPI_CODE                           
			, (select TOP 1 KPI_NAME from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@KPI_NAME                           
			, @LEVEL_NUMBER                       
			, (select TOP 1 KPI_UNIT from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@KPI_UNIT                           
			, (select TOP 1 KPI_UNIT from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@UNIT                               
			, (select TOP 1 RATIO from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@RADIO                              
			, (select TOP 1 MAX_RATIO_COMPLETE from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@MAX_RATIO_COMPLETE                 
			, (select TOP 1 ID from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@KPI_GROUP_DETAIL_ID                
			, @DESCRIPTION                        
			, @REMARK                             
			, @TOTAL_VALUE                        
			, @MONTH_1                            
			, @MONTH_2                            
			, @MONTH_3                            
			, @MONTH_4                            
			, @MONTH_5                            
			, @MONTH_6                            
			, @MONTH_7                            
			, @MONTH_8                            
			, @MONTH_9                            
			, @MONTH_10                           
			, @MONTH_11                           
			, @MONTH_12                           
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ALLOCATE_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     --, KPI_ALLOCATE_ID                      = @KPI_ALLOCATE_ID                    
		     --, ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     --, VERSION_NUMBER                       = @VERSION_NUMBER                     
		     --, EMPLOYEE_ID                          = @EMPLOYEE_ID                        
		     --, MA_NHAN_VIEN                         = @MA_NHAN_VIEN                       
		     , KPI_CODE                             = @KPI_CODE                           
		     , KPI_NAME                             = (select TOP 1 KPI_NAME from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@KPI_NAME                           
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , KPI_UNIT                             = (select TOP 1 KPI_UNIT from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@KPI_UNIT                           
		     , UNIT                                 = (select TOP 1 KPI_UNIT from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@UNIT                               
		     , RADIO                                = (select TOP 1 RATIO from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@RADIO                              
		     , MAX_RATIO_COMPLETE                   = (select TOP 1 MAX_RATIO_COMPLETE from vwB_KPI_STANDARD_DETAILS_Edit where KPI_STANDARD_ID = @KPI_STANDARD_ID AND KPI_CODE = @KPI_CODE)--@MAX_RATIO_COMPLETE                 
		     , KPI_GROUP_DETAIL_ID                  = @KPI_GROUP_DETAIL_ID                
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , TOTAL_VALUE                          = @TOTAL_VALUE                        
		     , MONTH_1                              = @MONTH_1                            
		     , MONTH_2                              = @MONTH_2                            
		     , MONTH_3                              = @MONTH_3                            
		     , MONTH_4                              = @MONTH_4                            
		     , MONTH_5                              = @MONTH_5                            
		     , MONTH_6                              = @MONTH_6                            
		     , MONTH_7                              = @MONTH_7                            
		     , MONTH_8                              = @MONTH_8                            
		     , MONTH_9                              = @MONTH_9                            
		     , MONTH_10                             = @MONTH_10                           
		     , MONTH_11                             = @MONTH_11                           
		     , MONTH_12                             = @MONTH_12                           
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		--where ID                                   = @ID;
		where ALLOCATE_CODE = @ALLOCATE_CODE AND MA_NHAN_VIEN = @MA_NHAN_VIEN AND KPI_CODE = @KPI_CODE;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ALLOCATE_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ALLOCATE_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_KPI_ALLOCATE_DETAILS', @TAG_SET_NAME;
	end -- if;

  end

GO


