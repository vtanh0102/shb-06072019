USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ACTUAL_RESULT_Update_Import]    Script Date: 7/20/2018 12:41:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[spB_KPI_ACTUAL_RESULT_Update_Import]
	( @ID                                 uniqueidentifier output
	
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @MA_NHAN_VIEN                       nvarchar(10)	
	, @PERCENT_FINAL_TOTAL                float	
	, @DESCRIPTION                        nvarchar(max)	
	)
as
  begin
	set nocount on

	update B_KPI_ACTUAL_RESULT
		   set
		       DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()            
		     , PERCENT_FINAL_TOTAL                  = @PERCENT_FINAL_TOTAL 
			 , LATEST_SYNC_DATE						= GETDATE()                     
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @DESCRIPTION   
	where YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN;		

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ACTUAL_RESULT_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ACTUAL_RESULT_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
  end
GO