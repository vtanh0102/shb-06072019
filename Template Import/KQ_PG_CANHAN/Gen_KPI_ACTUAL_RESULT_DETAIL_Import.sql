USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ACTUAL_RESULT_DETAIL_Import]    Script Date: 7/20/2018 12:04:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Datdq
-- Description:	Khoi tao chi tiet ket qua thuc hien KPI de bat dau thuc hien dong bo
-- =============================================
CREATE PROCEDURE [dbo].[spB_KPI_ACTUAL_RESULT_DETAIL_Import] 
	@iYear int,
	@sMONTH_PERIOD varchar(2),
	@MaNhanVien varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @guiACTUAL_RESULT_ID uniqueidentifier
	DECLARE @sACTUAL_RESULT_CODE varchar(50)

	DECLARE @guiEMPLOYEE_ID uniqueidentifier	
	DECLARE @sALLOCATE_CODE varchar(50)
		
	IF @MaNhanVien <> ''
		BEGIN
		DECLARE actualResultCursor CURSOR 
	  LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
		SELECT [ID], [ACTUAL_RESULT_CODE], [EMPLOYEE_ID] ,[ALLOCATE_CODE]
		FROM [B_KPI_ACTUAL_RESULT]
		WHERE 1=1
				AND [YEAR] = @iYear
				AND [MONTH_PERIOD] = @sMONTH_PERIOD
				AND MA_NHAN_VIEN = @MaNhanVien
	OPEN actualResultCURSOR
	FETCH NEXT FROM actualResultCURSOR 
		INTO @guiACTUAL_RESULT_ID, @sACTUAL_RESULT_CODE, @guiEMPLOYEE_ID, @sALLOCATE_CODE
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		PRINT @sALLOCATE_CODE
		---Khoi tao du lieu ket qua KPI detail--
		INSERT INTO [dbo].[B_KPI_ACT_RESULT_DETAIL]
           ([ID]
           ,[DELETED]
           ,[CREATED_BY]
           ,[DATE_ENTERED]
           ,[MODIFIED_USER_ID]
           ,[DATE_MODIFIED]
           ,[DATE_MODIFIED_UTC]
           ,[ASSIGNED_USER_ID]
           ,[TEAM_ID]
           ,[TEAM_SET_ID]
           ,[NAME]
           ,[ACTUAL_RESULT_CODE]
           ,[YEAR]
           ,[MONTH_PERIOD]
           ,[EMPLOYEE_ID]
           ,[MA_NHAN_VIEN]
           ,[VERSION_NUMBER]
           ,[KPI_ID]
           ,[KPI_CODE]
           ,[KPI_NAME]
           ,[KPI_UNIT]
           ,[LEVEL_NUMBER]
           ,[RATIO]
           ,[PLAN_VALUE]
           ,[SYNC_VALUE]
           ,[FINAL_VALUE]
           --,[PERCENT_SYNC_VALUE]
           --,[PERCENT_FINAL_VALUE]
           --,[PERCENT_MANUAL_VALUE]
           --,[DESCRIPTION]
           --,[REMARK]
           ,[LATEST_SYNC_DATE]
           ,[FLEX1]
           ,[FLEX2]
           ,[FLEX3]
           ,[FLEX4]
           ,[FLEX5])
		SELECT NEWID()
		  ,AlcDetail.[DELETED]
		  ,AlcDetail.[CREATED_BY]
		  ,AlcDetail.[DATE_ENTERED]
		  ,AlcDetail.[MODIFIED_USER_ID]
		  ,AlcDetail.[DATE_MODIFIED]
		  ,AlcDetail.[DATE_MODIFIED_UTC]
		  ,AlcDetail.[ASSIGNED_USER_ID]
		  ,AlcDetail.[TEAM_ID]
		  ,AlcDetail.[TEAM_SET_ID]
		  ,''
		  ,@sACTUAL_RESULT_CODE
		  ,@iYear
		  ,@sMONTH_PERIOD
		  ,@guiEMPLOYEE_ID
		  ,AlcDetail.MA_NHAN_VIEN
		  ,VERSION_NUMBER
		  ,mKpi.ID
		  ,AlcDetail.KPI_CODE
		  ,AlcDetail.KPI_NAME
		  ,AlcDetail.KPI_UNIT
		  ,mKpi.[LEVEL_NUMBER]
		  ,AlcDetail.[RADIO]
		  ,CASE
			WHEN @sMONTH_PERIOD='01' THEN MONTH_1
			WHEN @sMONTH_PERIOD='02' THEN MONTH_2
			WHEN @sMONTH_PERIOD='03' THEN MONTH_3
			WHEN @sMONTH_PERIOD='04' THEN MONTH_4
			WHEN @sMONTH_PERIOD='05' THEN MONTH_5
			WHEN @sMONTH_PERIOD='06' THEN MONTH_6
			WHEN @sMONTH_PERIOD='07' THEN MONTH_7
			WHEN @sMONTH_PERIOD='08' THEN MONTH_8
			WHEN @sMONTH_PERIOD='09' THEN MONTH_9
			WHEN @sMONTH_PERIOD='10' THEN MONTH_10
			WHEN @sMONTH_PERIOD='11' THEN MONTH_11
			WHEN @sMONTH_PERIOD='12' THEN MONTH_12
		   END       
		  ,0
		  ,0
		  --,0
		  --,0
		  --,0
		  --,''
		  --,''
		  ,GETDATE()      
		 ,[FLEX1]
		 ,[FLEX2]
		 ,[FLEX3]
		 ,[FLEX4]
		 ,[FLEX5]
		FROM [dbo].[B_KPI_ALLOCATE_DETAILS] AlcDetail
			INNER JOIN M_KPIS mKpi ON mKpi.[KPI_CODE]=AlcDetail.[KPI_CODE]
			WHERE 1=1 
			AND AlcDetail.[ALLOCATE_CODE] = @sALLOCATE_CODE		
			AND AlcDetail.MA_NHAN_VIEN = @MaNhanVien

	FETCH NEXT FROM actualResultCURSOR 
		INTO @guiACTUAL_RESULT_ID, @sACTUAL_RESULT_CODE, @guiEMPLOYEE_ID, @sALLOCATE_CODE
	END
	CLOSE actualResultCURSOR
	DEALLOCATE actualResultCURSOR
		END
END


GO


