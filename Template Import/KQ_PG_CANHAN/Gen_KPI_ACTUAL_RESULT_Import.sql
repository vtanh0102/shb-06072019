USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ACTUAL_RESULT_Import]    Script Date: 7/19/2018 11:56:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Datdq
-- Description:	Khoi tao ket qua KPI de bat dau dong bo tu cac he thong khac
-- =============================================
CREATE PROCEDURE [dbo].[spB_KPI_ACTUAL_RESULT_Import] 
  @ID    uniqueidentifier output,
  @MaNhanVien varchar(50),
  @iYear int,
  @sMonth varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @APPROVE_STATUS_DONE nvarchar(10) = '65'

	DECLARE @iNumberRec int

	--Check da khoi tao chua?
	SET @iNumberRec = (SELECT COUNT(*)
		FROM [B_KPI_ACTUAL_RESULT]
		WHERE 1=1
				AND [YEAR] = @iYear
				AND [MONTH_PERIOD] = @sMonth
				AND MA_NHAN_VIEN = @MaNhanVien
				AND DELETED = 0
		)
			
	PRINT @iNumberRec
	--Neu da khoi tao thi khong khoi tao lai
	if @iNumberRec > 0  return;

	if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
		set @ID = newid();
	end -- if;
	 	
	INSERT INTO [dbo].[B_KPI_ACTUAL_RESULT]
			([ID]
			,[DELETED]
			,[CREATED_BY]
			,[DATE_ENTERED]
			,[MODIFIED_USER_ID]
			,[DATE_MODIFIED]
			,[DATE_MODIFIED_UTC]
			,[ASSIGNED_USER_ID]
			,[TEAM_ID]
			,[TEAM_SET_ID]
			,[NAME]
			,[ACTUAL_RESULT_CODE]
			,[YEAR]
			,[MONTH_PERIOD]
			,[EMPLOYEE_ID]
			,[MA_NHAN_VIEN]
			,[VERSION_NUMBER]
			,[ALLOCATE_CODE]
			,[ASSIGN_BY]
			,[ASSIGN_DATE]
			,[PERCENT_SYNC_TOTAL]
			,[PERCENT_FINAL_TOTAL]
			,[PERCENT_MANUAL_TOTAL]
			,[TOTAL_AMOUNT_01]
			,[TOTAL_AMOUNT_02]
			,[TOTAL_AMOUNT_03]
			,[DESCRIPTION]
			,[REMARK]
			,[FILE_ID]
			,[LATEST_SYNC_DATE]
			--,[APPROVE_ID]
			--,[APPROVE_STATUS]
			--,[APPROVED_BY]
			--,[APPROVED_DATE]
			,[FLEX1]
			,[FLEX2]
			,[FLEX3]
			,[FLEX4]
			,[FLEX5])     
	SELECT @ID
		,[DELETED]
		,[CREATED_BY]
		,[DATE_ENTERED]
		,[MODIFIED_USER_ID]
		,[DATE_MODIFIED]
		,[DATE_MODIFIED_UTC]
		,[ASSIGNED_USER_ID]
		,[TEAM_ID]
		,[TEAM_SET_ID]
		,[NAME]
		, ALLOCATE_CODE     
		,[YEAR]
		,@sMonth
		,EMPLOYEE_ID
		,MA_NHAN_VIEN 
		,VERSION_NUMBER
		,ALLOCATE_CODE
		,ASSIGN_BY
		,ASSIGN_DATE
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,''
		,ALC.[FILE_ID]
		,GETDATE()
		--,APPROVED_BY
		--,APPROVE_STATUS
		--,APPROVED_BY
		--,APPROVED_DATE
		,ALC.[FLEX1]
		,ALC.[FLEX2]
		,ALC.[FLEX3]
		,ALC.[FLEX4]
		,ALC.[FLEX5]
	FROM [dbo].[B_KPI_ALLOCATES] ALC   
	WHERE 1=1 				
		AND ALC.[YEAR] = @iYear
		AND ALC.[APPROVE_STATUS] = @APPROVE_STATUS_DONE
		AND ALC.MA_NHAN_VIEN = @MaNhanVien AND ALC.DELETED = 0;

	EXEC [dbo].spB_KPI_ACTUAL_RESULT_DETAIL_Import @iYear, @sMonth, @MaNhanVien	
	
END