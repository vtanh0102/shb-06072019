USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ACTUAL_RESULT_Import]    Script Date: 7/7/2018 10:36:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ACTUAL_RESULT_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME                               nvarchar(150)
	, @ACTUAL_RESULT_CODE                 nvarchar(50)
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(50)
	, @EMPLOYEE_ID                        uniqueidentifier
	, @MA_NHAN_VIEN                       nvarchar(10)
	, @VERSION_NUMBER                     nvarchar(2)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @ASSIGN_BY                          uniqueidentifier
	, @ASSIGN_DATE                        datetime
	, @PERCENT_SYNC_TOTAL                 float
	, @PERCENT_FINAL_TOTAL                float
	, @PERCENT_MANUAL_TOTAL               float
	, @TOTAL_AMOUNT_01                    float
	, @TOTAL_AMOUNT_02                    float
	, @TOTAL_AMOUNT_03                    float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)
	, @FILE_ID                            uniqueidentifier
	, @LATEST_SYNC_DATE                   datetime
	, @APPROVE_ID                         uniqueidentifier
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        uniqueidentifier
	, @APPROVED_DATE                      datetime
	, @FLEX1                              nvarchar(50)
	, @FLEX2                              nvarchar(50)
	, @FLEX3                              nvarchar(50)
	, @FLEX4                              nvarchar(50)
	, @FLEX5                              nvarchar(50)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ACTUAL_RESULT where ACTUAL_RESULT_CODE = @ACTUAL_RESULT_CODE AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ACTUAL_RESULT
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, ACTUAL_RESULT_CODE                 
			, YEAR                               
			, MONTH_PERIOD                       
			, EMPLOYEE_ID                        
			, MA_NHAN_VIEN                       
			, VERSION_NUMBER                     
			, ALLOCATE_CODE                      
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, PERCENT_SYNC_TOTAL                 
			, PERCENT_FINAL_TOTAL                
			, PERCENT_MANUAL_TOTAL               
			, TOTAL_AMOUNT_01                    
			, TOTAL_AMOUNT_02                    
			, TOTAL_AMOUNT_03                    
			, DESCRIPTION                        
			, REMARK                             
			, FILE_ID                            
			, LATEST_SYNC_DATE                   
			, APPROVE_ID                         
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @ACTUAL_RESULT_CODE                 
			, @YEAR                               
			, @MONTH_PERIOD                       
			, @EMPLOYEE_ID                        
			, @MA_NHAN_VIEN                       
			, @VERSION_NUMBER                     
			, @ALLOCATE_CODE                      
			, @ASSIGN_BY                          
			, @ASSIGN_DATE                        
			, @PERCENT_SYNC_TOTAL                 
			, @PERCENT_FINAL_TOTAL                
			, @PERCENT_MANUAL_TOTAL               
			, @TOTAL_AMOUNT_01                    
			, @TOTAL_AMOUNT_02                    
			, @TOTAL_AMOUNT_03                    
			, @DESCRIPTION                        
			, @REMARK                             
			, @FILE_ID                            
			, @LATEST_SYNC_DATE                   
			, @APPROVE_ID                         
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ACTUAL_RESULT
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     , ACTUAL_RESULT_CODE                   = @ACTUAL_RESULT_CODE                 
		     , YEAR                                 = @YEAR                               
		     , MONTH_PERIOD                         = @MONTH_PERIOD                       
		     , EMPLOYEE_ID                          = @EMPLOYEE_ID                        
		     , MA_NHAN_VIEN                         = @MA_NHAN_VIEN                       
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , ASSIGN_BY                            = @ASSIGN_BY                          
		     , ASSIGN_DATE                          = @ASSIGN_DATE                        
		     , PERCENT_SYNC_TOTAL                   = @PERCENT_SYNC_TOTAL                 
		     , PERCENT_FINAL_TOTAL                  = @PERCENT_FINAL_TOTAL                
		     , PERCENT_MANUAL_TOTAL                 = @PERCENT_MANUAL_TOTAL               
		     , TOTAL_AMOUNT_01                      = @TOTAL_AMOUNT_01                    
		     , TOTAL_AMOUNT_02                      = @TOTAL_AMOUNT_02                    
		     , TOTAL_AMOUNT_03                      = @TOTAL_AMOUNT_03                    
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , FILE_ID                              = @FILE_ID                            
		     , LATEST_SYNC_DATE                     = @LATEST_SYNC_DATE                   
		     , APPROVE_ID                           = @APPROVE_ID                         
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 --where ID                                   = @ID                                 ;
		 where ACTUAL_RESULT_CODE = @ACTUAL_RESULT_CODE AND YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ACTUAL_RESULT_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ACTUAL_RESULT_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB0302', @TAG_SET_NAME;
	end -- if;

  end

GO


