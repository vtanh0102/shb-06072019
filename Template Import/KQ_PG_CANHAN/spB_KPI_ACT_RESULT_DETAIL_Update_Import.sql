USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ACT_RESULT_DETAIL_Update_Import]    Script Date: 7/20/2018 12:50:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ACT_RESULT_DETAIL_Update_Import]
	( @ID                                 uniqueidentifier output	
	, @YEAR                               int
	, @MONTH_PERIOD                       nvarchar(2)	
	, @MA_NHAN_VIEN                       nvarchar(50)	
	, @KPI_CODE                           nvarchar(50)	
	, @FINAL_VALUE                        float	
	, @REMARK                             nvarchar(max)	
	)
as
  begin
	set nocount on	
	update B_KPI_ACT_RESULT_DETAIL
		set   DATE_MODIFIED                        =  getdate()                          
		    , DATE_MODIFIED_UTC                    =  getutcdate()                        
		    , FINAL_VALUE                          = @FINAL_VALUE          
		    , DESCRIPTION                          = @REMARK                        
		    , REMARK                               = @REMARK                             
		    , LATEST_SYNC_DATE                     = GETDATE()                   
		                             

		where YEAR = @YEAR AND MONTH_PERIOD = @MONTH_PERIOD AND MA_NHAN_VIEN = @MA_NHAN_VIEN AND KPI_CODE = @KPI_CODE;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ACT_RESULT_DETAIL_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ACT_RESULT_DETAIL_CSTM ( ID_C ) values ( @ID );
		end -- if;
	end -- if;
  end

GO


