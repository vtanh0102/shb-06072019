USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ORG_ALLOCATE_DETAILS_Import]    Script Date: 7/14/2018 3:55:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ORG_ALLOCATE_DETAILS_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @NAME								  nvarchar(150)
	, @KPI_ALLOCATE_ID                    nvarchar(150)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @ORGANIZATION_CODE                  nvarchar(50)
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @LEVEL_NUMBER                       int
	, @KPI_UNIT                           int
	, @UNIT                               int
	, @RADIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @KPI_GROUP_DETAIL_ID                nvarchar(150)
	, @DESCRIPTION                        nvarchar(200)
	, @REMARK                             nvarchar(200)
	, @TOTAL_VALUE                        float
	, @MONTH_1                            float
	, @MONTH_2                            float
	, @MONTH_3                            float
	, @MONTH_4                            float
	, @MONTH_5                            float
	, @MONTH_6                            float
	, @MONTH_7                            float
	, @MONTH_8                            float
	, @MONTH_9                            float
	, @MONTH_10                           float
	, @MONTH_11                           float
	, @MONTH_12                           float
	, @FLEX1                              nvarchar(200)
	, @FLEX2                              nvarchar(200)
	, @FLEX3                              nvarchar(200)
	, @FLEX4                              nvarchar(300)
	, @FLEX5                              nvarchar(200)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier,@TYPE INT,@GROUP_KPI_ID uniqueidentifier;
	--SELECT TYPE FROM M_ORGANIZATION
	select TOP 1 @TYPE = LEVEL_NUMBER from M_ORGANIZATION where ORGANIZATION_CODE = @ORGANIZATION_CODE;
	--SELECT KPI_GROUP_ID FROM M_GROUP_KPIS
	select TOP 1 @GROUP_KPI_ID = ID from M_GROUP_KPIS where [TYPE] = @TYPE AND [YEAR] = @LEVEL_NUMBER AND APPROVE_STATUS = 65;

	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ORG_ALLOCATE_DETAILS where ALLOCATE_CODE = @ALLOCATE_CODE AND ORGANIZATION_CODE = @ORGANIZATION_CODE AND KPI_CODE = @KPI_CODE AND DELETED = 0) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ORG_ALLOCATE_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, NAME                               
			, KPI_ALLOCATE_ID                    
			, ALLOCATE_CODE                      
			, VERSION_NUMBER                     
			, ORGANIZATION_CODE                  
			, KPI_CODE                           
			, KPI_NAME                           
			, LEVEL_NUMBER                       
			, KPI_UNIT                           
			, UNIT                               
			, RADIO                              
			, MAX_RATIO_COMPLETE                 
			, KPI_GROUP_DETAIL_ID                
			, DESCRIPTION                        
			, REMARK                             
			, TOTAL_VALUE                        
			, MONTH_1                            
			, MONTH_2                            
			, MONTH_3                            
			, MONTH_4                            
			, MONTH_5                            
			, MONTH_6                            
			, MONTH_7                            
			, MONTH_8                            
			, MONTH_9                            
			, MONTH_10                           
			, MONTH_11                           
			, MONTH_12                           
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @NAME                               
			, @KPI_ALLOCATE_ID                    
			, @ALLOCATE_CODE                      
			, @VERSION_NUMBER                     
			, @ORGANIZATION_CODE                  
			, @KPI_CODE                           
			, @KPI_NAME                           
			, @LEVEL_NUMBER                       
			, (SELECT TOP 1 UNIT from vwM_GROUP_KPI_DETAILS_Edit  WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)----@KPI_UNIT                           
			, (SELECT TOP 1 UNIT FROM vwM_GROUP_KPI_DETAILS_Edit  WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)----@UNIT                               
			, (SELECT TOP 1 RATIO FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)----@RADIO                              
			, (SELECT TOP 1 MAX_RATIO_COMPLETE FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)--@MAX_RATIO_COMPLETE                 
			, @KPI_GROUP_DETAIL_ID                
			, @DESCRIPTION                        
			, @REMARK                             
			, @TOTAL_VALUE                        
			, @MONTH_1                            
			, @MONTH_2                            
			, @MONTH_3                            
			, @MONTH_4                            
			, @MONTH_5                            
			, @MONTH_6                            
			, @MONTH_7                            
			, @MONTH_8                            
			, @MONTH_9                            
			, @MONTH_10                           
			, @MONTH_11                           
			, @MONTH_12                           
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ORG_ALLOCATE_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , NAME                                 = @NAME                               
		     --, KPI_ALLOCATE_ID                      = @KPI_ALLOCATE_ID                    
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , ORGANIZATION_CODE                    = @ORGANIZATION_CODE                  
		     , KPI_CODE                             = @KPI_CODE                           
		     , KPI_NAME                             = @KPI_NAME                           
		     , LEVEL_NUMBER                         = @LEVEL_NUMBER                       
		     , KPI_UNIT                             = (SELECT TOP 1 UNIT FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)--@KPI_UNIT                           
		     , UNIT                                 = (SELECT TOP 1 UNIT FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)--@UNIT                               
		     , RADIO                                = (SELECT TOP 1 RATIO FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)--@RADIO                              
		     , MAX_RATIO_COMPLETE                   = (SELECT TOP 1 MAX_RATIO_COMPLETE FROM vwM_GROUP_KPI_DETAILS_Edit WHERE GROUP_KPI_ID = @GROUP_KPI_ID AND KPI_CODE = @KPI_CODE)--@MAX_RATIO_COMPLETE                 
		     , KPI_GROUP_DETAIL_ID                  = @KPI_GROUP_DETAIL_ID                
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             
		     , TOTAL_VALUE                          = @TOTAL_VALUE                        
		     , MONTH_1                              = @MONTH_1                            
		     , MONTH_2                              = @MONTH_2                            
		     , MONTH_3                              = @MONTH_3                            
		     , MONTH_4                              = @MONTH_4                            
		     , MONTH_5                              = @MONTH_5                            
		     , MONTH_6                              = @MONTH_6                            
		     , MONTH_7                              = @MONTH_7                            
		     , MONTH_8                              = @MONTH_8                            
		     , MONTH_9                              = @MONTH_9                            
		     , MONTH_10                             = @MONTH_10                           
		     , MONTH_11                             = @MONTH_11                           
		     , MONTH_12                             = @MONTH_12                           
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		 --where ID                                   = @ID                                 ;
		where ALLOCATE_CODE = @ALLOCATE_CODE AND ORGANIZATION_CODE = @ORGANIZATION_CODE AND KPI_CODE = @KPI_CODE AND DELETED = 0;
		exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @ALLOCATE_CODE;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ALLOCATE_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ALLOCATE_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'B_KPI_ORG_ALLOCATE_DETAIL', @TAG_SET_NAME;
	end -- if;

  end

GO


