USE [SplendidCRM1]
GO

/****** Object:  StoredProcedure [dbo].[spB_KPI_ORG_ALLOCATES_Import]    Script Date: 7/14/2018 3:39:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[spB_KPI_ORG_ALLOCATES_Import]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @ALLOCATE_NAME                      nvarchar(200)
	, @ALLOCATE_CODE                      nvarchar(50)
	, @YEAR                               nvarchar(50)
	, @PERIOD                             nvarchar(50)
	, @VERSION_NUMBER                     nvarchar(2)
	, @ORGANIZATION_CODE                  nvarchar(50)
	, @ORGANIZATION_NAME                  nvarchar(200)
	, @APPROVE_ID                         uniqueidentifier
	, @APPROVE_STATUS                     nvarchar(5)
	, @APPROVED_BY                        nvarchar(150)
	, @APPROVED_DATE                      datetime
	, @ALLOCATE_TYPE                      nvarchar(50)
	, @DESCRIPTION                        nvarchar(200)
	, @STAFT_NUMBER                       int
	, @TOTAL_PLAN_PERCENT                 float
	, @TOTAL_PLAN_VALUE                   float
	, @STATUS                             nvarchar(50)
	, @KPI_GROUP_ID                       nvarchar(150)
	, @FILE_ID                            nvarchar(150)
	, @ASSIGN_BY                          nvarchar(150)
	, @ASSIGN_DATE                        datetime
	, @FLEX1                              nvarchar(150)
	, @FLEX2                              nvarchar(150)
	, @FLEX3                              nvarchar(150)
	, @FLEX4                              nvarchar(150)
	, @FLEX5                              nvarchar(150)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from B_KPI_ORG_ALLOCATES where ALLOCATE_CODE = @ALLOCATE_CODE AND YEAR = @YEAR AND PERIOD = @PERIOD AND ORGANIZATION_CODE = @ORGANIZATION_CODE AND DELETED = 0) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into B_KPI_ORG_ALLOCATES
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, ALLOCATE_NAME                      
			, ALLOCATE_CODE                      
			, YEAR                               
			, PERIOD                             
			, VERSION_NUMBER                     
			, ORGANIZATION_CODE                  
			, ORGANIZATION_NAME                  
			, APPROVE_ID                         
			, APPROVE_STATUS                     
			, APPROVED_BY                        
			, APPROVED_DATE                      
			, ALLOCATE_TYPE                      
			, DESCRIPTION                        
			, STAFT_NUMBER                       
			, TOTAL_PLAN_PERCENT                 
			, TOTAL_PLAN_VALUE                   
			, STATUS                             
			, KPI_GROUP_ID                       
			, FILE_ID                            
			, ASSIGN_BY                          
			, ASSIGN_DATE                        
			, FLEX1                              
			, FLEX2                              
			, FLEX3                              
			, FLEX4                              
			, FLEX5                              

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @ALLOCATE_NAME                      
			, @ALLOCATE_CODE                      
			, @YEAR                               
			, @PERIOD                             
			, @VERSION_NUMBER                     
			, @ORGANIZATION_CODE                  
			, @ORGANIZATION_NAME                  
			, @APPROVE_ID                         
			, @APPROVE_STATUS                     
			, @APPROVED_BY                        
			, @APPROVED_DATE                      
			, @ALLOCATE_TYPE                      
			, @DESCRIPTION                        
			, @STAFT_NUMBER                       
			, @TOTAL_PLAN_PERCENT                 
			, @TOTAL_PLAN_VALUE                   
			, @STATUS                             
			, @KPI_GROUP_ID                       
			, @FILE_ID                            
			, (select ID from vwEMPLOYEES_Edit where USER_CODE_C =@ASSIGN_BY)--@ASSIGN_BY                          
			, getdate()--@ASSIGN_DATE                        
			, @FLEX1                              
			, @FLEX2                              
			, @FLEX3                              
			, @FLEX4                              
			, @FLEX5                              

			);
	end else begin
		update B_KPI_ORG_ALLOCATES
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , ALLOCATE_NAME                        = @ALLOCATE_NAME                      
		     , ALLOCATE_CODE                        = @ALLOCATE_CODE                      
		     , YEAR                                 = @YEAR                               
		     , PERIOD                               = @PERIOD                             
		     , VERSION_NUMBER                       = @VERSION_NUMBER                     
		     , ORGANIZATION_CODE                    = @ORGANIZATION_CODE                  
		     , ORGANIZATION_NAME                    = @ORGANIZATION_NAME                  
		     , APPROVE_ID                           = @APPROVE_ID                         
		     , APPROVE_STATUS                       = @APPROVE_STATUS                     
		     , APPROVED_BY                          = @APPROVED_BY                        
		     , APPROVED_DATE                        = @APPROVED_DATE                      
		     , ALLOCATE_TYPE                        = @ALLOCATE_TYPE                      
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , STAFT_NUMBER                         = @STAFT_NUMBER                       
		     , TOTAL_PLAN_PERCENT                   = @TOTAL_PLAN_PERCENT                 
		     , TOTAL_PLAN_VALUE                     = @TOTAL_PLAN_VALUE                   
		     , STATUS                               = @STATUS                             
		     , KPI_GROUP_ID                         = @KPI_GROUP_ID                       
		     , FILE_ID                              = @FILE_ID                            
		     , ASSIGN_BY                            = (select ID from vwEMPLOYEES_Edit where USER_CODE_C =@ASSIGN_BY)--@ASSIGN_BY                          
		     , ASSIGN_DATE                          = getdate()--@ASSIGN_DATE                        
		     , FLEX1                                = @FLEX1                              
		     , FLEX2                                = @FLEX2                              
		     , FLEX3                                = @FLEX3                              
		     , FLEX4                                = @FLEX4                              
		     , FLEX5                                = @FLEX5                              

		-- where ID                                   = @ID                                 ;
		where ALLOCATE_CODE = @ALLOCATE_CODE AND YEAR = @YEAR AND PERIOD = @PERIOD AND ORGANIZATION_CODE = @ORGANIZATION_CODE AND DELETED = 0;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from B_KPI_ORG_ALLOCATES_CSTM where ID_C = @ID) begin -- then
			insert into B_KPI_ORG_ALLOCATES_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPIB020202', @TAG_SET_NAME;
	end -- if;

  end

GO


