USE [CRM_V8]
GO
/****** Object:  StoredProcedure [dbo].[spB_Users_C_Import]    Script Date: 10/10/2018 8:59:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spB_Users_C_Import]
	-- Add the parameters for the stored procedure here
	( @ID                     uniqueidentifier
	, @USER_CODE_C            NVARCHAR(255)     
	, @TYPE_IDENTIFICATION_C  NVARCHAR(255)
    , @IDENTIFICATION_C       NVARCHAR(500)
    , @TITLE_ID_SOURCE        NVARCHAR(10)
    , @ORGANIZATION_PARENT_CODE        NVARCHAR(50)
    , @ORGANIZATION_CODE        NVARCHAR(50)
    , @BIRTH_DATE_C             DATETIME
    , @SEX_C                    NVARCHAR(10)
    , @LITERACY_ID_C            NVARCHAR(50)
    , @UNIVERSITY_C             NVARCHAR(500)
    , @CONTRACT_TYPE_C          NVARCHAR(255)
    , @CONTRACT_TERM_C          NVARCHAR(255)
    , @DATE_START_WORK_C        DATETIME
    , @DATE_END_WORK_C          DATETIME
    , @M_BUSINESS_CODE_C        NVARCHAR(255)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TITLE_ID_C uniqueidentifier
	DECLARE @ORGANIZATION_PARENT_ID_C uniqueidentifier
	DECLARE @ORGANIZATION_ID_C uniqueidentifier
	DECLARE @AREA_C uniqueidentifier
	DECLARE @POSOTION_ID_C uniqueidentifier	

	-- get tile_ID from Title_ID_SOURCE
	SELECT @TITLE_ID_C = ID FROM M_TITLE WHERE ID_SOURCE = CAST(@TITLE_ID_SOURCE as int)
	SELECT @ORGANIZATION_PARENT_ID_C = ID FROM M_ORGANIZATION WHERE ORGANIZATION_CODE = @ORGANIZATION_PARENT_CODE
	SELECT @ORGANIZATION_ID_C = ID,@AREA_C=AREA_ID FROM M_ORGANIZATION WHERE ORGANIZATION_CODE = @ORGANIZATION_CODE
	SELECT @POSOTION_ID_C = ID FROM M_POSITION WHERE POSITION_CODE = 'NHBL-TTS'
    -- Insert statements for procedure here
	UPDATE USERS_CSTM  SET USER_CODE_C = @USER_CODE_C
						,TYPE_IDENTIFICATION_ID_C= @TYPE_IDENTIFICATION_C
						,IDENTIFICATION_C= @IDENTIFICATION_C
						,TITLE_ID_C= @TITLE_ID_C
						,ORGANIZATION_PARENT_ID_C = @ORGANIZATION_PARENT_ID_C
						,ORGANIZATION_ID_C= @ORGANIZATION_ID_C
						,ORGANIZATION_CODE_C= @ORGANIZATION_CODE
						,BIRTH_DATE_C= @BIRTH_DATE_C
						,SEX_C= @SEX_C
						,LITERACY_ID_C= @LITERACY_ID_C
						,UNIVERSITY_C= @UNIVERSITY_C
						,CONTRACT_TYPE_C= @CONTRACT_TYPE_C
						,CONTRACT_TERM_C= @CONTRACT_TERM_C
						,DATE_START_WORK_C= @DATE_START_WORK_C
						,DATE_END_WORK_C= @DATE_END_WORK_C
						,M_BUSINESS_CODE_C= @M_BUSINESS_CODE_C
						,AREA_C    = @AREA_C
						,POSITION_ID_C = @POSOTION_ID_C
						, POSITION_CODE_C = 'NHBL-TTS'
						WHERE ID_C = @ID
END

