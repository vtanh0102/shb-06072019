-- Cap nhat db maping 22-10-2018 từ SHB -> TMP
--================================================================
-- Cấu hình lại bảng MAP DL kéo về chi chỉ tiêu KPIs
 update [CRM_V8TMP].[dbo].[M_SYS_TABLE_MAPPING]
		set [SOURCE_TABLE_COLUMNS]=
		N'FROMDATE,TODATE,HUY_DONG_TD,HUY_DONG_TANG_RONG,HUY_DONG_BQ,DU_NO_TD,DU_NO_TANG_RONG,DU_NO_BQ,SL_KH_VAY,NO_QH,TY_LE_NO_QH,NO_XAU,TY_LE_NO_XAU,SL_KH_VV_MOI,SL_KH_INTNET,SL_KH_SMS,SL_KH_DV_SMS,SL_KH_MOBILE,SL_THE_NOI_DIA,SL_THE_TD_QT,SL_THE_GHINO_QT,DS_GIAINGAN,DS_GIAINGAN_LUYKE,DU_NO_BQ_LUYKE,SL_KH_VV_MOI_LUYKE,HUY_DONG_DAUKY,DU_NO_DAUKY,HUY_DONG_TANG_RONG_LUYKE,DU_NO_TANG_RONG_LUYKE,HUY_DONG_BQ_LUYKE,SL_THE_NOI_DIA_LUYKE,SL_THE_GHINO_QT_LUYKE,SL_THE_TD_QT_LUYKE,SL_KH_INTNET_LUYKE,SL_KH_SMS_LUYKE,SL_KH_DV_SMS_LUYKE,SL_KH_MOBILE_LUYKE,NO_QH_DAUKY,NO_XAU_DAUKY,HUYDONG_DAUKY,TT_CHOVAY,TT_HUYDONG,TT_SMS,TT_CK_247,TT_CK_THUONG,TT_VAS,TT_KHAC,THU_THUAN_THE1,TT_LUYKE',
		[MAP_COLUMN_SOURCE]=
		N'DATE_SYNC,FROMDATE,TODATE,HUY_DONG_TD,HUY_DONG_TANG_RONG,HUY_DONG_BQ,DU_NO_TD,DU_NO_TANG_RONG,DU_NO_BQ,SL_KH_VAY,NO_QH,TY_LE_NO_QH,NO_XAU,TY_LE_NO_XAU,SL_KH_VV_MOI,SL_KH_INTNET,SL_KH_SMS,SL_KH_DV_SMS,SL_KH_MOBILE,SL_THE_NOI_DIA,SL_THE_TD_QT,SL_THE_GHINO_QT,DS_GIAINGAN,DS_GIAINGAN_LUYKE,DU_NO_BQ_LUYKE,SL_KH_VV_MOI_LUYKE,HUY_DONG_DAUKY,DU_NO_DAUKY,HUY_DONG_TANG_RONG_LUYKE,DU_NO_TANG_RONG_LUYKE,HUY_DONG_BQ_LUYKE,SL_THE_NOI_DIA_LUYKE,SL_THE_GHINO_QT_LUYKE,SL_THE_TD_QT_LUYKE,SL_KH_INTNET_LUYKE,SL_KH_SMS_LUYKE,SL_KH_DV_SMS_LUYKE,SL_KH_MOBILE_LUYKE,NO_QH_DAUKY,NO_XAU_DAUKY,HUYDONG_DAUKY,TT_CHOVAY,TT_HUYDONG,TT_SERVICE_SUM,TT_LUYKE',
		[MAP_COLUMN_TARGET]=
		N'DATE_SYNC,FROMDATE,TODATE,VALUE_1,VALUE_2,VALUE_3,VALUE_4,VALUE_5,VALUE_6,VALUE_7,VALUE_8,VALUE_9,VALUE_10,VALUE_11,VALUE_12,VALUE_13,VALUE_14,VALUE_15,VALUE_16,VALUE_17,VALUE_18,VALUE_19,VALUE_20,VALUE_21,VALUE_22,VALUE_23,VALUE_24,VALUE_25,VALUE_26,VALUE_27,VALUE_28,VALUE_29,VALUE_30,VALUE_31,VALUE_32,VALUE_33,VALUE_34,VALUE_35,VALUE_36,VALUE_37,VALUE_38,VALUE_50,VALUE_51,VALUE_53,VALUE_54'
where ID='003'

--Tạo thu thuần Dịch vụ
	-- Cập nhật [CRM_V8TMP].[dbo].[M_SYS_QUERY_MAPPING]
	--ID=3001
	---[SELECT_QUERY]=(TT_SMS+TT_CK_247+TT_CK_THUONG+TT_VAS+TT_KHAC+THU_THUAN_THE1) as TT_SERVICE_SUM

update [CRM_V8TMP].[dbo].[M_SYS_QUERY_MAPPING] set [SELECT_QUERY]=N'(TT_SMS+TT_CK_247+TT_CK_THUONG+TT_VAS+TT_KHAC+THU_THUAN_THE1) as TT_SERVICE_SUM',[DESCRIPTION]=N'Tạo thu thuần Dịch vụ' where ID='3001'

-- Thêm bảng Table TMP cho báo cáo, chạy script tạo bảng
	-- SCRIPT: RESULT_EMP.sql

-- Thêm cấu hình bảng MAP DL cho view kết quả báo cáo.
INSERT [dbo].[M_SYS_TABLE_MAPPING] ([ID], [DB_MAPPING_ID], [SOURCE_TABLE], [SOURCE_TABLE_KEY], [SOURCE_TABLE_COLUMNS], [TARGET_TABLE], [MAP_KEY_SOURCE_TO_TARGET], [MAP_COLUMN_SOURCE], [MAP_COLUMN_TARGET], [FILTER_JOIN_QUERY], [FILTER_WHERE_QUERY], [EXECUTE_ORDER]) VALUES (N'0016', N'ORA_TO_TMP', N'itshbho.CSL_KPIS_NET_VW', N'MAIN_SELL,POS_CD,RPT_YEAR,PERIOD,MACHUCVU', N'FROMDATE,TODATE,HUY_DONG_TD,HUY_DONG_TANG_RONG,HUY_DONG_BQ,DU_NO_TD,DU_NO_TANG_RONG,DU_NO_BQ,SL_KH_VAY,NO_QH,TY_LE_NO_QH,NO_XAU,TY_LE_NO_XAU,SL_KH_VV_MOI,SL_KH_INTNET,SL_KH_SMS,SL_KH_DV_SMS,SL_KH_MOBILE,SL_THE_NOI_DIA,SL_THE_TD_QT,SL_THE_GHINO_QT,DS_GIAINGAN,DS_GIAINGAN_LUYKE,DU_NO_BQ_LUYKE,SL_KH_VV_MOI_LUYKE,HUY_DONG_DAUKY,DU_NO_DAUKY,HUY_DONG_TANG_RONG_LUYKE,DU_NO_TANG_RONG_LUYKE,HUY_DONG_BQ_LUYKE,SL_THE_NOI_DIA_LUYKE,SL_THE_GHINO_QT_LUYKE,SL_THE_TD_QT_LUYKE,SL_KH_INTNET_LUYKE,SL_KH_SMS_LUYKE,SL_KH_DV_SMS_LUYKE,SL_KH_MOBILE_LUYKE,NO_QH_DAUKY,NO_XAU_DAUKY,HUYDONG_DAUKY,TT_CHOVAY,TT_HUYDONG,TT_LUYKE', N'B_KPI_RESULT_NET_TMP', N'USER_CODE,POS_CODE,YEAR,PERIOD,TITLE_CODE', N'FROMDATE,TODATE,HUY_DONG_TD,HUY_DONG_TANG_RONG,HUY_DONG_BQ,DU_NO_TD,DU_NO_TANG_RONG,DU_NO_BQ,SL_KH_VAY,NO_QH,TY_LE_NO_QH,NO_XAU,TY_LE_NO_XAU,SL_KH_VV_MOI,SL_KH_INTNET,SL_KH_SMS,SL_KH_DV_SMS,SL_KH_MOBILE,SL_THE_NOI_DIA,SL_THE_TD_QT,SL_THE_GHINO_QT,DS_GIAINGAN,DS_GIAINGAN_LUYKE,DU_NO_BQ_LUYKE,SL_KH_VV_MOI_LUYKE,HUY_DONG_DAUKY,DU_NO_DAUKY,HUY_DONG_TANG_RONG_LUYKE,DU_NO_TANG_RONG_LUYKE,HUY_DONG_BQ_LUYKE,SL_THE_NOI_DIA_LUYKE,SL_THE_GHINO_QT_LUYKE,SL_THE_TD_QT_LUYKE,SL_KH_INTNET_LUYKE,SL_KH_SMS_LUYKE,SL_KH_DV_SMS_LUYKE,SL_KH_MOBILE_LUYKE,NO_QH_DAUKY,NO_XAU_DAUKY,HUYDONG_DAUKY,TT_CHOVAY,TT_HUYDONG,TT_LUYKE', N'FROMDATE,TODATE,VALUE_1,VALUE_2,VALUE_3,VALUE_4,VALUE_5,VALUE_6,VALUE_7,VALUE_8,VALUE_9,VALUE_10,VALUE_11,VALUE_12,VALUE_13,VALUE_14,VALUE_15,VALUE_16,VALUE_17,VALUE_18,VALUE_19,VALUE_20,VALUE_21,VALUE_22,VALUE_23,VALUE_24,VALUE_25,VALUE_26,VALUE_27,VALUE_28,VALUE_29,VALUE_30,VALUE_31,VALUE_32,VALUE_33,VALUE_34,VALUE_35,VALUE_36,VALUE_37,VALUE_38,VALUE_50,VALUE_51,VALUE_54', NULL, NULL,7)
 
 -- Thêm mã MAIN_POS vào bảng B_KPI_RESULT_POS_TMP 
 -- Cập nhật vào bảng MAIN_POS vào table MAP
 
 -- Xóa DL cũ trong bản KPIs để chạy đồng bộ lại ( Cần comfirm với IT)
 TRUNCATE TABLE B_KPI_RESULT_EMP_TMP
  TRUNCATE TABLE B_KPI_RESULT_POS_TMP
  
  -- Xóa TITLE_CODE; POS_CD bảng B_KPI_RESULT_EMP_LOGS; B_KPI_RESULT_EMP_TMP
  
 -- => Chạy đồng bộ 2.[KPI]SHB_TMP
 
-- Cap nhat db maping 22-10-2018 từ TMP -> CRM 
--================================================================
-- Cập nhật  M_KPI_TOI_SYNC_MAPPING
		update [CRM_V8].[dbo].[M_KPI_TOI_SYNC_MAPPING]  set KPI_CODE='TT03', DESCRIPTION=N'Thu thuần dịch vụ' where KPI_CODE='TT04'

		-- Xóa đi mã TT06 
delete [CRM_V8].[dbo].[M_KPI_TOI_SYNC_MAPPING] where KPI_CODE='TT06'
-- Xóa đi các bản ghi MAP của thu thuần thẻ
delete [CRM_V8].[dbo].[M_KPI_TOI_SYNC_MAPPING] where TYPE='15'
-- Xoa DL thu thuan
TRUNCATE TABLE B_KPI_TOI_ACT_RESULT_DETAIL
TRUNCATE TABLE B_KPI_TOI_ACT_RESULT_DETAIL_AUDIT
TRUNCATE TABLE B_KPI_TOI_ACT_RESULT_DETAIL_B_KPI_TOI_ACTUAL_RESULT
TRUNCATE TABLE B_KPI_TOI_ACT_RESULT_DETAIL_CSTM_AUDIT

TRUNCATE TABLE B_KPI_TOI_ACTUAL_RESULT
TRUNCATE TABLE B_KPI_TOI_ACTUAL_RESULT_AUDIT
TRUNCATE TABLE B_KPI_TOI_ACTUAL_RESULT_CSTM_AUDIT

-- Cập nhật script để chạy đồng bộ tất cả 
update [CRM_V8TMP].[dbo].[SYNC_CONFIG] set [Value]='true' where [Key] ='FirstTimeTT'
update [CRM_V8TMP].[dbo].[SYNC_CONFIG] set [Value]='true' where [Key] ='FirstTimeKPI'

--INSERT [CRM_V8TMP].[dbo].[SYNC_HIS] ( [USER_ID_FK], [DATE_START_TMP], [DATE_END_TMP], [COUNT_TMP], [STATUS_TMP], [REASON_TMP], [DATE_START_CRM], [DATE_END_CRM], [COUNT_CRM], [STATUS_CRM], [REASON_CRM], [TYPE], [IP_SOURCE], [BROWSER]) VALUES ( N'00000000-0000-0000-0000-000000000001', CAST(N'2018-10-18T01:08:00.000' AS DateTime), CAST(N'2018-10-18T01:00:17.000' AS DateTime), N'39530/39530', 0, N'Thành công', NULL, NULL, NULL, NULL, N'', 2, NULL, NULL)
-- Cần kiểm tra lại store Proc trên CRM đã thiết lập mặc định cho tháng hiện tại hay chưa
-- xóa bảng Notify trong CRM 
-- Chạy 2.1; 2.2
-- Bật tính năng bắn Notify trong CRM


 
