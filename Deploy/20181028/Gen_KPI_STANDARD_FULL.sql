
GO

/****** Object:  StoredProcedure [dbo].[Gen_KPI_STANDARD_FULL]    Script Date: 10/30/2018 3:58:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Batch submitted through debugger: SQLQuery10.sql|7|0|C:\Users\Admin\AppData\Local\Temp\~vs4B95.sql
-- =============================================
-- Author:		Tungnx
-- Description:	Sinh Toan bo bo KPIs toi thieu
-- =============================================
ALTER PROCEDURE [dbo].[Gen_KPI_STANDARD_FULL] 
  @iYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Sinh KPI toi thieu cho cac tham nien
	EXEC Gen_KPI_STANDARD_BY_SENIORITY @iYear, 'HSTN'
	--Sinh KPI toi thieu cho cac chuc danh ban hang
	EXEC Gen_KPI_STANDARD_BY_POSITION @iYear, 'HSCV'

	--Sinh KPI toi thieu cho cac khu vuc
	EXEC Gen_KPI_STANDARD_BY_AREA @iYear, 'HSKV'

	--ROUND ALL VALUE
	UPDATE B_KPI_STANDARD_DETAILS SET VALUE_STD_PER_MONTH = ROUND(VALUE_STD_PER_MONTH,0,0)
	WHERE KPI_STANDARD_ID IN (
		SELECT [ID] FROM B_KPI_STANDARD
		WHERE [YEAR] = @iYear
	)

END
GO


