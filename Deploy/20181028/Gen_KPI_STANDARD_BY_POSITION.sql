
GO

/****** Object:  StoredProcedure [dbo].[Gen_KPI_STANDARD_BY_POSITION]    Script Date: 11/8/2018 10:57:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Batch submitted through debugger: SQLQuery10.sql|7|0|C:\Users\Admin\AppData\Local\Temp\~vs4B95.sql
-- =============================================
-- Author:		Tungnx
-- Description:	Sinh bo KPIs toi thieu theo Cap chuc danh ban hang
-- =============================================
ALTER PROCEDURE [dbo].[Gen_KPI_STANDARD_BY_POSITION] 
  @iYear int,
  @sType nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @APPROVE_STATUS_DONE nvarchar(10) = '65'

	DECLARE @B_KPI_STANDARD_ID uniqueidentifier;

	DECLARE @B_KPI_STANDARD_ID_BASE uniqueidentifier;
	DECLARE @KPI_STANDARD_CODE_BASE nvarchar(50);

	DECLARE @sFROM_CODE nvarchar(50);
	DECLARE @sTO_CODE nvarchar(50);
	DECLARE @sTO_NAME nvarchar(200);
	DECLARE @gidFROM_ID uniqueidentifier;
	DECLARE @gidTO_ID uniqueidentifier;
	DECLARE @fCONVERSION_RATE decimal(18,2);

	DECLARE @iSeq int = 0;

	BEGIN
		DECLARE lsConversionCURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY
		FOR 
			SELECT  
				[TYPE]
				,[FROM_CODE]				
				,[TO_CODE]
				,[TO_NAME]
				, [CONVERSION_RATE]
				,(SELECT TOP 1 [ID] FROM [M_POSITION] WHERE [POSITION_CODE] = [FROM_CODE]) AS [FROM_ID]
				,(SELECT TOP 1 [ID] FROM [M_POSITION] WHERE  [POSITION_CODE] = [TO_CODE] ) AS [TO_ID]
			FROM vwM_KPI_CONVERSION_RATE_Edit
			WHERE [STATUS] = '1' AND [TYPE] = @sType
			ORDER BY [TO_CODE]
		
		OPEN lsConversionCURSOR
			FETCH NEXT FROM lsConversionCURSOR 
				INTO @sTYPE, @sFROM_CODE, @sTO_CODE, @sTO_NAME, @fCONVERSION_RATE,@gidFROM_ID,@gidTO_ID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					
					--Get Base KPI_STANDARD_ID
					DECLARE lsKPIsStandardCURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY
						FOR 
							SELECT DISTINCT KPI_STD.[ID], KPI_STD.KPI_STANDARD_CODE
							FROM [B_KPI_STANDARD] KPI_STD
							INNER JOIN [B_KPI_STANDARD_CSTM] KPI_STD_CSTM ON (KPI_STD.ID = KPI_STD_CSTM.ID_C)
							WHERE 1=1
								AND KPI_STD.[YEAR] = @iYear
								AND KPI_STD.[APPROVE_STATUS] = @APPROVE_STATUS_DONE
								AND KPI_STD.[IS_BASE] = '1'
								AND KPI_STD.[POSITION_ID] = @gidFROM_ID
								AND KPI_STD.[ID] NOT IN (
									SELECT DISTINCT KPISTD.[ID]
									FROM [B_KPI_STANDARD] KPISTD
									INNER JOIN (
										
										SELECT 
											[YEAR],CONVERSIONRATE.FROM_CODE, KPIALLOCATE.M_BUSINESS_CODE_C
											,(SELECT TOP 1 [ID] FROM [M_POSITION] WHERE [POSITION_CODE] = CONVERSIONRATE.FROM_CODE) AS POSITION_ID			
										FROM M_KPI_CONVERSION_RATE CONVERSIONRATE
										INNER JOIN  (
											SELECT DISTINCT
												[YEAR]
												, alc.POSITION_ID
												, (SELECT TOP 1 [POSITION_CODE] FROM [M_POSITION] WHERE [ID] = alc.POSITION_ID ) AS [POSITION_CODE]
												, emp.M_BUSINESS_CODE_C
											FROM B_KPI_ALLOCATES alc
											INNER JOIN vwEMPLOYEES emp ON (alc.MA_NHAN_VIEN = emp.USER_CODE_C)			
											WHERE [YEAR] = @iYear
										) AS KPIALLOCATE ON ( (CONVERSIONRATE.TO_CODE = KPIALLOCATE.[POSITION_CODE]) OR (CONVERSIONRATE.FROM_CODE = KPIALLOCATE.[POSITION_CODE]) )
										
										UNION

										SELECT DISTINCT
												[YEAR]					
												, (SELECT TOP 1 [POSITION_CODE] FROM [M_POSITION] WHERE [ID] = alc.POSITION_ID ) AS [POSITION_CODE]					
												, emp.M_BUSINESS_CODE_C
												, alc.POSITION_ID
											FROM B_KPI_ALLOCATES alc
											INNER JOIN vwEMPLOYEES emp ON (alc.MA_NHAN_VIEN = emp.USER_CODE_C)			
											WHERE [YEAR] = @iYear

									) AS ALLOCATE ON KPISTD.[YEAR] = ALLOCATE.[YEAR] AND KPISTD.POSITION_ID = ALLOCATE.POSITION_ID AND KPISTD.BUSINESS_CODE = ALLOCATE.M_BUSINESS_CODE_C
									WHERE KPISTD.[YEAR] = @iYear AND KPISTD.IS_BASE = '1' AND KPISTD.[APPROVE_STATUS] = @APPROVE_STATUS_DONE									
								)
							ORDER BY KPI_STD.KPI_STANDARD_CODE ASC
							;
						
						--Duyet danh sach bo chi tieu co so
						OPEN lsKPIsStandardCURSOR
							FETCH NEXT FROM lsKPIsStandardCURSOR 
								INTO @B_KPI_STANDARD_ID_BASE, @KPI_STANDARD_CODE_BASE

							WHILE @@FETCH_STATUS = 0
								BEGIN
									--Kiem tra xem co bo KPIs co so chua? Neu chua co thi thoat
									IF @B_KPI_STANDARD_ID_BASE IS NULL 
									BEGIN
										--FETCH NEXT FROM lsConversionCURSOR 
										--INTO @sTYPE, @sFROM_CODE, @sTO_CODE, @sTO_NAME, @fCONVERSION_RATE,@gidFROM_ID,@gidTO_ID
										FETCH NEXT FROM lsKPIsStandardCURSOR 
											INTO @B_KPI_STANDARD_ID_BASE,  @KPI_STANDARD_CODE_BASE

										CONTINUE;
									END;

									--Xoa du lieu da sinh truoc do
									DELETE FROM B_KPI_STANDARD_DETAILS 
										WHERE [KPI_STANDARD_ID] IN (
											SELECT DISTINCT KPI_STD_DTL.[KPI_STANDARD_ID]
											FROM [B_KPI_STANDARD_DETAILS] KPI_STD_DTL
											INNER JOIN [B_KPI_STANDARD] KPI_STD ON (KPI_STD.ID = KPI_STD_DTL.[KPI_STANDARD_ID])
											INNER JOIN [B_KPI_STANDARD_CSTM] KPI_STD_CSTM ON (KPI_STD.ID = KPI_STD_CSTM.ID_C)
											WHERE 1=1
												AND [YEAR] = @iYear
												AND [POSITION_ID] = @gidTO_ID
												AND KPI_STD.KPI_STANDARD_CODE LIKE  @KPI_STANDARD_CODE_BASE + '%'
												--AND KPI_STD.[ID] LIKE  @B_KPI_STANDARD_ID_BASE
										);

									DELETE FROM B_KPI_STANDARD_CSTM
										WHERE ID_C IN (
											SELECT DISTINCT KPI_STD.[ID]
											FROM [B_KPI_STANDARD] KPI_STD
											INNER JOIN [B_KPI_STANDARD_CSTM] KPI_STD_CSTM ON (KPI_STD.ID = KPI_STD_CSTM.ID_C)
											WHERE 1=1
												AND [YEAR] = @iYear
												AND [POSITION_ID] = @gidTO_ID
												AND KPI_STD.KPI_STANDARD_CODE LIKE  @KPI_STANDARD_CODE_BASE + '%'
												--AND KPI_STD.[ID] LIKE  @B_KPI_STANDARD_ID_BASE
										);

									DELETE FROM B_KPI_STANDARD 
										WHERE ID IN (
											SELECT DISTINCT KPI_STD.[ID]
											FROM [B_KPI_STANDARD] KPI_STD
											WHERE 1=1
												AND [YEAR] = @iYear
												AND [POSITION_ID] = @gidTO_ID
												AND KPI_STD.KPI_STANDARD_CODE LIKE  @KPI_STANDARD_CODE_BASE + '%'
												--AND KPI_STD.[ID] LIKE  @B_KPI_STANDARD_ID_BASE
										);


									SET @B_KPI_STANDARD_ID = newid();

									--Sua code bo KPI toi thieu
									SET @iSeq = @iSeq + 1;

									--[B_KPI_STANDARD]
									INSERT INTO [B_KPI_STANDARD]
											([ID]
											,[DELETED]
											,[CREATED_BY]
											,[DATE_ENTERED]
											,[MODIFIED_USER_ID]
											,[DATE_MODIFIED]
											,[DATE_MODIFIED_UTC]
											,[ASSIGNED_USER_ID]
											,[TEAM_ID]
											,[TEAM_SET_ID]
											,[KPI_STANDARD_NAME]
											,[KPI_STANDARD_CODE]
											,[YEAR]
											,[VERSION_NUMBER]
											,[APPROVE_STATUS]
											,[APPROVED_BY]
											,[APPROVED_DATE]
											,[SENIORITY_ID]
											,[TYPE]
											,[POSITION_ID]
											,[BUSINESS_CODE]

											,[ORGANIZATION_ID]
											,[ORGANIZATION_CODE]

											,[CONTRACT_TYPE]
											,[IS_BASE]
											,[BASE_ID]
											,[DESCRIPTION]
											,[REMARK]
											,[STATUS]
											,[GROUP_KPI_ID])     
									SELECT @B_KPI_STANDARD_ID
											,[DELETED]
											,[CREATED_BY]
											,[DATE_ENTERED]
											,[MODIFIED_USER_ID]
											,[DATE_MODIFIED]
											,[DATE_MODIFIED_UTC]
											,[ASSIGNED_USER_ID]
											,[TEAM_ID]
											,[TEAM_SET_ID]
											,[KPI_STANDARD_NAME] + '-' + @sTO_NAME
											,[KPI_STANDARD_CODE] + '-'+ CONVERT(varchar(10),@iSeq)
											,[YEAR]
											,[VERSION_NUMBER]
											,[APPROVE_STATUS]
											,[APPROVED_BY]
											,[APPROVED_DATE]
											,[SENIORITY_ID]
											,[TYPE]
											,@gidTO_ID --[POSITION_ID]
											,[BUSINESS_CODE]

											,[ORGANIZATION_ID]
											,[ORGANIZATION_CODE]

											,[CONTRACT_TYPE]
											,[IS_BASE]
											,[BASE_ID]
											,[DESCRIPTION]
											,[REMARK]
											,[STATUS]
											,[GROUP_KPI_ID]
									FROM [B_KPI_STANDARD] KPI_STD
									INNER JOIN [B_KPI_STANDARD_CSTM]  KPI_STD_CSTM ON (KPI_STD.ID = KPI_STD_CSTM.ID_C)
									WHERE 1=1 				
										AND KPI_STD.[ID] = @B_KPI_STANDARD_ID_BASE;
					
									--[B_KPI_STANDARD_CSTM]
									INSERT INTO [B_KPI_STANDARD_CSTM]
										([ID_C],[AREA_ID_C])
										SELECT @B_KPI_STANDARD_ID,[AREA_ID_C]
										FROM [B_KPI_STANDARD_CSTM]
										WHERE [ID_C] = @B_KPI_STANDARD_ID_BASE;

									--[B_KPI_STANDARD_DETAILS]
									INSERT INTO [B_KPI_STANDARD_DETAILS]
										([ID]
										,[DELETED]
										,[CREATED_BY]
										,[DATE_ENTERED]
										,[MODIFIED_USER_ID]
										,[DATE_MODIFIED]
										,[DATE_MODIFIED_UTC]
										,[ASSIGNED_USER_ID]
										,[TEAM_ID]
										,[TEAM_SET_ID]
										,[KPI_STANDARD_ID]
										,[KPI_ID]
										,[KPI_CODE]
										,[KPI_NAME]
										,[KPI_UNIT]
										,[RATIO]
										,[LEVEL_NUMBER]
										,[MAX_RATIO_COMPLETE]
										,[GROUP_KPI_DETAIL_ID]
										,[DESCRIPTION]
										,[REMARK]
										,[VALUE_STD_PER_MONTH]
									)
									SELECT newid()
										,KPISTD.[DELETED]
										,KPISTD.[CREATED_BY]
										,KPISTD.[DATE_ENTERED]
										,KPISTD.[MODIFIED_USER_ID]
										,KPISTD.[DATE_MODIFIED]
										,KPISTD.[DATE_MODIFIED_UTC]
										,KPISTD.[ASSIGNED_USER_ID]
										,KPISTD.[TEAM_ID]
										,KPISTD.[TEAM_SET_ID]
										,@B_KPI_STANDARD_ID
										,KPISTD.[KPI_ID]
										,KPISTD.[KPI_CODE]
										,KPISTD.[KPI_NAME]
										,KPISTD.[KPI_UNIT]
										,KPISTD.[RATIO]
										,KPISTD.[LEVEL_NUMBER]
										,KPISTD.[MAX_RATIO_COMPLETE]
										,KPISTD.[GROUP_KPI_DETAIL_ID]
										,KPISTD.[DESCRIPTION]
										,KPISTD.[REMARK]
										, CASE WHEN MKPICSTM.[KPI_TYPE_C] = 'NX' THEN
												KPISTD.[VALUE_STD_PER_MONTH]
											ELSE
												--ROUND(@fCONVERSION_RATE * cast(KPISTD.[VALUE_STD_PER_MONTH] as decimal), 0) -- LAM TRON UP OR DOWN
												@fCONVERSION_RATE * cast(KPISTD.[VALUE_STD_PER_MONTH] as decimal(32,9))
											END
									FROM [B_KPI_STANDARD_DETAILS] KPISTD
									INNER JOIN M_KPIS MKPI ON (MKPI.[ID] = KPISTD.[KPI_ID])
									INNER JOIN M_KPIS_CSTM MKPICSTM ON (MKPI.[ID] = MKPICSTM.[ID_C])
									WHERE 1=1
										AND KPISTD.[KPI_STANDARD_ID] = @B_KPI_STANDARD_ID_BASE


									--Chuyen sang ban ghi tiep theo
									FETCH NEXT FROM lsKPIsStandardCURSOR 
									INTO @B_KPI_STANDARD_ID_BASE,  @KPI_STANDARD_CODE_BASE

								END --ket thuc danh sach KPIs co so

						CLOSE lsKPIsStandardCURSOR
						DEALLOCATE lsKPIsStandardCURSOR
						--Dong ket noi danh sach bo chi tieu co so

					--Cap nhat lan chay cuoi cung
					UPDATE M_KPI_CONVERSION_RATE 
						SET DATE_KPI_GENERATED = GETDATE()
						WHERE [TYPE]=@sTYPE
								AND [FROM_CODE]=@sFROM_CODE
								AND [TO_CODE]=@sTO_CODE

					--Duyet danh sach convert
					FETCH NEXT FROM lsConversionCURSOR 
					INTO @sTYPE, @sFROM_CODE, @sTO_CODE, @sTO_NAME, @fCONVERSION_RATE,@gidFROM_ID,@gidTO_ID

				END--end while

		CLOSE lsConversionCURSOR
		DEALLOCATE lsConversionCURSOR
    END
END






GO


