
GO

/****** Object:  StoredProcedure [dbo].[Gen_KPI_ACTUAL_RESULT]    Script Date: 12/12/2018 9:10:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Datdq
-- Description:	Khoi tao ket qua KPI de bat dau dong bo tu cac he thong khac
-- =============================================
ALTER PROCEDURE [dbo].[Gen_KPI_ACTUAL_RESULT]
@employeeID uniqueidentifier = '00000000-0000-0000-0000-000000000000'
WITH EXEC AS CALLER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @APPROVE_STATUS_DONE nvarchar(10) = '65'
	DECLARE @APPROVE_STATUS_DONTSENT nvarchar(10) = '00'

	DECLARE @iYear int = YEAR(getdate())
  -- Sua de chay tung thang hoac thang hien tai
	DECLARE @sMonth varchar(2) = CONVERT(char(2), getdate(), 101)
	--DECLARE @sMonth varchar(2) = '11'
	DECLARE @sMONTH_PERIOD varchar(8) = @sMonth -- + '.' + convert(nvarchar(4), @iYear)
	DECLARE @sACTUAL_RESULT_CODE varchar(50) = convert(nvarchar(4), @iYear) + @sMonth

	DECLARE @M_BUSINESS_CODE_C nvarchar(50) 
    DECLARE @POSITION_ID uniqueidentifier
    DECLARE @AREA_C nvarchar(50) 
    DECLARE @CONTRACT_TYPE_C nvarchar(50) 
    DECLARE @SENIORITY_C nvarchar(50) 
	DECLARE @ORGANIZATION_ID uniqueidentifier
	DECLARE @REPORT_TO_ID uniqueidentifier

	DECLARE @ALLOCATE_ID uniqueidentifier
	DECLARE @ALLOCATE_CODE nvarchar(50) 
	
	DECLARE @iNumberRec int
	
	IF @employeeID = '00000000-0000-0000-0000-000000000000'
		return;

	DECLARE @PERIOD nvarchar(50) 

	DECLARE @iCountActEmp int

	--Kiem tra ton tai ket qua phan giao cua bang cha chua?
	SET @iCountActEmp = (
		SELECT COUNT(*)
		FROM [B_KPI_ACTUAL_RESULT]
		WHERE 1=1
			AND [YEAR] = @iYear
			AND [MONTH_PERIOD] = @sMONTH_PERIOD
			AND [EMPLOYEE_ID] = @employeeID
	)

	PRINT @iCountActEmp
	 	
	SELECT @M_BUSINESS_CODE_C=M_BUSINESS_CODE_C,
		@POSITION_ID = POSITION_ID_C,
		@AREA_C=AREA_C, 
		@CONTRACT_TYPE_C=CONTRACT_TYPE_C,
		@SENIORITY_C=SENIORITY_C,
		@ORGANIZATION_ID = ORGANIZATION_ID_C
	FROm vwEMPLOYEES_Edit WHERE ID=@employeeID

	SELECT @PERIOD = [PERIOD], @ALLOCATE_ID = [ID], @ALLOCATE_CODE = ALLOCATE_CODE
	FROM [dbo].[vwB_KPI_ALLOCATES_List] ALC  		  
	WHERE 1=1 				
		AND ALC.[YEAR] = @iYear
		AND ALC.[APPROVE_STATUS] = @APPROVE_STATUS_DONE
		--AND ALC.[VERSION_NUMBER] = '0'		
		AND ALC.[EMPLOYEE_ID] = @employeeID
		AND ALC.M_BUSINESS_CODE_C = @M_BUSINESS_CODE_C
		AND ALC.POSITION_ID = @POSITION_ID 
		AND convert(nvarchar(50), ALC.AREA_ID) = @AREA_C
			AND ALC.CONTRACT_TYPE_C = @CONTRACT_TYPE_C
			AND ALC.SENIORITY_C = @SENIORITY_C
			AND ALC.ORGANIZATION_ID = @ORGANIZATION_ID
				
	
	DECLARE @CurrentPriod nvarchar(10)
	IF MONTH(getdate()) <=6 
		 SET @CurrentPriod = '00_06'
	IF MONTH(getdate()) >6 
		SET @CurrentPriod = '07_12'
		 
		PRINT @CurrentPriod

	--Neu chua co ket qua bang cha thi khoi tao
	IF @iCountActEmp = 0
		BEGIN
			INSERT INTO [dbo].[B_KPI_ACTUAL_RESULT]
				([ID]
				,[DELETED]
				,[CREATED_BY]
				,[DATE_ENTERED]
				,[MODIFIED_USER_ID]
				,[DATE_MODIFIED]
				,[DATE_MODIFIED_UTC]
				,[ASSIGNED_USER_ID]
				,[TEAM_ID]
				,[TEAM_SET_ID]
				,[NAME]
				,[ACTUAL_RESULT_CODE]
				,[YEAR]
				,[MONTH_PERIOD]
				,[EMPLOYEE_ID]
				,[MA_NHAN_VIEN]
				,[VERSION_NUMBER]
				,[ALLOCATE_CODE]
				,[ASSIGN_BY]
				,[ASSIGN_DATE]
				,[PERCENT_SYNC_TOTAL]
				,[PERCENT_FINAL_TOTAL]
				,[PERCENT_MANUAL_TOTAL]
				,[TOTAL_AMOUNT_01]
				,[TOTAL_AMOUNT_02]
				,[TOTAL_AMOUNT_03]
				,[DESCRIPTION]
				,[REMARK]
				,[FILE_ID]
				,[LATEST_SYNC_DATE]
				--,[APPROVE_ID]
				,[APPROVE_STATUS]
				--,[APPROVED_BY]
				--,[APPROVED_DATE]
				,[FLEX1]
				,[FLEX2]
				,[FLEX3]
				,[FLEX4]
				,[FLEX5])     
		SELECT newid()
			,0
			,ALC.[CREATED_BY_ID]
			,GETDATE()
			, ALC.[MODIFIED_USER_ID]
			,GETDATE()
			,ALC.[DATE_MODIFIED_UTC]
			,ALC.[ASSIGNED_USER_ID]
			,ALC.[TEAM_ID]
			,ALC.[TEAM_SET_ID]
			,ALC.[NAME]
			, @sACTUAL_RESULT_CODE + '_' + RIGHT(convert(nvarchar(36), EMPLOYEE_ID), 6) + '_ACT-KPI'     
			,[YEAR]
			,@sMONTH_PERIOD
			,EMPLOYEE_ID
			,MA_NHAN_VIEN 
			,VERSION_NUMBER
			,ALLOCATE_CODE
			,ASSIGN_BY
			,ASSIGN_DATE
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,''
			,ALC.[FILE_ID]
			,GETDATE()
			--,APPROVED_BY
			,@APPROVE_STATUS_DONTSENT
			--,APPROVED_BY
			--,APPROVED_DATE
			,ALC.[FLEX1]
			,ALC.[FLEX2]
			,ALC.[FLEX3]
			,ALC.[FLEX4]
			,ALC.[FLEX5]
		FROM [dbo].[vwB_KPI_ALLOCATES_List] ALC  		  
		WHERE 1=1 				
			AND ALC.[YEAR] = @iYear
			AND ALC.[APPROVE_STATUS] = @APPROVE_STATUS_DONE
			--AND ALC.[VERSION_NUMBER] = '0'		
			AND ALC.[EMPLOYEE_ID] = @employeeID
			AND ALC.M_BUSINESS_CODE_C = @M_BUSINESS_CODE_C
			AND ALC.POSITION_ID = @POSITION_ID 
			AND convert(nvarchar(50), ALC.AREA_ID) = @AREA_C
				AND ALC.CONTRACT_TYPE_C = @CONTRACT_TYPE_C
				AND ALC.SENIORITY_C = @SENIORITY_C
				AND ALC.ORGANIZATION_ID = @ORGANIZATION_ID
				AND (ALC.PERIOD = @CurrentPriod OR ALC.PERIOD = '00_12')
		END

	--Kiem tra ton tai ket qua phan giao cua bang cha chua?
	SET @iCountActEmp = (
		SELECT COUNT(*)
		FROM [B_KPI_ACT_RESULT_DETAIL]
		WHERE 1=1
			AND [YEAR] = @iYear
			AND [MONTH_PERIOD] = @sMONTH_PERIOD
			AND [EMPLOYEE_ID] = @employeeID
	)
	
	--Neu chua ton tai
	IF @iCountActEmp = 0
		BEGIN
			
			--Cap nhat lai Allocate code cho bang DETAIL, fix loi thong tin khong dong bo
			UPDATE B_KPI_ALLOCATE_DETAILS SET ALLOCATE_CODE = @ALLOCATE_CODE
			WHERE KPI_ALLOCATE_ID =@ALLOCATE_ID;

			--Khoi tao chi tiet ket qua phan giao
			EXEC [dbo].Gen_KPI_ACTUAL_RESULT_DETAIL @iYear, @sMONTH_PERIOD, @employeeID	
	
			--PRINT 'Tinh ket qua cua truong nhom'

			EXEC [dbo].CAL_TOTAL_FOR_REPORTER @employeeID
		END

END
GO


