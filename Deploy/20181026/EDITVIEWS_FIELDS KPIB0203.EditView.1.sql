update EDITVIEWS_FIELDS set DELETED = 1, DATE_MODIFIED_UTC = getutcdate(), MODIFIED_USER_ID = null where DELETED = 0 and EDIT_NAME = 'KPIB0203.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIB0203.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIB0203.EditView';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIB0203.EditView', 'KPIB0203', 'vwB_KPI_ALLOCATES_Edit', '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsHeader       'KPIB0203.EditView',  0, 'KPIB0203.LBL_DELIVERY_INFO_TITLE', 2;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView',  1, 'KPIB0203.LBL_ALLOCATE_CODE'      , 'ALLOCATE_CODE'    , null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIB0203.EditView',  2, 'KPIB0203.LBL_ALLOCATE_NAME'      , 'ALLOCATE_NAME'    , 1, 1, 200, 35, null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView',  3, 'KPIB0203.LBL_YEAR'               , 'YEAR'             , null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIB0203.EditView',  4, 'KPIB0203.LBL_PERIOD'             , 'PERIOD'           , 1, null, 'KPI_ALLOCATE_PERIOD_LIST', null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIB0203.EditView',  5, null;
	exec dbo.spEDITVIEWS_FIELDS_InsDatePick     'KPIB0203.EditView',  6, 'KPIB0203.LBL_ASSIGN_DATE'        , 'ASSIGN_DATE'      , 0, 1, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIB0203.EditView',  7, 'KPIB0203.LBL_DESCRIPTION'        , 'DESCRIPTION'      , 0, 1, 2, 90, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsSeparator    'KPIB0203.EditView',  8;
	exec dbo.spEDITVIEWS_FIELDS_InsHeader       'KPIB0203.EditView',  9, 'KPIB0203.LBL_EMPLOYEE_INFO_TITLE', 2;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 10, 'KPIB0203.LBL_EMPLOYEE_ID'        , 'MA_NHAN_VIEN'     , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 11, 'KPIB0203.LBL_EMPLOYEE_NAME'      , 'EMPLOYEE_NAME'    , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 12, 'KPIB0203.LBL_POSITION'           , 'POSITION'         , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 13, 'KPIB0203.TYPE_OF_LABOR'          , 'TYPE_OF_LABOR'    , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 14, 'KPIB0203.AREA'                   , 'AREA'             , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 15, 'KPIB0203.SENIORITY'              , 'SENIORITY'        , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 16, 'KPIB0203.DEPARTMENT_CODE'        , 'DEPARTMENT_CODE'  , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 17, 'KPIB0203.DEPARTMENT'             , 'DEPARTMENT'       , null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 18, 'KPIB0203.LBL_OFFICIAL_TIME'      , 'OFFICIAL_TIME'    , null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIB0203.EditView', 19, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 20, 'Hidden', null                              , 'EMPLOYEE_ID'      , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 21, 'Hidden', null                              , 'POSITION_ID'      , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 22, 'Hidden', null                              , 'AREA_ID'          , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 23, 'Hidden', null                              , 'ORGANIZATION_ID'  , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIB0203.EditView', 24, '.LBL_BUSINESS_CODE'              , 'BUSINESS_CODE'    , null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 25, 'Hidden', null                              , 'M_BUSINESS_CODE_C', null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 26, 'Hidden', null                              , 'SENIORITY_C'      , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 27, 'Hidden', null                              , 'CONTRACT_TYPE_C'  , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIB0203.EditView', 28, 'Hidden', null                              , 'KPI_STANDARD_ID'  , null, null                      , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIB0203.EditView', 29, null;
end -- if;
GO

