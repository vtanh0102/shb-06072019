update EDITVIEWS_FIELDS set DELETED = 1, DATE_MODIFIED_UTC = getutcdate(), MODIFIED_USER_ID = null where DELETED = 0 and EDIT_NAME = 'KPIM0101.EditView';
if not exists(select * from EDITVIEWS_FIELDS where EDIT_NAME = 'KPIM0101.EditView' and DELETED = 0) begin -- then
	print 'EDITVIEWS_FIELDS KPIM0101.EditView';
	exec dbo.spEDITVIEWS_InsertOnly             'KPIM0101.EditView', 'KPIM0101', 'vwM_GROUP_KPIS_Edit', '15%', '35%', null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.EditView',  0, 'KPIM0101.LBL_KPI_GROUP_CODE'  , 'KPI_GROUP_CODE'  , 0, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBound        'KPIM0101.EditView',  1, 'KPIM0101.LBL_KPI_GROUP_NAME'  , 'KPI_GROUP_NAME'  , 1, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsLabel        'KPIM0101.EditView',  2, 'KPIM0101.LBL_VERSION_NUMBER'  , 'VERSION_NUMBER'  , null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView',  3, 'KPIM0101.LBL_YEAR'            , 'YEAR'            , 1, 1, null                         , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView',  4, 'KPIM0101.LBL_APPROVE_STATUS'  , 'APPROVE_STATUS'  , 1, null, 'M_GROUP_KPIS_APPROVE_STATUS', null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView',  5, 'KPIM0101.LBL_STATUS'          , 'STATUS'          , 1, 1, 'M_GROUP_KPIS_STATUS'        , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsModulePopup  'KPIM0101.EditView',  6, 'KPIM0101.LBL_APPROVED_BY'     , 'APPROVED_BY'     , 0, null, 'ASSIGNED_TO', 'Users', null;
	exec dbo.spEDITVIEWS_FIELDS_InsDatePick     'KPIM0101.EditView',  7, 'KPIM0101.LBL_APPROVED_DATE'   , 'APPROVED_DATE'   , 0, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView',  8, 'KPIM0101.LBL_TYPE'            , 'TYPE'            , 0, null, 'M_GROUP_KPIS_TYPE'          , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView',  9, 'KPIM0101.LBL_BUSINESS_CODE'   , 'BUSINESS_CODE'   , 1, null, 'M_BUSINESS'                 , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView', 10, 'KPIM0101.LBL_ORGANIZATION_ID' , 'ORGANIZATION_ID' , 0, 1, 'M_ORGANIZATION'             , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView', 11, 'KPIM0101.LBL_LIST_POSITION_ID', 'POSITION_ID'     , 0, null, 'M_POSITION'                 , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView', 12, 'KPIB0201.LBL_CONTRACT_TYPE'   , 'CONTRACT_TYPE_C' , 1, null, 'CONTRACT_TYPE'              , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIM0101.EditView', 13, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBoundList    'KPIM0101.EditView', 14, 'KPIM0101.LBL_IS_COMMON'       , 'IS_COMMON'       , 1, null, 'yesno_dom'                  , null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIM0101.EditView', 15, null;
	exec dbo.spEDITVIEWS_FIELDS_Update null, null, 'KPIM0101.EditView', 16, 'Hidden', null                           , 'APPROVE_STATUS_H', null, null                         , 0, 0, null, null, null, null, null, null, null, -1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null;
	exec dbo.spEDITVIEWS_FIELDS_InsMultiLine    'KPIM0101.EditView', 17, 'KPIM0101.LBL_DESCRIPTION'     , 'DESCRIPTION'     , 0, 1, 1, 100, 3;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIM0101.EditView', 18, null;
	exec dbo.spEDITVIEWS_FIELDS_InsBlank        'KPIM0101.EditView', 19, null;
	exec dbo.spEDITVIEWS_FIELDS_UpdateDataFormat  null, 'KPIM0101.EditView', 'ORGANIZATION_ID', '{0}';
	exec dbo.spEDITVIEWS_FIELDS_UpdateDataFormat  null, 'KPIM0101.EditView', 'POSITION_ID', '{0}';
end -- if;
GO

