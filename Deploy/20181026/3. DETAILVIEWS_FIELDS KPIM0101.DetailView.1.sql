update DETAILVIEWS_FIELDS set DELETED = 1, DATE_MODIFIED_UTC = getutcdate(), MODIFIED_USER_ID = null where DELETED = 0 and DETAIL_NAME = 'KPIM0101.DetailView';
if not exists(select * from DETAILVIEWS_FIELDS where DETAIL_NAME = 'KPIM0101.DetailView' and DELETED = 0) begin -- then
	print 'DETAILVIEWS_FIELDS KPIM0101.DetailView';
	exec dbo.spDETAILVIEWS_InsertOnly          'KPIM0101.DetailView', 'KPIM0101', 'vwM_GROUP_KPIS_Edit', '15%', '35%', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound      'KPIM0101.DetailView',  0, 'KPIM0101.LBL_YEAR'            , 'YEAR'           , '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound      'KPIM0101.DetailView',  1, 'KPIM0101.LBL_VERSION_NUMBER'  , 'VERSION_NUMBER' , '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsModule     'KPIM0101.DetailView',  2, 'KPIM0101.LBL_CODE'            , 'KPI_GROUP_CODE' , '{0}', 'KPIM0101', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsModule     'KPIM0101.DetailView',  3, 'KPIM0101.LBL_NAME'            , 'KPI_GROUP_NAME' , '{0}', 'KPIM0101', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBoundList  'KPIM0101.DetailView',  4, 'KPIM0101.LBL_APPROVE_STATUS'  , 'APPROVE_STATUS' , '{0}', 'M_GROUP_KPIS_APPROVE_STATUS', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBoundList  'KPIM0101.DetailView',  5, 'KPIM0101.LBL_STATUS'          , 'STATUS'         , '{0}', 'M_GROUP_KPIS_STATUS'        , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound      'KPIM0101.DetailView',  6, 'KPIM0101.LBL_LIST_APPROVED_BY', 'APPROVED_BY'    , '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsModule     'KPIM0101.DetailView',  7, 'KPIM0101.LBL_APPROVED_DATE'   , 'APPROVED_DATE'  , null , 'KPIM0101', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBoundList  'KPIM0101.DetailView',  8, 'KPIM0101.LBL_TYPE'            , 'TYPE'           , '{0}', 'M_GROUP_KPIS_TYPE'          , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBoundList  'KPIM0101.DetailView',  9, '.LBL_BUSINESS_CODE'           , 'BUSINESS_CODE'  , '{0}', 'M_BUSINESS'                 , null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBound      'KPIM0101.DetailView', 10, 'KPIM0101.LBL_DESCRIPTION'     , 'DESCRIPTION'    , '{0}', null;
	exec dbo.spDETAILVIEWS_FIELDS_InsBoundList  'KPIM0101.DetailView', 11, 'KPIB0201.LBL_CONTRACT_TYPE'   , 'CONTRACT_TYPE_C', '{0}', 'CONTRACT_TYPE'              , null;
end -- if;
GO

