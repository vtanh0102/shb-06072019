if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'M_GROUP_KPIS_CSTM' and COLUMN_NAME = 'CONTRACT_TYPE_c') begin -- then
	print 'Adding M_GROUP_KPIS_CSTM.CONTRACT_TYPE_c';
	exec dbo.spFIELDS_META_DATA_Insert  null, null, 'CONTRACT_TYPE', 'KPIB0201.LBL_CONTRACT_TYPE', 'CONTRACT_TYPE', 'KPIM0101', 'varchar', 255, 0, 0, null, null, 0, false;
end -- if;
GO

