
GO

/****** Object:  StoredProcedure [dbo].[spM_GROUP_KPI_DETAILS_Update]    Script Date: 11/11/2018 2:25:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER Procedure [dbo].[spM_GROUP_KPI_DETAILS_Update]
	( @ID                                 uniqueidentifier output
	, @MODIFIED_USER_ID                   uniqueidentifier
	, @ASSIGNED_USER_ID                   uniqueidentifier
	, @TEAM_ID                            uniqueidentifier
	, @TEAM_SET_LIST                      varchar(8000)
	, @KPI_ID                             uniqueidentifier
	, @KPI_CODE                           nvarchar(50)
	, @KPI_NAME                           nvarchar(200)
	, @GROUP_KPI_ID                       nvarchar(50)
	, @UNIT                               int
	, @RATIO                              float
	, @MAX_RATIO_COMPLETE                 float
	, @DESCRIPTION                        nvarchar(max)
	, @REMARK                             nvarchar(max)

	, @TAG_SET_NAME                       nvarchar(4000)
	)
as
  begin
	set nocount on
	
	declare @TEAM_SET_ID         uniqueidentifier;
	exec dbo.spTEAM_SETS_NormalizeSet @TEAM_SET_ID out, @MODIFIED_USER_ID, @TEAM_ID, @TEAM_SET_LIST;

	if not exists(select * from M_GROUP_KPI_DETAILS where ID = @ID) begin -- then
		if dbo.fnIsEmptyGuid(@ID) = 1 begin -- then
			set @ID = newid();
		end -- if;
		insert into M_GROUP_KPI_DETAILS
			( ID                                 
			, CREATED_BY                         
			, DATE_ENTERED                       
			, MODIFIED_USER_ID                   
			, DATE_MODIFIED                      
			, DATE_MODIFIED_UTC                  
			, ASSIGNED_USER_ID                   
			, TEAM_ID                            
			, TEAM_SET_ID                        
			, KPI_ID                             
			, KPI_CODE 
			, KPI_NAME                           
			, GROUP_KPI_ID                       
			, UNIT                               
			, RATIO                              
			, MAX_RATIO_COMPLETE                 
			, DESCRIPTION                        
			, REMARK                             

			)
		values
			( @ID                                 
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			, @MODIFIED_USER_ID                   
			,  getdate()                          
			,  getutcdate()                       
			, @ASSIGNED_USER_ID                   
			, @TEAM_ID                            
			, @TEAM_SET_ID                        
			, @KPI_ID 
			, @KPI_CODE                             
			, @KPI_NAME                           
			, @GROUP_KPI_ID                       
			, @UNIT                               
			, @RATIO                              
			, @MAX_RATIO_COMPLETE                 
			, @DESCRIPTION                        
			, @REMARK                             

			);
	end else begin
		update M_GROUP_KPI_DETAILS
		   set MODIFIED_USER_ID                     = @MODIFIED_USER_ID                   
		     , DATE_MODIFIED                        =  getdate()                          
		     , DATE_MODIFIED_UTC                    =  getutcdate()                       
		     , ASSIGNED_USER_ID                     = @ASSIGNED_USER_ID                   
		     , TEAM_ID                              = @TEAM_ID                            
		     , TEAM_SET_ID                          = @TEAM_SET_ID                        
		     , KPI_ID                               = @KPI_ID
			 , KPI_CODE                               = @KPI_CODE                             
		     , KPI_NAME                             = @KPI_NAME                           
		     , GROUP_KPI_ID                         = @GROUP_KPI_ID                       
		     , UNIT                                 = @UNIT                               
		     , RATIO                                = @RATIO                              
		     , MAX_RATIO_COMPLETE                   = @MAX_RATIO_COMPLETE                 
		     , DESCRIPTION                          = @DESCRIPTION                        
		     , REMARK                               = @REMARK                             

		 where ID                                   = @ID                                 ;
		--exec dbo.spSUGARFAVORITES_UpdateName @MODIFIED_USER_ID, @ID, @NAME;
	end -- if;

	if @@ERROR = 0 begin -- then
		if not exists(select * from M_GROUP_KPI_DETAILS_CSTM where ID_C = @ID) begin -- then
			insert into M_GROUP_KPI_DETAILS_CSTM ( ID_C ) values ( @ID );
		end -- if;


	end -- if;
	if @@ERROR = 0 begin -- then
		exec dbo.spTAG_SETS_NormalizeSet @MODIFIED_USER_ID, @ID, N'KPID0101', @TAG_SET_NAME;
	end -- if;
	
	--wait for 1'
	WAITFOR DELAY '00:00:00.010'
  end
GO


