﻿using Sys.Core;
using Sys.Core.Enums;
using Sys.Core.Models;
using TempToCRM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Services;
using Sys.Core.Services;

namespace TempToCRM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start transfering");

            // Store Application Configucation
            string connectionString = ConfigurationManager.AppSettings["SQLServer"].ToString();
            string password = EncryptService.Decrypt(ConfigurationManager.AppSettings["SQLServerPassword"].ToString());
            Configuration.ConnectionString = connectionString;
            if (!string.IsNullOrWhiteSpace(password))
            {
                try
                {
                    Configuration.ConnectionString = string.Format(connectionString, password);
                }
                catch (Exception)
                {
                }
            }
            Configuration.UserAdminId = ConfigurationManager.AppSettings["user_id_admin"].ToString();
            Configuration.ConnectionCode = ConfigurationManager.AppSettings["ConnectionCode"].ToString();

            // Sync Temp data to CMR data
            SyncService syncService = new SyncService();
            //syncService.SyncHR();
            //syncService.SyncKPI();
            syncService.SyncTT();

            //if (System.Diagnostics.Debugger.IsAttached)
            //    Console.ReadLine();
        }
    }
}
