﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Source
{
    public class PositionTemp : BaseEntity
    {
        public int ID_SOURCE { set; get; }
        public string PositionCode { set; get; }
        public string PositionName { set; get; }
        public DateTime DateModified { set; get; }
    }
}
