﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Source
{
    public class RoleMapping : BaseEntity
    {
        public int ID { get; set; }
        public string TitleCode { get; set; }
        public string RoleName { get; set; }
    }
}
