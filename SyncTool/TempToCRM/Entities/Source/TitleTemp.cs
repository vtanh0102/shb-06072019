﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Source
{
    public class TitleTemp : BaseEntity
    {
        public int ID_Source { set; get; }
        public string TitleCode { set; get; }
        public string TitleName { set; get; }
        public int PosID { set; get; }
        public string DirectPost { set; get; }
        public string IndirectPost { set; get; }
        public string DirectBoss { set; get; }
        public string IndirectBoss { set; get; }
        public int PositionID { set; get; }
        public int IndirectBossID { set; get; }
        public DateTime DateModified { set; get; }
        public int ParentTitleID { set; get; }
    }
}
