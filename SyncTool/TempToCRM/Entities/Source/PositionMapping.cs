﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Source
{
    public class PositionMapping : BaseEntity
    {
        public int ID { get; set; }
        public string PositionName { get; set; }
        public string PositionCode { get; set; }
    }
}
