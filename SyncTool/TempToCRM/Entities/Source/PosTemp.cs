﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Source
{
    public class PosTemp : BaseEntity
    {
        public int IDSource { set; get; }
        /** MaPhongBanHachToan */
        public string PosCode { set; get; }
        /** MaChiNhanhHoachToan */
        public string PosMainCode { get; set; }
        public string PosName { set; get; }
        public string Description { set; get; }
        public int? ParentPosID { set; get; }
        public bool IsDelete { set; get; }
        public string Area { get; set; }
    }
}
