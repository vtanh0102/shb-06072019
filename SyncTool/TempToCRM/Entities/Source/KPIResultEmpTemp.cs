﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    public class KPIResultEmpTemp : BaseEntity
    {
        public string UserCode { get; set; }
        public string PosCode { get; set; }
        public int Year { get; set; }
        public string Period { get; set; }
        public string CusType { get; set; }
        public string MainCross { get; set; }
    }
}
