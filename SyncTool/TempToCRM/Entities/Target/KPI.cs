﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    public class KPI : BaseEntity
    {
        public string ID { get; set; }
        public string KPICode { get; set; }
        public string KPIName { get; set; }
        public string ParentID { get; set; }
        public int LevelNumber { get; set; }
        public int Unit { get; set; }
        public decimal Ratio { get; set; }
        public decimal MaxRatioComplete { get; set; }
        public decimal MinRatioComplete { get; set; }
        public decimal DefaultRatioComplete { get; set; }
        public string Status { get; set; }
    }
}