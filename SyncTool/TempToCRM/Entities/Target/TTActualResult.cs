﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("B_KPI_TOI_ACTUAL_RESULT")]
    public class TTActualResult : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("ACTUAL_RESULT_CODE")]
        public string ActualResultCode { get; set; }

        [Description("YEAR")]
        public int Year { get; set; }

        [Description("MONTH_PERIOD")]
        public string MonthPeriod { get; set; }

        [Description("EMPLOYEE_ID")]
        public string EmployeeID { get; set; }

        [Description("USER_CODE")]
        public string UserCode { get; set; }

        [Description("VERSION_NUMBER")]
        public string VersionNumber { get; set; }

        [Description("LATEST_SYNC_DATE")]
        public DateTime? LastestSyncDate { get; set; }

    }
}
