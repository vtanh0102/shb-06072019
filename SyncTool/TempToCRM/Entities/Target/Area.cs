﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    [Description("M_AREA")]
    public class Area : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("AREA_NAME")]
        public string AreaName { get; set; }

        [Description("AREA_CODE")]
        public string AreaCode { get; set; }
    }
}
