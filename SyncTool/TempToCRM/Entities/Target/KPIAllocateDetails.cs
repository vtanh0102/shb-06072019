﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    public class KPIAllocateDetails : BaseEntity
    {
        public string ID { get; set; }
        public string KPIAllocateID { get; set; }
        public string AllocateCode { get; set; }
        public string EmployeeID { get; set; }
        public string UserCode { get; set; }
        public string KPICode { get; set; }
        public int KPIUnit { get; set; }
        public int Unit { get; set; }
        public decimal Radio { get; set; }
        public decimal MaxRatioComplete { get; set; }
        public decimal TotalValue { get; set; }
        public decimal Month1 { get; set; }
        public decimal Month2 { get; set; }
        public decimal Month3 { get; set; }
        public decimal Month4 { get; set; }
        public decimal Month5 { get; set; }
        public decimal Month6 { get; set; }
        public decimal Month7 { get; set; }
        public decimal Month8 { get; set; }
        public decimal Month9 { get; set; }
        public decimal Month10 { get; set; }
        public decimal Month11 { get; set; }
        public decimal Month12 { get; set; }

        public KPI KPI { get; set; }
    }
}
