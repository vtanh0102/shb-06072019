﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("B_KPI_ACT_RESULT_DETAIL")]
    public class KPIActualResultDetails : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        //[Description("NAME")]
        //public string Name { get; set; }

        //[Description("ACTUAL_RESULT_CODE")]
        //public string ActualResultCode { get; set; }

        [Description("YEAR")]
        public int Year { get; set; }

        [Description("MONTH_PERIOD")]
        public string MonthPeriod { get; set; }

        //[Description("EMPLOYEE_ID")]
        //public string EmployeeID { get; set; }

        [Description("MA_NHAN_VIEN")]
        public string UserCode { get; set; }

        //[Description("VERSION_NUMBER")]
        //public string VersionNumber { get; set; }

        //[Description("KPI_ID")]
        //public string KPIID { get; set; }

        [Description("KPI_CODE")]
        public string KPICode { get; set; }

        //[Description("KPI_NAME")]
        //public string KPIName { get; set; }

        //[Description("KPI_UNIT")]
        //public int KPIUnit { get; set; }

        //[Description("LEVEL_NUMBER")]
        //public int LevelNumber { get; set; }

        //[Description("RATIO")]
        //public decimal Ratio { get; set; }

        //[Description("PLAN_VALUE")]
        //public decimal PlanValue { get; set; }

        [Description("SYNC_VALUE")]
        public decimal SyncValue { get; set; }

        [Description("FINAL_VALUE")]
        public decimal FinalValue { get; set; }

        //[Description("PERCENT_SYNC_VALUE")]
        //public decimal? PercentSyncValue { get; set; }

        //[Description("PERCENT_FINAL_VALUE")]
        //public decimal? PercentFinalValue { get; set; }

        //[Description("PERCENT_MANUAL_VALUE")]
        //public decimal? PercentManualValue { get; set; }

        [Description("LATEST_SYNC_DATE")]
        public DateTime LastestSyncDate { get; set; }
    }
}
