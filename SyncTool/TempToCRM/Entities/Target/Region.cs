﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    public class Region : BaseEntity
    {
        public string ID { get; set; }

        public string RegionName { get; set; }
    }
}
