﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    [Description("ACL_ROLES_USERS")]
    public class RolesUsers : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("Deleted")]
        public bool Deleted { get; set; }

        [Description("ROLE_ID")]
        public string RoleID { get; set; }

        [Description("USER_ID")]
        public string UserID { get; set; }
    }
}
