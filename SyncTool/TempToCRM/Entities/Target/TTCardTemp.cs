﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("M_TOI_CARD_TMP")]
    public class TTCardTemp : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("USER_CODE")]
        public string UserCode { get; set; }

        [Description("POS_CODE")]
        public string PosCode { get; set; }

        [Description("YEAR")]
        public int Year { get; set; }

        [Description("PERIOD")]
        public string Period { get; set; }

        [Description("KPI_CODE")]
        public string KPICode { get; set; }

        [Description("VALUE")]
        public decimal? Value { get; set; }
    }
}
