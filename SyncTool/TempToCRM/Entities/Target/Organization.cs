﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("M_ORGANIZATION")]
    public class Organization : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("ID_SOURCE")]
        public int? IDSource { get; set; }

        [Description("ORGANIZATION_CODE")]
        public string OrganizationCode { get; set; }

        [Description("ORGANIZATION_NAME")]
        public string OrganizationName { get; set; }

        [Description("LEVEL_NUMBER")]
        public int LevelNumber { get; set; }

        [Description("DESCRIPTION")]
        public string Description { get; set; }

        [Description("STATUS")]
        public string Status { get; set; }

        [Description("PARENT_ID")]
        public string ParentID { get; set; }

        //[Description("AREA_ID")]
        public string AreaID { get; set; }

        //[Description("REGION_ID")]
        public string Region { get; set; }

        [Description("DELETED")]
        public bool Deleted { get; set; }
    }
}
