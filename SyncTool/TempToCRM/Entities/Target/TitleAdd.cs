﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    [Description("B_TITLE_ADD")]
    public class TitleAdd : BaseEntity
    {
        [Description("ID")]
        public string ID { set; get; }

        [Description("ID_SOURCE")]
        public int IDSource { set; get; }

        [Description("ID_SOURCE_EMP")]
        public int? IDSourceEmp { set; get; }

        [Description("SoQuyetDinhID")]
        public int? SoQuyetDinhID { set; get; }

        [Description("NgayBatDau")]
        public DateTime? NgayBatDau { set; get; }

        [Description("NgayKetThuc")]
        public DateTime? NgayKetThuc { set; get; }

        [Description("ChucVuKiemNhiemID")]
        public int? ChucVuKiemNhiemID { set; get; }

        [Description("GhiChu")]
        public string GhiChu { set; get; }

        [Description("PhongBanKiemNhiemID")]
        public int? PhongBanKiemNhiemID { set; get; }

        [Description("SoQuyetDinh")]
        public string SoQuyetDinh { set; get; }

        [Description("TenChucVuKiemNhiem")]
        public string TenChucVuKiemNhiem { set; get; }

        [Description("CreatedByID")]
        public int? CreatedByID { set; get; }

        [Description("CreatedDate")]
        public DateTime? CreatedDate { set; get; }

        [Description("ModifyByID")]
        public int? ModifyByID { set; get; }

        [Description("ModifyDate")]
        public DateTime? ModifyDate { set; get; }

        [Description("XetDuyet")]
        public int? XetDuyet { set; get; }

        [Description("NgayQuyetDinh")]
        public DateTime? NgayQuyetDinh { set; get; }

    }
}
