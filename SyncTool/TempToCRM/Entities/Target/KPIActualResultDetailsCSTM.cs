﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("B_KPI_ACT_RESULT_DETAIL_CSTM")]
    public class KPIActualResultDetailsCSTM : BaseEntity
    {
        [Description("ID_C")]
        public string ID { get; set; }

        [Description("POS_CODE_C")]
        public string PosCode { get; set; }

    }
}
