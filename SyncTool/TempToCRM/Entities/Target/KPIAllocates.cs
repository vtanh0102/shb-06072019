﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    public class KPIAllocates : BaseEntity
    {
        public string ID { get; set; }
        public string AllocateCode { get; set; }
        public string Allocatename { get; set; }
        public int Year { get; set; }
        public string Period { get; set; }
        public string EmployeeID { get; set; }
        public string UserCode { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationCode { get; set; }
    }
}
