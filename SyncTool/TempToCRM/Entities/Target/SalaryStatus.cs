﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("B_SALARY_STATUS")]
    public class SalaryStatus : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("ID_SOURCE")]
        public int IDSource { get; set; }

        [Description("NAME")]
        public string Name { get; set; }
    }
}