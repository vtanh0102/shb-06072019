﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("B_KPI_ACTUAL_RESULT")]
    public class KPIActualResult : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("NAME")]
        public string Name { get; set; }

        [Description("ACTUAL_RESULT_CODE")]
        public string ActualResultCode { get; set; }

        [Description("YEAR")]
        public int Year { get; set; }

        [Description("MONTH_PERIOD")]
        public string MonthPeriod { get; set; }

        [Description("EMPLOYEE_ID")]
        public string EmployeeID { get; set; }

        [Description("MA_NHAN_VIEN")]
        public string UserCode { get; set; }

        [Description("VERSION_NUMBER")]
        public string VersionNumber { get; set; }

        [Description("ALLOCATE_CODE")]
        public string AllocateCode { get; set; }

        [Description("ASSIGN_BY")]
        public string AssignBy { get; set; }

        [Description("PERCENT_SYNC_TOTAL")]
        public decimal PercentSyncTotal { get; set; }

        [Description("PERCENT_FINAL_TOTAL")]
        public decimal PercentFinalTotal { get; set; }

        [Description("PERCENT_MANUAL_TOTAL")]
        public decimal? PercentManualTotal { get; set; }
    }
}
