﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    [Description("M_POSITION")]
    public class Position : BaseEntity
    {
        [Description("ID")]
        public string ID { set; get; }

        [Description("ID_SOURCE")]
        public int IDSource { set; get; }

        [Description("POSITION_CODE")]
        public string PositionCode { set; get; }

        [Description("POSITION_NAME")]
        public string PositionName { set; get; }

        //[Description("DATE_MODIFIED")]
        //public DateTime? DateModified { set; get; }
    }
}
