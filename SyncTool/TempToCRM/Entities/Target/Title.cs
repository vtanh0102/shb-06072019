﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities.Target
{
    [Description("M_TITLE")]
    public class Title : BaseEntity
    {
        [Description("ID")]
        public string ID { set; get; }

        [Description("ID_SOURCE")]
        public int IDSource { set; get; }

        [Description("TITLE_CODE")]
        public string TitleCode { set; get; }

        [Description("TITLE_NAME")]
        public string TitleName { set; get; }

        [Description("POS_ID")]
        public int? PosID { set; get; }

        [Description("DIRECT_REPORT")]
        public string DirectPost { set; get; }

        [Description("INDIRECT_REPORT")]
        public string IndirectPost { set; get; }

        [Description("DIRECT_BOSS")]
        public string DirectBoss { set; get; }

        [Description("INDIRECT_BOSS")]
        public string IndirectBoss { set; get; }

        [Description("POSITION_ID")]
        public int? PositionID { set; get; }

        [Description("INDIRECT_BOSS_ID")]
        public int? IndirectBossID { set; get; }

        [Description("DATE_MODIFIED")]
        public DateTime? DateModified { set; get; }

        [Description("PARENT_TITLE_ID")]
        public int? ParentTitleID { set; get; }
    }
}
