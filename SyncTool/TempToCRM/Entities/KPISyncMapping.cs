﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    public class KPISyncMapping : BaseEntity
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public int Year { get; set; }
        public string MonthPeriod { get; set; }
        public string SyncColumn { get; set; }
        public string KPICode { get; set; }
        public string Description { get; set; }
    }
}
