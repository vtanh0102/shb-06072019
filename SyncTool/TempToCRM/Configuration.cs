﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM
{
    public static class Configuration
    {
        public static string ConnectionString { get; set; }
        public static string ConnectionCode { get; set; }
        public static string UserAdminId { get; set; }
    }
}
