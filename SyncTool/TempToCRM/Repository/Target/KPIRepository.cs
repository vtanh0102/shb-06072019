﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM M_KPIS";
        private const string QUERY_BY_KEY = @"SELECT * FROM M_KPIS 
                                            WHERE KPI_CODE = '{0}'";

        public KPIRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<KPI> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            List<KPI> listKPI = new List<KPI>();
            foreach (DataRow row in dataTable.Rows)
            {
                KPI kpi = convertFromDataRow(row);
                listKPI.Add(kpi);
            }
            return listKPI;
        }

        public KPI GetByKey(string kpiCode)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_KEY, kpiCode);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            KPI KPI = convertFromDataRow(dataTable.Rows[0]);
            return KPI;
        }

        private KPI convertFromDataRow(DataRow dataRow)
        {
            KPI result = new KPI()
            {
                ID = dataRow["ID"].ToString(),
                KPICode = dataRow["KPI_CODE"].ToString(),
                KPIName = dataRow["KPI_NAME"].ToString(),
                ParentID = dataRow["PARENT_ID"].ToString(),
                LevelNumber = Convert.ToInt32(dataRow["LEVEL_NUMBER"]),
                Unit = Convert.ToInt32(dataRow["UNIT"]),
                Ratio = Convert.ToDecimal(dataRow["RATIO"]),
                MaxRatioComplete = Convert.ToDecimal(dataRow["MAX_RATIO_COMPLETE"]),
                MinRatioComplete = Convert.ToDecimal(dataRow["MIN_RATIO_COMPLETE"]),
                DefaultRatioComplete = Convert.ToDecimal(dataRow["DEFAULT_RATIO_COMPLETE"]),
                Status = dataRow["STATUS"].ToString(),
            };
            return result;
        }
    }
}
