﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;
using Sys.Core.Repository;

namespace TempToCRM.Repository
{
    public class SalaryStatusRepository : BaseRepository<SalaryStatus>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_SALARY_STATUS";

        public SalaryStatusRepository(string connectionString)
            : base(connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<SalaryStatus> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return new List<SalaryStatus>();

            List<SalaryStatus> listSalaryStatus = new List<SalaryStatus>();
            foreach (DataRow row in dataTable.Rows)
            {
                SalaryStatus salaryStatus = convertFromDataRow(row);
                listSalaryStatus.Add(salaryStatus);
            }
            return listSalaryStatus;
        }

        private SalaryStatus convertFromDataRow(DataRow dataRow)
        {
            SalaryStatus result = new SalaryStatus()
            {
                ID = dataRow["ID"].ToString(),
                IDSource = Convert.ToInt32(dataRow["ID_SOURCE"]),
                Name = dataRow["NAME"].ToString(),
            };
            return result;
        }
    }
}
