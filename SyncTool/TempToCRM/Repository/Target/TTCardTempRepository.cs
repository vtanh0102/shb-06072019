﻿using Sys.Core.DBHelper;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Extensions;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class TTCardTempRepository : BaseRepository<TTCardTemp>
    {
        private const string QUERY_ALL = @"SELECT * FROM M_TOI_CARD_TMP";

        public TTCardTempRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<TTCardTemp> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<TTCardTemp> result = new List<TTCardTemp>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private TTCardTemp convertFromDataRow(DataRow dataRow)
        {
            TTCardTemp result = new TTCardTemp();
            result.ID = dataRow["ID"].ToString();
            result.UserCode = dataRow["USER_CODE"].ToString();
            result.PosCode = dataRow["POS_CODE"].ToString();
            result.Year = Convert.ToInt32(dataRow["YEAR"]);
            result.Period = dataRow["PERIOD"].ToString();
            result.KPICode = dataRow["KPI_CODE"].ToString();
            result.Value = NumberUltility.ToNullableDecimal(dataRow["VALUE"]);
            return result;
        }

    }
}
