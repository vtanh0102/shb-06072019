﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;
using TempToCRM.Entities.Target;

namespace TempToCRM.Repository
{
    public class ACLRoleRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM ACL_ROLES WHERE DELETED = 0";

        public ACLRoleRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<ACLRole> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<ACLRole> listACLRole = new List<ACLRole>();
            foreach (DataRow row in dataTable.Rows)
                listACLRole.Add(convertFromDataRow(row));
            return listACLRole;
        }

        private ACLRole convertFromDataRow(DataRow dataRow)
        {
            ACLRole result = new ACLRole()
            {
                ID = dataRow["ID"].ToString(),
                Name = dataRow["NAME"].ToString(),
            };
            return result;
        }
    }
}
