﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities.Target;

namespace TempToCRM.Repository.Target
{
    public class PositionRepository : BaseRepository<Position>
    {

        private const string QUERY_ALL = @"SELECT * FROM [M_POSITION]";

        public PositionRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<Position> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<Position> result = new List<Position>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private Position convertFromDataRow(DataRow dataRow)
        {
            Position result = new Position()
            {
                ID = dataRow["ID"].ToString(),
                IDSource = dataRow.IsNull("ID_SOURCE") ? 0 : Convert.ToInt32(dataRow["ID_SOURCE"]),
                PositionCode = dataRow["POSITION_CODE"].ToString(),
                PositionName = dataRow["POSITION_NAME"].ToString(),
            };
            return result;
        }
    }
}
