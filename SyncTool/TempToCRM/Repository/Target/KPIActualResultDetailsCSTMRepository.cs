﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Utility;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIActualResultDetailsCSTMRepository : BaseRepository<KPIActualResultDetailsCSTM>
    {
        private const string QUERY_ALL = "SELECT * FROM B_KPI_ACT_RESULT_DETAIL_CSTM";

        public KPIActualResultDetailsCSTMRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<KPIActualResultDetailsCSTM> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<KPIActualResultDetailsCSTM> result = new List<KPIActualResultDetailsCSTM>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private KPIActualResultDetailsCSTM convertFromDataRow(DataRow dataRow)
        {
            KPIActualResultDetailsCSTM result = new KPIActualResultDetailsCSTM();
            result.ID = dataRow["ID_C"].ToString();
            result.PosCode = dataRow["POS_CODE"].ToString();
            return result;
        }

    }
}
