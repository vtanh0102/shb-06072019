﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;

namespace TempToCRM.Repository.Target
{
    public class TitleAddRepository : BaseRepository<TitleAdd>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_TITLE_ADD";
        private const string QUERY_BY_ID = @"SELECT * FROM B_TITLE_ADD WHERE ID = {0}";

        public TitleAddRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<TitleAdd> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<TitleAdd> result = new List<TitleAdd>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public TitleAdd GetByID(string ID)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_ID, ID);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            TitleAdd result = convertFromDataRow(dataTable.Rows[0]);
            return result;
        }

        private TitleAdd convertFromDataRow(DataRow row)
        {
            TitleAdd result = new TitleAdd();
            result.ID = row["ID"].ToString();
            result.IDSource = Convert.ToInt32(row["ID_SOURCE"]);
            result.IDSourceEmp = NumberUltility.ToNullable(row["ID_SOURCE_EMP"]);
            result.SoQuyetDinhID = NumberUltility.ToNullable(row["SoQuyetDinhID"]);
            result.NgayBatDau = DateTimeUltility.ToNullable(row["NgayBatDau"]);
            result.NgayKetThuc = DateTimeUltility.ToNullable(row["NgayKetThuc"]);
            result.ChucVuKiemNhiemID = NumberUltility.ToNullable(row["ChucVuKiemNhiemID"]);
            result.GhiChu = row["GhiChu"].ToString();
            result.PhongBanKiemNhiemID = NumberUltility.ToNullable(row["PhongBanKiemNhiemID"]);
            result.SoQuyetDinh = row["SoQuyetDinh"].ToString();
            result.TenChucVuKiemNhiem = row["TenChucVuKiemNhiem"].ToString();
            result.CreatedByID = NumberUltility.ToNullable(row["CreatedByID"]);
            result.CreatedDate = DateTimeUltility.ToNullable(row["CreatedDate"]);
            result.ModifyByID = NumberUltility.ToNullable(row["ModifyByID"]);
            result.ModifyDate = DateTimeUltility.ToNullable(row["ModifyDate"]);
            result.XetDuyet = NumberUltility.ToNullable(row["XetDuyet"]);
            result.NgayQuyetDinh = DateTimeUltility.ToNullable(row["NgayQuyetDinh"]);
            return result;
        }
    }
}
