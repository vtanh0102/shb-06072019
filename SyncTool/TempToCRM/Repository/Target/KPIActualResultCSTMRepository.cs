﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Extensions;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIActualResultCSTMRepository : BaseRepository<KPIActualResultCSTM>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_KPI_ACT_RESULT_CSTM";

        public KPIActualResultCSTMRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<KPIActualResultCSTM> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<KPIActualResultCSTM> result = new List<KPIActualResultCSTM>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private KPIActualResultCSTM convertFromDataRow(DataRow dataRow)
        {
            KPIActualResultCSTM result = new KPIActualResultCSTM();
            result.ID = dataRow["ID_C"].ToString();
            result.PosCode = dataRow["POS_CODE_C"].ToString();
            return result;
        }

    }
}
