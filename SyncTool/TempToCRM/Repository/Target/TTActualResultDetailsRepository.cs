﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Utility;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class TTActualResultDetailsRepository : BaseRepository<TTActualResultDetails>
    {
        private const string QUERY_ALL = "SELECT * FROM B_KPI_TOI_ACT_RESULT_DETAIL";
        private const string QUERY_BY_KEY = @"SELECT * FROM B_KPI_TOI_ACT_RESULT_DETAIL 
                                            WHERE YEAR = '{0}' AND MONTH_PERIOD = '{1}'
                                            AND MA_NHAN_VIEN = '{2}' AND KPI_CODE = '{3}'";

        public TTActualResultDetailsRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<TTActualResultDetails> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<TTActualResultDetails> result = new List<TTActualResultDetails>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private TTActualResultDetails convertFromDataRow(DataRow dataRow)
        {
            TTActualResultDetails result = new TTActualResultDetails();
            result.ID = dataRow["ID"].ToString();
            result.ActualResultCode = dataRow["ACTUAL_RESULT_CODE"].ToString();
            result.Year = Convert.ToInt32(dataRow["YEAR"]);
            result.MonthPeriod = dataRow["MONTH_PERIOD"].ToString();
            result.EmployeeID = dataRow["EMPLOYEE_ID"].ToString();
            result.UserCode = dataRow["USER_CODE"].ToString();
            result.VersionNumber = dataRow["VERSION_NUMBER"].ToString();
            result.KPIID = dataRow["KPI_ID"].ToString();
            result.KPICode = dataRow["KPI_CODE"].ToString();
            result.KPIName = dataRow["KPI_NAME"].ToString();
            result.KPIUnit = NumberUltility.ToNullable(dataRow["KPI_UNIT"]);
            result.LevelNumber = NumberUltility.ToNullable(dataRow["LEVEL_NUMBER"]);
            result.PlanValue = NumberUltility.ToNullableDecimal(dataRow["PLAN_VALUE"]);
            result.SyncValue = NumberUltility.ToNullableDecimal(dataRow["SYNC_VALUE"]);
            result.FinalValue = NumberUltility.ToNullableDecimal(dataRow["FINAL_VALUE"]);
            result.LastestSyncDate = DateTimeUltility.ToNullable(dataRow["LATEST_SYNC_DATE"]);
            return result;
        }

    }
}
