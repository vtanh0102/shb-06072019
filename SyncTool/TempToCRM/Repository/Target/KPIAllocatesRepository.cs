﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIAllocatesRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL_DETAILS = @"SELECT * FROM B_KPI_ALLOCATE_DETAILS";

        public KPIAllocatesRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<KPIAllocateDetails> GetAllDetails()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL_DETAILS, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            List<KPIAllocateDetails> listKPI = new List<KPIAllocateDetails>();
            foreach(DataRow row in dataTable.Rows)
                listKPI.Add(convertFromDataRow(row));
            return listKPI;
        }

        private KPIAllocateDetails convertFromDataRow(DataRow dataRow)
        {
            KPIAllocateDetails result = new KPIAllocateDetails()
            {
                ID = dataRow["ID"].ToString(),
                KPIAllocateID = dataRow["KPI_ALLOCATE_ID"].ToString(),
                AllocateCode = dataRow["ALLOCATE_CODE"].ToString(),
                EmployeeID = dataRow["EMPLOYEE_ID"].ToString(),
                UserCode = dataRow["MA_NHAN_VIEN"].ToString(),
                KPICode = dataRow["KPI_CODE"].ToString(),
                KPIUnit = Convert.ToInt32(dataRow["KPI_UNIT"]),
                Unit = Convert.ToInt32(dataRow["UNIT"]),
                Radio = Convert.ToDecimal(dataRow["RADIO"]),
                MaxRatioComplete = Convert.ToDecimal(dataRow["MAX_RATIO_COMPLETE"]),
                TotalValue = Convert.ToDecimal(dataRow["TOTAL_VALUE"]),
                Month1 = Convert.ToDecimal(dataRow["MONTH_1"]),
                Month2 = Convert.ToDecimal(dataRow["MONTH_2"]),
                Month3 = Convert.ToDecimal(dataRow["MONTH_3"]),
                Month4 = Convert.ToDecimal(dataRow["MONTH_4"]),
                Month5 = Convert.ToDecimal(dataRow["MONTH_5"]),
                Month6 = Convert.ToDecimal(dataRow["MONTH_6"]),
                Month7 = Convert.ToDecimal(dataRow["MONTH_7"]),
                Month8 = Convert.ToDecimal(dataRow["MONTH_8"]),
                Month9 = Convert.ToDecimal(dataRow["MONTH_9"]),
                Month10 = Convert.ToDecimal(dataRow["MONTH_10"]),
                Month11 = Convert.ToDecimal(dataRow["MONTH_11"]),
                Month12 = Convert.ToDecimal(dataRow["MONTH_12"]),
            };
            return result;
        }
    }
}
