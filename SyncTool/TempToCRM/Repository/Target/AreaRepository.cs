﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities.Target;

namespace TempToCRM.Repository
{
    public class AreaRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM M_AREA";

        public AreaRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Area> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            List<Area> listArea = new List<Area>();
            foreach (DataRow row in dataTable.Rows)
            {
                Area Area = convertFromDataRow(row);
                listArea.Add(Area);
            }
            return listArea;
        }

        private Area convertFromDataRow(DataRow dataRow)
        {
            Area result = new Area()
            {
                ID = dataRow["ID"].ToString(),
                AreaName = dataRow["AREA_NAME"].ToString(),
                AreaCode = dataRow["AREA_CODE"].ToString(),
            };
            return result;
        }
    }
}
