﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;

namespace TempToCRM.Repository.Target
{
    public class TitleRepository : BaseRepository<Title>
    {
        private const string QUERY_ALL = @"SELECT * FROM [M_TITLE]";
        private const string QUERY_BY_ID = @"SELECT * FROM [M_TITLE] WHERE ID = {0}";

        public TitleRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<Title> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<Title> result = new List<Title>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public Title GetByID(string ID)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_ID, ID);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            Title result = convertFromDataRow(dataTable.Rows[0]);
            return result;

        }

        private Title convertFromDataRow(DataRow row)
        {
            Title result = new Title();
            result.ID = row["ID"].ToString();
            result.IDSource = Convert.ToInt32(row["ID_SOURCE"]);
            result.TitleCode = row["TITLE_CODE"].ToString();
            result.TitleName = row["TITLE_NAME"].ToString();
            result.PosID = NumberUltility.ToNullable(row["POS_ID"]);
            result.DirectPost = row["DIRECT_REPORT"].ToString();
            result.IndirectPost = row["INDIRECT_REPORT"].ToString();
            result.DirectBoss = row["DIRECT_BOSS"].ToString();
            result.IndirectBoss = row["INDIRECT_BOSS"].ToString();
            result.IndirectBossID = NumberUltility.ToNullable(row["INDIRECT_BOSS_ID"]);
            result.PositionID = NumberUltility.ToNullable(row["POSITION_ID"]);
            result.DateModified = DateTimeUltility.ToNullable(row["DATE_MODIFIED"]);
            result.ParentTitleID = NumberUltility.ToNullable(row["PARENT_TITLE_ID"]);
            return result;
        }
    }
}
