﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Extensions;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIActualResultRepository : BaseRepository<KPIActualResult>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_KPI_ACTUAL_RESULT";
        private const string QUERY_BY_KEY = @"SELECT * FROM B_KPI_ACTUAL_RESULT 
                                            WHERE YEAR = '{0}' AND MONTH_PERIOD = '{1}'
                                            AND MA_NHAN_VIEN = '{2}' AND KPI_CODE = '{3}'";

        public KPIActualResultRepository(string connectionString) : base(connectionString)
        {
        }

        public List<KPIActualResult> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<KPIActualResult> result = new List<KPIActualResult>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public KPIActualResult GetByKey(string year, string month, string maNhanVien, string kpiCode)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_KEY, year, month, maNhanVien, kpiCode);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            KPIActualResult KPI = convertFromDataRow(dataTable.Rows[0]);
            return KPI;
        }

        private KPIActualResult convertFromDataRow(DataRow dataRow)
        {
            KPIActualResult result = new KPIActualResult();
            result.ID = dataRow["ID"].ToString();
            result.Name = dataRow["NAME"].ToString();
            result.ActualResultCode = dataRow["ACTUAL_RESULT_CODE"].ToString();
            result.Year = Convert.ToInt32(dataRow["YEAR"]);
            result.MonthPeriod = dataRow["MONTH_PERIOD"].ToString();
            result.EmployeeID = dataRow["EMPLOYEE_ID"].ToString();
            result.UserCode = dataRow["MA_NHAN_VIEN"].ToString();
            result.VersionNumber = dataRow["VERSION_NUMBER"].ToString();
            result.AllocateCode = dataRow["ALLOCATE_CODE"].ToString();
            result.AssignBy = dataRow["ASSIGN_BY"].ToString();
            result.PercentSyncTotal = dataRow.IsNull("PERCENT_SYNC_TOTAL") ? 0 : Convert.ToDecimal(dataRow["PERCENT_SYNC_TOTAL"]);
            result.PercentFinalTotal = dataRow.IsNull("PERCENT_FINAL_TOTAL") ? 0 : Convert.ToDecimal(dataRow["PERCENT_FINAL_TOTAL"]);
            result.PercentManualTotal = dataRow.IsNull("PERCENT_MANUAL_TOTAL") ? 0 : Convert.ToDecimal(dataRow["PERCENT_MANUAL_TOTAL"]);
            return result;
        }

    }
}
