﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPISyncMappingRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM M_KPI_TOI_SYNC_MAPPING";
        private const string QUERY_BY_DATE = @"SELECT * FROM M_KPI_TOI_SYNC_MAPPING 
                                               WHERE YEAR = {0} AND TYPE LIKE '{2}'
                                               AND CAST(SUBSTRING(MONTH_PERIOD, 1, 2) as int) <= {1}
                                               AND CAST(SUBSTRING(MONTH_PERIOD, 4, 2) as int) >= {1}";

        public KPISyncMappingRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<KPISyncMapping> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            List<KPISyncMapping> listKPISyncMapping = new List<KPISyncMapping>();
            foreach (DataRow row in dataTable.Rows)
            {
                KPISyncMapping KPISyncMapping = convertFromDataRow(row);
                listKPISyncMapping.Add(KPISyncMapping);
            }
            return listKPISyncMapping;
        }

        public List<KPISyncMapping> GetKPIByDate(DateTime datetime)
        {
            return GetByDate(datetime, "0%");
        }

        public List<KPISyncMapping> GetTTByDate(DateTime datetime)
        {
            return GetByDate(datetime, "1%");
        }

        private List<KPISyncMapping> GetByDate(DateTime datetime, string type)
        {
            int year = datetime.Year;
            int month = datetime.Month;

            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = string.Format(QUERY_BY_DATE, year, month, type);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            List<KPISyncMapping> listKPISyncMapping = new List<KPISyncMapping>();
            foreach (DataRow row in dataTable.Rows)
            {
                KPISyncMapping KPISyncMapping = convertFromDataRow(row);
                listKPISyncMapping.Add(KPISyncMapping);
            }
            return listKPISyncMapping;
        }

        private KPISyncMapping convertFromDataRow(DataRow dataRow)
        {
            KPISyncMapping result = new KPISyncMapping()
            {
                ID = dataRow["ID"].ToString(),
                Type = dataRow["TYPE"].ToString(),
                Year = Convert.ToInt32(dataRow["YEAR"]),
                MonthPeriod = dataRow["MONTH_PERIOD"].ToString(),
                SyncColumn = dataRow["SYNC_COLUMN"].ToString(),
                KPICode = dataRow["KPI_CODE"].ToString(),
                Description = dataRow["DESCRIPTION"].ToString(),
            };
            return result;
        }
    }
}
