﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;
using Sys.Core.Utility;

namespace TempToCRM.Repository
{
    public class OrganizationRepository : BaseRepository<Organization>
    {
        private const string QUERY_ALL = @"SELECT * FROM [M_ORGANIZATION]";
        private const string QUERY_BY_IDSOURCE = @"SELECT * FROM [M_ORGANIZATION] WHERE ID_SOURCE = {0}";

        public OrganizationRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<Organization> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<Organization> result = new List<Organization>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public Organization GetByIDSource(string idSource)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            string query = string.Format(QUERY_BY_IDSOURCE, idSource);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            return convertFromDataRow(dataTable.Rows[0]);
        }

        private Organization convertFromDataRow(DataRow dataRow)
        {
            Organization result = new Organization();
            result.ID = dataRow["ID"].ToString();
            result.IDSource = NumberUltility.ToNullable(dataRow["ID_SOURCE"]);
            result.OrganizationCode = dataRow["ORGANIZATION_CODE"].ToString();
            result.OrganizationName = dataRow["ORGANIZATION_NAME"].ToString();
            result.AreaID = (dataRow.IsNull("AREA_ID")) ? null : dataRow["AREA_ID"].ToString();
            result.ParentID = (dataRow.IsNull("PARENT_ID")) ? null : dataRow["PARENT_ID"].ToString();
            return result;
        }
    }
}
