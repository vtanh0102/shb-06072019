﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Utility;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class KPIActualResultDetailsRepository : BaseRepository<KPIActualResultDetails>
    {
        private const string QUERY_ALL = "SELECT * FROM B_KPI_ACT_RESULT_DETAIL";
        private const string QUERY_BY_KEY = @"SELECT * FROM B_KPI_ACT_RESULT_DETAIL 
                                            WHERE YEAR = '{0}' AND MONTH_PERIOD = {1}
                                            AND MA_NHAN_VIEN = '{2}' AND KPI_CODE = '{3}'";

        public KPIActualResultDetailsRepository(string connectionString) : base(connectionString)
        {
        }

        public List<KPIActualResultDetails> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<KPIActualResultDetails> result = new List<KPIActualResultDetails>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public KPIActualResultDetails GetByKey(string year, int month, string maNhanVien, string kpiCode)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_KEY, year, month, maNhanVien, kpiCode);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            KPIActualResultDetails KPI = convertFromDataRow(dataTable.Rows[0]);
            return KPI;
        }

        private KPIActualResultDetails convertFromDataRow(DataRow dataRow)
        {
            KPIActualResultDetails result = new KPIActualResultDetails();
            result.ID = dataRow["ID"].ToString();
            //result.Name = dataRow["NAME"].ToString();
            //result.ActualResultCode = dataRow["ACTUAL_RESULT_CODE"].ToString();
            result.Year = Convert.ToInt32(dataRow["YEAR"]);
            result.MonthPeriod = dataRow["MONTH_PERIOD"].ToString();
            //result.EmployeeID = dataRow["EMPLOYEE_ID"].ToString();
            result.UserCode = dataRow["MA_NHAN_VIEN"].ToString();
            //result.VersionNumber = dataRow["VERSION_NUMBER"].ToString();
            //result.KPIID = dataRow["KPI_ID"].ToString();
            result.KPICode = dataRow["KPI_CODE"].ToString();
            //result.KPIName = dataRow["KPI_NAME"].ToString();
            //result.KPIUnit = Convert.ToInt32(dataRow["KPI_UNIT"]);
            //result.LevelNumber = Convert.ToInt32(dataRow["LEVEL_NUMBER"]);
            //result.Ratio = Convert.ToDecimal(dataRow["RATIO"]);
            //result.PlanValue = Convert.ToDecimal(dataRow["PLAN_VALUE"]);
            //result.SyncValue = Convert.ToDecimal(dataRow["SYNC_VALUE"]);
            //result.FinalValue = Convert.ToDecimal(dataRow["FINAL_VALUE"]);
            //result.PercentSyncValue = dataRow.IsNull("PERCENT_SYNC_VALUE") ? 0 : Convert.ToDecimal(dataRow["PERCENT_SYNC_VALUE"]);
            //result.PercentFinalValue = dataRow.IsNull("PERCENT_FINAL_VALUE") ? 0 : Convert.ToDecimal(dataRow["PERCENT_FINAL_VALUE"]);
            //result.PercentManualValue = dataRow.IsNull("PERCENT_MANUAL_VALUE") ? 0 : Convert.ToDecimal(dataRow["PERCENT_MANUAL_VALUE"]);
            //result.LastestSyncDate = Convert.ToDateTime(dataRow["LATEST_SYNC_DATE"]);
            return result;
        }

    }
}
