﻿using Sys.Core.DBHelper;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Extensions;
using Sys.Core.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class TTActualResultRepository : BaseRepository<TTActualResult>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_KPI_TOI_ACTUAL_RESULT";

        public TTActualResultRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<TTActualResult> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<TTActualResult> result = new List<TTActualResult>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private TTActualResult convertFromDataRow(DataRow dataRow)
        {
            TTActualResult result = new TTActualResult();
            result.ID = dataRow["ID"].ToString();
            result.ActualResultCode = dataRow["ACTUAL_RESULT_CODE"].ToString();
            result.Year = Convert.ToInt32(dataRow["YEAR"]);
            result.MonthPeriod = dataRow["MONTH_PERIOD"].ToString();
            result.EmployeeID = dataRow["EMPLOYEE_ID"].ToString();
            result.UserCode = dataRow["USER_CODE"].ToString();
            result.VersionNumber = dataRow["VERSION_NUMBER"].ToString();
            result.LastestSyncDate = DateTimeUltility.ToNullable(dataRow["LATEST_SYNC_DATE"]);
            return result;
        }

    }
}
