﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities.Target;

namespace TempToCRM.Repository.Target
{
    public class RolesUsersRepository : BaseRepository<RolesUsers>
    {
        private const string QUERY_ALL = @"SELECT * FROM [ACL_ROLES_USERS]";

        public RolesUsersRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<RolesUsers> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<RolesUsers> result = new List<RolesUsers>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private RolesUsers convertFromDataRow(DataRow dataRow)
        {
            RolesUsers result = new RolesUsers()
            {
                ID = dataRow["ID"].ToString(),
                Deleted = Convert.ToBoolean(dataRow["DELETED"]),
                RoleID = dataRow["ROLE_ID"].ToString(),
                UserID = dataRow["USER_ID"].ToString(),
            };
            return result;
        }
    }
}
