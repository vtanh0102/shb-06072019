﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Repository.Source
{
    public class QTKinhNghiemTempRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM NS_QT_KINH_NGHIEM_TMP";

        public QTKinhNghiemTempRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DataTable GetTableTemp()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);
            return dataTable;
        }

    }
}
