﻿using Sys.Core.DBHelper;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Entities.Source;

namespace TempToCRM.Repository.Source
{
    public class PosTempRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM M_POS_TMP";

        public PosTempRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<PosTemp> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<PosTemp> result = new List<PosTemp>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private PosTemp convertFromDataRow(DataRow dataRow)
        {
            PosTemp result = new PosTemp()
            {
                IDSource = Convert.ToInt32(dataRow["ID_SOURCE"]),
                PosCode = dataRow["POS_CODE"].ToString(),
                PosMainCode = dataRow["POS_MAIN_CODE"].ToString(),
                PosName = dataRow["POS_NAME"].ToString(),
                Description = dataRow["DESCRIPTION"].ToString(),
                ParentPosID = NumberUltility.ToNullable(dataRow["PARENT_POS_ID"]),
                IsDelete = Convert.ToBoolean(dataRow["DELETED"]),
                Area = dataRow["AREA"].ToString(),
            };
            return result;
        }

        public DataTable GetTableTemp()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);
            return dataTable;
        }

    }
}
