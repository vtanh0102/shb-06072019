﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Extensions;

namespace TempToCRM.Repository
{
    public class KPIResultEmpTempRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM B_KPI_RESULT_EMP_TMP";
        private const string QUERY_CURRENT_MONTH = @"SELECT * FROM B_KPI_RESULT_EMP_TMP WHERE YEAR = YEAR(getdate()) AND PERIOD = '{0}'";

        public KPIResultEmpTempRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DataTable GetTableTemp(bool isFirstTime)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            string query = QUERY_ALL;
            if (!isFirstTime)
                query = string.Format(QUERY_CURRENT_MONTH, DateTime.Now.Month.StringFromMonth());

            var dataTable = dbHelper.getDataTable(query, CommandType.Text);
            return dataTable;
        }
    }
}
