﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Repository.Source
{
    public class SalaryStatusTmpRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM B_SALARY_STATUS_TMP";

        public SalaryStatusTmpRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DataTable GetTableTemp()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);
            return dataTable;
        }

    }
}
