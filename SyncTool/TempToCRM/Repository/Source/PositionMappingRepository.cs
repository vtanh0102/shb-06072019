﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Entities.Source;

namespace TempToCRM.Repository.Source
{
    public class PositionMappingRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM SYNC_POSITION_MAPPING";

        public PositionMappingRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<PositionMapping> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<PositionMapping> result = new List<PositionMapping>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private PositionMapping convertFromDataRow(DataRow dataRow)
        {
            PositionMapping result = new PositionMapping()
            {
                ID = Convert.ToInt32(dataRow["ID"]),
                PositionCode = dataRow["POSITION_CODE"].ToString(),
                PositionName = dataRow["POSITION_NAME"].ToString(),
            };
            return result;
        }

        public DataTable GetTableTemp()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);
            return dataTable;
        }

    }
}
