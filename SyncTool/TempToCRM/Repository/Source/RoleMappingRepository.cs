﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Entities.Source;

namespace TempToCRM.Repository.Source
{
    public class RoleMappingRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM SYNC_ROLE_MAPPING";
        private const string QUERY_DEFAULT = @"SELECT * FROM SYNC_ROLE_MAPPING WHERE TITLE_CODE = 'DEFAULT'";

        public RoleMappingRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<RoleMapping> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<RoleMapping> result = new List<RoleMapping>();
            foreach (DataRow row in dataTable.Rows)
                result.AddRange(convertFromDataRow(row));
            return result;
        }

        public List<RoleMapping> GetDefault()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_DEFAULT, CommandType.Text);

            List<RoleMapping> result = new List<RoleMapping>();
            foreach (DataRow row in dataTable.Rows)
                result.AddRange(convertFromDataRow(row));
            return result;
        }

        private List<RoleMapping> convertFromDataRow(DataRow dataRow)
        {
            var listTitleCode = dataRow["TITLE_CODE"].ToString().Split(',');
            List<RoleMapping> result = new List<RoleMapping>();
            foreach (string titleCode in listTitleCode)
            {
                RoleMapping roleMapping = new RoleMapping()
                {
                    ID = Convert.ToInt32(dataRow["ID"]),
                    TitleCode = titleCode.Trim(),
                    RoleName = dataRow["ROLE_NAME"].ToString(),
                };
                result.Add(roleMapping);
            }
            return result;
        }

        public DataTable GetTableTemp()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);
            return dataTable;
        }

    }
}
