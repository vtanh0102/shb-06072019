﻿using Sys.Core;
using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Services
{
    public abstract class BaseSyncService
    {
        protected DBConnection dbConnection { get; set; }
        // Config Repository
        private DBConnectionRepository dbConnectionRepository;
        private TableMappingRepository tableMappingRepository;

        public MessageLog messageLogService;

        public BaseSyncService(DBConnection dbConnection)
        {
            dbConnectionRepository = new DBConnectionRepository(Configuration.ConnectionString);
            tableMappingRepository = new TableMappingRepository(Configuration.ConnectionString);
            this.dbConnection = dbConnection;
        }

        protected abstract Result<string, DataTable> GetTempData();

    }
}
