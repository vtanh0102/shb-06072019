﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository.Target;
using TempToCRM.Repository.Source;
using Sys.Core.Enums;
using System.Data;
using TempToCRM.Entities.Source;
using Sys.Core;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;
using TempToCRM.Repository;
using TempToCRM.Entities;

namespace TempToCRM.Services
{
    public class SalaryStatusSyncService : BaseSyncService
    {
        private SalaryStatusTmpRepository salaryStatusTmpRepository;
        private SalaryStatusRepository salaryStatusRepository;

        public SalaryStatusSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            salaryStatusRepository = new SalaryStatusRepository(dbConnection.TargetConnectionString);
            salaryStatusTmpRepository = new SalaryStatusTmpRepository(dbConnection.SourceConnectionString);
        }

        public void Sync()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            #region Metadata
            var resultData = GetTempData();
            if (resultData.Code != ResultCode.Success)
            {
                resultData.Code = ResultCode.ConnectToDBNotSuccess;
                resultData.Message = resultData.Message;
                messageLogService.AddSyncError(resultData);
                return;
            }

            List<SalaryStatus> listSalaryStatus = new List<SalaryStatus>();
            try
            {
                listSalaryStatus = salaryStatusRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
                return;
            }
            #endregion

            var dataTable = resultData.ExtensiveInformation;
            foreach (DataRow data in dataTable.Rows)
            {
                try
                {
                    SalaryStatus existedValue = listSalaryStatus.FirstOrDefault(c => c.IDSource == Convert.ToInt32(data["ID_SOURCE"]));
                    if (existedValue == null)
                    {
                        SalaryStatus salaryStatus = new SalaryStatus();
                        MappingSalaryStatus(data, ref salaryStatus);
                        salaryStatus.ID = Guid.NewGuid().ToString();
                        salaryStatus.IDSource = Convert.ToInt32(data["ID_SOURCE"]);
                        salaryStatusRepository.Insert(salaryStatus);
                    }
                    else
                    {
                        MappingSalaryStatus(data, ref existedValue);
                        salaryStatusRepository.Update(existedValue);
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(SalaryStatus)), (int) data["ID_SOURCE"]);
                    continue;
                }
            }
        }

        private void MappingSalaryStatus(DataRow dr, ref SalaryStatus p)
        {
            p.Name = dr["NAME"].ToString();
        }

        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = salaryStatusTmpRepository.GetTableTemp();
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

      
    }
}
