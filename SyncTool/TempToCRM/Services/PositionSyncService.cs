﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository.Target;
using TempToCRM.Repository.Source;
using Sys.Core.Enums;
using System.Data;
using TempToCRM.Entities.Source;
using Sys.Core;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;

namespace TempToCRM.Services
{
    public class PositionSyncService : BaseSyncService
    {
        private PositionTmpRepository mPositionTmpRepository;
        private PositionRepository mPositionRepository;
        private PositionMappingRepository positionMappingRepository;

        public PositionSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            mPositionRepository = new PositionRepository(dbConnection.TargetConnectionString);
            mPositionTmpRepository = new PositionTmpRepository(dbConnection.SourceConnectionString);
            positionMappingRepository = new PositionMappingRepository(dbConnection.SourceConnectionString);
        }

        public void Sync()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            #region Metadata
            var resultData = GetTempData();
            if (resultData.Code != ResultCode.Success)
            {
                resultData.Code = ResultCode.ConnectToDBNotSuccess;
                resultData.Message = resultData.Message;
                messageLogService.AddSyncError(resultData);
                return;
            }

            List<Position> listPosition = new List<Position>();
            List<PositionMapping> listPositionMapping = new List<PositionMapping>();
            try
            {
                listPosition = mPositionRepository.GetAll();
                listPositionMapping = positionMappingRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
                return;
            }
            #endregion

            var dataTable = resultData.ExtensiveInformation;
            foreach (DataRow data in dataTable.Rows)
            {
                try
                {
                    Position existedValue = listPosition.FirstOrDefault(c => c.IDSource == Convert.ToInt32(data["ID_SOURCE"]));
                    string positionName = (string.IsNullOrWhiteSpace(data["POSITION_NAME"].ToString())) ? "SHB chưa nhập" : data["POSITION_NAME"].ToString();
                    PositionMapping existedMapping = listPositionMapping.FirstOrDefault(c => c.PositionName.ToUpper() == positionName.ToUpper());
                    if (existedMapping == null) continue;

                    if (existedValue == null)
                    {
                        Position position = new Position();
                        MappingPosition(data, ref position);
                        position.PositionCode = existedMapping.PositionCode;
                        position.ID = Guid.NewGuid().ToString();
                        mPositionRepository.Insert(position);
                    }
                    else
                    {
                        MappingPosition(data, ref existedValue);
                        existedValue.PositionCode = existedMapping.PositionCode;
                        mPositionRepository.Update(existedValue);
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(Position)), (int) data["ID_SOURCE"]);
                    continue;
                }
            }
        }

        private void MappingPosition(DataRow dr, ref Position p)
        {
            if (string.IsNullOrWhiteSpace(dr["ID_SOURCE"].ToString()))
                throw new Exception("ChucDanhID bị trống");
            if (string.IsNullOrWhiteSpace(dr["POSITION_NAME"].ToString()))
                throw new Exception("TenChucDanh bị trống");

            p.IDSource = Convert.ToInt32(dr["ID_SOURCE"]);
            p.PositionName = (string.IsNullOrWhiteSpace(dr["POSITION_NAME"].ToString())) ? "SHB chưa nhập" : dr["POSITION_NAME"].ToString();
        }

        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = mPositionTmpRepository.GetTableTemp();
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

      
    }
}
