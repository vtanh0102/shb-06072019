﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository.Target;
using TempToCRM.Repository.Source;
using Sys.Core.Enums;
using System.Data;
using TempToCRM.Entities.Source;
using Sys.Core;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;
using TempToCRM.Repository;
using Sys.Core.Models;
using TempToCRM.Entities;
using System.Text.RegularExpressions;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;

namespace TempToCRM.Services
{
    public class OrganizationSyncService : BaseSyncService
    {
        private PosTempRepository posTempRepository;
        private AreaRepository areaRepository;
        private OrganizationRepository organizationRepository;

        public OrganizationSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            posTempRepository = new PosTempRepository(dbConnection.SourceConnectionString);
            areaRepository = new AreaRepository(dbConnection.TargetConnectionString);
            organizationRepository = new OrganizationRepository(dbConnection.TargetConnectionString);
        }

        public void Sync()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            #region Metadata
            IEnumerable<PosTemp> tempData = new List<PosTemp>();
            IEnumerable<PosTemp> listMainOrganizationTmp = new List<PosTemp>();
            IEnumerable<Area> listArea = new List<Area>();
            IEnumerable<Organization> listOrganization = new List<Organization>();
            try
            {
                tempData = posTempRepository.GetAll();
                listMainOrganizationTmp = tempData.Where(c => c.ParentPosID == 1343);

                listArea = areaRepository.GetAll();
                listOrganization = organizationRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
                return;
            }
            #endregion

            foreach (var parentPos in listMainOrganizationTmp)
            {
                try
                {
                    // Chi nhánh
                    Organization existedValue = listOrganization.FirstOrDefault(c => c.IDSource == parentPos.IDSource);
                    var matchParentArea = listArea.FirstOrDefault(c => c.AreaName == parentPos.Area);
                    string ID;
                    if (existedValue == null)
                    {
                        Organization organization = new Organization();
                        ID = organization.ID = Guid.NewGuid().ToString();
                        MappingOrganization(parentPos, 3, false, ref organization);
                        organization.ParentID = null;
                        organizationRepository.Insert(organization);
                    }
                    else
                    {
                        ID = existedValue.ID;
                        MappingOrganization(parentPos, 3, false, ref existedValue);
                        existedValue.ParentID = null;
                        organizationRepository.Update(existedValue);
                    }

                    // Main Sub Organization
                    var listMainSubOrganization = tempData.Where(c => c.IDSource != parentPos.IDSource
                                                            && c.PosMainCode == parentPos.PosMainCode
                                                            && (c.PosCode == "120199" || c.PosCode == "120204" || c.PosCode == "120205"));
                    foreach (var mainSubPos in listMainSubOrganization)
                    {
                        Organization existedSubPos = listOrganization.FirstOrDefault(c => c.IDSource == mainSubPos.IDSource);
                        var matchSubArea = listArea.FirstOrDefault(c => c.AreaName == mainSubPos.Area);
                        if (existedSubPos == null)
                        {
                            Organization organization = new Organization();
                            organization.ID = Guid.NewGuid().ToString();
                            MappingOrganization(mainSubPos, 4, true, ref organization);
                            organization.ParentID = ID;
                            organizationRepository.Insert(organization);
                        }
                        else
                        {
                            MappingOrganization(mainSubPos, 4, true, ref existedSubPos);
                            existedSubPos.ParentID = ID;
                            organizationRepository.Update(existedSubPos);
                        }
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(Organization)), (int) parentPos.IDSource);
                    continue;
                }
            }

            // Sub Organization
            var listSubOrg = tempData.Where(c => c.PosMainCode.Length >= 4 && c.PosName.ToUpper().Contains("- PGD"));
            foreach (var subPos in listSubOrg)
            {
                try
                {
                    Organization existedSubPos = listOrganization.FirstOrDefault(c => c.IDSource == subPos.IDSource);
                    Organization parentPos = organizationRepository.GetByIDSource(subPos.ParentPosID.ToString());

                    var matchSubArea = listArea.FirstOrDefault(c => c.AreaName == subPos.Area);
                    if (existedSubPos == null)
                    {
                        Organization organization = new Organization();
                        organization.ID = Guid.NewGuid().ToString();
                        MappingOrganization(subPos, 4,  false,ref organization);
                        organization.ParentID = (parentPos != null) ? parentPos.ID : null;
                        organizationRepository.Insert(organization);
                    }
                    else
                    {
                        MappingOrganization(subPos, 4, false, ref existedSubPos);
                        existedSubPos.ParentID = (parentPos != null) ? parentPos.ID : null; 
                        organizationRepository.Update(existedSubPos);
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(Organization)), (int)subPos.IDSource);
                    continue;
                }
            }
        }

        private void MappingOrganization(PosTemp temp, int level, bool isAddPosCode, ref Organization organization)
        {
            if (string.IsNullOrWhiteSpace(temp.PosName))
                throw new Exception("Ten bị trống");

            organization.IDSource = temp.IDSource;
            if (!isAddPosCode)
                organization.OrganizationCode = temp.PosMainCode;
            else
                organization.OrganizationCode = temp.PosMainCode + "-" + temp.PosCode;

            organization.OrganizationName = temp.PosName;
            organization.Description = temp.Description;
            organization.Deleted = temp.IsDelete;
            organization.LevelNumber = level;

            if (string.IsNullOrWhiteSpace(organization.OrganizationCode))
                throw new Exception("MaPhongBan bị trống");
        }

        override protected Result<string, DataTable> GetTempData()
        {
            return null;
        }

    }
}
