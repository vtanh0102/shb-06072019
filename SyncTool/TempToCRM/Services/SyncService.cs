﻿using Sys.Core;
using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Repository.Source;
using Sys.Core.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Services
{
    public class SyncService
    {

        #region Fields

        private DBConnectionRepository dbConnectionRepository;
        private ConfigRepository configRepository;
        private MessageLog messageLogService;
        private NotificationService notificationService;

        private PositionSyncService positionSyncService;
        private OrganizationSyncService organizationSyncService;
        private TitleSyncService titleSyncService;
        private TitleAddSyncService titleAddSyncService;
        private SalaryStatusSyncService salaryStatusSyncService;
        private EmployeeSyncService employeeSyncService;
        private KPISyncService kpiSyncService;
        private TTSyncService ttSyncService;

        #endregion

        public SyncService()
        {
            try
            {
                messageLogService = new MessageLog("");
                dbConnectionRepository = new DBConnectionRepository(Configuration.ConnectionString);
                var dbConnection = dbConnectionRepository.GetDBConnectionInfo(Configuration.ConnectionCode);

                positionSyncService = new PositionSyncService(dbConnection);
                organizationSyncService = new OrganizationSyncService(dbConnection);
                titleSyncService = new TitleSyncService(dbConnection);
                titleAddSyncService = new TitleAddSyncService(dbConnection);
                salaryStatusSyncService = new SalaryStatusSyncService(dbConnection);
                employeeSyncService = new EmployeeSyncService(dbConnection);
                kpiSyncService = new KPISyncService(dbConnection);
                ttSyncService = new TTSyncService(dbConnection);

                configRepository = new ConfigRepository(dbConnection.SourceConnectionString);
                notificationService = new NotificationService(dbConnection.SourceConnectionString, dbConnection.TargetConnectionString);
                
                messageLogService = new MessageLog(dbConnection.SourceConnectionString);
                positionSyncService.messageLogService = messageLogService;
                organizationSyncService.messageLogService = messageLogService;
                titleSyncService.messageLogService = messageLogService;
                titleAddSyncService.messageLogService = messageLogService;
                salaryStatusSyncService.messageLogService = messageLogService;
                employeeSyncService.messageLogService = messageLogService;
                kpiSyncService.messageLogService = messageLogService;
                ttSyncService.messageLogService = messageLogService;
            }
            catch (Exception ex)
            {
                Result result = new Result();
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
            }
        }

        public void SyncHR()
        {
            var resultSync = CheckSyncHistory(TypeSys.HUMAN_RESOURCE);
            if (resultSync.Code != ResultCode.Success)
            {
                if (!resultSync.Data)
                    return;
                messageLogService.AddSyncError(resultSync);
            }

            organizationSyncService.Sync();
            positionSyncService.Sync();
            titleSyncService.Sync();
            titleAddSyncService.Sync();
            salaryStatusSyncService.Sync();

            var employeeResult = employeeSyncService.Sync();
            messageLogService.AddTotalRecord(employeeResult.ExtensiveInformation.Item1);
            messageLogService.SetSyncRecord(employeeResult.ExtensiveInformation.Item2);
            if (employeeResult.Code != ResultCode.Success)
                messageLogService.AddSyncError(employeeResult);

            AddCompleteNotification(TypeSys.HUMAN_RESOURCE);
            messageLogService.Update();
        }

        public void SyncKPI()
        {
            var resultSync = CheckSyncHistory(TypeSys.KPI);
            if (resultSync.Code != ResultCode.Success)
            {
                if (!resultSync.Data)
                    return;
                messageLogService.AddSyncError(resultSync);
            }

            var kpiSyncResult = kpiSyncService.Sync();
            messageLogService.AddTotalRecord(kpiSyncResult.ExtensiveInformation.Item1);
            messageLogService.SetSyncRecord(kpiSyncResult.ExtensiveInformation.Item2);
            if (kpiSyncResult.Code != ResultCode.Success)
                messageLogService.AddSyncError(kpiSyncResult);

            AddCompleteNotification(TypeSys.KPI);
            messageLogService.Update();
        }

        public void SyncTT()
        {
            var resultSync = CheckSyncHistory(TypeSys.KPI);
            if (resultSync.Code != ResultCode.Success)
            {
                if (!resultSync.Data)
                    return;
                messageLogService.AddSyncError(resultSync);
            }

            var ttSyncResult = ttSyncService.Sync();
            messageLogService.AddTotalRecord(ttSyncResult.ExtensiveInformation.Item1);
            messageLogService.SetSyncRecord(ttSyncResult.ExtensiveInformation.Item2);
            if (ttSyncResult.Code != ResultCode.Success)
                messageLogService.AddSyncError(ttSyncResult);

            AddCompleteNotification(TypeSys.KPI);
            messageLogService.Update();
        }

        protected Result<bool> CheckSyncHistory(TypeSys typeSys)
        {
            var result = new Result<bool>();
            result.Data = true;
            result.Code = ResultCode.Success;
            try
            {
                messageLogService.GetTodaySync(typeSys);
                return result;
            }
            catch (NoResultFoundException)
            {
                result.Data = false;
                result.Code = ResultCode.Fail;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

        protected void AddCompleteNotification(TypeSys typeSys)
        {
            bool enableNotification = configRepository.GetByKey<bool>("EnableNotify");
            if (enableNotification)
            {
                notificationService.AddCompleteNotification(false, typeSys);
                var resultNotify = notificationService.InsertToDB();
                if (resultNotify.Code != ResultCode.Success)
                    messageLogService.AddSyncError(resultNotify, "B_NOTIFICATION");
            }
        }

    }
}
