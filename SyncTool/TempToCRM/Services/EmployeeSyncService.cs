﻿using Sys.Core;
using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository;
using TempToCRM.Repository.Source;
using TempToCRM.Repository.Target;
using TempToCRM.Entities;
using TempToCRM.Entities.Target;
using TempToCRM.Entities.Source;
using Sys.Core.Services;
using Sys.Core.Extensions;
using Sys.Core.Repository.Source;

namespace TempToCRM.Services
{
    public class EmployeeSyncService : BaseSyncService
    {
        private RoleMappingRepository roleMappingRepository;
        private PosTempRepository posTempRepository;
        private UserTempRepository userTempRepository;
        private QTKinhNghiemTempRepository kinhnghiemRepository;
        private QTChuyenCanBoTempRepository chuyencanboRepository;

        private ACLRoleRepository aclRoleRepository;
        private RolesUsersRepository rolesUsersRepository;
        private UserRepository userRepository;
        private UserExtendRepository userExtendRepository;
        private OrganizationRepository organizationRepository;
        private PositionRepository positionRepository;
        private TitleRepository titleRepository;
        private TitleAddRepository titleAddRepository;
        private SalaryStatusRepository salaryStatusRepository;

        private NotificationService notificationService;
        private ConfigRepository configRepository;
        public Dictionary<string, string> SystemConfig { get; set; }

        public EmployeeSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            // Source
            roleMappingRepository = new RoleMappingRepository(dbConnection.SourceConnectionString);
            posTempRepository = new PosTempRepository(dbConnection.SourceConnectionString);
            userTempRepository = new UserTempRepository(dbConnection.SourceConnectionString);
            kinhnghiemRepository = new QTKinhNghiemTempRepository(dbConnection.SourceConnectionString);
            chuyencanboRepository = new QTChuyenCanBoTempRepository(dbConnection.SourceConnectionString);
            notificationService = new NotificationService(dbConnection.SourceConnectionString, dbConnection.TargetConnectionString);
            configRepository = new ConfigRepository(dbConnection.SourceConnectionString);

            // Target
            aclRoleRepository = new ACLRoleRepository(dbConnection.TargetConnectionString);
            rolesUsersRepository = new RolesUsersRepository(dbConnection.TargetConnectionString);
            userRepository = new UserRepository(dbConnection.TargetConnectionString);
            userExtendRepository = new UserExtendRepository(dbConnection.TargetConnectionString);
            organizationRepository = new OrganizationRepository(dbConnection.TargetConnectionString);
            positionRepository = new PositionRepository(dbConnection.TargetConnectionString);
            titleRepository = new TitleRepository(dbConnection.TargetConnectionString);
            titleAddRepository = new TitleAddRepository(dbConnection.TargetConnectionString);
            salaryStatusRepository = new SalaryStatusRepository(dbConnection.TargetConnectionString);
        }

        /// <summary>
        /// Tuple: Tổng số bản ghi & số bản ghi sync thành công
        /// </summary>
        public Result<string, Tuple<int, int>> Sync()
        {
            var result = new Result<string, Tuple<int, int>>();
            result.Code = ResultCode.Success;
            int totalRecord = 0;
            int syncRecord = 0;

            #region Metadata
            var resultData = GetTempData();
            if (resultData.Code != ResultCode.Success)
            {
                result.Code = resultData.Code;
                result.Message = resultData.Message;
                result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                return result;
            }

            List<RoleMapping> listRoleMapping = null;
            List<RoleMapping> listDefaultRoleMapping = null;
            DataTable tableKinhnghiem = null;
            DataTable tableChuyencanbo = null;
            List<ACLRole> listRole = null;
            List<RolesUsers> listRolesUsers = null;
            List<Organization> listOrg = null;
            List<Position> listPosition = null;
            List<Title> listTitle = null;
            List<TitleAdd> listTitleAdd = null;
            List<SalaryStatus> listSalaryStatus = null;
            List<User> listUser = null;
            List<UserExtend> listExtendUser = null;
            try
            {
                this.SystemConfig = configRepository.GetAll();

                listRoleMapping = roleMappingRepository.GetAll();
                listDefaultRoleMapping = roleMappingRepository.GetDefault();
                tableKinhnghiem = kinhnghiemRepository.GetTableTemp();
                tableChuyencanbo = chuyencanboRepository.GetTableTemp();

                listRole = aclRoleRepository.GetAll();
                listRolesUsers = rolesUsersRepository.GetAll();
                listOrg = organizationRepository.GetAll();
                listPosition = positionRepository.GetAll();
                listTitle = titleRepository.GetAll();
                listTitleAdd = titleAddRepository.GetAll();
                listSalaryStatus = salaryStatusRepository.GetAll();

                listUser = userRepository.GetAll();
                listExtendUser = userExtendRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                return result;
            }

            // Lấy tham số cấu hình notification
            bool enableNotification = Convert.ToBoolean(SystemConfig["EnableNotify"]);
            if (enableNotification)
            {
                var resultConfig = notificationService.RefreshListConfig("User");
                if (resultConfig.Code != ResultCode.Success)
                {
                    messageLogService.AddSyncError(resultConfig, EntityExtensions.GetTableName(typeof(User)));
                    return result;
                }
            }

            #endregion
            var dataTable = resultData.ExtensiveInformation;

            int index = 1;
            Console.WriteLine("Sync User");
            totalRecord = dataTable.Rows.Count;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                try
                {
                    Console.WriteLine("(" + index + "/" + dataTable.Rows.Count + ")");
                    index++;

                    // Tìm phòng ban
                    Organization matchOrg = listOrg.FirstOrDefault(c => c.IDSource == Convert.ToInt32(dataRow["POS_ID"]));
                    string posName = matchOrg != null ? matchOrg.OrganizationName : "";

                    // Tìm chi nhánh cho GĐ/PGĐ
                    bool isGD = false;
                    Organization matchChiNhanh = null;
                    string[] listCodeGD = {"A01","A01.02","A02","A02.01","A02.07"};
                    if (listCodeGD.Contains(dataRow["TITLE_CODE"].ToString().ToUpper()))
                    {
                        isGD = true;
                        matchChiNhanh = listOrg.FirstOrDefault(c => c.OrganizationCode == dataRow["POS_MAIN_CODE"].ToString());
                    }

                    // Tìm chức danh
                    var matchPosition = listPosition.FirstOrDefault(c => c.IDSource == NumberUltility.ToNullable(dataRow["POSITION_ID"]));

                    // Tìm chức vụ
                    var matchTitle = listTitle.FirstOrDefault(c => c.IDSource == Convert.ToInt32(dataRow["TITLE_ID"]));

                    // Tìm chức vụ kiêm nhiệm
                    var matchTitleAdd = listTitleAdd.FirstOrDefault(c => c.IDSource == NumberUltility.ToNullable(dataRow["TITLE_ADD_ID"]));

                    // Tìm trạng thái lương
                    var matchSalaryStatus = listSalaryStatus.FirstOrDefault(c => c.IDSource == NumberUltility.ToNullable(dataRow["SALARY_STATUS_ID"]));

                    #region User
                    string ID;
                    User updateUser = null;
                    // Insert/Update User
                    if (dataRow.IsNull("USER_CODE") || string.IsNullOrWhiteSpace(dataRow["USER_CODE"].ToString()))
                        throw new Exception("USER_CODE của nhân viên có ID: " + dataRow["ID_SOURCE"].ToString() + " bị trống");

                    var existedUserExtend = listExtendUser.FirstOrDefault(c => c.UserCode == dataRow["USER_CODE"].ToString());
                    if (existedUserExtend == null)
                    {
                        // UserExtend
                        UserExtend userExtend = new UserExtend();
                        MappingUserExtend(dataRow, tableKinhnghiem, tableChuyencanbo, ref userExtend, posName);
                        ID = userExtend.ID = Guid.NewGuid().ToString();
                        userExtend.OrganizationID = matchOrg != null ? matchOrg.ID : null;
                        userExtend.OrganizationCode = matchOrg != null ? matchOrg.OrganizationCode : null;
                        if (!isGD) userExtend.OrganizationParentID = matchOrg != null ? matchOrg.ParentID : null;
                        else userExtend.OrganizationParentID = matchChiNhanh != null ? matchChiNhanh.ID : null;
                        userExtend.AreaID = matchOrg != null ? matchOrg.AreaID : null;
                        userExtend.PositionID = matchPosition != null ? matchPosition.ID : null;
                        userExtend.PositionCode = matchPosition != null ? matchPosition.PositionCode : null;
                        userExtend.TitleID = matchTitle != null ? matchTitle.ID : null;
                        userExtend.TitleAddID = matchTitleAdd != null ? matchTitleAdd.ID : null;
                        userExtend.SalaryStatusID = matchSalaryStatus != null ? matchSalaryStatus.ID : null;
                        userExtendRepository.Insert(userExtend);

                        // User
                        User user = new User();
                        user.ID = ID;
                        MappingUser(dataRow, ref user);
                        user.UserHash = EncryptService.HashPassword(user.UserName);
                        userRepository.Insert(user);
                        updateUser = user;

                        // Add notification
                        if (enableNotification)
                            notificationService.AddInsertNotification(user.FirstName + " " + user.LastName, posName, ID, null);
                    }
                    else
                    {
                        User exitedUser = listUser.FirstOrDefault(c => c.ID == existedUserExtend.ID);
                        if (exitedUser == null) continue;

                        // User Extend
                        ID = existedUserExtend.ID;
                        MappingUserExtend(dataRow, tableKinhnghiem, tableChuyencanbo, ref existedUserExtend, posName);
                        existedUserExtend.OrganizationID = matchOrg != null ? matchOrg.ID : null;
                        existedUserExtend.AreaID = matchOrg != null ? matchOrg.AreaID : null;
                        existedUserExtend.PositionID = matchPosition != null ? matchPosition.ID : null;
                        existedUserExtend.PositionCode = matchPosition != null ? matchPosition.PositionCode : null;
                        existedUserExtend.OrganizationCode = matchOrg != null ? matchOrg.OrganizationCode : null;
                        if (!isGD) existedUserExtend.OrganizationParentID = matchOrg != null ? matchOrg.ParentID : null;
                        else existedUserExtend.OrganizationParentID = matchChiNhanh != null ? matchChiNhanh.ID : null;
                        var newTitleID = matchTitle != null ? matchTitle.ID : null;
                        if (existedUserExtend.TitleID != newTitleID)
                        {
                            notificationService.AddNotificationByID(2, exitedUser.FirstName + " " + exitedUser.LastName, posName, ID, null);
                            existedUserExtend.TitleID = matchTitle != null ? matchTitle.ID : null;
                        }
                        existedUserExtend.TitleAddID = matchTitleAdd != null ? matchTitleAdd.ID : null;
                        existedUserExtend.SalaryStatusID = matchSalaryStatus != null ? matchSalaryStatus.ID : null;
                        userExtendRepository.Update(existedUserExtend);

                        // User
                        MappingUser(dataRow, ref exitedUser);
                        exitedUser.UserHash = EncryptService.HashPassword(exitedUser.UserName);
                        userRepository.Update(exitedUser);
                        updateUser = exitedUser;

                        // Add notification
                        if (enableNotification)
                        {
                            notificationService.AddNullNotification(dataRow, exitedUser.FirstName + " " + exitedUser.LastName, posName, ID, null);
                            CheckUpdatePos(dataRow, existedUserExtend, matchOrg != null ? matchOrg.AreaID : null, exitedUser.FirstName + " " + exitedUser.LastName, posName);
                        }
                    }
                    #endregion

                    #region RolesUsers
                    var listMatchRoleMapping = listRoleMapping.Where(c => matchTitle != null && c.TitleCode == matchTitle.TitleCode);

                    // Insert Default Role nếu không thấy mapping
                    if (listMatchRoleMapping.Count() == 0)
                    {
                        foreach (var roleMapping in listDefaultRoleMapping)
                            MappingRolesUsers(roleMapping, listRole, listRolesUsers, updateUser, false);
                    }
                    else
                    {
                        foreach (var roleMapping in listMatchRoleMapping)
                            MappingRolesUsers(roleMapping, listRole, listRolesUsers, updateUser, true);
                    }
                    #endregion

                    syncRecord++;
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                    return result;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(User)), (int) dataRow["ID_SOURCE"]);
                    continue;
                }
            }

            if (enableNotification)
            {
                var resultNotify = notificationService.InsertToDB();
                if (resultNotify.Code != ResultCode.Success)
                    messageLogService.AddSyncError(resultNotify, "B_NOTIFICATION");
            }

            result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
            return result;
        }

        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = userTempRepository.GetTableTemp();
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

        protected void MappingRolesUsers(RoleMapping roleMapping, List<ACLRole> listRole, List<RolesUsers> listRolesUsers, User user, bool isSetPrimaryRole)
        {
            // Match Role
            var matchRole = listRole.Where(c => c.Name == roleMapping.RoleName).FirstOrDefault();
            if (matchRole == null)
                return;

            // Update primary role
            if (isSetPrimaryRole)
            {
                user.PrimaryRoleID = matchRole.ID;
                userRepository.Update(user);
            }

            // Insert nếu không tồn tại
            var matchRoleUser = listRolesUsers.Where(c => c.UserID == user.ID && c.RoleID == matchRole.ID).FirstOrDefault();
            if (matchRoleUser == null)
            {
                RolesUsers rolesUsers = new RolesUsers();
                rolesUsers.ID = Guid.NewGuid().ToString();
                rolesUsers.UserID = user.ID;
                rolesUsers.RoleID = matchRole.ID;
                rolesUsersRepository.Insert(rolesUsers);
            }
        }

        private void MappingUserExtend(DataRow dataRow, DataTable tableKinhnghiem, DataTable tableChuyencanbo, ref UserExtend userExtend, string posName)
        {
            if (string.IsNullOrWhiteSpace(dataRow["USER_CODE"].ToString()))
                throw new Exception("MaNhanVien bị trống");
            if (dataRow.IsNull("POS_ID"))
                throw new Exception("PhongBanID bị trống");
            if (dataRow.IsNull("POS_CODE"))
                throw new Exception("MaPhongBanHachToan của phòng ban " + dataRow["POS_ID"] + " bị trống");
            if (dataRow.IsNull("TITLE_ID"))
                throw new Exception("ChucVuID bị trống");
            if (dataRow.IsNull("POSITION_ID"))
                throw new Exception("ChucDanhID bị trống");

            userExtend.IDSource = Convert.ToInt32(dataRow["ID_SOURCE"]);
            userExtend.UserCode = dataRow["USER_CODE"].ToString();
            userExtend.BirthDate = DateTimeUltility.ToNullable(dataRow["BIRTH_DATE"]);
            userExtend.Sex = dataRow["SEX"].ToString();
            userExtend.RetiredDate = DateTimeUltility.ToNullable(dataRow["RETIRED_DATE"]);
            userExtend.ContractTypeIDSource = NumberUltility.ToNullable(dataRow["CONTRACT_TYPE_ID"]);
            int[] listSpecialContractType = { 0, 2, 3, 4, 16, 27, 28 };
            if (userExtend.ContractTypeIDSource == null || listSpecialContractType.Contains(userExtend.ContractTypeIDSource.Value))
                userExtend.ContractTypeIDSource = 5;
            userExtend.ManagerName = dataRow["MANAGER_NAME"].ToString();
            userExtend.DateStartWork = DateTimeUltility.ToNullable(dataRow["DATE_START_WORK"]);
            userExtend.DateStartIntern = DateTimeUltility.ToNullable(dataRow["DATE_START_INTERN"]);
            userExtend.DateStartProbation = DateTimeUltility.ToNullable(dataRow["DATE_START_PROBATION"]);
            userExtend.DateActingnPromoted = DateTimeUltility.ToNullable(dataRow["DATE_ACTING_PROMOTED"]);
            userExtend.OfficalPromotedDate = DateTimeUltility.ToNullable(dataRow["OFFICAL_PROMOTED_DATE"]);
            // Thâm niên
            if (userExtend.CurrentPositionWorkTime == null)
            {
                if (!dataRow.IsNull("CURRENT_POSITION_WORK_TIME"))
                    userExtend.CurrentPositionWorkTime = DateTimeUltility.ToNullable(dataRow["CURRENT_POSITION_WORK_TIME"]);
                else if (!dataRow.IsNull("DATE_START_WORK"))
                    userExtend.CurrentPositionWorkTime = DateTimeUltility.ToNullable(dataRow["DATE_START_WORK"]);
            }
            if (userExtend.CurrentPositionWorkTime != null)
            {
                int monthDiff = DateTimeUltility.MonthDifference(DateTime.Now, userExtend.CurrentPositionWorkTime.Value);
                string seniority = "";
                if (monthDiff <= 6)
                    seniority = "00_06";
                else if (monthDiff <= 12)
                    seniority = "06_12";
                else if (monthDiff <= 18)
                    seniority = "12_18";
                else if (monthDiff <= 24)
                    seniority = "18_24";
                else
                    seniority = "24_";
                if (userExtend.Seniority != seniority)
                {
                    notificationService.AddNotificationByID(5, dataRow["FIRST_NAME"] + " " + dataRow["LAST_NAME"], posName, userExtend.ID, null);
                    //notificationService.AddNotificationThamNien(userExtend.UserCode, userExtend.Seniority, seniority);
                    userExtend.Seniority = seniority;
                }
            }
            userExtend.PosNameCenter = dataRow["POS_NAME_CENTER"].ToString();
            userExtend.PosCode = dataRow["POS_CODE"].ToString();
            // Quá trình công tác
            var listPreviousEmployHistory = tableKinhnghiem.Select("NhanVienID = " + userExtend.IDSource);
            userExtend.PreviousEmployHistory = MappingPreviousEmployHistory(listPreviousEmployHistory);
            var listEmploymentHistory = tableChuyencanbo.Select("NhanVienID = " + userExtend.IDSource);
            userExtend.EmploymentHistory = MappingEmploymentHistory(listEmploymentHistory);
            userExtend.BussinessCode = dataRow["BUSINESS_TYPE"].ToString();
        }

        private void MappingUser(DataRow dataRow, ref User user)
        {
            if (string.IsNullOrWhiteSpace(dataRow["FIRST_NAME"].ToString()))
                throw new Exception("HoTen bị trống");
            // if (string.isnullorwhitespace(datarow["email1"].tostring()))
                // throw new exception("email bị trống");

            user.LastName = dataRow["LAST_NAME"].ToString();
            user.FirstName = dataRow["FIRST_NAME"].ToString();
            // Username
            string email1 = dataRow["EMAIL1"].ToString().Replace(".", "");
            int index = email1.IndexOf("@");
            if (index > 0)
                user.UserName = email1.Substring(0, index).ToLower();
            // if (string.IsNullOrWhiteSpace(user.UserName))
                // throw new Exception("Username bị trống do Email không đúng định dạng");

            user.Deleted = dataRow.IsNull("DELETED") ? false : Convert.ToBoolean(dataRow["DELETED"]);
            user.DateEntered = dataRow.IsNull("DATE_ENTERED") ? DateTime.Now : Convert.ToDateTime(dataRow["DATE_ENTERED"]);
            user.DateModified = dataRow.IsNull("DATE_MODIFIED") ? DateTime.Now : Convert.ToDateTime(dataRow["DATE_MODIFIED"]); ;
            user.PhoneHome = dataRow["PHONE_HOME"].ToString().Substring(0, Math.Min(dataRow["PHONE_HOME"].ToString().Length, 50));
            user.PhoneMobile = dataRow["PHONE_MOBILE"].ToString();
            user.PhoneWork = dataRow["PHONE_WORK"].ToString();
            user.PhoneFax = dataRow["PHONE_FAX"].ToString();
			if (string.IsNullOrWhiteSpace(user.Email1))
				user.Email1 = dataRow["EMAIL1"].ToString();
            user.FacebookID = dataRow["FACEBOOK_ID"].ToString();
            user.Status = "Active";
            user.ApproveStatus = Convert.ToInt32(dataRow["APPROVE_STATUS"]);
        }

        private string MappingPreviousEmployHistory(IEnumerable<DataRow> rows)
        {
            StringBuilder builder = new StringBuilder();
            string template = "{0}-{1}:{2}, {3}; ";
            foreach (DataRow row in rows)
            {
                DateTime? TuNgay = DateTimeUltility.ToNullable(row["TuNgay"]);
                String TuNgayFormat = TuNgay.HasValue ? TuNgay.Value.ToString("d/M") : "";
                DateTime? ToiNgay = DateTimeUltility.ToNullable(row["ToiNgay"]);
                String ToiNgayFormat = ToiNgay.HasValue ? ToiNgay.Value.ToString("d/M") : "";
                String ChucVu = row["ChucVu"].ToString();
                String NoiLamViec = row["NoiLamViec"].ToString();
                builder.Append(string.Format(template, TuNgayFormat, ToiNgayFormat, ChucVu, NoiLamViec));
            }
            return builder.ToString();
        }

        private string MappingEmploymentHistory(IEnumerable<DataRow> rows)
        {
            StringBuilder builder = new StringBuilder();
            string template = "Từ {0}-{1}:{2}, {3}; ";
            foreach (DataRow row in rows)
            {
                DateTime? NgayChuyen = DateTimeUltility.ToNullable(row["NgayChuyen"]);
                String NgayChuyenFormat = (NgayChuyen.HasValue) ? NgayChuyen.Value.ToString("d/M") : "";
                DateTime? NgayHetHan = DateTimeUltility.ToNullable(row["NgayHetHan"]);
                String NgayHetHanFormat = NgayHetHan.HasValue ? NgayHetHan.Value.ToString("d/M/yyyy") : "nay";
                String ChucVu = row["TenChucVuMoi"].ToString();
                String NoiLamViec = row["TenPhongBanMoi"].ToString();
                builder.Append(string.Format(template, NgayChuyenFormat, NgayHetHanFormat, ChucVu, NoiLamViec));
            }
            return builder.ToString();
        }

        private void CheckUpdatePos(DataRow row, UserExtend userExtend, string newAreaID, string name, string posName)
        {
            if (row["POS_CODE"].ToString() != userExtend.PosCode)
            {
                if (newAreaID == userExtend.AreaID)
                    notificationService.AddNotificationByID(3, name, posName, userExtend.ID, null);
                else
                    notificationService.AddNotificationByID(4, name, posName, userExtend.ID, null);
            }
        }
    }
}
