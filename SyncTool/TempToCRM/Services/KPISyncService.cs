﻿using Sys.Core;
using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Repository.Source;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Entities;
using TempToCRM.Extensions;
using TempToCRM.Repository;

namespace TempToCRM.Services
{
    public class KPISyncService : BaseSyncService
    {
        // Source Repository
        private ConfigRepository configRepository;
        private KPIResultEmpTempRepository kpiResultEmpTempRepository;
        // Target Repository
        private UserExtendRepository userExtendRepository;
        private KPISyncMappingRepository kpiSyncMappingRepository;
        private KPIRepository kpiRepository;
        private KPIAllocatesRepository kpiAllocatesRepository;
        private KPIActualResultDetailsRepository kpiActualResultDetailsRepository;
        private KPIActualResultDetailsCSTMRepository kpiActualResultDetailsCSTMRepository;

        public Dictionary<string, string> SystemConfig { get; set; }

        public KPISyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            // Source Repository
            configRepository = new ConfigRepository(dbConnection.SourceConnectionString);
            kpiResultEmpTempRepository = new KPIResultEmpTempRepository(dbConnection.SourceConnectionString);
            // Target Repository
            kpiSyncMappingRepository = new KPISyncMappingRepository(dbConnection.TargetConnectionString);
            userExtendRepository = new UserExtendRepository(dbConnection.TargetConnectionString);
            kpiRepository = new KPIRepository(dbConnection.TargetConnectionString);
            kpiAllocatesRepository = new KPIAllocatesRepository(dbConnection.TargetConnectionString);
            kpiActualResultDetailsRepository = new KPIActualResultDetailsRepository(dbConnection.TargetConnectionString);
            kpiActualResultDetailsCSTMRepository = new KPIActualResultDetailsCSTMRepository(dbConnection.TargetConnectionString);
        }

        public Result<string, Tuple<int, int>> Sync()
        {
            int totalRecord = 0;
            int syncRecord = 0;
            var result = new Result<string, Tuple<int, int>>();
            result.Code = ResultCode.Success;
            result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<KPI> listKPI = new List<KPI>();
            List<KPISyncMapping> listKPIMapping = new List<KPISyncMapping>();
            List<UserExtend> listUserExtend = new List<UserExtend>();
            try
            {
                this.SystemConfig = configRepository.GetAll();
                listKPIMapping = kpiSyncMappingRepository.GetKPIByDate(DateTime.Now);
                if (listKPIMapping == null) return result;
                listKPI = kpiRepository.GetAll();
                listUserExtend = userExtendRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }

            var resultData = GetTempData();
            var dataTable = resultData.ExtensiveInformation;
            if (resultData.Code != ResultCode.Success)
            {
                result.Code = resultData.Code;
                result.Message = resultData.Message;
                return result;
            }

            Dictionary<string, string> listColumnKPICode = listKPIMapping.ToDictionary(); 

            // Insert/Update Actual Result
            int index = 1;
            totalRecord = dataTable.Rows.Count;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                Console.WriteLine("(" + index + "/" + dataTable.Rows.Count + ")");
                index++;

                try
                {
                    // Filter theo User
                    var matchUser = listUserExtend.FirstOrDefault(c => c.UserCode == dataRow["USER_CODE"].ToString());
                    if (matchUser == null) continue;

                    // Gọi Store Procedure bên CRM
                    GenKPIProcedure(matchUser.ID);

                    // Update Actual Result Details
                    foreach (var columnKPICode in listColumnKPICode)
                    {
                        // Tìm KPI có tồn tại không
                        var matchKPI = listKPI.FirstOrDefault(c => c.KPICode == columnKPICode.Value);
                        if (matchKPI == null) continue;

                        // Tìm bản ghi muốn update
                        var exitedDetails = kpiActualResultDetailsRepository.GetByKey(dataRow["YEAR"].ToString(),
                                                dataRow["PERIOD"].ToString().MonthFromString(),
                                                dataRow["USER_CODE"].ToString(),
                                                columnKPICode.Value);
                        if (exitedDetails != null)
                        {
                            exitedDetails.SyncValue = dataRow.IsNull(columnKPICode.Key) ? 0 : Convert.ToDecimal(dataRow[columnKPICode.Key]);
                            exitedDetails.FinalValue = dataRow.IsNull(columnKPICode.Key) ? 0 : Convert.ToDecimal(dataRow[columnKPICode.Key]);
                            exitedDetails.LastestSyncDate = DateTime.Now;
                            kpiActualResultDetailsRepository.Update(exitedDetails);
                        }
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                    return result;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(KPIActualResultDetails)));
                    continue;
                }
            }
            sw.Stop();
            Console.WriteLine("Rows:         {0}", dataTable.Rows.Count);
            Console.WriteLine("Seconds:      {0}", sw.Elapsed.TotalSeconds);
            Console.WriteLine("Speed(r/s):   {0}", dataTable.Rows.Count / sw.Elapsed.TotalSeconds);

            result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);

            var resultCalculate = CalculatePercentage();
            if (resultData.Code != ResultCode.Success)
            {
                result.Code = resultData.Code;
                result.Message = resultData.Message;
                return result;
            }

            // Update giá trị sync lần đầu
            if (Convert.ToBoolean(SystemConfig["FirstTimeKPI"]))
            {
                try
                {
                    configRepository.SetByKey("FirstTimeKPI", "false");
                }
                catch (Exception dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    return result;
                }
            }

            return result;
        }

        // Lấy dữ liệu từ temp
        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = kpiResultEmpTempRepository.GetTableTemp(Convert.ToBoolean(SystemConfig["FirstTimeKPI"]));
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch(Exception)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                return result;
            }
        }

        private Result GenKPIProcedure(string userID)
        {
            Result result = new Result();
            result.Code = ResultCode.Success;

            try
            {
                var dbHelper = new DBHelper_New(dbConnection.TargetConnectionString, DbProviders.SqlServer);
                dbHelper.AddParameter("employeeID", userID);
                dbHelper.ExecuteNonQuery("Gen_KPI_ACTUAL_RESULT", CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                return result;
            }

            return result;
        }

        private Result CalculatePercentage()
        {
            Result result = new Result();
            result.Code = ResultCode.Success;

            try
            {
                var dbHelper = new DBHelper_New(dbConnection.TargetConnectionString, DbProviders.SqlServer);
                dbHelper.AddParameter("iYear", DateTime.Now.Year);
                dbHelper.AddParameter("sMONTH_PERIOD", DateTime.Now.ToString("MM"));
                dbHelper.ExecuteNonQuery("spKPI_EMP_CAL_PERCENTAGE", CommandType.StoredProcedure);

                dbHelper.ClearParameter();
                dbHelper.AddParameter("iYear", DateTime.Now.Year);
                dbHelper.AddParameter("sMONTH_PERIOD", DateTime.Now.ToString("MM"));
                dbHelper.ExecuteNonQuery("spKPI_EMP_CAL_TOTAL_PERCENTAGE", CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                return result;
            }

            return result;
        }

    }
}
