﻿using Sys.Core;
using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Repository.Source;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Entities;
using TempToCRM.Extensions;
using TempToCRM.Repository;

namespace TempToCRM.Services
{
    public class TTSyncService : BaseSyncService
    {
        // Source Repository
        private ConfigRepository configRepository;
        private KPIResultEmpTempRepository kpiResultEmpTempRepository;
        // Target Repository
        private UserExtendRepository userExtendRepository;
        private KPISyncMappingRepository kpiSyncMappingRepository;
        private KPIRepository kpiRepository;
        private TTActualResultRepository ttActualResultRepository;
        private TTActualResultDetailsRepository ttActualResultDetailsRepository;
        private TTCardTempRepository ttCardTempRepository;

        public Dictionary<string, string> SystemConfig { get; set; }

        public TTSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            // Source Repository
            configRepository = new ConfigRepository(dbConnection.SourceConnectionString);
            kpiResultEmpTempRepository = new KPIResultEmpTempRepository(dbConnection.SourceConnectionString);
            // Target Repository
            kpiSyncMappingRepository = new KPISyncMappingRepository(dbConnection.TargetConnectionString);
            userExtendRepository = new UserExtendRepository(dbConnection.TargetConnectionString);
            kpiRepository = new KPIRepository(dbConnection.TargetConnectionString);
            ttActualResultRepository = new TTActualResultRepository(dbConnection.TargetConnectionString);
            ttActualResultDetailsRepository = new TTActualResultDetailsRepository(dbConnection.TargetConnectionString);
            ttCardTempRepository = new TTCardTempRepository(dbConnection.TargetConnectionString);
        }

        public Result<string, Tuple<int, int>> Sync()
        {
            int totalRecord = 0;
            int syncRecord = 0;
            var result = new Result<string, Tuple<int, int>>();
            result.Code = ResultCode.Success;
            result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<KPISyncMapping> listKPIMapping = null;
            List<UserExtend> listUserExtend = null;
            List<TTActualResult> listActualResults = null;
            List<TTActualResultDetails> listActualResultDetails = null;
            List<TTCardTemp> listCardTemp = null;
            try
            {
                this.SystemConfig = configRepository.GetAll();
                listKPIMapping = kpiSyncMappingRepository.GetTTByDate(DateTime.Now);
                if (listKPIMapping == null)
                    return result;
                listUserExtend = userExtendRepository.GetAll();
                // Actual Result
                listActualResults = ttActualResultRepository.GetAll();
                listActualResultDetails = ttActualResultDetailsRepository.GetAll();
                listCardTemp = ttCardTempRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }

            var resultData = GetTempData();
            var dataTable = resultData.ExtensiveInformation;
            if (resultData.Code != ResultCode.Success)
            {
                result.Code = resultData.Code;
                result.Message = resultData.Message;
                return result;
            }

            IEnumerable<KPISyncMapping> listTTMapping = listKPIMapping.Where(c => c.Type == "10");
            IEnumerable<KPISyncMapping> listTTTmpMapping = listKPIMapping.Where(c => c.Type == "15");

            int index = 1;
            totalRecord = dataTable.Rows.Count;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                Console.WriteLine("(" + index + "/" + dataTable.Rows.Count + ")");
                index++;

                try
                {
                    // Filter theo User
                    var matchUser = listUserExtend.FirstOrDefault(c => c.UserCode == dataRow["USER_CODE"].ToString());
                    if (matchUser == null) continue;

                    var existedActualResult = listActualResults.FirstOrDefault(c => c.Year == Convert.ToInt32(dataRow["YEAR"])
                                                    && Convert.ToInt16(c.MonthPeriod) == dataRow["PERIOD"].ToString().MonthFromString()
                                                    && c.UserCode == dataRow["USER_CODE"].ToString());
                    if (existedActualResult == null)
                    {
                        TTActualResult ttActualResult = new TTActualResult();
                        ttActualResult.ID = Guid.NewGuid().ToString();
                        ttActualResult.Year = Convert.ToInt32(dataRow["YEAR"]);
                        ttActualResult.MonthPeriod = dataRow["PERIOD"].ToString().MonthFromString().ToString().PadLeft(2, '0');
                        ttActualResult.EmployeeID = matchUser != null ? matchUser.ID : null;
                        ttActualResult.UserCode = dataRow["USER_CODE"].ToString();
                        ttActualResult.ActualResultCode = ttActualResult.Year + "-" + ttActualResult.MonthPeriod + "-" + ttActualResult.UserCode;
                        ttActualResult.VersionNumber = "1";
                        ttActualResult.LastestSyncDate = DateTime.Now;
                        ttActualResultRepository.Insert(ttActualResult);
                        listActualResults.Add(ttActualResult);
                    }

                    #region Thu thuần
                    // Insert/Update Actual Result Details
                    foreach (var ttMapping in listTTMapping)
                    {
                        // Tìm bản ghi muốn update
                        var exitedDetails = listActualResultDetails.FirstOrDefault(c => c.Year == Convert.ToInt32(dataRow["YEAR"])
                                                                    && Convert.ToInt16(c.MonthPeriod) == dataRow["PERIOD"].ToString().MonthFromString()
                                                                    && c.UserCode == dataRow["USER_CODE"].ToString()
                                                                    && c.KPICode == ttMapping.KPICode);
                        if (exitedDetails != null)
                        {
                            exitedDetails.VersionNumber = (Convert.ToInt32(exitedDetails.VersionNumber) + 1).ToString();
                            exitedDetails.LastestSyncDate = DateTime.Now;
                            exitedDetails.KPIName = ttMapping.Description;
                            exitedDetails.SyncValue = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            exitedDetails.FinalValue = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            ttActualResultDetailsRepository.Update(exitedDetails);
                        }
                        else
                        {
                            TTActualResultDetails ttDetails = new TTActualResultDetails();
                            ttDetails.ID = Guid.NewGuid().ToString();
                            ttDetails.Year = Convert.ToInt32(dataRow["YEAR"]);
                            ttDetails.MonthPeriod = dataRow["PERIOD"].ToString().MonthFromString().ToString().PadLeft(2, '0');
                            ttDetails.EmployeeID = matchUser != null ? matchUser.ID : null;
                            ttDetails.UserCode = dataRow["USER_CODE"].ToString();
                            ttDetails.ActualResultCode = ttDetails.Year + "-" + ttDetails.MonthPeriod + "-" + ttDetails.UserCode;
                            ttDetails.VersionNumber = "1";
                            ttDetails.LastestSyncDate = DateTime.Now;
                            ttDetails.KPICode = ttMapping.KPICode;
                            ttDetails.KPIName = ttMapping.Description;
                            ttDetails.SyncValue = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            ttDetails.FinalValue = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            ttActualResultDetailsRepository.Insert(ttDetails);
                        }
                    }
                    #endregion

                    #region Thu thuần Temp
                    foreach (var ttMapping in listTTTmpMapping)
                    {
                        // Tìm bản ghi muốn update
                        var exitedCardTemp = listCardTemp.FirstOrDefault(c => c.Year == Convert.ToInt32(dataRow["YEAR"])
                                                                    && Convert.ToInt16(c.Period) == dataRow["PERIOD"].ToString().MonthFromString()
                                                                    && c.UserCode == dataRow["USER_CODE"].ToString()
                                                                    && c.PosCode == dataRow["POS_CODE"].ToString()
                                                                    && c.KPICode == ttMapping.KPICode);
                        if (exitedCardTemp != null)
                        {
                            exitedCardTemp.Value = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            ttCardTempRepository.Update(exitedCardTemp);
                        }
                        else
                        {
                            TTCardTemp ttCardTemp = new TTCardTemp();
                            ttCardTemp.ID = Guid.NewGuid().ToString();
                            ttCardTemp.Year = Convert.ToInt32(dataRow["YEAR"]);
                            ttCardTemp.Period = dataRow["PERIOD"].ToString().MonthFromString().ToString().PadLeft(2, '0');
                            ttCardTemp.UserCode = dataRow["USER_CODE"].ToString();
                            ttCardTemp.PosCode = dataRow["POS_CODE"].ToString();
                            ttCardTemp.KPICode = ttMapping.KPICode;
                            ttCardTemp.Value = dataRow.IsNull(ttMapping.SyncColumn) ? 0 : Convert.ToDecimal(dataRow[ttMapping.SyncColumn]);
                            ttCardTempRepository.Insert(ttCardTemp);
                        }
                    }
                    #endregion
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                    return result;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(TTActualResult)));
                    continue;
                }
            }
            sw.Stop();
            Console.WriteLine("Rows:         {0}", dataTable.Rows.Count);
            Console.WriteLine("Seconds:      {0}", sw.Elapsed.TotalSeconds);
            Console.WriteLine("Speed(r/s):   {0}", dataTable.Rows.Count / sw.Elapsed.TotalSeconds);

            // Update giá trị sync lần đầu
            if (Convert.ToBoolean(SystemConfig["FirstTimeTT"]))
            {
                try
                {
                    configRepository.SetByKey("FirstTimeTT", "false");
                }
                catch (Exception dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
                    return result;
                }
            }

            result.ExtensiveInformation = new Tuple<int, int>(totalRecord, syncRecord);
            return result;
        }

        // Lấy dữ liệu từ temp
        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = kpiResultEmpTempRepository.GetTableTemp(Convert.ToBoolean(SystemConfig["FirstTimeTT"]));
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                return result;
            }
        }

    }
}
