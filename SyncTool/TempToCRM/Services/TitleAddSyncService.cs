﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository.Target;
using TempToCRM.Repository.Source;
using Sys.Core.Enums;
using System.Data;
using TempToCRM.Entities.Source;
using Sys.Core;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;

namespace TempToCRM.Services
{
    public class TitleAddSyncService : BaseSyncService
    {
        private TitleAddTmpRepository titleAddTmpRepository;
        private TitleAddRepository titleAddRepository;

        public TitleAddSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            titleAddRepository = new TitleAddRepository(dbConnection.TargetConnectionString);
            titleAddTmpRepository = new TitleAddTmpRepository(dbConnection.SourceConnectionString);
        }

        public void Sync()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            #region Metadata
            var resultData = GetTempData();
            if (resultData.Code != ResultCode.Success)
            {
                resultData.Code = ResultCode.ConnectToDBNotSuccess;
                resultData.Message = resultData.Message;
                messageLogService.AddSyncError(resultData);
                return;
            }

            List<TitleAdd> listTitleAdd = new List<TitleAdd>();
            try
            {
                listTitleAdd = titleAddRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
                return;
            }
            #endregion

            var dataTable = resultData.ExtensiveInformation;
            foreach (DataRow data in dataTable.Rows)
            {
                try
                {
                    TitleAdd existedValue = listTitleAdd.FirstOrDefault(c => c.IDSource == Convert.ToInt32(data["ID_SOURCE"]));
                    if (existedValue == null)
                    {
                        TitleAdd titleAdd = new TitleAdd();
                        MappingTitleAdd(data, ref titleAdd);
                        titleAdd.ID = Guid.NewGuid().ToString();
                        titleAdd.IDSource = Convert.ToInt32(data["ID_SOURCE"]);
                        titleAddRepository.Insert(titleAdd);
                    }
                    else
                    {
                        MappingTitleAdd(data, ref existedValue);
                        titleAddRepository.Update(existedValue);
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(TitleAdd)), (int) data["ID_SOURCE"]);
                    continue;
                }
            }
        }

        private void MappingTitleAdd(DataRow row, ref TitleAdd p)
        {
            p.IDSourceEmp = NumberUltility.ToNullable(row["ID_SOURCE_EMP"]);
            p.SoQuyetDinhID = NumberUltility.ToNullable(row["SoQuyetDinhID"]);
            p.NgayBatDau = DateTimeUltility.ToNullable(row["NgayBatDau"]);
            p.NgayKetThuc = DateTimeUltility.ToNullable(row["NgayKetThuc"]);
            p.ChucVuKiemNhiemID = NumberUltility.ToNullable(row["ChucVuKiemNhiemID"]);
            p.GhiChu = row["GhiChu"].ToString();
            p.PhongBanKiemNhiemID = NumberUltility.ToNullable(row["PhongBanKiemNhiemID"]);
            p.SoQuyetDinh = row["SoQuyetDinh"].ToString();
            p.TenChucVuKiemNhiem = row["TenChucVuKiemNhiem"].ToString();
            p.CreatedByID = NumberUltility.ToNullable(row["CreatedByID"]);
            p.CreatedDate = DateTimeUltility.ToNullable(row["CreatedDate"]);
            p.ModifyByID = NumberUltility.ToNullable(row["ModifyByID"]);
            p.ModifyDate = DateTimeUltility.ToNullable(row["ModifyDate"]);
            p.XetDuyet = NumberUltility.ToNullable(row["XetDuyet"]);
            p.NgayQuyetDinh = DateTimeUltility.ToNullable(row["NgayQuyetDinh"]);
        }

        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = titleAddTmpRepository.GetTableTemp();
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

      
    }
}
