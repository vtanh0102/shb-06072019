﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository.Target;
using TempToCRM.Repository.Source;
using Sys.Core.Enums;
using System.Data;
using TempToCRM.Entities.Source;
using Sys.Core;
using TempToCRM.Entities.Target;
using Sys.Core.Utility;
using Sys.Core.Exceptions;
using Sys.Core.Extensions;
using Sys.Core.Models;

namespace TempToCRM.Services
{
    public class TitleSyncService : BaseSyncService
    {
        private TitleTmpRepository titleTmpRepository;
        private TitleRepository titleRepository;

        public TitleSyncService(DBConnection dbConnection)
            : base(dbConnection)
        {
            titleRepository = new TitleRepository(dbConnection.TargetConnectionString);
            titleTmpRepository = new TitleTmpRepository(dbConnection.SourceConnectionString);
        }

        public void Sync()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            #region Metadata
            var resultData = GetTempData();
            if (resultData.Code != ResultCode.Success)
            {
                resultData.Code = ResultCode.ConnectToDBNotSuccess;
                resultData.Message = resultData.Message;
                messageLogService.AddSyncError(resultData);
                return;
            }

            List<Title> listTitle = new List<Title>();
            try
            {
                listTitle = titleRepository.GetAll();
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                messageLogService.AddSyncError(result);
                return;
            }
            #endregion

            var dataTable = resultData.ExtensiveInformation;
            foreach (DataRow data in dataTable.Rows)
            {
                try
                {
                    Title existedValue = listTitle.FirstOrDefault(c => c.IDSource == Convert.ToInt32(data["ID_SOURCE"]));
                    if (existedValue == null)
                    {
                        Title Title = new Title();
                        MappingTitle(data, ref Title);
                        Title.ID = Guid.NewGuid().ToString();
                        Title.IDSource = Convert.ToInt32(data["ID_SOURCE"]);
                        titleRepository.Insert(Title);
                    }
                    else
                    {
                        MappingTitle(data, ref existedValue);
                        titleRepository.Update(existedValue);
                    }
                }
                catch (DBConnectException dbEx)
                {
                    result.Code = ResultCode.ConnectToDBNotSuccess;
                    result.Message = dbEx.Message;
                    messageLogService.AddSyncError(result);
                    return;
                }
                catch (Exception ex)
                {
                    var resultMapping = new Result<string>();
                    resultMapping.Code = ResultCode.Fail;
                    resultMapping.Message = ex.ToString();
                    messageLogService.AddSyncError(resultMapping, EntityExtensions.GetTableName(typeof(Title)), (int) data["ID_SOURCE"]);
                    continue;
                }
            }
        }

        private void MappingTitle(DataRow dr, ref Title p)
        {
            if (string.IsNullOrWhiteSpace(dr["TITLE_NAME"].ToString()))
                throw new Exception("TenChucVu bị trống");
            if (string.IsNullOrWhiteSpace(dr["TITLE_CODE"].ToString()))
                throw new Exception("MaChucVu bị trống");

            p.TitleCode = dr["TITLE_CODE"].ToString();
            p.TitleName = dr["TITLE_NAME"].ToString();
            p.PosID = NumberUltility.ToNullable(dr["POS_ID"]);
            p.DirectPost = dr["DIRECT_REPORT"].ToString();
            p.IndirectPost = dr["INDIRECT_REPORT"].ToString();
            p.DirectBoss = dr["DIRECT_BOSS"].ToString();
            p.IndirectBoss = dr["INDIRECT_BOSS"].ToString();
            p.PositionID = NumberUltility.ToNullable(dr["POSITION_ID"]);
            p.IndirectBossID = NumberUltility.ToNullable(dr["INDIRECT_BOSS_ID"]);
            p.ParentTitleID = NumberUltility.ToNullable(dr["PARENT_TITLE_ID"]);
            p.DateModified = DateTimeUltility.ToNullable(dr["DATE_MODIFIED"]);
        }

        override protected Result<string, DataTable> GetTempData()
        {
            var result = new Result<string, DataTable>();
            try
            {
                var tempTable = titleTmpRepository.GetTableTemp();
                result.ExtensiveInformation = tempTable;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }
        }

      
    }
}
