﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;

namespace TempToCRM.Extensions
{
    public static class KPISyncMappingExtensions
    {
        /// <summary>
        /// Convert thành Dictionary chứa Column | KPICode
        /// </summary>
        /// <param name="listMapping"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ToDictionary(this List<KPISyncMapping> listMapping)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            listMapping.ForEach(c => result.Add(c.SyncColumn, c.KPICode));
            return result;
        }
    }
}
