USE [SHBBanking_CRM]
GO
/****** Object:  Table [dbo].[B_EMPLOYMENT_HISTORY_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[B_EMPLOYMENT_HISTORY_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[CongTyID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NhanVienID] [int] NULL,
	[PhongBanCuID] [int] NULL,
	[PhongBanMoiID] [int] NULL,
	[NgayChuyen] [datetime] NULL,
	[TenPhongBanCu] [nvarchar](2500) NULL,
	[TenPhongBanMoi] [nvarchar](2500) NULL,
	[NgoaiCongTy] [bit] NULL,
	[LyDoChuyen] [nvarchar](1000) NULL,
	[XetDuyet] [int] NULL,
	[ChucVuCuID] [int] NULL,
	[TenChucVuMoi] [nvarchar](2500) NULL,
	[ChucVuMoiID] [int] NULL,
	[TenChucVuCu] [nvarchar](2500) NULL,
	[NghiViec] [bit] NOT NULL,
	[SoQuyetDinh] [nvarchar](500) NULL,
	[DonViDi] [nvarchar](2500) NULL,
	[DonViDen] [nvarchar](2500) NULL,
	[ChucVuOldID] [int] NULL,
	[TenChucVuNew] [nvarchar](2500) NULL,
	[ChucVuNewID] [int] NULL,
	[TenChucVuOLD] [nvarchar](2500) NULL,
	[ThoiHanThuThach] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ChucDanhCuID] [int] NULL,
	[ChucDanhMoiID] [int] NULL,
	[TenChucDanhCu] [nvarchar](500) NULL,
	[TenChucDanhMoi] [nvarchar](500) NULL,
	[LyDoChuyenID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[NgayHieuLuc] [datetime] NULL,
	[Type] [int] NULL,
	[TrangThaiCapNhat] [int] NULL,
 CONSTRAINT [PK_B_EMPLOYMENT_HISTORY_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[B_KPI_RESULT_EMP_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[B_KPI_RESULT_EMP_TMP](
	[USER_CODE] [nvarchar](10) NOT NULL,
	[POS_CODE] [nvarchar](6) NOT NULL,
	[YEAR] [nvarchar](4) NOT NULL,
	[PERIOD] [nvarchar](3) NOT NULL,
	[DATE_SYNC] [datetime] NOT NULL,
	[VALUE_1] [float] NULL,
	[VALUE_2] [float] NULL,
	[VALUE_3] [float] NULL,
	[VALUE_4] [float] NULL,
	[VALUE_5] [float] NULL,
	[VALUE_6] [float] NULL,
	[VALUE_7] [float] NULL,
	[VALUE_8] [float] NULL,
	[VALUE_9] [float] NULL,
	[VALUE_10] [float] NULL,
	[VALUE_11] [float] NULL,
	[VALUE_12] [float] NULL,
	[VALUE_13] [float] NULL,
	[VALUE_14] [float] NULL,
	[VALUE_15] [float] NULL,
	[VALUE_16] [float] NULL,
	[VALUE_17] [float] NULL,
	[VALUE_18] [float] NULL,
	[VALUE_19] [float] NULL,
	[VALUE_20] [float] NULL,
	[VALUE_21] [float] NULL,
	[VALUE_22] [float] NULL,
	[VALUE_23] [float] NULL,
	[VALUE_24] [float] NULL,
	[VALUE_25] [float] NULL,
	[VALUE_26] [float] NULL,
	[VALUE_27] [float] NULL,
	[VALUE_28] [float] NULL,
	[VALUE_29] [float] NULL,
	[VALUE_30] [float] NULL,
	[VALUE_31] [float] NULL,
	[VALUE_32] [float] NULL,
	[VALUE_33] [float] NULL,
	[VALUE_34] [float] NULL,
	[VALUE_35] [float] NULL,
	[VALUE_36] [float] NULL,
	[VALUE_37] [float] NULL,
	[VALUE_38] [float] NULL,
	[VALUE_39] [float] NULL,
	[VALUE_40] [float] NULL,
	[VALUE_41] [float] NULL,
	[VALUE_42] [float] NULL,
	[VALUE_43] [float] NULL,
	[VALUE_44] [float] NULL,
	[VALUE_45] [float] NULL,
	[VALUE_46] [float] NULL,
	[VALUE_47] [float] NULL,
	[VALUE_48] [float] NULL,
	[VALUE_49] [float] NULL,
	[VALUE_50] [float] NULL,
	[VALUE_51] [float] NULL,
	[VALUE_52] [float] NULL,
	[VALUE_53] [float] NULL,
	[VALUE_54] [float] NULL,
	[VALUE_55] [float] NULL,
	[VALUE_56] [float] NULL,
	[VALUE_57] [float] NULL,
	[VALUE_58] [float] NULL,
	[VALUE_59] [float] NULL,
	[VALUE_60] [float] NULL,
	[VALUE_61] [float] NULL,
	[VALUE_62] [float] NULL,
	[VALUE_63] [float] NULL,
	[VALUE_64] [float] NULL,
	[VALUE_65] [float] NULL,
	[VALUE_66] [float] NULL,
	[VALUE_67] [float] NULL,
	[VALUE_68] [float] NULL,
	[VALUE_69] [float] NULL,
	[VALUE_70] [float] NULL,
	[VALUE_71] [float] NULL,
	[VALUE_72] [float] NULL,
	[VALUE_73] [float] NULL,
	[VALUE_74] [float] NULL,
	[VALUE_75] [float] NULL,
	[VALUE_76] [float] NULL,
	[VALUE_77] [float] NULL,
	[VALUE_78] [float] NULL,
	[VALUE_79] [float] NULL,
	[VALUE_80] [float] NULL,
	[VALUE_81] [float] NULL,
	[VALUE_82] [float] NULL,
	[VALUE_83] [float] NULL,
	[VALUE_84] [float] NULL,
	[VALUE_85] [float] NULL,
	[VALUE_86] [float] NULL,
	[VALUE_87] [float] NULL,
	[VALUE_88] [float] NULL,
	[VALUE_89] [float] NULL,
	[VALUE_90] [float] NULL,
	[VALUE_91] [float] NULL,
	[VALUE_92] [float] NULL,
	[VALUE_93] [float] NULL,
	[VALUE_94] [float] NULL,
	[VALUE_95] [float] NULL,
	[VALUE_96] [float] NULL,
	[VALUE_97] [float] NULL,
	[VALUE_98] [float] NULL,
	[VALUE_99] [float] NULL,
	[VALUE_100] [float] NULL,
 CONSTRAINT [PK_B_KPI_RESULT_EMP_TMP] PRIMARY KEY CLUSTERED 
(
	[USER_CODE] ASC,
	[POS_CODE] ASC,
	[YEAR] ASC,
	[PERIOD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[B_PERFOMANCE_RATING_EMP_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[B_PERFOMANCE_RATING_EMP_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[ID_SOURCE_EMP] [int] NULL,
	[PERIOD] [datetime] NULL,
	[PERIOD_NAME] [nvarchar](200) NULL,
	[RATE] [nvarchar](100) NULL,
	[PERIOD_NUMBER] [nvarchar](10) NULL,
 CONSTRAINT [PK_B_PERFOMANCE_RATING_EMP_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[B_SALARY_STATUS_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[B_SALARY_STATUS_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[NAME] [nvarchar](3000) NULL,
 CONSTRAINT [PK_B_SALARY_STATUS_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[B_TITLE_ADD_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[B_TITLE_ADD_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[ID_SOURCE_EMP] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[ChucVuKiemNhiemID] [int] NULL,
	[GhiChu] [nvarchar](2000) NULL,
	[PhongBanKiemNhiemID] [int] NULL,
	[SoQuyetDinh] [nvarchar](100) NULL,
	[TenChucVuKiemNhiem] [nvarchar](1000) NULL,
	[SoCPKiemNhiem] [int] NULL,
	[ThuLaoKiemNhiem] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[NgayQuyetDinh] [datetime] NULL,
 CONSTRAINT [PK_B_TITLE_ADD_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_CONTRACT_TYPE_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_CONTRACT_TYPE_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[CONTRACT_CODE] [nvarchar](50) NULL,
	[CONTRACT_NAME] [nvarchar](500) NULL,
	[DATE_MODIFIED] [datetime] NULL,
 CONSTRAINT [PK_M_CONTRACT_TYPE_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_POS_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_POS_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[POS_CODE] [nvarchar](50) NULL,
	[POS_NAME] [nvarchar](100) NULL,
	[DESCRIPTION] [nvarchar](500) NULL,
	[PARENT_POS_ID] [int] NULL,
	[DELETED] [bit] NULL,
	[AREA] [nvarchar](50) NULL,
	[POS_CODE1] [nvarchar](50) NULL,
 CONSTRAINT [PK_M_POS_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_POS_TMP_1]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_POS_TMP_1](
	[ID_SOURCE] [int] NOT NULL,
	[POS_CODE] [nvarchar](50) NULL,
	[POS_NAME] [nvarchar](100) NULL,
	[DESCRIPTION] [nvarchar](500) NULL,
	[PARENT_POS_ID] [int] NULL,
	[DELETED] [bit] NULL,
	[AREA] [nvarchar](50) NULL,
 CONSTRAINT [PK_M_POS_TMP_1] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_POSITION_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_POSITION_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[POSITION_CODE] [nvarchar](550) NULL,
	[POSITION_NAME] [nvarchar](520) NULL,
	[DATE_MODIFIED] [datetime] NULL,
 CONSTRAINT [PK_M_POSITION_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_SYS_COLUMN_REPLACE]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_SYS_COLUMN_REPLACE](
	[ID] [nvarchar](10) NOT NULL,
	[TABLE_MAPPING_ID] [nvarchar](10) NOT NULL,
	[MAP_COLUMN] [nvarchar](100) NOT NULL,
	[MATCH] [nvarchar](4000) NULL,
	[REPLACE] [nvarchar](4000) NULL,
	[IS_REGEX] [bit] NOT NULL,
 CONSTRAINT [PK_M_SYS_COLUMN_REPLACE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_SYS_DB_MAPPING]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_SYS_DB_MAPPING](
	[ID] [varchar](10) NOT NULL,
	[SOURCE_CONNECTION_STRING] [nvarchar](1000) NOT NULL,
	[TARGET_CONNECTION_STRING] [nvarchar](1000) NOT NULL,
	[SOURCE_DB_TYPE] [int] NOT NULL,
	[TARGET_DB_TYPE] [int] NOT NULL,
 CONSTRAINT [PK_M_SYS_DB_MAPPING] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_SYS_QUERY_MAPPING]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_SYS_QUERY_MAPPING](
	[ID] [nvarchar](10) NOT NULL,
	[TABLE_MAPPING_ID] [nvarchar](10) NOT NULL,
	[DESCRIPTION] [nvarchar](4000) NULL,
	[SELECT_QUERY] [nvarchar](4000) NOT NULL,
	[JOIN_QUERY] [nvarchar](4000) NULL,
	[FILTER_QUERY] [nvarchar](4000) NULL,
 CONSTRAINT [PK_M_SYS_QUERY_MAPPING] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_SYS_TABLE_MAPPING]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_SYS_TABLE_MAPPING](
	[ID] [nvarchar](10) NOT NULL,
	[DB_MAPPING_ID] [nvarchar](10) NOT NULL,
	[SOURCE_TABLE] [nvarchar](1000) NOT NULL,
	[SOURCE_TABLE_KEY] [nvarchar](1000) NOT NULL,
	[SOURCE_TABLE_COLUMNS] [nvarchar](4000) NULL,
	[TARGET_TABLE] [nvarchar](1000) NOT NULL,
	[MAP_KEY_SOURCE_TO_TARGET] [nvarchar](1000) NOT NULL,
	[MAP_COLUMN_SOURCE] [nvarchar](4000) NULL,
	[MAP_COLUMN_TARGET] [nvarchar](4000) NULL,
	[FILTER_JOIN_QUERY] [nvarchar](4000) NULL,
	[FILTER_WHERE_QUERY] [nvarchar](4000) NULL,
 CONSTRAINT [PK_M_SYS_TABLE_MAPPING] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_TITLE_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_TITLE_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[TITLE_NAME] [nvarchar](520) NULL,
	[TITLE_CODE] [nvarchar](550) NULL,
	[POS_ID] [int] NULL,
	[DIRECT_REPORT] [nvarchar](50) NULL,
	[INDIRECT_REPORT] [nvarchar](50) NULL,
	[DIRECT_BOSS] [nvarchar](50) NULL,
	[INDIRECT_BOSS] [nvarchar](50) NULL,
	[POSITION_ID] [int] NULL,
	[INDIRECT_BOSS_ID] [int] NULL,
	[DATE_MODIFIED] [datetime] NULL,
	[PARENT_TITLE_ID] [int] NULL,
 CONSTRAINT [PK_M_TITLE_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[M_USERS_LOGS]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_USERS_LOGS](
	[ID_SOURCE] [int] NOT NULL,
	[USER_CODE] [varchar](50) NULL,
	[LAST_NAME] [nvarchar](50) NULL,
	[FIRST_NAME] [nvarchar](50) NOT NULL,
	[DELETED] [bit] NULL,
	[DATE_ENTERED] [datetime] NULL,
	[DATE_MODIFIED] [datetime] NULL,
	[PHONE_HOME] [nvarchar](100) NULL,
	[PHONE_MOBILE] [nvarchar](100) NULL,
	[PHONE_WORK] [nvarchar](100) NULL,
	[PHONE_FAX] [varchar](50) NULL,
	[EMAIL1] [varchar](100) NULL,
	[APPROVE_STATUS] [int] NULL,
	[FACEBOOK_ID] [nvarchar](50) NULL,
	[BIRTH_DATE] [datetime] NULL,
	[SEX] [nvarchar](8) NULL,
	[POS_ID] [int] NULL,
	[TITLE_ID] [int] NULL,
	[TITLE_ADD_ID] [int] NULL,
	[RETIRED_DATE] [datetime] NULL,
	[CONTRACT_TYPE_ID] [int] NULL,
	[POSITION_ID] [int] NULL,
	[MANAGER_NAME] [nvarchar](50) NULL,
	[DATE_START_WORK] [datetime] NULL,
	[DATE_START_INTERN] [datetime] NULL,
	[DATE_START_PROBATION] [datetime] NULL,
	[DATE_ACTING_PROMOTED] [datetime] NULL,
	[OFFICAL_PROMOTED_DATE] [datetime] NULL,
	[CURRENT_POSITION_WORK_TIME] [datetime] NULL,
	[SALARY_STATUS] [nvarchar](50) NULL,
	[POS_NAME_CENTER] [nvarchar](100) NULL,
	[AREA] [nvarchar](50) NULL,
	[BUSINESS_TYPE] [nvarchar](10) NULL,
 CONSTRAINT [PK_M_USERS_LOGS] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[M_USERS_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[M_USERS_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[USER_CODE] [varchar](50) NULL,
	[LAST_NAME] [nvarchar](50) NULL,
	[FIRST_NAME] [nvarchar](50) NOT NULL,
	[DELETED] [bit] NULL,
	[DATE_ENTERED] [datetime] NULL,
	[DATE_MODIFIED] [datetime] NULL,
	[PHONE_HOME] [nvarchar](100) NULL,
	[PHONE_MOBILE] [nvarchar](100) NULL,
	[PHONE_WORK] [nvarchar](100) NULL,
	[PHONE_FAX] [varchar](50) NULL,
	[EMAIL1] [varchar](100) NULL,
	[APPROVE_STATUS] [int] NULL,
	[FACEBOOK_ID] [nvarchar](50) NULL,
	[BIRTH_DATE] [datetime] NULL,
	[SEX] [nvarchar](8) NULL,
	[POS_ID] [int] NULL,
	[TITLE_ID] [int] NULL,
	[TITLE_ADD_ID] [int] NULL,
	[RETIRED_DATE] [datetime] NULL,
	[CONTRACT_TYPE_ID] [int] NULL,
	[POSITION_ID] [int] NULL,
	[MANAGER_NAME] [nvarchar](50) NULL,
	[DATE_START_WORK] [datetime] NULL,
	[DATE_START_INTERN] [datetime] NULL,
	[DATE_START_PROBATION] [datetime] NULL,
	[DATE_ACTING_PROMOTED] [datetime] NULL,
	[OFFICAL_PROMOTED_DATE] [datetime] NULL,
	[CURRENT_POSITION_WORK_TIME] [datetime] NULL,
	[SALARY_STATUS] [nvarchar](50) NULL,
	[POS_NAME_CENTER] [nvarchar](100) NULL,
	[AREA] [nvarchar](50) NULL,
	[BUSINESS_TYPE] [nvarchar](10) NULL,
 CONSTRAINT [PK_SYNC_USERS_TEMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NS_DS_LOAI_QUYET_DINH_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DS_LOAI_QUYET_DINH_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiQuyetDinhTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DS_LOAI_QUYET_DINH_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NS_DS_SO_QUYET_DINH_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NS_DS_SO_QUYET_DINH_TMP](
	[ID_SOURCE] [int] NOT NULL,
	[Ten] [nvarchar](500) NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NguoiQuyetDinh] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](100) NULL,
	[TomTatNoiDung] [nvarchar](250) NULL,
	[LoaiQuyetDinhID] [int] NULL,
	[NgayBatDauHieuLuc] [datetime] NULL,
	[NgayHetHieuLuc] [datetime] NULL,
	[NgayTao] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[NgaySua] [datetime] NULL,
	[NguoiSuaID] [int] NULL,
	[TrangThai] [int] NULL,
	[LoaiQuyetDinh] [nvarchar](100) NULL,
	[NguoiQuyetDinhID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenFile] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DS_SO_QUYET_DINH_TMP] PRIMARY KEY CLUSTERED 
(
	[ID_SOURCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NS_QT_CHUYEN_CAN_BO_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QT_CHUYEN_CAN_BO_TMP](
	[QTChuyenCanBoID] [int] NOT NULL,
	[CongTyID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NhanVienID] [int] NULL,
	[PhongBanCuID] [int] NULL,
	[PhongBanMoiID] [int] NULL,
	[NgayChuyen] [datetime] NULL,
	[TenPhongBanCu] [nvarchar](2500) NULL,
	[TenPhongBanMoi] [nvarchar](2500) NULL,
	[NgoaiCongTy] [bit] NULL,
	[LyDoChuyen] [nvarchar](1000) NULL,
	[XetDuyet] [int] NULL,
	[ChucVuCuID] [int] NULL,
	[TenChucVuMoi] [nvarchar](2500) NULL,
	[ChucVuMoiID] [int] NULL,
	[TenChucVuCu] [nvarchar](2500) NULL,
	[NghiViec] [bit] NOT NULL,
	[SoQuyetDinh] [nvarchar](500) NULL,
	[DonViDi] [nvarchar](2500) NULL,
	[DonViDen] [nvarchar](2500) NULL,
	[ChucVuOldID] [int] NULL,
	[TenChucVuNew] [nvarchar](2500) NULL,
	[ChucVuNewID] [int] NULL,
	[TenChucVuOLD] [nvarchar](2500) NULL,
	[ThoiHanThuThach] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ChucDanhCuID] [int] NULL,
	[ChucDanhMoiID] [int] NULL,
	[TenChucDanhCu] [nvarchar](500) NULL,
	[TenChucDanhMoi] [nvarchar](500) NULL,
	[LyDoChuyenID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[NgayHieuLuc] [datetime] NULL,
	[Type] [int] NULL,
	[TrangThaiCapNhat] [int] NULL,
 CONSTRAINT [PK_NS_QT_CHUYEN_CAN_BO_TMP] PRIMARY KEY CLUSTERED 
(
	[QTChuyenCanBoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NS_QT_KINH_NGHIEM_TMP]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QT_KINH_NGHIEM_TMP](
	[KinhNghiemID] [int] NOT NULL,
	[NhanVienID] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[NoiLamViec] [nvarchar](2500) NULL,
	[ChucVu] [nvarchar](2500) NULL,
	[GhiChu] [nvarchar](2500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThiSinhID] [int] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_QT_KINH_NGHIEM_TMP] PRIMARY KEY CLUSTERED 
(
	[KinhNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYNC_ERRORS]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYNC_ERRORS](
	[ID] [int] NOT NULL,
	[SYNC_HIS_ID] [int] NOT NULL,
	[SYNC_FROM] [nvarchar](50) NOT NULL,
	[ERROR_CODE] [int] NOT NULL,
	[ERROR_MESSAGE] [nvarchar](255) NOT NULL,
	[STACK_TRACE] [text] NULL,
	[TABLE_NAME] [varchar](255) NULL,
	[SOURCE_ID] [int] NULL,
 CONSTRAINT [PK_SYNC_ERRORS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYNC_HIS]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYNC_HIS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[USER_ID_FK] [uniqueidentifier] NOT NULL,
	[DATE_START_TMP] [datetime] NOT NULL,
	[DATE_END_TMP] [datetime] NULL,
	[COUNT_TMP] [varchar](50) NULL,
	[STATUS_TMP] [int] NULL,
	[REASON_TMP] [text] NULL,
	[DATE_START_CRM] [datetime] NULL,
	[DATE_END_CRM] [datetime] NULL,
	[COUNT_CRM] [varchar](50) NULL,
	[STATUS_CRM] [int] NULL,
	[REASON_CRM] [text] NULL,
	[TYPE] [int] NULL,
	[IP_SOURCE] [varchar](15) NULL,
	[BROWSER] [varchar](15) NULL,
 CONSTRAINT [PK_B_SYNS_HIS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYNC_NOTIFY_CONFIG]    Script Date: 7/31/2018 1:03:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYNC_NOTIFY_CONFIG](
	[ID] [int] NOT NULL,
	[TableName] [nvarchar](255) NOT NULL,
	[Field] [nvarchar](255) NOT NULL,
	[Module] [nvarchar](255) NOT NULL,
	[Condition] [int] NOT NULL,
	[TypeNotify] [varchar](20) NOT NULL,
 CONSTRAINT [PK_SYNC_NOTIFY_CONFIG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[B_EMPLOYMENT_HISTORY_TMP] ADD  CONSTRAINT [DF_B_EMPLOYMENT_HISTORY_TMP_NghiViec]  DEFAULT ((0)) FOR [NghiViec]
GO
ALTER TABLE [dbo].[NS_QT_CHUYEN_CAN_BO_TMP] ADD  CONSTRAINT [DF_NS_QT_CHUYEN_CAN_BO_TMP_NghiViec]  DEFAULT ((0)) FOR [NghiViec]
GO
