USE [GHR_SHB]
GO
/****** Object:  Table [dbo].[BC_BaoCao]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_BaoCao](
	[BaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenBaoCao] [nvarchar](500) NULL,
	[Data] [ntext] NULL,
	[NhomBaoCaoID] [int] NULL,
	[SQLCommand] [ntext] NULL,
	[SQLCmdGroup] [ntext] NULL,
	[GroupID1] [nvarchar](50) NULL,
	[GroupID2] [nvarchar](50) NULL,
	[GroupID3] [nvarchar](50) NULL,
	[HeaderData] [image] NULL,
	[DetailData] [image] NULL,
	[Type] [int] NULL,
	[MaBaoCao] [nvarchar](50) NULL,
	[DataSetID] [int] NULL,
	[DetailsTableID] [int] NULL,
	[STT] [int] NULL,
	[GridLayoutData] [ntext] NULL,
	[TenTA] [nvarchar](500) NULL,
	[HeaderData_EN] [image] NULL,
	[DetailDAta_EN] [image] NULL,
	[CustomerID] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_NS_BaoCao] PRIMARY KEY CLUSTERED 
(
	[BaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_BaoCao_Parameter]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_BaoCao_Parameter](
	[BaoCaoParameterID] [int] IDENTITY(1,1) NOT NULL,
	[BaoCaoID] [int] NULL,
	[ParameterID] [int] NULL,
 CONSTRAINT [PK_BC_BaoCao_Parameter] PRIMARY KEY CLUSTERED 
(
	[BaoCaoParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_BaoCaoSQL]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_BaoCaoSQL](
	[BaoCaoSQLID] [int] IDENTITY(1,1) NOT NULL,
	[BaoCaoID] [int] NULL,
	[SQLCommand] [ntext] NULL,
	[TableID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_BaoCaoSQL] PRIMARY KEY CLUSTERED 
(
	[BaoCaoSQLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_DataSet]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_DataSet](
	[DataSetID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Mota] [nvarchar](50) NULL,
	[Control] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_DataSet] PRIMARY KEY CLUSTERED 
(
	[DataSetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_Field]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_Field](
	[FieldID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[KieuDuLieu] [nvarchar](50) NULL,
	[TableID] [int] NULL,
	[IsKey] [bit] NULL,
	[Control] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_dsField] PRIMARY KEY CLUSTERED 
(
	[FieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_GridProperty]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_GridProperty](
	[GridPropertyID] [int] IDENTITY(1,1) NOT NULL,
	[Layout] [ntext] NULL,
	[GridName] [nvarchar](50) NULL,
	[GridCode] [nvarchar](50) NULL,
	[GridProperty] [int] NULL,
 CONSTRAINT [PK_BC_GridProperty] PRIMARY KEY CLUSTERED 
(
	[GridPropertyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_NhomBaoCao]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_NhomBaoCao](
	[NhomBaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[ParentID] [int] NULL,
	[MaNhom] [nvarchar](50) NULL,
	[TenTA] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_NS_NhomBaoCao] PRIMARY KEY CLUSTERED 
(
	[NhomBaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_Parameter]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_Parameter](
	[ParameterID] [int] IDENTITY(1,1) NOT NULL,
	[BaoCaoID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ValueField] [nvarchar](50) NULL,
	[NameField] [nvarchar](50) NULL,
	[DataType] [nvarchar](50) NULL,
	[SQLQuery] [nvarchar](500) NULL,
	[AllowNull] [bit] NULL,
	[ParamName] [nvarchar](50) NULL,
	[IsGroupParameter] [bit] NULL,
	[TableName] [nvarchar](50) NULL,
	[sWhereCommand] [nvarchar](500) NULL,
	[ID] [nvarchar](50) NULL,
	[ParentID] [nvarchar](50) NULL,
	[Ten] [nvarchar](50) NULL,
	[NameTA] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_NS_Parameter] PRIMARY KEY CLUSTERED 
(
	[ParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_PopupMenu]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_PopupMenu](
	[PopupMenuID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[Icon] [image] NULL,
	[STT] [int] NULL,
	[IsGroup] [bit] NULL,
	[SQLCommand] [nvarchar](500) NULL,
	[BaoCaoID] [int] NULL,
	[IsChildGrid] [bit] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_PopupMenu] PRIMARY KEY CLUSTERED 
(
	[PopupMenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_Relation]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_Relation](
	[RelationID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[ParentTableID] [int] NULL,
	[ChildTableID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_dsRelation] PRIMARY KEY CLUSTERED 
(
	[RelationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_RelationDetail]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_RelationDetail](
	[RelationDetailID] [int] IDENTITY(1,1) NOT NULL,
	[RelationID] [int] NULL,
	[ParentKeyID] [int] NULL,
	[ChildKeyID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_dsRelationDetail] PRIMARY KEY CLUSTERED 
(
	[RelationDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BC_Table]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BC_Table](
	[TableID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[DataSetID] [int] NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_BC_Table] PRIMARY KEY CLUSTERED 
(
	[TableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chitietluongtam]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chitietluongtam](
	[QTDienBienLuongChiTietID] [int] NOT NULL,
	[LoaiLuongID] [int] NULL,
	[QTDienBienLuongID] [int] NULL,
	[TienLuong] [char](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[GiaTri] [decimal](18, 5) NULL,
	[GiaTriMoi] [decimal](18, 5) NULL,
	[GiaTriStr] [nvarchar](1500) NULL,
	[GiaTriStrMoi] [nvarchar](1500) NULL,
	[NhanVienID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailContent]    Script Date: 6/21/2018 11:30:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailContent](
	[EmailContentID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [ntext] NULL,
	[IsHTMLFormat] [bit] NULL,
	[Subject] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[EmailContentGroupID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_EmailContent] PRIMARY KEY CLUSTERED 
(
	[EmailContentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailContentGroup]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailContentGroup](
	[EmailContentGroupID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_EmailContentGroup] PRIMARY KEY CLUSTERED 
(
	[EmailContentGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailHistory]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailHistory](
	[EmailHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[EmailContentID] [int] NULL,
	[DateSent] [datetime] NULL,
	[Name] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_EmailHistory] PRIMARY KEY CLUSTERED 
(
	[EmailHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailSentFail]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailSentFail](
	[EmailSentFailID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](500) NULL,
	[ContentTxt] [nvarchar](max) NULL,
	[Subject] [nvarchar](4000) NULL,
	[Reason] [nvarchar](4000) NULL,
	[IsHTMLFormat] [bit] NULL,
	[TaskID] [int] NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_EmailSentFail] PRIMARY KEY CLUSTERED 
(
	[EmailSentFailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailSentSuccess]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailSentSuccess](
	[EmailSendSuccessID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[EmailContentID] [int] NULL,
	[DateSent] [datetime] NULL,
	[TaskID] [int] NULL,
 CONSTRAINT [PK_EmailSentSuccess] PRIMARY KEY CLUSTERED 
(
	[EmailSendSuccessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailServerProfile]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailServerProfile](
	[EmailServerProfileID] [int] IDENTITY(1,1) NOT NULL,
	[SMTPServer] [nvarchar](50) NULL,
	[SMTPPort] [int] NULL,
	[SMTPUserName] [nvarchar](50) NULL,
	[SMTPPassword] [nvarchar](50) NULL,
	[ProfileUser] [nvarchar](50) NULL,
	[ProfilePassword] [nvarchar](50) NULL,
	[FromName] [nvarchar](50) NULL,
	[FromEmail] [nvarchar](50) NULL,
	[SMTPEnableSSL] [bit] NULL,
	[Name] [nvarchar](250) NULL,
	[IsMainProfile] [bit] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_ServerProfile] PRIMARY KEY CLUSTERED 
(
	[EmailServerProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTask]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTask](
	[EmailTaskID] [int] IDENTITY(1,1) NOT NULL,
	[EmailTaskName] [nvarchar](4000) NULL,
	[EmailCount] [int] NULL,
	[ContentID] [int] NULL,
	[ContentTxt] [nvarchar](4000) NULL,
	[Email] [nvarchar](500) NULL,
	[EmailQuery] [nvarchar](4000) NULL,
	[EmailProfileID] [int] NULL,
	[ParameterQuery] [nvarchar](4000) NULL,
	[Subject] [nvarchar](4000) NULL,
	[Status] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_EmailTask] PRIMARY KEY CLUSTERED 
(
	[EmailTaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailToSend]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailToSend](
	[EmailToSendID] [int] IDENTITY(1,1) NOT NULL,
	[ContentID] [int] NULL,
	[ContentTxt] [nvarchar](4000) NULL,
	[Email] [nvarchar](500) NULL,
	[EmailQuery] [nvarchar](4000) NULL,
	[EmailProfileID] [int] NULL,
	[ParameterQuery] [nvarchar](4000) NULL,
	[Subject] [nvarchar](4000) NULL,
	[Status] [int] NULL,
	[CustomerID] [int] NULL,
	[CC] [nvarchar](250) NULL,
	[BCC] [nvarchar](250) NULL,
 CONSTRAINT [PK_EmailToSent] PRIMARY KEY CLUSTERED 
(
	[EmailToSendID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_CongViec_TienDo]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_CongViec_TienDo](
	[CongViec_TienDoID] [int] IDENTITY(1,1) NOT NULL,
	[CongViecID] [int] NULL,
	[NgayThang] [datetime] NULL,
	[NoiDung] [nvarchar](500) NULL,
	[NhanVienID] [int] NULL,
	[TongDiem] [int] NULL,
 CONSTRAINT [PK_KPI_CongViec_TienDo] PRIMARY KEY CLUSTERED 
(
	[CongViec_TienDoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_DanhGiaCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_DanhGiaCongViec](
	[DanhGiaCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[MoTaCongViecID] [int] NULL,
	[NguoiDanhGiaID] [int] NULL,
	[DiemSo] [decimal](10, 2) NULL,
	[NhanVienID] [int] NULL,
	[NgayDanhGia] [datetime] NULL,
	[NhomDanhGiaID] [int] NULL,
 CONSTRAINT [PK_KPI_DanhGiaCongViec] PRIMARY KEY CLUSTERED 
(
	[DanhGiaCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_DuAn]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_DuAn](
	[DuAnID] [int] IDENTITY(1,1) NOT NULL,
	[ThoiGianBatDau] [datetime] NULL,
	[ThoiGianKetThuc] [datetime] NULL,
	[ThoiGianDuKienKetThuc] [datetime] NULL,
	[TongThuong] [money] NULL,
	[KhachHangID] [int] NULL,
	[TongDiem] [int] NULL,
	[DuAnChaID] [int] NULL,
	[MoTaDuAn] [nvarchar](500) NULL,
	[TenDuAn] [nvarchar](50) NULL,
	[TongDiemCanDat] [int] NULL,
	[NgayTao] [datetime] NULL,
	[NgaySua] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[NguoiSuaID] [int] NULL,
 CONSTRAINT [PK_KPI_DuAn] PRIMARY KEY CLUSTERED 
(
	[DuAnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_DuAn_NhomCongViec_NhanVien]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_DuAn_NhomCongViec_NhanVien](
	[DuAn_NhomCongViec_NhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[DuAnID] [int] NULL,
	[NhomCongViecID] [int] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_KPI_DuAn_NhomCongViec] PRIMARY KEY CLUSTERED 
(
	[DuAn_NhomCongViec_NhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_KetQua]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_KetQua](
	[KetQuaID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[DiemSo] [int] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_KPI_KetQua] PRIMARY KEY CLUSTERED 
(
	[KetQuaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_KetQuaCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_KetQuaCongViec](
	[KetQuaCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[Date] [datetime] NULL,
	[LoaiDanhGiaID] [int] NULL,
	[MoTaCongViecID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NguoiDanhGiaID] [int] NULL,
	[KinhNghiemLamViecID] [int] NULL,
	[DiemSo] [decimal](10, 2) NULL,
	[ThoiDiemTinhKQ] [datetime] NULL,
 CONSTRAINT [PK_KPI_KetQuaCongViec] PRIMARY KEY CLUSTERED 
(
	[KetQuaCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_KinhNghiemLamViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_KinhNghiemLamViec](
	[KinhNghiemLamViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenKinhNghiemLamViec] [nvarchar](100) NULL,
	[ChiTiet] [nvarchar](1000) NULL,
 CONSTRAINT [PK_KPI_KinhNghiemLamViec] PRIMARY KEY CLUSTERED 
(
	[KinhNghiemLamViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_LoaiDanhGia]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_LoaiDanhGia](
	[LoaiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[DiemSo] [int] NULL,
	[Type] [int] NULL,
	[Index] [int] NULL,
	[Min] [decimal](10, 2) NULL,
	[Max] [decimal](10, 2) NULL,
	[ThuTu] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_LoaiNhomCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_LoaiNhomCongViec](
	[LoaiNhomCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[DanhGiaTheoDiemSo] [bit] NULL,
	[KyDanhGia] [int] NULL,
 CONSTRAINT [PK_KPI_LoaiNhomCongViec] PRIMARY KEY CLUSTERED 
(
	[LoaiNhomCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_MoTacCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_MoTacCongViec](
	[MoTaCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Tyle] [decimal](10, 2) NULL,
	[NhomCongViecID] [int] NULL,
	[TongDiemCanDat] [decimal](10, 2) NULL,
	[Mota] [nvarchar](2000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_MoTaCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_MoTaCongViec](
	[MoTaCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](2000) NULL,
	[Tyle] [decimal](10, 2) NULL,
	[CongViecChaID] [int] NULL,
	[TongDiemCanDat] [decimal](10, 2) NULL,
	[Mota] [nvarchar](2000) NULL,
	[NhomCongViecID] [int] NULL,
 CONSTRAINT [PK_KPI_MoTacCongViec] PRIMARY KEY CLUSTERED 
(
	[MoTaCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_NguoiDanhGia_HeSo]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_NguoiDanhGia_HeSo](
	[HeSoDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDanhGiaID] [int] NULL,
	[HeSo] [decimal](10, 2) NULL,
 CONSTRAINT [PK_KPI_NguoiDanhGia_HeSo_1] PRIMARY KEY CLUSTERED 
(
	[HeSoDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_NhanVien_CongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_NhanVien_CongViec](
	[NhanVien_LoaiNhomCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CongViecID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_NhanVien_LoaiNhomCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_NhanVien_LoaiNhomCongViec](
	[NhanVien_LoaiNhomCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiNhomCongViecID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_NhomCongViec]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_NhomCongViec](
	[NhomCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[LoaiNhomCongViecID] [int] NULL,
	[TyLe] [decimal](10, 2) NULL,
	[NhomChaID] [int] NULL,
	[MoTaCongViec] [nvarchar](500) NULL,
	[TongDiemCanDat] [decimal](10, 2) NULL,
	[TheoDuAn] [bit] NULL,
 CONSTRAINT [PK_KPI_NhomCongViec] PRIMARY KEY CLUSTERED 
(
	[NhomCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_NhomDanhGia]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_NhomDanhGia](
	[NhomDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[HeSo] [decimal](10, 2) NULL,
	[Ten] [nvarchar](50) NULL,
 CONSTRAINT [PK_KPI_NguoiDanhGia_HeSo] PRIMARY KEY CLUSTERED 
(
	[NhomDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_PhongBan_DanhGia]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_PhongBan_DanhGia](
	[PhongBan_DanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[LoaiDanhGiaID] [int] NULL,
	[TieuChuanDanhGiaID] [int] NULL,
	[TyLe] [decimal](10, 2) NULL,
 CONSTRAINT [PK_KPI_PhongBan_DanhGia] PRIMARY KEY CLUSTERED 
(
	[PhongBan_DanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KPI_TongHopKetQua]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KPI_TongHopKetQua](
	[TongHopKetQuaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NhomCongViecID] [int] NULL,
	[LoaiDanhGiaID] [int] NULL,
	[LoaiNhomCongViecID] [int] NULL,
	[TongDiem] [decimal](10, 2) NULL,
	[TyLe] [decimal](10, 2) NULL,
	[NgayCapNhat] [datetime] NULL,
	[NhomDanhGiaID] [int] NULL,
 CONSTRAINT [PK_KPI_TongHopKetQua] PRIMARY KEY CLUSTERED 
(
	[TongHopKetQuaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MS_Config]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MS_Config](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Value] [nvarchar](50) NULL,
 CONSTRAINT [PK_MS_Config] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MSG_MessageSent]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MSG_MessageSent](
	[MessageSentID] [int] IDENTITY(1,1) NOT NULL,
	[PendingMessageID] [int] NULL,
	[DateSent] [datetime] NULL,
 CONSTRAINT [PK_NS_MSG_MessageSent] PRIMARY KEY CLUSTERED 
(
	[MessageSentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhanSu_Follow]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanSu_Follow](
	[NhanSuFollowID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NguoiDungID] [int] NULL,
	[CreatedByID] [int] NULL,
	[ModifyByID] [int] NULL,
 CONSTRAINT [PK_NhanSu_Follow] PRIMARY KEY CLUSTERED 
(
	[NhanSuFollowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[NhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[CaLVID] [int] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[QuocTichID] [int] NULL,
	[DotTuyenDungID] [int] NULL,
	[NganhHocID] [int] NULL,
	[LoaiLaoDongID] [int] NULL,
	[TinhThanhID] [int] NULL,
	[QuanHuyenID] [int] NULL,
	[TrinhDoID] [int] NULL,
	[TrinhDoTinHocID] [int] NULL,
	[HocHamID] [int] NULL,
	[HocViID] [int] NULL,
	[NhanVienPhuTrachID] [int] NULL,
	[TrangThaiID] [int] NULL,
	[TonGiaoID] [int] NULL,
	[DanTocID] [int] NULL,
	[TrinhDoNgoaiNguID] [int] NULL,
	[NghachCongChucID] [int] NULL,
	[ThamNienID] [int] NULL,
	[ThiSinhID] [int] NULL,
	[LoaiNhomCongViecID] [int] NULL,
	[MaNhanVien] [varchar](50) NULL,
	[Ho] [nvarchar](50) NULL,
	[HoTen] [nvarchar](50) NOT NULL,
	[DiaChi] [nvarchar](2000) NULL,
	[DienThoai] [nvarchar](100) NULL,
	[NgayHetHanHopDong] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CMTND] [nvarchar](50) NULL,
	[NgayBatDauLam] [datetime] NULL,
	[TenThuongDung] [nvarchar](50) NULL,
	[QueQuan] [nvarchar](350) NULL,
	[DiaChiThuongTru] [nvarchar](2000) NULL,
	[NoiCapCMT] [nvarchar](500) NULL,
	[NgayCapCMT] [datetime] NULL,
	[NgaySinh] [datetime] NULL,
	[GioiTinh] [nvarchar](8) NULL,
	[DienThoaiNha] [nvarchar](100) NULL,
	[DienThoaiCoQuan] [nvarchar](100) NULL,
	[Email] [varchar](100) NULL,
	[Fax] [varchar](50) NULL,
	[TinhTrangHonNhan] [nvarchar](255) NULL,
	[ThanhPhanGiaDinh] [nvarchar](255) NULL,
	[Anh] [image] NULL,
	[SoHopDong] [varchar](100) NULL,
	[SoBHXH] [varchar](30) NULL,
	[SoBHYT] [varchar](30) NULL,
	[MaBaoHiem] [varchar](30) NULL,
	[TaiKhoanNganHang] [varchar](250) NULL,
	[TenNganHang] [nvarchar](250) NULL,
	[DiaChiNganHang] [nvarchar](250) NULL,
	[NgayNghiViec] [datetime] NULL,
	[HinhThucTuyenDung] [nvarchar](100) NULL,
	[TinhTrangSucKhoe] [nvarchar](50) NULL,
	[NhomMau] [nvarchar](10) NULL,
	[ChieuCao] [nvarchar](50) NULL,
	[CanNang] [nvarchar](50) NULL,
	[MaChucDanh] [nvarchar](100) NULL,
	[HeSoLuong] [int] NULL,
	[ThoiHanTuyen] [int] NULL,
	[NhanDang] [nvarchar](250) NULL,
	[NguoiUpdate] [nvarchar](300) NULL,
	[NgayUpdate] [datetime] NULL,
	[MaTheChamCong] [nvarchar](50) NULL,
	[NgayNghiDuocPhep] [decimal](18, 2) NULL,
	[SoNgayNghiPhep] [decimal](18, 2) NULL,
	[SoNgayNghiPhepBanDau] [int] NULL,
	[NghiViec] [bit] NULL,
	[NgayKyHopDongLD] [datetime] NULL,
	[NoiSinh] [nvarchar](255) NULL,
	[NgayTinhLuongCoBan] [datetime] NULL,
	[LoaiHopDongID] [int] NULL,
	[ChucVuKiemNhiemID] [int] NULL,
	[ChucVuKiemNhiem] [nvarchar](50) NULL,
	[LoaiChucNang] [nvarchar](50) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[BacLuong] [nvarchar](10) NULL,
	[ChucDanhID] [int] NULL,
	[MaNVCu] [nvarchar](50) NULL,
	[GhiChu_TrinhDoChuyenMon] [nvarchar](250) NULL,
	[CongTyID] [int] NULL,
	[MoiQuanHe] [nvarchar](50) NULL,
	[IsCongDoanPhi] [bit] NULL,
	[TruongDaoTaoID] [int] NULL,
	[HeDaoTaoID] [int] NULL,
	[ChungChi] [nvarchar](300) NULL,
	[TrinhDoNgoaiNgu] [nvarchar](50) NULL,
	[XepLoaiNgoaiNgu] [nvarchar](50) NULL,
	[TrinhDoTinHoc] [nvarchar](50) NULL,
	[XepLoaiTinHoc] [nvarchar](50) NULL,
	[SoHoChieu] [nvarchar](50) NULL,
	[NgayHetHanHoChieu] [datetime] NULL,
	[ChuyenNganhID] [int] NULL,
	[ThamNien] [decimal](5, 1) NULL,
	[NgayCong] [int] NULL,
	[SoCongChuc] [varchar](30) NULL,
	[DangThuViec] [bit] NULL,
	[NoiDKKCBBD] [nvarchar](255) NULL,
	[NhomTinhLuongID] [int] NULL,
	[LuongCoBan] [int] NULL,
	[PhuCapPhucLoi] [int] NULL,
	[PhuCapThuHut] [int] NULL,
	[PhuCapTruc] [int] NULL,
	[PhuCapDiaBan] [int] NULL,
	[IsBHYT] [bit] NULL,
	[IsBHXH] [bit] NULL,
	[IsCDPhi] [bit] NULL,
	[IsTCMV] [bit] NULL,
	[LuongBHXH] [decimal](18, 0) NULL,
	[IsDeNghiCapSoBH] [bit] NULL,
	[IsCapSoBaoHiem] [bit] NULL,
	[NgayCapSoBH] [datetime] NULL,
	[NgayDeNghiCapSo] [datetime] NULL,
	[PhuCapKhac] [int] NULL,
	[PhuCapChucVu] [int] NULL,
	[PhuCapDocHai] [int] NULL,
	[PhuCapTrachNhiem] [int] NULL,
	[TroCap] [int] NULL,
	[PhuCapDienThoai] [int] NULL,
	[PhuCapAnCa] [int] NULL,
	[PhuCapCongTacPhi] [int] NULL,
	[SoPhepChuyenTuNamTruoc] [decimal](18, 2) NULL,
	[SoPhepBanDau] [decimal](18, 3) NULL,
	[SoPhepTheoQuyDinh] [decimal](18, 3) NULL,
	[SoPhepDaNghi] [decimal](18, 3) NULL,
	[SoPhepConLai] [decimal](18, 3) NULL,
	[NguoiPhuTrach] [nvarchar](50) NULL,
	[TrinhDoNgoaiNgu2ID] [int] NULL,
	[NamTotNghiep] [int] NULL,
	[MayChamCongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[DonvilamviecID] [int] NULL,
	[NhomphongbanID] [int] NULL,
	[NgayHocViec] [datetime] NULL,
	[NgayThuViec] [datetime] NULL,
	[NgayBoNhiemThuThach] [datetime] NULL,
	[NgayGiaHanThuThach] [datetime] NULL,
	[NgayHetHanHocViec] [datetime] NULL,
	[NgayHetHanThuViec] [datetime] NULL,
	[NgayHetHanThuThach] [datetime] NULL,
	[NgayHetHanDieuDong] [datetime] NULL,
	[TrangThaiTinhLuongID] [int] NULL,
	[DiaChiLienHe] [nvarchar](150) NULL,
	[masotraluong] [nvarchar](100) NULL,
	[danhgia] [nvarchar](max) NULL,
	[tknh_backup] [nvarchar](100) NULL,
	[Yahoo] [nvarchar](50) NULL,
	[Facebook] [nvarchar](50) NULL,
	[Skype] [nvarchar](50) NULL,
	[Twitter] [nvarchar](50) NULL,
	[Zalo] [nvarchar](50) NULL,
	[XetDuyet] [int] NULL,
	[IsDeleted] [bit] NULL,
	[Vung] [int] NULL,
	[thamgiabaohiemtaicongty] [nvarchar](100) NULL,
	[SoLuuFileHS] [nvarchar](100) NULL,
	[TinhTrangVayVonNH] [nvarchar](200) NULL,
	[SoLuuFileHSNS] [nvarchar](200) NULL,
	[MaCIF] [nvarchar](500) NULL,
	[NgayVaoHBB] [datetime] NULL,
	[NgayVaoThuViecHBB] [datetime] NULL,
	[NgayVaoChinhThucHBB] [datetime] NULL,
	[SoPhepBanDauBackUp] [decimal](18, 2) NULL,
	[NgayChoVayNV] [datetime] NULL,
	[NgayDenHanVayNV] [datetime] NULL,
	[DanhGia_BU] [nvarchar](200) NULL,
	[NgayHetHanViPhamCamKetThaiSan] [datetime] NULL,
	[NgayBatDauHanCheQuyenLoi] [datetime] NULL,
	[GhiChuHanCheQuyenLoi] [nvarchar](200) NULL,
	[TrangThaiUyQuyenQTThue] [nvarchar](50) NULL,
	[MaChiNhanhHachToan] [nvarchar](50) NULL,
	[MaPhongBanHachToan] [nvarchar](50) NULL,
	[NgayBoNhiemChinhThuc] [datetime] NULL,
	[XaPhuongID] [int] NULL,
	[PhepT32016] [decimal](18, 2) NULL,
	[PhepT462016] [decimal](18, 2) NULL,
	[NgayHetHanBoNhiem1] [datetime] NULL,
	[TaiKhoanNganHang_An] [nvarchar](100) NULL,
	[MaSoTraLuong_An] [nvarchar](100) NULL,
	[MaCif_An] [nvarchar](100) NULL,
	[MangNghiepVuID] [int] NULL,
	[NhomCaLamViecID] [int] NULL,
	[BaoCaoTrucTiepID] [int] NULL,
	[BaoCaoGianTiepID] [int] NULL,
	[DiaDiemLamViecNVID] [int] NULL,
	[Keyword] [nvarchar](250) NULL,
	[CapChucDanhID] [int] NULL,
	[NgayCap_HoChieu] [datetime] NULL,
	[NgayHetHan_HoChieu] [datetime] NULL,
	[Visa] [nvarchar](50) NULL,
	[NgayCap_Visa] [datetime] NULL,
	[NgayHetHan_Visa] [datetime] NULL,
	[LoaiThiThucID] [int] NULL,
	[GiayPhepLaoDong] [nvarchar](50) NULL,
	[NgayCap_GiayPhepLaoDong] [datetime] NULL,
	[NgayHetHan_GiayPhepLaoDong] [datetime] NULL,
	[DiaChi_SoNha] [nvarchar](50) NULL,
	[DiaChi_ThonXomID] [int] NULL,
	[DiaChi_XaPhuongID] [int] NULL,
	[DiaChi_QuanHuyenID] [int] NULL,
	[DiaChi_TinhID] [int] NULL,
	[DiaChiTT_SoNha] [nvarchar](50) NULL,
	[DiaChiTT_ThonXomID] [int] NULL,
	[DiaChiTT_XaPhuongID] [int] NULL,
	[DiaChiTT_QuanHuyenID] [int] NULL,
	[DiaChiTT_TinhID] [int] NULL,
	[QueQuan_SoNha] [nvarchar](50) NULL,
	[QueQuan_ThonXomID] [int] NULL,
	[QueQuan_XaPhuongID] [int] NULL,
	[QueQuan_QuanHuyenID] [int] NULL,
	[QueQuan_TinhID] [int] NULL,
	[NoiSinh_SoNha] [nvarchar](50) NULL,
	[NoiSinh_ThonXomID] [int] NULL,
	[NoiSinh_XaPhuongID] [int] NULL,
	[NoiSinh_QuanHuyenID] [int] NULL,
	[NoiSinh_TinhID] [int] NULL,
	[IsChuHoKhau] [bit] NULL,
	[NoiCap_HoChieu] [nvarchar](50) NULL,
	[NoiCap_Visa] [nvarchar](50) NULL,
	[NoiCap_GiayPhepLaoDong] [nvarchar](50) NULL,
 CONSTRAINT [PK_NhanVien1] PRIMARY KEY CLUSTERED 
(
	[NhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BaoCao]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BaoCao](
	[BaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenBaoCao] [nvarchar](50) NULL,
	[NhomBaoCaoID] [int] NULL,
	[Type] [int] NULL,
	[MaBaoCao] [nvarchar](50) NULL,
	[Data] [ntext] NULL,
 CONSTRAINT [PK_NSS_BaoCao] PRIMARY KEY CLUSTERED 
(
	[BaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_BHXHTongHop]    Script Date: 6/21/2018 11:30:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_BHXHTongHop](
	[BHXHTongHopID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiThamGiaID] [int] NULL,
	[MucDongCu] [decimal](10, 0) NULL,
	[MucDongMoi] [decimal](10, 0) NULL,
	[TuThangNam] [nvarchar](50) NULL,
	[ToiThangNam] [nvarchar](50) NULL,
	[TyleDong] [decimal](10, 3) NULL,
	[DaCoSoBHXH] [bit] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[IsTang] [bit] NULL,
	[Nhom] [nvarchar](50) NULL,
	[LoaiNhom] [nvarchar](50) NULL,
	[FromTable] [nvarchar](50) NULL,
	[TableID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NgayLap] [datetime] NULL,
	[IsAutogen] [bit] NULL,
	[MucDongBH_Cu] [decimal](10, 0) NULL,
	[MucDongBH_Moi] [decimal](10, 0) NULL,
	[TangGiam] [int] NULL,
	[IsChinhSua] [bit] NULL,
	[NgayDeNghiCapSo] [datetime] NULL,
	[NgayNghiViec] [datetime] NULL,
	[ThangNamDongBH] [nvarchar](50) NULL,
	[ThangNamNghiViec] [nvarchar](50) NULL,
	[MaTinh] [nvarchar](50) NULL,
	[MaBV] [nvarchar](50) NULL,
	[KeyID] [nvarchar](50) NULL,
	[TenChucVu] [nvarchar](50) NULL,
	[NoiDongBHXHID] [int] NULL,
	[LyDo] [nvarchar](250) NULL,
 CONSTRAINT [PK_NS_BH_BHXHTongHop] PRIMARY KEY CLUSTERED 
(
	[BHXHTongHopID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_ChotSo]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_ChotSo](
	[ChotSoID] [int] IDENTITY(1,1) NOT NULL,
	[BHXH_SL] [int] NULL,
	[BHXH_QL] [decimal](10, 0) NULL,
	[BHXH_PD] [decimal](10, 0) NULL,
	[BHYT_SL] [int] NULL,
	[BHYT_QL] [decimal](10, 0) NULL,
	[BHYT_PD] [decimal](10, 0) NULL,
	[BHTN_SL] [int] NULL,
	[BHTN_QL] [decimal](10, 0) NULL,
	[BHTN_PD] [decimal](10, 0) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[DonViID] [int] NULL,
	[ChotSo] [int] NULL,
	[BHXL_QL] [decimal](10, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_DonViDangKyBHXH]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_DonViDangKyBHXH](
	[DonViDangKyBHXHID] [int] IDENTITY(1,1) NOT NULL,
	[TenDonVi] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](150) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[Logo] [image] NULL,
	[ThuDienTu] [nvarchar](50) NULL,
	[MaDonVi] [nvarchar](50) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[TenTaiKhoan] [nvarchar](150) NULL,
	[TaiNganHang] [nvarchar](50) NULL,
	[SoTaiKhoan] [nvarchar](50) NULL,
	[TaiKhoanNganHang] [nvarchar](50) NULL,
	[Ten] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_BH_DonViDangKyBHXH] PRIMARY KEY CLUSTERED 
(
	[DonViDangKyBHXHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_DSKhaiCapSoBH]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_DSKhaiCapSoBH](
	[ToKhaiID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[MaSoBH] [varchar](50) NULL,
	[CoQuanCapSoBH] [nvarchar](255) NULL,
	[NgayCapSo] [datetime] NULL,
	[NoiDangKyKhamBanDau] [nvarchar](255) NULL,
	[NguoiCoCong] [int] NULL,
	[KhuVucLamViec] [int] NULL,
	[CanBoBHXHThamDinh] [nvarchar](50) NULL,
	[NgayBHXHThamDinh] [datetime] NULL,
	[CoQuanSDLDXacNhan] [nvarchar](255) NULL,
	[NgayCoQuanSDLDXacNhan] [datetime] NULL,
	[SoCapMoi] [bit] NULL,
	[LoaiBH] [int] NULL,
 CONSTRAINT [PK_BH_DSKhaiCapSoBH] PRIMARY KEY CLUSTERED 
(
	[ToKhaiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_DsThamGiaBHSK]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_DsThamGiaBHSK](
	[BHSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgayThamGia] [datetime] NULL,
	[TuNgay] [datetime] NULL,
	[DenNgay] [datetime] NULL,
	[SoBaoHiem] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_BH_DsThamGiaBHSK] PRIMARY KEY CLUSTERED 
(
	[BHSucKhoeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_HuuTri]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_HuuTri](
	[HuuTriID] [int] IDENTITY(1,1) NOT NULL,
	[QuyetDinhSo] [nvarchar](100) NULL,
	[NgayNghi] [datetime] NULL,
	[NguoiThamGiaBHID] [int] NULL,
	[NgayLap] [datetime] NULL,
	[LuongToiThieu] [decimal](18, 0) NULL,
	[LuongBinhQuan] [decimal](18, 0) NULL,
	[TongTienThanhToan] [decimal](18, 0) NULL,
	[MucLuongDongBHXH] [decimal](18, 0) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_BH_HUUTRI] PRIMARY KEY CLUSTERED 
(
	[HuuTriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_LanDeNghi_NhanVien]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_LanDeNghi_NhanVien](
	[LanDeNghiChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[LanDeNghiID] [int] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_BH_LanDeNghi_NhanVien] PRIMARY KEY CLUSTERED 
(
	[LanDeNghiChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_LanDeNghiCapSo]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_LanDeNghiCapSo](
	[LanDeNghiID] [int] IDENTITY(1,1) NOT NULL,
	[TieuDe] [nvarchar](255) NULL,
	[GhiChu] [nvarchar](4000) NULL,
	[NgayLap] [datetime] NULL,
	[NguoiLapID] [int] NULL,
 CONSTRAINT [PK_NS_BH_LanDeNghiCapSo] PRIMARY KEY CLUSTERED 
(
	[LanDeNghiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_LichSuThayDoiThongTin]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_LichSuThayDoiThongTin](
	[LichSuThayDoiThongTinID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiThamGiaID] [int] NULL,
	[NoiDungDieuChinh] [nvarchar](50) NULL,
	[GiaTriCu] [nvarchar](50) NULL,
	[GiaTriMoi] [nvarchar](50) NULL,
	[NgayThang] [datetime] NULL,
	[CanCuDieuChinh] [nvarchar](50) NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[LichSuThayDoiThongTinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_LoaiBienDong]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_LoaiBienDong](
	[LoaiBienDongID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[STTText] [nvarchar](50) NULL,
	[TangGiam] [int] NULL,
	[STT] [int] NULL,
	[DonViID] [int] NULL,
 CONSTRAINT [PK_NS_BH_LoaiBienDong] PRIMARY KEY CLUSTERED 
(
	[LoaiBienDongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_NguoiThamGia]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_NguoiThamGia](
	[NguoiThamGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgaySinh] [datetime] NULL,
	[NgayDeNghiCapSo] [datetime] NULL,
	[NgayCapSo] [datetime] NULL,
	[NgayCap] [datetime] NULL,
	[NgayNghiViec] [datetime] NULL,
	[NgayKyHopDong] [datetime] NULL,
	[Status] [int] NULL,
	[SoBaoHiem] [nvarchar](50) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[DTNgoaiCongTy] [bit] NULL,
	[HoVaTen] [nvarchar](50) NULL,
	[GioiTinh] [nvarchar](50) NULL,
	[NoiDKKhamBanDau] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](550) NULL,
	[CoQuanCapSoBH] [nvarchar](250) NULL,
	[LoaiBH] [int] NULL,
	[SoCapMoi] [bit] NULL,
	[MucLuongDongBH] [decimal](18, 0) NULL,
	[CMTND] [nvarchar](300) NULL,
	[NoiCap] [nvarchar](300) NULL,
	[DoiTuongBHXH] [nvarchar](50) NULL,
	[QueQuan] [nvarchar](550) NULL,
	[MaBV] [nvarchar](50) NULL,
	[SoHopDong] [nvarchar](50) NULL,
	[LoaiHopDongID] [int] NULL,
	[MaTinh] [nvarchar](50) NULL,
	[MaTheBHYT] [nvarchar](50) NULL,
	[BHYTTuNgay] [datetime] NULL,
	[BHYTToiNgay] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[NguoiLapID] [int] NULL,
	[BHXHTuNgay] [datetime] NULL,
	[BHXHToiNgay] [datetime] NULL,
	[DonViDangKyBHXHID] [int] NULL,
	[MucLuongDongBHCu] [decimal](18, 0) NULL,
	[BHTNTuNgay] [datetime] NULL,
	[BHTNToiNgay] [datetime] NULL,
	[NgayTangBHYT] [datetime] NULL,
	[NgayGiamBHYT] [datetime] NULL,
	[NgayTraTheBHYT] [datetime] NULL,
	[TyleDong] [decimal](18, 3) NULL,
	[DanToc] [nvarchar](50) NULL,
	[MaTinhCapCMT] [nvarchar](500) NULL,
	[DaCoSoBHXH] [bit] NULL,
	[ChucVuBHXH] [nvarchar](500) NULL,
	[MaNhanVien2] [nvarchar](50) NULL,
	[MaCu] [nvarchar](50) NULL,
	[CongTyID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[SoThangBanDau] [int] NULL,
	[SoNamBanDau] [int] NULL,
	[MucLuongTBBanDau] [decimal](18, 3) NULL,
	[NgayBatDauThamGiaDongBHXH] [datetime] NULL,
	[DiaChiLienHe] [nvarchar](550) NULL,
	[NoiDKKhamBanDauID] [int] NULL,
	[QQ_SoNha] [nvarchar](500) NULL,
	[QQ_XaPhuong] [nvarchar](500) NULL,
	[QQ_QuanHuyen] [nvarchar](500) NULL,
	[QQ_Tinh] [nvarchar](500) NULL,
	[TT_SoNha] [nvarchar](500) NULL,
	[TT_XaPhuong] [nvarchar](500) NULL,
	[TT_QuanHuyen] [nvarchar](500) NULL,
	[TT_Tinh] [nvarchar](500) NULL,
	[LH_SoNha] [nvarchar](500) NULL,
	[LH_XaPhuong] [nvarchar](500) NULL,
	[LH_QuanHuyen] [nvarchar](500) NULL,
	[LH_Tinh] [nvarchar](500) NULL,
	[NgayGiamBHXH] [datetime] NULL,
 CONSTRAINT [PK_NS_BH_NguoiThamGia] PRIMARY KEY CLUSTERED 
(
	[NguoiThamGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_NoiKhamChuaBenh]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_NoiKhamChuaBenh](
	[NoiKhamChuaBenhID] [int] IDENTITY(1,1) NOT NULL,
	[TenNoiKhamChuaBenh] [nvarchar](250) NULL,
	[MaNoiKhamChuaBenh] [nvarchar](50) NULL,
	[TinhID] [int] NULL,
 CONSTRAINT [PK_NS_BH_NoiKhamChuaBenh] PRIMARY KEY CLUSTERED 
(
	[NoiKhamChuaBenhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_TangGiamBHYT]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_TangGiamBHYT](
	[TangGiamBHYTID] [int] IDENTITY(1,1) NOT NULL,
	[Ngay] [datetime] NULL,
	[IsTang] [bit] NULL,
	[MucDong] [decimal](10, 0) NULL,
	[Tyle] [decimal](10, 3) NULL,
	[NguoiThamGiaID] [int] NULL,
 CONSTRAINT [PK_NS_BH_TangGiamBHYT] PRIMARY KEY CLUSTERED 
(
	[TangGiamBHYTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_ThoaiTruyThu]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_ThoaiTruyThu](
	[ThoaiTruyThuID] [int] IDENTITY(1,1) NOT NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[MucLuong] [decimal](18, 0) NULL,
	[Type] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NgayLap] [datetime] NULL,
	[SoThang] [int] NULL,
	[SoNam] [int] NULL,
	[MucTienHuong] [int] NULL,
	[NguoiLapID] [int] NULL,
	[NguoiThamGiaID] [int] NULL,
	[TyleDong] [decimal](18, 3) NULL,
	[Tyle] [decimal](18, 3) NULL,
	[DonViID] [int] NULL,
 CONSTRAINT [PK_NS_BH_ThoaiTruyThu] PRIMARY KEY CLUSTERED 
(
	[ThoaiTruyThuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_TNLDBNN]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_TNLDBNN](
	[TNLDBNNID] [int] IDENTITY(1,1) NOT NULL,
	[QuyetDinhSo] [nvarchar](100) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NguoiKy] [nvarchar](50) NULL,
	[NguoiKyID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[SoLanTaiNan] [int] NULL,
	[NguoiThamGiaBHID] [int] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[XetDuyet] [bit] NULL,
	[NgayLap] [datetime] NULL,
	[SoNgayNghiTaiGia] [int] NULL,
	[SoNgayNghiTapChung] [int] NULL,
	[TongSoTienThanhToan] [int] NULL,
	[MucLuongDongBHXH] [decimal](18, 0) NULL,
	[MucLuongToiThieu] [int] NULL,
	[PhanTramThuongTat] [decimal](18, 3) NULL,
	[CheDoHuong] [nvarchar](50) NULL,
	[NghiDuongSuc] [bit] NULL,
	[NgayTaiNan] [datetime] NULL,
	[NoiXayRaTaiNan] [nvarchar](250) NULL,
	[DonViID] [int] NULL,
 CONSTRAINT [PK_NH_BH_TNLDBNN] PRIMARY KEY CLUSTERED 
(
	[TNLDBNNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_TongHop]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_TongHop](
	[TongHopID] [int] IDENTITY(1,1) NOT NULL,
	[NgayThang] [datetime] NULL,
	[SL_BHXH] [int] NULL,
	[QL_BHXH] [decimal](18, 0) NULL,
	[PD_BHXH] [decimal](18, 0) NULL,
	[SL_BHYT] [int] NULL,
	[QL_BHYT] [decimal](18, 0) NULL,
	[PD_BHYT] [decimal](18, 0) NULL,
	[SL_BHTN] [int] NULL,
	[QL_BHTN] [decimal](18, 0) NULL,
	[PD_BHTN] [decimal](18, 0) NULL,
 CONSTRAINT [PK_NS_BH_TongHop] PRIMARY KEY CLUSTERED 
(
	[TongHopID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BH_TuTuat]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BH_TuTuat](
	[TuTuatID] [int] IDENTITY(1,1) NOT NULL,
	[QuyetDinhSo] [nvarchar](100) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NguoiKy] [nvarchar](50) NULL,
	[NguoiKyID] [int] NULL,
	[NguoiThamGiaBHID] [int] NULL,
	[NgayMat] [datetime] NULL,
	[XetDuyet] [bit] NULL,
	[NgayLap] [datetime] NULL,
	[TongSoTienThanhToan] [int] NULL,
	[MucLuongToiThieu] [int] NULL,
	[MucLuongDongBHXH] [decimal](18, 0) NULL,
	[SoNguoiMat] [int] NULL,
	[CheDoHuong] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[DonViID] [int] NULL,
 CONSTRAINT [PK_NS_BH_TuTuat] PRIMARY KEY CLUSTERED 
(
	[TuTuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_BieuDo]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_BieuDo](
	[BieuDoID] [int] IDENTITY(1,1) NOT NULL,
	[TenBieuDo] [nvarchar](50) NULL,
	[Command] [ntext] NULL,
	[LoaiBieuDo] [nvarchar](50) NULL,
	[KieuLietKe] [nvarchar](50) NULL,
	[SeriesName] [nvarchar](50) NULL,
	[CpyID] [int] NULL,
 CONSTRAINT [PK_BieuDo] PRIMARY KEY CLUSTERED 
(
	[BieuDoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_CaLamViec]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_CaLamViec](
	[CaLamViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenCa] [nvarchar](50) NULL,
	[TGBatDau] [datetime] NULL,
	[TGKetThuc] [datetime] NULL,
	[DuocSuDung] [bit] NULL,
	[LuongCa] [decimal](18, 3) NULL,
	[CpyID] [int] NULL,
	[ThoiGianNghiGiuaGio] [int] NULL,
	[TGBatDau1] [datetime] NULL,
	[TGKetThuc1] [datetime] NULL,
	[GroupID] [int] NULL,
	[MaCa] [nvarchar](50) NULL,
	[KyHieuChamCongID] [int] NULL,
	[GioBatDau] [datetime] NULL,
	[GioKetThuc] [datetime] NULL,
	[GioVaoXacDinhTu] [time](7) NULL,
	[GioVaoXacDinhDen] [time](7) NULL,
	[GioRaXacDinhTu] [time](7) NULL,
	[GioRaXacDinhDen] [time](7) NULL,
	[BatDauNghiGiuaCa] [datetime] NULL,
	[KetThucNghiGiuaCa] [datetime] NULL,
	[SoPhutChoPhepDenMuon] [int] NULL,
	[SoPhutChoPhepDenSom] [int] NULL,
	[GioBatDau1] [time](7) NULL,
	[GioKetThuc1] [time](7) NULL,
	[GioBatDau2] [time](7) NULL,
	[GioKetThuc2] [time](7) NULL,
	[SoPhutChoPhepVeSom] [int] NULL,
	[SoPhutToiThieuTinhLamThemSauCa] [int] NULL,
	[SoPhutToiThieuTinhLamThemTruocCa] [int] NULL,
	[SoPhutLamTronTinhGioLamThem] [int] NULL,
	[RaNgoaiBiTruGio] [bit] NULL,
	[CongNghiGiuCaVaoTGLam] [bit] NULL,
	[Block] [int] NULL,
	[CachTinhGio] [int] NULL,
	[ThoiGianNghiGiuaCa] [int] NULL,
	[SoPhutToiDaLamThemSauCa] [int] NULL,
	[SoPhutToiDaLamThemTruocCa] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThoiGianLamThemTruocCaXacDinhTu] [time](7) NULL,
	[ThoiGianLamThemSauCaXacDinhToi] [time](7) NULL,
	[CongTyID] [int] NULL,
	[NghiGiuaCaTu] [time](7) NULL,
	[NghiGiuaCaToi] [time](7) NULL,
	[TenCaTV] [nvarchar](50) NULL,
	[NhomCaLamViecID] [int] NULL,
	[Block_LamThem] [int] NULL,
 CONSTRAINT [PK_CaLamViec] PRIMARY KEY CLUSTERED 
(
	[CaLamViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_CaLamViec_NhomCa]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_CaLamViec_NhomCa](
	[CaLamViecNhomCaID] [int] IDENTITY(1,1) NOT NULL,
	[CaLamViecID] [int] NULL,
	[NhomCaLamViecID] [int] NULL,
 CONSTRAINT [PK_NS_CaLamViec_NhomCa] PRIMARY KEY CLUSTERED 
(
	[CaLamViecNhomCaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_CongViec]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_CongViec](
	[ChucVuCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[CongViecID] [int] NULL,
 CONSTRAINT [PK_NS_ChucVu_CongViec] PRIMARY KEY CLUSTERED 
(
	[ChucVuCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_DanhMucDaoTao]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_DanhMucDaoTao](
	[ChucVu_DanhMucDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[DanhMucDaoTaoID] [int] NULL,
	[MoTa] [nvarchar](50) NULL,
	[ThoiHanDaoTao] [int] NULL,
	[ThoiHanCanhBao] [int] NULL,
 CONSTRAINT [PK_NS_ChucVu_DanhMucDaoTao] PRIMARY KEY CLUSTERED 
(
	[ChucVu_DanhMucDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_GiamKhao]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_GiamKhao](
	[ChucVuGiamKhaoID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[GiamKhaoID] [int] NULL,
	[HeSo] [decimal](10, 3) NULL,
	[GhiChu] [nvarchar](150) NULL,
 CONSTRAINT [PK_NS_ChucVu_GiamKhao] PRIMARY KEY CLUSTERED 
(
	[ChucVuGiamKhaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_MonThi]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_MonThi](
	[ChucVuMonThiID] [int] IDENTITY(1,1) NOT NULL,
	[MonThiID] [int] NULL,
	[ChucVuVongThiID] [int] NULL,
	[ChucVuID] [int] NULL,
	[HeSo] [decimal](10, 0) NULL,
 CONSTRAINT [PK_NS_ChucVu_MonThi_1] PRIMARY KEY CLUSTERED 
(
	[ChucVuMonThiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_NangLuc]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_NangLuc](
	[ChucVuNangLucID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[NangLucID] [int] NULL,
	[TrongSo] [decimal](10, 0) NULL,
	[DiemKyVong] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsChucVu_NangLuc] PRIMARY KEY CLUSTERED 
(
	[ChucVuNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_NhomNangLuc]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_NhomNangLuc](
	[ChucVuNhomNangLucID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[NhomNangLucID] [int] NULL,
	[TrongSo] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ChucVu_NhomNangLuc] PRIMARY KEY CLUSTERED 
(
	[ChucVuNhomNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_VongThi]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_VongThi](
	[ChucVuVongThiID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[VongThiID] [int] NULL,
	[GhiChu] [nvarchar](150) NULL,
	[TenVongThi] [nvarchar](150) NULL,
	[STT] [int] NULL,
	[TongDiemYeuCau] [decimal](18, 0) NULL,
	[IsDaoTao] [bit] NULL,
 CONSTRAINT [PK_NS_ChucVu_MonThi] PRIMARY KEY CLUSTERED 
(
	[ChucVuVongThiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_ChucVu_YeuCau]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_ChucVu_YeuCau](
	[ChucVuYeuCauID] [int] IDENTITY(1,1) NOT NULL,
	[ChucVuID] [int] NULL,
	[TenYeuCau] [nvarchar](50) NULL,
	[MoTa] [nvarchar](250) NULL,
	[DoiTuongYeuCau] [nvarchar](250) NULL,
	[GiaTri] [int] NULL,
 CONSTRAINT [PK_NS_ChucVu_YeuCau] PRIMARY KEY CLUSTERED 
(
	[ChucVuYeuCauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_CV_CongViec]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_CV_CongViec](
	[CongViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenCongViec] [nvarchar](50) NULL,
	[NgayHetHan] [datetime] NULL,
	[CongViecChaID] [int] NULL,
	[DuAnID] [int] NULL,
	[NhomCongViecID] [int] NULL,
	[TrangThai] [int] NULL,
	[DoUuTien] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayHoanThanh] [datetime] NULL,
	[PhanTramHoanThanh] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_NS_CV_CongViec] PRIMARY KEY CLUSTERED 
(
	[CongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_ChiTieuKyThuat]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_ChiTieuKyThuat](
	[ChiTieuKyThuatID] [int] IDENTITY(1,1) NOT NULL,
	[TenChiTieu] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[DonViTinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_ChiTieuKyThuat] PRIMARY KEY CLUSTERED 
(
	[ChiTieuKyThuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_DsHangMuc]    Script Date: 6/21/2018 11:30:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_DsHangMuc](
	[HangMucID] [int] IDENTITY(1,1) NOT NULL,
	[TenHangMuc] [nvarchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[HopDongID] [int] NULL,
	[NgayBatDauThucTe] [datetime] NULL,
	[NgayKetThucThucTe] [datetime] NULL,
 CONSTRAINT [PK_NS_DA_HangMuc] PRIMARY KEY CLUSTERED 
(
	[HangMucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_DsQuiTrinh]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_DsQuiTrinh](
	[QuiTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[TenQuiTrinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_DsQuiTrinh] PRIMARY KEY CLUSTERED 
(
	[QuiTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_FileLuuTienDoTT]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_FileLuuTienDoTT](
	[HopDongThanhToanID] [int] IDENTITY(1,1) NOT NULL,
	[TienDoThanhToanID] [int] NULL,
	[FileLuuHoSo] [image] NULL,
	[TenFile] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_FileLuuTienDoTT] PRIMARY KEY CLUSTERED 
(
	[HopDongThanhToanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_HangMuc_ChiTieuKyThuat]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_HangMuc_ChiTieuKyThuat](
	[HopDong_ChiTieuKyThuatID] [int] IDENTITY(1,1) NOT NULL,
	[HangMucID] [int] NOT NULL,
	[ChiTieuKyThuatID] [int] NOT NULL,
	[GiaTri] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_DA_HopDong_ChiTieuKyThuat] PRIMARY KEY CLUSTERED 
(
	[HopDong_ChiTieuKyThuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_HangMuc_SuatDauTu]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_HangMuc_SuatDauTu](
	[HopDong_SuatDauTuID] [int] IDENTITY(1,1) NOT NULL,
	[GiaTri] [decimal](18, 2) NULL,
	[ThuTu] [int] NULL,
	[HangMucID] [int] NULL,
	[SuatDauTuID] [int] NULL,
 CONSTRAINT [PK_NS_DA_HopDong_SuatDauTu] PRIMARY KEY CLUSTERED 
(
	[HopDong_SuatDauTuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_HopDong_QuiTrinh]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_HopDong_QuiTrinh](
	[HopDong_QuiTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[HopDongID] [int] NULL,
	[QuiTrinhID] [int] NULL,
	[ThuTu] [int] NULL,
 CONSTRAINT [PK_NS_DA_HopDong_QuiTrinh] PRIMARY KEY CLUSTERED 
(
	[HopDong_QuiTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_NhanVien_QuiTrinh]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_NhanVien_QuiTrinh](
	[NhanVien_QuiTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[QuiTrinhID] [int] NULL,
	[CongViecID] [int] NULL,
	[KetQuaID] [int] NULL,
	[HangMucID] [int] NULL,
	[HopDongID] [int] NULL,
	[KinhNghiemLamViecID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[ThoiGianThucHien] [decimal](18, 2) NULL,
	[TinhTrangHoanThanh] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_DA_NhanVien_QuiTrinh] PRIMARY KEY CLUSTERED 
(
	[NhanVien_QuiTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_SuatDauTu]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_SuatDauTu](
	[SuatDauTuID] [int] IDENTITY(1,1) NOT NULL,
	[TenSuatDauTu] [nvarchar](50) NULL,
	[GhiChu] [ntext] NULL,
	[DonViTinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_SuatDauTu] PRIMARY KEY CLUSTERED 
(
	[SuatDauTuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_ThongTinChuDauTu]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_ThongTinChuDauTu](
	[ChuDauTuID] [int] IDENTITY(1,1) NOT NULL,
	[HoVaTen] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
	[DienThoaiLienLac] [nvarchar](50) NULL,
	[HopDongID] [int] NULL,
	[Email] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_ThongTinChuDauTu] PRIMARY KEY CLUSTERED 
(
	[ChuDauTuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_ThongTinHopDong]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_ThongTinHopDong](
	[HopDongID] [int] IDENTITY(1,1) NOT NULL,
	[SoHopDong] [nvarchar](50) NULL,
	[TomTatNoiDung] [nvarchar](200) NULL,
	[DuAn] [nvarchar](100) NULL,
	[CongTrinh] [nvarchar](50) NULL,
	[DiaDiem] [nvarchar](100) NULL,
	[GiaoThau] [nvarchar](50) NULL,
	[NhanThau] [nvarchar](50) NULL,
	[NoiDungCongViec] [nvarchar](200) NULL,
	[GiaTri] [decimal](18, 0) NULL,
	[TienDo] [bit] NULL,
	[NgayThucHien] [datetime] NULL,
	[NgayBatDau] [datetime] NULL,
	[ThoiGianThucHien] [int] NULL,
	[TongDienTichSan] [decimal](18, 2) NULL,
	[ChieuCaoCongTrinh] [decimal](18, 2) NULL,
	[SoTangHam] [int] NULL,
	[TongMucDauTu] [decimal](18, 0) NULL,
	[DanhGiaChung] [nvarchar](1000) NULL,
	[MaDuAn] [nvarchar](50) NULL,
	[NgayHoanThanhThucTe] [datetime] NULL,
	[VAT] [bit] NULL,
	[ChuDauTu] [nvarchar](500) NULL,
	[NgayKhoiCong] [datetime] NULL,
	[NgayHoanThanhDuKien] [datetime] NULL,
	[PWDadmin] [nvarchar](50) NULL,
	[PWDuser1] [nvarchar](50) NULL,
	[PWDuser2] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DA_ThongTinHopDong] PRIMARY KEY CLUSTERED 
(
	[HopDongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_TienDoCongViec]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_TienDoCongViec](
	[TienDoCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienQuiTrinhID] [int] NULL,
	[TinhTrangHoanThanh] [decimal](18, 2) NULL,
	[NgayThayDoi] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_DA_TienDoCongViec] PRIMARY KEY CLUSTERED 
(
	[TienDoCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DA_TienDoThanhToan]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DA_TienDoThanhToan](
	[TienDoThanhToanID] [int] IDENTITY(1,1) NOT NULL,
	[HopDongID] [int] NULL,
	[NgayThanhToan] [datetime] NULL,
	[SoTien] [decimal](18, 0) NULL,
	[TinhTrang] [bit] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[ThuTu] [int] NULL,
	[SoTienDaThanhToan] [decimal](18, 0) NULL,
	[MaHoaDon] [nvarchar](50) NULL,
	[NgayXuatHoaDon] [datetime] NULL,
	[HoSoThanhToan] [image] NULL,
 CONSTRAINT [PK_NS_DA_TienDoThanhToan] PRIMARY KEY CLUSTERED 
(
	[TienDoThanhToanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DangKyNghiPhep]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DangKyNghiPhep](
	[DangKyNghiPhepID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[LyDoNghi] [nvarchar](500) NULL,
	[KyHieuChamCongID] [int] NULL,
	[SoNgayDuocNghiThem] [decimal](10, 2) NULL,
	[XetDuyet] [int] NULL,
	[NgayDangKy] [datetime] NULL,
	[NgayXetDuyet] [datetime] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[LyDoTuChoi] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_DangKyNghiPhep] PRIMARY KEY CLUSTERED 
(
	[DangKyNghiPhepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_BoTieuChiDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_BoTieuChiDanhGia](
	[BoTieuChiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[TenBoTieuChiDanhGia] [nvarchar](250) NULL,
	[MaBoTieuChiDanhGia] [nvarchar](50) NULL,
	[IsUse] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DG_BoTieuChiDanhGia] PRIMARY KEY CLUSTERED 
(
	[BoTieuChiDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_ChiTieuDanhGiaPhongBan]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_ChiTieuDanhGiaPhongBan](
	[ChiTieuDanhGiaPhongBanID] [int] IDENTITY(1,1) NOT NULL,
	[MaSo] [nvarchar](50) NULL,
	[Ten] [nvarchar](50) NULL,
	[TenChiTieuDanhGiaPhongBanTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DG_ChiTieuDanhGiaPhongBan] PRIMARY KEY CLUSTERED 
(
	[ChiTieuDanhGiaPhongBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_CongViec]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_CongViec](
	[CongViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenCongViec] [nvarchar](50) NULL,
	[TyLe] [decimal](18, 3) NULL,
	[CongViecChaID] [int] NULL,
	[TongDiemCanDat] [decimal](18, 0) NULL,
	[MoTa] [nvarchar](250) NULL,
	[KyDanhGia] [int] NULL,
	[IsUse] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[ViTri] [int] NULL,
	[Ma] [nvarchar](50) NULL,
	[Level] [int] NULL,
	[CongThuc] [nvarchar](1250) NULL,
	[LamTron] [int] NULL,
	[IsNhapGiaTri] [bit] NULL,
	[CongThucQuy] [nvarchar](550) NULL,
	[CongThucNam] [nvarchar](550) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenCongViecTA] [nvarchar](50) NULL,
	[TrongSo] [decimal](10, 3) NULL,
	[MucTieu] [decimal](10, 0) NULL,
	[GiaTriLonNhat] [decimal](18, 3) NULL,
	[DiemQuyDoi] [decimal](18, 0) NULL,
	[DonViTinh] [nvarchar](20) NULL,
	[ValueList] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DG_CongViec] PRIMARY KEY CLUSTERED 
(
	[CongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_DoiTuongDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_DoiTuongDanhGia](
	[DoiTuongDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDanhGiaID] [int] NULL,
	[PhongBanID] [int] NULL,
	[NhanVienID] [int] NULL,
	[ChucVuID] [int] NULL,
	[NhomDanhGiaID] [int] NULL,
	[NguoiDuyetID] [int] NULL,
	[ChucDanhID] [int] NULL,
 CONSTRAINT [PK_NS_DG_DoiTuongDanhGia] PRIMARY KEY CLUSTERED 
(
	[DoiTuongDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_DotDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_DotDanhGia](
	[DotDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[MaDotDanhGia] [nvarchar](50) NULL,
	[TenDotDanhGia] [nvarchar](50) NULL,
	[NgayDanhGia] [datetime] NULL,
	[NguoiDanhGiaID] [int] NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Quy] [int] NULL,
	[Nam] [int] NULL,
	[NhomDanhGiaID] [int] NULL,
	[HeSoDanhGia] [decimal](10, 0) NULL,
	[KyDanhGia] [int] NULL,
	[BuocThucHienXetDuyetID] [int] NULL,
	[BoTieuChiDanhGiaID] [int] NULL,
	[TrangThai] [int] NULL,
	[TongDiem] [decimal](10, 0) NULL,
	[XepLoaiDanhGiaID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[IsVongCuoi] [bit] NULL,
 CONSTRAINT [PK_NS_DG_DotDanhGia] PRIMARY KEY CLUSTERED 
(
	[DotDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_KetQuaDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_KetQuaDanhGia](
	[KetQuaDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CongViecID] [int] NULL,
	[NguoiDanhGiaID] [int] NULL,
	[DiemSo] [decimal](18, 0) NULL,
	[Thang] [int] NULL,
	[Quy] [int] NULL,
	[Nam] [int] NULL,
	[DiemTam] [decimal](18, 0) NULL,
	[NguoiDuyetID] [int] NULL,
	[NgayDuyet] [datetime] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NhomDanhGiaID] [int] NULL,
	[HeSoDanhGia] [decimal](18, 3) NULL,
	[GiaTriStr] [nvarchar](50) NULL,
	[TrongSo] [decimal](18, 3) NULL,
	[CongViecChaID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[TongDiemCanDat] [int] NULL,
	[DotDanhGiaID] [int] NULL,
	[MoTaCongViec] [nvarchar](50) NULL,
	[MucTieu] [decimal](18, 0) NULL,
	[MucTieuCongTy] [decimal](18, 0) NULL,
	[DiemNhap] [decimal](18, 3) NULL,
	[TongDiem] [decimal](18, 3) NULL,
	[TT_TuNgay] [datetime] NULL,
	[TT_ToiNgay] [datetime] NULL,
	[NguyenNhan] [nvarchar](250) NULL,
	[KetQua] [nvarchar](250) NULL,
	[GiaiPhap] [nvarchar](250) NULL,
	[TyLeChatLuong] [decimal](18, 3) NULL,
	[GiaTriLonNhat] [decimal](18, 3) NULL,
 CONSTRAINT [PK_NS_DG_KetQuaDanhGia] PRIMARY KEY CLUSTERED 
(
	[KetQuaDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_KetQuaPheDuyet]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_KetQuaPheDuyet](
	[KetQuaPheDuyetID] [int] NOT NULL,
	[QuyTrinhBuocThucHienID] [int] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[TrangThai] [int] NULL,
	[LyDoTuChoi] [nvarchar](50) NULL,
	[NgayThucHien] [datetime] NULL,
	[NgayChuyenYeuCau] [datetime] NULL,
	[DiemSo] [decimal](10, 0) NULL,
	[XepLoaiDanhGiaID] [int] NULL,
	[DotDanhGiaID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DG_KetQuaPheDuyet] PRIMARY KEY CLUSTERED 
(
	[KetQuaPheDuyetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_KetQuaPheDuyet_ChiTiet]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_KetQuaPheDuyet_ChiTiet](
	[PheDuyetChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[KetQuaPheDuyetID] [int] NULL,
	[KetQuaDanhGiaID] [int] NULL,
	[DiemSo] [decimal](10, 0) NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DG_KetQuaPheDuyet_ChiTiet] PRIMARY KEY CLUSTERED 
(
	[PheDuyetChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_LoaiDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_LoaiDanhGia](
	[LoaiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Min] [decimal](18, 0) NULL,
	[Max] [decimal](18, 0) NULL,
	[HeSoTinhLuong] [decimal](18, 0) NULL,
	[TenLoaiDanhGiaTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DG_LoaiDanhGia] PRIMARY KEY CLUSTERED 
(
	[LoaiDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_MucTieuCongViec]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_MucTieuCongViec](
	[MucTieuCongViecID] [int] IDENTITY(1,1) NOT NULL,
	[CongViecID] [int] NULL,
	[Thang] [int] NULL,
	[Quy] [int] NULL,
	[Nam] [int] NULL,
	[DiemSo] [decimal](10, 3) NULL,
	[NhanVienID] [int] NULL,
	[XepLoaiDanhGiaID] [int] NULL,
 CONSTRAINT [PK_NS_DG_MucTieuCongViec] PRIMARY KEY CLUSTERED 
(
	[MucTieuCongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_NhanVien_CongViec]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_NhanVien_CongViec](
	[NhanVien_CongViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CongViecID] [int] NULL,
	[TyLe] [decimal](18, 0) NULL,
	[DiemTam] [int] NULL,
	[NguoiDuyetID] [int] NULL,
	[NgayDuyet] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TongDiemCanDat] [int] NULL,
	[CongViecChaID] [int] NULL,
	[MoTa] [nvarchar](50) NULL,
	[TrongSo] [decimal](18, 0) NULL,
	[MucTieu] [decimal](18, 0) NULL,
	[KyDanhGia] [int] NULL,
	[Status] [int] NULL,
	[MucTieuCongTy] [decimal](18, 0) NULL,
	[ThoiGianDanhGia] [nvarchar](50) NULL,
	[MaDotDanhGia] [nvarchar](50) NULL,
	[GiaTriLonNhat] [decimal](18, 0) NULL,
	[KH_BatDau] [datetime] NULL,
	[KH_KetThuc] [datetime] NULL,
	[DiemNhap] [decimal](18, 0) NULL,
 CONSTRAINT [PK_NS_DG_NhanVien_CongViec] PRIMARY KEY CLUSTERED 
(
	[NhanVien_CongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_NhomDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_NhomDanhGia](
	[NhomDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[HeSo] [decimal](18, 3) NULL,
	[Ten] [nvarchar](50) NULL,
	[IsUse] [bit] NULL,
	[TenNhomDanhGiaTA] [nvarchar](50) NULL,
	[MaNhomDanhGia] [nvarchar](50) NULL,
	[TenNhomDanhGia] [nvarchar](50) NULL,
	[IsNhanVienTuDanhGia] [bit] NULL,
	[ThuTu] [int] NULL,
	[BatBuocPhaiDGCapTruoc] [bit] NULL,
	[CapHienThiKetQua] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[QuyTrinhID] [int] NULL,
 CONSTRAINT [PK_NS_DG_NhomDanhGia] PRIMARY KEY CLUSTERED 
(
	[NhomDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_PhongBan_CongViec]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_PhongBan_CongViec](
	[PhongBan_CongViecID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[CongViecID] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[CongViecChaID] [int] NULL,
	[MoTa] [nvarchar](550) NULL,
	[TrongSo] [decimal](10, 1) NULL,
	[MucTieu] [decimal](10, 1) NULL,
	[KyDanhGia] [int] NULL,
	[Status] [int] NULL,
	[MucTieuCongTy] [decimal](10, 1) NULL,
	[MaDotDanhGia] [nvarchar](50) NULL,
	[GiaTriLonNhat] [decimal](10, 0) NULL,
 CONSTRAINT [PK_NS_DG_PhongBan_CongViec] PRIMARY KEY CLUSTERED 
(
	[PhongBan_CongViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_PhongBan_DanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_PhongBan_DanhGia](
	[PhongBan_DanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[LoaiDanhGiaID] [int] NULL,
	[TieuChuanDanhGiaID] [int] NULL,
	[Tyle] [decimal](18, 3) NULL,
	[NguoiDuyetID] [int] NULL,
 CONSTRAINT [PK_NS_DG_PhongBan_DanhGia] PRIMARY KEY CLUSTERED 
(
	[PhongBan_DanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_QuyTrinh]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_QuyTrinh](
	[QuyTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[TenQuyTrinh] [nvarchar](50) NULL,
	[MaQuyTrinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DG_QuyTrinh] PRIMARY KEY CLUSTERED 
(
	[QuyTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_TongHopDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_TongHopDanhGia](
	[TongHopDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CongViecID] [int] NULL,
	[Thang] [int] NULL,
	[Quy] [int] NULL,
	[Nam] [int] NULL,
	[DiemSo] [decimal](18, 3) NULL,
	[LoaiDanhGiaID] [int] NULL,
	[DiemTam] [int] NULL,
	[NguoiDuyetID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DG_TongHopDanhGia] PRIMARY KEY CLUSTERED 
(
	[TongHopDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_TongHopDanhGiaChiTiet]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_TongHopDanhGiaChiTiet](
	[TongHopDanhGiaChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CongViecID] [int] NULL,
	[Thang] [int] NULL,
	[Quy] [int] NULL,
	[Nam] [int] NULL,
	[DiemSo] [decimal](18, 3) NULL,
	[NhomDanhGiaID] [int] NULL,
	[DiemTam] [decimal](18, 3) NULL,
	[NguoiDuyetID] [int] NULL,
	[HeSoDanhGia] [decimal](18, 3) NULL,
	[GiaTriStr] [nvarchar](50) NULL,
	[IsChinhSua] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DG_TongHopDanhGiaChiTiet] PRIMARY KEY CLUSTERED 
(
	[TongHopDanhGiaChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DG_XepLoaiDanhGia]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DG_XepLoaiDanhGia](
	[XepLoaiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[TenXepLoaiDanhGia] [nvarchar](50) NULL,
	[Min] [decimal](10, 0) NULL,
	[Max] [decimal](10, 0) NULL,
	[HeSoTinhLuong] [decimal](10, 0) NULL,
	[TenXepLoaiDanhGiaTA] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DinhBienNhanSu]    Script Date: 6/21/2018 11:30:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DinhBienNhanSu](
	[DinhBienNhanSuID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[SoLuong] [int] NULL,
	[Nam] [int] NULL,
	[Thang] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[SLChoNghi] [int] NULL,
	[SLTuyenMoi] [int] NULL,
	[SLDieuChuyenDi] [int] NULL,
	[SLDieuChuyenToi] [int] NULL,
	[PhongBanChaID] [int] NULL,
	[SoLuongThucTe] [int] NULL,
	[HeSo] [decimal](18, 3) NULL,
 CONSTRAINT [PK_NS_HoachDinhNhanSu] PRIMARY KEY CLUSTERED 
(
	[DinhBienNhanSuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DSCapChucDanh]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DSCapChucDanh](
	[CapChucDanhID] [int] IDENTITY(1,1) NOT NULL,
	[TenCap] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DSCapChucDanh] PRIMARY KEY CLUSTERED 
(
	[CapChucDanhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChucDanh]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChucDanh](
	[ChucDanhID] [int] IDENTITY(1,1) NOT NULL,
	[TenChucDanh] [nvarchar](500) NULL,
	[MaChucDanh] [nvarchar](50) NULL,
	[ThuTu] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenChucDanhTA] [nvarchar](50) NULL,
	[NhomChucDanhID] [int] NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_NS_DsChucDanh] PRIMARY KEY CLUSTERED 
(
	[ChucDanhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChucVu]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChucVu](
	[ChucVuID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenChucVu] [nvarchar](520) NULL,
	[MaChucVu] [nvarchar](550) NULL,
	[PhongBanID] [int] NULL,
	[DienGiai] [nvarchar](500) NULL,
	[CongViecChinh] [nvarchar](4000) NULL,
	[TrachNhiem] [nvarchar](4000) NULL,
	[NghiaVu] [nvarchar](4000) NULL,
	[NghachCongChucID] [int] NULL,
	[QuyenHan] [nvarchar](4000) NULL,
	[YeuCau] [nvarchar](4000) NULL,
	[BaoCaoTrucTiep] [nvarchar](50) NULL,
	[BaoCaoGianTiep] [nvarchar](50) NULL,
	[QuanLyTrucTiep] [nvarchar](50) NULL,
	[QuanLyGianTiep] [nvarchar](50) NULL,
	[MucTieuCongViec] [nvarchar](50) NULL,
	[TieuChiDanhGia] [nvarchar](1000) NULL,
	[YeuCauTrinhDo] [nvarchar](1000) NULL,
	[YeuCauKinhNghiem] [nvarchar](1000) NULL,
	[KienThucCanThiet] [nvarchar](1000) NULL,
	[KyNangCanThiet] [nvarchar](1000) NULL,
	[TinhCachCaNhan] [nvarchar](1000) NULL,
	[QuanHeNoiBo] [nvarchar](1000) NULL,
	[QuanHeBenNgoai] [nvarchar](1000) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ChucDanhID] [int] NULL,
	[QuanLyTrucTiepID] [int] NULL,
	[QuanLyGianTiepID] [int] NULL,
	[TenChucVuTA] [nvarchar](50) NULL,
	[Keyword] [nvarchar](550) NULL,
	[PhepNam] [int] NULL,
	[UV_GioiTinh] [nvarchar](50) NULL,
	[UV_TuTuoi] [int] NULL,
	[UV_ToiTuoi] [int] NULL,
	[UV_TrinhDoHocVan] [int] NULL,
	[UV_ChuyenNganh] [int] NULL,
	[UV_NamKinhNghiem] [int] NULL,
	[UV_HangTotNghiep] [int] NULL,
	[UV_ChucVuDamNhiem] [int] NULL,
	[UV_DanhHieuKhenThuong] [nvarchar](500) NULL,
	[ChucVuChaID] [int] NULL,
	[NhomNghiepVuID] [int] NULL,
 CONSTRAINT [PK_NS_DsChucVu] PRIMARY KEY CLUSTERED 
(
	[ChucVuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChucVu_ChucNang]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChucVu_ChucNang](
	[ChucVu_ChucNangID] [int] IDENTITY(1,1) NOT NULL,
	[Mota] [nvarchar](50) NULL,
	[TieuChiDanhGia] [nvarchar](50) NULL,
	[ChucVuID] [int] NULL,
 CONSTRAINT [PK_NS_DsChucVu_ChucNang] PRIMARY KEY CLUSTERED 
(
	[ChucVu_ChucNangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChucVuDang]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChucVuDang](
	[ChucVuDangID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenChucVu] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenChucVuDangTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsChucVuDang] PRIMARY KEY CLUSTERED 
(
	[ChucVuDangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChucVuQuanDoi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChucVuQuanDoi](
	[ChucVuQuanDoiID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenChucVu] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenChucVuQuanDoiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsChucVuQuanDoi] PRIMARY KEY CLUSTERED 
(
	[ChucVuQuanDoiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChungChi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChungChi](
	[ChungChiID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenChungChiTA] [nvarchar](50) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_NS_DsChungChi] PRIMARY KEY CLUSTERED 
(
	[ChungChiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChuyenMon]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChuyenMon](
	[ChuyenMonID] [int] IDENTITY(1,1) NOT NULL,
	[HeDaoTao] [nvarchar](50) NOT NULL,
	[NganhDaoTao] [nvarchar](100) NOT NULL,
	[GhiChu] [nvarchar](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenChuyenMonTA] [nvarchar](50) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_TB_ChuyenMon] PRIMARY KEY CLUSTERED 
(
	[ChuyenMonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsChuyenNganh]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsChuyenNganh](
	[ChuyenNganhID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](500) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenChuyenNganhTA] [nvarchar](500) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_NS_DsChuyenNghanh] PRIMARY KEY CLUSTERED 
(
	[ChuyenNganhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsDanhMuc]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsDanhMuc](
	[DanhMucID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[Ten] [nvarchar](255) NULL,
	[MaDanhMuc] [varchar](30) NULL,
	[GiaTri] [nvarchar](50) NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](50) NULL,
 CONSTRAINT [PK_DanhMuc] PRIMARY KEY CLUSTERED 
(
	[DanhMucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsDanToc]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsDanToc](
	[DanTocID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [varchar](50) NULL,
	[Field1] [varchar](50) NULL,
	[Field2] [varchar](50) NULL,
	[Field3] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenDanTocTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsDanToc] PRIMARY KEY CLUSTERED 
(
	[DanTocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsDoiTac]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsDoiTac](
	[DoiTacID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](250) NULL,
	[Ma] [nvarchar](250) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsDoiTac] PRIMARY KEY CLUSTERED 
(
	[DoiTacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsDoiTuongChinhSach]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsDoiTuongChinhSach](
	[DoiTuongChinhSachID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenDoiTuong] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenDoiTuongChinhSachTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsDoiTuongChinhSach] PRIMARY KEY CLUSTERED 
(
	[DoiTuongChinhSachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsDotKhamSucKhoe]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsDotKhamSucKhoe](
	[DotKhamSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[NgayKham] [datetime] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NoiKham] [nvarchar](100) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTKhamSucKhoe] PRIMARY KEY CLUSTERED 
(
	[DotKhamSucKhoeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ns_DsGhiChuBDNS]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ns_DsGhiChuBDNS](
	[GhiChuBDNSID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](100) NULL,
 CONSTRAINT [PK_Ns_DsGhiChuBDNS] PRIMARY KEY CLUSTERED 
(
	[GhiChuBDNSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHangTotNghiep]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHangTotNghiep](
	[HangTotNghiepID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenHangTotNghiepTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsHangTotNghiep] PRIMARY KEY CLUSTERED 
(
	[HangTotNghiepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHeDaoTao]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHeDaoTao](
	[HeDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](250) NULL,
	[MoTa] [nvarchar](150) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenHeDaoTaoTA] [nvarchar](50) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_NS_DsHeDaoTao] PRIMARY KEY CLUSTERED 
(
	[HeDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHinhThucDaoTao]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHinhThucDaoTao](
	[HinhThucDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenHinhThuc] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHinhThucDaoTaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_HinhThucDaoTao] PRIMARY KEY CLUSTERED 
(
	[HinhThucDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHinhThucKhenThuong]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHinhThucKhenThuong](
	[KhenThuongID] [int] NULL,
	[CongTyID] [int] NULL,
	[TenKhenThuong] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[HinhThucKhenThuongID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHinhThucKhenThuongTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsHinhThucKhenThuong] PRIMARY KEY CLUSTERED 
(
	[HinhThucKhenThuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHinhThucKyLuat]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHinhThucKyLuat](
	[HinhThucKyLuatID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenHinhThucKyLuat] [nvarchar](100) NULL,
	[Ma] [nvarchar](50) NULL,
	[Ten] [nvarchar](100) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHinhThucKyLuatTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsHinhThucKyLuat] PRIMARY KEY CLUSTERED 
(
	[HinhThucKyLuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHinhThucNghiViec]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHinhThucNghiViec](
	[HinhThucNghiViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenHinhThucNghiViec] [nvarchar](50) NULL,
	[TenHinhThucNghiViecTA] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsHinhThucNghiViec] PRIMARY KEY CLUSTERED 
(
	[HinhThucNghiViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHocHam]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHocHam](
	[HocHamID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenHocHamTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_HocHam] PRIMARY KEY CLUSTERED 
(
	[HocHamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHocKhoi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHocKhoi](
	[HocKhoiID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHocKhoiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsHocKhoi] PRIMARY KEY CLUSTERED 
(
	[HocKhoiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHocVi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHocVi](
	[HocViID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenHocViTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_HocVi] PRIMARY KEY CLUSTERED 
(
	[HocViID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsHoSoThuTuc]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsHoSoThuTuc](
	[HoSoThuTucID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](150) NULL,
	[BatBuoc] [bit] NULL,
	[LoaiHopDongID] [int] NULL,
	[HanHoanThanh] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHoSoThuTucTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsHoSoThuTuc] PRIMARY KEY CLUSTERED 
(
	[HoSoThuTucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsKhenThuong]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsKhenThuong](
	[KhenThuongID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenKhenThuong] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKhenThuongTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsKhenThuong] PRIMARY KEY CLUSTERED 
(
	[KhenThuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsKhoanMucThuChi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsKhoanMucThuChi](
	[KhoanMucThuChiID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Type] [int] NULL,
	[KhoanMucChuThiID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Ma] [nvarchar](50) NULL,
	[HienThiBangLuong] [bit] NULL,
	[TenKhoanMucThuChiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsKhoanMucThuChi] PRIMARY KEY CLUSTERED 
(
	[KhoanMucThuChiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsKhoi]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsKhoi](
	[KhoiID] [int] IDENTITY(1,1) NOT NULL,
	[TenKhoi] [nvarchar](50) NULL,
	[MaKhoi] [nchar](20) NULL,
	[MoTa] [nvarchar](200) NULL,
	[KhoiChaID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKhoiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsKhoi] PRIMARY KEY CLUSTERED 
(
	[KhoiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiDuAn]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiDuAn](
	[LoaiDuAnID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenLoaiDuAn] [nvarchar](100) NULL,
	[MoTaLoaiDuAn] [nvarchar](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiDuAnTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_TB_LoaiDuAn] PRIMARY KEY CLUSTERED 
(
	[LoaiDuAnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiHinhKinhDoanh]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiHinhKinhDoanh](
	[LoaiHinhKinhDoanhID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenLoaiHinh] [nvarchar](250) NULL,
	[Ma] [varchar](10) NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiHinhKinhDoanhTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsLoaiHinhKinhDoanh] PRIMARY KEY CLUSTERED 
(
	[LoaiHinhKinhDoanhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiHopDong]    Script Date: 6/21/2018 11:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiHopDong](
	[LoaiHopDongID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenLoaiHopDong] [nvarchar](50) NULL,
	[MaLoaiHD] [nvarchar](50) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[ThoiGianBaoTruoc] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiHopDongTA] [nvarchar](50) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_TB_LoaiHopDong] PRIMARY KEY CLUSTERED 
(
	[LoaiHopDongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiKhamSucKhoe]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiKhamSucKhoe](
	[LoaiKhamSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiKhamSucKhoeTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_QTLoaiKhamSucKhoe] PRIMARY KEY CLUSTERED 
(
	[LoaiKhamSucKhoeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiNghiPhep]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiNghiPhep](
	[LoaiNghiPhepID] [int] NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[STT] [nvarchar](50) NULL,
	[SoNgayNghiToiDa] [int] NULL,
	[MaLoai] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiNghiPhepTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsLoaiNghiPhep] PRIMARY KEY CLUSTERED 
(
	[LoaiNghiPhepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiQuyetDinh]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiQuyetDinh](
	[LoaiQuyetDinhID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiQuyetDinhTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsDanhMucQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[LoaiQuyetDinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLoaiThiThuc]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLoaiThiThuc](
	[LoaiThiThucID] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiThiThuc] [nvarchar](50) NULL,
	[TenLoaiThiThucTA] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsLoaiThiThuc] PRIMARY KEY CLUSTERED 
(
	[LoaiThiThucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLyDoChuyenCanBo]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLyDoChuyenCanBo](
	[LyDoChuyenCanBoID] [int] IDENTITY(1,1) NOT NULL,
	[TenLyDoChuyenCanBo] [nvarchar](50) NULL,
	[TenLyDoChuyenCanBoTA] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsLyDoChuyenCanBo] PRIMARY KEY CLUSTERED 
(
	[LyDoChuyenCanBoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLyDoCongTac]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLyDoCongTac](
	[LyDoCongTacID] [int] IDENTITY(1,1) NOT NULL,
	[MaLyDoCongTac] [nvarchar](50) NULL,
	[TenLyDoCongTac] [nvarchar](50) NULL,
	[TenLyDoCongTacTA] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsLyDoCongTac] PRIMARY KEY CLUSTERED 
(
	[LyDoCongTacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLyDoNghiViec]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLyDoNghiViec](
	[LyDoNghiViecID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLyDoNghiViecTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsLyDoThoiViec] PRIMARY KEY CLUSTERED 
(
	[LyDoNghiViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLyDoTangCa]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLyDoTangCa](
	[LyDoTangCaID] [int] IDENTITY(1,1) NOT NULL,
	[TenLyDoTangCa] [nvarchar](50) NULL,
	[TenLyDoTangCaTA] [nvarchar](50) NULL,
	[MaLyDoTangCa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsLyDoTangCa] PRIMARY KEY CLUSTERED 
(
	[LyDoTangCaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsLyDoThayDoiLuong]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsLyDoThayDoiLuong](
	[LyDoThayDoiLuongID] [int] IDENTITY(1,1) NOT NULL,
	[TenLyDoThayDoiLuong] [nvarchar](200) NULL,
	[TenLyDoThayDoiLuongTA] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsLyDoThayDoiLuong] PRIMARY KEY CLUSTERED 
(
	[LyDoThayDoiLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsMangNghiepVu]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsMangNghiepVu](
	[MangNghiepVuID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](1000) NULL,
 CONSTRAINT [PK_NS_DsMangNghiepVu] PRIMARY KEY CLUSTERED 
(
	[MangNghiepVuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNangLuc]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNangLuc](
	[NangLucID] [int] NOT NULL,
	[NhomNangLucID] [int] NULL,
	[MaNangLuc] [nvarchar](50) NULL,
	[TenNangLuc] [nvarchar](50) NULL,
	[MoTa] [nvarchar](500) NULL,
	[NangLucCotLoi] [bit] NULL,
	[TrongSo] [int] NULL,
	[DiemKyVong] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNangLuc] PRIMARY KEY CLUSTERED 
(
	[NangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNangLuc_MoTa]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNangLuc_MoTa](
	[NangLucMoTaID] [int] NOT NULL,
	[NangLucID] [int] NULL,
	[TenMoTa] [nvarchar](550) NULL,
	[DiemSo] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNangLuc_MoTa] PRIMARY KEY CLUSTERED 
(
	[NangLucMoTaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNgachCongChuc]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNgachCongChuc](
	[NgachCongChucID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenMaNgach] [nvarchar](255) NULL,
	[ThoiHanNangLuong] [varchar](10) NULL,
	[Ma] [varchar](10) NULL,
	[HeSoCoBan] [float] NULL,
	[ThuTu] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNgachCongChucTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsNgachCongChuc] PRIMARY KEY CLUSTERED 
(
	[NgachCongChucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNganhHoc]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNganhHoc](
	[NganhHocID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenNganhHoc] [nvarchar](500) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNganhHocTA] [nvarchar](500) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_GH_DsNganhHoc] PRIMARY KEY CLUSTERED 
(
	[NganhHocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNgheNghiep]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNgheNghiep](
	[NgheNghiepID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenNgheNghiep] [nvarchar](100) NULL,
	[GhiChu] [nvarchar](300) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNgheNghiepTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NgheNghiep] PRIMARY KEY CLUSTERED 
(
	[NgheNghiepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNgoaiNgu]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNgoaiNgu](
	[NgoaiNguID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenNgoaiNgu] [nvarchar](50) NULL,
	[Ma] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNgoaiNguTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsNgoaiNgu] PRIMARY KEY CLUSTERED 
(
	[NgoaiNguID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNgoaiNgu_TrinhDo]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNgoaiNgu_TrinhDo](
	[TrinhDoNgoaiNguID] [int] IDENTITY(1,1) NOT NULL,
	[NgoaiNguID] [int] NULL,
	[TenTrinhDoNgoaiNgu] [nvarchar](50) NULL,
	[MaTrinhDoNgoaiNgu] [nvarchar](50) NULL,
	[TenTrinhDoNgoaiNguTA] [nvarchar](50) NULL,
	[TenNgoaiNguTA] [nvarchar](50) NULL,
	[Level] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNgoaiNgu_TrinhDo] PRIMARY KEY CLUSTERED 
(
	[TrinhDoNgoaiNguID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNguoiThamKhao]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNguoiThamKhao](
	[NguoiThamKhaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
	[NoiLamViec] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsNguoiThamKhao] PRIMARY KEY CLUSTERED 
(
	[NguoiThamKhaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNguonHoSo]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNguonHoSo](
	[NguonHoSoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNguonHoSoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsNguonHoSo] PRIMARY KEY CLUSTERED 
(
	[NguonHoSoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_dsnhombiendongluong_ghichu]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_dsnhombiendongluong_ghichu](
	[nhombiendong] [int] NOT NULL,
	[tennhombiendong] [nvarchar](100) NULL,
 CONSTRAINT [PK_ns_dsnhombiendongluong_ghichu] PRIMARY KEY CLUSTERED 
(
	[nhombiendong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNhomCaLamViec]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNhomCaLamViec](
	[NhomCaLamViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomCaLamViec] [nvarchar](50) NULL,
	[MaNhomCaLamViec] [nvarchar](50) NULL,
	[TenNhomCaLamViecTA] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNhomCaLamViec] PRIMARY KEY CLUSTERED 
(
	[NhomCaLamViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNhomChucDanh]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNhomChucDanh](
	[NhomChucDanhID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomChucDanh] [nvarchar](50) NULL,
	[MaNhomChucDanh] [nvarchar](50) NULL,
	[TenNhomChucDanhTA] [nvarchar](50) NULL,
	[ThuTu] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNhomChucDanh] PRIMARY KEY CLUSTERED 
(
	[NhomChucDanhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ns_DsNhomGhiChuLuong]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ns_DsNhomGhiChuLuong](
	[NhomGhiChuID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomGhiChuLuong] [nvarchar](150) NULL,
 CONSTRAINT [PK_Ns_DsNhomGhiChuLuong] PRIMARY KEY CLUSTERED 
(
	[NhomGhiChuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsNhomNangLuc]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsNhomNangLuc](
	[NhomNangLucID] [int] NOT NULL,
	[MaNhomNangLuc] [nvarchar](50) NULL,
	[TenNhomNangLuc] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNhomNangLuc] PRIMARY KEY CLUSTERED 
(
	[NhomNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsQuanHuyen]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsQuanHuyen](
	[QuanHuyenID] [int] IDENTITY(1,1) NOT NULL,
	[TinhID] [int] NULL,
	[TenQuanHuyen] [nvarchar](50) NULL,
	[MaHuyen] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LuongToiThieu] [decimal](18, 3) NULL,
	[LuongToiDa] [decimal](18, 3) NULL,
 CONSTRAINT [PK_GH_DsQuanHuyen] PRIMARY KEY CLUSTERED 
(
	[QuanHuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsQuocTich]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsQuocTich](
	[QuocTichID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenNuoc] [nvarchar](50) NULL,
	[MaNuoc] [varchar](20) NULL,
	[Field1] [varchar](50) NULL,
	[Field2] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsNuoc] PRIMARY KEY CLUSTERED 
(
	[QuocTichID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsSoQuyetDinh]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsSoQuyetDinh](
	[SoQuyetDinhID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](500) NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NguoiQuyetDinh] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](100) NULL,
	[TomTatNoiDung] [nvarchar](250) NULL,
	[LoaiQuyetDinhID] [int] NULL,
	[NgayBatDauHieuLuc] [datetime] NULL,
	[NgayHetHieuLuc] [datetime] NULL,
	[NgayTao] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[NgaySua] [datetime] NULL,
	[NguoiSuaID] [int] NULL,
	[TrangThai] [int] NULL,
	[LoaiQuyetDinh] [nvarchar](100) NULL,
	[NguoiQuyetDinhID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[FileQuyetDinh] [image] NULL,
	[TenFile] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsSoQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[SoQuyetDinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsThamNien]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsThamNien](
	[ThamNienID] [int] IDENTITY(1,1) NOT NULL,
	[TuNam] [decimal](10, 2) NULL,
	[ToiNam] [decimal](10, 2) NULL,
	[HeSo] [decimal](10, 4) NULL,
	[Name] [nvarchar](50) NULL,
	[ThuTu] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ThamNien] PRIMARY KEY CLUSTERED 
(
	[ThamNienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_dsThangLuongCb]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_dsThangLuongCb](
	[CbID] [int] NOT NULL,
	[bangluong] [nvarchar](10) NULL,
	[bac] [decimal](10, 2) NULL,
	[heso] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[CbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_dsThangLuongKD]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_dsThangLuongKD](
	[KdID] [int] NOT NULL,
	[bangluong] [nvarchar](20) NULL,
	[bac] [int] NULL,
	[heso] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[KdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsThonXom]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsThonXom](
	[ThonXomID] [int] IDENTITY(1,1) NOT NULL,
	[TenThonXom] [nvarchar](50) NULL,
	[MaThonXom] [nvarchar](50) NULL,
	[XaPhuongID] [int] NULL,
 CONSTRAINT [PK_NS_DsThonXom] PRIMARY KEY CLUSTERED 
(
	[ThonXomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsThuTucNghiViec]    Script Date: 6/21/2018 11:30:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsThuTucNghiViec](
	[ThuTucNghiViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenHoanTatNghiViec] [nvarchar](250) NULL,
	[NhomThuTuc] [nvarchar](50) NULL,
	[NhomThuTucTA] [nvarchar](50) NULL,
	[TenHoanTatNghiViecTA] [nvarchar](250) NULL,
	[MaHoanTatNghiViec] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsThuTucNghiViec] PRIMARY KEY CLUSTERED 
(
	[ThuTucNghiViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTinh]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTinh](
	[TinhID] [int] IDENTITY(1,1) NOT NULL,
	[TenTinh] [nvarchar](50) NULL,
	[Ma] [varchar](20) NOT NULL,
	[MaVung] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_GH_DsTinh] PRIMARY KEY CLUSTERED 
(
	[TinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTinHoc]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTinHoc](
	[TinHocID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[Ten] [nvarchar](100) NULL,
	[Ma] [varchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenTinHocTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DSTinHoc] PRIMARY KEY CLUSTERED 
(
	[TinHocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTinHoc_TrinhDo]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTinHoc_TrinhDo](
	[TrinhDoTinHocID] [int] IDENTITY(1,1) NOT NULL,
	[TinHocID] [int] NULL,
	[TenTrinhDoTinHoc] [nvarchar](50) NULL,
	[MaTrinhDoTinHoc] [nvarchar](50) NULL,
	[TenTrinhDoTinHocTA] [nvarchar](50) NULL,
	[Level] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DsTinHoc_TrinhDo] PRIMARY KEY CLUSTERED 
(
	[TrinhDoTinHocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTonGiao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTonGiao](
	[TonGiaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenTonGiaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TonGiao] PRIMARY KEY CLUSTERED 
(
	[TonGiaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTrangThai]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTrangThai](
	[TrangThaiID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](255) NULL,
	[CongtyID] [int] NULL,
	[Ma] [nchar](1) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenTrangThaiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsTrangThai] PRIMARY KEY CLUSTERED 
(
	[TrangThaiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTrangThaiTinhLuong]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTrangThaiTinhLuong](
	[TrangThaiTinhLuongID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsTrangThaiTinhLuong] PRIMARY KEY CLUSTERED 
(
	[TrangThaiTinhLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTrinhDo]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTrinhDo](
	[TrinhDoID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenTrinhDo] [nvarchar](100) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenTrinhDoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DsTrinhDo] PRIMARY KEY CLUSTERED 
(
	[TrinhDoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsTruongDaoTao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsTruongDaoTao](
	[TruongDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](500) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenTruongDaoTaoTA] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_DsTruongDaoTao] PRIMARY KEY CLUSTERED 
(
	[TruongDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsVanBang]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsVanBang](
	[VanBangID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[TenVanBang] [nvarchar](50) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Level] [int] NULL,
	[TenVanBangTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_GH_DsVanBang] PRIMARY KEY CLUSTERED 
(
	[VanBangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DsXaPhuong]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DsXaPhuong](
	[XaPhuongID] [int] IDENTITY(1,1) NOT NULL,
	[QuanHuyenID] [int] NULL,
	[TenXaPhuong] [nvarchar](50) NULL,
	[MaXaPhuong] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Keyword] [nvarchar](250) NULL,
	[TinhID] [int] NULL,
	[XaHuyenTinh] [nvarchar](1000) NULL,
 CONSTRAINT [PK_NS_DsXaPhuong] PRIMARY KEY CLUSTERED 
(
	[XaPhuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_CauHoiTracNghiem]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_CauHoiTracNghiem](
	[CauHoiTracNghiemID] [int] IDENTITY(1,1) NOT NULL,
	[TenCauHoi] [nvarchar](200) NULL,
	[DapAn1] [nvarchar](200) NULL,
	[DapAn2] [nvarchar](200) NULL,
	[DapAn3] [nvarchar](200) NULL,
	[DapAn4] [nvarchar](200) NULL,
	[DapAn5] [nvarchar](200) NULL,
	[DapAnDung] [int] NULL,
	[DienGiai] [nvarchar](500) NULL,
	[KhoaDaoTaoID] [int] NULL,
 CONSTRAINT [PK_NS_DT_CauHoiTracNghiem] PRIMARY KEY CLUSTERED 
(
	[CauHoiTracNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DanhGiaCongTacToChuc]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DanhGiaCongTacToChuc](
	[DanhGiaCongTacToChucID] [int] IDENTITY(1,1) NOT NULL,
	[DotDaoTaoID] [int] NULL,
	[TieuChiDanhGiaID] [int] NULL,
	[NhanXet] [nvarchar](50) NULL,
	[DanhGia] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NoiDungID] [int] NULL,
	[NhanVienID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DT_DanhGiaCongTacToChuc] PRIMARY KEY CLUSTERED 
(
	[DanhGiaCongTacToChucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DanhMucDaoTao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DanhMucDaoTao](
	[DanhMucDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenDanhMucDaoTaoTA] [nvarchar](50) NULL,
	[TenDanhMucDaoTao] [nvarchar](50) NULL,
	[MaDanhMucDaoTao] [nvarchar](50) NULL,
	[ChuyenNganhID] [int] NULL,
 CONSTRAINT [PK_NS_DT_DanhMucDaoTao] PRIMARY KEY CLUSTERED 
(
	[DanhMucDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao](
	[DotDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[HinhThucDaoTaoID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[VanBangID] [int] NULL,
	[KeHoachID] [int] NULL,
	[NuocDaoTaoID] [int] NULL,
	[ChungChiID] [int] NULL,
	[LopDaoTao] [nvarchar](50) NULL,
	[NoiDaoTao] [nvarchar](50) NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[ChiPhiCongTy] [int] NULL,
	[ChiPhiCaNhan] [int] NULL,
	[ChiPhiKhac] [int] NULL,
	[YCKhoaHoc] [nvarchar](50) NULL,
	[MoTa] [nvarchar](500) NULL,
	[TrangThai] [int] NULL,
	[NoiDungDaoTao] [ntext] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[YeuCauKhoaHocID] [int] NULL,
	[NguoiHuongDan] [nvarchar](50) NULL,
	[LanhDaoYeuCau] [nvarchar](500) NULL,
	[ChiPhiDaoTao] [decimal](18, 0) NULL,
	[NgayGuiBaoCaoDaoTao] [datetime] NULL,
	[ChucVuNguoiHuongDan] [nvarchar](50) NULL,
	[DoiTuongDaoTao] [nvarchar](50) NULL,
	[DienThoaiNoiDT] [nvarchar](50) NULL,
	[NguoiLienHe] [nvarchar](50) NULL,
	[DienThoaiNLH] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenDotDaoTaoTA] [nvarchar](50) NULL,
	[DangKyTuNgay] [datetime] NULL,
	[DangKyToiNgay] [datetime] NULL,
	[ChoPhepNhanVienDangKy] [bit] NULL,
	[SoLuongHocVien] [int] NULL,
	[NgayHetHan] [datetime] NULL,
	[NgayCapChungChi] [datetime] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao] PRIMARY KEY CLUSTERED 
(
	[DotDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao_ChiPhi]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao_ChiPhi](
	[ChiPhiDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[MoTa] [nvarchar](500) NULL,
	[DotDaoTaoID] [int] NULL,
	[SoTien] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_ChiPhi] PRIMARY KEY CLUSTERED 
(
	[ChiPhiDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao_NhanVien]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao_NhanVien](
	[DotDaoTao_NhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[DotDaoTaoID] [int] NULL,
	[NhanVienID] [int] NULL,
	[TenChucVu] [nvarchar](50) NULL,
	[KetQuaDaoTaoID] [int] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[NgayThem] [datetime] NULL,
	[NguoiThemID] [int] NULL,
	[NgayGuiBaoCaoDaoTao] [datetime] NULL,
	[UuDiem] [nvarchar](50) NULL,
	[NhuocDiem] [nvarchar](50) NULL,
	[NhanXet] [nvarchar](50) NULL,
	[DeXuat] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_NhanVien] PRIMARY KEY CLUSTERED 
(
	[DotDaoTao_NhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao_NhanVienDanhGia]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao_NhanVienDanhGia](
	[NhanVienDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[GV_DanhGiaID] [int] NULL,
	[GVNhanXet] [nvarchar](50) NULL,
	[BG_DanhGiaID] [int] NULL,
	[BGNhanXet] [nvarchar](50) NULL,
	[TC_DanhGiaID] [int] NULL,
	[TCDanhGia] [nvarchar](50) NULL,
	[NoiDungDaoTaoID] [int] NULL,
	[DotDaoTaoID] [int] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_NhanVienDanhGia] PRIMARY KEY CLUSTERED 
(
	[NhanVienDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao_NoiDung]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao_NoiDung](
	[NoiDungDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[DotDaoTaoID] [int] NULL,
	[NguoiDaoTao] [nvarchar](50) NULL,
	[DonViDaoTao] [nvarchar](500) NULL,
	[GiaoTrinhTaiLieu] [ntext] NULL,
	[ThoiGian] [int] NULL,
	[TuNgay] [datetime] NULL,
	[DenNgay] [datetime] NULL,
	[ChucVu] [nvarchar](50) NULL,
	[ChiPhi] [decimal](18, 0) NULL,
	[LienHe] [nvarchar](50) NULL,
	[UuDiemNDT] [nvarchar](500) NULL,
	[NhuocDiemNDT] [nvarchar](500) NULL,
	[NhanXetNDT] [nvarchar](500) NULL,
	[DeXuatNDT] [nvarchar](500) NULL,
	[DonViDaoTaoID] [int] NULL,
	[NguoiDaoTao_ChucVu] [nvarchar](250) NULL,
	[NguoiDaoTao_LienHe] [nvarchar](250) NULL,
	[NguoiDaoTao_UuDiem] [nvarchar](250) NULL,
	[NguoiDaoTao_NhuocDiem] [nvarchar](250) NULL,
	[NguoiDaoTao_NhanXet] [nvarchar](250) NULL,
	[DeXuatNguoiDaoTao] [nvarchar](250) NULL,
	[DanhMucDaoTaoID] [int] NULL,
	[SoThangCamKetSauDaoTao] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[IsChiPhiCaNhan] [bit] NULL,
	[FileDaoTao] [image] NULL,
	[FileName] [nvarchar](50) NULL,
	[KhoaDaoTaoID] [int] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_NoiDung] PRIMARY KEY CLUSTERED 
(
	[NoiDungDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DotDaoTao_NoiDung_CauHoi]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DotDaoTao_NoiDung_CauHoi](
	[NoiDungCauHoiTracNghiemID] [int] IDENTITY(1,1) NOT NULL,
	[NoiDungDaoTaoID] [int] NULL,
	[DapAn1] [nvarchar](200) NULL,
	[DapAn2] [nvarchar](200) NULL,
	[DapAn3] [nvarchar](200) NULL,
	[DapAn4] [nvarchar](200) NULL,
	[DapAn5] [nvarchar](200) NULL,
	[DapAnDung] [int] NULL,
	[DienGiai] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_NoiDung_CauHoi] PRIMARY KEY CLUSTERED 
(
	[NoiDungCauHoiTracNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_DsTieuChiDanhGia]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_DsTieuChiDanhGia](
	[TieuChiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[TenTieuChi] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[IsUse] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_DT_DsTieuChiDanhGia] PRIMARY KEY CLUSTERED 
(
	[TieuChiDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_KeHoach]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_KeHoach](
	[KeHoachID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[NgayLap] [datetime] NULL,
	[NguoiLapID] [int] NULL,
	[PhongBanLapID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[VanBangID] [int] NULL,
	[SoLuong] [int] NULL,
	[ChiPhiCongTy] [int] NULL,
	[NoiDaoTao] [nvarchar](50) NULL,
	[ChiPhiCaNhan] [int] NULL,
	[ChiPhiKhac] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[TrangThai] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[HinhThucDaoTaoID] [int] NULL,
	[CongTyID] [int] NULL,
	[CapDuocDaoTao] [nvarchar](50) NULL,
	[DoiTuongDaoTao] [nvarchar](50) NULL,
	[NoiDung] [nvarchar](500) NULL,
	[KeHoachChaID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKeHoachDaoTaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DT_KeHoach] PRIMARY KEY CLUSTERED 
(
	[KeHoachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_KeHoach_PhongBan]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_KeHoach_PhongBan](
	[KeHoach_PhongBanID] [int] IDENTITY(1,1) NOT NULL,
	[KeHoachID] [int] NULL,
	[PhongBanID] [int] NULL,
	[SoLuong] [int] NULL,
 CONSTRAINT [PK_NS_DT_KeHoach_PhongBan] PRIMARY KEY CLUSTERED 
(
	[KeHoach_PhongBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_KeHoachNhuCau]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_KeHoachNhuCau](
	[KeHoachNhuCauID] [int] IDENTITY(1,1) NOT NULL,
	[KeHoachID] [int] NULL,
	[NhuCauID] [int] NULL,
	[SoLuong] [int] NULL,
 CONSTRAINT [PK_NS_DT_KeHoachNhuCau] PRIMARY KEY CLUSTERED 
(
	[KeHoachNhuCauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_KetQuaDaoTao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_KetQuaDaoTao](
	[KetQuaDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[STT] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKetQuaDaoTaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DT_KetQuaDaoTao] PRIMARY KEY CLUSTERED 
(
	[KetQuaDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_KhoaDaoTao]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_KhoaDaoTao](
	[KhoaDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenKhoaDaoTao] [nvarchar](50) NULL,
	[MaKhoaDaoTao] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKhoaDaoTaoTA] [nvarchar](50) NULL,
	[ChuyenNganhID] [int] NULL,
	[NhomNangLucID] [int] NULL,
	[ThoiLuong] [int] NULL,
	[DonViDaoTao] [nvarchar](500) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_NS_DT_KhoaDaoTao] PRIMARY KEY CLUSTERED 
(
	[KhoaDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_NhanVien_TraLoiTracNghiem]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_NhanVien_TraLoiTracNghiem](
	[NhanVienTraLoiTracNghiemID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CauHoiTracNghiemID] [int] NULL,
	[DapAn] [int] NULL,
	[IsCorrect] [bit] NULL,
	[DotDaoTaoID] [int] NULL,
 CONSTRAINT [PK_NS_DT_NhanVien_TraLoiTracNghiem] PRIMARY KEY CLUSTERED 
(
	[NhanVienTraLoiTracNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_NhuCau]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_NhuCau](
	[NhuCauID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[PhongBanID] [int] NULL,
	[NoiDung] [nvarchar](1000) NULL,
	[SoLuongNhanVien] [int] NULL,
	[NgayLap] [datetime] NULL,
	[NguoiYeuCauID] [int] NULL,
	[NguoiLapID] [int] NULL,
	[GiaoTrinhDaoTao] [ntext] NULL,
	[HinhThucDaoTaoID] [int] NULL,
	[NgayCan] [datetime] NULL,
	[QuyTrinhDaoTao] [ntext] NULL,
	[TrangThai] [int] NULL,
	[SoLuongDuyet] [int] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[NgayXetDuyet] [datetime] NULL,
	[YKienXetDuyet] [nvarchar](250) NULL,
	[ThoiGianDaoTao] [nvarchar](50) NULL,
	[NhanSuDuKienDaoTao] [nvarchar](500) NULL,
	[ChiPhiDaoTao] [nvarchar](50) NULL,
	[DonViDaoTao] [nvarchar](250) NULL,
	[YKienQTNS] [nvarchar](250) NULL,
	[GioDaoTao] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[DiaDiemDaoTao] [nvarchar](50) NULL,
	[KeHoachID] [int] NULL,
	[DoiTuongDaoTao] [nvarchar](50) NULL,
	[TenNhuCauTA] [nvarchar](50) NULL,
	[LyDoDaoTao] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_DT_NhuCau] PRIMARY KEY CLUSTERED 
(
	[NhuCauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_NhuCau_NhanVien]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_NhuCau_NhanVien](
	[NhuCau_NhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[NhuCauID] [int] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_DT_NhuCau_NhanVien] PRIMARY KEY CLUSTERED 
(
	[NhuCau_NhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_DT_NoiDung_KetQua]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_DT_NoiDung_KetQua](
	[NoiDung_KetQuaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NoiDungDaoTaoID] [int] NULL,
	[KetQuaDaoTaoID] [int] NULL,
	[DiemSo] [decimal](18, 2) NULL,
	[GioBatDau] [datetime] NULL,
	[GioKetThuc] [datetime] NULL,
 CONSTRAINT [PK_NS_DT_DotDaoTao_NoiDung_KetQua] PRIMARY KEY CLUSTERED 
(
	[NoiDung_KetQuaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ns_GhiChuBienDongNhanSu]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ns_GhiChuBienDongNhanSu](
	[NV_BDNSID] [int] IDENTITY(1,1) NOT NULL,
	[TuThang] [datetime] NULL,
	[ToiThang] [datetime] NULL,
	[GhiChuBDNSID] [int] NULL,
	[GhiChu] [nvarchar](100) NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_Ns_GhiChuBienDongNhanSu] PRIMARY KEY CLUSTERED 
(
	[NV_BDNSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_HoachDinhNhanSu]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_HoachDinhNhanSu](
	[HoachDinhNhanSuID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[SoLuong] [int] NULL,
	[Nam] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_HopDong]    Script Date: 6/21/2018 11:30:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_HopDong](
	[HopDongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[MaHopDong] [nvarchar](50) NULL,
	[NoiDung] [ntext] NULL,
	[MucLuong] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[KyHDLanThu] [int] NULL,
	[ChucVu] [nvarchar](50) NULL,
	[NguoiKyHD] [nvarchar](50) NULL,
	[NgayKyHD] [datetime] NULL,
	[GhiChu] [ntext] NULL,
	[FileHopDong] [image] NULL,
	[ViTriHopDong] [nvarchar](100) NULL,
	[HinhThucHD] [nvarchar](100) NULL,
	[ChucVuID] [int] NULL,
	[LoaiHopDongID] [int] NULL,
	[CacLoaiPhuCap] [nvarchar](50) NULL,
	[BacLuong] [nvarchar](50) NULL,
	[HeSoLuong] [nvarchar](50) NULL,
	[PhongBanID] [int] NULL,
	[UuDiem] [nvarchar](2000) NULL,
	[NhuocDiem] [nvarchar](2000) NULL,
	[KetLuan] [nvarchar](2000) NULL,
	[TenFileHD] [nvarchar](50) NULL,
	[ChucDanhID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[NguoiKyChucDanhID] [int] NULL,
	[DiaDiemLamViec] [nvarchar](100) NULL,
	[ThoiGianLamViec] [nvarchar](100) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[PDFFile] [image] NULL,
	[XetDuyet] [int] NULL,
	[HopDongChinhID] [int] NULL,
 CONSTRAINT [PK_TB_HopDong] PRIMARY KEY CLUSTERED 
(
	[HopDongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_HopDongUyQuyen]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_HopDongUyQuyen](
	[HopDongUyQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiKyID] [int] NULL,
	[ThongTin] [nvarchar](100) NULL,
	[NgayKetThuc] [datetime] NULL,
	[NgayBatDau] [datetime] NULL,
 CONSTRAINT [PK_NS_HopDongUyQuyen] PRIMARY KEY CLUSTERED 
(
	[HopDongUyQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KANG_DoanhThuKhuVuc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KANG_DoanhThuKhuVuc](
	[DoanhThuKhuVucID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TongDoanhThu] [decimal](10, 3) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
 CONSTRAINT [PK_NS_KANG_DoanhThuKhuVuc] PRIMARY KEY CLUSTERED 
(
	[DoanhThuKhuVucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KANG_DoanhThuKyThuat]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KANG_DoanhThuKyThuat](
	[DoanhThuKyThuatID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TongTienThuVe] [decimal](10, 3) NULL,
	[TongTienTru] [decimal](10, 3) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
 CONSTRAINT [PK_NS_KANG_DoanhThuKyThuat] PRIMARY KEY CLUSTERED 
(
	[DoanhThuKyThuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KANG_DoanhThuNhomSP]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KANG_DoanhThuNhomSP](
	[DoanhThuNhomSPID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TongDoanhThu] [decimal](10, 3) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
 CONSTRAINT [PK_NS_KANG_DoanhThuNhomSP] PRIMARY KEY CLUSTERED 
(
	[DoanhThuNhomSPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KANG_DoanhThuSieuThiDaiLy]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KANG_DoanhThuSieuThiDaiLy](
	[DoanhThuSieuThiDaiLyID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[IsSieuThi] [bit] NULL,
	[DoanhThu] [decimal](10, 3) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
 CONSTRAINT [PK_NS_KANG_DoanhThuSieuThiDaiLy] PRIMARY KEY CLUSTERED 
(
	[DoanhThuSieuThiDaiLyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KD_DinhMucNam]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KD_DinhMucNam](
	[DinhMucNamID] [int] IDENTITY(1,1) NOT NULL,
	[Nam] [int] NULL,
	[PhongBanID] [int] NULL,
	[GiaTri] [decimal](10, 0) NULL,
 CONSTRAINT [PK_NS_KD_DinhMucNam] PRIMARY KEY CLUSTERED 
(
	[DinhMucNamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KD_DinhMucThang]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KD_DinhMucThang](
	[DinhMucThangID] [int] IDENTITY(1,1) NOT NULL,
	[DinhMucNamID] [int] NULL,
	[Thang] [int] NULL,
	[GiaTri] [decimal](10, 0) NULL,
	[GiaTriDM] [decimal](10, 0) NULL,
	[Nam] [int] NULL,
	[GiaTriThucTe] [decimal](10, 0) NULL,
	[DinhMuc] [decimal](10, 0) NULL,
	[DangKyKHKD] [decimal](10, 0) NULL,
	[GiaTriThucHienDinhMuc] [decimal](10, 0) NULL,
	[PhongBanID] [int] NULL,
 CONSTRAINT [PK_NS_KD_DinhMucThang] PRIMARY KEY CLUSTERED 
(
	[DinhMucThangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KD_KetQuaKinhDoanh]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KD_KetQuaKinhDoanh](
	[KetQuaKinhDoanhID] [int] IDENTITY(1,1) NOT NULL,
	[DinhMucThangID] [int] NULL,
	[GiaTri] [decimal](10, 0) NULL,
 CONSTRAINT [PK_NS_KD_KetQuaKinhDoanh] PRIMARY KEY CLUSTERED 
(
	[KetQuaKinhDoanhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_KetQuaDanhGiaNhanVien]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_KetQuaDanhGiaNhanVien](
	[KqDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NgayDanhGia] [datetime] NULL,
	[KetQuaDanhGia] [nvarchar](50) NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_KetQuaDanhGiaNhanVien] PRIMARY KEY CLUSTERED 
(
	[KqDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_News_Article]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_News_Article](
	[ArticleId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](4000) NULL,
	[ArticleContent] [nvarchar](4000) NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CategoryId] [int] NULL,
	[Image] [varchar](255) NULL,
	[HotNews] [bit] NULL,
	[HotEvent] [bit] NULL,
	[ShowAtSlider] [bit] NULL,
	[Author] [nvarchar](255) NULL,
 CONSTRAINT [PK_NS_News_Article] PRIMARY KEY CLUSTERED 
(
	[ArticleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_News_Category]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_News_Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[ShowHomePage] [bit] NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_News_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_ChuHoKhau]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_ChuHoKhau](
	[ChuHoKhauID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[HoVaTen] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[NgaySinh] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[SoHoKhau] [nvarchar](50) NULL,
	[LoaiGiayToID] [int] NULL,
	[DiaChi] [nvarchar](250) NULL,
	[SoNha] [nvarchar](50) NULL,
	[ThonXomID] [int] NULL,
	[XaPhuongID] [int] NULL,
	[QuanHuyenID] [int] NULL,
	[TinhID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_ChuHo] PRIMARY KEY CLUSTERED 
(
	[ChuHoKhauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_ChuyenMon]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_ChuyenMon](
	[ChuyenMonNhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[ChuyenMonID] [int] NULL,
	[NhanVienID] [int] NULL,
	[BangCap] [nvarchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_TB_ChuyenMonNhanVien] PRIMARY KEY CLUSTERED 
(
	[ChuyenMonNhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_DanhGiaNangLuc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_DanhGiaNangLuc](
	[DanhGiaNangLucID] [int] IDENTITY(1,1) NOT NULL,
	[NangLucID] [int] NULL,
	[DiemSo] [decimal](10, 0) NULL,
	[DiemKyVong] [decimal](10, 0) NULL,
	[NhomNangLucID] [int] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[KyDanhGiaNangLucID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_DanhGiaNangLuc] PRIMARY KEY CLUSTERED 
(
	[DanhGiaNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_DatCoc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_DatCoc](
	[DatCocID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgayDatCoc] [datetime] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[SoTien] [decimal](18, 0) NULL,
	[Type] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NhanVien_DatCoc] PRIMARY KEY CLUSTERED 
(
	[DatCocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_FileHoSo]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_FileHoSo](
	[HoSoID] [int] IDENTITY(1,1) NOT NULL,
	[TenFile] [nvarchar](50) NULL,
	[Data] [image] NULL,
	[NhanVienID] [int] NULL,
	[FileDate] [datetime] NULL,
	[NgayUpload] [datetime] NULL,
	[NguoiThemID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_FileHoSo] PRIMARY KEY CLUSTERED 
(
	[HoSoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_Follow]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_Follow](
	[NhanVienFollowID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[NhanVienID] [int] NULL,
	[Status] [int] NULL,
	[Priority] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVienFollow] PRIMARY KEY CLUSTERED 
(
	[NhanVienFollowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_HoSoThuTuc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_HoSoThuTuc](
	[NhanVien_HoSoThuTucID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[HoSoThuTucID] [int] NULL,
	[NgayNop] [datetime] NULL,
	[NguoiNhan] [nvarchar](150) NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[HanNop] [datetime] NULL,
	[DaNop] [bit] NULL,
	[NguoiNhanID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NguoiTra] [nvarchar](150) NULL,
	[NgayTra] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_HoSoThuTuc] PRIMARY KEY CLUSTERED 
(
	[NhanVien_HoSoThuTucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_KyDanhGiaNangLuc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_KyDanhGiaNangLuc](
	[KyDanhGiaNangLucID] [int] IDENTITY(1,1) NOT NULL,
	[MaKyDanhGia] [nvarchar](50) NULL,
	[NgayDanhGia] [datetime] NULL,
	[NguoiDanhGiaID] [int] NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Quy] [nchar](10) NULL,
	[Nam] [nchar](10) NULL,
	[NhomNangLucID] [int] NULL,
	[KyDanhGia] [int] NULL,
	[TongDiem] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_KyDanhGiaNangLuc] PRIMARY KEY CLUSTERED 
(
	[KyDanhGiaNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_KyQuyetDinh]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_KyQuyetDinh](
	[NhanVienKyQuyetDinhID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiQuyetDinhID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NhanVien_KyQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[NhanVienKyQuyetDinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_LoaiThongTin]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_LoaiThongTin](
	[LoaiThongTinID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiLapID] [int] NULL,
	[TenLoaiThongTin] [nvarchar](50) NULL,
	[NgayLap] [datetime] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[KieuDuLieu] [nvarchar](50) NULL,
	[ValueField] [nvarchar](50) NULL,
	[NameField] [nvarchar](50) NULL,
	[SQLQuery] [nvarchar](500) NULL,
	[AllowNull] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_LoaiThongTin] PRIMARY KEY CLUSTERED 
(
	[LoaiThongTinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_MayChamCong]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_MayChamCong](
	[NhanVienMayChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[MayChamCongID] [int] NULL,
	[MaChamCong] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_NhanVien_MayChamCong] PRIMARY KEY CLUSTERED 
(
	[NhanVienMayChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_NangLuc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_NangLuc](
	[NhanVienNangLucID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NangLucID] [int] NULL,
	[TrongSo] [decimal](10, 0) NULL,
	[DiemKyVong] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_NangLuc] PRIMARY KEY CLUSTERED 
(
	[NhanVienNangLucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_NganhHoc]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_NganhHoc](
	[NhanVienNganhHocID] [int] IDENTITY(1,1) NOT NULL,
	[NganhHocID] [int] NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[TruongDaoTaoID] [int] NULL,
	[NamTotNghiep] [int] NULL,
 CONSTRAINT [PK_NhanVienNghanhHoc] PRIMARY KEY CLUSTERED 
(
	[NhanVienNganhHocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_NgheNghiep]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_NgheNghiep](
	[NgheNghiepNhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[NgheNghiepID] [int] NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NgheNghiepNhanVien] PRIMARY KEY CLUSTERED 
(
	[NgheNghiepNhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_NghiViec]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_NghiViec](
	[NhanVienNghiViecID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[LyDoNghiViecID] [int] NULL,
	[NguoiQuyetDinh] [nvarchar](50) NULL,
	[NgayHieuLuc] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NgayDiLamLai] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[DaCoNguoiThayThe] [bit] NULL,
	[NguoiQuyetDinhID] [int] NULL,
	[TrangThaiCapNhat] [int] NULL,
	[HinhThucNghiViecID] [int] NULL,
 CONSTRAINT [PK_GH_NhanVienThoiViec] PRIMARY KEY CLUSTERED 
(
	[NhanVienNghiViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_NgoaiNgu]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_NgoaiNgu](
	[NhanVienNgoaiNguID] [int] IDENTITY(1,1) NOT NULL,
	[NgoaiNguID] [int] NULL,
	[NhanVienID] [int] NULL,
	[TrinhDo] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NoiDaoTao] [nvarchar](150) NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_TB_NgoaiNguNhanVien] PRIMARY KEY CLUSTERED 
(
	[NhanVienNgoaiNguID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_SoQuyetDinh]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_SoQuyetDinh](
	[SoQuyetDinhID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NhanVien_SoQuyetDinhID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_NS_NhanVien_SoQuyetDinh] PRIMARY KEY CLUSTERED 
(
	[NhanVien_SoQuyetDinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_TheoDoiNghiPhep]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_TheoDoiNghiPhep](
	[TheoDoiNghiPhepID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NamTheoDoi] [int] NULL,
	[SoNgayNghiDuocPhep] [decimal](18, 2) NULL,
	[SoNgayPhepDaNghi] [decimal](18, 2) NULL,
	[SoNgayPhepConLai] [decimal](18, 2) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_TheoDoiNghiPhep] PRIMARY KEY CLUSTERED 
(
	[TheoDoiNghiPhepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_ThongTinKhac]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_ThongTinKhac](
	[ThongTinKhacID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiThongTinID] [int] NULL,
	[GiaTri] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_ThongTinKhac] PRIMARY KEY CLUSTERED 
(
	[ThongTinKhacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_ThuChi]    Script Date: 6/21/2018 11:30:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_ThuChi](
	[ThuChiID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[KhoanMucThuChiID] [int] NULL,
	[Ngay] [datetime] NULL,
	[SoTien] [decimal](18, 0) NULL,
	[NguoiThucHienID] [int] NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
	[DienGiai] [nvarchar](150) NULL,
	[Status] [int] NULL,
	[NhanVien_ThuChiID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_ThuChi] PRIMARY KEY CLUSTERED 
(
	[ThuChiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVien_ThuTucNghiViec]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVien_ThuTucNghiViec](
	[NhanVienThuTucNghiViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienNghiViecID] [int] NULL,
	[ThucTucNghiViecID] [int] NULL,
	[NgayHoanThanh] [datetime] NULL,
	[NguoiKiemTraID] [int] NULL,
	[NguoiThucHienID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhanVien_ThuTucNghiViec] PRIMARY KEY CLUSTERED 
(
	[NhanVienThuTucNghiViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVienThoiViec]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVienThoiViec](
	[NhanVienThoiViecID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[LyDo] [nvarchar](250) NULL,
	[NguoiQuyetDinh] [nvarchar](50) NULL,
	[NgayHieuLuc] [datetime] NULL,
	[CapQuyetDinh] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhanVienTruongDuLieu]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhanVienTruongDuLieu](
	[NhanVienTruongDuLieuID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TruongDuLieuID] [int] NULL,
	[CongTyID] [int] NULL,
	[GhiChuID] [int] NULL,
	[GiaTri] [image] NULL,
 CONSTRAINT [PK_NS_NhanVienTruongDuLieu] PRIMARY KEY CLUSTERED 
(
	[NhanVienTruongDuLieuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhomBaoCao]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhomBaoCao](
	[NhomBaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[ParentID] [int] NULL,
 CONSTRAINT [PK_NSS_NhomBaoCao] PRIMARY KEY CLUSTERED 
(
	[NhomBaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_NhomCaLamViec]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_NhomCaLamViec](
	[NhomCaLamViecID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomCaLamViec] [nvarchar](50) NULL,
	[MaNhomCaLamViec] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_NhomCaLamViec] PRIMARY KEY CLUSTERED 
(
	[NhomCaLamViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_PT_KhaoSat]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_PT_KhaoSat](
	[KhaoSatID] [int] IDENTITY(1,1) NOT NULL,
	[TieuDe] [nvarchar](255) NULL,
	[NoiDung] [nvarchar](500) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[NhieuLuaChon] [bit] NULL,
 CONSTRAINT [PK_NS_KhaoSat] PRIMARY KEY CLUSTERED 
(
	[KhaoSatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_PT_KhaoSat_ChiTiet]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_PT_KhaoSat_ChiTiet](
	[ChiTietKhaoSatID] [int] IDENTITY(1,1) NOT NULL,
	[KhaoSatID] [int] NULL,
	[NoiDung] [nvarchar](500) NULL,
	[KetQuaKhaoSat] [int] NULL,
 CONSTRAINT [PK_NS_KhaoSat_ChiTiet] PRIMARY KEY CLUSTERED 
(
	[ChiTietKhaoSatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_PT_KhaoSat_NhanVien]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_PT_KhaoSat_NhanVien](
	[KhaoSatNhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[KhaoSatID] [int] NULL,
	[ChiTietID] [int] NULL,
	[KetQuaKhaoSat] [bit] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_PT_KhaoSat_NhanVien] PRIMARY KEY CLUSTERED 
(
	[KhaoSatNhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_qt_duan_dienbienluong]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_qt_duan_dienbienluong](
	[QTLuongDuAnID] [int] IDENTITY(1,1) NOT NULL,
	[DuAnID] [int] NULL,
	[TenChucDanhKN] [nvarchar](500) NULL,
	[TuThang] [datetime] NULL,
	[ToiThang] [datetime] NULL,
	[PhuCapDuAn] [decimal](18, 2) NULL,
	[nhanvienid] [int] NULL,
 CONSTRAINT [PK__ns_qt_du__9B9923FE0E44098D] PRIMARY KEY CLUSTERED 
(
	[QTLuongDuAnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTBoiDuong]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTBoiDuong](
	[QTBoiDuongId] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NOT NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[NoiBoiDuong] [nvarchar](200) NULL,
	[NoiDungBoiDuong] [nvarchar](500) NULL,
	[HinhThucBoiDuong] [nvarchar](200) NULL,
	[GhiChu] [ntext] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_QTBoiDuong] PRIMARY KEY CLUSTERED 
(
	[QTBoiDuongId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTCaLamViec]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTCaLamViec](
	[QTCaLamViecID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CaLamViecID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTCaLamViec] PRIMARY KEY CLUSTERED 
(
	[QTCaLamViecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTChiLuongDot1]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTChiLuongDot1](
	[TrangThaiTinhLuongDot1ID] [int] IDENTITY(1,1) NOT NULL,
	[TrangThaiTinhLuongID] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[createdbyid] [int] NULL,
	[createddate] [datetime] NULL,
	[modifybyid] [int] NULL,
	[modifydate] [datetime] NULL,
	[TrangThaiChoPheDuyetID] [int] NULL,
	[XetDuyet] [int] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_NS_QTChiLuongDot1] PRIMARY KEY CLUSTERED 
(
	[TrangThaiTinhLuongDot1ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTChuyenCanBo]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTChuyenCanBo](
	[QTChuyenCanBoID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NhanVienID] [int] NULL,
	[PhongBanCuID] [int] NULL,
	[PhongBanMoiID] [int] NULL,
	[NgayChuyen] [datetime] NULL,
	[TenPhongBanCu] [nvarchar](2500) NULL,
	[TenPhongBanMoi] [nvarchar](2500) NULL,
	[NgoaiCongTy] [bit] NULL,
	[LyDoChuyen] [nvarchar](1000) NULL,
	[XetDuyet] [int] NULL,
	[ChucVuCuID] [int] NULL,
	[TenChucVuMoi] [nvarchar](2500) NULL,
	[ChucVuMoiID] [int] NULL,
	[TenChucVuCu] [nvarchar](2500) NULL,
	[NghiViec] [bit] NOT NULL,
	[SoQuyetDinh] [nvarchar](500) NULL,
	[DonViDi] [nvarchar](2500) NULL,
	[DonViDen] [nvarchar](2500) NULL,
	[ChucVuOldID] [int] NULL,
	[TenChucVuNew] [nvarchar](2500) NULL,
	[ChucVuNewID] [int] NULL,
	[TenChucVuOLD] [nvarchar](2500) NULL,
	[ThoiHanThuThach] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ChucDanhCuID] [int] NULL,
	[ChucDanhMoiID] [int] NULL,
	[TenChucDanhCu] [nvarchar](500) NULL,
	[TenChucDanhMoi] [nvarchar](500) NULL,
	[LyDoChuyenID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[NgayHieuLuc] [datetime] NULL,
	[Type] [int] NULL,
	[TrangThaiCapNhat] [int] NULL,
 CONSTRAINT [PK_GH_QTChuyenCanBo] PRIMARY KEY CLUSTERED 
(
	[QTChuyenCanBoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTCongTac]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTCongTac](
	[QTCongTacID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[NoiCongTac] [nvarchar](200) NULL,
	[ChucVuCaoNhat] [nvarchar](200) NULL,
	[CongViecCuThe] [nvarchar](200) NULL,
	[GhiChu] [ntext] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[NgayQuyetDinh] [date] NULL,
	[NgayHieuLuc] [date] NULL,
	[LyDoCongTacID] [int] NULL,
 CONSTRAINT [PK_QTCongTac] PRIMARY KEY CLUSTERED 
(
	[QTCongTacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTCongTacNuocNgoai]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTCongTacNuocNgoai](
	[QTCongTacNuocNgoaiID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NuocCongTac] [nvarchar](50) NULL,
	[TenCoQuanNuocNgoai] [nvarchar](255) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[MucDichCongTac] [nvarchar](500) NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](250) NULL,
	[NuocCongTacID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[XetDuyet] [bit] NULL,
 CONSTRAINT [PK_QTCongTacNuocNgoai] PRIMARY KEY CLUSTERED 
(
	[QTCongTacNuocNgoaiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTDaoTao]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTDaoTao](
	[QTDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[DotDaoTaoID] [int] NULL,
	[NamNhapHoc] [int] NULL,
	[NamKetThuc] [int] NULL,
	[NoiDaoTao] [nvarchar](500) NULL,
	[NghanhDaoTao] [nvarchar](100) NULL,
	[ChuyenNganhDaoTao] [nvarchar](100) NULL,
	[HinhThucDaoTao] [nvarchar](200) NULL,
	[BangCap] [nvarchar](200) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[HinhThucDaoTaoID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[VanBangID] [int] NULL,
	[KeHoachID] [int] NULL,
	[NuocDaoTaoID] [int] NULL,
	[ChungChiID] [int] NULL,
	[KetQuaDaoTaoID] [int] NULL,
	[LopDaoTao] [nvarchar](500) NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[ChiPhiCongTy] [int] NULL,
	[ChiPhiCaNhan] [int] NULL,
	[ChiPhiKhac] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[NoiDungDaoTao] [ntext] NULL,
	[SauKhiVaoCongTy] [bit] NULL,
	[DiemSo] [nvarchar](50) NULL,
	[NoiDungDaoTaoID] [int] NULL,
	[DanhMucDaoTaoID] [int] NULL,
	[DoiTacDaoTaoID] [int] NULL,
	[SoThangCamKetSauDaoTao] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TruongDaoTaoID] [int] NULL,
	[HeDaoTaoID] [int] NULL,
	[ChuyenNganhID] [int] NULL,
	[XetDuyet] [int] NULL,
	[KhoaDaoTaoID] [int] NULL,
	[NgayHetHan] [datetime] NULL,
	[NgayCapChungChi] [datetime] NULL,
 CONSTRAINT [PK_QTDaoTao] PRIMARY KEY CLUSTERED 
(
	[QTDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTDienBienLuong]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTDienBienLuong](
	[QTDienBienLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[CongTyID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[SoQuyetDinh] [varchar](500) NULL,
	[NgayHuong] [datetime] NULL,
	[NoiLamViec] [nvarchar](1000) NULL,
	[LyDo] [nvarchar](1500) NULL,
	[XetDuyet] [int] NULL,
	[NhomTinhLuongID] [int] NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[ThangLuongID] [int] NULL,
	[MaLoaiLuong] [varchar](1250) NULL,
	[LuongCoBan] [int] NULL,
	[LuongBHXH] [int] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LyDoThayDoiLuongID] [int] NULL,
	[NgayHuongMoi] [datetime] NULL,
	[NhomTinhLuongMoiID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[MaPBanHachToan] [nvarchar](500) NULL,
	[MaCNhanhHachToan] [nvarchar](500) NULL,
	[ChucDanhID] [int] NULL,
	[PhongBanCuID] [int] NULL,
	[TrangThaiChiLuongID] [int] NULL,
	[TrangThaiChiLuongCuID] [int] NULL,
	[NgayHetHan] [datetime] NULL,
	[LayCongTheoNhomCongThuc] [bit] NULL,
	[NgayHuongBackDate] [datetime] NULL,
 CONSTRAINT [PK_TB_LuongNhanVien] PRIMARY KEY CLUSTERED 
(
	[QTDienBienLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTDienBienLuongChiTiet]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTDienBienLuongChiTiet](
	[QTDienBienLuongChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiLuongID] [int] NULL,
	[QTDienBienLuongID] [int] NULL,
	[TienLuong] [char](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[GiaTri] [decimal](18, 5) NULL,
	[GiaTriMoi] [decimal](18, 5) NULL,
	[GiaTriStr] [nvarchar](1500) NULL,
	[GiaTriStrMoi] [nvarchar](1500) NULL,
 CONSTRAINT [PK_NS_TL_ChiTietQTLuong] PRIMARY KEY CLUSTERED 
(
	[QTDienBienLuongChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTDongBHTN]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTDongBHTN](
	[QTDongBHTNID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetthuc] [datetime] NULL,
	[DonViCongViecChucVu] [nvarchar](255) NULL,
	[SoNam] [int] NULL,
	[SoThang] [int] NULL,
	[MucLuong] [decimal](18, 2) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NguoiThamGiaID] [int] NULL,
	[ChiTietCongViec] [nvarchar](255) NULL,
	[TyleDong] [decimal](18, 3) NULL,
	[DonViID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_QTDongBHTN] PRIMARY KEY CLUSTERED 
(
	[QTDongBHTNID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTDongBHXH]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTDongBHXH](
	[QTDongBHXHID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetthuc] [datetime] NULL,
	[DonViCongViecChucVu] [nvarchar](255) NULL,
	[SoNam] [int] NULL,
	[SoThang] [int] NULL,
	[MucLuong] [decimal](18, 2) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NguoiThamGiaID] [int] NULL,
	[ChiTietCongViec] [nvarchar](255) NULL,
	[TyleDong] [decimal](18, 3) NULL,
	[isDongBHXH] [bit] NULL,
	[DaBaoCao] [bit] NULL,
	[IsDongBHTN] [bit] NULL,
	[IsDongTuNguyen] [bit] NULL,
	[DonViID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LuongToiThieu] [int] NULL,
	[NgayBatDau_BD] [datetime] NULL,
	[NgayKetThuc_BD] [datetime] NULL,
	[MucLuongBD] [decimal](18, 0) NULL,
	[HeSo] [decimal](18, 2) NULL,
	[TyleBD] [decimal](18, 2) NULL,
	[HeSoPCCV] [decimal](18, 2) NULL,
	[NgayTraTheBHYT] [datetime] NULL,
	[KhongTraTheBHYT] [bit] NULL,
	[NgayLapBD] [datetime] NULL,
	[DotXetDuyetBD] [nvarchar](50) NULL,
	[LyDoDieuChinhBD] [nvarchar](50) NULL,
	[Ngach] [nvarchar](50) NULL,
	[Bac] [nvarchar](50) NULL,
	[ThangBaoCao] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_QTDongBHXH] PRIMARY KEY CLUSTERED 
(
	[QTDongBHXHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTHoatDongDoanThe]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTHoatDongDoanThe](
	[QTHoatDongDoanTheID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NOT NULL,
	[NgayThamGia] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[TenDoanThe] [nvarchar](200) NULL,
	[ChucVuCaoNhat] [nvarchar](100) NULL,
	[LyDoThamGia] [nvarchar](200) NULL,
	[NhanXet] [nvarchar](200) NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [ntext] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NgayChinhThuc] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_QTHoatDongDoanThe] PRIMARY KEY CLUSTERED 
(
	[QTHoatDongDoanTheID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKetQuaDanhGiaNhanVien]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKetQuaDanhGiaNhanVien](
	[DanhGiaNVID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[KyDanhGia] [datetime] NULL,
	[TenKyDanhGia] [nvarchar](200) NULL,
	[Diem] [nvarchar](10) NULL,
	[XepLoai] [nvarchar](100) NULL,
	[HeSoQuyDoi] [nvarchar](10) NULL,
	[SoKyDanhGia] [nvarchar](10) NULL,
 CONSTRAINT [PK_NS_QTKetQuaDanhGiaNhanVien] PRIMARY KEY CLUSTERED 
(
	[DanhGiaNVID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKetQuaKhamSucKhoe]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKetQuaKhamSucKhoe](
	[QTKetQuaKhamSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[KetQua] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[DotKhamSucKhoeID] [int] NULL,
	[LoaiKhamSucKhoeID] [int] NULL,
	[NhanVienID] [int] NULL,
	[Ten] [nvarchar](50) NULL,
	[KhamSucKhoeID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTKetQuaKhamSucKhoe] PRIMARY KEY CLUSTERED 
(
	[QTKetQuaKhamSucKhoeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKhac]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKhac](
	[QTKhacID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiQTID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[KetQua] [nvarchar](200) NULL,
	[GhiChu] [nvarchar](200) NULL,
	[DiaDiem] [nvarchar](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTKhac] PRIMARY KEY CLUSTERED 
(
	[QTKhacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKhamSucKhoe]    Script Date: 6/21/2018 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKhamSucKhoe](
	[QTKhamSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgayKham] [datetime] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NoiKham] [nvarchar](100) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKhenThuong]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKhenThuong](
	[QTKhenThuongID] [int] IDENTITY(1,1) NOT NULL,
	[SoQuyetDinhID] [int] NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinh] [varchar](500) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[HinhThucKhenThuong] [nvarchar](800) NULL,
	[CoQuanKhenThuong] [nvarchar](500) NULL,
	[NguoiKy] [nvarchar](1000) NULL,
	[LyDoKhenThuong] [nvarchar](1200) NULL,
	[GhiChu] [ntext] NULL,
	[XetDuyet] [int] NULL,
	[NgayKhenThuong] [datetime] NULL,
	[NguoiKyID] [int] NULL,
	[SoTienThuong] [decimal](18, 0) NULL,
	[HinhThucKhenThuongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThiSinhID] [int] NULL,
	[LyDoKhenThuongID] [int] NULL,
 CONSTRAINT [PK_QTKhenThuong] PRIMARY KEY CLUSTERED 
(
	[QTKhenThuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKhenThuongCon]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKhenThuongCon](
	[KhenThuongConID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TenCon] [nvarchar](50) NULL,
	[LyDoKhenThuong] [nvarchar](250) NULL,
	[NgayKhenThuong] [datetime] NULL,
	[CoQuanKhenThuong] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTKhenThuongCon] PRIMARY KEY CLUSTERED 
(
	[KhenThuongConID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKhongDongBHXH]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKhongDongBHXH](
	[QTKhongDongBHXHID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NguoiThamGiaID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[GhiChu] [nvarchar](150) NULL,
	[LyDo] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_QTKhongDongBHXH] PRIMARY KEY CLUSTERED 
(
	[QTKhongDongBHXHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKiemNhiem]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKiemNhiem](
	[QTKiemNhiemID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[ChucVuKiemNhiemID] [int] NULL,
	[GhiChu] [nvarchar](2000) NULL,
	[PhongBanKiemNhiemID] [int] NULL,
	[SoQuyetDinh] [nvarchar](100) NULL,
	[TenChucVuKiemNhiem] [nvarchar](1000) NULL,
	[SoCPKiemNhiem] [int] NULL,
	[ThuLaoKiemNhiem] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[NgayQuyetDinh] [datetime] NULL,
 CONSTRAINT [PK_NS_QTKiemNhiem] PRIMARY KEY CLUSTERED 
(
	[QTKiemNhiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKinhNghiem]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKinhNghiem](
	[KinhNghiemID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[NoiLamViec] [nvarchar](2500) NULL,
	[ChucVu] [nvarchar](2500) NULL,
	[GhiChu] [nvarchar](2500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThiSinhID] [int] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_QTKinhNghiem] PRIMARY KEY CLUSTERED 
(
	[KinhNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTKyLuat]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTKyLuat](
	[QTKyLuatID] [int] IDENTITY(1,1) NOT NULL,
	[SoQuyetDinhID] [int] NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayKyLuat] [datetime] NULL,
	[QuanKyLuat] [nvarchar](1500) NULL,
	[HinhThucKyLuat] [nvarchar](1500) NULL,
	[LyDoKyLuat] [ntext] NULL,
	[NguoiKy] [nvarchar](1500) NULL,
	[GhiChu] [nvarchar](1500) NULL,
	[XetDuyet] [int] NULL,
	[SoTienPhat] [decimal](18, 0) NULL,
	[SoQuyetDinh] [nvarchar](1500) NULL,
	[NguoiKyID] [int] NULL,
	[HinhThucKyLuatID] [int] NULL,
	[CoQuanKyLuat] [nvarchar](1500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LyDoKyLuatID] [int] NULL,
	[NgayHetHan] [datetime] NULL,
 CONSTRAINT [PK_QTKyLuat] PRIMARY KEY CLUSTERED 
(
	[QTKyLuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTLoaiKhamSucKhoe]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTLoaiKhamSucKhoe](
	[QTLoaiKhamSucKhoeID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTLoaiQuaTrinh]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTLoaiQuaTrinh](
	[LoaiQTID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiLapID] [int] NULL,
	[TenLoaiQT] [nvarchar](50) NULL,
	[NgayLap] [datetime] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_LoaiQuaTrinh] PRIMARY KEY CLUSTERED 
(
	[LoaiQTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTNghienCuuKhoaHoc]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTNghienCuuKhoaHoc](
	[QTNgienCuuKHID] [int] IDENTITY(1,1) NOT NULL,
	[SoQuyetDinhID] [int] NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[CapQuanLy] [nvarchar](255) NULL,
	[CapChuTri] [nvarchar](255) NULL,
	[ChucDanhThamGia] [nvarchar](255) NULL,
	[TenDeTaiDuAn] [nvarchar](255) NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTNghienCuuKhoaHoc] PRIMARY KEY CLUSTERED 
(
	[QTNgienCuuKHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTNghiPhep]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTNghiPhep](
	[QTNghiPhepID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NguoiKy] [nvarchar](100) NULL,
	[LyDoNghi] [nvarchar](500) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NguoiThamGiaBHID] [int] NULL,
	[Type] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[TenCoQuanYTe] [nvarchar](500) NULL,
	[MucDongBHXH] [decimal](18, 0) NULL,
	[SoTienDuocHuong] [decimal](18, 3) NULL,
	[SoNgayHuongTroCap] [int] NULL,
	[SoNgayNghi] [decimal](18, 3) NULL,
	[SoNgayNghi1] [int] NULL,
	[SoNgayNghi2] [int] NULL,
	[NguoiLapID] [int] NULL,
	[NgayLap] [datetime] NULL,
	[NguoiSuaID] [int] NULL,
	[NgaySua] [datetime] NULL,
	[DaBaoCao] [bit] NULL,
	[ThoiGianDongBHXH] [nvarchar](50) NULL,
	[SoNgayNghiLuyKeTuDauNam] [int] NULL,
	[SoNgayNghiTapChung] [int] NULL,
	[MucLuongToiThieu] [decimal](18, 0) NULL,
	[DieuKienTinhHuong] [nvarchar](250) NULL,
	[KyHieuChamCongID] [int] NULL,
	[SoNgayDuocNghiThem] [decimal](18, 2) NULL,
	[DonViID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[SoTienDuocHuongBD] [decimal](18, 0) NULL,
	[NgayLapBD] [datetime] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_QTNghiPhep] PRIMARY KEY CLUSTERED 
(
	[QTNghiPhepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTPhuCap]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTPhuCap](
	[QTPhuCapID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayHuong] [datetime] NULL,
	[LoaiPhuCap] [nvarchar](100) NULL,
	[MaPhuCap] [varchar](50) NULL,
	[HeSo] [int] NULL,
	[SoTienTuongUng] [int] NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[LoaiPhuCapID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_QTPhuCap] PRIMARY KEY CLUSTERED 
(
	[QTPhuCapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTQuanHeGiaDinh]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTQuanHeGiaDinh](
	[QTQuanHeGiaDinhID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[QuanHe] [nvarchar](100) NULL,
	[HoTen] [nvarchar](200) NULL,
	[NgaySinh] [datetime] NULL,
	[NgheNghiepHienNay] [nvarchar](1000) NULL,
	[DiaChiThuongTru] [nvarchar](1000) NULL,
	[GhiChu] [ntext] NULL,
	[GioiTinh] [nvarchar](50) NULL,
	[DienThoaiLienHe] [nvarchar](50) NULL,
	[GiamTru] [bit] NULL,
	[NgayBatDauGiamTru] [datetime] NULL,
	[NgayKetThucGiamTru] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThiSinhID] [int] NULL,
	[MaSoThue] [nvarchar](150) NULL,
	[CMTND] [nvarchar](150) NULL,
	[QuocTich] [nvarchar](150) NULL,
	[GiayKhaiSinhSo] [nvarchar](150) NULL,
	[QuyenSo] [nvarchar](150) NULL,
	[NoiDangKyGiayKhaiSinh] [nvarchar](150) NULL,
	[XetDuyet] [int] NULL,
	[TinhTrangNhanThan] [nvarchar](150) NULL,
	[IsDied] [bit] NULL,
	[IsLienLacKhanCap] [bit] NULL,
	[NgayHieuLucNuoiConNho] [datetime] NULL,
	[NgayHieuLucTroCapConNho] [datetime] NULL,
	[ThonXomID] [int] NULL,
	[XaPhuongID] [int] NULL,
	[QuanHuyenID] [int] NULL,
	[TinhID] [int] NULL,
	[QuanHe_ChuHo] [nvarchar](100) NULL,
	[MaSoBHXH] [nvarchar](100) NULL,
	[SoSoBHXH] [nvarchar](50) NULL,
	[LoaiGiayToID] [int] NULL,
	[SoHoKhau] [nvarchar](50) NULL,
	[NgaySinh_Loai] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_QTQuanHeGiaDinh] PRIMARY KEY CLUSTERED 
(
	[QTQuanHeGiaDinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTQuanHeThanNhanNuocNgoai]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTQuanHeThanNhanNuocNgoai](
	[QTQuanHeThanNhanNuocNgoaiID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[HoTen] [nvarchar](255) NULL,
	[NamSinh] [int] NULL,
	[QuanHe] [nvarchar](100) NULL,
	[NuocDinhCu] [nvarchar](100) NULL,
	[QuocTich] [nvarchar](100) NULL,
	[NamDinhCu] [int] NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](250) NULL,
	[GioiTinh] [bit] NULL,
	[NuocDinhCuID] [int] NULL,
	[QuocTichID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_QTQuanHeThanNhanNuocNgoai] PRIMARY KEY CLUSTERED 
(
	[QTQuanHeThanNhanNuocNgoaiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTThaiSan]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTThaiSan](
	[QTThaiSanID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[QuyetDinhSo] [nvarchar](100) NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NguoiKy] [nvarchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[SinhConLanThu] [int] NULL,
	[GhiChu] [nvarchar](1500) NULL,
	[XetDuyet] [int] NULL,
	[NgaySinhCon] [datetime] NULL,
	[DaDuocThanhToanBHXH] [bit] NULL,
	[TongSoTienThanhToan] [money] NULL,
	[NguoiThamGiaBHID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[TenCon] [nvarchar](500) NULL,
	[TinhTrangKhiSinh] [nvarchar](500) NULL,
	[SoNgayNghiTaiGia] [int] NULL,
	[SoNgayNghiTapChung] [int] NULL,
	[MucLuongDongBHXH] [decimal](18, 0) NULL,
	[MucLuongBinhQuan] [decimal](18, 0) NULL,
	[MucLuongToiThieu] [decimal](18, 0) NULL,
	[TenConThuHai] [nvarchar](500) NULL,
	[SoLuongCon] [int] NULL,
	[IsNhanNuoiCon] [bit] NULL,
	[NgayLap] [datetime] NULL,
	[NguoiLapID] [int] NULL,
	[TuoiConKhiNhanNuoi] [int] NULL,
	[IsDie] [bit] NULL,
	[NgayConChet] [datetime] NULL,
	[NghiDuongSuc] [bit] NULL,
	[ThoiGianDongBHXH] [nvarchar](500) NULL,
	[TongSoThang] [int] NULL,
	[CongTyID] [int] NULL,
	[DaBaoCao] [bit] NULL,
	[TenConThuBa] [nvarchar](500) NULL,
	[TenConThuTu] [nvarchar](500) NULL,
	[DonViID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[MucLuongBinhQuanBD] [int] NULL,
	[TongSoTienThanhToanBD] [money] NULL,
	[NgayLapBD] [datetime] NULL,
	[NgayDuKienSinh] [datetime] NULL,
	[NgayDiLamTroLai] [datetime] NULL,
 CONSTRAINT [PK_GH_QTThaiSan] PRIMARY KEY CLUSTERED 
(
	[QTThaiSanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_QTThamGiaLLVT]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_QTThamGiaLLVT](
	[QTThamGiaLLVTID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayNhapNgu] [datetime] NULL,
	[NgayXuatNgu] [datetime] NULL,
	[QuanHam] [nvarchar](50) NULL,
	[NgheNghiepChucVu] [nvarchar](255) NULL,
	[GhiChuID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](250) NULL,
	[QuanHamID] [int] NULL,
	[ChucVuQuanDoiID] [int] NULL,
	[DonViCongTacID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_QTThamGiaLLVT] PRIMARY KEY CLUSTERED 
(
	[QTThamGiaLLVTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_SHB_DonViLamViec]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_SHB_DonViLamViec](
	[DonvilamviecID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](2000) NULL,
 CONSTRAINT [PK_NS_SHB_DonViLamViec] PRIMARY KEY CLUSTERED 
(
	[DonvilamviecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_shb_dsduan]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_shb_dsduan](
	[DuAnID] [int] IDENTITY(1,1) NOT NULL,
	[TenDuAn] [nvarchar](300) NULL,
 CONSTRAINT [PK__ns_shb_d__2007B3CB19B5BC39] PRIMARY KEY CLUSTERED 
(
	[DuAnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_SHB_NhomPhongBan]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_SHB_NhomPhongBan](
	[NhomphongbanID] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](2000) NULL,
	[Ma] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_SHB_NhomPhongBan] PRIMARY KEY CLUSTERED 
(
	[NhomphongbanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_ChiPhiTuyenDung]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_ChiPhiTuyenDung](
	[ChiPhiTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[DotTuyenDungID] [int] NULL,
	[MoTa] [nvarchar](500) NULL,
	[SoTien] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TD_ChiPhiTuyenDung] PRIMARY KEY CLUSTERED 
(
	[ChiPhiTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_DotTuyenDung]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_DotTuyenDung](
	[DotTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](100) NULL,
	[Ma] [nvarchar](100) NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[KyNangYeuCau] [ntext] NULL,
	[GioiTinh] [nvarchar](8) NULL,
	[DoTuoi] [nvarchar](100) NULL,
	[SoLuong] [int] NULL,
	[NgayTao] [datetime] NULL,
	[TrangThai] [tinyint] NULL,
	[GhiChu] [ntext] NULL,
	[KetThuc] [bit] NULL,
	[ChiPhiTuyenDung] [int] NULL,
	[VanBangID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[TinHocID] [int] NULL,
	[NgoaiNguID] [int] NULL,
	[KeHoachTuyenDungID] [int] NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[ViTriTuyenDungID] [int] NULL,
	[MucLuongKhoiDiem] [decimal](18, 0) NULL,
	[MoTaCongViec] [ntext] NULL,
	[SoNamKinhNghiem] [decimal](18, 0) NULL,
	[LoaiHinhCongViec] [nvarchar](100) NULL,
	[DaoTaoSauTuyenDung] [bit] NULL,
	[MaSoDoiTacCap] [nvarchar](150) NULL,
	[NgayNhanHoSo] [datetime] NULL,
	[DonViDangTuyen] [nvarchar](250) NULL,
	[NgayDuKienDiLam] [datetime] NULL,
	[HinhThucTuyenDungID] [int] NULL,
	[PhongBanID] [int] NULL,
	[SoLuongDangTuyen] [int] NULL,
	[NguoiYeuCau] [nvarchar](500) NULL,
	[LyDoTuyenDung] [nvarchar](500) NULL,
	[PhuongAnTuyenDung] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[CongTyID] [int] NULL,
	[TenDotTuyenDungTA] [nvarchar](100) NULL,
 CONSTRAINT [PK_NS_DanhSachDotTuyenDung] PRIMARY KEY CLUSTERED 
(
	[DotTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_HinhThucTuyenDung]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_HinhThucTuyenDung](
	[HinhThucTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenHinhThucTuyenDungTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_HinhThucTuyenDung] PRIMARY KEY CLUSTERED 
(
	[HinhThucTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_HS_PhongVanDoiTac]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_HS_PhongVanDoiTac](
	[PhongVanDoiTacID] [int] IDENTITY(1,1) NOT NULL,
	[NgayHen1] [datetime] NULL,
	[KetQua1] [nvarchar](50) NULL,
	[NgayHen2] [datetime] NULL,
	[KetQua2] [nvarchar](50) NULL,
	[NgayHen3] [datetime] NULL,
	[KetQua3] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_HS_PhongVanDoiTac] PRIMARY KEY CLUSTERED 
(
	[PhongVanDoiTacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_KeHoachNhuCau]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_KeHoachNhuCau](
	[KeHoachNhuCauID] [int] IDENTITY(1,1) NOT NULL,
	[KeHoachID] [int] NULL,
	[NhuCauID] [int] NULL,
	[SoLuong] [int] NULL,
 CONSTRAINT [PK_NS_TD_KeHoachNhuCau] PRIMARY KEY CLUSTERED 
(
	[KeHoachNhuCauID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_KeHoachTuyenDung]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_KeHoachTuyenDung](
	[KeHoachTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[SoLuong] [int] NULL,
	[YeuCau] [nvarchar](500) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NgayTao] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[KeHoachChaID] [int] NULL,
	[TrangThai] [int] NULL,
	[ChiPhiDuKien] [decimal](18, 0) NULL,
	[NguonDuKien] [nvarchar](250) NULL,
	[CongTyID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenKeHoachTuyenDungTA] [nvarchar](50) NULL,
	[ThoiGianDangTuyen] [datetime] NULL,
	[ThoiGianToChucTuyenDung] [datetime] NULL,
	[ThoiGianHoanThanh] [datetime] NULL,
 CONSTRAINT [PK_NS_KeHoachTuyenDung] PRIMARY KEY CLUSTERED 
(
	[KeHoachTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_KetQuaDanhGiaDaoTao]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_KetQuaDanhGiaDaoTao](
	[KetQuaDanhGiaDaoTaoID] [int] NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[Ma] [varchar](10) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TD_KetQuaDanhGiaDaoTao] PRIMARY KEY CLUSTERED 
(
	[KetQuaDanhGiaDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_LoaiDaoTao]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_LoaiDaoTao](
	[LoaiDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
	[MaLoai] [nvarchar](50) NULL,
	[STT] [int] NULL,
	[ChiLoadXepLoai] [bit] NULL,
	[ChoPhepChuyenTrungTuyen] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLoaiDaoTaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_LoaiDaoTao] PRIMARY KEY CLUSTERED 
(
	[LoaiDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_LoaiDaoTao_DotTuyenDung]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_LoaiDaoTao_DotTuyenDung](
	[LoaiDaoTaoDotTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiDaoTaoID] [int] NULL,
	[DotTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TD_LoaiDaoTao_DotTuyenDung] PRIMARY KEY CLUSTERED 
(
	[LoaiDaoTaoDotTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_LopDaoTao]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_LopDaoTao](
	[LopDaoTaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenLop] [nvarchar](50) NULL,
	[GiaoVienChuNhiemID] [int] NULL,
	[NgayKhaiGiang] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[TrangThai] [int] NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
	[NguoiSuaID] [int] NULL,
	[NgaySua] [datetime] NULL,
	[TongDiemYeuCau] [decimal](18, 0) NULL,
	[LoaiLopID] [int] NULL,
	[SoLuongHocVien] [int] NULL,
	[CongTyID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[DoiTac] [nvarchar](50) NULL,
	[BoPhan] [nvarchar](50) NULL,
	[MucDich] [nvarchar](50) NULL,
	[DiaDiem] [nvarchar](50) NULL,
	[DonViToChuc] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenLopDaoTaoTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_LopDaoTao] PRIMARY KEY CLUSTERED 
(
	[LopDaoTaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_LopDaoTao_DanhGia]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_LopDaoTao_DanhGia](
	[DanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[Ma] [varchar](10) NULL,
	[Ten] [nvarchar](50) NULL,
	[KetQuaDanhGiaDaoTaoID] [int] NULL,
 CONSTRAINT [PK_NS_TD_LopDaoTao_XepLoai] PRIMARY KEY CLUSTERED 
(
	[DanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_LopDaoTao_ThiSinh]    Script Date: 6/21/2018 11:30:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_LopDaoTao_ThiSinh](
	[LopDaoTaoThiSinhID] [int] IDENTITY(1,1) NOT NULL,
	[LopDaoTaoID] [int] NULL,
	[ThiSinhID] [int] NULL,
	[NhanXet] [nvarchar](500) NULL,
	[TongDiem] [decimal](18, 0) NULL,
	[XepLoaiID] [int] NULL,
	[KetLuan] [nvarchar](500) NULL,
	[DotTuyenDungID] [int] NULL,
	[TrangThai] [int] NULL,
	[ViTinh] [decimal](18, 0) NULL,
	[NguoiChuyeID] [int] NULL,
	[NgayChuyen] [datetime] NULL,
	[DiemKhac] [decimal](18, 0) NULL,
	[TrangThaiNghiID] [int] NULL,
	[Diem4] [decimal](18, 0) NULL,
	[Diem5] [decimal](18, 0) NULL,
	[Diem6] [decimal](18, 0) NULL,
	[MaSo] [varchar](50) NULL,
	[DanhGiaID] [int] NULL,
	[NguoiChuyenID] [int] NULL,
	[KetQuaDanhGiaDaoTaoID] [int] NULL,
	[GhiChu] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_LopDaoTao_ThiSinh] PRIMARY KEY CLUSTERED 
(
	[LopDaoTaoThiSinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_MonThi]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_MonThi](
	[MonThiID] [int] IDENTITY(1,1) NOT NULL,
	[TenMonThi] [nvarchar](50) NULL,
	[MaMonThi] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenMonThiTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_MonThi] PRIMARY KEY CLUSTERED 
(
	[MonThiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_MonThi_ThiSinh]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_MonThi_ThiSinh](
	[MonThiThiSinhID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[MonThiID] [int] NULL,
	[ThiSinhID] [int] NULL,
	[DiemThi] [decimal](18, 3) NULL,
	[LoaiKetQua] [nvarchar](50) NULL,
	[VongThiID] [int] NULL,
	[NguoiChamDiemID] [int] NULL,
	[Field1] [nvarchar](50) NULL,
	[Field2] [nvarchar](50) NULL,
	[Field3] [nvarchar](50) NULL,
	[NgayChamThi] [datetime] NULL,
 CONSTRAINT [PK_MonThiThiSinh] PRIMARY KEY CLUSTERED 
(
	[MonThiThiSinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_NguoiKiemTra]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_NguoiKiemTra](
	[NguoiKiemTraID] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](50) NULL,
	[DiaChi] [ntext] NULL,
	[DienThoai] [nvarchar](50) NULL,
	[ChucVu] [nvarchar](50) NULL,
 CONSTRAINT [PK_NguoiKiemTra] PRIMARY KEY CLUSTERED 
(
	[NguoiKiemTraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_NguoiKiemTra_DotPhongVan]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_NguoiKiemTra_DotPhongVan](
	[NguoiKiemTraDotPhongVanID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiKiemTraID] [int] NOT NULL,
	[DotPhongVanID] [int] NOT NULL,
	[GhiChu] [ntext] NULL,
 CONSTRAINT [PK_NS_NguoiKiemTra_DotPhongVan] PRIMARY KEY CLUSTERED 
(
	[NguoiKiemTraDotPhongVanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_NhomThiSinh]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_NhomThiSinh](
	[NhomThiSinhID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenNhomThiSinhTA] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TD_NhomThiSinh] PRIMARY KEY CLUSTERED 
(
	[NhomThiSinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_NhuCauTuyenDung]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_NhuCauTuyenDung](
	[NhuCauTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[NhanVienID] [int] NULL,
	[NgayLap] [datetime] NULL,
	[ChucDanhID] [int] NULL,
	[KyNangYeuCau] [ntext] NULL,
	[MucLuong] [int] NULL,
	[PhuCap] [int] NULL,
	[GioiTinh] [bit] NULL,
	[DoTuoi] [nvarchar](100) NULL,
	[SoLuongYC] [int] NULL,
	[TrangThai] [tinyint] NULL,
	[GhiChu] [ntext] NULL,
	[Ten] [nvarchar](100) NULL,
	[SoLuongXD] [int] NULL,
	[SoLuongTD] [int] NULL,
	[VanBangID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[SoNamKinhNghiem] [int] NULL,
	[TrinhDoNgoaiNguID] [int] NULL,
	[KinhNghiemLamViec] [nvarchar](500) NULL,
	[NgayCan] [datetime] NULL,
	[TinhTrangGiaDinh] [int] NULL,
	[YeuCauKhac] [nvarchar](2000) NULL,
	[DotTuyenDungID] [int] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[NgayXetDuyet] [datetime] NULL,
	[YKienXetDuyet] [nvarchar](250) NULL,
	[YKienQTNS] [nvarchar](250) NULL,
	[YKienTruongPhong] [nvarchar](500) NULL,
	[YKienBGD] [nvarchar](500) NULL,
	[MoTaCongViecChinh] [nvarchar](500) NULL,
	[CongTyID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[FileDinhKem] [image] NULL,
	[TrinhDoID] [int] NULL,
	[TenFileDinhKem] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_NhuCauTuyenDung] PRIMARY KEY CLUSTERED 
(
	[NhuCauTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_ThiSinh]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_ThiSinh](
	[ThiSinhID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [int] NULL,
	[DanTocID] [int] NULL,
	[VanBangID] [int] NULL,
	[NgoaiNguID] [int] NULL,
	[TinHocID] [int] NULL,
	[NghanhHocID] [int] NULL,
	[NgheNghiepID] [int] NULL,
	[TrinhDoID] [int] NULL,
	[TonGiaoID] [int] NULL,
	[NhomThiSinhID] [int] NULL,
	[HocKhoiID] [int] NULL,
	[TruongDaoTaoID] [int] NULL,
	[HeDaoTaoID] [int] NULL,
	[HangTotNghiepID] [int] NULL,
	[NguonHoSoID] [int] NULL,
	[SoBaoDanh] [varchar](50) NULL,
	[SoCMTND] [varchar](20) NULL,
	[TenHo] [nvarchar](30) NULL,
	[Ten] [nvarchar](30) NULL,
	[DienThoai] [varchar](20) NULL,
	[Mobile] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[DiaChiLienHe] [nvarchar](300) NULL,
	[NgaySinh] [datetime] NULL,
	[Anh] [image] NULL,
	[GioiTinh] [nvarchar](8) NULL,
	[NoiSinh] [nvarchar](200) NULL,
	[NgayVaoDang] [datetime] NULL,
	[SoNamKinhNghiem] [tinyint] NULL,
	[KinhNghiemLamViec] [ntext] NULL,
	[HocVan] [ntext] NULL,
	[KyNang] [ntext] NULL,
	[MucTieuNgheNghiep] [ntext] NULL,
	[NguoiThamKhao] [ntext] NULL,
	[MucLuongHienTai] [int] NULL,
	[MucLuongMongMuon] [int] NULL,
	[TrangThai] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NgayTaoHoSo] [datetime] NULL,
	[NgayCap] [datetime] NULL,
	[NoiCap] [nvarchar](250) NULL,
	[NguyenQuan] [nvarchar](250) NULL,
	[HoKhauThuongTru] [nvarchar](250) NULL,
	[NamTotNghiep] [datetime] NULL,
	[NguoiTaoID] [int] NULL,
	[NguoiSuaID] [int] NULL,
	[NgaySua] [datetime] NULL,
	[NgaySanSangDiLam] [datetime] NULL,
	[TinhTrangHonNhan] [nvarchar](50) NULL,
	[DeXuat] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NgoaiNgu2ID] [int] NULL,
	[ViTriDuTuyenID] [int] NULL,
	[ViTriDuTuyen2ID] [int] NULL,
	[ViTriDuTuyen3ID] [int] NULL,
	[NguoiThamKhao1] [nvarchar](250) NULL,
	[NguoiThamKhao2] [nvarchar](250) NULL,
	[NguoiThamKhao3] [nvarchar](250) NULL,
	[MP_NguoiKhaiThacID] [int] NULL,
	[CV] [image] NULL,
	[FileCV] [nvarchar](50) NULL,
	[DiaChi_SoNha] [nvarchar](50) NULL,
	[DiaChi_ThonXomID] [int] NULL,
	[DiaChi_XaPhuongID] [int] NULL,
	[DiaChi_QuanHuyenID] [int] NULL,
	[DiaChi_TinhID] [int] NULL,
	[DiaChiTT_SoNha] [nvarchar](50) NULL,
	[DiaChiTT_ThonXomID] [int] NULL,
	[DiaChiTT_XaPhuongID] [int] NULL,
	[DiaChiTT_QuanHuyenID] [int] NULL,
	[DiaChiTT_TinhID] [int] NULL,
	[PhongBanID] [int] NULL,
	[NhanVienID] [int] NULL,
 CONSTRAINT [PK_TB_ThiSinh] PRIMARY KEY CLUSTERED 
(
	[ThiSinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_ThiSinh_DotPhongVan]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_ThiSinh_DotPhongVan](
	[ThiSinhDotPhongVanID] [int] IDENTITY(1,1) NOT NULL,
	[ThiSinhID] [int] NOT NULL,
	[DotPhongVanID] [int] NOT NULL,
	[TrangThai] [bit] NULL,
	[TongDiem] [int] NULL,
 CONSTRAINT [PK_NS_ThiSinh_DotPhongVan] PRIMARY KEY CLUSTERED 
(
	[ThiSinhDotPhongVanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_ThiSinh_DotTuyenDung]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_ThiSinh_DotTuyenDung](
	[ThiSinhDotTuyenDungID] [int] IDENTITY(1,1) NOT NULL,
	[ThiSinhID] [int] NULL,
	[DotTuyenDungID] [int] NULL,
	[TrangThai] [int] NULL,
	[SoBaoDanh] [nvarchar](50) NULL,
	[MucLuongHienTai] [int] NULL,
	[MucLuongMongMuon] [int] NULL,
	[ChucVuID] [int] NULL,
	[ThoiGianHenPhongVan] [datetime] NULL,
	[CaLamViecID] [int] NULL,
	[PVLan1] [datetime] NULL,
	[DiemLan1] [nvarchar](50) NULL,
	[PVLan2] [datetime] NULL,
	[DiemLan2] [nvarchar](50) NULL,
	[PVLan3] [datetime] NULL,
	[DiemLan3] [nvarchar](50) NULL,
	[DoiTacID] [int] NULL,
	[TinhTrangUngVien] [nvarchar](150) NULL,
	[GhiChu] [nvarchar](150) NULL,
	[MaSoDoiTacCap] [nvarchar](150) NULL,
	[ThoiGianHenPV] [datetime] NULL,
	[ChiTieuThang] [nvarchar](50) NULL,
	[ThamKhaoTD] [nvarchar](50) NULL,
	[BaoCaoTD] [nvarchar](50) NULL,
	[DeXuat] [nvarchar](500) NULL,
	[NgayBatDauLam] [datetime] NULL,
	[NgayHetThuViec] [datetime] NULL,
	[BoPhanLamVIec] [nvarchar](250) NULL,
	[NoiLamViec] [nvarchar](50) NULL,
	[BoPhan] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ThiSinh_DotTuyenDung] PRIMARY KEY CLUSTERED 
(
	[ThiSinhDotTuyenDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_ThiSinh_NguoiChamDiem]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_ThiSinh_NguoiChamDiem](
	[ThiSinhNguoiChamDiemID] [int] IDENTITY(1,1) NOT NULL,
	[VongThiID] [int] NULL,
	[ThiSinhID] [int] NULL,
	[NguoiChamDiemID] [int] NULL,
	[TongDiem] [decimal](10, 3) NULL,
	[HeSo] [decimal](10, 3) NULL,
	[TongDiemChuaNhanHS] [decimal](10, 3) NULL,
	[NhanXet] [nvarchar](500) NULL,
	[KetQua] [nvarchar](250) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TD_ThiSinh_NguoiChamDiem] PRIMARY KEY CLUSTERED 
(
	[ThiSinhNguoiChamDiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_TrangThaiNghiHoc]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_TrangThaiNghiHoc](
	[TrangThaiNghiHocID] [int] NULL,
	[TrangThaiNghiHoc] [nvarchar](255) NULL,
	[Ten] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[Ma] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_VongThi]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_VongThi](
	[VongThiID] [int] IDENTITY(1,1) NOT NULL,
	[DotTuyenDungID] [int] NULL,
	[TenVongThi] [nvarchar](50) NULL,
	[NgayBatDau] [datetime] NULL,
	[MoTa] [nvarchar](500) NULL,
	[DaThucHien] [bit] NULL,
	[STT] [int] NULL,
	[TongDiemYeuCau] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TenVongThiTA] [nvarchar](50) NULL,
	[NgayKetThuc] [datetime] NULL,
	[IsDaoTao] [bit] NULL,
 CONSTRAINT [PK_NS_VongThi] PRIMARY KEY CLUSTERED 
(
	[VongThiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_VongThi_MonThi]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_VongThi_MonThi](
	[VongThi_MonThiID] [int] IDENTITY(1,1) NOT NULL,
	[VongThiID] [int] NULL,
	[MonThiID] [int] NULL,
	[GhiChu] [nvarchar](500) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[NguoiKiemTra1] [nvarchar](50) NULL,
	[NguoiKiemTra2] [nvarchar](50) NULL,
	[NguoiKiemTra3] [nvarchar](50) NULL,
	[NguoiKiemTra4] [nvarchar](50) NULL,
	[KetQua] [nvarchar](500) NULL,
	[TinhTrang] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[HeSo] [float] NULL,
 CONSTRAINT [PK_NS_VongThi_MonThi] PRIMARY KEY CLUSTERED 
(
	[VongThi_MonThiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_VongThi_NguoiChamDiem]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_VongThi_NguoiChamDiem](
	[NguoiChamDiemID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[VongThiID] [int] NULL,
	[Type] [int] NULL,
	[HeSo] [decimal](18, 5) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TD_VongThi_NguoiChamDiem] PRIMARY KEY CLUSTERED 
(
	[NguoiChamDiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TD_VongThi_ThiSinh]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TD_VongThi_ThiSinh](
	[VongThiThiSinhID] [int] IDENTITY(1,1) NOT NULL,
	[ThiSinhID] [int] NOT NULL,
	[VongThiID] [int] NULL,
	[NhanXet] [nvarchar](500) NULL,
	[TongDiem] [int] NULL,
	[NguoiChamDiemID] [int] NULL,
	[MucLuongDeNghi] [decimal](18, 0) NULL,
	[NgayDeNghiDiLam] [datetime] NULL,
	[KetQua] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NgayThi] [datetime] NULL,
 CONSTRAINT [PK_NS_VongThi_ThiSinh] PRIMARY KEY CLUSTERED 
(
	[VongThiThiSinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TDOL_CapBac]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TDOL_CapBac](
	[CapBacID] [int] IDENTITY(1,1) NOT NULL,
	[MaCapBac] [nvarchar](50) NULL,
	[TenCapBac] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[TrangTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TDOL_CapBac] PRIMARY KEY CLUSTERED 
(
	[CapBacID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TDOL_KhuVuc]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TDOL_KhuVuc](
	[KhuVucID] [int] IDENTITY(1,1) NOT NULL,
	[MaKhuVuc] [nvarchar](50) NULL,
	[TenKhuVuc] [nvarchar](50) NULL,
	[TrangTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TDOL_KhuVuc] PRIMARY KEY CLUSTERED 
(
	[KhuVucID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TDOL_KinhNghiem]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TDOL_KinhNghiem](
	[KinhNghiemID] [int] IDENTITY(1,1) NOT NULL,
	[MaKinhNghiem] [nvarchar](50) NULL,
	[TenKinhNghiem] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[TrangTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TDOL_KinhNghiem] PRIMARY KEY CLUSTERED 
(
	[KinhNghiemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TDOL_MucLuong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TDOL_MucLuong](
	[MucLuongID] [int] IDENTITY(1,1) NOT NULL,
	[MaMucLuong] [nvarchar](50) NULL,
	[TenMucLuong] [nvarchar](50) NULL,
	[Tu] [int] NULL,
	[Toi] [int] NULL,
	[TrangTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TDOL_MucLuong] PRIMARY KEY CLUSTERED 
(
	[MucLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TDOL_NganhNghe]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TDOL_NganhNghe](
	[NganhNgheID] [int] IDENTITY(1,1) NOT NULL,
	[MaNganhNghe] [nvarchar](50) NULL,
	[TenNganhNghe] [nvarchar](50) NULL,
	[MoTa] [nvarchar](50) NULL,
	[TrangTuyenDungID] [int] NULL,
 CONSTRAINT [PK_NS_TDOL_NganhNghe] PRIMARY KEY CLUSTERED 
(
	[NganhNgheID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TienTe]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TienTe](
	[TienTeID] [int] IDENTITY(1,1) NOT NULL,
	[Ma] [nvarchar](50) NULL,
	[Ten] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TienTe] PRIMARY KEY CLUSTERED 
(
	[TienTeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TienTe_TyGia]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TienTe_TyGia](
	[TyGiaID] [int] IDENTITY(1,1) NOT NULL,
	[TienTeID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[TyGia] [decimal](18, 3) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TienTe_TyGia] PRIMARY KEY CLUSTERED 
(
	[TyGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_BangChamCong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_BangChamCong](
	[BangChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[KyHieuChamCongID] [int] NULL,
	[CaID] [int] NULL,
	[NgayChamCong] [datetime] NOT NULL,
	[GioDen] [datetime] NULL,
	[GioVe] [datetime] NULL,
	[LoaiChamCongID] [int] NULL,
	[Lock] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[NhomTinhLuongID] [int] NULL,
 CONSTRAINT [PK_ChamCong] PRIMARY KEY CLUSTERED 
(
	[BangChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_BangLuong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_BangLuong](
	[BangLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[TongLuong] [decimal](18, 0) NULL,
	[TongGiamTru] [decimal](18, 0) NULL,
	[ThucLinh] [decimal](18, 0) NULL,
	[DaNhanLuong] [bit] NULL,
	[NgayNhanLuong] [datetime] NULL,
	[GhiChu] [nvarchar](250) NULL,
	[QTDienBienLuongID] [int] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[NgayQuyetDinh] [nvarchar](50) NULL,
	[ChucDanhID] [int] NULL,
	[NhomLuongID] [int] NULL,
	[ghichudanhgia] [nvarchar](max) NULL,
	[Lock] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[MaDotTinhLuong] [nvarchar](50) NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_BangLuong] PRIMARY KEY CLUSTERED 
(
	[BangLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_tl_baocaoluong_nam]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_tl_baocaoluong_nam](
	[phongban] [nvarchar](100) NULL,
	[MaPBHoachToan] [nvarchar](50) NULL,
	[ThuTu] [bigint] NULL,
	[Luong_1] [decimal](38, 2) NULL,
	[Luong_2] [decimal](38, 2) NULL,
	[Luong_3] [decimal](38, 2) NULL,
	[Luong_4] [decimal](38, 2) NULL,
	[Luong_5] [decimal](38, 2) NULL,
	[Luong_6] [decimal](38, 2) NULL,
	[Luong_7] [decimal](38, 2) NULL,
	[Luong_8] [decimal](38, 2) NULL,
	[Luong_9] [decimal](38, 2) NULL,
	[Luong_10] [decimal](38, 2) NULL,
	[Luong_11] [decimal](38, 2) NULL,
	[Luong_12] [decimal](38, 2) NULL,
	[PhuCap_1] [decimal](38, 2) NULL,
	[PhuCap_2] [decimal](38, 2) NULL,
	[PhuCap_3] [decimal](38, 2) NULL,
	[PhuCap_4] [decimal](38, 2) NULL,
	[PhuCap_5] [decimal](38, 2) NULL,
	[PhuCap_6] [decimal](38, 2) NULL,
	[PhuCap_7] [decimal](38, 2) NULL,
	[PhuCap_8] [decimal](38, 2) NULL,
	[PhuCap_9] [decimal](38, 2) NULL,
	[PhuCap_10] [decimal](38, 2) NULL,
	[PhuCap_11] [decimal](38, 2) NULL,
	[PhuCap_12] [decimal](38, 2) NULL,
	[TT_ANCA_1] [decimal](38, 2) NULL,
	[TT_ANCA_2] [decimal](38, 2) NULL,
	[TT_ANCA_3] [decimal](38, 2) NULL,
	[TT_ANCA_4] [decimal](38, 2) NULL,
	[TT_ANCA_5] [decimal](38, 2) NULL,
	[TT_ANCA_6] [decimal](38, 2) NULL,
	[TT_ANCA_7] [decimal](38, 2) NULL,
	[TT_ANCA_8] [decimal](38, 2) NULL,
	[TT_ANCA_9] [decimal](38, 2) NULL,
	[TT_ANCA_10] [decimal](38, 2) NULL,
	[TT_ANCA_11] [decimal](38, 2) NULL,
	[TT_ANCA_12] [decimal](38, 2) NULL,
	[BHNLD_1] [decimal](38, 2) NULL,
	[BHNLD_2] [decimal](38, 2) NULL,
	[BHNLD_3] [decimal](38, 2) NULL,
	[BHNLD_4] [decimal](38, 2) NULL,
	[BHNLD_5] [decimal](38, 2) NULL,
	[BHNLD_6] [decimal](38, 2) NULL,
	[BHNLD_7] [decimal](38, 2) NULL,
	[BHNLD_8] [decimal](38, 2) NULL,
	[BHNLD_9] [decimal](38, 2) NULL,
	[BHNLD_10] [decimal](38, 2) NULL,
	[BHNLD_11] [decimal](38, 2) NULL,
	[BHNLD_12] [decimal](38, 2) NULL,
	[BHCTY_1] [decimal](38, 2) NULL,
	[BHCTY_2] [decimal](38, 2) NULL,
	[BHCTY_3] [decimal](38, 2) NULL,
	[BHCTY_4] [decimal](38, 2) NULL,
	[BHCTY_5] [decimal](38, 2) NULL,
	[BHCTY_6] [decimal](38, 2) NULL,
	[BHCTY_7] [decimal](38, 2) NULL,
	[BHCTY_8] [decimal](38, 2) NULL,
	[BHCTY_9] [decimal](38, 2) NULL,
	[BHCTY_10] [decimal](38, 2) NULL,
	[BHCTY_11] [decimal](38, 2) NULL,
	[BHCTY_12] [decimal](38, 2) NULL,
	[TTE_TTNCN_1] [decimal](38, 2) NULL,
	[TTE_TTNCN_2] [decimal](38, 2) NULL,
	[TTE_TTNCN_3] [decimal](38, 2) NULL,
	[TTE_TTNCN_4] [decimal](38, 2) NULL,
	[TTE_TTNCN_5] [decimal](38, 2) NULL,
	[TTE_TTNCN_6] [decimal](38, 2) NULL,
	[TTE_TTNCN_7] [decimal](38, 2) NULL,
	[TTE_TTNCN_8] [decimal](38, 2) NULL,
	[TTE_TTNCN_9] [decimal](38, 2) NULL,
	[TTE_TTNCN_10] [decimal](38, 2) NULL,
	[TTE_TTNCN_11] [decimal](38, 2) NULL,
	[TTE_TTNCN_12] [decimal](38, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CC_DuLieuChamCong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CC_DuLieuChamCong](
	[DuLieuChamCongID] [bigint] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[DauDocTheID] [int] NULL,
	[ThoiGian] [datetime] NULL,
	[VaoRa] [bit] NULL,
	[NhapTay] [bit] NULL,
	[MayChamCongID] [int] NULL,
	[MaChamCong] [nvarchar](20) NULL,
	[HoVaTen] [nvarchar](50) NULL,
	[PhongBanID] [int] NULL,
	[DuLieuKhongTongHop] [bit] NULL,
 CONSTRAINT [PK_NS_TL_CC_DuLieuChamCong] PRIMARY KEY CLUSTERED 
(
	[DuLieuChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CC_MayChamCong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CC_MayChamCong](
	[MayChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[SoBanGhi] [bigint] NULL,
	[ChinhVao] [bit] NULL,
	[Loai] [int] NULL,
	[CongGiaoTiep] [int] NULL,
	[BaudRate] [int] NULL,
	[IP] [nvarchar](50) NULL,
	[Port] [int] NULL,
	[KieuKetNoi] [int] NULL,
	[DuLieuDocLanCuoi] [datetime] NULL,
	[Serial] [nvarchar](50) NULL,
	[Key] [nvarchar](50) NULL,
	[XoaDuLieuSauKhiImport] [bit] NULL,
	[DuLieuKhongTongHop] [bit] NULL,
	[LichDownload] [time](7) NULL,
	[IsDownloading] [bit] NULL,
	[SoLuongBanGhiCuoiCung] [int] NULL,
	[SoLuongLuuVaoDB] [int] NULL,
 CONSTRAINT [PK_NS_TL_CC_MayChamCong] PRIMARY KEY CLUSTERED 
(
	[MayChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CC_TongHopTheoNgay]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CC_TongHopTheoNgay](
	[TongHopTheoNgayID] [bigint] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[CaLamViecID] [int] NULL,
	[PhongBanID] [int] NULL,
	[NgayChamCong] [date] NULL,
	[GioDen] [time](7) NULL,
	[GioVe] [time](7) NULL,
	[TGDiMuon] [decimal](10, 0) NULL,
	[TGDiMuonToi] [decimal](10, 0) NULL,
	[TGVeSom] [decimal](10, 0) NULL,
	[TGVeSomToi] [decimal](10, 0) NULL,
	[TGLamViec] [decimal](10, 0) NULL,
	[TGLamViecToi] [decimal](10, 0) NULL,
	[TGLamThem] [decimal](10, 0) NULL,
	[TGLamThemToi] [decimal](10, 0) NULL,
	[TGRaNgoai] [decimal](10, 0) NULL,
	[TGRaNgoaiToi] [decimal](10, 0) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[IsChinhSua] [bit] NULL,
	[IsNgayLe] [bit] NULL,
	[IsLamSangNgayHomSau] [bit] NULL,
	[KyHieuChamCongID] [int] NULL,
	[TGLamThemDem] [decimal](18, 1) NULL,
	[Lock] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[MaDotTongHopCong] [nvarchar](50) NULL,
	[IsLamThemNgayHomSau] [bit] NULL,
	[IsDiMuonCoLyDo] [bit] NULL,
	[IsVeSomCoLyDo] [bit] NULL,
	[LyDoDiMuon] [nvarchar](250) NULL,
	[LyDoVeSom] [nvarchar](250) NULL,
	[Status] [int] NULL,
	[XacNhanGioDen] [time](7) NULL,
	[XacNhanGioVe] [time](7) NULL,
	[GioRa] [time](7) NULL,
	[GioVao] [time](7) NULL,
	[XacNhanLyDo] [nvarchar](250) NULL,
	[YKienPheDuyet] [nvarchar](250) NULL,
	[ID] [nvarchar](50) NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_TL_CC_TongHopTheoNgay] PRIMARY KEY CLUSTERED 
(
	[TongHopTheoNgayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChamCong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChamCong](
	[ChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiChamCongID] [int] NULL,
	[NgayChamCong] [datetime] NULL,
	[SoGioLam] [decimal](18, 2) NULL,
	[CaLamViec] [char](1) NULL,
	[Type] [int] NULL,
	[Lock] [bit] NULL,
	[NhomTinhLuongID] [int] NULL,
 CONSTRAINT [PK_NS_TL_ChamCong] PRIMARY KEY CLUSTERED 
(
	[ChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ns_tl_chiluongbosungshb]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ns_tl_chiluongbosungshb](
	[NhanVienID] [int] NOT NULL,
	[phongbanid] [int] NULL,
	[chucdanhid] [int] NULL,
	[taikhoannganhang] [nvarchar](100) NULL,
	[tennganhang] [nvarchar](100) NULL,
	[macif] [nvarchar](100) NULL,
	[hovaten] [nvarchar](101) NULL,
	[MaNhanVien] [varchar](50) NULL,
	[NhomTinhLuongID] [int] NULL,
	[NghiViec] [bit] NULL,
	[NgayNghiViec] [datetime] NULL,
	[NgayBatDauHanCheQuyenLoi] [datetime] NULL,
	[NgayHetHanViPhamCamKetThaiSan] [datetime] NULL,
	[TrangThaiTinhLuongID] [int] NULL,
	[TenChucDanh] [nvarchar](500) NULL,
	[Ten] [nvarchar](100) NULL,
	[MaChiNhanhHachToan] [nvarchar](50) NULL,
	[MaPhongBanHachToan] [nvarchar](50) NULL,
	[MaPhongBan] [nvarchar](50) NULL,
	[thutu1] [bigint] NULL,
	[thutu2] [int] NULL,
	[CB_HS] [decimal](18, 5) NULL,
	[PT_Huong_LKD] [decimal](18, 5) NULL,
	[PT_Huong_LCB] [decimal](18, 5) NULL,
	[KD_HS] [decimal](18, 5) NULL,
	[DB_PCDieuDong] [decimal](18, 5) NULL,
	[DB_PCTH] [decimal](18, 5) NULL,
	[CB_LTT] [decimal](18, 5) NULL,
	[KD_LTT] [decimal](18, 5) NULL,
	[LuongKhoanNV] [decimal](18, 5) NULL,
	[tvhvnv] [nvarchar](5) NOT NULL,
	[BC_LKD_DB_LUONGBS] [decimal](38, 6) NULL,
	[BC_LKD_SoCong] [decimal](38, 2) NULL,
	[nctc] [decimal](10, 3) NULL,
	[TrangThaiChiLuongBoSung] [int] NOT NULL,
	[KetQuaDanhGia] [nvarchar](100) NULL,
	[BC_LKD_MucHuong] [decimal](38, 6) NULL,
	[MucChiLuongBoSung] [nvarchar](50) NOT NULL,
	[ghichuluongnv] [nvarchar](200) NULL,
	[ngaychiluong] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChiTietCongThucLuong]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChiTietCongThucLuong](
	[ChiTietCongThucLuongID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiLuongID] [int] NULL,
	[NhomCongThucLuongID] [int] NULL,
	[CongThucTinh] [nvarchar](1000) NULL,
	[ThuTu] [int] NULL,
 CONSTRAINT [PK_NS_TL_ChiTietCongThucLuong] PRIMARY KEY CLUSTERED 
(
	[ChiTietCongThucLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChiTietCongThucLuong_LichSu]    Script Date: 6/21/2018 11:30:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChiTietCongThucLuong_LichSu](
	[LichSuCongThucLuongID] [int] IDENTITY(1,1) NOT NULL,
	[ChiTietCongThucLuongID] [int] NULL,
	[CongThucTinh] [nvarchar](1000) NULL,
	[NgayChinhSua] [datetime] NULL,
	[NhomCongThucLuongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LoaiLuongID] [int] NULL,
 CONSTRAINT [PK_NS_TL_ChiTietCongThucLuong_LichSu] PRIMARY KEY CLUSTERED 
(
	[LichSuCongThucLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChiTietLuong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChiTietLuong](
	[ChiTietLuongID] [int] IDENTITY(1,1) NOT NULL,
	[BangLuongID] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[TienLuong] [decimal](18, 2) NULL,
	[ChinhSua] [bit] NULL,
	[NguoiChinhSuaID] [int] NULL,
	[NgayChinhSua] [datetime] NULL,
	[GiaTriGoc] [decimal](18, 2) NULL,
	[GhiChu] [nvarchar](550) NULL,
	[TienLuongStr] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_TL_ChiTietLuong] PRIMARY KEY CLUSTERED 
(
	[ChiTietLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_Chitietluong_Nam]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_Chitietluong_Nam](
	[BangLuongID] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[TienLuong] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChiTietLuongKhac]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChiTietLuongKhac](
	[ChiTietLuongID] [int] IDENTITY(1,1) NOT NULL,
	[BangLuongID] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[TienLuong] [int] NULL,
 CONSTRAINT [PK_NS_TL_ChiTietLuongKhac] PRIMARY KEY CLUSTERED 
(
	[ChiTietLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChotLuongDot1]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChotLuongDot1](
	[ChotLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[Luong] [decimal](18, 5) NULL,
	[QTDBLID] [int] NULL,
	[NgayHuong] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_ChotLuongDot1] PRIMARY KEY CLUSTERED 
(
	[ChotLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_Chotluongdot1_Tam]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_Chotluongdot1_Tam](
	[ChotLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[Luong] [decimal](18, 5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ChotPhepNam]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ChotPhepNam](
	[ChotPhepID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Nam] [int] NULL,
	[PhepConDu] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_TL_ChotPhepNam] PRIMARY KEY CLUSTERED 
(
	[ChotPhepID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CoCauLuong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CoCauLuong](
	[CoCauLuongID] [int] IDENTITY(1,1) NOT NULL,
	[TenCoCauLuong] [nvarchar](50) NULL,
	[MaCoCauLuong] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[DangHieuLuc] [bit] NULL,
 CONSTRAINT [PK_NS_TL_CoCauLuong] PRIMARY KEY CLUSTERED 
(
	[CoCauLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CongDoan]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CongDoan](
	[CongDoanID] [int] IDENTITY(1,1) NOT NULL,
	[MaCongDoan] [nvarchar](50) NULL,
	[TenCongDoan] [nvarchar](250) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_CongDoan] PRIMARY KEY CLUSTERED 
(
	[CongDoanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_CongThucLuongHeThong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_CongThucLuongHeThong](
	[CongThucLuongHeThongID] [int] IDENTITY(1,1) NOT NULL,
	[MaSo] [nvarchar](50) NULL,
	[Ten] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[ThuTu] [nvarchar](50) NULL,
	[LamTron] [int] NULL,
	[CongThuc] [nvarchar](250) NULL,
	[IsUse] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[TraVeKieuText] [bit] NULL,
	[TextFormat] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TL_LoaiLuongHeThong] PRIMARY KEY CLUSTERED 
(
	[CongThucLuongHeThongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_DangKyCong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_DangKyCong](
	[DangKyCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[KyHieuChamCongID] [int] NULL,
	[NgayChamCong] [datetime] NULL,
	[GioLamThem] [decimal](18, 1) NULL,
	[CaLamViecID] [int] NULL,
	[LamThemTruocCa] [bit] NULL,
	[GioBatDauLamThem] [time](7) NULL,
	[GioKetThucLamThem] [time](7) NULL,
	[Lock] [bit] NULL,
	[BDLamThemTruocCa] [time](7) NULL,
	[KTLamThemTruocCa] [time](7) NULL,
	[BDLamThemSauCa] [time](7) NULL,
	[KTLamThemSauCa] [time](7) NULL,
	[TuGio] [time](7) NULL,
	[ToiGio] [time](7) NULL,
	[LyDoTangCa] [nvarchar](50) NULL,
	[LyDoTangCaID] [int] NULL,
	[XetDuyet] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[GioLamThemTruocCa] [decimal](18, 1) NULL,
	[DangKyStr] [nvarchar](50) NULL,
	[YKienXetDuyet] [nvarchar](50) NULL,
	[CanhBaoTangCa] [nvarchar](50) NULL,
	[AnTangCa] [bit] NULL,
 CONSTRAINT [PK_NS_TL_DangKyCong] PRIMARY KEY CLUSTERED 
(
	[DangKyCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_DanhGia]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_DanhGia](
	[DanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[LoaiDanhGiaID] [int] NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
 CONSTRAINT [PK_NS_TL_DanhGia] PRIMARY KEY CLUSTERED 
(
	[DanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_DanhSachThangLuongKinhDoanh]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_DanhSachThangLuongKinhDoanh](
	[KdID] [int] NOT NULL,
	[BangLuong] [nvarchar](20) NULL,
	[b1] [decimal](10, 2) NULL,
	[b2] [decimal](10, 2) NULL,
	[b3] [decimal](10, 2) NULL,
	[b4] [decimal](10, 2) NULL,
	[b5] [decimal](10, 2) NULL,
	[b6] [decimal](10, 2) NULL,
	[b7] [decimal](10, 2) NULL,
	[b8] [decimal](10, 2) NULL,
	[b9] [decimal](10, 2) NULL,
	[b10] [decimal](10, 2) NULL,
	[b11] [decimal](10, 2) NULL,
	[b12] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[KdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_DieuChinhCongBoSung]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_DieuChinhCongBoSung](
	[DieuChinhCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[SoCong] [decimal](18, 3) NULL,
 CONSTRAINT [PK_NS_TL_DieuChinhCongBoSung] PRIMARY KEY CLUSTERED 
(
	[DieuChinhCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_DMLuongCoBan]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_DMLuongCoBan](
	[DMLuongCoBanID] [int] IDENTITY(1,1) NOT NULL,
	[NgachCongChucID] [int] NULL,
	[ThamNienID] [int] NULL,
	[PhongBanID] [int] NULL,
	[LuongCoBan] [int] NULL,
	[LuongTrachNhiem] [int] NULL,
	[LuongCB] [int] NULL,
 CONSTRAINT [PK_NS_DMLuongCoBan] PRIMARY KEY CLUSTERED 
(
	[DMLuongCoBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ns_tl_GhiChu_LuongNhanVien]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ns_tl_GhiChu_LuongNhanVien](
	[TLGhiChuNhanVien] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TuThang] [datetime] NULL,
	[ToiThang] [datetime] NULL,
	[GhiChu] [nvarchar](1000) NULL,
	[NhomGhiChuID] [int] NULL,
 CONSTRAINT [PK_Ns_tl_GhiChu_LuongNhanVien] PRIMARY KEY CLUSTERED 
(
	[TLGhiChuNhanVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_GhiChuChiLuongBoSung]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_GhiChuChiLuongBoSung](
	[GhiChuLBSID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NhanVienID] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
 CONSTRAINT [PK_NS_TL_GhiChuChiLuongBoSung] PRIMARY KEY CLUSTERED 
(
	[GhiChuLBSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_HachToan]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_HachToan](
	[HachToanID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[PhongBanID] [int] NULL,
	[QTDienBienLuongID] [int] NULL,
	[ChucVuID] [int] NULL,
	[NgayHuong] [datetime] NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[NhomTinhLuongID] [int] NULL,
	[MaPBHachToan] [nvarchar](50) NULL,
	[MaCNHachToan] [nvarchar](50) NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TL_HachToan] PRIMARY KEY CLUSTERED 
(
	[HachToanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_KhoaLuong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_KhoaLuong](
	[KhoaLuongID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[PhongBanID] [int] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_NS_TL_KhoaLuong] PRIMARY KEY CLUSTERED 
(
	[KhoaLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_KhongHienThi]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_KhongHienThi](
	[KHDID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[KhongHienBangThue] [bit] NULL,
	[KhongHienBangBH] [bit] NULL,
	[KhongHienBangDot1] [bit] NULL,
	[KhongHienBangDot2] [bit] NULL,
 CONSTRAINT [PK__NS_TL_Kh__1A2ECA133FDB6521] PRIMARY KEY CLUSTERED 
(
	[KHDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_KyHieu_LoaiChamCong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_KyHieu_LoaiChamCong](
	[KyHieu_LoaiChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[KyHieuChamCongID] [int] NULL,
	[LoaiChamCongID] [int] NULL,
	[SoGioLam] [decimal](18, 2) NULL,
	[SoGioLamNgayLe] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_TL_KyHieu_LoaiChamCong] PRIMARY KEY CLUSTERED 
(
	[KyHieu_LoaiChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_KyHieuChamCong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_KyHieuChamCong](
	[KyHieuChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiChamCongID] [int] NULL,
	[KyHieu] [nvarchar](50) NULL,
	[MoTa] [nvarchar](500) NULL,
	[SoGioLamHC] [float] NULL,
	[SoGioLamSX] [float] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_LoaiChamCong] PRIMARY KEY CLUSTERED 
(
	[KyHieuChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LenhSanXuat]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LenhSanXuat](
	[LenhSanXuatID] [int] IDENTITY(1,1) NOT NULL,
	[MaLenhSanXuat] [nvarchar](50) NULL,
	[TenLenhSanXuat] [nvarchar](250) NULL,
	[NgayThang] [date] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_LenhSanXuat] PRIMARY KEY CLUSTERED 
(
	[LenhSanXuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LoaiChamCong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LoaiChamCong](
	[LoaiChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[MaSo] [nvarchar](10) NULL,
	[TenLoai] [nvarchar](50) NULL,
	[HeSoLCB] [float] NULL,
	[LuongCoDinh] [int] NULL,
	[CongThucTe] [bit] NULL,
	[ThuTu] [int] NULL,
	[Type] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_LoaiChamCong_1] PRIMARY KEY CLUSTERED 
(
	[LoaiChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LoaiDanhGia]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LoaiDanhGia](
	[LoaiDanhGiaID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[KyHieu] [nvarchar](10) NULL,
	[HeSo] [float] NULL,
 CONSTRAINT [PK_NS_TL_LoaiDanhGia] PRIMARY KEY CLUSTERED 
(
	[LoaiDanhGiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LoaiLuong]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LoaiLuong](
	[LoaiLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhomLuongID] [int] NULL,
	[MaSo] [nvarchar](50) NULL,
	[Ten] [nvarchar](200) NULL,
	[Type] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[NhapQuyetDinh] [bit] NULL,
	[LamTron] [int] NULL,
	[ThuTu] [int] NULL,
	[HeSo] [float] NULL,
	[LoaiChamCongID] [int] NULL,
	[ThuTuHienThi] [int] NULL,
	[Color] [bigint] NULL,
	[SQLEventBeforeSave] [nvarchar](50) NULL,
	[SQLEventAfterSave] [nvarchar](550) NULL,
	[CoCauLuongID] [int] NULL,
	[TinhTaiDong] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[SQLGetItems] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TL_LoaiLuong] PRIMARY KEY CLUSTERED 
(
	[LoaiLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LoaiLuongKhac]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LoaiLuongKhac](
	[LoaiLuongID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](50) NULL,
	[Type] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[TheoNgay] [bit] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_LoaiPhuCap] PRIMARY KEY CLUSTERED 
(
	[LoaiLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_LuongDuAnSHB]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_LuongDuAnSHB](
	[LuongDuAnSHBID] [int] IDENTITY(1,1) NOT NULL,
	[TenDuAn] [nvarchar](50) NULL,
	[TuThang] [nvarchar](50) NULL,
	[ToiThang] [nvarchar](50) NULL,
	[PhuCapDuAn] [decimal](18, 0) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[NhanVienID] [int] NULL,
	[ChucDanh] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TL_LuongDuAnSHB] PRIMARY KEY CLUSTERED 
(
	[LuongDuAnSHBID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NganhHang]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NganhHang](
	[NganhHangID] [int] IDENTITY(1,1) NOT NULL,
	[Ten] [nvarchar](100) NULL,
	[Ma] [nvarchar](50) NULL,
	[Mota] [nvarchar](200) NULL,
	[TenNganhHang] [nvarchar](100) NULL,
	[MaNganhHang] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK__NS_TL_Ng__9871885D36F11965] PRIMARY KEY CLUSTERED 
(
	[NganhHangID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NGAYCONGLUONGBOSUNG]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NGAYCONGLUONGBOSUNG](
	[NgayCongLBSID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[NgayChamCong] [datetime] NULL,
	[SoGioLam] [decimal](10, 2) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[PhongBanID] [int] NULL,
 CONSTRAINT [PK_NS_TL_NGAYCONGLUONGBOSUNG] PRIMARY KEY CLUSTERED 
(
	[NgayCongLBSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NgayCongTieuChuan]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NgayCongTieuChuan](
	[NgayCongTieuChuanID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NgayCongChuan] [decimal](5, 2) NULL,
 CONSTRAINT [PK_NS_TL_NgayCongTieuChuan] PRIMARY KEY CLUSTERED 
(
	[NgayCongTieuChuanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NgayCongTieuChuanLuongBoSung]    Script Date: 6/21/2018 11:30:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NgayCongTieuChuanLuongBoSung](
	[NgayCongTCLBSID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[CongTieuChuan] [decimal](10, 2) NULL,
 CONSTRAINT [PK_NS_TL_NgayCongTieuChuanLuongBoSung] PRIMARY KEY CLUSTERED 
(
	[NgayCongTCLBSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NgayLe]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NgayLe](
	[NgayLeID] [int] IDENTITY(1,1) NOT NULL,
	[Thu] [int] NULL,
	[Ngay] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[MoTa] [nvarchar](250) NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_NgayLe] PRIMARY KEY CLUSTERED 
(
	[NgayLeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NhomCongThucLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NhomCongThucLuong](
	[NhomCongThucLuongID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhom] [nvarchar](50) NULL,
	[MaNhom] [nvarchar](50) NULL,
	[NhomChinh] [bit] NULL,
	[NBD_ChamCong] [int] NULL,
	[NKT_ChamCong] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[CoCauLuongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_NhomCongThucLuong] PRIMARY KEY CLUSTERED 
(
	[NhomCongThucLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_NhomLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_NhomLuong](
	[NhomLuongID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomLuong] [nvarchar](50) NULL,
	[GiamTru] [bit] NULL,
	[GhiChu] [nvarchar](150) NULL,
	[ThuTu] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[CoCauLuongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[KhongTinhLuong] [bit] NULL,
 CONSTRAINT [PK_NS_TL_NhomLuong] PRIMARY KEY CLUSTERED 
(
	[NhomLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_PhuCapHetHan]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_PhuCapHetHan](
	[PhuCapHetHanID] [int] IDENTITY(1,1) NOT NULL,
	[NgayHuong] [datetime] NULL,
	[NgayHetHan] [datetime] NULL,
	[LuongCapBac] [bit] NULL,
	[LuongKinhDoanh] [bit] NULL,
	[PhuCapAnCa] [bit] NULL,
	[PhuCapDienThoai] [bit] NULL,
	[PhuCapKhac] [bit] NULL,
	[PhuCapLaiXe] [bit] NULL,
	[PhuCapKiemNhiem] [bit] NULL,
	[PhuCapDieuDong] [bit] NULL,
	[CongTacPhi] [bit] NULL,
	[PhuCapLuongKTNB] [bit] NULL,
	[PhuCapDocHai] [bit] NULL,
	[PhuCapThuHut] [bit] NULL,
	[PhuCapTrangDiem] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_QuaTrinhTachCong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_QuaTrinhTachCong](
	[NhanVienID] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[STT] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_QuyetToanThue]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_QuyetToanThue](
	[nam] [int] NULL,
	[manhanvien] [varchar](50) NULL,
	[hovaten] [nvarchar](101) NULL,
	[CMTND] [nvarchar](50) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[TrangThaiUyQuyenQTThue] [nvarchar](50) NULL,
	[NhanVienID] [int] NULL,
	[LTL2_1] [decimal](38, 2) NULL,
	[LTL2_2] [decimal](38, 2) NULL,
	[LTL2_3] [decimal](38, 2) NULL,
	[LTL2_4] [decimal](38, 2) NULL,
	[LTL2_5] [decimal](38, 2) NULL,
	[LTL2_6] [decimal](38, 2) NULL,
	[LTL2_7] [decimal](38, 2) NULL,
	[LTL2_8] [decimal](38, 2) NULL,
	[LTL2_9] [decimal](38, 2) NULL,
	[LTL2_10] [decimal](38, 2) NULL,
	[LTL2_11] [decimal](38, 2) NULL,
	[LTL2_12] [decimal](38, 2) NULL,
	[ATNN2_1] [decimal](38, 2) NULL,
	[ATNN2_2] [decimal](38, 2) NULL,
	[ATNN2_3] [decimal](38, 2) NULL,
	[ATNN2_4] [decimal](38, 2) NULL,
	[ATNN2_5] [decimal](38, 2) NULL,
	[ATNN2_6] [decimal](38, 2) NULL,
	[ATNN2_7] [decimal](38, 2) NULL,
	[ATNN2_8] [decimal](38, 2) NULL,
	[ATNN2_9] [decimal](38, 2) NULL,
	[ATNN2_10] [decimal](38, 2) NULL,
	[ATNN2_11] [decimal](38, 2) NULL,
	[ATNN2_12] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_1] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_2] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_3] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_4] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_5] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_6] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_7] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_8] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_9] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_10] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_11] [decimal](38, 2) NULL,
	[PHUCAP_ANCA_12] [decimal](38, 2) NULL,
	[ANCA_THUE_1] [decimal](38, 2) NULL,
	[ANCA_THUE_2] [decimal](38, 2) NULL,
	[ANCA_THUE_3] [decimal](38, 2) NULL,
	[ANCA_THUE_4] [decimal](38, 2) NULL,
	[ANCA_THUE_5] [decimal](38, 2) NULL,
	[ANCA_THUE_6] [decimal](38, 2) NULL,
	[ANCA_THUE_7] [decimal](38, 2) NULL,
	[ANCA_THUE_8] [decimal](38, 2) NULL,
	[ANCA_THUE_9] [decimal](38, 2) NULL,
	[ANCA_THUE_10] [decimal](38, 2) NULL,
	[ANCA_THUE_11] [decimal](38, 2) NULL,
	[ANCA_THUE_12] [decimal](38, 2) NULL,
	[BHXH_NLD_1] [decimal](38, 2) NULL,
	[BHXH_NLD_2] [decimal](38, 2) NULL,
	[BHXH_NLD_3] [decimal](38, 2) NULL,
	[BHXH_NLD_4] [decimal](38, 2) NULL,
	[BHXH_NLD_5] [decimal](38, 2) NULL,
	[BHXH_NLD_6] [decimal](38, 2) NULL,
	[BHXH_NLD_7] [decimal](38, 2) NULL,
	[BHXH_NLD_8] [decimal](38, 2) NULL,
	[BHXH_NLD_9] [decimal](38, 2) NULL,
	[BHXH_NLD_10] [decimal](38, 2) NULL,
	[BHXH_NLD_11] [decimal](38, 2) NULL,
	[BHXH_NLD_12] [decimal](38, 2) NULL,
	[BHYT_NLD_1] [decimal](38, 2) NULL,
	[BHYT_NLD_2] [decimal](38, 2) NULL,
	[BHYT_NLD_3] [decimal](38, 2) NULL,
	[BHYT_NLD_4] [decimal](38, 2) NULL,
	[BHYT_NLD_5] [decimal](38, 2) NULL,
	[BHYT_NLD_6] [decimal](38, 2) NULL,
	[BHYT_NLD_7] [decimal](38, 2) NULL,
	[BHYT_NLD_8] [decimal](38, 2) NULL,
	[BHYT_NLD_9] [decimal](38, 2) NULL,
	[BHYT_NLD_10] [decimal](38, 2) NULL,
	[BHYT_NLD_11] [decimal](38, 2) NULL,
	[BHYT_NLD_12] [decimal](38, 2) NULL,
	[BHTN_NLD_1] [decimal](38, 2) NULL,
	[BHTN_NLD_2] [decimal](38, 2) NULL,
	[BHTN_NLD_3] [decimal](38, 2) NULL,
	[BHTN_NLD_4] [decimal](38, 2) NULL,
	[BHTN_NLD_5] [decimal](38, 2) NULL,
	[BHTN_NLD_6] [decimal](38, 2) NULL,
	[BHTN_NLD_7] [decimal](38, 2) NULL,
	[BHTN_NLD_8] [decimal](38, 2) NULL,
	[BHTN_NLD_9] [decimal](38, 2) NULL,
	[BHTN_NLD_10] [decimal](38, 2) NULL,
	[BHTN_NLD_11] [decimal](38, 2) NULL,
	[BHTN_NLD_12] [decimal](38, 2) NULL,
	[CDP2_1_1] [decimal](38, 2) NULL,
	[CDP2_1_2] [decimal](38, 2) NULL,
	[CDP2_1_3] [decimal](38, 2) NULL,
	[CDP2_1_4] [decimal](38, 2) NULL,
	[CDP2_1_5] [decimal](38, 2) NULL,
	[CDP2_1_6] [decimal](38, 2) NULL,
	[CDP2_1_7] [decimal](38, 2) NULL,
	[CDP2_1_8] [decimal](38, 2) NULL,
	[CDP2_1_9] [decimal](38, 2) NULL,
	[CDP2_1_10] [decimal](38, 2) NULL,
	[CDP2_1_11] [decimal](38, 2) NULL,
	[CDP2_1_12] [decimal](38, 2) NULL,
	[GT_GIACANH_1] [decimal](38, 2) NULL,
	[GT_GIACANH_2] [decimal](38, 2) NULL,
	[GT_GIACANH_3] [decimal](38, 2) NULL,
	[GT_GIACANH_4] [decimal](38, 2) NULL,
	[GT_GIACANH_5] [decimal](38, 2) NULL,
	[GT_GIACANH_6] [decimal](38, 2) NULL,
	[GT_GIACANH_7] [decimal](38, 2) NULL,
	[GT_GIACANH_8] [decimal](38, 2) NULL,
	[GT_GIACANH_9] [decimal](38, 2) NULL,
	[GT_GIACANH_10] [decimal](38, 2) NULL,
	[GT_GIACANH_11] [decimal](38, 2) NULL,
	[GT_GIACANH_12] [decimal](38, 2) NULL,
	[TTNCN2_1] [decimal](38, 2) NULL,
	[TTNCN2_2] [decimal](38, 2) NULL,
	[TTNCN2_3] [decimal](38, 2) NULL,
	[TTNCN2_4] [decimal](38, 2) NULL,
	[TTNCN2_5] [decimal](38, 2) NULL,
	[TTNCN2_6] [decimal](38, 2) NULL,
	[TTNCN2_7] [decimal](38, 2) NULL,
	[TTNCN2_8] [decimal](38, 2) NULL,
	[TTNCN2_9] [decimal](38, 2) NULL,
	[TTNCN2_10] [decimal](38, 2) NULL,
	[TTNCN2_11] [decimal](38, 2) NULL,
	[TTNCN2_12] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_1] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_2] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_3] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_4] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_5] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_6] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_7] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_8] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_9] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_10] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_11] [decimal](38, 2) NULL,
	[LKD_MUCHUONG_12] [decimal](38, 2) NULL,
	[TongGiamTruCaNhan] [int] NULL,
	[TongGiamTruNPT] [int] NULL,
	[TongBHCaNam] [decimal](38, 2) NULL,
	[BHXHNLD_1] [decimal](38, 2) NULL,
	[BHXHNLD_2] [decimal](38, 2) NULL,
	[BHXHNLD_3] [decimal](38, 2) NULL,
	[BHXHNLD_4] [decimal](38, 2) NULL,
	[BHXHNLD_5] [decimal](38, 2) NULL,
	[BHXHNLD_6] [decimal](38, 2) NULL,
	[BHXHNLD_7] [decimal](38, 2) NULL,
	[BHXHNLD_8] [decimal](38, 2) NULL,
	[BHXHNLD_9] [decimal](38, 2) NULL,
	[BHXHNLD_10] [decimal](38, 2) NULL,
	[BHXHNLD_11] [decimal](38, 2) NULL,
	[BHXHNLD_12] [decimal](38, 2) NULL,
	[SoThang] [int] NULL,
	[TongThucLinh] [decimal](38, 2) NULL,
	[TongAnCaThue] [decimal](38, 2) NULL,
	[TongPhuCapAnCa] [decimal](38, 2) NULL,
	[TongATNN2] [decimal](38, 2) NULL,
	[TongCDP] [decimal](38, 2) NULL,
	[TongGTGC] [decimal](38, 2) NULL,
	[TongTTNCN] [decimal](38, 2) NULL,
	[UyQuyen] [nvarchar](1) NULL,
	[TongSoNguoiPhuThuoc] [int] NULL,
	[tongthunhapchiuthue] [decimal](38, 2) NULL,
	[TongGiamTruCNNPT] [int] NULL,
	[TongThuNhapSauGiamTru] [decimal](38, 2) NULL,
	[ThuNhapNetSauGiamTruBinhQuanThang] [decimal](38, 6) NULL,
	[ThuNhapTruocThueMotThang] [numeric](38, 6) NULL,
	[MAPBHT_1] [nvarchar](50) NULL,
	[MAPBHT_2] [nvarchar](50) NULL,
	[MAPBHT_3] [nvarchar](50) NULL,
	[MAPBHT_4] [nvarchar](50) NULL,
	[MAPBHT_5] [nvarchar](50) NULL,
	[MAPBHT_6] [nvarchar](50) NULL,
	[MAPBHT_7] [nvarchar](50) NULL,
	[MAPBHT_8] [nvarchar](50) NULL,
	[MAPBHT_9] [nvarchar](50) NULL,
	[MAPBHT_10] [nvarchar](50) NULL,
	[MAPBHT_11] [nvarchar](50) NULL,
	[MAPBHT_12] [nvarchar](50) NULL,
	[MACNHT_1] [nvarchar](50) NULL,
	[MACNHT_2] [nvarchar](50) NULL,
	[MACNHT_3] [nvarchar](50) NULL,
	[MACNHT_4] [nvarchar](50) NULL,
	[MACNHT_5] [nvarchar](50) NULL,
	[MACNHT_6] [nvarchar](50) NULL,
	[MACNHT_7] [nvarchar](50) NULL,
	[MACNHT_8] [nvarchar](50) NULL,
	[MACNHT_9] [nvarchar](50) NULL,
	[MACNHT_10] [nvarchar](50) NULL,
	[MACNHT_11] [nvarchar](50) NULL,
	[MACNHT_12] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_SanPham]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_SanPham](
	[SanPhamID] [int] IDENTITY(1,1) NOT NULL,
	[TenSanPham] [nvarchar](500) NULL,
	[DonGia] [decimal](18, 0) NULL,
	[Type] [int] NULL,
	[PhongBanID] [int] NULL,
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[DonViTinh] [nvarchar](50) NULL,
	[SoLuong] [float] NULL,
	[Ten] [nvarchar](100) NULL,
	[Ma] [nvarchar](50) NULL,
	[NganhHangID] [int] NULL,
	[MaSanPham] [nvarchar](50) NULL,
	[CongTyID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_SanPham_NhanVien]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_SanPham_NhanVien](
	[SanPhamNhanVienID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[PhongBanID] [int] NULL,
	[SanPhamID] [int] NULL,
	[SoLuong] [decimal](10, 2) NULL,
	[ThoiGianSanXuat] [datetime] NULL,
	[DonGia] [decimal](10, 2) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LenhSanXuatID] [int] NULL,
	[CongDoanID] [int] NULL,
 CONSTRAINT [PK__NS_TL_Ma__9AF2920D3E923B2D] PRIMARY KEY CLUSTERED 
(
	[SanPhamNhanVienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_SanPhamCongDoan]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_SanPhamCongDoan](
	[SanPhamCongDoanID] [int] IDENTITY(1,1) NOT NULL,
	[SanPhamID] [int] NULL,
	[CongDoanID] [int] NULL,
	[DonGia] [decimal](10, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ThuTu] [int] NULL,
 CONSTRAINT [PK_NS_TL_SanPhamCongDoan] PRIMARY KEY CLUSTERED 
(
	[SanPhamCongDoanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_SanPhamCongDoanLSX]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_SanPhamCongDoanLSX](
	[SanPhamCongDoanLSXID] [int] IDENTITY(1,1) NOT NULL,
	[SanPhamID] [int] NULL,
	[CongDoanID] [int] NULL,
	[LenhSanXuatID] [int] NULL,
	[DonGia] [decimal](10, 0) NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_TL_SanPhamCongDoanLSX] PRIMARY KEY CLUSTERED 
(
	[SanPhamCongDoanLSXID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_SHB_LuongDuAn_Moi]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_SHB_LuongDuAn_Moi](
	[LuongDuAnID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[DuAnID] [int] NULL,
	[ChucDanhDuAn] [nvarchar](300) NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[PhuCapDuAn] [decimal](18, 2) NULL,
	[NgayCongCoDinh] [decimal](18, 2) NULL,
	[NgayCongDuAn] [decimal](18, 2) NULL,
	[LuongDuAn] [decimal](18, 2) NULL,
	[TGNT] [decimal](18, 2) NULL,
	[TGNN] [decimal](18, 2) NULL,
	[TGNL] [decimal](18, 2) NULL,
	[TGDNT] [decimal](18, 2) NULL,
	[TGDNN] [decimal](18, 2) NULL,
	[TGDNL] [decimal](18, 2) NULL,
 CONSTRAINT [PK__NS_TL_SH__BF118CD20A7378A9] PRIMARY KEY CLUSTERED 
(
	[LuongDuAnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TachPhongBan]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TachPhongBan](
	[TachPhongBanID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TuNgay] [datetime] NULL,
	[ToiNgay] [datetime] NULL,
	[ThangChiLuong] [int] NULL,
	[NamChiLuong] [int] NULL,
 CONSTRAINT [PK_NS_TL_TachPhongBan] PRIMARY KEY CLUSTERED 
(
	[TachPhongBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ThangLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ThangLuong](
	[ThangLuongID] [int] IDENTITY(1,1) NOT NULL,
	[LuongCoBan] [int] NULL,
	[LuongTrachNhiem] [int] NULL,
	[MaChucDanh] [nvarchar](50) NULL,
	[BacLuong] [decimal](5, 2) NULL,
	[NgachLuong] [nvarchar](50) NULL,
	[HeSo] [decimal](5, 2) NULL,
 CONSTRAINT [PK_NS_TL_ThangLuong] PRIMARY KEY CLUSTERED 
(
	[ThangLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ThueSuatTNCN]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ThueSuatTNCN](
	[ThueSuatID] [int] IDENTITY(1,1) NOT NULL,
	[LuongThangTu] [int] NULL,
	[LuongThangDen] [int] NULL,
	[ThueSuat] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_TL_ThueSuatTNCN] PRIMARY KEY CLUSTERED 
(
	[ThueSuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TinhSoGioLamViecLuongBoSung]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TinhSoGioLamViecLuongBoSung](
	[SoGioLamBoSungID] [int] IDENTITY(1,1) NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[NhanVienID] [int] NULL,
	[SoGioLam] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_TL_TinhSoGioLamViecLuongBoSung] PRIMARY KEY CLUSTERED 
(
	[SoGioLamBoSungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TongHopChamCong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TongHopChamCong](
	[TongHopChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[BangLuongID] [int] NULL,
	[LoaiChamCongID] [int] NULL,
	[TongSoGioLam] [float] NULL,
 CONSTRAINT [PK_NS_TL_TongHopChamCong] PRIMARY KEY CLUSTERED 
(
	[TongHopChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TongHopDangKyCong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TongHopDangKyCong](
	[TongHopDangKyCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiChamCongID] [int] NULL,
	[ThangChamCong] [datetime] NULL,
	[SoGioLam] [decimal](18, 2) NULL,
	[NgayChamCong] [datetime] NULL,
 CONSTRAINT [PK_NS_TL_TongHopDangKyCong] PRIMARY KEY CLUSTERED 
(
	[TongHopDangKyCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TongHopLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TongHopLuong](
	[TongHopLuongID] [int] IDENTITY(1,1) NOT NULL,
	[BangLuongID] [int] NULL,
	[NhomLuongID] [int] NULL,
	[TienLuong] [decimal](18, 2) NULL,
 CONSTRAINT [PK_NS_TL_TongHopLuong] PRIMARY KEY CLUSTERED 
(
	[TongHopLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_TongPhuThuocNam]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_TongPhuThuocNam](
	[NguoiPhuThuocID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[Nam] [int] NULL,
	[SoThang] [int] NULL,
 CONSTRAINT [PK_NS_TL_TongPhuThuocNam] PRIMARY KEY CLUSTERED 
(
	[NguoiPhuThuocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_ViPhamKyLuatLBS]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_ViPhamKyLuatLBS](
	[ViPhamKyLuatID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[DotChiLuong] [datetime] NULL,
	[TyLeHuong] [decimal](18, 5) NULL,
 CONSTRAINT [PK_NS_TL_ViPhamKyLuatLBS] PRIMARY KEY CLUSTERED 
(
	[ViPhamKyLuatID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TL_YeuCauChinhSuaLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TL_YeuCauChinhSuaLuong](
	[YeuCauChinhSuaLuongID] [int] IDENTITY(1,1) NOT NULL,
	[ChiTietLuongID] [int] NULL,
	[NguoiYeuCauID] [int] NULL,
	[NgayYeuCau] [datetime] NULL,
	[GiaTri] [decimal](10, 0) NULL,
	[GiaTriStr] [nvarchar](50) NULL,
	[XetDuyet] [int] NULL,
	[GiaTriGoc] [decimal](10, 0) NULL,
	[GiaTriGocStr] [nvarchar](50) NULL,
	[LyDoYeuCau] [nvarchar](500) NULL,
 CONSTRAINT [PK_NS_TL_YeuCauChinhSuaLuong] PRIMARY KEY CLUSTERED 
(
	[YeuCauChinhSuaLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TLBD_BangLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TLBD_BangLuong](
	[BangLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL,
	[DaNhanLuong] [bit] NULL,
	[NgayNhanLuong] [datetime] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[NgayQuyetDinh] [nvarchar](50) NULL,
	[TongLuong] [decimal](10, 0) NULL,
	[TongGiamTru] [decimal](10, 0) NULL,
	[ThucLinh] [decimal](10, 0) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[QTDienBienLuongID] [int] NULL,
	[ChucDanhID] [int] NULL,
	[NhomLuongID] [int] NULL,
	[Lock] [bit] NULL,
	[MaDotTinhLuong] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_TLBD_BangLuong] PRIMARY KEY CLUSTERED 
(
	[BangLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TLBD_ChamCong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TLBD_ChamCong](
	[ChamCongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[LoaiChamCongID] [int] NULL,
	[NgayChamCong] [datetime] NULL,
	[SoGioLam] [decimal](10, 2) NULL,
	[Type] [int] NULL,
	[CaLamViec] [char](1) NULL,
 CONSTRAINT [PK_NS_TLBD_ChamCong] PRIMARY KEY CLUSTERED 
(
	[ChamCongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TLBD_ChiTietLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TLBD_ChiTietLuong](
	[ChiTietLuongID] [int] IDENTITY(1,1) NOT NULL,
	[BangLuongID] [int] NULL,
	[LoaiLuongID] [int] NULL,
	[TienLuong] [decimal](10, 2) NULL,
	[ChinhSua] [bit] NULL,
	[NguoiChinhSuaID] [int] NULL,
	[NgayChinhSua] [datetime] NULL,
	[GiaTriGoc] [decimal](10, 2) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[TienLuongStr] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TLBD_ChiTietLuong] PRIMARY KEY CLUSTERED 
(
	[ChiTietLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TLBD_QTDienBienLuong]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TLBD_QTDienBienLuong](
	[QTDienBienLuongID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NOT NULL,
	[CongTyID] [int] NULL,
	[SoQuyetDinhID] [int] NULL,
	[SoQuyetDinh] [varchar](50) NULL,
	[NgayHuong] [datetime] NULL,
	[NoiLamViec] [nvarchar](250) NULL,
	[LyDo] [nvarchar](500) NULL,
	[XetDuyet] [bit] NULL,
	[NhomTinhLuongID] [int] NULL,
	[NgayQuyetDinh] [datetime] NULL,
	[ThangLuongID] [int] NULL,
	[MaLoaiLuong] [varchar](250) NULL,
	[LuongCoBan] [int] NULL,
	[LuongBHXH] [int] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[LyDoThayDoiLuongID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[NgayHetHan] [datetime] NULL,
	[LayCongTheoNhomCongThuc] [bit] NULL,
 CONSTRAINT [PK_NS_TLBD_QTDienBienLuong] PRIMARY KEY CLUSTERED 
(
	[QTDienBienLuongID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TLBD_QTDienBienLuongChiTiet]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TLBD_QTDienBienLuongChiTiet](
	[QTDienBienLuongChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiLuongID] [int] NULL,
	[QTDienBienLuongID] [int] NULL,
	[TienLuong] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[GiaTri] [decimal](18, 5) NULL,
	[GiaTriStr] [nvarchar](50) NULL,
 CONSTRAINT [PK_NS_TLBD_QTDienBienLuongChiTiet] PRIMARY KEY CLUSTERED 
(
	[QTDienBienLuongChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TruongDuLieu]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TruongDuLieu](
	[TruongDuLieuID] [int] IDENTITY(1,1) NOT NULL,
	[CongTyID] [varchar](50) NULL,
	[TenTruong] [nvarchar](50) NULL,
	[RoleMask] [int] NULL,
	[KieuTruong] [int] NULL,
	[CopyFromTruongDuLieuID] [int] NULL,
	[LocationID] [int] NULL,
	[ThuTu] [int] NULL,
	[ThuocTinh] [int] NULL,
 CONSTRAINT [PK_NS_TruongDuLieu] PRIMARY KEY CLUSTERED 
(
	[TruongDuLieuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TruongDuLieuChiTiet]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TruongDuLieuChiTiet](
	[TruongDuLieuChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[TruongDuLieuID] [int] NULL,
	[CongTyID] [int] NULL,
	[GiaTri] [int] NULL,
	[Text] [nvarchar](100) NULL,
 CONSTRAINT [PK_NS_TruongDuLieuChiTiet] PRIMARY KEY CLUSTERED 
(
	[TruongDuLieuChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_ChiTietThanhLy]    Script Date: 6/21/2018 11:30:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_ChiTietThanhLy](
	[ThietBiThanhLyID] [int] IDENTITY(1,1) NOT NULL,
	[ThietBiID] [int] NULL,
	[SoLuong] [int] NULL,
	[DonGia] [decimal](18, 0) NULL,
	[ThanhLyThietBiID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TS_ChiTietThanhLy] PRIMARY KEY CLUSTERED 
(
	[ThietBiThanhLyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_ChuyenThietBi]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_ChuyenThietBi](
	[ChuyenThietBiID] [int] IDENTITY(1,1) NOT NULL,
	[ThietBiID] [int] NULL,
	[PhongBanCuID] [int] NULL,
	[NhanVienCuID] [int] NULL,
	[PhongBanMoiID] [int] NULL,
	[NhanVienMoiID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[NgayChuyen] [datetime] NULL,
	[SoLuong] [decimal](18, 2) NULL,
	[LyDo] [nvarchar](250) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[ThietBiChaMoiID] [int] NULL,
	[ThietBiChaCuID] [int] NULL,
	[MaDuAnMoi] [nvarchar](50) NULL,
	[MaDuAnCu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ChuyenThietBi] PRIMARY KEY CLUSTERED 
(
	[ChuyenThietBiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_KiemKe]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_KiemKe](
	[KiemKeID] [int] IDENTITY(1,1) NOT NULL,
	[NgayKiemKe] [datetime] NULL,
	[NhanVienKiemKeID] [int] NULL,
	[GhiChu] [nvarchar](150) NULL,
	[PhongBanKiemKeID] [int] NULL,
	[TenDotKiemKe] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_TS_KiemKe] PRIMARY KEY CLUSTERED 
(
	[KiemKeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_KiemKeChiTiet]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_KiemKeChiTiet](
	[KiemKeChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[ThietBiID] [int] NULL,
	[NhanVienID] [int] NULL,
	[PhongBanID] [int] NULL,
	[SoLuong] [int] NULL,
	[SoLuongKiemKe] [int] NULL,
	[GhiChu] [nvarchar](50) NULL,
	[KiemKeID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TS_KiemKeChiTiet] PRIMARY KEY CLUSTERED 
(
	[KiemKeChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_NhaCungCap]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_NhaCungCap](
	[NhaCungCapID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhaCungCap] [nvarchar](50) NULL,
	[TenNguoiDaiDien] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[ThuDienTu] [nvarchar](50) NULL,
	[Ghichu] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TS_NhaCungCap] PRIMARY KEY CLUSTERED 
(
	[NhaCungCapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_NhomThietBi]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_NhomThietBi](
	[NhomThietBiID] [int] IDENTITY(1,1) NOT NULL,
	[NhomChaID] [int] NULL,
	[MaNhom] [nvarchar](50) NULL,
	[TenNhom] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](500) NULL,
	[MoTa] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TS_NhomThietBi] PRIMARY KEY CLUSTERED 
(
	[NhomThietBiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_SuaChuaThietBi]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_SuaChuaThietBi](
	[SuaChuaThietBiID] [int] IDENTITY(1,1) NOT NULL,
	[ThietBiID] [int] NULL,
	[LanSuaChua] [int] NULL,
	[NgaySuaChua] [datetime] NULL,
	[DonGia] [decimal](18, 0) NULL,
	[LyDoSua] [nvarchar](500) NULL,
	[ChiTietSua] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_TS_SuaChuaThietBi] PRIMARY KEY CLUSTERED 
(
	[SuaChuaThietBiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_ThanhLyThietBi]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_ThanhLyThietBi](
	[ThanhLyThietBiID] [int] IDENTITY(1,1) NOT NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](250) NULL,
	[NgayLap] [datetime] NULL,
	[KhachHang] [nvarchar](50) NULL,
	[LyDo] [nvarchar](500) NULL,
	[HinhThucXuLy] [nvarchar](500) NULL,
	[DaiDienCongTy] [nvarchar](50) NULL,
	[DiaDiem] [nvarchar](50) NULL,
	[ThietBiID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[SoLuong] [decimal](18, 2) NULL,
	[DonGia] [decimal](18, 0) NULL,
	[TongSoTien] [decimal](18, 0) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ThanhLyThietBi] PRIMARY KEY CLUSTERED 
(
	[ThanhLyThietBiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NS_TS_ThietBi]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NS_TS_ThietBi](
	[ThietBiID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[PhongBanID] [int] NULL,
	[MaThietBi] [nvarchar](50) NULL,
	[TenThietBi] [nvarchar](50) NULL,
	[MoTa] [nvarchar](4000) NULL,
	[DonViTinh] [nvarchar](50) NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[Image] [image] NULL,
	[Ghichu] [nvarchar](4000) NULL,
	[NhaCungCap] [nvarchar](50) NULL,
	[ThoiGianKhauHao] [decimal](18, 0) NULL,
	[NgayTinhKhauHao] [datetime] NULL,
	[GiaTri] [decimal](18, 0) NULL,
	[NhomThietBiID] [int] NULL,
	[NgayDuaVaoSuDung] [datetime] NULL,
	[ThietBiChaID] [int] NULL,
	[NhaCungCapID] [int] NULL,
	[NguoiKyID] [int] NULL,
	[SoQuyetDinh] [nvarchar](50) NULL,
	[CaNhanYeuCauID] [int] NULL,
	[MaPhu] [nvarchar](100) NULL,
	[Day] [nvarchar](50) NULL,
	[Cot] [int] NULL,
	[Tang] [int] NULL,
	[FileSo] [nvarchar](50) NULL,
	[NgayInTaiLieu] [datetime] NULL,
	[NgayGui] [datetime] NULL,
	[MaDuAn] [nvarchar](50) NULL,
	[KhoGiayTaiLieu] [nvarchar](50) NULL,
	[SoLuong] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_NS_ThietBi] PRIMARY KEY CLUSTERED 
(
	[ThietBiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhongBan]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhongBan](
	[PhongBanID] [int] IDENTITY(1,1) NOT NULL,
	[MaPhongBan] [nvarchar](50) NULL,
	[Ten] [nvarchar](100) NULL,
	[Mota] [nvarchar](500) NULL,
	[PhongBanChaID] [int] NULL,
	[ThuTu] [bigint] NULL,
	[ChucNang] [nvarchar](4000) NULL,
	[NhiemVu] [nvarchar](4000) NULL,
	[Icon] [image] NULL,
	[KhoiID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[CongTyID] [int] NULL,
	[IsChiNhanh] [int] NULL,
	[PhongBanCon] [nvarchar](500) NULL,
	[Level] [int] NULL,
	[PhongBanCha] [nvarchar](50) NULL,
	[NgayHieuLuc] [datetime] NULL,
	[NgayHetHieuLuc] [datetime] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[PhongBanGocID] [int] NULL,
	[ChucVuLanhDaoID] [int] NULL,
	[MaPBHoachToan] [nvarchar](50) NULL,
	[MaChiNhanhHoachToan] [nvarchar](50) NULL,
	[TenPhongBanTA] [nvarchar](50) NULL,
	[MaPBHachToanCu] [nvarchar](50) NULL,
	[Keyword] [nvarchar](250) NULL,
 CONSTRAINT [PK_PhongBan] PRIMARY KEY CLUSTERED 
(
	[PhongBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhongBan_DiChuyen]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhongBan_DiChuyen](
	[PhongBanDiChuyenID] [int] IDENTITY(1,1) NOT NULL,
	[PhongBanID] [int] NULL,
	[PhongBanChaCuID] [int] NULL,
	[PhongBanChaMoiID] [int] NULL,
	[NgayDiChuyen] [datetime] NULL,
	[NguoiDiChuyenID] [int] NULL,
 CONSTRAINT [PK_PhongBan_DiChuyen] PRIMARY KEY CLUSTERED 
(
	[PhongBanDiChuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PivotLuongBoSung]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PivotLuongBoSung](
	[MaCNHT] [nvarchar](50) NULL,
	[MaPBHT] [nvarchar](50) NULL,
	[MaPB] [nvarchar](50) NULL,
	[ThucLinh] [decimal](18, 2) NULL,
	[stt] [int] NULL,
	[Ten] [nvarchar](1000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Poll_Item]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Poll_Item](
	[PollItemID] [int] IDENTITY(1,1) NOT NULL,
	[PollID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ViewOrder] [int] NULL,
	[TotalVotes] [int] NULL,
 CONSTRAINT [PK_Poll_Item] PRIMARY KEY CLUSTERED 
(
	[PollItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Poll_Poll]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Poll_Poll](
	[PollID] [int] IDENTITY(1,1) NOT NULL,
	[DisplayText] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[StartDate] [datetime] NULL,
	[ExpiredDate] [datetime] NULL,
	[PhongBanID] [int] NULL,
	[ChucVuID] [int] NULL,
	[ChucDanhID] [int] NULL,
	[LoaiHopDongID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Poll_Poll] PRIMARY KEY CLUSTERED 
(
	[PollID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Poll_PollUser]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Poll_PollUser](
	[PollUserID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[PollItemID] [int] NULL,
	[Comment] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_Poll_PollUser] PRIMARY KEY CLUSTERED 
(
	[PollUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Activity]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Activity](
	[ActivityID] [bigint] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[TieuDe] [nvarchar](50) NULL,
	[Link] [nvarchar](50) NULL,
	[IsSaw] [bit] NULL,
	[NgayThang] [datetime] NULL,
	[NguoiThucHienID] [int] NULL,
	[Icon] [nvarchar](50) NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_SYS_Activity] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_CanhBao]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_CanhBao](
	[CanhBaoID] [int] IDENTITY(1,1) NOT NULL,
	[LoaiCanhBaoID] [int] NULL,
	[TieuDe] [nvarchar](255) NULL,
	[CauLenhSQL] [ntext] NULL,
	[MoTa] [nvarchar](250) NULL,
	[CauLenhSQLGrid] [ntext] NULL,
 CONSTRAINT [PK_SYS_CanhBao] PRIMARY KEY CLUSTERED 
(
	[CanhBaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Data]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Data](
	[DataID] [int] IDENTITY(1,1) NOT NULL,
	[DataCode] [nvarchar](50) NULL,
	[Data] [image] NULL,
	[TenBaoCao] [nvarchar](255) NULL,
	[Type] [int] NULL,
	[DataLayout] [image] NULL,
	[ReportCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_Data] PRIMARY KEY CLUSTERED 
(
	[DataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_FileUpdate]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_FileUpdate](
	[FileUpdateID] [int] IDENTITY(1,1) NOT NULL,
	[TenFile] [nvarchar](50) NULL,
	[Data] [image] NULL,
	[FileDate] [datetime] NULL,
	[NgayUpload] [datetime] NULL,
	[NguoiThemID] [int] NULL,
 CONSTRAINT [PK_FileUpdate] PRIMARY KEY CLUSTERED 
(
	[FileUpdateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Language]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LanguageCulture] [nvarchar](20) NOT NULL,
	[UniqueSeoCode] [nvarchar](2) NULL,
	[FlagImageFileName] [nvarchar](50) NULL,
	[Rtl] [bit] NOT NULL,
	[LimitedToCompanies] [bit] NOT NULL,
	[Published] [bit] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
 CONSTRAINT [PK__SYS_Lang__3214EC0709B45E9A] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_LichSuThayDoiThongTin]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LichSuThayDoiThongTin](
	[LichSuThayDoiThongTinID] [int] IDENTITY(1,1) NOT NULL,
	[TenBang] [nvarchar](50) NULL,
	[KeyFieldName] [nvarchar](50) NULL,
	[IDValue] [int] NULL,
	[Mota] [nvarchar](250) NULL,
	[NgayThayDoi] [datetime] NULL,
	[XetDuyet] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ID] [int] NULL,
 CONSTRAINT [PK_SYS_LichSuThayDoiThongTin] PRIMARY KEY CLUSTERED 
(
	[LichSuThayDoiThongTinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_LichSuThayDoiThongTinChiTiet]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LichSuThayDoiThongTinChiTiet](
	[LichSuThayDoiThongTinChiTietID] [int] IDENTITY(1,1) NOT NULL,
	[LichSuThayDoiThongTinID] [int] NULL,
	[TruongThongTin] [nvarchar](100) NULL,
	[GiaTriCu] [nvarchar](2000) NULL,
	[GiaTriMoi] [nvarchar](2000) NULL,
	[MoTaGiaTriCu] [nvarchar](500) NULL,
	[MoTaGiaTriMoi] [nvarchar](500) NULL,
	[KieuDuLieu] [nvarchar](50) NULL,
	[GiaTriDouble] [decimal](18, 5) NULL,
	[GiaTriDateTime] [datetime] NULL,
 CONSTRAINT [PK_SYS_LichSuThayDoiThongTinChiTiet] PRIMARY KEY CLUSTERED 
(
	[LichSuThayDoiThongTinChiTietID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_LoaiCanhBao]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LoaiCanhBao](
	[LoaiCanhBaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiCanhBao] [nvarchar](250) NULL,
	[MoTa] [nvarchar](500) NULL,
	[MaCanhBao] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_LoaiCanhBao] PRIMARY KEY CLUSTERED 
(
	[LoaiCanhBaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_LocaleStringResource]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LocaleStringResource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[ResourceName] [nvarchar](200) NOT NULL,
	[ResourceValue] [nvarchar](2000) NOT NULL,
 CONSTRAINT [PK__SYS_Loca__3214EC070D84EF7E] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Log]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [nvarchar](50) NULL,
	[TenLog] [nvarchar](255) NULL,
	[ChiTiet] [nvarchar](500) NULL,
	[LyDo] [nvarchar](500) NULL,
	[TenMay] [nvarchar](50) NULL,
	[ThoiGian] [datetime] NULL,
	[HanhDong] [nvarchar](255) NULL,
	[MoTa] [nvarchar](500) NULL,
	[NgayThang] [datetime] NULL,
	[Ip] [nvarchar](100) NULL,
	[PhanHe] [nvarchar](500) NULL,
 CONSTRAINT [PK_SYS_Log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_LogPage]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LogPage](
	[LogPageID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[Page] [nvarchar](50) NULL,
	[Parameters] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[AccessDate] [datetime] NULL,
 CONSTRAINT [PK_SYS_LogPage] PRIMARY KEY CLUSTERED 
(
	[LogPageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_MauBaoCao]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_MauBaoCao](
	[MauBaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[TenBaoCao] [nvarchar](500) NULL,
	[Type] [int] NULL,
	[MaBaoCao] [nvarchar](50) NULL,
	[NhomBaoCao] [nvarchar](50) NULL,
	[SQLCommand] [ntext] NULL,
	[Data] [image] NULL,
	[LoaiBaoCao] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_MauBaoCao] PRIMARY KEY CLUSTERED 
(
	[MauBaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung](
	[NguoiDungID] [int] IDENTITY(1,1) NOT NULL,
	[NhanVienID] [int] NULL,
	[TenDangNhap] [nvarchar](50) NULL,
	[MatKhau] [nvarchar](50) NULL,
	[LastLogin] [datetime] NULL,
	[LastLogout] [datetime] NULL,
	[Active] [bit] NULL,
	[QuyenSQL] [nvarchar](50) NULL,
	[PQ_ChiNhanh] [nvarchar](50) NULL,
	[PQ_BHXH] [nvarchar](50) NULL,
	[PG_BHXH] [nvarchar](50) NULL,
	[IsPortalAccount] [bit] NULL,
	[IsADAccount] [bit] NULL,
	[Settings] [image] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NhomNguoiDungID] [int] NULL,
	[ActiveModule] [nvarchar](50) NULL,
	[CapBacDanhGia] [int] NULL,
	[DanhGia_ReadOnly] [bit] NULL,
	[PhanQuyen_GhiChu] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[NhomQuyen] [int] NULL,
	[NhomQuyenID] [int] NULL,
	[AID] [nvarchar](50) NULL,
 CONSTRAINT [PK_DangNhap] PRIMARY KEY CLUSTERED 
(
	[NguoiDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung_BaoCao]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung_BaoCao](
	[NguoiDungBaoCaoID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[BaoCaoID] [int] NULL,
	[XetDuyet] [int] NULL,
	[Quyen] [int] NULL,
 CONSTRAINT [PK_SYS_NguoiDung_BaoCao] PRIMARY KEY CLUSTERED 
(
	[NguoiDungBaoCaoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung_NhomQuyen]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung_NhomQuyen](
	[NguoiDungNhomQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[NhomQuyen] [nvarchar](50) NULL,
	[NguoiDungID] [int] NULL,
	[DoiTuongID] [int] NULL,
	[Quyen] [int] NULL,
	[DoiTuongStr] [nvarchar](50) NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_SYS_NguoiDung_NhomQuyen] PRIMARY KEY CLUSTERED 
(
	[NguoiDungNhomQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung_NhomXetDuyet]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung_NhomXetDuyet](
	[NguoiDungNhomXetDuyetID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[NhomXetDuyetID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_SYS_NguoiDung_NhomXetDuyet] PRIMARY KEY CLUSTERED 
(
	[NguoiDungNhomXetDuyetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung_PhongBan]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung_PhongBan](
	[NguoiDungPhongBanID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[PhongBanID] [int] NULL,
	[PhanQuyenID] [int] NULL,
	[CongTyID] [int] NULL,
	[XetDuyet] [int] NULL,
 CONSTRAINT [PK_NS_NguoiDung_PhongBan] PRIMARY KEY CLUSTERED 
(
	[NguoiDungPhongBanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NguoiDung_Quyen]    Script Date: 6/21/2018 11:30:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NguoiDung_Quyen](
	[NguoiDungQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[QuyenID] [nvarchar](50) NULL,
	[ThaoTac] [int] NULL,
	[Action] [nvarchar](250) NULL,
	[Controller] [nvarchar](250) NULL,
	[XetDuyet] [int] NULL,
	[TenQuyen] [nvarchar](250) NULL,
 CONSTRAINT [PK_SYS_NguoiDung_Quyen] PRIMARY KEY CLUSTERED 
(
	[NguoiDungQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NhomNguoiDung]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NhomNguoiDung](
	[NhomNguoiDungID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomNguoiDung] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[DuLieu] [image] NULL,
 CONSTRAINT [PK_SYS_NhomNguoiDung] PRIMARY KEY CLUSTERED 
(
	[NhomNguoiDungID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NhomPhanQuyen]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NhomPhanQuyen](
	[NhomPhanQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomPhanQuyen] [nvarchar](50) NULL,
	[Quyen] [nvarchar](max) NULL,
	[Data] [image] NULL,
 CONSTRAINT [PK_SYS_NhomPhanQuyen] PRIMARY KEY CLUSTERED 
(
	[NhomPhanQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NhomQuyen]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NhomQuyen](
	[NhomQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[NhomChaID] [int] NULL,
	[TenNhomQuyen] [nvarchar](100) NULL,
	[MoTa] [nvarchar](200) NULL,
	[Quyen] [image] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_SYS_NhomQuyen] PRIMARY KEY CLUSTERED 
(
	[NhomQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_NhomXetDuyet]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_NhomXetDuyet](
	[NhomXetDuyetID] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomXetDuyet] [nvarchar](50) NULL,
	[MaNhomXetDuyet] [nvarchar](50) NULL,
	[SQLKiemTra] [nvarchar](500) NULL,
	[SQLUpdate] [nvarchar](500) NULL,
	[SQLTuChoi] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_NhomXetDuyet] PRIMARY KEY CLUSTERED 
(
	[NhomXetDuyetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_PlugIn]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_PlugIn](
	[PlugInID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[MenuPath] [nvarchar](250) NULL,
	[AttachUICode] [nvarchar](50) NULL,
	[Asm] [image] NULL,
	[Code] [nvarchar](50) NULL,
	[FromResource] [nvarchar](50) NULL,
	[ExeType] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_PlugIn] PRIMARY KEY CLUSTERED 
(
	[PlugInID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Process]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Process](
	[ProcessID] [int] IDENTITY(1,1) NOT NULL,
	[ProcessCode] [nvarchar](50) NULL,
	[Parameters] [nvarchar](max) NULL,
	[TotalItems] [int] NULL,
	[CompletedItems] [int] NULL,
	[FailItems] [int] NULL,
	[TimeStart] [datetime] NULL,
	[TimeFinished] [datetime] NULL,
	[Status] [int] NULL,
	[CPUUsage] [int] NULL,
	[MemoryUsage] [int] NULL,
	[FailItemID] [nvarchar](max) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[Des] [nvarchar](500) NULL,
 CONSTRAINT [PK_SYS_Process] PRIMARY KEY CLUSTERED 
(
	[ProcessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_ProcessFailItem]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ProcessFailItem](
	[FailItemID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ProcessID] [int] NULL,
	[Reason] [nvarchar](250) NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_SYS_ProcessFailItem] PRIMARY KEY CLUSTERED 
(
	[FailItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_ProcessLog]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ProcessLog](
	[ProcessLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcessID] [int] NULL,
	[CreatedTime] [datetime] NULL,
	[Mess] [nvarchar](250) NULL,
	[ItemID] [int] NULL,
 CONSTRAINT [PK_SYS_ProcessLog] PRIMARY KEY CLUSTERED 
(
	[ProcessLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_Quyen]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Quyen](
	[QuyenID] [int] NOT NULL,
	[NhomQuyenID] [nvarchar](50) NULL,
	[MaSo] [nvarchar](50) NULL,
	[TenQuyen] [nvarchar](255) NULL,
	[MoTa] [nvarchar](50) NULL,
 CONSTRAINT [PK_SYS_Quyen] PRIMARY KEY CLUSTERED 
(
	[QuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_ThongSo]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ThongSo](
	[ThongSoID] [int] IDENTITY(1,1) NOT NULL,
	[MaSo] [nvarchar](50) NULL,
	[TenThongSo] [nvarchar](100) NULL,
	[MoTa] [nvarchar](250) NULL,
	[GiaTri] [ntext] NULL,
	[GiaTriMacDinh] [nvarchar](50) NULL,
	[KieuDuLieu] [nvarchar](50) NULL,
	[Nhom] [nvarchar](50) NULL,
	[TenTA] [nvarchar](50) NULL,
	[XMLDes] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_SYS_ThongSo] PRIMARY KEY CLUSTERED 
(
	[ThongSoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_ThongTinCongTy]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ThongTinCongTy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenCongTy] [nvarchar](150) NULL,
	[DienThoai1] [nvarchar](50) NULL,
	[ThuDienTu] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[Logo] [image] NULL,
	[Website] [nvarchar](50) NULL,
	[ActiveCode] [nvarchar](150) NULL,
	[Version] [nvarchar](50) NULL,
	[Data] [image] NULL,
	[Fax] [nvarchar](50) NULL,
	[DienThoai] [nvarchar](50) NULL,
	[SoDKKD] [nvarchar](50) NULL,
	[MaSoThue] [nvarchar](50) NULL,
	[IsCongTyCon] [bit] NULL,
	[MaDonVi] [nvarchar](50) NULL,
	[MaCK] [nvarchar](50) NULL,
	[TienMatDK] [decimal](18, 3) NULL,
	[TienDeTraLai] [decimal](18, 3) NULL,
	[CongNoThu] [decimal](18, 3) NULL,
	[TienTe] [int] NULL,
	[CDPhi] [float] NULL,
	[BHYT] [float] NULL,
	[BHXH] [float] NULL,
	[TCMV] [float] NULL,
	[MucLuongToiThieu] [int] NULL,
	[TenDaiDien] [nvarchar](50) NULL,
	[TenChucVuDaiDien] [nvarchar](50) NULL,
	[CMTDaiDien] [nvarchar](50) NULL,
	[NgayCapCMTDaiDien] [datetime] NULL,
	[NgayCapDKKD] [datetime] NULL,
	[ChucVuDaiDien] [nvarchar](50) NULL,
	[CongThucTinhPhep] [nchar](1) NULL,
	[DataCI] [varbinary](max) NULL,
	[MaKhachHang] [nvarchar](50) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NguoiKyHDLDID] [int] NULL,
	[NguoiDaiDienID] [int] NULL,
 CONSTRAINT [PK_ThongTinNH] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_UserSettings]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_UserSettings](
	[UserSettingID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[Settings] [image] NULL,
 CONSTRAINT [PK_SYS_UserSettings] PRIMARY KEY CLUSTERED 
(
	[UserSettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_XetDuyet]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_XetDuyet](
	[XetDuyetID] [int] IDENTITY(1,1) NOT NULL,
	[TenXetDuyet] [nvarchar](500) NULL,
	[XMLDuLieu] [nvarchar](4000) NULL,
	[SQLUpdate] [nvarchar](500) NULL,
	[SQLList] [nvarchar](500) NULL,
	[DataKeyName] [nvarchar](255) NULL,
	[SQLTuChoi] [nvarchar](500) NULL,
	[XMLXemDuLieu] [nvarchar](4000) NULL,
	[QuyTrinhID] [int] NULL,
	[DataKeyID] [int] NULL,
	[StepIndex] [int] NULL,
	[TrangThai] [int] NULL,
	[Type] [int] NULL,
	[NhomXetDuyetID] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[NgayXetDuyet] [datetime] NULL,
	[YKienXetDuyet] [nvarchar](2000) NULL,
	[YKienKhongXetDuyet] [nvarchar](2000) NULL,
	[HTMLMota] [nvarchar](250) NULL,
	[ObjectID] [int] NULL,
	[NguoiTaoID] [int] NULL,
	[NgayTao] [datetime] NULL,
	[GhiChu] [nvarchar](2000) NULL,
	[SQLXetDuyetThanhCong] [nvarchar](500) NULL,
	[SQLTuChoiXetDuyet] [nvarchar](500) NULL,
 CONSTRAINT [PK_SYS_XetDuyet] PRIMARY KEY CLUSTERED 
(
	[XetDuyetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_XetDuyet_UyQuyen]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_XetDuyet_UyQuyen](
	[XetDuyetUyQuyenID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiUyQuyenID] [int] NULL,
	[NguoiDuocUyQuyenID] [int] NULL,
	[XetDuyetID] [int] NULL,
	[LoaiQuyTrinhID] [int] NULL,
	[QuyTrinhID] [int] NULL,
	[TrangThai] [int] NULL,
	[HieuLuc_TuNgay] [datetime] NULL,
	[HieuLuc_ToiNgay] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedByID] [int] NULL,
 CONSTRAINT [PK_SYS_XetDuyet_UyQuyen] PRIMARY KEY CLUSTERED 
(
	[XetDuyetUyQuyenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sysdiagrams]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysdiagrams](
	[name] [nvarchar](128) NOT NULL,
	[principal_id] [int] NOT NULL,
	[diagram_id] [int] IDENTITY(1,1) NOT NULL,
	[version] [int] NULL,
	[definition] [varbinary](max) NULL,
 CONSTRAINT [PK__sysdiagrams__1758727B] PRIMARY KEY CLUSTERED 
(
	[diagram_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tam]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tam](
	[NhanVienID] [int] NULL,
	[soqd] [nvarchar](500) NULL,
	[ngayhuong] [datetime] NULL,
	[ngayquyetdinh] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tam_CongLuong]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tam_CongLuong](
	[NhanVienID] [int] NULL,
	[CapMotID] [int] NULL,
	[CapHaiID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tam_HoSo]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tam_HoSo](
	[NhanVienID] [int] NULL,
	[CapMotID] [int] NULL,
	[CapHaiID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tam_impdblmoi]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tam_impdblmoi](
	[nhanvienid] [int] NULL,
	[phongbanid] [int] NULL,
	[chucdanhid] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tam_luong]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tam_luong](
	[NhanVienID] [int] NULL,
	[Thang] [int] NULL,
	[Nam] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tam_nguoidung]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tam_nguoidung](
	[nguoidungid] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tam_TaiKhoan]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tam_TaiKhoan](
	[NhanVienID] [int] NULL,
	[MaNhanVien] [nvarchar](50) NULL,
	[TaiKhoan] [nvarchar](50) NULL,
	[Quyen] [int] NULL,
	[PhongBanID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbDinhBien]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDinhBien](
	[PhongBanChaID] [int] NOT NULL,
	[PhongBanID] [int] NULL,
	[PhongBan] [nvarchar](500) NULL,
	[ChucVu] [nvarchar](500) NULL,
	[SoLuong] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbDinhBien_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[test]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[test](
	[getdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_LoaiQuyTrinh]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_LoaiQuyTrinh](
	[MaLoaiQuyTrinh] [nvarchar](50) NOT NULL,
	[TenLoaiQuyTrinh] [nvarchar](50) NULL,
	[IsUse] [bit] NULL,
	[BatBuocCauHinh] [bit] NULL,
	[QuyTrinhMacDinh] [bit] NULL,
	[SQLGetThongTin] [nvarchar](500) NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[SQLGroupQuery] [nvarchar](500) NULL,
	[SelectQuyTrinh] [nvarchar](500) NULL,
 CONSTRAINT [PK_WF_LoaiQuyTrinh] PRIMARY KEY CLUSTERED 
(
	[MaLoaiQuyTrinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NguoiDung_NguoiXetDuyet]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NguoiDung_NguoiXetDuyet](
	[NDNXDID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiNhapID] [int] NULL,
	[NguoiXetDuyetCap1ID] [int] NULL,
	[NguoiXetDuyetCap2ID] [int] NULL,
 CONSTRAINT [PK_WF_NguoiDung_NguoiXetDuyet] PRIMARY KEY CLUSTERED 
(
	[NDNXDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NguoiDung_QuyTrinh]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NguoiDung_QuyTrinh](
	[NguoiDungQuyTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[NguoiDungID] [int] NULL,
	[QuyTrinhID] [int] NULL,
	[ChucVuID] [int] NULL,
	[ChucDanhID] [int] NULL,
	[CongTyID] [int] NULL,
	[LevelMin] [int] NULL,
	[LevelMax] [int] NULL,
	[CreatedByID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifyByID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[NhomXetDuyetID] [int] NULL,
 CONSTRAINT [PK_WF_NguoiDung_QuyTrinh] PRIMARY KEY CLUSTERED 
(
	[NguoiDungQuyTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_QuyTrinh]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_QuyTrinh](
	[QuyTrinhID] [int] IDENTITY(1,1) NOT NULL,
	[TenQuyTrinh] [nvarchar](250) NULL,
	[MaQuyTrinh] [nvarchar](250) NULL,
	[SQLXetDuyetThanhCong] [nvarchar](max) NULL,
	[SQLTuChoiXetDuyet] [nvarchar](max) NULL,
	[HTMLMotaID] [int] NULL,
	[SQLKiemTraTrung] [nvarchar](max) NULL,
	[SQLGetParameters] [nvarchar](max) NULL,
	[MaLoaiQuyTrinh] [nvarchar](100) NULL,
	[NVID] [int] NULL,
	[LQDID] [int] NULL,
 CONSTRAINT [PK_WF_QuyTrinh] PRIMARY KEY CLUSTERED 
(
	[QuyTrinhID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_QuyTrinh_BuocThucHien]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_QuyTrinh_BuocThucHien](
	[QuyTrinhBuocThucHienID] [int] IDENTITY(1,1) NOT NULL,
	[QuyTrinhID] [int] NULL,
	[TenBuocThucHien] [nvarchar](50) NULL,
	[NguoiXetDuyetID] [int] NULL,
	[ChoPhepQuayLai] [bit] NULL,
	[ThuTu] [int] NULL,
	[SQLNguoiXetDuyet] [nvarchar](max) NULL,
	[EmailContentID] [int] NULL,
	[SQLXetDuyet] [nvarchar](50) NULL,
	[SQLTuChoi] [nvarchar](50) NULL,
	[QuayTroLaiBuocID] [int] NULL,
	[NhomXetDuyetID] [int] NULL,
	[IsNhomXetDuyet] [bit] NULL,
	[NguoiDungXetDuyetID] [int] NULL,
 CONSTRAINT [PK_WF_QuyTrinh_BuocThucHien] PRIMARY KEY CLUSTERED 
(
	[QuyTrinhBuocThucHienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_QuyTrinh_ThucHien]    Script Date: 6/21/2018 11:30:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_QuyTrinh_ThucHien](
	[QuyTrinhThucHienID] [int] IDENTITY(1,1) NOT NULL,
	[QuyTrinhBuocThucHienID] [int] NULL,
	[NguoiXetDuyetID] [int] NULL,
	[GhiChu] [nvarchar](2500) NULL,
	[TrangThai] [int] NULL,
	[LyDoTuChoi] [nvarchar](2500) NULL,
	[NgayThucHien] [datetime] NULL,
	[XetDuyetID] [int] NULL,
	[NgayChuyenYeuCau] [datetime] NULL,
	[NhomXetDuyetID] [int] NULL,
	[NguoiDungXetDuyetID] [int] NULL,
 CONSTRAINT [PK_WF_QuyTrinh_ThucHien] PRIMARY KEY CLUSTERED 
(
	[QuyTrinhThucHienID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NS_DG_KetQuaDanhGia] ADD  CONSTRAINT [DF_NS_DG_KetQuaDanhGia_DiemTam]  DEFAULT ((0)) FOR [DiemTam]
GO
ALTER TABLE [dbo].[NS_QTChuyenCanBo] ADD  CONSTRAINT [DF_NS_QTChuyenCanBo_NghiViec]  DEFAULT ((0)) FOR [NghiViec]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 : Chưa duyệt
1 : Chưa nhận / Chưa chi / Chưa thu
2 : Đã nhận / Đã chi / Đã thu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NS_NhanVien_ThuChi', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thoi gian dao dong (minute)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ThongTinCongTy', @level2type=N'COLUMN',@level2name=N'ID'
GO
