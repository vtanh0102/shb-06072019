﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysRun
{
    public static class Configuration
    {
        public static string ConnectionString { get; set; }
        public static string CRMConnectionCode { get; set; }
        public static string UserAdminId { get; set; }
        public static string RedisServer { get; set; }
    }
}
