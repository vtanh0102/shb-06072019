﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using System.Configuration;
using Sys.Core.Enums;
using Sys.Core.AbstractSys;
using Sys.Core.DBHelper;
using System.Data;
using Sys.Core;
using System.Diagnostics;
using Sys.Core.Services;

namespace SysRun.ImpSys
{
    public class SysHumanResource : SysSolution
    {

        #region Constructors

        public SysHumanResource(string connectionString, string connectionCode, string crmConnectionCode, string redisServer)
            : base(connectionString, crmConnectionCode, redisServer)
        {
            this.ConnectionCode = connectionCode;
            this.TypeSys = TypeSys.HUMAN_RESOURCE;
        }

        #endregion

        #region Methods

        public override void ExecuteSys(string userId)
        {
            // Thêm dữ liệu vào bảng lịch sử đồng bộ
            messageLogService.Insert(Configuration.UserAdminId, TypeSys);

            // Chuẩn bị: lấy thông tin cấu hình trong DB, insert lịch sử đồng bộ
            var resultPrepare = PrepareData();
            if (resultPrepare.Code != ResultCode.Success)
            {
                messageLogService.AddSyncError(resultPrepare);
                messageLogService.Update();
                return;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach(TableMapping tableMapping in this.ListTableMapping)
            {
                Console.WriteLine(tableMapping.SourceTable);
                SyncTable(tableMapping);
                Console.WriteLine("===================");
            }

            // Insert Notification
            bool enableNotification = Convert.ToBoolean(SystemConfig["EnableNotify"]);
            if (enableNotification)
            {
                notificationService.AddCompleteNotification(true, this.TypeSys);
                var resultNotify = notificationService.InsertToDB();
                if (resultNotify.Code != ResultCode.Success)
                    messageLogService.AddSyncError(resultNotify, "B_NOTIFICATION");
            }

            messageLogService.Update();
            sw.Stop();
            Console.WriteLine("Total seconds: {0}", sw.Elapsed.TotalSeconds);
        }

        #endregion

    }
}
