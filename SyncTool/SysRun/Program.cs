﻿using Sys.Core;
using Sys.Core.Enums;
using Sys.Core.Models;
using Sys.Core.Services;
using SysRun.ImpSys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysRun
{
    class Program
    {
        static void Main(string[] args)
        {
            // Store Application Configucation
            string connectionString = ConfigurationManager.AppSettings["SQLServer"].ToString();
            string password = EncryptService.Decrypt(ConfigurationManager.AppSettings["SQLServerPassword"].ToString());
            Configuration.ConnectionString = connectionString;
            if (!string.IsNullOrWhiteSpace(password))
            {
                try
                {
                    Configuration.ConnectionString = string.Format(connectionString, password);
                }
                catch (Exception) { }
            }
            Configuration.CRMConnectionCode = ConfigurationManager.AppSettings["CRMConnectionCode"].ToString();
            Configuration.UserAdminId = ConfigurationManager.AppSettings["user_id_admin"].ToString();
            Configuration.RedisServer = ConfigurationManager.AppSettings["RedisServer"].ToString();

            // Fetch CRM Data
            //Console.WriteLine("Transfer HRM data...");
            //var HRMConnectionCode = ConfigurationManager.AppSettings["HRMConnectionCode"].ToString();
            //SysHumanResource sysHRM = new SysHumanResource(Configuration.ConnectionString, HRMConnectionCode, Configuration.CRMConnectionCode, Configuration.RedisServer);
            //sysHRM.ExecuteSys(Configuration.UserAdminId);

            // Fetch KPI Data
            Console.WriteLine("Transfer KPI data...");
            var KPIConnectionCode = ConfigurationManager.AppSettings["KPIConnectionCode"].ToString();
            SysKPI sysKPI = new SysKPI(Configuration.ConnectionString, KPIConnectionCode, Configuration.CRMConnectionCode, Configuration.RedisServer);
            sysKPI.ExecuteSys(Configuration.UserAdminId);

            Console.WriteLine("Complete transfering");
            if (System.Diagnostics.Debugger.IsAttached)
                Console.ReadLine();
        }
    }
}
