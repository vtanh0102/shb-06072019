﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;
using System.Reflection;
using System.Data;
using Sys.Core.Models;

namespace Sys.Core.Utility
{
    public static class Common
    {
        #region "common method"

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetNonce()
        {
            var rand = new Random();
            int nonce = rand.Next(int.MaxValue);
            return nonce.ToString();
        }

        public static string GetTimeStamp()
        {
            var ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// Get age from birthday
        /// </summary>
        /// <param name="birthday"></param>
        /// <returns> age </returns>
        public static int GetAge(DateTime birthday)
        {
            DateTime dt = DateTime.Now;
            int age = dt.Year - birthday.Year;
            int month = dt.Month - birthday.Month;
            int day = dt.Day - birthday.Day;
            if (month < 0)
            {
                age--;
            }
            else
            {
                if (month == 0 && day < 0)
                {
                    age--;
                }
            }

            if (age < 0) age = 0;
            return age;
        }

        /// <summary>
        /// Join array varArray with  varJoinString
        /// </summary>
        /// <param name="varArray"></param>
        /// <param name="varJoinString"></param>
        /// <returns> return String from Join array varArray with  varJoinString</returns>
        public static string Join(string[] varArray, string varJoinString)
        {
            string newJoinVar = "";

            for (int j = 0, n = varArray.Length - 1; j < n; j++)
            {
                newJoinVar += varArray[j].ToString() + varJoinString;
            }
            if (varArray.Length >= 1)
                newJoinVar += varArray[varArray.Length - 1].ToString();
            return newJoinVar;
        }

        /// <summary>
        /// Split the variables into array based on split string you provide, default split provided by csharp doesnt support second param as string.
        /// </summary>
        /// <param name="searchString">The string in which you want to search</param>
        /// <param name="splitString">Split string on which split to be done.</param>
        /// <returns>Array</returns>
        public static string[] Split(string searchString, string splitString)
        {
            //Usage:
            //string[] x;		
            //x=Split("[*]","[*]");
            //Response.Write("UpperBound:"+x.GetUpperBound(0)+"<br>");
            //for (int k=0;k<=x.GetUpperBound(0);k++)		
            //Response.Write(k+"-"+x[k].ToString()+	":<br>");	
            string srchString = searchString; //Search string in which split string need to be searched
            int arrUbound = 0;				//Upper Bound of the array.
            int SplitI = 0;					//A var.
            int startat = 0;					//A var.
            string[] splitArray;			//Array which will be returned
            //First of all know the upper bound of the array, which necessary before sending item to it.
            int charpos = srchString.IndexOf(splitString, 0);
            if (charpos == -1)
                arrUbound = 1;
            else
            {
                while ((charpos != -1) && (charpos < srchString.Length))
                {
                    arrUbound++;
                    charpos = srchString.IndexOf(splitString, charpos + splitString.Length);
                }
                arrUbound++;
            }
            splitArray = new string[arrUbound]; //Define the array
            if (arrUbound == 1)				//No split string found
                splitArray[0] = srchString;	//Send string as it is in the first element of the array.
            else
            {
                charpos = srchString.IndexOf(splitString, 0);
                while ((charpos != -1) && (charpos < srchString.Length))
                {
                    splitArray[SplitI] = srchString.Substring(startat, charpos - startat);
                    startat = charpos + splitString.Length;
                    SplitI++;
                    if (startat >= srchString.Length)
                        break;
                    else
                        charpos = srchString.IndexOf(splitString, startat);
                }
                if (startat >= srchString.Length)
                    splitArray[SplitI] = ""; //Split string found at end of the string, add a blank array element.
                else
                    splitArray[SplitI] = srchString.Substring(startat, (srchString.Length - startat)); //Add the last element into the array.
            }
            return splitArray;
        }


        /// <summary>
        /// ReplaceAll needle in haystack by replacement
        /// </summary>
        /// <param name="haystack"></param>
        /// <param name="needle"></param>
        /// <param name="replacement"></param>
        /// <returns>string haystack not exists needle </returns>
        public static string ReplaceAll(string haystack, string needle, string replacement)
        {
            if (needle.Equals(replacement)) return haystack;
            string[] newvarbarray = Split(haystack, needle);
            return Join(newvarbarray, replacement);

        }



        /// <summary>
        /// Ham nay dung de cat xau theo do dai duoc chuyen vao theo length
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string cutStrExper(string inputText, int length)
        {
            if (!String.IsNullOrEmpty(inputText))
            {
                if (inputText.Length > length)
                {
                    inputText = inputText.Substring(0, length) + " ...";
                }
                return inputText;
            }
            else
            {
                return String.Empty;
            }
        }


        /// <summary>
        /// Get Parameter value from QueryString in current context with name requestString
        /// </summary>
        /// <param name="requestString"></param>
        /// <param name="defaultID"></param>
        /// <returns> value</returns>
        public static int GetIdFromQueryString(string requestString, int defaultID)
        {
            string requestStringtamp = "?" + requestString;
            requestString = HttpContext.Current.Request.QueryString[requestString];
            if (String.IsNullOrEmpty(requestString))
            {
                requestString = HttpContext.Current.Request.QueryString[requestStringtamp];
            }

            if (!String.IsNullOrEmpty(requestString))
            {
                int.TryParse(requestString, out defaultID);
            }
            return defaultID;
        }






        /// <summary>
        /// Get Parameter value from QueryString with name requestString
        /// </summary>
        /// <param name="requestString"></param>
        /// <param name="defaultvalue"></param>
        /// <returns>value</returns>
        public static string GetStringFromQueryString(string requestString, string defaultvalue)
        {
            string requestStringtamp = "?" + requestString;
            requestString = HttpContext.Current.Request.QueryString[requestString];
            if (String.IsNullOrEmpty(requestString))
            {
                requestString = HttpContext.Current.Request.QueryString[requestStringtamp];
            }

            if (!String.IsNullOrEmpty(requestString))
                return requestString.Trim();
            else
                return defaultvalue.Trim();
        }




        /// <summary>
        /// Get date from string date format (dd-MM-yyyy OR dd-MM-yyyy hh:mm:ss or dd/MM/yyyy or dd/MM/yyyy hh:mm:ss)
        /// </summary>
        /// <param name="date"></param>
        /// <returns>date</returns>
        public static DateTime GetDateTime(string stringdate)
        {
            DateTime dt = DateTime.Now;
            try
            {
                string[] strs = stringdate.Trim().Replace("\'", "").Split(' ');
                if (strs.Length > 1)
                {
                    dt = Getdate(strs[0], strs[1], dt);
                }
                else
                {
                    dt = Getdate(strs[0], String.Empty, dt);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("stringdate is cannot Parse to date", "stringdate");
            }
            return dt;
        }

        /// <summary>
        /// Get date from string date format (dd-MM-yyyy OR dd-MM-yyyy hh:mm:ss or dd/MM/yyyy or dd/MM/yyyy hh:mm:ss)
        /// </summary>
        /// <param name="date"></param>
        /// <returns>date</returns>
        public static DateTime GetDateTimeYMD(string stringdate)
        {
            DateTime dt = DateTime.Now;
            try
            {
                string[] strs = stringdate.Trim().Replace("\'", "").Split(' ');
                if (strs.Length > 1)
                {
                    dt = GetdateYMD(strs[0], strs[1], dt);
                }
                else
                {
                    dt = GetdateYMD(strs[0], String.Empty, dt);
                }
            }
            catch (Exception)
            {
                throw new ArgumentException("stringdate is cannot Parse to date", "stringdate");
            }
            return dt;
        }
        private static DateTime GetdateYMD(string date, string time, DateTime defaultdate)
        {
            if (String.IsNullOrEmpty(date)) return defaultdate;
            int year = defaultdate.Year;
            int month = defaultdate.Month;
            int day = defaultdate.Day;
            int hour = defaultdate.Hour;
            int min = defaultdate.Minute;
            int sec = defaultdate.Second;

            date = date.Trim();
            string[] strs = date.Split('-');
            if (strs.Length < 3)
            {
                strs = date.Split('/');
            }
            if (strs.Length == 3)
            {
                int.TryParse(strs[2], out day);
                int.TryParse(strs[1], out month);
                int.TryParse(strs[0], out year);

                if (!String.IsNullOrEmpty(time))
                {
                    string[] strs2 = time.Split(':');
                    if (strs2.Length == 3)
                    {
                        int.TryParse(strs2[0], out hour);
                        int.TryParse(strs2[1], out min);
                        int.TryParse(strs2[2], out sec);

                    }
                }

                return new DateTime(year, month, day, hour, min, sec);
            }
            else
            {
                return defaultdate;
            }

        }

        private static DateTime Getdate(string date, string time, DateTime defaultdate)
        {
            if (String.IsNullOrEmpty(date)) return defaultdate;
            int year = defaultdate.Year;
            int month = defaultdate.Month;
            int day = defaultdate.Day;
            int hour = defaultdate.Hour;
            int min = defaultdate.Minute;
            int sec = defaultdate.Second;

            date = date.Trim();
            string[] strs = date.Split('-');
            if (strs.Length < 3)
            {
                strs = date.Split('/');
            }
            if (strs.Length == 3)
            {
                int.TryParse(strs[0], out day);
                int.TryParse(strs[1], out month);
                int.TryParse(strs[2], out year);

                if (!String.IsNullOrEmpty(time))
                {
                    string[] strs2 = time.Split(':');
                    if (strs2.Length == 3)
                    {
                        int.TryParse(strs2[0], out hour);
                        int.TryParse(strs2[1], out min);
                        int.TryParse(strs2[2], out sec);

                    }
                }

                return new DateTime(year, month, day, hour, min, sec);
            }
            else
            {
                return defaultdate;
            }

        }

        public static string GetClientIP()
        {
            var ip = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_CITRIX"] != null)
                {
                    ip = HttpContext.Current.Request.ServerVariables["HTTP_CITRIX"];
                }

                if (string.IsNullOrEmpty(ip) && HttpContext.Current.Request.Headers["CITRIX_CLIENT_HEADER"] != null)
                {
                    ip = HttpContext.Current.Request.Headers["CITRIX_CLIENT_HEADER"];
                }

                if (string.IsNullOrEmpty(ip))
                {
                    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                    {
                        ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    }
                    if (ip == "")
                    {
                        ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(typeof(Common), ex.Message, ex);
                return "";
            }

            return ip.Replace('|', '#').Trim();
        }

        public static bool ValidateIP(string consumerip, string clientIp)
        {
            var result = false;
            try
            {
                if (consumerip.Contains("0.0.0.0") || consumerip.Contains(clientIp))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(typeof(Common),
                    string.Format("ValidateIP(consumerip={0}): Error={1}", consumerip, ex.Message), ex);
                result = false;
            }
            finally
            {
                if (!result)
                {
                    CommonProvider.LogService.Warn(typeof(Common),
                        string.Format("Request by consumerip={0} from ip={1})", consumerip, clientIp));
                }
            }
            return result;
        }

        public static string HideInfo(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return String.Empty;

            }
            char[] chars = input.ToString().ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {

                if (i > 0 && ((int)chars[i - 1] == 64 || (int)chars[i] == 64))
                {
                    chars[i] = chars[i];

                }
                else
                {
                    if (i > 2)
                    {
                        chars[i] = '*';
                    }

                }
            }
            return new string(chars);
        }



        #endregion

        #region "msisdn"
        /// <summary>
        /// Kiểm tra đầu vào có phải là số điện thoại hay không
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsMobile(string input)
        {
            var isMobile = false;
            if (string.IsNullOrEmpty(input))
            {
                return isMobile;
            }
            isMobile = Regex.IsMatch(input, @"^((0|84|\+84)\d{9,10})$", RegexOptions.IgnoreCase);
            return isMobile;
        }
        public static string HideMobileInfo(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return String.Empty;

            }
            char[] chars = input.ToString().ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {

                if (i < chars.Length - 3)
                {
                    chars[i] = '*';

                }
                else
                {
                    chars[i] = chars[i];
                }
            }
            return new string(chars);
        }

        /// <summary>
        /// format ve dang 09xxxxxxxx
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static string GetRegularMobile(string mobile)
        {
            if (mobile.StartsWith("84"))
                mobile = string.Format("0{0}", mobile.Substring(2, mobile.Length - 2));
            if (mobile.StartsWith("+84"))
                mobile = string.Format("0{0}", mobile.Substring(3, mobile.Length - 3));
            return mobile;
        }

        /// <summary>
        /// format ve dang 849xxxxxxxx
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static string GetNationalMobile(string mobile)
        {
            if (mobile.StartsWith("0"))
                mobile = string.Format("84{0}", mobile.Substring(1, mobile.Length - 1));
            return mobile;
        }

        /// <summary>
        /// format ve dang bo 84 va 0 
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static string GetMobile(string mobile)
        {
            if (mobile.StartsWith("0"))
            {
                mobile = string.Format("{0}", mobile.Substring(1, mobile.Length - 1));
                return mobile;
            }

            if (mobile.StartsWith("84"))
            {
                mobile = string.Format("{0}", mobile.Substring(2, mobile.Length - 2));
                return mobile;
            }

            return mobile;
        }
        #endregion

        #region "Escape"
        public static string StripDiacritics(string accented)
        {

            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");

            string strFormD = accented.Normalize(System.Text.NormalizationForm.FormD);

            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');

        }
        public static string RemoveSign(string text)
        {
            string result = String.Empty;
            if (!string.IsNullOrEmpty(text))
            {
                text = HttpUtility.HtmlDecode(text);
                result = StripDiacritics(text);
                result = Regex.Replace(result, @"[^a-zA-Z_0-9\s]", String.Empty);
                result = Regex.Replace(result.Trim(), @"\s{2,}", " ");
                result = result.Replace(" ", "-");
                result = Regex.Replace(result, @"-{2,}", @"-");
            }
            return result;
        }

        /// <summary> 
        /// Remove all script in string content
        /// </summary>
        /// <param name="s"></param>
        /// <returns>string</returns>
        public static string EscapeScript(string s)
        {
            return Regex.Replace(System.Web.HttpUtility.HtmlDecode(s), @"<script[\s\S]+</script *>", string.Empty, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Remove all html tag in string content
        /// </summary>
        /// <param name="s"></param>
        /// <returns>string</returns>
        public static string EscapeTags(string s)
        {
            s = Regex.Replace(System.Web.HttpUtility.HtmlDecode(s), @"<[^>]*>", " ");
            s = Regex.Replace(s, @"\s{2,}", " ", RegexOptions.IgnoreCase);
            return s.Replace("  ", " ").Trim();
        }


        /// <summary>
        /// Remove all special characters in inputtext
        /// </summary>
        /// <param name="inputtext"></param>
        /// <returns>string inputtext</returns>
        public static string EscapeText(string inputtext, bool removehtmltag)
        {
            if (!String.IsNullOrEmpty(inputtext))
            {
                inputtext = EscapeScript(inputtext);
                if (removehtmltag) inputtext = EscapeTags(inputtext);
                inputtext = System.Web.HttpUtility.HtmlDecode(inputtext);
                return Regex.Replace(inputtext, @"[\'\“\”\^\´\`\n\t\r]", String.Empty);
            }
            else
            {
                return inputtext;
            }
        }
        public static string EscapeName(string s)
        {

            if (s.IndexOfAny(new char[] { '[', ']', '*', '.', ' ', '(', ')' }) != -1)
                return s;
            else
                return "[" + s + "]";

        }
        #endregion

        #region "hashed path file"
        /// <summary>
        /// Bacth: Hash file content into string
        /// for example: 1e5e4212f86d8ecbe5acc956c97fa373
        /// </summary>
        /// <param name="file">file content - array of bytes</param>
        /// <returns>string with 32 characters of length</returns>
        public static string HashFile(byte[] file)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            byte[] hashed = md5.ComputeHash(file);
            foreach (byte b in hashed)
                // convert to hexa
                sb.Append(b.ToString("x2").ToLower());

            // sb = set of hexa characters
            return sb.ToString();
        }

        /// <summary>
        /// Bacth: detemine path to store file
        /// for example: [1e]-[5e]-[42]-[1e5e4212f86d8ecbe5acc956c97fa373]
        /// </summary>
        /// <param name="file">file content - array of bytes</param>
        /// <returns>hashed path</returns>
        public static List<string> GetPath(byte[] file)
        {
            string hashed = HashFile(file);
            List<string> toReturn = new List<string>(3);
            toReturn.Add(hashed.Substring(0, 3));
            toReturn.Add(hashed.Substring(3, 3));
            toReturn.Add(hashed.Substring(6, 3));
            toReturn.Add(hashed);
            return toReturn; // for example: [1e5]-[e42]-[12f]-[1e5e4212f86d8ecbe5acc956c97fa373]
        }
        public static List<String> GetPaths(string md5fileName)
        {
            List<String> toReturn = new List<String>(4);
            toReturn.Add(md5fileName.Substring(0, 3));
            toReturn.Add(md5fileName.Substring(3, 3));
            toReturn.Add(md5fileName.Substring(6, 3));
            toReturn.Add(md5fileName);
            return toReturn; // for example: [1e5]-[e42]-[12f]-[1e5e4212f86d8ecbe5acc956c97fa373]
        }
        #endregion

        #region "code"
        /// <summary>
        /// Dùng để tạo mã verify
        /// </summary>
        /// <returns></returns>
        public static string GeneVerifyCode()
        {

            var chars = "0123456789abcdefghp";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());
            return result;
        }

        /// <summary>
        /// Dùng để tạo mã khoản vay
        /// </summary>
        /// <returns></returns>
        public static string GenRandomCode()
        {
            //string[] pp = ("q,w,e,r,t,y,u,i,o,p,a,s,d,f,g,h,j,k,l,z,x,c,v,b,n,m,1,2,3,4,5,6,7,8,9").Split(',');
            string[] pp = ("0,1,2,3,4,5,6,7,8,9").Split(',');
            string tmp = "";
            Random rd = new Random();
            for (int i = 1; i <= 9; i++)
            {
                tmp += pp[rd.Next(0, pp.Length - 1)];
            }
            return tmp.ToUpper();
        }

        /// <summary>
        /// Dùng để tạo mã giao dịch
        /// </summary>
        /// <returns></returns>
        public static string GenOrderCode()
        {
            string[] pp = ("q,w,e,r,t,y,u,i,o,p,a,s,d,f,g,h,j,k,l,z,x,c,v,b,n,m,1,2,3,4,5,6,7,8,9").Split(',');
            string tmp = "";
            Random rd = new Random();
            for (int i = 1; i <= 7; i++)
            {
                tmp += pp[rd.Next(0, pp.Length - 1)];
            }
            var timeSpan = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
            return tmp.ToUpper() + timeSpan.ToString();
        }
        /// <summary>
        ///  Dùng để tạo mã OTP
        /// </summary>
        /// <returns></returns>
        public static string GeneOtpCode()
        {
            var random = new Random();
            var result = random.Next(100000, 999999);
            return result.ToString();
        }

        /// <summary>
        ///  Dùng để tạo mã OTP với 4 chữ số
        /// </summary>
        /// <returns></returns>
        public static string Generate4OtpCode()
        {
            var random = new Random();
            var result = random.Next(1000, 9999);
            return result.ToString();
        }

        #endregion

        #region image
        /// <summary>
        /// Lưu file ảnh từ base64
        /// </summary>
        /// <param name="fullOutputPath"></param>
        /// <param name="base64String"></param>
        public static string SaveBase64AsImage(string urlRoot, string base64String)
        {
            try
            {
                byte[] imageBytes = Convert.FromBase64String(base64String);

                var infoPath = GetPath(imageBytes);
                var absolute = urlRoot + "/" + infoPath[0] + "/" + infoPath[1] + "/" + infoPath[2] + "/" + infoPath[3] + ".png";
                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), infoPath[0], infoPath[1], infoPath[2]);
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string imageName = infoPath[3] + ".png";
                string imgPath = Path.Combine(path, imageName);

                Base64ToImage(base64String).Save(imgPath);
                return absolute;
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(typeof(Common), ex);
                return string.Empty;
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        #endregion

        #region Compare

        /// <summary>
        /// True: is same | False: is different
        /// </summary>
        public static bool CompareDataRow(TableMapping tableMapping, DataRow source, DataRow target)
        {
            int index = 0;
            foreach (string sourceColumn in tableMapping.MapColSource)
            {
                string targetColumn = tableMapping.MapColTarget[index];
                if (source.IsNull(sourceColumn) && target.IsNull(targetColumn))
                {
                    index++;
                    continue;
                }
                else if (source.IsNull(sourceColumn) && !target.IsNull(targetColumn))
                    return false;
                else if (!source.IsNull(sourceColumn) && target.IsNull(targetColumn))
                    return false;

                if (!source[sourceColumn].ToString().Equals(target[targetColumn].ToString()))
                    return false;
                index++;
            }
            return true;
        }

        #endregion
    }
}
