﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Utility
{
    public static class NumberUltility
    {
        public static int? ToNullable(object o)
        {
            int i;
            if (int.TryParse(o.ToString(), out i)) return i;
            return null;
        }

        public static decimal? ToNullableDecimal(object o)
        {
            decimal i;
            if (decimal.TryParse(o.ToString(), out i)) return i;
            return null;
        }

        public static int? DecimalToInt(object o)
        {
            decimal i;
            if (decimal.TryParse(o.ToString(), out i))
                return Convert.ToInt32(i);
            return null;
        }

    }
}
