﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Utility
{
    public class HttpHelper
    {
        static readonly ILog logger = LogManager.GetLogger("Sys.Core.Utility.HttpHelper");
        public static string HttpRequestGet(string url, int timeout=60000)
        {
            var result = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                //request.Proxy = null;
                request.UseDefaultCredentials = true;
                request.CookieContainer = new CookieContainer();
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "text/xml; encoding='utf-8'";
                request.KeepAlive = false;
                request.AllowAutoRedirect = true;
                request.Timeout = timeout;
                var response = (HttpWebResponse)request.GetResponse();
                var stream = response.GetResponseStream();
                if (stream != null)
                {
                    result = new StreamReader(stream).ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return result;
        }

        public static string HttpRequestPost(string url, byte[] postData)
        {
            string result = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.UseDefaultCredentials = true;
                request.CookieContainer = new CookieContainer();
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = postData.Length;
                //myRequest.KeepAlive = false;
                request.AllowAutoRedirect = true;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(postData, 0, postData.Length);
                    stream.Close();
                }
                var response = (HttpWebResponse)request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        result = new StreamReader(stream).ReadToEnd();
                        stream.Close();
                        response.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                result = null;
            }
            return result;
        }

        public static string Request(string url, string method = "GET", Dictionary<string, string> formBody = null)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                var encoding = new UTF8Encoding();
                if (!"GET".Equals(method) && formBody != null)
                {
                    var formData = string.Empty;
                    foreach (var key in formBody.Keys)
                    {
                        formData += string.Format("&{0}={1}", key, formBody[key]);
                    }
                    formData = formData.TrimStart('&');
                    var data = encoding.GetBytes(formData);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    var writer = request.GetRequestStream();
                    writer.Write(data, 0, data.Length);
                    writer.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                var stream = response.GetResponseStream();
                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    var stringData = reader.ReadToEnd();
                    reader.Close();
                    stream.Close();
                    response.Close();
                    return stringData;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return string.Empty;
        }

        public static string Request(string url, string method, string contentType, System.Collections.Specialized.NameValueCollection header,
           string data, string proxyAddress = null)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            if (!string.IsNullOrEmpty(contentType))
                request.ContentType = contentType;
            if (header != null)
                request.Headers.Add(header);

            if (!string.IsNullOrEmpty(proxyAddress))
            {
                IWebProxy proxy = new WebProxy(proxyAddress);
                proxy.Credentials = new NetworkCredential();
                request.Proxy = proxy;
            }
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(data))
                {
                    using (var swt = new StreamWriter(request.GetRequestStream()))
                    {
                        swt.Write(data);
                    }
                }


                using (WebResponse response = request.GetResponse())
                {
                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                result = null;
            }
            return result;
        }
    }
}
