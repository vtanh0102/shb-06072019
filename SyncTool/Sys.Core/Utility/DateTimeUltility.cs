﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Utility
{
    public static class DateTimeUltility
    {
        public static DateTime? ToNullable(object datetime)
        {
            DateTime result;
            if (DateTime.TryParse(datetime.ToString(), out result) == true)
                return result;
            else
                return null;
        }

        public static int MonthDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }

    }
}
