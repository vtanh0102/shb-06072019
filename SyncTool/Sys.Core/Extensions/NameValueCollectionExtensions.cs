﻿using System.Collections.Generic;
using  System.Collections.Specialized;

namespace Sys.Core.Extensions
{
    public static class NameValueCollectionExtensions
    {
         public static Dictionary<string, string> ToDictionary(this NameValueCollection nameValueCollection)
         {
             var dict = new Dictionary<string, string>();
             for (int i = 0; i < nameValueCollection.Count; i++)
             {
                 dict.Add(nameValueCollection.Keys[i].ToLower(), nameValueCollection[i]);
             }
             return dict;
         }
    }
}
