﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sys.Core.Extensions
{
    public static class DictionaryExtensions
    {
        public static bool IsNullOrEmpty<TKey, TValue>(this Dictionary<TKey, TValue> dict)
        {
            return dict == null || dict.Count == 0;
        }

        public static bool Compare<TKey, TValue>(this Dictionary<TKey, TValue> dict1, Dictionary<TKey, TValue> dict2)
        {
            if (dict1 == null || dict2 == null)
                return false;

            if (dict1.Count != dict2.Count)
                return false;

            TValue val;
            Dictionary<TKey, TValue> query = dict1
                .Where(d => !dict2.TryGetValue(d.Key, out val) || val.ToString() != d.Value.ToString())
                .ToDictionary(x => x.Key, x => x.Value);

            return query.Count == 0;
        }

        public static Dictionary<TKey, TValue> Paging<TKey, TValue>(this Dictionary<TKey, TValue> dict, int pageIndex,
                                                                    int pageSize)
        {
            return dict.Keys.Paging(pageIndex, pageSize).ToDictionary(key => key, key => dict[key]);
        }
    }
}