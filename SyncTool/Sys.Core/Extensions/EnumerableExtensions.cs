﻿using System.Collections.Generic;
using System.Linq;

namespace Sys.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TKey> Paging<TKey>(this IEnumerable<TKey> vals, int pageIndex, int pageSize)
        {
            return vals.Skip(pageIndex*pageSize).Take(pageSize);
        }
    }
}