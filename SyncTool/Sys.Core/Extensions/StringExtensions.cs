﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace Sys.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64(this string toEncode)
        {
            byte[] toEncodeAsBytes
                = Encoding.ASCII.GetBytes(toEncode);
            string returnValue
                = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string FromBase64(this string encodedData)
        {
            byte[] encodedDataAsBytes
                = Convert.FromBase64String(encodedData);
            string returnValue =
                Encoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static T ToObject<T>(this string source) where T : class, new()
        {
            if (source.IsStringNullOrEmpty()) return default(T);
            return CommonProvider.JsonService.ToObject<T>(source);
        }

        public static bool EqualsExtend(this string source, object obj)
        {
            return source.Equals(obj.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
                   source.Trim().Equals(obj.ToString().Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool ContainsExtend(this string source, object obj)
        {
            return source.Contains(obj.ToString()) || source.ToLower().Contains(obj.ToString().ToLower());
        }

        public static bool EndsWithExtend(this string source, string obj)
        {
            return source.EndsWith(obj) || source.ToLower().Trim().EndsWith(obj.ToLower().Trim());
        }

        public static bool StartsWithExtend(this string source, string obj)
        {
            return source.StartsWith(obj) || source.ToLower().Trim().StartsWith(obj.ToLower().Trim());
        }

        public static string ToMd5(this string originalString)
        {
            byte[] encodedBytes;
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var originalBytes = Encoding.Default.GetBytes(originalString);
                encodedBytes = md5.ComputeHash(originalBytes);
            }
            return BitConverter.ToString(encodedBytes).Replace("-", String.Empty).ToLower();
        }

        public static string ToEbankMd5(this string originalString)
        {
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password) 
            byte[] encodedBytes;
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                byte[] originalBytes = Encoding.Default.GetBytes(originalString);
                encodedBytes = md5.ComputeHash(originalBytes);
            }

            //Convert encoded bytes back to a 'readable' string 
            return BitConverter.ToString(encodedBytes).ToLower().Replace("-", "");
        }

        private static Dictionary<string, int> monthMap = new Dictionary<string, int>()
            {
                { "JAN", 01 },
                { "FEB", 02 },
                { "MAR", 03 },
                { "APR", 04 },
                { "MAY", 05 },
                { "JUN", 06 },
                { "JUL", 07 },
                { "AUG", 08 },
                { "SEP", 09 },
                { "OCT", 10 },
                { "NOV", 11 },
                { "DEC", 12 },
            };
        public static int MonthFromString(this string shortMonth)
        {
            return monthMap[shortMonth.Trim().ToUpper()];
        }

        public static string StringFromMonth(this int month)
        {
            return monthMap.FirstOrDefault(c => c.Value == month).Key;
        }
    }
}