﻿using System;
using System.Web;

namespace Sys.Core.Extensions
{
    public static class UriExtension
    {
        public static string GetQueryParam(this Uri uri, string paramName)
        {
            return HttpUtility.ParseQueryString(uri.Query).Get(paramName) ?? string.Empty;
        }

        public static string GetBaseAddress(this Uri uri)
        {
            if (uri != null)
                return !"localhost".Equals(uri.Host) ? uri.Host : string.Format("{0}:{1}", uri.Host, uri.Port);
            return string.Empty;
        }

        public static string GetServerUrl(this Uri uri, bool isToLower = true, string portForCitrix = "")
        {
            var serverUrl = string.Empty;
            if (uri == null) return serverUrl;

            if (uri.Port == 443 || uri.Port == 80 ||
                (!string.IsNullOrEmpty(portForCitrix) && portForCitrix.Contains(uri.Port.ToString())))
            {
                serverUrl = string.Format("{0}://{1}/", uri.Scheme, uri.Host);
            }
            else
            {
                serverUrl = string.Format("{0}://{1}:{2}/", uri.Scheme, uri.Host, uri.Port);
            }

            //Provider.LogService.Debug(typeof(UriExtension)
            //    , string.Format("GetServerUrl(uri={0}, isToLower={1}, portForCitrix={2})::serverUrl={3}"
            //    , uri.AbsoluteUri, isToLower, portForCitrix, serverUrl));

            return (isToLower) ? serverUrl.ToLower() : serverUrl;
        }
    }
}


//using System;
//using System.Web;

//namespace Graph.Go.Vn.Common.Extensions
//{
//    public static class UriExtension
//    {
//        public static string GetQueryParam(this Uri uri, string paramName)
//        {
//            return HttpUtility.ParseQueryString(uri.Query).Get(paramName) ?? string.Empty;
//        }

//        public static string GetBaseAddress(this Uri uri)
//        {
//            var baseAddress = string.Empty;
//            if (uri != null)
//                baseAddress = !"localhost".Equals(uri.Host) ? uri.Host : string.Format("{0}:{1}", uri.Host, uri.Port);

//            if (baseAddress.Contains("graph.vgg.vn"))
//                baseAddress = baseAddress.Replace("http://", "https://");

//            Provider.LogService.Debug(typeof(UriExtension), string.Format("GetBaseAddress(uri={0})::baseAddress={1}", uri, baseAddress));
//            return baseAddress;
//        }

//        public static string GetServerUrl(this Uri uri, bool isToLower = true)
//        {
//            var serverUrl = string.Empty;
//            if (uri != null)
//            {
//                serverUrl = string.Format("{0}://{1}/", uri.Scheme, uri.Host);


//                //if (uri.Port == 443 || uri.Port == 80)
//                //serverUrl = string.Format("{0}://{1}/", uri.Scheme, uri.Host);
//                //else
//                //    serverUrl = string.Format("{0}://{1}:{2}/", uri.Scheme, uri.Host, uri.Port);

//                if (serverUrl.Contains("graph.vgg.vn"))
//                    serverUrl = serverUrl.Replace("http://", "https://");

//                Provider.LogService.Debug(typeof(UriExtension), string.Format("GetServerUrl(uri={0})::serverUrl={1}", uri, serverUrl));

//                return (isToLower) ? serverUrl.ToLower() : serverUrl;
//            }

//            //serverUrl = serverUrl.Replace("http://", "https://");

//            return serverUrl;
//        }
//    }
//}