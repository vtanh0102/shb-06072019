﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Extensions
{
    public static class EntityExtensions
    {
        public static string GetTableName<T>(this T item) where T : BaseEntity
        {
            Type type = typeof(T);
            return GetTableName(type);
        }

        public static string GetTableName(Type type)
        {
            var descriptions = (DescriptionAttribute[])type.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (descriptions.Length == 0)
                return null;
            return descriptions[0].Description;
        }

        public static Dictionary<string, string> GetColumnValue<T>(this T item) where T : BaseEntity
        {
            var properties = typeof(T).GetProperties();
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var property in properties)
            {
                DescriptionAttribute objcolumn = (Attribute.GetCustomAttribute(property, typeof(DescriptionAttribute)) as DescriptionAttribute);
                if (objcolumn == null)
                    continue;

                string column = objcolumn.Description;
                if (string.IsNullOrWhiteSpace(column))
                    continue;

                object objvalue = item.GetType().GetProperty(property.Name).GetValue(item, null);
                if (objvalue == null)
                    result.Add(column, null);
                else
                    result.Add(column, objvalue.ToString());
            }
            return result;
        }

    }
}
