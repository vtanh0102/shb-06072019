﻿using System;
using System.Text.RegularExpressions;

namespace Sys.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static double Epoch(this DateTime val)
        {
            return Math.Floor((val.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds);
        }

        public static DateTime FromJsonValue(string input)
        {
            var rg = new Regex("\\([^\\d]*(?<time>\\d+?)[^\\d]*\\)");
            MatchCollection mtCol = rg.Matches(input);
            return mtCol.Count > 0 ? FromMilliseconds(mtCol[0].Groups["time"].Value) : DateTime.MinValue;
        }

        public static DateTime FromMilliseconds(string milliseconds)
        {
            long exprice;
            return long.TryParse(milliseconds, out exprice) ? FromMilliseconds(exprice) : DateTime.UtcNow;
        }

        public static DateTime FromMilliseconds(long milliseconds)
        {
            var d = new DateTime(1970, 1, 1);
            d = d.AddMilliseconds(milliseconds);
            return d.ToLocalTime();
        }

        public static DateTime FromEpoch(string epoch)
        {
            long exprice;
            return long.TryParse(epoch, out exprice) ? FromEpoch(exprice) : DateTime.UtcNow;
        }

        public static DateTime FromEpoch(long epoch)
        {
            var d = new DateTime(1970, 1, 1);
            d = d.AddSeconds(epoch);
            return d.ToLocalTime();
        }
    }
}