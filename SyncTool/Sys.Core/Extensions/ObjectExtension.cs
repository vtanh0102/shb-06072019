using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sys.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object source)
        {
            if (source == null) return string.Empty;
            return CommonProvider.JsonService.ToJson(source);
        }

        /// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="byteArray">byte array to get object</param>
        /// <returns>object</returns>
        public static T To<T>(this byte[] byteArray)
        {
            if (byteArray == null)
                return default(T);

            try
            {
                // convert byte array to memory stream
                using (var memoryStream = new MemoryStream(byteArray))
                {
                    var binaryFormatter = new BinaryFormatter();

                    // set memory stream position to starting point
                    memoryStream.Position = 0;

                    // Deserializes a stream into an object graph and return as a object.
                    object obj = binaryFormatter.Deserialize(memoryStream);
                    return (T)obj;
                }
            }
            catch (Exception exception)
            {
                CommonProvider.LogService.Error(typeof(ObjectExtensions), exception.Message, exception);
            }
            // Error occured, return null
            return default(T);
        }

        /// <summary>
        /// Function to get byte array from a object
        /// </summary>
        /// <param name="_Object">object to get byte array</param>
        /// <returns>Byte Array</returns>
        public static byte[] ToByteArray(this object _Object)
        {
            try
            {
                // create new memory stream
                using (var memoryStream = new MemoryStream())
                {
                    var binaryFormatter = new BinaryFormatter();

                    // Serializes an object, or graph of connected objects, to the given stream.
                    binaryFormatter.Serialize(memoryStream, _Object);

                    // convert stream to byte array and return
                    return memoryStream.ToArray();
                }
            }
            catch (Exception exception)
            {
                CommonProvider.LogService.Error(typeof(ObjectExtensions), exception.Message, exception);
            }
            // Error occured, return null
            return null;
        }

        public static decimal ToDecimal(this object source)
        {
            if (source.IsStringNullOrEmpty()) return 0;

            decimal value;
            return decimal.TryParse(source.ToString(), out value) ? value : 0;
        }

        public static float ToFloat(this object source)
        {
            if (source.IsStringNullOrEmpty()) return 0;

            float value;
            return float.TryParse(source.ToString(), out value) ? value : 0;
        }

        public static int ToInt32(this object source)
        {
            if (source.IsStringNullOrEmpty()) return 0;

            int value;
            return int.TryParse(source.ToString(), out value) ? value : 0;
        }

        public static long ToInt64(this object source)
        {
            if (source.IsStringNullOrEmpty()) return 0;

            long value;
            return long.TryParse(source.ToString(), out value) ? value : 0;
        }

        public static byte ToByte(this object source)
        {
            if (source.IsStringNullOrEmpty()) return 0;

            byte value;
            return byte.TryParse(source.ToString(), out value) ? value : Convert.ToByte(0);
        }

        public static bool IsInteger(this object obj)
        {
            int outObj;
            return int.TryParse(obj.ToString(), out outObj);
        }

        public static bool IsLong(this object obj)
        {
            long outObj;
            return long.TryParse(obj.ToString(), out outObj);
        }

        public static bool IsIntegerNull(this object obj)
        {
            try
            {
                if (obj.IsStringNullOrEmpty()) return true;
                if (obj.IsInteger() && obj.ToString().Trim() == string.Empty) return true;
                if (obj.IsInteger() && obj.ToString().Trim() == "0") return true;
                if (obj.IsInteger() && obj.ToString().Trim() == "-1") return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsLongNull(this object obj)
        {
            try
            {
                if (obj.IsStringNullOrEmpty()) return true;
                if (obj.IsLong() && obj.ToString().Trim() == string.Empty) return true;
                if (obj.IsLong() && obj.ToString().Trim() == "0") return true;
                if (obj.IsLong() && obj.ToString().Trim() == "-1") return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsStringNullOrEmpty(this object obj)
        {
            return obj == null ||
                   obj.ToString().Length == 0 ||
                   obj.ToString().Trim().Length == 0;
        }

        private static bool IsDateTime(this object obj)
        {
            DateTime outObj;
            return DateTime.TryParse(obj.ToString(), out outObj);
        }

        public static bool IsDateTimeNull(this object obj)
        {
            try
            {
                if (obj == null) return true;
                var val = obj.ToString();
                if (obj.IsDateTime() && val.Trim() == string.Empty) return true;
                if (obj.IsDateTime() && Convert.ToDateTime(obj) == DateTime.MinValue) return true;
                if (obj.IsDateTime() && val.IndexOf("1/1/1900", StringComparison.Ordinal) > -1) return true;
                if (obj.IsDateTime() && val.IndexOf("01/01/1900", StringComparison.Ordinal) > -1) return true;
                if (obj.IsDateTime() && val.IndexOf("1/1/1753", StringComparison.Ordinal) > -1) return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool ToBoolean(this object source)
        {
            if (source.IsStringNullOrEmpty()) return false;

            bool value;
            return bool.TryParse(source.ToString(), out value) && value;
        }

        public static DateTime ToDateTime(this object source, string format = "dd/MM/yyyy hh:mm:ss")
        {
            if (source is DateTime)
                return (DateTime)source;

            return source.IsStringNullOrEmpty()
                       ? default(DateTime)
                       : DateTime.ParseExact(source.ToString(), format, CultureInfo.InvariantCulture);
        }

        public static string GetDescription(this object value, string unknown = "Unknown")
        {
            try
            {
                var fi = value.GetType().GetField(value.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                return (attributes.Length > 0) ? attributes[0].Description : string.Empty;
            }
            catch (Exception)
            {
                return unknown;
                //return string.Empty;
            }
        }
    }
}