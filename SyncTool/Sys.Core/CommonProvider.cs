﻿using Sys.Core.Cache;
using Sys.Core.IServices;
using Sys.Core.Services;
using Sys.Core.DBHelper;
using System.Configuration;

namespace Sys.Core
{
    public class CommonProvider : ProviderBase
    {
        public static ILogService LogService
        {
            get { return CreateInstance<LogService>(); }
        }

        public static IJsonService JsonService
        {
            get { return CreateInstance<JsonService>(); }
        }

        public static IMessageLog MessageLog
        {
            get { return CreateInstance<MessageLog>(); }
        }

        private static readonly object ObjLockDataCaching = new object();
        private static IDataCaching _dataCaching;
        public static IDataCaching DataCaching
        {
            get
            {
                if (_dataCaching == null)
                {
                    lock (ObjLockDataCaching)
                    {
                        if (_dataCaching == null)
                        {
                            _dataCaching = CreateInstance<DataCaching>();
                        }
                    }
                }
                return _dataCaching;
            }
        }

        private static readonly object ObjLockRedisService = new object();

        private static RedisService _redisService;
        public static RedisService RedisService
        {
            get
            {
                if (_redisService == null)
                {
                    lock (ObjLockRedisService)
                    {
                        if (_redisService == null)
                        {
                            _redisService = CreateInstance<RedisService>(ConfigurationManager.AppSettings["RedisServer"].ToString());
                        }
                    }
                }
                return _redisService;
            }
        }

    }
}