using System;
using Sys.Core.Enums;

namespace Sys.Core.IServices
{
    public interface ILogService
    {
        T OnError<T>(Type typeOfOwner, T result, Exception ex = null, ResultCode code = ResultCode.Unknown,
                     string message = default(string)) where T : Result;

        void Debug(Type typeOfOwner, object message, Exception exception = null);
        void DebugFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args);
        void DebugFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2);
        void DebugFormat(Type typeOfOwner, string format, object arg0, object arg1);
        void DebugFormat(Type typeOfOwner, string format, object arg0);
        void DebugFormat(Type typeOfOwner, string format, params object[] args);
        void Error(Type typeOfOwner, object message, Exception exception = null);
        void ErrorFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args);
        void ErrorFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2);
        void ErrorFormat(Type typeOfOwner, string format, object arg0, object arg1);
        void ErrorFormat(Type typeOfOwner, string format, object arg0);
        void ErrorFormat(Type typeOfOwner, string format, params object[] args);
        void Fatal(Type typeOfOwner, object message, Exception exception = null);
        void FatalFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args);
        void FatalFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2);
        void FatalFormat(Type typeOfOwner, string format, object arg0, object arg1);
        void FatalFormat(Type typeOfOwner, string format, object arg0);
        void FatalFormat(Type typeOfOwner, string format, params object[] args);
        void Info(Type typeOfOwner, object message, Exception exception = null);
        void InfoFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args);
        void InfoFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2);
        void InfoFormat(Type typeOfOwner, string format, object arg0, object arg1);
        void InfoFormat(Type typeOfOwner, string format, object arg0);
        void InfoFormat(Type typeOfOwner, string format, params object[] args);
        void Warn(Type typeOfOwner, object message, Exception exception = null);
        void WarnFormat(Type typeOfOwner, IFormatProvider provider, string format, params object[] args);
        void WarnFormat(Type typeOfOwner, string format, object arg0, object arg1, object arg2);
        void WarnFormat(Type typeOfOwner, string format, object arg0, object arg1);
        void WarnFormat(Type typeOfOwner, string format, object arg0);
        void WarnFormat(Type typeOfOwner, string format, params object[] args);
    }
}