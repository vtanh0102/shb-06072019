﻿namespace Sys.Core.IServices
{
    public interface IJsonService
    {
        T ToObject<T>(string jsonString);

        string ToJson(object obj);

        string ToJson(object obj, string dateTimeFormat);

    }
}
