﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.DBHelper
{
    public enum DbProviders
    {
        SqlServer = 0,
        OleDb = 1,
        Oracle = 2,
        ODBC = 3
    }
}
