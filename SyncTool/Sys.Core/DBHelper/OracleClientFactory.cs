﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.DBHelper
{
    public class OracleClientFactory : DbProviderFactory
    {
        public OracleClientFactory(string sConnectionString)
            : base(sConnectionString
                  , "Oracle.DataAccess"
                  , "Oracle.DataAccess.Client.OracleConnection"
                  , "Oracle.DataAccess.Client.OracleCommand"
                  , "Oracle.DataAccess.Client.OracleDataAdapter"
                  , "Oracle.DataAccess.Client.OracleParameter"
                  , "Oracle.DataAccess.Client.OracleCommandBuilder"
                  )
        {
        }
    }
}
