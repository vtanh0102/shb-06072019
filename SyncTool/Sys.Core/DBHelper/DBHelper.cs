﻿using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Sys.Core;
using Sys.Core.Models;

namespace Sys.Core.DBHelper
{
    public class DBHelper
    {
        #region properties
        private readonly string connectionString;
        #endregion

        #region init
        public DBHelper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DBHelper(string ip, int port, string db, string id, string pass, int connectTimeout, int minPoolSize, int maxPoolSize)
        {
            connectionString = string.Format("MultipleActiveResultSets=True;Data Source={0},{1};Network Library=DBMSSOCN;Initial Catalog={2};Persist Security Info=True;User ID={3};Password={4};Connect Timeout={5};Min Pool Size = {6};Max Pool Size = {7};",
                        ip, port, db, id, pass, connectTimeout, minPoolSize, maxPoolSize);
        }
        #endregion

        #region open/close connection

        private SqlConnection OpenSqlConnection()
        {
            try
            {
                var sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                return sqlConnection;
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("OpenSqlConnection():Error={0}", ex.Message), ex);
                throw;
            }
        }

        public bool IsServerConnected()
        {
            using (var l_oConnection = new SqlConnection(connectionString))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }

        private void CloseSqlConnection(ref SqlConnection sqlConnection)
        {
            if (sqlConnection == null)
                return;
            try
            {

                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("CloseSqlConnection(sqlConnection.State={0}):Error={1}"
                    , sqlConnection.State, ex.StackTrace), ex);
            }
        }
        
        #endregion

        #region Get SqlDataReader
        public SqlDataReader GetDataReader(SqlCommand sqlCommand, params SqlParameter[] Parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                if (Parameters != null)
                {
                    foreach (var sqlParameter in Parameters)
                    {
                        sqlCommand.Parameters.Add(sqlParameter);
                    }
                }
                sqlCommand.Connection = sqlConnection;
                return sqlCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("GetDataReader():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }

        public SqlDataReader GetDataReader(string strSQL, params SqlParameter[] Parameters)
        {
            var sqlCommand = new SqlCommand(strSQL);
            return GetDataReader(sqlCommand, Parameters);
        }

        #endregion

        #region Get List<T>
        public List<T> GetList<T>(SqlCommand sqlCommand, params SqlParameter[] Parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                if (Parameters != null)
                {
                    foreach (var sqlParameter in Parameters)
                    {
                        sqlCommand.Parameters.Add(sqlParameter);
                    }
                }
                sqlCommand.Connection = sqlConnection;
                var sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.FieldCount == 0)
                    return null;

                var m_List = new List<T>();

                var builder = DynamicBuilder<T>.CreateBuilder(sqlDataReader);

                while (sqlDataReader.Read())
                {
                    var r = builder.Build(sqlDataReader);
                    m_List.Add(r);
                }
                sqlDataReader.Close();
                return m_List;
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("GetDataReader():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }

        /// <summary>
        /// get dynamic data - datdq - 16/05/2018
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public List<DynamicTable> GetDataDynamic(string strQuery)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                List<DynamicTable> products = new List<DynamicTable>();
                SqlCommand cmd = new SqlCommand { CommandText = strQuery, CommandType = CommandType.Text, Connection = sqlConnection };                
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    DataTable t = new DataTable();
                    a.Fill(t);

                    DynamicTable p = null;

                    foreach (DataRow row in t.Rows)
                    {
                        p = new DynamicTable();
                        foreach (DataColumn col in t.Columns)
                        {
                            string property = col.ColumnName.ToString();
                            string propertyValue = row[col.ColumnName].ToString();

                            Dictionary<string, string> dictionary = new Dictionary<string, string>();

                            dictionary.Add(property, propertyValue);

                            p.DynamicProperties.Add(dictionary);
                        }

                        products.Add(p);
                    }
                    return products;
                }
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("GetDataReader():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }

        public List<T> GetList<T>(SqlCommand sqlCommand, out int totalRows)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                totalRows = 0;
                sqlCommand.Connection = sqlConnection;
                var sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.FieldCount == 0)
                {
                    return null;
                }

                var m_List = new List<T>();

                var builder = DynamicBuilder<T>.CreateBuilder(sqlDataReader);

                while (sqlDataReader.Read())
                {
                    T r = builder.Build(sqlDataReader);
                    m_List.Add(r);
                }
                if (sqlDataReader.NextResult())
                {
                    if (sqlDataReader.Read())
                    {
                        totalRows = (int)sqlDataReader["TotalRowCount"];
                    }
                }
                sqlDataReader.Close();
                return m_List;
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("GetDataReader():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        public List<T> GetList<T>(string strSQL)
        {
            var sqlCommand = new SqlCommand(strSQL);
            return GetList<T>(sqlCommand);
        }
        #endregion

        # region Get DataTable
        public DataTable getDataTable(SqlCommand sqlCommand, params SqlParameter[] Parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                if (Parameters != null)
                {
                    foreach (var sqlParameter in Parameters)
                    {
                        sqlCommand.Parameters.Add(sqlParameter);
                    }
                }
                sqlCommand.Connection = sqlConnection;
                var sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                var dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);

                return dataSet.Tables.Count > 0 ? dataSet.Tables[0] : null;
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("getDataTable():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }

        public DataTable getDataTable(string strSQL, params SqlParameter[] Parameters)
        {
            var sqlCommand = new SqlCommand(strSQL);
            return getDataTable(sqlCommand, Parameters);
        }

        public DataTable getDataTableSP(string SPName, params SqlParameter[] Parameters)
        {
            var sqlCommand = new SqlCommand(SPName) { CommandType = CommandType.StoredProcedure };
            return getDataTable(sqlCommand, Parameters);
        }
        #endregion

        # region ExecuteScalar

        public object ExecuteScalar(SqlCommand sqlCommand, params SqlParameter[] Parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                if (Parameters != null)
                {
                    foreach (var sqlParameter in Parameters)
                    {
                        sqlCommand.Parameters.Add(sqlParameter);
                    }
                }
                sqlCommand.Connection = sqlConnection;
                var result =  sqlCommand.ExecuteScalar();
                return result;
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }


        public object ExecuteScalar(string commandText, CommandType commandType, params SqlParameter[] Parameters)
        {
            var sqlCommand = new SqlCommand(commandText) { CommandType = commandType };
            return ExecuteScalar(sqlCommand, Parameters);
        }

        #endregion

        # region ExecuteNonQuery

        public int ExecuteNonQuery(SqlCommand sqlCommand, params SqlParameter[] Parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                if (Parameters != null)
                {
                    foreach (var sqlParameter in Parameters)
                    {
                        sqlCommand.Parameters.Add(sqlParameter);
                    }
                }
                sqlCommand.Connection = sqlConnection;
                return sqlCommand.ExecuteNonQuery();
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }
        }

        public int ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] Parameters)
        {
            var sqlCommand = new SqlCommand(commandText) { CommandType = commandType };
            return ExecuteNonQuery(sqlCommand, Parameters);
        }

        #endregion


        #region "Methods Query StoreProcedure"

        private static void AddParameters(DbCommand command, object[] args)
        {
            if (args == null)
            {
                return;
            }

            // Create numbered parameters
            IEnumerable<DbParameter> parameters = args.Select((o, index) =>
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = index.ToString(CultureInfo.InvariantCulture);
                parameter.Value = o ?? DBNull.Value;
                return parameter;
            });

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }
        }

        private static void AddParameters(DbCommand command, SqlParameter[] parameters)
        {
            if (parameters == null)
            {
                return;
            }


            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }
        }

        private static void AddParameters(DbCommand command, string[] parameterNames, object[] parameterValues)
        {
            if (parameterNames == null)
            {
                return;
            }
            if (parameterValues == null)
            {
                return;
            }

            // Create numbered parameters
            if (parameterNames.Length < parameterValues.Length)
            {
                return;
            }


            IEnumerable<DbParameter> parameters = parameterValues.Select((o, index) =>
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = parameterNames[index];
                parameter.Value = o ?? DBNull.Value;

                return parameter;

            });

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }
        }

        private static IEnumerable<string> GetColumnNames(DbDataRecord record)
        {
            // Get all of the column names for this query
            for (int i = 0; i < record.FieldCount; i++)
            {
                yield return record.GetName(i);
            }
        }

        private string SafeSqlLiteral(string commandText)
        {
            //return commandText.Replace("'", "''");
            return commandText;
        }

        private IEnumerable<dynamic> QueryInternal(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = commandType;
                command.CommandText = commandText;

                AddParameters(command, parameters);
                using (command)
                {
                    IEnumerable<string> columnNames = null;
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            if (columnNames == null)
                            {
                                columnNames = GetColumnNames(record);
                            }
                            yield return new DynamicRecord(columnNames, record);
                        }
                    }
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }
        
        private IEnumerable<dynamic> QueryInternal(string commandText, string[] parametersName, params object[] parameters)
        {
            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = commandText;

                AddParameters(command, parametersName, parameters);
                using (command)
                {
                    IEnumerable<string> columnNames = null;
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            if (columnNames == null)
                            {
                                columnNames = GetColumnNames(record);
                            }
                            yield return new DynamicRecord(columnNames, record);
                        }
                    }
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        /// <summary>
        /// Using Only for StoreProcedure, commandText is storeprocedure name
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parametersName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public dynamic QuerySingle(string commandText, string[] parametersName, params object[] args)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            return QueryInternal(SafeSqlLiteral(commandText), parametersName, args).FirstOrDefault();
        }

        /// <summary>
        /// Sử dụng khi cần lấy tham số out
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public dynamic QuerySingle(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            return QueryInternal(SafeSqlLiteral(commandText), commandType, parameters).FirstOrDefault();
        }

        /// <summary>
        /// Sử dụng khi cần lấy ra tham số out
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> Query(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }
            // Return a readonly collection
            return QueryInternal(SafeSqlLiteral(commandText), commandType, parameters).ToList().AsReadOnly();
        }

        /// <summary>
        /// Using Only for StoreProcedure, commandText is storeprocedure name
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parametersName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IEnumerable<dynamic> Query(string commandText, string[] parametersName, params object[] parameters)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }
            // Return a readonly collection
            return QueryInternal(SafeSqlLiteral(commandText), parametersName, parameters).ToList().AsReadOnly();
        }

        /// <summary>
        /// Sử dụng khi cần lấy tham số out
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int Execute(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = commandType;
                command.CommandText = SafeSqlLiteral(commandText);

                AddParameters(command, parameters);
                using (command)
                {
                    return command.ExecuteNonQuery();
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        /// <summary>
        /// Using Only for StoreProcedure, commandText is storeprocedure name
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parametersName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public int Execute(string commandText, string[] parametersName, params object[] args)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = SafeSqlLiteral(commandText);

                AddParameters(command, parametersName, args);
                using (command)
                {
                    return command.ExecuteNonQuery();
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        /// <summary>
        /// Using Only for StoreProcedure, commandText is storeprocedure name
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parametersName"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public dynamic QueryValue(string commandText, string[] parametersName, params object[] args)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = SafeSqlLiteral(commandText);

                AddParameters(command, parametersName, args);
                using (command)
                {
                    return command.ExecuteScalar();
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        /// <summary>
        /// Sử dụng khi cần lấy kèm tham số out
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public dynamic QueryValue(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            if (String.IsNullOrEmpty(commandText))
            {
                throw new Exception();
            }

            var sqlConnection = OpenSqlConnection();
            try
            {
                DbCommand command = sqlConnection.CreateCommand();
                command.CommandType = commandType;
                command.CommandText = SafeSqlLiteral(commandText);

                AddParameters(command, parameters);
                using (command)
                {
                    return command.ExecuteScalar();
                }
            }
            finally
            {
                CloseSqlConnection(ref sqlConnection);
            }

        }

        #endregion

        #region Disponse
        public void Disponse()
        {
            try
            {
                GC.SuppressFinalize(this);
            }
            catch (Exception)
            {
            }
        }

        ~DBHelper()
        {
            Disponse();
        }
        #endregion
    }
}