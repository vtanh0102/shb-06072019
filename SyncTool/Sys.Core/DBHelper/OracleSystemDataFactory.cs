﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.DBHelper
{
    public class OracleSystemDataFactory : DbProviderFactory
    {
        public OracleSystemDataFactory(string sConnectionString)
            : base(sConnectionString
                  , "System.Data.OracleClient"
                  , "System.Data.OracleClient.OracleConnection"
                  , "System.Data.OracleClient.OracleCommand"
                  , "System.Data.OracleClient.OracleDataAdapter"
                  , "System.Data.OracleClient.OracleParameter"
                  , "System.Data.OracleClient.OracleCommandBuilder"
                  )
        {
        }
    }
}
