﻿using System;
using System.Data;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using Oracle.ManagedDataAccess.Client;
using System.IO;
using Sys.Core.Models;
using System.Collections.Generic;

namespace Sys.Core.DBHelper
{
    public class DBHelper_New
    {
        #region private members
        private string _connectionstring = "";
        private DbConnection _connection;
        private DbCommand _command;
        private DbProviderFactory _factory = null;
        private DbProviders _provider;
        #endregion
        #region properties
        public string connectionstring
        {
            get
            {
                return _connectionstring;
            }
            set
            {
                if (value != "")
                {
                    _connectionstring = value;
                }
            }
        }
        public DbConnection connection
        {
            get
            {
                return _connection;
            }
        }
        public DbCommand command
        {
            get
            {
                return _command;
            }
        }
        #endregion
        #region methods

        public bool IsServerConnected()
        {
            try
            {
                OpenConnection();
                return true;
            }
            catch (DbException ex)
            {
                return false;
            }
        }

        public DBHelper_New(string conStr, DbProviders providerList)
        {
            _provider = providerList;
            switch (providerList)
            {
                case DbProviders.SqlServer:
                    _factory = SqlClientFactory.Instance;
                    break;
                case DbProviders.Oracle:
                    _factory = OracleClientFactory.Instance;
                    break;
                case DbProviders.OleDb:
                    _factory = OleDbFactory.Instance;
                    break;
                case DbProviders.ODBC:
                    _factory = OdbcFactory.Instance;
                    break;
            }
            _command = _factory.CreateCommand();
            connectionstring = conStr;

        }
        private void OpenConnection()
        {
            try
            {
                _connection = _factory.CreateConnection();
                _connection.ConnectionString = connectionstring;
                _connection.Open();
                _command.Connection = connection;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        #region parameters
        public int AddParameter(string name, object value)
        {
            DbParameter parm = _factory.CreateParameter();
            parm.ParameterName = name;
            parm.Value = value;
            return command.Parameters.Add(parm);
        }
        public int AddParameter(DbParameter parameter)
        {
            return command.Parameters.Add(parameter);
        }
        public void ClearParameter()
        {
            if (_command != null)
            {
                if (_command.Parameters.Count > 0)
                {
                    _command.Parameters.Clear();
                }
            }
        }
        #endregion
        #region transactions
        private void BeginTransaction()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                OpenConnection();
            }
            command.Transaction = connection.BeginTransaction();
        }
        private void CommitTransaction()
        {
            command.Transaction.Commit();
            connection.Close();
        }
        private void RollbackTransaction()
        {
            command.Transaction.Rollback();
            connection.Close();
        }
        #endregion
        #region execute database functions
        public int ExecuteNonQuery(string query, CommandType commandtype)
        {
            OpenConnection();
            command.CommandText = query;
            command.CommandType = commandtype;
            int i = -1;
            try
            {               
                BeginTransaction();
                i = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw (ex);
            }
            finally
            {
                CommitTransaction();
                command.Parameters.Clear();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                    command.Dispose();
                }
            }
            return i;
        }
        public object ExecuteScaler(string query, CommandType commandtype)
        {
            OpenConnection();
            command.CommandText = query;
            command.CommandType = commandtype;
            object obj = null;
            try
            {                
                BeginTransaction();
                obj = command.ExecuteScalar();
                CommitTransaction();
            }
            catch (Exception ex)
            {
                RollbackTransaction();
                throw (ex);
            }
            finally
            {
                command.Parameters.Clear();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                    command.Dispose();
                }
            }
            return obj;
        }
        public DbDataReader ExecuteReader(string query, CommandType commandtype, ConnectionState connectionstate)
        {
            OpenConnection();
            command.CommandText = query;
            command.CommandType = commandtype;
            DbDataReader reader = null;
            try
            {               
                if (connectionstate == System.Data.ConnectionState.Open)
                {
                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                }
                else
                {
                    reader = command.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                command.Parameters.Clear();
            }
            return reader;
        }
        public DataSet GetDataSet(string query, CommandType commandtype)
        {
            OpenConnection();
            DbDataAdapter adapter = _factory.CreateDataAdapter();
            command.CommandText = query;
            command.CommandType = commandtype;
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            try
            {               
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Parameters.Clear();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                    command.Dispose();
                }
            }
            return ds;
        }

        public List<DynamicTable> GetDataDynamic(string strQuery, CommandType commandtype)
        {
            List<DynamicTable> products = new List<DynamicTable>();
            try
            {
                OpenConnection();
                DbDataAdapter adapter = _factory.CreateDataAdapter();
                command.CommandText = strQuery;
                command.CommandType = commandtype;
                adapter.SelectCommand = command;
                
                DataTable t = new DataTable();
                adapter.Fill(t);

                DynamicTable p = null;

                foreach (DataRow row in t.Rows)
                {
                    p = new DynamicTable();
                    foreach (DataColumn col in t.Columns)
                    {
                        string property = col.ColumnName.ToString();
                        string propertyValue = row[col.ColumnName].ToString();

                        Dictionary<string, string> dictionary = new Dictionary<string, string>();

                        dictionary.Add(property, propertyValue);

                        p.DynamicProperties.Add(dictionary);
                    }

                    products.Add(p);
                }
            }
            catch (Exception ex)
            {
                CommonProvider.LogService.Error(GetType(), string.Format("GetDataReader():Error={0}", ex.Message), ex);
                throw;
            }
            finally
            {
                command.Parameters.Clear();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                    command.Dispose();
                }
            }
            return products;

        }

        public DataTable getDataTable(string query, CommandType commandtype)
        {
            var dataSet = GetDataSet(query, commandtype);
            return dataSet.Tables.Count > 0 ? dataSet.Tables[0] : null;
        }

        #endregion
        #endregion
    }
}