﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.DBHelper
{
    public class DbProviderFactories
    {
        public static DbProviderFactory GetFactory(string sSplendidProvider, string sConnectionString)
        {
            switch (sSplendidProvider)
            {
                case "System.Data.SqlClient":
                    {
                        return new SqlClientFactory(sConnectionString);
                    }
                case "System.Data.OracleClient":
                    {
                        return new OracleSystemDataFactory(sConnectionString);
                    }
                case "Oracle.DataAccess.Client":
                    {
                        return new OracleClientFactory(sConnectionString);
                    }               
                default:
                    throw (new Exception("Unsupported factory " + sSplendidProvider));
            }
        }
    }
}
