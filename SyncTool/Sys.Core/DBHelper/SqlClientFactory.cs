﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.DBHelper
{
    public class SqlClientFactory : DbProviderFactory
    {
        public SqlClientFactory(string sConnectionString)
            : base(sConnectionString
                  , "System.Data"
                  , "System.Data.SqlClient.SqlConnection"
                  , "System.Data.SqlClient.SqlCommand"
                  , "System.Data.SqlClient.SqlDataAdapter"
                  , "System.Data.SqlClient.SqlParameter"
                  , "System.Data.SqlClient.SqlCommandBuilder"
                  )
        {
        }
    }
}
