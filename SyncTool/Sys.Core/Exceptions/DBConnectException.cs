﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Exceptions
{
    public class DBConnectException : Exception
    {
        public DBConnectException()
        {
        }

        public DBConnectException(string message)
        : base(message)
        {
        }

        public DBConnectException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
