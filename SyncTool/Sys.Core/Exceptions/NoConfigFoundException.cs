﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Exceptions
{
    public class NoResultFoundException : Exception
    {
        public NoResultFoundException()
        {
        }

        public NoResultFoundException(string message)
        : base(message)
        {
        }

        public NoResultFoundException(string message, Exception inner)
        : base(message, inner)
        {
        }

    }
}
