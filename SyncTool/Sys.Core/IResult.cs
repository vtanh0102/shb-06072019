﻿namespace Sys.Core
{
    public interface IResult
    {
        //[JsonProperty(PropertyName = "_code")]
        //ResultCode Code { get; set; }

        //[JsonProperty(PropertyName = "_message")]
        //string Message { get; set; }
    }

    public interface IResult<T> : IResult
    {
        //T Data { get; set; }
    }

    public interface IResult<T, TH> : IResult<T>
    {
        //TH ExtensiveInformation { get; set; }
    }
}