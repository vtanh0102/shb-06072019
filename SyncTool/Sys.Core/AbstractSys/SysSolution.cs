﻿using Sys.Core.Enums;
using Sys.Core.Models;
using Sys.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Config;
using Sys.Core.DBHelper;
using Sys.Core.Repository;
using Sys.Core.Exceptions;
using Sys.Core.IServices;
using Sys.Core.Services;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Sys.Core.Cache;
using Sys.Core.Models.Source;
using Sys.Core.Repository.Source;
using Sys.Core.Utility;
using StackExchange.Redis.Extensions.Core;
using System.Data.SqlClient;

namespace Sys.Core.AbstractSys
{
    public abstract class SysSolution
    {

        #region Properties

        /// <summary>
        /// Connection đến DB chứa thông tin Config
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Thông tin User Admin đế lưu log
        /// </summary>
        public string UserAdminId { get; set; }

        /// <summary>
        /// Thông tin kết nối Redis
        /// </summary>
        public string RedisServer { get; set; }

        /// <summary>
        /// Kịch bản connection lưu trong DB
        /// </summary>
        public string ConnectionCode { get; set; }

        public string CrmConnectionCode { get; set; }

        /// <summary>
        /// Loại hệ thống: HUMAN_RESOURCE | KPI
        /// </summary>
        public TypeSys TypeSys { get; set; }

        /// <summary>
        /// Thông tin kết nối Database của SHB và Temp
        /// </summary>
        public DBConnection DBConnection { get; set; }

        /// <summary>
        /// Thông tin kết nối Database của Temp và CRM
        /// </summary>
        public DBConnection CRMDBConnection { get; set; }

        /// <summary>
        /// Thông tin Connection đến Source
        /// </summary>
        public List<TableMapping> ListTableMapping { get; set; }

        public SyncHis SyncHis { get; set; }
        public Dictionary<string, string> SystemConfig { get; set; }

        private DBConnectionRepository dbConnectionRepository;
        private TableMappingRepository tableMappingRepository;
        private ConfigRepository configRepository;

        protected MessageLog messageLogService;
        protected NotificationService notificationService;
        private QueryBuilderService queryBuilderService;

        private RedisService redisService;

        #endregion

        #region Constructors

        public SysSolution(string connectionString, string crmConnectionCode, string redisServer)
        {
            this.ConnectionString = connectionString;
            this.CrmConnectionCode = crmConnectionCode;
            this.RedisServer = redisServer;
            dbConnectionRepository = new DBConnectionRepository(connectionString);
            tableMappingRepository = new TableMappingRepository(connectionString);
            configRepository = new ConfigRepository(connectionString);
            messageLogService = new MessageLog(connectionString);
            queryBuilderService = new QueryBuilderService();
            try
            {
                redisService = new RedisService(redisServer);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Chưa cài Redis Server");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Thực hiện đồng bộ
        /// </summary>
        public abstract void ExecuteSys(string userId);

        /// <summary>
        /// Chuẩn bị trước khi lấy dữ liệu về
        /// </summary>
        /// <returns></returns>
        protected Result<string> PrepareData()
        {
            var result = new Result<string>();
            try
            {
                var dbHelperConfig = new DBHelper_New(this.ConnectionString, DbProviders.SqlServer);
                // System Config
                this.SystemConfig = configRepository.GetAll();
                // Get DB Mapping from Database
                this.DBConnection = dbConnectionRepository.GetDBConnectionInfo(this.ConnectionCode);
                var crmConnection = dbConnectionRepository.GetDBConnectionInfo(this.CrmConnectionCode);
                // Get list TableMapping
                this.ListTableMapping = tableMappingRepository.GetTableMapping(this.ConnectionCode);
                notificationService = new NotificationService(this.ConnectionString, crmConnection.TargetConnectionString);
            }
            catch (NoResultFoundException)
            {
                result.Code = ResultCode.DBConfigNotFound;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
            }
            return result;
        }


        /// <summary>
        /// Bắt đầu đồng bộ 1 bảng với thông tin từ TableMapping
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <returns></returns>
        protected Result<string> SyncTable(TableMapping tableMapping)
        {
            #region GetData
            // Lấy data
            var result = GetSourceData(tableMapping);
            if (result.Code != ResultCode.Success)
            {
                messageLogService.AddSyncError(result, tableMapping.TargetTable);
                return result;
            }
            var sourceTable = result.ExtensiveInformation;
            #endregion

            // Lấy dữ liệu target từ cache hoặc DB
            bool enableCache = Convert.ToBoolean(SystemConfig["EnableCache"]);
            var resultTargetTable = GetTargetData(tableMapping, enableCache);
            if (resultTargetTable.Code != ResultCode.Success)
            {
                messageLogService.AddSyncError(resultTargetTable, tableMapping.TargetTable);
                return result;
            }
            var targetTable = resultTargetTable.ExtensiveInformation;

            // So sánh từng bản ghi lấy được
            DataRowCollection listData = sourceTable.Rows;
            messageLogService.AddTotalRecord(listData.Count);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            int index = 1;
            foreach (DataRow data in listData)
            {
                Console.WriteLine("(" + index + "/" + listData.Count + ")");
                index++;

                bool isError = false;
                #region Replace Data
                var resultReplace = ReplaceData(data, tableMapping);
                if (resultReplace.Code != ResultCode.Success)
                {
                    messageLogService.AddSyncError(resultReplace, tableMapping.TargetTable, Convert.ToInt32(data[tableMapping.TargetTableKey[0]]));
                    isError = true;
                }
                #endregion

                #region Compare Data
                var resultCompareData = CompareData(data, tableMapping, targetTable);
                if (resultCompareData.Code != ResultCode.Success)
                {
                    messageLogService.AddSyncError(resultCompareData, tableMapping.TargetTable, Convert.ToInt32(data[tableMapping.SourceTableKey[0]]));
                    isError = true;
                    if (resultCompareData.Code == ResultCode.ConnectToDBNotSuccess)
                        return result;
                }
                #endregion

                #region Insert Data
                var resultInsertData = InsertData(data, tableMapping, resultCompareData.Data);
                if (resultInsertData.Code != ResultCode.Success)
                {
                    if (TypeSys == TypeSys.HUMAN_RESOURCE)
                        messageLogService.AddSyncError(resultInsertData, tableMapping.TargetTable, Convert.ToInt32(data["ID"]));
                    else
                        messageLogService.AddSyncError(resultInsertData, tableMapping.TargetTable);
                    isError = true;
                    if (resultInsertData.Code == ResultCode.ConnectToDBNotSuccess)
                        return result;
                }
                #endregion

                if (!isError)
                    messageLogService.IncreaseSyncRecord();
            }

            // Refresh Cache
            if (enableCache)
            {
                var cacheTable = RenameColumn(sourceTable, tableMapping);
                redisService.Set<DataTable>(tableMapping.TargetTable, cacheTable);
            }

            sw.Stop();
            Console.WriteLine("Rows:         {0}", listData.Count);
            Console.WriteLine("Seconds:      {0}", sw.Elapsed.TotalSeconds);
            Console.WriteLine("Speed(r/s):   {0}", listData.Count / sw.Elapsed.TotalSeconds);

            return result;
        }

        /// <summary>
        /// Lấy dữ liệu từ SHB
        /// </summary>
        /// <returns></returns>
        private Result<string, DataTable> GetSourceData(TableMapping tableMapping)
        {
            var result = new Result<string, DataTable>();
            result.Code = ResultCode.Success;
            try
            {
                var sourceDBHelper = new DBHelper_New(this.DBConnection.SourceConnectionString, this.DBConnection.SourceDBType);

                // Build get data query
                string query = queryBuilderService.BuildGetDataSourceQuery(tableMapping);
                result.ExtensiveInformation = sourceDBHelper.getDataTable(query, CommandType.Text);
                if (result.ExtensiveInformation == null || result.ExtensiveInformation.Rows.Count == 0)
                    result.Code = ResultCode.DataIsNull;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// Lấy dữ liệu từ bảng TMP
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <param name="enableCache"></param>
        /// <returns></returns>
        private Result<string, DataTable> GetTargetData(TableMapping tableMapping, bool enableCache)
        {
            var result = new Result<string, DataTable>();
            result.Code = ResultCode.Success;

            // 6. Lấy dữ liệu cũ từ cache
            DataTable targetTable = null;

            if (enableCache)
                targetTable = redisService.Get<DataTable>(tableMapping.TargetTable);

            if (targetTable != null)
            {
                result.ExtensiveInformation = targetTable;
                return result;
            }

            try
            {
                string query = queryBuilderService.BuildGetDataTargetQuery(tableMapping);
                DBHelper.DBHelper targetSQLServer = new DBHelper.DBHelper(DBConnection.TargetConnectionString);
                result.ExtensiveInformation = targetSQLServer.getDataTable(query);
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                return result;
            }            
        }

        /// <summary>
        /// Replace dữ liệu cũ sang dữ liệu mới
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tableMapping"></param>
        /// <returns></returns>
        private Result<string> ReplaceData(DataRow data, TableMapping tableMapping)
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;
            try
            {
                foreach (var replaceInfo in tableMapping.ListColumnReplace)
                {
                    if (data.IsNull(replaceInfo.MapColumn))
                        break;
                    // Regex replace, chỉ replace 1 phần data
                    if (replaceInfo.IsRegex)
                    {
                        string replaceValue = Regex.Replace(data[replaceInfo.MapColumn].ToString(), replaceInfo.Match, replaceInfo.Replace);
                        data[replaceInfo.MapColumn] = replaceValue;
                    }
                    // Replace toàn bộ dữ liệu
                    else
                    {
                        if (data[replaceInfo.MapColumn].ToString().Equals(replaceInfo.Match))
                            data[replaceInfo.MapColumn] = replaceInfo.Replace;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.Fail;
                result.Message = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// So sánh dữ liệu hiện tại và dữ liệu lấy về
        /// </summary>
        /// <param name="source">Data mới cần compare</param>
        /// <param name="tableMapping">Dữ liệu mapping table</param>
        /// <param name="targetTable">Dữ liệu source lấy từ cache</param>
        private Result<StatusCompareData> CompareData(DataRow source, TableMapping tableMapping, DataTable targetTable)
        {
            var result = new Result<StatusCompareData>();
            try
            {
                string QUERY_EXIST = queryBuilderService.BuildCompareDataQuery(source, tableMapping);
                var existTable = targetTable.Select(QUERY_EXIST);

                // Compare
                result.Code = ResultCode.Success;
                if (existTable != null && existTable.Count() > 0)
                {
                    var target = existTable[0];
                    bool isSame = Common.CompareDataRow(tableMapping, source, target);

                    if (!isSame)
                    {
                        result.Data = StatusCompareData.UPDATE;
                        return result;
                    }
                    result.Data = StatusCompareData.NO_CHANGE;
                    return result;
                }

                result.Data = StatusCompareData.INSERT;
                return result;
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
                result.Data = StatusCompareData.ERROR;
            }
            return result;
        }

        /// <summary>
        /// Insert, Update dữ liệu vào bảng đích và ghi log các thông tin
        /// </summary>
        /// <param name="source"></param>
        /// <param name="mapToTarget"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private Result<string> InsertData(DataRow source, TableMapping tableMapping, StatusCompareData type)
        {
            
            var result = new Result<string>();
            result.Code = ResultCode.Success;
            if (type == StatusCompareData.NO_CHANGE)
                return result;

            try
            {
                DBHelper.DBHelper targetSQLServer = new DBHelper.DBHelper(DBConnection.TargetConnectionString);

                var listSourceColumn = tableMapping.MapColSource;
                var listTargetColumn = tableMapping.MapColTarget;
                var listSourceKey = tableMapping.SourceTableKey;
                var listTargetKey = tableMapping.TargetTableKey;

                if (type == StatusCompareData.INSERT)
                {
                    string query = queryBuilderService.BuildInsertTmpQuery(source, tableMapping);
                    targetSQLServer.ExecuteNonQuery(query, CommandType.Text, null);
                }
                else if (type == StatusCompareData.UPDATE)
                {
                    string query = queryBuilderService.BuildUpdateTmpQuery(source, tableMapping);
                    targetSQLServer.ExecuteNonQuery(query, CommandType.Text, null);
                }

                // 5. Kiểm tra tham số ghi log
                bool enableLog = Convert.ToBoolean(SystemConfig["EnableLog"]);
                string[] logTables = {"M_USERS_TMP", "B_KPI_RESULT_EMP_TMP"};
                if (enableLog == true && logTables.Contains(tableMapping.TargetTable))
                {
                    string logQuery = queryBuilderService.BuildLogQuery(source, tableMapping, type);
                    targetSQLServer.ExecuteNonQuery(logQuery, CommandType.Text, null);
                }
            }
            catch (SqlException sqlex)
            {
                switch (sqlex.Number)
                {
                    case -2:
                    case -1:
                    case 2:
                    case 53:
                        result.Code = ResultCode.ConnectToDBNotSuccess;
                        result.Message = sqlex.ToString();
                        break;
                    default:
                        result.Code = ResultCode.Fail;
                        result.Message = sqlex.ToString();
                        break;
                }
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.Fail;
                result.Message = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// Đổi tên columns DataTable lấy được thành DataTable đích
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="tableMapping"></param>
        /// <returns></returns>
        private DataTable RenameColumn(DataTable dataTable, TableMapping tableMapping)
        {
            IList<string> listSourceColumn = new List<string>(tableMapping.MapColSource.Concat(tableMapping.SourceTableKey));
            IList<string> listTargetColumn = new List<string>(tableMapping.MapColTarget.Concat(tableMapping.TargetTableKey));
            for (int i = 0; i < listSourceColumn.Count(); i++)
            {
                dataTable.Columns[listSourceColumn[i]].ColumnName = listTargetColumn[i];
            }
            return dataTable;
        }

        #endregion

    }
}
