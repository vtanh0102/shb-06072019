﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Cache
{
    public interface IDataCaching
    {
        object GetCache(string key);
        object GetHashCache(string hashKey, object param);
        void InsertCache(string key, object data, double expireTime);
        void InsertCacheNoExpireTime(string key, object data);
        bool RemoveCache(string key);
        void SetHashCache(string hashKey, object param, double expireTime, object data);
    }
}
