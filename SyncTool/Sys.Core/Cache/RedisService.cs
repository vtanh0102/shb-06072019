﻿using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;

namespace Sys.Core.Cache
{
    public class RedisService : ICacheService
    {

        StackExchangeRedisCacheClient _cacheClient;

        public RedisService(string connectionString)
        {
            var serializer = new NewtonsoftSerializer();
            _cacheClient = new StackExchangeRedisCacheClient(serializer, connectionString);
        }

        public void Set<T>(string key, T value)
        {
            _cacheClient.Add<T>(key, value);
        }

        public void Set<T>(string key, T value, TimeSpan timeout)
        {
            _cacheClient.Add<T>(key, value, timeout);
        }

        public T Get<T>(string key)
        {
            return _cacheClient.Get<T>(key);
        }

        public bool Remove(string key)
        {
            return _cacheClient.Remove(key);
        }

        public bool IsInCache(string key)
        {
            throw new NotImplementedException();
        }
    }
}