using System;
using System.Collections.Generic;

namespace Sys.Core.Cache
{
    public interface ICacheService
    {
        void Set<T>(string key, T value);

        void Set<T>(string key, T value, TimeSpan timeout);

        T Get<T>(string key);

        bool Remove(string key);

        bool IsInCache(string key);       
    }
}