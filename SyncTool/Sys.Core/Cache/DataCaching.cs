﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Sys.Core.Cache
{
    public class DataCaching : IDataCaching
    {
        public object GetCache(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }


        public object GetHashCache(string hashKey, object param)
        {
            Hashtable hashtable1 = (Hashtable)GetCache(hashKey);
            if (hashtable1 == null)
            {
                return null;
            }
            if (hashtable1[param] == null)
            {
                return null;
            }
            return hashtable1[param];
        }


        public void InsertCache(string key, object data, double expireTime)
        {
            if (expireTime == 0)
            {
                expireTime = 3;
            }

            RemoveCache(key);
            HttpRuntime.Cache.Insert(key, data, null, DateTime.Now.AddHours(expireTime), System.Web.Caching.Cache.NoSlidingExpiration);
        }


        public void InsertCacheNoExpireTime(string key, object data)
        {
            if (data != null)
            {
                HttpRuntime.Cache.Insert(key, data);
            }
        }

        public bool RemoveCache(string key)
        {
            if (string.IsNullOrEmpty(key)) return true;

            if (HttpContext.Current.Cache[key] != null)
            {
                HttpRuntime.Cache.Remove(key);
                return true;
            }
            return false;
        }


        public void SetHashCache(string hashKey, object param, double expireTime, object data)
        {
            Hashtable hashtable1 = (Hashtable)GetCache(hashKey);
            if (hashtable1 == null)
            {
                hashtable1 = new Hashtable();
                hashtable1.Add(param, data);
                InsertCache(hashKey, hashtable1, expireTime);
            }
            else if ((hashtable1[param] == null) && !hashtable1.ContainsKey(param))
            {
                hashtable1.Add(param, data);
            }
        }
    }
}
