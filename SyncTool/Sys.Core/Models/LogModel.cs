﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class LogModel
    {
        public string Value01 { get; set; }
        public string Value02 { get; set; }
        public string Value03 { get; set; }
        public string Value04 { get; set; }
        public string Value05 { get; set; }

        private const string DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

        public LogModel(DateTime? dateStart = null, string action = "", string actionDetails = "", string message = "", string stackTrace = "")
        {
            this.Value01 = (dateStart == null) ? DateTime.Now.ToString(DATETIME_FORMAT) : dateStart.Value.ToString(DATETIME_FORMAT);
            this.Value02 = action;
            this.Value03 = actionDetails;
            this.Value04 = message;
            this.Value05 = stackTrace;
        }

        public LogModel(string tableName, string ID, string code, string message, string stackTrace)
        {
            this.Value01 = tableName;
            this.Value02 = ID;
            this.Value03 = code;
            this.Value04 = message;
            this.Value05 = stackTrace;
        }
    }
}
