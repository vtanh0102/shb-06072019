﻿using Sys.Core.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class SysSource
    {
        /// <summary>
        /// connection Nguon
        /// </summary>
        public string conStrSource { get; set; }
        /// <summary>
        /// bảng Nguôn
        /// </summary>
        public string tableNameSource { get; set; }
        /// <summary>
        /// key Bảng Nguồn
        /// </summary>
        public string keyColNameSource { get; set; }
        /// <summary>
        /// các cot bảng Nguồn
        /// </summary>
        public List<string> listColNameSource { get; set; }
    }
}
