﻿namespace Sys.Core.Models
{
    public class NotifyConfig
    {
        public int ID { get; set; }
        public string TableName { get; set; }
        public string Module { get; set; }
        public string Field { get; set; }
        public SyncNotifyCondition Condition { get; set; }
        public string TypeNotify { get; set; }
        public string MessageTemplate { get; set; }
    }
}