﻿namespace Sys.Core.Models
{
    public enum SyncNotifyCondition
    {
        ISNULL = 0,
        ISCHANGED = 1,
        ISNEW = 2,
        ISCHANGEWITHCONDITION = 3,
    }
}
