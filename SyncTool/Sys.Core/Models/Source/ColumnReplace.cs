﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class ColumnReplace
    {
        public string ID { get; set; }

        public string TableMappingID { get; set; }

        public string MapColumn { get; set; }

        public string Match { get; set; }

        public string Replace { get; set; }

        public bool IsRegex { get; set; }
    }
}
