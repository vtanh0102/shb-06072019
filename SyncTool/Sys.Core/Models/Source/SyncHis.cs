﻿using Sys.Core.Enums;
using System;
using System.ComponentModel;
using Sys.Core.Utility;

namespace Sys.Core.Models.Source
{
    [Description("SYNC_HIS")]
    public class SyncHis : BaseEntity
    {
        [Description("ID")]
        public int? ID { get; set; }

        [Description("USER_ID_FK")]
        public string UserID { get; set; }

        [Description("DATE_START_TMP")]
        public DateTime DateStartTmp { get; set; }

        [Description("DATE_END_TMP")]
        public DateTime? DateEndTmp { get; set; }

        [Description("COUNT_TMP")]
        public string CountTmp { get; set; }

        [Description("STATUS_TMP")]
        public int? statusTmp { get; set; }
        public SyncHisStatus? StatusTmp
        {
            get { return (SyncHisStatus?)statusTmp; }
            set { statusTmp = (int?)value; }
        }

        [Description("REASON_TMP")]
        public string ReasonTmp
        {
            get { return StatusTmp.GetDescription(); }
        }

        [Description("DATE_START_CRM")]
        public DateTime? DateStartCrm { get; set; }

        [Description("DATE_END_CRM")]
        public DateTime? DateEndCrm { get; set; }

        [Description("COUNT_CRM")]
        public string CountCrm { get; set; }

        [Description("STATUS_CRM")]
        public int? statusCrm { get; set; }
        public SyncHisStatus? StatusCrm
        {
            get { return (SyncHisStatus?)statusCrm; }
            set { statusCrm = (int?)value; }
        }

        [Description("REASON_CRM")]
        public string ReasonCrm
        {
            get { return StatusCrm.GetDescription(); }
        }

        [Description("TYPE")]
        public int? type { get; set; }
        public TypeSys? Type
        {
            get { return (TypeSys?)type; }
            set { type = (int?)value; }
        }

        [Description("IP_SOURCE")]
        public string IPSource { get; set; }

        [Description("BROWSER")]
        public string Browser { get; set; }

        public bool IsToTemp { get; set; }
        public int TotalRecords { get; set; }
        public int SyncRecords { get; set; }

    }
}
