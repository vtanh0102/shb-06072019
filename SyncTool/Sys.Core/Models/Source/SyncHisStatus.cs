﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models.Source
{
    public enum SyncHisStatus
    {
        [Description("Thành công")]
        SUCCESS = 0,

        [Description("Mất kết nối")]
        LOST_CONNECTION = 1,

        [Description("Lỗi dữ liệu")]
        DATA_ERROR = 2,

        [Description("Mất kết nối và có lỗi dữ liệu")]
        LOST_CONNECTION_AND_ERROR = 3,
    }
}
