﻿using Sys.Core.Enums;
using System.ComponentModel;

namespace Sys.Core.Models.Source
{
    [Description("SYNC_ERRORS")]
    public class SyncError : BaseEntity
    {
        [Description("ID")]
        public int? ID { get; set; }

        [Description("SYNC_HIS_ID")]
        public int SyncHisID { get; set; }

        [Description("SYNC_FROM")]
        public string SyncFrom { get; set; }

        [Description("ERROR_CODE")]
        public int errorCode { get; set; }
        public ResultCode ErrorCode
        {
            get { return (ResultCode)errorCode; }
            set { errorCode = (int)value; }
        }

        [Description("STACK_TRACE")]
        public string StackTrace { get; set; }

        [Description("ERROR_MESSAGE")]
        public string ErrorMessage { get; set; }

        [Description("TABLE_NAME")]
        public string TableName { get; set; }

        [Description("SOURCE_ID")]
        public int? SourceID { get; set; }
    }
}
