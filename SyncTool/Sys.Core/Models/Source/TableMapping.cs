﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class TableMapping
    {

        public TableMapping()
        {
            ListQueryMapping = new List<QueryMapping>();
            ListColumnReplace = new List<ColumnReplace>();
        }

        public string ID { get; set; }

        public string DBMappingCode { get; set; }

        public string SourceTable { get; set; }

        public List<string> SourceTableKey { get; set; }

        public List<string> SourceTableColumns { get; set; }

        public string TargetTable { get; set; }

        public List<string> TargetTableKey { get; set; }

        public List<string> MapColSource { get; set; }

        public List<string> MapColTarget { get; set; }

        public string FilterJoin { get; set; }

        public string FilterQuery { get; set; }

        public List<QueryMapping> ListQueryMapping { get; set; }

        public List<ColumnReplace> ListColumnReplace { get; set; }

    }
}
