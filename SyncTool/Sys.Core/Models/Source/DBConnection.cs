﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.DBHelper;

namespace Sys.Core.Models
{
    public class DBConnection
    {
        public string SourceConnectionString { get; set; }

        public string SourcePassword { get; set; }

        public DbProviders SourceDBType { get; set; }

        public string TargetConnectionString { get; set; }

        public string TargetPassword { get; set; }

        public DbProviders TargetDBType { get; set; }
    }
}
