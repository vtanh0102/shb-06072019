﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class QueryMapping
    {
        public string ID { get; set; }

        public string TableMappingID { get; set; }

        public string Description { get; set; }

        public string SelectQuery { get; set; }

        public string JoinQuery { get; set; }

        public string FilterQuery { get; set; }
    }
}
