﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    public class ListSysMapToTarget
    {
        /// <summary>
        /// connection str dich
        /// </summary>
        public string conStrTarget { get; set; }

        public List<SysMapToTarget> listSysMapToTarget { get; set; }
        
    }

    public class SysMapToTarget
    {
        /// <summary>
        /// ten bang dong bo
        /// </summary>
        public string tableNameTarget { get; set; }
        /// <summary>
        /// map key nguon-key dich
        /// </summary>
        public List<string> mapKeySourceToTarget { get; set; }
        /// <summary>
        /// map cols nguon - cols dich
        /// </summary>
        public List<string> mapColSourceToTarget { get; set; }
    }
}
