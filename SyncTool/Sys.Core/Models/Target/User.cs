﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("USERS")]
    public class User : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("USER_NAME")]
        public string UserName { get; set; }

        [Description("USER_HASH")]
        public string UserHash { get; set; }

        [Description("LAST_NAME")]
        public string LastName { get; set; }

        [Description("FIRST_NAME")]
        public string FirstName { get; set; }

        [Description("DELETED")]
        public bool Deleted { get; set; }

        [Description("DATE_ENTERED")]
        public DateTime? DateEntered { get; set; }

        [Description("DATE_MODIFIED")]
        public DateTime? DateModified { get; set; }

        [Description("PHONE_HOME")]
        public string PhoneHome { get; set; }

        [Description("PHONE_MOBILE")]
        public string PhoneMobile { get; set; }

        [Description("PHONE_WORK")]
        public string PhoneWork { get; set; }

        [Description("PHONE_FAX")]
        public string PhoneFax { get; set; }

        [Description("EMAIL1")]
        public string Email1 { get; set; }

        [Description("STATUS")]
        public string Status { get; set; }

        //[Description("APPROVE_STATUS")]
        public int ApproveStatus { get; set; }

        [Description("FACEBOOK_ID")]
        public string FacebookID { get; set; }

        [Description("PRIMARY_ROLE_ID")]
        public string PrimaryRoleID { get; set; }
    }
}
