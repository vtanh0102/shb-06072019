﻿using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempToCRM.Entities
{
    [Description("USERS_CSTM")]
    public class UserExtend : BaseEntity
    {
        [Description("ID_C")]
        public string ID { get; set; }

        [Description("ID_SOURCE_C")]
        public int IDSource { get; set; }

        [Description("USER_CODE_C")]
        public string UserCode { get; set; }

        [Description("BIRTH_DATE_C")]
        public DateTime? BirthDate { get; set; }

        [Description("SEX_C")]
        public string Sex { get; set; }

        [Description("RETIRED_DATE_C")]
        public DateTime? RetiredDate { get; set; }

        [Description("CONTRACT_TYPE_C")]
        public int? ContractTypeIDSource { get; set; }

        [Description("MANAGER_NAME_C")]
        public string ManagerName { get; set; }

        [Description("DATE_START_WORK_C")]
        public DateTime? DateStartWork { get; set; }

        [Description("DATE_START_INTERN_C")]
        public DateTime? DateStartIntern { get; set; }

        [Description("DATE_START_PROBATION_C")]
        public DateTime? DateStartProbation { get; set; }

        [Description("DATE_ACTING_PROMOTED_C")]
        public DateTime? DateActingnPromoted { get; set; }

        [Description("OFFICAL_PROMOTED_DATE_C")]
        public DateTime? OfficalPromotedDate { get; set; }

        [Description("CURRENT_POSITION_WORK_TIME_C")]
        public DateTime? CurrentPositionWorkTime { get; set; }

        [Description("SENIORITY_C")]
        public string Seniority { get; set; }

        [Description("SALARY_STATUS_ID_C")]
        public string SalaryStatusID { get; set; }

        [Description("POS_NAME_CENTER_C")]
        public string PosNameCenter { get; set; }

        [Description("POS_CODE_C")]
        public string PosCode { get; set; }

        [Description("AREA_C")]
        public string AreaID { get; set; }

        [Description("PREVIOUS_EMPLOY_HISTORY_C")]
        public string PreviousEmployHistory { get; set; }

        [Description("EMPLOYMENT_HISTORY_C")]
        public string EmploymentHistory { get; set; }

        [Description("M_BUSINESS_CODE_C")]
        public string BussinessCode { get; set; }

        [Description("IS_SYNC_C")]
        public bool IsSync { get { return true; } }

        [Description("ORGANIZATION_ID_C")]
        public string OrganizationID { get; set; }

        [Description("ORGANIZATION_CODE_C")]
        public string OrganizationCode { get; set; }

        [Description("ORGANIZATION_PARENT_ID_C")]
        public string OrganizationParentID { get; set; }

        [Description("TITLE_ID_C")]
        public string TitleID { get; set; }

        [Description("TITLE_ADD_ID_C")]
        public string TitleAddID { get; set; }

        [Description("POSITION_ID_C")]
        public string PositionID { get; set; }

        [Description("POSITION_CODE_C")]
        public string PositionCode { get; set; }

        [Description("DATE_SYNC_C")]
        public DateTime DateSync { get { return DateTime.Now; } }

    }
}
