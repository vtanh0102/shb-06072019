﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Models
{
    [Description("B_NOTIFICATION")]
    public class Notification : BaseEntity
    {
        [Description("ID")]
        public string ID { get; set; }

        [Description("NAME")]
        public string Name { get; set; }

        [Description("DATE")]
        public DateTime Date { get; set; }

        [Description("TYPEMESSAGE")]
        public string TypeMessage { get; set; }

        [Description("TITLE")]
        public string Title { get; set; }

        [Description("MESSAGE")]
        public string Message { get; set; }

        [Description("DETAIL")]
        public string Detail { get; set; }

        [Description("PRIORITY")]
        public int Priority { get; set; }

        [Description("FROMID")]
        public string FromID { get; set; }

        public string ToUserCode { get; set; }

        [Description("TOID")]
        public string ToID { get; set; }

        [Description("REFERENCEIDS")]
        public string RefenceIDs { get; set; }

        [Description("NOTIFYLEVEL")]
        public string NotifyLevel { get; set; }

        [Description("STATUS")]
        public int Status { get; set; }

        public Notification()
        {
            var today = DateTime.Now;
            this.ID = Guid.NewGuid().ToString();
            this.Date = today;
            this.Name = "Noti_" + today.ToString("yyyy-MM-dd");
            this.TypeMessage = "warning";
            this.Detail = "";
            this.Priority = 1;
            this.FromID = "a0ee1dbe-6eaa-48f6-8b32-e449cf189fd6";
            this.RefenceIDs = null;
            this.ToID = null;
            this.ToUserCode = null;
            this.NotifyLevel = "12";
            this.Status = 1;
        }

    }
}
