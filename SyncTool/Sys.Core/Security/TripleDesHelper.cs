﻿using System.Security.Cryptography;
using System.Text;
using System;
using log4net;

namespace Sys.Core.Security
{
    public static class TripleDesHelper
    {
        static readonly ILog logger = LogManager.GetLogger("Sys.Core.Security.TripleDesHelper");
        private const string TripleDesHelper_Key = "TripleDesHelper_Key";
        private const string TripleDesHelper_UseHashing = "TripleDesHelper_UseHashing";


        public static string TripleDesEncrypt(this string content, string key, bool useHashing = true)
        {
            return Encrypt(key, content, useHashing);
        }

        /// <summary>
        /// Ma hoa Tripdes thong thuong
        /// </summary>
        /// <param name="key"></param>
        /// <param name="content"></param>
        /// <param name="useHashing"></param>
        /// <returns></returns>
        public static string Encrypt(string key, string content, bool useHashing = true)
        {
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(content);

            byte[] keyArray = GetKeyArray(key, useHashing);
            using (var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
            {
                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return System.Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
        }

        /// <summary>
        /// Dung de giai ma khi value ma hoa tu ham Encrypt
        /// </summary>
        /// <param name="key"></param>
        /// <param name="content"></param>
        /// <param name="useHashing"></param>
        /// <returns></returns>
        public static string Decrypt(string key, string content, bool useHashing = true)
        {
            try
            {
                byte[] toEncryptArray = System.Convert.FromBase64String(content);
                byte[] keyArray = GetKeyArray(key, useHashing);
                using (var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform cTransform = tdes.CreateDecryptor();
                    byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                    return Encoding.UTF8.GetString(resultArray);
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Ma hoa Trip des voi key dung option 24 byte (ios)
        /// </summary>
        /// <param name="toEncrypt"></param>
        /// <param name="key"></param>
        /// <param name="useHashing"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string Encrypt24(string toEncrypt, string key, bool useHashing = true, CipherMode mode = CipherMode.ECB)
        {
            try
            {
                byte[] keyArray;
                var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

                if (useHashing)
                {
                    var md5 = Sys.Core.Security.Encrypt.MD5(key);
                    var buffer = Encoding.UTF8.GetBytes(md5);
                    keyArray = new byte[24];
                    for (int i = 0; i < keyArray.Length; i++)
                        keyArray[i] = buffer[i];
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = mode, Padding = PaddingMode.PKCS7 };

                var cTransform = tdes.CreateEncryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return string.Empty;
            }
        }
        /// <summary>
        /// Giai ma cho ham Encrypt24
        /// </summary>
        /// <param name="toDecrypt"></param>
        /// <param name="key"></param>
        /// <param name="useHashing"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string Decrypt24(string toDecrypt, string key, bool useHashing = true, CipherMode mode = CipherMode.ECB)
        {
            try
            {
                byte[] keyArray;
                var toEncryptArray = Convert.FromBase64String(toDecrypt);

                if (useHashing)
                {
                    var md5 = Sys.Core.Security.Encrypt.MD5(key);
                    var buffer = Encoding.UTF8.GetBytes(md5);
                    keyArray = new byte[24];
                    for (int i = 0; i < keyArray.Length; i++)
                        keyArray[i] = buffer[i];
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 };

                var cTransform = tdes.CreateDecryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Encoding.UTF8.GetString(resultArray);
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                return string.Empty;
            }
        }




        /// <summary>
        /// Ma hoa cho option key 32
        /// </summary>
        /// <param name="toEncrypt"></param>
        /// <param name="key"></param>
        /// <param name="useHashing"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string Encrypt32(string toEncrypt, string key, bool useHashing = true, CipherMode mode = CipherMode.ECB)
        {
            try
            {
                byte[] keyArray;
                var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);

                if (useHashing)
                {
                    var md5 = Sys.Core.Security.Encrypt.MD5(key);
                    var buffer = Encoding.UTF8.GetBytes(md5);
                    keyArray = new byte[32];
                    for (int i = 0; i < keyArray.Length; i++)
                        keyArray[i] = buffer[i];
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = mode, Padding = PaddingMode.PKCS7 };

                var cTransform = tdes.CreateEncryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Giai ma cho ham ma hoa Encrypt32
        /// </summary>
        /// <param name="toDecrypt"></param>
        /// <param name="key"></param>
        /// <param name="useHashing"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string Decrypt32(string toDecrypt, string key, bool useHashing = true, CipherMode mode = CipherMode.ECB)
        {
            try
            {
                byte[] keyArray;
                var toEncryptArray = Convert.FromBase64String(toDecrypt);

                if (useHashing)
                {
                    var md5 = Sys.Core.Security.Encrypt.MD5(key);
                    var buffer = Encoding.UTF8.GetBytes(md5);
                    keyArray = new byte[32];
                    for (int i = 0; i < keyArray.Length; i++)
                        keyArray[i] = buffer[i];
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider { Key = keyArray, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 };

                var cTransform = tdes.CreateDecryptor();
                var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return string.Empty;
            }
        }

        

     

        private static byte[] GetKeyArray(string key, bool useHashing = true)
        {
            byte[] keyArray;
            if (useHashing)
            {
                using (var hashmd5 = new MD5CryptoServiceProvider())
                {
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                }
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);
            return keyArray;
        }

    }
}
