﻿using log4net;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Sys.Core.Security
{
    public class Encrypt
    {
        static readonly ILog logger = LogManager.GetLogger("Sys.Core.Security.Encrypt");

        public string GetSha1Signature(string httpMethod, string url, string data, string consumerSecretKey, string requestTokenSecretKey = null)
        {
            var sigBaseString = httpMethod + "&" + Uri.EscapeDataString(url) + "&" + Uri.EscapeDataString(data);
            string signature = GetSignature(sigBaseString, consumerSecretKey, requestTokenSecretKey);

            return signature;
        }

        private string GetSignature(string sigBaseString, string consumerSecretKey, string requestTokenSecretKey = null)
        {
            var signingKey = string.Format("{0}&{1}", consumerSecretKey, !string.IsNullOrEmpty(requestTokenSecretKey) ? requestTokenSecretKey : "");
            var byteKey = Encoding.ASCII.GetBytes(signingKey);

            using (var myhmacsha1 = new HMACSHA1(byteKey))
            {
                byte[] byteArray = Encoding.ASCII.GetBytes(sigBaseString);
                using (var stream = new MemoryStream(byteArray))
                {
                    byte[] hashValue = myhmacsha1.ComputeHash(stream);
                    var hash = Convert.ToBase64String(hashValue);
                    return hash;
                }
            }
        }

        public static string CreateSignRSA(string data, string privateKey)
        {
            //RSACryptoServiceProvider rsaCryptoIPT = new RSACryptoServiceProvider(1024);

            var cpsParameter = new CspParameters { Flags = CspProviderFlags.UseMachineKeyStore };
            var rsaCryptoIpt = new RSACryptoServiceProvider(1024, cpsParameter);

            rsaCryptoIpt.FromXmlString(privateKey);
            return
                Convert.ToBase64String(rsaCryptoIpt.SignData(new ASCIIEncoding().GetBytes(data),
                                                             new SHA1CryptoServiceProvider()));
        }     

        public static string HashString(string value)
        {
            byte[] hash = new MD5CryptoServiceProvider().ComputeHash(Encoding.ASCII.GetBytes(value));
            return hash.Aggregate(string.Empty, (current, b) => current + b.ToString("x2").ToLower());
        }

      

        /// <summary>
        /// thu vien nay md5 voi ma unicode bi sai
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string MD5NoUnicode(string s)
        {
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(s);
            byte[] encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes).ToLower().Replace("-", "");
        }

        /// <summary>
        /// Ma hoa MD5
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string MD5(string data)
        {
            UTF8Encoding encoding1 = new UTF8Encoding();
            MD5CryptoServiceProvider provider1 = new MD5CryptoServiceProvider();
            byte[] buffer1 = encoding1.GetBytes(data);
            byte[] buffer2 = provider1.ComputeHash(buffer1);
            return BitConverter.ToString(buffer2).Replace("-", "").ToLower();
        }
      

        /// <summary>
        /// Encrypt AES-128 khop voi server PHP
        /// </summary>
        /// <param name="data"></param>
        /// <param name="officialKey"></param>
        /// <param name="iv"></param>
        /// <param name="keySize"></param>
        /// <param name="blockSize"></param>
        /// <param name="mode"></param>
        /// <param name="padding"></param>
        /// <returns></returns>
        public static string EncryptAES(string data, string officialKey, string iv, int keySize = 128, int blockSize = 128, CipherMode mode = CipherMode.ECB, PaddingMode padding = PaddingMode.PKCS7)
        {
            var utf8dataBytes = Encoding.UTF8.GetBytes(data);
            var utf8KeyBytes = Encoding.UTF8.GetBytes(officialKey);
            var utf8IVBytes = Encoding.UTF8.GetBytes(iv);

            var keyAES128 = new byte[16];
            var md5Ecrypt = new MD5CryptoServiceProvider();
            var md5HashOfKey = md5Ecrypt.ComputeHash(utf8KeyBytes);
            //Array.Copy(md5HashOfKey, keyAES128, Math.Min(keyAES128.Length, md5HashOfKey.Length));
            var ivAES128 = new byte[16];
            var md5HashOfIV = md5Ecrypt.ComputeHash(utf8IVBytes);
            //Array.Copy(md5HashOfIV, ivAES128, Math.Min(ivAES128.Length, md5HashOfIV.Length));

            byte[] encryptedBlob = null;

            using (var aes = new RijndaelManaged())
            {
                try
                {
                    aes.Padding = padding;
                    aes.Mode = mode;
                    aes.KeySize = keySize;
                    aes.BlockSize = blockSize;
                    aes.IV = md5HashOfIV;
                    aes.Key = md5HashOfKey;
                    var bytes = utf8dataBytes;
                    var cxform = aes.CreateEncryptor();
                    encryptedBlob = cxform.TransformFinalBlock(bytes, 0, bytes.Length);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return String.Empty;
                }
                finally
                {
                    aes.Clear();
                }
            }

            return Convert.ToBase64String(encryptedBlob);
        }
        public static string VTOEncrypt(string key, string data)
        {
            data = data.Trim();
            byte[] keydata = Encoding.ASCII.GetBytes(key);
            string md5String =
                BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(keydata)).Replace("-", "").ToLower();
            byte[] tripleDesKey = Encoding.ASCII.GetBytes(md5String.Substring(0, 24));
            TripleDES tripdes = TripleDES.Create();
            tripdes.Mode = CipherMode.ECB;
            tripdes.Key = tripleDesKey;
            tripdes.GenerateIV();
            var ms = new MemoryStream();
            var encStream = new CryptoStream(ms, tripdes.CreateEncryptor(), CryptoStreamMode.Write);
            encStream.Write(Encoding.ASCII.GetBytes(data), 0, Encoding.ASCII.GetByteCount(data));
            encStream.FlushFinalBlock();
            byte[] cryptoByte = ms.ToArray();
            ms.Close();
            encStream.Close();
            return Convert.ToBase64String(cryptoByte, 0, cryptoByte.GetLength(0)).Trim();
        }

        public static string VTODecrypt(string key, string data)
        {
            byte[] keydata = Encoding.ASCII.GetBytes(key);
            string md5String =
                BitConverter.ToString(new MD5CryptoServiceProvider().ComputeHash(keydata)).Replace("-", "").ToLower();
            byte[] tripleDesKey = Encoding.ASCII.GetBytes(md5String.Substring(0, 24));
            TripleDES tripdes = TripleDES.Create();
            tripdes.Mode = CipherMode.ECB;
            tripdes.Key = tripleDesKey;
            byte[] cryptByte = Convert.FromBase64String(data);
            var ms = new MemoryStream(cryptByte, 0, cryptByte.Length);
            ICryptoTransform cryptoTransform = tripdes.CreateDecryptor();
            var decStream = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Read);
            var read = new StreamReader(decStream);
            return (read.ReadToEnd());
        }
    }
}
