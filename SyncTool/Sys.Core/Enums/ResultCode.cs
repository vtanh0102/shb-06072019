﻿using System;
using System.ComponentModel;

namespace Sys.Core.Enums
{
    [Flags]
    public enum ResultCode
    {

        [Description("Bạn đã thực hiện thành công.")]
        Success = 0,

        [Description("Thực hiện nghiệp vụ không thành công. Xin vui lòng thử lại sau.")]
        Fail = -100,

        [Description("Bạn không thể sử dụng tính năng này.")]
        NotAllow = -200,

        [Description("Dữ liệu lấy về trống.")]
        DataIsNull = -300,

        [Description("Chưa có config kết nối trong hệ thống.")]
        DBConfigNotFound = -400,

        [Description("Kết nối đến Database không thành công.")]
        ConnectToDBNotSuccess = -404,

        [Description("Đã có lỗi trong quá trình xử lý. Xin vui lòng thử lại sau.")]
        Unknown = -999,
    }
}