﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Enums
{
    public enum StatusCompareData
    {
        ERROR = -99,
        NO_CHANGE = -1,
        INSERT = 0,
        UPDATE = 1
    }

    public enum StatusCaculateData
    {
        TRUE = 0,
        FALSE = 1,
    }

    public enum StatusGetData
    {
        TRUE = 0,
        FALSE = 1,
    }

    public enum StatusValidateData
    {
        TRUE = 0,
        FALSE = 1,
    }

    public enum StatusPrepareData
    {
        TRUE = 0,
        FALSE = 1,
    }

    public enum StatusInsertData
    {
        TRUE = 0,
        FALSE = 1,
    }

    public enum StatusTypeInsert
    {
        INSERT = 0,
        UPDATE = 1,
    }
}
