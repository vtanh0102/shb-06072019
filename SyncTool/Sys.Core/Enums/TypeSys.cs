﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Enums
{
    public enum TypeSys
    {
        HUMAN_RESOURCE = 1,
        KPI = 2
    }
}
