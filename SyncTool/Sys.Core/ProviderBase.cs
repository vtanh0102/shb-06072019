﻿using System;

namespace Sys.Core
{
    public class ProviderBase
    {
        protected static T CreateInstance<T>()
        {
            return Activator.CreateInstance<T>();
        }

        protected static T CreateInstance<T>(string args) where T : class
        {
            if (args == null) throw new ArgumentNullException("args");
            return (T)Activator.CreateInstance(typeof(T), args);
        }
    }
}