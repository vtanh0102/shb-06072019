using System;
using System.Runtime.Serialization;

using Sys.Core.Enums;
using Sys.Core.Security;
using Sys.Core.Extensions;

namespace Sys.Core
{    
    public class Result : IResult
    { 
        public ResultCode Code { get; set; }
     
        public string Message { get; set; }

        public Result()
        {
        }
    }

    public class Result<T> : Result, IResult<T>
    {
        public T Data { get; set; }
    }

    public class Result<T, TH> : Result<T>, IResult<T, TH>
    {
        private TH _extensiveInformation;

        public TH ExtensiveInformation
        {
            get { return _extensiveInformation; }
            set { _extensiveInformation = value; }
        }
    }
}