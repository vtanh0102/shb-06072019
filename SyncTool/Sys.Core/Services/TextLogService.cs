﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using Sys.Core.Models;

namespace Sys.Core.Services
{
    public class TextLogService
    {
        private const string LOG_FOLDER = "LOGS";
        private const string FILE_PREFIX = "LOG_";
        private const string FILE_EXTENSION = ".log";
        private const string FILENAME_DATETIME_FORMAT = "yyyy.MM.dd HH.mm.ss";

        public void WriteLog(IEnumerable<LogModel> logs)
        {
            string Filename = FILE_PREFIX + DateTime.Now.ToString(FILENAME_DATETIME_FORMAT) + FILE_EXTENSION;
            List<string> writeTexts = new List<string>();
            foreach (var log in logs)
                writeTexts.Add(LogToString(log));
            WriteLog(Filename, writeTexts);
        }

        public void WriteLog(LogModel log)
        {
            string Filename = FILE_PREFIX + DateTime.Now.ToString(FILENAME_DATETIME_FORMAT) + FILE_EXTENSION;
            List<string> writeTexts = new List<string>();
            writeTexts.Add(LogToString(log));
            WriteLog(Filename, writeTexts);
        }

        public void WriteLog(string Filename, List<string> writeTexts)
        {
            Directory.CreateDirectory(LOG_FOLDER);
            File.WriteAllLines(LOG_FOLDER + "/" + Filename, writeTexts.ToArray());
        }

        protected string LogToString(LogModel logModel)
        {
            return string.Join("; ", new string[] { logModel.Value01, logModel.Value02, logModel.Value03, logModel.Value04, logModel.Value05 });
        }

    }
}
