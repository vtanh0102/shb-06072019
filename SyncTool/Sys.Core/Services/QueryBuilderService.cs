﻿using Sys.Core.Enums;
using Sys.Core.Models;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Services
{
    public class QueryBuilderService
    {
        /// <summary>
        /// Build a query to get data from Source. Query have following structure:
        /// SELECT {SOURCE TABLE COLUMNS}
        /// {0}: SELECT STATEMENT
        /// FROM SOURCE TABLE
        /// {1}: SELECT JOIN STATEMENT
        /// {2}: FILTER JOIN STATEMENT
        /// WHERE
        /// {3}: SELECT FILTER STATEMENT
        /// {4}: FILTER WHERE STATEMENT
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <returns>string Query</returns>
        public string BuildGetDataSourceQuery(TableMapping tableMapping)
        {
            var stringBuilder = new StringBuilder();
            // SELECT SOURCE TABLE COLUMNS
            stringBuilder.AppendLine("SELECT DISTINCT");
            // Thêm Table trước Column để tránh bị trùng tên cột
            var listSourceColumns = tableMapping.SourceTableColumns.ConvertAll(x => tableMapping.SourceTable + "." + x);
            stringBuilder.AppendLine(string.Join(",", listSourceColumns));

            // QUERY SELECT STATEMENT
            foreach (QueryMapping queryMapping in tableMapping.ListQueryMapping)
            {
                if (!string.IsNullOrWhiteSpace(queryMapping.SelectQuery))
                    stringBuilder.AppendLine("," + queryMapping.SelectQuery);
            }
            foreach (string key in tableMapping.SourceTableKey)
            {
                stringBuilder.Append(",");
                stringBuilder.AppendLine(tableMapping.SourceTable + "." + key);
            }

            // Source Table
            stringBuilder.AppendLine("FROM " + tableMapping.SourceTable);

            // Select Join statement
            foreach (QueryMapping queryMapping in tableMapping.ListQueryMapping)
            {
                stringBuilder.AppendLine(queryMapping.JoinQuery);
            }

            // Filter Join statement
            stringBuilder.AppendLine(tableMapping.FilterJoin);

            // WHERE
            stringBuilder.AppendLine("WHERE 1=1");

            // Select filter statement
            foreach (QueryMapping queryMapping in tableMapping.ListQueryMapping)
            {
                if (!string.IsNullOrWhiteSpace(queryMapping.FilterQuery))
                    stringBuilder.AppendLine(queryMapping.JoinQuery);
            }

            // Filter
            if (!string.IsNullOrWhiteSpace(tableMapping.FilterQuery))
                stringBuilder.AppendLine("AND " + tableMapping.FilterQuery);

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Build Query lấy dữ liệu từ TMP Table
        /// </summary>
        /// <param name="tableMapping"></param>
        /// <returns></returns>
        public string BuildGetDataTargetQuery(TableMapping tableMapping)
        {
            string query = "SELECT " + string.Join(",", tableMapping.MapColTarget) +
                    ", " + string.Join(",", tableMapping.TargetTableKey) +
                    " FROM " + tableMapping.TargetTable;
            return query;
        }

        /// <summary>
        /// Build Query so sánh giá trị theo các cột Primary Keys
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tableMapping"></param>
        /// <returns></returns>
        public string BuildCompareDataQuery(DataRow data, TableMapping tableMapping)
        {
            string query = "";
            for (int i = 0; i < tableMapping.SourceTableKey.Count; i++)
            {
                if (i != 0)
                    query += "AND ";
                query += tableMapping.TargetTableKey[i] + " = '" + data[tableMapping.SourceTableKey[i]].ToString() + "' ";
            }
            return query;
        }

        public string BuildInsertTmpQuery(DataRow data, TableMapping tableMapping)
        {
            var listSourceColumn = tableMapping.MapColSource;
            var listTargetColumn = tableMapping.MapColTarget;
            var listSourceKey = tableMapping.SourceTableKey;
            var listTargetKey = tableMapping.TargetTableKey;

            var valueInsert = BuildInsertValue(data, listSourceColumn);
            valueInsert += BuildInsertKey(data, listSourceKey);
            valueInsert = valueInsert.TrimEnd(',');
            var template = "INSERT INTO {0} ({1}, {2}) VALUES ({3})";
            var result = string.Format(template, tableMapping.TargetTable, string.Join(",", listTargetColumn), string.Join(",", listTargetKey), valueInsert);
            return result;
        }

        public string BuildUpdateTmpQuery(DataRow data, TableMapping tableMapping)
        {
            var listSourceColumn = tableMapping.MapColSource;
            var listTargetColumn = tableMapping.MapColTarget;
            var listSourceKey = tableMapping.SourceTableKey;
            var listTargetKey = tableMapping.TargetTableKey;

            var valueUpdate = "";
            for (int i = 0; i < listSourceColumn.Count; i++)
            {
                if (data.IsNull(listSourceColumn[i]))
                    valueUpdate += listTargetColumn[i] + " = NULL,";
                else
                    valueUpdate += listTargetColumn[i] + " = N'" + data[listSourceColumn[i]].ToString().Replace("'", "''") + "',";
            }
            valueUpdate = valueUpdate.TrimEnd(',');
            var template = "UPDATE {0} SET {1} WHERE 1=1 ";
            var sqlUpdate = string.Format(template, tableMapping.TargetTable, valueUpdate);
            for (int i = 0; i < tableMapping.SourceTableKey.Count; i++)
            {
                sqlUpdate += "AND ";
                sqlUpdate += listTargetKey[i] + " = N'" + data[listSourceKey[i]].ToString() + "' ";
            }
            return sqlUpdate;
        }

        public string BuildLogQuery(DataRow data, TableMapping tableMapping, StatusCompareData compareStatus)
        {
            List<string> listSourceColumn = tableMapping.MapColSource;
            List<string> listTargetColumn = tableMapping.MapColTarget;
            List<string> listSourceKey = tableMapping.SourceTableKey;
            List<string> listTargetKey = tableMapping.TargetTableKey;
            string TableLogName = tableMapping.TargetTable.Substring(0, tableMapping.TargetTable.LastIndexOf('_')) + "_LOGS";

            string valueInsert = BuildInsertValue(data, listSourceColumn);
            valueInsert += BuildInsertKey(data, listSourceKey);
            valueInsert = valueInsert.TrimEnd(',');
            string template = "INSERT INTO {0} ({1}, {2}) VALUES ({3})";
            string result = string.Format(template,
                TableLogName,
                "DATE_UPDATED, OPERATION_TYPE," + string.Join(",", listTargetColumn),
                string.Join(",", listTargetKey), 
                "'" + DateTime.Now.ToString("s") + "','" + compareStatus.ToString() + "'," + valueInsert);
            return result;
        }

        private string BuildInsertValue(DataRow data, List<string> listColumns)
        {
            var result = "";
            foreach (string column in listColumns)
            {
                if (data.IsNull(column))
                    result += "NULL,";
                else
                    result += "N'" + data[column].ToString().Replace("'", "''") + "',";
            }
            return result;
        }

        private string BuildInsertKey(DataRow data, List<string> listKey)
        {
            var result = "";
            foreach (string key in listKey)
                result += "N'" + data[key].ToString() + "',";
            return result;
        }
    }
}
