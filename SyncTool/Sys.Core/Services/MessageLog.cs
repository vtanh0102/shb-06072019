﻿using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Exceptions;
using Sys.Core.IServices;
using Sys.Core.Models;
using Sys.Core.Models.Source;
using Sys.Core.Repository.Source;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Services
{
    public class MessageLog : IMessageLog
    {
        private TextLogService textLogService;
        private SyncHisRepository syncHisRepository;
        private SyncErrorsRepository syncErrorsRepository;

        private SyncHis SyncHis { get; set; }
        private List<SyncError> SyncErrors { get; set; }

        public MessageLog(string connectionString)
        {
            textLogService = new TextLogService();
            syncHisRepository = new SyncHisRepository(connectionString);
            syncErrorsRepository = new SyncErrorsRepository(connectionString);

            SyncHis = new SyncHis();
            SyncHis.IsToTemp = false;
            SyncErrors = new List<SyncError>();
        }

        /// <summary>
        /// Tạo SyncHis mới và insert vào DB
        /// </summary>
        public void Insert(string userID, TypeSys typeSys)
        {
            // Insert SyncHis vào DB
            SyncHis = new SyncHis();
            SyncHis.UserID = userID;
            SyncHis.Type = typeSys;

            SyncHis.DateStartTmp = DateTime.Now;
            SyncHis.IsToTemp = true;

            try
            {
                string id = syncHisRepository.Insert(SyncHis);
                SyncHis.ID = Convert.ToInt32(id);
            }
            catch (Exception ex)
            {
                SyncHis.StatusTmp = SyncHisStatus.LOST_CONNECTION;
                WriteLogToText(ex.ToString());
            }
        }

        /// <summary>
        /// Tìm lần đồng bộ mới nhất
        /// </summary>
        public void GetLastestSyncHis()
        {
            try
            {
                SyncHis = syncHisRepository.GetLastest((TypeSys)SyncHis.Type);
                SyncHis.DateStartCrm = DateTime.Now;
                SyncHis.IsToTemp = true;
            }
            catch (Exception ex)
            {
                SyncHis.StatusTmp = SyncHisStatus.LOST_CONNECTION;
                WriteLogToText(ex.ToString());
            }
        }

        /// <summary>
        /// Tìm lần đồng bộ mới nhất chưa đồng
        /// </summary>
        public void GetTodaySync(TypeSys typeSys)
        {
            try
            {
                var unSync = syncHisRepository.GetTodaySync(typeSys);
                if (unSync == null)
                    throw new NoResultFoundException();

                this.SyncHis = unSync;
                this.SyncHis.DateStartCrm = DateTime.Now;
                this.SyncHis.IsToTemp = false;
                syncHisRepository.Update(SyncHis);
            }
            catch(NoResultFoundException nex)
            {
                throw nex;
            }
            catch (Exception ex)
            {
                SyncHis.StatusCrm = SyncHisStatus.LOST_CONNECTION;
                WriteLogToText(ex.ToString());
            }
        }

        /// <summary>
        /// Update thông tin SyncHis và SyncError vào bảng
        /// </summary>
        public void Update()
        {
            if (SyncHis.IsToTemp)
            {
                SyncHis.DateEndTmp = DateTime.Now;
                SyncHis.CountTmp = SyncHis.SyncRecords.ToString() + "/" + SyncHis.TotalRecords.ToString();
                if (SyncErrors.Count == 0)
                    SyncHis.StatusTmp = SyncHisStatus.SUCCESS;
                else
                {
                    var isDisconnect = SyncErrors.FirstOrDefault(c => c.ErrorCode == ResultCode.ConnectToDBNotSuccess);
                    if (isDisconnect == null)
                        SyncHis.StatusTmp = SyncHisStatus.DATA_ERROR;
                    else if (isDisconnect != null && SyncErrors.Count == 1)
                        SyncHis.StatusTmp = SyncHisStatus.LOST_CONNECTION;
                    else
                        SyncHis.StatusTmp = SyncHisStatus.LOST_CONNECTION_AND_ERROR;
                }
            }
            else
            {
                SyncHis.DateEndCrm = DateTime.Now;
                SyncHis.CountCrm = SyncHis.SyncRecords.ToString() + "/" + SyncHis.TotalRecords.ToString();
                if (SyncErrors.Count == 0)
                    SyncHis.StatusCrm = SyncHisStatus.SUCCESS;
                else
                {
                    var isDisconnect = SyncErrors.FirstOrDefault(c => c.ErrorCode == ResultCode.ConnectToDBNotSuccess);
                    if (isDisconnect == null)
                        SyncHis.StatusCrm = SyncHisStatus.DATA_ERROR;
                    else if (isDisconnect != null && SyncErrors.Count == 1)
                        SyncHis.StatusCrm = SyncHisStatus.LOST_CONNECTION;
                    else
                        SyncHis.StatusCrm = SyncHisStatus.LOST_CONNECTION_AND_ERROR;
                }
            }

            try
            {
                syncHisRepository.Update(SyncHis);
                foreach (var SyncError in SyncErrors)
                    syncErrorsRepository.Insert(SyncError);
            }
            catch (Exception ex)
            {
                SyncHis.StatusTmp = SyncHisStatus.LOST_CONNECTION;
                WriteLogToText();
            }
        }

        /// <summary>
        /// Thêm vào tổng records cần sync
        /// </summary>
        /// <param name="records"></param>
        public void AddTotalRecord(int records)
        {
            SyncHis.TotalRecords += records;
        }

        /// <summary>
        /// Thêm vào tổng records được sync thành công
        /// </summary>
        public void IncreaseSyncRecord()
        {
            SyncHis.SyncRecords++;
        }

        public void SetSyncRecord(int records)
        {
            SyncHis.SyncRecords = records;
        }

        /// <summary>
        /// Tạo ra SyncError
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <param name="tableName"></param>
        /// <param name="sourceID"></param>
        public void AddSyncError(Result result, string tableName = null, int? sourceID = null)
        {
            this.SyncErrors.Add(BuildSyncError(result, tableName, sourceID));
            if (result.Code == ResultCode.ConnectToDBNotSuccess)
                WriteLogToText();
        }

        private void WriteLogToText(string message = "")
        {
            var listLog = BuildLogs();
            if (!string.IsNullOrWhiteSpace(message))
                listLog.Add(new LogModel(DateTime.Now, message));
            textLogService.WriteLog(listLog);
            Environment.Exit(0);
        }

        private List<LogModel> BuildLogs()
        {
            List<LogModel> listLog = new List<LogModel>();
            if (SyncHis.IsToTemp)
                listLog.Add(new LogModel(SyncHis.DateStartTmp, "Bắt đầu đồng bộ SHB -> TMP", SyncHis.SyncRecords.ToString() + "/" + SyncHis.TotalRecords.ToString()));
            else
                listLog.Add(new LogModel(SyncHis.DateStartCrm, "Bắt đầu đồng bộ TMP -> CRM", SyncHis.SyncRecords.ToString() + "/" + SyncHis.TotalRecords.ToString()));

            foreach (var error in SyncErrors)
                listLog.Add(new LogModel(error.TableName, error.SourceID.ToString(), error.errorCode.ToString(), error.ErrorMessage, error.StackTrace));

            listLog.Add(new LogModel(DateTime.Now, "Mất kết nối"));

            return listLog;
        }

        private SyncError BuildSyncError(Result result, string tableName = null, int? sourceID = null)
        {
            SyncError syncError = new SyncError();
            syncError.SyncHisID = (this.SyncHis.ID != null) ? this.SyncHis.ID.Value : 0;
            syncError.SyncFrom = SyncHis.IsToTemp ? "SHB -> TMP" : "TMP -> CRM";
            syncError.ErrorCode = result.Code;
            syncError.ErrorMessage = result.Code.GetDescription();
            syncError.StackTrace = result.Message;
            syncError.TableName = tableName;
            syncError.SourceID = sourceID;
            return syncError;
        }

    }
}
