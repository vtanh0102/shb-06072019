﻿using Sys.Core.Enums;
using Sys.Core.Models;
using Sys.Core.Repository;
using Sys.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempToCRM.Repository;

namespace Sys.Core.Services
{
    public class NotificationService
    {
        protected List<Notification> ListNotification { get; set; }
        protected List<NotifyConfig> ListNotifyConfig { get; set; }
        private NotifyConfigRepository notifyConfigRepository;

        private NotificationRepository notificationRepository;
        private UserExtendRepository userExtendRepository;

        public NotificationService(string connectionString, string crmConnectionString)
        {
            notifyConfigRepository = new NotifyConfigRepository(connectionString);
            ListNotifyConfig = new List<NotifyConfig>();

            notificationRepository = new NotificationRepository(crmConnectionString);
            userExtendRepository = new UserExtendRepository(crmConnectionString);
            ListNotification = new List<Notification>();
        }

        /// <summary>
        /// Lấy cấu hình Notification Config mới
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public Result<string> RefreshListConfig(string module)
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            try
            {
                ListNotifyConfig = notifyConfigRepository.GetByModule(module);
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
            }

            ListNotification = new List<Notification>();

            return result;
        }

        public void AddInsertNotification(string name, string posName, string toID, string referenceID = null)
        {
            var listNullNotifyConfig = ListNotifyConfig.Where(c => c.Condition == SyncNotifyCondition.ISNEW).ToList();
            if (listNullNotifyConfig.Count() == 0)
                return;

            ListNotification.Add(BuildNotification(listNullNotifyConfig[0], name, posName, toID, referenceID));
        }

        public void AddNullNotification(DataRow source, string name, string posName, string toID, string referenceID = null)
        {
            // Check case NULL
            var listNullNotifyConfig = ListNotifyConfig.Where(c => c.Condition == SyncNotifyCondition.ISNULL);
            foreach (var notifyConfig in listNullNotifyConfig)
            {
                if (source.IsNull(notifyConfig.Field))
                    ListNotification.Add(BuildNotification(notifyConfig, name, posName, toID, referenceID));
            }
        }

        public void AddNotificationByID(int ID, string name, string posName, string toID, string referenceID = null)
        {
            var notifyConfig = ListNotifyConfig.FirstOrDefault(c => c.ID == ID);
            if (notifyConfig == null)
                return;
            ListNotification.Add(BuildNotification(notifyConfig, name, posName, toID, referenceID));
        }

        /// <summary>
        /// Tạo notification theo Notification Config
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="tableMapping"></param>
        //public void AddUpdateNotification(DataRow source, BaseEntity target)
        //{
        //    // Check case Changed
        //    var listChangedNotifyConfig = ListNotifyConfig.Where(c => c.Condition == SyncNotifyCondition.ISCHANGED);
        //    Dictionary<string, string> listColumnValues = target.GetColumnValue();
        //    foreach (var notifyConfig in listChangedNotifyConfig)
        //    {
        //        string sourceColumn = listColumnValues.MapColSource[index];
        //        string targetColumn = tableMapping.MapColTarget[index];
        //        if (!source[sourceColumn].ToString().Equals(target[targetColumn].ToString()))
        //            ListNotification.Add(BuildNotification(source["FIRST_NAME"] + " " + source["LAST_NAME"], ));
        //    }
        //}

        public void AddCompleteNotification(bool isToTemp, TypeSys typeSys)
        {
            string message = "";
            if (isToTemp)
                if (typeSys == TypeSys.HUMAN_RESOURCE)
                    message = "[SHB-CRM] Đồng bộ nhân sự từ SHB -> TMP thành công";
                else
                    message = "[SHB-CRM] Đồng bộ KPI từ SHB -> TMP thành công";
            else
                if (typeSys == TypeSys.HUMAN_RESOURCE)
                    message = "[TMP-CRM] Đồng bộ nhân sự từ SHB -> TMP thành công";
                else
                    message = "[TMP-CRM] Đồng bộ KPI từ SHB -> TMP thành công";

            Notification notification = new Notification();
            notification.ToID = "3518EB21-2ACA-4F67-8A8B-34C182C02C38";
            notification.TypeMessage = "Information";
            notification.Title = "Sync Complete";
            notification.Message = message;
            ListNotification.Add(notification);
        }

        /// <summary>
        /// Insert toàn bộ dữ liệu vào DB
        /// </summary>
        /// <returns></returns>
        public Result<string> InsertToDB()
        {
            var result = new Result<string>();
            result.Code = ResultCode.Success;

            try
            {
                foreach (var notification in ListNotification)
                {
                    notificationRepository.Insert(notification);
                }
            }
            catch (Exception ex)
            {
                result.Code = ResultCode.ConnectToDBNotSuccess;
                result.Message = ex.ToString();
            }

            return result;
        }

        public Notification BuildNotification(NotifyConfig notifyConfig, string name, string posName, string toID, string referenceID)
        {
            Notification notification = new Notification();
            notification.TypeMessage = notifyConfig.TypeNotify;
            notification.Title = notifyConfig.Module + " changed information";
            notification.Message = string.Format(notifyConfig.MessageTemplate, name, posName);
            notification.RefenceIDs = referenceID;
            notification.ToID = toID;
            return notification;
        }

    }
}
