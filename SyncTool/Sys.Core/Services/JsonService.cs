﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Sys.Core.IServices;

namespace Sys.Core.Services
{
    public class JsonService:IJsonService
    {
        public T ToObject<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public string ToJson(object obj)
        {
            //JsonConvert.SerializeObject(obj, Formatting.None,new IsoDateTimeConverter{DateTimeFormat = "yyyy-MM-dd hh:mm:ss"});
            return JsonConvert.SerializeObject(obj);
        }

        public string ToJson(object obj, string dateTimeFormat)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });
        }
    }
}
