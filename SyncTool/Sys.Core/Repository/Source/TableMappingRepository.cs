﻿using Sys.Core.DBHelper;
using Sys.Core.Exceptions;
using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository
{
    public class TableMappingRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL_TABLE = "SELECT * FROM M_SYS_TABLE_MAPPING WHERE DB_MAPPING_ID = '{0}' ORDER BY EXECUTE_ORDER";
        private const string QUERY_ALL_QUERY = "SELECT * FROM M_SYS_QUERY_MAPPING WHERE TABLE_MAPPING_ID = '{0}'";
        private const string QUERY_ALL_COLUMN_REPLACE = "SELECT * FROM M_SYS_COLUMN_REPLACE WHERE TABLE_MAPPING_ID = '{0}'";

        public TableMappingRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Lấy list dữ liệu Mapping giữa các table
        /// </summary>
        /// <returns>List<TableMapping></returns>
        public List<TableMapping> GetTableMapping(string dbMappingCode)
        {
            var dbHelperConfig = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var tableMappingQuery = String.Format(QUERY_ALL_TABLE, dbMappingCode);
            var tableMappingTable = dbHelperConfig.getDataTable(tableMappingQuery, CommandType.Text);
            if (tableMappingTable.Rows.Count == 0)
                throw new NoResultFoundException();

            List<TableMapping> listTableMapping = new List<TableMapping>();
            foreach (DataRow row in tableMappingTable.Rows)
            {
                TableMapping mapping = convertToTableMapping(dbMappingCode, row);
                listTableMapping.Add(mapping);
            }
            return listTableMapping;
        }

        private TableMapping convertToTableMapping(string dbMappingCode, DataRow dataRow)
        {
            TableMapping mapping = new TableMapping()
            {
                ID = dataRow["ID"].ToString(),
                DBMappingCode = dbMappingCode,
                SourceTable = dataRow["SOURCE_TABLE"].ToString(),
                SourceTableKey = dataRow["SOURCE_TABLE_KEY"].ToString().Split(',').ToList(),
                SourceTableColumns = dataRow["SOURCE_TABLE_COLUMNS"].ToString().Split(',').ToList(),
                TargetTable = dataRow["TARGET_TABLE"].ToString(),
                TargetTableKey = dataRow["MAP_KEY_SOURCE_TO_TARGET"].ToString().Split(',').ToList(),
                MapColSource = dataRow["MAP_COLUMN_SOURCE"].ToString().Split(',').ToList(),
                MapColTarget = dataRow["MAP_COLUMN_TARGET"].ToString().Split(',').ToList(),
                FilterJoin = dataRow["FILTER_JOIN_QUERY"].ToString(),
                FilterQuery = dataRow["FILTER_WHERE_QUERY"].ToString(),
            };
            mapping.ListQueryMapping = GetQueryMapping(mapping.ID);
            mapping.ListColumnReplace = GetColumnReplace(mapping.ID);
            return mapping;
        }

        /// <summary>
        /// Lấy list mapping các column trong bảng
        /// </summary>
        /// <returns>List<QueryMapping></returns>
        private List<QueryMapping> GetQueryMapping(string tableMappingCode)
        {
            var dbHelperConfig = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var queryMappingQuery = String.Format(QUERY_ALL_QUERY, tableMappingCode);
            var queryMappingTable = dbHelperConfig.getDataTable(queryMappingQuery, CommandType.Text);

            List<QueryMapping> listQueryMapping = new List<QueryMapping>();
            foreach (DataRow row in queryMappingTable.Rows)
            {
                QueryMapping mapping = new QueryMapping()
                {
                    ID = row["ID"].ToString(),
                    Description = row["DESCRIPTION"].ToString(),
                    TableMappingID = row["TABLE_MAPPING_ID"].ToString(),
                    SelectQuery = row["SELECT_QUERY"].ToString(),
                    JoinQuery = row["JOIN_QUERY"].ToString(),
                    FilterQuery = row["FILTER_QUERY"].ToString(),
                };
                listQueryMapping.Add(mapping);
            }
            return listQueryMapping;
        }

        /// <summary>
        /// Lấy list replace giá trị các column được định nghĩa
        /// </summary>
        /// <returns>List<QueryMapping></returns>
        private List<ColumnReplace> GetColumnReplace(string tableMappingCode)
        {
            var dbHelperConfig = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var replaceColumnQuery = String.Format(QUERY_ALL_COLUMN_REPLACE, tableMappingCode);
            var replaceColumnTable = dbHelperConfig.getDataTable(replaceColumnQuery, CommandType.Text);

            List<ColumnReplace> listColumnReplace = new List<ColumnReplace>();
            foreach (DataRow row in replaceColumnTable.Rows)
            {
                ColumnReplace replace = new ColumnReplace()
                {
                    ID = row["ID"].ToString(),
                    TableMappingID = row["TABLE_MAPPING_ID"].ToString(),
                    MapColumn = row["MAP_COLUMN"].ToString(),
                    Match = row["MATCH"].ToString(),
                    Replace = row["REPLACE"].ToString(),
                    IsRegex = Convert.ToBoolean(row["IS_REGEX"]),
                };
                listColumnReplace.Add(replace);
            }
            return listColumnReplace;
        }

    }
}
