﻿using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Models.Source;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository.Source
{
    public class SyncErrorsRepository : BaseRepository<SyncError>
    {
        private const string QUERY_ALL = @"SELECT * FROM SYNC_ERRORS";
        private const string QUERY_BY_SYNCHIS_ID = @"SELECT * FROM SYNC_ERRORS WHERE SYNC_HIS_ID = {0}";
        private const string QUERY_BY_ID = @"SELECT * FROM SYNC_ERRORS WHERE ID = {0}";

        public SyncErrorsRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<SyncError> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<SyncError> result = new List<SyncError>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public List<SyncError> GetBySyncHis(int syncHisID)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_SYNCHIS_ID, syncHisID);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            List<SyncError> result = new List<SyncError>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public SyncError GetByID(string ID)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_ID, ID);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            SyncError result = convertFromDataRow(dataTable.Rows[0]);
            return result;
        }

        private SyncError convertFromDataRow(DataRow row)
        {
            SyncError result = new SyncError()
            {
                ID = Convert.ToInt32(row["ID"]),
                SyncHisID = Convert.ToInt32(row["SYNC_HIS_ID"]),
                SyncFrom = row["SYNC_FROM"].ToString(),
                ErrorCode = (ResultCode)row["ERROR_CODE"],
                ErrorMessage = row["ERROR_MESSAGE"].ToString(),
                TableName = row["TABLE_NAME"].ToString(),
                SourceID = NumberUltility.ToNullable(row["SOURCE_ID"]),
            };
            return result;
        }
    }
}
