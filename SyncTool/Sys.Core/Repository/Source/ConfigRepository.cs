﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Exceptions;
using System.Data.SqlClient;

namespace Sys.Core.Repository.Source
{
    public class ConfigRepository
    {
        private string connectionString = "";
        private const string QUERY_ALL = "SELECT * FROM SYNC_CONFIG";
        private const string QUERY_BY_KEY = "SELECT * FROM SYNC_CONFIG WHERE [Key] = '{0}'";
        private const string UPDATE_BY_KEY = "UPDATE SYNC_CONFIG SET Value = N'{1}' WHERE [Key] = '{0}'";

        public ConfigRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Dictionary<string, string> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var table = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (DataRow row in table.Rows)
            {
                result.Add(row["KEY"].ToString(), row["VALUE"].ToString());
            }
            return result;
        }

        public T GetByKey<T>(string key)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            string query = string.Format(QUERY_BY_KEY, key);
            var table = dbHelper.getDataTable(query, CommandType.Text);
            return (T) Convert.ChangeType(table.Rows[0]["VALUE"], typeof(T));
        }

        public void SetByKey(string key, string value)
        {
            try
            {
                var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
                var query = string.Format(UPDATE_BY_KEY, key, value);
                dbHelper.ExecuteNonQuery(query, CommandType.Text);
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case -2:
                    case -1:
                    case 2:
                    case 53:
                        throw new DBConnectException(ex.Message);
                    default:
                        throw ex;
                }
            }
        }

    }
}
