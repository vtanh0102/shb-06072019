﻿using Sys.Core.DBHelper;
using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository
{
    public class NotifyConfigRepository
    {

        private string connectionString = "";
        private const string QUERY_ALL = @"SELECT * FROM SYNC_NOTIFY_CONFIG";
        private const string QUERY_BY_TABLE = @"SELECT * FROM SYNC_NOTIFY_CONFIG WHERE TableName LIKE '{0}'";
        private const string QUERY_BY_MODULE = @"SELECT * FROM SYNC_NOTIFY_CONFIG WHERE Module LIKE '{0}'";

        public NotifyConfigRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<NotifyConfig> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<NotifyConfig> result = new List<NotifyConfig>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertToObject(row));
            return result;
        }

        public List<NotifyConfig> GetByTableName(string tableName)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(string.Format(QUERY_BY_TABLE, tableName), CommandType.Text);

            List<NotifyConfig> result = new List<NotifyConfig>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertToObject(row));
            return result;
        }

        public List<NotifyConfig> GetByModule(string module)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(string.Format(QUERY_BY_MODULE, module), CommandType.Text);

            List<NotifyConfig> result = new List<NotifyConfig>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertToObject(row));
            return result;
        }

        protected NotifyConfig convertToObject(DataRow row)
        {
            NotifyConfig result = new NotifyConfig()
            {
                ID = Convert.ToInt32(row["ID"]),
                TableName = row["TableName"].ToString(),
                Module = row["Module"].ToString(),
                Field = row["Field"].ToString(),
                Condition = (SyncNotifyCondition)(int)row["Condition"],
                TypeNotify = row["TypeNotify"].ToString(),
                MessageTemplate = row["MessageTemplate"].ToString(),
            };
            return result;
        }

    }
}
