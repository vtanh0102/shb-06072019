﻿using Sys.Core.DBHelper;
using Sys.Core.Exceptions;
using Sys.Core.Models;
using Sys.Core.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository
{
    public class DBConnectionRepository
    {

        private string connectionString = "";
        private const string QUERY_ALL = "SELECT * FROM M_SYS_DB_MAPPING WHERE ID = '{0}'";

        public DBConnectionRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Lấy thông tin Connection giữa các DB
        /// </summary>
        /// <param name="dbHelperConfig"></param>
        /// <param name="result">Thay đổi Code của result nếu có lỗi</param>
        /// <returns>DBConnection</returns>
        public DBConnection GetDBConnectionInfo(string connectionCode)
        {
            var dpHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_ALL, connectionCode);
            var table = dpHelper.getDataTable(query, CommandType.Text);
            if (table.Rows.Count == 0)
                throw new NoResultFoundException();

            var row = table.Rows[0];
            DBConnection dbConnection = new DBConnection();
            dbConnection.SourceDBType = (DbProviders)Convert.ToInt32(row["SOURCE_DB_TYPE"]);
            dbConnection.TargetDBType = (DbProviders)Convert.ToInt32(row["TARGET_DB_TYPE"]);

            dbConnection.SourceConnectionString = row["SOURCE_CONNECTION_STRING"].ToString();
            dbConnection.SourcePassword = row["SOURCE_PASSWORD"].ToString();
            if (!string.IsNullOrWhiteSpace(dbConnection.SourcePassword))
            {
                try
                {
                    dbConnection.SourceConnectionString = string.Format(dbConnection.SourceConnectionString, EncryptService.Decrypt(dbConnection.SourcePassword));
                }
                catch (Exception)
                {
                    // Nếu không format được string thì không làm gì cả
                }
            }

            dbConnection.TargetConnectionString = row["TARGET_CONNECTION_STRING"].ToString();
            dbConnection.TargetPassword = row["TARGET_PASSWORD"].ToString();
            if (!string.IsNullOrWhiteSpace(dbConnection.TargetPassword))
            {
                try
                {
                    dbConnection.TargetConnectionString = string.Format(dbConnection.TargetConnectionString, EncryptService.Decrypt(dbConnection.TargetPassword));
                }
                catch (Exception)
                {
                    // Nếu không format được string thì không làm gì cả
                }
            }

            return dbConnection;
        }

    }
}
