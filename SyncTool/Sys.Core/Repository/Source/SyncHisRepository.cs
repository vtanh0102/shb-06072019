﻿using Sys.Core.DBHelper;
using Sys.Core.Enums;
using Sys.Core.Models.Source;
using Sys.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository.Source
{
    public class SyncHisRepository : BaseRepository<SyncHis>
    {
        private const string QUERY_ALL = @"SELECT * FROM SYNC_HIS";
        private const string QUERY_LASTEST = @"SELECT TOP 1 * FROM SYNC_HIS WHERE TYPE = {0} ORDER BY DATE_START_TMP DESC";
        private const string QUERY_TODAY_SYNC = @"SELECT TOP 1 * FROM SYNC_HIS WHERE TYPE = {0} AND CAST(DATE_START_TMP as DATE) = '{1}' ORDER BY DATE_START_TMP DESC";

        public SyncHisRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<SyncHis> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<SyncHis> result = new List<SyncHis>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public SyncHis GetLastest(TypeSys typeSys)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            string query = string.Format(QUERY_LASTEST, (int)typeSys);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            SyncHis result = convertFromDataRow(dataTable.Rows[0]);
            return result;
        }

        public SyncHis GetTodaySync(TypeSys typeSys)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            string query = string.Format(QUERY_TODAY_SYNC, (int)typeSys, DateTime.Now.ToString("yyyy-MM-dd"));
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            SyncHis result = convertFromDataRow(dataTable.Rows[0]);
            if (result.DateEndTmp == null)
                return null;

            return result;
        }


        private SyncHis convertFromDataRow(DataRow row)
        {
            SyncHis result = new SyncHis()
            {
                ID = Convert.ToInt32(row["ID"]),
                UserID = row["USER_ID_FK"].ToString(),
                DateStartTmp = Convert.ToDateTime(row["DATE_START_TMP"]),
                DateEndTmp = DateTimeUltility.ToNullable(row["DATE_END_TMP"]),
                CountTmp = row["COUNT_TMP"].ToString(),
                StatusTmp = row.IsNull("STATUS_TMP") ? (SyncHisStatus?) null : (SyncHisStatus) row["STATUS_TMP"],
                DateStartCrm = DateTimeUltility.ToNullable(row["DATE_START_CRM"]),
                DateEndCrm = DateTimeUltility.ToNullable(row["DATE_END_CRM"]),
                CountCrm = row["COUNT_CRM"].ToString(),
                StatusCrm = row.IsNull("STATUS_CRM") ? (SyncHisStatus?) null : (SyncHisStatus)Convert.ToInt32(row["STATUS_CRM"]),
                Type = row.IsNull("TYPE") ? (TypeSys?)null : (TypeSys)row["TYPE"],
                IPSource = row["IP_SOURCE"].ToString(),
                Browser = row["BROWSER"].ToString(),
            };
            return result;
        }
    }
}
