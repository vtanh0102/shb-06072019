﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Extensions;
using System.Data.SqlClient;
using Sys.Core.Exceptions;

namespace Sys.Core.Repository
{
    public abstract class BaseRepository<T> where T : BaseEntity
    {
        protected string connectionString = "";
        private const string QUERY_INSERT = "INSERT INTO {0} ({1}) VALUES ({2})";
        private const string QUERY_UPDATE = "UPDATE {0} SET {1} WHERE {2} = '{3}'";

        public BaseRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public string Insert(T entity)
        {
            try
            {
                var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);

                var tablename = entity.GetTableName();
                var listValue = entity.GetColumnValue();
                StringBuilder columnBuilder = new StringBuilder(), valueBuilder = new StringBuilder();
                foreach (var pair in listValue)
                {
                    // ID NULL nghĩa là auto increasement
                    if (pair.Key == "ID" && pair.Value == null)
                        continue;

                    columnBuilder.Append(pair.Key + ",");
                    if (pair.Value == null)
                        valueBuilder.Append("NULL,");
                    else
                        valueBuilder.Append("N'" + pair.Value.Replace("'", "''") + "',");
                }

                var insertQuery = string.Format(QUERY_INSERT, tablename,
                                                columnBuilder.ToString().TrimEnd(','),
                                                valueBuilder.ToString().TrimEnd(','));
                string id = dbHelper.ExecuteScaler(insertQuery + ";SELECT SCOPE_IDENTITY()", CommandType.Text).ToString();
                return id;
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case -2: case -1: case 2: case 53:
                        throw new DBConnectException(ex.Message);
                    default:
                        throw ex;
                }
            }
        }

        public void Update(T entity)
        {
            try
            {
                var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);

                var tablename = entity.GetTableName();
                var listValue = entity.GetColumnValue();
                StringBuilder valueBuilder = new StringBuilder();
                string key = "ID";
                foreach (var pair in listValue)
                {
                    if (pair.Key == "ID" || pair.Key == "ID_C")
                    {
                        key = pair.Key;
                        continue;
                    }
                    if (pair.Value == null)
                        valueBuilder.Append(pair.Key + " = NULL,");
                    else
                        valueBuilder.Append(pair.Key + " = N'" + pair.Value.ToString().Replace("'", "''") + "',");
                }
                var updateQuery = string.Format(QUERY_UPDATE, tablename, valueBuilder.ToString().TrimEnd(','), key, listValue[key]);
                dbHelper.ExecuteNonQuery(updateQuery, CommandType.Text).ToString();
            }
            catch (SqlException ex)
            {
                switch (ex.Number)
                {
                    case -2:
                    case -1:
                    case 2:
                    case 53:
                        throw new DBConnectException(ex.Message);
                    default:
                        throw ex;
                }
            }
        }

    }
}