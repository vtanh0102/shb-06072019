﻿using Sys.Core.DBHelper;
using Sys.Core.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using TempToCRM.Entities;

namespace TempToCRM.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        private const string QUERY_ALL = @"SELECT * FROM [USERS]";
        private const string QUERY_BY_ID = @"SELECT * FROM [USERS] WHERE ID = {0}";

        public UserRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<User> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<User> result = new List<User>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        public User GetByID(string ID)
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var query = String.Format(QUERY_BY_ID, ID);
            var dataTable = dbHelper.getDataTable(query, CommandType.Text);

            if (dataTable.Rows.Count == 0)
                return null;

            User result = convertFromDataRow(dataTable.Rows[0]);
            return result;

        }

        private User convertFromDataRow(DataRow dataRow)
        {
            User result = new User()
            {
                ID = dataRow["ID"].ToString(),
                FirstName = dataRow["FIRST_NAME"].ToString(),
                LastName = dataRow["LAST_NAME"].ToString(),
            };
            return result;
        }
    }
}
