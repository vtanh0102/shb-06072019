﻿using Sys.Core.DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sys.Core.Models;
using Sys.Core.Repository;
using TempToCRM.Entities;
using Sys.Core.Utility;

namespace TempToCRM.Repository
{
    public class UserExtendRepository : BaseRepository<UserExtend>
    {
        private const string QUERY_ALL = @"SELECT * FROM [USERS_CSTM]";

        public UserExtendRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<UserExtend> GetAll()
        {
            var dbHelper = new DBHelper_New(this.connectionString, DbProviders.SqlServer);
            var dataTable = dbHelper.getDataTable(QUERY_ALL, CommandType.Text);

            List<UserExtend> result = new List<UserExtend>();
            foreach (DataRow row in dataTable.Rows)
                result.Add(convertFromDataRow(row));
            return result;
        }

        private UserExtend convertFromDataRow(DataRow dataRow)
        {
            UserExtend result = new UserExtend()
            {
                ID = dataRow["ID_C"].ToString(),
                UserCode = dataRow["USER_CODE_C"].ToString(),
                PosCode = dataRow["POS_CODE_C"].ToString(),
                TitleID = dataRow["TITLE_ID_C"].ToString(),
                Seniority = dataRow["SENIORITY_C"].ToString(),
                CurrentPositionWorkTime = DateTimeUltility.ToNullable(dataRow["CURRENT_POSITION_WORK_TIME_C"]),
            };
            return result;
        }
    }
}
