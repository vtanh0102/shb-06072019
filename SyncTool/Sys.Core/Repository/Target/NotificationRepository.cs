﻿using Sys.Core.DBHelper;
using Sys.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Core.Repository
{
    public class NotificationRepository : BaseRepository<Notification>
    {
        private const string QUERY_ALL = @"SELECT * FROM B_NOTIFICATION";

        public NotificationRepository(string connectionString)
            : base(connectionString)
        {
            this.connectionString = connectionString;
        }
    }
}
